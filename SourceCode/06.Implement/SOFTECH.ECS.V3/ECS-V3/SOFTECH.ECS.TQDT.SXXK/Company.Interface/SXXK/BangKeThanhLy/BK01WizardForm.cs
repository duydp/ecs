using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using Company.BLL.SXXK;
using Company.Interface.Report.SXXK;
using Company.Interface.SXXK.BangKeThanhLy;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK01WizardForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKToKhaiNhapCollection bkTKNCollection = new BKToKhaiNhapCollection();
        public ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();

        public BK01WizardForm()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            int i = this.HSTL.getBKToKhaiNhap();
            if (i < 0)
            {
                BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                bk.MaBangKe = "DTLTKN";
                bk.TenBangKe = "DTLTKN";
                bk.STTHang = this.HSTL.BKCollection.Count + 1;
                bk.bkTKNCollection = this.bkTKNCollection;
                this.HSTL.BKCollection.Add(bk);
            }
            else
            {
                this.HSTL.BKCollection[i].bkTKNCollection = this.bkTKNCollection;
            }
            BK02WizardForm bk02 = new BK02WizardForm();
            bk02.MdiParent = this.ParentForm;
            bk02.HSTL = this.HSTL;
            bk02.Show();
            this.Close();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public int KiemTraTruocKhiLuu()
        {
            BKToKhaiXuatCollection bktkxCollection1 = new BKToKhaiXuat().SelectCollectionAll();
            BKToKhaiXuatCollection bktkxCollection2 = this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion;
            foreach (BKToKhaiXuat bk2 in bktkxCollection2)
            {
                foreach (BKToKhaiXuat bk1 in bktkxCollection1)
                {
                    if (bk2.SoToKhai == bk1.SoToKhai && bk2.MaLoaiHinh == bk1.MaLoaiHinh && bk2.NamDangKy == bk1.NamDangKy && bk2.MaHaiQuan == bk1.MaHaiQuan) return 2;

                }
            }
            BKToKhaiNhapCollection bktknCollection1 = new BKToKhaiNhap().SelectCollectionDynamic("UserName != '" + MainForm.EcsQuanTri.Identity.Name + "'", "");
            BKToKhaiNhapCollection bktknCollection2 = this.bkTKNCollection;
            foreach (BKToKhaiNhap bk2 in bktknCollection2)
            {
                foreach (BKToKhaiNhap bk1 in bktknCollection1)
                {
                    if (bk2.SoToKhai == bk1.SoToKhai && bk2.MaLoaiHinh == bk1.MaLoaiHinh && bk2.NamDangKy == bk1.NamDangKy && bk2.MaHaiQuan == bk1.MaHaiQuan) return 1;

                }

            }
            return 0;
        }
        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (this.bkTKNCollection.Count == 0)
            {
                //ShowMessage("Bạn hãy nhập danh sách tờ khai nhập để tạo mới hồ sơ.", false);
                MLMessages("Bạn hãy nhập danh sách tờ khai nhập để tạo mới hồ sơ.", "MSG_THK82", "", false);
            }
            else
            {
                try
                {
                    //int k = KiemTraTruocKhiLuu();
                    //if (k > 0)
                    //{
                    //    if (k == 1)
                    //    {
                    //        if(ShowMessage("Có một số tờ khai nhập đã thuộc về bộ hồ sơ của người dùng khác. \nBạn có muốn tiếp tục không?", true) != "Yes")
                    //        return;

                    //    }
                    //    if (k == 2)
                    //    {
                    //        ShowMessage("Có một số tờ khai xuất đã thuộc về bộ hồ sơ của người dùng khác", false);
                    //        return;
                    //    }

                    //}
                    int i = this.HSTL.getBKToKhaiNhap();
                    if (i < 0)
                    {
                        BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                        bk.MaBangKe = "DTLTKN";
                        bk.TenBangKe = "DTLTKN";
                        bk.STTHang = this.HSTL.BKCollection.Count + 1;
                        bk.bkTKNCollection = this.bkTKNCollection;
                        this.HSTL.BKCollection.Add(bk);
                    }
                    else
                    {
                        this.HSTL.BKCollection[i].bkTKNCollection = this.bkTKNCollection;
                    }
                    this.HSTL.LanThanhLy = new HoSoThanhLyDangKy().GetLanThanhLyMoiNhat(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    if (this.HSTL.InsertUpdateFull())
                    {
                        //ShowMessage("Hồ sơ thanh khoản đã tạo thành công.", false);
                        MLMessages("Hồ sơ thanh khoản đã tạo thành công.", "MSG_THK83", "", false);
                        show_CapNhatHoSoThanhLyForm(this.HSTL);
                        this.Close();
                    }
                    else
                        // ShowMessage("Tạo mới hồ sơ không thành công.", false);
                        MLMessages("Tạo mới hồ sơ không thành công.", "MSG_THK84", "", false);
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                }
            }

        }
        private void show_CapNhatHoSoThanhLyForm(HoSoThanhLyDangKy HSTL)
        {
            Form[] forms = this.ParentForm.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CapNhatHoSoThanhLyForm"))
                {
                    forms[i].Close();
                }
            }
            CapNhatHoSoThanhLyForm capNhatHSTLForm = new CapNhatHoSoThanhLyForm();
            capNhatHSTLForm.HSTL = HSTL;
            capNhatHSTLForm.MdiParent = this.ParentForm;
            capNhatHSTLForm.Show();
        }
        private DateTime getMaxDateBKTKX()
        {
            int i = this.HSTL.getBKToKhaiXuat();
            //this.HSTL.BKCollection[i].LoadChiTietBangKe();
            BKToKhaiXuatCollection bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;
            DateTime dt = bkTKXCollection[0].NgayDangKy;
            for (int j = 1; j < bkTKXCollection.Count; j++)
            {
                if (dt < bkTKXCollection[j].NgayDangKy) dt = bkTKXCollection[j].NgayDangKy;
            }
            return dt;
        }
        private void BK01WizardForm_Load(object sender, EventArgs e)
        {
            dgTKN.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Today;
            dgTKNTL.RootTable.Columns["NgayThucNhap"].DefaultValue = DateTime.Today;
            int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);

            //Hungtq updated, 24/02/2012.
            lblChenhLechNgay.Text = chenhLech.ToString() + " (ngày).";

            //ctrDonViHaiQuan.Ma = this.HSTL.MaHaiQuanTiepNhan;
            DateTime fDay = new DateTime(DateTime.Now.Year - 1, 1, 1);
            ccFromDate.Value = fDay;

            ccToDate.Value = Company.KDT.SHARE.Components.Globals.DenNgay_HSTK;

            DateTime fromDate = ccFromDate.Value;
            DateTime toDate = ccToDate.Value;

            if (toDate > this.getMaxDateBKTKX().AddDays(0 - chenhLech)) toDate = this.getMaxDateBKTKX().AddDays(0 - chenhLech);

            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                toDate = ccToDate.Value;

            this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKNChuaThanhLyByDate(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate, MainForm.EcsQuanTri.Identity.Name, txtTenChuHang.Text);

            int i = this.HSTL.getBKToKhaiNhap();
            if (i >= 0)
            {
                this.bkTKNCollection = this.HSTL.BKCollection[i].bkTKNCollection;
                foreach (BKToKhaiNhap bk in this.bkTKNCollection)
                {
                    this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
                }
            }
            try
            {
                dgTKN.DataSource = this.tkmdCollection;
            }
            catch
            { }
            try
            {
                dgTKNTL.DataSource = this.bkTKNCollection;
            }
            catch { }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ToKhaiMauDichCollection tkmds = new ToKhaiMauDichCollection();
            foreach (GridEXRow row in dgTKN.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkmd = (ToKhaiMauDich)row.DataRow;
                    BKToKhaiNhap bk = new BKToKhaiNhap();
                    bk.MaHaiQuan = tkmd.MaHaiQuan;
                    bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                    bk.NamDangKy = tkmd.NamDangKy;
                    bk.NgayDangKy = tkmd.NgayDangKy;
                    bk.NgayThucNhap = tkmd.NGAY_THN_THX;
                    bk.SoToKhai = tkmd.SoToKhai;
                    bk.UserName = MainForm.EcsQuanTri.Identity.Name;
                    this.bkTKNCollection.Add(bk);
                    tkmds.Add(tkmd);
                }
            }
            foreach (ToKhaiMauDich tkmd in tkmds)
                this.RemoveTKMD(tkmd);
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }

            try
            {
                dgTKNTL.Refetch();
            }
            catch
            {
                dgTKNTL.Refresh();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            BKToKhaiNhapCollection bks = new BKToKhaiNhapCollection();
            foreach (GridEXRow row in dgTKNTL.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    BKToKhaiNhap bk = (BKToKhaiNhap)row.DataRow;
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.MaHaiQuan = bk.MaHaiQuan;
                    tkmd.SoToKhai = bk.SoToKhai;
                    tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                    tkmd.NgayDangKy = bk.NgayDangKy;
                    tkmd.NamDangKy = bk.NamDangKy;
                    tkmd.NGAY_THN_THX = bk.NgayThucNhap;
                    this.tkmdCollection.Add(tkmd);
                    bks.Add(bk);
                }
            }
            foreach (BKToKhaiNhap bk in bks)
                this.RemoveBKTKN(bk);
            try
            {
                dgTKNTL.Refetch();
            }
            catch
            {
                dgTKNTL.Refresh();
            }
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }

        }
        private void RemoveTKMD(ToKhaiMauDich tkmd)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == tkmd.SoToKhai && this.tkmdCollection[i].MaLoaiHinh == tkmd.MaLoaiHinh && this.tkmdCollection[i].NamDangKy == tkmd.NamDangKy && this.tkmdCollection[i].MaHaiQuan == tkmd.MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveTKMD(int SoToKhai, string MaLoaiHinh, short NamDangKy, string MaHaiQuan)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == SoToKhai && this.tkmdCollection[i].MaLoaiHinh == MaLoaiHinh && this.tkmdCollection[i].NamDangKy == NamDangKy && this.tkmdCollection[i].MaHaiQuan == MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveBKTKN(BKToKhaiNhap bk)
        {
            for (int i = 0; i < this.bkTKNCollection.Count; i++)
            {
                if (this.bkTKNCollection[i].SoToKhai == bk.SoToKhai && this.bkTKNCollection[i].MaLoaiHinh == bk.MaLoaiHinh && this.bkTKNCollection[i].NamDangKy == bk.NamDangKy && this.bkTKNCollection[i].MaHaiQuan == bk.MaHaiQuan)
                {
                    this.bkTKNCollection.RemoveAt(i);
                    break;
                }
            }
        }

        private void dgTKN_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }
        private void dgTKNTL_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                tkmd.Load();
                f.TKMD = tkmd;
                f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                f.ShowDialog();
            }
        }

        private void dgTKNTL_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                BKToKhaiNhap bk = (BKToKhaiNhap)e.Row.DataRow;
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.MaHaiQuan = bk.MaHaiQuan;
                tkmd.SoToKhai = bk.SoToKhai;
                tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                tkmd.NgayDangKy = bk.NgayDangKy;
                tkmd.NamDangKy = bk.NamDangKy;
                tkmd.NGAY_THN_THX = bk.NgayThucNhap;
                this.tkmdCollection.Add(tkmd);
            }
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);

            //Hungtq updated, 24/02/2012.
            lblChenhLechNgay.Text = chenhLech.ToString() + " (ngày).";

            DateTime fromDate = ccFromDate.Value;
            DateTime toDate = ccToDate.Value;
            if (fromDate > toDate)
            {
                MLMessages("Từ ngày phải nhỏ hơn hoặc bằng đến ngày.", "MSG_SEA01", "", false);
                return;
            }
            if (toDate > this.getMaxDateBKTKX().AddDays(0 - chenhLech)) toDate = this.getMaxDateBKTKX().AddDays(0 - chenhLech);

            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                toDate = ccToDate.Value;

            this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKNChuaThanhLyByDate(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate, MainForm.EcsQuanTri.Identity.Name, txtTenChuHang.Text);
            foreach (BKToKhaiNhap bk in this.bkTKNCollection)
            {
                this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
            }
            dgTKN.DataSource = this.tkmdCollection;
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }
        }

        private void dgTKN_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["Ngay_THN_THX"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["Ngay_THN_THX"].Text = "";
        }

        private void dgTKNTL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["NgayThucNhap"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucNhap"].Text = "";

        }

        private void dgTKNTL_DeletingRecords(object sender, CancelEventArgs e)
        {
            // if (ShowMessage("Khi bạn xóa các tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
            if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
            {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = dgTKNTL.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {

                    if (i.RowType == RowType.Record)
                    {
                        BKToKhaiNhap bk = (BKToKhaiNhap)i.GetRow().DataRow;
                        if (bk.ID > 0)
                            bk.DeleteFull();
                        ToKhaiMauDich tkmd = new ToKhaiMauDich();
                        tkmd.MaHaiQuan = bk.MaHaiQuan;
                        tkmd.SoToKhai = bk.SoToKhai;
                        tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                        tkmd.NgayDangKy = bk.NgayDangKy;
                        tkmd.NamDangKy = bk.NamDangKy;
                        tkmd.NGAY_THN_THX = bk.NgayThucNhap;
                        this.tkmdCollection.Add(tkmd);
                    }
                }
                try
                {
                    dgTKN.Refetch();
                }
                catch
                {
                    dgTKN.Refresh();
                }
                this.Cursor = Cursors.Default;
            }
        }

        private void dgTKN_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgTKN.GetRow();
            ToKhaiMauDich TKMD = (ToKhaiMauDich)row.DataRow;
            DateTime temp = TKMD.NGAY_THN_THX;
            TKMD.Load();
            TKMD.NGAY_THN_THX = temp;
            TKMD.Update();
        }

        private void dgTKNTL_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgTKNTL.GetRow();
            BKToKhaiNhap bk = (BKToKhaiNhap)row.DataRow;
            bk.Update();
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.SoToKhai = bk.SoToKhai;
            TKMD.MaLoaiHinh = bk.MaLoaiHinh;
            TKMD.MaHaiQuan = bk.MaHaiQuan;
            TKMD.NamDangKy = bk.NamDangKy;
            TKMD.Load();
            TKMD.NGAY_THN_THX = bk.NgayThucNhap;
            TKMD.Update();
        }


        private void btnGoTo_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTKN.FindAll(dgTKN.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, Convert.ToInt64(txtSoToKhai.Value)) > 0)
                {

                    foreach (GridEXSelectedItem item in dgTKN.SelectedItems)
                        item.GetRow().IsChecked = true;
                }
                else
                {
                    MLMessages("Không có tờ khai này trong hệ thống", "MSG_THK79", "", false);
                }
            }
            catch
            { }
        }

        private void btnLayTKN_Click(object sender, EventArgs e)
        {
            int i = this.HSTL.getBKToKhaiXuat();
            BKToKhaiXuatCollection bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;
            string where = " CAST(SoToKhaiXuat AS VARCHAR(5)) + CAST(NamDangKyXuat AS VARCHAR(4)) + MaLoaiHinhXuat IN(";
            for (int j = 0; j < bkTKXCollection.Count; j++)
            {
                if (j < bkTKXCollection.Count - 1)
                    where += "'" + bkTKXCollection[j].SoToKhai + "" + bkTKXCollection[j].NamDangKy + "" + bkTKXCollection[j].MaLoaiHinh + "',";
                else
                    where += "'" + bkTKXCollection[j].SoToKhai + "" + bkTKXCollection[j].NamDangKy + "" + bkTKXCollection[j].MaLoaiHinh + "')";
            }
            DataSet ds = PhanBoToKhaiNhap.SelectDanhSachTKN(where);
            if (ds.Tables[0].Rows.Count < 1)
            {
                ShowMessage(setText(" Không có tờ khai nhập từ bảng phân bổ của tờ khai xuất đã chọn", "No import declaration in the allocation list of this export declaration(s)"), false);
                return;
            }
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (Convert.ToInt32(dr["SoToKhaiNhap"]) > 0)
                    if (!CheckTKNIsExist(Convert.ToInt32(dr["SoToKhaiNhap"]), dr["MaLoaiHinhNhap"].ToString(), Convert.ToInt16(dr["NamDangKyNhap"])))
                    {
                        BKToKhaiNhap bk = new BKToKhaiNhap();
                        ToKhaiMauDich tkmd = new ToKhaiMauDich();
                        tkmd.SoToKhai = bk.SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]);
                        tkmd.MaLoaiHinh = bk.MaLoaiHinh = dr["MaLoaiHinhNhap"].ToString();
                        tkmd.NamDangKy = bk.NamDangKy = Convert.ToInt16(dr["NamDangKyNhap"]);
                        tkmd.MaHaiQuan = bk.MaHaiQuan = dr["MaHaiQuanNhap"].ToString();
                        tkmd.Load();
                        bk.NgayDangKy = tkmd.NgayDangKy;
                        bk.NgayThucNhap = tkmd.NGAY_THN_THX;
                        bk.UserName = MainForm.EcsQuanTri.Identity.Name;
                        this.bkTKNCollection.Add(bk);
                        this.RemoveTKMD(tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NamDangKy, tkmd.MaHaiQuan);
                    }
            }
            try
            {
                dgTKNTL.Refetch();
            }
            catch
            {
                dgTKNTL.Refresh();
            }
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }

        }
        public bool CheckTKNIsExist(int soToKhaiNhap, string maLoaiHinhNhap, short namDangKy)
        {
            foreach (BKToKhaiNhap bk in this.bkTKNCollection)
            {
                if (bk.SoToKhai == soToKhaiNhap && bk.NamDangKy == namDangKy && bk.MaLoaiHinh == maLoaiHinhNhap)
                    return true;
            }
            return false;
        }

        private void btnPhanBoTuDong_Click(object sender, EventArgs e)
        {
            CheckSanPhamChuaCoDinhMuc();
            if (dgTKNTL.RowCount > 0)
            {
                string str = "Chương trình sẽ tự động bỏ ra danh sách tờ khai nhập đã chọn vào thanh khoản.";
                str += "\nBạn có muốn tiếp tục không ?";
                if (ShowMessage(str, true) == "No")
                {
                    return;
                }
            }
            this.autoRemoveBKTKN();
            BangKeHoSoThanhLyCollection bkhscoll = HSTL.BKCollection;
            DateTime fromDate = ccFromDate.Value;
            DateTime toDate = ccToDate.Value;

            int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
            if (toDate > this.getMaxDateBKTKX().AddDays(0 - chenhLech)) toDate = this.getMaxDateBKTKX().AddDays(0 - chenhLech);

            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                toDate = ccToDate.Value;

            DataSet dsTKTon = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKNTonChuaThanhLyByDateAndTenChuHang(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate, MainForm.EcsQuanTri.Identity.Name, txtTenChuHang.Text);
            frmDispInfo obj = new frmDispInfo(bkhscoll, dsTKTon);
            obj.ShowDialog();
            this.autoAddBKTKN(obj.DSTKNTL);
            //this.ThucHienPhanBo();
        }

        private void CheckSanPhamChuaCoDinhMuc()
        {
            int i = this.HSTL.getBKToKhaiXuat();
            BKToKhaiXuatCollection bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;
            string where = "";
            foreach (BKToKhaiXuat bkTKX in bkTKXCollection)
            {
                where += "'" + bkTKX.SoToKhai + bkTKX.MaLoaiHinh + bkTKX.NamDangKy + "',";
            }
            where = where.Remove(where.Length - 1);
            DataSet ds = this.HSTL.GetDSSPChuaCoDM(where);
            if (ds.Tables[0].Rows.Count > 0)
            {
                //   ShowMessage("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", false);
                MLMessages("Có " + ds.Tables[0].Rows.Count + " mặt hàng chưa có định mức.", "MSG_THK94", "", false);
                SanPhamChuaCoDMForm f = new SanPhamChuaCoDMForm();
                f.ds = ds;
                f.ShowDialog();
            }
        }

        //private void ThucHienPhanBo()
        //{
        //    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
        //    BangKeHoSoThanhLyCollection bkhscoll = HSTL.BKCollection;
        //    BKToKhaiXuatCollection BKTKXColl = new BKToKhaiXuatCollection();
        //    DateTime fromDate = ccFromDate.Value;
        //    DateTime toDate = ccToDate.Value;
        //    ToKhaiMauDichCollection DSTKN = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKNChuaThanhLyByDate2(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate, MainForm.EcsQuanTri.Identity.Name, txtTenChuHang.Text);
        //    ToKhaiMauDichCollection DSTKNTL = new ToKhaiMauDichCollection();
        //    foreach (BangKeHoSoThanhLy bkhs in bkhscoll)
        //    {
        //        if (bkhs.MaBangKe == "DTLTKX")
        //        {
        //            BKTKXColl.AddRange(bkhs.bkTKXColletion);
        //        }
        //    }

        //    HangMauDichCollection HMDColl = new HangMauDichCollection();
        //    foreach (BKToKhaiXuat bk in BKTKXColl)
        //    {
        //        ToKhaiMauDich TKX = new ToKhaiMauDich();
        //        TKX.SoToKhai = bk.SoToKhai;
        //        TKX.MaHaiQuan = bk.MaHaiQuan;
        //        TKX.MaLoaiHinh = bk.MaLoaiHinh;
        //        TKX.NamDangKy = bk.NamDangKy;
        //        if (TKX.Load())
        //        {
        //            TKX.LoadHMDCollection();

        //            /**
        //             * Nguyen Phu lieu
        //             * */
        //            if (TKX.Xuat_NPL_SP == "N")
        //            {
        //                for (int i0 = 0; i0 < TKX.HMDCollection.Count; i0++)
        //                {
        //                    HangMauDich hang = TKX.HMDCollection[i0];

        //                    int i1 = 0;
        //                    while (hang.SoLuong > 0 && i1 < DSTKNTL.Count)
        //                    {
        //                        if (DSTKNTL[i1].NGAY_THN_THX < TKX.NgayDangKy)
        //                        {
        //                            DSTKNTL[i1].LoadHMDCollection();
        //                            for (int i2 = 0; i2 < DSTKNTL[i1].HMDCollection.Count; i2++)
        //                            {
        //                                if (hang.MaPhu.ToUpper().Trim() == DSTKNTL[i1].HMDCollection[i2].MaPhu.ToUpper().Trim())
        //                                {
        //                                    if (DSTKNTL[i1].HMDCollection[i2].SoLuong > hang.SoLuong)
        //                                    {
        //                                        DSTKNTL[i1].HMDCollection[i2].SoLuong -= hang.SoLuong;
        //                                        hang.SoLuong = 0;
        //                                    }
        //                                    else
        //                                    {
        //                                        hang.SoLuong -= DSTKNTL[i1].HMDCollection[i2].SoLuong;
        //                                        DSTKNTL[i1].HMDCollection[i2].SoLuong = 0;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        i1++;
        //                    }
        //                    i1 = 0;
        //                    while (hang.SoLuong > 0 && i1 < DSTKN.Count)
        //                    {
        //                        if (!this.checkExist(DSTKN[i1], DSTKNTL) && DSTKN[i1].NGAY_THN_THX < TKX.NgayDangKy)
        //                        {
        //                            DSTKN[i1].LoadHMDCollection();
        //                            for (int i2 = 0; i2 < DSTKN[i1].HMDCollection.Count; i2++)
        //                            {
        //                                if (hang.MaPhu.ToUpper().Trim() == DSTKN[i1].HMDCollection[i2].MaPhu.ToUpper().Trim())
        //                                {
        //                                    Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
        //                                    nplTon.SoToKhai = DSTKN[i1].HMDCollection[i2].SoToKhai;
        //                                    nplTon.MaLoaiHinh = DSTKN[i1].HMDCollection[i2].MaLoaiHinh;
        //                                    nplTon.NamDangKy = DSTKN[i1].HMDCollection[i2].NamDangKy;
        //                                    nplTon.MaHaiQuan = DSTKN[i1].HMDCollection[i2].MaHaiQuan;
        //                                    nplTon.MaNPL = DSTKN[i1].HMDCollection[i2].MaPhu;
        //                                    if (nplTon.Load())
        //                                    {
        //                                        DSTKN[i1].HMDCollection[i2].SoLuong = nplTon.Ton;
        //                                        if (DSTKN[i1].HMDCollection[i2].SoLuong > hang.SoLuong)
        //                                        {
        //                                            DSTKN[i1].HMDCollection[i2].SoLuong -= hang.SoLuong;
        //                                            hang.SoLuong = 0;
        //                                        }
        //                                        else
        //                                        {
        //                                            hang.SoLuong -= DSTKN[i1].HMDCollection[i2].SoLuong;
        //                                            DSTKN[i1].HMDCollection[i2].SoLuong = 0;
        //                                        }
        //                                        DSTKNTL.Add(DSTKN[i1]);
        //                                        //this.autoAddBKTKN(DSTKN[i1]);
        //                                        //this.autoRemoveTKN(DSTKN[i1]);
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        i1++;
        //                    }
        //                }
        //            }
        //            /**
        //             * San pham
        //             * */
        //            else
        //            {
        //                HangMauDichCollection HMDCollTemp = TKX.HMDCollection;
        //                foreach (HangMauDich hmd in HMDCollTemp)
        //                {
        //                    DataSet ds = Company.BLL.SXXK.DinhMuc.getDinhMucOfSanPham(hmd.MaPhu, hmd.MaHaiQuan, GlobalSettings.MA_DON_VI, hmd.SoLuong, GlobalSettings.SoThapPhan.LuongNPL);
        //                    for (int i0 = 0; i0 < ds.Tables[0].Rows.Count; i0++)
        //                    {
        //                        DataRow dr = ds.Tables[0].Rows[i0];
        //                        int i1 = 0;
        //                        while (Decimal.Parse(dr["SoLuong"].ToString()) > 0 && i1 < DSTKNTL.Count)
        //                        {
        //                            if (DSTKNTL[i1].NGAY_THN_THX < TKX.NGAY_THN_THX)
        //                            {
        //                                DSTKNTL[i1].LoadHMDCollection();
        //                                for (int i2 = 0; i2 < DSTKNTL[i1].HMDCollection.Count; i2++)
        //                                {
        //                                    if (DSTKNTL[i1].HMDCollection[i2].MaPhu.ToUpper().Trim() == dr["MaNguyenPhuLieu"].ToString().ToUpper().Trim())
        //                                    {
        //                                        if (DSTKNTL[i1].HMDCollection[i2].SoLuong > Decimal.Parse(dr["SoLuong"].ToString()))
        //                                        {
        //                                            DSTKNTL[i1].HMDCollection[i2].SoLuong -= Decimal.Parse(dr["SoLuong"].ToString());
        //                                            dr["SoLuong"] = 0;
        //                                        }
        //                                        else
        //                                        {
        //                                            dr["SoLuong"] = Decimal.Parse(dr["SoLuong"].ToString()) - DSTKNTL[i1].HMDCollection[i2].SoLuong;
        //                                            DSTKNTL[i1].HMDCollection[i2].SoLuong = 0;
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            i1++;
        //                        }
        //                        i1 = 0;
        //                        while (Decimal.Parse(dr["SoLuong"].ToString()) > 0 && i1 < DSTKN.Count)
        //                        {
        //                            if (!this.checkExist(DSTKN[i1], DSTKNTL) && DSTKN[i1].NGAY_THN_THX < TKX.NgayDangKy)
        //                            {
        //                                DSTKN[i1].LoadHMDCollection();
        //                                for (int i2 = 0; i2 < DSTKN[i1].HMDCollection.Count; i2++)
        //                                {
        //                                    if (dr["MaNguyenPhuLieu"].ToString().ToUpper().Trim() == DSTKN[i1].HMDCollection[i2].MaPhu.ToUpper().Trim())
        //                                    {
        //                                        Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
        //                                        nplTon.SoToKhai = DSTKN[i1].HMDCollection[i2].SoToKhai;
        //                                        nplTon.MaLoaiHinh = DSTKN[i1].HMDCollection[i2].MaLoaiHinh;
        //                                        nplTon.NamDangKy = DSTKN[i1].HMDCollection[i2].NamDangKy;
        //                                        nplTon.MaHaiQuan = DSTKN[i1].HMDCollection[i2].MaHaiQuan;
        //                                        nplTon.MaNPL = DSTKN[i1].HMDCollection[i2].MaPhu;
        //                                        if (nplTon.Load())
        //                                        {
        //                                            DSTKN[i1].HMDCollection[i2].SoLuong = nplTon.Ton;
        //                                            if (DSTKN[i1].HMDCollection[i2].SoLuong > Decimal.Parse(dr["SoLuong"].ToString()))
        //                                            {
        //                                                DSTKN[i1].HMDCollection[i2].SoLuong -= Decimal.Parse(dr["SoLuong"].ToString());
        //                                                dr["SoLuong"] = 0;
        //                                            }
        //                                            else
        //                                            {
        //                                                dr["SoLuong"] = Decimal.Parse(dr["SoLuong"].ToString()) - DSTKN[i1].HMDCollection[i2].SoLuong;
        //                                                DSTKN[i1].HMDCollection[i2].SoLuong = 0;
        //                                            }
        //                                            DSTKNTL.Add(DSTKN[i1]);
        //                                            //this.autoAddBKTKN(DSTKN[i1]);
        //                                            //this.autoRemoveTKN(DSTKN[i1]);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            i1++;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    this.autoAddBKTKN(DSTKNTL);
        //    this.Cursor = System.Windows.Forms.Cursors.Default;
        //}

        private void autoRemoveBKTKN()
        {
            for (int k = 0; k < dgTKNTL.RowCount; k++)
            {
                BKToKhaiNhap bk = (BKToKhaiNhap)dgTKNTL.GetRow(k).DataRow;
                ToKhaiMauDich tkmd = new ToKhaiMauDich();
                tkmd.MaHaiQuan = bk.MaHaiQuan;
                tkmd.SoToKhai = bk.SoToKhai;
                tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                tkmd.NgayDangKy = bk.NgayDangKy;
                tkmd.NamDangKy = bk.NamDangKy;
                tkmd.NGAY_THN_THX = bk.NgayThucNhap;
                this.tkmdCollection.Add(tkmd);
            }
            bkTKNCollection = new BKToKhaiNhapCollection();
            dgTKNTL.DataSource = bkTKNCollection;
            dgTKN.DataSource = tkmdCollection;
            try
            {
                dgTKNTL.Refetch();
            }
            catch
            {
                dgTKNTL.Refresh();
            }
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }
        }

        private void autoAddBKTKN(ToKhaiMauDichCollection tkCol)
        {
            foreach (ToKhaiMauDich tk in tkCol)
            {
                this.autoRemoveTKN(tk);
                this.autoAddBK(tk);
            }
        }

        private void autoAddBK(ToKhaiMauDich tk)
        {
            BKToKhaiNhap bk = new BKToKhaiNhap();
            bk.MaHaiQuan = tk.MaHaiQuan;
            bk.MaLoaiHinh = tk.MaLoaiHinh;
            bk.NamDangKy = tk.NamDangKy;
            bk.NgayDangKy = tk.NgayDangKy;
            bk.NgayThucNhap = tk.NGAY_THN_THX;
            bk.SoToKhai = tk.SoToKhai;
            bk.UserName = MainForm.EcsQuanTri.Identity.Name;
            this.bkTKNCollection.Add(bk);
            try
            {
                dgTKNTL.Refetch();
            }
            catch
            {
                dgTKNTL.Refresh();
            }
        }

        private void autoRemoveTKN(ToKhaiMauDich tk)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == tk.SoToKhai && this.tkmdCollection[i].MaLoaiHinh == tk.MaLoaiHinh
                    && this.tkmdCollection[i].NamDangKy == tk.NamDangKy)
                {
                    this.tkmdCollection.RemoveAt(i);
                    break;
                }
            }
            try
            {
                dgTKN.Refetch();
            }
            catch
            {
                dgTKN.Refresh();
            }
        }

        private bool checkExist(ToKhaiMauDich tk, ToKhaiMauDichCollection tkCol)
        {
            for (int i = 0; i < tkCol.Count; i++)
            {
                if (tk.MaHaiQuan == tkCol[i].MaHaiQuan && tk.MaLoaiHinh == tkCol[i].MaLoaiHinh
                    && tk.NamDangKy == tkCol[i].NamDangKy && tk.SoToKhai == tkCol[i].SoToKhai)
                    return true;
            }
            return false;
        }
    }
}