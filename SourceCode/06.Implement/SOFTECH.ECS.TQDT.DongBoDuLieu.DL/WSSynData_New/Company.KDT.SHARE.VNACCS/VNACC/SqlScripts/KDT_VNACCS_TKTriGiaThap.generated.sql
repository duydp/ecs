-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]
	@MaLoaiHinh varchar(1),
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSua varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@SoHouseAWB varchar(20),
	@SoMasterAWB varchar(20),
	@SoLuong numeric(12, 4),
	@TrongLuong numeric(14, 4),
	@MaDDLuuKho varchar(7),
	@TenMayBayChoHang nvarchar(12),
	@NgayHangDen datetime,
	@MaCangDoHang varchar(3),
	@MaDDNhanHangCuoiCung varchar(5),
	@MaDiaDiemXepHang varchar(6),
	@TongTriGiaTinhThue numeric(24, 4),
	@MaTTTongTriGiaTinhThue varchar(3),
	@GiaKhaiBao numeric(12, 4),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MoTaHangHoa nvarchar(200),
	@MaNuocXuatXu varchar(2),
	@TriGiaTinhThue numeric(24, 4),
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@TrangThaiXuLy nchar(10),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TKTriGiaThap]
(
	[MaLoaiHinh],
	[SoToKhai],
	[PhanLoaiBaoCaoSua],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[SoHouseAWB],
	[SoMasterAWB],
	[SoLuong],
	[TrongLuong],
	[MaDDLuuKho],
	[TenMayBayChoHang],
	[NgayHangDen],
	[MaCangDoHang],
	[MaDDNhanHangCuoiCung],
	[MaDiaDiemXepHang],
	[TongTriGiaTinhThue],
	[MaTTTongTriGiaTinhThue],
	[GiaKhaiBao],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[MoTaHangHoa],
	[MaNuocXuatXu],
	[TriGiaTinhThue],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@MaLoaiHinh,
	@SoToKhai,
	@PhanLoaiBaoCaoSua,
	@PhanLoaiToChuc,
	@CoQuanHaiQuan,
	@NhomXuLyHS,
	@MaDonVi,
	@TenDonVi,
	@MaBuuChinhDonVi,
	@DiaChiDonVi,
	@SoDienThoaiDonVi,
	@MaDoiTac,
	@TenDoiTac,
	@MaBuuChinhDoiTac,
	@DiaChiDoiTac1,
	@DiaChiDoiTac2,
	@DiaChiDoiTac3,
	@DiaChiDoiTac4,
	@MaNuocDoiTac,
	@SoHouseAWB,
	@SoMasterAWB,
	@SoLuong,
	@TrongLuong,
	@MaDDLuuKho,
	@TenMayBayChoHang,
	@NgayHangDen,
	@MaCangDoHang,
	@MaDDNhanHangCuoiCung,
	@MaDiaDiemXepHang,
	@TongTriGiaTinhThue,
	@MaTTTongTriGiaTinhThue,
	@GiaKhaiBao,
	@PhanLoaiGiaHD,
	@MaDieuKienGiaHD,
	@MaTTHoaDon,
	@TongTriGiaHD,
	@MaPhanLoaiPhiVC,
	@MaTTPhiVC,
	@PhiVanChuyen,
	@MaPhanLoaiPhiBH,
	@MaTTPhiBH,
	@PhiBaoHiem,
	@MoTaHangHoa,
	@MaNuocXuatXu,
	@TriGiaTinhThue,
	@GhiChu,
	@SoQuanLyNoiBoDN,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]
	@ID bigint,
	@MaLoaiHinh varchar(1),
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSua varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@SoHouseAWB varchar(20),
	@SoMasterAWB varchar(20),
	@SoLuong numeric(12, 4),
	@TrongLuong numeric(14, 4),
	@MaDDLuuKho varchar(7),
	@TenMayBayChoHang nvarchar(12),
	@NgayHangDen datetime,
	@MaCangDoHang varchar(3),
	@MaDDNhanHangCuoiCung varchar(5),
	@MaDiaDiemXepHang varchar(6),
	@TongTriGiaTinhThue numeric(24, 4),
	@MaTTTongTriGiaTinhThue varchar(3),
	@GiaKhaiBao numeric(12, 4),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MoTaHangHoa nvarchar(200),
	@MaNuocXuatXu varchar(2),
	@TriGiaTinhThue numeric(24, 4),
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@TrangThaiXuLy nchar(10),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TKTriGiaThap]
SET
	[MaLoaiHinh] = @MaLoaiHinh,
	[SoToKhai] = @SoToKhai,
	[PhanLoaiBaoCaoSua] = @PhanLoaiBaoCaoSua,
	[PhanLoaiToChuc] = @PhanLoaiToChuc,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHS] = @NhomXuLyHS,
	[MaDonVi] = @MaDonVi,
	[TenDonVi] = @TenDonVi,
	[MaBuuChinhDonVi] = @MaBuuChinhDonVi,
	[DiaChiDonVi] = @DiaChiDonVi,
	[SoDienThoaiDonVi] = @SoDienThoaiDonVi,
	[MaDoiTac] = @MaDoiTac,
	[TenDoiTac] = @TenDoiTac,
	[MaBuuChinhDoiTac] = @MaBuuChinhDoiTac,
	[DiaChiDoiTac1] = @DiaChiDoiTac1,
	[DiaChiDoiTac2] = @DiaChiDoiTac2,
	[DiaChiDoiTac3] = @DiaChiDoiTac3,
	[DiaChiDoiTac4] = @DiaChiDoiTac4,
	[MaNuocDoiTac] = @MaNuocDoiTac,
	[SoHouseAWB] = @SoHouseAWB,
	[SoMasterAWB] = @SoMasterAWB,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[MaDDLuuKho] = @MaDDLuuKho,
	[TenMayBayChoHang] = @TenMayBayChoHang,
	[NgayHangDen] = @NgayHangDen,
	[MaCangDoHang] = @MaCangDoHang,
	[MaDDNhanHangCuoiCung] = @MaDDNhanHangCuoiCung,
	[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[MaTTTongTriGiaTinhThue] = @MaTTTongTriGiaTinhThue,
	[GiaKhaiBao] = @GiaKhaiBao,
	[PhanLoaiGiaHD] = @PhanLoaiGiaHD,
	[MaDieuKienGiaHD] = @MaDieuKienGiaHD,
	[MaTTHoaDon] = @MaTTHoaDon,
	[TongTriGiaHD] = @TongTriGiaHD,
	[MaPhanLoaiPhiVC] = @MaPhanLoaiPhiVC,
	[MaTTPhiVC] = @MaTTPhiVC,
	[PhiVanChuyen] = @PhiVanChuyen,
	[MaPhanLoaiPhiBH] = @MaPhanLoaiPhiBH,
	[MaTTPhiBH] = @MaTTPhiBH,
	[PhiBaoHiem] = @PhiBaoHiem,
	[MoTaHangHoa] = @MoTaHangHoa,
	[MaNuocXuatXu] = @MaNuocXuatXu,
	[TriGiaTinhThue] = @TriGiaTinhThue,
	[GhiChu] = @GhiChu,
	[SoQuanLyNoiBoDN] = @SoQuanLyNoiBoDN,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]
	@ID bigint,
	@MaLoaiHinh varchar(1),
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSua varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@SoHouseAWB varchar(20),
	@SoMasterAWB varchar(20),
	@SoLuong numeric(12, 4),
	@TrongLuong numeric(14, 4),
	@MaDDLuuKho varchar(7),
	@TenMayBayChoHang nvarchar(12),
	@NgayHangDen datetime,
	@MaCangDoHang varchar(3),
	@MaDDNhanHangCuoiCung varchar(5),
	@MaDiaDiemXepHang varchar(6),
	@TongTriGiaTinhThue numeric(24, 4),
	@MaTTTongTriGiaTinhThue varchar(3),
	@GiaKhaiBao numeric(12, 4),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MoTaHangHoa nvarchar(200),
	@MaNuocXuatXu varchar(2),
	@TriGiaTinhThue numeric(24, 4),
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@TrangThaiXuLy nchar(10),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TKTriGiaThap] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TKTriGiaThap] 
		SET
			[MaLoaiHinh] = @MaLoaiHinh,
			[SoToKhai] = @SoToKhai,
			[PhanLoaiBaoCaoSua] = @PhanLoaiBaoCaoSua,
			[PhanLoaiToChuc] = @PhanLoaiToChuc,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHS] = @NhomXuLyHS,
			[MaDonVi] = @MaDonVi,
			[TenDonVi] = @TenDonVi,
			[MaBuuChinhDonVi] = @MaBuuChinhDonVi,
			[DiaChiDonVi] = @DiaChiDonVi,
			[SoDienThoaiDonVi] = @SoDienThoaiDonVi,
			[MaDoiTac] = @MaDoiTac,
			[TenDoiTac] = @TenDoiTac,
			[MaBuuChinhDoiTac] = @MaBuuChinhDoiTac,
			[DiaChiDoiTac1] = @DiaChiDoiTac1,
			[DiaChiDoiTac2] = @DiaChiDoiTac2,
			[DiaChiDoiTac3] = @DiaChiDoiTac3,
			[DiaChiDoiTac4] = @DiaChiDoiTac4,
			[MaNuocDoiTac] = @MaNuocDoiTac,
			[SoHouseAWB] = @SoHouseAWB,
			[SoMasterAWB] = @SoMasterAWB,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[MaDDLuuKho] = @MaDDLuuKho,
			[TenMayBayChoHang] = @TenMayBayChoHang,
			[NgayHangDen] = @NgayHangDen,
			[MaCangDoHang] = @MaCangDoHang,
			[MaDDNhanHangCuoiCung] = @MaDDNhanHangCuoiCung,
			[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[MaTTTongTriGiaTinhThue] = @MaTTTongTriGiaTinhThue,
			[GiaKhaiBao] = @GiaKhaiBao,
			[PhanLoaiGiaHD] = @PhanLoaiGiaHD,
			[MaDieuKienGiaHD] = @MaDieuKienGiaHD,
			[MaTTHoaDon] = @MaTTHoaDon,
			[TongTriGiaHD] = @TongTriGiaHD,
			[MaPhanLoaiPhiVC] = @MaPhanLoaiPhiVC,
			[MaTTPhiVC] = @MaTTPhiVC,
			[PhiVanChuyen] = @PhiVanChuyen,
			[MaPhanLoaiPhiBH] = @MaPhanLoaiPhiBH,
			[MaTTPhiBH] = @MaTTPhiBH,
			[PhiBaoHiem] = @PhiBaoHiem,
			[MoTaHangHoa] = @MoTaHangHoa,
			[MaNuocXuatXu] = @MaNuocXuatXu,
			[TriGiaTinhThue] = @TriGiaTinhThue,
			[GhiChu] = @GhiChu,
			[SoQuanLyNoiBoDN] = @SoQuanLyNoiBoDN,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TKTriGiaThap]
		(
			[MaLoaiHinh],
			[SoToKhai],
			[PhanLoaiBaoCaoSua],
			[PhanLoaiToChuc],
			[CoQuanHaiQuan],
			[NhomXuLyHS],
			[MaDonVi],
			[TenDonVi],
			[MaBuuChinhDonVi],
			[DiaChiDonVi],
			[SoDienThoaiDonVi],
			[MaDoiTac],
			[TenDoiTac],
			[MaBuuChinhDoiTac],
			[DiaChiDoiTac1],
			[DiaChiDoiTac2],
			[DiaChiDoiTac3],
			[DiaChiDoiTac4],
			[MaNuocDoiTac],
			[SoHouseAWB],
			[SoMasterAWB],
			[SoLuong],
			[TrongLuong],
			[MaDDLuuKho],
			[TenMayBayChoHang],
			[NgayHangDen],
			[MaCangDoHang],
			[MaDDNhanHangCuoiCung],
			[MaDiaDiemXepHang],
			[TongTriGiaTinhThue],
			[MaTTTongTriGiaTinhThue],
			[GiaKhaiBao],
			[PhanLoaiGiaHD],
			[MaDieuKienGiaHD],
			[MaTTHoaDon],
			[TongTriGiaHD],
			[MaPhanLoaiPhiVC],
			[MaTTPhiVC],
			[PhiVanChuyen],
			[MaPhanLoaiPhiBH],
			[MaTTPhiBH],
			[PhiBaoHiem],
			[MoTaHangHoa],
			[MaNuocXuatXu],
			[TriGiaTinhThue],
			[GhiChu],
			[SoQuanLyNoiBoDN],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@MaLoaiHinh,
			@SoToKhai,
			@PhanLoaiBaoCaoSua,
			@PhanLoaiToChuc,
			@CoQuanHaiQuan,
			@NhomXuLyHS,
			@MaDonVi,
			@TenDonVi,
			@MaBuuChinhDonVi,
			@DiaChiDonVi,
			@SoDienThoaiDonVi,
			@MaDoiTac,
			@TenDoiTac,
			@MaBuuChinhDoiTac,
			@DiaChiDoiTac1,
			@DiaChiDoiTac2,
			@DiaChiDoiTac3,
			@DiaChiDoiTac4,
			@MaNuocDoiTac,
			@SoHouseAWB,
			@SoMasterAWB,
			@SoLuong,
			@TrongLuong,
			@MaDDLuuKho,
			@TenMayBayChoHang,
			@NgayHangDen,
			@MaCangDoHang,
			@MaDDNhanHangCuoiCung,
			@MaDiaDiemXepHang,
			@TongTriGiaTinhThue,
			@MaTTTongTriGiaTinhThue,
			@GiaKhaiBao,
			@PhanLoaiGiaHD,
			@MaDieuKienGiaHD,
			@MaTTHoaDon,
			@TongTriGiaHD,
			@MaPhanLoaiPhiVC,
			@MaTTPhiVC,
			@PhiVanChuyen,
			@MaPhanLoaiPhiBH,
			@MaTTPhiBH,
			@PhiBaoHiem,
			@MoTaHangHoa,
			@MaNuocXuatXu,
			@TriGiaTinhThue,
			@GhiChu,
			@SoQuanLyNoiBoDN,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TKTriGiaThap]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TKTriGiaThap] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaLoaiHinh],
	[SoToKhai],
	[PhanLoaiBaoCaoSua],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[SoHouseAWB],
	[SoMasterAWB],
	[SoLuong],
	[TrongLuong],
	[MaDDLuuKho],
	[TenMayBayChoHang],
	[NgayHangDen],
	[MaCangDoHang],
	[MaDDNhanHangCuoiCung],
	[MaDiaDiemXepHang],
	[TongTriGiaTinhThue],
	[MaTTTongTriGiaTinhThue],
	[GiaKhaiBao],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[MoTaHangHoa],
	[MaNuocXuatXu],
	[TriGiaTinhThue],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TKTriGiaThap]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaLoaiHinh],
	[SoToKhai],
	[PhanLoaiBaoCaoSua],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[SoHouseAWB],
	[SoMasterAWB],
	[SoLuong],
	[TrongLuong],
	[MaDDLuuKho],
	[TenMayBayChoHang],
	[NgayHangDen],
	[MaCangDoHang],
	[MaDDNhanHangCuoiCung],
	[MaDiaDiemXepHang],
	[TongTriGiaTinhThue],
	[MaTTTongTriGiaTinhThue],
	[GiaKhaiBao],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[MoTaHangHoa],
	[MaNuocXuatXu],
	[TriGiaTinhThue],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TKTriGiaThap] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaLoaiHinh],
	[SoToKhai],
	[PhanLoaiBaoCaoSua],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[SoHouseAWB],
	[SoMasterAWB],
	[SoLuong],
	[TrongLuong],
	[MaDDLuuKho],
	[TenMayBayChoHang],
	[NgayHangDen],
	[MaCangDoHang],
	[MaDDNhanHangCuoiCung],
	[MaDiaDiemXepHang],
	[TongTriGiaTinhThue],
	[MaTTTongTriGiaTinhThue],
	[GiaKhaiBao],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[MoTaHangHoa],
	[MaNuocXuatXu],
	[TriGiaTinhThue],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TKTriGiaThap]	

GO

