--KhoCFS
--USE [ECS_TQDT_SXXK_V4_HT_2]
GO

/****** Object:  Table [dbo].[t_KDT_KhoCFS_DangKy]    Script Date: 07/02/2015 19:26:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_KhoCFS_DangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[MaHQ] [varchar](6) NOT NULL,
	[MaKhoCFS] [varchar](20) NOT NULL,
	[GuidStr] [varchar](100) NULL,
	[TrangThaiXuLy] [int] NOT NULL,
 CONSTRAINT [PK_t_KDT_KhoCFS_DangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


--Detail
--USE [ECS_TQDT_SXXK_V4_HT_2]
GO

/****** Object:  Table [dbo].[t_KDT_KhoCFS_Details]    Script Date: 07/02/2015 19:27:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_KhoCFS_Details](
	[TKMD_ID] [bigint] NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[SoToKhai] [numeric](12, 0) NOT NULL,
	[NgayDangKy] [datetime] NOT NULL,
	[MaLoaiHinh] [varchar](6) NOT NULL,
 CONSTRAINT [PK_t_KDT_KhoCFS_Details_1] PRIMARY KEY CLUSTERED 
(
	[TKMD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


--store khoCFS
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_DangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_DangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_DangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_DangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_DangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_DangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_DangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_DangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHQ varchar(6),
	@MaKhoCFS varchar(20),
	@GuidStr varchar(100),
	@TrangThaiXuLy int,
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_KDT_KhoCFS_DangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHQ],
	[MaKhoCFS],
	[GuidStr],
	[TrangThaiXuLy]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHQ,
	@MaKhoCFS,
	@GuidStr,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_Update]
	@ID int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHQ varchar(6),
	@MaKhoCFS varchar(20),
	@GuidStr varchar(100),
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_KhoCFS_DangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHQ] = @MaHQ,
	[MaKhoCFS] = @MaKhoCFS,
	[GuidStr] = @GuidStr,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_InsertUpdate]
	@ID int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHQ varchar(6),
	@MaKhoCFS varchar(20),
	@GuidStr varchar(100),
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_KhoCFS_DangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_KhoCFS_DangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHQ] = @MaHQ,
			[MaKhoCFS] = @MaKhoCFS,
			[GuidStr] = @GuidStr,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_KhoCFS_DangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHQ],
			[MaKhoCFS],
			[GuidStr],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHQ,
			@MaKhoCFS,
			@GuidStr,
			@TrangThaiXuLy
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_KhoCFS_DangKy]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_KhoCFS_DangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHQ],
	[MaKhoCFS],
	[GuidStr],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_KhoCFS_DangKy]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHQ],
	[MaKhoCFS],
	[GuidStr],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_KhoCFS_DangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHQ],
	[MaKhoCFS],
	[GuidStr],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_KhoCFS_DangKy]	

GO

--Store Detail
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_KhoCFS_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_Detail_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_Insert]
	@TKMD_ID bigint,
	@Master_ID bigint,
	@SoToKhai numeric(12, 0),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(6)
AS
INSERT INTO [dbo].[t_KDT_KhoCFS_Details]
(
	[TKMD_ID],
	[Master_ID],
	[SoToKhai],
	[NgayDangKy],
	[MaLoaiHinh]
)
VALUES
(
	@TKMD_ID,
	@Master_ID,
	@SoToKhai,
	@NgayDangKy,
	@MaLoaiHinh
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_Detail_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_Update]
	@TKMD_ID bigint,
	@Master_ID bigint,
	@SoToKhai numeric(12, 0),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(6)
AS

UPDATE
	[dbo].[t_KDT_KhoCFS_Details]
SET
	[Master_ID] = @Master_ID,
	[SoToKhai] = @SoToKhai,
	[NgayDangKy] = @NgayDangKy,
	[MaLoaiHinh] = @MaLoaiHinh
WHERE
	[TKMD_ID] = @TKMD_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_Detail_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_InsertUpdate]
	@TKMD_ID bigint,
	@Master_ID bigint,
	@SoToKhai numeric(12, 0),
	@NgayDangKy datetime,
	@MaLoaiHinh varchar(6)
AS
IF EXISTS(SELECT [TKMD_ID] FROM [dbo].[t_KDT_KhoCFS_Details] WHERE [TKMD_ID] = @TKMD_ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_KhoCFS_Details] 
		SET
			[Master_ID] = @Master_ID,
			[SoToKhai] = @SoToKhai,
			[NgayDangKy] = @NgayDangKy,
			[MaLoaiHinh] = @MaLoaiHinh
		WHERE
			[TKMD_ID] = @TKMD_ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_KhoCFS_Details]
	(
			[TKMD_ID],
			[Master_ID],
			[SoToKhai],
			[NgayDangKy],
			[MaLoaiHinh]
	)
	VALUES
	(
			@TKMD_ID,
			@Master_ID,
			@SoToKhai,
			@NgayDangKy,
			@MaLoaiHinh
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_Detail_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_Delete]
	@TKMD_ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_KhoCFS_Details]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_Detail_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_KhoCFS_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_Detail_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_Load]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TKMD_ID],
	[Master_ID],
	[SoToKhai],
	[NgayDangKy],
	[MaLoaiHinh]
FROM
	[dbo].[t_KDT_KhoCFS_Details]
WHERE
	[TKMD_ID] = @TKMD_ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_Detail_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[TKMD_ID],
	[Master_ID],
	[SoToKhai],
	[NgayDangKy],
	[MaLoaiHinh]
FROM [dbo].[t_KDT_KhoCFS_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_Detail_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_Detail_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TKMD_ID],
	[Master_ID],
	[SoToKhai],
	[NgayDangKy],
	[MaLoaiHinh]
FROM
	[dbo].[t_KDT_KhoCFS_Details]	

GO


----------------------------------------------------------------------------------------
Go

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '21.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('21.4',GETDATE(), N'Cập nhật Kho CFS')
END	

