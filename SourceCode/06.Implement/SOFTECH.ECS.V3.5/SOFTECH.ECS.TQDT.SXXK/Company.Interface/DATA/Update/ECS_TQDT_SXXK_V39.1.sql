GO
 IF OBJECT_ID(N'T_KDT_VNACC_SoThamChieuNoiBo') IS NOT NULL
	DROP TABLE T_KDT_VNACC_SoThamChieuNoiBo
GO

CREATE TABLE T_KDT_VNACC_SoThamChieuNoiBo
(
		ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
		TKMD_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACC_ToKhaiMauDich(ID),
		SoThamChieu NVARCHAR(255) NULL
)

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_DeleteBy_TKMD_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Insert]
	@TKMD_ID bigint,
	@SoThamChieu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_VNACC_SoThamChieuNoiBo]
(
	[TKMD_ID],
	[SoThamChieu]
)
VALUES 
(
	@TKMD_ID,
	@SoThamChieu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThamChieu nvarchar(255)
AS

UPDATE
	[dbo].[T_KDT_VNACC_SoThamChieuNoiBo]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoThamChieu] = @SoThamChieu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThamChieu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_VNACC_SoThamChieuNoiBo] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_VNACC_SoThamChieuNoiBo] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThamChieu] = @SoThamChieu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_VNACC_SoThamChieuNoiBo]
		(
			[TKMD_ID],
			[SoThamChieu]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThamChieu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_VNACC_SoThamChieuNoiBo]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[T_KDT_VNACC_SoThamChieuNoiBo]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_VNACC_SoThamChieuNoiBo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThamChieu]
FROM
	[dbo].[T_KDT_VNACC_SoThamChieuNoiBo]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThamChieu]
FROM
	[dbo].[T_KDT_VNACC_SoThamChieuNoiBo]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoThamChieu]
FROM [dbo].[T_KDT_VNACC_SoThamChieuNoiBo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACC_SoThamChieuNoiBo_SelectAll]



AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThamChieu]
FROM
	[dbo].[T_KDT_VNACC_SoThamChieuNoiBo]	

GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '39.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('39.1',GETDATE(), N'CẬP NHẬT IN ĐỊNH DANH HÀNG HÓA')
END