﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class ThietBi
    {
        #region Properties.

        public long HopDong_ID { set; get; }
        public string Ma { set; get; }
        public string Ten { set; get; }
        public string MaHS { set; get; }
        public string DVT_ID { set; get; }
        public decimal SoLuongDangKy { set; get; }
        public string NuocXX_ID { set; get; }
        public string TinhTrang { set; get; }
        public double DonGia { set; get; }
        public double TriGia { set; get; }
        public string NguyenTe_ID { set; get; }
        public int STTHang { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<ThietBi> ConvertToCollection(IDataReader reader)
        {
            IList<ThietBi> collection = new List<ThietBi>();
            while (reader.Read())
            {
                ThietBi entity = new ThietBi();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static ThietBi Load(long hopDong_ID, string ma)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, ma);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<ThietBi> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<ThietBi> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<ThietBi> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<ThietBi> SelectCollectionBy_HopDong_ID(long hopDong_ID)
        {
            IDataReader reader = SelectReaderBy_HopDong_ID(hopDong_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_HopDong_ID(long hopDong_ID)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_SelectBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_HopDong_ID(long hopDong_ID)
        {
            const string spName = "p_KDT_GC_ThietBi_SelectBy_HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static int InsertThietBi(long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, string nuocXX_ID, string tinhTrang, double donGia, double triGia, string nguyenTe_ID, int sTTHang)
        {
            ThietBi entity = new ThietBi();
            entity.HopDong_ID = hopDong_ID;
            entity.Ma = ma;
            entity.Ten = ten;
            entity.MaHS = maHS;
            entity.DVT_ID = dVT_ID;
            entity.SoLuongDangKy = soLuongDangKy;
            entity.NuocXX_ID = nuocXX_ID;
            entity.TinhTrang = tinhTrang;
            entity.DonGia = donGia;
            entity.TriGia = triGia;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.STTHang = sTTHang;
            return entity.Insert();
        }

        public int Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, TriGia);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<ThietBi> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ThietBi item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateThietBi(long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, string nuocXX_ID, string tinhTrang, double donGia, double triGia, string nguyenTe_ID, int sTTHang)
        {
            ThietBi entity = new ThietBi();
            entity.HopDong_ID = hopDong_ID;
            entity.Ma = ma;
            entity.Ten = ten;
            entity.MaHS = maHS;
            entity.DVT_ID = dVT_ID;
            entity.SoLuongDangKy = soLuongDangKy;
            entity.NuocXX_ID = nuocXX_ID;
            entity.TinhTrang = tinhTrang;
            entity.DonGia = donGia;
            entity.TriGia = triGia;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.STTHang = sTTHang;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_GC_ThietBi_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, TriGia);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<ThietBi> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ThietBi item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateThietBi(long hopDong_ID, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, string nuocXX_ID, string tinhTrang, double donGia, double triGia, string nguyenTe_ID, int sTTHang)
        {
            ThietBi entity = new ThietBi();
            entity.HopDong_ID = hopDong_ID;
            entity.Ma = ma;
            entity.Ten = ten;
            entity.MaHS = maHS;
            entity.DVT_ID = dVT_ID;
            entity.SoLuongDangKy = soLuongDangKy;
            entity.NuocXX_ID = nuocXX_ID;
            entity.TinhTrang = tinhTrang;
            entity.DonGia = donGia;
            entity.TriGia = triGia;
            entity.NguyenTe_ID = nguyenTe_ID;
            entity.STTHang = sTTHang;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, TinhTrang);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, DonGia);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, TriGia);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<ThietBi> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ThietBi item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteThietBi(long hopDong_ID, string ma)
        {
            ThietBi entity = new ThietBi();
            entity.HopDong_ID = hopDong_ID;
            entity.Ma = ma;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_HopDong_ID(long hopDong_ID)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_DeleteBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_GC_ThietBi_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<ThietBi> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (ThietBi item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}