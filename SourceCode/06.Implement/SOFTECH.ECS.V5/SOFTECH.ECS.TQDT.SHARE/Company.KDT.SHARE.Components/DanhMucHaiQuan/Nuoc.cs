﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class Nuoc
    {
        public static List<Nuoc> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<Nuoc> collection = new List<Nuoc>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                Nuoc entity = new Nuoc();
                entity.ID = dataSet.Tables[0].Rows[i]["ID"].ToString();
                entity.Ten = dataSet.Tables[0].Rows[i]["Ten"].ToString();
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
