﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Infragistics.Excel;

namespace Company.Interface.SXXK
{
    public partial class NPL_DM_Over_3_Form : BaseForm
    {
        public DataTable dtDM = new DataTable();

        public NPL_DM_Over_3_Form()
        {
            InitializeComponent();
        }

        private void SanPhamCoDMThuaForm_Load(object sender, EventArgs e)
        {
            this.dgList.DataSource = dtDM;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            //if (e.Row.RowType == RowType.Record)
            //    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets.Add("DMSanPhamCoNPLQua3");
            ws.Columns[0].Width = 5000;
            ws.Columns[1].Width = 5000;
            ws.Columns[2].Width = 10000;
            ws.Columns[3].Width = 5000;
            ws.Columns[4].Width = 5000;
            ws.Columns[5].Width = 5000;
            ws.Columns[6].Width = 5000;
            WorksheetRowCollection wsrc = ws.Rows;
            WorksheetRow wsr0 = ws.Rows[0];
            wsr0.Cells[0].Value = "Mã sản phẩm";
            wsr0.Cells[1].Value = "Mã nguyên phụ liệu";
            wsr0.Cells[2].Value = "Tên nguyên phụ liệu";
            wsr0.Cells[3].Value = "Định mức";
            wsr0.Cells[4].Value = "Tỷ lệ hao hụt";
            wsr0.Cells[5].Value = "Định mức & hao hụt";
            wsr0.Cells[6].Value = "Định mức thực tế";
            DataTable table = this.dtDM;

            for (int i = 0; i < table.Rows.Count; i++)
            {
                WorksheetRow wsr = ws.Rows[i + 1];
                wsr.Cells[0].Value = table.Rows[i]["MaSP"].ToString();
                wsr.Cells[1].Value = table.Rows[i]["MaNPL"].ToString();
                wsr.Cells[2].Value = table.Rows[i]["TenNPL"].ToString();
                wsr.Cells[3].Value = table.Rows[i]["DinhMuc"].ToString();
                wsr.Cells[4].Value = table.Rows[i]["TyLeHaoHut"].ToString();
                wsr.Cells[5].Value = table.Rows[i]["DinhMuc_HaoHut"].ToString();
                wsr.Cells[6].Value = table.Rows[i]["DinhMucThucTe"].ToString();
            }

            saveFileDialog1.FileName =Application.StartupPath+ "\\Danh sach NPL co DM qua 3.xls";
            DialogResult rs = saveFileDialog1.ShowDialog(this);
            if (rs == DialogResult.OK)
            {
                string fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
                System.Diagnostics.Process.Start(fileName);
            }
        }

    }
}