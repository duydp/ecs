using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data.SqlClient;
using Logger;

namespace Company.Interface.Report.SXXK
{
    public partial class BKToKhaiNhap_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 1;
        //private int STTBanLuu = 0;
        private decimal TongTienThueTKTiep = 0;
        //private decimal TongTienThueHoan = 0;
        //private decimal TongLuongSuDung = 0;
        private decimal TongTienThueHoanTatCa = 0;
        //private decimal TongTienThueHoanCuaToKhai = 0;
        //private decimal TongThueNKPhaiNopCuaToKhai = 0;
        //private decimal TongTienThueTKTiepCuaToKhai = 0;
        //private decimal TongThueTKKoThu = 0;
        //private decimal TongTienTKTiep = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        public int NamThanhLy;
        public string SoTK = "SoTK";
        public string TenNPL = "1";
        public string MaDoanhNghiep;
        public decimal tien = 0;
        public decimal tienKT = 0;

        public BKToKhaiNhap_TT38()
        {
            InitializeComponent();
            lblThueTheoDoiTiep.AfterPrint += new EventHandler(lblTongTienThueTKTiep_AfterPrint);
            lblTongThueKhongThu.BeforePrint += new System.Drawing.Printing.PrintEventHandler(lblTongThueKhongThu_BeforePrint);
            lblTongThueLanSauTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(lblTongThueLanSauTK_BeforePrint);
        }

        void lblTongThueLanSauTK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongThueLanSauTK.Text = TongTienThueTKTiep.ToString("N0");
        }

        void lblTongThueKhongThu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongThueKhongThu.Text = TongTienThueHoanTatCa.ToString("N0");
        }

        void lblTongTienThueTKTiep_AfterPrint(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(((DevExpress.XtraReports.UI.XRTableCell)sender).Text))
                TongTienThueTKTiep = TongTienThueTKTiep + System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
        }

        public void BindReport(string where)
        {
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);
            DataTable dataSource = new DataTable();
            dataSource = GetDataSource();

            int i = 0;
            DateTime ngayDangKyNhap = DateTime.Today;
            string soToKhaiNhap = "";
            foreach (DataRow dr in dataSource.Rows)
            {
                
                if (ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == dr["SoToKhaiNhap"].ToString())
                {
                    dr["STT"] = i;
                }
                else
                {
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = dr["SoToKhaiNhap"].ToString();
                    i++;
                    dr["STT"] = i;
                }
            }

            DetailReport.DataSource = dataSource;
            this.GroupHeader1.Visible = false;
            //if (GlobalSettings.MA_DON_VI.Trim() == "0400101556")
            //{
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                //new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
              //new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
              
                //new DevExpress.XtraReports.UI.GroupField("SoHopDongNhap")
            });
            //}
            //else 
            //{
            //    this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            //    new DevExpress.XtraReports.UI.GroupField("STT"),
            //    new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
            //    new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
            //    //new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
            //  // new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
              
            //    new DevExpress.XtraReports.UI.GroupField("SoHopDongNhap")
            //});
            //}
            

            

            lblSTT_G_F.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblSoToKhaiNhap_D.DataBindings.Add("Text", DetailReport.DataSource, "SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", DetailReport.DataSource, "NgayDangKyNhap");
            lblNgayDangKyNhap_G_F.DataBindings.Add("Text", DetailReport.DataSource, "NgayDangKyNhap");
            lblTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
            lblTenDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
            lblLuongNhapTK.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhapTK", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblLuongTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhapTonDauKy", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblLuongNPLSuDung.DataBindings.Add("Text", DetailReport.DataSource, "LuongNPLSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblLuongTonCuoi.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            
            lblTienThueDauKy.DataBindings.Add("Text", DetailReport.DataSource, "ThueTonDauKy",Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblThueKhongThu.DataBindings.Add("Text", DetailReport.DataSource, "ThueKhongThu", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblThueTheoDoiTiep.DataBindings.Add("Text", DetailReport.DataSource, "ThueTheoDoiTiep", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblSo_CTTT_N.DataBindings.Add("Text", DetailReport.DataSource, "SoCTTT_N");
            lblTriGiaHang.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaHangThucXuat", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblPhanBo.DataBindings.Add("Text", DetailReport.DataSource, "GhiChu");

            //if (GlobalSettings.MA_DON_VI.Trim() == "0400101556")
                lblGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDongNhap");
            //else
              //  lblSoHopDong_Group.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDongNhap");

            

            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            if (SoHoSo > 0)
            {
                // khanhhn 20/02/2012 - Xóa Từ Xin Hoàn (Theo yêu cầu của Grozbecker)
                lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                lblSHSTK.Width = 200;
            }
        
            //HQ
            //xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            xrLabel5.Text = GlobalSettings.TieudeNgay;
        }
        private DataTable GetDataSource()
        {
            try
            {

                string spName = "[p_SXXK_BKToKhaiNhap_TT38]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
                db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, NamThanhLy);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                //db.AddInParameter(dbCommand, "@SapXepNgayDangKyTKN", SqlDbType.VarChar, GlobalSettings.SoThapPhan.SapXepTheoTK == 0 ? 0 : 1);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void lblTongTienThueHoan_AfterPrint(object sender, EventArgs e)
        {
            
            if (TenNPL != "" && GlobalSettings.MA_DON_VI.Trim() == "0400101556")
            {
                
                if (!string.IsNullOrEmpty(((DevExpress.XtraReports.UI.XRTableCell)sender).Text))
                    TongTienThueHoanTatCa = TongTienThueHoanTatCa + System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
            }else
                if (!string.IsNullOrEmpty(((DevExpress.XtraReports.UI.XRTableCell)sender).Text))
                    TongTienThueHoanTatCa = TongTienThueHoanTatCa + System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
            try
            {
                
                if (TenNPL != "")
                    tien += System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
                else
                    lblThueKhongThu.Text = "";
                
            }
            catch (Exception)
            {

                //throw;
            }

           
                
        }

        private void lblSoToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (test == "")
            //{
            //    test = lblSoToKhaiNhap.Text;
            //}
            //else if (test != lblSoToKhaiNhap.Text)
            //{
            //    test = lblSoToKhaiNhap.Text;
            //}
            //else
            //    lblSoToKhaiNhap.Text = "";

        }

        private void lblSoToKhai_Group_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string stt = lblSTT_Group.Text;
            //string sotokhai = lblSoToKhai_Group.Text;
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
           
        }

        private void lblSTT_Group_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
            //var x = GetCurrentColumnValue("STT");
            //lblSTT_Group.Text = GetCurrentColumnValue("STT").ToString();
        }

        private void lblSTT_Group_AfterPrint(object sender, EventArgs e)
        {
            //var x = GetCurrentColumnValue("STT");
            //lblSTT_Group.Text = GetCurrentColumnValue("STT").ToString();
        }

        private void lblSoToKhaiNhap_Group_AfterPrint(object sender, EventArgs e)
        {
            SoTK = lblSoToKhaiNhap_Group.Text;
            STT += 1;
        }

        private void lblNgayDangKyNhap_Group_AfterPrint(object sender, EventArgs e)
        {
            STT += 1;
        }

        private void lblSoHopDong_Group_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string[] split = lblSoHopDong_Group.Text.Split('.',',');
            if (split.Length > 0) 
            {
                lblSoHopDong_Group.Text = split[0];
            }
            
        }

        private void lblTenNPL_AfterPrint(object sender, EventArgs e)
        {
            if(TenNPL == "")
            lblTenNPL.Text = "";
        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == lblTenNPL.Text) 
            {
                TenNPL =lblTenNPL.Text= "";

            }            
            else
                TenNPL = lblTenNPL.Text;
        }

        private void lblSoToKhaiNhap_Group_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //SoTK = lbl
        }

        private void lblSoToKhaiNhapG_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if(STT!=0)
                lblSoToKhaiNhapG_F.Text = "TK: "+lblSoToKhaiNhap_D.Text;
            
        }

        private void lblTongThueHoanKhongThu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if(TenNPL=="")
            lblThueKhongThu.Text = "";
            
            lblTongThueHoanKhongThu.Text = tien.ToString("N");
            tien = 0;
        }

        private void lblGhiChu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblGhiChu.Text = "";
            else 
            {
                string[] split = lblGhiChu.Text.Split('.', ',');
                if (split.Length > 0)
                {
                    lblGhiChu.Text = split[0];
                }
            }
            
        }

        private void lblTongThueTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongThueTiep.Text = tienKT.ToString("N");

            tienKT = 0;
        }

        private void lblTenDVT_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL == "")
                lblTenDVT.Text = "";
        }

        private void lblLuongNhapTK_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL == "")
                lblLuongNhapTK.Text = "";
        }

        private void lblLuongTonDauKy_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL == "")
                lblLuongTonDauKy.Text = "";
        }

        private void lblLuongNPLSuDung_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL == "")
                lblLuongNPLSuDung.Text = "";
        }

        private void lblLuongTonCuoi_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL == "")
                lblLuongTonCuoi.Text = "";
        }

        private void lblTenDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblTenDVT.Text = "";
        }

        private void lblLuongNhapTK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblLuongNhapTK.Text = "";
        }

        private void lblLuongTonDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblLuongTonDauKy.Text = "";
        }

        private void lblLuongNPLSuDung_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblLuongNPLSuDung.Text = "";
        }

        private void lblLuongTonCuoi_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblLuongTonCuoi.Text = "";
        }

        private void lblTienThueDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblTienThueDauKy.Text = "";
        }

        private void lblThueTheoDoiTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblThueTheoDoiTiep.Text = "";
        }

        private void lblThueTheoDoiTiep_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL != "")
                tienKT += System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
            else
                lblThueTheoDoiTiep.Text = "";
        }

        private void lblSoToKhaiNhap_D_AfterPrint(object sender, EventArgs e)
        {
            //if()
            //SoTK = lblSoToKhaiNhap_Group.Text;
            //STT += 1;
        }

        private void lblSTT_G_F_AfterPrint(object sender, EventArgs e)
        {
            SoTK = lblSoToKhaiNhap_D.Text;
            STT += 1;
        }

        
      

       
       
    }
}
