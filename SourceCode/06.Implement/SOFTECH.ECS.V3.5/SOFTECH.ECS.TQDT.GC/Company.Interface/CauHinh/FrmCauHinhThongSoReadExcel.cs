﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface.CauHinh
{
    public partial class FrmCauHinhThongSoReadExcel : Company.Interface.BaseForm
    {
        public FrmCauHinhThongSoReadExcel()
        {
            InitializeComponent();
        }
        public int FormatLuongNPL = GlobalSettings.SoThapPhan.LuongNPL;
        public int FormatDonGiaNPL = GlobalSettings.SoThapPhan.LuongNPL;

        public int FormatLuongSP = GlobalSettings.SoThapPhan.LuongSP;
        public int FormatDonGiaSP = GlobalSettings.SoThapPhan.LuongSP;

        public int FormatLuongTB = GlobalSettings.SoThapPhan.LuongNPL;
        public int FormatDonGiaTB = GlobalSettings.SoThapPhan.LuongNPL;

        public int FormatSoLuong = GlobalSettings.SoThapPhan.TLHH;
        public int FormatSoLuong1 = GlobalSettings.SoThapPhan.TLHH;

        public int FormatDonGia = GlobalSettings.SoThapPhan.DinhMuc;
        public int FormatTriGia = GlobalSettings.SoThapPhan.TLHH;

        public int FormatDMSD = GlobalSettings.SoThapPhan.DinhMuc;
        public int FormatTLHH = GlobalSettings.SoThapPhan.TLHH;

        public void SaveConfigXML()
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                #region CẤU HÌNH NPL
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode row = doc.SelectSingleNode("ROOT/NPL/ROWNUMBER");
                row.InnerText = txtRowNPL.Text.ToString();

                XmlNode Code = doc.SelectSingleNode("ROOT/NPL/MA");
                Code.InnerText = txtMaNPL.Text.ToString();

                XmlNode Name = doc.SelectSingleNode("ROOT/NPL/TEN");
                Name.InnerText = txtTenNPL.Text.ToString();

                XmlNode HSCode = doc.SelectSingleNode("ROOT/NPL/MAHS");
                HSCode.InnerText = txtMaHSNPL.Text.ToString();

                XmlNode DVT = doc.SelectSingleNode("ROOT/NPL/DVT");
                DVT.InnerText = txtDVTNPL.Text.ToString();

                XmlNode Quantity = doc.SelectSingleNode("ROOT/NPL/SOLUONG");
                Quantity.InnerText = txtSoLuongNPL.Text.ToString();

                XmlNode UnitPrice = doc.SelectSingleNode("ROOT/NPL/DONGIA");
                UnitPrice.InnerText = txtDonGiaNPL.Text.ToString();

                XmlNode Source = doc.SelectSingleNode("ROOT/NPL/NGUONCUNGCAP");
                Source.InnerText = chkTCU.Checked.ToString();

                XmlNode DecimalSoLuongNPL = doc.SelectSingleNode("ROOT/NPL/SOTHAPPHAN/SOLUONG");
                DecimalSoLuongNPL.InnerText = nmrSoLuongNPL.Value.ToString();

                XmlNode DecimalDonGiaNPL = doc.SelectSingleNode("ROOT/NPL/SOTHAPPHAN/DONGIA");
                DecimalDonGiaNPL.InnerText = nmrDonGiaNPL.Value.ToString();

                #endregion
                #region CẤU HÌNH SP
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowSP = doc.SelectSingleNode("ROOT/SP/ROWNUMBER");
                rowSP.InnerText = txtRowNPL.Text.ToString();

                XmlNode CodeSP = doc.SelectSingleNode("ROOT/SP/MA");
                CodeSP.InnerText = txtMaSP.Text.ToString();

                XmlNode NameSP = doc.SelectSingleNode("ROOT/SP/TEN");
                NameSP.InnerText = txtTenSP.Text.ToString();

                XmlNode HSCodeSP = doc.SelectSingleNode("ROOT/SP/MAHS");
                HSCodeSP.InnerText = txtMaHSSP.Text.ToString();

                XmlNode DVTSP = doc.SelectSingleNode("ROOT/SP/DVT");
                DVTSP.InnerText = txtDVTSP.Text.ToString();

                XmlNode QuantitySP = doc.SelectSingleNode("ROOT/SP/SOLUONG");
                QuantitySP.InnerText = txtSoLuongSP.Text.ToString();

                XmlNode UnitPriceSP = doc.SelectSingleNode("ROOT/SP/DONGIA");
                UnitPriceSP.InnerText = txtDonGiaSP.Text.ToString();

                XmlNode Type = doc.SelectSingleNode("ROOT/SP/LOAISPGC");
                Type.InnerText = txtLoaiSPGC.Text.ToString();

                XmlNode DecimalSoLuongSP = doc.SelectSingleNode("ROOT/SP/SOTHAPPHAN/SOLUONG");
                DecimalSoLuongSP.InnerText = nmrSoLuongSP.Value.ToString();

                XmlNode DecimalDonGiaSP = doc.SelectSingleNode("ROOT/SP/SOTHAPPHAN/DONGIA");
                DecimalDonGiaSP.InnerText = nmrDonGiaSP.Value.ToString();

                #endregion

                #region CẤU HÌNH THIẾT BỊ
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowTB = doc.SelectSingleNode("ROOT/TB/ROWNUMBER");
                rowTB.InnerText = txtRowTB.Text.ToString();

                XmlNode CodeTB = doc.SelectSingleNode("ROOT/TB/MA");
                CodeTB.InnerText = txtMaTB.Text.ToString();

                XmlNode NameTB = doc.SelectSingleNode("ROOT/TB/TEN");
                NameTB.InnerText = txtTenTB.Text.ToString();

                XmlNode HSCodeTB = doc.SelectSingleNode("ROOT/TB/MAHS");
                HSCodeTB.InnerText = txtMaHSTB.Text.ToString();

                XmlNode DVTTB = doc.SelectSingleNode("ROOT/TB/DVT");
                DVTTB.InnerText = txtDVTTB.Text.ToString();

                XmlNode CountryTB = doc.SelectSingleNode("ROOT/TB/XUATXU");
                CountryTB.InnerText = txtXuatXu.Text.ToString();

                XmlNode QuantityTB = doc.SelectSingleNode("ROOT/TB/SOLUONG");
                QuantityTB.InnerText = txtSoLuongTB.Text.ToString();

                XmlNode UnitPriceTB = doc.SelectSingleNode("ROOT/TB/DONGIA");
                UnitPriceTB.InnerText = txtDonGiaTB.Text.ToString();

                XmlNode CurrencyTB = doc.SelectSingleNode("ROOT/TB/NGUYENTE");
                CurrencyTB.InnerText = txtNguyenTe.Text.ToString();

                XmlNode Status = doc.SelectSingleNode("ROOT/TB/TINHTRANG");
                Status.InnerText = txtTinhTrang.Text.ToString();

                XmlNode DecimalSoLuongTB = doc.SelectSingleNode("ROOT/TB/SOTHAPPHAN/SOLUONG");
                DecimalSoLuongTB.InnerText = nmrSoLuongTB.Value.ToString();

                XmlNode DecimalDonGiaTB = doc.SelectSingleNode("ROOT/TB/SOTHAPPHAN/DONGIA");
                DecimalDonGiaTB.InnerText = nmrDonGiaTB.Value.ToString();

                #endregion

                #region CẤU HÌNH ĐỊNH MỨC
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowDM = doc.SelectSingleNode("ROOT/DM/ROWNUMBER");
                rowDM.InnerText = txtRowDM.Text.ToString();

                XmlNode CodeSanPham = doc.SelectSingleNode("ROOT/DM/MASP");
                CodeSanPham.InnerText = txtMaSanPham.Text.ToString();

                XmlNode CodeNPL = doc.SelectSingleNode("ROOT/DM/MANPL");
                CodeNPL.InnerText = txtMaNguyenPhuLieu.Text.ToString();

                XmlNode DMSD = doc.SelectSingleNode("ROOT/DM/DINHMUCSUDUNG");
                DMSD.InnerText = txtDMSD.Text.ToString();

                XmlNode TLHH = doc.SelectSingleNode("ROOT/DM/TLHH");
                TLHH.InnerText = txtTLHH.Text.ToString();

                XmlNode NPLCungUng = doc.SelectSingleNode("ROOT/DM/NPLCUNGUNG");
                NPLCungUng.InnerText = txtNPLCungUng.Text.ToString();

                XmlNode Replace = doc.SelectSingleNode("ROOT/DM/THAYTHE");
                Replace.InnerText = chkOverwrite.Checked.ToString();

                XmlNode DinhDanh = doc.SelectSingleNode("ROOT/DM/MADINHDANH");
                DinhDanh.InnerText = txtMaDD.Text.ToString();

                XmlNode DecimalDMSD = doc.SelectSingleNode("ROOT/DM/SOTHAPPHAN/DMSD");
                DecimalDMSD.InnerText = nmrDMSD.Value.ToString();

                XmlNode DecimalTLHH = doc.SelectSingleNode("ROOT/DM/SOTHAPPHAN/TLHH");
                DecimalTLHH.InnerText = nmrTLHH.Value.ToString();

                #endregion
                doc.Save(Application.StartupPath + "\\ExcelSettingAll.xml");
                ShowMessage("Lưu thành công ", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }           
        }
        public void SaveConfigHangHoa()
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");

                //XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
                //sheet.InnerText = sheetName;

                XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
                row.InnerText = txtRow.Text;

                XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
                code.InnerText = txtMaHangColumn.Text;

                XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
                name.InnerText = txtTenHangColumn.Text;

                XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
                hs.InnerText = txtMaHSColumn.Text;

                XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
                dvt.InnerText = txtDVTColumn.Text;

                XmlNode dvt2 = doc.SelectSingleNode("TSTK/DVT2");
                dvt2.InnerText = txtDVTColumn2.Text;

                XmlNode quan = doc.SelectSingleNode("TSTK/Soluong");
                quan.InnerText = txtSoLuongColumn.Text;

                XmlNode quan2 = doc.SelectSingleNode("TSTK/Soluong2");
                quan2.InnerText = txtSoLuongColumn2.Text;

                XmlNode priceunit = doc.SelectSingleNode("TSTK/Dongia");
                priceunit.InnerText = txtDonGiaColumn.Text;

                XmlNode made = doc.SelectSingleNode("TSTK/Xuatxu");
                made.InnerText = txtXuatXuColumn.Text;

                XmlNode tgtt = doc.SelectSingleNode("TSTK/TriGia");
                tgtt.InnerText = txtTriGiaTT.Text;

                XmlNode tsXNK = doc.SelectSingleNode("TSTK/ThueXNK");
                tsXNK.InnerText = txtBieuThueXNK.Text;

                XmlNode tsGTGT = doc.SelectSingleNode("TSTK/ThueSuat");
                tsGTGT.InnerText = txtThueSuat.Text;

                doc.Save(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }
        public void ReadConfigHangHoa()
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");
                XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
                cbbSheetName.Text = sheet.InnerText;

                XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
                txtRow.Text = row.InnerText;

                XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
                txtMaHangColumn.Text = code.InnerText;

                XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
                txtTenHangColumn.Text = name.InnerText;

                XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
                txtMaHSColumn.Text = hs.InnerText;

                XmlNode made = doc.SelectSingleNode("TSTK/Xuatxu");
                txtXuatXuColumn.Text = made.InnerText;

                XmlNode quan = doc.SelectSingleNode("TSTK/Soluong");
                txtSoLuongColumn.Text = quan.InnerText;

                XmlNode quan2 = doc.SelectSingleNode("TSTK/Soluong2");
                txtSoLuongColumn2.Text = quan.InnerText;

                XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
                txtDVTColumn.Text = dvt.InnerText;

                XmlNode dvt2 = doc.SelectSingleNode("TSTK/DVT2");
                txtDVTColumn2.Text = dvt.InnerText;

                XmlNode priceunit = doc.SelectSingleNode("TSTK/Dongia");
                txtDonGiaColumn.Text = priceunit.InnerText;

                XmlNode tgTT = doc.SelectSingleNode("TSTK/TriGia");
                txtTriGiaTT.Text = tgTT.InnerText;

                XmlNode tsXNK = doc.SelectSingleNode("TSTK/ThueXNK");
                txtBieuThueXNK.Text = tsXNK.InnerText;

                XmlNode tsThueSuat = doc.SelectSingleNode("TSTK/ThueSuat");
                txtThueSuat.Text = tsThueSuat.InnerText;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ReadConfigXML()
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                #region CẤU HÌNH NPL
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode row = doc.SelectSingleNode("ROOT/NPL/ROWNUMBER");
                txtRowNPL.Text = row.InnerText;

                XmlNode Code = doc.SelectSingleNode("ROOT/NPL/MA");
                txtMaNPL.Text = Code.InnerText;

                XmlNode Name = doc.SelectSingleNode("ROOT/NPL/TEN");
                txtTenNPL.Text = Name.InnerText;

                XmlNode HSCode = doc.SelectSingleNode("ROOT/NPL/MAHS");
                txtMaHSNPL.Text = HSCode.InnerText;

                XmlNode DVT = doc.SelectSingleNode("ROOT/NPL/DVT");
                txtDVTNPL.Text = DVT.InnerText;

                XmlNode Quantity = doc.SelectSingleNode("ROOT/NPL/SOLUONG");
                txtSoLuongNPL.Text = Quantity.InnerText;

                XmlNode UnitPrice = doc.SelectSingleNode("ROOT/NPL/DONGIA");
                txtDonGiaNPL.Text = UnitPrice.InnerText;

                XmlNode Source = doc.SelectSingleNode("ROOT/NPL/NGUONCUNGCAP");
                chkTCU.Checked = Convert.ToBoolean(Source.InnerText);

                XmlNode DecimalSoLuongNPL = doc.SelectSingleNode("ROOT/NPL/SOTHAPPHAN/SOLUONG");
                nmrSoLuongNPL.Value = DecimalSoLuongNPL.InnerText;

                XmlNode DecimalDonGiaNPL = doc.SelectSingleNode("ROOT/NPL/SOTHAPPHAN/DONGIA");
                nmrDonGiaNPL.Value = DecimalDonGiaNPL.InnerText;

                #endregion
                #region CẤU HÌNH SP
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowSP = doc.SelectSingleNode("ROOT/SP/ROWNUMBER");
                txtRowNPL.Text = rowSP.InnerText;

                XmlNode CodeSP = doc.SelectSingleNode("ROOT/SP/MA");
                txtMaSP.Text = CodeSP.InnerText;

                XmlNode NameSP = doc.SelectSingleNode("ROOT/SP/TEN");
                txtTenSP.Text = NameSP.InnerText;

                XmlNode HSCodeSP = doc.SelectSingleNode("ROOT/SP/MAHS");
                txtMaHSSP.Text = HSCodeSP.InnerText;

                XmlNode DVTSP = doc.SelectSingleNode("ROOT/SP/DVT");
                txtDVTSP.Text = DVTSP.InnerText;

                XmlNode QuantitySP = doc.SelectSingleNode("ROOT/SP/SOLUONG");
                txtSoLuongSP.Text = QuantitySP.InnerText;

                XmlNode UnitPriceSP = doc.SelectSingleNode("ROOT/SP/DONGIA");
                txtDonGiaSP.Text = UnitPriceSP.InnerText;

                XmlNode Type = doc.SelectSingleNode("ROOT/SP/LOAISPGC");
                txtLoaiSPGC.Text = Type.InnerText;

                XmlNode DecimalSoLuongSP = doc.SelectSingleNode("ROOT/SP/SOTHAPPHAN/SOLUONG");
                nmrSoLuongSP.Value = DecimalSoLuongSP.InnerText;

                XmlNode DecimalDonGiaSP = doc.SelectSingleNode("ROOT/SP/SOTHAPPHAN/DONGIA");
                nmrDonGiaSP.Value = DecimalDonGiaSP.InnerText;
                #endregion

                #region CẤU HÌNH THIẾT BỊ
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowTB = doc.SelectSingleNode("ROOT/TB/ROWNUMBER");
                txtRowTB.Text = rowTB.InnerText;

                XmlNode CodeTB = doc.SelectSingleNode("ROOT/TB/MA");
                txtMaTB.Text = CodeTB.InnerText;
                XmlNode NameTB = doc.SelectSingleNode("ROOT/TB/TEN");
                txtTenTB.Text = NameTB.InnerText;

                XmlNode HSCodeTB = doc.SelectSingleNode("ROOT/TB/MAHS");
                txtMaHSTB.Text = HSCodeTB.InnerText;

                XmlNode DVTTB = doc.SelectSingleNode("ROOT/TB/DVT");
                txtDVTTB.Text = DVTTB.InnerText;

                XmlNode CountryTB = doc.SelectSingleNode("ROOT/TB/XUATXU");
                txtXuatXu.Text = CountryTB.InnerText;

                XmlNode QuantityTB = doc.SelectSingleNode("ROOT/TB/SOLUONG");
                txtSoLuongTB.Text = QuantityTB.InnerText;

                XmlNode UnitPriceTB = doc.SelectSingleNode("ROOT/TB/DONGIA");
                txtDonGiaTB.Text = UnitPriceTB.InnerText;

                XmlNode CurrencyTB = doc.SelectSingleNode("ROOT/TB/NGUYENTE");
                txtNguyenTe.Text = CurrencyTB.InnerText;

                XmlNode Status = doc.SelectSingleNode("ROOT/TB/TINHTRANG");
                txtTinhTrang.Text = Status.InnerText;

                XmlNode DecimalSoLuongTB = doc.SelectSingleNode("ROOT/TB/SOTHAPPHAN/SOLUONG");
                nmrSoLuongTB.Value = DecimalSoLuongTB.InnerText;

                XmlNode DecimalDonGiaTB = doc.SelectSingleNode("ROOT/TB/SOTHAPPHAN/DONGIA");
                nmrDonGiaTB.Value = DecimalDonGiaTB.InnerText;

                #endregion

                #region CẤU HÌNH ĐỊNH MỨC
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowDM = doc.SelectSingleNode("ROOT/DM/ROWNUMBER");
                txtRowDM.Text = rowDM.InnerText;
                XmlNode CodeSanPham = doc.SelectSingleNode("ROOT/DM/MASP");
                txtMaSanPham.Text = CodeSanPham.InnerText;

                XmlNode CodeNPL = doc.SelectSingleNode("ROOT/DM/MANPL");
                txtMaNguyenPhuLieu.Text = CodeNPL.InnerText;

                XmlNode DMSD = doc.SelectSingleNode("ROOT/DM/DINHMUCSUDUNG");
                txtDMSD.Text = DMSD.InnerText = txtDMSD.Text.ToString();

                XmlNode TLHH = doc.SelectSingleNode("ROOT/DM/TLHH");
                txtTLHH.Text = TLHH.InnerText = txtTLHH.Text.ToString();

                XmlNode NPLCungUng = doc.SelectSingleNode("ROOT/DM/NPLCUNGUNG");
                txtNPLCungUng.Text = NPLCungUng.InnerText;

                XmlNode Replace = doc.SelectSingleNode("ROOT/DM/THAYTHE");
                chkOverwrite.Checked = Convert.ToBoolean(Replace.InnerText);

                XmlNode DinhDanh = doc.SelectSingleNode("ROOT/DM/MADINHDANH");
                txtMaDD.Text = DinhDanh.InnerText;

                XmlNode DecimalDMSD = doc.SelectSingleNode("ROOT/DM/SOTHAPPHAN/DMSD");
                nmrDMSD.Value = DecimalDMSD.InnerText;

                XmlNode DecimalTLHH = doc.SelectSingleNode("ROOT/DM/SOTHAPPHAN/TLHH");
                nmrTLHH.Value = DecimalTLHH.InnerText;

                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ReadDefaultNPL(UIComboBox SheeetName, NumericEditBox BeginRow, EditBox ProductCode, EditBox ProductName, EditBox ProductHSCode, EditBox ProductUnit, EditBox ProductQuantity, EditBox ProductUnitPrice, UICheckBox ProductSource)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                #region CẤU HÌNH NPL
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode row = doc.SelectSingleNode("ROOT/NPL/ROWNUMBER");
                BeginRow.Text = row.InnerText;

                XmlNode Code = doc.SelectSingleNode("ROOT/NPL/MA");
                ProductCode.Text = Code.InnerText;

                XmlNode Name = doc.SelectSingleNode("ROOT/NPL/TEN");
                ProductName.Text = Name.InnerText;

                XmlNode HSCode = doc.SelectSingleNode("ROOT/NPL/MAHS");
                ProductHSCode.Text = HSCode.InnerText;

                XmlNode DVT = doc.SelectSingleNode("ROOT/NPL/DVT");
                ProductUnit.Text = DVT.InnerText;
#if GC_V4
                XmlNode Quantity = doc.SelectSingleNode("ROOT/NPL/SOLUONG");
                ProductQuantity.Text = Quantity.InnerText;

                XmlNode UnitPrice = doc.SelectSingleNode("ROOT/NPL/DONGIA");
                ProductUnitPrice.Text = UnitPrice.InnerText;

                XmlNode Source = doc.SelectSingleNode("ROOT/NPL/NGUONCUNGCAP");
                ProductSource.Checked = Convert.ToBoolean(Source.InnerText);

                XmlNode DecimalSoLuongNPL = doc.SelectSingleNode("ROOT/NPL/SOTHAPPHAN/SOLUONG");
                FormatLuongNPL = Convert.ToInt32(DecimalSoLuongNPL.InnerText);

                XmlNode DecimalDonGiaNPL = doc.SelectSingleNode("ROOT/NPL/SOTHAPPHAN/DONGIA");
                FormatDonGiaNPL = Convert.ToInt32(DecimalDonGiaNPL.InnerText);
#endif
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ReadDefaultSP(UIComboBox SheeetName, NumericEditBox BeginRow, EditBox ProductCode, EditBox ProductName, EditBox ProductHSCode, EditBox ProductUnit, EditBox ProductQuantity, EditBox ProductUnitPrice, EditBox ProductType)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                #region CẤU HÌNH SP
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowSP = doc.SelectSingleNode("ROOT/SP/ROWNUMBER");
                BeginRow.Text = rowSP.InnerText;

                XmlNode CodeSP = doc.SelectSingleNode("ROOT/SP/MA");
                ProductCode.Text = CodeSP.InnerText;

                XmlNode NameSP = doc.SelectSingleNode("ROOT/SP/TEN");
                ProductName.Text = NameSP.InnerText;

                XmlNode HSCodeSP = doc.SelectSingleNode("ROOT/SP/MAHS");
                ProductHSCode.Text = HSCodeSP.InnerText;

                XmlNode DVTSP = doc.SelectSingleNode("ROOT/SP/DVT");
                ProductUnit.Text = DVTSP.InnerText;
#if GC_V4
                XmlNode QuantitySP = doc.SelectSingleNode("ROOT/SP/SOLUONG");
                ProductQuantity.Text = QuantitySP.InnerText;

                XmlNode UnitPriceSP = doc.SelectSingleNode("ROOT/SP/DONGIA");
                ProductUnitPrice.Text = UnitPriceSP.InnerText;

                XmlNode Type = doc.SelectSingleNode("ROOT/SP/LOAISPGC");
                ProductType.Text = Type.InnerText;

                XmlNode DecimalSoLuongSP = doc.SelectSingleNode("ROOT/SP/SOTHAPPHAN/SOLUONG");
                FormatLuongSP = Convert.ToInt32(DecimalSoLuongSP.InnerText);

                XmlNode DecimalDonGiaSP = doc.SelectSingleNode("ROOT/SP/SOTHAPPHAN/DONGIA");
                FormatDonGiaSP = Convert.ToInt32(DecimalDonGiaSP.InnerText);
#endif
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ReadDefaultTB(UIComboBox SheeetName, NumericEditBox BeginRow, EditBox ProductCode, EditBox ProductName, EditBox ProductHSCode, EditBox ProductUnit, EditBox ProductCountry, EditBox ProductQuantity, EditBox ProductUnitPrice, EditBox ProductCurrency, EditBox ProductStatus)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");
                #region CẤU HÌNH THIẾT BỊ
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowTB = doc.SelectSingleNode("ROOT/TB/ROWNUMBER");
                BeginRow.Text = rowTB.InnerText;

                XmlNode CodeTB = doc.SelectSingleNode("ROOT/TB/MA");
                ProductCode.Text = CodeTB.InnerText;

                XmlNode NameTB = doc.SelectSingleNode("ROOT/TB/TEN");
                ProductName.Text = NameTB.InnerText;

                XmlNode HSCodeTB = doc.SelectSingleNode("ROOT/TB/MAHS");
                ProductHSCode.Text = HSCodeTB.InnerText;

                XmlNode DVTTB = doc.SelectSingleNode("ROOT/TB/DVT");
                ProductUnit.Text = DVTTB.InnerText;

                XmlNode CountryTB = doc.SelectSingleNode("ROOT/TB/XUATXU");
                ProductCountry.Text = CountryTB.InnerText;

                XmlNode QuantityTB = doc.SelectSingleNode("ROOT/TB/SOLUONG");
                ProductQuantity.Text = QuantityTB.InnerText;

                XmlNode UnitPriceTB = doc.SelectSingleNode("ROOT/TB/DONGIA");
                ProductUnitPrice.Text = UnitPriceTB.InnerText;

                XmlNode CurrencyTB = doc.SelectSingleNode("ROOT/TB/NGUYENTE");
                ProductCurrency.Text = CurrencyTB.InnerText;

                XmlNode Status = doc.SelectSingleNode("ROOT/TB/TINHTRANG");
                ProductStatus.Text = Status.InnerText;

                XmlNode DecimalSoLuongTB = doc.SelectSingleNode("ROOT/TB/SOTHAPPHAN/SOLUONG");
                FormatLuongTB = Convert.ToInt32(DecimalSoLuongTB.InnerText);

                XmlNode DecimalDonGiaTB = doc.SelectSingleNode("ROOT/TB/SOTHAPPHAN/DONGIA");
                FormatDonGiaTB = Convert.ToInt32(DecimalDonGiaTB.InnerText);

                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public void ReadDefaultDM(UIComboBox SheeetName, NumericEditBox BeginRow, EditBox ProductCodeSP, EditBox ProductCodeNPL, EditBox ProductDMSD, EditBox ProductTLHH, EditBox ProductNPLCU, EditBox ProductDinhDanh, UICheckBox ProductReplace)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                #region CẤU HÌNH ĐỊNH MỨC
                //XmlNode SheetName = doc.SelectSingleNode("NPL/SHEETNAME");
                //SheetName.InnerText = sheetName;
                XmlNode rowDM = doc.SelectSingleNode("ROOT/DM/ROWNUMBER");
                BeginRow.Text = rowDM.InnerText;

                XmlNode CodeSanPham = doc.SelectSingleNode("ROOT/DM/MASP");
                ProductCodeSP.Text = CodeSanPham.InnerText;

                XmlNode CodeNPL = doc.SelectSingleNode("ROOT/DM/MANPL");
                ProductCodeNPL.Text = CodeNPL.InnerText;

                XmlNode DMSD = doc.SelectSingleNode("ROOT/DM/DINHMUCSUDUNG");
                ProductDMSD.Text = DMSD.InnerText = txtDMSD.Text.ToString();

                XmlNode TLHH = doc.SelectSingleNode("ROOT/DM/TLHH");
                ProductTLHH.Text = TLHH.InnerText = txtTLHH.Text.ToString();

#if GC_V4
                XmlNode NPLCungUng = doc.SelectSingleNode("ROOT/DM/NPLCUNGUNG");
                ProductNPLCU.Text = NPLCungUng.InnerText;
#endif
                XmlNode Replace = doc.SelectSingleNode("ROOT/DM/THAYTHE");
                ProductReplace.Checked = Convert.ToBoolean(Replace.InnerText);

                XmlNode DinhDanh = doc.SelectSingleNode("ROOT/DM/MADINHDANH");
                ProductDinhDanh.Text = DinhDanh.InnerText;

                XmlNode DecimalDMSD = doc.SelectSingleNode("ROOT/DM/SOTHAPPHAN/DMSD");
                FormatDMSD = Convert.ToInt32(DecimalDMSD.InnerText);

                XmlNode DecimalTLHH = doc.SelectSingleNode("ROOT/DM/SOTHAPPHAN/TLHH");
                FormatTLHH = Convert.ToInt32(DecimalTLHH.InnerText);
                
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ReadDefault(UIComboBox sheetName, NumericEditBox beginRow,EditBox productCode,EditBox productName,EditBox hSCode,EditBox madeIn,EditBox quantity,EditBox quantity2,EditBox unitCal,EditBox unitCal2,EditBox price,EditBox TriGia,EditBox TSXNK,EditBox ThueSuat,EditBox MaMucThue,EditBox MaMienGiam,EditBox TienMienGiam)
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(Application.StartupPath + "\\ExcelSetting_VNACCS.xml");
                XmlNode sheet = doc.SelectSingleNode("TSTK/Tensheet");
                sheetName.Text = sheet.InnerText;

                XmlNode row = doc.SelectSingleNode("TSTK/Dongbatdau");
                beginRow.Text = row.InnerText;

                XmlNode code = doc.SelectSingleNode("TSTK/MaHang");
                productCode.Text = code.InnerText;

                XmlNode name = doc.SelectSingleNode("TSTK/TenHang");
                productName.Text = name.InnerText;

                XmlNode hs = doc.SelectSingleNode("TSTK/MaHS");
                hSCode.Text = hs.InnerText;

                XmlNode made = doc.SelectSingleNode("TSTK/Xuatxu");
                madeIn.Text = made.InnerText;

                XmlNode quan = doc.SelectSingleNode("TSTK/Soluong");
                quantity.Text = quan.InnerText;

                XmlNode quan2 = doc.SelectSingleNode("TSTK/Soluong2");
                quantity2.Text = quan.InnerText;

                XmlNode dvt = doc.SelectSingleNode("TSTK/DVT");
                unitCal.Text = dvt.InnerText;

                XmlNode dvt2 = doc.SelectSingleNode("TSTK/DVT2");
                unitCal2.Text = dvt.InnerText;

                XmlNode priceunit = doc.SelectSingleNode("TSTK/Dongia");
                price.Text = priceunit.InnerText;

                XmlNode tgTT = doc.SelectSingleNode("TSTK/TriGia");
                TriGia.Text = tgTT.InnerText;

                XmlNode tsXNK = doc.SelectSingleNode("TSTK/ThueXNK");
                TSXNK.Text = tsXNK.InnerText;

                XmlNode tsThueSuat = doc.SelectSingleNode("TSTK/ThueSuat");
                ThueSuat.Text = tsThueSuat.InnerText;

                XmlNode tsGTGT = doc.SelectSingleNode("TSTK/MaMucThue");
                MaMucThue.Text = tsGTGT.InnerText;

                XmlNode tsTTDB = doc.SelectSingleNode("TSTK/MaMienGiam");
                MaMienGiam.Text = tsTTDB.InnerText;

                XmlNode tltk = doc.SelectSingleNode("TSTK/TienMienGiam");
                TienMienGiam.Text = tltk.InnerText;

                XmlNode DecimalSoLuong = doc.SelectSingleNode("TSTK/SoThapPhan/Soluong");
                FormatSoLuong = Convert.ToInt32(DecimalSoLuong.InnerText);

                XmlNode DecimalSoLuong1 = doc.SelectSingleNode("TSTK/SoThapPhan/Soluong2");
                FormatSoLuong1 = Convert.ToInt32(DecimalSoLuong1.InnerText);

                XmlNode DecimalDonGia = doc.SelectSingleNode("TSTK/SoThapPhan/Dongia");
                FormatDonGia = Convert.ToInt32(DecimalDonGia.InnerText);

                XmlNode DecimalTriGia = doc.SelectSingleNode("TSTK/SoThapPhan/TriGia");
                FormatTriGia = Convert.ToInt32(DecimalTriGia.InnerText);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public void SaveConfigNPL(String BeginRow, String ProductCode, String ProductName, String ProductHSCode, String ProductUnit, String ProductQuantity, String ProductUnitPrice, Boolean ProductSource)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                XmlNode Row = doc.SelectSingleNode("ROOT/NPL/ROWNUMBER");
                Row.InnerText = BeginRow;

                XmlNode Code = doc.SelectSingleNode("ROOT/NPL/MA");
                Code.InnerText = ProductCode;

                XmlNode Name = doc.SelectSingleNode("ROOT/NPL/TEN");
                Name.InnerText = ProductName;

                XmlNode HSCode = doc.SelectSingleNode("ROOT/NPL/MAHS");
                HSCode.InnerText = ProductHSCode;

                XmlNode DVT = doc.SelectSingleNode("ROOT/NPL/DVT");
                DVT.InnerText = ProductUnit;
#if GC_V4
                XmlNode Quantity = doc.SelectSingleNode("ROOT/NPL/SOLUONG");
                Quantity.InnerText = ProductQuantity;

                XmlNode UnitPrice = doc.SelectSingleNode("ROOT/NPL/DONGIA");
                UnitPrice.InnerText = ProductUnitPrice;

                XmlNode Source = doc.SelectSingleNode("ROOT/NPL/NGUONCUNGCAP");
                Source.InnerText = ProductSource.ToString();
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void SaveConfigSP(String BeginRow, String ProductCode, String ProductName, String ProductHSCode, String ProductUnit, String ProductQuantity, String ProductUnitPrice, String ProductType)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                XmlNode rowSP = doc.SelectSingleNode("SP/ROWNUMBER");
                rowSP.InnerText = BeginRow;

                XmlNode CodeSP = doc.SelectSingleNode("ROOT/SP/MA");
                CodeSP.InnerText = ProductCode;

                XmlNode NameSP = doc.SelectSingleNode("ROOT/SP/TEN");
                NameSP.InnerText = ProductName;

                XmlNode HSCodeSP = doc.SelectSingleNode("ROOT/SP/MAHS");
                HSCodeSP.InnerText = ProductHSCode;

                XmlNode DVTSP = doc.SelectSingleNode("ROOT/SP/DVT");
                DVTSP.InnerText = ProductUnit;
#if GC_V4
                XmlNode QuantitySP = doc.SelectSingleNode("ROOT/SP/SOLUONG");
                QuantitySP.InnerText = ProductQuantity;

                XmlNode UnitPriceSP = doc.SelectSingleNode("ROOT/SP/DONGIA");
                UnitPriceSP.InnerText = txtDonGiaSP.Text.ToString();

                XmlNode Type = doc.SelectSingleNode("ROOT/SP/LOAISPGC");
                Type.InnerText = ProductType;
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void SaveConfigTB(String BeginRow, String ProductCode, String ProductName, String ProductHSCode, String ProductUnit, String ProductCountry, String ProductQuantity, String ProductUnitPrice, String ProductCurrency, String ProductStatus)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                XmlNode rowTB = doc.SelectSingleNode("ROOT/TB/ROWNUMBER");
                rowTB.InnerText = BeginRow;

                XmlNode CodeTB = doc.SelectSingleNode("ROOT/TB/MA");
                CodeTB.InnerText = ProductCode;

                XmlNode NameTB = doc.SelectSingleNode("ROOT/TB/TEN");
                NameTB.InnerText = ProductName;

                XmlNode HSCodeTB = doc.SelectSingleNode("ROOT/TB/MAHS");
                HSCodeTB.InnerText = ProductHSCode;

                XmlNode DVTTB = doc.SelectSingleNode("ROOT/TB/DVT");
                DVTTB.InnerText = ProductUnit;

                XmlNode CountryTB = doc.SelectSingleNode("ROOT/TB/XUATXU");
                CountryTB.InnerText = ProductCountry;

                XmlNode QuantityTB = doc.SelectSingleNode("ROOT/TB/SOLUONG");
                QuantityTB.InnerText = ProductQuantity;

                XmlNode UnitPriceTB = doc.SelectSingleNode("ROOT/TB/DONGIA");
                UnitPriceTB.InnerText = ProductUnitPrice;

                XmlNode CurrencyTB = doc.SelectSingleNode("ROOT/TB/NGUYENTE");
                CurrencyTB.InnerText = ProductCurrency;

                XmlNode Status = doc.SelectSingleNode("ROOT/TB/TINHTRANG");
                Status.InnerText = ProductStatus;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void SaveConfigDM(String SheeetName, String BeginRow, String ProductCodeSP, String ProductCodeNPL, String ProductDMSD, String ProductTLHH, String ProductNPLCU, String ProductDinhDanh, Boolean ProductReplace)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\ExcelSettingAll.xml");

                XmlNode rowDM = doc.SelectSingleNode("ROOT/DM/ROWNUMBER");
                rowDM.InnerText = BeginRow;

                XmlNode CodeSanPham = doc.SelectSingleNode("ROOT/DM/MASP");
                CodeSanPham.InnerText = ProductCodeSP;

                XmlNode CodeNPL = doc.SelectSingleNode("ROOT/DM/MANPL");
                CodeNPL.InnerText = ProductCodeNPL;

                XmlNode DMSD = doc.SelectSingleNode("ROOT/DM/DINHMUCSUDUNG");
                DMSD.InnerText = ProductDMSD;

                XmlNode TLHH = doc.SelectSingleNode("ROOT/DM/TLHH");
                TLHH.InnerText = ProductTLHH;
#if GC_V4
                XmlNode NPLCungUng = doc.SelectSingleNode("ROOT/DM/NPLCUNGUNG");
                NPLCungUng.InnerText = ProductNPLCU;
#endif
                XmlNode Replace = doc.SelectSingleNode("ROOT/DM/THAYTHE");
                Replace.InnerText = ProductReplace.ToString();

                XmlNode DinhDanh = doc.SelectSingleNode("ROOT/DM/MADINHDANH");
                DinhDanh.InnerText = ProductDinhDanh;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveConfigXML();
            SaveConfigHangHoa();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmCauHinhThongSoReadExcel_Load(object sender, EventArgs e)
        {
#if SXXK_V4
            uiTabPage3.TabVisible = false;
            grbSP.Visible = false;
            grbGC.Visible = false;
#endif
            ReadConfigXML();
            ReadConfigHangHoa();
        }
    }
}
