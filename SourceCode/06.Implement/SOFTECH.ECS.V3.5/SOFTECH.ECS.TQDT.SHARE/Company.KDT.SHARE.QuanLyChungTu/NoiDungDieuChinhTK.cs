﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class NoiDungDieuChinhTK
    {
        public List<NoiDungDieuChinhTKDetail> listNDDCTKChiTiet = new List<NoiDungDieuChinhTKDetail>();
        public List<NoiDungDieuChinhTK> ListNoiDungDCTK = new List<NoiDungDieuChinhTK>();
        public void LoadListNDDCTK()
        {
            listNDDCTKChiTiet = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(this.ID);
        }

        /// <summary>
        /// Chi load 4 fields
        /// </summary>
        /// <param name="tkmd_id"></param>
        public void LoadListNoiDungDCTKByTKMD_ID(long tkmd_id)
        {
            ListNoiDungDCTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(tkmd_id);
        }

        public void InsertUpdateFull(List<NoiDungDieuChinhTKDetail> listNDDCTKCT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                    try
                    {
                        foreach (NoiDungDieuChinhTKDetail ndct in listNDDCTKCT)
                        {
                            ndct.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            if (id == 0)
                                ndct.ID = 0;

                            if (ndct.ID == 0)
                                ndct.Insert(transaction);
                            else
                                ndct.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void InsertUpdateExistIDDieuChinh(List<NoiDungDieuChinhTKDetail> listNDDCTKCT, int idDieuChinh)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = idDieuChinh;
                    try
                    {
                        foreach (NoiDungDieuChinhTKDetail ndct in listNDDCTKCT)
                        {
                            ndct.Id_DieuChinh = idDieuChinh;
                            if (id == 0)
                                ndct.ID = 0;

                            if (ndct.ID == 0)
                                ndct.Insert(transaction);
                            else
                                ndct.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTK_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
            db.AddInParameter(dbCommand, "@NgayDK", SqlDbType.DateTime, NgayDK.Year == 1753 ? DBNull.Value : (object)NgayDK);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgaySua", SqlDbType.DateTime, NgaySua.Year == 1753 ? DBNull.Value : (object)NgaySua);
            db.AddInParameter(dbCommand, "@SoDieuChinh", SqlDbType.Int, SoDieuChinh);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int Insert(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
            db.AddInParameter(dbCommand, "@NgayDK", SqlDbType.DateTime, NgayDK.Year <= 1753 ? DBNull.Value : (object)NgayDK);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgaySua", SqlDbType.DateTime, NgaySua.Year <= 1753 ? DBNull.Value : (object)NgaySua);
            db.AddInParameter(dbCommand, "@SoDieuChinh", SqlDbType.Int, SoDieuChinh);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteByTKMD(SqlTransaction transaction, long TKMD_ID, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_DeleteDynamic]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            string whereCondition = "TKMD_ID = " + TKMD_ID;
            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderMaxSoDieuChinh(long tkmd_id)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmd_id);
            
            return db.ExecuteReader(dbCommand);
        }
        public static IDataReader SelectReaderMaxSoDieuChinh(long tkmd_id, string maLoaiHinh)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmd_id);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, maLoaiHinh);

            return db.ExecuteReader(dbCommand);
        }
        public static IDataReader SelectReaderMaxSoDieuChinh2(long tkmd_id, string maLoaiHinh)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTK_SelectMax_SoDieuChinh";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmd_id);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, maLoaiHinh);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static int SelectMaxId_DieuChinh()
        {
            int maxIDDieuChinh = 0;
            SqlDataReader reader = (SqlDataReader)SelectReaderMaxId_DieuChinh();
            while (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) maxIDDieuChinh = reader.GetInt32(reader.GetOrdinal("ID"));

            }
            reader.Close();
            return maxIDDieuChinh;
        }
        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderMaxId_DieuChinh()
        {
            const string spName = "p_KDT_NoiDungDieuChinhTK_SelectMax_Id_DieuChinh";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        public static int SelectMaxSoDieuChinh(long tkmd_id)
        {
            int maxSoDieuChinh = 0;
            SqlDataReader reader = (SqlDataReader)SelectReaderMaxSoDieuChinh(tkmd_id);
            while (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoDieuChinh"))) maxSoDieuChinh = reader.GetInt32(reader.GetOrdinal("SoDieuChinh"));

            }
            reader.Close();
            return maxSoDieuChinh;
        }

        public static int SelectMaxSoDieuChinh(long tkmd_id, string maLoaiHinh)
        {
            int maxSoDieuChinh = 0;
            SqlDataReader reader = (SqlDataReader)SelectReaderMaxSoDieuChinh(tkmd_id, maLoaiHinh);
            while (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoDieuChinh"))) maxSoDieuChinh = reader.GetInt32(reader.GetOrdinal("SoDieuChinh"));

            }
            reader.Close();
            return maxSoDieuChinh;
        }
        public static int SelectMaxSoDieuChinh2(long tkmd_id, string maLoaiHinh)
        {
            int maxSoDieuChinh = 0;
            SqlDataReader reader = (SqlDataReader)SelectReaderMaxSoDieuChinh2(tkmd_id, maLoaiHinh);
            while (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoDieuChinh"))) maxSoDieuChinh = reader.GetInt32(reader.GetOrdinal("SoDieuChinh"));

            }
            reader.Close();
            return maxSoDieuChinh;
        }
    }
}
