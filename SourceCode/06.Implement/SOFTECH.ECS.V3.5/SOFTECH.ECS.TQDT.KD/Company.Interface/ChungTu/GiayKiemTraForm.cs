﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#endif

namespace Company.Interface
{
    public partial class GiayKiemTraForm : Company.Interface.BaseForm
    {

        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        public GiayKiemTra GiayKiemTra;
        private FeedBackContent feedbackContent;
        private string msgInfor = string.Empty;
        private DataTable dtLoaiChungTu = LoaiChungTu.SelectAll().Tables[0];
        public GiayKiemTraForm()
        {
            InitializeComponent();
        }

        private bool checkSoHopDong(string soHopDong)
        {
            foreach (HopDongThuongMai HDTM in TKMD.HopDongThuongMaiCollection)
            {
                if (HDTM.SoHopDongTM.Trim().ToUpper() == soHopDong.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HangGiayKiemTra> hangGiayKTCollection = new List<HangGiayKiemTra>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangGiayKiemTra hang = new HangGiayKiemTra();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hang.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hang.ID = Convert.ToInt64(row["ID"]);
                        hangGiayKTCollection.Add(hang);
                    }
                }
                foreach (HangGiayKiemTra item in hangGiayKTCollection)
                {
                    try
                    {
                        if (item.ID > 0)
                        {
                            item.Delete();
                        }
                        foreach (HangGiayKiemTra hang in GiayKiemTra.HangCollection)
                        {
                            if (hang.HMD_ID == item.HMD_ID)
                            {
                                GiayKiemTra.HangCollection.Remove(hang);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        private void BindData()
        {
            //Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
            //hmd.TKMD_ID = TKMD.ID;
            dgList.DataSource = GiayKiemTra.ConvertListToDataSet(HangMauDich.SelectBy_TKMD_ID(TKMD.ID).Tables[0]);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            if (GiayKiemTra.ChungTuCollection != null && GiayKiemTra.ChungTuCollection.Count > 0)
            {
                dgListChungTu.DataSource = GiayKiemTra.ChungTuCollection;
                try { dgListChungTu.Refetch(); }
                catch { dgListChungTu.Refresh(); }
            }
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid)
                    return;
                if (GiayKiemTra.HangCollection.Count == 0)
                {
                    ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                    return;
                }

                //Ngonnt 23/01/2013
                //if (chkChungNoiDungKiemTra.Checked)
                //{
                //epError.Clear();
                //if (string.IsNullOrEmpty(txtDiaDiemKiemTra.Text))
                //{
                //    epError.SetError(txtDiaDiemKiemTra, "Không được để trống Địa điểm kiểm tra");
                //    return;
                //}
                //if (string.IsNullOrEmpty(txtTenCoQuanKT.Text))
                //{
                //    epError.SetError(txtTenCoQuanKT, "Không được để trống Tên cơ quan kiểm tra");
                //    return;
                //}
                //}

                //Kiểm tra chứng từ
                bool check = true;
                string msg = "";
                int count = 0;
                GridEXRow[] rows = dgList.GetRows();
                foreach (GridEXRow row in rows)
                {
                    DataRowView rowview = (DataRowView)row.DataRow;
                    foreach (HangGiayKiemTra item in GiayKiemTra.HangCollection)
                    {
                        if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
                        {
                            item.LoaiChungTu = rowview["LoaiChungTu"].ToString();
                            item.TenChungTu = rowview["TenChungTu"].ToString();
                            item.SoChungTu = rowview["SoChungTu"].ToString();

                            item.NgayPhatHanh = Convert.ToDateTime(rowview["NgayPhatHanh"].ToString());
                            item.TenCoQuanKT = rowview["TenCoQuanKT"].ToString();
                            item.DiaDiemKiemTra = rowview["DiaDiemKiemTra"].ToString();

                            string re = "";
                            if (item.LoaiChungTu.Length == 0) re += "- Loại chứng từ";
                            if (item.TenChungTu.Length == 0)
                            {
                                if (re.Length != 0) re += ",\r\n";
                                re += "- Tên chứng từ";
                            }
                            if (item.SoChungTu.Length == 0)
                            {
                                if (re.Length != 0) re += ",\r\n";
                                re += "- Số chứng từ";
                            }
                            if (item.NgayPhatHanh.Year <= 1900)
                            {
                                if (re.Length != 0) re += ",\r\n";
                                re += "- Ngày phát hành";
                            }
                            if (item.TenCoQuanKT.Length == 0)
                            {
                                if (re.Length != 0) re += ",\r\n";
                                re += "- Tên cơ quan kiểm tra";
                            }
                            if (item.DiaDiemKiemTra.Length == 0)
                            {
                                if (re.Length != 0) re += ",\r\n";
                                re += "- Địa điểm kiểm tra";
                            }
                            if (re.Length != 0)
                            {                                
                                msg += String.Format("Dòng hàng {0} dữ liệu không được để trống:\r\n{1}\r\n", (count + 1), re);
                                check &= false;
                            }
                            count++;
                        }
                    }
                }

                if (!check)
                {
                    if (msg != "")
                        ShowMessage(msg, false);
                    return;
                }
                //ngonnt 23/01

                //if(!check) 


                //TKMD.HopDongThuongMaiCollection.Remove(HopDongTM);
                //HopDongTM.Delete();
                //             if (GiayKiemTra.ID == 0 && checkSoHopDong(txtSoGiayKT.Text))
                //             {
                //                 ShowMessage("Số hợp đồng này đã tồn tại.", false);
                //                 return;
                //             }
                GiayKiemTra.DiaDiemKiemTra = txtDiaDiemKiemTra.Text.Trim();
                GiayKiemTra.MaCoQuanKT = txtMaCoQuanKT.Text.Trim();
                GiayKiemTra.MaNguoiDuocCap = txtMaNguoiCap.Text.Trim();
                GiayKiemTra.NgayDangKy = ccNgayGiayKT.Value;
                GiayKiemTra.LoaiGiay = cbLoaiGiayKT.SelectedValue.ToString().Trim();
                GiayKiemTra.SoGiayKiemTra = txtSoGiayKT.Text.Trim();
                GiayKiemTra.TenCoQuanKT = txtTenCoQuanKT.Text.Trim();
                GiayKiemTra.TenNguoiDuocCap = txtTenNguoiCap.Text.Trim();
                GiayKiemTra.KetQuaKiemTra = txtKetQuaKiemTra.Text.Trim();
                GiayKiemTra.TKMD_ID = TKMD.ID;
                if (isKhaiBoSung)
                    GiayKiemTra.LoaiKB = 1;
                else
                    GiayKiemTra.LoaiKB = 0;
                GridEXRow[] rowCollection = dgList.GetRows();
                foreach (GridEXRow row in rowCollection)
                {
                    DataRowView rowview = (DataRowView)row.DataRow;
                    foreach (HangGiayKiemTra item in GiayKiemTra.HangCollection)
                    {
                        if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
                        {
                            item.GhiChu = row.Cells["GhiChu"].Text;

                            item.MaHS = rowview["MaHS"].ToString();
                            item.MaHang = rowview["MaPhu"].ToString();
                            item.TenHang = rowview["TenHang"].ToString();
                            item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
                            item.DVT_ID = rowview["DVT_ID"].ToString();
                            item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
                            item.GhiChu = rowview["GhiChu"].ToString();
                            item.DiaDiemKiemTra = rowview["DiaDiemKiemTra"].ToString();
                            item.MaCoQuanKT = rowview["MaCoQuanKT"].ToString();
                            item.TenCoQuanKT = rowview["TenCoQuanKT"].ToString();
                            item.KetQuaKT = rowview["KetQuaKT"].ToString();
                            item.LoaiChungTu = rowview["LoaiChungTu"].ToString();
                            item.TenChungTu = rowview["TenChungTu"].ToString();
                            item.SoChungTu = rowview["SoChungTu"].ToString();
                            if (!string.IsNullOrEmpty(rowview["NgayPhatHanh"].ToString()))
                            {
                                DateTime time = new DateTime(1000, 1, 1);
                                item.NgayPhatHanh = DateTime.TryParse(rowview["NgayPhatHanh"].ToString(), out time) ? time : new DateTime(1000, 1, 1);
                            }
                            break;
                        }
                    }
                }
                try
                {
                    if (string.IsNullOrEmpty(GiayKiemTra.GuidStr)) GiayKiemTra.GuidStr = Guid.NewGuid().ToString();
                    if (GiayKiemTra.NgayTiepNhan.Year < 1900) GiayKiemTra.NgayTiepNhan = new DateTime(1900, 1, 1);
                    bool isAddNew = GiayKiemTra.ID == 0;
                    GiayKiemTra.InsertUpdateFull();
                    if (isAddNew)
                        TKMD.GiayKiemTraCollection.Add(GiayKiemTra);
                    BindData();
                    ShowMessage("Lưu thành công.", false);
                    if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET && TKMD.SoToKhai > 0)
                        btnKhaiBao.Enabled = true;

                    //                 TKMD.SoHopDong = GiayKiemTra.SoHopDongTM;
                    //                 TKMD.NgayHopDong = GiayKiemTra.NgayHopDongTM;
                    //                 TKMD.NgayHetHanHopDong = GiayKiemTra.ThoiHanThanhToan;

                }
                catch (Exception ex)
                {
                    SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        public List<HangMauDich> ConvertListToHangMauDichCollection(List<HopDongThuongMaiDetail> listHangHopDong, List<HangMauDich> hangCollection)
        {

            List<HangMauDich> tmp = new List<HangMauDich>();

            foreach (HopDongThuongMaiDetail item in listHangHopDong)
            {
                foreach (HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất hàng khi kích nút Chọn hàng
            //if (HopDongTM.ListHangMDOfHopDong.Count > 0)
            //{
            //    f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(HopDongTM.ListHangMDOfHopDong, TKMD.HMDCollection);
            //}
            f.ShowDialog(this);
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HangGiayKiemTra hangGiayKT in GiayKiemTra.HangCollection)
                    {
                        if (hangGiayKT.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HangGiayKiemTra hang = new HangGiayKiemTra();
                        hang.HMD_ID = HMD.ID;
                        hang.GiayKiemTra_ID = GiayKiemTra.ID;
                        hang.MaHang = HMD.MaPhu;
                        hang.MaHS = HMD.MaHS;
                        hang.TenHang = HMD.TenHang;
                        hang.DVT_ID = HMD.DVT_ID;
                        //hang.SoThuTuHang = HMD.SoThuTuHang;
                        hang.SoLuong = HMD.SoLuong;
                        hang.NuocXX_ID = HMD.NuocXX_ID;
                        if (chkChungNoiDungKiemTra.Checked)
                        {
                            hang.DiaDiemKiemTra = txtDiaDiemKiemTra.Text.Trim();
                            hang.MaCoQuanKT = txtMaCoQuanKT.Text.Trim();
                            hang.TenCoQuanKT = txtTenCoQuanKT.Text.Trim();
                            hang.KetQuaKT = txtKetQuaKiemTra.Text.Trim();
                        }
                        GiayKiemTra.HangCollection.Add(hang);
                    }
                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void HopDongForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Load danh dach loai chung tu vao Grid
                //  foreach (DataRow rowNuoc in Company.KDT.SHARE.Components.Globals.GlobalDanhMucChuanHQ.Tables["LoaiChungTu"].Rows)
                //     dgList.RootTable.Columns["LoaiChungTu"].ValueList.Add(rowNuoc["ID"].ToString(), rowNuoc["Ten"].ToString());
                dgList.RootTable.Columns["LoaiChungTu"].ValueList.PopulateValueList(LoaiChungTuGiayKiemTra.SelectCollectionAll(), "ID", "TenChungTu");

                uiPanel1.Visible = false;
                btnAddChungTu.Visible = false;
                //Update by KHANHHN 05/03/2012
                if (GiayKiemTra == null || GiayKiemTra.ID == 0)
                    GiayKiemTra = new GiayKiemTra { TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO };

                // an cac button truoc khi load
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;


                if (GiayKiemTra != null && GiayKiemTra.ID <= 0)
                {
                    txtMaNguoiCap.Text = TKMD.MaDoanhNghiep;
                    txtTenNguoiCap.Text = TKMD.TenDoanhNghiep;
                }

                else if (GiayKiemTra != null && GiayKiemTra.ID > 0)
                {
                    txtDiaDiemKiemTra.Text = GiayKiemTra.DiaDiemKiemTra;
                    txtMaCoQuanKT.Text = GiayKiemTra.MaCoQuanKT;
                    txtMaNguoiCap.Text = GiayKiemTra.MaNguoiDuocCap;
                    ccNgayGiayKT.Value = GiayKiemTra.NgayDangKy;
                    cbLoaiGiayKT.SelectedValue = GiayKiemTra.LoaiGiay;
                    txtSoGiayKT.Text = GiayKiemTra.SoGiayKiemTra;
                    txtTenCoQuanKT.Text = GiayKiemTra.TenCoQuanKT;
                    txtTenNguoiCap.Text = GiayKiemTra.TenNguoiDuocCap;
                    txtKetQuaKiemTra.Text = GiayKiemTra.KetQuaKiemTra;
                    ccNgayGiayKT.Text = GiayKiemTra.NgayDangKy.ToShortDateString();
                    GiayKiemTra.HangCollection = (List<HangGiayKiemTra>)HangGiayKiemTra.SelectCollectionBy_GiayKiemTra_ID(GiayKiemTra.ID);
                    GiayKiemTra.ChungTuCollection = (List<ChungTuGiayKiemTra>)ChungTuGiayKiemTra.SelectCollectionBy_GiayKiemTra_ID(GiayKiemTra.ID);
                    BindData();
                }

                if (TKMD.ID <= 0)
                {
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = false;
                }
                else if (GiayKiemTra.SoTiepNhan > 0)
                {
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else if (TKMD.PhanLuong != "")
                {
                    btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                }
                SetButtonStateHOPDONG(TKMD, isKhaiBoSung, GiayKiemTra);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (GiayKiemTra.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }

            try
            {
                if ((GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO) || (GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET))
                    GiayKiemTra.GuidStr = Guid.NewGuid().ToString();
                else if (GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    return;
                }

                ObjectSend msgSend = SingleMessage.BoSungGiayKiemTra(TKMD, GiayKiemTra);

                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                //sendMessageForm.Message.XmlSaveMessage(GiayKiemTra.ID, "Bổ sung giấy kiểm tra");
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendMessageForm.Message.XmlSaveMessage(GiayKiemTra.ID, "Bổ sung giấy kiểm tra");
                    GiayKiemTra.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    GiayKiemTra.Update();
                    SetButtonStateHOPDONG(TKMD, isKhaiBoSung, GiayKiemTra);
                    btnLayPhanHoi_Click(null, null);
                }
                else if (isSend && feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }



        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            #region V3

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBack(TKMD, GiayKiemTra.GuidStr);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                isFeedBack = sendMessageForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetButtonStateHOPDONG(TKMD, isKhaiBoSung, GiayKiemTra);
                }


            }
            //             ObjectSend msgSend = SingleMessage.FeedBack(TKMD, HopDongTM.GuidStr);
            //             SendMessageForm sendMessageForm = new SendMessageForm();
            //             sendMessageForm.Send += SendMessage;
            //             sendMessageForm.DoSend(msgSend);

            #endregion

        }

        #region Begin VALIDATE HOP DONG

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateHopDong()
        {
            bool isValid = true;

            //So_CT	varchar(50)
            isValid = ValidateControl.ValidateLength(txtSoGiayKT, 50, err, "Số hợp đồng");

            //Ma_PTTT	varchar(10)
            isValid &= ValidateControl.ValidateLength(cbLoaiGiayKT, 10, err, "Loại giấy thanh toán");


            //Ma_NT	char(3)

            //Ma_DV	varchar(14)
            isValid &= ValidateControl.ValidateLength(txtMaNguoiCap, 14, err, "Mã đơn vị mua");

            //Ma_DV_DT	varchar(14)
            isValid &= ValidateControl.ValidateLength(txtMaCoQuanKT, 14, err, "Mã đơn vị bán");

            return isValid;
        }

        #endregion End VALIDATE HOP DONG


        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOP DONG.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOPDONG(ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra GiayKiemTra)
        {
            if (GiayKiemTra == null)
                return false;

            if (GiayKiemTra != null)
            {
                txtSoGiayKT.Text = GiayKiemTra.SoGiayKiemTra;
            }
            if (GiayKiemTra.LoaiKB == 1)
            {

                switch (GiayKiemTra.TrangThaiXuLy)
                {
                    case -1:
                        lbTrangThai.Text = "Chưa Khai Báo";
                        break;
                    case -3:
                        lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                        break;
                    case 0:
                        lbTrangThai.Text = "Chờ Duyệt";
                        break;
                    case 1:
                        lbTrangThai.Text = "Đã Duyệt";
                        break;
                    case 2:
                        lbTrangThai.Text = "Hải quan không phê duyệt";
                        break;

                }
            }
            else
                lbTrangThai.Text = string.Empty;
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else if (tkmd.SoToKhai > 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {

                bool khaiBaoEnable = (GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnXoa.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET ||
                                   GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || GiayKiemTra.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
            txtSoTiepNhan.Text = GiayKiemTra.SoTiepNhan > 0 ? GiayKiemTra.SoTiepNhan.ToString() : string.Empty;
            if (GiayKiemTra.NgayTiepNhan.Year > 1900) ccNgayTiepNhan.Value = GiayKiemTra.NgayTiepNhan;
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (GiayKiemTra.GuidStr != null && GiayKiemTra.GuidStr != "")
            {
                ThongDiepForm f = new ThongDiepForm();
                f.DeclarationIssuer = AdditionalDocumentType.HOP_DONG;
                f.ItemID = GiayKiemTra.ID;
                f.ShowDialog(this);
            }

            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<HangGiayKiemTra> HangGiayKTCollection = new List<HangGiayKiemTra>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangGiayKiemTra hangtmp = new HangGiayKiemTra();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hangtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hangtmp.ID = Convert.ToInt64(row["ID"]);
                        HangGiayKTCollection.Add(hangtmp);
                    }
                }
                foreach (HangGiayKiemTra hangTemp in HangGiayKTCollection)
                {
                    try
                    {
                        if (hangTemp.ID > 0)
                        {
                            hangTemp.Delete();
                        }
                        foreach (HangGiayKiemTra hang in GiayKiemTra.HangCollection)
                        {
                            if (hang.HMD_ID == hangTemp.HMD_ID)
                            {
                                GiayKiemTra.HangCollection.Remove(hang);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            List<HangGiayKiemTra> HangGiayKTCollection = new List<HangGiayKiemTra>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangGiayKiemTra hangtmp = new HangGiayKiemTra();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hangtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hangtmp.ID = Convert.ToInt64(row["ID"]);
                        HangGiayKTCollection.Add(hangtmp);
                    }
                }
                foreach (HangGiayKiemTra hangTemp in HangGiayKTCollection)
                {
                    try
                    {
                        if (hangTemp.ID > 0)
                        {
                            hangTemp.Delete();
                        }
                        foreach (HangGiayKiemTra hang in GiayKiemTra.HangCollection)
                        {
                            if (hang.HMD_ID == hangTemp.HMD_ID)
                            {
                                GiayKiemTra.HangCollection.Remove(hang);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(GiayKiemTra.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                GiayKiemTra.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                noidung = "Hải quan từ chối tiếp nhận.\r\nLý do: " + noidung;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            //noidung = noidung.Replace(string.Format("Message [{0}]", HopDongTM.GuidStr), string.Empty);
                            //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                GiayKiemTra.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);
                                if (feedbackContent.Acceptance.Length == 10)
                                    GiayKiemTra.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    GiayKiemTra.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    GiayKiemTra.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                //GiayKiemTra.NamTiepNhan = GiayKiemTra.NgayTiepNhan.Year;
                                //HopDongTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(GiayKiemTra.ID, MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan, noidung);
                                GiayKiemTra.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                                noidung = "Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: " + GiayKiemTra.SoTiepNhan.ToString() + "\r\nNgày tiếp nhận: " + GiayKiemTra.NgayTiepNhan.ToString();
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(GiayKiemTra.ID, MessageTitle.KhaiBaoBoSungHopDongThanhCong, noidung);
                                GiayKiemTra.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                                noidung = "Hợp đồng đã được duyệt.\r\n Thông tin trả về từ hải quan: " + noidung;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(GiayKiemTra.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }

                    if (isUpdate)
                    {
                        GiayKiemTra.Update();
                        SetButtonStateHOPDONG(TKMD, isKhaiBoSung, GiayKiemTra);
                    }
                    msgInfor = noidung;
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion

        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "DVT_ID")
            {
                int val = DonViTinhID(e.Value.ToString());
                if (val != -1)
                    e.Value = Convert.ToInt32(val);
            }
        }

        private void btnAddChungTu_Click(object sender, EventArgs e)
        {
            AddChungTuGiayKTForm f = new AddChungTuGiayKTForm();
            f.GiayKiemTra = this.GiayKiemTra;
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            BindData();
        }

        private void dgListChungTu_FormattingRow(object sender, RowLoadEventArgs e)
        {
            DataRow[] loaichungtu = dtLoaiChungTu.Select("ID = " + e.Row.Cells["LoaiChungTu"].Value.ToString());
            if (loaichungtu != null && loaichungtu.Length > 0)
            {
                e.Row.Cells["LoaiChungTu"].Text = loaichungtu[0]["Ten"].ToString();
            }

        }

        private void dgListChungTu_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                ChungTuGiayKiemTra chungtu = (ChungTuGiayKiemTra)e.Row.DataRow;
                if (chungtu != null)
                {
                    AddChungTuGiayKTForm f = new AddChungTuGiayKTForm();
                    f.GiayKiemTra = this.GiayKiemTra;
                    f.ChungTu = chungtu;
                    f.TKMD = this.TKMD;
                    f.ShowDialog(this);
                    BindData();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListChungTu_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {

            List<ChungTuGiayKiemTra> ChungtuCollection = new List<ChungTuGiayKiemTra>();
            GridEXSelectedItemCollection items = dgListChungTu.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuGiayKiemTra chungtu = new ChungTuGiayKiemTra();
                        chungtu = (ChungTuGiayKiemTra)i.GetRow().DataRow;
                        //chungtu.ID = Convert.ToInt64(row["ID"]);
                        //chungtu.SoChungTu = row["SoChungTu"].ToString();
                        ChungtuCollection.Add(chungtu);
                    }
                }
                foreach (ChungTuGiayKiemTra chungtuTemp in ChungtuCollection)
                {
                    try
                    {
                        if (chungtuTemp.ID > 0)
                        {
                            chungtuTemp.Delete();
                        }
                        foreach (ChungTuGiayKiemTra chungTu in GiayKiemTra.ChungTuCollection)
                        {
                            if (chungTu.SoChungTu.Trim().ToUpper() == chungtuTemp.SoChungTu.Trim().ToUpper())
                            {
                                GiayKiemTra.ChungTuCollection.Remove(chungTu);
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    { Logger.LocalLogger.Instance().WriteMessage(ex); }
                }
            }
            BindData();
        }

        private void dgListChungTu_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<ChungTuGiayKiemTra> ChungtuCollection = new List<ChungTuGiayKiemTra>();
            GridEXSelectedItemCollection items = dgListChungTu.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuGiayKiemTra chungtu = new ChungTuGiayKiemTra();
                        chungtu = (ChungTuGiayKiemTra)i.GetRow().DataRow;
                        //chungtu.ID = Convert.ToInt64(row["ID"]);
                        //chungtu.SoChungTu = row["SoChungTu"].ToString();
                        ChungtuCollection.Add(chungtu);
                    }
                }
                foreach (ChungTuGiayKiemTra chungtuTemp in ChungtuCollection)
                {
                    try
                    {
                        if (chungtuTemp.ID > 0)
                        {
                            chungtuTemp.Delete();
                        }
                        foreach (ChungTuGiayKiemTra chungTu in GiayKiemTra.ChungTuCollection)
                        {
                            if (chungTu.SoChungTu.Trim().ToUpper() == chungtuTemp.SoChungTu.Trim().ToUpper())
                            {
                                GiayKiemTra.ChungTuCollection.Remove(chungTu);
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    { Logger.LocalLogger.Instance().WriteMessage(ex); }
                }
            }
            BindData();
        }

        private void cbLoaiGiayKT_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtKetQuaKiemTra.Enabled = (cbLoaiGiayKT.SelectedValue.ToString() == "DK");
        }

        private void chkChungNoiDungKiemTra_CheckedChanged(object sender, EventArgs e)
        {
            if (chkChungNoiDungKiemTra.Checked)
            {
                GridEXRow[] rowCollection = dgList.GetRows();
                foreach (GridEXRow row in rowCollection)
                {
                    DataRowView rowview = (DataRowView)row.DataRow;

                    rowview["DiaDiemKiemTra"] = txtDiaDiemKiemTra.Text;
                    rowview["MaCoQuanKT"] = txtMaCoQuanKT.Text;
                    rowview["TenCoQuanKT"] = txtTenCoQuanKT.Text;
                    rowview["KetQuaKT"] = txtKetQuaKiemTra.Text;
                }
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refetch();
                }

            }
        }
    }
}

