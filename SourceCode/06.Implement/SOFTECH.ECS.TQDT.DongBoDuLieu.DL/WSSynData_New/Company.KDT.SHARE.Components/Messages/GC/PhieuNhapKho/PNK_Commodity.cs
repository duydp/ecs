﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
   public class PNK_Commodity
    {
        /// <summary>
        /// Số thứ tự
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }

        /// <summary>
        /// Mô tả hàng hóa
        /// </summary>
        [XmlElement("description")]
        public string Description { set; get; }

        /// <summary>
        /// Mã hàng do doanh nghiệp khai
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }

        /// <summary>
        /// Kiểu
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Mã định danh của lệnh sản xuất
        /// </summary>
        [XmlElement("productCtrlNo")]
        public string ProductCtrlNo { set; get; }

        /// <summary>
        /// V5 
        /// </summary>
        [XmlElement("origin")]
        public string Origin { set; get; }

        /// <summary>
        /// V5 
        /// </summary>
        [XmlElement("usage")]
        public string Usage { set; get; }
    }
}
