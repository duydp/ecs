﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class SXXK_TKN : DeclarationBase
    {
        /// <summary>
        /// Người đại diện doanh nghiệp
        /// </summary>
        [XmlElement("RepresentativePerson")]
        public RepresentativePerson RepresentativePerson { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// 
        [XmlElement("CustomsGoodsItem")]
        public CustomsGoodsItem CustomsGoodsItem { get; set; }
        ///// <summary>
        ///// 
        ///// </summary>
        //[XmlElement("AdditionalInformation")]
        //public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        /// Thông tin về hàng hóa
        /// </summary>
        [XmlElement("GoodsShipment")]
        public GoodsShipment GoodsShipment { get; set; }
        /// <summary>
        /// Danh sách giấy phép NK đi kèm
        /// </summary>
        [XmlArray("Licenses")]
        [XmlArrayItem("License")]
        public List<License> Licenses { get; set; }
        /// <summary>
        /// Danh sách hợp đồng khai kèm
        /// </summary>
        [XmlArray("ContractDocuments")]
        [XmlArrayItem("ContractDocument")]
        public List<ContractDocument> ContractDocuments { get; set; }
        /// <summary>
        /// Danh sách hóa đơn thương mại khai kèm   
        /// </summary>
        [XmlArray("CommercialInvoices")]
        [XmlArrayItem("CommercialInvoice")]
        public List<CommercialInvoice> CommercialInvoices { get; set; }
        /// <summary>
        /// Danh sách C/O khai kèm 
        /// </summary>
        [XmlArray("CertificateOfOrigins")]
        [XmlArrayItem("CertificateOfOrigin")]
        public List<CertificateOfOrigin> CertificateOfOrigins { get; set; }
        /// <summary>
        /// Danh sách vận đơn đi kèm
        /// </summary>
        [XmlArray("BillOfLadings")]
        [XmlArrayItem("BillOfLading")]
        public List<BillOfLading> BillOfLadings { get; set; }
        /// <summary>
        /// Đơn xin chuyển cửa khẩu
        /// </summary>
        [XmlElement("CustomsOfficeChangedRequest")]
        public CustomsOfficeChangedRequest CustomsOfficeChangedRequest { get; set; }
        /// <summary>
        /// Danh sách chứng từ đính kèm, có thể lặp lại
        /// </summary>
        [XmlArray("AttachDocuments")]
        [XmlArrayItem("AttachDocumentItem")]
        public List<AttachDocumentItem> AttachDocumentItem { get; set; }
        /// <summary>
        /// C/O, những Giấy phép
        /// </summary>
        [XmlArray("AdditionalDocuments")]
        [XmlArrayItem("AdditionalDocument")]
        public List<AdditionalDocument> AdditionalDocumentEx { get; set; }
    }
}
