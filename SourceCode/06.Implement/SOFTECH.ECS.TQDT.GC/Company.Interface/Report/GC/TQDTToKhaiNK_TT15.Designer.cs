namespace Company.Interface.Report
{
    partial class TQDTToKhaiNK_TT15
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TQDTToKhaiNK_TT15));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.GroupHeader = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoThamChieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblChiCucHQDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiCucHQCuaKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaySoThamChieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoPhuLucToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNguoiXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiayPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHoaDonThuongMai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNguoiNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHoaDonThuongMai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHetHanGiayPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHetHanHopDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblMSTNguoiNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblVanTaiDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCangXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaCangdoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenNguoiUyThac = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayVanTaiDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCangXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCangDoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTNguoiUT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongTienVanTai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNuocXK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSoHieuPTVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDenPTVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNuoc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenDaiLy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKienGiaoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongThucThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTDaiLy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDongTienThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTyGiaTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBangChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader3 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter3 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongContainer = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChungTuDiKem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayIn = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblKetQuaPhanLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMSTNguoiNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMSTNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMSTHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 30;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader
            // 
            this.GroupHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrLabel4,
            this.xrLabel9,
            this.xrLabel8,
            this.xrTable3,
            this.lblCucHaiQuan,
            this.winControlContainer1});
            this.GroupHeader.Height = 413;
            this.GroupHeader.Name = "GroupHeader";
            this.GroupHeader.StylePriority.UseFont = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.Location = new System.Drawing.Point(25, 33);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.Size = new System.Drawing.Size(100, 23);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Cục Hải Quan:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Location = new System.Drawing.Point(700, 50);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Size = new System.Drawing.Size(83, 17);
            this.xrLabel4.Text = "HQ/2012-NK";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.Location = new System.Drawing.Point(25, 8);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.Size = new System.Drawing.Size(208, 23);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "HẢI QUAN VIỆT NAM";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.Location = new System.Drawing.Point(242, 8);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.Size = new System.Drawing.Size(358, 25);
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "TỜ KHAI HÀNG HÓA NHẬP KHẨU";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Location = new System.Drawing.Point(25, 67);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow1,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow37});
            this.xrTable3.Size = new System.Drawing.Size(758, 346);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90,
            this.lblSoThamChieu,
            this.xrTableCell71,
            this.lblSoToKhai,
            this.xrTableCell92});
            this.xrTableRow25.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.StylePriority.UseFont = false;
            this.xrTableRow25.Weight = 1;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Text = " Chi cục Hải Quan đăng ký tờ khai:";
            this.xrTableCell89.Weight = 0.98812664907651715;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.StylePriority.UseFont = false;
            this.xrTableCell90.StylePriority.UseTextAlignment = false;
            this.xrTableCell90.Text = " Số tham chiếu:";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell90.Weight = 0.34960422163588389;
            // 
            // lblSoThamChieu
            // 
            this.lblSoThamChieu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoThamChieu.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoThamChieu.Name = "lblSoThamChieu";
            this.lblSoThamChieu.StylePriority.UseBorders = false;
            this.lblSoThamChieu.StylePriority.UseFont = false;
            this.lblSoThamChieu.StylePriority.UseTextAlignment = false;
            this.lblSoThamChieu.Text = "59569";
            this.lblSoThamChieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoThamChieu.Weight = 0.17663520827258572;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.Text = " Số tờ khai:";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell71.Weight = 0.42597557766572558;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.StylePriority.UseBorders = false;
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "330";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoToKhai.Weight = 0.32952371469820568;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell92.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorderColor = false;
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.StylePriority.UseForeColor = false;
            this.xrTableCell92.Text = " Công chức đăng ký tờ khai";
            this.xrTableCell92.Weight = 0.730134628651082;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblChiCucHQDangKy,
            this.xrTableCell94,
            this.xrTableCell72,
            this.lblNgayDangKy,
            this.xrTableCell96});
            this.xrTableRow26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.StylePriority.UseFont = false;
            this.xrTableRow26.Weight = 0.99999999999999989;
            // 
            // lblChiCucHQDangKy
            // 
            this.lblChiCucHQDangKy.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblChiCucHQDangKy.Name = "lblChiCucHQDangKy";
            this.lblChiCucHQDangKy.StylePriority.UseBorders = false;
            this.lblChiCucHQDangKy.StylePriority.UseTextAlignment = false;
            this.lblChiCucHQDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCucHQDangKy.Weight = 0.98812664907651715;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.StylePriority.UseTextAlignment = false;
            this.xrTableCell94.Text = " Ngày, giờ gửi:";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell94.Weight = 0.52770448548812665;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell72.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.StylePriority.UseTextAlignment = false;
            this.xrTableCell72.Text = " Ngày, giờ đăng ký:";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell72.Weight = 0.42846830572722944;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.StylePriority.UseBorders = false;
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayDangKy.Text = "25/08/2011 00:00";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayDangKy.Weight = 0.32952371469820568;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.Weight = 0.72617684500992108;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.lblChiCucHQCuaKhau,
            this.lblNgaySoThamChieu,
            this.xrTableCell73,
            this.lblSoPhuLucToKhai,
            this.xrTableCell100});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell97.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.StylePriority.UseFont = false;
            this.xrTableCell97.StylePriority.UseTextAlignment = false;
            this.xrTableCell97.Text = " Chi cục Hải quan cửa khẩu xuất:";
            this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell97.Weight = 0.72295514511873349;
            // 
            // lblChiCucHQCuaKhau
            // 
            this.lblChiCucHQCuaKhau.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblChiCucHQCuaKhau.Name = "lblChiCucHQCuaKhau";
            this.lblChiCucHQCuaKhau.StylePriority.UseBorders = false;
            this.lblChiCucHQCuaKhau.StylePriority.UseTextAlignment = false;
            this.lblChiCucHQCuaKhau.Text = "C021";
            this.lblChiCucHQCuaKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCucHQCuaKhau.Weight = 0.26385224274406333;
            // 
            // lblNgaySoThamChieu
            // 
            this.lblNgaySoThamChieu.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaySoThamChieu.Name = "lblNgaySoThamChieu";
            this.lblNgaySoThamChieu.StylePriority.UseFont = false;
            this.lblNgaySoThamChieu.StylePriority.UseTextAlignment = false;
            this.lblNgaySoThamChieu.Text = "25/08/2011 00:00";
            this.lblNgaySoThamChieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgaySoThamChieu.Weight = 0.52506596306068609;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.StylePriority.UseTextAlignment = false;
            this.xrTableCell73.Text = " Số lượng phụ lục tờ khai:";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell73.Weight = 0.63427305506759879;
            // 
            // lblSoPhuLucToKhai
            // 
            this.lblSoPhuLucToKhai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoPhuLucToKhai.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoPhuLucToKhai.Name = "lblSoPhuLucToKhai";
            this.lblSoPhuLucToKhai.StylePriority.UseBorders = false;
            this.lblSoPhuLucToKhai.StylePriority.UseFont = false;
            this.lblSoPhuLucToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoPhuLucToKhai.Text = "1";
            this.lblSoPhuLucToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoPhuLucToKhai.Weight = 0.12371896535783628;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.Weight = 0.730134628651082;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell1,
            this.lblLoaiHinh});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.StylePriority.UseFont = false;
            this.xrTableCell101.Text = " 1.Người xuất khẩu:";
            this.xrTableCell101.Weight = 0.98812664907651715;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = " 5. Mã loại hình:";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.5270448548812664;
            // 
            // lblLoaiHinh
            // 
            this.lblLoaiHinh.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLoaiHinh.Name = "lblLoaiHinh";
            this.lblLoaiHinh.StylePriority.UseBorders = false;
            this.lblLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblLoaiHinh.Text = "NDT01 - Nhập đầu tư";
            this.lblLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblLoaiHinh.Weight = 1.4848284960422165;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNguoiXK,
            this.xrTableCell7,
            this.xrTableCell74,
            this.lblGiayPhep,
            this.xrTableCell9,
            this.lblHopDong});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1;
            // 
            // lblNguoiXK
            // 
            this.lblNguoiXK.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNguoiXK.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiXK.Name = "lblNguoiXK";
            this.lblNguoiXK.StylePriority.UseBorders = false;
            this.lblNguoiXK.StylePriority.UseFont = false;
            this.lblNguoiXK.Weight = 0.98812664907651715;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = " 6.Hóa đơn thương mại";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 0.65765171503957787;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.StylePriority.UseTextAlignment = false;
            this.xrTableCell74.Text = " 7. Giấy phép số:";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell74.Weight = 0.366424802110818;
            // 
            // lblGiayPhep
            // 
            this.lblGiayPhep.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblGiayPhep.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiayPhep.Name = "lblGiayPhep";
            this.lblGiayPhep.StylePriority.UseBorders = false;
            this.lblGiayPhep.StylePriority.UseFont = false;
            this.lblGiayPhep.StylePriority.UseTextAlignment = false;
            this.lblGiayPhep.Text = "So GP";
            this.lblGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblGiayPhep.Weight = 0.25956464379947236;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = " 8. Hợp đồng:";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell9.Weight = 0.2968337730870711;
            // 
            // lblHopDong
            // 
            this.lblHopDong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblHopDong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHopDong.Name = "lblHopDong";
            this.lblHopDong.StylePriority.UseBorders = false;
            this.lblHopDong.StylePriority.UseFont = false;
            this.lblHopDong.StylePriority.UseTextAlignment = false;
            this.lblHopDong.Text = "368/HD-TVD1-P2";
            this.lblHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblHopDong.Weight = 0.4313984168865434;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell107,
            this.lblHoaDonThuongMai,
            this.xrTableCell5,
            this.lblNgayGiayPhep,
            this.xrTableCell14,
            this.lblNgayHopDong});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.StylePriority.UseFont = false;
            this.xrTableCell107.Text = " 2.Người nhập khẩu:";
            this.xrTableCell107.Weight = 0.98812664907651693;
            // 
            // lblHoaDonThuongMai
            // 
            this.lblHoaDonThuongMai.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblHoaDonThuongMai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoaDonThuongMai.Name = "lblHoaDonThuongMai";
            this.lblHoaDonThuongMai.StylePriority.UseBorders = false;
            this.lblHoaDonThuongMai.StylePriority.UseFont = false;
            this.lblHoaDonThuongMai.StylePriority.UseTextAlignment = false;
            this.lblHoaDonThuongMai.Text = "368/HD-TVD1-P2-110901";
            this.lblHoaDonThuongMai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblHoaDonThuongMai.Weight = 0.65567282321899734;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = " Ngày:";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 0.31794195250659629;
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.StylePriority.UseBorders = false;
            this.lblNgayGiayPhep.StylePriority.UseTextAlignment = false;
            this.lblNgayGiayPhep.Text = "19/08/2010";
            this.lblNgayGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayGiayPhep.Weight = 0.31002638522427439;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = " Ngày:";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 0.33245382585751965;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.StylePriority.UseBorders = false;
            this.lblNgayHopDong.StylePriority.UseTextAlignment = false;
            this.lblNgayHopDong.Text = "19/08/2010";
            this.lblNgayHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayHopDong.Weight = 0.39577836411609485;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblNguoiNK,
            this.lblNgayHoaDonThuongMai,
            this.xrTableCell8,
            this.lblNgayHetHanGiayPhep,
            this.xrTableCell75,
            this.lblNgayHetHanHopDong});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.StylePriority.UseBorders = false;
            this.xrTableRow31.Weight = 1;
            // 
            // lblNguoiNK
            // 
            this.lblNguoiNK.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNguoiNK.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiNK.Name = "lblNguoiNK";
            this.lblNguoiNK.StylePriority.UseBorders = false;
            this.lblNguoiNK.StylePriority.UseFont = false;
            this.lblNguoiNK.Weight = 0.99076517150395782;
            // 
            // lblNgayHoaDonThuongMai
            // 
            this.lblNgayHoaDonThuongMai.Name = "lblNgayHoaDonThuongMai";
            this.lblNgayHoaDonThuongMai.StylePriority.UseTextAlignment = false;
            this.lblNgayHoaDonThuongMai.Text = "29/08/2010";
            this.lblNgayHoaDonThuongMai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayHoaDonThuongMai.Weight = 0.65699208443271784;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = " Ngày hết hạn:";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 0.31794195250659635;
            // 
            // lblNgayHetHanGiayPhep
            // 
            this.lblNgayHetHanGiayPhep.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayHetHanGiayPhep.Name = "lblNgayHetHanGiayPhep";
            this.lblNgayHetHanGiayPhep.StylePriority.UseBorders = false;
            this.lblNgayHetHanGiayPhep.StylePriority.UseTextAlignment = false;
            this.lblNgayHetHanGiayPhep.Text = "19/08/2010";
            this.lblNgayHetHanGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayHetHanGiayPhep.Weight = 0.31002638522427445;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.StylePriority.UseTextAlignment = false;
            this.xrTableCell75.Text = " Ngày hết hạn:";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell75.Weight = 0.33443271767810018;
            // 
            // lblNgayHetHanHopDong
            // 
            this.lblNgayHetHanHopDong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayHetHanHopDong.Name = "lblNgayHetHanHopDong";
            this.lblNgayHetHanHopDong.StylePriority.UseBorders = false;
            this.lblNgayHetHanHopDong.StylePriority.UseTextAlignment = false;
            this.lblNgayHetHanHopDong.Text = "19/08/2010";
            this.lblNgayHetHanHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayHetHanHopDong.Weight = 0.38984168865435348;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.lblMSTNguoiNhapKhau,
            this.xrTableCell10,
            this.xrTableCell114,
            this.xrTableCell115});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1;
            // 
            // lblMSTNguoiNhapKhau
            // 
            this.lblMSTNguoiNhapKhau.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMSTNguoiNhapKhau.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMSTNguoiNK});
            this.lblMSTNguoiNhapKhau.Name = "lblMSTNguoiNhapKhau";
            this.lblMSTNguoiNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblMSTNguoiNhapKhau.StylePriority.UseBorders = false;
            this.lblMSTNguoiNhapKhau.StylePriority.UsePadding = false;
            this.lblMSTNguoiNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMSTNguoiNhapKhau.Text = "0100101308123";
            this.lblMSTNguoiNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMSTNguoiNhapKhau.Weight = 0.69129287598944589;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = " 9.Vận đơn:(số/ngày)";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.65303430079155689;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell114.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorderColor = false;
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.StylePriority.UseForeColor = false;
            this.xrTableCell114.StylePriority.UseTextAlignment = false;
            this.xrTableCell114.Text = " 10.Cảng xếp hàng:";
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell114.Weight = 0.6279683377308708;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell115.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorderColor = false;
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.StylePriority.UseForeColor = false;
            this.xrTableCell115.StylePriority.UseTextAlignment = false;
            this.xrTableCell115.Text = " 11.Cảng dỡ hàng:";
            this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell115.Weight = 0.73218997361477556;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116,
            this.lblVanTaiDon,
            this.lblCangXepHang,
            this.lblMaCangdoHang});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Text = " 3.Người ủy thác/ người được ủy quyền:";
            this.xrTableCell116.Weight = 0.98812664907651693;
            // 
            // lblVanTaiDon
            // 
            this.lblVanTaiDon.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblVanTaiDon.Name = "lblVanTaiDon";
            this.lblVanTaiDon.StylePriority.UseBorders = false;
            this.lblVanTaiDon.StylePriority.UseTextAlignment = false;
            this.lblVanTaiDon.Text = "QLMD02/03";
            this.lblVanTaiDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblVanTaiDon.Weight = 0.65567282321899745;
            // 
            // lblCangXepHang
            // 
            this.lblCangXepHang.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblCangXepHang.Name = "lblCangXepHang";
            this.lblCangXepHang.StylePriority.UseBorders = false;
            this.lblCangXepHang.StylePriority.UseTextAlignment = false;
            this.lblCangXepHang.Text = "SHANGHAI";
            this.lblCangXepHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblCangXepHang.Weight = 0.6279683377308708;
            // 
            // lblMaCangdoHang
            // 
            this.lblMaCangdoHang.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblMaCangdoHang.Name = "lblMaCangdoHang";
            this.lblMaCangdoHang.StylePriority.UseBorders = false;
            this.lblMaCangdoHang.StylePriority.UseTextAlignment = false;
            this.lblMaCangdoHang.Text = "C021";
            this.lblMaCangdoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaCangdoHang.Weight = 0.72823218997361461;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenNguoiUyThac,
            this.lblNgayVanTaiDon,
            this.lblTenCangXepHang,
            this.lblTenCangDoHang});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // lblTenNguoiUyThac
            // 
            this.lblTenNguoiUyThac.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTenNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiUyThac.Name = "lblTenNguoiUyThac";
            this.lblTenNguoiUyThac.StylePriority.UseBorders = false;
            this.lblTenNguoiUyThac.StylePriority.UseFont = false;
            this.lblTenNguoiUyThac.Weight = 0.98812664907651715;
            // 
            // lblNgayVanTaiDon
            // 
            this.lblNgayVanTaiDon.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayVanTaiDon.Name = "lblNgayVanTaiDon";
            this.lblNgayVanTaiDon.StylePriority.UseBorders = false;
            this.lblNgayVanTaiDon.StylePriority.UseTextAlignment = false;
            this.lblNgayVanTaiDon.Text = "17/10/2010";
            this.lblNgayVanTaiDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblNgayVanTaiDon.Weight = 0.65765171503957787;
            // 
            // lblTenCangXepHang
            // 
            this.lblTenCangXepHang.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenCangXepHang.Name = "lblTenCangXepHang";
            this.lblTenCangXepHang.StylePriority.UseBorders = false;
            this.lblTenCangXepHang.Weight = 0.62994722955145122;
            // 
            // lblTenCangDoHang
            // 
            this.lblTenCangDoHang.Name = "lblTenCangDoHang";
            this.lblTenCangDoHang.StylePriority.UseTextAlignment = false;
            this.lblTenCangDoHang.Text = "Cảng Tiên sa";
            this.lblTenCangDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTenCangDoHang.Weight = 0.72427440633245355;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.lblMSTNguoiUT,
            this.xrTableCell3,
            this.lblPhuongTienVanTai,
            this.xrTableCell2,
            this.lblNuocXK});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.StylePriority.UsePadding = false;
            this.xrTableCell119.StylePriority.UseTextAlignment = false;
            this.xrTableCell119.Text = "MST ";
            this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell119.Weight = 0.29815303430079154;
            // 
            // lblMSTNguoiUT
            // 
            this.lblMSTNguoiUT.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMSTNguoiUyThac});
            this.lblMSTNguoiUT.Name = "lblMSTNguoiUT";
            this.lblMSTNguoiUT.StylePriority.UseTextAlignment = false;
            this.lblMSTNguoiUT.Text = "0100101308123";
            this.lblMSTNguoiUT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMSTNguoiUT.Weight = 0.69393139841688656;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = " 12.Phương tiện vận tải:";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 0.5250659630606862;
            // 
            // lblPhuongTienVanTai
            // 
            this.lblPhuongTienVanTai.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.lblPhuongTienVanTai.Name = "lblPhuongTienVanTai";
            this.lblPhuongTienVanTai.StylePriority.UseBorders = false;
            this.lblPhuongTienVanTai.StylePriority.UseTextAlignment = false;
            this.lblPhuongTienVanTai.Text = "QI LIN MEN";
            this.lblPhuongTienVanTai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPhuongTienVanTai.Weight = 0.75461741424802131;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = " 13.Nước xuất khẩu:";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 0.47097625329815285;
            // 
            // lblNuocXK
            // 
            this.lblNuocXK.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblNuocXK.Name = "lblNuocXK";
            this.lblNuocXK.StylePriority.UseBorders = false;
            this.lblNuocXK.StylePriority.UseTextAlignment = false;
            this.lblNuocXK.Text = "CN";
            this.lblNuocXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNuocXK.Weight = 0.25725593667546159;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell77,
            this.lblTenSoHieuPTVT,
            this.xrTableCell78,
            this.lblNgayDenPTVT,
            this.lblTenNuoc});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Text = " 4.Đại lý hải quan:";
            this.xrTableCell123.Weight = 0.98812664907651715;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.StylePriority.UseTextAlignment = false;
            this.xrTableCell77.Text = " Tên, số hiệu:";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell77.Weight = 0.29518469656992091;
            // 
            // lblTenSoHieuPTVT
            // 
            this.lblTenSoHieuPTVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenSoHieuPTVT.Name = "lblTenSoHieuPTVT";
            this.lblTenSoHieuPTVT.StylePriority.UseBorders = false;
            this.lblTenSoHieuPTVT.StylePriority.UseTextAlignment = false;
            this.lblTenSoHieuPTVT.Text = "TenSoHieuPTVT";
            this.lblTenSoHieuPTVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTenSoHieuPTVT.Weight = 0.47328496042216367;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.StylePriority.UseTextAlignment = false;
            this.xrTableCell78.Text = " Ngày đến:";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell78.Weight = 0.23779683377308714;
            // 
            // lblNgayDenPTVT
            // 
            this.lblNgayDenPTVT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblNgayDenPTVT.Name = "lblNgayDenPTVT";
            this.lblNgayDenPTVT.StylePriority.UseBorders = false;
            this.lblNgayDenPTVT.StylePriority.UseTextAlignment = false;
            this.lblNgayDenPTVT.Text = "19/08/2011";
            this.lblNgayDenPTVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayDenPTVT.Weight = 0.27737467018469664;
            // 
            // lblTenNuoc
            // 
            this.lblTenNuoc.Name = "lblTenNuoc";
            this.lblTenNuoc.StylePriority.UseTextAlignment = false;
            this.lblTenNuoc.Text = "China";
            this.lblTenNuoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTenNuoc.Weight = 0.72823218997361461;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenDaiLy,
            this.xrTableCell4,
            this.lblDieuKienGiaoHang,
            this.xrTableCell12,
            this.lblPhuongThucThanhToan});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1;
            // 
            // lblTenDaiLy
            // 
            this.lblTenDaiLy.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTenDaiLy.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDaiLy.Name = "lblTenDaiLy";
            this.lblTenDaiLy.StylePriority.UseBorders = false;
            this.lblTenDaiLy.StylePriority.UseFont = false;
            this.lblTenDaiLy.Weight = 0.98812664907651715;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = " 14.Điều kiện giao hàng:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.56068601583113464;
            // 
            // lblDieuKienGiaoHang
            // 
            this.lblDieuKienGiaoHang.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDieuKienGiaoHang.Name = "lblDieuKienGiaoHang";
            this.lblDieuKienGiaoHang.StylePriority.UseBorders = false;
            this.lblDieuKienGiaoHang.StylePriority.UseTextAlignment = false;
            this.lblDieuKienGiaoHang.Text = "CIF";
            this.lblDieuKienGiaoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDieuKienGiaoHang.Weight = 0.55672823218997369;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = " 15.Phương thức thanh toán:";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 0.62532981530343;
            // 
            // lblPhuongThucThanhToan
            // 
            this.lblPhuongThucThanhToan.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblPhuongThucThanhToan.Name = "lblPhuongThucThanhToan";
            this.lblPhuongThucThanhToan.StylePriority.UseBorders = false;
            this.lblPhuongThucThanhToan.StylePriority.UseTextAlignment = false;
            this.lblPhuongThucThanhToan.Text = "CASH";
            this.lblPhuongThucThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblPhuongThucThanhToan.Weight = 0.2691292875989445;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.lblMSTDaiLy,
            this.xrTableCell11,
            this.lblDongTienThanhToan,
            this.xrTableCell76,
            this.lblTyGiaTinhThue});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.StylePriority.UsePadding = false;
            this.xrTableCell129.StylePriority.UseTextAlignment = false;
            this.xrTableCell129.Text = "MST ";
            this.xrTableCell129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell129.Weight = 0.29815303430079154;
            // 
            // lblMSTDaiLy
            // 
            this.lblMSTDaiLy.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMSTHaiQuan});
            this.lblMSTDaiLy.Name = "lblMSTDaiLy";
            this.lblMSTDaiLy.StylePriority.UseTextAlignment = false;
            this.lblMSTDaiLy.Text = "0100101308123";
            this.lblMSTDaiLy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMSTDaiLy.Weight = 0.69393139841688656;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = " 16.Đồng tiền thanh toán:";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell11.Weight = 0.56068601583113464;
            // 
            // lblDongTienThanhToan
            // 
            this.lblDongTienThanhToan.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDongTienThanhToan.Name = "lblDongTienThanhToan";
            this.lblDongTienThanhToan.StylePriority.UseBorders = false;
            this.lblDongTienThanhToan.StylePriority.UseTextAlignment = false;
            this.lblDongTienThanhToan.Text = "USD";
            this.lblDongTienThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDongTienThanhToan.Weight = 0.55672823218997369;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.Text = " 17.Tỷ giá tính thuế:";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell76.Weight = 0.62730870712401043;
            // 
            // lblTyGiaTinhThue
            // 
            this.lblTyGiaTinhThue.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTyGiaTinhThue.Name = "lblTyGiaTinhThue";
            this.lblTyGiaTinhThue.StylePriority.UseBorders = false;
            this.lblTyGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.lblTyGiaTinhThue.Text = "20803";
            this.lblTyGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTyGiaTinhThue.Weight = 0.263192612137203;
            // 
            // lblCucHaiQuan
            // 
            this.lblCucHaiQuan.BorderWidth = 0;
            this.lblCucHaiQuan.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCucHaiQuan.Location = new System.Drawing.Point(125, 33);
            this.lblCucHaiQuan.Name = "lblCucHaiQuan";
            this.lblCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHaiQuan.Size = new System.Drawing.Size(283, 23);
            this.lblCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.Location = new System.Drawing.Point(608, 8);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.Size = new System.Drawing.Size(173, 25);
            this.winControlContainer1.WinControl = this.label1;
            // 
            // label1
            // 
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 24);
            this.label1.TabIndex = 0;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader1,
            this.ReportFooter1});
            this.DetailReport1.Level = 0;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.Height = 25;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(25, 0);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.Size = new System.Drawing.Size(758, 25);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang1,
            this.lblMoTaHang1,
            this.lblMaHSHang1,
            this.lblXuatXuHang1,
            this.lblCheDoUuDaiHang1,
            this.lblLuongHang1,
            this.lblDVTHang1,
            this.lblDonGiaHang1,
            this.lblTGNTHang1});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow2.Weight = 0.51489361702127678;
            // 
            // lblSTTHang1
            // 
            this.lblSTTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTHang1.Name = "lblSTTHang1";
            this.lblSTTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTHang1.StylePriority.UseBorders = false;
            this.lblSTTHang1.Text = "1";
            this.lblSTTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang1.Weight = 0.032981530343007916;
            // 
            // lblMoTaHang1
            // 
            this.lblMoTaHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMoTaHang1.Multiline = true;
            this.lblMoTaHang1.Name = "lblMoTaHang1";
            this.lblMoTaHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHang1.StylePriority.UseBorders = false;
            this.lblMoTaHang1.StylePriority.UseTextAlignment = false;
            this.lblMoTaHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang1.Weight = 0.25197889182058059;
            // 
            // lblMaHSHang1
            // 
            this.lblMaHSHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaHSHang1.Multiline = true;
            this.lblMaHSHang1.Name = "lblMaHSHang1";
            this.lblMaHSHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHSHang1.StylePriority.UseBorders = false;
            this.lblMaHSHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang1.Weight = 0.12137203166226911;
            // 
            // lblXuatXuHang1
            // 
            this.lblXuatXuHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXuatXuHang1.Name = "lblXuatXuHang1";
            this.lblXuatXuHang1.StylePriority.UseBorders = false;
            this.lblXuatXuHang1.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang1.Weight = 0.098944591029023754;
            // 
            // lblCheDoUuDaiHang1
            // 
            this.lblCheDoUuDaiHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCheDoUuDaiHang1.Multiline = true;
            this.lblCheDoUuDaiHang1.Name = "lblCheDoUuDaiHang1";
            this.lblCheDoUuDaiHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCheDoUuDaiHang1.StylePriority.UseBorders = false;
            this.lblCheDoUuDaiHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblCheDoUuDaiHang1.Weight = 0.0883905013192612;
            // 
            // lblLuongHang1
            // 
            this.lblLuongHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongHang1.Multiline = true;
            this.lblLuongHang1.Name = "lblLuongHang1";
            this.lblLuongHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang1.StylePriority.UseBorders = false;
            this.lblLuongHang1.StylePriority.UseTextAlignment = false;
            this.lblLuongHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang1.Weight = 0.10949868073878628;
            // 
            // lblDVTHang1
            // 
            this.lblDVTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVTHang1.Name = "lblDVTHang1";
            this.lblDVTHang1.StylePriority.UseBorders = false;
            this.lblDVTHang1.StylePriority.UseTextAlignment = false;
            this.lblDVTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang1.Weight = 0.061345646437994727;
            // 
            // lblDonGiaHang1
            // 
            this.lblDonGiaHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang1.Name = "lblDonGiaHang1";
            this.lblDonGiaHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblDonGiaHang1.StylePriority.UseBorders = false;
            this.lblDonGiaHang1.StylePriority.UsePadding = false;
            this.lblDonGiaHang1.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang1.Weight = 0.11642480211081795;
            // 
            // lblTGNTHang1
            // 
            this.lblTGNTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTGNTHang1.Name = "lblTGNTHang1";
            this.lblTGNTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGNTHang1.StylePriority.UseBorders = false;
            this.lblTGNTHang1.StylePriority.UsePadding = false;
            this.lblTGNTHang1.StylePriority.UseTextAlignment = false;
            this.lblTGNTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang1.Weight = 0.11906332453825859;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.ReportHeader1.Height = 35;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable11
            // 
            this.xrTable11.Location = new System.Drawing.Point(25, 0);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable11.Size = new System.Drawing.Size(758, 35);
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell15,
            this.xrTableCell67,
            this.xrTableCell68});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.StylePriority.UseTextAlignment = false;
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow14.Weight = 0.45053191489361721;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "Số TT";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell61.Weight = 0.032981530343007916;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Multiline = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "18.Mô tả hàng hóa";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 0.25197889182058059;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Multiline = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "19.Mã số hàng hóa";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.12137203166226911;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.Text = "20.Xuất xứ";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 0.098944591029023754;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Multiline = true;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.Text = "21.Chế độ ưu đãi";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell65.Weight = 0.0883905013192612;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Multiline = true;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "22.Lượng hàng";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell66.Weight = 0.10949868073878628;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "23.Đơn vị tính";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.061345646437994727;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.Text = "24.Đơn giá ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 0.11675461741424802;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.Text = "21.Trị giá nguyên tệ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.11873350923482852;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.ReportFooter1.Height = 172;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Location = new System.Drawing.Point(25, 0);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow10,
            this.xrTableRow9,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow11});
            this.xrTable2.Size = new System.Drawing.Size(758, 172);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell33});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow3.Weight = 0.49972512938470404;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = "Loại thuế";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell26.Weight = 0.28496042216358841;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 0.30870712401055411;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Multiline = true;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell30.Weight = 0.10949868073878628;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Text = "Tiền thuế";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell33.Weight = 0.29683377308707126;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.lblTGTTNKHang1,
            this.lblThueSuatNKHang1,
            this.lblTienThueNKHang1});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.StylePriority.UseTextAlignment = false;
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow10.Weight = 0.36272167912593462;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = " 26. Thuế nhập khẩu";
            this.xrTableCell53.Weight = 0.28496042216358841;
            // 
            // lblTGTTNKHang1
            // 
            this.lblTGTTNKHang1.Name = "lblTGTTNKHang1";
            this.lblTGTTNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGTTNKHang1.StylePriority.UsePadding = false;
            this.lblTGTTNKHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang1.Text = "0";
            this.lblTGTTNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang1.Weight = 0.30870712401055411;
            // 
            // lblThueSuatNKHang1
            // 
            this.lblThueSuatNKHang1.Name = "lblThueSuatNKHang1";
            this.lblThueSuatNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatNKHang1.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang1.Text = "0";
            this.lblThueSuatNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatNKHang1.Weight = 0.10949868073878628;
            // 
            // lblTienThueNKHang1
            // 
            this.lblTienThueNKHang1.Name = "lblTienThueNKHang1";
            this.lblTienThueNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTienThueNKHang1.StylePriority.UseBorders = false;
            this.lblTienThueNKHang1.StylePriority.UsePadding = false;
            this.lblTienThueNKHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang1.Text = "0";
            this.lblTienThueNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang1.Weight = 0.29683377308707126;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.lblTGTTTTDBHang1,
            this.lblThueSuatTTDBHang1,
            this.lblTienThueTTDBHang1});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 0.34894479585968952;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = " 27.Thuế TTĐB";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell49.Weight = 0.28496042216358841;
            // 
            // lblTGTTTTDBHang1
            // 
            this.lblTGTTTTDBHang1.Name = "lblTGTTTTDBHang1";
            this.lblTGTTTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGTTTTDBHang1.StylePriority.UsePadding = false;
            this.lblTGTTTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTTTDBHang1.Text = "0";
            this.lblTGTTTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTTTDBHang1.Weight = 0.30870712401055411;
            // 
            // lblThueSuatTTDBHang1
            // 
            this.lblThueSuatTTDBHang1.Name = "lblThueSuatTTDBHang1";
            this.lblThueSuatTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatTTDBHang1.StylePriority.UsePadding = false;
            this.lblThueSuatTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTTDBHang1.Text = "0";
            this.lblThueSuatTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatTTDBHang1.Weight = 0.10949868073878628;
            // 
            // lblTienThueTTDBHang1
            // 
            this.lblTienThueTTDBHang1.Name = "lblTienThueTTDBHang1";
            this.lblTienThueTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTienThueTTDBHang1.StylePriority.UseBorders = false;
            this.lblTienThueTTDBHang1.StylePriority.UsePadding = false;
            this.lblTienThueTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueTTDBHang1.Text = "0";
            this.lblTienThueTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueTTDBHang1.Weight = 0.29683377308707126;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.lblTGTTBVMTHang1,
            this.lblThueSuatBVMTHang1,
            this.lblTienThueBVMTHang1});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.Weight = 0.34894479585968963;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.Text = " 28.Thuế  BVMT";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell45.Weight = 0.28496042216358841;
            // 
            // lblTGTTBVMTHang1
            // 
            this.lblTGTTBVMTHang1.Name = "lblTGTTBVMTHang1";
            this.lblTGTTBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGTTBVMTHang1.StylePriority.UsePadding = false;
            this.lblTGTTBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTBVMTHang1.Text = "0";
            this.lblTGTTBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTBVMTHang1.Weight = 0.30870712401055411;
            // 
            // lblThueSuatBVMTHang1
            // 
            this.lblThueSuatBVMTHang1.Name = "lblThueSuatBVMTHang1";
            this.lblThueSuatBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatBVMTHang1.StylePriority.UsePadding = false;
            this.lblThueSuatBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatBVMTHang1.Text = "0";
            this.lblThueSuatBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatBVMTHang1.Weight = 0.10949868073878628;
            // 
            // lblTienThueBVMTHang1
            // 
            this.lblTienThueBVMTHang1.Name = "lblTienThueBVMTHang1";
            this.lblTienThueBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTienThueBVMTHang1.StylePriority.UseBorders = false;
            this.lblTienThueBVMTHang1.StylePriority.UsePadding = false;
            this.lblTienThueBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueBVMTHang1.Text = "0";
            this.lblTienThueBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueBVMTHang1.Weight = 0.29683377308707126;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.lblTGTTVATHang1,
            this.lblThueSuatVATHang1,
            this.lblTienThueVATHang1});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.Weight = 0.34894479585968952;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.Text = " 29.Thuế GTGT";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.28496042216358841;
            // 
            // lblTGTTVATHang1
            // 
            this.lblTGTTVATHang1.Name = "lblTGTTVATHang1";
            this.lblTGTTVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTGTTVATHang1.StylePriority.UsePadding = false;
            this.lblTGTTVATHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang1.Text = "0";
            this.lblTGTTVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang1.Weight = 0.30870712401055411;
            // 
            // lblThueSuatVATHang1
            // 
            this.lblThueSuatVATHang1.Name = "lblThueSuatVATHang1";
            this.lblThueSuatVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatVATHang1.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang1.Text = "0";
            this.lblThueSuatVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatVATHang1.Weight = 0.10949868073878628;
            // 
            // lblTienThueVATHang1
            // 
            this.lblTienThueVATHang1.Name = "lblTienThueVATHang1";
            this.lblTienThueVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTienThueVATHang1.StylePriority.UsePadding = false;
            this.lblTienThueVATHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang1.Text = "0";
            this.lblTienThueVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang1.Weight = 0.29683377308707126;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.lblTongTienThue});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.34984933870040269;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.StylePriority.UseTextAlignment = false;
            this.xrTableCell79.Text = " 30.Tổng số tiền thuế(ô 26+27+28+29):";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell79.Weight = 0.29683377308707126;
            // 
            // lblTongTienThue
            // 
            this.lblTongTienThue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTongTienThue.Name = "lblTongTienThue";
            this.lblTongTienThue.StylePriority.UseBorders = false;
            this.lblTongTienThue.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue.Text = "1.069.215.815";
            this.lblTongTienThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTongTienThue.Weight = 0.70316622691292874;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80,
            this.lblBangChu});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.34984933870040258;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.StylePriority.UseTextAlignment = false;
            this.xrTableCell80.Text = " Bằng chữ:";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell80.Weight = 0.098944591029023782;
            // 
            // lblBangChu
            // 
            this.lblBangChu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblBangChu.Name = "lblBangChu";
            this.lblBangChu.StylePriority.UseBorders = false;
            this.lblBangChu.StylePriority.UseTextAlignment = false;
            this.lblBangChu.Text = "Một tỷ không trăm sáu mươi chín triệu hai trăm mười lăm nghìn tám trăm mười lăm đ" +
                "ồng chẵn";
            this.lblBangChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblBangChu.Weight = 0.90105540897097625;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.ReportHeader3,
            this.ReportFooter3});
            this.DetailReport3.Level = 1;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail3.Height = 75;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Location = new System.Drawing.Point(25, 0);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17});
            this.xrTable4.Size = new System.Drawing.Size(758, 75);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont1,
            this.lblSoHieuCont1,
            this.lblSoLuongKienCont1,
            this.lblTrongLuongCont1});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1.6800000000000002;
            // 
            // lblSTTCont1
            // 
            this.lblSTTCont1.Name = "lblSTTCont1";
            this.lblSTTCont1.StylePriority.UseTextAlignment = false;
            this.lblSTTCont1.Text = "1";
            this.lblSTTCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont1.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont1
            // 
            this.lblSoHieuCont1.Name = "lblSoHieuCont1";
            this.lblSoHieuCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont1.StylePriority.UsePadding = false;
            this.lblSoHieuCont1.Weight = 0.891820580474934;
            // 
            // lblSoLuongKienCont1
            // 
            this.lblSoLuongKienCont1.Name = "lblSoLuongKienCont1";
            this.lblSoLuongKienCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont1.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont1.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont1.Weight = 1.1187335092348281;
            // 
            // lblTrongLuongCont1
            // 
            this.lblTrongLuongCont1.Name = "lblTrongLuongCont1";
            this.lblTrongLuongCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont1.StylePriority.UsePadding = false;
            this.lblTrongLuongCont1.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont1.Weight = 0.88918205804749351;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont2,
            this.lblSoHieuCont2,
            this.lblSoLuongKienCont2,
            this.lblTrongLuongCont2});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 1.6800000000000002;
            // 
            // lblSTTCont2
            // 
            this.lblSTTCont2.Name = "lblSTTCont2";
            this.lblSTTCont2.StylePriority.UseTextAlignment = false;
            this.lblSTTCont2.Text = "2";
            this.lblSTTCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont2.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont2
            // 
            this.lblSoHieuCont2.Name = "lblSoHieuCont2";
            this.lblSoHieuCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont2.StylePriority.UsePadding = false;
            this.lblSoHieuCont2.Weight = 0.891820580474934;
            // 
            // lblSoLuongKienCont2
            // 
            this.lblSoLuongKienCont2.Name = "lblSoLuongKienCont2";
            this.lblSoLuongKienCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont2.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont2.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont2.Weight = 1.1187335092348281;
            // 
            // lblTrongLuongCont2
            // 
            this.lblTrongLuongCont2.Name = "lblTrongLuongCont2";
            this.lblTrongLuongCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont2.StylePriority.UsePadding = false;
            this.lblTrongLuongCont2.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont2.Weight = 0.88918205804749351;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont3,
            this.lblSoHieuCont3,
            this.lblSoLuongKienCont3,
            this.lblTrongLuongCont3});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.Weight = 1.6800000000000002;
            // 
            // lblSTTCont3
            // 
            this.lblSTTCont3.Name = "lblSTTCont3";
            this.lblSTTCont3.StylePriority.UseTextAlignment = false;
            this.lblSTTCont3.Text = "3";
            this.lblSTTCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont3.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont3
            // 
            this.lblSoHieuCont3.Name = "lblSoHieuCont3";
            this.lblSoHieuCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont3.StylePriority.UsePadding = false;
            this.lblSoHieuCont3.Weight = 0.891820580474934;
            // 
            // lblSoLuongKienCont3
            // 
            this.lblSoLuongKienCont3.Name = "lblSoLuongKienCont3";
            this.lblSoLuongKienCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont3.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont3.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont3.Weight = 1.1187335092348281;
            // 
            // lblTrongLuongCont3
            // 
            this.lblTrongLuongCont3.Name = "lblTrongLuongCont3";
            this.lblTrongLuongCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont3.StylePriority.UsePadding = false;
            this.lblTrongLuongCont3.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont3.Weight = 0.88918205804749351;
            // 
            // ReportHeader3
            // 
            this.ReportHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.ReportHeader3.Height = 58;
            this.ReportHeader3.Name = "ReportHeader3";
            // 
            // xrTable6
            // 
            this.xrTable6.Location = new System.Drawing.Point(25, 0);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5});
            this.xrTable6.Size = new System.Drawing.Size(758, 58);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "31.Lượng hàng, số hiệu container";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell25.Weight = 3;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell31,
            this.xrTableCell32});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow5.Weight = 1.3200000000000003;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "Số\r\n TT";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell27.Weight = 0.10026385224274405;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Text = "a.Số hiệu container";
            this.xrTableCell28.Weight = 0.891820580474934;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "b.Số lượng kiện trong container";
            this.xrTableCell31.Weight = 1.1187335092348281;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "c.Trọng lượng hàng trong container";
            this.xrTableCell32.Weight = 0.88918205804749351;
            // 
            // ReportFooter3
            // 
            this.ReportFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter3.Height = 25;
            this.ReportFooter3.Name = "ReportFooter3";
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Location = new System.Drawing.Point(25, 0);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable5.Size = new System.Drawing.Size(758, 25);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell39});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1.6800000000000002;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.10026385224274405;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.891820580474934;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 1.1187335092348281;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.lblTongContainer});
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseFont = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell39.Weight = 0.88918205804749351;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Location = new System.Drawing.Point(0, 0);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel2.Size = new System.Drawing.Size(50, 25);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = " Cộng:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTongContainer
            // 
            this.lblTongContainer.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblTongContainer.Location = new System.Drawing.Point(50, 0);
            this.lblTongContainer.Name = "lblTongContainer";
            this.lblTongContainer.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblTongContainer.Size = new System.Drawing.Size(175, 25);
            this.lblTongContainer.StylePriority.UseBorders = false;
            this.lblTongContainer.StylePriority.UseTextAlignment = false;
            this.lblTongContainer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GroupFooter
            // 
            this.GroupFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.GroupFooter.Height = 298;
            this.GroupFooter.Name = "GroupFooter";
            // 
            // xrTable7
            // 
            this.xrTable7.Location = new System.Drawing.Point(25, 0);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow13,
            this.xrTableRow24});
            this.xrTable7.Size = new System.Drawing.Size(758, 298);
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 4.88;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.lblChungTuDiKem});
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Weight = 1.2176781002638522;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Location = new System.Drawing.Point(0, 0);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.Size = new System.Drawing.Size(308, 25);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = " 32.Chứng từ đi kèm:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblChungTuDiKem
            // 
            this.lblChungTuDiKem.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblChungTuDiKem.Location = new System.Drawing.Point(0, 25);
            this.lblChungTuDiKem.Name = "lblChungTuDiKem";
            this.lblChungTuDiKem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblChungTuDiKem.Size = new System.Drawing.Size(308, 100);
            this.lblChungTuDiKem.StylePriority.UseBorders = false;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell82.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.lblNgayIn,
            this.xrLabel7});
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseBorders = false;
            this.xrTableCell82.Weight = 1.7823218997361474;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Location = new System.Drawing.Point(100, 0);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.Size = new System.Drawing.Size(250, 33);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "33.Tôi xin cam đoan, chịu trách nhiệm trước         pháp luật về nội dung khai tr" +
                "ên tờ khai";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayIn
            // 
            this.lblNgayIn.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblNgayIn.Location = new System.Drawing.Point(100, 33);
            this.lblNgayIn.Name = "lblNgayIn";
            this.lblNgayIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayIn.Size = new System.Drawing.Size(250, 17);
            this.lblNgayIn.StylePriority.UseBorders = false;
            this.lblNgayIn.StylePriority.UseTextAlignment = false;
            this.lblNgayIn.Text = "Ngày...tháng...năm.....";
            this.lblNgayIn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic);
            this.xrLabel7.Location = new System.Drawing.Point(100, 50);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.Size = new System.Drawing.Size(250, 23);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "(Người khai ký, ghi rõ họ tên, đóng dấu)";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell84,
            this.xrTableCell69,
            this.xrTableCell85});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 1.4399999999999995;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "34. Kết quả phân luồng và hướng dẫn làm thủ tục hải quan";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell40.Weight = 0.98878627968337729;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.Text = "36. Xác nhận của hải quan giám sát";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell84.Weight = 0.692612137203166;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.Text = "37.Xác nhận giải phóng hàng/ đưa hàng về bảo quản/ chuyển cửa khẩu";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell69.Weight = 0.78990765171503952;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.StylePriority.UseTextAlignment = false;
            this.xrTableCell85.Text = " 38. Xác nhận thông quan";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell85.Weight = 0.52869393139841681;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblKetQuaPhanLuong,
            this.xrTableCell83,
            this.xrTableCell91,
            this.xrTableCell93});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 3.3999999999999981;
            // 
            // lblKetQuaPhanLuong
            // 
            this.lblKetQuaPhanLuong.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.lblKetQuaPhanLuong.Multiline = true;
            this.lblKetQuaPhanLuong.Name = "lblKetQuaPhanLuong";
            this.lblKetQuaPhanLuong.StylePriority.UseBorders = false;
            this.lblKetQuaPhanLuong.Weight = 0.98878627968337729;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.Weight = 0.692612137203166;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.78990765171503952;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.52869393139841681;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell70,
            this.xrTableCell88});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 2.2000000000000011;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.Text = "35. Ghi chép khác:";
            this.xrTableCell86.Weight = 0.988126649076517;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.Weight = 0.6932717678100262;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Weight = 0.78990765171503952;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Weight = 0.52869393139841681;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UsePadding = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "MST";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.29551451187335093;
            // 
            // lblMSTNguoiNK
            // 
            this.lblMSTNguoiNK.Location = new System.Drawing.Point(0, 0);
            this.lblMSTNguoiNK.Name = "lblMSTNguoiNK";
            this.lblMSTNguoiNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMSTNguoiNK.Size = new System.Drawing.Size(175, 26);
            // 
            // lblMSTNguoiUyThac
            // 
            this.lblMSTNguoiUyThac.Location = new System.Drawing.Point(0, 0);
            this.lblMSTNguoiUyThac.Name = "lblMSTNguoiUyThac";
            this.lblMSTNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMSTNguoiUyThac.Size = new System.Drawing.Size(175, 26);
            // 
            // lblMSTHaiQuan
            // 
            this.lblMSTHaiQuan.Location = new System.Drawing.Point(0, 0);
            this.lblMSTHaiQuan.Name = "lblMSTHaiQuan";
            this.lblMSTHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMSTHaiQuan.Size = new System.Drawing.Size(175, 26);
            // 
            // TQDTToKhaiNK_TT15
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader,
            this.DetailReport1,
            this.DetailReport3,
            this.GroupFooter});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(5, 15, 4, 18);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell lblChiCucHQDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaySoThamChieu;
        private DevExpress.XtraReports.UI.XRTableCell lblSoPhuLucToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoiXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell lblCangXepHang;
        private DevExpress.XtraReports.UI.XRTableCell lblMaCangdoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiUyThac;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCangXepHang;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCangDoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell lblMSTNguoiUT;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongTienVanTai;
        private DevExpress.XtraReports.UI.XRTableCell lblNuocXK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDenPTVT;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNuoc;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDaiLy;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKienGiaoHang;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongThucThanhToan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell lblMSTDaiLy;
        private DevExpress.XtraReports.UI.XRTableCell lblDongTienThanhToan;
        private DevExpress.XtraReports.UI.XRTableCell lblTyGiaTinhThue;
        private DevExpress.XtraReports.UI.XRTableCell lblMSTNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell lblVanTaiDon;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayVanTaiDon;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSoHieuPTVT;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThue;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell lblBangChu;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter3;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel lblNgayIn;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRLabel lblCucHaiQuan;
        private DevExpress.XtraReports.UI.XRTableCell lblGiayPhep;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblHoaDonThuongMai;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell lblHopDong;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoiNK;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHetHanGiayPhep;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHetHanHopDong;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraReports.UI.XRTableCell lblChiCucHQCuaKhau;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHoaDonThuongMai;
        private DevExpress.XtraReports.UI.XRTableCell lblSoThamChieu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lblKetQuaPhanLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblChungTuDiKem;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblTongContainer;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRLabel lblMSTNguoiNK;
        private DevExpress.XtraReports.UI.XRLabel lblMSTNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblMSTHaiQuan;
    }
}
