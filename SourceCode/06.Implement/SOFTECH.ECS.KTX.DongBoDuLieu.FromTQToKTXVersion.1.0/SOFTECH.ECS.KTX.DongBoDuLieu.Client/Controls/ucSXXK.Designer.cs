namespace DongBo.Controls
{
    partial class ucSXXK
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListNPLHQ_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListSPHQ_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDMHQ_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKHQ_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListNPLDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListSPDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDMDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucSXXK));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabHQ = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dgListNPLHQ = new Janus.Windows.GridEX.GridEX();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dgListSPHQ = new Janus.Windows.GridEX.GridEX();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.dgListDMHQ = new Janus.Windows.GridEX.GridEX();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.dgListTKHQ = new Janus.Windows.GridEX.GridEX();
            this.lblTitleDestination = new System.Windows.Forms.Label();
            this.lblTitleSource = new System.Windows.Forms.Label();
            this.chkOver = new System.Windows.Forms.CheckBox();
            this.tabDN = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgListNPLDN = new Janus.Windows.GridEX.GridEX();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgListSPDN = new Janus.Windows.GridEX.GridEX();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgListDMDN = new Janus.Windows.GridEX.GridEX();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgListTKDN = new Janus.Windows.GridEX.GridEX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMoveLeft = new System.Windows.Forms.Button();
            this.btnMoveRight = new System.Windows.Forms.Button();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnExportXML = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pnlBottom.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabHQ.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLHQ)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPHQ)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDMHQ)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKHQ)).BeginInit();
            this.tabDN.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLDN)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPDN)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDMDN)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKDN)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.label5);
            this.pnlBottom.Controls.Add(this.label2);
            this.pnlBottom.Controls.Add(this.pictureBox2);
            this.pnlBottom.Controls.Add(this.pictureBox1);
            this.pnlBottom.Controls.Add(this.btnExportXML);
            this.pnlBottom.Location = new System.Drawing.Point(5, 369);
            this.pnlBottom.Size = new System.Drawing.Size(644, 49);
            // 
            // pnlTop
            // 
            this.pnlTop.Size = new System.Drawing.Size(644, 28);
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.tableLayoutPanel1);
            this.pnlContent.Size = new System.Drawing.Size(644, 341);
            // 
            // lblCaption
            // 
            this.lblCaption.Size = new System.Drawing.Size(275, 22);
            this.lblCaption.Text = "Đồng bộ dữ liệu phân hệ SXXK";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.LightBlue;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tabHQ, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTitleDestination, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTitleSource, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkOver, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tabDN, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(634, 331);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tabHQ
            // 
            this.tabHQ.Controls.Add(this.tabPage5);
            this.tabHQ.Controls.Add(this.tabPage6);
            this.tabHQ.Controls.Add(this.tabPage8);
            this.tabHQ.Controls.Add(this.tabPage7);
            this.tabHQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabHQ.Location = new System.Drawing.Point(3, 20);
            this.tabHQ.Name = "tabHQ";
            this.tabHQ.SelectedIndex = 0;
            this.tabHQ.Size = new System.Drawing.Size(293, 285);
            this.tabHQ.TabIndex = 24;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dgListNPLHQ);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(285, 259);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Nguyên Phụ Liệu";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dgListNPLHQ
            // 
            this.dgListNPLHQ.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPLHQ.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListNPLHQ.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListNPLHQ_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLHQ_DesignTimeLayout.LayoutString");
            this.dgListNPLHQ.DesignTimeLayout = dgListNPLHQ_DesignTimeLayout;
            this.dgListNPLHQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLHQ.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLHQ.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLHQ.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLHQ.GroupByBoxVisible = false;
            this.dgListNPLHQ.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLHQ.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListNPLHQ.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLHQ.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPLHQ.Hierarchical = true;
            this.dgListNPLHQ.Location = new System.Drawing.Point(3, 3);
            this.dgListNPLHQ.Name = "dgListNPLHQ";
            this.dgListNPLHQ.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPLHQ.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLHQ.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLHQ.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLHQ.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListNPLHQ.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListNPLHQ.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLHQ.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListNPLHQ.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPLHQ.Size = new System.Drawing.Size(279, 253);
            this.dgListNPLHQ.TabIndex = 24;
            this.dgListNPLHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListNPLHQ.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListNPLHQ_KeyDown);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.dgListSPHQ);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(285, 259);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Sản Phẩm";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // dgListSPHQ
            // 
            this.dgListSPHQ.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSPHQ.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListSPHQ.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListSPHQ_DesignTimeLayout.LayoutString = resources.GetString("dgListSPHQ_DesignTimeLayout.LayoutString");
            this.dgListSPHQ.DesignTimeLayout = dgListSPHQ_DesignTimeLayout;
            this.dgListSPHQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSPHQ.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListSPHQ.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPHQ.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSPHQ.GroupByBoxVisible = false;
            this.dgListSPHQ.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPHQ.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListSPHQ.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPHQ.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSPHQ.Hierarchical = true;
            this.dgListSPHQ.Location = new System.Drawing.Point(3, 3);
            this.dgListSPHQ.Name = "dgListSPHQ";
            this.dgListSPHQ.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListSPHQ.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSPHQ.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListSPHQ.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPHQ.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListSPHQ.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListSPHQ.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPHQ.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListSPHQ.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListSPHQ.Size = new System.Drawing.Size(279, 253);
            this.dgListSPHQ.TabIndex = 25;
            this.dgListSPHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListSPHQ.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListSPHQ_KeyDown);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.dgListDMHQ);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(285, 259);
            this.tabPage8.TabIndex = 4;
            this.tabPage8.Text = "Định mức";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // dgListDMHQ
            // 
            this.dgListDMHQ.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDMHQ.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDMHQ.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListDMHQ_DesignTimeLayout.LayoutString = resources.GetString("dgListDMHQ_DesignTimeLayout.LayoutString");
            this.dgListDMHQ.DesignTimeLayout = dgListDMHQ_DesignTimeLayout;
            this.dgListDMHQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDMHQ.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDMHQ.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMHQ.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDMHQ.GroupByBoxVisible = false;
            this.dgListDMHQ.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMHQ.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDMHQ.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMHQ.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDMHQ.Hierarchical = true;
            this.dgListDMHQ.Location = new System.Drawing.Point(3, 3);
            this.dgListDMHQ.Name = "dgListDMHQ";
            this.dgListDMHQ.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDMHQ.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDMHQ.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDMHQ.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMHQ.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDMHQ.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDMHQ.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMHQ.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDMHQ.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDMHQ.Size = new System.Drawing.Size(279, 253);
            this.dgListDMHQ.TabIndex = 25;
            this.dgListDMHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListDMHQ.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListDMHQ_KeyDown);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.dgListTKHQ);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(285, 259);
            this.tabPage7.TabIndex = 3;
            this.tabPage7.Text = "Tờ khai";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // dgListTKHQ
            // 
            this.dgListTKHQ.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKHQ.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListTKHQ.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListTKHQ_DesignTimeLayout.LayoutString = resources.GetString("dgListTKHQ_DesignTimeLayout.LayoutString");
            this.dgListTKHQ.DesignTimeLayout = dgListTKHQ_DesignTimeLayout;
            this.dgListTKHQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTKHQ.FlatBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.dgListTKHQ.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKHQ.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKHQ.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKHQ.GroupByBoxVisible = false;
            this.dgListTKHQ.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKHQ.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListTKHQ.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKHQ.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTKHQ.Hierarchical = true;
            this.dgListTKHQ.Location = new System.Drawing.Point(3, 3);
            this.dgListTKHQ.Name = "dgListTKHQ";
            this.dgListTKHQ.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKHQ.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKHQ.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKHQ.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKHQ.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKHQ.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListTKHQ.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKHQ.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKHQ.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTKHQ.Size = new System.Drawing.Size(279, 253);
            this.dgListTKHQ.TabIndex = 25;
            this.dgListTKHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTKHQ.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListTKHQ_KeyDown);
            // 
            // lblTitleDestination
            // 
            this.lblTitleDestination.AutoSize = true;
            this.lblTitleDestination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitleDestination.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleDestination.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleDestination.Image = ((System.Drawing.Image)(resources.GetObject("lblTitleDestination.Image")));
            this.lblTitleDestination.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTitleDestination.Location = new System.Drawing.Point(337, 0);
            this.lblTitleDestination.Name = "lblTitleDestination";
            this.lblTitleDestination.Size = new System.Drawing.Size(294, 17);
            this.lblTitleDestination.TabIndex = 1;
            this.lblTitleDestination.Text = "Dữ liệu Doanh Nghiệp";
            this.lblTitleDestination.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTitleSource
            // 
            this.lblTitleSource.AutoSize = true;
            this.lblTitleSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitleSource.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleSource.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleSource.Image = ((System.Drawing.Image)(resources.GetObject("lblTitleSource.Image")));
            this.lblTitleSource.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitleSource.Location = new System.Drawing.Point(3, 0);
            this.lblTitleSource.Name = "lblTitleSource";
            this.lblTitleSource.Size = new System.Drawing.Size(293, 17);
            this.lblTitleSource.TabIndex = 1;
            this.lblTitleSource.Text = "Dữ liệu Hải Quan";
            this.lblTitleSource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkOver
            // 
            this.chkOver.AutoSize = true;
            this.chkOver.Location = new System.Drawing.Point(3, 311);
            this.chkOver.Name = "chkOver";
            this.chkOver.Size = new System.Drawing.Size(91, 17);
            this.chkOver.TabIndex = 9;
            this.chkOver.Text = "Ghi đè dữ liệu";
            this.chkOver.UseVisualStyleBackColor = true;
            this.chkOver.CheckedChanged += new System.EventHandler(this.chkOver_CheckedChanged);
            // 
            // tabDN
            // 
            this.tabDN.Controls.Add(this.tabPage1);
            this.tabDN.Controls.Add(this.tabPage2);
            this.tabDN.Controls.Add(this.tabPage3);
            this.tabDN.Controls.Add(this.tabPage4);
            this.tabDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabDN.Location = new System.Drawing.Point(337, 20);
            this.tabDN.Name = "tabDN";
            this.tabDN.SelectedIndex = 0;
            this.tabDN.Size = new System.Drawing.Size(294, 285);
            this.tabDN.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgListNPLDN);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(286, 259);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Nguyên Phụ Liệu";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgListNPLDN
            // 
            this.dgListNPLDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPLDN.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListNPLDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListNPLDN_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLDN_DesignTimeLayout.LayoutString");
            this.dgListNPLDN.DesignTimeLayout = dgListNPLDN_DesignTimeLayout;
            this.dgListNPLDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLDN.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLDN.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLDN.GroupByBoxVisible = false;
            this.dgListNPLDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListNPLDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPLDN.Hierarchical = true;
            this.dgListNPLDN.Location = new System.Drawing.Point(3, 3);
            this.dgListNPLDN.Name = "dgListNPLDN";
            this.dgListNPLDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPLDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLDN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLDN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListNPLDN.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListNPLDN.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListNPLDN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPLDN.Size = new System.Drawing.Size(280, 253);
            this.dgListNPLDN.TabIndex = 24;
            this.dgListNPLDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListNPLDN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListNPLDN_KeyDown);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgListSPDN);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(286, 259);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sản Phẩm";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgListSPDN
            // 
            this.dgListSPDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSPDN.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListSPDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListSPDN_DesignTimeLayout.LayoutString = resources.GetString("dgListSPDN_DesignTimeLayout.LayoutString");
            this.dgListSPDN.DesignTimeLayout = dgListSPDN_DesignTimeLayout;
            this.dgListSPDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSPDN.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListSPDN.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSPDN.GroupByBoxVisible = false;
            this.dgListSPDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListSPDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSPDN.Hierarchical = true;
            this.dgListSPDN.Location = new System.Drawing.Point(3, 3);
            this.dgListSPDN.Name = "dgListSPDN";
            this.dgListSPDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListSPDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSPDN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListSPDN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListSPDN.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListSPDN.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListSPDN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListSPDN.Size = new System.Drawing.Size(280, 253);
            this.dgListSPDN.TabIndex = 25;
            this.dgListSPDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListSPDN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListSPDN_KeyDown);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgListDMDN);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(286, 259);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "Định mức";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgListDMDN
            // 
            this.dgListDMDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDMDN.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDMDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListDMDN_DesignTimeLayout.LayoutString = resources.GetString("dgListDMDN_DesignTimeLayout.LayoutString");
            this.dgListDMDN.DesignTimeLayout = dgListDMDN_DesignTimeLayout;
            this.dgListDMDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDMDN.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDMDN.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDMDN.GroupByBoxVisible = false;
            this.dgListDMDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDMDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDMDN.Hierarchical = true;
            this.dgListDMDN.Location = new System.Drawing.Point(3, 3);
            this.dgListDMDN.Name = "dgListDMDN";
            this.dgListDMDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDMDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDMDN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDMDN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDMDN.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDMDN.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDMDN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDMDN.Size = new System.Drawing.Size(280, 253);
            this.dgListDMDN.TabIndex = 25;
            this.dgListDMDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListDMDN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListDMDN_KeyDown);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgListTKDN);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(286, 259);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tờ khai";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgListTKDN
            // 
            this.dgListTKDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKDN.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListTKDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListTKDN_DesignTimeLayout.LayoutString = resources.GetString("dgListTKDN_DesignTimeLayout.LayoutString");
            this.dgListTKDN.DesignTimeLayout = dgListTKDN_DesignTimeLayout;
            this.dgListTKDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTKDN.FlatBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.dgListTKDN.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKDN.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKDN.GroupByBoxVisible = false;
            this.dgListTKDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListTKDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTKDN.Hierarchical = true;
            this.dgListTKDN.Location = new System.Drawing.Point(3, 3);
            this.dgListTKDN.Name = "dgListTKDN";
            this.dgListTKDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKDN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKDN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKDN.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListTKDN.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKDN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTKDN.Size = new System.Drawing.Size(280, 253);
            this.dgListTKDN.TabIndex = 25;
            this.dgListTKDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTKDN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListTKDN_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnMoveLeft);
            this.panel1.Controls.Add(this.btnMoveRight);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(302, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(29, 285);
            this.panel1.TabIndex = 20;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnMoveLeft
            // 
            this.btnMoveLeft.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnMoveLeft.AutoSize = true;
            this.btnMoveLeft.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeft.Image = global::DongBo.Properties.Resources.pictureBox2_Image;
            this.btnMoveLeft.Location = new System.Drawing.Point(0, 143);
            this.btnMoveLeft.Name = "btnMoveLeft";
            this.btnMoveLeft.Size = new System.Drawing.Size(29, 23);
            this.btnMoveLeft.TabIndex = 0;
            this.btnMoveLeft.UseVisualStyleBackColor = true;
            this.btnMoveLeft.Click += new System.EventHandler(this.btnMoveLeft_Click);
            // 
            // btnMoveRight
            // 
            this.btnMoveRight.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnMoveRight.AutoSize = true;
            this.btnMoveRight.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRight.Image = global::DongBo.Properties.Resources.pictureBox1_Image;
            this.btnMoveRight.Location = new System.Drawing.Point(0, 114);
            this.btnMoveRight.Name = "btnMoveRight";
            this.btnMoveRight.Size = new System.Drawing.Size(29, 23);
            this.btnMoveRight.TabIndex = 0;
            this.btnMoveRight.UseVisualStyleBackColor = true;
            this.btnMoveRight.Click += new System.EventHandler(this.btnMoveRight_Click);
            // 
            // imgList
            // 
            this.imgList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imgList.ImageSize = new System.Drawing.Size(16, 16);
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(352, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Chỉ chuyển tất cả dữ liệu mới trong danh sách được chọn qua dữ liệu DN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(283, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Loại bỏ dữ liệu đã được chọn ra khỏi danh sách dữ liệu DN";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(14, 26);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(14, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnExportXML
            // 
            this.btnExportXML.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnExportXML.AutoSize = true;
            this.btnExportXML.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportXML.Location = new System.Drawing.Point(550, 1);
            this.btnExportXML.Name = "btnExportXML";
            this.btnExportXML.Size = new System.Drawing.Size(90, 27);
            this.btnExportXML.TabIndex = 26;
            this.btnExportXML.Text = "Xuất XML";
            this.btnExportXML.UseVisualStyleBackColor = true;
            this.btnExportXML.Click += new System.EventHandler(this.btnExportXML_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "_SXXK";
            this.saveFileDialog1.Filter = "Xml|*.xml";
            this.saveFileDialog1.RestoreDirectory = true;
            this.saveFileDialog1.Title = "Lưu tệp tin";
            // 
            // ucSXXK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ucSXXK";
            this.Size = new System.Drawing.Size(654, 418);
            this.Load += new System.EventHandler(this.ucSXXK_Load);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlContent.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabHQ.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLHQ)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPHQ)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDMHQ)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKHQ)).EndInit();
            this.tabDN.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLDN)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPDN)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDMDN)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKDN)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMoveLeft;
        private System.Windows.Forms.Button btnMoveRight;
        private System.Windows.Forms.CheckBox chkOver;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTitleDestination;
        private System.Windows.Forms.Label lblTitleSource;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabHQ;
        private System.Windows.Forms.TabPage tabPage5;
        private Janus.Windows.GridEX.GridEX dgListNPLHQ;
        private System.Windows.Forms.TabPage tabPage6;
        private Janus.Windows.GridEX.GridEX dgListSPHQ;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private Janus.Windows.GridEX.GridEX dgListDMHQ;
        private Janus.Windows.GridEX.GridEX dgListTKHQ;
        private System.Windows.Forms.TabControl tabDN;
        private System.Windows.Forms.TabPage tabPage1;
        private Janus.Windows.GridEX.GridEX dgListNPLDN;
        private System.Windows.Forms.TabPage tabPage2;
        private Janus.Windows.GridEX.GridEX dgListSPDN;
        private System.Windows.Forms.TabPage tabPage3;
        private Janus.Windows.GridEX.GridEX dgListDMDN;
        private System.Windows.Forms.TabPage tabPage4;
        private Janus.Windows.GridEX.GridEX dgListTKDN;
        private System.Windows.Forms.Button btnExportXML;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;

    }
}
