﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace ECS.AutoUpdate
{
    public class RemoteFile
    {
        #region The private fields
        private string path = "";
        private string url = "";
        private string lastver = "";
        private int size = 0;
        private bool needRestart = false;
		private bool isDatabase;
        #endregion

        #region The public property
        public string Path { get { return path; } }
        public string Url { get { return url; } }
        public string LastVer { get { return lastver; } }
        public int Size { get { return size; } }
        public bool NeedRestart { get { return needRestart; } }
		public bool IsDatabase { get { return isDatabase; } }
        public int FullSize;
        #endregion

        #region The constructor of AutoUpdater
        public RemoteFile(XmlNode node, string defaultPath)
        {
            this.path = node.Attributes["path"].Value;
            this.url =node.Attributes["url"] != null ? node.Attributes["url"].Value : (defaultPath +"/"+ this.path);
            this.lastver =node.Attributes["lastver"]!=null? node.Attributes["lastver"].Value:"1.0";
            this.size =node.Attributes["size"]!=null? int.Parse(node.Attributes["size"].Value):1;
            this.needRestart = node.Attributes["needRestart"] != null ? Convert.ToBoolean(node.Attributes["needRestart"].Value) : false;
            this.isDatabase = node.Attributes["isDatabase"] != null ? bool.Parse(node.Attributes["isDatabase"].Value) : false;
            FullSize = node.Attributes["fullSize"] != null ? int.Parse(node.Attributes["fullSize"].Value) : 1;
        }
        public override bool Equals(object obj)
        {
            RemoteFile source = (RemoteFile)obj;
            return (string.Compare(source.Path, this.Path, StringComparison.OrdinalIgnoreCase) == 0
                    && string.Compare(source.LastVer, this.LastVer, StringComparison.OrdinalIgnoreCase) == 0
                    && source.FullSize == this.FullSize);
        }
        #endregion
    }
}
