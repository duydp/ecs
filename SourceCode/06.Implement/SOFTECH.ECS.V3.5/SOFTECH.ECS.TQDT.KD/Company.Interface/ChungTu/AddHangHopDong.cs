﻿using System;

using System.Data;
using System.Threading;
using System.Globalization;
using System.Resources;
using Company.Controls.CustomValidation;
using Company.KDT.SHARE.QuanLyChungTu;
/* LanNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
using Company.KD.BLL.KDT.SXXK;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using Company.Interface.SXXK;
using System.Collections.Generic;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using Company.Interface.SXXK;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.Interface.GC;
#endif
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class AddHangHopDong : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public ChungTuGiayKiemTra ChungTu;
        public GiayKiemTra GiayKiemTra;
        public string NhomLoaiHinh = "N";
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
        public List<HopDongThuongMaiDetail> listHopDongThuongMaiDetail;
        int FormatSoLuong;
        int DonGia;
        int TriGia;



        public AddHangHopDong()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {

            /*
            switch (this.NhomLoaiHinh[0].ToString())
            {
                case "N":
                    //if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.isBrower = true;
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    this.NPLRegistedForm.ShowDialog(this);
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "X":
                    //if (this.SPRegistedForm == null)
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.isBowrer = true;
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog(this);
                    if (this.SPRegistedForm.SanPhamSelected.Ma != "")
                    {
                        txtMaHang.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                    }
                    break;
            }
            */
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            /*
            if (NhomLoaiHinh.StartsWith("N"))
            {
                Company.KD.BLL.SXXK.HangHoaNhap npl = new Company.KD.BLL.SXXK.HangHoaNhap();
                npl.Ma = txtMaHang.Text.Trim();
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = "";
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten.Trim();
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                  //epError.SetError(txtMaHang, null);
                }

            }
            else
            {
                Company.KD.BLL.SXXK.HangHoaXuat sp = new Company.KD.BLL.SXXK.HangHoaXuat();
                sp.Ma = txtMaHang.Text.Trim();
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = "";
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma.Trim();
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                    //epError.SetError(txtMaHang, null);
                }
            }
*/
        }

        private void AddHangHopDong_Load(object sender, EventArgs e)
        {
#if KD_V4
             FormatSoLuong =    GlobalSettings.SoThapPhan.SoLuongHMD;
             DonGia =    GlobalSettings.SoThapPhan.DonGiaNT;
             TriGia =   GlobalSettings.SoThapPhan.TriGiaNT;
#elif GC_V4
            FormatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
            DonGia = GlobalSettings.DonGiaNT;
            TriGia = GlobalSettings.TriGiaNT;
#elif SXXK_V4
            FormatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
            DonGia = GlobalSettings.DonGiaNT;
            TriGia = GlobalSettings.TriGiaNT;
#endif
            cbDonViTinh.DataSource = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;

            txtLuong.FormatString = "N" + FormatSoLuong;
            txtDGNT.FormatString = "N" + DonGia;
            txtTGNT.FormatString = "N" + TriGia;
        }
        private HopDongThuongMaiDetail get()
        {

            HopDongThuongMaiDetail hangHopDong = new HopDongThuongMaiDetail();
            hangHopDong.MaPhu = txtMaHang.Text.Trim();
            hangHopDong.TenHang = txtTenHang.Text.Trim();
            hangHopDong.MaHS = txtMaHS.Text.Trim();
            hangHopDong.NuocXX_ID = ctrNuocXX.Ma;
            hangHopDong.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hangHopDong.DonGiaKB = Convert.ToDouble(txtDGNT.Value);
            hangHopDong.TriGiaKB = Convert.ToDouble(txtTGNT.Value);
            hangHopDong.DVT_ID = cbDonViTinh.SelectedValue.ToString().Trim();
            return hangHopDong;

        }
        private void resetForm()
        {
            txtMaHang.Text = string.Empty;
            txtTenHang.Text = string.Empty;

        }
        private bool save()
        {
            try
            {
                if (string.IsNullOrEmpty(txtTenHang.Text) || string.IsNullOrEmpty(txtMaHS.Text) || string.IsNullOrEmpty(ctrNuocXX.Ma))
                {
                    ShowMessage("Vui lòng nhập đầy đủ thông tin hàng trong hợp đồng", false);
                    return false;
                }
                if (Convert.ToDecimal(txtLuong.Value) <= 0 || Convert.ToDouble(txtDGNT.Value) <= 0 || Convert.ToDouble(txtTGNT.Value) <= 0)
                {
                    ShowMessage("Vui lòng nhập đầy đủ số lượng, đơn giá, trị giá hàng", false);
                    return false;
                }
                HopDongThuongMaiDetail hangHopDong = new HopDongThuongMaiDetail();
                hangHopDong.MaPhu = txtMaHang.Text.Trim();
                hangHopDong.TenHang = txtTenHang.Text.Trim();
                hangHopDong.MaHS = txtMaHS.Text.Trim();
                hangHopDong.NuocXX_ID = ctrNuocXX.Ma;
                hangHopDong.SoLuong = Convert.ToDecimal(txtLuong.Value);
                hangHopDong.DonGiaKB = Convert.ToDouble(txtDGNT.Value);
                hangHopDong.TriGiaKB = Convert.ToDouble(txtTGNT.Value);
                hangHopDong.DVT_ID = cbDonViTinh.SelectedValue.ToString().Trim();
                listHopDongThuongMaiDetail.Add(hangHopDong);
                return true;

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi cập nhật dữ liệu : " + ex.Message, false);
                return false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (save())
                this.Close();
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (save())
                resetForm();
        }





    }
}