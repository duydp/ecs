namespace Company.Interface.SXXK.BangKe
{
    partial class BK_HangTonKho_DNCXForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList2_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK_HangTonKho_DNCXForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList2 = new Janus.Windows.GridEX.GridEX();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdluu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdluu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdNhanPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanPhanHoi");
            this.cmdKetQua1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQua");
            this.cmdHuyHangTon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyHangTon");
            this.cmdThemMoi = new Janus.Windows.UI.CommandBars.UICommand("cmdThemMoi");
            this.cmdluu = new Janus.Windows.UI.CommandBars.UICommand("cmdluu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdNhanPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanPhanHoi");
            this.cmdKetQua = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQua");
            this.cmdHuyHangTon = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyHangTon");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.clcNgayTiepNhan = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.txtNam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtQuy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiButton1);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(836, 420);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(748, 392);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgList2);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 91);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(836, 295);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.Text = "Danh sách hàng tồn kho";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgList2
            // 
            this.dgList2.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList2.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList2.AlternatingColors = true;
            this.dgList2.AutomaticSort = false;
            this.dgList2.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList2.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList2.ColumnAutoResize = true;
            dgList2_DesignTimeLayout.LayoutString = resources.GetString("dgList2_DesignTimeLayout.LayoutString");
            this.dgList2.DesignTimeLayout = dgList2_DesignTimeLayout;
            this.dgList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList2.GroupByBoxVisible = false;
            this.dgList2.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList2.ImageList = this.ImageList1;
            this.dgList2.Location = new System.Drawing.Point(3, 17);
            this.dgList2.Name = "dgList2";
            this.dgList2.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList2.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList2.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList2.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList2.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList2.Size = new System.Drawing.Size(830, 275);
            this.dgList2.TabIndex = 0;
            this.dgList2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList2.VisualStyleManager = this.vsmMain;
            this.dgList2.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList2_RowDoubleClick);
            // 
            // uiButton1
            // 
            this.uiButton1.Enabled = false;
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(675, 465);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(71, 23);
            this.uiButton1.TabIndex = 15;
            this.uiButton1.Text = "Xóa";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemMoi,
            this.cmdluu,
            this.cmdKhaiBao,
            this.cmdNhanPhanHoi,
            this.cmdKetQua,
            this.cmdHuyHangTon});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("138fb5bc-fa3e-4e41-9ef8-f7ff82bd7094");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 448);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(836, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdluu1,
            this.cmdKhaiBao1,
            this.cmdNhanPhanHoi1,
            this.cmdKetQua1,
            this.cmdHuyHangTon1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(488, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdluu1
            // 
            this.cmdluu1.Key = "cmdluu";
            this.cmdluu1.Name = "cmdluu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdNhanPhanHoi1
            // 
            this.cmdNhanPhanHoi1.Key = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi1.Name = "cmdNhanPhanHoi1";
            // 
            // cmdKetQua1
            // 
            this.cmdKetQua1.Key = "cmdKetQua";
            this.cmdKetQua1.Name = "cmdKetQua1";
            // 
            // cmdHuyHangTon1
            // 
            this.cmdHuyHangTon1.Key = "cmdHuyHangTon";
            this.cmdHuyHangTon1.Name = "cmdHuyHangTon1";
            // 
            // cmdThemMoi
            // 
            this.cmdThemMoi.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemMoi.Image")));
            this.cmdThemMoi.Key = "cmdThemMoi";
            this.cmdThemMoi.Name = "cmdThemMoi";
            this.cmdThemMoi.Text = "Thêm";
            // 
            // cmdluu
            // 
            this.cmdluu.Image = ((System.Drawing.Image)(resources.GetObject("cmdluu.Image")));
            this.cmdluu.Key = "cmdluu";
            this.cmdluu.Name = "cmdluu";
            this.cmdluu.Text = "Lưu bảng kê";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdNhanPhanHoi
            // 
            this.cmdNhanPhanHoi.Image = ((System.Drawing.Image)(resources.GetObject("cmdNhanPhanHoi.Image")));
            this.cmdNhanPhanHoi.Key = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi.Name = "cmdNhanPhanHoi";
            this.cmdNhanPhanHoi.Text = "Nhận phản hồi";
            // 
            // cmdKetQua
            // 
            this.cmdKetQua.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQua.Image")));
            this.cmdKetQua.Key = "cmdKetQua";
            this.cmdKetQua.Name = "cmdKetQua";
            this.cmdKetQua.Text = "Kết quả xử lý";
            // 
            // cmdHuyHangTon
            // 
            this.cmdHuyHangTon.Image = ((System.Drawing.Image)(resources.GetObject("cmdHuyHangTon.Image")));
            this.cmdHuyHangTon.Key = "cmdHuyHangTon";
            this.cmdHuyHangTon.Name = "cmdHuyHangTon";
            this.cmdHuyHangTon.Text = "Khai báo huỷ";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 420);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(836, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 420);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(836, 28);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.clcNgayTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtNam);
            this.uiGroupBox1.Controls.Add(this.txtQuy);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(836, 85);
            this.uiGroupBox1.TabIndex = 16;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(557, 24);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThai.TabIndex = 2;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(493, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Trạng thái :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(229, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ngày thiếp nhận";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(229, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Quý quyết toán";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Số tiếp nhận ";
            // 
            // clcNgayTiepNhan
            // 
            this.clcNgayTiepNhan.Location = new System.Drawing.Point(328, 20);
            this.clcNgayTiepNhan.Name = "clcNgayTiepNhan";
            this.clcNgayTiepNhan.ReadOnly = false;
            this.clcNgayTiepNhan.Size = new System.Drawing.Size(130, 21);
            this.clcNgayTiepNhan.TabIndex = 1;
            this.clcNgayTiepNhan.TagName = "";
            this.clcNgayTiepNhan.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayTiepNhan.WhereCondition = "";
            // 
            // txtNam
            // 
            this.txtNam.Location = new System.Drawing.Point(107, 47);
            this.txtNam.Name = "txtNam";
            this.txtNam.Size = new System.Drawing.Size(100, 21);
            this.txtNam.TabIndex = 0;
            this.txtNam.VisualStyleManager = this.vsmMain;
            // 
            // txtQuy
            // 
            this.txtQuy.Location = new System.Drawing.Point(328, 47);
            this.txtQuy.Name = "txtQuy";
            this.txtQuy.Size = new System.Drawing.Size(39, 21);
            this.txtQuy.TabIndex = 0;
            this.txtQuy.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Location = new System.Drawing.Point(107, 20);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(100, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Năm quyết toán";
            // 
            // BK_HangTonKho_DNCXForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(836, 448);
            this.Controls.Add(this.LeftRebar1);
            this.Controls.Add(this.RightRebar1);
            this.Controls.Add(this.TopRebar1);
            this.Controls.Add(this.BottomRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK_HangTonKho_DNCXForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Bảng kê hàng tồn kho";
            this.Load += new System.EventHandler(this.BKNPLCungUngForm_Load);
            this.Controls.SetChildIndex(this.BottomRebar1, 0);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.RightRebar1, 0);
            this.Controls.SetChildIndex(this.LeftRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgList2;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemMoi;
        private Janus.Windows.UI.CommandBars.UICommand cmdluu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanPhanHoi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanPhanHoi;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQua1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQua;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtNam;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuy;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.UI.CommandBars.UICommand cmdluu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyHangTon;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyHangTon1;
    }
}