﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportNPL_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        private int STT = 0;
        public ReportNPL_TT196()
        {
            InitializeComponent();
        }
        public void BindReportDinhMucDaDangKy()
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;

            //TOSO: Load NPL Bo sung
            if (this.NPLCollection.Count != 0)
            {
                System.Collections.Generic.List<Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung> nplBSListKDT = null;
                foreach (NguyenPhuLieu npl in this.NPLCollection)
                {
                    nplBSListKDT = (System.Collections.Generic.List<Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung>)Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung.SelectCollectionDynamicNguyenPhuLieuBoSung(npl.MaHaiQuan.Trim() != "" ? npl.MaHaiQuan.Trim() : GlobalSettings.MA_HAI_QUAN, npl.MaDoanhNghiep != "" ? npl.MaDoanhNghiep : GlobalSettings.MA_DON_VI, npl.Ma);

                    npl.NPLChinh = nplBSListKDT != null && nplBSListKDT.Count > 0 ? nplBSListKDT[0].NPLChinh : false;
                }
            }

            this.DataSource = this.NPLCollection;
            lblMaSP.DataBindings.Add("Text", this.DataSource, "Ma");
            lblTenSP.DataBindings.Add("Text", this.DataSource, "Ten");
            lblMaHS.DataBindings.Add("Text", this.DataSource, "MaHS");
            lblNgayHQ.Text = Properties.Settings.Default.TieudeNgay;
            lblNgayDN.Text = GlobalSettings.TieudeNgay;
        }
        public void BindHD(string HD)
        {
            lblHopDong.Text = HD;
        }
        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDVT.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_ID"));
        }

        private void ReportNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;

        }

        private void xrTableCell7_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if ((bool)GetCurrentColumnValue("NPLChinh")) lblNPLChinh.Text = "X";
            else lblNPLChinh.Text = "";
        }

    }
}
