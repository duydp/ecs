﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.GC.BLL.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;

namespace Company.Interface.KDT.GC
{
    public partial class SanPhamGCEditForm : BaseForm
    {
        public SanPham SPDetail;
        public HopDong HD = new HopDong();
        public bool isBrower = false;
        public SanPhamGCEditForm()
        {
            InitializeComponent();
            SPDetail = new SanPham();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.SPDetail = null;
            this.Close();
        }
        private void reset()
        {
            txtMa.Text = "";
            txtTen.Text = "";
            txtMaHS.Text = "";
            txtSoLuong.Text = "0";
            txtDonGia.Text = "0";
            error.SetError(txtMaHS, null);
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }

            SPDetail = new SanPham();
        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            error.SetError(txtMaHS, null);

        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            // Kiểm tra tính hợp lệ của mã HS.
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //    error.SetIconPadding(txtMaHS, -8);
            //    error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
            //    txtMaHS.Focus();
            //    return;
            //}


            checkExitsSanPhamAndSTTHang();
        }
        private void LoadLoaiSanPham()
        {
            //DataTable loaiSp = Company.GC.BLL.KDT.GC.NhomSanPham.SelectBy_HopDong_ID(HD.ID).Tables[0];
            //cbNhomSP.DataSource = loaiSp;
            //cbNhomSP.DisplayMember = "TenSanPham";
            //cbNhomSP.ValueMember = "ID";
            //if (loaiSp.Rows.Count > 0)
            //    cbNhomSP.SelectedIndex = 0;
        }
        private void SanPhamGCEditForm_Load(object sender, EventArgs e)
        {
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            txtDonGia.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;

            if (!isBrower)
            {
                txtMa.Focus();
                this._DonViTinh = DonViTinh.SelectAll();
                cbDonViTinh.DataSource = this._DonViTinh;
                cbDonViTinh.DisplayMember = "Ten";
                cbDonViTinh.ValueMember = "ID";
                cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
                cbNhomSP.DataSource = HD.NhomSPCollection;
                cbNhomSP.DisplayMember = "TenSanPham";
                cbNhomSP.ValueMember = "MaSanPham";
                txtMa.Text = SPDetail.Ma.Trim();
                txtTen.Text = SPDetail.Ten.Trim();
                txtMaHS.Text = SPDetail.MaHS.Trim();
                txtSoLuong.Text = SPDetail.SoLuongDangKy.ToString();
                txtDonGia.Text = SPDetail.DonGia.ToString();
                if (SPDetail.Ma != "")
                {
                    cbDonViTinh.SelectedValue = SPDetail.DVT_ID;
                    cbNhomSP.SelectedValue = SPDetail.NhomSanPham_ID.ToString();
                }
                if (this.OpenType == OpenFormType.Edit)
                {
                    ;
                }
                else
                {
                    btnAdd.Enabled = false;
                    btnXoa.Enabled = false;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                }
                //if (HD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET )
                //{
                //    btnAdd.Enabled = false;
                //    btnXoa.Enabled = false;
                //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                //}
                //if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                //{
                //    btnAdd.Enabled = false;
                //    btnXoa.Enabled = false;
                //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                //}           
            }
            else
            {
                uiGroupBox2.Visible = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                this.Width = dgList.Width;
                this.Height = dgList.Height;
                btnAdd.Visible = false;
                btnClose.Visible = false;
                HD.LoadCollection();
            }
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            dgList.DataSource = HD.SPCollection;

        }
        private void checkExitsSanPhamAndSTTHang()
        {
            HD.SPCollection.Remove(SPDetail);
            foreach (SanPham sp in HD.SPCollection)
            {
                if (sp.Ma.Trim().ToUpper() == txtMa.Text.Trim().ToUpper())
                {
                    showMsg("MSG_2702055");
                    //string st = MLMessages("Đã có sản phẩm này trong danh sách.","MSG_WRN06","", false);
                    if (SPDetail.Ma.Trim() != "")
                        HD.SPCollection.Add(SPDetail);
                    txtMa.Focus();
                    return;
                }

            }
            SPDetail.SoLuongDangKy = Convert.ToDecimal(txtSoLuong.Text);
            SPDetail.MaHS = txtMaHS.Text.Trim();
            SPDetail.Ten = txtTen.Text.Trim();
            SPDetail.NhomSanPham_ID = cbNhomSP.SelectedValue.ToString();
            SPDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            SPDetail.Ma = txtMa.Text.Trim();
            SPDetail.HopDong_ID = HD.ID;
            SPDetail.DonGia = Convert.ToDecimal(txtDonGia.Text);
            HD.SPCollection.Add(SPDetail);
            reset();
            this.setErro();

        }
        private void txtMaHS_Leave(object sender, EventArgs e)
        {

        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                SPDetail = (SanPham)e.Row.DataRow;
                if (!isBrower)
                {
                    txtMa.Text = SPDetail.Ma.Trim();
                    txtTen.Text = SPDetail.Ten.Trim();
                    txtMaHS.Text = SPDetail.MaHS.Trim();
                    txtSoLuong.Text = SPDetail.SoLuongDangKy.ToString().Trim();
                    txtDonGia.Text = SPDetail.DonGia.ToString().Trim();
                    cbDonViTinh.SelectedValue = SPDetail.DVT_ID;
                    cbNhomSP.SelectedValue = SPDetail.NhomSanPham_ID;
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenNhomSanPham"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value);
                e.Row.Cells["NhomSanPham_ID"].Text = e.Row.Cells["TenNhomSanPham"].Text;
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    SanPham sp1 = (SanPham)row.GetRow().DataRow;
                    if (SanPham.CheckExitSPinDinhMuc(sp1.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, HD.ID))
                    {
                        if (showMsg("MSG_0203076", sp1.Ma, true) != "Yes")
                        {
                            e.Cancel = true;
                            return;
                        }
                        else
                            continue;
                    }
                    sp1.Delete();
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            SanPhamCollection spColl = new SanPhamCollection();
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    SanPham sp1 = (SanPham)row.GetRow().DataRow;
                    if (SanPham.CheckExitSPinDinhMuc(sp1.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, HD.ID))
                    {
                        if (showMsg("MSG_0203076", sp1.Ma, true) != "Yes")
                        {
                            return;
                        }
                        else
                            continue;
                    }
                    try
                    {
                        sp1.Delete();
                    }
                    catch { }
                    spColl.Add(sp1);
                }
                foreach (SanPham spp in spColl)
                {
                    HD.SPCollection.Remove(spp);
                }
                dgList.DataSource = HD.SPCollection;
                try
                {
                    dgList.Refetch();
                }
                catch { dgList.Refresh(); }
            }
        }


    }
}