﻿/*
Run this script on:

        VDM-WTS09\SQLSERVER.ECS_TQDT_GC_V4_19_09_2015_[09_12]_THIANCO    -  This database will be modified

to synchronize it with:

        113.160.225.156,2461.ECS_TQDT_GC_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 01/12/2016 7:53:23 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] DROP
CONSTRAINT [FK_t_HangMauDich_t_ToKhaiMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] DROP CONSTRAINT [IX_t_KDT_HangMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[t_KDT_VNACC_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich] DROP CONSTRAINT [DF__t_KDT_VNA__HopDo__259319E7]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_PhieuNhapKho]'
GO
CREATE TABLE [dbo].[t_KDT_GC_PhieuNhapKho]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[NgayNhapKho] [datetime] NULL,
[SoPhieu] [numeric] (18, 0) NULL,
[SoHopDong] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KhachHang] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NhapTaiKho] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgoaiTe] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TyGia] [numeric] (18, 4) NULL,
[LyDo] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NhaCungCap] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToKhai] [numeric] (12, 0) NULL,
[SoInvoice] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayInvoice] [datetime] NULL,
[SoBL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoKien] [numeric] (18, 0) NULL,
[TongVAT] [numeric] (26, 4) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__t_KDT_GC__3214EC2768D35913] on [dbo].[t_KDT_GC_PhieuNhapKho]'
GO
ALTER TABLE [dbo].[t_KDT_GC_PhieuNhapKho] ADD CONSTRAINT [PK__t_KDT_GC__3214EC2768D35913] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayNhapKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[NhapTaiKho],
	[NgoaiTe],
	[TyGia],
	[LyDo],
	[NhaCungCap],
	[ToKhai],
	[SoInvoice],
	[NgayInvoice],
	[SoBL],
	[SoKien],
	[TongVAT]
FROM
	[dbo].[t_KDT_GC_PhieuNhapKho]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayNhapKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[NhapTaiKho],
	[NgoaiTe],
	[TyGia],
	[LyDo],
	[NhaCungCap],
	[ToKhai],
	[SoInvoice],
	[NgayInvoice],
	[SoBL],
	[SoKien],
	[TongVAT]
FROM
	[dbo].[t_KDT_GC_PhieuNhapKho]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_PhieuNhapKho_Hang]'
GO
CREATE TABLE [dbo].[t_KDT_GC_PhieuNhapKho_Hang]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[MaSoHang] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ma2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mau] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Size] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVTLuong1] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong1] [numeric] (12, 4) NULL,
[SoLuongThucNhap] [numeric] (12, 4) NULL,
[DonGiaNgoaiTe] [numeric] (24, 6) NULL,
[DonGiaHoaDon] [numeric] (24, 6) NULL,
[ThanhTienNgoaiTe] [numeric] (24, 4) NULL,
[TriGiaHoaDon] [numeric] (24, 4) NULL,
[GhiChu] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaTTDonGia] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__t_KDT_GC__3214EC276D2DF9DC] on [dbo].[t_KDT_GC_PhieuNhapKho_Hang]'
GO
ALTER TABLE [dbo].[t_KDT_GC_PhieuNhapKho_Hang] ADD CONSTRAINT [PK__t_KDT_GC__3214EC276D2DF9DC] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Insert]
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@Ma2 nvarchar(30),
	@TenHang nvarchar(200),
	@Mau nvarchar(30),
	@Size nvarchar(30),
	@DVTLuong1 varchar(4),
	@SoLuong1 numeric(12, 4),
	@SoLuongThucNhap numeric(12, 4),
	@DonGiaNgoaiTe numeric(24, 6),
	@DonGiaHoaDon numeric(24, 6),
	@ThanhTienNgoaiTe numeric(24, 4),
	@TriGiaHoaDon numeric(24, 4),
	@GhiChu nvarchar(200),
	@MaTTDonGia varchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhieuNhapKho_Hang]
(
	[TKMD_ID],
	[MaSoHang],
	[Ma2],
	[TenHang],
	[Mau],
	[Size],
	[DVTLuong1],
	[SoLuong1],
	[SoLuongThucNhap],
	[DonGiaNgoaiTe],
	[DonGiaHoaDon],
	[ThanhTienNgoaiTe],
	[TriGiaHoaDon],
	[GhiChu],
	[MaTTDonGia]
)
VALUES 
(
	@TKMD_ID,
	@MaSoHang,
	@Ma2,
	@TenHang,
	@Mau,
	@Size,
	@DVTLuong1,
	@SoLuong1,
	@SoLuongThucNhap,
	@DonGiaNgoaiTe,
	@DonGiaHoaDon,
	@ThanhTienNgoaiTe,
	@TriGiaHoaDon,
	@GhiChu,
	@MaTTDonGia
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@Ma2 nvarchar(30),
	@TenHang nvarchar(200),
	@Mau nvarchar(30),
	@Size nvarchar(30),
	@DVTLuong1 varchar(4),
	@SoLuong1 numeric(12, 4),
	@SoLuongThucNhap numeric(12, 4),
	@DonGiaNgoaiTe numeric(24, 6),
	@DonGiaHoaDon numeric(24, 6),
	@ThanhTienNgoaiTe numeric(24, 4),
	@TriGiaHoaDon numeric(24, 4),
	@GhiChu nvarchar(200),
	@MaTTDonGia varchar(4)
AS

UPDATE
	[dbo].[t_KDT_GC_PhieuNhapKho_Hang]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaSoHang] = @MaSoHang,
	[Ma2] = @Ma2,
	[TenHang] = @TenHang,
	[Mau] = @Mau,
	[Size] = @Size,
	[DVTLuong1] = @DVTLuong1,
	[SoLuong1] = @SoLuong1,
	[SoLuongThucNhap] = @SoLuongThucNhap,
	[DonGiaNgoaiTe] = @DonGiaNgoaiTe,
	[DonGiaHoaDon] = @DonGiaHoaDon,
	[ThanhTienNgoaiTe] = @ThanhTienNgoaiTe,
	[TriGiaHoaDon] = @TriGiaHoaDon,
	[GhiChu] = @GhiChu,
	[MaTTDonGia] = @MaTTDonGia
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Hang_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@Ma2 nvarchar(30),
	@TenHang nvarchar(200),
	@Mau nvarchar(30),
	@Size nvarchar(30),
	@DVTLuong1 varchar(4),
	@SoLuong1 numeric(12, 4),
	@SoLuongThucNhap numeric(12, 4),
	@DonGiaNgoaiTe numeric(24, 6),
	@DonGiaHoaDon numeric(24, 6),
	@ThanhTienNgoaiTe numeric(24, 4),
	@TriGiaHoaDon numeric(24, 4),
	@GhiChu nvarchar(200),
	@MaTTDonGia varchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhieuNhapKho_Hang] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhieuNhapKho_Hang] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaSoHang] = @MaSoHang,
			[Ma2] = @Ma2,
			[TenHang] = @TenHang,
			[Mau] = @Mau,
			[Size] = @Size,
			[DVTLuong1] = @DVTLuong1,
			[SoLuong1] = @SoLuong1,
			[SoLuongThucNhap] = @SoLuongThucNhap,
			[DonGiaNgoaiTe] = @DonGiaNgoaiTe,
			[DonGiaHoaDon] = @DonGiaHoaDon,
			[ThanhTienNgoaiTe] = @ThanhTienNgoaiTe,
			[TriGiaHoaDon] = @TriGiaHoaDon,
			[GhiChu] = @GhiChu,
			[MaTTDonGia] = @MaTTDonGia
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhieuNhapKho_Hang]
		(
			[TKMD_ID],
			[MaSoHang],
			[Ma2],
			[TenHang],
			[Mau],
			[Size],
			[DVTLuong1],
			[SoLuong1],
			[SoLuongThucNhap],
			[DonGiaNgoaiTe],
			[DonGiaHoaDon],
			[ThanhTienNgoaiTe],
			[TriGiaHoaDon],
			[GhiChu],
			[MaTTDonGia]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaSoHang,
			@Ma2,
			@TenHang,
			@Mau,
			@Size,
			@DVTLuong1,
			@SoLuong1,
			@SoLuongThucNhap,
			@DonGiaNgoaiTe,
			@DonGiaHoaDon,
			@ThanhTienNgoaiTe,
			@TriGiaHoaDon,
			@GhiChu,
			@MaTTDonGia
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhieuNhapKho_Hang]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[Ma2],
	[TenHang],
	[Mau],
	[Size],
	[DVTLuong1],
	[SoLuong1],
	[SoLuongThucNhap],
	[DonGiaNgoaiTe],
	[DonGiaHoaDon],
	[ThanhTienNgoaiTe],
	[TriGiaHoaDon],
	[GhiChu],
	[MaTTDonGia]
FROM
	[dbo].[t_KDT_GC_PhieuNhapKho_Hang]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[Ma2],
	[TenHang],
	[Mau],
	[Size],
	[DVTLuong1],
	[SoLuong1],
	[SoLuongThucNhap],
	[DonGiaNgoaiTe],
	[DonGiaHoaDon],
	[ThanhTienNgoaiTe],
	[TriGiaHoaDon],
	[GhiChu],
	[MaTTDonGia]
FROM
	[dbo].[t_KDT_GC_PhieuNhapKho_Hang]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_PhieuXuatKho]'
GO
CREATE TABLE [dbo].[t_KDT_GC_PhieuXuatKho]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[NgayXuatKho] [datetime] NULL,
[SoPhieu] [numeric] (18, 0) NULL,
[SoHopDong] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KhachHang] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Style] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PO] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DonViNhan] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XuatTaiKho] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong] [numeric] (18, 2) NULL,
[DVTSoLuong] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__t_KDT_GC__3214EC2770747ADB] on [dbo].[t_KDT_GC_PhieuXuatKho]'
GO
ALTER TABLE [dbo].[t_KDT_GC_PhieuXuatKho] ADD CONSTRAINT [PK__t_KDT_GC__3214EC2770747ADB] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Insert]
	@TKMD_ID bigint,
	@NgayXuatKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@Style nvarchar(50),
	@PO nvarchar(50),
	@DonViNhan nvarchar(200),
	@XuatTaiKho nvarchar(200),
	@SoLuong numeric(18, 2),
	@DVTSoLuong varchar(15),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhieuXuatKho]
(
	[TKMD_ID],
	[NgayXuatKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[Style],
	[PO],
	[DonViNhan],
	[XuatTaiKho],
	[SoLuong],
	[DVTSoLuong]
)
VALUES 
(
	@TKMD_ID,
	@NgayXuatKho,
	@SoPhieu,
	@SoHopDong,
	@KhachHang,
	@Style,
	@PO,
	@DonViNhan,
	@XuatTaiKho,
	@SoLuong,
	@DVTSoLuong
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayXuatKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@Style nvarchar(50),
	@PO nvarchar(50),
	@DonViNhan nvarchar(200),
	@XuatTaiKho nvarchar(200),
	@SoLuong numeric(18, 2),
	@DVTSoLuong varchar(15)
AS

UPDATE
	[dbo].[t_KDT_GC_PhieuXuatKho]
SET
	[TKMD_ID] = @TKMD_ID,
	[NgayXuatKho] = @NgayXuatKho,
	[SoPhieu] = @SoPhieu,
	[SoHopDong] = @SoHopDong,
	[KhachHang] = @KhachHang,
	[Style] = @Style,
	[PO] = @PO,
	[DonViNhan] = @DonViNhan,
	[XuatTaiKho] = @XuatTaiKho,
	[SoLuong] = @SoLuong,
	[DVTSoLuong] = @DVTSoLuong
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayXuatKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@Style nvarchar(50),
	@PO nvarchar(50),
	@DonViNhan nvarchar(200),
	@XuatTaiKho nvarchar(200),
	@SoLuong numeric(18, 2),
	@DVTSoLuong varchar(15)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhieuXuatKho] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhieuXuatKho] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[NgayXuatKho] = @NgayXuatKho,
			[SoPhieu] = @SoPhieu,
			[SoHopDong] = @SoHopDong,
			[KhachHang] = @KhachHang,
			[Style] = @Style,
			[PO] = @PO,
			[DonViNhan] = @DonViNhan,
			[XuatTaiKho] = @XuatTaiKho,
			[SoLuong] = @SoLuong,
			[DVTSoLuong] = @DVTSoLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhieuXuatKho]
		(
			[TKMD_ID],
			[NgayXuatKho],
			[SoPhieu],
			[SoHopDong],
			[KhachHang],
			[Style],
			[PO],
			[DonViNhan],
			[XuatTaiKho],
			[SoLuong],
			[DVTSoLuong]
		)
		VALUES 
		(
			@TKMD_ID,
			@NgayXuatKho,
			@SoPhieu,
			@SoHopDong,
			@KhachHang,
			@Style,
			@PO,
			@DonViNhan,
			@XuatTaiKho,
			@SoLuong,
			@DVTSoLuong
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhieuXuatKho]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayXuatKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[Style],
	[PO],
	[DonViNhan],
	[XuatTaiKho],
	[SoLuong],
	[DVTSoLuong]
FROM
	[dbo].[t_KDT_GC_PhieuXuatKho]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayXuatKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[Style],
	[PO],
	[DonViNhan],
	[XuatTaiKho],
	[SoLuong],
	[DVTSoLuong]
FROM
	[dbo].[t_KDT_GC_PhieuXuatKho]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_PhieuXuatKho_Hang]'
GO
CREATE TABLE [dbo].[t_KDT_GC_PhieuXuatKho_Hang]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[MaSoHang] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaNPL2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHang] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Art] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCC] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Size] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Color] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuong1] [numeric] (12, 4) NULL,
[DVTLuong1] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DM] [numeric] (18, 2) NULL,
[PhanTram] [numeric] (18, 2) NULL,
[Nhucau] [numeric] (24, 4) NULL,
[ThucNhan] [numeric] (24, 4) NULL,
[GhiChu] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID_HangTK] [bigint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__t_KDT_GC__3214EC27116B5A52] on [dbo].[t_KDT_GC_PhieuXuatKho_Hang]'
GO
ALTER TABLE [dbo].[t_KDT_GC_PhieuXuatKho_Hang] ADD CONSTRAINT [PK__t_KDT_GC__3214EC27116B5A52] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Insert]
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaNPL2 nvarchar(30),
	@TenHang nvarchar(200),
	@Art nvarchar(30),
	@NCC nvarchar(30),
	@Size nvarchar(30),
	@Color nvarchar(30),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@DM numeric(18, 2),
	@PhanTram numeric(18, 2),
	@Nhucau numeric(24, 4),
	@ThucNhan numeric(24, 4),
	@GhiChu nvarchar(200),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhieuXuatKho_Hang]
(
	[TKMD_ID],
	[MaSoHang],
	[MaNPL2],
	[TenHang],
	[Art],
	[NCC],
	[Size],
	[Color],
	[SoLuong1],
	[DVTLuong1],
	[DM],
	[PhanTram],
	[Nhucau],
	[ThucNhan],
	[GhiChu]
)
VALUES 
(
	@TKMD_ID,
	@MaSoHang,
	@MaNPL2,
	@TenHang,
	@Art,
	@NCC,
	@Size,
	@Color,
	@SoLuong1,
	@DVTLuong1,
	@DM,
	@PhanTram,
	@Nhucau,
	@ThucNhan,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaNPL2 nvarchar(30),
	@TenHang nvarchar(200),
	@Art nvarchar(30),
	@NCC nvarchar(30),
	@Size nvarchar(30),
	@Color nvarchar(30),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@DM numeric(18, 2),
	@PhanTram numeric(18, 2),
	@Nhucau numeric(24, 4),
	@ThucNhan numeric(24, 4),
	@GhiChu nvarchar(200)
AS

UPDATE
	[dbo].[t_KDT_GC_PhieuXuatKho_Hang]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaSoHang] = @MaSoHang,
	[MaNPL2] = @MaNPL2,
	[TenHang] = @TenHang,
	[Art] = @Art,
	[NCC] = @NCC,
	[Size] = @Size,
	[Color] = @Color,
	[SoLuong1] = @SoLuong1,
	[DVTLuong1] = @DVTLuong1,
	[DM] = @DM,
	[PhanTram] = @PhanTram,
	[Nhucau] = @Nhucau,
	[ThucNhan] = @ThucNhan,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Hang_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaNPL2 nvarchar(30),
	@TenHang nvarchar(200),
	@Art nvarchar(30),
	@NCC nvarchar(30),
	@Size nvarchar(30),
	@Color nvarchar(30),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@DM numeric(18, 2),
	@PhanTram numeric(18, 2),
	@Nhucau numeric(24, 4),
	@ThucNhan numeric(24, 4),
	@GhiChu nvarchar(200)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhieuXuatKho_Hang] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhieuXuatKho_Hang] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaSoHang] = @MaSoHang,
			[MaNPL2] = @MaNPL2,
			[TenHang] = @TenHang,
			[Art] = @Art,
			[NCC] = @NCC,
			[Size] = @Size,
			[Color] = @Color,
			[SoLuong1] = @SoLuong1,
			[DVTLuong1] = @DVTLuong1,
			[DM] = @DM,
			[PhanTram] = @PhanTram,
			[Nhucau] = @Nhucau,
			[ThucNhan] = @ThucNhan,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhieuXuatKho_Hang]
		(
			[TKMD_ID],
			[MaSoHang],
			[MaNPL2],
			[TenHang],
			[Art],
			[NCC],
			[Size],
			[Color],
			[SoLuong1],
			[DVTLuong1],
			[DM],
			[PhanTram],
			[Nhucau],
			[ThucNhan],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaSoHang,
			@MaNPL2,
			@TenHang,
			@Art,
			@NCC,
			@Size,
			@Color,
			@SoLuong1,
			@DVTLuong1,
			@DM,
			@PhanTram,
			@Nhucau,
			@ThucNhan,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhieuXuatKho_Hang]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaNPL2],
	[TenHang],
	[Art],
	[NCC],
	[Size],
	[Color],
	[SoLuong1],
	[DVTLuong1],
	[DM],
	[PhanTram],
	[Nhucau],
	[ThucNhan],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_PhieuXuatKho_Hang]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaNPL2],
	[TenHang],
	[Art],
	[NCC],
	[Size],
	[Color],
	[SoLuong1],
	[DVTLuong1],
	[DM],
	[PhanTram],
	[Nhucau],
	[ThucNhan],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_PhieuXuatKho_Hang]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HCT_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HCT_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_NguyenPhuLieuTon]'
GO
EXEC sp_refreshview N'[dbo].[t_View_NguyenPhuLieuTon]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_DinhMuc]'
GO
EXEC sp_refreshview N'[dbo].[t_View_DinhMuc]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_View_CanDoiXuatNhap]'
GO
CREATE VIEW dbo.t_View_CanDoiXuatNhap
AS
SELECT     dbo.t_View_DinhMuc.HopDong_ID, dbo.t_View_DinhMuc.MaSanPham, dbo.t_View_DinhMuc.MaNguyenPhuLieu, dbo.t_View_DinhMuc.DinhMucChung, 
                      dbo.t_View_NguyenPhuLieuTon.LuongTon, dbo.t_GC_NguyenPhuLieu.Ten
FROM         dbo.t_View_DinhMuc INNER JOIN
                      dbo.t_View_NguyenPhuLieuTon ON dbo.t_View_DinhMuc.HopDong_ID = dbo.t_View_NguyenPhuLieuTon.HopDong_ID AND 
                      dbo.t_View_DinhMuc.MaNguyenPhuLieu = dbo.t_View_NguyenPhuLieuTon.MaNguyenPhuLieu INNER JOIN
                      dbo.t_GC_NguyenPhuLieu ON dbo.t_View_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID AND 
                      dbo.t_View_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Insert]
	@TKMD_ID bigint,
	@NgayNhapKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@NhapTaiKho nvarchar(200),
	@NgoaiTe varchar(20),
	@TyGia numeric(18, 4),
	@LyDo nvarchar(200),
	@NhaCungCap nvarchar(200),
	@ToKhai numeric(12, 0),
	@SoInvoice nvarchar(50),
	@NgayInvoice datetime,
	@SoBL nvarchar(50),
	@SoKien numeric(18, 0),
	@TongVAT numeric(26, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_PhieuNhapKho]
(
	[TKMD_ID],
	[NgayNhapKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[NhapTaiKho],
	[NgoaiTe],
	[TyGia],
	[LyDo],
	[NhaCungCap],
	[ToKhai],
	[SoInvoice],
	[NgayInvoice],
	[SoBL],
	[SoKien],
	[TongVAT]
)
VALUES 
(
	@TKMD_ID,
	@NgayNhapKho,
	@SoPhieu,
	@SoHopDong,
	@KhachHang,
	@NhapTaiKho,
	@NgoaiTe,
	@TyGia,
	@LyDo,
	@NhaCungCap,
	@ToKhai,
	@SoInvoice,
	@NgayInvoice,
	@SoBL,
	@SoKien,
	@TongVAT
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayNhapKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@NhapTaiKho nvarchar(200),
	@NgoaiTe varchar(20),
	@TyGia numeric(18, 4),
	@LyDo nvarchar(200),
	@NhaCungCap nvarchar(200),
	@ToKhai numeric(12, 0),
	@SoInvoice nvarchar(50),
	@NgayInvoice datetime,
	@SoBL nvarchar(50),
	@SoKien numeric(18, 0),
	@TongVAT numeric(26, 4)
AS

UPDATE
	[dbo].[t_KDT_GC_PhieuNhapKho]
SET
	[TKMD_ID] = @TKMD_ID,
	[NgayNhapKho] = @NgayNhapKho,
	[SoPhieu] = @SoPhieu,
	[SoHopDong] = @SoHopDong,
	[KhachHang] = @KhachHang,
	[NhapTaiKho] = @NhapTaiKho,
	[NgoaiTe] = @NgoaiTe,
	[TyGia] = @TyGia,
	[LyDo] = @LyDo,
	[NhaCungCap] = @NhaCungCap,
	[ToKhai] = @ToKhai,
	[SoInvoice] = @SoInvoice,
	[NgayInvoice] = @NgayInvoice,
	[SoBL] = @SoBL,
	[SoKien] = @SoKien,
	[TongVAT] = @TongVAT
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayNhapKho datetime,
	@SoPhieu numeric(18, 0),
	@SoHopDong nvarchar(30),
	@KhachHang nvarchar(200),
	@NhapTaiKho nvarchar(200),
	@NgoaiTe varchar(20),
	@TyGia numeric(18, 4),
	@LyDo nvarchar(200),
	@NhaCungCap nvarchar(200),
	@ToKhai numeric(12, 0),
	@SoInvoice nvarchar(50),
	@NgayInvoice datetime,
	@SoBL nvarchar(50),
	@SoKien numeric(18, 0),
	@TongVAT numeric(26, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_PhieuNhapKho] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_PhieuNhapKho] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[NgayNhapKho] = @NgayNhapKho,
			[SoPhieu] = @SoPhieu,
			[SoHopDong] = @SoHopDong,
			[KhachHang] = @KhachHang,
			[NhapTaiKho] = @NhapTaiKho,
			[NgoaiTe] = @NgoaiTe,
			[TyGia] = @TyGia,
			[LyDo] = @LyDo,
			[NhaCungCap] = @NhaCungCap,
			[ToKhai] = @ToKhai,
			[SoInvoice] = @SoInvoice,
			[NgayInvoice] = @NgayInvoice,
			[SoBL] = @SoBL,
			[SoKien] = @SoKien,
			[TongVAT] = @TongVAT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_PhieuNhapKho]
		(
			[TKMD_ID],
			[NgayNhapKho],
			[SoPhieu],
			[SoHopDong],
			[KhachHang],
			[NhapTaiKho],
			[NgoaiTe],
			[TyGia],
			[LyDo],
			[NhaCungCap],
			[ToKhai],
			[SoInvoice],
			[NgayInvoice],
			[SoBL],
			[SoKien],
			[TongVAT]
		)
		VALUES 
		(
			@TKMD_ID,
			@NgayNhapKho,
			@SoPhieu,
			@SoHopDong,
			@KhachHang,
			@NhapTaiKho,
			@NgoaiTe,
			@TyGia,
			@LyDo,
			@NhaCungCap,
			@ToKhai,
			@SoInvoice,
			@NgayInvoice,
			@SoBL,
			@SoKien,
			@TongVAT
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_PhieuNhapKho]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_VNACC_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich] ALTER COLUMN [HopDong_ID] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC01HSTK_GC_TT38]'
GO

     
create PROCEDURE [dbo].[p_GC_BC01HSTK_GC_TT38]       
 -- Add the parameters for the stored procedure here      
    @IDHopDong BIGINT ,
    @MaHaiQuan CHAR(6) ,
    @MaDoanhNghiep VARCHAR(14),
    @TuNgay DATETIME,
    @DenNgay DATETIME
AS 
    BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
        SET NOCOUNT ON ;    
        SELECT  ROW_NUMBER() OVER ( ORDER BY ToKhai.MaNguyenPhuLieu ) AS 'STT' ,
                ToKhai.IDHopDong AS ID ,
                ToKhai.MaDoanhNghiep ,
                ToKhai.MaHaiQuan ,
                ToKhai.SoToKhai ,
                ToKhai.MaLoaiHinh ,
                ToKhai.NgayDangKy ,
                ToKhai.TenHang AS TenNguyenPhuLieu ,
                ToKhai.MaNguyenPhuLieu ,
                ToKhai.SoLuong ,
                t_HaiQuan_DonViTinh.Ten AS DVT ,
                ToKhai.SoLuong AS TongLuong
        FROM    ( SELECT    CONVERT(VARCHAR(20), ( CASE WHEN t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%V%'
                                                        THEN ( SELECT TOP 1
                                                              SoTKVNACCS
                                                              FROM
                                                              t_VNACCS_CapSoToKhai
                                                              WHERE
                                                              SoTK = t_KDT_ToKhaiMauDich.SoToKhai
                                                             )
                                                        ELSE t_KDT_ToKhaiMauDich.SoToKhai
                                                   END )) AS SoToKhai ,
                            t_KDT_ToKhaiMauDich.MaLoaiHinh ,
                            t_KDT_ToKhaiMauDich.NgayDangKy ,
                            t_KDT_ToKhaiMauDich.IDHopDong ,
                            t_KDT_ToKhaiMauDich.MaHaiQuan ,
                            t_KDT_HangMauDich.MaPhu AS MaNguyenPhuLieu ,
                            t_KDT_HangMauDich.SoLuong ,
                            t_KDT_HangMauDich.TenHang ,
                            t_KDT_HangMauDich.DVT_ID ,
                            t_KDT_ToKhaiMauDich.MaDoanhNghiep
                  FROM      t_KDT_ToKhaiMauDich
                            INNER JOIN t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                            INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_KDT_ToKhaiMauDich.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
                  WHERE     ( t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 )
                            AND ( t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong )
                            AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'N%' )
                            AND ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'N' )
                  UNION
                  SELECT    CONVERT(VARCHAR(20), ( CASE WHEN t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%V%'
                                                        THEN ( SELECT TOP 1
                                                              SoTKVNACCS
                                                              FROM
                                                              t_VNACCS_CapSoToKhai
                                                              WHERE
                                                              SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai
                                                             )
                                                        ELSE t_KDT_GC_ToKhaiChuyenTiep.SoToKhai
                                                   END )) AS SoToKhai ,
                            t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,
                            t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                            t_KDT_GC_ToKhaiChuyenTiep.IDHopDong ,
                            t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan ,
                            t_KDT_GC_HangChuyenTiep.MaHang AS MaNguyenPhuLieu ,
                            t_KDT_GC_HangChuyenTiep.SoLuong ,
                            t_KDT_GC_HangChuyenTiep.TenHang ,
                            t_KDT_GC_HangChuyenTiep.ID_DVT ,
                            t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                  FROM      t_KDT_GC_ToKhaiChuyenTiep
                            INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                  WHERE     ( t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 )
                            AND ( t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong )
                            AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh IN (
                                  'PHPLN', 'PHSPN', 'NGC18', 'NGC19', 'PHTBN',
                                  'NVE23' ) )
                            AND ( t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'N' )
                ) AS ToKhai
                INNER JOIN t_HaiQuan_DonViTinh ON ToKhai.DVT_ID = t_HaiQuan_DonViTinh.ID
        WHERE   ( ToKhai.IDHopDong = @IDHopDong ) AND (ToKhai.MaDoanhNghiep = @MaDoanhNghiep) AND (ToKhai.MaHaiQuan = @MaHaiQuan) AND ToKhai.NgayDangKy >= @TuNgay AND ToKhai.NgayDangKy <= @DenNgay
ORDER BY MaNguyenPhuLieu,ToKhai.NgayDangKy
    END
    
    SELECT * FROM dbo.t_KDT_ToKhaiMauDich
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_ALL_BC07HSTK_GC_TT38]'
GO
-- =============================================
-- Author:		minhnd
-- Create date: 16/05/2015
-- Description:	All Máy móc, Thiết bị
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_ALL_BC07HSTK_GC_TT38] 
	-- Add the parameters for the stored procedure here
	--@HD_ID BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT  ROW_NUMBER() OVER ( ORDER BY ThietBi.SoHopDong ) AS 'STT' ,
        ThietBi.SoHopDong ,
        --ThietBi.NgayKy ,
        --ThietBi.NgayHetHan ,
        --ThietBi.SoToKhai ,
        --ThietBi.NgayDangKy ,
        --MAX(ThietBi.MaLoaiHinh) as MaLoaiHinh ,
        ThietBi.MaHang,
        MAX(ThietBi.TenHang) as TenHang ,
        MAX(t_HaiQuan_DonViTinh.Ten) AS DVT ,
        SUM(ThietBi.SoLuongTamNhap) as SoLuongTamNhap ,
        SUM(ThietBi.SoLuongTaiXuat) as SoLuongTaiXuat ,
        case when (SUM(ThietBi.SoLuongTamNhap) - SUM(ThietBi.SoLuongTaiXuat))<0 then 0 else (SUM(ThietBi.SoLuongTamNhap) - SUM(ThietBi.SoLuongTaiXuat)) end as ConLai,
        MAX(ThietBi.GhiChu) as GhiChu
        
        --SUM(ABS(ThietBi.SoLuongTamNhap - ThietBi.SoLuongTaiXuat)) AS ConLai
--select *
FROM    ( SELECT    t_KDT_GC_HopDong.SoHopDong ,
                    t_KDT_GC_HopDong.NgayKy ,
                    t_KDT_GC_HopDong.NgayHetHan ,
                    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ( SELECT TOP 1
                                        SoTKVNACCS
                                FROM    dbo.t_VNACCS_CapSoToKhai
                                WHERE   SoTK = t_KDT_ToKhaiMauDich.SoToKhai
                              )
                         ELSE SoToKhai
                    END AS SoToKhai ,
                    t_KDT_ToKhaiMauDich.NgayDangKy ,
                    t_KDT_HangMauDich.ID ,
                    t_KDT_HangMauDich.TKMD_ID AS ID_ToKhai ,
                    t_KDT_HangMauDich.MaPhu AS MaHang ,
                    t_KDT_HangMauDich.TenHang ,
                    t_KDT_HangMauDich.DVT_ID AS DVT ,
                    --t_KDT_ToKhaiMauDich.SoToKhai ,
                    --t_KDT_ToKhaiMauDich.NgayDangKy ,
                    --t_KDT_HangMauDich.SoLuong AS SoLuongTamNhap ,
                    --0 AS SoLuongTaiXuat ,
                    case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%N%' then t_KDT_HangMauDich.SoLuong else 0 end AS SoLuongTamNhap, 
                                              case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%X%' then t_KDT_HangMauDich.SoLuong else 0 end AS SoLuongTaiXuat, 
                    t_KDT_ToKhaiMauDich.MaLoaiHinh ,
                    t_KDT_ToKhaiMauDich.MaHaiQuan ,
                    t_KDT_ToKhaiMauDich.MaDoanhNghiep,
                    Case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_ToKhaiMauDich.SoToKhai)) else (select top 1 SoHopDong from t_KDT_ToKhaiMauDich where SoToKhai = t_KDT_ToKhaiMauDich.SoToKhai)  end AS GhiChu

          FROM      t_KDT_HangMauDich
                    INNER JOIN t_KDT_ToKhaiMauDich ON t_KDT_HangMauDich.TKMD_ID = t_KDT_ToKhaiMauDich.ID
                    INNER JOIN t_KDT_GC_HopDong ON t_KDT_GC_HopDong.ID = t_KDT_ToKhaiMauDich.IDHopDong
          WHERE     ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'T' )
                    AND t_KDT_GC_HopDong.NamTN >= 2014
                    or t_KDT_ToKhaiMauDich.MaLoaiHinh = 'G23'

--select * from t_KDT_ToKhaiMauDich where ID = 10377
                       --AND (t_KDT_ToKhaiMauDich.IDHopDong = @HD_ID) 
                       --minhnd	fix thiết bị
                       --AND (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%NGC%')
                       --Or (t_KDT_ToKhaiMauDich.MaLoaiHinh = 'NVG13')
                       --minhnd	fix thiết bị
                       
                       --minhnd	fix hangmau
                       --UNION
                       --SELECT     0 as ID, 0 AS ID_ToKhai, t_GC_HangMau.Ma as MaHang, 
                       --                      t_GC_HangMau.Ten as TenHang, t_GC_HangMau.DVT_ID AS DVT, t_KDT_ToKhaiMauDich.SoToKhai, 
                       --                      t_KDT_ToKhaiMauDich.NgayDangKy, t_GC_HangMau.SoLuongDangKy AS SoLuongTamNhap, 0 AS SoLuongTaiXuat, 
                       --                      t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.MaHaiQuan AS MaHaiQuan, 
                       --                      t_KDT_ToKhaiMauDich.MaDoanhNghiep
                       --FROM         t_KDT_ToKhaiMauDich INNER JOIN
                       --                      t_GC_HangMau ON t_KDT_ToKhaiMauDich.IDHopDong = t_GC_HangMau.HopDong_ID
                       --WHERE     (t_GC_HangMau.HopDong_ID = @HD_ID)
                       
                       --minhnd	fix hangmau
          UNION
          SELECT    t_KDT_GC_HopDong.SoHopDong ,
                    t_KDT_GC_HopDong.NgayKy ,
                    t_KDT_GC_HopDong.NgayHetHan ,
                    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ( SELECT TOP 1
                                        SoTKVNACCS
                                FROM    dbo.t_VNACCS_CapSoToKhai
                                WHERE   SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai
                              )
                         ELSE SoToKhai
                    END AS SoToKhai ,
                    t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                    t_KDT_GC_HangChuyenTiep.ID ,
                    t_KDT_GC_HangChuyenTiep.Master_ID AS ID_ToKhai ,
                    t_KDT_GC_HangChuyenTiep.MaHang ,
                    t_KDT_GC_HangChuyenTiep.TenHang ,
                    t_KDT_GC_HangChuyenTiep.ID_DVT AS DVT ,
                    --t_KDT_GC_ToKhaiChuyenTiep.SoToKhai ,
                    --t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                    --0 AS SoLuongTamNhap ,
                    --t_KDT_GC_HangChuyenTiep.SoLuong AS SoLuongTaiXuat ,
                    case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%N%' then t_KDT_GC_HangChuyenTiep.SoLuong else 0 end AS SoLuongTamNhap, 
                                              case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%X%' then t_KDT_GC_HangChuyenTiep.SoLuong else 0 end AS SoLuongTaiXuat, 
                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,
                    t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan AS MaHaiQuan ,
                    t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep,
                    Case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai)) else (select top 1 SoHopDong from t_KDT_ToKhaiMauDich where SoToKhai = t_KDT_ToKhaiMauDich.SoToKhai)  end AS GhiChu
          FROM      t_KDT_GC_ToKhaiChuyenTiep
                    INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                    INNER JOIN dbo.t_KDT_GC_HopDong ON t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = t_KDT_GC_HopDong.ID
          WHERE     ( t_KDT_GC_HopDong.NamTN >= 2014 )
                    AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%TBN%' OR
                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%G22%' 
                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh in ('NGC20','XGC20')
                          or t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%'
                          and t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'T'
                        )
        ) AS ThietBi
        INNER JOIN t_HaiQuan_DonViTinh ON ThietBi.DVT = t_HaiQuan_DonViTinh.ID
GROUP BY ThietBi.SoHopDong ,
        --ThietBi.NgayKy ,
        --ThietBi.NgayHetHan ,
        --ThietBi.SoToKhai ,
        --ThietBi.NgayDangKy ,
        --ThietBi.MaLoaiHinh,
        ThietBi.MaHang ,
        --ThietBi.TenHang ,
        --t_HaiQuan_DonViTinh.Ten ,
        ThietBi.MaHaiQuan ,
        ThietBi.MaDoanhNghiep
HAVING  ( ThietBi.MaHaiQuan = 'C34C' )
        AND ( ThietBi.MaDoanhNghiep = '0400101556' )
ORDER BY ThietBi.SoHopDong, ThietBi.MaHang

end
--select * from t_KDT_ToKhaiMauDich where NgayDangKy = '2014-11-24'
--update t_KDT_VNACC_ToKhaiMauDich set TrangThaiXuLy = 0 where SoToKhai = 100370301730
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HMD_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HMD_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_XuatNhapTon]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: 
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_XuatNhapTon]
	@HopDongID BIGINT,
	@IDPhieuXuat BIGINT,
	@MaPO VARCHAR(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

select a.MaNPL,sum(a.LuongNhap) as LuongNhap, sum(a.LuongXuat) as LuongXuat, max(b.Ten) as TenNPL, MAX(c.NameV5) as DVT, Sum(a.LuongNhap) - sum (a.LuongXuat) as LuongTon from 
(
select   Ma2 as MaNPL, SoLuong1 as LuongNhap, 0 as LuongXuat, 0 as LuongTon 
from dbo.t_KDT_GC_PhieuNhapKho_Hang
where TKMD_ID in (select ID from dbo.t_KDT_GC_PhieuNhapKho where TKMD_ID in (Select ID from t_kdt_vnacc_tokhaimaudich where HopDong_ID = @HopDongID))
union 
select MaNPL2 as MaNPL, 0 as LuongNhap, NhuCau as LuongXuat, 0 as LuongTon 
from dbo.t_KDT_GC_PhieuXuatKho_Hang
WHERE TKMD_ID in (select ID from  dbo.t_KDT_GC_PhieuXuatKho WHERE PO != @MaPO AND TKMD_ID != @IDPhieuXuat AND TKMD_ID in (Select ID from t_kdt_vnacc_tokhaimaudich where HopDong_ID = @HopDongID))
) as a
inner join (select * from t_gc_nguyenphulieu where HopDong_ID = @HopDongID) as b 
on a.MaNPL = b.Ma
inner join (select * from dbo.t_VNACCS_Mapper where LoaiMapper = 'DVT' ) as c
on b.DVT_ID = c.CodeV4 

group by a.MaNPL
order by a.MaNPL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SP_Xuat_HD]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SP_Xuat_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_BC02HSTK_GC_TT38]'
GO


CREATE PROCEDURE [dbo].[p_GC_BC02HSTK_GC_TT38]       
 -- Add the parameters for the stored procedure here      
    @IDHopDong BIGINT ,
    @MaHaiQuan CHAR(6) ,
    @MaDoanhNghiep VARCHAR(14) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
    BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
        SET NOCOUNT ON ;      
 -- p_GC_BC02HSTK_GC_TT117 710, 'C34C','0400101556'      
-- SET @IDHopDong=710 ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',      
    --ROW_NUMBER() OVER (ORDER BY v_KDT_SP_Xuat_HD.MaPhu) AS 'STT',   
        SELECT DISTINCT
                ROW_NUMBER() OVER ( ORDER BY v_KDT_SP_Xuat_HD.MaPhu ) AS 'STT' ,
                CONVERT(VARCHAR(50), ( CASE WHEN ToKhai.MaLoaiHinh LIKE '%V%'
                                            THEN ( SELECT TOP 1
                                                            SoTKVNACCS
                                                   FROM     t_VNACCS_CapSoToKhai
                                                   WHERE    SoTK = ToKhai.SoToKhai
                                                 )
                                            ELSE ToKhai.SoToKhai
                                       END )) AS SoToKhai ,
                         ToKhai.MaLoaiHinh,
                ToKhai.NgayDangKy ,
                v_KDT_SP_Xuat_HD.MaPhu AS MaHang ,
                ToKhai.TenHang ,
                t_HaiQuan_DonViTinh.Ten AS DVT ,
                ToKhai.SoLuong ,
                ToKhai.SoLuong AS TongCong
        FROM    v_KDT_SP_Xuat_HD
                INNER JOIN ( SELECT t_KDT_ToKhaiMauDich.SoToKhai ,
                                    t_KDT_ToKhaiMauDich.MaLoaiHinh ,
                                    t_KDT_ToKhaiMauDich.NgayDangKy ,
                                    t_KDT_ToKhaiMauDich.IDHopDong ,
                                    t_KDT_ToKhaiMauDich.MaHaiQuan ,
                                    t_KDT_HangMauDich.MaPhu AS MaHang ,
                                    t_KDT_HangMauDich.TenHang ,
                                    t_KDT_HangMauDich.SoLuong ,
                                    t_KDT_ToKhaiMauDich.MaDoanhNghiep
                             FROM   t_KDT_ToKhaiMauDich
                                    INNER JOIN t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                             WHERE  ( t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 )
                                    AND ( t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong )
                                    AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' )
                                    AND ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' )
                             UNION
                             SELECT t_KDT_GC_ToKhaiChuyenTiep.SoToKhai ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,
                                    t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                                    t_KDT_GC_ToKhaiChuyenTiep.IDHopDong ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan ,
                                    t_KDT_GC_HangChuyenTiep.MaHang ,
                                    t_KDT_GC_HangChuyenTiep.TenHang ,
                                    t_KDT_GC_HangChuyenTiep.SoLuong ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                             FROM   t_KDT_GC_ToKhaiChuyenTiep
                                    INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                             WHERE  ( t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 )
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong )
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX'
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19'
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XVE54'
                                        )
                           ) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong
                                          AND v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang
                INNER JOIN t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID
                INNER JOIN t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
        WHERE   ( v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong )
                AND ( ToKhai.MaDoanhNghiep = @MaDoanhNghiep )
                AND ( ToKhai.MaHaiQuan = @MaHaiQuan )
                AND ToKhai.NgayDangKy>= @TuNgay
                AND ToKhai.NgayDangKy<=@DenNgay
        ORDER BY MaHang,ToKhai.NgayDangKy   
      
    END   
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhieuNhapKho] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 10 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[NgayNhapKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[NhapTaiKho],
	[NgoaiTe],
	[TyGia],
	[LyDo],
	[NhaCungCap],
	[ToKhai],
	[SoInvoice],
	[NgayInvoice],
	[SoBL],
	[SoKien],
	[TongVAT]
FROM [dbo].[t_KDT_GC_PhieuNhapKho] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Hang_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhieuNhapKho_Hang] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 05 September, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[Ma2],
	[TenHang],
	[Mau],
	[Size],
	[DVTLuong1],
	[SoLuong1],
	[SoLuongThucNhap],
	[DonGiaNgoaiTe],
	[DonGiaHoaDon],
	[ThanhTienNgoaiTe],
	[TriGiaHoaDon],
	[GhiChu],
	[MaTTDonGia]
FROM [dbo].[t_KDT_GC_PhieuNhapKho_Hang] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhieuXuatKho] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 26 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[NgayXuatKho],
	[SoPhieu],
	[SoHopDong],
	[KhachHang],
	[Style],
	[PO],
	[DonViNhan],
	[XuatTaiKho],
	[SoLuong],
	[DVTSoLuong]
FROM [dbo].[t_KDT_GC_PhieuXuatKho] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Hang_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_PhieuXuatKho_Hang] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 28 August, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_PhieuXuatKho_Hang_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaNPL2],
	[TenHang],
	[Art],
	[NCC],
	[Size],
	[Color],
	[SoLuong1],
	[DVTLuong1],
	[DM],
	[PhanTram],
	[Nhucau],
	[ThucNhan],
	[GhiChu]
FROM [dbo].[t_KDT_GC_PhieuXuatKho_Hang] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[t_KDT_VNACC_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich] ADD CONSTRAINT [DF__t_KDT_VNA__HopDo__7EC430E0] DEFAULT ('0') FOR [HopDong_ID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_ToKhaiMauDich] ADD
CONSTRAINT [FK_t_KDT_ToKhaiMauDich_t_KDT_GC_HopDong] FOREIGN KEY ([IDHopDong]) REFERENCES [dbo].[t_KDT_GC_HopDong] ([ID]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ADD
CONSTRAINT [FK_t_HangMauDich_t_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

GO
create PROCEDURE [dbo].[p_GC_GetDinhMucSanPhamForBC06_TT38]         
 -- Add the parameters for the stored procedure here        
    @IDHopDong BIGINT ,
    @MaHaiQuan CHAR(6) ,
    @MaDoanhNghiep VARCHAR(14) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
    BEGIN    
        SELECT DISTINCT
                --ROW_NUMBER() OVER ( ORDER BY v_KDT_SP_Xuat_HD.MaPhu ) AS 'STT' ,
                CONVERT(VARCHAR(50), ( CASE WHEN ToKhai.MaLoaiHinh LIKE '%V%'
                                            THEN ( SELECT TOP 1
                                                            SoTKVNACCS
                                                   FROM     t_VNACCS_CapSoToKhai
                                                   WHERE    SoTK = ToKhai.SoToKhai
                                                 )
                                            ELSE ToKhai.SoToKhai
                                       END )) AS SoToKhai ,
                ToKhai.MaLoaiHinh ,
                ToKhai.NgayDangKy ,
                v_KDT_SP_Xuat_HD.MaPhu AS MaHang ,
                ToKhai.TenHang ,
                '' AS DVT ,
                ToKhai.SoLuong ,
                ToKhai.SoLuong AS TongCong ,
                DM.MaNguyenPhuLieu ,
                CONVERT(DECIMAL(16,6),DM.DinhMucSuDung) AS DinhMucSuDung ,
                CONVERT(DECIMAL(16,6),DM.TyLeHaoHut) AS TyLeHaoHut ,
                CONVERT(DECIMAL(24,6),ToKhai.SoLuong * DM.DinhMucSuDung) AS LuongNPLChuaHH ,
                ( ToKhai.SoLuong * DM.DinhMucSuDung ) + ( ToKhai.SoLuong
                                                          * ( DM.DinhMucSuDung
                                                              * ( DM.TyLeHaoHut
                                                              / 100 ) ) ) AS LuongNPLCoHH
        FROM    v_KDT_SP_Xuat_HD
                INNER JOIN ( SELECT t_KDT_ToKhaiMauDich.SoToKhai ,
                                    t_KDT_ToKhaiMauDich.MaLoaiHinh ,
                                    t_KDT_ToKhaiMauDich.NgayDangKy ,
                                    t_KDT_ToKhaiMauDich.IDHopDong ,
                                    t_KDT_ToKhaiMauDich.MaHaiQuan ,
                                    t_KDT_HangMauDich.MaPhu AS MaHang ,
                                    t_KDT_HangMauDich.TenHang ,
                                    t_KDT_HangMauDich.SoLuong ,
                                    t_KDT_ToKhaiMauDich.MaDoanhNghiep
                             FROM   t_KDT_ToKhaiMauDich
                                    INNER JOIN t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                             WHERE  ( t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 )
                                    AND ( t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong )
                                    AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' )
                                    AND ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' )
                             UNION
                             SELECT t_KDT_GC_ToKhaiChuyenTiep.SoToKhai ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,
                                    t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                                    t_KDT_GC_ToKhaiChuyenTiep.IDHopDong ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan ,
                                    t_KDT_GC_HangChuyenTiep.MaHang ,
                                    t_KDT_GC_HangChuyenTiep.TenHang ,
                                    t_KDT_GC_HangChuyenTiep.SoLuong ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                             FROM   t_KDT_GC_ToKhaiChuyenTiep
                                    INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                             WHERE  ( t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 )
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong )
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX'
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19'
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XVE54'
                                        )
                           ) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong
                                          AND v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang
                INNER JOIN t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID
                INNER JOIN t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
                INNER JOIN dbo.t_GC_DinhMuc AS DM ON ToKhai.MaHang = DM.MaSanPham
                                                     AND ToKhai.IDHopDong = DM.HopDong_ID
        WHERE   ( v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong )
                AND ( ToKhai.MaDoanhNghiep = @MaDoanhNghiep )
                AND ( ToKhai.MaHaiQuan = @MaHaiQuan )
                AND ToKhai.NgayDangKy >= @TuNgay
                AND ToKhai.NgayDangKy <= @DenNgay
ORDER BY        MaNguyenPhuLieu, 
                ToKhai.NgayDangKy,
                MaHang     
    END
	

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '22.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('22.1',GETDATE(), N'KhoHang_PhieuKho')
END	