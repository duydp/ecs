﻿

using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean=Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.BLL.KDT.SXXK;
using System.Xml.Serialization;
using System.Data;

namespace Company.Interface
{
	public partial class SelectToKhaiMauDichThongKeForm : BaseForm
	{
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        public DataTable table = new DataTable();
        private string xmlCurrent = "";
        public string nhomLoaiHinh = "";
        public SelectToKhaiMauDichThongKeForm()
		{
			InitializeComponent();
		}

		//-----------------------------------------------------------------------------------------

     
		private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
		{
            try
			{
                table = new DataTable();
                table.Columns.Add("MaPhu");
                table.Columns.Add("TenHang");
                table.Columns.Add("SoLuong");
                table.Columns.Add("TKMD_ID");
                table.Columns.Add("SoTiepNhan");                               
                this.search();               
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private ToKhaiMauDich getTKMDByID(long id)
        {
            foreach (ToKhaiMauDich tk in this.tkmdCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {           
            if (e.Row.RowType == RowType.Record)
            {
                long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                ToKhaiMauDich TKMD= this.getTKMDByID(id);
                TKMD.LoadHMDCollection();
                table.Rows.Clear();
                foreach (HangMauDich HMD in TKMD.HMDCollection)
                {
                    table.Rows.Add(new string[] { HMD.MaPhu,HMD.TenHang,HMD.SoLuong.ToString(),TKMD.ID.ToString(),TKMD.SoTiepNhan.ToString() });
                }
                this.Close();
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            
            string where = "1 = 1";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
            }

            
            
            where += " AND TrangThaiXuLy <>1 ";
            where += " AND MaLoaiHinh LIKE 'X%'";
            // Thực hiện tìm kiếm.            
            this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.tkmdCollection;
            
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case 0:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case 1:
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                               
                        }
                        break;
                    case 2:
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                }
            }
        }

     
       
     

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
             table.Rows.Clear();
             GridEXSelectedItemCollection items = dgList.SelectedItems;
             if (items.Count > 0)
             {
                 foreach (GridEXSelectedItem i in items)
                 {
                     if (i.RowType == RowType.Record)
                     {
                         ToKhaiMauDich TKMD = (ToKhaiMauDich)i.GetRow().DataRow;
                         TKMD.LoadHMDCollection();                         
                         foreach (HangMauDich HMD in TKMD.HMDCollection)
                         {
                             table.Rows.Add(new string[] { HMD.MaPhu, HMD.TenHang, HMD.SoLuong.ToString(), TKMD.ID.ToString(), TKMD.SoTiepNhan.ToString() });
                         }
                     }
                 }
             }
        }

     
	}
}