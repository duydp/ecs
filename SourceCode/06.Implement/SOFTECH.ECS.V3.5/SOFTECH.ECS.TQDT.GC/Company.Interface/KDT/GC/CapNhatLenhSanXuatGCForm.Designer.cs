﻿namespace Company.Interface.KDT.GC
{
    partial class CapNhatLenhSanXuatGCForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout cbbLenhSanXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout cbHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grListSP_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgListNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListNPL_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CapNhatLenhSanXuatGCForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbTinhTrang = new Janus.Windows.EditControls.UIComboBox();
            this.cbbLenhSanXuat = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.clcNgayHĐ = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayHH = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbHopDong = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnAuto = new Janus.Windows.EditControls.UIButton();
            this.btnUpdate = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListSP = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMaSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLenhSanXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 726), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 726);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 702);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 702);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox9);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox10);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1010, 726);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1010, 178);
            this.uiGroupBox1.TabIndex = 345;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.cbbTinhTrang);
            this.uiGroupBox5.Controls.Add(this.cbbLenhSanXuat);
            this.uiGroupBox5.Controls.Add(this.clcNgayHĐ);
            this.uiGroupBox5.Controls.Add(this.clcTuNgay);
            this.uiGroupBox5.Controls.Add(this.clcNgayHH);
            this.uiGroupBox5.Controls.Add(this.clcDenNgay);
            this.uiGroupBox5.Controls.Add(this.txtGhiChu);
            this.uiGroupBox5.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox5.Controls.Add(this.label4);
            this.uiGroupBox5.Controls.Add(this.txtPO);
            this.uiGroupBox5.Controls.Add(this.label3);
            this.uiGroupBox5.Controls.Add(this.label2);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.label11);
            this.uiGroupBox5.Controls.Add(this.label8);
            this.uiGroupBox5.Controls.Add(this.label10);
            this.uiGroupBox5.Controls.Add(this.label7);
            this.uiGroupBox5.Controls.Add(this.label15);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 56);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1004, 119);
            this.uiGroupBox5.TabIndex = 345;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbTinhTrang
            // 
            this.cbbTinhTrang.BackColor = System.Drawing.Color.White;
            this.cbbTinhTrang.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbTinhTrang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Tạo mới";
            uiComboBoxItem1.Value = "0";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Đang sản xuất";
            uiComboBoxItem2.Value = "1";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Đã hoàn thành";
            uiComboBoxItem3.Value = "2";
            this.cbbTinhTrang.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3});
            this.cbbTinhTrang.Location = new System.Drawing.Point(695, 83);
            this.cbbTinhTrang.Name = "cbbTinhTrang";
            this.cbbTinhTrang.Size = new System.Drawing.Size(160, 22);
            this.cbbTinhTrang.TabIndex = 42;
            this.cbbTinhTrang.ValueMember = "ID";
            this.cbbTinhTrang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbbLenhSanXuat
            // 
            cbbLenhSanXuat_DesignTimeLayout.LayoutString = resources.GetString("cbbLenhSanXuat_DesignTimeLayout.LayoutString");
            this.cbbLenhSanXuat.DesignTimeLayout = cbbLenhSanXuat_DesignTimeLayout;
            this.cbbLenhSanXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLenhSanXuat.Location = new System.Drawing.Point(105, 19);
            this.cbbLenhSanXuat.Name = "cbbLenhSanXuat";
            this.cbbLenhSanXuat.SelectedIndex = -1;
            this.cbbLenhSanXuat.SelectedItem = null;
            this.cbbLenhSanXuat.Size = new System.Drawing.Size(301, 21);
            this.cbbLenhSanXuat.TabIndex = 41;
            this.cbbLenhSanXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbLenhSanXuat.ValueChanged += new System.EventHandler(this.cbbLenhSanXuat_ValueChanged);
            this.cbbLenhSanXuat.TextChanged += new System.EventHandler(this.cbbLenhSanXuat_ValueChanged);
            // 
            // clcNgayHĐ
            // 
            this.clcNgayHĐ.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHĐ.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHĐ.DropDownCalendar.Name = "";
            this.clcNgayHĐ.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHĐ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHĐ.Location = new System.Drawing.Point(309, 84);
            this.clcNgayHĐ.Name = "clcNgayHĐ";
            this.clcNgayHĐ.ReadOnly = true;
            this.clcNgayHĐ.Size = new System.Drawing.Size(97, 21);
            this.clcNgayHĐ.TabIndex = 38;
            this.clcNgayHĐ.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcNgayHĐ.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.CustomFormat = "dd/MM/yyyy";
            this.clcTuNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcTuNgay.DropDownCalendar.Name = "";
            this.clcTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcTuNgay.Location = new System.Drawing.Point(485, 19);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.Size = new System.Drawing.Size(97, 21);
            this.clcTuNgay.TabIndex = 37;
            this.clcTuNgay.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayHH
            // 
            this.clcNgayHH.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHH.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHH.DropDownCalendar.Name = "";
            this.clcNgayHH.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHH.Location = new System.Drawing.Point(485, 84);
            this.clcNgayHH.Name = "clcNgayHH";
            this.clcNgayHH.ReadOnly = true;
            this.clcNgayHH.Size = new System.Drawing.Size(97, 21);
            this.clcNgayHH.TabIndex = 40;
            this.clcNgayHH.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcNgayHH.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcDenNgay
            // 
            this.clcDenNgay.CustomFormat = "dd/MM/yyyy";
            this.clcDenNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDenNgay.DropDownCalendar.Name = "";
            this.clcDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcDenNgay.Location = new System.Drawing.Point(695, 19);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.Size = new System.Drawing.Size(97, 21);
            this.clcDenNgay.TabIndex = 39;
            this.clcDenNgay.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            this.clcDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(105, 50);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(477, 22);
            this.txtGhiChu.TabIndex = 34;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.BackColor = System.Drawing.Color.White;
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(105, 83);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.ReadOnly = true;
            this.txtSoHopDong.Size = new System.Drawing.Size(131, 22);
            this.txtSoHopDong.TabIndex = 35;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(412, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 14);
            this.label4.TabIndex = 33;
            this.label4.Text = "Ngày HH :";
            // 
            // txtPO
            // 
            this.txtPO.BackColor = System.Drawing.Color.White;
            this.txtPO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPO.Location = new System.Drawing.Point(695, 50);
            this.txtPO.Name = "txtPO";
            this.txtPO.Size = new System.Drawing.Size(160, 22);
            this.txtPO.TabIndex = 36;
            this.txtPO.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(242, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 14);
            this.label3.TabIndex = 28;
            this.label3.Text = "Ngày HĐ :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(601, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 14);
            this.label2.TabIndex = 25;
            this.label2.Text = "Đến ngày :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(412, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 14);
            this.label1.TabIndex = 26;
            this.label1.Text = "Từ ngày :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(601, 87);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 14);
            this.label11.TabIndex = 31;
            this.label11.Text = "Tình trạng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 14);
            this.label8.TabIndex = 32;
            this.label8.Text = "Ghi chú : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(601, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 14);
            this.label10.TabIndex = 29;
            this.label10.Text = "Số đơn hàng";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 14);
            this.label7.TabIndex = 30;
            this.label7.Text = "Số HĐ :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(11, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 14);
            this.label15.TabIndex = 27;
            this.label15.Text = "Lệnh sản xuất";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.cbHopDong);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1004, 48);
            this.uiGroupBox4.TabIndex = 344;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbHopDong
            // 
            cbHopDong_DesignTimeLayout.LayoutString = resources.GetString("cbHopDong_DesignTimeLayout.LayoutString");
            this.cbHopDong.DesignTimeLayout = cbHopDong_DesignTimeLayout;
            this.cbHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHopDong.Location = new System.Drawing.Point(107, 16);
            this.cbHopDong.Name = "cbHopDong";
            this.cbHopDong.SelectedIndex = -1;
            this.cbHopDong.SelectedItem = null;
            this.cbHopDong.Size = new System.Drawing.Size(475, 21);
            this.cbHopDong.TabIndex = 305;
            this.cbHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbHopDong.ValueChanged += new System.EventHandler(this.cbHopDong_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 86;
            this.label5.Text = "Số hợp đồng : ";
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.AutoScroll = true;
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox10.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox10.Location = new System.Drawing.Point(0, 667);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(1010, 59);
            this.uiGroupBox10.TabIndex = 354;
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(507, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(500, 48);
            this.uiGroupBox2.TabIndex = 9;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(416, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnAuto);
            this.uiGroupBox7.Controls.Add(this.btnUpdate);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(504, 48);
            this.uiGroupBox7.TabIndex = 8;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnAuto
            // 
            this.btnAuto.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuto.Image = ((System.Drawing.Image)(resources.GetObject("btnAuto.Image")));
            this.btnAuto.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAuto.Location = new System.Drawing.Point(275, 16);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(220, 23);
            this.btnAuto.TabIndex = 4;
            this.btnAuto.Text = "Cập nhật tự động lệnh sản xuất";
            this.btnAuto.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageSize = new System.Drawing.Size(20, 20);
            this.btnUpdate.Location = new System.Drawing.Point(9, 16);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(118, 23);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 178);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(507, 489);
            this.uiGroupBox3.TabIndex = 355;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.grListSP);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(501, 478);
            this.uiGroupBox8.TabIndex = 5;
            this.uiGroupBox8.Text = "Danh sách sản phẩm";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListSP
            // 
            this.grListSP.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListSP.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListSP.ColumnAutoResize = true;
            grListSP_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grListSP_DesignTimeLayout_Reference_0.Instance")));
            grListSP_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grListSP_DesignTimeLayout_Reference_0});
            grListSP_DesignTimeLayout.LayoutString = resources.GetString("grListSP_DesignTimeLayout.LayoutString");
            this.grListSP.DesignTimeLayout = grListSP_DesignTimeLayout;
            this.grListSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListSP.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListSP.FrozenColumns = 5;
            this.grListSP.GroupByBoxVisible = false;
            this.grListSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSP.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListSP.Hierarchical = true;
            this.grListSP.Location = new System.Drawing.Point(3, 67);
            this.grListSP.Name = "grListSP";
            this.grListSP.RecordNavigator = true;
            this.grListSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListSP.Size = new System.Drawing.Size(495, 408);
            this.grListSP.TabIndex = 87;
            this.grListSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListSP.SelectionChanged += new System.EventHandler(this.grListSP_SelectionChanged);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearch);
            this.uiGroupBox6.Controls.Add(this.label16);
            this.uiGroupBox6.Controls.Add(this.txtMaSP);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(495, 50);
            this.uiGroupBox6.TabIndex = 86;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(407, 16);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 87;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 86;
            this.label16.Text = "Mã SP";
            // 
            // txtMaSP
            // 
            this.txtMaSP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSP.BackColor = System.Drawing.Color.White;
            this.txtMaSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSP.Location = new System.Drawing.Point(100, 17);
            this.txtMaSP.Name = "txtMaSP";
            this.txtMaSP.Size = new System.Drawing.Size(301, 21);
            this.txtMaSP.TabIndex = 85;
            this.txtMaSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSP.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.dgListNPL);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox9.Location = new System.Drawing.Point(507, 178);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(503, 489);
            this.uiGroupBox9.TabIndex = 356;
            this.uiGroupBox9.Text = "Thông tin định mức của sản phẩm";
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            dgListNPL_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListNPL_DesignTimeLayout_Reference_0.Instance")));
            dgListNPL_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListNPL_DesignTimeLayout_Reference_0});
            dgListNPL_DesignTimeLayout.LayoutString = resources.GetString("dgListNPL_DesignTimeLayout.LayoutString");
            this.dgListNPL.DesignTimeLayout = dgListNPL_DesignTimeLayout;
            this.dgListNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(3, 17);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RecordNavigator = true;
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPL.Size = new System.Drawing.Size(497, 469);
            this.dgListNPL.TabIndex = 343;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListNPL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNPL_LoadingRow);
            // 
            // CapNhatLenhSanXuatGCForm
            // 
            this.ClientSize = new System.Drawing.Size(1216, 732);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CapNhatLenhSanXuatGCForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cập nhật lệnh sản xuất cho sản phẩm";
            this.Load += new System.EventHandler(this.CapNhatLenhSanXuatGCForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLenhSanXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIButton btnUpdate;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.GridEX dgListNPL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.GridEX grListSP;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSP;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIComboBox cbbTinhTrang;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbLenhSanXuat;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHĐ;
        private Janus.Windows.CalendarCombo.CalendarCombo clcTuNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHH;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDenNgay;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtPO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        public Janus.Windows.GridEX.EditControls.MultiColumnCombo cbHopDong;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIButton btnAuto;

    }
}
