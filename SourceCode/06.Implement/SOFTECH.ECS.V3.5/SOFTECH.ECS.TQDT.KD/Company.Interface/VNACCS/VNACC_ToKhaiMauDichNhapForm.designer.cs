﻿namespace Company.Interface
{
    partial class VNACC_ToKhaiMauDichNhapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ToKhaiMauDichNhapForm));
            Janus.Windows.GridEX.GridEXLayout grListTyGia_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListSacThue_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.grbDonVi = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoDienThoaiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayNhapKhoDauTien = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.btnMaHQ = new Janus.Windows.EditControls.UIButton();
            this.btnMaLoaiHinh = new Janus.Windows.EditControls.UIButton();
            this.txtSoToKhaiDauTien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrNhomXuLyHS = new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy();
            this.ctrCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.clcNgayDangKy = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcThoiHanTaiNhapTaiXuat = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrMaPhuongThucVT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiHH = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrPhanLoaiToChuc = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaLoaiHinh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTongSoTKChiaNho = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoNhanhToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoToKhaiTNTX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblPhanLuong = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.grbDoiTac = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaNuocDoiTac = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtDiaChiDoiTac2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDiaChiDoiTac4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTenDaiLyHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLyHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.tabThongtin = new Janus.Windows.UI.Tab.UITab();
            this.TabItemThongtin = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtUyThacXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label71 = new System.Windows.Forms.Label();
            this.rdbHangMau = new Janus.Windows.EditControls.UIRadioButton();
            this.rdbSanPham = new Janus.Windows.EditControls.UIRadioButton();
            this.rdbThietBi = new Janus.Windows.EditControls.UIRadioButton();
            this.rdbNPL = new Janus.Windows.EditControls.UIRadioButton();
            this.txtSoHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox14 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayDen = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrDiaDiemDichVC = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.clcNgayKhoiHanhVC = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label59 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaNHBaoLanh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaXDThoiHanNopThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtKyHieuCTBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtSoCTBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamPhatHanhBL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrDiaDiemXepHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ctrMaDDLuuKho = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDiaDiemDoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.clcNgayHangDen = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.txtMaPTVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuongCont = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txtSoHieuKyHieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenPTVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaNHTraThueThay = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaLyDoDeNghi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrNguoiNopThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtKyHieuCTHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtNamPhatHanhHM = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoCTHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaKetQuaKiemTra = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapQuy4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapQuy5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapQuy3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapQuy1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaVanBanPhapQuy2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDieuKienGiaHD = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrPhuongThucTT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaPhanLoaiTriGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.clcNgayPhatHanhHD = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrPhanLoaiHD = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTHoaDon = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtSoTiepNhanHD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtTongHeSoPhanBo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongTriGiaHD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDVTTrongLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaDVTSoLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoQuanLyNoiBoDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNgayHD1 = new System.Windows.Forms.Label();
            this.lblNgayHD = new System.Windows.Forms.Label();
            this.lblSoHD = new System.Windows.Forms.Label();
            this.TabItemPhanHoi = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox15 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox17 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListTyGia = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListSacThue = new Janus.Windows.GridEX.GridEX();
            this.txtTongSoDongHangCuaToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSoTrangCuaToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtSoTienBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label67 = new System.Windows.Forms.Label();
            this.txtTongTienThuePhaiNop = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label62 = new System.Windows.Forms.Label();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdToKhaiTriGia1 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.cmdVanDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmDinhKemDT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDinhKemDT");
            this.cmdTrungChuyen1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTrungChuyen");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdInAn1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInAn");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChiThiHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHQ");
            this.cmdKetQuaTraVe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaTraVe");
            this.cmdMau301 = new Janus.Windows.UI.CommandBars.UICommand("cmdMau30");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdToKhaiNhanh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiNhanh");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdThemHangDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHangDon");
            this.cmdThemHangExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHangExcel");
            this.cmdChiThiHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHQ");
            this.cmdGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdBoSungCont1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungCont");
            this.cmdSendVoucher1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendVoucher");
            this.cmdDinhKemDT = new Janus.Windows.UI.CommandBars.UICommand("cmdDinhKemDT");
            this.cmdTrungChuyen = new Janus.Windows.UI.CommandBars.UICommand("cmdTrungChuyen");
            this.cmdToKhaiTriGia = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.cmdInAn = new Janus.Windows.UI.CommandBars.UICommand("cmdInAn");
            this.cmdKetQuaTraVe = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaTraVe");
            this.cmdThemHangDon = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHangDon");
            this.cmdThemHangExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHangExcel");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdReloadData1 = new Janus.Windows.UI.CommandBars.UICommand("cmdReloadData");
            this.cmdReloadData = new Janus.Windows.UI.CommandBars.UICommand("cmdReloadData");
            this.cmdBoSungCont = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungCont");
            this.cmdMau30 = new Janus.Windows.UI.CommandBars.UICommand("cmdMau30");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdSendVoucher = new Janus.Windows.UI.CommandBars.UICommand("cmdSendVoucher");
            this.cmdToKhaiNhanh = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiNhanh");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.ucCategory1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).BeginInit();
            this.grbDonVi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).BeginInit();
            this.grbDoiTac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabThongtin)).BeginInit();
            this.tabThongtin.SuspendLayout();
            this.TabItemThongtin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).BeginInit();
            this.uiGroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            this.TabItemPhanHoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).BeginInit();
            this.uiGroupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).BeginInit();
            this.uiGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListTyGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListSacThue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 743), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Image = ((System.Drawing.Image)(resources.GetObject("uiPanelGuide.Image")));
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 743);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 719);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 719);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(1070, 743);
            // 
            // grbDonVi
            // 
            this.grbDonVi.BackColor = System.Drawing.Color.Transparent;
            this.grbDonVi.Controls.Add(this.txtDiaChiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaDonVi);
            this.grbDonVi.Controls.Add(this.label3);
            this.grbDonVi.Controls.Add(this.txtSoDienThoaiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaBuuChinhDonVi);
            this.grbDonVi.Controls.Add(this.label5);
            this.grbDonVi.Controls.Add(this.txtTenDonVi);
            this.grbDonVi.Controls.Add(this.label4);
            this.grbDonVi.Controls.Add(this.label1);
            this.grbDonVi.Controls.Add(this.label2);
            this.grbDonVi.Location = new System.Drawing.Point(6, 127);
            this.grbDonVi.Name = "grbDonVi";
            this.grbDonVi.Size = new System.Drawing.Size(306, 154);
            this.grbDonVi.TabIndex = 1;
            this.grbDonVi.Text = "Người nhập khẩu";
            this.grbDonVi.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiDonVi
            // 
            this.txtDiaChiDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDonVi.BackColor = System.Drawing.SystemColors.Info;
            this.txtDiaChiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDonVi.Location = new System.Drawing.Point(54, 84);
            this.txtDiaChiDonVi.MaxLength = 100;
            this.txtDiaChiDonVi.Multiline = true;
            this.txtDiaChiDonVi.Name = "txtDiaChiDonVi";
            this.txtDiaChiDonVi.Size = new System.Drawing.Size(226, 40);
            this.txtDiaChiDonVi.TabIndex = 2;
            this.txtDiaChiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDonVi.BackColor = System.Drawing.SystemColors.Info;
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(54, 17);
            this.txtMaDonVi.MaxLength = 13;
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.Size = new System.Drawing.Size(226, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Địa chỉ";
            // 
            // txtSoDienThoaiDonVi
            // 
            this.txtSoDienThoaiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDienThoaiDonVi.Location = new System.Drawing.Point(185, 127);
            this.txtSoDienThoaiDonVi.MaxLength = 255;
            this.txtSoDienThoaiDonVi.Name = "txtSoDienThoaiDonVi";
            this.txtSoDienThoaiDonVi.Size = new System.Drawing.Size(95, 21);
            this.txtSoDienThoaiDonVi.TabIndex = 4;
            this.txtSoDienThoaiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDienThoaiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaBuuChinhDonVi
            // 
            this.txtMaBuuChinhDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDonVi.Location = new System.Drawing.Point(54, 127);
            this.txtMaBuuChinhDonVi.MaxLength = 255;
            this.txtMaBuuChinhDonVi.Name = "txtMaBuuChinhDonVi";
            this.txtMaBuuChinhDonVi.Size = new System.Drawing.Size(79, 21);
            this.txtMaBuuChinhDonVi.TabIndex = 3;
            this.txtMaBuuChinhDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(132, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Điện thoại";
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.BackColor = System.Drawing.SystemColors.Info;
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(54, 41);
            this.txtTenDonVi.MaxLength = 100;
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.Size = new System.Drawing.Size(226, 40);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Bưu chính";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Mã";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Tên";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtMaUyThac);
            this.uiGroupBox2.Controls.Add(this.txtTenUyThac);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Location = new System.Drawing.Point(6, 285);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(306, 83);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.Text = "Người ủy thác nhập khẩu";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtMaUyThac
            // 
            this.txtMaUyThac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaUyThac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaUyThac.Location = new System.Drawing.Point(54, 18);
            this.txtMaUyThac.MaxLength = 13;
            this.txtMaUyThac.Name = "txtMaUyThac";
            this.txtMaUyThac.Size = new System.Drawing.Size(227, 21);
            this.txtMaUyThac.TabIndex = 0;
            this.txtMaUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenUyThac
            // 
            this.txtTenUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenUyThac.Location = new System.Drawing.Point(53, 44);
            this.txtTenUyThac.MaxLength = 100;
            this.txtTenUyThac.Multiline = true;
            this.txtTenUyThac.Name = "txtTenUyThac";
            this.txtTenUyThac.Size = new System.Drawing.Size(228, 31);
            this.txtTenUyThac.TabIndex = 1;
            this.txtTenUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 33;
            this.label12.Text = "Mã";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Tên";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.clcNgayNhapKhoDauTien);
            this.uiGroupBox3.Controls.Add(this.btnMaHQ);
            this.uiGroupBox3.Controls.Add(this.btnMaLoaiHinh);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhaiDauTien);
            this.uiGroupBox3.Controls.Add(this.ctrNhomXuLyHS);
            this.uiGroupBox3.Controls.Add(this.ctrCoQuanHaiQuan);
            this.uiGroupBox3.Controls.Add(this.clcNgayDangKy);
            this.uiGroupBox3.Controls.Add(this.clcThoiHanTaiNhapTaiXuat);
            this.uiGroupBox3.Controls.Add(this.ctrMaPhuongThucVT);
            this.uiGroupBox3.Controls.Add(this.ctrMaPhanLoaiHH);
            this.uiGroupBox3.Controls.Add(this.ctrPhanLoaiToChuc);
            this.uiGroupBox3.Controls.Add(this.ctrMaLoaiHinh);
            this.uiGroupBox3.Controls.Add(this.txtTongSoTKChiaNho);
            this.uiGroupBox3.Controls.Add(this.txtSoNhanhToKhai);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhaiTNTX);
            this.uiGroupBox3.Controls.Add(this.label61);
            this.uiGroupBox3.Controls.Add(this.label41);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.label28);
            this.uiGroupBox3.Controls.Add(this.label30);
            this.uiGroupBox3.Controls.Add(this.label29);
            this.uiGroupBox3.Controls.Add(this.label27);
            this.uiGroupBox3.Controls.Add(this.label23);
            this.uiGroupBox3.Controls.Add(this.label69);
            this.uiGroupBox3.Controls.Add(this.label26);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Controls.Add(this.label25);
            this.uiGroupBox3.Controls.Add(this.label24);
            this.uiGroupBox3.Controls.Add(this.lblPhanLuong);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Location = new System.Drawing.Point(6, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(928, 126);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayNhapKhoDauTien
            // 
            this.clcNgayNhapKhoDauTien.Location = new System.Drawing.Point(490, 64);
            this.clcNgayNhapKhoDauTien.Name = "clcNgayNhapKhoDauTien";
            this.clcNgayNhapKhoDauTien.ReadOnly = false;
            this.clcNgayNhapKhoDauTien.Size = new System.Drawing.Size(88, 21);
            this.clcNgayNhapKhoDauTien.TabIndex = 4;
            this.clcNgayNhapKhoDauTien.TagName = "";
            this.clcNgayNhapKhoDauTien.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayNhapKhoDauTien.WhereCondition = "";
            this.clcNgayNhapKhoDauTien.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // btnMaHQ
            // 
            this.btnMaHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaHQ.ImageIndex = 4;
            this.btnMaHQ.Location = new System.Drawing.Point(564, 36);
            this.btnMaHQ.Name = "btnMaHQ";
            this.btnMaHQ.Size = new System.Drawing.Size(21, 21);
            this.btnMaHQ.TabIndex = 18;
            this.btnMaHQ.Text = "...";
            this.btnMaHQ.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnMaHQ.Click += new System.EventHandler(this.btnMaHQ_Click);
            // 
            // btnMaLoaiHinh
            // 
            this.btnMaLoaiHinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaLoaiHinh.ImageIndex = 4;
            this.btnMaLoaiHinh.Location = new System.Drawing.Point(343, 63);
            this.btnMaLoaiHinh.Name = "btnMaLoaiHinh";
            this.btnMaLoaiHinh.Size = new System.Drawing.Size(21, 21);
            this.btnMaLoaiHinh.TabIndex = 18;
            this.btnMaLoaiHinh.Text = "...";
            this.btnMaLoaiHinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnMaLoaiHinh.Click += new System.EventHandler(this.btnMaLoaiHinh_Click);
            // 
            // txtSoToKhaiDauTien
            // 
            this.txtSoToKhaiDauTien.Location = new System.Drawing.Point(241, 9);
            this.txtSoToKhaiDauTien.Name = "txtSoToKhaiDauTien";
            this.txtSoToKhaiDauTien.Size = new System.Drawing.Size(90, 21);
            this.txtSoToKhaiDauTien.TabIndex = 1;
            this.txtSoToKhaiDauTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoToKhaiDauTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrNhomXuLyHS
            // 
            this.ctrNhomXuLyHS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ctrNhomXuLyHS.Code = "";
            this.ctrNhomXuLyHS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNhomXuLyHS.CustomsCode = null;
            this.ctrNhomXuLyHS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNhomXuLyHS.IsOnlyWarning = true;
            this.ctrNhomXuLyHS.IsValidate = true;
            this.ctrNhomXuLyHS.Location = new System.Drawing.Point(695, 36);
            this.ctrNhomXuLyHS.Name = "ctrNhomXuLyHS";
            this.ctrNhomXuLyHS.Name_VN = "";
            this.ctrNhomXuLyHS.SetValidate = false;
            this.ctrNhomXuLyHS.ShowColumnCode = true;
            this.ctrNhomXuLyHS.ShowColumnName = false;
            this.ctrNhomXuLyHS.Size = new System.Drawing.Size(165, 21);
            this.ctrNhomXuLyHS.TabIndex = 8;
            this.ctrNhomXuLyHS.TagName = "";
            this.ctrNhomXuLyHS.WhereCondition = "";
            // 
            // ctrCoQuanHaiQuan
            // 
            this.ctrCoQuanHaiQuan.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrCoQuanHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHaiQuan.Code = "";
            this.ctrCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHaiQuan.IsOnlyWarning = false;
            this.ctrCoQuanHaiQuan.IsValidate = true;
            this.ctrCoQuanHaiQuan.Location = new System.Drawing.Point(289, 36);
            this.ctrCoQuanHaiQuan.Name = "ctrCoQuanHaiQuan";
            this.ctrCoQuanHaiQuan.Name_VN = "";
            this.ctrCoQuanHaiQuan.SetOnlyWarning = false;
            this.ctrCoQuanHaiQuan.SetValidate = false;
            this.ctrCoQuanHaiQuan.ShowColumnCode = true;
            this.ctrCoQuanHaiQuan.ShowColumnName = true;
            this.ctrCoQuanHaiQuan.Size = new System.Drawing.Size(282, 21);
            this.ctrCoQuanHaiQuan.TabIndex = 7;
            this.ctrCoQuanHaiQuan.TagCode = "";
            this.ctrCoQuanHaiQuan.TagName = "";
            this.ctrCoQuanHaiQuan.Where = null;
            this.ctrCoQuanHaiQuan.WhereCondition = "";
            this.ctrCoQuanHaiQuan.Leave += new System.EventHandler(this.ctrCoQuanHaiQuan_Leave);
            // 
            // clcNgayDangKy
            // 
            this.clcNgayDangKy.Location = new System.Drawing.Point(519, 9);
            this.clcNgayDangKy.Name = "clcNgayDangKy";
            this.clcNgayDangKy.ReadOnly = false;
            this.clcNgayDangKy.Size = new System.Drawing.Size(88, 21);
            this.clcNgayDangKy.TabIndex = 4;
            this.clcNgayDangKy.TagName = "";
            this.clcNgayDangKy.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDangKy.WhereCondition = "";
            this.clcNgayDangKy.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // clcThoiHanTaiNhapTaiXuat
            // 
            this.clcThoiHanTaiNhapTaiXuat.Location = new System.Drawing.Point(325, 94);
            this.clcThoiHanTaiNhapTaiXuat.Name = "clcThoiHanTaiNhapTaiXuat";
            this.clcThoiHanTaiNhapTaiXuat.ReadOnly = false;
            this.clcThoiHanTaiNhapTaiXuat.Size = new System.Drawing.Size(89, 21);
            this.clcThoiHanTaiNhapTaiXuat.TabIndex = 12;
            this.clcThoiHanTaiNhapTaiXuat.TagName = "";
            this.clcThoiHanTaiNhapTaiXuat.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcThoiHanTaiNhapTaiXuat.WhereCondition = "";
            this.clcThoiHanTaiNhapTaiXuat.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // ctrMaPhuongThucVT
            // 
            this.ctrMaPhuongThucVT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaPhuongThucVT.Appearance.Options.UseBackColor = true;
            this.ctrMaPhuongThucVT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E005;
            this.ctrMaPhuongThucVT.Code = "";
            this.ctrMaPhuongThucVT.ColorControl = System.Drawing.SystemColors.Info;
            this.ctrMaPhuongThucVT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhuongThucVT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhuongThucVT.IsOnlyWarning = false;
            this.ctrMaPhuongThucVT.IsValidate = true;
            this.ctrMaPhuongThucVT.Location = new System.Drawing.Point(597, 93);
            this.ctrMaPhuongThucVT.Name = "ctrMaPhuongThucVT";
            this.ctrMaPhuongThucVT.Name_VN = "";
            this.ctrMaPhuongThucVT.SetOnlyWarning = false;
            this.ctrMaPhuongThucVT.SetValidate = false;
            this.ctrMaPhuongThucVT.ShowColumnCode = true;
            this.ctrMaPhuongThucVT.ShowColumnName = true;
            this.ctrMaPhuongThucVT.Size = new System.Drawing.Size(264, 21);
            this.ctrMaPhuongThucVT.TabIndex = 13;
            this.ctrMaPhuongThucVT.TagName = "";
            this.ctrMaPhuongThucVT.Where = null;
            this.ctrMaPhuongThucVT.WhereCondition = "";
            this.ctrMaPhuongThucVT.Leave += new System.EventHandler(this.ctrMaPhuongThucVT_Leave);
            // 
            // ctrMaPhanLoaiHH
            // 
            this.ctrMaPhanLoaiHH.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaPhanLoaiHH.Appearance.Options.UseBackColor = true;
            this.ctrMaPhanLoaiHH.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E003;
            this.ctrMaPhanLoaiHH.Code = "";
            this.ctrMaPhanLoaiHH.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaPhanLoaiHH.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiHH.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiHH.IsOnlyWarning = false;
            this.ctrMaPhanLoaiHH.IsValidate = true;
            this.ctrMaPhanLoaiHH.Location = new System.Drawing.Point(695, 64);
            this.ctrMaPhanLoaiHH.Name = "ctrMaPhanLoaiHH";
            this.ctrMaPhanLoaiHH.Name_VN = "";
            this.ctrMaPhanLoaiHH.SetOnlyWarning = false;
            this.ctrMaPhanLoaiHH.SetValidate = false;
            this.ctrMaPhanLoaiHH.ShowColumnCode = true;
            this.ctrMaPhanLoaiHH.ShowColumnName = true;
            this.ctrMaPhanLoaiHH.Size = new System.Drawing.Size(164, 21);
            this.ctrMaPhanLoaiHH.TabIndex = 10;
            this.ctrMaPhanLoaiHH.TagName = "";
            this.ctrMaPhanLoaiHH.Where = null;
            this.ctrMaPhanLoaiHH.WhereCondition = "";
            // 
            // ctrPhanLoaiToChuc
            // 
            this.ctrPhanLoaiToChuc.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrPhanLoaiToChuc.Appearance.Options.UseBackColor = true;
            this.ctrPhanLoaiToChuc.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E004;
            this.ctrPhanLoaiToChuc.Code = "";
            this.ctrPhanLoaiToChuc.ColorControl = System.Drawing.Color.Empty;
            this.ctrPhanLoaiToChuc.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPhanLoaiToChuc.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrPhanLoaiToChuc.IsOnlyWarning = false;
            this.ctrPhanLoaiToChuc.IsValidate = true;
            this.ctrPhanLoaiToChuc.Location = new System.Drawing.Point(759, 10);
            this.ctrPhanLoaiToChuc.Name = "ctrPhanLoaiToChuc";
            this.ctrPhanLoaiToChuc.Name_VN = "";
            this.ctrPhanLoaiToChuc.SetOnlyWarning = false;
            this.ctrPhanLoaiToChuc.SetValidate = false;
            this.ctrPhanLoaiToChuc.ShowColumnCode = true;
            this.ctrPhanLoaiToChuc.ShowColumnName = false;
            this.ctrPhanLoaiToChuc.Size = new System.Drawing.Size(102, 21);
            this.ctrPhanLoaiToChuc.TabIndex = 5;
            this.ctrPhanLoaiToChuc.TagName = "";
            this.ctrPhanLoaiToChuc.Where = null;
            this.ctrPhanLoaiToChuc.WhereCondition = "";
            // 
            // ctrMaLoaiHinh
            // 
            this.ctrMaLoaiHinh.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaLoaiHinh.Appearance.Options.UseBackColor = true;
            this.ctrMaLoaiHinh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ctrMaLoaiHinh.Code = "";
            this.ctrMaLoaiHinh.ColorControl = System.Drawing.SystemColors.Info;
            this.ctrMaLoaiHinh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaLoaiHinh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaLoaiHinh.IsOnlyWarning = false;
            this.ctrMaLoaiHinh.IsValidate = true;
            this.ctrMaLoaiHinh.Location = new System.Drawing.Point(73, 64);
            this.ctrMaLoaiHinh.Name = "ctrMaLoaiHinh";
            this.ctrMaLoaiHinh.Name_VN = "";
            this.ctrMaLoaiHinh.SetOnlyWarning = false;
            this.ctrMaLoaiHinh.SetValidate = false;
            this.ctrMaLoaiHinh.ShowColumnCode = true;
            this.ctrMaLoaiHinh.ShowColumnName = true;
            this.ctrMaLoaiHinh.Size = new System.Drawing.Size(271, 21);
            this.ctrMaLoaiHinh.TabIndex = 9;
            this.ctrMaLoaiHinh.TagName = "";
            this.ctrMaLoaiHinh.Where = null;
            this.ctrMaLoaiHinh.WhereCondition = "";
            // 
            // txtTongSoTKChiaNho
            // 
            this.txtTongSoTKChiaNho.DecimalDigits = 20;
            this.txtTongSoTKChiaNho.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTongSoTKChiaNho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTKChiaNho.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongSoTKChiaNho.Location = new System.Drawing.Point(371, 9);
            this.txtTongSoTKChiaNho.MaxLength = 15;
            this.txtTongSoTKChiaNho.Name = "txtTongSoTKChiaNho";
            this.txtTongSoTKChiaNho.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtTongSoTKChiaNho.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTKChiaNho.Size = new System.Drawing.Size(24, 21);
            this.txtTongSoTKChiaNho.TabIndex = 3;
            this.txtTongSoTKChiaNho.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTKChiaNho.Value = null;
            this.txtTongSoTKChiaNho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoNhanhToKhai
            // 
            this.txtSoNhanhToKhai.DecimalDigits = 20;
            this.txtSoNhanhToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoNhanhToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoNhanhToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoNhanhToKhai.Location = new System.Drawing.Point(337, 9);
            this.txtSoNhanhToKhai.MaxLength = 15;
            this.txtSoNhanhToKhai.Name = "txtSoNhanhToKhai";
            this.txtSoNhanhToKhai.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtSoNhanhToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoNhanhToKhai.Size = new System.Drawing.Size(24, 21);
            this.txtSoNhanhToKhai.TabIndex = 2;
            this.txtSoNhanhToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoNhanhToKhai.Value = null;
            this.txtSoNhanhToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 12;
            this.txtSoToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhai.Location = new System.Drawing.Point(72, 9);
            this.txtSoToKhai.MaxLength = 15;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhai.Size = new System.Drawing.Size(76, 21);
            this.txtSoToKhai.TabIndex = 0;
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.Value = null;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoToKhaiTNTX
            // 
            this.txtSoToKhaiTNTX.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoToKhaiTNTX.DecimalDigits = 12;
            this.txtSoToKhaiTNTX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhaiTNTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiTNTX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhaiTNTX.Location = new System.Drawing.Point(148, 94);
            this.txtSoToKhaiTNTX.MaxLength = 15;
            this.txtSoToKhaiTNTX.Name = "txtSoToKhaiTNTX";
            this.txtSoToKhaiTNTX.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtSoToKhaiTNTX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhaiTNTX.Size = new System.Drawing.Size(89, 21);
            this.txtSoToKhaiTNTX.TabIndex = 11;
            this.txtSoToKhaiTNTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhaiTNTX.Value = null;
            this.txtSoToKhaiTNTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoToKhaiTNTX.ValueChanged += new System.EventHandler(this.txtSoToKhaiTNTX_ValueChanged);
            this.txtSoToKhaiTNTX.ButtonClick += new System.EventHandler(this.txtSoToKhaiTNTX_ButtonClick);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(5, 41);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(91, 13);
            this.label61.TabIndex = 33;
            this.label61.Text = "Phân loại kiểm tra";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(617, 14);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(134, 13);
            this.label41.TabIndex = 33;
            this.label41.Text = "Phân loại cá nhân /tổ chức";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(4, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Mã loại hình";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(426, 97);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(168, 13);
            this.label28.TabIndex = 33;
            this.label28.Text = "Mã hiệu phương thức vận chuyển";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(238, 98);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 13);
            this.label30.TabIndex = 33;
            this.label30.Text = "Thời hạn tái xuất";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(406, 13);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(113, 13);
            this.label29.TabIndex = 33;
            this.label29.Text = "Ngày dự kiến khai báo";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(194, 40);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 13);
            this.label27.TabIndex = 6;
            this.label27.Text = "Cơ quan Hải quan";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Enabled = false;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(360, 13);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 33;
            this.label23.Text = "/";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(369, 68);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(121, 13);
            this.label69.TabIndex = 33;
            this.label69.Text = "Ngày nhập kho đầu tiên";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(591, 40);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 13);
            this.label26.TabIndex = 33;
            this.label26.Text = "Mã bộ phận xử lý";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(161, 13);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Số TK đầu tiên";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(578, 67);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(115, 13);
            this.label25.TabIndex = 33;
            this.label25.Text = "Mã phân loại hàng hóa";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(5, 98);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(142, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "Số tờ khai tạm nhập tái xuất";
            // 
            // lblPhanLuong
            // 
            this.lblPhanLuong.AutoSize = true;
            this.lblPhanLuong.BackColor = System.Drawing.Color.Transparent;
            this.lblPhanLuong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLuong.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPhanLuong.Location = new System.Drawing.Point(99, 41);
            this.lblPhanLuong.Name = "lblPhanLuong";
            this.lblPhanLuong.Size = new System.Drawing.Size(15, 14);
            this.lblPhanLuong.TabIndex = 6;
            this.lblPhanLuong.Text = "  ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Số tờ khai";
            // 
            // grbDoiTac
            // 
            this.grbDoiTac.BackColor = System.Drawing.Color.Transparent;
            this.grbDoiTac.Controls.Add(this.ctrMaNuocDoiTac);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac2);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac1);
            this.grbDoiTac.Controls.Add(this.txtMaDoiTac);
            this.grbDoiTac.Controls.Add(this.label6);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac4);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac3);
            this.grbDoiTac.Controls.Add(this.label15);
            this.grbDoiTac.Controls.Add(this.label14);
            this.grbDoiTac.Controls.Add(this.label13);
            this.grbDoiTac.Controls.Add(this.txtTenDaiLyHaiQuan);
            this.grbDoiTac.Controls.Add(this.txtMaDaiLyHQ);
            this.grbDoiTac.Controls.Add(this.label64);
            this.grbDoiTac.Controls.Add(this.txtMaBuuChinhDoiTac);
            this.grbDoiTac.Controls.Add(this.label17);
            this.grbDoiTac.Controls.Add(this.label16);
            this.grbDoiTac.Controls.Add(this.txtTenDoiTac);
            this.grbDoiTac.Controls.Add(this.label8);
            this.grbDoiTac.Controls.Add(this.label9);
            this.grbDoiTac.Controls.Add(this.label10);
            this.grbDoiTac.Location = new System.Drawing.Point(5, 429);
            this.grbDoiTac.Name = "grbDoiTac";
            this.grbDoiTac.Size = new System.Drawing.Size(306, 303);
            this.grbDoiTac.TabIndex = 3;
            this.grbDoiTac.Text = "Người xuất khẩu";
            this.grbDoiTac.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrMaNuocDoiTac
            // 
            this.ctrMaNuocDoiTac.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaNuocDoiTac.Appearance.Options.UseBackColor = true;
            this.ctrMaNuocDoiTac.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrMaNuocDoiTac.Code = "";
            this.ctrMaNuocDoiTac.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaNuocDoiTac.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaNuocDoiTac.IsOnlyWarning = false;
            this.ctrMaNuocDoiTac.IsValidate = true;
            this.ctrMaNuocDoiTac.Location = new System.Drawing.Point(58, 225);
            this.ctrMaNuocDoiTac.Name = "ctrMaNuocDoiTac";
            this.ctrMaNuocDoiTac.Name_VN = "";
            this.ctrMaNuocDoiTac.SetOnlyWarning = false;
            this.ctrMaNuocDoiTac.SetValidate = false;
            this.ctrMaNuocDoiTac.ShowColumnCode = true;
            this.ctrMaNuocDoiTac.ShowColumnName = true;
            this.ctrMaNuocDoiTac.Size = new System.Drawing.Size(223, 21);
            this.ctrMaNuocDoiTac.TabIndex = 7;
            this.ctrMaNuocDoiTac.TagCode = "";
            this.ctrMaNuocDoiTac.TagName = "";
            this.ctrMaNuocDoiTac.Where = null;
            this.ctrMaNuocDoiTac.WhereCondition = "";
            this.ctrMaNuocDoiTac.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(this.ctrMaNuocDoiTac_Leave);
            // 
            // txtDiaChiDoiTac2
            // 
            this.txtDiaChiDoiTac2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac2.Location = new System.Drawing.Point(58, 130);
            this.txtDiaChiDoiTac2.MaxLength = 100;
            this.txtDiaChiDoiTac2.Multiline = true;
            this.txtDiaChiDoiTac2.Name = "txtDiaChiDoiTac2";
            this.txtDiaChiDoiTac2.Size = new System.Drawing.Size(223, 40);
            this.txtDiaChiDoiTac2.TabIndex = 3;
            this.txtDiaChiDoiTac2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiDoiTac2.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // txtDiaChiDoiTac1
            // 
            this.txtDiaChiDoiTac1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac1.BackColor = System.Drawing.SystemColors.Info;
            this.txtDiaChiDoiTac1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDiaChiDoiTac1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac1.Location = new System.Drawing.Point(58, 87);
            this.txtDiaChiDoiTac1.MaxLength = 100;
            this.txtDiaChiDoiTac1.Multiline = true;
            this.txtDiaChiDoiTac1.Name = "txtDiaChiDoiTac1";
            this.txtDiaChiDoiTac1.Size = new System.Drawing.Size(223, 40);
            this.txtDiaChiDoiTac1.TabIndex = 2;
            this.txtDiaChiDoiTac1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiDoiTac1.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // txtMaDoiTac
            // 
            this.txtMaDoiTac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDoiTac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDoiTac.ButtonText = "...";
            this.txtMaDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoiTac.Location = new System.Drawing.Point(58, 17);
            this.txtMaDoiTac.MaxLength = 13;
            this.txtMaDoiTac.Name = "txtMaDoiTac";
            this.txtMaDoiTac.Size = new System.Drawing.Size(223, 21);
            this.txtMaDoiTac.TabIndex = 0;
            this.txtMaDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Địa chỉ (1)";
            // 
            // txtDiaChiDoiTac4
            // 
            this.txtDiaChiDoiTac4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac4.Location = new System.Drawing.Point(208, 198);
            this.txtDiaChiDoiTac4.MaxLength = 255;
            this.txtDiaChiDoiTac4.Name = "txtDiaChiDoiTac4";
            this.txtDiaChiDoiTac4.Size = new System.Drawing.Size(73, 21);
            this.txtDiaChiDoiTac4.TabIndex = 6;
            this.txtDiaChiDoiTac4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiDoiTac4.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // txtDiaChiDoiTac3
            // 
            this.txtDiaChiDoiTac3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac3.Location = new System.Drawing.Point(58, 198);
            this.txtDiaChiDoiTac3.MaxLength = 255;
            this.txtDiaChiDoiTac3.Name = "txtDiaChiDoiTac3";
            this.txtDiaChiDoiTac3.Size = new System.Drawing.Size(105, 21);
            this.txtDiaChiDoiTac3.TabIndex = 5;
            this.txtDiaChiDoiTac3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiDoiTac3.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(162, 202);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Quốc gia";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1, 201);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Tỉnh/Thành";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1, 133);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Địa chỉ (2)";
            // 
            // txtTenDaiLyHaiQuan
            // 
            this.txtTenDaiLyHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLyHaiQuan.Location = new System.Drawing.Point(68, 275);
            this.txtTenDaiLyHaiQuan.MaxLength = 255;
            this.txtTenDaiLyHaiQuan.Name = "txtTenDaiLyHaiQuan";
            this.txtTenDaiLyHaiQuan.ReadOnly = true;
            this.txtTenDaiLyHaiQuan.Size = new System.Drawing.Size(213, 21);
            this.txtTenDaiLyHaiQuan.TabIndex = 9;
            this.txtTenDaiLyHaiQuan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDaiLyHaiQuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDaiLyHQ
            // 
            this.txtMaDaiLyHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLyHQ.Location = new System.Drawing.Point(68, 250);
            this.txtMaDaiLyHQ.MaxLength = 255;
            this.txtMaDaiLyHQ.Name = "txtMaDaiLyHQ";
            this.txtMaDaiLyHQ.Size = new System.Drawing.Size(95, 21);
            this.txtMaDaiLyHQ.TabIndex = 8;
            this.txtMaDaiLyHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLyHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(-2, 279);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(71, 13);
            this.label64.TabIndex = 32;
            this.label64.Text = "Tên đại lý HQ";
            // 
            // txtMaBuuChinhDoiTac
            // 
            this.txtMaBuuChinhDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDoiTac.Location = new System.Drawing.Point(58, 173);
            this.txtMaBuuChinhDoiTac.MaxLength = 255;
            this.txtMaBuuChinhDoiTac.Name = "txtMaBuuChinhDoiTac";
            this.txtMaBuuChinhDoiTac.Size = new System.Drawing.Size(79, 21);
            this.txtMaBuuChinhDoiTac.TabIndex = 4;
            this.txtMaBuuChinhDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaBuuChinhDoiTac.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(-2, 254);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "Mã đại lý HQ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1, 229);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Mã nước";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.BackColor = System.Drawing.SystemColors.Info;
            this.txtTenDoiTac.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtTenDoiTac.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(58, 43);
            this.txtTenDoiTac.MaxLength = 100;
            this.txtTenDoiTac.Multiline = true;
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(223, 40);
            this.txtTenDoiTac.TabIndex = 1;
            this.txtTenDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDoiTac.ButtonClick += new System.EventHandler(this.txtTenDoiTac_ButtonClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Bưu chính";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Mã";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Tên";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.tabThongtin);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1070, 743);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // tabThongtin
            // 
            this.tabThongtin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabThongtin.Location = new System.Drawing.Point(0, 0);
            this.tabThongtin.Name = "tabThongtin";
            this.tabThongtin.Size = new System.Drawing.Size(1070, 743);
            this.tabThongtin.TabIndex = 14;
            this.tabThongtin.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.TabItemThongtin,
            this.TabItemPhanHoi});
            this.tabThongtin.VisualStyleManager = this.vsmMain;
            // 
            // TabItemThongtin
            // 
            this.TabItemThongtin.Controls.Add(this.uiGroupBox4);
            this.TabItemThongtin.Location = new System.Drawing.Point(1, 21);
            this.TabItemThongtin.Name = "TabItemThongtin";
            this.TabItemThongtin.Size = new System.Drawing.Size(1068, 721);
            this.TabItemThongtin.TabStop = true;
            this.TabItemThongtin.Text = "Thông tin khai báo";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.uiGroupBox18);
            this.uiGroupBox4.Controls.Add(this.rdbHangMau);
            this.uiGroupBox4.Controls.Add(this.rdbSanPham);
            this.uiGroupBox4.Controls.Add(this.rdbThietBi);
            this.uiGroupBox4.Controls.Add(this.rdbNPL);
            this.uiGroupBox4.Controls.Add(this.txtSoHD);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox14);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox4.Controls.Add(this.grbDonVi);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox12);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox13);
            this.uiGroupBox4.Controls.Add(this.grbDoiTac);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox10);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox4.Controls.Add(this.uiGroupBox11);
            this.uiGroupBox4.Controls.Add(this.lblNgayHD1);
            this.uiGroupBox4.Controls.Add(this.lblNgayHD);
            this.uiGroupBox4.Controls.Add(this.lblSoHD);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1068, 721);
            this.uiGroupBox4.TabIndex = 14;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox18.Controls.Add(this.txtUyThacXK);
            this.uiGroupBox18.Controls.Add(this.label71);
            this.uiGroupBox18.Location = new System.Drawing.Point(6, 374);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(306, 55);
            this.uiGroupBox18.TabIndex = 34;
            this.uiGroupBox18.Text = "Người ủy thác xuất khẩu";
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox18.VisualStyleManager = this.vsmMain;
            // 
            // txtUyThacXK
            // 
            this.txtUyThacXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUyThacXK.Location = new System.Drawing.Point(53, 16);
            this.txtUyThacXK.MaxLength = 100;
            this.txtUyThacXK.Multiline = true;
            this.txtUyThacXK.Name = "txtUyThacXK";
            this.txtUyThacXK.Size = new System.Drawing.Size(228, 33);
            this.txtUyThacXK.TabIndex = 1;
            this.txtUyThacXK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtUyThacXK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.BackColor = System.Drawing.Color.Transparent;
            this.label71.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(8, 27);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(25, 13);
            this.label71.TabIndex = 32;
            this.label71.Text = "Tên";
            // 
            // rdbHangMau
            // 
            this.rdbHangMau.Location = new System.Drawing.Point(718, 677);
            this.rdbHangMau.Name = "rdbHangMau";
            this.rdbHangMau.Size = new System.Drawing.Size(104, 23);
            this.rdbHangMau.TabIndex = 14;
            this.rdbHangMau.Text = "Hàng mẫu";
            this.rdbHangMau.Visible = false;
            this.rdbHangMau.VisualStyleManager = this.vsmMain;
            // 
            // rdbSanPham
            // 
            this.rdbSanPham.Location = new System.Drawing.Point(718, 617);
            this.rdbSanPham.Name = "rdbSanPham";
            this.rdbSanPham.Size = new System.Drawing.Size(104, 23);
            this.rdbSanPham.TabIndex = 14;
            this.rdbSanPham.Text = "Sản phẩm";
            this.rdbSanPham.Visible = false;
            this.rdbSanPham.VisualStyleManager = this.vsmMain;
            // 
            // rdbThietBi
            // 
            this.rdbThietBi.Location = new System.Drawing.Point(718, 657);
            this.rdbThietBi.Name = "rdbThietBi";
            this.rdbThietBi.Size = new System.Drawing.Size(104, 23);
            this.rdbThietBi.TabIndex = 14;
            this.rdbThietBi.Text = "Thiết bị";
            this.rdbThietBi.Visible = false;
            this.rdbThietBi.VisualStyleManager = this.vsmMain;
            // 
            // rdbNPL
            // 
            this.rdbNPL.Checked = true;
            this.rdbNPL.Location = new System.Drawing.Point(718, 637);
            this.rdbNPL.Name = "rdbNPL";
            this.rdbNPL.Size = new System.Drawing.Size(104, 23);
            this.rdbNPL.TabIndex = 14;
            this.rdbNPL.TabStop = true;
            this.rdbNPL.Text = "Nguyên phụ liệu";
            this.rdbNPL.Visible = false;
            this.rdbNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHD
            // 
            this.txtSoHD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoHD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoHD.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoHD.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(823, 638);
            this.txtSoHD.MaxLength = 12;
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(96, 21);
            this.txtSoHD.TabIndex = 4;
            this.txtSoHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHD.ButtonClick += new System.EventHandler(this.txtSoHD_ButtonClick);
            // 
            // uiGroupBox14
            // 
            this.uiGroupBox14.Controls.Add(this.clcNgayDen);
            this.uiGroupBox14.Controls.Add(this.ctrDiaDiemDichVC);
            this.uiGroupBox14.Controls.Add(this.clcNgayKhoiHanhVC);
            this.uiGroupBox14.Controls.Add(this.label59);
            this.uiGroupBox14.Controls.Add(this.label63);
            this.uiGroupBox14.Controls.Add(this.label58);
            this.uiGroupBox14.Location = new System.Drawing.Point(316, 485);
            this.uiGroupBox14.Name = "uiGroupBox14";
            this.uiGroupBox14.Size = new System.Drawing.Size(391, 65);
            this.uiGroupBox14.TabIndex = 8;
            this.uiGroupBox14.Text = "Đề nghị chuyển cửa khẩu";
            this.uiGroupBox14.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayDen
            // 
            this.clcNgayDen.Location = new System.Drawing.Point(269, 15);
            this.clcNgayDen.Name = "clcNgayDen";
            this.clcNgayDen.ReadOnly = false;
            this.clcNgayDen.Size = new System.Drawing.Size(89, 21);
            this.clcNgayDen.TabIndex = 1;
            this.clcNgayDen.TagName = "";
            this.clcNgayDen.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayDen.WhereCondition = "";
            this.clcNgayDen.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // ctrDiaDiemDichVC
            // 
            this.ctrDiaDiemDichVC.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrDiaDiemDichVC.Appearance.Options.UseBackColor = true;
            this.ctrDiaDiemDichVC.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrDiaDiemDichVC.Code = "";
            this.ctrDiaDiemDichVC.ColorControl = System.Drawing.Color.Empty;
            this.ctrDiaDiemDichVC.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemDichVC.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemDichVC.IsOnlyWarning = false;
            this.ctrDiaDiemDichVC.IsValidate = true;
            this.ctrDiaDiemDichVC.Location = new System.Drawing.Point(206, 41);
            this.ctrDiaDiemDichVC.Name = "ctrDiaDiemDichVC";
            this.ctrDiaDiemDichVC.Name_VN = "";
            this.ctrDiaDiemDichVC.SetOnlyWarning = false;
            this.ctrDiaDiemDichVC.SetValidate = false;
            this.ctrDiaDiemDichVC.ShowColumnCode = true;
            this.ctrDiaDiemDichVC.ShowColumnName = false;
            this.ctrDiaDiemDichVC.Size = new System.Drawing.Size(152, 21);
            this.ctrDiaDiemDichVC.TabIndex = 2;
            this.ctrDiaDiemDichVC.TagName = "";
            this.ctrDiaDiemDichVC.Where = null;
            this.ctrDiaDiemDichVC.WhereCondition = "";
            // 
            // clcNgayKhoiHanhVC
            // 
            this.clcNgayKhoiHanhVC.Location = new System.Drawing.Point(101, 15);
            this.clcNgayKhoiHanhVC.Name = "clcNgayKhoiHanhVC";
            this.clcNgayKhoiHanhVC.ReadOnly = false;
            this.clcNgayKhoiHanhVC.Size = new System.Drawing.Size(89, 21);
            this.clcNgayKhoiHanhVC.TabIndex = 0;
            this.clcNgayKhoiHanhVC.TagName = "";
            this.clcNgayKhoiHanhVC.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayKhoiHanhVC.WhereCondition = "";
            this.clcNgayKhoiHanhVC.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(6, 46);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(195, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "Địa điểm đích cho vận chuyển bảo thuế";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(216, 19);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(53, 13);
            this.label63.TabIndex = 35;
            this.label63.Text = "Ngày đến";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(6, 18);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(97, 13);
            this.label58.TabIndex = 35;
            this.label58.Text = "Ngày khởi hành VC";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.Controls.Add(this.ctrMaNHBaoLanh);
            this.uiGroupBox8.Controls.Add(this.ctrMaXDThoiHanNopThue);
            this.uiGroupBox8.Controls.Add(this.txtKyHieuCTBaoLanh);
            this.uiGroupBox8.Controls.Add(this.label34);
            this.uiGroupBox8.Controls.Add(this.txtSoCTBaoLanh);
            this.uiGroupBox8.Controls.Add(this.txtNamPhatHanhBL);
            this.uiGroupBox8.Controls.Add(this.label35);
            this.uiGroupBox8.Controls.Add(this.label36);
            this.uiGroupBox8.Controls.Add(this.label37);
            this.uiGroupBox8.Controls.Add(this.label38);
            this.uiGroupBox8.Location = new System.Drawing.Point(712, 446);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(222, 165);
            this.uiGroupBox8.TabIndex = 11;
            this.uiGroupBox8.Text = "Bảo lảnh thuế";
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaNHBaoLanh
            // 
            this.ctrMaNHBaoLanh.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaNHBaoLanh.Appearance.Options.UseBackColor = true;
            this.ctrMaNHBaoLanh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E035;
            this.ctrMaNHBaoLanh.Code = "";
            this.ctrMaNHBaoLanh.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaNHBaoLanh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaNHBaoLanh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaNHBaoLanh.IsOnlyWarning = false;
            this.ctrMaNHBaoLanh.IsValidate = true;
            this.ctrMaNHBaoLanh.Location = new System.Drawing.Point(92, 43);
            this.ctrMaNHBaoLanh.Name = "ctrMaNHBaoLanh";
            this.ctrMaNHBaoLanh.Name_VN = "";
            this.ctrMaNHBaoLanh.SetOnlyWarning = false;
            this.ctrMaNHBaoLanh.SetValidate = false;
            this.ctrMaNHBaoLanh.ShowColumnCode = true;
            this.ctrMaNHBaoLanh.ShowColumnName = false;
            this.ctrMaNHBaoLanh.Size = new System.Drawing.Size(103, 21);
            this.ctrMaNHBaoLanh.TabIndex = 1;
            this.ctrMaNHBaoLanh.TagName = "";
            this.ctrMaNHBaoLanh.Where = null;
            this.ctrMaNHBaoLanh.WhereCondition = "";
            // 
            // ctrMaXDThoiHanNopThue
            // 
            this.ctrMaXDThoiHanNopThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaXDThoiHanNopThue.Appearance.Options.UseBackColor = true;
            this.ctrMaXDThoiHanNopThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E019;
            this.ctrMaXDThoiHanNopThue.Code = "";
            this.ctrMaXDThoiHanNopThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaXDThoiHanNopThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaXDThoiHanNopThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaXDThoiHanNopThue.IsOnlyWarning = false;
            this.ctrMaXDThoiHanNopThue.IsValidate = true;
            this.ctrMaXDThoiHanNopThue.Location = new System.Drawing.Point(92, 15);
            this.ctrMaXDThoiHanNopThue.Name = "ctrMaXDThoiHanNopThue";
            this.ctrMaXDThoiHanNopThue.Name_VN = "";
            this.ctrMaXDThoiHanNopThue.SetOnlyWarning = false;
            this.ctrMaXDThoiHanNopThue.SetValidate = false;
            this.ctrMaXDThoiHanNopThue.ShowColumnCode = true;
            this.ctrMaXDThoiHanNopThue.ShowColumnName = false;
            this.ctrMaXDThoiHanNopThue.Size = new System.Drawing.Size(104, 21);
            this.ctrMaXDThoiHanNopThue.TabIndex = 0;
            this.ctrMaXDThoiHanNopThue.TagName = "";
            this.ctrMaXDThoiHanNopThue.Where = null;
            this.ctrMaXDThoiHanNopThue.WhereCondition = "";
            // 
            // txtKyHieuCTBaoLanh
            // 
            this.txtKyHieuCTBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKyHieuCTBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKyHieuCTBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuCTBaoLanh.Location = new System.Drawing.Point(92, 100);
            this.txtKyHieuCTBaoLanh.MaxLength = 12;
            this.txtKyHieuCTBaoLanh.Name = "txtKyHieuCTBaoLanh";
            this.txtKyHieuCTBaoLanh.Size = new System.Drawing.Size(104, 21);
            this.txtKyHieuCTBaoLanh.TabIndex = 3;
            this.txtKyHieuCTBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKyHieuCTBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(-1, 139);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 13);
            this.label34.TabIndex = 33;
            this.label34.Text = "Số chứng từ";
            // 
            // txtSoCTBaoLanh
            // 
            this.txtSoCTBaoLanh.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoCTBaoLanh.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoCTBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCTBaoLanh.Location = new System.Drawing.Point(92, 131);
            this.txtSoCTBaoLanh.MaxLength = 12;
            this.txtSoCTBaoLanh.Name = "txtSoCTBaoLanh";
            this.txtSoCTBaoLanh.Size = new System.Drawing.Size(104, 21);
            this.txtSoCTBaoLanh.TabIndex = 4;
            this.txtSoCTBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCTBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNamPhatHanhBL
            // 
            this.txtNamPhatHanhBL.DecimalDigits = 12;
            this.txtNamPhatHanhBL.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNamPhatHanhBL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamPhatHanhBL.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNamPhatHanhBL.Location = new System.Drawing.Point(92, 69);
            this.txtNamPhatHanhBL.MaxLength = 15;
            this.txtNamPhatHanhBL.Name = "txtNamPhatHanhBL";
            this.txtNamPhatHanhBL.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtNamPhatHanhBL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNamPhatHanhBL.Size = new System.Drawing.Size(104, 21);
            this.txtNamPhatHanhBL.TabIndex = 2;
            this.txtNamPhatHanhBL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNamPhatHanhBL.Value = null;
            this.txtNamPhatHanhBL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(1, 108);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(89, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "Ký hiệu chứng từ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(1, 77);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(80, 13);
            this.label36.TabIndex = 33;
            this.label36.Text = "Năm phát hành";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(1, 47);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 13);
            this.label37.TabIndex = 33;
            this.label37.Text = "Mã ngân hàng";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(1, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 13);
            this.label38.TabIndex = 33;
            this.label38.Text = "Mã XD nộp thuế";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtTenDiaDiemDoHang);
            this.uiGroupBox5.Controls.Add(this.txtTenDiaDiemXepHang);
            this.uiGroupBox5.Controls.Add(this.txtMaDiaDiemXepHang);
            this.uiGroupBox5.Controls.Add(this.txtMaDiaDiemDoHang);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemXepHang);
            this.uiGroupBox5.Controls.Add(this.ctrMaDDLuuKho);
            this.uiGroupBox5.Controls.Add(this.ctrDiaDiemDoHang);
            this.uiGroupBox5.Controls.Add(this.clcNgayHangDen);
            this.uiGroupBox5.Controls.Add(this.txtMaPTVC);
            this.uiGroupBox5.Controls.Add(this.txtSoLuongCont);
            this.uiGroupBox5.Controls.Add(this.label7);
            this.uiGroupBox5.Controls.Add(this.label55);
            this.uiGroupBox5.Controls.Add(this.label54);
            this.uiGroupBox5.Controls.Add(this.label57);
            this.uiGroupBox5.Controls.Add(this.label56);
            this.uiGroupBox5.Controls.Add(this.label53);
            this.uiGroupBox5.Controls.Add(this.label52);
            this.uiGroupBox5.Controls.Add(this.label51);
            this.uiGroupBox5.Controls.Add(this.label50);
            this.uiGroupBox5.Controls.Add(this.txtSoHieuKyHieu);
            this.uiGroupBox5.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox5.Controls.Add(this.txtTenPTVC);
            this.uiGroupBox5.Location = new System.Drawing.Point(317, 127);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(617, 119);
            this.uiGroupBox5.TabIndex = 4;
            this.uiGroupBox5.Text = "Vận đơn";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDiaDiemDoHang
            // 
            this.txtTenDiaDiemDoHang.Location = new System.Drawing.Point(430, 13);
            this.txtTenDiaDiemDoHang.Name = "txtTenDiaDiemDoHang";
            this.txtTenDiaDiemDoHang.Size = new System.Drawing.Size(138, 21);
            this.txtTenDiaDiemDoHang.TabIndex = 37;
            this.txtTenDiaDiemDoHang.Visible = false;
            this.txtTenDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDiaDiemXepHang
            // 
            this.txtTenDiaDiemXepHang.Location = new System.Drawing.Point(430, 39);
            this.txtTenDiaDiemXepHang.Name = "txtTenDiaDiemXepHang";
            this.txtTenDiaDiemXepHang.Size = new System.Drawing.Size(139, 21);
            this.txtTenDiaDiemXepHang.TabIndex = 37;
            this.txtTenDiaDiemXepHang.Visible = false;
            this.txtTenDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDiaDiemXepHang
            // 
            this.txtMaDiaDiemXepHang.Location = new System.Drawing.Point(373, 39);
            this.txtMaDiaDiemXepHang.Name = "txtMaDiaDiemXepHang";
            this.txtMaDiaDiemXepHang.Size = new System.Drawing.Size(50, 21);
            this.txtMaDiaDiemXepHang.TabIndex = 37;
            this.txtMaDiaDiemXepHang.Visible = false;
            this.txtMaDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDiaDiemDoHang
            // 
            this.txtMaDiaDiemDoHang.Location = new System.Drawing.Point(373, 13);
            this.txtMaDiaDiemDoHang.Name = "txtMaDiaDiemDoHang";
            this.txtMaDiaDiemDoHang.Size = new System.Drawing.Size(50, 21);
            this.txtMaDiaDiemDoHang.TabIndex = 37;
            this.txtMaDiaDiemDoHang.Visible = false;
            this.txtMaDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrDiaDiemXepHang
            // 
            this.ctrDiaDiemXepHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDiaDiemXepHang.Appearance.Options.UseBackColor = true;
            this.ctrDiaDiemXepHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ctrDiaDiemXepHang.Code = "";
            this.ctrDiaDiemXepHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemXepHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemXepHang.IsOnlyWarning = false;
            this.ctrDiaDiemXepHang.IsValidate = true;
            this.ctrDiaDiemXepHang.Location = new System.Drawing.Point(373, 37);
            this.ctrDiaDiemXepHang.Name = "ctrDiaDiemXepHang";
            this.ctrDiaDiemXepHang.Name_VN = "";
            this.ctrDiaDiemXepHang.SetOnlyWarning = false;
            this.ctrDiaDiemXepHang.SetValidate = false;
            this.ctrDiaDiemXepHang.ShowColumnCode = true;
            this.ctrDiaDiemXepHang.ShowColumnName = true;
            this.ctrDiaDiemXepHang.Size = new System.Drawing.Size(185, 21);
            this.ctrDiaDiemXepHang.TabIndex = 3;
            this.ctrDiaDiemXepHang.TagCode = "";
            this.ctrDiaDiemXepHang.TagName = "";
            this.ctrDiaDiemXepHang.Where = null;
            this.ctrDiaDiemXepHang.WhereCondition = "";
            // 
            // ctrMaDDLuuKho
            // 
            this.ctrMaDDLuuKho.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDDLuuKho.Appearance.Options.UseBackColor = true;
            this.ctrMaDDLuuKho.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ctrMaDDLuuKho.Code = "";
            this.ctrMaDDLuuKho.ColorControl = System.Drawing.SystemColors.Info;
            this.ctrMaDDLuuKho.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDDLuuKho.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDDLuuKho.IsOnlyWarning = false;
            this.ctrMaDDLuuKho.IsValidate = true;
            this.ctrMaDDLuuKho.Location = new System.Drawing.Point(183, 39);
            this.ctrMaDDLuuKho.Name = "ctrMaDDLuuKho";
            this.ctrMaDDLuuKho.Name_VN = "";
            this.ctrMaDDLuuKho.SetOnlyWarning = false;
            this.ctrMaDDLuuKho.SetValidate = false;
            this.ctrMaDDLuuKho.ShowColumnCode = true;
            this.ctrMaDDLuuKho.ShowColumnName = false;
            this.ctrMaDDLuuKho.Size = new System.Drawing.Size(96, 21);
            this.ctrMaDDLuuKho.TabIndex = 1;
            this.ctrMaDDLuuKho.TagName = "";
            this.ctrMaDDLuuKho.Where = null;
            this.ctrMaDDLuuKho.WhereCondition = "";
            // 
            // ctrDiaDiemDoHang
            // 
            this.ctrDiaDiemDoHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDiaDiemDoHang.Appearance.Options.UseBackColor = true;
            this.ctrDiaDiemDoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ctrDiaDiemDoHang.Code = "";
            this.ctrDiaDiemDoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemDoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemDoHang.IsOnlyWarning = false;
            this.ctrDiaDiemDoHang.IsValidate = true;
            this.ctrDiaDiemDoHang.Location = new System.Drawing.Point(373, 12);
            this.ctrDiaDiemDoHang.Name = "ctrDiaDiemDoHang";
            this.ctrDiaDiemDoHang.Name_VN = "";
            this.ctrDiaDiemDoHang.SetOnlyWarning = false;
            this.ctrDiaDiemDoHang.SetValidate = false;
            this.ctrDiaDiemDoHang.ShowColumnCode = true;
            this.ctrDiaDiemDoHang.ShowColumnName = true;
            this.ctrDiaDiemDoHang.Size = new System.Drawing.Size(185, 21);
            this.ctrDiaDiemDoHang.TabIndex = 2;
            this.ctrDiaDiemDoHang.TagCode = "";
            this.ctrDiaDiemDoHang.TagName = "";
            this.ctrDiaDiemDoHang.Where = null;
            this.ctrDiaDiemDoHang.WhereCondition = "";
            // 
            // clcNgayHangDen
            // 
            this.clcNgayHangDen.Location = new System.Drawing.Point(83, 66);
            this.clcNgayHangDen.Name = "clcNgayHangDen";
            this.clcNgayHangDen.ReadOnly = false;
            this.clcNgayHangDen.Size = new System.Drawing.Size(89, 21);
            this.clcNgayHangDen.TabIndex = 4;
            this.clcNgayHangDen.TagName = "";
            this.clcNgayHangDen.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHangDen.WhereCondition = "";
            this.clcNgayHangDen.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // txtMaPTVC
            // 
            this.txtMaPTVC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaPTVC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaPTVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPTVC.Location = new System.Drawing.Point(266, 66);
            this.txtMaPTVC.MaxLength = 12;
            this.txtMaPTVC.Name = "txtMaPTVC";
            this.txtMaPTVC.Size = new System.Drawing.Size(82, 21);
            this.txtMaPTVC.TabIndex = 5;
            this.txtMaPTVC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaPTVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaPTVC.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // txtSoLuongCont
            // 
            this.txtSoLuongCont.DecimalDigits = 12;
            this.txtSoLuongCont.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongCont.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongCont.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongCont.Location = new System.Drawing.Point(496, 93);
            this.txtSoLuongCont.MaxLength = 15;
            this.txtSoLuongCont.Name = "txtSoLuongCont";
            this.txtSoLuongCont.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongCont.Size = new System.Drawing.Size(73, 21);
            this.txtSoLuongCont.TabIndex = 8;
            this.txtSoLuongCont.Text = "0";
            this.txtSoLuongCont.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongCont.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongCont.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(408, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Số container";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(7, 97);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(71, 13);
            this.label55.TabIndex = 36;
            this.label55.Text = "Ký hiệu và số";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(7, 70);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(80, 13);
            this.label54.TabIndex = 36;
            this.label54.Text = "Ngày hàng đến";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(348, 70);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(53, 13);
            this.label57.TabIndex = 36;
            this.label57.Text = "Tên PTVC";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(172, 70);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(95, 13);
            this.label56.TabIndex = 36;
            this.label56.Text = "Mã PT vận chuyển";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(277, 43);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(96, 13);
            this.label53.TabIndex = 36;
            this.label53.Text = "Địa điểm xếp hàng";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(277, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(90, 13);
            this.label52.TabIndex = 36;
            this.label52.Text = "Địa điểm dở hàng";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(7, 43);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(179, 13);
            this.label51.TabIndex = 36;
            this.label51.Text = "Mã địa điểm lưu kho chờ thông quan";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(7, 20);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(19, 13);
            this.label50.TabIndex = 36;
            this.label50.Text = "Số";
            // 
            // txtSoHieuKyHieu
            // 
            this.txtSoHieuKyHieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuKyHieu.Location = new System.Drawing.Point(83, 93);
            this.txtSoHieuKyHieu.MaxLength = 255;
            this.txtSoHieuKyHieu.Name = "txtSoHieuKyHieu";
            this.txtSoHieuKyHieu.Size = new System.Drawing.Size(318, 21);
            this.txtSoHieuKyHieu.TabIndex = 7;
            this.txtSoHieuKyHieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuKyHieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHieuKyHieu.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon.Location = new System.Drawing.Point(51, 15);
            this.txtSoVanDon.MaxLength = 255;
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(180, 21);
            this.txtSoVanDon.TabIndex = 0;
            this.txtSoVanDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanDon.ButtonClick += new System.EventHandler(this.txtSoVanDon_ButtonClick);
            // 
            // txtTenPTVC
            // 
            this.txtTenPTVC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenPTVC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenPTVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPTVC.Location = new System.Drawing.Point(403, 66);
            this.txtTenPTVC.MaxLength = 12;
            this.txtTenPTVC.Name = "txtTenPTVC";
            this.txtTenPTVC.Size = new System.Drawing.Size(166, 21);
            this.txtTenPTVC.TabIndex = 6;
            this.txtTenPTVC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenPTVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenPTVC.ButtonClick += new System.EventHandler(this.txtTenPTVC_ButtonClick);
            this.txtTenPTVC.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.ctrMaNHTraThueThay);
            this.uiGroupBox7.Controls.Add(this.ctrMaLyDoDeNghi);
            this.uiGroupBox7.Controls.Add(this.ctrNguoiNopThue);
            this.uiGroupBox7.Controls.Add(this.txtKyHieuCTHanMuc);
            this.uiGroupBox7.Controls.Add(this.label33);
            this.uiGroupBox7.Controls.Add(this.txtNamPhatHanhHM);
            this.uiGroupBox7.Controls.Add(this.txtSoCTHanMuc);
            this.uiGroupBox7.Controls.Add(this.label32);
            this.uiGroupBox7.Controls.Add(this.label31);
            this.uiGroupBox7.Controls.Add(this.label22);
            this.uiGroupBox7.Controls.Add(this.label21);
            this.uiGroupBox7.Controls.Add(this.label60);
            this.uiGroupBox7.Location = new System.Drawing.Point(712, 250);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(222, 184);
            this.uiGroupBox7.TabIndex = 10;
            this.uiGroupBox7.Text = "Hạn mức";
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaNHTraThueThay
            // 
            this.ctrMaNHTraThueThay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaNHTraThueThay.Appearance.Options.UseBackColor = true;
            this.ctrMaNHTraThueThay.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E035;
            this.ctrMaNHTraThueThay.Code = "";
            this.ctrMaNHTraThueThay.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaNHTraThueThay.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaNHTraThueThay.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaNHTraThueThay.IsOnlyWarning = false;
            this.ctrMaNHTraThueThay.IsValidate = true;
            this.ctrMaNHTraThueThay.Location = new System.Drawing.Point(94, 70);
            this.ctrMaNHTraThueThay.Name = "ctrMaNHTraThueThay";
            this.ctrMaNHTraThueThay.Name_VN = "";
            this.ctrMaNHTraThueThay.SetOnlyWarning = false;
            this.ctrMaNHTraThueThay.SetValidate = false;
            this.ctrMaNHTraThueThay.ShowColumnCode = true;
            this.ctrMaNHTraThueThay.ShowColumnName = false;
            this.ctrMaNHTraThueThay.Size = new System.Drawing.Size(103, 21);
            this.ctrMaNHTraThueThay.TabIndex = 2;
            this.ctrMaNHTraThueThay.TagName = "";
            this.ctrMaNHTraThueThay.Where = null;
            this.ctrMaNHTraThueThay.WhereCondition = "";
            // 
            // ctrMaLyDoDeNghi
            // 
            this.ctrMaLyDoDeNghi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaLyDoDeNghi.Appearance.Options.UseBackColor = true;
            this.ctrMaLyDoDeNghi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E016;
            this.ctrMaLyDoDeNghi.Code = "";
            this.ctrMaLyDoDeNghi.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaLyDoDeNghi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaLyDoDeNghi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaLyDoDeNghi.IsOnlyWarning = false;
            this.ctrMaLyDoDeNghi.IsValidate = true;
            this.ctrMaLyDoDeNghi.Location = new System.Drawing.Point(94, 42);
            this.ctrMaLyDoDeNghi.Name = "ctrMaLyDoDeNghi";
            this.ctrMaLyDoDeNghi.Name_VN = "";
            this.ctrMaLyDoDeNghi.SetOnlyWarning = false;
            this.ctrMaLyDoDeNghi.SetValidate = false;
            this.ctrMaLyDoDeNghi.ShowColumnCode = true;
            this.ctrMaLyDoDeNghi.ShowColumnName = false;
            this.ctrMaLyDoDeNghi.Size = new System.Drawing.Size(103, 21);
            this.ctrMaLyDoDeNghi.TabIndex = 1;
            this.ctrMaLyDoDeNghi.TagName = "";
            this.ctrMaLyDoDeNghi.Where = null;
            this.ctrMaLyDoDeNghi.WhereCondition = "";
            // 
            // ctrNguoiNopThue
            // 
            this.ctrNguoiNopThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrNguoiNopThue.Appearance.Options.UseBackColor = true;
            this.ctrNguoiNopThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E015;
            this.ctrNguoiNopThue.Code = "";
            this.ctrNguoiNopThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrNguoiNopThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNguoiNopThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNguoiNopThue.IsOnlyWarning = false;
            this.ctrNguoiNopThue.IsValidate = true;
            this.ctrNguoiNopThue.Location = new System.Drawing.Point(94, 15);
            this.ctrNguoiNopThue.Name = "ctrNguoiNopThue";
            this.ctrNguoiNopThue.Name_VN = "";
            this.ctrNguoiNopThue.SetOnlyWarning = false;
            this.ctrNguoiNopThue.SetValidate = false;
            this.ctrNguoiNopThue.ShowColumnCode = true;
            this.ctrNguoiNopThue.ShowColumnName = true;
            this.ctrNguoiNopThue.Size = new System.Drawing.Size(104, 21);
            this.ctrNguoiNopThue.TabIndex = 0;
            this.ctrNguoiNopThue.TagName = "";
            this.ctrNguoiNopThue.Where = null;
            this.ctrNguoiNopThue.WhereCondition = "";
            // 
            // txtKyHieuCTHanMuc
            // 
            this.txtKyHieuCTHanMuc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKyHieuCTHanMuc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKyHieuCTHanMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKyHieuCTHanMuc.Location = new System.Drawing.Point(94, 127);
            this.txtKyHieuCTHanMuc.MaxLength = 12;
            this.txtKyHieuCTHanMuc.Name = "txtKyHieuCTHanMuc";
            this.txtKyHieuCTHanMuc.Size = new System.Drawing.Size(104, 21);
            this.txtKyHieuCTHanMuc.TabIndex = 4;
            this.txtKyHieuCTHanMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKyHieuCTHanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(1, 162);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(66, 13);
            this.label33.TabIndex = 33;
            this.label33.Text = "Số chứng từ";
            // 
            // txtNamPhatHanhHM
            // 
            this.txtNamPhatHanhHM.DecimalDigits = 12;
            this.txtNamPhatHanhHM.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtNamPhatHanhHM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamPhatHanhHM.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtNamPhatHanhHM.Location = new System.Drawing.Point(94, 98);
            this.txtNamPhatHanhHM.MaxLength = 15;
            this.txtNamPhatHanhHM.Name = "txtNamPhatHanhHM";
            this.txtNamPhatHanhHM.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtNamPhatHanhHM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNamPhatHanhHM.Size = new System.Drawing.Size(104, 21);
            this.txtNamPhatHanhHM.TabIndex = 3;
            this.txtNamPhatHanhHM.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNamPhatHanhHM.Value = null;
            this.txtNamPhatHanhHM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoCTHanMuc
            // 
            this.txtSoCTHanMuc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoCTHanMuc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoCTHanMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCTHanMuc.Location = new System.Drawing.Point(94, 156);
            this.txtSoCTHanMuc.MaxLength = 12;
            this.txtSoCTHanMuc.Name = "txtSoCTHanMuc";
            this.txtSoCTHanMuc.Size = new System.Drawing.Size(104, 21);
            this.txtSoCTHanMuc.TabIndex = 5;
            this.txtSoCTHanMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCTHanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(1, 132);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(89, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "Ký hiệu chứng từ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(1, 103);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(80, 13);
            this.label31.TabIndex = 33;
            this.label31.Text = "Năm phát hành";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(1, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 13);
            this.label22.TabIndex = 33;
            this.label22.Text = "Mã ngân hàng";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 33;
            this.label21.Text = "Người nộp thuế";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(1, 46);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(85, 13);
            this.label60.TabIndex = 33;
            this.label60.Text = "Mã lý do đề nghị";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.Controls.Add(this.ctrMaKetQuaKiemTra);
            this.uiGroupBox9.Controls.Add(this.ctrMaVanBanPhapQuy4);
            this.uiGroupBox9.Controls.Add(this.ctrMaVanBanPhapQuy5);
            this.uiGroupBox9.Controls.Add(this.ctrMaVanBanPhapQuy3);
            this.uiGroupBox9.Controls.Add(this.ctrMaVanBanPhapQuy1);
            this.uiGroupBox9.Controls.Add(this.ctrMaVanBanPhapQuy2);
            this.uiGroupBox9.Controls.Add(this.label40);
            this.uiGroupBox9.Controls.Add(this.label39);
            this.uiGroupBox9.Location = new System.Drawing.Point(317, 552);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(390, 76);
            this.uiGroupBox9.TabIndex = 9;
            this.uiGroupBox9.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaKetQuaKiemTra
            // 
            this.ctrMaKetQuaKiemTra.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaKetQuaKiemTra.Appearance.Options.UseBackColor = true;
            this.ctrMaKetQuaKiemTra.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E007;
            this.ctrMaKetQuaKiemTra.Code = "";
            this.ctrMaKetQuaKiemTra.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaKetQuaKiemTra.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaKetQuaKiemTra.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaKetQuaKiemTra.IsOnlyWarning = false;
            this.ctrMaKetQuaKiemTra.IsValidate = true;
            this.ctrMaKetQuaKiemTra.Location = new System.Drawing.Point(115, 12);
            this.ctrMaKetQuaKiemTra.Name = "ctrMaKetQuaKiemTra";
            this.ctrMaKetQuaKiemTra.Name_VN = "";
            this.ctrMaKetQuaKiemTra.SetOnlyWarning = false;
            this.ctrMaKetQuaKiemTra.SetValidate = false;
            this.ctrMaKetQuaKiemTra.ShowColumnCode = true;
            this.ctrMaKetQuaKiemTra.ShowColumnName = true;
            this.ctrMaKetQuaKiemTra.Size = new System.Drawing.Size(205, 21);
            this.ctrMaKetQuaKiemTra.TabIndex = 0;
            this.ctrMaKetQuaKiemTra.TagName = "";
            this.ctrMaKetQuaKiemTra.Where = null;
            this.ctrMaKetQuaKiemTra.WhereCondition = "";
            // 
            // ctrMaVanBanPhapQuy4
            // 
            this.ctrMaVanBanPhapQuy4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapQuy4.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapQuy4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapQuy4.Code = "";
            this.ctrMaVanBanPhapQuy4.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapQuy4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapQuy4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapQuy4.IsOnlyWarning = false;
            this.ctrMaVanBanPhapQuy4.IsValidate = true;
            this.ctrMaVanBanPhapQuy4.Location = new System.Drawing.Point(230, 48);
            this.ctrMaVanBanPhapQuy4.Name = "ctrMaVanBanPhapQuy4";
            this.ctrMaVanBanPhapQuy4.Name_VN = "";
            this.ctrMaVanBanPhapQuy4.SetOnlyWarning = false;
            this.ctrMaVanBanPhapQuy4.SetValidate = false;
            this.ctrMaVanBanPhapQuy4.ShowColumnCode = true;
            this.ctrMaVanBanPhapQuy4.ShowColumnName = false;
            this.ctrMaVanBanPhapQuy4.Size = new System.Drawing.Size(62, 21);
            this.ctrMaVanBanPhapQuy4.TabIndex = 4;
            this.ctrMaVanBanPhapQuy4.TagName = "";
            this.ctrMaVanBanPhapQuy4.Where = null;
            this.ctrMaVanBanPhapQuy4.WhereCondition = "";
            // 
            // ctrMaVanBanPhapQuy5
            // 
            this.ctrMaVanBanPhapQuy5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapQuy5.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapQuy5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapQuy5.Code = "";
            this.ctrMaVanBanPhapQuy5.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapQuy5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapQuy5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapQuy5.IsOnlyWarning = false;
            this.ctrMaVanBanPhapQuy5.IsValidate = true;
            this.ctrMaVanBanPhapQuy5.Location = new System.Drawing.Point(300, 48);
            this.ctrMaVanBanPhapQuy5.Name = "ctrMaVanBanPhapQuy5";
            this.ctrMaVanBanPhapQuy5.Name_VN = "";
            this.ctrMaVanBanPhapQuy5.SetOnlyWarning = false;
            this.ctrMaVanBanPhapQuy5.SetValidate = false;
            this.ctrMaVanBanPhapQuy5.ShowColumnCode = true;
            this.ctrMaVanBanPhapQuy5.ShowColumnName = false;
            this.ctrMaVanBanPhapQuy5.Size = new System.Drawing.Size(62, 21);
            this.ctrMaVanBanPhapQuy5.TabIndex = 5;
            this.ctrMaVanBanPhapQuy5.TagName = "";
            this.ctrMaVanBanPhapQuy5.Where = null;
            this.ctrMaVanBanPhapQuy5.WhereCondition = "";
            // 
            // ctrMaVanBanPhapQuy3
            // 
            this.ctrMaVanBanPhapQuy3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapQuy3.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapQuy3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapQuy3.Code = "";
            this.ctrMaVanBanPhapQuy3.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapQuy3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapQuy3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapQuy3.IsOnlyWarning = false;
            this.ctrMaVanBanPhapQuy3.IsValidate = true;
            this.ctrMaVanBanPhapQuy3.Location = new System.Drawing.Point(160, 48);
            this.ctrMaVanBanPhapQuy3.Name = "ctrMaVanBanPhapQuy3";
            this.ctrMaVanBanPhapQuy3.Name_VN = "";
            this.ctrMaVanBanPhapQuy3.SetOnlyWarning = false;
            this.ctrMaVanBanPhapQuy3.SetValidate = false;
            this.ctrMaVanBanPhapQuy3.ShowColumnCode = true;
            this.ctrMaVanBanPhapQuy3.ShowColumnName = false;
            this.ctrMaVanBanPhapQuy3.Size = new System.Drawing.Size(62, 21);
            this.ctrMaVanBanPhapQuy3.TabIndex = 3;
            this.ctrMaVanBanPhapQuy3.TagName = "";
            this.ctrMaVanBanPhapQuy3.Where = null;
            this.ctrMaVanBanPhapQuy3.WhereCondition = "";
            // 
            // ctrMaVanBanPhapQuy1
            // 
            this.ctrMaVanBanPhapQuy1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapQuy1.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapQuy1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapQuy1.Code = "";
            this.ctrMaVanBanPhapQuy1.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapQuy1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapQuy1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapQuy1.IsOnlyWarning = false;
            this.ctrMaVanBanPhapQuy1.IsValidate = true;
            this.ctrMaVanBanPhapQuy1.Location = new System.Drawing.Point(20, 48);
            this.ctrMaVanBanPhapQuy1.Name = "ctrMaVanBanPhapQuy1";
            this.ctrMaVanBanPhapQuy1.Name_VN = "";
            this.ctrMaVanBanPhapQuy1.SetOnlyWarning = false;
            this.ctrMaVanBanPhapQuy1.SetValidate = false;
            this.ctrMaVanBanPhapQuy1.ShowColumnCode = true;
            this.ctrMaVanBanPhapQuy1.ShowColumnName = false;
            this.ctrMaVanBanPhapQuy1.Size = new System.Drawing.Size(62, 21);
            this.ctrMaVanBanPhapQuy1.TabIndex = 1;
            this.ctrMaVanBanPhapQuy1.TagName = "";
            this.ctrMaVanBanPhapQuy1.Where = null;
            this.ctrMaVanBanPhapQuy1.WhereCondition = "";
            // 
            // ctrMaVanBanPhapQuy2
            // 
            this.ctrMaVanBanPhapQuy2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaVanBanPhapQuy2.Appearance.Options.UseBackColor = true;
            this.ctrMaVanBanPhapQuy2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A519;
            this.ctrMaVanBanPhapQuy2.Code = "";
            this.ctrMaVanBanPhapQuy2.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaVanBanPhapQuy2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaVanBanPhapQuy2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaVanBanPhapQuy2.IsOnlyWarning = false;
            this.ctrMaVanBanPhapQuy2.IsValidate = true;
            this.ctrMaVanBanPhapQuy2.Location = new System.Drawing.Point(90, 48);
            this.ctrMaVanBanPhapQuy2.Name = "ctrMaVanBanPhapQuy2";
            this.ctrMaVanBanPhapQuy2.Name_VN = "";
            this.ctrMaVanBanPhapQuy2.SetOnlyWarning = false;
            this.ctrMaVanBanPhapQuy2.SetValidate = false;
            this.ctrMaVanBanPhapQuy2.ShowColumnCode = true;
            this.ctrMaVanBanPhapQuy2.ShowColumnName = false;
            this.ctrMaVanBanPhapQuy2.Size = new System.Drawing.Size(62, 21);
            this.ctrMaVanBanPhapQuy2.TabIndex = 2;
            this.ctrMaVanBanPhapQuy2.TagName = "";
            this.ctrMaVanBanPhapQuy2.Where = null;
            this.ctrMaVanBanPhapQuy2.WhereCondition = "";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(13, 34);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(111, 13);
            this.label40.TabIndex = 36;
            this.label40.Text = "Mã văn bản pháp quy";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(13, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(101, 13);
            this.label39.TabIndex = 36;
            this.label39.Text = "Mã kết quả kiểm tra";
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.ctrMaDieuKienGiaHD);
            this.uiGroupBox12.Controls.Add(this.ctrPhuongThucTT);
            this.uiGroupBox12.Controls.Add(this.ctrMaPhanLoaiTriGia);
            this.uiGroupBox12.Controls.Add(this.clcNgayPhatHanhHD);
            this.uiGroupBox12.Controls.Add(this.ctrPhanLoaiHD);
            this.uiGroupBox12.Controls.Add(this.ctrMaTTHoaDon);
            this.uiGroupBox12.Controls.Add(this.label42);
            this.uiGroupBox12.Controls.Add(this.label43);
            this.uiGroupBox12.Controls.Add(this.txtSoTiepNhanHD);
            this.uiGroupBox12.Controls.Add(this.label44);
            this.uiGroupBox12.Controls.Add(this.txtTongHeSoPhanBo);
            this.uiGroupBox12.Controls.Add(this.txtTongTriGiaHD);
            this.uiGroupBox12.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox12.Controls.Add(this.label70);
            this.uiGroupBox12.Controls.Add(this.label45);
            this.uiGroupBox12.Controls.Add(this.label46);
            this.uiGroupBox12.Controls.Add(this.label47);
            this.uiGroupBox12.Controls.Add(this.label48);
            this.uiGroupBox12.Controls.Add(this.label49);
            this.uiGroupBox12.Location = new System.Drawing.Point(317, 249);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(390, 184);
            this.uiGroupBox12.TabIndex = 5;
            this.uiGroupBox12.Text = "Hóa đơn";
            this.uiGroupBox12.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDieuKienGiaHD
            // 
            this.ctrMaDieuKienGiaHD.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaDieuKienGiaHD.Appearance.Options.UseBackColor = true;
            this.ctrMaDieuKienGiaHD.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E011;
            this.ctrMaDieuKienGiaHD.Code = "";
            this.ctrMaDieuKienGiaHD.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDieuKienGiaHD.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDieuKienGiaHD.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDieuKienGiaHD.IsOnlyWarning = false;
            this.ctrMaDieuKienGiaHD.IsValidate = true;
            this.ctrMaDieuKienGiaHD.Location = new System.Drawing.Point(101, 93);
            this.ctrMaDieuKienGiaHD.Name = "ctrMaDieuKienGiaHD";
            this.ctrMaDieuKienGiaHD.Name_VN = "";
            this.ctrMaDieuKienGiaHD.SetOnlyWarning = false;
            this.ctrMaDieuKienGiaHD.SetValidate = false;
            this.ctrMaDieuKienGiaHD.ShowColumnCode = true;
            this.ctrMaDieuKienGiaHD.ShowColumnName = false;
            this.ctrMaDieuKienGiaHD.Size = new System.Drawing.Size(90, 21);
            this.ctrMaDieuKienGiaHD.TabIndex = 5;
            this.ctrMaDieuKienGiaHD.TagName = "";
            this.ctrMaDieuKienGiaHD.Where = null;
            this.ctrMaDieuKienGiaHD.WhereCondition = "";
            // 
            // ctrPhuongThucTT
            // 
            this.ctrPhuongThucTT.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrPhuongThucTT.Appearance.Options.UseBackColor = true;
            this.ctrPhuongThucTT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E009;
            this.ctrPhuongThucTT.Code = "";
            this.ctrPhuongThucTT.ColorControl = System.Drawing.Color.Empty;
            this.ctrPhuongThucTT.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPhuongThucTT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrPhuongThucTT.IsOnlyWarning = false;
            this.ctrPhuongThucTT.IsValidate = true;
            this.ctrPhuongThucTT.Location = new System.Drawing.Point(259, 67);
            this.ctrPhuongThucTT.Name = "ctrPhuongThucTT";
            this.ctrPhuongThucTT.Name_VN = "";
            this.ctrPhuongThucTT.SetOnlyWarning = false;
            this.ctrPhuongThucTT.SetValidate = false;
            this.ctrPhuongThucTT.ShowColumnCode = true;
            this.ctrPhuongThucTT.ShowColumnName = false;
            this.ctrPhuongThucTT.Size = new System.Drawing.Size(106, 21);
            this.ctrPhuongThucTT.TabIndex = 4;
            this.ctrPhuongThucTT.TagName = "";
            this.ctrPhuongThucTT.Where = null;
            this.ctrPhuongThucTT.WhereCondition = "";
            // 
            // ctrMaPhanLoaiTriGia
            // 
            this.ctrMaPhanLoaiTriGia.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaPhanLoaiTriGia.Appearance.Options.UseBackColor = true;
            this.ctrMaPhanLoaiTriGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E010;
            this.ctrMaPhanLoaiTriGia.Code = "";
            this.ctrMaPhanLoaiTriGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaPhanLoaiTriGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaPhanLoaiTriGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaPhanLoaiTriGia.IsOnlyWarning = false;
            this.ctrMaPhanLoaiTriGia.IsValidate = true;
            this.ctrMaPhanLoaiTriGia.Location = new System.Drawing.Point(259, 93);
            this.ctrMaPhanLoaiTriGia.Name = "ctrMaPhanLoaiTriGia";
            this.ctrMaPhanLoaiTriGia.Name_VN = "";
            this.ctrMaPhanLoaiTriGia.SetOnlyWarning = false;
            this.ctrMaPhanLoaiTriGia.SetValidate = false;
            this.ctrMaPhanLoaiTriGia.ShowColumnCode = true;
            this.ctrMaPhanLoaiTriGia.ShowColumnName = false;
            this.ctrMaPhanLoaiTriGia.Size = new System.Drawing.Size(106, 21);
            this.ctrMaPhanLoaiTriGia.TabIndex = 6;
            this.ctrMaPhanLoaiTriGia.TagName = "";
            this.ctrMaPhanLoaiTriGia.Where = null;
            this.ctrMaPhanLoaiTriGia.WhereCondition = "";
            // 
            // clcNgayPhatHanhHD
            // 
            this.clcNgayPhatHanhHD.Location = new System.Drawing.Point(101, 67);
            this.clcNgayPhatHanhHD.Name = "clcNgayPhatHanhHD";
            this.clcNgayPhatHanhHD.ReadOnly = false;
            this.clcNgayPhatHanhHD.Size = new System.Drawing.Size(89, 21);
            this.clcNgayPhatHanhHD.TabIndex = 3;
            this.clcNgayPhatHanhHD.TagName = "";
            this.clcNgayPhatHanhHD.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayPhatHanhHD.WhereCondition = "";
            this.clcNgayPhatHanhHD.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(this.txt_TextChanged);
            // 
            // ctrPhanLoaiHD
            // 
            this.ctrPhanLoaiHD.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrPhanLoaiHD.Appearance.Options.UseBackColor = true;
            this.ctrPhanLoaiHD.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E008;
            this.ctrPhanLoaiHD.Code = "";
            this.ctrPhanLoaiHD.ColorControl = System.Drawing.Color.Empty;
            this.ctrPhanLoaiHD.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrPhanLoaiHD.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrPhanLoaiHD.IsOnlyWarning = false;
            this.ctrPhanLoaiHD.IsValidate = true;
            this.ctrPhanLoaiHD.Location = new System.Drawing.Point(101, 13);
            this.ctrPhanLoaiHD.Name = "ctrPhanLoaiHD";
            this.ctrPhanLoaiHD.Name_VN = "";
            this.ctrPhanLoaiHD.SetOnlyWarning = false;
            this.ctrPhanLoaiHD.SetValidate = false;
            this.ctrPhanLoaiHD.ShowColumnCode = true;
            this.ctrPhanLoaiHD.ShowColumnName = false;
            this.ctrPhanLoaiHD.Size = new System.Drawing.Size(89, 21);
            this.ctrPhanLoaiHD.TabIndex = 0;
            this.ctrPhanLoaiHD.TagName = "";
            this.ctrPhanLoaiHD.Where = null;
            this.ctrPhanLoaiHD.WhereCondition = "";
            // 
            // ctrMaTTHoaDon
            // 
            this.ctrMaTTHoaDon.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaTTHoaDon.Appearance.Options.UseBackColor = true;
            this.ctrMaTTHoaDon.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTHoaDon.Code = "";
            this.ctrMaTTHoaDon.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTHoaDon.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTHoaDon.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTHoaDon.IsOnlyWarning = false;
            this.ctrMaTTHoaDon.IsValidate = true;
            this.ctrMaTTHoaDon.Location = new System.Drawing.Point(285, 120);
            this.ctrMaTTHoaDon.Name = "ctrMaTTHoaDon";
            this.ctrMaTTHoaDon.Name_VN = "";
            this.ctrMaTTHoaDon.SetOnlyWarning = false;
            this.ctrMaTTHoaDon.SetValidate = false;
            this.ctrMaTTHoaDon.ShowColumnCode = true;
            this.ctrMaTTHoaDon.ShowColumnName = false;
            this.ctrMaTTHoaDon.Size = new System.Drawing.Size(79, 21);
            this.ctrMaTTHoaDon.TabIndex = 8;
            this.ctrMaTTHoaDon.TagName = "";
            this.ctrMaTTHoaDon.Where = null;
            this.ctrMaTTHoaDon.WhereCondition = "";
            this.ctrMaTTHoaDon.Load += new System.EventHandler(this.ctrMaTTHoaDon_Load);
            this.ctrMaTTHoaDon.Leave += new System.EventHandler(this.ctrMaTTHoaDon_Leave);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(4, 17);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(98, 13);
            this.label42.TabIndex = 35;
            this.label42.Text = "Phân loại hình thức";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(189, 17);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(67, 13);
            this.label43.TabIndex = 35;
            this.label43.Text = "Số tiếp nhận";
            // 
            // txtSoTiepNhanHD
            // 
            this.txtSoTiepNhanHD.DecimalDigits = 12;
            this.txtSoTiepNhanHD.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTiepNhanHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhanHD.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTiepNhanHD.Location = new System.Drawing.Point(258, 13);
            this.txtSoTiepNhanHD.MaxLength = 15;
            this.txtSoTiepNhanHD.Name = "txtSoTiepNhanHD";
            this.txtSoTiepNhanHD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTiepNhanHD.Size = new System.Drawing.Size(106, 21);
            this.txtSoTiepNhanHD.TabIndex = 1;
            this.txtSoTiepNhanHD.Text = "0";
            this.txtSoTiepNhanHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhanHD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTiepNhanHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(4, 44);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(61, 13);
            this.label44.TabIndex = 35;
            this.label44.Text = "Số hóa đơn";
            // 
            // txtTongHeSoPhanBo
            // 
            this.txtTongHeSoPhanBo.BackColor = System.Drawing.Color.White;
            this.txtTongHeSoPhanBo.DecimalDigits = 20;
            this.txtTongHeSoPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongHeSoPhanBo.FormatString = "G20";
            this.txtTongHeSoPhanBo.Location = new System.Drawing.Point(115, 147);
            this.txtTongHeSoPhanBo.Name = "txtTongHeSoPhanBo";
            this.txtTongHeSoPhanBo.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtTongHeSoPhanBo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongHeSoPhanBo.Size = new System.Drawing.Size(178, 21);
            this.txtTongHeSoPhanBo.TabIndex = 7;
            this.txtTongHeSoPhanBo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongHeSoPhanBo.Value = null;
            this.txtTongHeSoPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongTriGiaHD
            // 
            this.txtTongTriGiaHD.BackColor = System.Drawing.SystemColors.Info;
            this.txtTongTriGiaHD.DecimalDigits = 20;
            this.txtTongTriGiaHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaHD.FormatString = "G20";
            this.txtTongTriGiaHD.Location = new System.Drawing.Point(101, 120);
            this.txtTongTriGiaHD.Name = "txtTongTriGiaHD";
            this.txtTongTriGiaHD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTriGiaHD.Size = new System.Drawing.Size(178, 21);
            this.txtTongTriGiaHD.TabIndex = 7;
            this.txtTongTriGiaHD.Text = "0";
            this.txtTongTriGiaHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTriGiaHD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoHoaDon.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDon.Location = new System.Drawing.Point(101, 40);
            this.txtSoHoaDon.MaxLength = 12;
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(263, 21);
            this.txtSoHoaDon.TabIndex = 2;
            this.txtSoHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHoaDon.Leave += new System.EventHandler(this.txt_Leave);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(5, 152);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(102, 13);
            this.label70.TabIndex = 35;
            this.label70.Text = "Tổng hệ số phân bổ";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(189, 71);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(75, 13);
            this.label45.TabIndex = 35;
            this.label45.Text = "PT thanh toán";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(5, 125);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(61, 13);
            this.label46.TabIndex = 35;
            this.label46.Text = "Tổng trị giá";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(4, 71);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(84, 13);
            this.label47.TabIndex = 35;
            this.label47.Text = "Ngày phát hành";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(189, 97);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(67, 13);
            this.label48.TabIndex = 35;
            this.label48.Text = "Phân loại giá";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(5, 98);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 35;
            this.label49.Text = "Điều kiện giá";
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.Controls.Add(this.ctrMaDVTTrongLuong);
            this.uiGroupBox13.Controls.Add(this.txtTrongLuong);
            this.uiGroupBox13.Location = new System.Drawing.Point(522, 435);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(185, 45);
            this.uiGroupBox13.TabIndex = 7;
            this.uiGroupBox13.Text = "Tổng trọng lượng hàng";
            this.uiGroupBox13.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDVTTrongLuong
            // 
            this.ctrMaDVTTrongLuong.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaDVTTrongLuong.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTTrongLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E006;
            this.ctrMaDVTTrongLuong.Code = "";
            this.ctrMaDVTTrongLuong.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTTrongLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTrongLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTTrongLuong.IsOnlyWarning = false;
            this.ctrMaDVTTrongLuong.IsValidate = true;
            this.ctrMaDVTTrongLuong.Location = new System.Drawing.Point(120, 17);
            this.ctrMaDVTTrongLuong.Name = "ctrMaDVTTrongLuong";
            this.ctrMaDVTTrongLuong.Name_VN = "";
            this.ctrMaDVTTrongLuong.SetOnlyWarning = false;
            this.ctrMaDVTTrongLuong.SetValidate = false;
            this.ctrMaDVTTrongLuong.ShowColumnCode = true;
            this.ctrMaDVTTrongLuong.ShowColumnName = false;
            this.ctrMaDVTTrongLuong.Size = new System.Drawing.Size(48, 21);
            this.ctrMaDVTTrongLuong.TabIndex = 1;
            this.ctrMaDVTTrongLuong.TagName = "";
            this.ctrMaDVTTrongLuong.Where = null;
            this.ctrMaDVTTrongLuong.WhereCondition = "";
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.BackColor = System.Drawing.SystemColors.Info;
            this.txtTrongLuong.DecimalDigits = 20;
            this.txtTrongLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTrongLuong.Location = new System.Drawing.Point(3, 17);
            this.txtTrongLuong.MaxLength = 15;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTrongLuong.Size = new System.Drawing.Size(100, 21);
            this.txtTrongLuong.TabIndex = 0;
            this.txtTrongLuong.Text = "0";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.Controls.Add(this.txtGhiChu);
            this.uiGroupBox10.Location = new System.Drawing.Point(316, 676);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(391, 59);
            this.uiGroupBox10.TabIndex = 13;
            this.uiGroupBox10.Text = "Ghi chú";
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(6, 14);
            this.txtGhiChu.MaxLength = 100;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(355, 41);
            this.txtGhiChu.TabIndex = 0;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.ctrMaDVTSoLuong);
            this.uiGroupBox6.Controls.Add(this.txtSoLuong);
            this.uiGroupBox6.Location = new System.Drawing.Point(317, 434);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(200, 45);
            this.uiGroupBox6.TabIndex = 6;
            this.uiGroupBox6.Text = "Số lượng kiện";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaDVTSoLuong
            // 
            this.ctrMaDVTSoLuong.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaDVTSoLuong.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTSoLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ctrMaDVTSoLuong.Code = "";
            this.ctrMaDVTSoLuong.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTSoLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTSoLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTSoLuong.IsOnlyWarning = false;
            this.ctrMaDVTSoLuong.IsValidate = true;
            this.ctrMaDVTSoLuong.Location = new System.Drawing.Point(131, 17);
            this.ctrMaDVTSoLuong.Name = "ctrMaDVTSoLuong";
            this.ctrMaDVTSoLuong.Name_VN = "";
            this.ctrMaDVTSoLuong.SetOnlyWarning = false;
            this.ctrMaDVTSoLuong.SetValidate = false;
            this.ctrMaDVTSoLuong.ShowColumnCode = true;
            this.ctrMaDVTSoLuong.ShowColumnName = false;
            this.ctrMaDVTSoLuong.Size = new System.Drawing.Size(56, 21);
            this.ctrMaDVTSoLuong.TabIndex = 1;
            this.ctrMaDVTSoLuong.TagName = "";
            this.ctrMaDVTSoLuong.Where = null;
            this.ctrMaDVTSoLuong.WhereCondition = "";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoLuong.DecimalDigits = 20;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong.Location = new System.Drawing.Point(3, 17);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(110, 21);
            this.txtSoLuong.TabIndex = 0;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.Controls.Add(this.txtSoQuanLyNoiBoDN);
            this.uiGroupBox11.Location = new System.Drawing.Point(317, 631);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(390, 42);
            this.uiGroupBox11.TabIndex = 12;
            this.uiGroupBox11.Text = "Số quản lý nội bộ doanh nghiệp";
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // txtSoQuanLyNoiBoDN
            // 
            this.txtSoQuanLyNoiBoDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoQuanLyNoiBoDN.Location = new System.Drawing.Point(8, 16);
            this.txtSoQuanLyNoiBoDN.MaxLength = 255;
            this.txtSoQuanLyNoiBoDN.Name = "txtSoQuanLyNoiBoDN";
            this.txtSoQuanLyNoiBoDN.Size = new System.Drawing.Size(353, 21);
            this.txtSoQuanLyNoiBoDN.TabIndex = 0;
            this.txtSoQuanLyNoiBoDN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoQuanLyNoiBoDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblNgayHD1
            // 
            this.lblNgayHD1.AutoSize = true;
            this.lblNgayHD1.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayHD1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHD1.Location = new System.Drawing.Point(834, 682);
            this.lblNgayHD1.Name = "lblNgayHD1";
            this.lblNgayHD1.Size = new System.Drawing.Size(75, 13);
            this.lblNgayHD1.TabIndex = 6;
            this.lblNgayHD1.Text = "25/10/2014";
            // 
            // lblNgayHD
            // 
            this.lblNgayHD.AutoSize = true;
            this.lblNgayHD.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHD.Location = new System.Drawing.Point(820, 662);
            this.lblNgayHD.Name = "lblNgayHD";
            this.lblNgayHD.Size = new System.Drawing.Size(80, 13);
            this.lblNgayHD.TabIndex = 6;
            this.lblNgayHD.Text = "Ngày hợp đồng";
            // 
            // lblSoHD
            // 
            this.lblSoHD.AutoSize = true;
            this.lblSoHD.BackColor = System.Drawing.Color.Transparent;
            this.lblSoHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHD.Location = new System.Drawing.Point(819, 623);
            this.lblSoHD.Name = "lblSoHD";
            this.lblSoHD.Size = new System.Drawing.Size(67, 13);
            this.lblSoHD.TabIndex = 6;
            this.lblSoHD.Text = "Số hợp đồng";
            // 
            // TabItemPhanHoi
            // 
            this.TabItemPhanHoi.Controls.Add(this.uiGroupBox15);
            this.TabItemPhanHoi.Location = new System.Drawing.Point(1, 21);
            this.TabItemPhanHoi.Name = "TabItemPhanHoi";
            this.TabItemPhanHoi.Size = new System.Drawing.Size(1068, 721);
            this.TabItemPhanHoi.TabStop = true;
            this.TabItemPhanHoi.Text = "Thông báo thuế";
            // 
            // uiGroupBox15
            // 
            this.uiGroupBox15.AutoScroll = true;
            this.uiGroupBox15.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox15.Controls.Add(this.uiGroupBox17);
            this.uiGroupBox15.Controls.Add(this.uiGroupBox16);
            this.uiGroupBox15.Controls.Add(this.txtTongSoDongHangCuaToKhai);
            this.uiGroupBox15.Controls.Add(this.txtTongSoTrangCuaToKhai);
            this.uiGroupBox15.Controls.Add(this.label68);
            this.uiGroupBox15.Controls.Add(this.txtSoTienBaoLanh);
            this.uiGroupBox15.Controls.Add(this.label67);
            this.uiGroupBox15.Controls.Add(this.txtTongTienThuePhaiNop);
            this.uiGroupBox15.Controls.Add(this.label66);
            this.uiGroupBox15.Controls.Add(this.label65);
            this.uiGroupBox15.Controls.Add(this.txtTriGiaTinhThue);
            this.uiGroupBox15.Controls.Add(this.label62);
            this.uiGroupBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox15.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox15.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox15.Name = "uiGroupBox15";
            this.uiGroupBox15.Size = new System.Drawing.Size(1068, 721);
            this.uiGroupBox15.TabIndex = 0;
            this.uiGroupBox15.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox17
            // 
            this.uiGroupBox17.Controls.Add(this.grListTyGia);
            this.uiGroupBox17.Location = new System.Drawing.Point(11, 141);
            this.uiGroupBox17.Name = "uiGroupBox17";
            this.uiGroupBox17.Size = new System.Drawing.Size(280, 100);
            this.uiGroupBox17.TabIndex = 39;
            this.uiGroupBox17.Text = "Tỷ giá";
            this.uiGroupBox17.VisualStyleManager = this.vsmMain;
            // 
            // grListTyGia
            // 
            this.grListTyGia.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListTyGia.ColumnAutoResize = true;
            grListTyGia_DesignTimeLayout.LayoutString = resources.GetString("grListTyGia_DesignTimeLayout.LayoutString");
            this.grListTyGia.DesignTimeLayout = grListTyGia_DesignTimeLayout;
            this.grListTyGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListTyGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grListTyGia.GroupByBoxVisible = false;
            this.grListTyGia.Location = new System.Drawing.Point(3, 17);
            this.grListTyGia.Name = "grListTyGia";
            this.grListTyGia.Size = new System.Drawing.Size(274, 80);
            this.grListTyGia.TabIndex = 40;
            this.grListTyGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListTyGia.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.Controls.Add(this.grListSacThue);
            this.uiGroupBox16.Location = new System.Drawing.Point(297, 3);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(566, 238);
            this.uiGroupBox16.TabIndex = 38;
            this.uiGroupBox16.Text = "Sắc thuế";
            this.uiGroupBox16.VisualStyleManager = this.vsmMain;
            // 
            // grListSacThue
            // 
            this.grListSacThue.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListSacThue.ColumnAutoResize = true;
            grListSacThue_DesignTimeLayout.LayoutString = resources.GetString("grListSacThue_DesignTimeLayout.LayoutString");
            this.grListSacThue.DesignTimeLayout = grListSacThue_DesignTimeLayout;
            this.grListSacThue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListSacThue.GroupByBoxVisible = false;
            this.grListSacThue.Location = new System.Drawing.Point(3, 17);
            this.grListSacThue.Name = "grListSacThue";
            this.grListSacThue.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListSacThue.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSacThue.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListSacThue.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListSacThue.Size = new System.Drawing.Size(560, 218);
            this.grListSacThue.TabIndex = 0;
            this.grListSacThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListSacThue.VisualStyleManager = this.vsmMain;
            // 
            // txtTongSoDongHangCuaToKhai
            // 
            this.txtTongSoDongHangCuaToKhai.BackColor = System.Drawing.Color.White;
            this.txtTongSoDongHangCuaToKhai.DecimalDigits = 20;
            this.txtTongSoDongHangCuaToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoDongHangCuaToKhai.FormatString = "G20";
            this.txtTongSoDongHangCuaToKhai.Location = new System.Drawing.Point(223, 115);
            this.txtTongSoDongHangCuaToKhai.Name = "txtTongSoDongHangCuaToKhai";
            this.txtTongSoDongHangCuaToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoDongHangCuaToKhai.Size = new System.Drawing.Size(52, 21);
            this.txtTongSoDongHangCuaToKhai.TabIndex = 36;
            this.txtTongSoDongHangCuaToKhai.Text = "0";
            this.txtTongSoDongHangCuaToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoDongHangCuaToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoDongHangCuaToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSoTrangCuaToKhai
            // 
            this.txtTongSoTrangCuaToKhai.BackColor = System.Drawing.Color.White;
            this.txtTongSoTrangCuaToKhai.DecimalDigits = 20;
            this.txtTongSoTrangCuaToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTrangCuaToKhai.FormatString = "G20";
            this.txtTongSoTrangCuaToKhai.Location = new System.Drawing.Point(223, 88);
            this.txtTongSoTrangCuaToKhai.Name = "txtTongSoTrangCuaToKhai";
            this.txtTongSoTrangCuaToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTrangCuaToKhai.Size = new System.Drawing.Size(52, 21);
            this.txtTongSoTrangCuaToKhai.TabIndex = 36;
            this.txtTongSoTrangCuaToKhai.Text = "0";
            this.txtTongSoTrangCuaToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTrangCuaToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTrangCuaToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(8, 120);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(154, 13);
            this.label68.TabIndex = 37;
            this.label68.Text = "Tổng số dòng hàng của tờ khai";
            // 
            // txtSoTienBaoLanh
            // 
            this.txtSoTienBaoLanh.BackColor = System.Drawing.Color.White;
            this.txtSoTienBaoLanh.DecimalDigits = 20;
            this.txtSoTienBaoLanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienBaoLanh.FormatString = "G20";
            this.txtSoTienBaoLanh.Location = new System.Drawing.Point(144, 62);
            this.txtSoTienBaoLanh.Name = "txtSoTienBaoLanh";
            this.txtSoTienBaoLanh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienBaoLanh.Size = new System.Drawing.Size(131, 21);
            this.txtSoTienBaoLanh.TabIndex = 36;
            this.txtSoTienBaoLanh.Text = "0";
            this.txtSoTienBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(8, 93);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(132, 13);
            this.label67.TabIndex = 37;
            this.label67.Text = "Tổng số  trang của tờ khai";
            // 
            // txtTongTienThuePhaiNop
            // 
            this.txtTongTienThuePhaiNop.BackColor = System.Drawing.Color.White;
            this.txtTongTienThuePhaiNop.DecimalDigits = 20;
            this.txtTongTienThuePhaiNop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTienThuePhaiNop.FormatString = "G20";
            this.txtTongTienThuePhaiNop.Location = new System.Drawing.Point(144, 35);
            this.txtTongTienThuePhaiNop.Name = "txtTongTienThuePhaiNop";
            this.txtTongTienThuePhaiNop.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTienThuePhaiNop.Size = new System.Drawing.Size(131, 21);
            this.txtTongTienThuePhaiNop.TabIndex = 36;
            this.txtTongTienThuePhaiNop.Text = "0";
            this.txtTongTienThuePhaiNop.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTienThuePhaiNop.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTienThuePhaiNop.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(8, 66);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(84, 13);
            this.label66.TabIndex = 37;
            this.label66.Text = "Số tiền bảo lãnh";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(8, 39);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(121, 13);
            this.label65.TabIndex = 37;
            this.label65.Text = "Tổng tiền thuế phải nộp";
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.BackColor = System.Drawing.Color.White;
            this.txtTriGiaTinhThue.DecimalDigits = 20;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.FormatString = "G20";
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(144, 8);
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(131, 21);
            this.txtTriGiaTinhThue.TabIndex = 36;
            this.txtTriGiaTinhThue.Text = "0";
            this.txtTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(6, 13);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(107, 13);
            this.label62.TabIndex = 37;
            this.label62.Text = "Tổng trị giá tính thuế";
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdChiThiHQ,
            this.cmdGiayPhep,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdDinhKemDT,
            this.cmdTrungChuyen,
            this.cmdToKhaiTriGia,
            this.cmdInAn,
            this.cmdKetQuaTraVe,
            this.cmdThemHangDon,
            this.cmdThemHangExcel,
            this.cmdKetQuaXuLy,
            this.cmdReloadData,
            this.cmdBoSungCont,
            this.cmdMau30,
            this.cmdPrint,
            this.cmdSendVoucher,
            this.cmdToKhaiNhanh});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdToKhaiTriGia1,
            this.cmdVanDon1,
            this.cmDinhKemDT1,
            this.cmdTrungChuyen1,
            this.Separator4,
            this.cmdLuu1,
            this.cmdKhaiBao1,
            this.cmdInAn1,
            this.cmdKetQuaXuLy1,
            this.Separator1,
            this.cmdChiThiHQ1,
            this.cmdKetQuaTraVe1,
            this.cmdMau301,
            this.cmdPrint1,
            this.cmdToKhaiNhanh1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1276, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdToKhaiTriGia1
            // 
            this.cmdToKhaiTriGia1.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia1.Name = "cmdToKhaiTriGia1";
            this.cmdToKhaiTriGia1.Text = "Phí & Các khoản điều chỉnh TG";
            // 
            // cmdVanDon1
            // 
            this.cmdVanDon1.Key = "cmdGiayPhep";
            this.cmdVanDon1.Name = "cmdVanDon1";
            // 
            // cmDinhKemDT1
            // 
            this.cmDinhKemDT1.Key = "cmdDinhKemDT";
            this.cmDinhKemDT1.Name = "cmDinhKemDT1";
            // 
            // cmdTrungChuyen1
            // 
            this.cmdTrungChuyen1.Key = "cmdTrungChuyen";
            this.cmdTrungChuyen1.Name = "cmdTrungChuyen1";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdInAn1
            // 
            this.cmdInAn1.Key = "cmdInAn";
            this.cmdInAn1.Name = "cmdInAn1";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdChiThiHQ1
            // 
            this.cmdChiThiHQ1.Image = ((System.Drawing.Image)(resources.GetObject("cmdChiThiHQ1.Image")));
            this.cmdChiThiHQ1.Key = "cmdChiThiHQ";
            this.cmdChiThiHQ1.Name = "cmdChiThiHQ1";
            // 
            // cmdKetQuaTraVe1
            // 
            this.cmdKetQuaTraVe1.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaTraVe1.Image")));
            this.cmdKetQuaTraVe1.Key = "cmdKetQuaTraVe";
            this.cmdKetQuaTraVe1.Name = "cmdKetQuaTraVe1";
            // 
            // cmdMau301
            // 
            this.cmdMau301.Key = "cmdMau30";
            this.cmdMau301.Name = "cmdMau301";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // cmdToKhaiNhanh1
            // 
            this.cmdToKhaiNhanh1.Key = "cmdToKhaiNhanh";
            this.cmdToKhaiNhanh1.Name = "cmdToKhaiNhanh1";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHangDon1,
            this.cmdThemHangExcel1});
            this.cmdThemHang.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemHang.Image")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdThemHangDon1
            // 
            this.cmdThemHangDon1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemHangDon1.Image")));
            this.cmdThemHangDon1.Key = "cmdThemHangDon";
            this.cmdThemHangDon1.Name = "cmdThemHangDon1";
            // 
            // cmdThemHangExcel1
            // 
            this.cmdThemHangExcel1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemHangExcel1.Image")));
            this.cmdThemHangExcel1.Key = "cmdThemHangExcel";
            this.cmdThemHangExcel1.Name = "cmdThemHangExcel1";
            // 
            // cmdChiThiHQ
            // 
            this.cmdChiThiHQ.Key = "cmdChiThiHQ";
            this.cmdChiThiHQ.Name = "cmdChiThiHQ";
            this.cmdChiThiHQ.Text = "Chỉ thị HQ";
            // 
            // cmdGiayPhep
            // 
            this.cmdGiayPhep.Image = ((System.Drawing.Image)(resources.GetObject("cmdGiayPhep.Image")));
            this.cmdGiayPhep.Key = "cmdGiayPhep";
            this.cmdGiayPhep.Name = "cmdGiayPhep";
            this.cmdGiayPhep.Text = "Giấy phép";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Image = ((System.Drawing.Image)(resources.GetObject("cmdLuu.Image")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBoSungCont1,
            this.cmdSendVoucher1});
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai Báo";
            // 
            // cmdBoSungCont1
            // 
            this.cmdBoSungCont1.Image = ((System.Drawing.Image)(resources.GetObject("cmdBoSungCont1.Image")));
            this.cmdBoSungCont1.Key = "cmdBoSungCont";
            this.cmdBoSungCont1.Name = "cmdBoSungCont1";
            // 
            // cmdSendVoucher1
            // 
            this.cmdSendVoucher1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendVoucher1.Image")));
            this.cmdSendVoucher1.Key = "cmdSendVoucher";
            this.cmdSendVoucher1.Name = "cmdSendVoucher1";
            // 
            // cmdDinhKemDT
            // 
            this.cmdDinhKemDT.Image = ((System.Drawing.Image)(resources.GetObject("cmdDinhKemDT.Image")));
            this.cmdDinhKemDT.Key = "cmdDinhKemDT";
            this.cmdDinhKemDT.Name = "cmdDinhKemDT";
            this.cmdDinhKemDT.Text = "Đính kèm ĐT";
            // 
            // cmdTrungChuyen
            // 
            this.cmdTrungChuyen.Image = ((System.Drawing.Image)(resources.GetObject("cmdTrungChuyen.Image")));
            this.cmdTrungChuyen.Key = "cmdTrungChuyen";
            this.cmdTrungChuyen.Name = "cmdTrungChuyen";
            this.cmdTrungChuyen.Text = "Trung chuyển";
            // 
            // cmdToKhaiTriGia
            // 
            this.cmdToKhaiTriGia.Image = ((System.Drawing.Image)(resources.GetObject("cmdToKhaiTriGia.Image")));
            this.cmdToKhaiTriGia.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Name = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Text = "Phí & Các khoản điều chỉnh trị giá";
            // 
            // cmdInAn
            // 
            this.cmdInAn.Image = ((System.Drawing.Image)(resources.GetObject("cmdInAn.Image")));
            this.cmdInAn.Key = "cmdInAn";
            this.cmdInAn.Name = "cmdInAn";
            this.cmdInAn.Text = "In TK tạm";
            this.cmdInAn.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdKetQuaTraVe
            // 
            this.cmdKetQuaTraVe.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaTraVe.Image")));
            this.cmdKetQuaTraVe.Key = "cmdKetQuaTraVe";
            this.cmdKetQuaTraVe.Name = "cmdKetQuaTraVe";
            this.cmdKetQuaTraVe.Text = "Thông tin HQ";
            // 
            // cmdThemHangDon
            // 
            this.cmdThemHangDon.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemHangDon.Image")));
            this.cmdThemHangDon.Key = "cmdThemHangDon";
            this.cmdThemHangDon.Name = "cmdThemHangDon";
            this.cmdThemHangDon.Text = "Thêm hàng";
            // 
            // cmdThemHangExcel
            // 
            this.cmdThemHangExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemHangExcel.Image")));
            this.cmdThemHangExcel.Key = "cmdThemHangExcel";
            this.cmdThemHangExcel.Name = "cmdThemHangExcel";
            this.cmdThemHangExcel.Text = "Thêm hàng từ Excel";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdReloadData1});
            this.cmdKetQuaXuLy.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaXuLy.Image")));
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "KQ xử lý";
            // 
            // cmdReloadData1
            // 
            this.cmdReloadData1.Key = "cmdReloadData";
            this.cmdReloadData1.Name = "cmdReloadData1";
            // 
            // cmdReloadData
            // 
            this.cmdReloadData.Image = ((System.Drawing.Image)(resources.GetObject("cmdReloadData.Image")));
            this.cmdReloadData.Key = "cmdReloadData";
            this.cmdReloadData.Name = "cmdReloadData";
            this.cmdReloadData.Text = "Cập nhật lại tờ khai";
            // 
            // cmdBoSungCont
            // 
            this.cmdBoSungCont.Key = "cmdBoSungCont";
            this.cmdBoSungCont.Name = "cmdBoSungCont";
            this.cmdBoSungCont.Text = "Khai báo bổ sung Container";
            // 
            // cmdMau30
            // 
            this.cmdMau30.Image = ((System.Drawing.Image)(resources.GetObject("cmdMau30.Image")));
            this.cmdMau30.Key = "cmdMau30";
            this.cmdMau30.Name = "cmdMau30";
            this.cmdMau30.Text = "Mẫu 30_TT38";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Text = "In TK không có Message";
            // 
            // cmdSendVoucher
            // 
            this.cmdSendVoucher.Key = "cmdSendVoucher";
            this.cmdSendVoucher.Name = "cmdSendVoucher";
            this.cmdSendVoucher.Text = "Khai báo chứng từ";
            // 
            // cmdToKhaiNhanh
            // 
            this.cmdToKhaiNhanh.Image = ((System.Drawing.Image)(resources.GetObject("cmdToKhaiNhanh.Image")));
            this.cmdToKhaiNhanh.Key = "cmdToKhaiNhanh";
            this.cmdToKhaiNhanh.Name = "cmdToKhaiNhanh";
            this.cmdToKhaiNhanh.Text = "Danh sách tờ khai nhánh";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1276, 28);
            // 
            // ucCategory1
            // 
            this.ucCategory1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucCategory1.Appearance.Options.UseBackColor = true;
            this.ucCategory1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E006;
            this.ucCategory1.Code = "";
            this.ucCategory1.ColorControl = System.Drawing.Color.Empty;
            this.ucCategory1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory1.IsOnlyWarning = false;
            this.ucCategory1.IsValidate = true;
            this.ucCategory1.Location = new System.Drawing.Point(116, 14);
            this.ucCategory1.Name = "ucCategory1";
            this.ucCategory1.Name_VN = "";
            this.ucCategory1.SetOnlyWarning = false;
            this.ucCategory1.SetValidate = false;
            this.ucCategory1.ShowColumnCode = true;
            this.ucCategory1.ShowColumnName = true;
            this.ucCategory1.Size = new System.Drawing.Size(56, 26);
            this.ucCategory1.TabIndex = 1;
            this.ucCategory1.TagName = "";
            this.ucCategory1.Where = null;
            this.ucCategory1.WhereCondition = "";
            // 
            // VNACC_ToKhaiMauDichNhapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1276, 777);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ToKhaiMauDichNhapForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai nhập";
            this.Load += new System.EventHandler(this.VNACC_ToKhaiMauDichXuatForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VNACC_ToKhaiMauDichNhapForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).EndInit();
            this.grbDonVi.ResumeLayout(false);
            this.grbDonVi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).EndInit();
            this.grbDoiTac.ResumeLayout(false);
            this.grbDoiTac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabThongtin)).EndInit();
            this.tabThongtin.ResumeLayout(false);
            this.TabItemThongtin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            this.uiGroupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).EndInit();
            this.uiGroupBox14.ResumeLayout(false);
            this.uiGroupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            this.uiGroupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            this.uiGroupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            this.TabItemPhanHoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).EndInit();
            this.uiGroupBox15.ResumeLayout(false);
            this.uiGroupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).EndInit();
            this.uiGroupBox17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListTyGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListSacThue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox grbDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonVi;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDonVi;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonVi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox grbDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoiTac;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac4;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDaiLyHQ;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDonVi;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuCTHanMuc;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCTHanMuc;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieuCTBaoLanh;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCTBaoLanh;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuanLyNoiBoDN;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdVanDon1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHQ;
        private Janus.Windows.UI.CommandBars.UICommand cmdGiayPhep;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox12;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label56;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuKyHieu;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenPTVC;
        private System.Windows.Forms.Label label57;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox14;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTKChiaNho;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoNhanhToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiTNTX;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhanHD;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHQ1;
        private Janus.Windows.UI.CommandBars.UICommand cmDinhKemDT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTrungChuyen1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDinhKemDT;
        private Janus.Windows.UI.CommandBars.UICommand cmdTrungChuyen;
        private Janus.Windows.UI.CommandBars.UICommand cmdToKhaiTriGia1;
        private Janus.Windows.UI.CommandBars.UICommand cmdToKhaiTriGia;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhuongThucVT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiHH;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLoaiHinh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhanLoaiHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTHoaDon;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTrongLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrNguoiNopThue;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhanLoaiToChuc;
        private System.Windows.Forms.Label label41;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPTVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaLyDoDeNghi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaXDThoiHanNopThue;
        private System.Windows.Forms.Label label60;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanhBL;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanhHM;
        private Janus.Windows.UI.Tab.UITab tabThongtin;
        private Janus.Windows.UI.Tab.UITabPage TabItemThongtin;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.UI.Tab.UITabPage TabItemPhanHoi;
        private System.Windows.Forms.Label label61;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox15;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThue;
        private System.Windows.Forms.Label label62;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox16;
        private Janus.Windows.GridEX.GridEX grListSacThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTienThuePhaiNop;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoDongHangCuaToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongSoTrangCuaToKhai;
        private System.Windows.Forms.Label label68;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienBaoLanh;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox17;
        private Janus.Windows.GridEX.GridEX grListTyGia;
        private System.Windows.Forms.Label label63;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDaiLyHaiQuan;
        private System.Windows.Forms.Label label64;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcThoiHanTaiNhapTaiXuat;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHangDen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayPhatHanhHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayKhoiHanhVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayDangKy;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapQuy4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapQuy5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapQuy3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapQuy1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaVanBanPhapQuy2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongCont;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTSoLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrDiaDiemXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrDiaDiemDoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDieuKienGiaHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhuongThucTT;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhanLoaiTriGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDDLuuKho;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDiaDiemDichVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaKetQuaKiemTra;
        private System.Windows.Forms.Label lblPhanLuong;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaNuocDoiTac;
        private Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy ctrNhomXuLyHS;
        private Janus.Windows.UI.CommandBars.UICommand cmdInAn1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInAn;
        private Janus.Windows.UI.CommandBars.UICommand Separator4;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaTraVe1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaTraVe;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaNHBaoLanh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaNHTraThueThay;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHangDon1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHangExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHangDon;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHangExcel;
        private Janus.Windows.EditControls.UIRadioButton rdbNPL;
        private Janus.Windows.EditControls.UIRadioButton rdbThietBi;
        private Janus.Windows.EditControls.UIRadioButton rdbSanPham;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy;
        private Janus.Windows.EditControls.UIRadioButton rdbHangMau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhaiDauTien;
        private System.Windows.Forms.Label lblNgayHD1;
        private System.Windows.Forms.Label lblNgayHD;
        private System.Windows.Forms.Label lblSoHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHD;
        private Janus.Windows.EditControls.UIButton btnMaLoaiHinh;
        private Janus.Windows.EditControls.UIButton btnMaHQ;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemDoHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemDoHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemXepHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayNhapKhoDauTien;
        private System.Windows.Forms.Label label69;
        private Janus.Windows.UI.CommandBars.UICommand cmdReloadData1;
        private Janus.Windows.UI.CommandBars.UICommand cmdReloadData;
        private Janus.Windows.UI.CommandBars.UICommand cmdBoSungCont;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox18;
        private Janus.Windows.GridEX.EditControls.EditBox txtUyThacXK;
        private System.Windows.Forms.Label label71;
        private Janus.Windows.UI.CommandBars.UICommand cmdMau30;
        private Janus.Windows.UI.CommandBars.UICommand cmdMau301;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongHeSoPhanBo;
        private System.Windows.Forms.Label label70;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UICommand cmdBoSungCont1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendVoucher1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendVoucher;
        private Janus.Windows.UI.CommandBars.UICommand cmdToKhaiNhanh1;
        private Janus.Windows.UI.CommandBars.UICommand cmdToKhaiNhanh;
    }
}
