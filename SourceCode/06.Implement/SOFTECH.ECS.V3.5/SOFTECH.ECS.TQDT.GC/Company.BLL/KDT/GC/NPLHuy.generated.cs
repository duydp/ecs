﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class NPLHuy : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string MaNPL { set; get; }
		public string TenNPL { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
		public decimal LuongNPLHuy { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<NPLHuy> ConvertToCollection(IDataReader reader)
		{
			List<NPLHuy> collection = new List<NPLHuy>();
			while (reader.Read())
			{
				NPLHuy entity = new NPLHuy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLHuy"))) entity.LuongNPLHuy = reader.GetDecimal(reader.GetOrdinal("LuongNPLHuy"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<NPLHuy> collection, long id)
        {
            foreach (NPLHuy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_NPLHuy VALUES(@Master_ID, @MaNPL, @TenNPL, @MaHS, @DVT_ID, @LuongNPLHuy)";
            string update = "UPDATE t_KDT_GC_NPLHuy SET Master_ID = @Master_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongNPLHuy = @LuongNPLHuy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_NPLHuy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNPLHuy", SqlDbType.Decimal, "LuongNPLHuy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNPLHuy", SqlDbType.Decimal, "LuongNPLHuy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_NPLHuy VALUES(@Master_ID, @MaNPL, @TenNPL, @MaHS, @DVT_ID, @LuongNPLHuy)";
            string update = "UPDATE t_KDT_GC_NPLHuy SET Master_ID = @Master_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongNPLHuy = @LuongNPLHuy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_NPLHuy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNPLHuy", SqlDbType.Decimal, "LuongNPLHuy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNPLHuy", SqlDbType.Decimal, "LuongNPLHuy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
        public static DataSet ViewNPLHuy(long HopDongID)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT dk.ID,dk.NgayTiepNhan,dk.HopDong_ID,dk.LyDoHuy,npl.MaNPL,npl.TenNPL,npl.DVT_ID,npl.LuongNPLHuy FROM t_KDT_GC_NPLHuyDangKy AS dk JOIN t_KDT_GC_NPLHuy AS npl ON dk.ID = npl.Master_ID where dk.HopDong_ID = "+HopDongID;
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@HopDongID", SqlDbType.BigInt, HopDongID);

            return db.ExecuteDataSet(dbCommand);
        }
		public static NPLHuy Load(long id)
		{
			const string spName = "[dbo].[p_NPLHuy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<NPLHuy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_NPLHuy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_NPLHuy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_NPLHuy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_NPLHuy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertNPLHuy(long master_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongNPLHuy)
		{
			NPLHuy entity = new NPLHuy();	
			entity.Master_ID = master_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNPLHuy = luongNPLHuy;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_NPLHuy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNPLHuy", SqlDbType.Decimal, LuongNPLHuy);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateNPLHuy(long id, long master_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongNPLHuy)
		{
			NPLHuy entity = new NPLHuy();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNPLHuy = luongNPLHuy;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_NPLHuy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNPLHuy", SqlDbType.Decimal, LuongNPLHuy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateNPLHuy(long id, long master_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongNPLHuy)
		{
			NPLHuy entity = new NPLHuy();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNPLHuy = luongNPLHuy;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_NPLHuy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNPLHuy", SqlDbType.Decimal, LuongNPLHuy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteNPLHuy(long id)
		{
			NPLHuy entity = new NPLHuy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_NPLHuy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_NPLHuy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}