﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class VNACC_VouchersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem15 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem16 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem17 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem18 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem19 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem20 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grListCO_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_VouchersForm));
            Janus.Windows.GridEX.GridEXLayout grListVanDon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListHoaDon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListGiayPhep_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout cbbLoaiGiayPhep_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListContainer_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListCTKhac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grListDKLNG_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEdit");
            this.cmdCancel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdEdit");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.lblFileNameVanDon = new System.Windows.Forms.Label();
            this.dtpNgayVanDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbbLoaiVanDon = new Janus.Windows.EditControls.UIComboBox();
            this.ctrNuocPHVanDon = new Company.Interface.Controls.NuocHControl();
            this.txtGhiChuVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaDiemCTQC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblFileNameHopDong = new System.Windows.Forms.Label();
            this.dtpNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpThoiHanThanhToan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtGhiChuHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblFileNameHoaDon = new System.Windows.Forms.Label();
            this.dtpNgayPhatHanhHoaDonTM = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtGhiChuHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHoaDonTM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lblFileNameGiayPhep = new System.Windows.Forms.Label();
            this.dtpNgayHetHanGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpNgayCapGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtNguoiCapGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiCapGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lblFileNamContainer = new System.Windows.Forms.Label();
            this.dtpNgayDangKyToKhai = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtGhiChuContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.lblFileNameCTKhac = new System.Windows.Forms.Label();
            this.cbbLoaiChungTu = new Janus.Windows.EditControls.UIComboBox();
            this.dtpNgayChungTuKhac = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtTenChungTuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuCTKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNoiPhatHanhCTKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoChungTuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.dtpGioLamTT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpNgayDangKyThuTuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtNoiDung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.lblFileNameCO = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateNgayCapCO = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbbLoaiCO = new Janus.Windows.EditControls.UIComboBox();
            this.txtNuocCapCO = new Company.Interface.Controls.NuocHControl();
            this.txtGhiChuCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiCapCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtToChucCapCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCtrVouchers = new Janus.Windows.UI.Tab.UITab();
            this.tpCO = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListCO = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAutoFeedbackCO = new System.Windows.Forms.CheckBox();
            this.linkHuongDanKySo = new System.Windows.Forms.LinkLabel();
            this.linkHuongDanKB = new System.Windows.Forms.LinkLabel();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteCO = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnViewCO = new Janus.Windows.EditControls.UIButton();
            this.btnAddCO = new Janus.Windows.EditControls.UIButton();
            this.label30 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtFileSizeCO = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.clcNgayTiepNhanCO = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtSoTiepNhanCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpVanDon = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListVanDon = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox38 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAutoFeedbackVD = new System.Windows.Forms.CheckBox();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.uiButton4 = new Janus.Windows.EditControls.UIButton();
            this.uiButton5 = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteVanDon = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.label69 = new System.Windows.Forms.Label();
            this.btnViewVanDon = new Janus.Windows.EditControls.UIButton();
            this.btnAddVanDon = new Janus.Windows.EditControls.UIButton();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.txtFileSizeVanDon = new System.Windows.Forms.Label();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.clcNgayTNVanDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.txtSoTiepNhanVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpHopDong = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListHopDong = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox41 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAutoFeedbackHD = new System.Windows.Forms.CheckBox();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.uiButton6 = new Janus.Windows.EditControls.UIButton();
            this.uiButton8 = new Janus.Windows.EditControls.UIButton();
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteHopDong = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.label88 = new System.Windows.Forms.Label();
            this.btnViewHopDong = new Janus.Windows.EditControls.UIButton();
            this.btnAddHopDong = new Janus.Windows.EditControls.UIButton();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.txtFileSizeHopDong = new System.Windows.Forms.Label();
            this.uiGroupBox14 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTongTriGiaHopDong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.uiGroupBox15 = new Janus.Windows.EditControls.UIGroupBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.clcNgayTNHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.txtSoTNHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpHoaDon = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListHoaDon = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox17 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox42 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAutoFeedbackHoaDon = new System.Windows.Forms.CheckBox();
            this.linkLabel7 = new System.Windows.Forms.LinkLabel();
            this.linkLabel8 = new System.Windows.Forms.LinkLabel();
            this.uiButton9 = new Janus.Windows.EditControls.UIButton();
            this.uiButton10 = new Janus.Windows.EditControls.UIButton();
            this.uiButton7 = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteHoaDon = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.label97 = new System.Windows.Forms.Label();
            this.btnViewHoaDon = new Janus.Windows.EditControls.UIButton();
            this.btnAddHoaDon = new Janus.Windows.EditControls.UIButton();
            this.label61 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.txtFileSizeHoaDon = new System.Windows.Forms.Label();
            this.uiGroupBox19 = new Janus.Windows.EditControls.UIGroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.uiGroupBox20 = new Janus.Windows.EditControls.UIGroupBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.clcNgayTNHoaDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label77 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.txtSoTNHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpGiayPhep = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox21 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListGiayPhep = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox22 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox43 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAutoFeedbackGP = new System.Windows.Forms.CheckBox();
            this.linkLabel9 = new System.Windows.Forms.LinkLabel();
            this.linkLabel10 = new System.Windows.Forms.LinkLabel();
            this.uiButton12 = new Janus.Windows.EditControls.UIButton();
            this.uiButton13 = new Janus.Windows.EditControls.UIButton();
            this.uiButton11 = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteGiayPhep = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox23 = new Janus.Windows.EditControls.UIGroupBox();
            this.label106 = new System.Windows.Forms.Label();
            this.btnViewGiayPhep = new Janus.Windows.EditControls.UIButton();
            this.btnAddGiayPhep = new Janus.Windows.EditControls.UIButton();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.txtFileSizeGiayPhep = new System.Windows.Forms.Label();
            this.uiGroupBox24 = new Janus.Windows.EditControls.UIGroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cbbLoaiGiayPhep = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.uiGroupBox25 = new Janus.Windows.EditControls.UIGroupBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.clcNgayTNGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.txtSoTNGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpContainer = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox26 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListContainer = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox27 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox44 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAutoFeedbackCT = new System.Windows.Forms.CheckBox();
            this.linkLabel11 = new System.Windows.Forms.LinkLabel();
            this.linkLabel12 = new System.Windows.Forms.LinkLabel();
            this.uiButton14 = new Janus.Windows.EditControls.UIButton();
            this.uiButton16 = new Janus.Windows.EditControls.UIButton();
            this.uiButton15 = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteContainer = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox28 = new Janus.Windows.EditControls.UIGroupBox();
            this.label112 = new System.Windows.Forms.Label();
            this.btnViewContainer = new Janus.Windows.EditControls.UIButton();
            this.btnAddContainer = new Janus.Windows.EditControls.UIButton();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.txtFileSizeContainer = new System.Windows.Forms.Label();
            this.uiGroupBox29 = new Janus.Windows.EditControls.UIGroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtMaHQTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaLoaiHinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox30 = new Janus.Windows.EditControls.UIGroupBox();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.clcNgayTNContainer = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.txtSoTNContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpCTKhac = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox31 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListCTKhac = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox32 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox45 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAutoFeedbackCTK = new System.Windows.Forms.CheckBox();
            this.linkLabel13 = new System.Windows.Forms.LinkLabel();
            this.linkLabel14 = new System.Windows.Forms.LinkLabel();
            this.uiButton17 = new Janus.Windows.EditControls.UIButton();
            this.uiButton18 = new Janus.Windows.EditControls.UIButton();
            this.uiButton19 = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteCTKhac = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox33 = new Janus.Windows.EditControls.UIGroupBox();
            this.label113 = new System.Windows.Forms.Label();
            this.btnViewCTKhac = new Janus.Windows.EditControls.UIButton();
            this.btnAddCTKhac = new Janus.Windows.EditControls.UIButton();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.txtFileSizeChungTuKhac = new System.Windows.Forms.Label();
            this.uiGroupBox34 = new Janus.Windows.EditControls.UIGroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this.uiGroupBox35 = new Janus.Windows.EditControls.UIGroupBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.clcNgayTNCTKhac = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.txtSoTNCTKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQCTKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpLamNgoaiGio = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox36 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListDKLNG = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox37 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkAutoFeedbackOT = new System.Windows.Forms.CheckBox();
            this.uiButton23 = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteOverTime = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox39 = new Janus.Windows.EditControls.UIGroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.uiGroupBox40 = new Janus.Windows.EditControls.UIGroupBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.clcNgayTNDKLNG = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.txtSoTNDKLNG = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHQDKLNG = new Janus.Windows.GridEX.EditControls.EditBox();
            this.timerAutoFeedBack = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCtrVouchers)).BeginInit();
            this.tbCtrVouchers.SuspendLayout();
            this.tpCO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.tpVanDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListVanDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox38)).BeginInit();
            this.uiGroupBox38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            this.tpHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox41)).BeginInit();
            this.uiGroupBox41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).BeginInit();
            this.uiGroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).BeginInit();
            this.uiGroupBox15.SuspendLayout();
            this.tpHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).BeginInit();
            this.uiGroupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox42)).BeginInit();
            this.uiGroupBox42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox19)).BeginInit();
            this.uiGroupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox20)).BeginInit();
            this.uiGroupBox20.SuspendLayout();
            this.tpGiayPhep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox21)).BeginInit();
            this.uiGroupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListGiayPhep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox22)).BeginInit();
            this.uiGroupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox43)).BeginInit();
            this.uiGroupBox43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox23)).BeginInit();
            this.uiGroupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox24)).BeginInit();
            this.uiGroupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiGiayPhep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).BeginInit();
            this.uiGroupBox25.SuspendLayout();
            this.tpContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox26)).BeginInit();
            this.uiGroupBox26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox27)).BeginInit();
            this.uiGroupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox44)).BeginInit();
            this.uiGroupBox44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox28)).BeginInit();
            this.uiGroupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox29)).BeginInit();
            this.uiGroupBox29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox30)).BeginInit();
            this.uiGroupBox30.SuspendLayout();
            this.tpCTKhac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox31)).BeginInit();
            this.uiGroupBox31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListCTKhac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox32)).BeginInit();
            this.uiGroupBox32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox45)).BeginInit();
            this.uiGroupBox45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox33)).BeginInit();
            this.uiGroupBox33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox34)).BeginInit();
            this.uiGroupBox34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox35)).BeginInit();
            this.uiGroupBox35.SuspendLayout();
            this.tpLamNgoaiGio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox36)).BeginInit();
            this.uiGroupBox36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListDKLNG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox37)).BeginInit();
            this.uiGroupBox37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox39)).BeginInit();
            this.uiGroupBox39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox40)).BeginInit();
            this.uiGroupBox40.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 665), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 665);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 641);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 641);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.tbCtrVouchers);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(1136, 665);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdEdit,
            this.cmdCancel,
            this.cmdUpdateGuidString});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.cmdSave1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdUpdateGuidString1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(565, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            // 
            // cmdSend
            // 
            this.cmdSend.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdEdit1,
            this.cmdCancel1});
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "KHAI BÁO";
            // 
            // cmdEdit1
            // 
            this.cmdEdit1.Key = "cmdEdit";
            this.cmdEdit1.Name = "cmdEdit1";
            this.cmdEdit1.Text = "KHAI BÁO SỬA";
            // 
            // cmdCancel1
            // 
            this.cmdCancel1.Key = "cmdCancel";
            this.cmdCancel1.Name = "cmdCancel1";
            this.cmdCancel1.Text = "KHAI BÁO HỦY";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "LƯU LẠI";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "LẤY PHẢN HỒI";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "KẾT QUẢ XỬ LÝ";
            // 
            // cmdEdit
            // 
            this.cmdEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdEdit.Image")));
            this.cmdEdit.Key = "cmdEdit";
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Text = "KHAI BÁO SỬA ĐĂNG KÝ LÀM NGOÀI GIỜ";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancel.Image")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Text = "KHAI BÁO HỦY ĐĂNG KÝ LÀM NGOÀI GIỜ";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "CẬP NHẬT CHUỖI PHẢN HỒI";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1342, 28);
            // 
            // lblFileNameVanDon
            // 
            this.lblFileNameVanDon.AutoSize = true;
            this.lblFileNameVanDon.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameVanDon.ForeColor = System.Drawing.Color.Blue;
            this.lblFileNameVanDon.Location = new System.Drawing.Point(124, 27);
            this.lblFileNameVanDon.Name = "lblFileNameVanDon";
            this.lblFileNameVanDon.Size = new System.Drawing.Size(63, 13);
            this.lblFileNameVanDon.TabIndex = 84;
            this.lblFileNameVanDon.Text = "{File Name}";
            // 
            // dtpNgayVanDon
            // 
            this.dtpNgayVanDon.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayVanDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayVanDon.DropDownCalendar.Name = "";
            this.dtpNgayVanDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayVanDon.Location = new System.Drawing.Point(716, 24);
            this.dtpNgayVanDon.Name = "dtpNgayVanDon";
            this.dtpNgayVanDon.Nullable = true;
            this.dtpNgayVanDon.NullButtonText = "Xóa";
            this.dtpNgayVanDon.ShowNullButton = true;
            this.dtpNgayVanDon.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayVanDon.TabIndex = 8;
            this.dtpNgayVanDon.TodayButtonText = "Hôm nay";
            this.dtpNgayVanDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // cbbLoaiVanDon
            // 
            this.cbbLoaiVanDon.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Đường biển";
            uiComboBoxItem9.Value = 1;
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Đường không";
            uiComboBoxItem10.Value = 2;
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Đường bộ";
            uiComboBoxItem11.Value = 3;
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = true;
            uiComboBoxItem12.Text = "Đường sắt";
            uiComboBoxItem12.Value = 4;
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Đường sông";
            uiComboBoxItem13.Value = 5;
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Loại khác";
            uiComboBoxItem14.Value = 9;
            this.cbbLoaiVanDon.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem9,
            uiComboBoxItem10,
            uiComboBoxItem11,
            uiComboBoxItem12,
            uiComboBoxItem13,
            uiComboBoxItem14});
            this.cbbLoaiVanDon.Location = new System.Drawing.Point(716, 59);
            this.cbbLoaiVanDon.Name = "cbbLoaiVanDon";
            this.cbbLoaiVanDon.Size = new System.Drawing.Size(254, 21);
            this.cbbLoaiVanDon.TabIndex = 9;
            this.cbbLoaiVanDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // ctrNuocPHVanDon
            // 
            this.ctrNuocPHVanDon.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocPHVanDon.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrNuocPHVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocPHVanDon.Location = new System.Drawing.Point(127, 58);
            this.ctrNuocPHVanDon.Ma = "";
            this.ctrNuocPHVanDon.Name = "ctrNuocPHVanDon";
            this.ctrNuocPHVanDon.ReadOnly = false;
            this.ctrNuocPHVanDon.Size = new System.Drawing.Size(267, 22);
            this.ctrNuocPHVanDon.TabIndex = 5;
            this.ctrNuocPHVanDon.VisualStyleManager = null;
            // 
            // txtGhiChuVanDon
            // 
            this.txtGhiChuVanDon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChuVanDon.Location = new System.Drawing.Point(127, 133);
            this.txtGhiChuVanDon.MaxLength = 2000;
            this.txtGhiChuVanDon.Multiline = true;
            this.txtGhiChuVanDon.Name = "txtGhiChuVanDon";
            this.txtGhiChuVanDon.Size = new System.Drawing.Size(843, 21);
            this.txtGhiChuVanDon.TabIndex = 7;
            this.txtGhiChuVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaDiemCTQC
            // 
            this.txtDiaDiemCTQC.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDiaDiemCTQC.Location = new System.Drawing.Point(213, 98);
            this.txtDiaDiemCTQC.MaxLength = 255;
            this.txtDiaDiemCTQC.Name = "txtDiaDiemCTQC";
            this.txtDiaDiemCTQC.Size = new System.Drawing.Size(757, 21);
            this.txtDiaDiemCTQC.TabIndex = 6;
            this.txtDiaDiemCTQC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoVanDon.Location = new System.Drawing.Point(127, 24);
            this.txtSoVanDon.MaxLength = 35;
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(443, 21);
            this.txtSoVanDon.TabIndex = 4;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(21, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Địa điểm chuyển tải /quá cảnh :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(612, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "Ngày vận đơn :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(21, 137);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "Ghi chú khác :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(21, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 13);
            this.label14.TabIndex = 45;
            this.label14.Text = "Nước phát hành :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(612, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 13);
            this.label15.TabIndex = 44;
            this.label15.Text = "Loại vận đơn :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(21, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Số vận đơn :";
            // 
            // lblFileNameHopDong
            // 
            this.lblFileNameHopDong.AutoSize = true;
            this.lblFileNameHopDong.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameHopDong.ForeColor = System.Drawing.Color.Blue;
            this.lblFileNameHopDong.Location = new System.Drawing.Point(124, 26);
            this.lblFileNameHopDong.Name = "lblFileNameHopDong";
            this.lblFileNameHopDong.Size = new System.Drawing.Size(63, 13);
            this.lblFileNameHopDong.TabIndex = 43;
            this.lblFileNameHopDong.Text = "{File Name}";
            // 
            // dtpNgayHopDong
            // 
            this.dtpNgayHopDong.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayHopDong.DropDownCalendar.Name = "";
            this.dtpNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHopDong.Location = new System.Drawing.Point(587, 23);
            this.dtpNgayHopDong.Name = "dtpNgayHopDong";
            this.dtpNgayHopDong.Nullable = true;
            this.dtpNgayHopDong.NullButtonText = "Xóa";
            this.dtpNgayHopDong.ShowNullButton = true;
            this.dtpNgayHopDong.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayHopDong.TabIndex = 5;
            this.dtpNgayHopDong.TodayButtonText = "Hôm nay";
            this.dtpNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtpThoiHanThanhToan
            // 
            this.dtpThoiHanThanhToan.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpThoiHanThanhToan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpThoiHanThanhToan.DropDownCalendar.Name = "";
            this.dtpThoiHanThanhToan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpThoiHanThanhToan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpThoiHanThanhToan.Location = new System.Drawing.Point(152, 62);
            this.dtpThoiHanThanhToan.Name = "dtpThoiHanThanhToan";
            this.dtpThoiHanThanhToan.Nullable = true;
            this.dtpThoiHanThanhToan.NullButtonText = "Xóa";
            this.dtpThoiHanThanhToan.ShowNullButton = true;
            this.dtpThoiHanThanhToan.Size = new System.Drawing.Size(159, 21);
            this.dtpThoiHanThanhToan.TabIndex = 6;
            this.dtpThoiHanThanhToan.TodayButtonText = "Hôm nay";
            this.dtpThoiHanThanhToan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtGhiChuHopDong
            // 
            this.txtGhiChuHopDong.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChuHopDong.Location = new System.Drawing.Point(126, 97);
            this.txtGhiChuHopDong.MaxLength = 2000;
            this.txtGhiChuHopDong.Multiline = true;
            this.txtGhiChuHopDong.Name = "txtGhiChuHopDong";
            this.txtGhiChuHopDong.Size = new System.Drawing.Size(804, 21);
            this.txtGhiChuHopDong.TabIndex = 8;
            this.txtGhiChuHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoHopDong.Location = new System.Drawing.Point(126, 23);
            this.txtSoHopDong.MaxLength = 50;
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(343, 21);
            this.txtSoHopDong.TabIndex = 4;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(485, 66);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 42;
            this.label19.Text = "Tổng trị giá :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(20, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(111, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "Thời hạn thanh toán :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(20, 101);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "Ghi chú khác :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(485, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(87, 13);
            this.label23.TabIndex = 44;
            this.label23.Text = "Ngày hợp đồng :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(20, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(74, 13);
            this.label24.TabIndex = 43;
            this.label24.Text = "Số hợp đồng :";
            // 
            // lblFileNameHoaDon
            // 
            this.lblFileNameHoaDon.AutoSize = true;
            this.lblFileNameHoaDon.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameHoaDon.ForeColor = System.Drawing.Color.Blue;
            this.lblFileNameHoaDon.Location = new System.Drawing.Point(124, 26);
            this.lblFileNameHoaDon.Name = "lblFileNameHoaDon";
            this.lblFileNameHoaDon.Size = new System.Drawing.Size(63, 13);
            this.lblFileNameHoaDon.TabIndex = 43;
            this.lblFileNameHoaDon.Text = "{File Name}";
            // 
            // dtpNgayPhatHanhHoaDonTM
            // 
            this.dtpNgayPhatHanhHoaDonTM.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayPhatHanhHoaDonTM.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayPhatHanhHoaDonTM.DropDownCalendar.Name = "";
            this.dtpNgayPhatHanhHoaDonTM.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayPhatHanhHoaDonTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayPhatHanhHoaDonTM.Location = new System.Drawing.Point(826, 20);
            this.dtpNgayPhatHanhHoaDonTM.Name = "dtpNgayPhatHanhHoaDonTM";
            this.dtpNgayPhatHanhHoaDonTM.Nullable = true;
            this.dtpNgayPhatHanhHoaDonTM.NullButtonText = "Xóa";
            this.dtpNgayPhatHanhHoaDonTM.ShowNullButton = true;
            this.dtpNgayPhatHanhHoaDonTM.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayPhatHanhHoaDonTM.TabIndex = 5;
            this.dtpNgayPhatHanhHoaDonTM.TodayButtonText = "Hôm nay";
            this.dtpNgayPhatHanhHoaDonTM.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtGhiChuHoaDon
            // 
            this.txtGhiChuHoaDon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChuHoaDon.Location = new System.Drawing.Point(164, 58);
            this.txtGhiChuHoaDon.MaxLength = 2000;
            this.txtGhiChuHoaDon.Name = "txtGhiChuHoaDon";
            this.txtGhiChuHoaDon.Size = new System.Drawing.Size(821, 21);
            this.txtGhiChuHoaDon.TabIndex = 6;
            this.txtGhiChuHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHoaDonTM
            // 
            this.txtSoHoaDonTM.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoHoaDonTM.Location = new System.Drawing.Point(164, 20);
            this.txtSoHoaDonTM.MaxLength = 50;
            this.txtSoHoaDonTM.Name = "txtSoHoaDonTM";
            this.txtSoHoaDonTM.Size = new System.Drawing.Size(420, 21);
            this.txtSoHoaDonTM.TabIndex = 4;
            this.txtSoHoaDonTM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(14, 62);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(74, 13);
            this.label29.TabIndex = 46;
            this.label29.Text = "Ghi chú khác :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(603, 24);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(190, 13);
            this.label31.TabIndex = 44;
            this.label31.Text = "Ngày phát hành hóa đơn thương mại :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(12, 24);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(128, 13);
            this.label32.TabIndex = 43;
            this.label32.Text = "Số hóa đơn thương mại  :";
            // 
            // lblFileNameGiayPhep
            // 
            this.lblFileNameGiayPhep.AutoSize = true;
            this.lblFileNameGiayPhep.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameGiayPhep.ForeColor = System.Drawing.Color.Blue;
            this.lblFileNameGiayPhep.Location = new System.Drawing.Point(124, 26);
            this.lblFileNameGiayPhep.Name = "lblFileNameGiayPhep";
            this.lblFileNameGiayPhep.Size = new System.Drawing.Size(63, 13);
            this.lblFileNameGiayPhep.TabIndex = 43;
            this.lblFileNameGiayPhep.Text = "{File Name}";
            // 
            // dtpNgayHetHanGiayPhep
            // 
            this.dtpNgayHetHanGiayPhep.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayHetHanGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayHetHanGiayPhep.DropDownCalendar.Name = "";
            this.dtpNgayHetHanGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayHetHanGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayHetHanGiayPhep.Location = new System.Drawing.Point(827, 106);
            this.dtpNgayHetHanGiayPhep.Name = "dtpNgayHetHanGiayPhep";
            this.dtpNgayHetHanGiayPhep.Nullable = true;
            this.dtpNgayHetHanGiayPhep.NullButtonText = "Xóa";
            this.dtpNgayHetHanGiayPhep.ShowNullButton = true;
            this.dtpNgayHetHanGiayPhep.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayHetHanGiayPhep.TabIndex = 9;
            this.dtpNgayHetHanGiayPhep.TodayButtonText = "Hôm nay";
            this.dtpNgayHetHanGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtpNgayCapGiayPhep
            // 
            this.dtpNgayCapGiayPhep.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayCapGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayCapGiayPhep.DropDownCalendar.Name = "";
            this.dtpNgayCapGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayCapGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayCapGiayPhep.Location = new System.Drawing.Point(827, 26);
            this.dtpNgayCapGiayPhep.Name = "dtpNgayCapGiayPhep";
            this.dtpNgayCapGiayPhep.Nullable = true;
            this.dtpNgayCapGiayPhep.NullButtonText = "Xóa";
            this.dtpNgayCapGiayPhep.ShowNullButton = true;
            this.dtpNgayCapGiayPhep.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayCapGiayPhep.TabIndex = 5;
            this.dtpNgayCapGiayPhep.TodayButtonText = "Hôm nay";
            this.dtpNgayCapGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtNguoiCapGiayPhep
            // 
            this.txtNguoiCapGiayPhep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNguoiCapGiayPhep.Location = new System.Drawing.Point(155, 106);
            this.txtNguoiCapGiayPhep.MaxLength = 255;
            this.txtNguoiCapGiayPhep.Name = "txtNguoiCapGiayPhep";
            this.txtNguoiCapGiayPhep.Size = new System.Drawing.Size(488, 21);
            this.txtNguoiCapGiayPhep.TabIndex = 8;
            this.txtNguoiCapGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChuGiayPhep
            // 
            this.txtGhiChuGiayPhep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChuGiayPhep.Location = new System.Drawing.Point(155, 147);
            this.txtGhiChuGiayPhep.MaxLength = 2000;
            this.txtGhiChuGiayPhep.Name = "txtGhiChuGiayPhep";
            this.txtGhiChuGiayPhep.Size = new System.Drawing.Size(831, 21);
            this.txtGhiChuGiayPhep.TabIndex = 10;
            this.txtGhiChuGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNoiCapGiayPhep
            // 
            this.txtNoiCapGiayPhep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNoiCapGiayPhep.Location = new System.Drawing.Point(155, 67);
            this.txtNoiCapGiayPhep.MaxLength = 255;
            this.txtNoiCapGiayPhep.Name = "txtNoiCapGiayPhep";
            this.txtNoiCapGiayPhep.Size = new System.Drawing.Size(488, 21);
            this.txtNoiCapGiayPhep.TabIndex = 6;
            this.txtNoiCapGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoGiayPhep.Location = new System.Drawing.Point(155, 26);
            this.txtSoGiayPhep.MaxLength = 35;
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(488, 21);
            this.txtSoGiayPhep.TabIndex = 4;
            this.txtSoGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Location = new System.Drawing.Point(675, 110);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(129, 13);
            this.label34.TabIndex = 47;
            this.label34.Text = "Ngày hết hạn giấy phép :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Location = new System.Drawing.Point(21, 110);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 13);
            this.label35.TabIndex = 42;
            this.label35.Text = "Người cấp giấy phép :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Location = new System.Drawing.Point(21, 71);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(99, 13);
            this.label36.TabIndex = 41;
            this.label36.Text = "Nơi cấp giấy phép :";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Location = new System.Drawing.Point(21, 151);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(74, 13);
            this.label37.TabIndex = 46;
            this.label37.Text = "Ghi chú khác :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Location = new System.Drawing.Point(675, 71);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 13);
            this.label38.TabIndex = 45;
            this.label38.Text = "Loại giấy phép :";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Location = new System.Drawing.Point(675, 30);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(109, 13);
            this.label39.TabIndex = 44;
            this.label39.Text = "Ngày cấp giấy phép :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Location = new System.Drawing.Point(21, 30);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(76, 13);
            this.label40.TabIndex = 43;
            this.label40.Text = "Số giấy phép :";
            // 
            // lblFileNamContainer
            // 
            this.lblFileNamContainer.AutoSize = true;
            this.lblFileNamContainer.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNamContainer.ForeColor = System.Drawing.Color.Blue;
            this.lblFileNamContainer.Location = new System.Drawing.Point(124, 26);
            this.lblFileNamContainer.Name = "lblFileNamContainer";
            this.lblFileNamContainer.Size = new System.Drawing.Size(63, 13);
            this.lblFileNamContainer.TabIndex = 43;
            this.lblFileNamContainer.Text = "{File Name}";
            // 
            // dtpNgayDangKyToKhai
            // 
            this.dtpNgayDangKyToKhai.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayDangKyToKhai.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayDangKyToKhai.DropDownCalendar.Name = "";
            this.dtpNgayDangKyToKhai.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayDangKyToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDangKyToKhai.Location = new System.Drawing.Point(578, 21);
            this.dtpNgayDangKyToKhai.Name = "dtpNgayDangKyToKhai";
            this.dtpNgayDangKyToKhai.Nullable = true;
            this.dtpNgayDangKyToKhai.NullButtonText = "Xóa";
            this.dtpNgayDangKyToKhai.ShowNullButton = true;
            this.dtpNgayDangKyToKhai.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayDangKyToKhai.TabIndex = 5;
            this.dtpNgayDangKyToKhai.TodayButtonText = "Hôm nay";
            this.dtpNgayDangKyToKhai.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtGhiChuContainer
            // 
            this.txtGhiChuContainer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChuContainer.Location = new System.Drawing.Point(125, 98);
            this.txtGhiChuContainer.MaxLength = 2000;
            this.txtGhiChuContainer.Name = "txtGhiChuContainer";
            this.txtGhiChuContainer.Size = new System.Drawing.Size(612, 21);
            this.txtGhiChuContainer.TabIndex = 8;
            this.txtGhiChuContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoToKhai.Location = new System.Drawing.Point(125, 21);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(150, 21);
            this.txtSoToKhai.TabIndex = 4;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Location = new System.Drawing.Point(20, 102);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(74, 13);
            this.label43.TabIndex = 42;
            this.label43.Text = "Ghi chú khác :";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Location = new System.Drawing.Point(19, 65);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(70, 13);
            this.label44.TabIndex = 41;
            this.label44.Text = "Mã loại hình :";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Location = new System.Drawing.Point(473, 65);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(72, 13);
            this.label46.TabIndex = 45;
            this.label46.Text = "Mã hải quan :";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Location = new System.Drawing.Point(473, 25);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 44;
            this.label47.Text = "Ngày đăng ký :";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Location = new System.Drawing.Point(19, 25);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(61, 13);
            this.label48.TabIndex = 43;
            this.label48.Text = "Số tờ khai :";
            // 
            // lblFileNameCTKhac
            // 
            this.lblFileNameCTKhac.AutoSize = true;
            this.lblFileNameCTKhac.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameCTKhac.ForeColor = System.Drawing.Color.Blue;
            this.lblFileNameCTKhac.Location = new System.Drawing.Point(124, 22);
            this.lblFileNameCTKhac.Name = "lblFileNameCTKhac";
            this.lblFileNameCTKhac.Size = new System.Drawing.Size(63, 13);
            this.lblFileNameCTKhac.TabIndex = 43;
            this.lblFileNameCTKhac.Text = "{File Name}";
            // 
            // cbbLoaiChungTu
            // 
            this.cbbLoaiChungTu.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem15.FormatStyle.Alpha = 0;
            uiComboBoxItem15.IsSeparator = false;
            uiComboBoxItem15.Text = "Bảng kê chi tiết hàng hóa";
            uiComboBoxItem15.Value = "1";
            uiComboBoxItem16.FormatStyle.Alpha = 0;
            uiComboBoxItem16.IsSeparator = false;
            uiComboBoxItem16.Text = "Thông tin giấy chứng nhận kiểm tra chuyên ngành";
            uiComboBoxItem16.Value = "2";
            uiComboBoxItem17.FormatStyle.Alpha = 0;
            uiComboBoxItem17.IsSeparator = false;
            uiComboBoxItem17.Text = "Chứng từ chứng minh tổ chức, cá nhân đủ điều kiện xuất khẩu, nhập khẩu hàng hóa t" +
                "heo quy định của pháp luật về đầu tư";
            uiComboBoxItem17.Value = "3";
            uiComboBoxItem18.FormatStyle.Alpha = 0;
            uiComboBoxItem18.IsSeparator = false;
            uiComboBoxItem18.Text = "Hợp đồng ủy thác";
            uiComboBoxItem18.Value = "4";
            uiComboBoxItem19.FormatStyle.Alpha = 0;
            uiComboBoxItem19.IsSeparator = false;
            uiComboBoxItem19.Text = "Chứng từ xác định hàng hóa nhập khẩu được áp dụng thuế suất thuế GTGT 5%";
            uiComboBoxItem19.Value = "5";
            uiComboBoxItem20.FormatStyle.Alpha = 0;
            uiComboBoxItem20.IsSeparator = false;
            uiComboBoxItem20.Text = "Chứng từ khác";
            uiComboBoxItem20.Value = "9";
            this.cbbLoaiChungTu.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem15,
            uiComboBoxItem16,
            uiComboBoxItem17,
            uiComboBoxItem18,
            uiComboBoxItem19,
            uiComboBoxItem20});
            this.cbbLoaiChungTu.Location = new System.Drawing.Point(720, 72);
            this.cbbLoaiChungTu.Name = "cbbLoaiChungTu";
            this.cbbLoaiChungTu.Size = new System.Drawing.Size(406, 21);
            this.cbbLoaiChungTu.TabIndex = 7;
            this.cbbLoaiChungTu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // dtpNgayChungTuKhac
            // 
            this.dtpNgayChungTuKhac.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayChungTuKhac.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayChungTuKhac.DropDownCalendar.Name = "";
            this.dtpNgayChungTuKhac.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayChungTuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayChungTuKhac.Location = new System.Drawing.Point(720, 30);
            this.dtpNgayChungTuKhac.Name = "dtpNgayChungTuKhac";
            this.dtpNgayChungTuKhac.Nullable = true;
            this.dtpNgayChungTuKhac.NullButtonText = "Xóa";
            this.dtpNgayChungTuKhac.ShowNullButton = true;
            this.dtpNgayChungTuKhac.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayChungTuKhac.TabIndex = 5;
            this.dtpNgayChungTuKhac.TodayButtonText = "Hôm nay";
            this.dtpNgayChungTuKhac.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtTenChungTuKhac
            // 
            this.txtTenChungTuKhac.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtTenChungTuKhac.Location = new System.Drawing.Point(125, 72);
            this.txtTenChungTuKhac.MaxLength = 255;
            this.txtTenChungTuKhac.Name = "txtTenChungTuKhac";
            this.txtTenChungTuKhac.Size = new System.Drawing.Size(488, 21);
            this.txtTenChungTuKhac.TabIndex = 6;
            this.txtTenChungTuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChuCTKhac
            // 
            this.txtGhiChuCTKhac.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChuCTKhac.Location = new System.Drawing.Point(125, 151);
            this.txtGhiChuCTKhac.MaxLength = 2000;
            this.txtGhiChuCTKhac.Name = "txtGhiChuCTKhac";
            this.txtGhiChuCTKhac.Size = new System.Drawing.Size(488, 21);
            this.txtGhiChuCTKhac.TabIndex = 9;
            this.txtGhiChuCTKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNoiPhatHanhCTKhac
            // 
            this.txtNoiPhatHanhCTKhac.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNoiPhatHanhCTKhac.Location = new System.Drawing.Point(125, 111);
            this.txtNoiPhatHanhCTKhac.MaxLength = 255;
            this.txtNoiPhatHanhCTKhac.Name = "txtNoiPhatHanhCTKhac";
            this.txtNoiPhatHanhCTKhac.Size = new System.Drawing.Size(488, 21);
            this.txtNoiPhatHanhCTKhac.TabIndex = 8;
            this.txtNoiPhatHanhCTKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoChungTuKhac
            // 
            this.txtSoChungTuKhac.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoChungTuKhac.Location = new System.Drawing.Point(125, 30);
            this.txtSoChungTuKhac.MaxLength = 35;
            this.txtSoChungTuKhac.Name = "txtSoChungTuKhac";
            this.txtSoChungTuKhac.Size = new System.Drawing.Size(488, 21);
            this.txtSoChungTuKhac.TabIndex = 4;
            this.txtSoChungTuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Location = new System.Drawing.Point(21, 155);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(74, 13);
            this.label50.TabIndex = 47;
            this.label50.Text = "Ghi chú khác :";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Location = new System.Drawing.Point(20, 115);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(81, 13);
            this.label51.TabIndex = 42;
            this.label51.Text = "Nơi phát hành :";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Location = new System.Drawing.Point(19, 76);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(79, 13);
            this.label52.TabIndex = 41;
            this.label52.Text = "Tên chứng từ :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(619, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 13);
            this.label18.TabIndex = 44;
            this.label18.Text = "Loại chứng từ : ";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Location = new System.Drawing.Point(619, 34);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(86, 13);
            this.label55.TabIndex = 44;
            this.label55.Text = "Ngày chứng từ :";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Location = new System.Drawing.Point(19, 34);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(73, 13);
            this.label56.TabIndex = 43;
            this.label56.Text = "Số chứng từ :";
            // 
            // dtpGioLamTT
            // 
            this.dtpGioLamTT.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Time;
            // 
            // 
            // 
            this.dtpGioLamTT.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpGioLamTT.DropDownCalendar.Name = "";
            this.dtpGioLamTT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpGioLamTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGioLamTT.Location = new System.Drawing.Point(127, 65);
            this.dtpGioLamTT.Name = "dtpGioLamTT";
            this.dtpGioLamTT.Nullable = true;
            this.dtpGioLamTT.NullButtonText = "Xóa";
            this.dtpGioLamTT.ShowNullButton = true;
            this.dtpGioLamTT.ShowUpDown = true;
            this.dtpGioLamTT.Size = new System.Drawing.Size(159, 21);
            this.dtpGioLamTT.TabIndex = 5;
            this.dtpGioLamTT.TodayButtonText = "Hôm nay";
            this.dtpGioLamTT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtpNgayDangKyThuTuc
            // 
            this.dtpNgayDangKyThuTuc.CustomFormat = "HH : MM";
            this.dtpNgayDangKyThuTuc.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dtpNgayDangKyThuTuc.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtpNgayDangKyThuTuc.DropDownCalendar.Name = "";
            this.dtpNgayDangKyThuTuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpNgayDangKyThuTuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDangKyThuTuc.Location = new System.Drawing.Point(127, 27);
            this.dtpNgayDangKyThuTuc.Name = "dtpNgayDangKyThuTuc";
            this.dtpNgayDangKyThuTuc.Nullable = true;
            this.dtpNgayDangKyThuTuc.NullButtonText = "Xóa";
            this.dtpNgayDangKyThuTuc.ShowNullButton = true;
            this.dtpNgayDangKyThuTuc.Size = new System.Drawing.Size(159, 21);
            this.dtpNgayDangKyThuTuc.TabIndex = 4;
            this.dtpNgayDangKyThuTuc.TodayButtonText = "Hôm nay";
            this.dtpNgayDangKyThuTuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNoiDung.Location = new System.Drawing.Point(125, 102);
            this.txtNoiDung.MaxLength = 2000;
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Size = new System.Drawing.Size(593, 21);
            this.txtNoiDung.TabIndex = 6;
            this.txtNoiDung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Location = new System.Drawing.Point(20, 106);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(56, 13);
            this.label59.TabIndex = 42;
            this.label59.Text = "Nội dung :";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Location = new System.Drawing.Point(21, 69);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(85, 13);
            this.label60.TabIndex = 41;
            this.label60.Text = "Giờ làm thủ tục :";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Location = new System.Drawing.Point(21, 31);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(80, 13);
            this.label64.TabIndex = 43;
            this.label64.Text = "Ngày đăng ký :";
            // 
            // lblFileNameCO
            // 
            this.lblFileNameCO.AutoSize = true;
            this.lblFileNameCO.BackColor = System.Drawing.Color.Transparent;
            this.lblFileNameCO.ForeColor = System.Drawing.Color.Blue;
            this.lblFileNameCO.Location = new System.Drawing.Point(124, 27);
            this.lblFileNameCO.Name = "lblFileNameCO";
            this.lblFileNameCO.Size = new System.Drawing.Size(63, 13);
            this.lblFileNameCO.TabIndex = 43;
            this.lblFileNameCO.Text = "{File Name}";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(21, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "Tên File :";
            // 
            // dateNgayCapCO
            // 
            this.dateNgayCapCO.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.dateNgayCapCO.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dateNgayCapCO.DropDownCalendar.Name = "";
            this.dateNgayCapCO.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dateNgayCapCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateNgayCapCO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dateNgayCapCO.Location = new System.Drawing.Point(125, 65);
            this.dateNgayCapCO.Name = "dateNgayCapCO";
            this.dateNgayCapCO.Nullable = true;
            this.dateNgayCapCO.NullButtonText = "Xóa";
            this.dateNgayCapCO.ShowNullButton = true;
            this.dateNgayCapCO.Size = new System.Drawing.Size(159, 21);
            this.dateNgayCapCO.TabIndex = 5;
            this.dateNgayCapCO.TodayButtonText = "Hôm nay";
            this.dateNgayCapCO.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // cbbLoaiCO
            // 
            this.cbbLoaiCO.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Form D";
            uiComboBoxItem1.Value = "D";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Form E";
            uiComboBoxItem2.Value = "E";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Form AK";
            uiComboBoxItem3.Value = "AK";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Form S";
            uiComboBoxItem4.Value = "S";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Form AJ";
            uiComboBoxItem5.Value = "AJ";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Form JV";
            uiComboBoxItem6.Value = "JV";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Form AANZ";
            uiComboBoxItem7.Value = "AANZ";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Form AI";
            uiComboBoxItem8.Value = "AI";
            this.cbbLoaiCO.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbLoaiCO.Location = new System.Drawing.Point(720, 26);
            this.cbbLoaiCO.Name = "cbbLoaiCO";
            this.cbbLoaiCO.Size = new System.Drawing.Size(254, 21);
            this.cbbLoaiCO.TabIndex = 8;
            this.cbbLoaiCO.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtNuocCapCO
            // 
            this.txtNuocCapCO.BackColor = System.Drawing.Color.Transparent;
            this.txtNuocCapCO.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.txtNuocCapCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNuocCapCO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNuocCapCO.Location = new System.Drawing.Point(720, 64);
            this.txtNuocCapCO.Ma = "";
            this.txtNuocCapCO.Name = "txtNuocCapCO";
            this.txtNuocCapCO.ReadOnly = false;
            this.txtNuocCapCO.Size = new System.Drawing.Size(267, 22);
            this.txtNuocCapCO.TabIndex = 9;
            this.txtNuocCapCO.VisualStyleManager = null;
            // 
            // txtGhiChuCO
            // 
            this.txtGhiChuCO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtGhiChuCO.Location = new System.Drawing.Point(721, 107);
            this.txtGhiChuCO.MaxLength = 200;
            this.txtGhiChuCO.Multiline = true;
            this.txtGhiChuCO.Name = "txtGhiChuCO";
            this.txtGhiChuCO.Size = new System.Drawing.Size(345, 61);
            this.txtGhiChuCO.TabIndex = 10;
            this.txtGhiChuCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNguoiCapCO
            // 
            this.txtNguoiCapCO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNguoiCapCO.Location = new System.Drawing.Point(125, 144);
            this.txtNguoiCapCO.MaxLength = 255;
            this.txtNguoiCapCO.Name = "txtNguoiCapCO";
            this.txtNguoiCapCO.Size = new System.Drawing.Size(470, 21);
            this.txtNguoiCapCO.TabIndex = 7;
            this.txtNguoiCapCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtToChucCapCO
            // 
            this.txtToChucCapCO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtToChucCapCO.Location = new System.Drawing.Point(125, 106);
            this.txtToChucCapCO.MaxLength = 255;
            this.txtToChucCapCO.Name = "txtToChucCapCO";
            this.txtToChucCapCO.Size = new System.Drawing.Size(470, 21);
            this.txtToChucCapCO.TabIndex = 6;
            this.txtToChucCapCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoCO
            // 
            this.txtSoCO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoCO.Location = new System.Drawing.Point(125, 26);
            this.txtSoCO.MaxLength = 35;
            this.txtSoCO.Name = "txtSoCO";
            this.txtSoCO.Size = new System.Drawing.Size(470, 21);
            this.txtSoCO.TabIndex = 4;
            this.txtSoCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(21, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 47;
            this.label5.Text = "Người cấp C/O :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(20, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Tổ chức cấp C/O :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(19, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "Ngày cấp C/O :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(631, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "Ghi chú khác :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(631, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Nước cấp C/O :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(631, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 44;
            this.label9.Text = "Loại C/O :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(19, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Số C/O :";
            // 
            // tbCtrVouchers
            // 
            this.tbCtrVouchers.BackColor = System.Drawing.Color.Transparent;
            this.tbCtrVouchers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbCtrVouchers.Location = new System.Drawing.Point(0, 0);
            this.tbCtrVouchers.Name = "tbCtrVouchers";
            this.tbCtrVouchers.Size = new System.Drawing.Size(1136, 665);
            this.tbCtrVouchers.TabIndex = 2;
            this.tbCtrVouchers.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tpCO,
            this.tpVanDon,
            this.tpHopDong,
            this.tpHoaDon,
            this.tpGiayPhep,
            this.tpContainer,
            this.tpCTKhac,
            this.tpLamNgoaiGio});
            this.tbCtrVouchers.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            this.tbCtrVouchers.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.tbCtrVouchers_SelectedTabChanged);
            // 
            // tpCO
            // 
            this.tpCO.Controls.Add(this.uiGroupBox4);
            this.tpCO.Controls.Add(this.uiGroupBox5);
            this.tpCO.Controls.Add(this.uiGroupBox3);
            this.tpCO.Controls.Add(this.uiGroupBox2);
            this.tpCO.Controls.Add(this.uiGroupBox1);
            this.tpCO.Location = new System.Drawing.Point(1, 21);
            this.tpCO.Name = "tpCO";
            this.tpCO.Size = new System.Drawing.Size(1134, 643);
            this.tpCO.TabStop = true;
            this.tpCO.Text = "KHAI BÁO C/O";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.grListCO);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 406);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1134, 192);
            this.uiGroupBox4.TabIndex = 5;
            this.uiGroupBox4.Text = "Danh sách chứng từ C/O khai báo";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListCO
            // 
            this.grListCO.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListCO.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListCO.ColumnAutoResize = true;
            grListCO_DesignTimeLayout.LayoutString = resources.GetString("grListCO_DesignTimeLayout.LayoutString");
            this.grListCO.DesignTimeLayout = grListCO_DesignTimeLayout;
            this.grListCO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListCO.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListCO.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListCO.FrozenColumns = 5;
            this.grListCO.GroupByBoxVisible = false;
            this.grListCO.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListCO.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListCO.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListCO.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListCO.Hierarchical = true;
            this.grListCO.Location = new System.Drawing.Point(3, 17);
            this.grListCO.Name = "grListCO";
            this.grListCO.RecordNavigator = true;
            this.grListCO.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListCO.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListCO.Size = new System.Drawing.Size(1128, 172);
            this.grListCO.TabIndex = 82;
            this.grListCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListCO.VisualStyleManager = this.vsmMain;
            this.grListCO.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListCO_RowDoubleClick);
            this.grListCO.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListCO_LoadingRow);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.chkAutoFeedbackCO);
            this.uiGroupBox5.Controls.Add(this.linkHuongDanKySo);
            this.uiGroupBox5.Controls.Add(this.linkHuongDanKB);
            this.uiGroupBox5.Controls.Add(this.uiButton2);
            this.uiGroupBox5.Controls.Add(this.btnDeleteCO);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 598);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1134, 45);
            this.uiGroupBox5.TabIndex = 4;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkAutoFeedbackCO
            // 
            this.chkAutoFeedbackCO.AutoSize = true;
            this.chkAutoFeedbackCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoFeedbackCO.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoFeedbackCO.Location = new System.Drawing.Point(584, 18);
            this.chkAutoFeedbackCO.Name = "chkAutoFeedbackCO";
            this.chkAutoFeedbackCO.Size = new System.Drawing.Size(338, 17);
            this.chkAutoFeedbackCO.TabIndex = 16;
            this.chkAutoFeedbackCO.Text = "Tự động Nhận phản hồi đến khi có Số tiếp nhận khai báo";
            this.chkAutoFeedbackCO.UseVisualStyleBackColor = true;
            this.chkAutoFeedbackCO.CheckedChanged += new System.EventHandler(this.chkAutoFeedback_CheckedChanged);
            // 
            // linkHuongDanKySo
            // 
            this.linkHuongDanKySo.AutoSize = true;
            this.linkHuongDanKySo.Location = new System.Drawing.Point(241, 21);
            this.linkHuongDanKySo.Name = "linkHuongDanKySo";
            this.linkHuongDanKySo.Size = new System.Drawing.Size(306, 13);
            this.linkHuongDanKySo.TabIndex = 15;
            this.linkHuongDanKySo.TabStop = true;
            this.linkHuongDanKySo.Text = "Hướng dẫn Ký chữ ký số cho File đính kèm trước khi gửi lên HQ";
            this.linkHuongDanKySo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKySo_LinkClicked);
            // 
            // linkHuongDanKB
            // 
            this.linkHuongDanKB.AutoSize = true;
            this.linkHuongDanKB.Location = new System.Drawing.Point(14, 21);
            this.linkHuongDanKB.Name = "linkHuongDanKB";
            this.linkHuongDanKB.Size = new System.Drawing.Size(196, 13);
            this.linkHuongDanKB.TabIndex = 15;
            this.linkHuongDanKB.TabStop = true;
            this.linkHuongDanKB.Text = "Hướng dẫn Khai báo chứng từ đính kèm";
            this.linkHuongDanKB.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKB_LinkClicked);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Image = ((System.Drawing.Image)(resources.GetObject("uiButton2.Image")));
            this.uiButton2.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton2.Location = new System.Drawing.Point(1038, 16);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(76, 23);
            this.uiButton2.TabIndex = 14;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton2.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteCO
            // 
            this.btnDeleteCO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteCO.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteCO.Image")));
            this.btnDeleteCO.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteCO.Location = new System.Drawing.Point(956, 16);
            this.btnDeleteCO.Name = "btnDeleteCO";
            this.btnDeleteCO.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteCO.TabIndex = 13;
            this.btnDeleteCO.Text = "Xóa";
            this.btnDeleteCO.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteCO.Click += new System.EventHandler(this.btnDeleteCO_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnViewCO);
            this.uiGroupBox3.Controls.Add(this.btnAddCO);
            this.uiGroupBox3.Controls.Add(this.label30);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label66);
            this.uiGroupBox3.Controls.Add(this.label45);
            this.uiGroupBox3.Controls.Add(this.txtFileSizeCO);
            this.uiGroupBox3.Controls.Add(this.lblFileNameCO);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 305);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1134, 101);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.Text = "Thông tin File đính kèm";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnViewCO
            // 
            this.btnViewCO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewCO.Image = ((System.Drawing.Image)(resources.GetObject("btnViewCO.Image")));
            this.btnViewCO.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewCO.Location = new System.Drawing.Point(647, 16);
            this.btnViewCO.Name = "btnViewCO";
            this.btnViewCO.Size = new System.Drawing.Size(125, 23);
            this.btnViewCO.TabIndex = 12;
            this.btnViewCO.Text = "Xem File";
            this.btnViewCO.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewCO.Click += new System.EventHandler(this.btnViewCO_Click);
            // 
            // btnAddCO
            // 
            this.btnAddCO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCO.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCO.Image")));
            this.btnAddCO.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddCO.Location = new System.Drawing.Point(520, 16);
            this.btnAddCO.Name = "btnAddCO";
            this.btnAddCO.Size = new System.Drawing.Size(125, 23);
            this.btnAddCO.TabIndex = 11;
            this.btnAddCO.Text = "Thêm File";
            this.btnAddCO.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddCO.Click += new System.EventHandler(this.btnAddCO_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(21, 48);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 13);
            this.label30.TabIndex = 43;
            this.label30.Text = "Dung lượng : ";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label66.Location = new System.Drawing.Point(17, 75);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(851, 13);
            this.label66.TabIndex = 43;
            this.label66.Text = "Phần mềm hỗ trợ ký File định dạng ( .docx ; .xlsx ; .pdf ) . File hình ảnh ( .jpg" +
                " ; .png ; .jpeg ; .... ) phần mềm sẽ Tự động Convert sang PDF để thực hiện ký .";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(241, 48);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(722, 13);
            this.label45.TabIndex = 43;
            this.label45.Text = "Doanh nghiệp chú ý   : Tổng dung lượng File đính kèm không được quá (1MB) và File" +
                " đính kèm phải Ký chữ ký số trước khi đính kèm File vào phần mềm";
            // 
            // txtFileSizeCO
            // 
            this.txtFileSizeCO.AutoSize = true;
            this.txtFileSizeCO.BackColor = System.Drawing.Color.Transparent;
            this.txtFileSizeCO.ForeColor = System.Drawing.Color.Red;
            this.txtFileSizeCO.Location = new System.Drawing.Point(124, 48);
            this.txtFileSizeCO.Name = "txtFileSizeCO";
            this.txtFileSizeCO.Size = new System.Drawing.Size(55, 13);
            this.txtFileSizeCO.TabIndex = 43;
            this.txtFileSizeCO.Text = "{File Size}";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.dateNgayCapCO);
            this.uiGroupBox2.Controls.Add(this.txtSoCO);
            this.uiGroupBox2.Controls.Add(this.cbbLoaiCO);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.txtNuocCapCO);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.txtGhiChuCO);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.txtNguoiCapCO);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.txtToChucCapCO);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1134, 205);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông tin về C/O";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(19, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1047, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Doanh nghiệp chú ý : Nếu các trường Nhập liệu hệ thống bắt buộc phải Nhập liệu nh" +
                "ưng Doanh nghiệp không có thông tin Nhập liệu vào thì Doanh nghiệp Nhập vào trườ" +
                "ng đó giá trị là : N/A";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.label28);
            this.uiGroupBox1.Controls.Add(this.clcNgayTiepNhanCO);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.label42);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhanCO);
            this.uiGroupBox1.Controls.Add(this.txtMaHQCO);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông tin chung";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(328, 26);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(87, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "Ngày tiếp nhận :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(328, 58);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 13);
            this.label28.TabIndex = 43;
            this.label28.Text = "Trạng thái xử lý : ";
            // 
            // clcNgayTiepNhanCO
            // 
            this.clcNgayTiepNhanCO.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.clcNgayTiepNhanCO.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayTiepNhanCO.DropDownCalendar.Name = "";
            this.clcNgayTiepNhanCO.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTiepNhanCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTiepNhanCO.Location = new System.Drawing.Point(436, 21);
            this.clcNgayTiepNhanCO.Name = "clcNgayTiepNhanCO";
            this.clcNgayTiepNhanCO.Nullable = true;
            this.clcNgayTiepNhanCO.NullButtonText = "Xóa";
            this.clcNgayTiepNhanCO.ReadOnly = true;
            this.clcNgayTiepNhanCO.ShowNullButton = true;
            this.clcNgayTiepNhanCO.Size = new System.Drawing.Size(159, 21);
            this.clcNgayTiepNhanCO.TabIndex = 2;
            this.clcNgayTiepNhanCO.TodayButtonText = "Hôm nay";
            this.clcNgayTiepNhanCO.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(19, 58);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 13);
            this.label26.TabIndex = 43;
            this.label26.Text = "Số tiếp nhận  :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(21, 26);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 13);
            this.label22.TabIndex = 43;
            this.label22.Text = "Mã hải quan :";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.ForeColor = System.Drawing.Color.Blue;
            this.label42.Location = new System.Drawing.Point(433, 58);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(86, 13);
            this.label42.TabIndex = 43;
            this.label42.Text = "{Chưa khai báo}";
            // 
            // txtSoTiepNhanCO
            // 
            this.txtSoTiepNhanCO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTiepNhanCO.Location = new System.Drawing.Point(125, 53);
            this.txtSoTiepNhanCO.Name = "txtSoTiepNhanCO";
            this.txtSoTiepNhanCO.ReadOnly = true;
            this.txtSoTiepNhanCO.Size = new System.Drawing.Size(150, 21);
            this.txtSoTiepNhanCO.TabIndex = 3;
            this.txtSoTiepNhanCO.Text = "0";
            this.txtSoTiepNhanCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQCO
            // 
            this.txtMaHQCO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQCO.Location = new System.Drawing.Point(125, 21);
            this.txtMaHQCO.Name = "txtMaHQCO";
            this.txtMaHQCO.ReadOnly = true;
            this.txtMaHQCO.Size = new System.Drawing.Size(150, 21);
            this.txtMaHQCO.TabIndex = 1;
            this.txtMaHQCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // tpVanDon
            // 
            this.tpVanDon.Controls.Add(this.uiGroupBox6);
            this.tpVanDon.Controls.Add(this.uiGroupBox7);
            this.tpVanDon.Controls.Add(this.uiGroupBox8);
            this.tpVanDon.Controls.Add(this.uiGroupBox9);
            this.tpVanDon.Controls.Add(this.uiGroupBox10);
            this.tpVanDon.Location = new System.Drawing.Point(1, 21);
            this.tpVanDon.Name = "tpVanDon";
            this.tpVanDon.Size = new System.Drawing.Size(1134, 643);
            this.tpVanDon.TabStop = true;
            this.tpVanDon.Text = "KHAI BÁO VẬN ĐƠN";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.grListVanDon);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 388);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1134, 210);
            this.uiGroupBox6.TabIndex = 10;
            this.uiGroupBox6.Text = "Danh sách chứng từ vận đơn khai báo";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListVanDon
            // 
            this.grListVanDon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListVanDon.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListVanDon.ColumnAutoResize = true;
            grListVanDon_DesignTimeLayout.LayoutString = resources.GetString("grListVanDon_DesignTimeLayout.LayoutString");
            this.grListVanDon.DesignTimeLayout = grListVanDon_DesignTimeLayout;
            this.grListVanDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListVanDon.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListVanDon.FrozenColumns = 5;
            this.grListVanDon.GroupByBoxVisible = false;
            this.grListVanDon.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListVanDon.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListVanDon.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListVanDon.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListVanDon.Hierarchical = true;
            this.grListVanDon.Location = new System.Drawing.Point(3, 17);
            this.grListVanDon.Name = "grListVanDon";
            this.grListVanDon.RecordNavigator = true;
            this.grListVanDon.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListVanDon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListVanDon.Size = new System.Drawing.Size(1128, 190);
            this.grListVanDon.TabIndex = 87;
            this.grListVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListVanDon.VisualStyleManager = this.vsmMain;
            this.grListVanDon.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListVanDon_RowDoubleClick);
            this.grListVanDon.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListVanDon_LoadingRow);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox38);
            this.uiGroupBox7.Controls.Add(this.uiButton1);
            this.uiGroupBox7.Controls.Add(this.btnDeleteVanDon);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 598);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1134, 45);
            this.uiGroupBox7.TabIndex = 9;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox38
            // 
            this.uiGroupBox38.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox38.Controls.Add(this.chkAutoFeedbackVD);
            this.uiGroupBox38.Controls.Add(this.linkLabel3);
            this.uiGroupBox38.Controls.Add(this.linkLabel4);
            this.uiGroupBox38.Controls.Add(this.uiButton4);
            this.uiGroupBox38.Controls.Add(this.uiButton5);
            this.uiGroupBox38.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox38.Location = new System.Drawing.Point(3, -3);
            this.uiGroupBox38.Name = "uiGroupBox38";
            this.uiGroupBox38.Size = new System.Drawing.Size(1128, 45);
            this.uiGroupBox38.TabIndex = 14;
            this.uiGroupBox38.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkAutoFeedbackVD
            // 
            this.chkAutoFeedbackVD.AutoSize = true;
            this.chkAutoFeedbackVD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoFeedbackVD.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoFeedbackVD.Location = new System.Drawing.Point(569, 19);
            this.chkAutoFeedbackVD.Name = "chkAutoFeedbackVD";
            this.chkAutoFeedbackVD.Size = new System.Drawing.Size(338, 17);
            this.chkAutoFeedbackVD.TabIndex = 17;
            this.chkAutoFeedbackVD.Text = "Tự động Nhận phản hồi đến khi có Số tiếp nhận khai báo";
            this.chkAutoFeedbackVD.UseVisualStyleBackColor = true;
            this.chkAutoFeedbackVD.CheckedChanged += new System.EventHandler(this.chkAutoFeedback_CheckedChanged);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(241, 21);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(306, 13);
            this.linkLabel3.TabIndex = 15;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Hướng dẫn Ký chữ ký số cho File đính kèm trước khi gửi lên HQ";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKySo_LinkClicked);
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(14, 21);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(196, 13);
            this.linkLabel4.TabIndex = 15;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Hướng dẫn Khai báo chứng từ đính kèm";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKB_LinkClicked);
            // 
            // uiButton4
            // 
            this.uiButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton4.Image = ((System.Drawing.Image)(resources.GetObject("uiButton4.Image")));
            this.uiButton4.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton4.Location = new System.Drawing.Point(1032, 16);
            this.uiButton4.Name = "uiButton4";
            this.uiButton4.Size = new System.Drawing.Size(76, 23);
            this.uiButton4.TabIndex = 14;
            this.uiButton4.Text = "Đóng";
            this.uiButton4.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton4.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton5
            // 
            this.uiButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton5.Image = ((System.Drawing.Image)(resources.GetObject("uiButton5.Image")));
            this.uiButton5.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton5.Location = new System.Drawing.Point(950, 16);
            this.uiButton5.Name = "uiButton5";
            this.uiButton5.Size = new System.Drawing.Size(76, 23);
            this.uiButton5.TabIndex = 13;
            this.uiButton5.Text = "Xóa";
            this.uiButton5.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton5.Click += new System.EventHandler(this.btnDeleteVanDon_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton1.Location = new System.Drawing.Point(1038, 16);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(76, 23);
            this.uiButton1.TabIndex = 13;
            this.uiButton1.Text = "Đóng";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteVanDon
            // 
            this.btnDeleteVanDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteVanDon.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVanDon.Image")));
            this.btnDeleteVanDon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteVanDon.Location = new System.Drawing.Point(956, 16);
            this.btnDeleteVanDon.Name = "btnDeleteVanDon";
            this.btnDeleteVanDon.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteVanDon.TabIndex = 12;
            this.btnDeleteVanDon.Text = "Xóa";
            this.btnDeleteVanDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteVanDon.Click += new System.EventHandler(this.btnDeleteVanDon_Click);
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.label69);
            this.uiGroupBox8.Controls.Add(this.lblFileNameVanDon);
            this.uiGroupBox8.Controls.Add(this.btnViewVanDon);
            this.uiGroupBox8.Controls.Add(this.btnAddVanDon);
            this.uiGroupBox8.Controls.Add(this.label53);
            this.uiGroupBox8.Controls.Add(this.label54);
            this.uiGroupBox8.Controls.Add(this.label57);
            this.uiGroupBox8.Controls.Add(this.txtFileSizeVanDon);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 288);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox8.TabIndex = 8;
            this.uiGroupBox8.Text = "Thông tin File đính kèm";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label69.Location = new System.Drawing.Point(21, 75);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(851, 13);
            this.label69.TabIndex = 85;
            this.label69.Text = "Phần mềm hỗ trợ ký File định dạng ( .docx ; .xlsx ; .pdf ) . File hình ảnh ( .jpg" +
                " ; .png ; .jpeg ; .... ) phần mềm sẽ Tự động Convert sang PDF để thực hiện ký .";
            // 
            // btnViewVanDon
            // 
            this.btnViewVanDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewVanDon.Image = ((System.Drawing.Image)(resources.GetObject("btnViewVanDon.Image")));
            this.btnViewVanDon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewVanDon.Location = new System.Drawing.Point(643, 17);
            this.btnViewVanDon.Name = "btnViewVanDon";
            this.btnViewVanDon.Size = new System.Drawing.Size(125, 23);
            this.btnViewVanDon.TabIndex = 11;
            this.btnViewVanDon.Text = "Xem File";
            this.btnViewVanDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewVanDon.Click += new System.EventHandler(this.btnViewVanDon_Click);
            // 
            // btnAddVanDon
            // 
            this.btnAddVanDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddVanDon.Image = ((System.Drawing.Image)(resources.GetObject("btnAddVanDon.Image")));
            this.btnAddVanDon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddVanDon.Location = new System.Drawing.Point(516, 17);
            this.btnAddVanDon.Name = "btnAddVanDon";
            this.btnAddVanDon.Size = new System.Drawing.Size(125, 23);
            this.btnAddVanDon.TabIndex = 10;
            this.btnAddVanDon.Text = "Thêm File";
            this.btnAddVanDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddVanDon.Click += new System.EventHandler(this.btnAddVanDon_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Location = new System.Drawing.Point(21, 48);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(72, 13);
            this.label53.TabIndex = 43;
            this.label53.Text = "Dung lượng : ";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Location = new System.Drawing.Point(21, 26);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(51, 13);
            this.label54.TabIndex = 43;
            this.label54.Text = "Tên File :";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.ForeColor = System.Drawing.Color.Red;
            this.label57.Location = new System.Drawing.Point(241, 48);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(722, 13);
            this.label57.TabIndex = 43;
            this.label57.Text = "Doanh nghiệp chú ý   : Tổng dung lượng File đính kèm không được quá (1MB) và File" +
                " đính kèm phải Ký chữ ký số trước khi đính kèm File vào phần mềm";
            // 
            // txtFileSizeVanDon
            // 
            this.txtFileSizeVanDon.AutoSize = true;
            this.txtFileSizeVanDon.BackColor = System.Drawing.Color.Transparent;
            this.txtFileSizeVanDon.ForeColor = System.Drawing.Color.Red;
            this.txtFileSizeVanDon.Location = new System.Drawing.Point(124, 48);
            this.txtFileSizeVanDon.Name = "txtFileSizeVanDon";
            this.txtFileSizeVanDon.Size = new System.Drawing.Size(55, 13);
            this.txtFileSizeVanDon.TabIndex = 43;
            this.txtFileSizeVanDon.Text = "{File Size}";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.label7);
            this.uiGroupBox9.Controls.Add(this.dtpNgayVanDon);
            this.uiGroupBox9.Controls.Add(this.label16);
            this.uiGroupBox9.Controls.Add(this.cbbLoaiVanDon);
            this.uiGroupBox9.Controls.Add(this.label15);
            this.uiGroupBox9.Controls.Add(this.ctrNuocPHVanDon);
            this.uiGroupBox9.Controls.Add(this.label14);
            this.uiGroupBox9.Controls.Add(this.txtGhiChuVanDon);
            this.uiGroupBox9.Controls.Add(this.label13);
            this.uiGroupBox9.Controls.Add(this.txtDiaDiemCTQC);
            this.uiGroupBox9.Controls.Add(this.label12);
            this.uiGroupBox9.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox9.Controls.Add(this.label8);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(1134, 188);
            this.uiGroupBox9.TabIndex = 7;
            this.uiGroupBox9.Text = "Thông tin về vận đơn";
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(21, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(1047, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "Doanh nghiệp chú ý : Nếu các trường Nhập liệu hệ thống bắt buộc phải Nhập liệu nh" +
                "ưng Doanh nghiệp không có thông tin Nhập liệu vào thì Doanh nghiệp Nhập vào trườ" +
                "ng đó giá trị là : N/A";
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.AutoScroll = true;
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.label70);
            this.uiGroupBox10.Controls.Add(this.label71);
            this.uiGroupBox10.Controls.Add(this.clcNgayTNVanDon);
            this.uiGroupBox10.Controls.Add(this.label72);
            this.uiGroupBox10.Controls.Add(this.label73);
            this.uiGroupBox10.Controls.Add(this.label74);
            this.uiGroupBox10.Controls.Add(this.txtSoTiepNhanVanDon);
            this.uiGroupBox10.Controls.Add(this.txtMaHQVanDon);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox10.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox10.TabIndex = 6;
            this.uiGroupBox10.Text = "Thông tin chung";
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.Location = new System.Drawing.Point(328, 26);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(87, 13);
            this.label70.TabIndex = 43;
            this.label70.Text = "Ngày tiếp nhận :";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.BackColor = System.Drawing.Color.Transparent;
            this.label71.Location = new System.Drawing.Point(328, 58);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(93, 13);
            this.label71.TabIndex = 43;
            this.label71.Text = "Trạng thái xử lý : ";
            // 
            // clcNgayTNVanDon
            // 
            this.clcNgayTNVanDon.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.clcNgayTNVanDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayTNVanDon.DropDownCalendar.Name = "";
            this.clcNgayTNVanDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTNVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTNVanDon.Location = new System.Drawing.Point(436, 21);
            this.clcNgayTNVanDon.Name = "clcNgayTNVanDon";
            this.clcNgayTNVanDon.Nullable = true;
            this.clcNgayTNVanDon.NullButtonText = "Xóa";
            this.clcNgayTNVanDon.ReadOnly = true;
            this.clcNgayTNVanDon.ShowNullButton = true;
            this.clcNgayTNVanDon.Size = new System.Drawing.Size(159, 21);
            this.clcNgayTNVanDon.TabIndex = 2;
            this.clcNgayTNVanDon.TodayButtonText = "Hôm nay";
            this.clcNgayTNVanDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.BackColor = System.Drawing.Color.Transparent;
            this.label72.Location = new System.Drawing.Point(19, 58);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(77, 13);
            this.label72.TabIndex = 43;
            this.label72.Text = "Số tiếp nhận  :";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.BackColor = System.Drawing.Color.Transparent;
            this.label73.Location = new System.Drawing.Point(21, 26);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(72, 13);
            this.label73.TabIndex = 43;
            this.label73.Text = "Mã hải quan :";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.BackColor = System.Drawing.Color.Transparent;
            this.label74.ForeColor = System.Drawing.Color.Blue;
            this.label74.Location = new System.Drawing.Point(433, 58);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(86, 13);
            this.label74.TabIndex = 43;
            this.label74.Text = "{Chưa khai báo}";
            // 
            // txtSoTiepNhanVanDon
            // 
            this.txtSoTiepNhanVanDon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTiepNhanVanDon.Location = new System.Drawing.Point(125, 53);
            this.txtSoTiepNhanVanDon.Name = "txtSoTiepNhanVanDon";
            this.txtSoTiepNhanVanDon.ReadOnly = true;
            this.txtSoTiepNhanVanDon.Size = new System.Drawing.Size(150, 21);
            this.txtSoTiepNhanVanDon.TabIndex = 3;
            this.txtSoTiepNhanVanDon.Text = "0";
            this.txtSoTiepNhanVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQVanDon
            // 
            this.txtMaHQVanDon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQVanDon.Location = new System.Drawing.Point(125, 21);
            this.txtMaHQVanDon.Name = "txtMaHQVanDon";
            this.txtMaHQVanDon.ReadOnly = true;
            this.txtMaHQVanDon.Size = new System.Drawing.Size(150, 21);
            this.txtMaHQVanDon.TabIndex = 1;
            this.txtMaHQVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // tpHopDong
            // 
            this.tpHopDong.Controls.Add(this.uiGroupBox11);
            this.tpHopDong.Controls.Add(this.uiGroupBox12);
            this.tpHopDong.Controls.Add(this.uiGroupBox13);
            this.tpHopDong.Controls.Add(this.uiGroupBox14);
            this.tpHopDong.Controls.Add(this.uiGroupBox15);
            this.tpHopDong.Location = new System.Drawing.Point(1, 21);
            this.tpHopDong.Name = "tpHopDong";
            this.tpHopDong.Size = new System.Drawing.Size(1134, 643);
            this.tpHopDong.TabStop = true;
            this.tpHopDong.Text = "KHAI BÁO HỢP ĐỒNG";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.grListHopDong);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox11.Location = new System.Drawing.Point(0, 351);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1134, 247);
            this.uiGroupBox11.TabIndex = 15;
            this.uiGroupBox11.Text = "Danh sách chứng từ hợp đồng khai báo";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListHopDong
            // 
            this.grListHopDong.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListHopDong.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListHopDong.ColumnAutoResize = true;
            grListHopDong_DesignTimeLayout.LayoutString = resources.GetString("grListHopDong_DesignTimeLayout.LayoutString");
            this.grListHopDong.DesignTimeLayout = grListHopDong_DesignTimeLayout;
            this.grListHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHopDong.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListHopDong.FrozenColumns = 5;
            this.grListHopDong.GroupByBoxVisible = false;
            this.grListHopDong.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHopDong.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListHopDong.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHopDong.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHopDong.Hierarchical = true;
            this.grListHopDong.Location = new System.Drawing.Point(3, 17);
            this.grListHopDong.Name = "grListHopDong";
            this.grListHopDong.RecordNavigator = true;
            this.grListHopDong.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHopDong.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHopDong.Size = new System.Drawing.Size(1128, 227);
            this.grListHopDong.TabIndex = 88;
            this.grListHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListHopDong.VisualStyleManager = this.vsmMain;
            this.grListHopDong.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListHopDong_RowDoubleClick);
            this.grListHopDong.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListHopDong_LoadingRow);
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.uiGroupBox41);
            this.uiGroupBox12.Controls.Add(this.uiButton3);
            this.uiGroupBox12.Controls.Add(this.btnDeleteHopDong);
            this.uiGroupBox12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox12.Location = new System.Drawing.Point(0, 598);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(1134, 45);
            this.uiGroupBox12.TabIndex = 14;
            this.uiGroupBox12.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox41
            // 
            this.uiGroupBox41.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox41.Controls.Add(this.chkAutoFeedbackHD);
            this.uiGroupBox41.Controls.Add(this.linkLabel5);
            this.uiGroupBox41.Controls.Add(this.linkLabel6);
            this.uiGroupBox41.Controls.Add(this.uiButton6);
            this.uiGroupBox41.Controls.Add(this.uiButton8);
            this.uiGroupBox41.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox41.Location = new System.Drawing.Point(3, -3);
            this.uiGroupBox41.Name = "uiGroupBox41";
            this.uiGroupBox41.Size = new System.Drawing.Size(1128, 45);
            this.uiGroupBox41.TabIndex = 13;
            this.uiGroupBox41.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkAutoFeedbackHD
            // 
            this.chkAutoFeedbackHD.AutoSize = true;
            this.chkAutoFeedbackHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoFeedbackHD.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoFeedbackHD.Location = new System.Drawing.Point(564, 19);
            this.chkAutoFeedbackHD.Name = "chkAutoFeedbackHD";
            this.chkAutoFeedbackHD.Size = new System.Drawing.Size(338, 17);
            this.chkAutoFeedbackHD.TabIndex = 17;
            this.chkAutoFeedbackHD.Text = "Tự động Nhận phản hồi đến khi có Số tiếp nhận khai báo";
            this.chkAutoFeedbackHD.UseVisualStyleBackColor = true;
            this.chkAutoFeedbackHD.CheckedChanged += new System.EventHandler(this.chkAutoFeedback_CheckedChanged);
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.Location = new System.Drawing.Point(241, 21);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(306, 13);
            this.linkLabel5.TabIndex = 15;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Hướng dẫn Ký chữ ký số cho File đính kèm trước khi gửi lên HQ";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKySo_LinkClicked);
            // 
            // linkLabel6
            // 
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.Location = new System.Drawing.Point(14, 21);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(196, 13);
            this.linkLabel6.TabIndex = 15;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "Hướng dẫn Khai báo chứng từ đính kèm";
            this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKB_LinkClicked);
            // 
            // uiButton6
            // 
            this.uiButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton6.Image = ((System.Drawing.Image)(resources.GetObject("uiButton6.Image")));
            this.uiButton6.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton6.Location = new System.Drawing.Point(1032, 16);
            this.uiButton6.Name = "uiButton6";
            this.uiButton6.Size = new System.Drawing.Size(76, 23);
            this.uiButton6.TabIndex = 14;
            this.uiButton6.Text = "Đóng";
            this.uiButton6.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton6.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton8
            // 
            this.uiButton8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton8.Image = ((System.Drawing.Image)(resources.GetObject("uiButton8.Image")));
            this.uiButton8.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton8.Location = new System.Drawing.Point(950, 16);
            this.uiButton8.Name = "uiButton8";
            this.uiButton8.Size = new System.Drawing.Size(76, 23);
            this.uiButton8.TabIndex = 13;
            this.uiButton8.Text = "Xóa";
            this.uiButton8.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton8.Click += new System.EventHandler(this.btnDeleteHopDong_Click);
            // 
            // uiButton3
            // 
            this.uiButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton3.Image = ((System.Drawing.Image)(resources.GetObject("uiButton3.Image")));
            this.uiButton3.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton3.Location = new System.Drawing.Point(1038, 16);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(76, 23);
            this.uiButton3.TabIndex = 12;
            this.uiButton3.Text = "Đóng";
            this.uiButton3.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton3.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteHopDong
            // 
            this.btnDeleteHopDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteHopDong.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteHopDong.Image")));
            this.btnDeleteHopDong.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteHopDong.Location = new System.Drawing.Point(956, 16);
            this.btnDeleteHopDong.Name = "btnDeleteHopDong";
            this.btnDeleteHopDong.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteHopDong.TabIndex = 11;
            this.btnDeleteHopDong.Text = "Xóa";
            this.btnDeleteHopDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteHopDong.Click += new System.EventHandler(this.btnDeleteHopDong_Click);
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.AutoScroll = true;
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.label88);
            this.uiGroupBox13.Controls.Add(this.btnViewHopDong);
            this.uiGroupBox13.Controls.Add(this.btnAddHopDong);
            this.uiGroupBox13.Controls.Add(this.label62);
            this.uiGroupBox13.Controls.Add(this.lblFileNameHopDong);
            this.uiGroupBox13.Controls.Add(this.label63);
            this.uiGroupBox13.Controls.Add(this.label65);
            this.uiGroupBox13.Controls.Add(this.txtFileSizeHopDong);
            this.uiGroupBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox13.Location = new System.Drawing.Point(0, 257);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(1134, 94);
            this.uiGroupBox13.TabIndex = 13;
            this.uiGroupBox13.Text = "Thông tin File đính kèm";
            this.uiGroupBox13.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.BackColor = System.Drawing.Color.Transparent;
            this.label88.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label88.Location = new System.Drawing.Point(21, 72);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(851, 13);
            this.label88.TabIndex = 86;
            this.label88.Text = "Phần mềm hỗ trợ ký File định dạng ( .docx ; .xlsx ; .pdf ) . File hình ảnh ( .jpg" +
                " ; .png ; .jpeg ; .... ) phần mềm sẽ Tự động Convert sang PDF để thực hiện ký .";
            // 
            // btnViewHopDong
            // 
            this.btnViewHopDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewHopDong.Image = ((System.Drawing.Image)(resources.GetObject("btnViewHopDong.Image")));
            this.btnViewHopDong.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewHopDong.Location = new System.Drawing.Point(603, 16);
            this.btnViewHopDong.Name = "btnViewHopDong";
            this.btnViewHopDong.Size = new System.Drawing.Size(125, 23);
            this.btnViewHopDong.TabIndex = 10;
            this.btnViewHopDong.Text = "Xem File";
            this.btnViewHopDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewHopDong.Click += new System.EventHandler(this.btnViewHopDong_Click);
            // 
            // btnAddHopDong
            // 
            this.btnAddHopDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddHopDong.Image = ((System.Drawing.Image)(resources.GetObject("btnAddHopDong.Image")));
            this.btnAddHopDong.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddHopDong.Location = new System.Drawing.Point(476, 16);
            this.btnAddHopDong.Name = "btnAddHopDong";
            this.btnAddHopDong.Size = new System.Drawing.Size(125, 23);
            this.btnAddHopDong.TabIndex = 9;
            this.btnAddHopDong.Text = "Thêm File";
            this.btnAddHopDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddHopDong.Click += new System.EventHandler(this.btnAddHopDong_Click);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Location = new System.Drawing.Point(21, 48);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(72, 13);
            this.label62.TabIndex = 43;
            this.label62.Text = "Dung lượng : ";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Location = new System.Drawing.Point(21, 26);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(51, 13);
            this.label63.TabIndex = 43;
            this.label63.Text = "Tên File :";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.ForeColor = System.Drawing.Color.Red;
            this.label65.Location = new System.Drawing.Point(241, 48);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(722, 13);
            this.label65.TabIndex = 43;
            this.label65.Text = "Doanh nghiệp chú ý   : Tổng dung lượng File đính kèm không được quá (1MB) và File" +
                " đính kèm phải Ký chữ ký số trước khi đính kèm File vào phần mềm";
            // 
            // txtFileSizeHopDong
            // 
            this.txtFileSizeHopDong.AutoSize = true;
            this.txtFileSizeHopDong.BackColor = System.Drawing.Color.Transparent;
            this.txtFileSizeHopDong.ForeColor = System.Drawing.Color.Red;
            this.txtFileSizeHopDong.Location = new System.Drawing.Point(124, 48);
            this.txtFileSizeHopDong.Name = "txtFileSizeHopDong";
            this.txtFileSizeHopDong.Size = new System.Drawing.Size(55, 13);
            this.txtFileSizeHopDong.TabIndex = 43;
            this.txtFileSizeHopDong.Text = "{File Size}";
            // 
            // uiGroupBox14
            // 
            this.uiGroupBox14.AutoScroll = true;
            this.uiGroupBox14.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox14.Controls.Add(this.txtTongTriGiaHopDong);
            this.uiGroupBox14.Controls.Add(this.label17);
            this.uiGroupBox14.Controls.Add(this.dtpNgayHopDong);
            this.uiGroupBox14.Controls.Add(this.label24);
            this.uiGroupBox14.Controls.Add(this.dtpThoiHanThanhToan);
            this.uiGroupBox14.Controls.Add(this.label23);
            this.uiGroupBox14.Controls.Add(this.txtGhiChuHopDong);
            this.uiGroupBox14.Controls.Add(this.label21);
            this.uiGroupBox14.Controls.Add(this.label20);
            this.uiGroupBox14.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox14.Controls.Add(this.label19);
            this.uiGroupBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox14.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox14.Name = "uiGroupBox14";
            this.uiGroupBox14.Size = new System.Drawing.Size(1134, 157);
            this.uiGroupBox14.TabIndex = 1;
            this.uiGroupBox14.Text = "Thông tin về hợp đồng";
            this.uiGroupBox14.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtTongTriGiaHopDong
            // 
            this.txtTongTriGiaHopDong.Location = new System.Drawing.Point(587, 62);
            this.txtTongTriGiaHopDong.Name = "txtTongTriGiaHopDong";
            this.txtTongTriGiaHopDong.Size = new System.Drawing.Size(159, 21);
            this.txtTongTriGiaHopDong.TabIndex = 48;
            this.txtTongTriGiaHopDong.Text = "0.00";
            this.txtTongTriGiaHopDong.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtTongTriGiaHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label17.Location = new System.Drawing.Point(21, 130);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(1047, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "Doanh nghiệp chú ý : Nếu các trường Nhập liệu hệ thống bắt buộc phải Nhập liệu nh" +
                "ưng Doanh nghiệp không có thông tin Nhập liệu vào thì Doanh nghiệp Nhập vào trườ" +
                "ng đó giá trị là : N/A";
            // 
            // uiGroupBox15
            // 
            this.uiGroupBox15.AutoScroll = true;
            this.uiGroupBox15.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox15.Controls.Add(this.label78);
            this.uiGroupBox15.Controls.Add(this.label79);
            this.uiGroupBox15.Controls.Add(this.clcNgayTNHopDong);
            this.uiGroupBox15.Controls.Add(this.label80);
            this.uiGroupBox15.Controls.Add(this.label81);
            this.uiGroupBox15.Controls.Add(this.label82);
            this.uiGroupBox15.Controls.Add(this.txtSoTNHopDong);
            this.uiGroupBox15.Controls.Add(this.txtMaHQHopDong);
            this.uiGroupBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox15.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox15.Name = "uiGroupBox15";
            this.uiGroupBox15.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox15.TabIndex = 11;
            this.uiGroupBox15.Text = "Thông tin chung";
            this.uiGroupBox15.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.Transparent;
            this.label78.Location = new System.Drawing.Point(328, 26);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(87, 13);
            this.label78.TabIndex = 43;
            this.label78.Text = "Ngày tiếp nhận :";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.BackColor = System.Drawing.Color.Transparent;
            this.label79.Location = new System.Drawing.Point(328, 58);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(93, 13);
            this.label79.TabIndex = 43;
            this.label79.Text = "Trạng thái xử lý : ";
            // 
            // clcNgayTNHopDong
            // 
            this.clcNgayTNHopDong.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.clcNgayTNHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayTNHopDong.DropDownCalendar.Name = "";
            this.clcNgayTNHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTNHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTNHopDong.Location = new System.Drawing.Point(436, 21);
            this.clcNgayTNHopDong.Name = "clcNgayTNHopDong";
            this.clcNgayTNHopDong.Nullable = true;
            this.clcNgayTNHopDong.NullButtonText = "Xóa";
            this.clcNgayTNHopDong.ReadOnly = true;
            this.clcNgayTNHopDong.ShowNullButton = true;
            this.clcNgayTNHopDong.Size = new System.Drawing.Size(159, 21);
            this.clcNgayTNHopDong.TabIndex = 2;
            this.clcNgayTNHopDong.TodayButtonText = "Hôm nay";
            this.clcNgayTNHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.BackColor = System.Drawing.Color.Transparent;
            this.label80.Location = new System.Drawing.Point(19, 58);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(77, 13);
            this.label80.TabIndex = 43;
            this.label80.Text = "Số tiếp nhận  :";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.BackColor = System.Drawing.Color.Transparent;
            this.label81.Location = new System.Drawing.Point(21, 26);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(72, 13);
            this.label81.TabIndex = 43;
            this.label81.Text = "Mã hải quan :";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.BackColor = System.Drawing.Color.Transparent;
            this.label82.ForeColor = System.Drawing.Color.Blue;
            this.label82.Location = new System.Drawing.Point(433, 58);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(86, 13);
            this.label82.TabIndex = 43;
            this.label82.Text = "{Chưa khai báo}";
            // 
            // txtSoTNHopDong
            // 
            this.txtSoTNHopDong.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTNHopDong.Location = new System.Drawing.Point(125, 53);
            this.txtSoTNHopDong.Name = "txtSoTNHopDong";
            this.txtSoTNHopDong.ReadOnly = true;
            this.txtSoTNHopDong.Size = new System.Drawing.Size(150, 21);
            this.txtSoTNHopDong.TabIndex = 3;
            this.txtSoTNHopDong.Text = "0";
            this.txtSoTNHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQHopDong
            // 
            this.txtMaHQHopDong.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQHopDong.Location = new System.Drawing.Point(125, 21);
            this.txtMaHQHopDong.Name = "txtMaHQHopDong";
            this.txtMaHQHopDong.ReadOnly = true;
            this.txtMaHQHopDong.Size = new System.Drawing.Size(150, 21);
            this.txtMaHQHopDong.TabIndex = 1;
            this.txtMaHQHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // tpHoaDon
            // 
            this.tpHoaDon.Controls.Add(this.uiGroupBox16);
            this.tpHoaDon.Controls.Add(this.uiGroupBox17);
            this.tpHoaDon.Controls.Add(this.uiGroupBox18);
            this.tpHoaDon.Controls.Add(this.uiGroupBox19);
            this.tpHoaDon.Controls.Add(this.uiGroupBox20);
            this.tpHoaDon.Location = new System.Drawing.Point(1, 21);
            this.tpHoaDon.Name = "tpHoaDon";
            this.tpHoaDon.Size = new System.Drawing.Size(1134, 643);
            this.tpHoaDon.TabStop = true;
            this.tpHoaDon.Text = "KHAI BÁO HÓA ĐƠN";
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox16.Controls.Add(this.grListHoaDon);
            this.uiGroupBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox16.Location = new System.Drawing.Point(0, 323);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(1134, 275);
            this.uiGroupBox16.TabIndex = 20;
            this.uiGroupBox16.Text = "Danh sách chứng từ hóa đơn khai báo";
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListHoaDon
            // 
            this.grListHoaDon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListHoaDon.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListHoaDon.ColumnAutoResize = true;
            grListHoaDon_DesignTimeLayout.LayoutString = resources.GetString("grListHoaDon_DesignTimeLayout.LayoutString");
            this.grListHoaDon.DesignTimeLayout = grListHoaDon_DesignTimeLayout;
            this.grListHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHoaDon.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListHoaDon.FrozenColumns = 5;
            this.grListHoaDon.GroupByBoxVisible = false;
            this.grListHoaDon.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHoaDon.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListHoaDon.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHoaDon.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHoaDon.Hierarchical = true;
            this.grListHoaDon.Location = new System.Drawing.Point(3, 17);
            this.grListHoaDon.Name = "grListHoaDon";
            this.grListHoaDon.RecordNavigator = true;
            this.grListHoaDon.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHoaDon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHoaDon.Size = new System.Drawing.Size(1128, 255);
            this.grListHoaDon.TabIndex = 90;
            this.grListHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListHoaDon.VisualStyleManager = this.vsmMain;
            this.grListHoaDon.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListHoaDon_RowDoubleClick);
            this.grListHoaDon.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListHoaDon_LoadingRow);
            // 
            // uiGroupBox17
            // 
            this.uiGroupBox17.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox17.Controls.Add(this.uiGroupBox42);
            this.uiGroupBox17.Controls.Add(this.uiButton7);
            this.uiGroupBox17.Controls.Add(this.btnDeleteHoaDon);
            this.uiGroupBox17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox17.Location = new System.Drawing.Point(0, 598);
            this.uiGroupBox17.Name = "uiGroupBox17";
            this.uiGroupBox17.Size = new System.Drawing.Size(1134, 45);
            this.uiGroupBox17.TabIndex = 19;
            this.uiGroupBox17.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox42
            // 
            this.uiGroupBox42.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox42.Controls.Add(this.chkAutoFeedbackHoaDon);
            this.uiGroupBox42.Controls.Add(this.linkLabel7);
            this.uiGroupBox42.Controls.Add(this.linkLabel8);
            this.uiGroupBox42.Controls.Add(this.uiButton9);
            this.uiGroupBox42.Controls.Add(this.uiButton10);
            this.uiGroupBox42.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox42.Location = new System.Drawing.Point(3, -3);
            this.uiGroupBox42.Name = "uiGroupBox42";
            this.uiGroupBox42.Size = new System.Drawing.Size(1128, 45);
            this.uiGroupBox42.TabIndex = 11;
            this.uiGroupBox42.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkAutoFeedbackHoaDon
            // 
            this.chkAutoFeedbackHoaDon.AutoSize = true;
            this.chkAutoFeedbackHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoFeedbackHoaDon.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoFeedbackHoaDon.Location = new System.Drawing.Point(575, 20);
            this.chkAutoFeedbackHoaDon.Name = "chkAutoFeedbackHoaDon";
            this.chkAutoFeedbackHoaDon.Size = new System.Drawing.Size(338, 17);
            this.chkAutoFeedbackHoaDon.TabIndex = 17;
            this.chkAutoFeedbackHoaDon.Text = "Tự động Nhận phản hồi đến khi có Số tiếp nhận khai báo";
            this.chkAutoFeedbackHoaDon.UseVisualStyleBackColor = true;
            this.chkAutoFeedbackHoaDon.CheckedChanged += new System.EventHandler(this.chkAutoFeedback_CheckedChanged);
            // 
            // linkLabel7
            // 
            this.linkLabel7.AutoSize = true;
            this.linkLabel7.Location = new System.Drawing.Point(241, 21);
            this.linkLabel7.Name = "linkLabel7";
            this.linkLabel7.Size = new System.Drawing.Size(306, 13);
            this.linkLabel7.TabIndex = 15;
            this.linkLabel7.TabStop = true;
            this.linkLabel7.Text = "Hướng dẫn Ký chữ ký số cho File đính kèm trước khi gửi lên HQ";
            this.linkLabel7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKySo_LinkClicked);
            // 
            // linkLabel8
            // 
            this.linkLabel8.AutoSize = true;
            this.linkLabel8.Location = new System.Drawing.Point(14, 21);
            this.linkLabel8.Name = "linkLabel8";
            this.linkLabel8.Size = new System.Drawing.Size(196, 13);
            this.linkLabel8.TabIndex = 15;
            this.linkLabel8.TabStop = true;
            this.linkLabel8.Text = "Hướng dẫn Khai báo chứng từ đính kèm";
            this.linkLabel8.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKB_LinkClicked);
            // 
            // uiButton9
            // 
            this.uiButton9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton9.Image = ((System.Drawing.Image)(resources.GetObject("uiButton9.Image")));
            this.uiButton9.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton9.Location = new System.Drawing.Point(1032, 16);
            this.uiButton9.Name = "uiButton9";
            this.uiButton9.Size = new System.Drawing.Size(76, 23);
            this.uiButton9.TabIndex = 14;
            this.uiButton9.Text = "Đóng";
            this.uiButton9.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton9.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton10
            // 
            this.uiButton10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton10.Image = ((System.Drawing.Image)(resources.GetObject("uiButton10.Image")));
            this.uiButton10.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton10.Location = new System.Drawing.Point(950, 16);
            this.uiButton10.Name = "uiButton10";
            this.uiButton10.Size = new System.Drawing.Size(76, 23);
            this.uiButton10.TabIndex = 13;
            this.uiButton10.Text = "Xóa";
            this.uiButton10.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton10.Click += new System.EventHandler(this.btnDeleteHoaDon_Click);
            // 
            // uiButton7
            // 
            this.uiButton7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton7.Image = ((System.Drawing.Image)(resources.GetObject("uiButton7.Image")));
            this.uiButton7.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton7.Location = new System.Drawing.Point(1038, 16);
            this.uiButton7.Name = "uiButton7";
            this.uiButton7.Size = new System.Drawing.Size(76, 23);
            this.uiButton7.TabIndex = 10;
            this.uiButton7.Text = "Đóng";
            this.uiButton7.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton7.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteHoaDon
            // 
            this.btnDeleteHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteHoaDon.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteHoaDon.Image")));
            this.btnDeleteHoaDon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteHoaDon.Location = new System.Drawing.Point(956, 16);
            this.btnDeleteHoaDon.Name = "btnDeleteHoaDon";
            this.btnDeleteHoaDon.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteHoaDon.TabIndex = 9;
            this.btnDeleteHoaDon.Text = "Xóa";
            this.btnDeleteHoaDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteHoaDon.Click += new System.EventHandler(this.btnDeleteHoaDon_Click);
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.AutoScroll = true;
            this.uiGroupBox18.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox18.Controls.Add(this.label97);
            this.uiGroupBox18.Controls.Add(this.btnViewHoaDon);
            this.uiGroupBox18.Controls.Add(this.btnAddHoaDon);
            this.uiGroupBox18.Controls.Add(this.label61);
            this.uiGroupBox18.Controls.Add(this.lblFileNameHoaDon);
            this.uiGroupBox18.Controls.Add(this.label67);
            this.uiGroupBox18.Controls.Add(this.label68);
            this.uiGroupBox18.Controls.Add(this.txtFileSizeHoaDon);
            this.uiGroupBox18.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox18.Location = new System.Drawing.Point(0, 218);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(1134, 105);
            this.uiGroupBox18.TabIndex = 18;
            this.uiGroupBox18.Text = "Thông tin File đính kèm";
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.BackColor = System.Drawing.Color.Transparent;
            this.label97.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label97.Location = new System.Drawing.Point(21, 78);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(851, 13);
            this.label97.TabIndex = 87;
            this.label97.Text = "Phần mềm hỗ trợ ký File định dạng ( .docx ; .xlsx ; .pdf ) . File hình ảnh ( .jpg" +
                " ; .png ; .jpeg ; .... ) phần mềm sẽ Tự động Convert sang PDF để thực hiện ký .";
            // 
            // btnViewHoaDon
            // 
            this.btnViewHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewHoaDon.Image = ((System.Drawing.Image)(resources.GetObject("btnViewHoaDon.Image")));
            this.btnViewHoaDon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewHoaDon.Location = new System.Drawing.Point(658, 16);
            this.btnViewHoaDon.Name = "btnViewHoaDon";
            this.btnViewHoaDon.Size = new System.Drawing.Size(125, 23);
            this.btnViewHoaDon.TabIndex = 8;
            this.btnViewHoaDon.Text = "Xem File";
            this.btnViewHoaDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewHoaDon.Click += new System.EventHandler(this.btnViewHoaDon_Click);
            // 
            // btnAddHoaDon
            // 
            this.btnAddHoaDon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddHoaDon.Image = ((System.Drawing.Image)(resources.GetObject("btnAddHoaDon.Image")));
            this.btnAddHoaDon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddHoaDon.Location = new System.Drawing.Point(527, 16);
            this.btnAddHoaDon.Name = "btnAddHoaDon";
            this.btnAddHoaDon.Size = new System.Drawing.Size(125, 23);
            this.btnAddHoaDon.TabIndex = 7;
            this.btnAddHoaDon.Text = "Thêm File";
            this.btnAddHoaDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddHoaDon.Click += new System.EventHandler(this.btnAddHoaDon_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Location = new System.Drawing.Point(21, 48);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(72, 13);
            this.label61.TabIndex = 43;
            this.label61.Text = "Dung lượng : ";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Location = new System.Drawing.Point(21, 26);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(51, 13);
            this.label67.TabIndex = 43;
            this.label67.Text = "Tên File :";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.ForeColor = System.Drawing.Color.Red;
            this.label68.Location = new System.Drawing.Point(241, 48);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(722, 13);
            this.label68.TabIndex = 43;
            this.label68.Text = "Doanh nghiệp chú ý   : Tổng dung lượng File đính kèm không được quá (1MB) và File" +
                " đính kèm phải Ký chữ ký số trước khi đính kèm File vào phần mềm";
            // 
            // txtFileSizeHoaDon
            // 
            this.txtFileSizeHoaDon.AutoSize = true;
            this.txtFileSizeHoaDon.BackColor = System.Drawing.Color.Transparent;
            this.txtFileSizeHoaDon.ForeColor = System.Drawing.Color.Red;
            this.txtFileSizeHoaDon.Location = new System.Drawing.Point(124, 48);
            this.txtFileSizeHoaDon.Name = "txtFileSizeHoaDon";
            this.txtFileSizeHoaDon.Size = new System.Drawing.Size(55, 13);
            this.txtFileSizeHoaDon.TabIndex = 43;
            this.txtFileSizeHoaDon.Text = "{File Size}";
            // 
            // uiGroupBox19
            // 
            this.uiGroupBox19.AutoScroll = true;
            this.uiGroupBox19.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox19.Controls.Add(this.label25);
            this.uiGroupBox19.Controls.Add(this.dtpNgayPhatHanhHoaDonTM);
            this.uiGroupBox19.Controls.Add(this.txtSoHoaDonTM);
            this.uiGroupBox19.Controls.Add(this.label29);
            this.uiGroupBox19.Controls.Add(this.txtGhiChuHoaDon);
            this.uiGroupBox19.Controls.Add(this.label31);
            this.uiGroupBox19.Controls.Add(this.label32);
            this.uiGroupBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox19.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox19.Name = "uiGroupBox19";
            this.uiGroupBox19.Size = new System.Drawing.Size(1134, 118);
            this.uiGroupBox19.TabIndex = 17;
            this.uiGroupBox19.Text = "Thông tin hóa đơn";
            this.uiGroupBox19.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label25.Location = new System.Drawing.Point(13, 90);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(1047, 13);
            this.label25.TabIndex = 47;
            this.label25.Text = "Doanh nghiệp chú ý : Nếu các trường Nhập liệu hệ thống bắt buộc phải Nhập liệu nh" +
                "ưng Doanh nghiệp không có thông tin Nhập liệu vào thì Doanh nghiệp Nhập vào trườ" +
                "ng đó giá trị là : N/A";
            // 
            // uiGroupBox20
            // 
            this.uiGroupBox20.AutoScroll = true;
            this.uiGroupBox20.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox20.Controls.Add(this.label75);
            this.uiGroupBox20.Controls.Add(this.label76);
            this.uiGroupBox20.Controls.Add(this.clcNgayTNHoaDon);
            this.uiGroupBox20.Controls.Add(this.label77);
            this.uiGroupBox20.Controls.Add(this.label83);
            this.uiGroupBox20.Controls.Add(this.label84);
            this.uiGroupBox20.Controls.Add(this.txtSoTNHoaDon);
            this.uiGroupBox20.Controls.Add(this.txtMaHQHoaDon);
            this.uiGroupBox20.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox20.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox20.Name = "uiGroupBox20";
            this.uiGroupBox20.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox20.TabIndex = 16;
            this.uiGroupBox20.Text = "Thông tin chung";
            this.uiGroupBox20.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.BackColor = System.Drawing.Color.Transparent;
            this.label75.Location = new System.Drawing.Point(328, 26);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(87, 13);
            this.label75.TabIndex = 43;
            this.label75.Text = "Ngày tiếp nhận :";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.BackColor = System.Drawing.Color.Transparent;
            this.label76.Location = new System.Drawing.Point(328, 58);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(93, 13);
            this.label76.TabIndex = 43;
            this.label76.Text = "Trạng thái xử lý : ";
            // 
            // clcNgayTNHoaDon
            // 
            this.clcNgayTNHoaDon.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.clcNgayTNHoaDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayTNHoaDon.DropDownCalendar.Name = "";
            this.clcNgayTNHoaDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTNHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTNHoaDon.Location = new System.Drawing.Point(436, 21);
            this.clcNgayTNHoaDon.Name = "clcNgayTNHoaDon";
            this.clcNgayTNHoaDon.Nullable = true;
            this.clcNgayTNHoaDon.NullButtonText = "Xóa";
            this.clcNgayTNHoaDon.ReadOnly = true;
            this.clcNgayTNHoaDon.ShowNullButton = true;
            this.clcNgayTNHoaDon.Size = new System.Drawing.Size(159, 21);
            this.clcNgayTNHoaDon.TabIndex = 2;
            this.clcNgayTNHoaDon.TodayButtonText = "Hôm nay";
            this.clcNgayTNHoaDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.BackColor = System.Drawing.Color.Transparent;
            this.label77.Location = new System.Drawing.Point(19, 58);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(77, 13);
            this.label77.TabIndex = 43;
            this.label77.Text = "Số tiếp nhận  :";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.BackColor = System.Drawing.Color.Transparent;
            this.label83.Location = new System.Drawing.Point(21, 26);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(72, 13);
            this.label83.TabIndex = 43;
            this.label83.Text = "Mã hải quan :";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.BackColor = System.Drawing.Color.Transparent;
            this.label84.ForeColor = System.Drawing.Color.Blue;
            this.label84.Location = new System.Drawing.Point(433, 58);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(86, 13);
            this.label84.TabIndex = 43;
            this.label84.Text = "{Chưa khai báo}";
            // 
            // txtSoTNHoaDon
            // 
            this.txtSoTNHoaDon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTNHoaDon.Location = new System.Drawing.Point(125, 53);
            this.txtSoTNHoaDon.Name = "txtSoTNHoaDon";
            this.txtSoTNHoaDon.ReadOnly = true;
            this.txtSoTNHoaDon.Size = new System.Drawing.Size(150, 21);
            this.txtSoTNHoaDon.TabIndex = 3;
            this.txtSoTNHoaDon.Text = "0";
            this.txtSoTNHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQHoaDon
            // 
            this.txtMaHQHoaDon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQHoaDon.Location = new System.Drawing.Point(125, 21);
            this.txtMaHQHoaDon.Name = "txtMaHQHoaDon";
            this.txtMaHQHoaDon.ReadOnly = true;
            this.txtMaHQHoaDon.Size = new System.Drawing.Size(150, 21);
            this.txtMaHQHoaDon.TabIndex = 1;
            this.txtMaHQHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // tpGiayPhep
            // 
            this.tpGiayPhep.Controls.Add(this.uiGroupBox21);
            this.tpGiayPhep.Controls.Add(this.uiGroupBox22);
            this.tpGiayPhep.Controls.Add(this.uiGroupBox23);
            this.tpGiayPhep.Controls.Add(this.uiGroupBox24);
            this.tpGiayPhep.Controls.Add(this.uiGroupBox25);
            this.tpGiayPhep.Location = new System.Drawing.Point(1, 21);
            this.tpGiayPhep.Name = "tpGiayPhep";
            this.tpGiayPhep.Size = new System.Drawing.Size(1134, 643);
            this.tpGiayPhep.TabStop = true;
            this.tpGiayPhep.Text = "KHAI BÁO GIẤY PHÉP";
            // 
            // uiGroupBox21
            // 
            this.uiGroupBox21.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox21.Controls.Add(this.grListGiayPhep);
            this.uiGroupBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox21.Location = new System.Drawing.Point(0, 400);
            this.uiGroupBox21.Name = "uiGroupBox21";
            this.uiGroupBox21.Size = new System.Drawing.Size(1134, 198);
            this.uiGroupBox21.TabIndex = 20;
            this.uiGroupBox21.Text = "Danh sách chứng từ giấy phép khai báo";
            this.uiGroupBox21.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListGiayPhep
            // 
            this.grListGiayPhep.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListGiayPhep.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListGiayPhep.ColumnAutoResize = true;
            grListGiayPhep_DesignTimeLayout.LayoutString = resources.GetString("grListGiayPhep_DesignTimeLayout.LayoutString");
            this.grListGiayPhep.DesignTimeLayout = grListGiayPhep_DesignTimeLayout;
            this.grListGiayPhep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListGiayPhep.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListGiayPhep.FrozenColumns = 5;
            this.grListGiayPhep.GroupByBoxVisible = false;
            this.grListGiayPhep.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListGiayPhep.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListGiayPhep.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListGiayPhep.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListGiayPhep.Hierarchical = true;
            this.grListGiayPhep.Location = new System.Drawing.Point(3, 17);
            this.grListGiayPhep.Name = "grListGiayPhep";
            this.grListGiayPhep.RecordNavigator = true;
            this.grListGiayPhep.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListGiayPhep.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListGiayPhep.Size = new System.Drawing.Size(1128, 178);
            this.grListGiayPhep.TabIndex = 90;
            this.grListGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListGiayPhep.VisualStyleManager = this.vsmMain;
            this.grListGiayPhep.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListGiayPhep_RowDoubleClick);
            this.grListGiayPhep.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListGiayPhep_LoadingRow);
            // 
            // uiGroupBox22
            // 
            this.uiGroupBox22.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox22.Controls.Add(this.uiGroupBox43);
            this.uiGroupBox22.Controls.Add(this.uiButton11);
            this.uiGroupBox22.Controls.Add(this.btnDeleteGiayPhep);
            this.uiGroupBox22.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox22.Location = new System.Drawing.Point(0, 598);
            this.uiGroupBox22.Name = "uiGroupBox22";
            this.uiGroupBox22.Size = new System.Drawing.Size(1134, 45);
            this.uiGroupBox22.TabIndex = 19;
            this.uiGroupBox22.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox43
            // 
            this.uiGroupBox43.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox43.Controls.Add(this.chkAutoFeedbackGP);
            this.uiGroupBox43.Controls.Add(this.linkLabel9);
            this.uiGroupBox43.Controls.Add(this.linkLabel10);
            this.uiGroupBox43.Controls.Add(this.uiButton12);
            this.uiGroupBox43.Controls.Add(this.uiButton13);
            this.uiGroupBox43.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox43.Location = new System.Drawing.Point(3, -3);
            this.uiGroupBox43.Name = "uiGroupBox43";
            this.uiGroupBox43.Size = new System.Drawing.Size(1128, 45);
            this.uiGroupBox43.TabIndex = 15;
            this.uiGroupBox43.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkAutoFeedbackGP
            // 
            this.chkAutoFeedbackGP.AutoSize = true;
            this.chkAutoFeedbackGP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoFeedbackGP.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoFeedbackGP.Location = new System.Drawing.Point(579, 21);
            this.chkAutoFeedbackGP.Name = "chkAutoFeedbackGP";
            this.chkAutoFeedbackGP.Size = new System.Drawing.Size(338, 17);
            this.chkAutoFeedbackGP.TabIndex = 17;
            this.chkAutoFeedbackGP.Text = "Tự động Nhận phản hồi đến khi có Số tiếp nhận khai báo";
            this.chkAutoFeedbackGP.UseVisualStyleBackColor = true;
            this.chkAutoFeedbackGP.CheckedChanged += new System.EventHandler(this.chkAutoFeedback_CheckedChanged);
            // 
            // linkLabel9
            // 
            this.linkLabel9.AutoSize = true;
            this.linkLabel9.Location = new System.Drawing.Point(241, 21);
            this.linkLabel9.Name = "linkLabel9";
            this.linkLabel9.Size = new System.Drawing.Size(306, 13);
            this.linkLabel9.TabIndex = 15;
            this.linkLabel9.TabStop = true;
            this.linkLabel9.Text = "Hướng dẫn Ký chữ ký số cho File đính kèm trước khi gửi lên HQ";
            this.linkLabel9.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKySo_LinkClicked);
            // 
            // linkLabel10
            // 
            this.linkLabel10.AutoSize = true;
            this.linkLabel10.Location = new System.Drawing.Point(14, 21);
            this.linkLabel10.Name = "linkLabel10";
            this.linkLabel10.Size = new System.Drawing.Size(196, 13);
            this.linkLabel10.TabIndex = 15;
            this.linkLabel10.TabStop = true;
            this.linkLabel10.Text = "Hướng dẫn Khai báo chứng từ đính kèm";
            this.linkLabel10.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKB_LinkClicked);
            // 
            // uiButton12
            // 
            this.uiButton12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton12.Image = ((System.Drawing.Image)(resources.GetObject("uiButton12.Image")));
            this.uiButton12.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton12.Location = new System.Drawing.Point(1032, 16);
            this.uiButton12.Name = "uiButton12";
            this.uiButton12.Size = new System.Drawing.Size(76, 23);
            this.uiButton12.TabIndex = 14;
            this.uiButton12.Text = "Đóng";
            this.uiButton12.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton12.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton13
            // 
            this.uiButton13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton13.Image = ((System.Drawing.Image)(resources.GetObject("uiButton13.Image")));
            this.uiButton13.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton13.Location = new System.Drawing.Point(950, 16);
            this.uiButton13.Name = "uiButton13";
            this.uiButton13.Size = new System.Drawing.Size(76, 23);
            this.uiButton13.TabIndex = 13;
            this.uiButton13.Text = "Xóa";
            this.uiButton13.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton13.Click += new System.EventHandler(this.btnDeleteGiayPhep_Click);
            // 
            // uiButton11
            // 
            this.uiButton11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton11.Image = ((System.Drawing.Image)(resources.GetObject("uiButton11.Image")));
            this.uiButton11.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton11.Location = new System.Drawing.Point(1038, 16);
            this.uiButton11.Name = "uiButton11";
            this.uiButton11.Size = new System.Drawing.Size(76, 23);
            this.uiButton11.TabIndex = 14;
            this.uiButton11.Text = "Đóng";
            this.uiButton11.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton11.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteGiayPhep
            // 
            this.btnDeleteGiayPhep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteGiayPhep.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteGiayPhep.Image")));
            this.btnDeleteGiayPhep.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteGiayPhep.Location = new System.Drawing.Point(956, 16);
            this.btnDeleteGiayPhep.Name = "btnDeleteGiayPhep";
            this.btnDeleteGiayPhep.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteGiayPhep.TabIndex = 13;
            this.btnDeleteGiayPhep.Text = "Xóa";
            this.btnDeleteGiayPhep.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteGiayPhep.Click += new System.EventHandler(this.btnDeleteGiayPhep_Click);
            // 
            // uiGroupBox23
            // 
            this.uiGroupBox23.AutoScroll = true;
            this.uiGroupBox23.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox23.Controls.Add(this.label106);
            this.uiGroupBox23.Controls.Add(this.btnViewGiayPhep);
            this.uiGroupBox23.Controls.Add(this.btnAddGiayPhep);
            this.uiGroupBox23.Controls.Add(this.label85);
            this.uiGroupBox23.Controls.Add(this.lblFileNameGiayPhep);
            this.uiGroupBox23.Controls.Add(this.label86);
            this.uiGroupBox23.Controls.Add(this.label87);
            this.uiGroupBox23.Controls.Add(this.txtFileSizeGiayPhep);
            this.uiGroupBox23.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox23.Location = new System.Drawing.Point(0, 299);
            this.uiGroupBox23.Name = "uiGroupBox23";
            this.uiGroupBox23.Size = new System.Drawing.Size(1134, 101);
            this.uiGroupBox23.TabIndex = 18;
            this.uiGroupBox23.Text = "Thông tin File đính kèm";
            this.uiGroupBox23.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.BackColor = System.Drawing.Color.Transparent;
            this.label106.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label106.Location = new System.Drawing.Point(21, 74);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(851, 13);
            this.label106.TabIndex = 87;
            this.label106.Text = "Phần mềm hỗ trợ ký File định dạng ( .docx ; .xlsx ; .pdf ) . File hình ảnh ( .jpg" +
                " ; .png ; .jpeg ; .... ) phần mềm sẽ Tự động Convert sang PDF để thực hiện ký .";
            // 
            // btnViewGiayPhep
            // 
            this.btnViewGiayPhep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewGiayPhep.Image = ((System.Drawing.Image)(resources.GetObject("btnViewGiayPhep.Image")));
            this.btnViewGiayPhep.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewGiayPhep.Location = new System.Drawing.Point(659, 16);
            this.btnViewGiayPhep.Name = "btnViewGiayPhep";
            this.btnViewGiayPhep.Size = new System.Drawing.Size(125, 23);
            this.btnViewGiayPhep.TabIndex = 12;
            this.btnViewGiayPhep.Text = "Xem File";
            this.btnViewGiayPhep.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewGiayPhep.Click += new System.EventHandler(this.btnViewGiayPhep_Click);
            // 
            // btnAddGiayPhep
            // 
            this.btnAddGiayPhep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddGiayPhep.Image = ((System.Drawing.Image)(resources.GetObject("btnAddGiayPhep.Image")));
            this.btnAddGiayPhep.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddGiayPhep.Location = new System.Drawing.Point(532, 16);
            this.btnAddGiayPhep.Name = "btnAddGiayPhep";
            this.btnAddGiayPhep.Size = new System.Drawing.Size(125, 23);
            this.btnAddGiayPhep.TabIndex = 11;
            this.btnAddGiayPhep.Text = "Thêm File";
            this.btnAddGiayPhep.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddGiayPhep.Click += new System.EventHandler(this.btnAddGiayPhep_Click);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.BackColor = System.Drawing.Color.Transparent;
            this.label85.Location = new System.Drawing.Point(21, 48);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(72, 13);
            this.label85.TabIndex = 43;
            this.label85.Text = "Dung lượng : ";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.BackColor = System.Drawing.Color.Transparent;
            this.label86.Location = new System.Drawing.Point(21, 26);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(51, 13);
            this.label86.TabIndex = 43;
            this.label86.Text = "Tên File :";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.BackColor = System.Drawing.Color.Transparent;
            this.label87.ForeColor = System.Drawing.Color.Red;
            this.label87.Location = new System.Drawing.Point(241, 48);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(722, 13);
            this.label87.TabIndex = 43;
            this.label87.Text = "Doanh nghiệp chú ý   : Tổng dung lượng File đính kèm không được quá (1MB) và File" +
                " đính kèm phải Ký chữ ký số trước khi đính kèm File vào phần mềm";
            // 
            // txtFileSizeGiayPhep
            // 
            this.txtFileSizeGiayPhep.AutoSize = true;
            this.txtFileSizeGiayPhep.BackColor = System.Drawing.Color.Transparent;
            this.txtFileSizeGiayPhep.ForeColor = System.Drawing.Color.Red;
            this.txtFileSizeGiayPhep.Location = new System.Drawing.Point(124, 48);
            this.txtFileSizeGiayPhep.Name = "txtFileSizeGiayPhep";
            this.txtFileSizeGiayPhep.Size = new System.Drawing.Size(55, 13);
            this.txtFileSizeGiayPhep.TabIndex = 43;
            this.txtFileSizeGiayPhep.Text = "{File Size}";
            // 
            // uiGroupBox24
            // 
            this.uiGroupBox24.AutoScroll = true;
            this.uiGroupBox24.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox24.Controls.Add(this.label33);
            this.uiGroupBox24.Controls.Add(this.cbbLoaiGiayPhep);
            this.uiGroupBox24.Controls.Add(this.dtpNgayHetHanGiayPhep);
            this.uiGroupBox24.Controls.Add(this.label40);
            this.uiGroupBox24.Controls.Add(this.dtpNgayCapGiayPhep);
            this.uiGroupBox24.Controls.Add(this.label39);
            this.uiGroupBox24.Controls.Add(this.label38);
            this.uiGroupBox24.Controls.Add(this.txtNguoiCapGiayPhep);
            this.uiGroupBox24.Controls.Add(this.label37);
            this.uiGroupBox24.Controls.Add(this.txtGhiChuGiayPhep);
            this.uiGroupBox24.Controls.Add(this.label36);
            this.uiGroupBox24.Controls.Add(this.txtNoiCapGiayPhep);
            this.uiGroupBox24.Controls.Add(this.label35);
            this.uiGroupBox24.Controls.Add(this.txtSoGiayPhep);
            this.uiGroupBox24.Controls.Add(this.label34);
            this.uiGroupBox24.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox24.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox24.Name = "uiGroupBox24";
            this.uiGroupBox24.Size = new System.Drawing.Size(1134, 199);
            this.uiGroupBox24.TabIndex = 17;
            this.uiGroupBox24.Text = "Thông tin về giấy phép";
            this.uiGroupBox24.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label33.Location = new System.Drawing.Point(21, 176);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(1047, 13);
            this.label33.TabIndex = 306;
            this.label33.Text = "Doanh nghiệp chú ý : Nếu các trường Nhập liệu hệ thống bắt buộc phải Nhập liệu nh" +
                "ưng Doanh nghiệp không có thông tin Nhập liệu vào thì Doanh nghiệp Nhập vào trườ" +
                "ng đó giá trị là : N/A";
            // 
            // cbbLoaiGiayPhep
            // 
            cbbLoaiGiayPhep_DesignTimeLayout.LayoutString = resources.GetString("cbbLoaiGiayPhep_DesignTimeLayout.LayoutString");
            this.cbbLoaiGiayPhep.DesignTimeLayout = cbbLoaiGiayPhep_DesignTimeLayout;
            this.cbbLoaiGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoaiGiayPhep.Location = new System.Drawing.Point(827, 66);
            this.cbbLoaiGiayPhep.MaxLength = 4;
            this.cbbLoaiGiayPhep.Name = "cbbLoaiGiayPhep";
            this.cbbLoaiGiayPhep.SelectedIndex = -1;
            this.cbbLoaiGiayPhep.SelectedItem = null;
            this.cbbLoaiGiayPhep.Size = new System.Drawing.Size(292, 21);
            this.cbbLoaiGiayPhep.TabIndex = 305;
            this.cbbLoaiGiayPhep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbbLoaiGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox25
            // 
            this.uiGroupBox25.AutoScroll = true;
            this.uiGroupBox25.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox25.Controls.Add(this.label89);
            this.uiGroupBox25.Controls.Add(this.label90);
            this.uiGroupBox25.Controls.Add(this.clcNgayTNGiayPhep);
            this.uiGroupBox25.Controls.Add(this.label91);
            this.uiGroupBox25.Controls.Add(this.label92);
            this.uiGroupBox25.Controls.Add(this.label93);
            this.uiGroupBox25.Controls.Add(this.txtSoTNGiayPhep);
            this.uiGroupBox25.Controls.Add(this.txtMaHQGiayPhep);
            this.uiGroupBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox25.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox25.Name = "uiGroupBox25";
            this.uiGroupBox25.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox25.TabIndex = 16;
            this.uiGroupBox25.Text = "Thông tin chung";
            this.uiGroupBox25.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.BackColor = System.Drawing.Color.Transparent;
            this.label89.Location = new System.Drawing.Point(328, 26);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(87, 13);
            this.label89.TabIndex = 43;
            this.label89.Text = "Ngày tiếp nhận :";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.BackColor = System.Drawing.Color.Transparent;
            this.label90.Location = new System.Drawing.Point(328, 58);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(93, 13);
            this.label90.TabIndex = 43;
            this.label90.Text = "Trạng thái xử lý : ";
            // 
            // clcNgayTNGiayPhep
            // 
            this.clcNgayTNGiayPhep.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.clcNgayTNGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayTNGiayPhep.DropDownCalendar.Name = "";
            this.clcNgayTNGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTNGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTNGiayPhep.Location = new System.Drawing.Point(436, 21);
            this.clcNgayTNGiayPhep.Name = "clcNgayTNGiayPhep";
            this.clcNgayTNGiayPhep.Nullable = true;
            this.clcNgayTNGiayPhep.NullButtonText = "Xóa";
            this.clcNgayTNGiayPhep.ReadOnly = true;
            this.clcNgayTNGiayPhep.ShowNullButton = true;
            this.clcNgayTNGiayPhep.Size = new System.Drawing.Size(159, 21);
            this.clcNgayTNGiayPhep.TabIndex = 2;
            this.clcNgayTNGiayPhep.TodayButtonText = "Hôm nay";
            this.clcNgayTNGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.BackColor = System.Drawing.Color.Transparent;
            this.label91.Location = new System.Drawing.Point(19, 58);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(77, 13);
            this.label91.TabIndex = 43;
            this.label91.Text = "Số tiếp nhận  :";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.BackColor = System.Drawing.Color.Transparent;
            this.label92.Location = new System.Drawing.Point(21, 26);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(72, 13);
            this.label92.TabIndex = 43;
            this.label92.Text = "Mã hải quan :";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.BackColor = System.Drawing.Color.Transparent;
            this.label93.ForeColor = System.Drawing.Color.Blue;
            this.label93.Location = new System.Drawing.Point(433, 58);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(86, 13);
            this.label93.TabIndex = 43;
            this.label93.Text = "{Chưa khai báo}";
            // 
            // txtSoTNGiayPhep
            // 
            this.txtSoTNGiayPhep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTNGiayPhep.Location = new System.Drawing.Point(125, 53);
            this.txtSoTNGiayPhep.Name = "txtSoTNGiayPhep";
            this.txtSoTNGiayPhep.ReadOnly = true;
            this.txtSoTNGiayPhep.Size = new System.Drawing.Size(150, 21);
            this.txtSoTNGiayPhep.TabIndex = 3;
            this.txtSoTNGiayPhep.Text = "0";
            this.txtSoTNGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQGiayPhep
            // 
            this.txtMaHQGiayPhep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQGiayPhep.Location = new System.Drawing.Point(125, 21);
            this.txtMaHQGiayPhep.Name = "txtMaHQGiayPhep";
            this.txtMaHQGiayPhep.ReadOnly = true;
            this.txtMaHQGiayPhep.Size = new System.Drawing.Size(150, 21);
            this.txtMaHQGiayPhep.TabIndex = 1;
            this.txtMaHQGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // tpContainer
            // 
            this.tpContainer.Controls.Add(this.uiGroupBox26);
            this.tpContainer.Controls.Add(this.uiGroupBox27);
            this.tpContainer.Controls.Add(this.uiGroupBox28);
            this.tpContainer.Controls.Add(this.uiGroupBox29);
            this.tpContainer.Controls.Add(this.uiGroupBox30);
            this.tpContainer.Location = new System.Drawing.Point(1, 21);
            this.tpContainer.Name = "tpContainer";
            this.tpContainer.Size = new System.Drawing.Size(1134, 643);
            this.tpContainer.TabStop = true;
            this.tpContainer.Text = "KHAI BÁO CONTAINER ĐÍNH KÈM";
            // 
            // uiGroupBox26
            // 
            this.uiGroupBox26.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox26.Controls.Add(this.grListContainer);
            this.uiGroupBox26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox26.Location = new System.Drawing.Point(0, 352);
            this.uiGroupBox26.Name = "uiGroupBox26";
            this.uiGroupBox26.Size = new System.Drawing.Size(1134, 246);
            this.uiGroupBox26.TabIndex = 20;
            this.uiGroupBox26.Text = "Danh sách chứng từ Container khai báo";
            this.uiGroupBox26.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListContainer
            // 
            this.grListContainer.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListContainer.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListContainer.ColumnAutoResize = true;
            grListContainer_DesignTimeLayout.LayoutString = resources.GetString("grListContainer_DesignTimeLayout.LayoutString");
            this.grListContainer.DesignTimeLayout = grListContainer_DesignTimeLayout;
            this.grListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListContainer.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListContainer.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListContainer.FrozenColumns = 5;
            this.grListContainer.GroupByBoxVisible = false;
            this.grListContainer.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListContainer.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListContainer.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListContainer.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListContainer.Hierarchical = true;
            this.grListContainer.Location = new System.Drawing.Point(3, 17);
            this.grListContainer.Name = "grListContainer";
            this.grListContainer.RecordNavigator = true;
            this.grListContainer.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListContainer.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListContainer.Size = new System.Drawing.Size(1128, 226);
            this.grListContainer.TabIndex = 91;
            this.grListContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListContainer.VisualStyleManager = this.vsmMain;
            this.grListContainer.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListContainer_RowDoubleClick);
            this.grListContainer.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListContainer_LoadingRow);
            // 
            // uiGroupBox27
            // 
            this.uiGroupBox27.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox27.Controls.Add(this.uiGroupBox44);
            this.uiGroupBox27.Controls.Add(this.uiButton15);
            this.uiGroupBox27.Controls.Add(this.btnDeleteContainer);
            this.uiGroupBox27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox27.Location = new System.Drawing.Point(0, 598);
            this.uiGroupBox27.Name = "uiGroupBox27";
            this.uiGroupBox27.Size = new System.Drawing.Size(1134, 45);
            this.uiGroupBox27.TabIndex = 19;
            this.uiGroupBox27.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox44
            // 
            this.uiGroupBox44.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox44.Controls.Add(this.chkAutoFeedbackCT);
            this.uiGroupBox44.Controls.Add(this.linkLabel11);
            this.uiGroupBox44.Controls.Add(this.linkLabel12);
            this.uiGroupBox44.Controls.Add(this.uiButton14);
            this.uiGroupBox44.Controls.Add(this.uiButton16);
            this.uiGroupBox44.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox44.Location = new System.Drawing.Point(3, -3);
            this.uiGroupBox44.Name = "uiGroupBox44";
            this.uiGroupBox44.Size = new System.Drawing.Size(1128, 45);
            this.uiGroupBox44.TabIndex = 13;
            this.uiGroupBox44.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkAutoFeedbackCT
            // 
            this.chkAutoFeedbackCT.AutoSize = true;
            this.chkAutoFeedbackCT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoFeedbackCT.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoFeedbackCT.Location = new System.Drawing.Point(562, 19);
            this.chkAutoFeedbackCT.Name = "chkAutoFeedbackCT";
            this.chkAutoFeedbackCT.Size = new System.Drawing.Size(338, 17);
            this.chkAutoFeedbackCT.TabIndex = 17;
            this.chkAutoFeedbackCT.Text = "Tự động Nhận phản hồi đến khi có Số tiếp nhận khai báo";
            this.chkAutoFeedbackCT.UseVisualStyleBackColor = true;
            this.chkAutoFeedbackCT.CheckedChanged += new System.EventHandler(this.chkAutoFeedback_CheckedChanged);
            // 
            // linkLabel11
            // 
            this.linkLabel11.AutoSize = true;
            this.linkLabel11.Location = new System.Drawing.Point(241, 21);
            this.linkLabel11.Name = "linkLabel11";
            this.linkLabel11.Size = new System.Drawing.Size(306, 13);
            this.linkLabel11.TabIndex = 15;
            this.linkLabel11.TabStop = true;
            this.linkLabel11.Text = "Hướng dẫn Ký chữ ký số cho File đính kèm trước khi gửi lên HQ";
            this.linkLabel11.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKySo_LinkClicked);
            // 
            // linkLabel12
            // 
            this.linkLabel12.AutoSize = true;
            this.linkLabel12.Location = new System.Drawing.Point(14, 21);
            this.linkLabel12.Name = "linkLabel12";
            this.linkLabel12.Size = new System.Drawing.Size(196, 13);
            this.linkLabel12.TabIndex = 15;
            this.linkLabel12.TabStop = true;
            this.linkLabel12.Text = "Hướng dẫn Khai báo chứng từ đính kèm";
            this.linkLabel12.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKB_LinkClicked);
            // 
            // uiButton14
            // 
            this.uiButton14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton14.Image = ((System.Drawing.Image)(resources.GetObject("uiButton14.Image")));
            this.uiButton14.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton14.Location = new System.Drawing.Point(1032, 16);
            this.uiButton14.Name = "uiButton14";
            this.uiButton14.Size = new System.Drawing.Size(76, 23);
            this.uiButton14.TabIndex = 14;
            this.uiButton14.Text = "Đóng";
            this.uiButton14.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton14.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton16
            // 
            this.uiButton16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton16.Image = ((System.Drawing.Image)(resources.GetObject("uiButton16.Image")));
            this.uiButton16.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton16.Location = new System.Drawing.Point(950, 16);
            this.uiButton16.Name = "uiButton16";
            this.uiButton16.Size = new System.Drawing.Size(76, 23);
            this.uiButton16.TabIndex = 13;
            this.uiButton16.Text = "Xóa";
            this.uiButton16.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton16.Click += new System.EventHandler(this.btnDeleteContainer_Click);
            // 
            // uiButton15
            // 
            this.uiButton15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton15.Image = ((System.Drawing.Image)(resources.GetObject("uiButton15.Image")));
            this.uiButton15.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton15.Location = new System.Drawing.Point(1038, 16);
            this.uiButton15.Name = "uiButton15";
            this.uiButton15.Size = new System.Drawing.Size(76, 23);
            this.uiButton15.TabIndex = 12;
            this.uiButton15.Text = "Đóng";
            this.uiButton15.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton15.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteContainer
            // 
            this.btnDeleteContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteContainer.Image")));
            this.btnDeleteContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteContainer.Location = new System.Drawing.Point(956, 16);
            this.btnDeleteContainer.Name = "btnDeleteContainer";
            this.btnDeleteContainer.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteContainer.TabIndex = 11;
            this.btnDeleteContainer.Text = "Xóa";
            this.btnDeleteContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteContainer.Click += new System.EventHandler(this.btnDeleteContainer_Click);
            // 
            // uiGroupBox28
            // 
            this.uiGroupBox28.AutoScroll = true;
            this.uiGroupBox28.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox28.Controls.Add(this.label112);
            this.uiGroupBox28.Controls.Add(this.lblFileNamContainer);
            this.uiGroupBox28.Controls.Add(this.btnViewContainer);
            this.uiGroupBox28.Controls.Add(this.btnAddContainer);
            this.uiGroupBox28.Controls.Add(this.label94);
            this.uiGroupBox28.Controls.Add(this.label95);
            this.uiGroupBox28.Controls.Add(this.label96);
            this.uiGroupBox28.Controls.Add(this.txtFileSizeContainer);
            this.uiGroupBox28.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox28.Location = new System.Drawing.Point(0, 250);
            this.uiGroupBox28.Name = "uiGroupBox28";
            this.uiGroupBox28.Size = new System.Drawing.Size(1134, 102);
            this.uiGroupBox28.TabIndex = 18;
            this.uiGroupBox28.Text = "Thông tin File đính kèm";
            this.uiGroupBox28.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.BackColor = System.Drawing.Color.Transparent;
            this.label112.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label112.Location = new System.Drawing.Point(21, 75);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(851, 13);
            this.label112.TabIndex = 87;
            this.label112.Text = "Phần mềm hỗ trợ ký File định dạng ( .docx ; .xlsx ; .pdf ) . File hình ảnh ( .jpg" +
                " ; .png ; .jpeg ; .... ) phần mềm sẽ Tự động Convert sang PDF để thực hiện ký .";
            // 
            // btnViewContainer
            // 
            this.btnViewContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnViewContainer.Image")));
            this.btnViewContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewContainer.Location = new System.Drawing.Point(649, 16);
            this.btnViewContainer.Name = "btnViewContainer";
            this.btnViewContainer.Size = new System.Drawing.Size(125, 23);
            this.btnViewContainer.TabIndex = 10;
            this.btnViewContainer.Text = "Xem File";
            this.btnViewContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewContainer.Click += new System.EventHandler(this.btnViewContainer_Click);
            // 
            // btnAddContainer
            // 
            this.btnAddContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnAddContainer.Image")));
            this.btnAddContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddContainer.Location = new System.Drawing.Point(522, 16);
            this.btnAddContainer.Name = "btnAddContainer";
            this.btnAddContainer.Size = new System.Drawing.Size(125, 23);
            this.btnAddContainer.TabIndex = 9;
            this.btnAddContainer.Text = "Thêm File";
            this.btnAddContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddContainer.Click += new System.EventHandler(this.btnAddContainer_Click);
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.BackColor = System.Drawing.Color.Transparent;
            this.label94.Location = new System.Drawing.Point(21, 48);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(72, 13);
            this.label94.TabIndex = 43;
            this.label94.Text = "Dung lượng : ";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.BackColor = System.Drawing.Color.Transparent;
            this.label95.Location = new System.Drawing.Point(21, 26);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(51, 13);
            this.label95.TabIndex = 43;
            this.label95.Text = "Tên File :";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.BackColor = System.Drawing.Color.Transparent;
            this.label96.ForeColor = System.Drawing.Color.Red;
            this.label96.Location = new System.Drawing.Point(241, 48);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(722, 13);
            this.label96.TabIndex = 43;
            this.label96.Text = "Doanh nghiệp chú ý   : Tổng dung lượng File đính kèm không được quá (1MB) và File" +
                " đính kèm phải Ký chữ ký số trước khi đính kèm File vào phần mềm";
            // 
            // txtFileSizeContainer
            // 
            this.txtFileSizeContainer.AutoSize = true;
            this.txtFileSizeContainer.BackColor = System.Drawing.Color.Transparent;
            this.txtFileSizeContainer.ForeColor = System.Drawing.Color.Red;
            this.txtFileSizeContainer.Location = new System.Drawing.Point(124, 48);
            this.txtFileSizeContainer.Name = "txtFileSizeContainer";
            this.txtFileSizeContainer.Size = new System.Drawing.Size(55, 13);
            this.txtFileSizeContainer.TabIndex = 43;
            this.txtFileSizeContainer.Text = "{File Size}";
            // 
            // uiGroupBox29
            // 
            this.uiGroupBox29.AutoScroll = true;
            this.uiGroupBox29.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox29.Controls.Add(this.label41);
            this.uiGroupBox29.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox29.Controls.Add(this.label48);
            this.uiGroupBox29.Controls.Add(this.dtpNgayDangKyToKhai);
            this.uiGroupBox29.Controls.Add(this.label47);
            this.uiGroupBox29.Controls.Add(this.txtGhiChuContainer);
            this.uiGroupBox29.Controls.Add(this.label46);
            this.uiGroupBox29.Controls.Add(this.txtMaHQTK);
            this.uiGroupBox29.Controls.Add(this.txtMaLoaiHinh);
            this.uiGroupBox29.Controls.Add(this.label44);
            this.uiGroupBox29.Controls.Add(this.label43);
            this.uiGroupBox29.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox29.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox29.Name = "uiGroupBox29";
            this.uiGroupBox29.Size = new System.Drawing.Size(1134, 150);
            this.uiGroupBox29.TabIndex = 17;
            this.uiGroupBox29.Text = "Thông tin về tờ khai";
            this.uiGroupBox29.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label41.Location = new System.Drawing.Point(20, 127);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(1047, 13);
            this.label41.TabIndex = 46;
            this.label41.Text = "Doanh nghiệp chú ý : Nếu các trường Nhập liệu hệ thống bắt buộc phải Nhập liệu nh" +
                "ưng Doanh nghiệp không có thông tin Nhập liệu vào thì Doanh nghiệp Nhập vào trườ" +
                "ng đó giá trị là : N/A";
            // 
            // txtMaHQTK
            // 
            this.txtMaHQTK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQTK.Location = new System.Drawing.Point(578, 60);
            this.txtMaHQTK.Name = "txtMaHQTK";
            this.txtMaHQTK.Size = new System.Drawing.Size(159, 21);
            this.txtMaHQTK.TabIndex = 7;
            this.txtMaHQTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaLoaiHinh
            // 
            this.txtMaLoaiHinh.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaLoaiHinh.Location = new System.Drawing.Point(125, 60);
            this.txtMaLoaiHinh.Name = "txtMaLoaiHinh";
            this.txtMaLoaiHinh.Size = new System.Drawing.Size(150, 21);
            this.txtMaLoaiHinh.TabIndex = 6;
            this.txtMaLoaiHinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox30
            // 
            this.uiGroupBox30.AutoScroll = true;
            this.uiGroupBox30.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox30.Controls.Add(this.label98);
            this.uiGroupBox30.Controls.Add(this.label99);
            this.uiGroupBox30.Controls.Add(this.clcNgayTNContainer);
            this.uiGroupBox30.Controls.Add(this.label100);
            this.uiGroupBox30.Controls.Add(this.label101);
            this.uiGroupBox30.Controls.Add(this.label102);
            this.uiGroupBox30.Controls.Add(this.txtSoTNContainer);
            this.uiGroupBox30.Controls.Add(this.txtMaHQContainer);
            this.uiGroupBox30.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox30.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox30.Name = "uiGroupBox30";
            this.uiGroupBox30.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox30.TabIndex = 16;
            this.uiGroupBox30.Text = "Thông tin chung";
            this.uiGroupBox30.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.BackColor = System.Drawing.Color.Transparent;
            this.label98.Location = new System.Drawing.Point(328, 26);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(87, 13);
            this.label98.TabIndex = 43;
            this.label98.Text = "Ngày tiếp nhận :";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.BackColor = System.Drawing.Color.Transparent;
            this.label99.Location = new System.Drawing.Point(328, 58);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(93, 13);
            this.label99.TabIndex = 43;
            this.label99.Text = "Trạng thái xử lý : ";
            // 
            // clcNgayTNContainer
            // 
            this.clcNgayTNContainer.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.clcNgayTNContainer.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayTNContainer.DropDownCalendar.Name = "";
            this.clcNgayTNContainer.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTNContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTNContainer.Location = new System.Drawing.Point(436, 21);
            this.clcNgayTNContainer.Name = "clcNgayTNContainer";
            this.clcNgayTNContainer.Nullable = true;
            this.clcNgayTNContainer.NullButtonText = "Xóa";
            this.clcNgayTNContainer.ReadOnly = true;
            this.clcNgayTNContainer.ShowNullButton = true;
            this.clcNgayTNContainer.Size = new System.Drawing.Size(159, 21);
            this.clcNgayTNContainer.TabIndex = 2;
            this.clcNgayTNContainer.TodayButtonText = "Hôm nay";
            this.clcNgayTNContainer.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.BackColor = System.Drawing.Color.Transparent;
            this.label100.Location = new System.Drawing.Point(19, 58);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(77, 13);
            this.label100.TabIndex = 43;
            this.label100.Text = "Số tiếp nhận  :";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.BackColor = System.Drawing.Color.Transparent;
            this.label101.Location = new System.Drawing.Point(21, 26);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(72, 13);
            this.label101.TabIndex = 43;
            this.label101.Text = "Mã hải quan :";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.BackColor = System.Drawing.Color.Transparent;
            this.label102.ForeColor = System.Drawing.Color.Blue;
            this.label102.Location = new System.Drawing.Point(433, 58);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(86, 13);
            this.label102.TabIndex = 43;
            this.label102.Text = "{Chưa khai báo}";
            // 
            // txtSoTNContainer
            // 
            this.txtSoTNContainer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTNContainer.Location = new System.Drawing.Point(125, 53);
            this.txtSoTNContainer.Name = "txtSoTNContainer";
            this.txtSoTNContainer.ReadOnly = true;
            this.txtSoTNContainer.Size = new System.Drawing.Size(150, 21);
            this.txtSoTNContainer.TabIndex = 3;
            this.txtSoTNContainer.Text = "0";
            this.txtSoTNContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQContainer
            // 
            this.txtMaHQContainer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQContainer.Location = new System.Drawing.Point(125, 21);
            this.txtMaHQContainer.Name = "txtMaHQContainer";
            this.txtMaHQContainer.ReadOnly = true;
            this.txtMaHQContainer.Size = new System.Drawing.Size(150, 21);
            this.txtMaHQContainer.TabIndex = 1;
            this.txtMaHQContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // tpCTKhac
            // 
            this.tpCTKhac.Controls.Add(this.uiGroupBox31);
            this.tpCTKhac.Controls.Add(this.uiGroupBox32);
            this.tpCTKhac.Controls.Add(this.uiGroupBox33);
            this.tpCTKhac.Controls.Add(this.uiGroupBox34);
            this.tpCTKhac.Controls.Add(this.uiGroupBox35);
            this.tpCTKhac.Location = new System.Drawing.Point(1, 21);
            this.tpCTKhac.Name = "tpCTKhac";
            this.tpCTKhac.Size = new System.Drawing.Size(1134, 643);
            this.tpCTKhac.TabStop = true;
            this.tpCTKhac.Text = "KHAI BÁO CHỨNG TỪ KHÁC";
            // 
            // uiGroupBox31
            // 
            this.uiGroupBox31.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox31.Controls.Add(this.grListCTKhac);
            this.uiGroupBox31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox31.Location = new System.Drawing.Point(0, 411);
            this.uiGroupBox31.Name = "uiGroupBox31";
            this.uiGroupBox31.Size = new System.Drawing.Size(1134, 187);
            this.uiGroupBox31.TabIndex = 20;
            this.uiGroupBox31.Text = "Danh sách chứng từ khai báo";
            this.uiGroupBox31.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListCTKhac
            // 
            this.grListCTKhac.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListCTKhac.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListCTKhac.ColumnAutoResize = true;
            grListCTKhac_DesignTimeLayout.LayoutString = resources.GetString("grListCTKhac_DesignTimeLayout.LayoutString");
            this.grListCTKhac.DesignTimeLayout = grListCTKhac_DesignTimeLayout;
            this.grListCTKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListCTKhac.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListCTKhac.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListCTKhac.FrozenColumns = 5;
            this.grListCTKhac.GroupByBoxVisible = false;
            this.grListCTKhac.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListCTKhac.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListCTKhac.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListCTKhac.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListCTKhac.Hierarchical = true;
            this.grListCTKhac.Location = new System.Drawing.Point(3, 17);
            this.grListCTKhac.Name = "grListCTKhac";
            this.grListCTKhac.RecordNavigator = true;
            this.grListCTKhac.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListCTKhac.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListCTKhac.Size = new System.Drawing.Size(1128, 167);
            this.grListCTKhac.TabIndex = 92;
            this.grListCTKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListCTKhac.VisualStyleManager = this.vsmMain;
            this.grListCTKhac.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListCTKhac_RowDoubleClick);
            this.grListCTKhac.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListCTKhac_LoadingRow);
            // 
            // uiGroupBox32
            // 
            this.uiGroupBox32.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox32.Controls.Add(this.uiGroupBox45);
            this.uiGroupBox32.Controls.Add(this.uiButton19);
            this.uiGroupBox32.Controls.Add(this.btnDeleteCTKhac);
            this.uiGroupBox32.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox32.Location = new System.Drawing.Point(0, 598);
            this.uiGroupBox32.Name = "uiGroupBox32";
            this.uiGroupBox32.Size = new System.Drawing.Size(1134, 45);
            this.uiGroupBox32.TabIndex = 19;
            this.uiGroupBox32.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox45
            // 
            this.uiGroupBox45.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox45.Controls.Add(this.chkAutoFeedbackCTK);
            this.uiGroupBox45.Controls.Add(this.linkLabel13);
            this.uiGroupBox45.Controls.Add(this.linkLabel14);
            this.uiGroupBox45.Controls.Add(this.uiButton17);
            this.uiGroupBox45.Controls.Add(this.uiButton18);
            this.uiGroupBox45.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox45.Location = new System.Drawing.Point(3, -3);
            this.uiGroupBox45.Name = "uiGroupBox45";
            this.uiGroupBox45.Size = new System.Drawing.Size(1128, 45);
            this.uiGroupBox45.TabIndex = 14;
            this.uiGroupBox45.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkAutoFeedbackCTK
            // 
            this.chkAutoFeedbackCTK.AutoSize = true;
            this.chkAutoFeedbackCTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoFeedbackCTK.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoFeedbackCTK.Location = new System.Drawing.Point(564, 20);
            this.chkAutoFeedbackCTK.Name = "chkAutoFeedbackCTK";
            this.chkAutoFeedbackCTK.Size = new System.Drawing.Size(338, 17);
            this.chkAutoFeedbackCTK.TabIndex = 17;
            this.chkAutoFeedbackCTK.Text = "Tự động Nhận phản hồi đến khi có Số tiếp nhận khai báo";
            this.chkAutoFeedbackCTK.UseVisualStyleBackColor = true;
            this.chkAutoFeedbackCTK.CheckedChanged += new System.EventHandler(this.chkAutoFeedback_CheckedChanged);
            // 
            // linkLabel13
            // 
            this.linkLabel13.AutoSize = true;
            this.linkLabel13.Location = new System.Drawing.Point(241, 21);
            this.linkLabel13.Name = "linkLabel13";
            this.linkLabel13.Size = new System.Drawing.Size(306, 13);
            this.linkLabel13.TabIndex = 15;
            this.linkLabel13.TabStop = true;
            this.linkLabel13.Text = "Hướng dẫn Ký chữ ký số cho File đính kèm trước khi gửi lên HQ";
            this.linkLabel13.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKySo_LinkClicked);
            // 
            // linkLabel14
            // 
            this.linkLabel14.AutoSize = true;
            this.linkLabel14.Location = new System.Drawing.Point(14, 21);
            this.linkLabel14.Name = "linkLabel14";
            this.linkLabel14.Size = new System.Drawing.Size(196, 13);
            this.linkLabel14.TabIndex = 15;
            this.linkLabel14.TabStop = true;
            this.linkLabel14.Text = "Hướng dẫn Khai báo chứng từ đính kèm";
            this.linkLabel14.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHuongDanKB_LinkClicked);
            // 
            // uiButton17
            // 
            this.uiButton17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton17.Image = ((System.Drawing.Image)(resources.GetObject("uiButton17.Image")));
            this.uiButton17.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton17.Location = new System.Drawing.Point(1032, 16);
            this.uiButton17.Name = "uiButton17";
            this.uiButton17.Size = new System.Drawing.Size(76, 23);
            this.uiButton17.TabIndex = 14;
            this.uiButton17.Text = "Đóng";
            this.uiButton17.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton17.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiButton18
            // 
            this.uiButton18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton18.Image = ((System.Drawing.Image)(resources.GetObject("uiButton18.Image")));
            this.uiButton18.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton18.Location = new System.Drawing.Point(950, 16);
            this.uiButton18.Name = "uiButton18";
            this.uiButton18.Size = new System.Drawing.Size(76, 23);
            this.uiButton18.TabIndex = 13;
            this.uiButton18.Text = "Xóa";
            this.uiButton18.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton18.Click += new System.EventHandler(this.btnDeleteCTKhac_Click);
            // 
            // uiButton19
            // 
            this.uiButton19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton19.Image = ((System.Drawing.Image)(resources.GetObject("uiButton19.Image")));
            this.uiButton19.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton19.Location = new System.Drawing.Point(1038, 16);
            this.uiButton19.Name = "uiButton19";
            this.uiButton19.Size = new System.Drawing.Size(76, 23);
            this.uiButton19.TabIndex = 13;
            this.uiButton19.Text = "Đóng";
            this.uiButton19.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton19.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteCTKhac
            // 
            this.btnDeleteCTKhac.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteCTKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteCTKhac.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteCTKhac.Image")));
            this.btnDeleteCTKhac.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteCTKhac.Location = new System.Drawing.Point(956, 16);
            this.btnDeleteCTKhac.Name = "btnDeleteCTKhac";
            this.btnDeleteCTKhac.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteCTKhac.TabIndex = 12;
            this.btnDeleteCTKhac.Text = "Xóa";
            this.btnDeleteCTKhac.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteCTKhac.Click += new System.EventHandler(this.btnDeleteCTKhac_Click);
            // 
            // uiGroupBox33
            // 
            this.uiGroupBox33.AutoScroll = true;
            this.uiGroupBox33.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox33.Controls.Add(this.label113);
            this.uiGroupBox33.Controls.Add(this.btnViewCTKhac);
            this.uiGroupBox33.Controls.Add(this.btnAddCTKhac);
            this.uiGroupBox33.Controls.Add(this.label103);
            this.uiGroupBox33.Controls.Add(this.lblFileNameCTKhac);
            this.uiGroupBox33.Controls.Add(this.label104);
            this.uiGroupBox33.Controls.Add(this.label105);
            this.uiGroupBox33.Controls.Add(this.txtFileSizeChungTuKhac);
            this.uiGroupBox33.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox33.Location = new System.Drawing.Point(0, 310);
            this.uiGroupBox33.Name = "uiGroupBox33";
            this.uiGroupBox33.Size = new System.Drawing.Size(1134, 101);
            this.uiGroupBox33.TabIndex = 18;
            this.uiGroupBox33.Text = "Thông tin File đính kèm";
            this.uiGroupBox33.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.BackColor = System.Drawing.Color.Transparent;
            this.label113.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label113.Location = new System.Drawing.Point(21, 74);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(851, 13);
            this.label113.TabIndex = 87;
            this.label113.Text = "Phần mềm hỗ trợ ký File định dạng ( .docx ; .xlsx ; .pdf ) . File hình ảnh ( .jpg" +
                " ; .png ; .jpeg ; .... ) phần mềm sẽ Tự động Convert sang PDF để thực hiện ký .";
            // 
            // btnViewCTKhac
            // 
            this.btnViewCTKhac.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewCTKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewCTKhac.Image = ((System.Drawing.Image)(resources.GetObject("btnViewCTKhac.Image")));
            this.btnViewCTKhac.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewCTKhac.Location = new System.Drawing.Point(648, 12);
            this.btnViewCTKhac.Name = "btnViewCTKhac";
            this.btnViewCTKhac.Size = new System.Drawing.Size(125, 23);
            this.btnViewCTKhac.TabIndex = 11;
            this.btnViewCTKhac.Text = "Xem File";
            this.btnViewCTKhac.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewCTKhac.Click += new System.EventHandler(this.btnViewCTKhac_Click);
            // 
            // btnAddCTKhac
            // 
            this.btnAddCTKhac.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddCTKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCTKhac.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCTKhac.Image")));
            this.btnAddCTKhac.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddCTKhac.Location = new System.Drawing.Point(521, 12);
            this.btnAddCTKhac.Name = "btnAddCTKhac";
            this.btnAddCTKhac.Size = new System.Drawing.Size(125, 23);
            this.btnAddCTKhac.TabIndex = 10;
            this.btnAddCTKhac.Text = "Thêm File";
            this.btnAddCTKhac.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddCTKhac.Click += new System.EventHandler(this.btnAddCTKhac_Click);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.BackColor = System.Drawing.Color.Transparent;
            this.label103.Location = new System.Drawing.Point(21, 48);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(72, 13);
            this.label103.TabIndex = 43;
            this.label103.Text = "Dung lượng : ";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.BackColor = System.Drawing.Color.Transparent;
            this.label104.Location = new System.Drawing.Point(21, 26);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(51, 13);
            this.label104.TabIndex = 43;
            this.label104.Text = "Tên File :";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.BackColor = System.Drawing.Color.Transparent;
            this.label105.ForeColor = System.Drawing.Color.Red;
            this.label105.Location = new System.Drawing.Point(241, 48);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(722, 13);
            this.label105.TabIndex = 43;
            this.label105.Text = "Doanh nghiệp chú ý   : Tổng dung lượng File đính kèm không được quá (1MB) và File" +
                " đính kèm phải Ký chữ ký số trước khi đính kèm File vào phần mềm";
            // 
            // txtFileSizeChungTuKhac
            // 
            this.txtFileSizeChungTuKhac.AutoSize = true;
            this.txtFileSizeChungTuKhac.BackColor = System.Drawing.Color.Transparent;
            this.txtFileSizeChungTuKhac.ForeColor = System.Drawing.Color.Red;
            this.txtFileSizeChungTuKhac.Location = new System.Drawing.Point(124, 48);
            this.txtFileSizeChungTuKhac.Name = "txtFileSizeChungTuKhac";
            this.txtFileSizeChungTuKhac.Size = new System.Drawing.Size(55, 13);
            this.txtFileSizeChungTuKhac.TabIndex = 43;
            this.txtFileSizeChungTuKhac.Text = "{File Size}";
            // 
            // uiGroupBox34
            // 
            this.uiGroupBox34.AutoScroll = true;
            this.uiGroupBox34.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox34.Controls.Add(this.label49);
            this.uiGroupBox34.Controls.Add(this.cbbLoaiChungTu);
            this.uiGroupBox34.Controls.Add(this.txtSoChungTuKhac);
            this.uiGroupBox34.Controls.Add(this.dtpNgayChungTuKhac);
            this.uiGroupBox34.Controls.Add(this.label56);
            this.uiGroupBox34.Controls.Add(this.txtTenChungTuKhac);
            this.uiGroupBox34.Controls.Add(this.label55);
            this.uiGroupBox34.Controls.Add(this.txtGhiChuCTKhac);
            this.uiGroupBox34.Controls.Add(this.label18);
            this.uiGroupBox34.Controls.Add(this.txtNoiPhatHanhCTKhac);
            this.uiGroupBox34.Controls.Add(this.label52);
            this.uiGroupBox34.Controls.Add(this.label51);
            this.uiGroupBox34.Controls.Add(this.label50);
            this.uiGroupBox34.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox34.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox34.Name = "uiGroupBox34";
            this.uiGroupBox34.Size = new System.Drawing.Size(1134, 210);
            this.uiGroupBox34.TabIndex = 17;
            this.uiGroupBox34.Text = "Thông tin về chứng từ";
            this.uiGroupBox34.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label49.Location = new System.Drawing.Point(21, 183);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(1047, 13);
            this.label49.TabIndex = 48;
            this.label49.Text = "Doanh nghiệp chú ý : Nếu các trường Nhập liệu hệ thống bắt buộc phải Nhập liệu nh" +
                "ưng Doanh nghiệp không có thông tin Nhập liệu vào thì Doanh nghiệp Nhập vào trườ" +
                "ng đó giá trị là : N/A";
            // 
            // uiGroupBox35
            // 
            this.uiGroupBox35.AutoScroll = true;
            this.uiGroupBox35.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox35.Controls.Add(this.label107);
            this.uiGroupBox35.Controls.Add(this.label108);
            this.uiGroupBox35.Controls.Add(this.clcNgayTNCTKhac);
            this.uiGroupBox35.Controls.Add(this.label109);
            this.uiGroupBox35.Controls.Add(this.label110);
            this.uiGroupBox35.Controls.Add(this.label111);
            this.uiGroupBox35.Controls.Add(this.txtSoTNCTKhac);
            this.uiGroupBox35.Controls.Add(this.txtMaHQCTKhac);
            this.uiGroupBox35.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox35.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox35.Name = "uiGroupBox35";
            this.uiGroupBox35.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox35.TabIndex = 16;
            this.uiGroupBox35.Text = "Thông tin chung";
            this.uiGroupBox35.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.BackColor = System.Drawing.Color.Transparent;
            this.label107.Location = new System.Drawing.Point(328, 26);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(87, 13);
            this.label107.TabIndex = 43;
            this.label107.Text = "Ngày tiếp nhận :";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.BackColor = System.Drawing.Color.Transparent;
            this.label108.Location = new System.Drawing.Point(328, 58);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(93, 13);
            this.label108.TabIndex = 43;
            this.label108.Text = "Trạng thái xử lý : ";
            // 
            // clcNgayTNCTKhac
            // 
            this.clcNgayTNCTKhac.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.clcNgayTNCTKhac.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayTNCTKhac.DropDownCalendar.Name = "";
            this.clcNgayTNCTKhac.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTNCTKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTNCTKhac.Location = new System.Drawing.Point(436, 21);
            this.clcNgayTNCTKhac.Name = "clcNgayTNCTKhac";
            this.clcNgayTNCTKhac.Nullable = true;
            this.clcNgayTNCTKhac.NullButtonText = "Xóa";
            this.clcNgayTNCTKhac.ReadOnly = true;
            this.clcNgayTNCTKhac.ShowNullButton = true;
            this.clcNgayTNCTKhac.Size = new System.Drawing.Size(159, 21);
            this.clcNgayTNCTKhac.TabIndex = 2;
            this.clcNgayTNCTKhac.TodayButtonText = "Hôm nay";
            this.clcNgayTNCTKhac.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.BackColor = System.Drawing.Color.Transparent;
            this.label109.Location = new System.Drawing.Point(19, 58);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(77, 13);
            this.label109.TabIndex = 43;
            this.label109.Text = "Số tiếp nhận  :";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.BackColor = System.Drawing.Color.Transparent;
            this.label110.Location = new System.Drawing.Point(21, 26);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(72, 13);
            this.label110.TabIndex = 43;
            this.label110.Text = "Mã hải quan :";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.BackColor = System.Drawing.Color.Transparent;
            this.label111.ForeColor = System.Drawing.Color.Blue;
            this.label111.Location = new System.Drawing.Point(433, 58);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(86, 13);
            this.label111.TabIndex = 43;
            this.label111.Text = "{Chưa khai báo}";
            // 
            // txtSoTNCTKhac
            // 
            this.txtSoTNCTKhac.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTNCTKhac.Location = new System.Drawing.Point(125, 53);
            this.txtSoTNCTKhac.Name = "txtSoTNCTKhac";
            this.txtSoTNCTKhac.ReadOnly = true;
            this.txtSoTNCTKhac.Size = new System.Drawing.Size(150, 21);
            this.txtSoTNCTKhac.TabIndex = 3;
            this.txtSoTNCTKhac.Text = "0";
            this.txtSoTNCTKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQCTKhac
            // 
            this.txtMaHQCTKhac.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQCTKhac.Location = new System.Drawing.Point(125, 21);
            this.txtMaHQCTKhac.Name = "txtMaHQCTKhac";
            this.txtMaHQCTKhac.ReadOnly = true;
            this.txtMaHQCTKhac.Size = new System.Drawing.Size(150, 21);
            this.txtMaHQCTKhac.TabIndex = 1;
            this.txtMaHQCTKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // tpLamNgoaiGio
            // 
            this.tpLamNgoaiGio.Controls.Add(this.uiGroupBox36);
            this.tpLamNgoaiGio.Controls.Add(this.uiGroupBox37);
            this.tpLamNgoaiGio.Controls.Add(this.uiGroupBox39);
            this.tpLamNgoaiGio.Controls.Add(this.uiGroupBox40);
            this.tpLamNgoaiGio.Location = new System.Drawing.Point(1, 21);
            this.tpLamNgoaiGio.Name = "tpLamNgoaiGio";
            this.tpLamNgoaiGio.Size = new System.Drawing.Size(1134, 643);
            this.tpLamNgoaiGio.TabStop = true;
            this.tpLamNgoaiGio.Text = "KHAI BÁO ĐĂNG KÝ LÀM NGOÀI GIỜ";
            // 
            // uiGroupBox36
            // 
            this.uiGroupBox36.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox36.Controls.Add(this.grListDKLNG);
            this.uiGroupBox36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox36.Location = new System.Drawing.Point(0, 254);
            this.uiGroupBox36.Name = "uiGroupBox36";
            this.uiGroupBox36.Size = new System.Drawing.Size(1134, 344);
            this.uiGroupBox36.TabIndex = 20;
            this.uiGroupBox36.Text = "Danh sách đăng ký làm ngoài giờ khai báo";
            this.uiGroupBox36.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListDKLNG
            // 
            this.grListDKLNG.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListDKLNG.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListDKLNG.ColumnAutoResize = true;
            grListDKLNG_DesignTimeLayout.LayoutString = resources.GetString("grListDKLNG_DesignTimeLayout.LayoutString");
            this.grListDKLNG.DesignTimeLayout = grListDKLNG_DesignTimeLayout;
            this.grListDKLNG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListDKLNG.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListDKLNG.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListDKLNG.FrozenColumns = 5;
            this.grListDKLNG.GroupByBoxVisible = false;
            this.grListDKLNG.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListDKLNG.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListDKLNG.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListDKLNG.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListDKLNG.Hierarchical = true;
            this.grListDKLNG.Location = new System.Drawing.Point(3, 17);
            this.grListDKLNG.Name = "grListDKLNG";
            this.grListDKLNG.RecordNavigator = true;
            this.grListDKLNG.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListDKLNG.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListDKLNG.Size = new System.Drawing.Size(1128, 324);
            this.grListDKLNG.TabIndex = 93;
            this.grListDKLNG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListDKLNG.VisualStyleManager = this.vsmMain;
            this.grListDKLNG.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListDKLNG_RowDoubleClick);
            this.grListDKLNG.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListDKLNG_LoadingRow);
            // 
            // uiGroupBox37
            // 
            this.uiGroupBox37.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox37.Controls.Add(this.chkAutoFeedbackOT);
            this.uiGroupBox37.Controls.Add(this.uiButton23);
            this.uiGroupBox37.Controls.Add(this.btnDeleteOverTime);
            this.uiGroupBox37.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox37.Location = new System.Drawing.Point(0, 598);
            this.uiGroupBox37.Name = "uiGroupBox37";
            this.uiGroupBox37.Size = new System.Drawing.Size(1134, 45);
            this.uiGroupBox37.TabIndex = 19;
            this.uiGroupBox37.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkAutoFeedbackOT
            // 
            this.chkAutoFeedbackOT.AutoSize = true;
            this.chkAutoFeedbackOT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAutoFeedbackOT.ForeColor = System.Drawing.Color.Blue;
            this.chkAutoFeedbackOT.Location = new System.Drawing.Point(11, 17);
            this.chkAutoFeedbackOT.Name = "chkAutoFeedbackOT";
            this.chkAutoFeedbackOT.Size = new System.Drawing.Size(338, 17);
            this.chkAutoFeedbackOT.TabIndex = 17;
            this.chkAutoFeedbackOT.Text = "Tự động Nhận phản hồi đến khi có Số tiếp nhận khai báo";
            this.chkAutoFeedbackOT.UseVisualStyleBackColor = true;
            this.chkAutoFeedbackOT.CheckedChanged += new System.EventHandler(this.chkAutoFeedback_CheckedChanged);
            // 
            // uiButton23
            // 
            this.uiButton23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton23.Image = ((System.Drawing.Image)(resources.GetObject("uiButton23.Image")));
            this.uiButton23.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton23.Location = new System.Drawing.Point(1038, 16);
            this.uiButton23.Name = "uiButton23";
            this.uiButton23.Size = new System.Drawing.Size(76, 23);
            this.uiButton23.TabIndex = 8;
            this.uiButton23.Text = "Đóng";
            this.uiButton23.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton23.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteOverTime
            // 
            this.btnDeleteOverTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteOverTime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteOverTime.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteOverTime.Image")));
            this.btnDeleteOverTime.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteOverTime.Location = new System.Drawing.Point(956, 16);
            this.btnDeleteOverTime.Name = "btnDeleteOverTime";
            this.btnDeleteOverTime.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteOverTime.TabIndex = 7;
            this.btnDeleteOverTime.Text = "Xóa";
            this.btnDeleteOverTime.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteOverTime.Click += new System.EventHandler(this.btnDeleteOverTime_Click);
            // 
            // uiGroupBox39
            // 
            this.uiGroupBox39.AutoScroll = true;
            this.uiGroupBox39.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox39.Controls.Add(this.label58);
            this.uiGroupBox39.Controls.Add(this.dtpGioLamTT);
            this.uiGroupBox39.Controls.Add(this.label64);
            this.uiGroupBox39.Controls.Add(this.dtpNgayDangKyThuTuc);
            this.uiGroupBox39.Controls.Add(this.label60);
            this.uiGroupBox39.Controls.Add(this.label59);
            this.uiGroupBox39.Controls.Add(this.txtNoiDung);
            this.uiGroupBox39.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox39.Location = new System.Drawing.Point(0, 100);
            this.uiGroupBox39.Name = "uiGroupBox39";
            this.uiGroupBox39.Size = new System.Drawing.Size(1134, 154);
            this.uiGroupBox39.TabIndex = 17;
            this.uiGroupBox39.Text = "Thông tin về đăng ký làm ngoài giờ";
            this.uiGroupBox39.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label58.Location = new System.Drawing.Point(20, 132);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(1047, 13);
            this.label58.TabIndex = 44;
            this.label58.Text = "Doanh nghiệp chú ý : Nếu các trường Nhập liệu hệ thống bắt buộc phải Nhập liệu nh" +
                "ưng Doanh nghiệp không có thông tin Nhập liệu vào thì Doanh nghiệp Nhập vào trườ" +
                "ng đó giá trị là : N/A";
            // 
            // uiGroupBox40
            // 
            this.uiGroupBox40.AutoScroll = true;
            this.uiGroupBox40.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox40.Controls.Add(this.label116);
            this.uiGroupBox40.Controls.Add(this.label117);
            this.uiGroupBox40.Controls.Add(this.clcNgayTNDKLNG);
            this.uiGroupBox40.Controls.Add(this.label118);
            this.uiGroupBox40.Controls.Add(this.label119);
            this.uiGroupBox40.Controls.Add(this.label120);
            this.uiGroupBox40.Controls.Add(this.txtSoTNDKLNG);
            this.uiGroupBox40.Controls.Add(this.txtMaHQDKLNG);
            this.uiGroupBox40.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox40.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox40.Name = "uiGroupBox40";
            this.uiGroupBox40.Size = new System.Drawing.Size(1134, 100);
            this.uiGroupBox40.TabIndex = 16;
            this.uiGroupBox40.Text = "Thông tin chung";
            this.uiGroupBox40.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.BackColor = System.Drawing.Color.Transparent;
            this.label116.Location = new System.Drawing.Point(328, 26);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(87, 13);
            this.label116.TabIndex = 43;
            this.label116.Text = "Ngày tiếp nhận :";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.BackColor = System.Drawing.Color.Transparent;
            this.label117.Location = new System.Drawing.Point(328, 58);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(93, 13);
            this.label117.TabIndex = 43;
            this.label117.Text = "Trạng thái xử lý : ";
            // 
            // clcNgayTNDKLNG
            // 
            this.clcNgayTNDKLNG.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.clcNgayTNDKLNG.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayTNDKLNG.DropDownCalendar.Name = "";
            this.clcNgayTNDKLNG.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTNDKLNG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTNDKLNG.Location = new System.Drawing.Point(436, 21);
            this.clcNgayTNDKLNG.Name = "clcNgayTNDKLNG";
            this.clcNgayTNDKLNG.Nullable = true;
            this.clcNgayTNDKLNG.NullButtonText = "Xóa";
            this.clcNgayTNDKLNG.ReadOnly = true;
            this.clcNgayTNDKLNG.ShowNullButton = true;
            this.clcNgayTNDKLNG.Size = new System.Drawing.Size(159, 21);
            this.clcNgayTNDKLNG.TabIndex = 2;
            this.clcNgayTNDKLNG.TodayButtonText = "Hôm nay";
            this.clcNgayTNDKLNG.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.BackColor = System.Drawing.Color.Transparent;
            this.label118.Location = new System.Drawing.Point(19, 58);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(77, 13);
            this.label118.TabIndex = 43;
            this.label118.Text = "Số tiếp nhận  :";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.BackColor = System.Drawing.Color.Transparent;
            this.label119.Location = new System.Drawing.Point(21, 26);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(72, 13);
            this.label119.TabIndex = 43;
            this.label119.Text = "Mã hải quan :";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.BackColor = System.Drawing.Color.Transparent;
            this.label120.ForeColor = System.Drawing.Color.Blue;
            this.label120.Location = new System.Drawing.Point(433, 58);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(86, 13);
            this.label120.TabIndex = 43;
            this.label120.Text = "{Chưa khai báo}";
            // 
            // txtSoTNDKLNG
            // 
            this.txtSoTNDKLNG.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoTNDKLNG.Location = new System.Drawing.Point(125, 53);
            this.txtSoTNDKLNG.Name = "txtSoTNDKLNG";
            this.txtSoTNDKLNG.ReadOnly = true;
            this.txtSoTNDKLNG.Size = new System.Drawing.Size(150, 21);
            this.txtSoTNDKLNG.TabIndex = 3;
            this.txtSoTNDKLNG.Text = "0";
            this.txtSoTNDKLNG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHQDKLNG
            // 
            this.txtMaHQDKLNG.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaHQDKLNG.Location = new System.Drawing.Point(125, 21);
            this.txtMaHQDKLNG.Name = "txtMaHQDKLNG";
            this.txtMaHQDKLNG.ReadOnly = true;
            this.txtMaHQDKLNG.Size = new System.Drawing.Size(150, 21);
            this.txtMaHQDKLNG.TabIndex = 1;
            this.txtMaHQDKLNG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // timerAutoFeedBack
            // 
            this.timerAutoFeedBack.Tick += new System.EventHandler(this.timerAutoFeedBack_Tick);
            // 
            // VNACC_VouchersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1342, 699);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_VouchersForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "KHAI BÁO CHỨNG TỪ ";
            this.Load += new System.EventHandler(this.VNACC_VouchersForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbCtrVouchers)).EndInit();
            this.tbCtrVouchers.ResumeLayout(false);
            this.tpCO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.tpVanDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListVanDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox38)).EndInit();
            this.uiGroupBox38.ResumeLayout(false);
            this.uiGroupBox38.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            this.tpHopDong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox41)).EndInit();
            this.uiGroupBox41.ResumeLayout(false);
            this.uiGroupBox41.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            this.uiGroupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).EndInit();
            this.uiGroupBox14.ResumeLayout(false);
            this.uiGroupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).EndInit();
            this.uiGroupBox15.ResumeLayout(false);
            this.uiGroupBox15.PerformLayout();
            this.tpHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).EndInit();
            this.uiGroupBox17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox42)).EndInit();
            this.uiGroupBox42.ResumeLayout(false);
            this.uiGroupBox42.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            this.uiGroupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox19)).EndInit();
            this.uiGroupBox19.ResumeLayout(false);
            this.uiGroupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox20)).EndInit();
            this.uiGroupBox20.ResumeLayout(false);
            this.uiGroupBox20.PerformLayout();
            this.tpGiayPhep.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox21)).EndInit();
            this.uiGroupBox21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListGiayPhep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox22)).EndInit();
            this.uiGroupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox43)).EndInit();
            this.uiGroupBox43.ResumeLayout(false);
            this.uiGroupBox43.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox23)).EndInit();
            this.uiGroupBox23.ResumeLayout(false);
            this.uiGroupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox24)).EndInit();
            this.uiGroupBox24.ResumeLayout(false);
            this.uiGroupBox24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLoaiGiayPhep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).EndInit();
            this.uiGroupBox25.ResumeLayout(false);
            this.uiGroupBox25.PerformLayout();
            this.tpContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox26)).EndInit();
            this.uiGroupBox26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox27)).EndInit();
            this.uiGroupBox27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox44)).EndInit();
            this.uiGroupBox44.ResumeLayout(false);
            this.uiGroupBox44.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox28)).EndInit();
            this.uiGroupBox28.ResumeLayout(false);
            this.uiGroupBox28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox29)).EndInit();
            this.uiGroupBox29.ResumeLayout(false);
            this.uiGroupBox29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox30)).EndInit();
            this.uiGroupBox30.ResumeLayout(false);
            this.uiGroupBox30.PerformLayout();
            this.tpCTKhac.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox31)).EndInit();
            this.uiGroupBox31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListCTKhac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox32)).EndInit();
            this.uiGroupBox32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox45)).EndInit();
            this.uiGroupBox45.ResumeLayout(false);
            this.uiGroupBox45.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox33)).EndInit();
            this.uiGroupBox33.ResumeLayout(false);
            this.uiGroupBox33.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox34)).EndInit();
            this.uiGroupBox34.ResumeLayout(false);
            this.uiGroupBox34.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox35)).EndInit();
            this.uiGroupBox35.ResumeLayout(false);
            this.uiGroupBox35.PerformLayout();
            this.tpLamNgoaiGio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox36)).EndInit();
            this.uiGroupBox36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListDKLNG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox37)).EndInit();
            this.uiGroupBox37.ResumeLayout(false);
            this.uiGroupBox37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox39)).EndInit();
            this.uiGroupBox39.ResumeLayout(false);
            this.uiGroupBox39.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox40)).EndInit();
            this.uiGroupBox40.ResumeLayout(false);
            this.uiGroupBox40.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiCO;
        private Company.Interface.Controls.NuocHControl txtNuocCapCO;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiCapCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtToChucCapCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiVanDon;
        private Company.Interface.Controls.NuocHControl ctrNuocPHVanDon;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemCTQC;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDonTM;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiCapGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayPhep;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuContainer;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;

        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuCTKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiPhatHanhCTKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTuKhac;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDung;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label64;


        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuHoaDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiCapGiayPhep;

        private Janus.Windows.GridEX.EditControls.EditBox txtTenChungTuKhac;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar ucCalendar10;

        private System.Windows.Forms.Label lblFileNameCO;
        private System.Windows.Forms.Label lblFileNameVanDon;
        private System.Windows.Forms.Label lblFileNameHopDong;
        private System.Windows.Forms.Label lblFileNameHoaDon;
        private System.Windows.Forms.Label lblFileNameGiayPhep;
        private System.Windows.Forms.Label lblFileNamContainer;
        private System.Windows.Forms.Label lblFileNameCTKhac;
        private Janus.Windows.UI.CommandBars.UICommand cmdEdit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdEdit;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.CalendarCombo.CalendarCombo dateNgayCapCO;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayVanDon;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayHopDong;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpThoiHanThanhToan;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayPhatHanhHoaDonTM;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayCapGiayPhep;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayHetHanGiayPhep;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayDangKyToKhai;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayChungTuKhac;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpNgayDangKyThuTuc;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpGioLamTT;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiChungTu;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.UI.Tab.UITab tbCtrVouchers;
        private Janus.Windows.UI.Tab.UITabPage tpCO;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQCO;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTiepNhanCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhanCO;
        private Janus.Windows.UI.Tab.UITabPage tpVanDon;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnViewCO;
        private Janus.Windows.EditControls.UIButton btnAddCO;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIButton btnDeleteCO;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label txtFileSizeCO;
        private System.Windows.Forms.Label label45;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.GridEX.GridEX grListCO;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton btnDeleteVanDon;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIButton btnViewVanDon;
        private Janus.Windows.EditControls.UIButton btnAddVanDon;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label txtFileSizeVanDon;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTNVanDon;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhanVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQVanDon;
        private Janus.Windows.GridEX.GridEX grListVanDon;
        private Janus.Windows.UI.Tab.UITabPage tpHopDong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox12;
        private Janus.Windows.EditControls.UIButton uiButton3;
        private Janus.Windows.EditControls.UIButton btnDeleteHopDong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.EditControls.UIButton btnViewHopDong;
        private Janus.Windows.EditControls.UIButton btnAddHopDong;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label txtFileSizeHopDong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox14;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox15;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTNHopDong;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTNHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQHopDong;
        private Janus.Windows.UI.Tab.UITabPage tpHoaDon;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox16;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox17;
        private Janus.Windows.EditControls.UIButton uiButton7;
        private Janus.Windows.EditControls.UIButton btnDeleteHoaDon;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox18;
        private Janus.Windows.EditControls.UIButton btnViewHoaDon;
        private Janus.Windows.EditControls.UIButton btnAddHoaDon;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label txtFileSizeHoaDon;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox19;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox20;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTNHoaDon;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTNHoaDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQHoaDon;
        private Janus.Windows.UI.Tab.UITabPage tpGiayPhep;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox21;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox22;
        private Janus.Windows.EditControls.UIButton uiButton11;
        private Janus.Windows.EditControls.UIButton btnDeleteGiayPhep;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox23;
        private Janus.Windows.EditControls.UIButton btnViewGiayPhep;
        private Janus.Windows.EditControls.UIButton btnAddGiayPhep;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label txtFileSizeGiayPhep;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox24;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox25;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTNGiayPhep;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTNGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQGiayPhep;
        private Janus.Windows.UI.Tab.UITabPage tpContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox26;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox27;
        private Janus.Windows.EditControls.UIButton uiButton15;
        private Janus.Windows.EditControls.UIButton btnDeleteContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox28;
        private Janus.Windows.EditControls.UIButton btnViewContainer;
        private Janus.Windows.EditControls.UIButton btnAddContainer;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label txtFileSizeContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox29;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox30;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTNContainer;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTNContainer;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQContainer;
        private Janus.Windows.UI.Tab.UITabPage tpCTKhac;
        private Janus.Windows.UI.Tab.UITabPage tpLamNgoaiGio;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox31;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox32;
        private Janus.Windows.EditControls.UIButton uiButton19;
        private Janus.Windows.EditControls.UIButton btnDeleteCTKhac;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox33;
        private Janus.Windows.EditControls.UIButton btnViewCTKhac;
        private Janus.Windows.EditControls.UIButton btnAddCTKhac;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label txtFileSizeChungTuKhac;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox34;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox35;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTNCTKhac;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTNCTKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQCTKhac;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox36;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox37;
        private Janus.Windows.EditControls.UIButton uiButton23;
        private Janus.Windows.EditControls.UIButton btnDeleteOverTime;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox39;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox40;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTNDKLNG;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTNDKLNG;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQDKLNG;
        private Janus.Windows.GridEX.GridEX grListHopDong;
        private Janus.Windows.GridEX.GridEX grListHoaDon;
        private Janus.Windows.GridEX.GridEX grListGiayPhep;
        private Janus.Windows.GridEX.GridEX grListContainer;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQTK;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLoaiHinh;
        private Janus.Windows.GridEX.GridEX grListCTKhac;
        private Janus.Windows.GridEX.GridEX grListDKLNG;
        private System.Windows.Forms.LinkLabel linkHuongDanKySo;
        private System.Windows.Forms.LinkLabel linkHuongDanKB;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox38;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private Janus.Windows.EditControls.UIButton uiButton4;
        private Janus.Windows.EditControls.UIButton uiButton5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox41;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.LinkLabel linkLabel6;
        private Janus.Windows.EditControls.UIButton uiButton6;
        private Janus.Windows.EditControls.UIButton uiButton8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox42;
        private System.Windows.Forms.LinkLabel linkLabel7;
        private System.Windows.Forms.LinkLabel linkLabel8;
        private Janus.Windows.EditControls.UIButton uiButton9;
        private Janus.Windows.EditControls.UIButton uiButton10;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox43;
        private System.Windows.Forms.LinkLabel linkLabel9;
        private System.Windows.Forms.LinkLabel linkLabel10;
        private Janus.Windows.EditControls.UIButton uiButton12;
        private Janus.Windows.EditControls.UIButton uiButton13;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox44;
        private System.Windows.Forms.LinkLabel linkLabel11;
        private System.Windows.Forms.LinkLabel linkLabel12;
        private Janus.Windows.EditControls.UIButton uiButton14;
        private Janus.Windows.EditControls.UIButton uiButton16;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox45;
        private System.Windows.Forms.LinkLabel linkLabel13;
        private System.Windows.Forms.LinkLabel linkLabel14;
        private Janus.Windows.EditControls.UIButton uiButton17;
        private Janus.Windows.EditControls.UIButton uiButton18;
        public Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbLoaiGiayPhep;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.CheckBox chkAutoFeedbackCO;
        private System.Windows.Forms.Timer timerAutoFeedBack;
        private System.Windows.Forms.CheckBox chkAutoFeedbackVD;
        private System.Windows.Forms.CheckBox chkAutoFeedbackHD;
        private System.Windows.Forms.CheckBox chkAutoFeedbackHoaDon;
        private System.Windows.Forms.CheckBox chkAutoFeedbackGP;
        private System.Windows.Forms.CheckBox chkAutoFeedbackCT;
        private System.Windows.Forms.CheckBox chkAutoFeedbackCTK;
        private System.Windows.Forms.CheckBox chkAutoFeedbackOT;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaHopDong;
    }
}