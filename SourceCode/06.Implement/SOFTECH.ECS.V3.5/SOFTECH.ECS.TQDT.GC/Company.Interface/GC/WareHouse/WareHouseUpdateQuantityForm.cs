﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseUpdateQuantityForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_TONDAUKY TONDAUKY = new T_KHOKETOAN_TONDAUKY();
        public bool isAdd ;
        public String loaiHangHoa = "N";
        public HopDong HD;
        public WareHouseUpdateQuantityForm()
        {
            InitializeComponent();

            cbHopDong.DataSource = HopDong.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = -1;

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";

            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinh.DataSource = this._DonViTinh;
            txtDonViTinh.DisplayMember = "Ten";
            txtDonViTinh.ValueMember = "Ten";
            txtDonViTinh.ReadOnly = false;
        }

        private void WareHouseUpdateQuantityForm_Load(object sender, EventArgs e)
        {

        }
        private void GetData()
        {
            try
            {
                TONDAUKY.MAHANGHOA = txtMaHang.Text;
                TONDAUKY.TENHANGHOA = txtTenHang.Text;
                TONDAUKY.DVT = txtDonViTinh.SelectedValue.ToString();
                TONDAUKY.MAKHO = cbbMaKho.Value.ToString();
                TONDAUKY.TENKHO = txtTenKho.Text;
                TONDAUKY.LOAIHANGHOA = Convert.ToDecimal(cbbLoaiHang.SelectedValue.ToString());
                TONDAUKY.LUONGTON = Convert.ToDecimal(txtSoLuong.Text);
                TONDAUKY.DONGIA = Convert.ToDecimal(txtDonGiaTT.Text);
                TONDAUKY.TRIGIA = Convert.ToDecimal(txtTriGiaVND.Text);
                TONDAUKY.NGUYENTE = cbbMaNT.Code;
                TONDAUKY.TYGIA = Convert.ToDecimal(txtTyGia.Text);
                TONDAUKY.THUESUATXNK = Convert.ToInt32(txtThueXNK.Text);
                TONDAUKY.DINHKHOAN = txtDinhKhoan.Text;
                TONDAUKY.GHICHU = txtGhiChu.Text;
                TONDAUKY.HOPDONG_ID = HD.ID;
                TONDAUKY.NAMQT = HD.NgayDangKy.Year;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetData()
        {
            try
            {
                txtMaHang.Text = TONDAUKY.MAHANGHOA;
                txtTenHang.Text = TONDAUKY.TENHANGHOA;
                txtDonViTinh.SelectedValue = TONDAUKY.DVT;
                cbbMaKho.Value = TONDAUKY.MAKHO;
                txtTenKho.Text = TONDAUKY.TENKHO;
                txtSoLuong.Text = TONDAUKY.LUONGTON.ToString();
                txtDonGiaTT.Text = TONDAUKY.DONGIA.ToString();
                txtTriGiaVND.Text = TONDAUKY.TRIGIA.ToString();
                cbbMaNT.Code = TONDAUKY.NGUYENTE;
                txtTyGia.Text = TONDAUKY.TYGIA.ToString();
                txtThueXNK.Text = TONDAUKY.THUESUATXNK.ToString();
                txtDinhKhoan.Text = TONDAUKY.DINHKHOAN;
                txtGhiChu.Text = TONDAUKY.GHICHU;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = T_KHOKETOAN_TONDAUKY.SelectCollectionDynamic("HOPDONG_ID = " + HD.ID + " AND LOAIHANGHOA=" + cbbLoaiHang.SelectedValue.ToString() + "", "ID");
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaHang, errorProvider, " Mã hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHang, errorProvider, " Tên hàng hóa ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinh, errorProvider, " Đơn vị tính ");

                isValid &= ValidateControl.ValidateChoose(cbbMaKho, errorProvider, " Mã kho ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenKho, errorProvider, " Tên kho ", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, " Số lượng ");
                isValid &= ValidateControl.ValidateNull(txtDonGiaTT, errorProvider, " Đơn giá ", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtTriGiaVND, errorProvider, " Trị giá ");
                isValid &= ValidateControl.ValidateNull(cbbMaNT, errorProvider, " Nguyên tệ ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetData();
                TONDAUKY.InsertUpdate();
                TONDAUKY = new T_KHOKETOAN_TONDAUKY();
                txtMaHang.Text = String.Empty;
                txtTenHang.Text = String.Empty;
                txtDonViTinh.SelectedValue = String.Empty;
                txtSoLuong.Text = String.Empty;
                txtDonGiaTT.Text = String.Empty;
                txtTriGiaVND.Text = String.Empty;
                txtThueXNK.Text = String.Empty;
                isAdd = true;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.Cells["LOAIHANGHOA"].Value != null)
                {
                    switch (e.Row.Cells["LOAIHANGHOA"].Value.ToString())
                    {
                        case "1":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Nguyên phụ liệu";
                            break;
                        case "2":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Sản phẩm";
                            break;
                        case "3":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Thiết bị";
                            break;
                        case "4":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Hàng mẫu";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType==Janus.Windows.GridEX.RowType.Record)
                {
                    TONDAUKY = new T_KHOKETOAN_TONDAUKY();
                    TONDAUKY = (T_KHOKETOAN_TONDAUKY)e.Row.DataRow;
                    isAdd = false;
                    SetData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoLuong_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaTT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaTT.Text) + Convert.ToDecimal(txtThueXNK.Text) ;
                txtTriGiaVND.Value = TriGiaTT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void txtDonGiaTT_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaTT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaTT.Text) + Convert.ToDecimal(txtThueXNK.Text);
                txtTriGiaVND.Value = TriGiaTT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void txtThueXNK_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaTT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaTT.Text) + Convert.ToDecimal(txtThueXNK.Text);
                txtTriGiaVND.Value = TriGiaTT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value.ToString()));
                clcTuNgay.Value = HD.NgayDangKy;
                clcDenNgay.Value = HD.NgayGiaHan.Year == 1900 ? HD.NgayHetHan : HD.NgayGiaHan;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (cbbLoaiHang == null)
                    return;
                loaiHangHoa = cbbLoaiHang.SelectedValue.ToString();
                if (loaiHangHoa == "1")
                {
                    WareHouseNPLRegistedForm f = new WareHouseNPLRegistedForm();
                    f.HD = HD;
                    f.ShowDialog();
                    if (f.NPLSelected != null)
                    {
                        txtMaHang.Text = f.NPLSelected.MANPL.Trim();
                        txtTenHang.Text = f.NPLSelected.TENNPL.Trim();
                        txtDonViTinh.Text = f.NPLSelected.DVT.ToString().Trim();
                    }
                }
                else if (loaiHangHoa == "2")
                {
                    WareHouseSPRegistedForm f = new WareHouseSPRegistedForm();
                    f.HD = HD;
                    f.ShowDialog();
                    if (f.SPSelected != null)
                    {
                        txtMaHang.Text = f.SPSelected.MASP;
                        txtTenHang.Text = f.SPSelected.TENSP.Trim();
                        txtDonViTinh.Text = f.SPSelected.DVT;
                    }
                }
                else if (loaiHangHoa == "3")
                {

                }
                else if (loaiHangHoa == "4")
                {

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {

        }

        private void cbbLoaiHang_SelectedValueChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}
