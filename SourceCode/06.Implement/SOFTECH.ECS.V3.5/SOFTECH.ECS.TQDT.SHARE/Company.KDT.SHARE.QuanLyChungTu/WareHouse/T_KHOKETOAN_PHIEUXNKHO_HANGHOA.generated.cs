﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_PHIEUXNKHO_HANGHOA : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long PHIEUXNKHO_ID { set; get; }
		public long STT { set; get; }
		public string LOAIHANG { set; get; }
		public string MAHANG { set; get; }
		public string TENHANG { set; get; }
		public string DVT { set; get; }
		public string MAHANGMAP { set; get; }
		public string TENHANGMAP { set; get; }
		public string DVTMAP { set; get; }
		public decimal SOLUONG { set; get; }
		public decimal DONGIA { set; get; }
		public decimal DONGIANT { set; get; }
		public decimal TRIGIANT { set; get; }
		public decimal THANHTIEN { set; get; }
		public string MAKHO { set; get; }
		public string TAIKHOANNO { set; get; }
		public string TAIKHOANCO { set; get; }
		public decimal TYLEQD { set; get; }
		public decimal SOLUONGQD { set; get; }
		public decimal THUEXNK { set; get; }
		public decimal THUETTDB { set; get; }
		public decimal THUEKHAC { set; get; }
		public decimal TRIGIANTSAUPB { set; get; }
		public string GHICHU { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> collection = new List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA>();
			while (reader.Read())
			{
				T_KHOKETOAN_PHIEUXNKHO_HANGHOA entity = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PHIEUXNKHO_ID"))) entity.PHIEUXNKHO_ID = reader.GetInt64(reader.GetOrdinal("PHIEUXNKHO_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt64(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LOAIHANG"))) entity.LOAIHANG = reader.GetString(reader.GetOrdinal("LOAIHANG"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAHANG"))) entity.MAHANG = reader.GetString(reader.GetOrdinal("MAHANG"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENHANG"))) entity.TENHANG = reader.GetString(reader.GetOrdinal("TENHANG"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAHANGMAP"))) entity.MAHANGMAP = reader.GetString(reader.GetOrdinal("MAHANGMAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENHANGMAP"))) entity.TENHANGMAP = reader.GetString(reader.GetOrdinal("TENHANGMAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTMAP"))) entity.DVTMAP = reader.GetString(reader.GetOrdinal("DVTMAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOLUONG"))) entity.SOLUONG = reader.GetDecimal(reader.GetOrdinal("SOLUONG"));
				if (!reader.IsDBNull(reader.GetOrdinal("DONGIA"))) entity.DONGIA = reader.GetDecimal(reader.GetOrdinal("DONGIA"));
				if (!reader.IsDBNull(reader.GetOrdinal("DONGIANT"))) entity.DONGIANT = reader.GetDecimal(reader.GetOrdinal("DONGIANT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIANT"))) entity.TRIGIANT = reader.GetDecimal(reader.GetOrdinal("TRIGIANT"));
				if (!reader.IsDBNull(reader.GetOrdinal("THANHTIEN"))) entity.THANHTIEN = reader.GetDecimal(reader.GetOrdinal("THANHTIEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAKHO"))) entity.MAKHO = reader.GetString(reader.GetOrdinal("MAKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TAIKHOANNO"))) entity.TAIKHOANNO = reader.GetString(reader.GetOrdinal("TAIKHOANNO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TAIKHOANCO"))) entity.TAIKHOANCO = reader.GetString(reader.GetOrdinal("TAIKHOANCO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TYLEQD"))) entity.TYLEQD = reader.GetDecimal(reader.GetOrdinal("TYLEQD"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOLUONGQD"))) entity.SOLUONGQD = reader.GetDecimal(reader.GetOrdinal("SOLUONGQD"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUEXNK"))) entity.THUEXNK = reader.GetDecimal(reader.GetOrdinal("THUEXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUETTDB"))) entity.THUETTDB = reader.GetDecimal(reader.GetOrdinal("THUETTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUEKHAC"))) entity.THUEKHAC = reader.GetDecimal(reader.GetOrdinal("THUEKHAC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIANTSAUPB"))) entity.TRIGIANTSAUPB = reader.GetDecimal(reader.GetOrdinal("TRIGIANTSAUPB"));
				if (!reader.IsDBNull(reader.GetOrdinal("GHICHU"))) entity.GHICHU = reader.GetString(reader.GetOrdinal("GHICHU"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> collection, long id)
        {
            foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_PHIEUXNKHO_HANGHOA VALUES(@PHIEUXNKHO_ID, @STT, @LOAIHANG, @MAHANG, @TENHANG, @DVT, @MAHANGMAP, @TENHANGMAP, @DVTMAP, @SOLUONG, @DONGIA, @DONGIANT, @TRIGIANT, @THANHTIEN, @MAKHO, @TAIKHOANNO, @TAIKHOANCO, @TYLEQD, @SOLUONGQD, @THUEXNK, @THUETTDB, @THUEKHAC, @TRIGIANTSAUPB, @GHICHU)";
            string update = "UPDATE T_KHOKETOAN_PHIEUXNKHO_HANGHOA SET PHIEUXNKHO_ID = @PHIEUXNKHO_ID, STT = @STT, LOAIHANG = @LOAIHANG, MAHANG = @MAHANG, TENHANG = @TENHANG, DVT = @DVT, MAHANGMAP = @MAHANGMAP, TENHANGMAP = @TENHANGMAP, DVTMAP = @DVTMAP, SOLUONG = @SOLUONG, DONGIA = @DONGIA, DONGIANT = @DONGIANT, TRIGIANT = @TRIGIANT, THANHTIEN = @THANHTIEN, MAKHO = @MAKHO, TAIKHOANNO = @TAIKHOANNO, TAIKHOANCO = @TAIKHOANCO, TYLEQD = @TYLEQD, SOLUONGQD = @SOLUONGQD, THUEXNK = @THUEXNK, THUETTDB = @THUETTDB, THUEKHAC = @THUEKHAC, TRIGIANTSAUPB = @TRIGIANTSAUPB, GHICHU = @GHICHU WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_PHIEUXNKHO_HANGHOA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, "PHIEUXNKHO_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.BigInt, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAIHANG", SqlDbType.VarChar, "LOAIHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHANG", SqlDbType.NVarChar, "MAHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENHANG", SqlDbType.NVarChar, "TENHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHANGMAP", SqlDbType.NVarChar, "MAHANGMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENHANGMAP", SqlDbType.NVarChar, "TENHANGMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTMAP", SqlDbType.NVarChar, "DVTMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOLUONG", SqlDbType.Decimal, "SOLUONG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIANT", SqlDbType.Decimal, "DONGIANT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANT", SqlDbType.Decimal, "TRIGIANT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THANHTIEN", SqlDbType.Decimal, "THANHTIEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOANNO", SqlDbType.NVarChar, "TAIKHOANNO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOANCO", SqlDbType.NVarChar, "TAIKHOANCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYLEQD", SqlDbType.Decimal, "TYLEQD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOLUONGQD", SqlDbType.Decimal, "SOLUONGQD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THUEXNK", SqlDbType.Decimal, "THUEXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THUETTDB", SqlDbType.Decimal, "THUETTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THUEKHAC", SqlDbType.Decimal, "THUEKHAC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANTSAUPB", SqlDbType.Decimal, "TRIGIANTSAUPB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, "PHIEUXNKHO_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.BigInt, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAIHANG", SqlDbType.VarChar, "LOAIHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHANG", SqlDbType.NVarChar, "MAHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENHANG", SqlDbType.NVarChar, "TENHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHANGMAP", SqlDbType.NVarChar, "MAHANGMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENHANGMAP", SqlDbType.NVarChar, "TENHANGMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTMAP", SqlDbType.NVarChar, "DVTMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOLUONG", SqlDbType.Decimal, "SOLUONG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIANT", SqlDbType.Decimal, "DONGIANT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANT", SqlDbType.Decimal, "TRIGIANT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THANHTIEN", SqlDbType.Decimal, "THANHTIEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOANNO", SqlDbType.NVarChar, "TAIKHOANNO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOANCO", SqlDbType.NVarChar, "TAIKHOANCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYLEQD", SqlDbType.Decimal, "TYLEQD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOLUONGQD", SqlDbType.Decimal, "SOLUONGQD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THUEXNK", SqlDbType.Decimal, "THUEXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THUETTDB", SqlDbType.Decimal, "THUETTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THUEKHAC", SqlDbType.Decimal, "THUEKHAC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANTSAUPB", SqlDbType.Decimal, "TRIGIANTSAUPB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_PHIEUXNKHO_HANGHOA VALUES(@PHIEUXNKHO_ID, @STT, @LOAIHANG, @MAHANG, @TENHANG, @DVT, @MAHANGMAP, @TENHANGMAP, @DVTMAP, @SOLUONG, @DONGIA, @DONGIANT, @TRIGIANT, @THANHTIEN, @MAKHO, @TAIKHOANNO, @TAIKHOANCO, @TYLEQD, @SOLUONGQD, @THUEXNK, @THUETTDB, @THUEKHAC, @TRIGIANTSAUPB, @GHICHU)";
            string update = "UPDATE T_KHOKETOAN_PHIEUXNKHO_HANGHOA SET PHIEUXNKHO_ID = @PHIEUXNKHO_ID, STT = @STT, LOAIHANG = @LOAIHANG, MAHANG = @MAHANG, TENHANG = @TENHANG, DVT = @DVT, MAHANGMAP = @MAHANGMAP, TENHANGMAP = @TENHANGMAP, DVTMAP = @DVTMAP, SOLUONG = @SOLUONG, DONGIA = @DONGIA, DONGIANT = @DONGIANT, TRIGIANT = @TRIGIANT, THANHTIEN = @THANHTIEN, MAKHO = @MAKHO, TAIKHOANNO = @TAIKHOANNO, TAIKHOANCO = @TAIKHOANCO, TYLEQD = @TYLEQD, SOLUONGQD = @SOLUONGQD, THUEXNK = @THUEXNK, THUETTDB = @THUETTDB, THUEKHAC = @THUEKHAC, TRIGIANTSAUPB = @TRIGIANTSAUPB, GHICHU = @GHICHU WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_PHIEUXNKHO_HANGHOA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, "PHIEUXNKHO_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.BigInt, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAIHANG", SqlDbType.VarChar, "LOAIHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHANG", SqlDbType.NVarChar, "MAHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENHANG", SqlDbType.NVarChar, "TENHANG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHANGMAP", SqlDbType.NVarChar, "MAHANGMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENHANGMAP", SqlDbType.NVarChar, "TENHANGMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTMAP", SqlDbType.NVarChar, "DVTMAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOLUONG", SqlDbType.Decimal, "SOLUONG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIANT", SqlDbType.Decimal, "DONGIANT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANT", SqlDbType.Decimal, "TRIGIANT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THANHTIEN", SqlDbType.Decimal, "THANHTIEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOANNO", SqlDbType.NVarChar, "TAIKHOANNO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOANCO", SqlDbType.NVarChar, "TAIKHOANCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYLEQD", SqlDbType.Decimal, "TYLEQD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SOLUONGQD", SqlDbType.Decimal, "SOLUONGQD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THUEXNK", SqlDbType.Decimal, "THUEXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THUETTDB", SqlDbType.Decimal, "THUETTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@THUEKHAC", SqlDbType.Decimal, "THUEKHAC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANTSAUPB", SqlDbType.Decimal, "TRIGIANTSAUPB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, "PHIEUXNKHO_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.BigInt, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAIHANG", SqlDbType.VarChar, "LOAIHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHANG", SqlDbType.NVarChar, "MAHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENHANG", SqlDbType.NVarChar, "TENHANG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHANGMAP", SqlDbType.NVarChar, "MAHANGMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENHANGMAP", SqlDbType.NVarChar, "TENHANGMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTMAP", SqlDbType.NVarChar, "DVTMAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOLUONG", SqlDbType.Decimal, "SOLUONG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIANT", SqlDbType.Decimal, "DONGIANT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANT", SqlDbType.Decimal, "TRIGIANT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THANHTIEN", SqlDbType.Decimal, "THANHTIEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOANNO", SqlDbType.NVarChar, "TAIKHOANNO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOANCO", SqlDbType.NVarChar, "TAIKHOANCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYLEQD", SqlDbType.Decimal, "TYLEQD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SOLUONGQD", SqlDbType.Decimal, "SOLUONGQD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THUEXNK", SqlDbType.Decimal, "THUEXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THUETTDB", SqlDbType.Decimal, "THUETTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@THUEKHAC", SqlDbType.Decimal, "THUEKHAC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANTSAUPB", SqlDbType.Decimal, "TRIGIANTSAUPB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_PHIEUXNKHO_HANGHOA Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> SelectCollectionBy_PHIEUXNKHO_ID(long pHIEUXNKHO_ID)
		{
            IDataReader reader = SelectReaderBy_PHIEUXNKHO_ID(pHIEUXNKHO_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_PHIEUXNKHO_ID(long pHIEUXNKHO_ID)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectBy_PHIEUXNKHO_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, pHIEUXNKHO_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicBCQT(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamicBCQT]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_PHIEUXNKHO_ID(long pHIEUXNKHO_ID)
		{
			const string spName = "p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectBy_PHIEUXNKHO_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, pHIEUXNKHO_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_PHIEUXNKHO_HANGHOA(long pHIEUXNKHO_ID, long sTT, string lOAIHANG, string mAHANG, string tENHANG, string dVT, string mAHANGMAP, string tENHANGMAP, string dVTMAP, decimal sOLUONG, decimal dONGIA, decimal dONGIANT, decimal tRIGIANT, decimal tHANHTIEN, string mAKHO, string tAIKHOANNO, string tAIKHOANCO, decimal tYLEQD, decimal sOLUONGQD, decimal tHUEXNK, decimal tHUETTDB, decimal tHUEKHAC, decimal tRIGIANTSAUPB, string gHICHU)
		{
			T_KHOKETOAN_PHIEUXNKHO_HANGHOA entity = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();	
			entity.PHIEUXNKHO_ID = pHIEUXNKHO_ID;
			entity.STT = sTT;
			entity.LOAIHANG = lOAIHANG;
			entity.MAHANG = mAHANG;
			entity.TENHANG = tENHANG;
			entity.DVT = dVT;
			entity.MAHANGMAP = mAHANGMAP;
			entity.TENHANGMAP = tENHANGMAP;
			entity.DVTMAP = dVTMAP;
			entity.SOLUONG = sOLUONG;
			entity.DONGIA = dONGIA;
			entity.DONGIANT = dONGIANT;
			entity.TRIGIANT = tRIGIANT;
			entity.THANHTIEN = tHANHTIEN;
			entity.MAKHO = mAKHO;
			entity.TAIKHOANNO = tAIKHOANNO;
			entity.TAIKHOANCO = tAIKHOANCO;
			entity.TYLEQD = tYLEQD;
			entity.SOLUONGQD = sOLUONGQD;
			entity.THUEXNK = tHUEXNK;
			entity.THUETTDB = tHUETTDB;
			entity.THUEKHAC = tHUEKHAC;
			entity.TRIGIANTSAUPB = tRIGIANTSAUPB;
			entity.GHICHU = gHICHU;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, PHIEUXNKHO_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, STT);
			db.AddInParameter(dbCommand, "@LOAIHANG", SqlDbType.VarChar, LOAIHANG);
			db.AddInParameter(dbCommand, "@MAHANG", SqlDbType.NVarChar, MAHANG);
			db.AddInParameter(dbCommand, "@TENHANG", SqlDbType.NVarChar, TENHANG);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@MAHANGMAP", SqlDbType.NVarChar, MAHANGMAP);
			db.AddInParameter(dbCommand, "@TENHANGMAP", SqlDbType.NVarChar, TENHANGMAP);
			db.AddInParameter(dbCommand, "@DVTMAP", SqlDbType.NVarChar, DVTMAP);
			db.AddInParameter(dbCommand, "@SOLUONG", SqlDbType.Decimal, SOLUONG);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@DONGIANT", SqlDbType.Decimal, DONGIANT);
			db.AddInParameter(dbCommand, "@TRIGIANT", SqlDbType.Decimal, TRIGIANT);
			db.AddInParameter(dbCommand, "@THANHTIEN", SqlDbType.Decimal, THANHTIEN);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TAIKHOANNO", SqlDbType.NVarChar, TAIKHOANNO);
			db.AddInParameter(dbCommand, "@TAIKHOANCO", SqlDbType.NVarChar, TAIKHOANCO);
			db.AddInParameter(dbCommand, "@TYLEQD", SqlDbType.Decimal, TYLEQD);
			db.AddInParameter(dbCommand, "@SOLUONGQD", SqlDbType.Decimal, SOLUONGQD);
			db.AddInParameter(dbCommand, "@THUEXNK", SqlDbType.Decimal, THUEXNK);
			db.AddInParameter(dbCommand, "@THUETTDB", SqlDbType.Decimal, THUETTDB);
			db.AddInParameter(dbCommand, "@THUEKHAC", SqlDbType.Decimal, THUEKHAC);
			db.AddInParameter(dbCommand, "@TRIGIANTSAUPB", SqlDbType.Decimal, TRIGIANTSAUPB);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_PHIEUXNKHO_HANGHOA(long id, long pHIEUXNKHO_ID, long sTT, string lOAIHANG, string mAHANG, string tENHANG, string dVT, string mAHANGMAP, string tENHANGMAP, string dVTMAP, decimal sOLUONG, decimal dONGIA, decimal dONGIANT, decimal tRIGIANT, decimal tHANHTIEN, string mAKHO, string tAIKHOANNO, string tAIKHOANCO, decimal tYLEQD, decimal sOLUONGQD, decimal tHUEXNK, decimal tHUETTDB, decimal tHUEKHAC, decimal tRIGIANTSAUPB, string gHICHU)
		{
			T_KHOKETOAN_PHIEUXNKHO_HANGHOA entity = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();			
			entity.ID = id;
			entity.PHIEUXNKHO_ID = pHIEUXNKHO_ID;
			entity.STT = sTT;
			entity.LOAIHANG = lOAIHANG;
			entity.MAHANG = mAHANG;
			entity.TENHANG = tENHANG;
			entity.DVT = dVT;
			entity.MAHANGMAP = mAHANGMAP;
			entity.TENHANGMAP = tENHANGMAP;
			entity.DVTMAP = dVTMAP;
			entity.SOLUONG = sOLUONG;
			entity.DONGIA = dONGIA;
			entity.DONGIANT = dONGIANT;
			entity.TRIGIANT = tRIGIANT;
			entity.THANHTIEN = tHANHTIEN;
			entity.MAKHO = mAKHO;
			entity.TAIKHOANNO = tAIKHOANNO;
			entity.TAIKHOANCO = tAIKHOANCO;
			entity.TYLEQD = tYLEQD;
			entity.SOLUONGQD = sOLUONGQD;
			entity.THUEXNK = tHUEXNK;
			entity.THUETTDB = tHUETTDB;
			entity.THUEKHAC = tHUEKHAC;
			entity.TRIGIANTSAUPB = tRIGIANTSAUPB;
			entity.GHICHU = gHICHU;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, PHIEUXNKHO_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, STT);
			db.AddInParameter(dbCommand, "@LOAIHANG", SqlDbType.VarChar, LOAIHANG);
			db.AddInParameter(dbCommand, "@MAHANG", SqlDbType.NVarChar, MAHANG);
			db.AddInParameter(dbCommand, "@TENHANG", SqlDbType.NVarChar, TENHANG);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@MAHANGMAP", SqlDbType.NVarChar, MAHANGMAP);
			db.AddInParameter(dbCommand, "@TENHANGMAP", SqlDbType.NVarChar, TENHANGMAP);
			db.AddInParameter(dbCommand, "@DVTMAP", SqlDbType.NVarChar, DVTMAP);
			db.AddInParameter(dbCommand, "@SOLUONG", SqlDbType.Decimal, SOLUONG);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@DONGIANT", SqlDbType.Decimal, DONGIANT);
			db.AddInParameter(dbCommand, "@TRIGIANT", SqlDbType.Decimal, TRIGIANT);
			db.AddInParameter(dbCommand, "@THANHTIEN", SqlDbType.Decimal, THANHTIEN);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TAIKHOANNO", SqlDbType.NVarChar, TAIKHOANNO);
			db.AddInParameter(dbCommand, "@TAIKHOANCO", SqlDbType.NVarChar, TAIKHOANCO);
			db.AddInParameter(dbCommand, "@TYLEQD", SqlDbType.Decimal, TYLEQD);
			db.AddInParameter(dbCommand, "@SOLUONGQD", SqlDbType.Decimal, SOLUONGQD);
			db.AddInParameter(dbCommand, "@THUEXNK", SqlDbType.Decimal, THUEXNK);
			db.AddInParameter(dbCommand, "@THUETTDB", SqlDbType.Decimal, THUETTDB);
			db.AddInParameter(dbCommand, "@THUEKHAC", SqlDbType.Decimal, THUEKHAC);
			db.AddInParameter(dbCommand, "@TRIGIANTSAUPB", SqlDbType.Decimal, TRIGIANTSAUPB);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_PHIEUXNKHO_HANGHOA(long id, long pHIEUXNKHO_ID, long sTT, string lOAIHANG, string mAHANG, string tENHANG, string dVT, string mAHANGMAP, string tENHANGMAP, string dVTMAP, decimal sOLUONG, decimal dONGIA, decimal dONGIANT, decimal tRIGIANT, decimal tHANHTIEN, string mAKHO, string tAIKHOANNO, string tAIKHOANCO, decimal tYLEQD, decimal sOLUONGQD, decimal tHUEXNK, decimal tHUETTDB, decimal tHUEKHAC, decimal tRIGIANTSAUPB, string gHICHU)
		{
			T_KHOKETOAN_PHIEUXNKHO_HANGHOA entity = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();			
			entity.ID = id;
			entity.PHIEUXNKHO_ID = pHIEUXNKHO_ID;
			entity.STT = sTT;
			entity.LOAIHANG = lOAIHANG;
			entity.MAHANG = mAHANG;
			entity.TENHANG = tENHANG;
			entity.DVT = dVT;
			entity.MAHANGMAP = mAHANGMAP;
			entity.TENHANGMAP = tENHANGMAP;
			entity.DVTMAP = dVTMAP;
			entity.SOLUONG = sOLUONG;
			entity.DONGIA = dONGIA;
			entity.DONGIANT = dONGIANT;
			entity.TRIGIANT = tRIGIANT;
			entity.THANHTIEN = tHANHTIEN;
			entity.MAKHO = mAKHO;
			entity.TAIKHOANNO = tAIKHOANNO;
			entity.TAIKHOANCO = tAIKHOANCO;
			entity.TYLEQD = tYLEQD;
			entity.SOLUONGQD = sOLUONGQD;
			entity.THUEXNK = tHUEXNK;
			entity.THUETTDB = tHUETTDB;
			entity.THUEKHAC = tHUEKHAC;
			entity.TRIGIANTSAUPB = tRIGIANTSAUPB;
			entity.GHICHU = gHICHU;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, PHIEUXNKHO_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, STT);
			db.AddInParameter(dbCommand, "@LOAIHANG", SqlDbType.VarChar, LOAIHANG);
			db.AddInParameter(dbCommand, "@MAHANG", SqlDbType.NVarChar, MAHANG);
			db.AddInParameter(dbCommand, "@TENHANG", SqlDbType.NVarChar, TENHANG);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@MAHANGMAP", SqlDbType.NVarChar, MAHANGMAP);
			db.AddInParameter(dbCommand, "@TENHANGMAP", SqlDbType.NVarChar, TENHANGMAP);
			db.AddInParameter(dbCommand, "@DVTMAP", SqlDbType.NVarChar, DVTMAP);
			db.AddInParameter(dbCommand, "@SOLUONG", SqlDbType.Decimal, SOLUONG);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@DONGIANT", SqlDbType.Decimal, DONGIANT);
			db.AddInParameter(dbCommand, "@TRIGIANT", SqlDbType.Decimal, TRIGIANT);
			db.AddInParameter(dbCommand, "@THANHTIEN", SqlDbType.Decimal, THANHTIEN);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TAIKHOANNO", SqlDbType.NVarChar, TAIKHOANNO);
			db.AddInParameter(dbCommand, "@TAIKHOANCO", SqlDbType.NVarChar, TAIKHOANCO);
			db.AddInParameter(dbCommand, "@TYLEQD", SqlDbType.Decimal, TYLEQD);
			db.AddInParameter(dbCommand, "@SOLUONGQD", SqlDbType.Decimal, SOLUONGQD);
			db.AddInParameter(dbCommand, "@THUEXNK", SqlDbType.Decimal, THUEXNK);
			db.AddInParameter(dbCommand, "@THUETTDB", SqlDbType.Decimal, THUETTDB);
			db.AddInParameter(dbCommand, "@THUEKHAC", SqlDbType.Decimal, THUEKHAC);
			db.AddInParameter(dbCommand, "@TRIGIANTSAUPB", SqlDbType.Decimal, TRIGIANTSAUPB);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_PHIEUXNKHO_HANGHOA(long id)
		{
			T_KHOKETOAN_PHIEUXNKHO_HANGHOA entity = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_PHIEUXNKHO_ID(long pHIEUXNKHO_ID)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteBy_PHIEUXNKHO_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@PHIEUXNKHO_ID", SqlDbType.BigInt, pHIEUXNKHO_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}