using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.DuLieuChuan
{
	public partial class KDT_GC_PhieuNhapKho_Hang : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public string MaSoHang { set; get; }
		public string Ma2 { set; get; }
		public string TenHang { set; get; }
		public string Mau { set; get; }
		public string Size { set; get; }
		public string DVTLuong1 { set; get; }
		public decimal SoLuong1 { set; get; }
		public decimal SoLuongThucNhap { set; get; }
		public decimal DonGiaNgoaiTe { set; get; }
		public decimal DonGiaHoaDon { set; get; }
		public decimal ThanhTienNgoaiTe { set; get; }
		public decimal TriGiaHoaDon { set; get; }
		public string GhiChu { set; get; }
		public string MaTTDonGia { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_PhieuNhapKho_Hang> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_PhieuNhapKho_Hang> collection = new List<KDT_GC_PhieuNhapKho_Hang>();
			while (reader.Read())
			{
				KDT_GC_PhieuNhapKho_Hang entity = new KDT_GC_PhieuNhapKho_Hang();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHang"))) entity.MaSoHang = reader.GetString(reader.GetOrdinal("MaSoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma2"))) entity.Ma2 = reader.GetString(reader.GetOrdinal("Ma2"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Mau"))) entity.Mau = reader.GetString(reader.GetOrdinal("Mau"));
				if (!reader.IsDBNull(reader.GetOrdinal("Size"))) entity.Size = reader.GetString(reader.GetOrdinal("Size"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTLuong1"))) entity.DVTLuong1 = reader.GetString(reader.GetOrdinal("DVTLuong1"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong1"))) entity.SoLuong1 = reader.GetDecimal(reader.GetOrdinal("SoLuong1"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongThucNhap"))) entity.SoLuongThucNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongThucNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaNgoaiTe"))) entity.DonGiaNgoaiTe = reader.GetDecimal(reader.GetOrdinal("DonGiaNgoaiTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaHoaDon"))) entity.DonGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("DonGiaHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhTienNgoaiTe"))) entity.ThanhTienNgoaiTe = reader.GetDecimal(reader.GetOrdinal("ThanhTienNgoaiTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHoaDon"))) entity.TriGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("TriGiaHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTDonGia"))) entity.MaTTDonGia = reader.GetString(reader.GetOrdinal("MaTTDonGia"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_PhieuNhapKho_Hang> collection, long id)
        {
            foreach (KDT_GC_PhieuNhapKho_Hang item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_PhieuNhapKho_Hang VALUES(@TKMD_ID, @MaSoHang, @Ma2, @TenHang, @Mau, @Size, @DVTLuong1, @SoLuong1, @SoLuongThucNhap, @DonGiaNgoaiTe, @DonGiaHoaDon, @ThanhTienNgoaiTe, @TriGiaHoaDon, @GhiChu, @MaTTDonGia)";
            string update = "UPDATE t_KDT_GC_PhieuNhapKho_Hang SET TKMD_ID = @TKMD_ID, MaSoHang = @MaSoHang, Ma2 = @Ma2, TenHang = @TenHang, Mau = @Mau, Size = @Size, DVTLuong1 = @DVTLuong1, SoLuong1 = @SoLuong1, SoLuongThucNhap = @SoLuongThucNhap, DonGiaNgoaiTe = @DonGiaNgoaiTe, DonGiaHoaDon = @DonGiaHoaDon, ThanhTienNgoaiTe = @ThanhTienNgoaiTe, TriGiaHoaDon = @TriGiaHoaDon, GhiChu = @GhiChu, MaTTDonGia = @MaTTDonGia WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhieuNhapKho_Hang WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma2", SqlDbType.NVarChar, "Ma2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Mau", SqlDbType.NVarChar, "Mau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Size", SqlDbType.NVarChar, "Size", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThucNhap", SqlDbType.Decimal, "SoLuongThucNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaNgoaiTe", SqlDbType.Decimal, "DonGiaNgoaiTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhTienNgoaiTe", SqlDbType.Decimal, "ThanhTienNgoaiTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDonGia", SqlDbType.VarChar, "MaTTDonGia", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma2", SqlDbType.NVarChar, "Ma2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Mau", SqlDbType.NVarChar, "Mau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Size", SqlDbType.NVarChar, "Size", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThucNhap", SqlDbType.Decimal, "SoLuongThucNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaNgoaiTe", SqlDbType.Decimal, "DonGiaNgoaiTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhTienNgoaiTe", SqlDbType.Decimal, "ThanhTienNgoaiTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDonGia", SqlDbType.VarChar, "MaTTDonGia", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_PhieuNhapKho_Hang VALUES(@TKMD_ID, @MaSoHang, @Ma2, @TenHang, @Mau, @Size, @DVTLuong1, @SoLuong1, @SoLuongThucNhap, @DonGiaNgoaiTe, @DonGiaHoaDon, @ThanhTienNgoaiTe, @TriGiaHoaDon, @GhiChu, @MaTTDonGia)";
            string update = "UPDATE t_KDT_GC_PhieuNhapKho_Hang SET TKMD_ID = @TKMD_ID, MaSoHang = @MaSoHang, Ma2 = @Ma2, TenHang = @TenHang, Mau = @Mau, Size = @Size, DVTLuong1 = @DVTLuong1, SoLuong1 = @SoLuong1, SoLuongThucNhap = @SoLuongThucNhap, DonGiaNgoaiTe = @DonGiaNgoaiTe, DonGiaHoaDon = @DonGiaHoaDon, ThanhTienNgoaiTe = @ThanhTienNgoaiTe, TriGiaHoaDon = @TriGiaHoaDon, GhiChu = @GhiChu, MaTTDonGia = @MaTTDonGia WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhieuNhapKho_Hang WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma2", SqlDbType.NVarChar, "Ma2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Mau", SqlDbType.NVarChar, "Mau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Size", SqlDbType.NVarChar, "Size", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThucNhap", SqlDbType.Decimal, "SoLuongThucNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaNgoaiTe", SqlDbType.Decimal, "DonGiaNgoaiTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThanhTienNgoaiTe", SqlDbType.Decimal, "ThanhTienNgoaiTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTDonGia", SqlDbType.VarChar, "MaTTDonGia", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHang", SqlDbType.VarChar, "MaSoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma2", SqlDbType.NVarChar, "Ma2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Mau", SqlDbType.NVarChar, "Mau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Size", SqlDbType.NVarChar, "Size", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTLuong1", SqlDbType.VarChar, "DVTLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong1", SqlDbType.Decimal, "SoLuong1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThucNhap", SqlDbType.Decimal, "SoLuongThucNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaNgoaiTe", SqlDbType.Decimal, "DonGiaNgoaiTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaHoaDon", SqlDbType.Decimal, "DonGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThanhTienNgoaiTe", SqlDbType.Decimal, "ThanhTienNgoaiTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaHoaDon", SqlDbType.Decimal, "TriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTDonGia", SqlDbType.VarChar, "MaTTDonGia", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_PhieuNhapKho_Hang Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_PhieuNhapKho_Hang> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_PhieuNhapKho_Hang> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_PhieuNhapKho_Hang> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_PhieuNhapKho_Hang(long tKMD_ID, string maSoHang, string ma2, string tenHang, string mau, string size, string dVTLuong1, decimal soLuong1, decimal soLuongThucNhap, decimal donGiaNgoaiTe, decimal donGiaHoaDon, decimal thanhTienNgoaiTe, decimal triGiaHoaDon, string ghiChu, string maTTDonGia)
		{
			KDT_GC_PhieuNhapKho_Hang entity = new KDT_GC_PhieuNhapKho_Hang();	
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.Ma2 = ma2;
			entity.TenHang = tenHang;
			entity.Mau = mau;
			entity.Size = size;
			entity.DVTLuong1 = dVTLuong1;
			entity.SoLuong1 = soLuong1;
			entity.SoLuongThucNhap = soLuongThucNhap;
			entity.DonGiaNgoaiTe = donGiaNgoaiTe;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.ThanhTienNgoaiTe = thanhTienNgoaiTe;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.GhiChu = ghiChu;
			entity.MaTTDonGia = maTTDonGia;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@Ma2", SqlDbType.NVarChar, Ma2);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@Mau", SqlDbType.NVarChar, Mau);
			db.AddInParameter(dbCommand, "@Size", SqlDbType.NVarChar, Size);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@SoLuongThucNhap", SqlDbType.Decimal, SoLuongThucNhap);
			db.AddInParameter(dbCommand, "@DonGiaNgoaiTe", SqlDbType.Decimal, DonGiaNgoaiTe);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@ThanhTienNgoaiTe", SqlDbType.Decimal, ThanhTienNgoaiTe);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaTTDonGia", SqlDbType.VarChar, MaTTDonGia);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_PhieuNhapKho_Hang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuNhapKho_Hang item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_PhieuNhapKho_Hang(long id, long tKMD_ID, string maSoHang, string ma2, string tenHang, string mau, string size, string dVTLuong1, decimal soLuong1, decimal soLuongThucNhap, decimal donGiaNgoaiTe, decimal donGiaHoaDon, decimal thanhTienNgoaiTe, decimal triGiaHoaDon, string ghiChu, string maTTDonGia)
		{
			KDT_GC_PhieuNhapKho_Hang entity = new KDT_GC_PhieuNhapKho_Hang();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.Ma2 = ma2;
			entity.TenHang = tenHang;
			entity.Mau = mau;
			entity.Size = size;
			entity.DVTLuong1 = dVTLuong1;
			entity.SoLuong1 = soLuong1;
			entity.SoLuongThucNhap = soLuongThucNhap;
			entity.DonGiaNgoaiTe = donGiaNgoaiTe;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.ThanhTienNgoaiTe = thanhTienNgoaiTe;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.GhiChu = ghiChu;
			entity.MaTTDonGia = maTTDonGia;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_PhieuNhapKho_Hang_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@Ma2", SqlDbType.NVarChar, Ma2);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@Mau", SqlDbType.NVarChar, Mau);
			db.AddInParameter(dbCommand, "@Size", SqlDbType.NVarChar, Size);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@SoLuongThucNhap", SqlDbType.Decimal, SoLuongThucNhap);
			db.AddInParameter(dbCommand, "@DonGiaNgoaiTe", SqlDbType.Decimal, DonGiaNgoaiTe);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@ThanhTienNgoaiTe", SqlDbType.Decimal, ThanhTienNgoaiTe);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaTTDonGia", SqlDbType.VarChar, MaTTDonGia);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_PhieuNhapKho_Hang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuNhapKho_Hang item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_PhieuNhapKho_Hang(long id, long tKMD_ID, string maSoHang, string ma2, string tenHang, string mau, string size, string dVTLuong1, decimal soLuong1, decimal soLuongThucNhap, decimal donGiaNgoaiTe, decimal donGiaHoaDon, decimal thanhTienNgoaiTe, decimal triGiaHoaDon, string ghiChu, string maTTDonGia)
		{
			KDT_GC_PhieuNhapKho_Hang entity = new KDT_GC_PhieuNhapKho_Hang();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.MaSoHang = maSoHang;
			entity.Ma2 = ma2;
			entity.TenHang = tenHang;
			entity.Mau = mau;
			entity.Size = size;
			entity.DVTLuong1 = dVTLuong1;
			entity.SoLuong1 = soLuong1;
			entity.SoLuongThucNhap = soLuongThucNhap;
			entity.DonGiaNgoaiTe = donGiaNgoaiTe;
			entity.DonGiaHoaDon = donGiaHoaDon;
			entity.ThanhTienNgoaiTe = thanhTienNgoaiTe;
			entity.TriGiaHoaDon = triGiaHoaDon;
			entity.GhiChu = ghiChu;
			entity.MaTTDonGia = maTTDonGia;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaSoHang", SqlDbType.VarChar, MaSoHang);
			db.AddInParameter(dbCommand, "@Ma2", SqlDbType.NVarChar, Ma2);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@Mau", SqlDbType.NVarChar, Mau);
			db.AddInParameter(dbCommand, "@Size", SqlDbType.NVarChar, Size);
			db.AddInParameter(dbCommand, "@DVTLuong1", SqlDbType.VarChar, DVTLuong1);
			db.AddInParameter(dbCommand, "@SoLuong1", SqlDbType.Decimal, SoLuong1);
			db.AddInParameter(dbCommand, "@SoLuongThucNhap", SqlDbType.Decimal, SoLuongThucNhap);
			db.AddInParameter(dbCommand, "@DonGiaNgoaiTe", SqlDbType.Decimal, DonGiaNgoaiTe);
			db.AddInParameter(dbCommand, "@DonGiaHoaDon", SqlDbType.Decimal, DonGiaHoaDon);
			db.AddInParameter(dbCommand, "@ThanhTienNgoaiTe", SqlDbType.Decimal, ThanhTienNgoaiTe);
			db.AddInParameter(dbCommand, "@TriGiaHoaDon", SqlDbType.Decimal, TriGiaHoaDon);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaTTDonGia", SqlDbType.VarChar, MaTTDonGia);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_PhieuNhapKho_Hang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuNhapKho_Hang item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_PhieuNhapKho_Hang(long id)
		{
			KDT_GC_PhieuNhapKho_Hang entity = new KDT_GC_PhieuNhapKho_Hang();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_Hang_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_PhieuNhapKho_Hang> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuNhapKho_Hang item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}