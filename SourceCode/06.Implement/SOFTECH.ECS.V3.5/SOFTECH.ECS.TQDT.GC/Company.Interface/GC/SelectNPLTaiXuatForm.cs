using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.GC
{
    public partial class SelectNPLTaiXuatForm : BaseForm
    {
        public PhanBoToKhaiXuat pbToKhaiXuat = new PhanBoToKhaiXuat();
        public IList<PhanBoToKhaiXuat> pbToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
        public ToKhaiMauDich TKMD;

        private int SoToKhai;
        private string MaLoaiHinh;
        private DateTime NgayDangKy;
        private string TenNPL;
        private string DVT_ID;
        private decimal SoLuong;
        public bool isPhanBo = false;
        public SelectNPLTaiXuatForm()
        {
            InitializeComponent();
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenNPL = e.Row.Cells["TenHang"].Text;
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["Ton"].Value);
                this.MaHaiQuan = TKMD.MaHaiQuan;
                if (MaLoaiHinh.Contains("V"))
                    txtSoToKhai.Text = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(e.Row.Cells["SoToKhai"].Value.ToString())).ToString();
                else
                    txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
                txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
                txtLuong.Text = this.SoLuong.ToString();
                btnAdd.Enabled = true;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateChoose(cbhang, errorProvider, "Mã nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtLuongTaiXuat, errorProvider, "Lượng tái xuất", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoToKhai, errorProvider, "Số tờ khai", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNPL, errorProvider, "Mã nguyên phụ liệu ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtLuong, errorProvider, "Lượng tái xuất", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (checkExit(this.SoToKhai, this.MaLoaiHinh, Convert.ToInt16(this.NgayDangKy.Year), this.MaHaiQuan, txtMaNPL.Text))
                {
                    //ShowMessage("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác.", false);
                    showMsg("MSG_240207");
                    txtSoToKhai.Text = "";
                    txtLuong.Value = 0;
                    txtMaNPL.Text = "";
                    return;
                }
                if (this.SoLuong < Convert.ToDecimal(txtLuong.Value))
                {
                    showMsg("MSG_WRN23");
                    //ShowMessage("Lượng tái xuất phải nhỏ hơn hoặc bằng lượng nguyên phụ liệu tồn của tờ khai.", false);
                    txtLuong.Value = 0;
                    txtLuong.Focus();
                    return;
                }
                PhanBoToKhaiNhap pbTKNhap = new PhanBoToKhaiNhap();
                if (this.SoToKhai == 0 && Convert.ToDouble(txtLuong.Text) == 0)
                {
                    pbTKNhap.LuongTonDau = Convert.ToDouble(this.SoLuong);
                    pbTKNhap.LuongPhanBo = 0;
                    pbTKNhap.LuongTonCuoi = 0;
                    pbTKNhap.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    pbTKNhap.MaHaiQuanNhap = TKMD.MaHaiQuan;
                    pbTKNhap.MaNPL = txtMaNPL.Text.Trim();
                    pbTKNhap.NamDangKyNhap = (short)this.NgayDangKy.Year;
                    pbTKNhap.SoToKhaiNhap = this.SoToKhai;
                    pbTKNhap.MaLoaiHinhNhap = this.MaLoaiHinh;
                    pbTKNhap.LuongCungUng = Convert.ToDouble(txtLuong.Text);
                    pbTKNhap.MuaVN = 0;

                }
                else
                {
                    pbTKNhap.LuongTonDau = Convert.ToDouble(this.SoLuong);
                    pbTKNhap.LuongPhanBo = Convert.ToDouble(txtLuong.Text);
                    pbTKNhap.LuongTonCuoi = pbTKNhap.LuongTonDau - pbTKNhap.LuongPhanBo;
                    pbTKNhap.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    pbTKNhap.MaHaiQuanNhap = TKMD.MaHaiQuan;
                    pbTKNhap.MaNPL = txtMaNPL.Text.Trim();
                    pbTKNhap.NamDangKyNhap = (short)this.NgayDangKy.Year;
                    pbTKNhap.SoToKhaiNhap = this.SoToKhai;
                    pbTKNhap.MaLoaiHinhNhap = this.MaLoaiHinh;
                    pbTKNhap.MuaVN = 0;
                }


                decimal TongLuongCungUng = 0;
                foreach (PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    if (pbTKN.MaNPL.Trim().ToUpper() == cbhang.SelectedValue.ToString())
                    {
                        TongLuongCungUng += (decimal)pbTKN.LuongPhanBo;
                    }
                }
                if ((TongLuongCungUng + (decimal)pbTKNhap.LuongPhanBo) > Convert.ToDecimal(txtLuongTaiXuat.Text))
                {
                    showMsg("MSG_WRN24");
                    //ShowMessage("Tổng lượng chọn trong các tờ khai nhập chưa bằng lượng tái xuất.", false);
                    return;
                }
                pbToKhaiXuat.PhanBoToKhaiNhapCollection.Add(pbTKNhap);
                dgLispbTKX.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                txtLuong.Value = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool checkExit(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            foreach (PhanBoToKhaiNhap nplTT in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                if (nplTT.SoToKhaiNhap == soToKhai && nplTT.MaLoaiHinhNhap == maLoaiHinh && nplTT.NamDangKyNhap == namDangKy && nplTT.MaHaiQuanNhap == maHaiQuan && nplTT.MaNPL == maNPL)
                    return true;
            }
            return false;
        }
        private void SelectPhanBoToKhaiXuatOfMaNPL(string MaNPL)
        {
            foreach (PhanBoToKhaiXuat pbTKX in TKMD.PhanBoToKhaiXuatCollection)
            {
                if (pbTKX.MaSP.Trim().ToUpper() == MaNPL.Trim().ToUpper())
                {
                    pbToKhaiXuat = pbTKX;
                    txtLuongTaiXuat.Text = pbToKhaiXuat.SoLuongXuat.ToString();
                    return;
                }
            }
        }
        private void BK07Form_Load(object sender, EventArgs e)
        {
            
            txtLuongTaiXuat.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            dgListTon.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongTonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongPhanBo"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongTonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            
            if (TKMD.HMDCollection.Count == 0)
            {
                TKMD.LoadHMDCollection();
            }
            cbhang.Items.Clear();
            foreach (HangMauDich HMD in TKMD.HMDCollection)
            {
                cbhang.Items.Add(HMD.TenHang + " / " + HMD.MaPhu, HMD.MaPhu);
            }
            //cbhang.SelectedIndex = 0;
            if (TKMD.MaLoaiHinh.Contains("V"))
            {
                string sotokhaiVNACCS = CapSoToKhai.GetSoTKVNACCS(TKMD.SoToKhai).ToString();
                this.Text = "Phân bổ cho tờ khai tái xuất số : " + sotokhaiVNACCS + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NgayDangKy.Year;
            }
            else
                this.Text = "Phân bổ cho tờ khai tái xuất số : " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NgayDangKy.Year;
        }
        private DataRow GetRowTon(DataTable dt, int soToKhai, int namDangKy, string maLoaiHinh, string maNPL)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToInt32(dr["SoToKhai"]) == soToKhai && Convert.ToDateTime(dr["NgayDangKy"]).Year == namDangKy && Convert.ToString(dr["MaLoaiHinh"]) == maLoaiHinh && Convert.ToString(dr["MaNPL"]).Trim() == maNPL.Trim())
                    return dr;
            }
            return null;
        }
        private void BindData()
        {
            DataTable NPLNhapTonThucTeCollection = NPLNhapTonThucTe.SelectBy_HopDongAndMaNPLAndTonHonO(TKMD.IDHopDong, cbhang.SelectedValue.ToString(), TKMD.NgayDangKy).Tables[0];
            foreach (PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                //DataRow rowTon = NPLNhapTonThucTeCollection.Select("SoToKhai=" + pbTKN.SoToKhaiNhap + " and MaLoaiHinh='" + pbTKN.MaLoaiHinhNhap + "' and year(NgayDangKy)=" + pbTKN.NamDangKyNhap + " and MaHaiQuan='" + pbTKN.MaHaiQuanNhap + "' and MaNPL='" + pbTKN.MaNPL.Trim()+ "'")[0];
                DataRow rowTon = GetRowTon(NPLNhapTonThucTeCollection, pbTKN.SoToKhaiNhap, pbTKN.NamDangKyNhap, pbTKN.MaLoaiHinhNhap, pbTKN.MaNPL);
                rowTon["Ton"] = (Convert.ToDouble(rowTon["Ton"]) - pbTKN.LuongPhanBo);                
            }
            dgListTon.DataSource = NPLNhapTonThucTeCollection;
            dgLispbTKX.DataSource = pbToKhaiXuat.PhanBoToKhaiNhapCollection;
        }
        private bool KiemTraLuongTaiXuat(string maNPL, IList<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection,double LuongTaiXuat)
        {
            decimal TongTaixuat=0;
            foreach (PhanBoToKhaiNhap pbTKN in PhanBoToKhaiNhapCollection)
            {
                TongTaixuat += (decimal) pbTKN.LuongPhanBo;
            }
            if (TongTaixuat != (decimal)LuongTaiXuat)
                return false;
            return true; 
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                foreach(PhanBoToKhaiXuat pbToKhaiX in TKMD.PhanBoToKhaiXuatCollection)
                {

                    if (pbToKhaiX.PhanBoToKhaiNhapCollection == null || pbToKhaiX.PhanBoToKhaiNhapCollection.Count == 0)
                    {
                        showMsg("MSG_WRN25", pbToKhaiX.MaSP);
                        //ShowMessage("Bạn chưa chọn phân bổ tờ khai nhập cho mặt hàng " + pbToKhaiX.MaSP + ".", false);
                        return;
                    }
                    if (!KiemTraLuongTaiXuat(pbToKhaiX.MaSP, pbToKhaiX.PhanBoToKhaiNhapCollection, Convert.ToDouble(pbToKhaiX.SoLuongXuat)))
                    {
                        showMsg("MSG_WRN26");
                        //ShowMessage("Lượng tái xuất được chọn trong các tờ khai nhập chưa bằng lượng tái xuất.",false);
                        return;
                    }
                }
                this.Cursor = Cursors.WaitCursor;
                PhanBoToKhaiXuat.PhanBoChoToKhaiTaiXuat(TKMD, GlobalSettings.SoThapPhan.LuongNPL);
                isPhanBo = true;
                this.Close();
            }
            catch (Exception ex)
            {
                //showMsg("MSG_2702004", ex.Message);
                ShowMessage("Lỗi: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("V"))
                {
                    string sotokhaiVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(e.Row.Cells["SoToKhai"].Value.ToString())).ToString();
                    e.Row.Cells["SoToKhai"].Text = sotokhaiVNACCS + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                }
                else
                    e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                NguyenPhuLieu NPL = new NguyenPhuLieu();
                NPL.Ma = e.Row.Cells["MaNPL"].Text.Trim();
                NPL.HopDong_ID = TKMD.IDHopDong;
                NPL.Load();
                e.Row.Cells["TenHang"].Text = NPL.Ten;
            }
        }

        private void dgList2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.Cells["MaLoaiHinhNhap"].Value.ToString().Contains("V"))
            {
                string sotokhaiVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(e.Row.Cells["SoToKhaiNhap"].Value.ToString())).ToString();
                e.Row.Cells["SoToKhaiNhap"].Text = sotokhaiVNACCS + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhNhap"].Value) + "/" + e.Row.Cells["NamDangKyNhap"].Text;
            }
            else
                e.Row.Cells["SoToKhaiNhap"].Text = e.Row.Cells["SoToKhaiNhap"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhNhap"].Value) + "/" + e.Row.Cells["NamDangKyNhap"].Text;
        }

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
           
            
            this.MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinhNhap"].Value);
            //this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKyNhap"].Value);
            //this. = e.Row.Cells["TenNPL"].Text;
          //  this.DVT_ID = e.Row.Cells["DVT_ID"].Text;
            this.MaHaiQuan = e.Row.Cells["MaHaiQuanNhap"].Text;
            txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
            txtLuong.Value = Convert.ToDecimal(e.Row.Cells["LuongPhanBo"].Value);
            btnAdd.Enabled = false;
            this.SoLuong = Convert.ToDecimal(e.Row.Cells["LuongTonDau"].Text);
            if (MaLoaiHinh.Contains("V"))
                txtSoToKhai.Text = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(e.Row.Cells["SoToKhaiNhap"].Value.ToString())).ToString();
            else
                txtSoToKhai.Text = e.Row.Cells["SoToKhaiNhap"].Text;

        }

      
        private void cbhang_SelectedIndexChanged(object sender, EventArgs e)
        {            
            SelectPhanBoToKhaiXuatOfMaNPL(cbhang.SelectedValue.ToString());
            if (this.pbToKhaiXuat.PhanBoToKhaiNhapCollection == null)
                this.pbToKhaiXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
            BindData();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {

            if (TKMD.PhanBoToKhaiXuatCollection.Count > 0)
            {
                string st = ShowMessage("Nếu nhập từ excel sẽ xóa hết các thông tin đã nhập trước đó. Bạn có muốn tiếp tục không ?", true);
                if (st != "Yes")
                    return;
            }
            ReadExcelFormNPLPhanBo f = new ReadExcelFormNPLPhanBo();
            f.ShowDialog();

            if (f.table.Rows.Count == 0)
                return;

            TKMD.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            foreach (HangMauDich hct in TKMD.HMDCollection)
            {
                string manpl = hct.MaPhu;
                pbToKhaiXuat = new PhanBoToKhaiXuat();
                pbToKhaiXuat.ID_TKMD = TKMD.ID;
                pbToKhaiXuat.MaDoanhNghiep = TKMD.MaLoaiHinh;
                pbToKhaiXuat.MaSP = manpl;
                pbToKhaiXuat.SoLuongXuat = hct.SoLuong;
                pbToKhaiXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
                TKMD.PhanBoToKhaiXuatCollection.Add(pbToKhaiXuat);
                DataRow[] rows = f.table.Select("MaNPL='" + manpl + "'");
                if (rows.Length == 0)
                {
                    string st = ShowMessage("Không có dữ liệu phân bổ cho nguyên phụ liệu : " + manpl + " Bạn có muốn tiếp tục không ?", true);
                    if (st == "Yes")
                        continue;
                    else
                        return;
                }
                foreach (DataRow row in rows)
                {
                    NPLNhapTonThucTe nplTon = null;
                    if (row["SOTK"].ToString().Length > 5)
                    {
                        nplTon = NPLNhapTonThucTe.Load(Convert.ToDecimal(row["SOTK"]), row["MALH"].ToString().Trim(), Convert.ToInt16(row["NamDK"]), TKMD.MaHaiQuan, manpl);
                    }
                    else
                        nplTon = NPLNhapTonThucTe.Load(Convert.ToInt32(row["SOTK"]), row["MALH"].ToString().Trim(), Convert.ToInt16(row["NamDK"]), TKMD.MaHaiQuan, manpl);
                    if (nplTon == null)
                    {
                        string st = ShowMessage("Không có dữ liệu tồn của nguyên phụ liệu : " + manpl + " trên tờ khai : " + row["SOTK"] + "/" + row["MALH"] + "/" + row["NamDK"] + " Bạn có muốn tiếp tục không ?", true);
                        if (st == "Yes")
                            continue;
                        else
                            return;
                    }
                    PhanBoToKhaiNhap pbTKNhap = new PhanBoToKhaiNhap();
                    pbTKNhap.LuongTonDau = Math.Round(Convert.ToDouble(nplTon.Ton), GlobalSettings.SoThapPhan.LuongNPL);
                    pbTKNhap.LuongPhanBo = Math.Round(Convert.ToDouble(row["Luong"]), GlobalSettings.SoThapPhan.LuongNPL);
                    if (pbTKNhap.LuongPhanBo > pbTKNhap.LuongTonDau)
                    {
                        string st = ShowMessage("Lượng phân bổ của nguyên phụ liệu : " + manpl + " trên tờ khai : " + row["SOTK"] + "/" + row["MALH"] + "/" + row["NamDK"] + "là : " + pbTKNhap.LuongPhanBo + " lớn hơn lượng tồn đầu của tờ khai là : " + nplTon.Ton + " Bạn có muốn tiếp tục không ?", true);
                        if (st != "Yes")
                            return;
                    }
                    pbTKNhap.LuongTonCuoi = pbTKNhap.LuongTonDau - pbTKNhap.LuongPhanBo;
                    pbTKNhap.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    pbTKNhap.MaHaiQuanNhap = TKMD.MaHaiQuan;
                    pbTKNhap.MaNPL = manpl;
                    pbTKNhap.NamDangKyNhap = Convert.ToInt16(row["NamDK"]);
                    pbTKNhap.SoToKhaiNhap = nplTon.SoToKhai;
                    pbTKNhap.MaLoaiHinhNhap = nplTon.MaLoaiHinh;
                    pbTKNhap.MuaVN = 0;
                    pbToKhaiXuat.PhanBoToKhaiNhapCollection.Add(pbTKNhap);
                }

            }
            cbhang_SelectedIndexChanged(null, null);
        }

        private void dgLispbTKX_SelectionChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    GridEXSelectedItemCollection items = dgLispbTKX.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            this.SoToKhai = Convert.ToInt32(i.GetRow().Cells["SoToKhai"].Value);
            //            this.MaLoaiHinh = i.GetRow().Cells["MaLoaiHinh"].Text;
            //            this.NgayDangKy = Convert.ToDateTime(i.GetRow().Cells["NgayDangKy"].Value);
            //            this.TenNPL = i.GetRow().Cells["TenHang"].Text;
            //            this.SoLuong = Convert.ToDecimal(i.GetRow().Cells["Ton"].Value);
            //            this.MaHaiQuan = TKMD.MaHaiQuan;
            //            if (MaLoaiHinh.Contains("V"))
            //                txtSoToKhai.Text = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(i.GetRow().Cells["SoToKhai"].Value.ToString())).ToString();
            //            else
            //                txtSoToKhai.Text = i.GetRow().Cells["SoToKhai"].Text;
            //            txtMaNPL.Text = i.GetRow().Cells["MaNPL"].Text;
            //            txtLuong.Text = this.SoLuong.ToString();
            //            btnAdd.Enabled = true;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }

        private void dgListTon_SelectionChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    GridEXSelectedItemCollection items = dgListTon.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            this.MaLoaiHinh = Convert.ToString(i.GetRow().Cells["MaLoaiHinhNhap"].Value);
            //            //this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKyNhap"].Value);
            //            //this. = e.Row.Cells["TenNPL"].Text;
            //            //  this.DVT_ID = e.Row.Cells["DVT_ID"].Text;
            //            this.MaHaiQuan = i.GetRow().Cells["MaHaiQuanNhap"].Text;
            //            txtMaNPL.Text = i.GetRow().Cells["MaNPL"].Text;
            //            txtLuong.Value = Convert.ToDecimal(i.GetRow().Cells["LuongPhanBo"].Value);
            //            btnAdd.Enabled = false;
            //            this.SoLuong = Convert.ToDecimal(i.GetRow().Cells["LuongTonDau"].Text);
            //            if (MaLoaiHinh.Contains("V"))
            //                txtSoToKhai.Text = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(i.GetRow().Cells["SoToKhaiNhap"].Value.ToString())).ToString();
            //            else
            //                txtSoToKhai.Text = i.GetRow().Cells["SoToKhaiNhap"].Text;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }
        

    }
}