﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.ExportTotalData
{
    public partial class VNACC_BaoCaoTongHopQuyDoiNPLSPForm : BaseForm
    {
        public DateTime fromDate;
        public DateTime toDate;
        public VNACC_BaoCaoTongHopQuyDoiNPLSPForm()
        {
            InitializeComponent();
        }

        private void VNACC_BaoCaoTongHopQuyDoiNPLSPForm_Load(object sender, EventArgs e)
        {
#if GC_V4
            BindHopDong();
            cbHopDong.SelectedIndex = 0;
#endif
#if SXXK_V4
            cbHopDong.Visible = true;
            cbHopDong.Enabled = false;
#endif
            clcDenNgay.Value = clcTuNgay.Value.AddDays(30);
        }
#if GC_V4
        private void BindHopDong()
        {
            try
            {
                HopDong hd = new HopDong();
                hd.SoHopDong = "";
                hd.ID = 0;
                List<HopDong> collection;
                {
                    string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                    collection = HopDong.SelectCollectionDynamic(where, "ID");
                }
                collection.Add(hd);
                cbHopDong.DataSource = collection;
                cbHopDong.DisplayMember = "SoHopDong";
                cbHopDong.ValueMember = "ID";
                cbHopDong.SelectedIndex = collection.Count - 1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
#endif
        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Báo cáo Tổng hợp Quy đổi NPL từ Thành phẩm  Từ ngày_" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " Đến ngày _" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetSeachWhere()
        {
            try
            {
                string where = String.Empty;
                where = " TKMD.MaDonVi='" + GlobalSettings.MA_DON_VI + "' AND TKMD.TrangThaiXuLy IN ('2','3') ";
#if GC_V4
                if (!String.IsNullOrEmpty(cbHopDong.Text.Trim()))
                    where = where + " AND TKMD.HopDong_ID = " + cbHopDong.Value.ToString() + "";
#endif
                fromDate = clcTuNgay.Value;
                toDate = clcDenNgay.Value;
                where = where + " AND TKMD.NgayDangKy Between '" + fromDate.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + toDate.ToString("yyyy-MM-dd 23:59:59") + "' ";

                if (!String.IsNullOrEmpty(txtMaHangHoa.Text.Trim()))
                    where = where + " AND HMD.MaHangHoa LIKE '%" + txtMaHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtTenHangHoa.Text.Trim()))
                    where = where + " AND HMD.TenHang LIKE '%" + txtTenHangHoa.Text.Trim() + "%' ";
                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
#if GC_V4
                if (cbHopDong.Value != null)
                {
                    DataSet ds = new DataSet();
                    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                    string sp = "p_KDT_GC_NPLXuatTon";
                    DbCommand cmd = db.GetStoredProcCommand(sp);
                    db.AddInParameter(cmd, "@WhereCondition", SqlDbType.NVarChar, GetSeachWhere());
                    db.AddInParameter(cmd, "@OrderByExpression", SqlDbType.NVarChar, "");
                    db.LoadDataSet(cmd, ds, "t_NPLXuatTon");
                    grList.Refresh();
                    grList.DataSource = ds.Tables[0];
                    grList.Refetch();
                }
#elif SXXK_V4
                DataSet ds = new DataSet();
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                string sp = "p_KDT_SXXK_NPLXuatTon";
                DbCommand cmd = db.GetStoredProcCommand(sp);
                db.AddInParameter(cmd, "@WhereCondition", SqlDbType.NVarChar, GetSeachWhere());
                db.AddInParameter(cmd, "@OrderByExpression", SqlDbType.NVarChar, "");
                db.LoadDataSet(cmd, ds, "t_NPLXuatTon");
                grList.Refresh();
                grList.DataSource = ds.Tables[0];
                grList.Refetch();
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnSearch_Click(null, null);
                grList.RootTable.Groups.Clear();
                grList.RootTable.GroupHeaderTotals.Clear();

                GridEXGroup group = new GridEXGroup();
                GridEXColumn column = new GridEXColumn();
                column = grList.RootTable.Columns[""+ cbbGroupBy.SelectedValue.ToString() +""];
                group = new GridEXGroup(column, Janus.Windows.GridEX.SortOrder.Ascending);
                grList.RootTable.Groups.Add(group);

                //column = grList.RootTable.Columns["LuongSP"];
                //GridEXGroupHeaderTotal groupHeader = new GridEXGroupHeaderTotal();
                //groupHeader.AggregateFunction = AggregateFunction.Sum;
                //groupHeader.Column = column;
                //groupHeader.TotalPrefix = "( Tổng lượng SP : ";
                //groupHeader.TotalSuffix = ")";
                //grList.RootTable.GroupHeaderTotals.Add(groupHeader);

                column = grList.RootTable.Columns["LuongNPL"];
                GridEXGroupHeaderTotal groupHeaderNPL = new GridEXGroupHeaderTotal();
                groupHeaderNPL.AggregateFunction = AggregateFunction.Sum;
                groupHeaderNPL.Column = column;
                groupHeaderNPL.TotalPrefix = "( Tổng lượng NPL : ";
                groupHeaderNPL.TotalSuffix = ")";
                grList.RootTable.GroupHeaderTotals.Add(groupHeaderNPL);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
    }
}
