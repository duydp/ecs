﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Configuration;

namespace Company.KDT.SHARE.Components
{
    public class Version
    {
        protected static SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_Version";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        public static string GetVersion()
        {
            try
            {
                DataSet ds = SelectAll();
                string version = "", date = "";
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    version = ds.Tables[0].Rows[0]["Version"].ToString();
                    date = System.Convert.ToDateTime(ds.Tables[0].Rows[0]["Date"]).ToShortDateString();
                    return version + ", " + date;
                }
                else
                    return "";
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }      
    }
}