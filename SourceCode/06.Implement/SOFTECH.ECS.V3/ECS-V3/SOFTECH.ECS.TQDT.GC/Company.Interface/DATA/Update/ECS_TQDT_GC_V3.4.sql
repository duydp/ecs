﻿/*
Run this script on:

192.168.72.100.ECS_TQDT_SXXK_V2_VERSION    -  This database will be modified

to synchronize it with:

192.168.72.100.ECS_TQDT_SXXK_V2

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.0.2 from Red Gate Software Ltd at 07/09/2012 12:13:21 PM

*/
		
SET XACT_ABORT ON
GO
SET ARITHABORT ON
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
BEGIN TRANSACTION

DELETE FROM [dbo].[t_HaiQuan_CuaKhau] WHERE ID IN ('D02S', 'D007', 'D008', 'D009', 'D010', 'D011')

-- Add rows to [dbo].[t_HaiQuan_CuaKhau]
INSERT INTO [dbo].[t_HaiQuan_CuaKhau] ([ID], [Ten], [MaCuc]) VALUES ('D007', N'Chi cục HQ CPN - DHL', '02')
INSERT INTO [dbo].[t_HaiQuan_CuaKhau] ([ID], [Ten], [MaCuc]) VALUES ('D008', N'Chi cục HQ CPN - FEDEX', '02')
INSERT INTO [dbo].[t_HaiQuan_CuaKhau] ([ID], [Ten], [MaCuc]) VALUES ('D009', N'Chi cục HQ CPN - UPS', '02')
INSERT INTO [dbo].[t_HaiQuan_CuaKhau] ([ID], [Ten], [MaCuc]) VALUES ('D010', N'Chi cục HQ CPN - TNT', '02')
INSERT INTO [dbo].[t_HaiQuan_CuaKhau] ([ID], [Ten], [MaCuc]) VALUES ('D011', N'Chi cục HQ CPN - EMS', '02')



DELETE FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE ID = 'D02S'
INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan] ([ID], [Ten]) VALUES ('D02S', N'Chi Cục HQ Chuyển Phát Nhanh')

UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '113.160.180.180' WHERE ID = 27  

-- Operation applied to 5 rows out of 5
COMMIT TRANSACTION
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.4', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.4', getdate(), null)
	end 


