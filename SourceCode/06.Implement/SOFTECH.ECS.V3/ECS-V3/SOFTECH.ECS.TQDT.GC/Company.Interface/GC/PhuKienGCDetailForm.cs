﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.KDT.GC.PCTFormTemplate;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using Company.Interface.KDT.GC;

namespace Company.Interface.GC
{
    public partial class PhuKienGCDetailForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public PhuKienDangKy PKDK = new PhuKienDangKy();
        public static bool isEdit = false;
        //-----------------------------------------------------------------------------------------
        public PhuKienGCDetailForm()
        {
            InitializeComponent();

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
        }


        //-----------------------------------------------------------------------------------------
        private void PhuKienGCSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.PKDK.LoadCollection();
            dgList.DataSource = PKDK.PKCollection;

        }

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            string id_loaiphukien = e.Row.Cells[1].Text.Trim();
            switch (id_loaiphukien.Trim())
            {
                #region nguyên phụ liệu

                case "803": { showPKBoSungNPL(true, "803"); } break;
                case "503": { showPKBoSungNPL(true, "503"); } break;
                case "103": { showPKBoSungNPL(true, "103"); } break;

                #endregion
                case "101": { showPKHuyHD(true); } break;//Hủy hợp đồng
                case "201": { showPKGiaHanHD(false); } break;//Gia hạn hợp đồng
                #region Sản phẩm
                case "802": { showPKDieuChinhNhomSP(false, "802"); } break;//Thêm mới
                case "502": { showPKDieuChinhNhomSP(false, "502"); } break;//Sửa
                case "102": { showPKDieuChinhNhomSP(false, "102"); } break;//Hủy
                #endregion Sản phẩm

                case "H06":
                    {
                        showPKGiaHanHD(true);
                    }
                    break;
                case "H10":
                    {
                        showPKHuyHD(true);
                    } break;
                case "H11":
                    {
                        showPKMoPhuKienDocLap(true);
                    } break;

                case "N05": { showPKDieuChinhSoLuongNPL(true); } break;
                case "N06": { showPKDieuChinhDVTNPL(true); } break;
                case "N11": { showPKBoSungNPLVietNam(true); } break;
                case "S06": { showPKDieuChinhDVTSP(true); } break;
                case "S10": { showPKDieuChinhMaSP(true); } break;
                case "S13": { showPKDieuChinhChiTietMaSP(true); } break;

                case "T01": { showPKDieuChinhThietBi(true); } break;
                #region Thiết bị
               /* case "T07": { showPKBoSungThietBi(true); } break;*/
                case "804": { showPKBoSungThietBi(true, id_loaiphukien.Trim()); } break;//Bổ sung thiết bị
                #endregion Thiết bị
                case "S09": { showPKDieuChinhSLSanPham(true); } break;
            }

        }
        private void showPKDieuChinhSLSanPham(bool isAdd)
        {
            FormDieuChinhSLSP f = new FormDieuChinhSLSP();
            f.HD = HD;
            f.pkdk = PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void showPKGiaHanHD(bool isAdd)
        {
            PKGiahanHDForm f = new PKGiahanHDForm();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void showPKHuyHD(bool isAdd)
        {

        }
        private void showPKMoPhuKienDocLap(bool isAdd)
        {

        }
        private void showPKBoSungNPL(bool isAdd, string maLPK)
        {
            NguyenPhuLieuGCBoSungForm f = new NguyenPhuLieuGCBoSungForm();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.MaLoaiPK = maLPK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhSoLuongNPL(bool isAdd)
        {
            FormDieuChinhSLNPL f = new FormDieuChinhSLNPL();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhDVTNPL(bool isAdd)
        {
            FormDieuChinhDVTNPL f = new FormDieuChinhDVTNPL();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungNPLVietNam(bool isAdd)
        {
            FormNPLNhapVN f = new FormNPLNhapVN();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhDVTSP(bool isAdd)
        {
            FormDieuChinhDVTSP f = new FormDieuChinhDVTSP();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhMaSP(bool isAdd)
        {
            FormDieuChinhMaSP f = new FormDieuChinhMaSP();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhChiTietMaSP(bool isAdd)
        {
            SanPhamGCBoSungForm f = new SanPhamGCBoSungForm();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhNhomSP(bool isAdd, string MaLPK)
        {

            FormSanPham f = new FormSanPham();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.MaLoaiPK = MaLPK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungThietBi(bool isAdd, string maLoaiPK)
        {
            ThietBiGCBoSungForm f = new ThietBiGCBoSungForm();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.MaLoaiPK = maLoaiPK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhThietBi(bool isAdd)
        {
            FormDieuChinhSLThietBi f = new FormDieuChinhSLThietBi();
            f.boolFlag = isAdd;
            f.HD = HD;
            f.pkdk = this.PKDK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa phụ kiện này không?", true) == "Yes")
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = (Company.GC.BLL.KDT.GC.LoaiPhuKien)e.Row.DataRow;
                    if (LoaiPK.ID > 0)
                    {
                        LoaiPK.Delete(this.PKDK.HopDong_ID);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

    }
}
