IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongAnMaSP]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongAnMaSP]   
GO
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDongAnMaSP]  
 @ID_HopDong bigint  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT   
 [SoTiepNhan],  
 [TrangThaiXuLy],  
 [NgayTiepNhan],  
 [ID_HopDong],  
 [MaHaiQuan],  
 [MaDoanhNghiep],  
 [GUIDSTR],  
 [DeXuatKhac],  
 [LyDoSua],  
 [ActionStatus],  
 [GuidReference],  
 [NamTN],  
 [HUONGDAN],  
 [PhanLuong],  
 [Huongdan_PL],  
 [LoaiDinhMuc],
 MaSanPham  
FROM  
 [dbo].[t_KDT_GC_DinhMucDangKy]  INNER JOIN dbo.t_KDT_GC_DinhMuc ON t_KDT_GC_DinhMuc.Master_ID = t_KDT_GC_DinhMucDangKy.ID 
WHERE  
 [ID_HopDong] = @ID_HopDong  
 

GO
    
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]    
-- Database: ECS_TQDT_KD_VNACCS    
-- Author: Ngo Thanh Tung    
-- Time created: Thursday, January 23, 2014    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT     
 t_KDT_VNACC_ToKhaiMauDich.ID,    
 [SoToKhai],    
 [SoToKhaiDauTien],    
 [SoNhanhToKhai],    
 [TongSoTKChiaNho],    
 [SoToKhaiTNTX],    
 [MaLoaiHinh],    
 [MaPhanLoaiHH],    
 [MaPhuongThucVT],    
 [PhanLoaiToChuc],    
 [CoQuanHaiQuan],    
 [NhomXuLyHS],    
 [ThoiHanTaiNhapTaiXuat],    
 [NgayDangKy],    
 [MaDonVi],    
 [TenDonVi],    
 [MaBuuChinhDonVi],    
 [DiaChiDonVi],    
 [SoDienThoaiDonVi],    
 [MaUyThac],    
 [TenUyThac],    
 [MaDoiTac],    
 [TenDoiTac],    
 [MaBuuChinhDoiTac],    
 [DiaChiDoiTac1],    
 [DiaChiDoiTac2],    
 [DiaChiDoiTac3],    
 [DiaChiDoiTac4],    
 [MaNuocDoiTac],    
 [NguoiUyThacXK],    
 [MaDaiLyHQ],    
 [SoLuong],    
 [MaDVTSoLuong],    
 [TrongLuong],    
 [MaDVTTrongLuong],    
 [MaDDLuuKho],    
 [SoHieuKyHieu],    
 [MaPTVC],    
 [TenPTVC],    
 [NgayHangDen],    
 [MaDiaDiemDoHang],    
 [TenDiaDiemDohang],    
 [MaDiaDiemXepHang],    
 [TenDiaDiemXepHang],    
 [SoLuongCont],    
 [MaKetQuaKiemTra],    
 [PhanLoaiHD],    
 [SoTiepNhanHD],    
 [SoHoaDon],    
 [NgayPhatHanhHD],    
 [PhuongThucTT],    
 [PhanLoaiGiaHD],    
 [MaDieuKienGiaHD],    
 [MaTTHoaDon],    
 [TongTriGiaHD],    
 [MaPhanLoaiTriGia],    
 [SoTiepNhanTKTriGia],    
 [MaTTHieuChinhTriGia],    
 [GiaHieuChinhTriGia],    
 [MaPhanLoaiPhiVC],    
 [MaTTPhiVC],    
 [PhiVanChuyen],    
 [MaPhanLoaiPhiBH],    
 [MaTTPhiBH],    
 [PhiBaoHiem],    
 [SoDangKyBH],    
 [ChiTietKhaiTriGia],    
 [TriGiaTinhThue],    
 [PhanLoaiKhongQDVND],    
 [MaTTTriGiaTinhThue],    
 [TongHeSoPhanBoTG],    
 [MaLyDoDeNghiBP],    
 [NguoiNopThue],    
 [MaNHTraThueThay],    
 [NamPhatHanhHM],    
 [KyHieuCTHanMuc],    
 [SoCTHanMuc],    
 [MaXDThoiHanNopThue],    
 [MaNHBaoLanh],    
 [NamPhatHanhBL],    
 [KyHieuCTBaoLanh],    
 [SoCTBaoLanh],    
 [NgayNhapKhoDau],    
 [NgayKhoiHanhVC],    
 [DiaDiemDichVC],    
 [NgayDen],    
 [GhiChu],    
 [SoQuanLyNoiBoDN],    
 [TrangThaiXuLy],  
 t_KDT_VNACC_ToKhaiMauDich.InputMessageID ,  
 t_KDT_VNACC_ToKhaiMauDich.MessageTag ,  
 t_KDT_VNACC_ToKhaiMauDich.IndexTag ,  
 [PhanLoaiBaoCaoSuaDoi],    
 [MaPhanLoaiKiemTra],    
 [MaSoThueDaiDien],    
 [TenCoQuanHaiQuan],    
 [NgayThayDoiDangKy],    
 [BieuThiTruongHopHetHan],    
 [TenDaiLyHaiQuan],    
 [MaNhanVienHaiQuan],    
 [TenDDLuuKho],    
 [MaPhanLoaiTongGiaCoBan],    
 [PhanLoaiCongThucChuan],    
 [MaPhanLoaiDieuChinhTriGia],    
 [PhuongPhapDieuChinhTriGia],    
 [TongTienThuePhaiNop],    
 [SoTienBaoLanh],    
 [TenTruongDonViHaiQuan],    
 [NgayCapPhep],    
 [PhanLoaiThamTraSauThongQuan],    
 [NgayPheDuyetBP],    
 [NgayHoanThanhKiemTraBP],    
 [SoNgayDoiCapPhepNhapKhau],    
 [TieuDe],    
 [MaSacThueAnHan_VAT],    
 [TenSacThueAnHan_VAT],    
 [HanNopThueSauKhiAnHan_VAT],    
 [PhanLoaiNopThue],    
 [TongSoTienThueXuatKhau],    
 [MaTTTongTienThueXuatKhau],    
 [TongSoTienLePhi],    
 [MaTTCuaSoTienBaoLanh],    
 [SoQuanLyNguoiSuDung],    
 [NgayHoanThanhKiemTra],    
 [TongSoTrangCuaToKhai],    
 [TongSoDongHangCuaToKhai],    
 [NgayKhaiBaoNopThue],    
 [MaVanbanPhapQuy1],    
 [MaVanbanPhapQuy2],    
 [MaVanbanPhapQuy3],    
 [MaVanbanPhapQuy4],    
 [MaVanbanPhapQuy5],    
 [HopDong_ID],    
 [HopDong_So],    
 [LoaiHang],    
 Templ_1 ,  
 TKMD_ID ,  
 SoTT ,  
 SoVanDon ,  
 SoVanDonChu ,  
 NamVanDonChu ,  
 LoaiDinhDanh ,  
 NgayVanDon ,  
 SoDinhDanh FROM dbo.t_KDT_VNACC_ToKhaiMauDich FULL JOIN dbo.t_KDT_VNACC_TK_SoVanDon ON t_KDT_VNACC_TK_SoVanDon.TKMD_ID = t_KDT_VNACC_ToKhaiMauDich.ID     
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL    
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.3',GETDATE(), N'CẬP NHẬT PROCEDURE KIỂM TRA ĐỊNH MỨC')
END