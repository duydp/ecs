﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseHangHoaReadExcel : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_PHIEUXNKHO PhieuXNKho = new T_KHOKETOAN_PHIEUXNKHO();
        public int FormatSoLuong = 8;

        public int FormatDonGia =8;
        public int FormatTriGia = 8;
        public WareHouseHangHoaReadExcel()
        {
            InitializeComponent();
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                // Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.ShowDialog(this);
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                //error.SetIconPadding(txtRow, 8);
                return;

            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            //editing :
            WorksheetRowCollection wsrc = ws.Rows;
            char MaHangHoaColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int  MaHangHoaCol = ConvertCharToInt(MaHangHoaColumn);

            char TenHangHoaColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int  TenHangHoaCol = ConvertCharToInt(TenHangHoaColumn);

            char DVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int  DVTCol = ConvertCharToInt(DVTColumn);

            char MaHangHoaMapColumn = Convert.ToChar(txtMaHangMap.Text.Trim());
            int  MaHangHoaMapCol = ConvertCharToInt(MaHangHoaMapColumn);

            char SoLuongColumn = Convert.ToChar(txtSoLuongColumn.Text.Trim());
            int  SoLuongCol = ConvertCharToInt(SoLuongColumn);

            char DonGiaColumn = Convert.ToChar(txtDonGiaColumn.Text.Trim());
            int  DonGiaCol = ConvertCharToInt(DonGiaColumn);

            char ThanhTienColumn = Convert.ToChar(txtThanhTien.Text.Trim());
            int  ThanhTienCol = ConvertCharToInt(ThanhTienColumn);

            char TKNoColumn = Convert.ToChar(txtTKNo.Text.Trim());
            int  TKNoCol = ConvertCharToInt(TKNoColumn);

            char TKCoColumn = Convert.ToChar(txtTKCo.Text);
            int  TKCoCol = ConvertCharToInt(TKCoColumn);

            char MaKhoColumn = Convert.ToChar(txtMaKho.Text);
            int  MaKhoCol = ConvertCharToInt(MaKhoColumn);

            char GhiChuColumn = Convert.ToChar(txtGhiChu.Text);
            int  GhiChuCol = ConvertCharToInt(GhiChuColumn);

            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaExits = "";
            string errorTenHangHoa = "";
            string errorDVT = "";
            string errorDVTExits= "";
            string errorMaHangHoaMap = "";
            string errorMaHangHoaMapExits = "";
            string errorSoLuong = "";
            string errorSoLuongValid = "";
            string errorDonGia = "";
            string errorDonGiaValid = "";
            string errorThanhTien = "";
            string errorThanhTienValid = "";
            string errorTKNo = "";
            string errorTKNoExits = "";
            string errorTKCo = "";
            string errorTKCoExits = "";
            string errorMaKho = "";
            string errorMaKhoExits = "";
            List<T_KHOKETOAN_DANHSACHKHO> DANHSACHKHOCollection = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            List<T_KHOKETOAN_TAIKHOAN> TAIKHOANCollection = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            List<T_KHOKETOAN_NGUYENPHULIEU> NPLCollection = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionAll();
            List<T_KHOKETOAN_SANPHAM> SPCollection = T_KHOKETOAN_SANPHAM.SelectCollectionAll();
            List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionAll();
            List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionAll();
            List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
            List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> HangCollection = new List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA>();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        bool isExits = false;
                        T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                        try
                        {
                            PhieuXNKHang.MAHANG = Convert.ToString(wsr.Cells[MaHangHoaCol].Value).Trim();
                            if (PhieuXNKHang.MAHANG.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAHANG + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                switch (PhieuXNKho.LOAIHANGHOA)
                                {
                                    case "1":
                                        foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPLMapCollection)
                                        {
                                            if (item.MANPL == PhieuXNKHang.MAHANG)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        break;
                                    case "2":
                                        foreach (T_KHOKETOAN_SANPHAM_MAP item in SPMapCollection)
                                        {
                                            if (item.MASP == PhieuXNKHang.MAHANG)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAHANG + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAHANG + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            PhieuXNKHang.TENHANG = Convert.ToString(wsr.Cells[TenHangHoaCol].Value).Trim();
                            if (PhieuXNKHang.TENHANG.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.TENHANG + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.TENHANG + "]\n";
                            isAdd = false;
                        }
                        isExits = false;
                        try
                        {
                            isExits = false;
                            PhieuXNKHang.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                            if (PhieuXNKHang.DVT.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.DVT + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                {
                                    if (item.Code == PhieuXNKHang.DVT)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.DVT + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.DVT + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            PhieuXNKHang.MAHANGMAP = Convert.ToString(wsr.Cells[MaHangHoaMapCol].Value).Trim();
                            if (PhieuXNKHang.MAHANGMAP.Trim().Length == 0)
                            {
                                errorMaHangHoaMap += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAHANGMAP + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                switch (PhieuXNKho.LOAIHANGHOA)
                                {
                                    case "1":
                                        foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPLMapCollection)
                                        {
                                            if (item.MANPLMAP == PhieuXNKHang.MAHANGMAP)
                                            {
                                                PhieuXNKHang.TENHANGMAP = item.TENNPLMAP;
                                                PhieuXNKHang.DVTMAP = item.DVTMAP;
                                                PhieuXNKHang.TYLEQD = item.TYLEQD;
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        break;
                                    case "2":
                                        foreach (T_KHOKETOAN_SANPHAM_MAP item in SPMapCollection)
                                        {
                                            if (item.MASPMAP == PhieuXNKHang.MAHANGMAP)
                                            {
                                                PhieuXNKHang.TENHANGMAP = item.TENSPMAP;
                                                PhieuXNKHang.DVTMAP = item.DVTMAP;
                                                PhieuXNKHang.TYLEQD = item.TYLEQD;
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHangHoaMapExits += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAHANGMAP + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoaMap += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAHANGMAP + "]\n";
                            isAdd = false;
                        }
                        isExits = false;
                        try
                        {
                            PhieuXNKHang.SOLUONG = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value);
                            if (PhieuXNKHang.SOLUONG.ToString().Length == 0)
                            {
                                errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.SOLUONG + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(PhieuXNKHang.SOLUONG, FormatSoLuong) != PhieuXNKHang.SOLUONG)
                                {
                                    errorSoLuongValid += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.SOLUONG + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.SOLUONG + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            PhieuXNKHang.DONGIA = Convert.ToDecimal(Convert.ToDecimal(wsr.Cells[DonGiaCol].Value));
                            if (PhieuXNKHang.DONGIA.ToString().Length == 0)
                            {
                                errorDonGia += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.DONGIA + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(PhieuXNKHang.DONGIA, FormatDonGia) != PhieuXNKHang.DONGIA)
                                {
                                    errorDonGiaValid += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.DONGIA + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorDonGia += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.DONGIA + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            PhieuXNKHang.THANHTIEN = Convert.ToDecimal(Convert.ToDecimal(wsr.Cells[ThanhTienCol].Value));
                            if (PhieuXNKHang.THANHTIEN.ToString().Length == 0)
                            {
                                errorThanhTien += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.THANHTIEN + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(PhieuXNKHang.THANHTIEN, FormatTriGia) != PhieuXNKHang.THANHTIEN)
                                {
                                    errorThanhTienValid += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.THANHTIEN + "]\n";
                                    isAdd = false;
                                    isExits = true;
                                }
                                if (!isExits)
                                {
                                    if (PhieuXNKHang.SOLUONG * PhieuXNKHang.DONGIA != PhieuXNKHang.THANHTIEN)
                                    {
                                        errorThanhTienValid += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.SOLUONG + "]-[" + PhieuXNKHang.DONGIA + "]-[" + PhieuXNKHang.THANHTIEN + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            errorThanhTien += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.THANHTIEN + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            PhieuXNKHang.SOLUONGQD = PhieuXNKHang.TYLEQD * PhieuXNKHang.SOLUONG;
                            PhieuXNKHang.LOAIHANG = PhieuXNKho.LOAIHANGHOA;
                            PhieuXNKHang.DONGIANT = PhieuXNKHang.DONGIA * PhieuXNKho.TYGIA;
                            PhieuXNKHang.TRIGIANT = PhieuXNKHang.THANHTIEN * PhieuXNKho.TYGIA;
                            PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.TRIGIANT;
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        isExits = false;
                        try
                        {
                            isExits = false;
                            PhieuXNKHang.TAIKHOANNO = Convert.ToString(wsr.Cells[TKNoCol].Value).Trim();
                            if (PhieuXNKHang.TAIKHOANNO.Trim().Length == 0)
                            {
                                errorTKNo += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.TAIKHOANNO + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (T_KHOKETOAN_TAIKHOAN item in TAIKHOANCollection)
                                {
                                    if (item.MATK == PhieuXNKHang.TAIKHOANNO)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorTKNoExits += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.TAIKHOANNO + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTKNo += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.TAIKHOANNO + "]\n";
                            isAdd = false;
                        }
                        isExits = false;
                        try
                        {
                            isExits = false;
                            PhieuXNKHang.TAIKHOANCO = Convert.ToString(wsr.Cells[TKCoCol].Value).Trim();
                            if (PhieuXNKHang.TAIKHOANCO.Trim().Length == 0)
                            {
                                errorTKCo += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.TAIKHOANCO + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (T_KHOKETOAN_TAIKHOAN item in TAIKHOANCollection)
                                {
                                    if (item.MATK == PhieuXNKHang.TAIKHOANCO)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorTKCoExits += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.TAIKHOANCO + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTKCo += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.TAIKHOANCO + "]\n";
                            isAdd = false;
                        }
                        isExits = false;
                        try
                        {
                            isExits = false;
                            PhieuXNKHang.MAKHO = Convert.ToString(wsr.Cells[MaKhoCol].Value).Trim();
                            if (PhieuXNKHang.MAKHO.Trim().Length == 0)
                            {
                                errorMaKho += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAKHO + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (T_KHOKETOAN_DANHSACHKHO item in DANHSACHKHOCollection)
                                {
                                    if (item.MAKHO == PhieuXNKHang.MAKHO)
                                    {                                       
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorMaKhoExits += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAKHO + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaKho += "[" + (wsr.Index + 1) + "]-[" + PhieuXNKHang.MAKHO + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            PhieuXNKHang.GHICHU = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        if (isAdd)
                            HangCollection.Add(PhieuXNKHang);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " không được để trống";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " không được để trống";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT 1] : \n" + errorDVT + " không được để trống";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT 1] : \n" + errorDVTExits + " không hợp lệ. ĐVT phải là ĐVT VNACCS (Ví dụ : Cái/Chiếc là : PCE)";
            if (!String.IsNullOrEmpty(errorMaHangHoaMap))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA MAP] : \n" + errorMaHangHoaMap + " không được để trống";
            if (!String.IsNullOrEmpty(errorMaHangHoaMapExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA MAP] : \n" + errorMaHangHoaMapExits + " chưa được Map";
            if (!String.IsNullOrEmpty(errorSoLuong))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG ] : " + errorSoLuong + " không được để trống";
            if (!String.IsNullOrEmpty(errorSoLuongValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG ] : \n" + errorSoLuongValid + " đã cấu hình chỉ được nhập tối đa " + FormatSoLuong + " số thập phân . Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel \n";
            if (!String.IsNullOrEmpty(errorDonGia))
                errorTotal += "\n - [STT] -[ĐƠN GIÁ] : " + errorDonGia + " không được để trống";
            if (!String.IsNullOrEmpty(errorDonGiaValid))
                errorTotal += "\n - [STT] -[ĐƠN GIÁ] : \n" + errorDonGiaValid + " đã cấu hình chỉ được nhập tối đa " + FormatDonGia + " số thập phân . Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel\n";
            if (!String.IsNullOrEmpty(errorThanhTien))
                errorTotal += "\n - [STT] -[THÀNH TIỀN] : " + errorThanhTien + " không được để trống";
            if (!String.IsNullOrEmpty(errorThanhTienValid))
                errorTotal += "\n - [STT] -[THÀNH TIỀN] : \n" + errorThanhTienValid + " đã cấu hình chỉ được nhập tối đa " + FormatTriGia + " số thập phân  .Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel\n";
            if (!String.IsNullOrEmpty(errorThanhTienValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG]- [ĐƠN GIÁ] - [THÀNH TIỀN] : " + errorThanhTienValid + " Tổng thành tiền không đúng bằng : Số lượng * Đơn giá";
            if (!String.IsNullOrEmpty(errorTKNo))
                errorTotal += "\n - [STT] -[TÀI KHOẢN NỢ] : \n" + errorTKNo + " không được để trống";
            if (!String.IsNullOrEmpty(errorTKNoExits))
                errorTotal += "\n - [STT] -[TÀI KHOẢN NỢ] : \n" + errorTKNoExits + " không đúng với danh sách tài khoản nợ trên Hệ thống";
            if (!String.IsNullOrEmpty(errorTKCo))
                errorTotal += "\n - [STT] -[TÀI KHOẢN CÓ] : \n" + errorTKCo + " không được để trống";
            if (!String.IsNullOrEmpty(errorTKCoExits))
                errorTotal += "\n - [STT] -[TÀI KHOẢN CÓ] : \n" + errorTKCoExits + " không đúng với danh sách tài khoản có trên Hệ thống";
            if (!String.IsNullOrEmpty(errorMaKho))
                errorTotal += "\n - [STT] -[MÃ KHO] : \n" + errorMaKho + " không được để trống";
            if (!String.IsNullOrEmpty(errorMaKhoExits))
                errorTotal += "\n - [STT] -[MÃ KHO] : \n" + errorMaKhoExits + " chưa được đăng ký vào Hệ thống";
            if (String.IsNullOrEmpty(errorTotal))
            {
                int i = 1;
                foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in HangCollection)
                {
                    item.STT = i;
                    PhieuXNKho.HangCollection.Add(item);
                    i++;
                }
                ShowMessageTQDT("Thông báo từ hệ thống", "Nhập hàng từ Excel thành công ", false);
            }
            else
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "Nhập hàng từ Excel không thành công. \n" + errorTotal, false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
