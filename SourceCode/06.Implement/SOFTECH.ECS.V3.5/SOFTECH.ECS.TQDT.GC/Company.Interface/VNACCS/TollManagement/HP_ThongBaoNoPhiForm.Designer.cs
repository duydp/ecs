﻿namespace Company.Interface.VNACCS.TollManagement
{
    partial class HP_ThongBaoNoPhiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListPhi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HP_ThongBaoNoPhiForm));
            this.grbHangRoi = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayTB = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayCT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSoCT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnViewFile = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListPhi = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangRoi)).BeginInit();
            this.grbHangRoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListPhi)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox11);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.grbHangRoi);
            this.grbMain.Size = new System.Drawing.Size(1078, 553);
            // 
            // grbHangRoi
            // 
            this.grbHangRoi.AutoScroll = true;
            this.grbHangRoi.BackColor = System.Drawing.Color.Transparent;
            this.grbHangRoi.Controls.Add(this.clcNgayTB);
            this.grbHangRoi.Controls.Add(this.clcNgayCT);
            this.grbHangRoi.Controls.Add(this.txtSoTB);
            this.grbHangRoi.Controls.Add(this.label2);
            this.grbHangRoi.Controls.Add(this.txtSoCT);
            this.grbHangRoi.Controls.Add(this.label1);
            this.grbHangRoi.Controls.Add(this.label20);
            this.grbHangRoi.Controls.Add(this.label19);
            this.grbHangRoi.Dock = System.Windows.Forms.DockStyle.Top;
            this.grbHangRoi.Location = new System.Drawing.Point(0, 0);
            this.grbHangRoi.Name = "grbHangRoi";
            this.grbHangRoi.Size = new System.Drawing.Size(1078, 99);
            this.grbHangRoi.TabIndex = 1;
            this.grbHangRoi.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // clcNgayTB
            // 
            this.clcNgayTB.AutoSize = false;
            this.clcNgayTB.BackColor = System.Drawing.Color.White;
            this.clcNgayTB.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.clcNgayTB.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayTB.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcNgayTB.DropDownCalendar.Name = "";
            this.clcNgayTB.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayTB.Location = new System.Drawing.Point(130, 50);
            this.clcNgayTB.Name = "clcNgayTB";
            this.clcNgayTB.Nullable = true;
            this.clcNgayTB.NullButtonText = "Xóa";
            this.clcNgayTB.ReadOnly = true;
            this.clcNgayTB.ShowNullButton = true;
            this.clcNgayTB.Size = new System.Drawing.Size(159, 22);
            this.clcNgayTB.TabIndex = 19;
            this.clcNgayTB.TodayButtonText = "Hôm nay";
            this.clcNgayTB.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayCT
            // 
            this.clcNgayCT.AutoSize = false;
            this.clcNgayCT.BackColor = System.Drawing.Color.White;
            this.clcNgayCT.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.clcNgayCT.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayCT.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcNgayCT.DropDownCalendar.Name = "";
            this.clcNgayCT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayCT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayCT.Location = new System.Drawing.Point(460, 50);
            this.clcNgayCT.Name = "clcNgayCT";
            this.clcNgayCT.Nullable = true;
            this.clcNgayCT.NullButtonText = "Xóa";
            this.clcNgayCT.ReadOnly = true;
            this.clcNgayCT.ShowNullButton = true;
            this.clcNgayCT.Size = new System.Drawing.Size(159, 22);
            this.clcNgayCT.TabIndex = 19;
            this.clcNgayCT.TodayButtonText = "Hôm nay";
            this.clcNgayCT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoTB
            // 
            this.txtSoTB.BackColor = System.Drawing.Color.White;
            this.txtSoTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTB.Location = new System.Drawing.Point(130, 16);
            this.txtSoTB.Name = "txtSoTB";
            this.txtSoTB.ReadOnly = true;
            this.txtSoTB.Size = new System.Drawing.Size(161, 22);
            this.txtSoTB.TabIndex = 18;
            this.txtSoTB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(326, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "Ngày tờ khai nộp phí :";
            // 
            // txtSoCT
            // 
            this.txtSoCT.BackColor = System.Drawing.Color.White;
            this.txtSoCT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCT.Location = new System.Drawing.Point(460, 16);
            this.txtSoCT.Name = "txtSoCT";
            this.txtSoCT.ReadOnly = true;
            this.txtSoCT.Size = new System.Drawing.Size(161, 22);
            this.txtSoCT.TabIndex = 18;
            this.txtSoCT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(326, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Số tờ khai nộp phí :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(22, 58);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(103, 14);
            this.label20.TabIndex = 16;
            this.label20.Text = "Ngày thông báo :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(22, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(90, 14);
            this.label19.TabIndex = 17;
            this.label19.Text = "Số thông báo :";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnViewFile);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 506);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1078, 47);
            this.uiGroupBox1.TabIndex = 5;
            // 
            // btnViewFile
            // 
            this.btnViewFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewFile.Image = ((System.Drawing.Image)(resources.GetObject("btnViewFile.Image")));
            this.btnViewFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnViewFile.Location = new System.Drawing.Point(887, 14);
            this.btnViewFile.Name = "btnViewFile";
            this.btnViewFile.Size = new System.Drawing.Size(93, 23);
            this.btnViewFile.TabIndex = 31;
            this.btnViewFile.Text = "Xem File";
            this.btnViewFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnViewFile.Click += new System.EventHandler(this.btnViewFile_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(986, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.AutoScroll = true;
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.dgListPhi);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox11.Location = new System.Drawing.Point(0, 99);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1078, 407);
            this.uiGroupBox11.TabIndex = 6;
            this.uiGroupBox11.Text = "Danh sách khoản mục phí";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListPhi
            // 
            this.dgListPhi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListPhi.AlternatingColors = true;
            this.dgListPhi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListPhi.ColumnAutoResize = true;
            dgListPhi_DesignTimeLayout.LayoutString = resources.GetString("dgListPhi_DesignTimeLayout.LayoutString");
            this.dgListPhi.DesignTimeLayout = dgListPhi_DesignTimeLayout;
            this.dgListPhi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListPhi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListPhi.FrozenColumns = 3;
            this.dgListPhi.GroupByBoxVisible = false;
            this.dgListPhi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListPhi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListPhi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListPhi.Location = new System.Drawing.Point(3, 17);
            this.dgListPhi.Margin = new System.Windows.Forms.Padding(0);
            this.dgListPhi.Name = "dgListPhi";
            this.dgListPhi.RecordNavigator = true;
            this.dgListPhi.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListPhi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListPhi.Size = new System.Drawing.Size(1072, 387);
            this.dgListPhi.TabIndex = 15;
            this.dgListPhi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // HP_ThongBaoNoPhiForm
            // 
            this.ClientSize = new System.Drawing.Size(1078, 553);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HP_ThongBaoNoPhiForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông báo nộp phí";
            this.Load += new System.EventHandler(this.HP_ThongBaoNoPhiForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangRoi)).EndInit();
            this.grbHangRoi.ResumeLayout(false);
            this.grbHangRoi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListPhi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox grbHangRoi;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.GridEX dgListPhi;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTB;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayCT;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTB;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnViewFile;
    }
}
