using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class NoiDungDieuChinhTK
	{
		#region Properties.
		
		public int ID { set; get; }
		public long TKMD_ID { set; get; }
		public int SoTK { set; get; }
		public DateTime NgayDK { set; get; }
		public string MaLoaiHinh { set; get; }
		public DateTime NgaySua { set; get; }
		public int SoDieuChinh { set; get; }
		public int TrangThai { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static NoiDungDieuChinhTK Load(int id)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
			NoiDungDieuChinhTK entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new NoiDungDieuChinhTK();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) entity.SoTK = reader.GetInt32(reader.GetOrdinal("SoTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDK"))) entity.NgayDK = reader.GetDateTime(reader.GetOrdinal("NgayDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgaySua"))) entity.NgaySua = reader.GetDateTime(reader.GetOrdinal("NgaySua"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDieuChinh"))) entity.SoDieuChinh = reader.GetInt32(reader.GetOrdinal("SoDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<NoiDungDieuChinhTK> SelectCollectionAll()
		{
			List<NoiDungDieuChinhTK> collection = new List<NoiDungDieuChinhTK>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				NoiDungDieuChinhTK entity = new NoiDungDieuChinhTK();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) entity.SoTK = reader.GetInt32(reader.GetOrdinal("SoTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDK"))) entity.NgayDK = reader.GetDateTime(reader.GetOrdinal("NgayDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgaySua"))) entity.NgaySua = reader.GetDateTime(reader.GetOrdinal("NgaySua"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDieuChinh"))) entity.SoDieuChinh = reader.GetInt32(reader.GetOrdinal("SoDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<NoiDungDieuChinhTK> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<NoiDungDieuChinhTK> collection = new List<NoiDungDieuChinhTK>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				NoiDungDieuChinhTK entity = new NoiDungDieuChinhTK();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) entity.SoTK = reader.GetInt32(reader.GetOrdinal("SoTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDK"))) entity.NgayDK = reader.GetDateTime(reader.GetOrdinal("NgayDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgaySua"))) entity.NgaySua = reader.GetDateTime(reader.GetOrdinal("NgaySua"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDieuChinh"))) entity.SoDieuChinh = reader.GetInt32(reader.GetOrdinal("SoDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		

        public static List<NoiDungDieuChinhTK> SelectCollectionBy_TKMD_ID(long tkmd_id)
        {
            List<NoiDungDieuChinhTK> collection = new List<NoiDungDieuChinhTK>();
            SqlDataReader reader = (SqlDataReader)SelectReaderBy_TKMD_ID(tkmd_id);
            while (reader.Read())
            {
                NoiDungDieuChinhTK entity = new NoiDungDieuChinhTK();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTK"))) entity.SoTK = reader.GetInt32(reader.GetOrdinal("SoTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDK"))) entity.NgayDK = reader.GetDateTime(reader.GetOrdinal("NgayDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoDieuChinh"))) entity.SoDieuChinh = reader.GetInt32(reader.GetOrdinal("SoDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgaySua"))) entity.NgaySua = reader.GetDateTime(reader.GetOrdinal("NgaySua"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKMD_ID(long tkmd_id)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmd_id);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_TKMD_ID(long tkmd_id)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTK_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tkmd_id);

            return db.ExecuteReader(dbCommand);
        }
        

        #endregion

        //---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertNoiDungDieuChinhTK(long tKMD_ID, int soTK, DateTime ngayDK, string maLoaiHinh, DateTime ngaySua, int soDieuChinh, int trangThai)
		{
			NoiDungDieuChinhTK entity = new NoiDungDieuChinhTK();	
			entity.TKMD_ID = tKMD_ID;
			entity.SoTK = soTK;
			entity.NgayDK = ngayDK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgaySua = ngaySua;
			entity.SoDieuChinh = soDieuChinh;
			entity.TrangThai = trangThai;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
			db.AddInParameter(dbCommand, "@NgayDK", SqlDbType.DateTime, NgayDK.Year <= 1753 ? DBNull.Value : (object) NgayDK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgaySua", SqlDbType.DateTime, NgaySua.Year <= 1753 ? DBNull.Value : (object) NgaySua);
			db.AddInParameter(dbCommand, "@SoDieuChinh", SqlDbType.Int, SoDieuChinh);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<NoiDungDieuChinhTK> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTK item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateNoiDungDieuChinhTK(int id, long tKMD_ID, int soTK, DateTime ngayDK, string maLoaiHinh, DateTime ngaySua, int soDieuChinh, int trangThai)
		{
			NoiDungDieuChinhTK entity = new NoiDungDieuChinhTK();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoTK = soTK;
			entity.NgayDK = ngayDK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgaySua = ngaySua;
			entity.SoDieuChinh = soDieuChinh;
			entity.TrangThai = trangThai;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_NoiDungDieuChinhTK_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
			db.AddInParameter(dbCommand, "@NgayDK", SqlDbType.DateTime, NgayDK.Year == 1753 ? DBNull.Value : (object) NgayDK);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgaySua", SqlDbType.DateTime, NgaySua.Year == 1753 ? DBNull.Value : (object) NgaySua);
			db.AddInParameter(dbCommand, "@SoDieuChinh", SqlDbType.Int, SoDieuChinh);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<NoiDungDieuChinhTK> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTK item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateNoiDungDieuChinhTK(int id, long tKMD_ID, int soTK, DateTime ngayDK, string maLoaiHinh, DateTime ngaySua, int soDieuChinh, int trangThai)
		{
			NoiDungDieuChinhTK entity = new NoiDungDieuChinhTK();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoTK = soTK;
			entity.NgayDK = ngayDK;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgaySua = ngaySua;
			entity.SoDieuChinh = soDieuChinh;
			entity.TrangThai = trangThai;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            //db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.Int, SoTK);
            //db.AddInParameter(dbCommand, "@NgayDK", SqlDbType.DateTime, NgayDK.Year == 1753 ? DBNull.Value : (object) NgayDK);
            //db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            //db.AddInParameter(dbCommand, "@NgaySua", SqlDbType.DateTime, NgaySua.Year == 1753 ? DBNull.Value : (object) NgaySua);
            //db.AddInParameter(dbCommand, "@SoDieuChinh", SqlDbType.Int, SoDieuChinh);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<NoiDungDieuChinhTK> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTK item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteNoiDungDieuChinhTK(int id)
		{
			NoiDungDieuChinhTK entity = new NoiDungDieuChinhTK();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTK_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<NoiDungDieuChinhTK> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTK item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}