﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Company.KDT.SHARE.VNACCS.Maper
{
    public class VNACCMaperToObject
    {
        private static CultureInfo cltInfo
        {
            get
            {
                var ci = new CultureInfo("en-us");
                ci.NumberFormat.NumberDecimalSeparator = ".";
                return ci;
            }
            set
            {

            }
        }

        public static KDT_VNACC_HoaDon GetFromIVA(VAL0870 iva, KDT_VNACC_HoaDon hoadon)
        {
            //KDT_VNACC_HoaDon hoadon = new KDT_VNACC_HoaDon();

            hoadon.SoTiepNhan = Convert.ToDecimal(iva.NIV.GetValue(), cltInfo);
            hoadon.PhanLoaiXuatNhap = iva.YNK.GetValue().ToString();
            hoadon.MaDaiLyHQ = iva.TII.GetValue().ToString();
            hoadon.SoHoaDon = iva.IVN.GetValue().ToString();
            hoadon.NgayLapHoaDon = Convert.ToDateTime(iva.IVD.GetValue());
            hoadon.DiaDiemLapHoaDon = iva.IVB.GetValue().ToString();
            hoadon.PhuongThucThanhToan = iva.HOS.GetValue().ToString();
            hoadon.MaNguoiXNK = iva.IMC.GetValue().ToString();
            hoadon.TenNguoiXNK = iva.IMN.GetValue().ToString();
            hoadon.MaBuuChinh_NguoiXNK = iva.IMY.GetValue().ToString();
            hoadon.DiaChi_NguoiXNK = iva.IMA.GetValue().ToString();
            hoadon.SoDienThoai_NguoiXNK = iva.IMT.GetValue().ToString();
            hoadon.NguoiLapHoaDon = iva.SAA.GetValue().ToString();
            hoadon.MaNguoiGuiNhan = iva.EPC.GetValue().ToString();
            hoadon.TenNguoiGuiNhan = iva.EPN.GetValue().ToString();
            hoadon.MaBuuChinhNguoiGuiNhan = iva.EP1.GetValue().ToString();
            hoadon.DiaChiNguoiNhanGui1 = iva.EPA.GetValue().ToString();
            hoadon.DiaChiNguoiNhanGui2 = iva.EP2.GetValue().ToString();
            hoadon.DiaChiNguoiNhanGui3 = iva.EP3.GetValue().ToString();
            hoadon.DiaChiNguoiNhanGui4 = iva.EP4.GetValue().ToString();
            hoadon.SoDienThoaiNhanGui = iva.EP6.GetValue().ToString();
            hoadon.NuocSoTaiNhanGui = iva.EEE.GetValue().ToString();
            hoadon.MaKyHieu = iva.KNO.GetValue().ToString();
            hoadon.PhanLoaiVanChuyen = iva.TTP.GetValue().ToString();
            hoadon.TenPTVC = iva.VSS.GetValue().ToString();
            hoadon.SoHieuChuyenDi = iva.VNO.GetValue().ToString();
            hoadon.MaDiaDiemXepHang = iva.PSC.GetValue().ToString();
            hoadon.TenDiaDiemXepHang = iva.PSN.GetValue().ToString();
            hoadon.ThoiKyXephang = Convert.ToDateTime(iva.PSD.GetValue());
            hoadon.MaDiaDiemDoHang = iva.DST.GetValue().ToString();
            hoadon.TenDiaDiemDoHang = iva.DCN.GetValue().ToString();
            hoadon.MaDiaDiemTrungChuyen = iva.KYC.GetValue().ToString();
            hoadon.TenDiaDiemTrungChuyen = iva.KYN.GetValue().ToString();
            hoadon.TrongLuongGross = Convert.ToDecimal(iva.GWJ.GetValue(), cltInfo);
            hoadon.MaDVT_TrongLuongGross = iva.GWT.GetValue().ToString();
            hoadon.TrongLuongThuan = Convert.ToDecimal(iva.JWJ.GetValue(), cltInfo);
            hoadon.MaDVT_TrongLuongThuan = iva.JWT.GetValue().ToString();
            hoadon.TongTheTich = Convert.ToDecimal(iva.STJ.GetValue(), cltInfo);
            hoadon.MaDVT_TheTich = iva.STT.GetValue().ToString();
            hoadon.TongSoKienHang = Convert.ToDecimal(iva.NOJ.GetValue(), cltInfo);
            hoadon.MaDVT_KienHang = iva.NOT.GetValue().ToString();
            hoadon.GhiChuChuHang = iva.NT3.GetValue().ToString();
            hoadon.SoPL = iva.PLN.GetValue().ToString();
            hoadon.NganHangLC = iva.LCB.GetValue().ToString();
            hoadon.TriGiaFOB = Convert.ToDecimal(iva.FON.GetValue(), cltInfo);
            hoadon.MaTT_FOB = iva.FOT.GetValue().ToString();
            hoadon.SoTienFOB = Convert.ToDecimal(iva.FKK.GetValue(), cltInfo);
            hoadon.MaTT_TienFOB = iva.FKT.GetValue().ToString();
            hoadon.PhiVanChuyen = Convert.ToDecimal(iva.FR3.GetValue(), cltInfo);
            hoadon.MaTT_PhiVC = iva.FR2.GetValue().ToString();
            hoadon.NoiThanhToanPhiVC = iva.FR4.GetValue().ToString();
            hoadon.ChiPhiXepHang1 = Convert.ToDecimal(iva.FS1.GetValue(), cltInfo);
            hoadon.MaTT_ChiPhiXepHang1 = iva.FT1.GetValue().ToString();
            hoadon.LoaiChiPhiXepHang1 = iva.FH1.GetValue().ToString();
            hoadon.ChiPhiXepHang2 = Convert.ToDecimal(iva.FS2.GetValue(), cltInfo);
            hoadon.MaTT_ChiPhiXepHang2 = iva.FT2.GetValue().ToString();
            hoadon.LoaiChiPhiXepHang2 = iva.FH2.GetValue().ToString();
            hoadon.PhiVC_DuongBo = Convert.ToDecimal(iva.NUH.GetValue(), cltInfo);
            hoadon.MaTT_PhiVC_DuongBo = iva.NTK.GetValue().ToString();
            hoadon.PhiBaoHiem = Convert.ToDecimal(iva.IN3.GetValue(), cltInfo);
            hoadon.MaTT_PhiBaoHiem = iva.IN2.GetValue().ToString();
            hoadon.SoTienPhiBaoHiem = Convert.ToDecimal(iva.IK1.GetValue(), cltInfo);
            hoadon.MaTT_TienPhiBaoHiem = iva.IK2.GetValue().ToString();
            hoadon.SoTienKhauTru = Convert.ToDecimal(iva.NBG.GetValue(), cltInfo);
            hoadon.MaTT_TienKhauTru = iva.NBC.GetValue().ToString();
            hoadon.LoaiKhauTru = iva.NBS.GetValue().ToString();
            hoadon.SoTienKhac = Convert.ToDecimal(iva.SKG.GetValue(), cltInfo);
            hoadon.MaTT_TienKhac = iva.SKC.GetValue().ToString();
            hoadon.LoaiSoTienKhac = iva.SKS.GetValue().ToString();
            hoadon.TongTriGiaHoaDon = Convert.ToDecimal(iva.IP4.GetValue(), cltInfo);
            hoadon.MaTT_TongTriGia = iva.IP3.GetValue().ToString();
            hoadon.DieuKienGiaHoaDon = iva.IP2.GetValue().ToString();
            hoadon.DiaDiemGiaoHang = iva.HWT.GetValue().ToString();
            hoadon.GhiChuDacBiet = iva.NT1.GetValue().ToString();
            hoadon.TongSoDongHang = Convert.ToDecimal(iva.SNM.GetValue(), cltInfo);
            List<KDT_VNACC_PhanLoaiHoaDon> listphanloai = new List<KDT_VNACC_PhanLoaiHoaDon>();
            if (iva.NOM.listAttribute != null && iva.NOM.listAttribute[0].ListValue != null)
            {

                for (int i = 0; i < iva.NOM.listAttribute[0].ListValue.Count; i++)
                {
                    if (!string.IsNullOrEmpty(iva.NOM.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        KDT_VNACC_PhanLoaiHoaDon phanloai = new KDT_VNACC_PhanLoaiHoaDon()
                        {
                            MaPhaLoai = iva.NOM.listAttribute[0].GetValueCollection(i).ToString(),
                            So = iva.NOM.listAttribute[1].GetValueCollection(i).ToString(),
                            NgayThangNam = Convert.ToDateTime(iva.NOM.listAttribute[2].GetValueCollection(i))
                        };
                        if (phanloai.MaPhaLoai != null && !string.IsNullOrEmpty(phanloai.MaPhaLoai.Trim()))
                            listphanloai.Add(phanloai);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_PhanLoaiHoaDon>(hoadon.PhanLoaiCollection, listphanloai);

            for (int i = 0; i < iva.HangHoaTrongHoaDon.Count; i++)
            {
                if (i < hoadon.HangCollection.Count)
                    hoadon.HangCollection[i] = GetHangFromIVA(iva.HangHoaTrongHoaDon[i], hoadon.HangCollection[i]);
            }
            //             foreach (var item in iva.HangHoaTrongHoaDon)
            //             {
            //                 hoadon.HangCollection.Add(GetHangFromIVA(item));
            //             }
            return hoadon;
        }

        public static KDT_VNACC_HoaDon GetFromIVA(VAL0940 iva, KDT_VNACC_HoaDon hoadon)
        {
            //KDT_VNACC_HoaDon hoadon = new KDT_VNACC_HoaDon();

            hoadon.SoTiepNhan = Convert.ToDecimal(iva.NIV.GetValue(), cltInfo);
            hoadon.PhanLoaiXuatNhap = iva.YNK.GetValue().ToString();
            hoadon.MaDaiLyHQ = iva.TII.GetValue().ToString();
            hoadon.SoHoaDon = iva.IVN.GetValue().ToString();
            hoadon.NgayLapHoaDon = Convert.ToDateTime(iva.IVD.GetValue());
            hoadon.DiaDiemLapHoaDon = iva.IVB.GetValue().ToString();
            hoadon.PhuongThucThanhToan = iva.HOS.GetValue().ToString();
            hoadon.MaNguoiXNK = iva.IMC.GetValue().ToString();
            hoadon.TenNguoiXNK = iva.IMN.GetValue().ToString();
            hoadon.MaBuuChinh_NguoiXNK = iva.IMY.GetValue().ToString();
            hoadon.DiaChi_NguoiXNK = iva.IMA.GetValue().ToString();
            hoadon.SoDienThoai_NguoiXNK = iva.IMT.GetValue().ToString();
            hoadon.NguoiLapHoaDon = iva.SAA.GetValue().ToString();
            hoadon.MaNguoiGuiNhan = iva.EPC.GetValue().ToString();
            hoadon.TenNguoiGuiNhan = iva.EPN.GetValue().ToString();
            hoadon.MaBuuChinhNguoiGuiNhan = iva.EP1.GetValue().ToString();
            hoadon.DiaChiNguoiNhanGui1 = iva.EPA.GetValue().ToString();
            hoadon.DiaChiNguoiNhanGui2 = iva.EP2.GetValue().ToString();
            hoadon.DiaChiNguoiNhanGui3 = iva.EP3.GetValue().ToString();
            hoadon.DiaChiNguoiNhanGui4 = iva.EP4.GetValue().ToString();
            hoadon.SoDienThoaiNhanGui = iva.EP6.GetValue().ToString();
            hoadon.NuocSoTaiNhanGui = iva.EEE.GetValue().ToString();
            hoadon.MaKyHieu = iva.KNO.GetValue().ToString();
            hoadon.PhanLoaiVanChuyen = iva.TTP.GetValue().ToString();
            hoadon.TenPTVC = iva.VSS.GetValue().ToString();
            hoadon.SoHieuChuyenDi = iva.VNO.GetValue().ToString();
            hoadon.MaDiaDiemXepHang = iva.PSC.GetValue().ToString();
            hoadon.TenDiaDiemXepHang = iva.PSN.GetValue().ToString();
            hoadon.ThoiKyXephang = Convert.ToDateTime(iva.PSD.GetValue());
            hoadon.MaDiaDiemDoHang = iva.DST.GetValue().ToString();
            hoadon.TenDiaDiemDoHang = iva.DCN.GetValue().ToString();
            hoadon.MaDiaDiemTrungChuyen = iva.KYC.GetValue().ToString();
            hoadon.TenDiaDiemTrungChuyen = iva.KYN.GetValue().ToString();
            hoadon.TrongLuongGross = Convert.ToDecimal(iva.GWJ.GetValue(), cltInfo);
            hoadon.MaDVT_TrongLuongGross = iva.GWT.GetValue().ToString();
            hoadon.TrongLuongThuan = Convert.ToDecimal(iva.JWJ.GetValue(), cltInfo);
            hoadon.MaDVT_TrongLuongThuan = iva.JWT.GetValue().ToString();
            hoadon.TongTheTich = Convert.ToDecimal(iva.STJ.GetValue(), cltInfo);
            hoadon.MaDVT_TheTich = iva.STT.GetValue().ToString();
            hoadon.TongSoKienHang = Convert.ToDecimal(iva.NOJ.GetValue(), cltInfo);
            hoadon.MaDVT_KienHang = iva.NOT.GetValue().ToString();
            hoadon.GhiChuChuHang = iva.NT3.GetValue().ToString();
            hoadon.SoPL = iva.PLN.GetValue().ToString();
            hoadon.NganHangLC = iva.LCB.GetValue().ToString();
            hoadon.TriGiaFOB = Convert.ToDecimal(iva.FON.GetValue(), cltInfo);
            hoadon.MaTT_FOB = iva.FOT.GetValue().ToString();
            hoadon.SoTienFOB = Convert.ToDecimal(iva.FKK.GetValue(), cltInfo);
            hoadon.MaTT_TienFOB = iva.FKT.GetValue().ToString();
            hoadon.PhiVanChuyen = Convert.ToDecimal(iva.FR3.GetValue(), cltInfo);
            hoadon.MaTT_PhiVC = iva.FR2.GetValue().ToString();
            hoadon.NoiThanhToanPhiVC = iva.FR4.GetValue().ToString();
            hoadon.ChiPhiXepHang1 = Convert.ToDecimal(iva.FS1.GetValue(), cltInfo);
            hoadon.MaTT_ChiPhiXepHang1 = iva.FT1.GetValue().ToString();
            hoadon.LoaiChiPhiXepHang1 = iva.FH1.GetValue().ToString();
            hoadon.ChiPhiXepHang2 = Convert.ToDecimal(iva.FS2.GetValue(), cltInfo);
            hoadon.MaTT_ChiPhiXepHang2 = iva.FT2.GetValue().ToString();
            hoadon.LoaiChiPhiXepHang2 = iva.FH2.GetValue().ToString();
            hoadon.PhiVC_DuongBo = Convert.ToDecimal(iva.NUH.GetValue(), cltInfo);
            hoadon.MaTT_PhiVC_DuongBo = iva.NTK.GetValue().ToString();
            hoadon.PhiBaoHiem = Convert.ToDecimal(iva.IN3.GetValue(), cltInfo);
            hoadon.MaTT_PhiBaoHiem = iva.IN2.GetValue().ToString();
            hoadon.SoTienPhiBaoHiem = Convert.ToDecimal(iva.IK1.GetValue(), cltInfo);
            hoadon.MaTT_TienPhiBaoHiem = iva.IK2.GetValue().ToString();
            hoadon.SoTienKhauTru = Convert.ToDecimal(iva.NBG.GetValue(), cltInfo);
            hoadon.MaTT_TienKhauTru = iva.NBC.GetValue().ToString();
            hoadon.LoaiKhauTru = iva.NBS.GetValue().ToString();
            hoadon.SoTienKhac = Convert.ToDecimal(iva.SKG.GetValue(), cltInfo);
            hoadon.MaTT_TienKhac = iva.SKC.GetValue().ToString();
            hoadon.LoaiSoTienKhac = iva.SKS.GetValue().ToString();
            hoadon.TongTriGiaHoaDon = Convert.ToDecimal(iva.IP4.GetValue(), cltInfo);
            hoadon.MaTT_TongTriGia = iva.IP3.GetValue().ToString();
            hoadon.DieuKienGiaHoaDon = iva.IP2.GetValue().ToString();
            hoadon.DiaDiemGiaoHang = iva.HWT.GetValue().ToString();
            hoadon.GhiChuDacBiet = iva.NT1.GetValue().ToString();
            hoadon.TongSoDongHang = Convert.ToDecimal(iva.SNM.GetValue(), cltInfo);
            List<KDT_VNACC_PhanLoaiHoaDon> listphanloai = new List<KDT_VNACC_PhanLoaiHoaDon>();
            if (iva.NOM.listAttribute != null && iva.NOM.listAttribute[0].ListValue != null)
            {

                for (int i = 0; i < iva.NOM.listAttribute[0].ListValue.Count; i++)
                {
                    if (!string.IsNullOrEmpty(iva.NOM.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        KDT_VNACC_PhanLoaiHoaDon phanloai = new KDT_VNACC_PhanLoaiHoaDon()
                        {
                            MaPhaLoai = iva.NOM.listAttribute[0].GetValueCollection(i).ToString(),
                            So = iva.NOM.listAttribute[1].GetValueCollection(i).ToString(),
                            NgayThangNam = Convert.ToDateTime(iva.NOM.listAttribute[2].GetValueCollection(i))
                        };
                        if (phanloai.MaPhaLoai != null && !string.IsNullOrEmpty(phanloai.MaPhaLoai.Trim()))
                            listphanloai.Add(phanloai);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_PhanLoaiHoaDon>(hoadon.PhanLoaiCollection, listphanloai);

            for (int i = 0; i < iva.HangHoaTrongHoaDon.Count; i++)
            {
                if (i < hoadon.HangCollection.Count)
                    hoadon.HangCollection[i] = GetHangFromIVA(iva.HangHoaTrongHoaDon[i], hoadon.HangCollection[i]);
            }
            //foreach (var item in iva.HangHoaTrongHoaDon)
            //{
            //    hoadon.HangCollection.Add(GetHangFromIVA(item));
            //}
            return hoadon;
        }

        public static KDT_VNACC_HangHoaDon GetHangFromIVA(IVA_HANGHOA iva, KDT_VNACC_HangHoaDon hanghoadon)
        {
            //KDT_VNACC_HangHoaDon hanghoadon = new KDT_VNACC_HangHoaDon();
            //hanghoadon = Convert.ToDecimal(iva.RNO.GetValue());
            if (string.IsNullOrEmpty(iva.CMD.GetValue().ToString().Trim()))
                return hanghoadon;
            hanghoadon.MaHang = iva.SNO.GetValue().ToString();
            hanghoadon.MaSoHang = iva.CMD.GetValue().ToString();
            hanghoadon.TenHang = iva.CMN.GetValue().ToString();
            hanghoadon.MaNuocXX = iva.ORC.GetValue().ToString();
            hanghoadon.TenNuocXX = iva.ORN.GetValue().ToString();
            hanghoadon.SoKienHang = iva.KBN.GetValue().ToString();
            hanghoadon.SoLuong1 = Convert.ToDecimal(iva.QN1.GetValue(), cltInfo);
            hanghoadon.MaDVT_SoLuong1 = iva.QT1.GetValue().ToString();
            hanghoadon.SoLuong2 = Convert.ToDecimal(iva.QN2.GetValue(), cltInfo);
            hanghoadon.MaDVT_SoLuong2 = iva.QT2.GetValue().ToString();
            hanghoadon.DonGiaHoaDon = Convert.ToDecimal(iva.TNK.GetValue(), cltInfo);
            hanghoadon.MaTT_DonGia = iva.TNC.GetValue().ToString();
            hanghoadon.MaDVT_DonGia = iva.TSC.GetValue().ToString();
            hanghoadon.TriGiaHoaDon = Convert.ToDecimal(iva.KKT.GetValue(), cltInfo);
            hanghoadon.MaTT_GiaTien = iva.KKC.GetValue().ToString();
            hanghoadon.LoaiKhauTru = iva.NSR.GetValue().ToString();
            hanghoadon.SoTienKhauTru = Convert.ToDecimal(iva.NGA.GetValue(), cltInfo);
            hanghoadon.MaTT_TienKhauTru = iva.NTC.GetValue().ToString();
            return hanghoadon;
        }

        public static KDT_VNACCS_TKTriGiaThap GetFromMIC(MIC mic, KDT_VNACCS_TKTriGiaThap ToKhaiTGThap)
        {
            //KDT_VNACCS_TKTriGiaThap ToKhaiTGThap = new KDT_VNACCS_TKTriGiaThap();
            ToKhaiTGThap.SoToKhai = Convert.ToDecimal(mic.ICN.GetValue(), cltInfo);
            ToKhaiTGThap.PhanLoaiBaoCaoSua = mic.JYO.GetValue().ToString();
            ToKhaiTGThap.PhanLoaiToChuc = mic.SKB.GetValue().ToString();
            ToKhaiTGThap.CoQuanHaiQuan = mic.CH.GetValue().ToString();
            ToKhaiTGThap.NhomXuLyHS = mic.CHB.GetValue().ToString();
            ToKhaiTGThap.MaDonVi = mic.IMC.GetValue().ToString();
            ToKhaiTGThap.TenDonVi = mic.IMN.GetValue().ToString();
            ToKhaiTGThap.MaBuuChinhDonVi = mic.IMY.GetValue().ToString();
            ToKhaiTGThap.DiaChiDonVi = mic.IMA.GetValue().ToString();
            ToKhaiTGThap.SoDienThoaiDonVi = mic.IMT.GetValue().ToString();
            ToKhaiTGThap.MaDoiTac = mic.EPC.GetValue().ToString();
            ToKhaiTGThap.TenDoiTac = mic.EPN.GetValue().ToString();
            ToKhaiTGThap.MaBuuChinhDoiTac = mic.EPY.GetValue().ToString();
            ToKhaiTGThap.DiaChiDoiTac1 = mic.EPA.GetValue().ToString();
            ToKhaiTGThap.DiaChiDoiTac2 = mic.EP2.GetValue().ToString();
            ToKhaiTGThap.DiaChiDoiTac3 = mic.EP3.GetValue().ToString();
            ToKhaiTGThap.DiaChiDoiTac4 = mic.EP4.GetValue().ToString();
            ToKhaiTGThap.MaNuocDoiTac = mic.EPO.GetValue().ToString();
            ToKhaiTGThap.SoHouseAWB = mic.HAB.GetValue().ToString();
            ToKhaiTGThap.SoMasterAWB = mic.MAB.GetValue().ToString();
            ToKhaiTGThap.SoLuong = Convert.ToDecimal(mic.NO.GetValue(), cltInfo);
            ToKhaiTGThap.TrongLuong = Convert.ToDecimal(mic.GW.GetValue(), cltInfo);
            ToKhaiTGThap.MaDDLuuKho = mic.ST.GetValue().ToString();
            ToKhaiTGThap.TenMayBayChoHang = mic.VSN.GetValue().ToString();
            ToKhaiTGThap.NgayHangDen = Convert.ToDateTime(mic.ARR.GetValue());
            ToKhaiTGThap.MaCangDoHang = mic.DST.GetValue().ToString();
            ToKhaiTGThap.MaDiaDiemXepHang = mic.PSC.GetValue().ToString();
            ToKhaiTGThap.PhanLoaiGiaHD = mic.IP1.GetValue().ToString();
            ToKhaiTGThap.MaDieuKienGiaHD = mic.IP2.GetValue().ToString();
            ToKhaiTGThap.MaTTHoaDon = mic.IP3.GetValue().ToString();
            ToKhaiTGThap.TongTriGiaHD = Convert.ToDecimal(mic.IP4.GetValue(), cltInfo);
            ToKhaiTGThap.MaPhanLoaiPhiVC = mic.FR1.GetValue().ToString();
            ToKhaiTGThap.MaTTPhiVC = mic.FR2.GetValue().ToString();
            ToKhaiTGThap.PhiVanChuyen = Convert.ToDecimal(mic.FR3.GetValue(), cltInfo);
            ToKhaiTGThap.MaPhanLoaiPhiBH = mic.IN1.GetValue().ToString();
            ToKhaiTGThap.MaTTPhiBH = mic.IN2.GetValue().ToString();
            ToKhaiTGThap.PhiBaoHiem = Convert.ToDecimal(mic.IN3.GetValue(), cltInfo);
            ToKhaiTGThap.MoTaHangHoa = mic.CMN.GetValue().ToString();
            ToKhaiTGThap.MaNuocXuatXu = mic.OR.GetValue().ToString();
            ToKhaiTGThap.TriGiaTinhThue = Convert.ToDecimal(mic.DPR.GetValue(), cltInfo);
            ToKhaiTGThap.GhiChu = mic.NT1.GetValue().ToString();
            ToKhaiTGThap.SoQuanLyNoiBoDN = mic.REF.GetValue().ToString();

            return ToKhaiTGThap;

        }

        public static KDT_VNACCS_TKTriGiaThap GetFromMEC(MEC mec, KDT_VNACCS_TKTriGiaThap ToKhaiTGThap)
        {
            //KDT_VNACCS_TKTriGiaThap ToKhaiTGThap = new KDT_VNACCS_TKTriGiaThap();
            ToKhaiTGThap.SoToKhai = Convert.ToDecimal(mec.ECN.GetValue(), cltInfo);
            ToKhaiTGThap.PhanLoaiBaoCaoSua = mec.JKN.GetValue().ToString();
            ToKhaiTGThap.CoQuanHaiQuan = mec.CH.GetValue().ToString();
            ToKhaiTGThap.NhomXuLyHS = mec.CHB.GetValue().ToString();
            ToKhaiTGThap.MaDonVi = mec.EPC.GetValue().ToString();
            ToKhaiTGThap.TenDonVi = mec.EPN.GetValue().ToString();
            ToKhaiTGThap.MaBuuChinhDonVi = mec.EPP.GetValue().ToString();
            ToKhaiTGThap.DiaChiDonVi = mec.EPA.GetValue().ToString();
            ToKhaiTGThap.SoDienThoaiDonVi = mec.EPT.GetValue().ToString();
            ToKhaiTGThap.MaDoiTac = mec.CGC.GetValue().ToString();
            ToKhaiTGThap.TenDoiTac = mec.CGN.GetValue().ToString();
            ToKhaiTGThap.MaBuuChinhDoiTac = mec.CGP.GetValue().ToString();
            ToKhaiTGThap.DiaChiDoiTac1 = mec.CGA.GetValue().ToString();
            ToKhaiTGThap.DiaChiDoiTac2 = mec.CAT.GetValue().ToString();
            ToKhaiTGThap.DiaChiDoiTac3 = mec.CAC.GetValue().ToString();
            ToKhaiTGThap.DiaChiDoiTac4 = mec.CAS.GetValue().ToString();
            ToKhaiTGThap.MaNuocDoiTac = mec.CGK.GetValue().ToString();
            ToKhaiTGThap.SoHouseAWB = mec.AWB.GetValue().ToString();
            ToKhaiTGThap.SoLuong = Convert.ToDecimal(mec.NO.GetValue(), cltInfo);
            ToKhaiTGThap.TrongLuong = Convert.ToDecimal(mec.GW.GetValue(), cltInfo);
            ToKhaiTGThap.MaDDLuuKho = mec.ST.GetValue().ToString();
            ToKhaiTGThap.MaDDNhanHangCuoiCung = mec.DSC.GetValue().ToString();
            ToKhaiTGThap.MaDiaDiemXepHang = mec.PSC.GetValue().ToString();
            ToKhaiTGThap.MaTTTongTriGiaTinhThue = mec.FCD.GetValue().ToString();
            ToKhaiTGThap.TongTriGiaTinhThue = Convert.ToDecimal(mec.FKK.GetValue(), cltInfo);
            ToKhaiTGThap.GiaKhaiBao = Convert.ToDecimal(mec.SKK.GetValue(), cltInfo);
            ToKhaiTGThap.MoTaHangHoa = mec.CMN.GetValue().ToString();
            ToKhaiTGThap.GhiChu = mec.NT.GetValue().ToString();
            ToKhaiTGThap.SoQuanLyNoiBoDN = mec.REF.GetValue().ToString();



            return ToKhaiTGThap;

        }

        public static KDT_VNACC_ToKhaiMauDich GetTKMDFromVAD1AG(VAD1AG0 VAD1AG, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            //KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();

            TKMD.SoToKhai = Convert.ToDecimal(VAD1AG.ICN.GetValue(), cltInfo);
            TKMD.PhanLoaiBaoCaoSuaDoi = VAD1AG.JKN.GetValue().ToString();
            TKMD.SoToKhaiDauTien = VAD1AG.FIC.GetValue().ToString();
            TKMD.SoNhanhToKhai = Convert.ToDecimal(VAD1AG.BNO.GetValue(), cltInfo);
            TKMD.TongSoTKChiaNho = Convert.ToDecimal(VAD1AG.DNO.GetValue(), cltInfo);
            TKMD.SoToKhaiTNTX = Convert.ToDecimal(VAD1AG.TDN.GetValue(), cltInfo);
            TKMD.MaPhanLoaiKiemTra = VAD1AG.A06.GetValue().ToString();
            TKMD.MaLoaiHinh = VAD1AG.ICB.GetValue().ToString();
            TKMD.MaPhanLoaiHH = VAD1AG.CCC.GetValue().ToString();
            TKMD.MaPhuongThucVT = VAD1AG.MTC.GetValue().ToString();
            TKMD.PhanLoaiToChuc = VAD1AG.SKB.GetValue().ToString();
            TKMD.MaSoThueDaiDien = VAD1AG.A00.GetValue().ToString();
            TKMD.TenCoQuanHaiQuan = VAD1AG.A07.GetValue().ToString();
            TKMD.NhomXuLyHS = VAD1AG.CHB.GetValue().ToString();
            TKMD.NgayDangKy = Convert.ToDateTime(VAD1AG.A09.GetValue());
            TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAD1AG.AD1.GetValue().ToString());
            TKMD.NgayThayDoiDangKy = Convert.ToDateTime(VAD1AG.AD2.GetValue());
            TKMD.NgayThayDoiDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayThayDoiDangKy, VAD1AG.AD3.GetValue().ToString());
            TKMD.ThoiHanTaiNhapTaiXuat = Convert.ToDateTime(VAD1AG.RED.GetValue());
            TKMD.BieuThiTruongHopHetHan = VAD1AG.AAA.GetValue().ToString();
            TKMD.MaDonVi = VAD1AG.IMC.GetValue().ToString();
            TKMD.TenDonVi = VAD1AG.IMN.GetValue().ToString();
            TKMD.MaBuuChinhDonVi = VAD1AG.IMY.GetValue().ToString();
            TKMD.DiaChiDonVi = VAD1AG.IMA.GetValue().ToString();
            TKMD.SoDienThoaiDonVi = VAD1AG.IMT.GetValue().ToString();
            TKMD.MaUyThac = VAD1AG.NMC.GetValue().ToString();
            TKMD.TenUyThac = VAD1AG.NMN.GetValue().ToString();
            TKMD.MaDoiTac = VAD1AG.EPC.GetValue().ToString();
            TKMD.TenDoiTac = VAD1AG.EPN.GetValue().ToString();
            TKMD.MaBuuChinhDoiTac = VAD1AG.EPY.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAD1AG.EPA.GetValue().ToString();
            TKMD.DiaChiDoiTac2 = VAD1AG.EP2.GetValue().ToString();
            TKMD.DiaChiDoiTac3 = VAD1AG.EP3.GetValue().ToString();
            TKMD.DiaChiDoiTac4 = VAD1AG.EP4.GetValue().ToString();
            TKMD.MaNuocDoiTac = VAD1AG.EPO.GetValue().ToString();
            TKMD.NguoiUyThacXK = VAD1AG.ENM.GetValue().ToString();
            TKMD.MaDaiLyHQ = VAD1AG.A37.GetValue().ToString();
            TKMD.TenDaiLyHaiQuan = VAD1AG.A38.GetValue().ToString();
            TKMD.MaNhanVienHaiQuan = VAD1AG.A39.GetValue().ToString();
            #region Vận đơn
            if (TKMD.VanDonCollection == null) TKMD.VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
            List<KDT_VNACC_TK_SoVanDon> ListVanDon = new List<KDT_VNACC_TK_SoVanDon>();
            if (VAD1AG.BL_.listAttribute != null && VAD1AG.BL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.BL_.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TK_SoVanDon SoVanDon = new KDT_VNACC_TK_SoVanDon();
                    SoVanDon.SoTT = i + 1;
                    SoVanDon.SoVanDon = VAD1AG.BL_.listAttribute[0].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(SoVanDon.SoVanDon.Trim()))
                        ListVanDon.Add(SoVanDon);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_SoVanDon>(TKMD.VanDonCollection, ListVanDon);
            #endregion
            TKMD.SoLuong = Convert.ToDecimal(VAD1AG.NO.GetValue(), cltInfo);
            TKMD.MaDVTSoLuong = VAD1AG.NOT.GetValue().ToString();
            TKMD.TrongLuong = Convert.ToDecimal(VAD1AG.GW.GetValue(), cltInfo);
            TKMD.MaDVTTrongLuong = VAD1AG.GWT.GetValue().ToString();
            TKMD.SoLuongCont = Convert.ToDecimal(VAD1AG.COC.GetValue(), cltInfo);
            TKMD.MaDDLuuKho = VAD1AG.ST.GetValue().ToString();
            TKMD.TenDDLuuKho = VAD1AG.A51.GetValue().ToString();
            TKMD.MaDiaDiemDoHang = VAD1AG.DST.GetValue().ToString();
            TKMD.TenDiaDiemDohang = VAD1AG.DSN.GetValue().ToString();
            TKMD.MaDiaDiemXepHang = VAD1AG.PSC.GetValue().ToString();
            TKMD.TenDiaDiemXepHang = VAD1AG.PSN.GetValue().ToString();
            TKMD.MaPTVC = VAD1AG.VSC.GetValue().ToString();
            TKMD.TenPTVC = VAD1AG.VSN.GetValue().ToString();
            TKMD.NgayHangDen = Convert.ToDateTime(VAD1AG.ARR.GetValue());
            TKMD.SoHieuKyHieu = VAD1AG.MRK.GetValue().ToString();
            TKMD.NgayNhapKhoDau = Convert.ToDateTime(VAD1AG.ISD.GetValue());
            #region Mã văn bản pháp quy khác

            if (VAD1AG.OL_.listAttribute != null && VAD1AG.OL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.OL_.listAttribute[0].ListValue.Count; i++)
                {
                    if (string.IsNullOrEmpty(VAD1AG.OL_.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        switch (i)
                        {
                            case 0:
                                TKMD.MaVanbanPhapQuy1 = VAD1AG.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 1:
                                TKMD.MaVanbanPhapQuy2 = VAD1AG.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 2:
                                TKMD.MaVanbanPhapQuy3 = VAD1AG.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 3:
                                TKMD.MaVanbanPhapQuy4 = VAD1AG.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 4:
                                TKMD.MaVanbanPhapQuy5 = VAD1AG.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                        }
                    }
                }
            }
            #endregion

            TKMD.PhanLoaiHD = VAD1AG.IV1.GetValue().ToString();
            TKMD.SoHoaDon = VAD1AG.IV3.GetValue().ToString();
            TKMD.SoTiepNhanHD = Convert.ToDecimal(VAD1AG.IV2.GetValue(), cltInfo);
            TKMD.NgayPhatHanhHD = Convert.ToDateTime(VAD1AG.IVD.GetValue());
            TKMD.PhuongThucTT = VAD1AG.IVP.GetValue().ToString();
            TKMD.PhanLoaiGiaHD = VAD1AG.IP1.GetValue().ToString();
            TKMD.MaDieuKienGiaHD = VAD1AG.IP2.GetValue().ToString();
            TKMD.MaTTHoaDon = VAD1AG.IP3.GetValue().ToString();
            TKMD.TongTriGiaHD = Convert.ToDecimal(VAD1AG.IP4.GetValue(), cltInfo);
            TKMD.TriGiaTinhThue = Convert.ToDecimal(VAD1AG.A86.GetValue(), cltInfo);
            //TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(VAD1AG.TP.GetValue(), cltInfo);
            TKMD.MaPhanLoaiTongGiaCoBan = VAD1AG.A97.GetValue().ToString();
            TKMD.MaKetQuaKiemTra = VAD1AG.N4.GetValue().ToString();
            #region Giấy phép
            List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
            if (VAD1AG.SS_.listAttribute != null && VAD1AG.SS_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.SS_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.GiayPhepCollection == null) TKMD.GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
                    KDT_VNACC_TK_GiayPhep giayPhep = new KDT_VNACC_TK_GiayPhep();
                    giayPhep.PhanLoai = VAD1AG.SS_.listAttribute[0].GetValueCollection(i).ToString();
                    giayPhep.SoGiayPhep = VAD1AG.SS_.listAttribute[1].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(giayPhep.SoGiayPhep))
                        listGiayPhep.Add(giayPhep);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_GiayPhep>(TKMD.GiayPhepCollection, listGiayPhep);
            #endregion

            TKMD.MaPhanLoaiTriGia = VAD1AG.VD1.GetValue().ToString();
            TKMD.SoTiepNhanTKTriGia = string.IsNullOrEmpty(VAD1AG.VD2.GetValue().ToString()) ? 0 : Convert.ToDecimal(VAD1AG.VD2.GetValue(), cltInfo); //Convert.ToDecimal(VAD1AG.VD2.GetValue(), cltInfo);
            TKMD.PhanLoaiCongThucChuan = VAD1AG.A93.GetValue().ToString();
            TKMD.MaPhanLoaiDieuChinhTriGia = VAD1AG.A94.GetValue().ToString();
            TKMD.PhuongPhapDieuChinhTriGia = VAD1AG.A95.GetValue().ToString();
            TKMD.MaTTHieuChinhTriGia = VAD1AG.VCC.GetValue().ToString();
            TKMD.GiaHieuChinhTriGia = Convert.ToDecimal(VAD1AG.VPC.GetValue(), cltInfo);
            TKMD.MaPhanLoaiPhiVC = VAD1AG.FR1.GetValue().ToString();
            TKMD.MaTTPhiVC = VAD1AG.FR2.GetValue().ToString();
            TKMD.PhiVanChuyen = Convert.ToDecimal(VAD1AG.FR3.GetValue(), cltInfo);
            TKMD.MaPhanLoaiPhiBH = VAD1AG.IN1.GetValue().ToString();
            TKMD.MaTTPhiBH = VAD1AG.IN2.GetValue().ToString();
            TKMD.PhiBaoHiem = Convert.ToDecimal(VAD1AG.IN3.GetValue(), cltInfo);
            TKMD.SoDangKyBH = VAD1AG.IN4.GetValue().ToString();
            #region Khoản điều chỉnh
            List<KDT_VNACC_TK_KhoanDieuChinh> ListKDC = new List<KDT_VNACC_TK_KhoanDieuChinh>();
            if (VAD1AG.VR_.listAttribute != null && VAD1AG.VR_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.VR_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.KhoanDCCollection == null) TKMD.KhoanDCCollection = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    KDT_VNACC_TK_KhoanDieuChinh khoanDieuChinh = new KDT_VNACC_TK_KhoanDieuChinh();
                    khoanDieuChinh.MaTenDieuChinh = VAD1AG.VR_.listAttribute[0].GetValueCollection(i).ToString();
                    khoanDieuChinh.MaPhanLoaiDieuChinh = VAD1AG.VR_.listAttribute[1].GetValueCollection(i).ToString();
                    khoanDieuChinh.MaTTDieuChinhTriGia = VAD1AG.VR_.listAttribute[2].GetValueCollection(i).ToString();
                    khoanDieuChinh.TriGiaKhoanDieuChinh = Convert.ToDecimal(VAD1AG.VR_.listAttribute[3].GetValueCollection(i).ToString(), cltInfo);
                    khoanDieuChinh.TongHeSoPhanBo = Convert.ToDecimal(VAD1AG.VR_.listAttribute[4].GetValueCollection(i).ToString(), cltInfo);
                    khoanDieuChinh.SoTT = i + 1;
                    if (!string.IsNullOrEmpty(khoanDieuChinh.MaTenDieuChinh) || !string.IsNullOrEmpty(khoanDieuChinh.MaPhanLoaiDieuChinh))
                        ListKDC.Add(khoanDieuChinh);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_KhoanDieuChinh>(TKMD.KhoanDCCollection, ListKDC);
            #endregion

            TKMD.ChiTietKhaiTriGia = VAD1AG.VLD.GetValue().ToString();
            #region Sắc thuế
            List<KDT_VNACC_TK_PhanHoi_SacThue> lSacThue = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
            if (VAD1AG.KF1.listAttribute != null && VAD1AG.KF1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.KF1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.SacThueCollection == null) TKMD.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                    KDT_VNACC_TK_PhanHoi_SacThue sacthue = new KDT_VNACC_TK_PhanHoi_SacThue();
                    sacthue.MaSacThue = VAD1AG.KF1.listAttribute[0].GetValueCollection(i).ToString();
                    sacthue.TenSacThue = VAD1AG.KF1.listAttribute[1].GetValueCollection(i).ToString();
                    sacthue.TongTienThue = System.Convert.ToDecimal(VAD1AG.KF1.listAttribute[2].GetValueCollection(i));
                    sacthue.SoDongTongTienThue = System.Convert.ToDecimal(VAD1AG.KF1.listAttribute[3].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(sacthue.MaSacThue))
                        lSacThue.Add(sacthue);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_SacThue>(TKMD.SacThueCollection, lSacThue);
            #endregion
            TKMD.TongTienThuePhaiNop = Convert.ToDecimal(VAD1AG.B02.GetValue(), cltInfo);
            TKMD.SoTienBaoLanh = Convert.ToDecimal(VAD1AG.B03.GetValue(), cltInfo);
            #region Tỷ giá
            List<KDT_VNACC_TK_PhanHoi_TyGia> lTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
            if (VAD1AG.KJ1.listAttribute != null && VAD1AG.KJ1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.KJ1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TyGiaCollection == null) TKMD.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    KDT_VNACC_TK_PhanHoi_TyGia tygia = new KDT_VNACC_TK_PhanHoi_TyGia();
                    tygia.MaTTTyGiaTinhThue = VAD1AG.KJ1.listAttribute[0].GetValueCollection(i).ToString();
                    tygia.TyGiaTinhThue = Convert.ToDecimal(VAD1AG.KJ1.listAttribute[1].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(tygia.MaTTTyGiaTinhThue))
                    {
                        lTyGia.Add(tygia);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_TyGia>(TKMD.TyGiaCollection, lTyGia);
            #endregion

            TKMD.MaXDThoiHanNopThue = VAD1AG.ENC.GetValue().ToString();
            TKMD.NguoiNopThue = VAD1AG.TPM.GetValue().ToString();
            TKMD.MaLyDoDeNghiBP = VAD1AG.BP.GetValue().ToString();
            TKMD.PhanLoaiNopThue = VAD1AG.B08.GetValue().ToString();
            TKMD.TongSoTrangCuaToKhai = Convert.ToDecimal(VAD1AG.B12.GetValue(), cltInfo);
            TKMD.TongSoDongHangCuaToKhai = Convert.ToDecimal(VAD1AG.B13.GetValue(), cltInfo);
            #region Phân loại đính kèm khai báo điện tử
            List<KDT_VNACC_TK_DinhKemDienTu> lDinhKem = new List<KDT_VNACC_TK_DinhKemDienTu>();
            if (VAD1AG.EA_.listAttribute != null && VAD1AG.EA_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.EA_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.DinhKemCollection == null) TKMD.DinhKemCollection = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    KDT_VNACC_TK_DinhKemDienTu dinhkem = new KDT_VNACC_TK_DinhKemDienTu();
                    dinhkem.PhanLoai = VAD1AG.EA_.listAttribute[0].GetValueCollection(i).ToString();
                    dinhkem.SoDinhKemKhaiBaoDT = System.Convert.ToDecimal(VAD1AG.EA_.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(dinhkem.PhanLoai) || dinhkem.SoDinhKemKhaiBaoDT > 0)
                    {
                        lDinhKem.Add(dinhkem);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_DinhKemDienTu>(TKMD.DinhKemCollection, lDinhKem);
            #endregion

            TKMD.GhiChu = VAD1AG.NT2.GetValue().ToString();
            TKMD.SoQuanLyNoiBoDN = VAD1AG.REF.GetValue().ToString();
            TKMD.SoQuanLyNguoiSuDung = VAD1AG.B16.GetValue().ToString();
            //TKMD.PhanLoaiChi = VAD1AG.CCM.GetValue().ToString();
            #region Chỉ thị HQ
            List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
            if (VAD1AG.D__.listAttribute != null && VAD1AG.D__.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.D__.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ChiThiHQCollection == null) TKMD.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                    ChiThiHQ.PhanLoai = VAD1AG.CCM.GetValue().ToString();
                    ChiThiHQ.Ngay = Convert.ToDateTime(VAD1AG.D__.listAttribute[0].GetValueCollection(i));
                    ChiThiHQ.Ten = VAD1AG.D__.listAttribute[1].GetValueCollection(i).ToString();
                    ChiThiHQ.NoiDung = VAD1AG.D__.listAttribute[2].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(ChiThiHQ.Ten) || !string.IsNullOrEmpty(ChiThiHQ.NoiDung))
                        lChiThi.Add(ChiThiHQ);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_ChiThiHaiQuan>(TKMD.ChiThiHQCollection, lChiThi);
            #endregion
            TKMD.TenTruongDonViHaiQuan = VAD1AG.C20.GetValue().ToString();
            TKMD.NgayCapPhep = Convert.ToDateTime(VAD1AG.C21.GetValue());
            TKMD.NgayCapPhep = HelperVNACCS.AddHoursToDateTime(TKMD.NgayCapPhep, VAD1AG.AD4.GetValue().ToString());
            TKMD.NgayHoanThanhKiemTra = Convert.ToDateTime(VAD1AG.C22.GetValue());
            TKMD.NgayHoanThanhKiemTra = HelperVNACCS.AddHoursToDateTime(TKMD.NgayHoanThanhKiemTra, VAD1AG.AD5.GetValue().ToString());
            TKMD.PhanLoaiThamTraSauThongQuan = VAD1AG.C23.GetValue().ToString();
            TKMD.NgayPheDuyetBP = Convert.ToDateTime(VAD1AG.C24.GetValue());
            TKMD.NgayPheDuyetBP = HelperVNACCS.AddHoursToDateTime(TKMD.NgayPheDuyetBP, VAD1AG.AD6.GetValue().ToString());
            TKMD.NgayHoanThanhKiemTraBP = Convert.ToDateTime(VAD1AG.C25.GetValue());
            TKMD.NgayHoanThanhKiemTraBP = HelperVNACCS.AddHoursToDateTime(TKMD.NgayHoanThanhKiemTraBP, VAD1AG.AD7.GetValue().ToString());
            TKMD.SoNgayDoiCapPhepNhapKhau = Convert.ToDecimal(VAD1AG.C26.GetValue(), cltInfo);
            TKMD.TieuDe = VAD1AG.B23.GetValue().ToString();
            #region Ân hạn thuế
            List<KDT_VNACC_TK_PhanHoi_AnHan> lAnHan = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
            if (VAD1AG.KN1.listAttribute != null && VAD1AG.KN1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.KN1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.AnHangCollection == null) TKMD.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
                    KDT_VNACC_TK_PhanHoi_AnHan anhan = new KDT_VNACC_TK_PhanHoi_AnHan();
                    anhan.MaSacThueAnHan = VAD1AG.KN1.listAttribute[0].GetValueCollection(i).ToString();
                    anhan.TenSacThueAnHan = VAD1AG.KN1.listAttribute[1].GetValueCollection(i).ToString();
                    anhan.HanNopThueSauKhiAnHan = Convert.ToDateTime(VAD1AG.KN1.listAttribute[2].GetValueCollection(i));
                    if (string.IsNullOrEmpty(anhan.MaSacThueAnHan))
                    {
                        lAnHan.Add(anhan);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_AnHan>(TKMD.AnHangCollection, lAnHan);
            #endregion


            //             TKMD.MaSacThueAnHan_VAT = VAD1AG.ADX.GetValue().ToString();
            //             TKMD.TenSacThueAnHan_VAT = VAD1AG.ADY.GetValue().ToString();
            //             TKMD.HanNopThueSauKhiAnHan_VAT = Convert.ToDateTime(VAD1AG.ADZ.GetValue());
            TKMD.NgayKhoiHanhVC = Convert.ToDateTime(VAD1AG.DPD.GetValue());
            #region Trung chuyen
            List<KDT_VNACC_TK_TrungChuyen> ltrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
            if (VAD1AG.ST_.listAttribute != null && VAD1AG.ST_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG.ST_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TrungChuyenCollection == null) TKMD.TrungChuyenCollection = new List<KDT_VNACC_TK_TrungChuyen>();
                    KDT_VNACC_TK_TrungChuyen trungchuyen = new KDT_VNACC_TK_TrungChuyen();
                    trungchuyen.MaDiaDiem = VAD1AG.ST_.listAttribute[0].GetValueCollection(i).ToString();
                    trungchuyen.NgayDen = System.Convert.ToDateTime(VAD1AG.ST_.listAttribute[1].GetValueCollection(i));
                    trungchuyen.NgayKhoiHanh = System.Convert.ToDateTime(VAD1AG.ST_.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(trungchuyen.MaDiaDiem))
                    {
                        ltrungChuyen.Add(trungchuyen);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_TrungChuyen>(TKMD.TrungChuyenCollection, ltrungChuyen);
            #endregion

            TKMD.DiaDiemDichVC = VAD1AG.ARP.GetValue().ToString();
            TKMD.NgayDen = Convert.ToDateTime(VAD1AG.ADT.GetValue());

            if (TKMD.HangCollection != null)
            {
                if (TKMD.HangCollection.Count != VAD1AG.HangMD.Count)
                {
                    KDT_VNACC_HangMauDich.DeleteCollection(TKMD.HangCollection);
                    TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                }
            }
            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {
                TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                for (int i = 0; i < VAD1AG.HangMD.Count; i++)
                {
                    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        hmd.tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        hmd.tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    hmd = GetHMDFromVAD1AG_Hang(VAD1AG.HangMD[i], hmd);
                    if (hmd != null)
                        TKMD.HangCollection.Add(hmd);
                }
            }
            else
            for (int i = 0; i < VAD1AG.HangMD.Count; i++)
            {
                if (i < TKMD.HangCollection.Count) 
                {
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        TKMD.HangCollection[i].tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        TKMD.HangCollection[i].tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    TKMD.HangCollection[i] = GetHMDFromVAD1AG_Hang(VAD1AG.HangMD[i], TKMD.HangCollection[i]);
                }
                    
            }
            //foreach (VAD1AG0_HANG item in VAD1AG.HangMD)
            //{
            //    KDT_VNACC_HangMauDich HMD = GetHMDFromVAD1AG_Hang(item);
            //    if (HMD != null)
            //        TKMD.HangCollection.Add(HMD);
            //}


            return TKMD;
        }

        public static KDT_VNACC_HangMauDich GetHMDFromVAD1AG_Hang(VAD1AG_HANG VAD1AG_Hang, KDT_VNACC_HangMauDich HMD)
        {
            //KDT_VNACC_HangMauDich HMD = new KDT_VNACC_HangMauDich();
            if (HMD == null) HMD = new KDT_VNACC_HangMauDich();
            if (string.IsNullOrEmpty(VAD1AG_Hang.CMD.GetValue().ToString()))
                return HMD;
            HMD.SoDong = VAD1AG_Hang.B25.GetValue().ToString();
            HMD.MaSoHang = VAD1AG_Hang.CMD.GetValue().ToString();
            HMD.MaQuanLy = VAD1AG_Hang.GZC.GetValue().ToString();
            HMD.MaPhanLoaiTaiXacNhanGia = VAD1AG_Hang.B29.GetValue().ToString();
            HMD.MaHangHoaKhaiBao = VAD1AG_Hang.CMN.GetValue().ToString();
            HMD.SoLuong1 = Convert.ToDecimal(VAD1AG_Hang.QN1.GetValue(), cltInfo);
            HMD.DVTLuong1 = VAD1AG_Hang.QT1.GetValue().ToString();
            HMD.SoMucKhaiKhoanDC = string.Empty;
            if (VAD1AG_Hang.VN_.listAttribute != null && VAD1AG_Hang.VN_.listAttribute[0].ListValue != null)
            {
                string SoMucKhai = string.Empty;
                for (int i = 0; i < VAD1AG_Hang.VN_.listAttribute[0].ListValue.Count; i++)
                {
                    if (!string.IsNullOrEmpty(VAD1AG_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        SoMucKhai += VAD1AG_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString();
                    }
                }
                HMD.SoMucKhaiKhoanDC = SoMucKhai;
            }
            HMD.SoLuong2 = Convert.ToDecimal(VAD1AG_Hang.QN2.GetValue(), cltInfo);
            HMD.DVTLuong2 = VAD1AG_Hang.QT2.GetValue().ToString();
            HMD.TriGiaHoaDon = Convert.ToDecimal(VAD1AG_Hang.BPR.GetValue(), cltInfo);
            HMD.DonGiaHoaDon = Convert.ToDecimal(VAD1AG_Hang.UPR.GetValue(), cltInfo);
            HMD.MaTTDonGia = VAD1AG_Hang.UPC.GetValue().ToString();
            HMD.DVTDonGia = VAD1AG_Hang.TSC.GetValue().ToString();
            HMD.TriGiaTinhThueS = Convert.ToDecimal(VAD1AG_Hang.B36.GetValue(), cltInfo);
            HMD.MaTTTriGiaTinhThue = VAD1AG_Hang.B50.GetValue().ToString();
            HMD.TriGiaTinhThue = Convert.ToDecimal(VAD1AG_Hang.B51.GetValue(), cltInfo);
            HMD.SoLuongTinhThue = Convert.ToDecimal(VAD1AG_Hang.B37.GetValue(), cltInfo);
            HMD.MaDVTDanhThue = VAD1AG_Hang.B38.GetValue().ToString();
            HMD.DonGiaTinhThue = Convert.ToDecimal(VAD1AG_Hang.KKT.GetValue(), cltInfo);
            HMD.DV_SL_TrongDonGiaTinhThue = VAD1AG_Hang.KKS.GetValue().ToString();
            //HMD.MaPhanLoaiThueSuatThue = VAD1AG_Hang.B42.GetValue().ToString();
            HMD.ThueSuatThue = VAD1AG_Hang.B43.GetValue().ToString().Replace("%","");
           // HMD.ThueSuat = HMD.ThueSuatThue.Contains('%') ? System.Convert.ToDecimal(HMD.ThueSuatThue.Substring(0, HMD.ThueSuatThue.Length - 1).Trim(), cltInfo) : HMD.ThueSuat;
            HMD.PhanLoaiThueSuatThue = VAD1AG_Hang.SKB.GetValue().ToString();
            HMD.MaThueNKTheoLuong = VAD1AG_Hang.SPD.GetValue().ToString();
            HMD.SoTienThue = Convert.ToDecimal(VAD1AG_Hang.B47.GetValue(), cltInfo);
            HMD.NuocXuatXu = VAD1AG_Hang.OR.GetValue().ToString();
            HMD.TenNoiXuatXu = VAD1AG_Hang.B56.GetValue().ToString();
            HMD.MaBieuThueNK = VAD1AG_Hang.ORS.GetValue().ToString();
            HMD.SoTienGiamThue = Convert.ToDecimal(VAD1AG_Hang.B49.GetValue(), cltInfo);
            HMD.MaHanNgach = VAD1AG_Hang.KWS.GetValue().ToString();
            HMD.SoTTDongHangTKTNTX = VAD1AG_Hang.TDL.GetValue().ToString();
            HMD.SoDMMienThue = VAD1AG_Hang.TXN.GetValue().ToString();
            HMD.SoDongDMMienThue = VAD1AG_Hang.TXR.GetValue().ToString();
            HMD.MaMienGiam = VAD1AG_Hang.RE.GetValue().ToString();
            HMD.DieuKhoanMienGiam = VAD1AG_Hang.B59.GetValue().ToString();
            List<KDT_VNACC_HangMauDich_ThueThuKhac> lThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
            if (VAD1AG_Hang.KQ1.listAttribute != null && VAD1AG_Hang.KQ1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG_Hang.KQ1.listAttribute[0].ListValue.Count; i++)
                {
                    if (HMD.ThueThuKhacCollection == null)
                    {
                        HMD.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                    }
                    KDT_VNACC_HangMauDich_ThueThuKhac thukhac = new KDT_VNACC_HangMauDich_ThueThuKhac();
                    thukhac.TenKhoanMucThueVaThuKhac = VAD1AG_Hang.KQ1.listAttribute[0].GetValueCollection(i).ToString();
                    thukhac.MaTSThueThuKhac = VAD1AG_Hang.KQ1.listAttribute[1].GetValueCollection(i).ToString();
                    thukhac.TriGiaTinhThueVaThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.KQ1.listAttribute[2].GetValueCollection(i), cltInfo);
                    thukhac.SoLuongTinhThueVaThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.KQ1.listAttribute[3].GetValueCollection(i), cltInfo);
                    thukhac.MaDVTDanhThueVaThuKhac = VAD1AG_Hang.KQ1.listAttribute[4].GetValueCollection(i).ToString();
                    thukhac.ThueSuatThueVaThuKhac = VAD1AG_Hang.KQ1.listAttribute[5].GetValueCollection(i).ToString();
                    thukhac.SoTienThueVaThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.KQ1.listAttribute[6].GetValueCollection(i), cltInfo);
                    thukhac.MaMGThueThuKhac = VAD1AG_Hang.KQ1.listAttribute[7].GetValueCollection(i).ToString();
                    thukhac.DieuKhoanMienGiamThueVaThuKhac = VAD1AG_Hang.KQ1.listAttribute[8].GetValueCollection(i).ToString();
                    thukhac.SoTienGiamThueThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.KQ1.listAttribute[9].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(thukhac.MaTSThueThuKhac))
                    {
                        lThuKhac.Add(thukhac);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_HangMauDich_ThueThuKhac>(HMD.ThueThuKhacCollection, lThuKhac);
            if (HMD.MaSoHang != null && !string.IsNullOrEmpty(HMD.MaSoHang))
                return HMD;
            else
                return null;

        }


        public static KDT_VNACC_ToKhaiMauDich GetTKMDFromVAD1AC0(VAD1AC0 VAD1AC0, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            //KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();

            TKMD.SoToKhai = Convert.ToDecimal(VAD1AC0.ICN.GetValue(), cltInfo);
            TKMD.PhanLoaiBaoCaoSuaDoi = VAD1AC0.JKN.GetValue().ToString();
            TKMD.SoToKhaiDauTien = VAD1AC0.FIC.GetValue().ToString();
            TKMD.SoNhanhToKhai = Convert.ToDecimal(VAD1AC0.BNO.GetValue(), cltInfo);
            TKMD.TongSoTKChiaNho = Convert.ToDecimal(VAD1AC0.DNO.GetValue(), cltInfo);
            TKMD.SoToKhaiTNTX = Convert.ToDecimal(VAD1AC0.TDN.GetValue(), cltInfo);
            TKMD.MaPhanLoaiKiemTra = VAD1AC0.A06.GetValue().ToString();
            TKMD.MaLoaiHinh = VAD1AC0.ICB.GetValue().ToString();
            TKMD.MaPhanLoaiHH = VAD1AC0.CCC.GetValue().ToString();
            TKMD.MaPhuongThucVT = VAD1AC0.MTC.GetValue().ToString();
            TKMD.PhanLoaiToChuc = VAD1AC0.SKB.GetValue().ToString();
            TKMD.MaSoThueDaiDien = VAD1AC0.A00.GetValue().ToString();
            TKMD.TenCoQuanHaiQuan = VAD1AC0.A07.GetValue().ToString();
            TKMD.NhomXuLyHS = VAD1AC0.CHB.GetValue().ToString();
            TKMD.NgayDangKy = Convert.ToDateTime(VAD1AC0.A09.GetValue());
            TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAD1AC0.AD1.GetValue().ToString());
            TKMD.NgayThayDoiDangKy = Convert.ToDateTime(VAD1AC0.AD2.GetValue());
            TKMD.NgayThayDoiDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayThayDoiDangKy, VAD1AC0.AD3.GetValue().ToString());
            TKMD.ThoiHanTaiNhapTaiXuat = Convert.ToDateTime(VAD1AC0.RED.GetValue());
            TKMD.BieuThiTruongHopHetHan = VAD1AC0.AAA.GetValue().ToString();
            TKMD.MaDonVi = VAD1AC0.IMC.GetValue().ToString();
            TKMD.TenDonVi = VAD1AC0.IMN.GetValue().ToString();
            TKMD.MaBuuChinhDonVi = VAD1AC0.IMY.GetValue().ToString();
            TKMD.DiaChiDonVi = VAD1AC0.IMA.GetValue().ToString();
            TKMD.SoDienThoaiDonVi = VAD1AC0.IMT.GetValue().ToString();
            TKMD.MaUyThac = VAD1AC0.NMC.GetValue().ToString();
            TKMD.TenUyThac = VAD1AC0.NMN.GetValue().ToString();
            TKMD.MaDoiTac = VAD1AC0.EPC.GetValue().ToString();
            TKMD.TenDoiTac = VAD1AC0.EPN.GetValue().ToString();
            TKMD.MaBuuChinhDoiTac = VAD1AC0.EPY.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAD1AC0.EPA.GetValue().ToString();
            TKMD.DiaChiDoiTac2 = VAD1AC0.EP2.GetValue().ToString();
            TKMD.DiaChiDoiTac3 = VAD1AC0.EP3.GetValue().ToString();
            TKMD.DiaChiDoiTac4 = VAD1AC0.EP4.GetValue().ToString();
            TKMD.MaNuocDoiTac = VAD1AC0.EPO.GetValue().ToString();
            TKMD.NguoiUyThacXK = VAD1AC0.ENM.GetValue().ToString();
            TKMD.MaDaiLyHQ = VAD1AC0.A37.GetValue().ToString();
            TKMD.TenDaiLyHaiQuan = VAD1AC0.A38.GetValue().ToString();
            TKMD.MaNhanVienHaiQuan = VAD1AC0.A39.GetValue().ToString();
            #region Vận đơn
            if (TKMD.VanDonCollection == null) TKMD.VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
            List<KDT_VNACC_TK_SoVanDon> ListVanDon = new List<KDT_VNACC_TK_SoVanDon>();
            if (VAD1AC0.BL_.listAttribute != null && VAD1AC0.BL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.BL_.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TK_SoVanDon SoVanDon = new KDT_VNACC_TK_SoVanDon();
                    SoVanDon.SoTT = i + 1;
                    SoVanDon.SoVanDon = VAD1AC0.BL_.listAttribute[0].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(SoVanDon.SoVanDon.Trim()))
                        ListVanDon.Add(SoVanDon);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_SoVanDon>(TKMD.VanDonCollection, ListVanDon);
            #endregion
            TKMD.SoLuong = Convert.ToDecimal(VAD1AC0.NO.GetValue(), cltInfo);
            TKMD.MaDVTSoLuong = VAD1AC0.NOT.GetValue().ToString();
            TKMD.TrongLuong = Convert.ToDecimal(VAD1AC0.GW.GetValue(), cltInfo);
            TKMD.MaDVTTrongLuong = VAD1AC0.GWT.GetValue().ToString();
            TKMD.SoLuongCont = Convert.ToDecimal(VAD1AC0.COC.GetValue(), cltInfo);
            TKMD.MaDDLuuKho = VAD1AC0.ST.GetValue().ToString();
            TKMD.TenDDLuuKho = VAD1AC0.A51.GetValue().ToString();
            TKMD.MaDiaDiemDoHang = VAD1AC0.DST.GetValue().ToString();
            TKMD.TenDiaDiemDohang = VAD1AC0.DSN.GetValue().ToString();
            TKMD.MaDiaDiemXepHang = VAD1AC0.PSC.GetValue().ToString();
            TKMD.TenDiaDiemXepHang = VAD1AC0.PSN.GetValue().ToString();
            TKMD.MaPTVC = VAD1AC0.VSC.GetValue().ToString();
            TKMD.TenPTVC = VAD1AC0.VSN.GetValue().ToString();
            TKMD.NgayHangDen = Convert.ToDateTime(VAD1AC0.ARR.GetValue());
            TKMD.SoHieuKyHieu = VAD1AC0.MRK.GetValue().ToString();
            TKMD.NgayNhapKhoDau = Convert.ToDateTime(VAD1AC0.ISD.GetValue());
            #region Mã văn bản pháp quy khác

            if (VAD1AC0.OL_.listAttribute != null && VAD1AC0.OL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.OL_.listAttribute[0].ListValue.Count; i++)
                {
                    if (!string.IsNullOrEmpty(VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        switch (i)
                        {
                            case 0:
                                TKMD.MaVanbanPhapQuy1 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 1:
                                TKMD.MaVanbanPhapQuy2 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 2:
                                TKMD.MaVanbanPhapQuy3 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 3:
                                TKMD.MaVanbanPhapQuy4 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 4:
                                TKMD.MaVanbanPhapQuy5 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                        }
                    }
                }
            }
            #endregion

            TKMD.PhanLoaiHD = VAD1AC0.IV1.GetValue().ToString();
            TKMD.SoHoaDon = VAD1AC0.IV3.GetValue().ToString();
            TKMD.SoTiepNhanHD = Convert.ToDecimal(VAD1AC0.IV2.GetValue(), cltInfo);
            TKMD.NgayPhatHanhHD = Convert.ToDateTime(VAD1AC0.IVD.GetValue());
            TKMD.PhuongThucTT = VAD1AC0.IVP.GetValue().ToString();
            TKMD.PhanLoaiGiaHD = VAD1AC0.IP1.GetValue().ToString();
            TKMD.MaDieuKienGiaHD = VAD1AC0.IP2.GetValue().ToString();
            TKMD.MaTTHoaDon = VAD1AC0.IP3.GetValue().ToString();
            TKMD.TongTriGiaHD = Convert.ToDecimal(VAD1AC0.IP4.GetValue(), cltInfo);
            TKMD.TriGiaTinhThue = Convert.ToDecimal(VAD1AC0.A86.GetValue(), cltInfo);
            //TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(VAD1AC0.TP.GetValue(), cltInfo);
            TKMD.MaPhanLoaiTongGiaCoBan = VAD1AC0.A97.GetValue().ToString();
            TKMD.MaKetQuaKiemTra = VAD1AC0.N4.GetValue().ToString();
            #region Giấy phép
            List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
            if (VAD1AC0.SS_.listAttribute != null && VAD1AC0.SS_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.SS_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.GiayPhepCollection == null) TKMD.GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
                    KDT_VNACC_TK_GiayPhep giayPhep = new KDT_VNACC_TK_GiayPhep();
                    giayPhep.PhanLoai = VAD1AC0.SS_.listAttribute[0].GetValueCollection(i).ToString();
                    giayPhep.SoGiayPhep = VAD1AC0.SS_.listAttribute[1].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(giayPhep.SoGiayPhep))
                        listGiayPhep.Add(giayPhep);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_GiayPhep>(TKMD.GiayPhepCollection, listGiayPhep);
            #endregion

            TKMD.MaPhanLoaiTriGia = VAD1AC0.VD1.GetValue().ToString();
            TKMD.SoTiepNhanTKTriGia = string.IsNullOrEmpty(VAD1AC0.VD2.GetValue().ToString()) ? 0 : Convert.ToDecimal(VAD1AC0.VD2.GetValue(), cltInfo); //Convert.ToDecimal(VAD1AC0.VD2.GetValue(), cltInfo);
            TKMD.PhanLoaiCongThucChuan = VAD1AC0.A93.GetValue().ToString();
            TKMD.MaPhanLoaiDieuChinhTriGia = VAD1AC0.A94.GetValue().ToString();
            TKMD.PhuongPhapDieuChinhTriGia = VAD1AC0.A95.GetValue().ToString();
            TKMD.MaTTHieuChinhTriGia = VAD1AC0.VCC.GetValue().ToString();
            TKMD.GiaHieuChinhTriGia = Convert.ToDecimal(VAD1AC0.VPC.GetValue(), cltInfo);
            TKMD.MaPhanLoaiPhiVC = VAD1AC0.FR1.GetValue().ToString();
            TKMD.MaTTPhiVC = VAD1AC0.FR2.GetValue().ToString();
            TKMD.PhiVanChuyen = Convert.ToDecimal(VAD1AC0.FR3.GetValue(), cltInfo);
            TKMD.MaPhanLoaiPhiBH = VAD1AC0.IN1.GetValue().ToString();
            TKMD.MaTTPhiBH = VAD1AC0.IN2.GetValue().ToString();
            TKMD.PhiBaoHiem = Convert.ToDecimal(VAD1AC0.IN3.GetValue(), cltInfo);
            TKMD.SoDangKyBH = VAD1AC0.IN4.GetValue().ToString();
            #region Khoản điều chỉnh
            List<KDT_VNACC_TK_KhoanDieuChinh> ListKDC = new List<KDT_VNACC_TK_KhoanDieuChinh>();
            if (VAD1AC0.VR_.listAttribute != null && VAD1AC0.VR_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.VR_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.KhoanDCCollection == null) TKMD.KhoanDCCollection = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    KDT_VNACC_TK_KhoanDieuChinh khoanDieuChinh = new KDT_VNACC_TK_KhoanDieuChinh();
                    khoanDieuChinh.MaTenDieuChinh = VAD1AC0.VR_.listAttribute[0].GetValueCollection(i).ToString();
                    khoanDieuChinh.MaPhanLoaiDieuChinh = VAD1AC0.VR_.listAttribute[1].GetValueCollection(i).ToString();
                    khoanDieuChinh.MaTTDieuChinhTriGia = VAD1AC0.VR_.listAttribute[2].GetValueCollection(i).ToString();
                    khoanDieuChinh.TriGiaKhoanDieuChinh = Convert.ToDecimal(VAD1AC0.VR_.listAttribute[3].GetValueCollection(i), cltInfo);
                    khoanDieuChinh.TongHeSoPhanBo = Convert.ToDecimal(VAD1AC0.VR_.listAttribute[4].GetValueCollection(i).ToString(), cltInfo);
                    khoanDieuChinh.SoTT = i + 1;
                    if (!string.IsNullOrEmpty(khoanDieuChinh.MaTenDieuChinh) || !string.IsNullOrEmpty(khoanDieuChinh.MaPhanLoaiDieuChinh))
                        ListKDC.Add(khoanDieuChinh);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_KhoanDieuChinh>(TKMD.KhoanDCCollection, ListKDC);
            #endregion

            TKMD.ChiTietKhaiTriGia = VAD1AC0.VLD.GetValue().ToString();
            #region Sắc thuế
            List<KDT_VNACC_TK_PhanHoi_SacThue> lSacThue = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
            if (VAD1AC0.KF1.listAttribute != null && VAD1AC0.KF1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.KF1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.SacThueCollection == null) TKMD.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                    KDT_VNACC_TK_PhanHoi_SacThue sacthue = new KDT_VNACC_TK_PhanHoi_SacThue();
                    sacthue.MaSacThue = VAD1AC0.KF1.listAttribute[0].GetValueCollection(i).ToString();
                    sacthue.TenSacThue = VAD1AC0.KF1.listAttribute[1].GetValueCollection(i).ToString();
                    sacthue.TongTienThue = System.Convert.ToDecimal(VAD1AC0.KF1.listAttribute[2].GetValueCollection(i), cltInfo);
                    sacthue.SoDongTongTienThue = System.Convert.ToDecimal(VAD1AC0.KF1.listAttribute[3].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(sacthue.MaSacThue))
                        lSacThue.Add(sacthue);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_SacThue>(TKMD.SacThueCollection, lSacThue);
            #endregion
            TKMD.TongTienThuePhaiNop = Convert.ToDecimal(VAD1AC0.B02.GetValue(), cltInfo);
            TKMD.SoTienBaoLanh = Convert.ToDecimal(VAD1AC0.B03.GetValue(), cltInfo);
            #region Tỷ giá
            List<KDT_VNACC_TK_PhanHoi_TyGia> lTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
            if (VAD1AC0.KJ1.listAttribute != null && VAD1AC0.KJ1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.KJ1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TyGiaCollection == null) TKMD.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    KDT_VNACC_TK_PhanHoi_TyGia tygia = new KDT_VNACC_TK_PhanHoi_TyGia();
                    tygia.MaTTTyGiaTinhThue = VAD1AC0.KJ1.listAttribute[0].GetValueCollection(i).ToString();
                    tygia.TyGiaTinhThue = Convert.ToDecimal(VAD1AC0.KJ1.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(tygia.MaTTTyGiaTinhThue))
                    {
                        lTyGia.Add(tygia);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_TyGia>(TKMD.TyGiaCollection, lTyGia);
            #endregion

            TKMD.MaXDThoiHanNopThue = VAD1AC0.ENC.GetValue().ToString();
            TKMD.NguoiNopThue = VAD1AC0.TPM.GetValue().ToString();
            TKMD.MaLyDoDeNghiBP = VAD1AC0.BP.GetValue().ToString();
            TKMD.PhanLoaiNopThue = VAD1AC0.B08.GetValue().ToString();
            TKMD.TongSoTrangCuaToKhai = Convert.ToDecimal(VAD1AC0.B12.GetValue(), cltInfo);
            TKMD.TongSoDongHangCuaToKhai = Convert.ToDecimal(VAD1AC0.B13.GetValue(), cltInfo);
            #region Phân loại đính kèm khai báo điện tử
            List<KDT_VNACC_TK_DinhKemDienTu> lDinhKem = new List<KDT_VNACC_TK_DinhKemDienTu>();
            if (VAD1AC0.EA_.listAttribute != null && VAD1AC0.EA_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.EA_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.DinhKemCollection == null) TKMD.DinhKemCollection = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    KDT_VNACC_TK_DinhKemDienTu dinhkem = new KDT_VNACC_TK_DinhKemDienTu();
                    dinhkem.PhanLoai = VAD1AC0.EA_.listAttribute[0].GetValueCollection(i).ToString();
                    dinhkem.SoDinhKemKhaiBaoDT = System.Convert.ToDecimal(VAD1AC0.EA_.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(dinhkem.PhanLoai) || dinhkem.SoDinhKemKhaiBaoDT > 0)
                    {
                        lDinhKem.Add(dinhkem);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_DinhKemDienTu>(TKMD.DinhKemCollection, lDinhKem);
            #endregion

            TKMD.GhiChu = VAD1AC0.NT2.GetValue().ToString();
            TKMD.SoQuanLyNoiBoDN = VAD1AC0.REF.GetValue().ToString();
            TKMD.SoQuanLyNguoiSuDung = VAD1AC0.B16.GetValue().ToString();
            //TKMD.PhanLoaiChi = VAD1AC0.CCM.GetValue().ToString();
            #region Chỉ thị HQ
            List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
            if (VAD1AC0.D__.listAttribute != null && VAD1AC0.D__.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.D__.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ChiThiHQCollection == null) TKMD.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                    ChiThiHQ.PhanLoai = VAD1AC0.CCM.GetValue().ToString();
                    ChiThiHQ.Ngay = Convert.ToDateTime(VAD1AC0.D__.listAttribute[0].GetValueCollection(i));
                    ChiThiHQ.Ten = VAD1AC0.D__.listAttribute[1].GetValueCollection(i).ToString();
                    ChiThiHQ.NoiDung = VAD1AC0.D__.listAttribute[2].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(ChiThiHQ.Ten) || !string.IsNullOrEmpty(ChiThiHQ.NoiDung))
                        lChiThi.Add(ChiThiHQ);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_ChiThiHaiQuan>(TKMD.ChiThiHQCollection, lChiThi);
            #endregion
            TKMD.NgayKhaiBaoNopThue = Convert.ToDateTime(VAD1AC0.ADY.GetValue());
            TKMD.NgayKhaiBaoNopThue = HelperVNACCS.AddHoursToDateTime(TKMD.NgayKhaiBaoNopThue, VAD1AC0.ADZ.GetValue().ToString());


            TKMD.TieuDe = VAD1AC0.B23.GetValue().ToString();
            #region Ân hạn thuế
            List<KDT_VNACC_TK_PhanHoi_AnHan> lAnHan = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
            if (VAD1AC0.KN1.listAttribute != null && VAD1AC0.KN1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.KN1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.AnHangCollection == null) TKMD.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
                    KDT_VNACC_TK_PhanHoi_AnHan anhan = new KDT_VNACC_TK_PhanHoi_AnHan();
                    anhan.MaSacThueAnHan = VAD1AC0.KN1.listAttribute[0].GetValueCollection(i).ToString();
                    anhan.TenSacThueAnHan = VAD1AC0.KN1.listAttribute[1].GetValueCollection(i).ToString();
                    anhan.HanNopThueSauKhiAnHan = Convert.ToDateTime(VAD1AC0.KN1.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(anhan.MaSacThueAnHan))
                    {
                        lAnHan.Add(anhan);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_AnHan>(TKMD.AnHangCollection, lAnHan);
            #endregion


            //TKMD.TenSacThueAnHan_VAT = VAD1AC0.ADY.GetValue().ToString();
            //TKMD.HanNopThueSauKhiAnHan_VAT = Convert.ToDateTime(VAD1AC0.ADZ.GetValue());
            TKMD.NgayKhoiHanhVC = Convert.ToDateTime(VAD1AC0.DPD.GetValue());
            #region Trung chuyen
            List<KDT_VNACC_TK_TrungChuyen> ltrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
            if (VAD1AC0.ST_.listAttribute != null && VAD1AC0.ST_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.ST_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TrungChuyenCollection == null) TKMD.TrungChuyenCollection = new List<KDT_VNACC_TK_TrungChuyen>();
                    KDT_VNACC_TK_TrungChuyen trungchuyen = new KDT_VNACC_TK_TrungChuyen();
                    trungchuyen.MaDiaDiem = VAD1AC0.ST_.listAttribute[0].GetValueCollection(i).ToString();
                    trungchuyen.NgayDen = System.Convert.ToDateTime(VAD1AC0.ST_.listAttribute[1].GetValueCollection(i));
                    trungchuyen.NgayKhoiHanh = System.Convert.ToDateTime(VAD1AC0.ST_.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(trungchuyen.MaDiaDiem))
                    {
                        ltrungChuyen.Add(trungchuyen);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_TrungChuyen>(TKMD.TrungChuyenCollection, ltrungChuyen);
            #endregion

            TKMD.DiaDiemDichVC = VAD1AC0.ARP.GetValue().ToString();
            TKMD.NgayDen = Convert.ToDateTime(VAD1AC0.ADT.GetValue());

            if (TKMD.HangCollection != null)
            {
                if (TKMD.HangCollection.Count != VAD1AC0.HangMD.Count)
                {
                    KDT_VNACC_HangMauDich.DeleteCollection(TKMD.HangCollection);
                    TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                }
            }

            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {
                TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                for (int i = 0; i < VAD1AC0.HangMD.Count; i++)
                {
                    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        hmd.tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        hmd.tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    hmd = GetHMDFromVAD1AC0_Hang(VAD1AC0.HangMD[i], hmd);
                    if (hmd != null)
                        TKMD.HangCollection.Add(hmd);
                }
            }
            else
            {
                //if (TKMD.HangCollection == null) TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                for (int i = 0; i < VAD1AC0.HangMD.Count; i++)
                {
                    if (i < TKMD.HangCollection.Count)
                    {
                        //minhnd Check to khai nhập-xuất
                        if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                            TKMD.HangCollection[i].tkxuat = false;
                        else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                            TKMD.HangCollection[i].tkxuat = true;
                        //minhnd Check to khai nhập-xuất
                        TKMD.HangCollection[i] = GetHMDFromVAD1AC0_Hang(VAD1AC0.HangMD[i], TKMD.HangCollection[i]);
                    }
                }
            }
            //foreach (VAD1AC0_HANG item in VAD1AC0.HangMD)
            //{
            //    KDT_VNACC_HangMauDich HMD = GetHMDFromVAD1AC0_Hang(item);
            //    if (HMD != null)
            //        TKMD.HangCollection.Add(HMD);
            //}


            return TKMD;
        }

        public static KDT_VNACC_HangMauDich GetHMDFromVAD1AC0_Hang(VAD1AC0_HANG VAD1AG_Hang, KDT_VNACC_HangMauDich HMD)
        {
            //KDT_VNACC_HangMauDich HMD = new KDT_VNACC_HangMauDich();
            if (string.IsNullOrEmpty(VAD1AG_Hang.CMD.GetValue().ToString()))
                return HMD;
            HMD.SoDong = VAD1AG_Hang.B25.GetValue().ToString();
            HMD.MaSoHang = VAD1AG_Hang.CMD.GetValue().ToString();
            HMD.MaQuanLy = VAD1AG_Hang.GZC.GetValue().ToString();
            HMD.MaPhanLoaiTaiXacNhanGia = VAD1AG_Hang.B29.GetValue().ToString();

            HMD.MaHangHoaKhaiBao = VAD1AG_Hang.CMN.GetValue().ToString();

            HMD.SoLuong1 = Convert.ToDecimal(VAD1AG_Hang.QN1.GetValue(), cltInfo);
            HMD.DVTLuong1 = VAD1AG_Hang.QT1.GetValue().ToString();

            if (VAD1AG_Hang.VN_.listAttribute != null && VAD1AG_Hang.VN_.listAttribute[0].ListValue != null)
            {
                string SoMucKhai = string.Empty;
                for (int i = 0; i < VAD1AG_Hang.VN_.listAttribute[0].ListValue.Count; i++)
                {

                    if (!string.IsNullOrEmpty(VAD1AG_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        SoMucKhai += VAD1AG_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString();

                    }
                }
                HMD.SoMucKhaiKhoanDC = SoMucKhai;
            }
            HMD.SoLuong2 = Convert.ToDecimal(VAD1AG_Hang.QN2.GetValue(), cltInfo);
            HMD.DVTLuong2 = VAD1AG_Hang.QT2.GetValue().ToString();
            HMD.TriGiaHoaDon = Convert.ToDecimal(VAD1AG_Hang.BPR.GetValue(), cltInfo);
            HMD.DonGiaHoaDon = Convert.ToDecimal(VAD1AG_Hang.UPR.GetValue(), cltInfo);
            HMD.MaTTDonGia = VAD1AG_Hang.UPC.GetValue().ToString();
            HMD.DVTDonGia = VAD1AG_Hang.TSC.GetValue().ToString();
            HMD.TriGiaTinhThueS = Convert.ToDecimal(VAD1AG_Hang.B36.GetValue(), cltInfo);
            HMD.MaTTTriGiaTinhThue = VAD1AG_Hang.B50.GetValue().ToString();
            HMD.TriGiaTinhThue = Convert.ToDecimal(VAD1AG_Hang.B51.GetValue(), cltInfo);
            HMD.SoLuongTinhThue = Convert.ToDecimal(VAD1AG_Hang.B37.GetValue(), cltInfo);
            HMD.MaDVTDanhThue = VAD1AG_Hang.B38.GetValue().ToString();
            HMD.DonGiaTinhThue = Convert.ToDecimal(VAD1AG_Hang.KKT.GetValue(), cltInfo);
            HMD.DV_SL_TrongDonGiaTinhThue = VAD1AG_Hang.KKS.GetValue().ToString();
            HMD.MaPhanLoaiThueSuatThue = VAD1AG_Hang.B42.GetValue().ToString();
            string thueSuat = VAD1AG_Hang.B43.GetValue().ToString().Trim();
            HMD.ThueSuatThue = thueSuat.Contains('%') ? thueSuat.Replace("%", "") : thueSuat;
            // HMD.ThueSuat = HMD.ThueSuatThue.Contains('%') ? System.Convert.ToDecimal(HMD.ThueSuatThue.Substring(0, HMD.ThueSuatThue.Length - 1).Trim(), cltInfo) : HMD.ThueSuat;
            HMD.PhanLoaiThueSuatThue = VAD1AG_Hang.SKB.GetValue().ToString();
            HMD.MaThueNKTheoLuong = VAD1AG_Hang.SPD.GetValue().ToString();
            HMD.SoTienThue = Convert.ToDecimal(VAD1AG_Hang.B47.GetValue(), cltInfo);

            HMD.NuocXuatXu = VAD1AG_Hang.OR.GetValue().ToString();

            HMD.TenNoiXuatXu = VAD1AG_Hang.B56.GetValue().ToString();
            HMD.MaBieuThueNK = VAD1AG_Hang.ORS.GetValue().ToString();
            HMD.SoTienGiamThue = Convert.ToDecimal(VAD1AG_Hang.B49.GetValue(), cltInfo);
            HMD.MaHanNgach = VAD1AG_Hang.KWS.GetValue().ToString();
            HMD.SoTTDongHangTKTNTX = VAD1AG_Hang.TDL.GetValue().ToString();
            HMD.SoDMMienThue = VAD1AG_Hang.TXN.GetValue().ToString();
            HMD.SoDongDMMienThue = VAD1AG_Hang.TXR.GetValue().ToString();
            HMD.MaMienGiam = VAD1AG_Hang.RE.GetValue().ToString();
            HMD.DieuKhoanMienGiam = VAD1AG_Hang.B59.GetValue().ToString();
            List<KDT_VNACC_HangMauDich_ThueThuKhac> lThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
            if (VAD1AG_Hang.KQ1.listAttribute != null && VAD1AG_Hang.KQ1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG_Hang.KQ1.listAttribute[0].ListValue.Count; i++)
                {
                    if (HMD.ThueThuKhacCollection == null)
                    {
                        HMD.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                    }
                    KDT_VNACC_HangMauDich_ThueThuKhac thukhac = new KDT_VNACC_HangMauDich_ThueThuKhac();
                    thukhac.TenKhoanMucThueVaThuKhac = VAD1AG_Hang.KQ1.listAttribute[0].GetValueCollection(i).ToString();
                    thukhac.MaTSThueThuKhac = VAD1AG_Hang.KQ1.listAttribute[1].GetValueCollection(i).ToString();
                    thukhac.TriGiaTinhThueVaThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.KQ1.listAttribute[2].GetValueCollection(i), cltInfo);
                    thukhac.SoLuongTinhThueVaThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.KQ1.listAttribute[3].GetValueCollection(i), cltInfo);
                    thukhac.MaDVTDanhThueVaThuKhac = VAD1AG_Hang.KQ1.listAttribute[4].GetValueCollection(i).ToString();
                    thukhac.ThueSuatThueVaThuKhac = VAD1AG_Hang.KQ1.listAttribute[5].GetValueCollection(i).ToString();
                    thukhac.SoTienThueVaThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.KQ1.listAttribute[6].GetValueCollection(i), cltInfo);
                    thukhac.MaMGThueThuKhac = VAD1AG_Hang.KQ1.listAttribute[7].GetValueCollection(i).ToString();
                    thukhac.DieuKhoanMienGiamThueVaThuKhac = VAD1AG_Hang.KQ1.listAttribute[8].GetValueCollection(i).ToString();
                    thukhac.SoTienGiamThueThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.KQ1.listAttribute[9].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(thukhac.MaTSThueThuKhac))
                    {
                        lThuKhac.Add(thukhac);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_HangMauDich_ThueThuKhac>(HMD.ThueThuKhacCollection, lThuKhac);
            if (HMD.MaSoHang != null && !string.IsNullOrEmpty(HMD.MaSoHang))
                return HMD;
            else
                return null;
        }

        public static KDT_VNACC_ToKhaiMauDich GetTKMDFromVAD4190(VAD4190 VAD1AC0, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            //KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();

            TKMD.SoToKhai = Convert.ToDecimal(VAD1AC0.ICN.GetValue(), cltInfo);
            TKMD.SoToKhaiDauTien = VAD1AC0.FIC.GetValue().ToString();
            TKMD.SoNhanhToKhai = Convert.ToDecimal(VAD1AC0.BNO.GetValue(), cltInfo);
            TKMD.TongSoTKChiaNho = Convert.ToDecimal(VAD1AC0.DNO.GetValue(), cltInfo);
            TKMD.SoToKhaiTNTX = Convert.ToDecimal(VAD1AC0.TDN.GetValue(), cltInfo);
            TKMD.MaLoaiHinh = VAD1AC0.ICB.GetValue().ToString();
            TKMD.MaPhanLoaiHH = VAD1AC0.CCC.GetValue().ToString();
            TKMD.MaPhuongThucVT = VAD1AC0.MTC.GetValue().ToString();
            TKMD.PhanLoaiToChuc = VAD1AC0.SKB.GetValue().ToString();
            TKMD.NhomXuLyHS = VAD1AC0.CHB.GetValue().ToString();
            TKMD.ThoiHanTaiNhapTaiXuat = Convert.ToDateTime(VAD1AC0.RED.GetValue());
            TKMD.MaDonVi = VAD1AC0.IMC.GetValue().ToString();
            TKMD.TenDonVi = VAD1AC0.IMN.GetValue().ToString();
            TKMD.MaBuuChinhDonVi = VAD1AC0.IMY.GetValue().ToString();
            TKMD.DiaChiDonVi = VAD1AC0.IMA.GetValue().ToString();
            TKMD.SoDienThoaiDonVi = VAD1AC0.IMT.GetValue().ToString();
            TKMD.MaUyThac = VAD1AC0.NMC.GetValue().ToString();
            TKMD.TenUyThac = VAD1AC0.NMN.GetValue().ToString();
            TKMD.MaDoiTac = VAD1AC0.EPC.GetValue().ToString();
            TKMD.TenDoiTac = VAD1AC0.EPN.GetValue().ToString();
            TKMD.MaBuuChinhDoiTac = VAD1AC0.EPY.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAD1AC0.EPA.GetValue().ToString();
            TKMD.DiaChiDoiTac2 = VAD1AC0.EP2.GetValue().ToString();
            TKMD.DiaChiDoiTac3 = VAD1AC0.EP3.GetValue().ToString();
            TKMD.DiaChiDoiTac4 = VAD1AC0.EP4.GetValue().ToString();
            TKMD.MaNuocDoiTac = VAD1AC0.EPO.GetValue().ToString();
            TKMD.NguoiUyThacXK = VAD1AC0.ENM.GetValue().ToString();
            #region Vận đơn
            if (TKMD.VanDonCollection == null) TKMD.VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
            List<KDT_VNACC_TK_SoVanDon> ListVanDon = new List<KDT_VNACC_TK_SoVanDon>();
            if (VAD1AC0.BL_.listAttribute != null && VAD1AC0.BL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.BL_.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TK_SoVanDon SoVanDon = new KDT_VNACC_TK_SoVanDon();
                    SoVanDon.SoTT = i + 1;
                    SoVanDon.SoVanDon = VAD1AC0.BL_.listAttribute[0].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(SoVanDon.SoVanDon.Trim()))
                        ListVanDon.Add(SoVanDon);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_SoVanDon>(TKMD.VanDonCollection, ListVanDon);
            #endregion
            TKMD.SoLuong = Convert.ToDecimal(VAD1AC0.NO.GetValue(), cltInfo);
            TKMD.MaDVTSoLuong = VAD1AC0.NOT.GetValue().ToString();
            TKMD.TrongLuong = Convert.ToDecimal(VAD1AC0.GW.GetValue(), cltInfo);
            TKMD.MaDVTTrongLuong = VAD1AC0.GWT.GetValue().ToString();
            TKMD.SoLuongCont = Convert.ToDecimal(VAD1AC0.COC.GetValue(), cltInfo);
            TKMD.MaDDLuuKho = VAD1AC0.ST.GetValue().ToString();
            TKMD.MaDiaDiemDoHang = VAD1AC0.DST.GetValue().ToString();
            TKMD.TenDiaDiemDohang = VAD1AC0.DSN.GetValue().ToString();
            TKMD.MaDiaDiemXepHang = VAD1AC0.PSC.GetValue().ToString();
            TKMD.TenDiaDiemXepHang = VAD1AC0.PSN.GetValue().ToString();
            TKMD.MaPTVC = VAD1AC0.VSC.GetValue().ToString();
            TKMD.TenPTVC = VAD1AC0.VSN.GetValue().ToString();
            TKMD.NgayHangDen = Convert.ToDateTime(VAD1AC0.ARR.GetValue());
            TKMD.SoHieuKyHieu = VAD1AC0.MRK.GetValue().ToString();
            TKMD.NgayNhapKhoDau = Convert.ToDateTime(VAD1AC0.ISD.GetValue());
            #region Mã văn bản pháp quy khác

            if (VAD1AC0.OL_.listAttribute != null && VAD1AC0.OL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.OL_.listAttribute[0].ListValue.Count; i++)
                {
                    if (!string.IsNullOrEmpty(VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        switch (i)
                        {
                            case 0:
                                TKMD.MaVanbanPhapQuy1 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 1:
                                TKMD.MaVanbanPhapQuy2 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 2:
                                TKMD.MaVanbanPhapQuy3 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 3:
                                TKMD.MaVanbanPhapQuy4 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 4:
                                TKMD.MaVanbanPhapQuy5 = VAD1AC0.OL_.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                        }
                    }
                }
            }
            #endregion

            TKMD.PhanLoaiHD = VAD1AC0.IV1.GetValue().ToString();
            TKMD.SoHoaDon = VAD1AC0.IV3.GetValue().ToString();
            TKMD.SoTiepNhanHD = Convert.ToDecimal(VAD1AC0.IV2.GetValue(), cltInfo);
            TKMD.NgayPhatHanhHD = Convert.ToDateTime(VAD1AC0.IVD.GetValue());
            TKMD.PhuongThucTT = VAD1AC0.IVP.GetValue().ToString();
            TKMD.PhanLoaiGiaHD = VAD1AC0.IP1.GetValue().ToString();
            TKMD.MaDieuKienGiaHD = VAD1AC0.IP2.GetValue().ToString();
            TKMD.MaTTHoaDon = VAD1AC0.IP3.GetValue().ToString();
            TKMD.TongTriGiaHD = Convert.ToDecimal(VAD1AC0.IP4.GetValue(), cltInfo);
            //TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(VAD1AC0.TP.GetValue(), cltInfo);
            TKMD.MaKetQuaKiemTra = VAD1AC0.N4.GetValue().ToString();
            #region Giấy phép
            List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
            if (VAD1AC0.SS_.listAttribute != null && VAD1AC0.SS_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.SS_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.GiayPhepCollection == null) TKMD.GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
                    KDT_VNACC_TK_GiayPhep giayPhep = new KDT_VNACC_TK_GiayPhep();
                    giayPhep.PhanLoai = VAD1AC0.SS_.listAttribute[0].GetValueCollection(i).ToString();
                    giayPhep.SoGiayPhep = VAD1AC0.SS_.listAttribute[1].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(giayPhep.SoGiayPhep))
                        listGiayPhep.Add(giayPhep);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_GiayPhep>(TKMD.GiayPhepCollection, listGiayPhep);
            #endregion

            TKMD.MaPhanLoaiTriGia = VAD1AC0.VD1.GetValue().ToString();
            TKMD.SoTiepNhanTKTriGia = string.IsNullOrEmpty(VAD1AC0.VD2.GetValue().ToString()) ? 0 : Convert.ToDecimal(VAD1AC0.VD2.GetValue(), cltInfo);
            TKMD.MaTTHieuChinhTriGia = VAD1AC0.VCC.GetValue().ToString();
            TKMD.GiaHieuChinhTriGia = Convert.ToDecimal(VAD1AC0.VPC.GetValue(), cltInfo);
            TKMD.MaPhanLoaiPhiVC = VAD1AC0.FR1.GetValue().ToString();
            TKMD.MaTTPhiVC = VAD1AC0.FR2.GetValue().ToString();
            TKMD.PhiVanChuyen = Convert.ToDecimal(VAD1AC0.FR3.GetValue(), cltInfo);
            TKMD.MaPhanLoaiPhiBH = VAD1AC0.IN1.GetValue().ToString();
            TKMD.MaTTPhiBH = VAD1AC0.IN2.GetValue().ToString();
            TKMD.PhiBaoHiem = Convert.ToDecimal(VAD1AC0.IN3.GetValue(), cltInfo);
            TKMD.SoDangKyBH = VAD1AC0.IN4.GetValue().ToString();
            #region Khoản điều chỉnh
            List<KDT_VNACC_TK_KhoanDieuChinh> ListKDC = new List<KDT_VNACC_TK_KhoanDieuChinh>();
            if (VAD1AC0.VR_.listAttribute != null && VAD1AC0.VR_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.VR_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.KhoanDCCollection == null) TKMD.KhoanDCCollection = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    KDT_VNACC_TK_KhoanDieuChinh khoanDieuChinh = new KDT_VNACC_TK_KhoanDieuChinh();
                    khoanDieuChinh.MaTenDieuChinh = VAD1AC0.VR_.listAttribute[0].GetValueCollection(i).ToString();
                    khoanDieuChinh.MaPhanLoaiDieuChinh = VAD1AC0.VR_.listAttribute[1].GetValueCollection(i).ToString();
                    khoanDieuChinh.MaTTDieuChinhTriGia = VAD1AC0.VR_.listAttribute[2].GetValueCollection(i).ToString();
                    khoanDieuChinh.TriGiaKhoanDieuChinh = Convert.ToDecimal(VAD1AC0.VR_.listAttribute[3].GetValueCollection(i), cltInfo);
                    khoanDieuChinh.TongHeSoPhanBo = Convert.ToDecimal(VAD1AC0.VR_.listAttribute[4].GetValueCollection(i).ToString(), cltInfo);
                    khoanDieuChinh.SoTT = i + 1;
                    if (!string.IsNullOrEmpty(khoanDieuChinh.MaTenDieuChinh) || !string.IsNullOrEmpty(khoanDieuChinh.MaPhanLoaiDieuChinh))
                        ListKDC.Add(khoanDieuChinh);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_KhoanDieuChinh>(TKMD.KhoanDCCollection, ListKDC);
            #endregion

            TKMD.ChiTietKhaiTriGia = VAD1AC0.VLD.GetValue().ToString();



            TKMD.MaXDThoiHanNopThue = VAD1AC0.ENC.GetValue().ToString();
            TKMD.NguoiNopThue = VAD1AC0.TPM.GetValue().ToString();
            TKMD.MaLyDoDeNghiBP = VAD1AC0.BP.GetValue().ToString();
            #region Phân loại đính kèm khai báo điện tử
            List<KDT_VNACC_TK_DinhKemDienTu> lDinhKem = new List<KDT_VNACC_TK_DinhKemDienTu>();
            if (VAD1AC0.EA_.listAttribute != null && VAD1AC0.EA_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.EA_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.DinhKemCollection == null) TKMD.DinhKemCollection = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    KDT_VNACC_TK_DinhKemDienTu dinhkem = new KDT_VNACC_TK_DinhKemDienTu();
                    dinhkem.PhanLoai = VAD1AC0.EA_.listAttribute[0].GetValueCollection(i).ToString();
                    dinhkem.SoDinhKemKhaiBaoDT = System.Convert.ToDecimal(VAD1AC0.EA_.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(dinhkem.PhanLoai) || dinhkem.SoDinhKemKhaiBaoDT > 0)
                    {
                        lDinhKem.Add(dinhkem);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_DinhKemDienTu>(TKMD.DinhKemCollection, lDinhKem);
            #endregion

            TKMD.GhiChu = VAD1AC0.NT2.GetValue().ToString();
            TKMD.SoQuanLyNoiBoDN = VAD1AC0.REF.GetValue().ToString();
            //TKMD.PhanLoaiChi = VAD1AC0.CCM.GetValue().ToString();
            #region Chỉ thị HQ
            List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
            if (VAD1AC0.DTI.listAttribute != null && VAD1AC0.DTI.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.DTI.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ChiThiHQCollection == null) TKMD.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                    ChiThiHQ.PhanLoai = VAD1AC0.CCM.GetValue().ToString();
                    ChiThiHQ.Ngay = Convert.ToDateTime(VAD1AC0.DTI.listAttribute[0].GetValueCollection(i));
                    ChiThiHQ.Ten = VAD1AC0.DTI.listAttribute[1].GetValueCollection(i).ToString();
                    ChiThiHQ.NoiDung = VAD1AC0.DTI.listAttribute[2].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(ChiThiHQ.Ten) || !string.IsNullOrEmpty(ChiThiHQ.NoiDung))
                        lChiThi.Add(ChiThiHQ);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_ChiThiHaiQuan>(TKMD.ChiThiHQCollection, lChiThi);
            #endregion

            //TKMD.TenSacThueAnHan_VAT = VAD1AC0.ADY.GetValue().ToString();
            //TKMD.HanNopThueSauKhiAnHan_VAT = Convert.ToDateTime(VAD1AC0.ADZ.GetValue());
            TKMD.NgayKhoiHanhVC = Convert.ToDateTime(VAD1AC0.DPD.GetValue());
            #region Trung chuyen
            List<KDT_VNACC_TK_TrungChuyen> ltrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
            if (VAD1AC0.ST_.listAttribute != null && VAD1AC0.ST_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AC0.ST_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TrungChuyenCollection == null) TKMD.TrungChuyenCollection = new List<KDT_VNACC_TK_TrungChuyen>();
                    KDT_VNACC_TK_TrungChuyen trungchuyen = new KDT_VNACC_TK_TrungChuyen();
                    trungchuyen.MaDiaDiem = VAD1AC0.ST_.listAttribute[0].GetValueCollection(i).ToString();
                    trungchuyen.NgayDen = System.Convert.ToDateTime(VAD1AC0.ST_.listAttribute[1].GetValueCollection(i));
                    trungchuyen.NgayKhoiHanh = System.Convert.ToDateTime(VAD1AC0.ST_.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(trungchuyen.MaDiaDiem))
                    {
                        ltrungChuyen.Add(trungchuyen);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_TrungChuyen>(TKMD.TrungChuyenCollection, ltrungChuyen);
            #endregion

            TKMD.DiaDiemDichVC = VAD1AC0.ARP.GetValue().ToString();
            TKMD.NgayDen = Convert.ToDateTime(VAD1AC0.ADT.GetValue());
            if (TKMD.HangCollection != null)
            {
                if (TKMD.HangCollection.Count != VAD1AC0.HangMD.Count)
                {
                    KDT_VNACC_HangMauDich.DeleteCollection(TKMD.HangCollection);
                    TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                }
            }

            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {
                TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                for (int i = 0; i < VAD1AC0.HangMD.Count; i++)
                {
                    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        hmd.tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        hmd.tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    hmd = GetHMDFromVAD4190_Hang(VAD1AC0.HangMD[i], hmd);
                    if (hmd != null)
                        TKMD.HangCollection.Add(hmd);
                }
            }
            else
            {
                for (int i = 0; i < VAD1AC0.HangMD.Count; i++)
                {
                    if (i < TKMD.HangCollection.Count)
                    {
                        //minhnd Check to khai nhập-xuất
                        if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                            TKMD.HangCollection[i].tkxuat = false;
                        else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                            TKMD.HangCollection[i].tkxuat = true;
                        //minhnd Check to khai nhập-xuất
                        TKMD.HangCollection[i] = GetHMDFromVAD4190_Hang(VAD1AC0.HangMD[i], TKMD.HangCollection[i]);
                    }
                }
            }
            //foreach (VAD1AC0_HANG item in VAD1AC0.HangMD)
            //{
            //    KDT_VNACC_HangMauDich HMD = GetHMDFromVAD1AC0_Hang(item);
            //    if (HMD != null)
            //        TKMD.HangCollection.Add(HMD);
            //}


            return TKMD;
        }

        public static KDT_VNACC_HangMauDich GetHMDFromVAD4190_Hang(VAD4190_HANG VAD1AG_Hang, KDT_VNACC_HangMauDich HMD)
        {
            //KDT_VNACC_HangMauDich HMD = new KDT_VNACC_HangMauDich();
            if (string.IsNullOrEmpty(VAD1AG_Hang.CMD.GetValue().ToString()))
                return HMD;
            HMD.MaSoHang = VAD1AG_Hang.CMD.GetValue().ToString();
            HMD.MaQuanLy = VAD1AG_Hang.GZC.GetValue().ToString();
            HMD.MaHangHoaKhaiBao = VAD1AG_Hang.CMN.GetValue().ToString();
            HMD.SoLuong1 = Convert.ToDecimal(VAD1AG_Hang.QN1.GetValue(), cltInfo);
            HMD.DVTLuong1 = VAD1AG_Hang.QT1.GetValue().ToString();

            if (VAD1AG_Hang.VN_.listAttribute != null && VAD1AG_Hang.VN_.listAttribute[0].ListValue != null)
            {
                string SoMucKhai = string.Empty;
                for (int i = 0; i < VAD1AG_Hang.VN_.listAttribute[0].ListValue.Count; i++)
                {

                    if (!string.IsNullOrEmpty(VAD1AG_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        SoMucKhai += VAD1AG_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString();
                    }

                }
                HMD.SoMucKhaiKhoanDC = SoMucKhai;
            }
            HMD.SoLuong2 = Convert.ToDecimal(VAD1AG_Hang.QN2.GetValue(), cltInfo);
            HMD.DVTLuong2 = VAD1AG_Hang.QT2.GetValue().ToString();
            HMD.TriGiaHoaDon = Convert.ToDecimal(VAD1AG_Hang.BPR.GetValue(), cltInfo);
            HMD.DonGiaHoaDon = Convert.ToDecimal(VAD1AG_Hang.UPR.GetValue(), cltInfo);
            HMD.MaTTDonGia = VAD1AG_Hang.UPC.GetValue().ToString();
            HMD.DVTDonGia = VAD1AG_Hang.TSC.GetValue().ToString();
            //HMD.ThueSuat = HMD.ThueSuatThue.Contains('%') ? System.Convert.ToDecimal(HMD.ThueSuatThue.Substring(0, HMD.ThueSuatThue.Length - 1).Trim(), cltInfo) : HMD.ThueSuat;
            HMD.MaThueNKTheoLuong = VAD1AG_Hang.SPD.GetValue().ToString();
            HMD.NuocXuatXu = VAD1AG_Hang.OR.GetValue().ToString();
            HMD.MaBieuThueNK = VAD1AG_Hang.ORS.GetValue().ToString();
            HMD.MaHanNgach = VAD1AG_Hang.KWS.GetValue().ToString();
            HMD.SoTTDongHangTKTNTX = VAD1AG_Hang.TDL.GetValue().ToString();
            HMD.SoDMMienThue = VAD1AG_Hang.TXN.GetValue().ToString();
            HMD.SoDongDMMienThue = VAD1AG_Hang.TXR.GetValue().ToString();
            HMD.MaMienGiam = VAD1AG_Hang.RE.GetValue().ToString();
            List<KDT_VNACC_HangMauDich_ThueThuKhac> lThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
            if (VAD1AG_Hang.TX_.listAttribute != null && VAD1AG_Hang.TX_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD1AG_Hang.TX_.listAttribute[0].ListValue.Count; i++)
                {
                    if (HMD.ThueThuKhacCollection == null)
                    {
                        HMD.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                    }
                    KDT_VNACC_HangMauDich_ThueThuKhac thukhac = new KDT_VNACC_HangMauDich_ThueThuKhac();
                    thukhac.MaTSThueThuKhac = VAD1AG_Hang.TX_.listAttribute[0].GetValueCollection(i).ToString();
                    thukhac.MaMGThueThuKhac = VAD1AG_Hang.TX_.listAttribute[1].GetValueCollection(i).ToString();
                    thukhac.SoTienGiamThueThuKhac = System.Convert.ToDecimal(VAD1AG_Hang.TX_.listAttribute[2].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(thukhac.MaTSThueThuKhac))
                    {
                        lThuKhac.Add(thukhac);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_HangMauDich_ThueThuKhac>(HMD.ThueThuKhacCollection, lThuKhac);
            if (HMD.MaSoHang != null && !string.IsNullOrEmpty(HMD.MaSoHang))
                return HMD;
            else
                return null;
        }
        public static KDT_VNACC_ToKhaiMauDich GetTKMDFromVAD0AP0(VAD0AP0 VAD0AP0, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            //KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();

            TKMD.SoToKhai = Convert.ToDecimal(VAD0AP0.ICN.GetValue(), cltInfo);
            TKMD.SoToKhaiDauTien = VAD0AP0.FIC.GetValue().ToString();
            TKMD.SoNhanhToKhai = Convert.ToDecimal(VAD0AP0.BNO.GetValue(), cltInfo);
            TKMD.TongSoTKChiaNho = Convert.ToDecimal(VAD0AP0.DNO.GetValue(), cltInfo);
            TKMD.SoToKhaiTNTX = Convert.ToDecimal(VAD0AP0.TDN.GetValue(), cltInfo);
            TKMD.MaPhanLoaiKiemTra = VAD0AP0.A07.GetValue().ToString();
            TKMD.MaLoaiHinh = VAD0AP0.A03.GetValue().ToString();
            TKMD.MaPhanLoaiHH = VAD0AP0.ADF.GetValue().ToString();
            TKMD.MaPhuongThucVT = VAD0AP0.ADG.GetValue().ToString();
            TKMD.PhanLoaiToChuc = VAD0AP0.A06.GetValue().ToString();
            TKMD.MaSoThueDaiDien = VAD0AP0.A01.GetValue().ToString();
            TKMD.TenCoQuanHaiQuan = VAD0AP0.A08.GetValue().ToString();
            TKMD.NhomXuLyHS = VAD0AP0.A09.GetValue().ToString();
            TKMD.NgayDangKy = Convert.ToDateTime(VAD0AP0.A10.GetValue());
            TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAD0AP0.AD1.GetValue().ToString());
            TKMD.NgayThayDoiDangKy = Convert.ToDateTime(VAD0AP0.AD2.GetValue());
            TKMD.NgayThayDoiDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAD0AP0.AD3.GetValue().ToString());
            TKMD.ThoiHanTaiNhapTaiXuat = Convert.ToDateTime(VAD0AP0.RED.GetValue());
            TKMD.BieuThiTruongHopHetHan = VAD0AP0.AAA.GetValue().ToString();
            TKMD.MaDonVi = VAD0AP0.IMC.GetValue().ToString();
            TKMD.TenDonVi = VAD0AP0.A20.GetValue().ToString();
            TKMD.MaBuuChinhDonVi = VAD0AP0.A21.GetValue().ToString();
            TKMD.DiaChiDonVi = VAD0AP0.A22.GetValue().ToString();
            TKMD.SoDienThoaiDonVi = VAD0AP0.A26.GetValue().ToString();
            TKMD.MaUyThac = VAD0AP0.A27.GetValue().ToString();
            TKMD.TenUyThac = VAD0AP0.A28.GetValue().ToString();
            TKMD.MaDoiTac = VAD0AP0.A29.GetValue().ToString();
            TKMD.TenDoiTac = VAD0AP0.A30.GetValue().ToString();
            TKMD.MaBuuChinhDoiTac = VAD0AP0.A31.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAD0AP0.A32.GetValue().ToString();
            TKMD.DiaChiDoiTac2 = VAD0AP0.A33.GetValue().ToString();
            TKMD.DiaChiDoiTac3 = VAD0AP0.A34.GetValue().ToString();
            TKMD.DiaChiDoiTac4 = VAD0AP0.A35.GetValue().ToString();
            TKMD.MaNuocDoiTac = VAD0AP0.A36.GetValue().ToString();
            TKMD.NguoiUyThacXK = VAD0AP0.A37.GetValue().ToString();
            TKMD.MaDaiLyHQ = VAD0AP0.A38.GetValue().ToString();
            TKMD.TenDaiLyHaiQuan = VAD0AP0.A39.GetValue().ToString();
            TKMD.MaNhanVienHaiQuan = VAD0AP0.A40.GetValue().ToString();

            #region Vận đơn
            if (TKMD.VanDonCollection == null) TKMD.VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
            List<KDT_VNACC_TK_SoVanDon> ListVanDon = new List<KDT_VNACC_TK_SoVanDon>();
            if (VAD0AP0.KA1.listAttribute != null && VAD0AP0.KA1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.KA1.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TK_SoVanDon SoVanDon = new KDT_VNACC_TK_SoVanDon();
                    SoVanDon.SoTT = i + 1;
                    SoVanDon.SoVanDon = VAD0AP0.KA1.listAttribute[0].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(SoVanDon.SoVanDon.Trim()))
                        ListVanDon.Add(SoVanDon);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_SoVanDon>(TKMD.VanDonCollection, ListVanDon);
            #endregion
            TKMD.SoLuong = Convert.ToDecimal(VAD0AP0.A49.GetValue(), cltInfo);
            TKMD.MaDVTSoLuong = VAD0AP0.A50.GetValue().ToString();
            TKMD.TrongLuong = Convert.ToDecimal(VAD0AP0.A53.GetValue(), cltInfo);
            TKMD.MaDVTTrongLuong = VAD0AP0.A54.GetValue().ToString();
            TKMD.SoLuongCont = Convert.ToDecimal(VAD0AP0.A57.GetValue(), cltInfo);
            TKMD.MaDDLuuKho = VAD0AP0.A51.GetValue().ToString();
            TKMD.TenDDLuuKho = VAD0AP0.A52.GetValue().ToString();
            TKMD.MaDiaDiemDoHang = VAD0AP0.A42.GetValue().ToString();
            TKMD.TenDiaDiemDohang = VAD0AP0.A43.GetValue().ToString();
            TKMD.MaDiaDiemXepHang = VAD0AP0.A44.GetValue().ToString();
            TKMD.TenDiaDiemXepHang = VAD0AP0.A45.GetValue().ToString();
            TKMD.MaPTVC = VAD0AP0.A46.GetValue().ToString();
            TKMD.TenPTVC = VAD0AP0.A47.GetValue().ToString();
            TKMD.NgayHangDen = Convert.ToDateTime(VAD0AP0.A48.GetValue());
            TKMD.SoHieuKyHieu = VAD0AP0.A62.GetValue().ToString();
            TKMD.NgayNhapKhoDau = Convert.ToDateTime(VAD0AP0.A58.GetValue());
            #region Mã văn bản pháp quy khác

            if (VAD0AP0.KB1.listAttribute != null && VAD0AP0.KB1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.KB1.listAttribute[0].ListValue.Count; i++)
                {
                    if (!string.IsNullOrEmpty(VAD0AP0.KB1.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        switch (i)
                        {
                            case 0:
                                TKMD.MaVanbanPhapQuy1 = VAD0AP0.KB1.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 1:
                                TKMD.MaVanbanPhapQuy2 = VAD0AP0.KB1.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 2:
                                TKMD.MaVanbanPhapQuy3 = VAD0AP0.KB1.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 3:
                                TKMD.MaVanbanPhapQuy4 = VAD0AP0.KB1.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                            case 4:
                                TKMD.MaVanbanPhapQuy5 = VAD0AP0.KB1.listAttribute[0].GetValueCollection(i).ToString();
                                break;
                        }
                    }
                }
            }
            #endregion
            TKMD.PhanLoaiHD = VAD0AP0.A72.GetValue().ToString();
            TKMD.SoHoaDon = VAD0AP0.A73.GetValue().ToString();
            TKMD.SoTiepNhanHD = Convert.ToDecimal(VAD0AP0.A74.GetValue(), cltInfo);
            TKMD.NgayPhatHanhHD = Convert.ToDateTime(VAD0AP0.IVD.GetValue());
            TKMD.PhuongThucTT = VAD0AP0.IVP.GetValue().ToString();
            TKMD.PhanLoaiGiaHD = VAD0AP0.A75.GetValue().ToString();
            TKMD.MaDieuKienGiaHD = VAD0AP0.A76.GetValue().ToString();
            TKMD.MaTTHoaDon = VAD0AP0.A77.GetValue().ToString();
            TKMD.TongTriGiaHD = Convert.ToDecimal(VAD0AP0.A78.GetValue(), cltInfo);
            TKMD.TriGiaTinhThue = Convert.ToDecimal(VAD0AP0.A87.GetValue(), cltInfo);
            //TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(VAD0AP0.A97.GetValue(), cltInfo);
            TKMD.MaPhanLoaiTongGiaCoBan = VAD0AP0.A98.GetValue().ToString();
            TKMD.MaKetQuaKiemTra = VAD0AP0.B02.GetValue().ToString();
            TKMD.MaPhanLoaiTriGia = VAD0AP0.A88.GetValue().ToString();
            TKMD.SoTiepNhanTKTriGia = string.IsNullOrEmpty(VAD0AP0.A89.GetValue().ToString()) ? 0 : Convert.ToDecimal(VAD0AP0.A89.GetValue(), cltInfo);
            TKMD.PhanLoaiCongThucChuan = VAD0AP0.A94.GetValue().ToString();
            TKMD.MaPhanLoaiDieuChinhTriGia = VAD0AP0.A95.GetValue().ToString();
            TKMD.PhuongPhapDieuChinhTriGia = VAD0AP0.A96.GetValue().ToString();
            TKMD.MaTTHieuChinhTriGia = VAD0AP0.ADD.GetValue().ToString();
            TKMD.GiaHieuChinhTriGia = Convert.ToDecimal(VAD0AP0.ADE.GetValue(), cltInfo);
            TKMD.MaPhanLoaiPhiVC = VAD0AP0.A79.GetValue().ToString();
            TKMD.MaTTPhiVC = VAD0AP0.A80.GetValue().ToString();
            TKMD.PhiVanChuyen = Convert.ToDecimal(VAD0AP0.A81.GetValue(), cltInfo);
            TKMD.MaPhanLoaiPhiBH = VAD0AP0.A82.GetValue().ToString();
            TKMD.MaTTPhiBH = VAD0AP0.A83.GetValue().ToString();
            TKMD.PhiBaoHiem = Convert.ToDecimal(VAD0AP0.A84.GetValue(), cltInfo);
            TKMD.SoDangKyBH = VAD0AP0.A85.GetValue().ToString();

            #region Giấy phép
            List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
            if (VAD0AP0.KC1.listAttribute != null && VAD0AP0.KC1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.KC1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.GiayPhepCollection == null) TKMD.GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
                    KDT_VNACC_TK_GiayPhep giayPhep = new KDT_VNACC_TK_GiayPhep();
                    giayPhep.PhanLoai = VAD0AP0.KC1.listAttribute[0].GetValueCollection(i).ToString();
                    giayPhep.SoGiayPhep = VAD0AP0.KC1.listAttribute[1].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(giayPhep.SoGiayPhep))
                        listGiayPhep.Add(giayPhep);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_GiayPhep>(TKMD.GiayPhepCollection, listGiayPhep);
            #endregion
            #region Khoản điều chỉnh
            List<KDT_VNACC_TK_KhoanDieuChinh> ListKDC = new List<KDT_VNACC_TK_KhoanDieuChinh>();
            if (VAD0AP0.VR1.listAttribute != null && VAD0AP0.VR1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.VR1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.KhoanDCCollection == null) TKMD.KhoanDCCollection = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    KDT_VNACC_TK_KhoanDieuChinh khoanDieuChinh = new KDT_VNACC_TK_KhoanDieuChinh();
                    khoanDieuChinh.MaTenDieuChinh = VAD0AP0.VR1.listAttribute[0].GetValueCollection(i).ToString();
                    khoanDieuChinh.MaPhanLoaiDieuChinh = VAD0AP0.VR1.listAttribute[1].GetValueCollection(i).ToString();
                    khoanDieuChinh.MaTTDieuChinhTriGia = VAD0AP0.VR1.listAttribute[2].GetValueCollection(i).ToString();
                    khoanDieuChinh.TriGiaKhoanDieuChinh = Convert.ToDecimal(VAD0AP0.VR1.listAttribute[3].GetValueCollection(i), cltInfo);
                    khoanDieuChinh.TongHeSoPhanBo = Convert.ToDecimal(VAD0AP0.VR1.listAttribute[4].GetValueCollection(i).ToString(), cltInfo);
                    khoanDieuChinh.SoTT = i + 1;
                    if (!string.IsNullOrEmpty(khoanDieuChinh.MaTenDieuChinh) || !string.IsNullOrEmpty(khoanDieuChinh.MaPhanLoaiDieuChinh))
                        ListKDC.Add(khoanDieuChinh);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_KhoanDieuChinh>(TKMD.KhoanDCCollection, ListKDC);
            #endregion

            TKMD.ChiTietKhaiTriGia = VAD0AP0.VLD.GetValue().ToString();
            #region Sắc thuế
            List<KDT_VNACC_TK_PhanHoi_SacThue> lSacThue = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
            if (VAD0AP0.KE1.listAttribute != null && VAD0AP0.KE1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.KE1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.SacThueCollection == null) TKMD.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                    KDT_VNACC_TK_PhanHoi_SacThue sacthue = new KDT_VNACC_TK_PhanHoi_SacThue();
                    sacthue.MaSacThue = VAD0AP0.KE1.listAttribute[0].GetValueCollection(i).ToString();
                    sacthue.TenSacThue = VAD0AP0.KE1.listAttribute[1].GetValueCollection(i).ToString();
                    sacthue.TongTienThue = System.Convert.ToDecimal(VAD0AP0.KE1.listAttribute[2].GetValueCollection(i), cltInfo);
                    sacthue.SoDongTongTienThue = System.Convert.ToDecimal(VAD0AP0.KE1.listAttribute[3].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(sacthue.MaSacThue))
                        lSacThue.Add(sacthue);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_SacThue>(TKMD.SacThueCollection, lSacThue);
            #endregion
            TKMD.TongTienThuePhaiNop = Convert.ToDecimal(VAD0AP0.B03.GetValue(), cltInfo);
            TKMD.SoTienBaoLanh = Convert.ToDecimal(VAD0AP0.B04.GetValue(), cltInfo);
            #region Tỷ giá
            List<KDT_VNACC_TK_PhanHoi_TyGia> lTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
            if (VAD0AP0.KI1.listAttribute != null && VAD0AP0.KI1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.KI1.listAttribute.Count; i++)
                {
                    if (TKMD.TyGiaCollection == null) TKMD.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    KDT_VNACC_TK_PhanHoi_TyGia tygia = new KDT_VNACC_TK_PhanHoi_TyGia();
                    tygia.MaTTTyGiaTinhThue = VAD0AP0.KI1.listAttribute[0].GetValueCollection(i).ToString();
                    tygia.TyGiaTinhThue = Convert.ToDecimal(VAD0AP0.KI1.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(tygia.MaTTTyGiaTinhThue))
                    {
                        lTyGia.Add(tygia);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_TyGia>(TKMD.TyGiaCollection, lTyGia);
            #endregion

            TKMD.MaLyDoDeNghiBP = VAD0AP0.B07.GetValue().ToString();
            TKMD.PhanLoaiNopThue = VAD0AP0.B12.GetValue().ToString();
            TKMD.NguoiNopThue = VAD0AP0.TPM.GetValue().ToString();
            TKMD.TongSoTrangCuaToKhai = Convert.ToDecimal(VAD0AP0.B13.GetValue(), cltInfo);
            TKMD.TongSoDongHangCuaToKhai = Convert.ToDecimal(VAD0AP0.B14.GetValue(), cltInfo);

            #region Phân loại đính kèm khai báo điện tử
            List<KDT_VNACC_TK_DinhKemDienTu> lDinhKem = new List<KDT_VNACC_TK_DinhKemDienTu>();
            if (VAD0AP0.EA1.listAttribute != null && VAD0AP0.EA1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.EA1.listAttribute.Count; i++)
                {
                    if (TKMD.DinhKemCollection == null) TKMD.DinhKemCollection = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    KDT_VNACC_TK_DinhKemDienTu dinhkem = new KDT_VNACC_TK_DinhKemDienTu();
                    dinhkem.PhanLoai = Convert.ToString(VAD0AP0.EA1.listAttribute[0].GetValueCollection(i).ToString());
                    dinhkem.SoDinhKemKhaiBaoDT = System.Convert.ToDecimal(VAD0AP0.EA1.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(dinhkem.PhanLoai) || dinhkem.SoDinhKemKhaiBaoDT > 0)
                    {
                        lDinhKem.Add(dinhkem);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_DinhKemDienTu>(TKMD.DinhKemCollection, lDinhKem);
            #endregion

            TKMD.GhiChu = VAD0AP0.B15.GetValue().ToString();
            TKMD.SoQuanLyNoiBoDN = VAD0AP0.B19.GetValue().ToString();
            TKMD.SoQuanLyNguoiSuDung = VAD0AP0.B17.GetValue().ToString();

            #region Chỉ thị HQ
            List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
            if (VAD0AP0.D01.listAttribute != null && VAD0AP0.D01.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.D01.listAttribute.Count; i++)
                {
                    if (TKMD.ChiThiHQCollection == null) TKMD.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                    ChiThiHQ.PhanLoai = VAD0AP0.B99.GetValue().ToString();
                    ChiThiHQ.Ngay = Convert.ToDateTime(VAD0AP0.D01.listAttribute[0].GetValueCollection(i));
                    ChiThiHQ.Ten = VAD0AP0.D01.listAttribute[1].GetValueCollection(i).ToString();
                    ChiThiHQ.NoiDung = VAD0AP0.D01.listAttribute[2].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(ChiThiHQ.Ten) || !string.IsNullOrEmpty(ChiThiHQ.NoiDung))
                        lChiThi.Add(ChiThiHQ);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_ChiThiHaiQuan>(TKMD.ChiThiHQCollection, lChiThi);
            #endregion

            TKMD.MaNHTraThueThay = VAD0AP0.ADH.GetValue().ToString();
            TKMD.NamPhatHanhHM = Convert.ToDecimal(VAD0AP0.ADO.GetValue(), cltInfo);
            TKMD.KyHieuCTHanMuc = VAD0AP0.ADP.GetValue().ToString();
            TKMD.SoCTHanMuc = VAD0AP0.ADQ.GetValue().ToString();
            TKMD.MaXDThoiHanNopThue = VAD0AP0.ADI.GetValue().ToString();
            TKMD.MaNHBaoLanh = VAD0AP0.ADJ.GetValue().ToString();
            TKMD.NamPhatHanhBL = Convert.ToDecimal(VAD0AP0.ADK.GetValue(), cltInfo);
            TKMD.KyHieuCTBaoLanh = VAD0AP0.ADL.GetValue().ToString();
            TKMD.SoCTBaoLanh = VAD0AP0.ADM.GetValue().ToString();
            TKMD.NgayCapPhep = Convert.ToDateTime(VAD0AP0.B21.GetValue());
            TKMD.NgayCapPhep = HelperVNACCS.AddHoursToDateTime(TKMD.NgayCapPhep, VAD0AP0.AD4.GetValue().ToString());
            TKMD.NgayHoanThanhKiemTra = Convert.ToDateTime(VAD0AP0.B22.GetValue());
            TKMD.NgayHoanThanhKiemTra = HelperVNACCS.AddHoursToDateTime(TKMD.NgayHoanThanhKiemTra, VAD0AP0.AD5.GetValue().ToString());
            TKMD.PhanLoaiThamTraSauThongQuan = VAD0AP0.B23.GetValue().ToString();
            TKMD.NgayPheDuyetBP = Convert.ToDateTime(VAD0AP0.B24.GetValue());
            TKMD.NgayPheDuyetBP = HelperVNACCS.AddHoursToDateTime(TKMD.NgayPheDuyetBP, VAD0AP0.AD6.GetValue().ToString());
            TKMD.NgayHoanThanhKiemTraBP = Convert.ToDateTime(VAD0AP0.B25.GetValue());
            TKMD.NgayHoanThanhKiemTraBP = HelperVNACCS.AddHoursToDateTime(TKMD.NgayHoanThanhKiemTraBP, VAD0AP0.AD7.GetValue().ToString());
            TKMD.SoNgayDoiCapPhepNhapKhau = Convert.ToDecimal(VAD0AP0.B26.GetValue());
            TKMD.TieuDe = VAD0AP0.B42.GetValue().ToString();

            #region Ân hạn thuế
            List<KDT_VNACC_TK_PhanHoi_AnHan> lAnHan = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
            if (VAD0AP0.KL1.listAttribute != null && VAD0AP0.KL1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.KL1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.AnHangCollection == null) TKMD.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
                    KDT_VNACC_TK_PhanHoi_AnHan anhan = new KDT_VNACC_TK_PhanHoi_AnHan();
                    anhan.MaSacThueAnHan = VAD0AP0.KL1.listAttribute[0].GetValueCollection(i).ToString();
                    anhan.TenSacThueAnHan = VAD0AP0.KL1.listAttribute[1].GetValueCollection(i).ToString();
                    anhan.HanNopThueSauKhiAnHan = Convert.ToDateTime(VAD0AP0.KL1.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(anhan.MaSacThueAnHan))
                    {
                        lAnHan.Add(anhan);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_AnHan>(TKMD.AnHangCollection, lAnHan);
            #endregion
            TKMD.MaSacThueAnHan_VAT = VAD0AP0.ADX.GetValue().ToString();
            TKMD.TenSacThueAnHan_VAT = VAD0AP0.ADY.GetValue().ToString();
            TKMD.HanNopThueSauKhiAnHan_VAT = Convert.ToDateTime(VAD0AP0.ADZ.GetValue());
            TKMD.NgayKhoiHanhVC = Convert.ToDateTime(VAD0AP0.B27.GetValue());

            #region Trung chuyen
            List<KDT_VNACC_TK_TrungChuyen> ltrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
            if (VAD0AP0.ST1.listAttribute != null && VAD0AP0.ST1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0.ST1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TrungChuyenCollection == null) TKMD.TrungChuyenCollection = new List<KDT_VNACC_TK_TrungChuyen>();
                    KDT_VNACC_TK_TrungChuyen trungchuyen = new KDT_VNACC_TK_TrungChuyen();
                    trungchuyen.MaDiaDiem = VAD0AP0.ST1.listAttribute[0].GetValueCollection(i).ToString();
                    trungchuyen.NgayDen = System.Convert.ToDateTime(VAD0AP0.ST1.listAttribute[1].GetValueCollection(i));
                    trungchuyen.NgayKhoiHanh = System.Convert.ToDateTime(VAD0AP0.ST1.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(trungchuyen.MaDiaDiem))
                    {
                        ltrungChuyen.Add(trungchuyen);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_TrungChuyen>(TKMD.TrungChuyenCollection, ltrungChuyen);
            #endregion

            TKMD.DiaDiemDichVC = VAD0AP0.ARP.GetValue().ToString();
            TKMD.NgayDen = Convert.ToDateTime(VAD0AP0.B28.GetValue());
            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {
                TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                for (int i = 0; i < VAD0AP0.HangMD.Count; i++)
                {
                    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        hmd.tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        hmd.tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    hmd = GetHMDFromVAD0AP0_Hang(VAD0AP0.HangMD[i], hmd);
                    if (hmd != null)
                        TKMD.HangCollection.Add(hmd);
                }
            }
            else
            {
                for (int i = 0; i < VAD0AP0.HangMD.Count; i++)
                {
                    if (i < TKMD.HangCollection.Count)
                    {
                        //minhnd Check to khai nhập-xuất
                        if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                            TKMD.HangCollection[i].tkxuat = false;
                        else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                            TKMD.HangCollection[i].tkxuat = true;
                        //minhnd Check to khai nhập-xuất
                        TKMD.HangCollection[i] = GetHMDFromVAD0AP0_Hang(VAD0AP0.HangMD[i], TKMD.HangCollection[i]);
                    }
                }
            }
            return TKMD;
        }

        public static KDT_VNACC_HangMauDich GetHMDFromVAD0AP0_Hang(VAD0AP0_HANG VAD0AP0_Hang, KDT_VNACC_HangMauDich HMD)
        {
            //KDT_VNACC_HangMauDich HMD = new KDT_VNACC_HangMauDich();
            if (string.IsNullOrEmpty(VAD0AP0_Hang.B48.GetValue().ToString()))
                return HMD;
            HMD.SoDong = VAD0AP0_Hang.B46.GetValue().ToString();
            HMD.MaSoHang = VAD0AP0_Hang.B48.GetValue().ToString();
            HMD.MaQuanLy = VAD0AP0_Hang.ADN.GetValue().ToString();
            HMD.MaPhanLoaiTaiXacNhanGia = VAD0AP0_Hang.B50.GetValue().ToString();
            HMD.MaHangHoaKhaiBao = VAD0AP0_Hang.B51.GetValue().ToString();
            HMD.SoLuong1 = Convert.ToDecimal(VAD0AP0_Hang.B52.GetValue(), cltInfo);
            HMD.DVTLuong1 = VAD0AP0_Hang.B53.GetValue().ToString();

            if (VAD0AP0_Hang.VN1.listAttribute != null && VAD0AP0_Hang.VN1.listAttribute[0].ListValue != null)
            {
                string SoMucKhai = string.Empty;
                for (int i = 0; i < VAD0AP0_Hang.VN1.listAttribute[0].ListValue.Count; i++)
                {

                    if (!string.IsNullOrEmpty(VAD0AP0_Hang.VN1.listAttribute[0].GetValueCollection(i).ToString()))
                    {
                        SoMucKhai += VAD0AP0_Hang.VN1.listAttribute[0].GetValueCollection(i).ToString();
                    }

                }
                HMD.SoMucKhaiKhoanDC = SoMucKhai;
            }
            HMD.SoLuong2 = Convert.ToDecimal(VAD0AP0_Hang.B55.GetValue(), cltInfo);
            HMD.DVTLuong2 = VAD0AP0_Hang.B56.GetValue().ToString();
            HMD.TriGiaHoaDon = Convert.ToDecimal(VAD0AP0_Hang.B69.GetValue(), cltInfo);
            HMD.DonGiaHoaDon = Convert.ToDecimal(VAD0AP0_Hang.UPR.GetValue(), cltInfo);
            HMD.MaTTDonGia = VAD0AP0_Hang.UPC.GetValue().ToString();
            HMD.DVTDonGia = VAD0AP0_Hang.TSC.GetValue().ToString();
            HMD.TriGiaTinhThueS = Convert.ToDecimal(VAD0AP0_Hang.B57.GetValue(), cltInfo);
            HMD.MaTTTriGiaTinhThueS = VAD0AP0_Hang.B71.GetValue().ToString();
            HMD.TriGiaTinhThue = Convert.ToDecimal(VAD0AP0_Hang.B72.GetValue(), cltInfo);
            HMD.SoLuongTinhThue = Convert.ToDecimal(VAD0AP0_Hang.B58.GetValue(), cltInfo);
            HMD.MaDVTDanhThue = VAD0AP0_Hang.B59.GetValue().ToString();
            HMD.DonGiaTinhThue = Convert.ToDecimal(VAD0AP0_Hang.KKT.GetValue(), cltInfo);
            HMD.DV_SL_TrongDonGiaTinhThue = VAD0AP0_Hang.KKS.GetValue().ToString();
            HMD.MaPhanLoaiThueSuatThue = VAD0AP0_Hang.B63.GetValue().ToString();
            HMD.ThueSuatThue = VAD0AP0_Hang.B64.GetValue().ToString().Replace("%","");
            HMD.PhanLoaiThueSuatThue = VAD0AP0_Hang.SKB.GetValue().ToString();
            HMD.MaThueNKTheoLuong = VAD0AP0_Hang.SPD.GetValue().ToString();
            HMD.SoTienThue = Convert.ToDecimal(VAD0AP0_Hang.B68.GetValue(), cltInfo);
            HMD.NuocXuatXu = VAD0AP0_Hang.B76.GetValue().ToString();
            HMD.TenNoiXuatXu = VAD0AP0_Hang.B77.GetValue().ToString();
            HMD.MaBieuThueNK = VAD0AP0_Hang.B78.GetValue().ToString();
            HMD.SoTienGiamThue = Convert.ToDecimal(VAD0AP0_Hang.B70.GetValue(), cltInfo);
            HMD.MaHanNgach = VAD0AP0_Hang.KWS.GetValue().ToString();
            HMD.SoTTDongHangTKTNTX = VAD0AP0_Hang.TDL.GetValue().ToString();
            HMD.SoDMMienThue = VAD0AP0_Hang.TXN.GetValue().ToString();
            HMD.SoDongDMMienThue = VAD0AP0_Hang.TXR.GetValue().ToString();
            HMD.MaMienGiam = VAD0AP0_Hang.B79.GetValue().ToString();
            HMD.DieuKhoanMienGiam = VAD0AP0_Hang.B80.GetValue().ToString();

            List<KDT_VNACC_HangMauDich_ThueThuKhac> lThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
            if (VAD0AP0_Hang.KO1.listAttribute != null && VAD0AP0_Hang.KO1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAD0AP0_Hang.KO1.listAttribute[0].ListValue.Count; i++)
                {
                    if (HMD.ThueThuKhacCollection == null)
                    {
                        HMD.ThueThuKhacCollection = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                    }
                    KDT_VNACC_HangMauDich_ThueThuKhac thukhac = new KDT_VNACC_HangMauDich_ThueThuKhac();
                    thukhac.TenKhoanMucThueVaThuKhac = VAD0AP0_Hang.KO1.listAttribute[0].GetValueCollection(i).ToString();
                    thukhac.MaTSThueThuKhac = VAD0AP0_Hang.KO1.listAttribute[1].GetValueCollection(i).ToString();
                    thukhac.TriGiaTinhThueVaThuKhac = System.Convert.ToDecimal(VAD0AP0_Hang.KO1.listAttribute[2].GetValueCollection(i), cltInfo);
                    thukhac.SoLuongTinhThueVaThuKhac = System.Convert.ToDecimal(VAD0AP0_Hang.KO1.listAttribute[3].GetValueCollection(i), cltInfo);
                    thukhac.MaDVTDanhThueVaThuKhac = VAD0AP0_Hang.KO1.listAttribute[4].GetValueCollection(i).ToString();
                    thukhac.ThueSuatThueVaThuKhac = VAD0AP0_Hang.KO1.listAttribute[5].GetValueCollection(i).ToString();
                    thukhac.SoTienThueVaThuKhac = System.Convert.ToDecimal(VAD0AP0_Hang.KO1.listAttribute[6].GetValueCollection(i), cltInfo);
                    thukhac.MaMGThueThuKhac = VAD0AP0_Hang.KO1.listAttribute[7].GetValueCollection(i).ToString();
                    thukhac.DieuKhoanMienGiamThueVaThuKhac = VAD0AP0_Hang.KO1.listAttribute[8].GetValueCollection(i).ToString();
                    thukhac.SoTienGiamThueThuKhac = System.Convert.ToDecimal(VAD0AP0_Hang.KO1.listAttribute[9].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(thukhac.MaTSThueThuKhac))
                    {
                        lThuKhac.Add(thukhac);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_HangMauDich_ThueThuKhac>(HMD.ThueThuKhacCollection, lThuKhac);
            if (HMD.MaSoHang != null && !string.IsNullOrEmpty(HMD.MaSoHang))
                return HMD;
            else
                return null;
        }
        public static KDT_VNACC_ToKhaiMauDich GetTKMDFromVAE0LC0(VAE0LC0 VAE0LC0, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            TKMD.SoToKhai = System.Convert.ToDecimal(VAE0LC0.ECN.GetValue(), cltInfo);
            TKMD.SoToKhaiDauTien = VAE0LC0.FIC.GetValue().ToString();
            TKMD.SoNhanhToKhai = System.Convert.ToDecimal(VAE0LC0.BNO.GetValue(), cltInfo);
            TKMD.TongSoTKChiaNho = System.Convert.ToDecimal(VAE0LC0.DNO.GetValue(), cltInfo);
            TKMD.SoToKhaiTNTX = System.Convert.ToDecimal(VAE0LC0.TNO.GetValue(), cltInfo);
            TKMD.MaPhanLoaiKiemTra = VAE0LC0.K07.GetValue().ToString();
            TKMD.MaLoaiHinh = VAE0LC0.K03.GetValue().ToString();
            TKMD.MaPhanLoaiHH = VAE0LC0.K05.GetValue().ToString();
            TKMD.MaPhuongThucVT = VAE0LC0.ADI.GetValue().ToString();
            TKMD.MaSoThueDaiDien = VAE0LC0.K01.GetValue().ToString();
            //TKMD.CoQuanHaiQuan = 
            TKMD.TenCoQuanHaiQuan = VAE0LC0.K08.GetValue().ToString();
            TKMD.NhomXuLyHS = VAE0LC0.K09.GetValue().ToString();
            TKMD.NgayDangKy = Convert.ToDateTime(VAE0LC0.K10.GetValue());
            TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAE0LC0.AD1.GetValue().ToString());
            TKMD.NgayThayDoiDangKy = Convert.ToDateTime(VAE0LC0.AD2.GetValue());
            TKMD.NgayThayDoiDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayThayDoiDangKy, VAE0LC0.AD3.GetValue().ToString());
            TKMD.ThoiHanTaiNhapTaiXuat = Convert.ToDateTime(VAE0LC0.RID.GetValue());
            TKMD.BieuThiTruongHopHetHan = VAE0LC0.AAA.GetValue().ToString();
            TKMD.MaDonVi = VAE0LC0.K16.GetValue().ToString();
            TKMD.TenDonVi = VAE0LC0.K17.GetValue().ToString();
            TKMD.MaBuuChinhDonVi = VAE0LC0.K18.GetValue().ToString();
            TKMD.DiaChiDonVi = VAE0LC0.K19.GetValue().ToString();
            TKMD.SoDienThoaiDonVi = VAE0LC0.K23.GetValue().ToString();
            TKMD.MaUyThac = VAE0LC0.ADJ.GetValue().ToString();
            TKMD.TenUyThac = VAE0LC0.ADK.GetValue().ToString();
            TKMD.MaDoiTac = VAE0LC0.K24.GetValue().ToString();
            TKMD.TenDoiTac = VAE0LC0.K25.GetValue().ToString();
            TKMD.MaBuuChinhDoiTac = VAE0LC0.K26.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAE0LC0.K27.GetValue().ToString();
            TKMD.DiaChiDoiTac2 = VAE0LC0.K28.GetValue().ToString();
            TKMD.DiaChiDoiTac3 = VAE0LC0.K29.GetValue().ToString();
            TKMD.DiaChiDoiTac4 = VAE0LC0.K30.GetValue().ToString();
            TKMD.MaNuocDoiTac = VAE0LC0.K31.GetValue().ToString();
            TKMD.MaDaiLyHQ = VAE0LC0.K32.GetValue().ToString();
            TKMD.TenDaiLyHaiQuan = VAE0LC0.K33.GetValue().ToString();
            TKMD.MaNhanVienHaiQuan = VAE0LC0.K34.GetValue().ToString();
            //TKMD.SoVanDon = VAE0LC0.K35.GetValue().ToString();
            #region  Vận đơn
            string soVanDon = VAE0LC0.K35.GetValue().ToString();
            if (!string.IsNullOrEmpty(soVanDon))
            {
                TKMD.VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
                TKMD.VanDonCollection.Add(new KDT_VNACC_TK_SoVanDon() { SoVanDon = soVanDon });
            }
            #endregion
            TKMD.SoLuong = Convert.ToDecimal(VAE0LC0.K36.GetValue(), cltInfo);
            TKMD.MaDVTSoLuong = VAE0LC0.K37.GetValue().ToString();
            TKMD.TrongLuong = Convert.ToDecimal(VAE0LC0.K39.GetValue(), cltInfo);
            TKMD.MaDVTTrongLuong = VAE0LC0.K40.GetValue().ToString();
            TKMD.MaDDLuuKho = VAE0LC0.K41.GetValue().ToString();
            TKMD.TenDDLuuKho = VAE0LC0.K42.GetValue().ToString();
            TKMD.MaDiaDiemDoHang = VAE0LC0.K43.GetValue().ToString();
            TKMD.TenDiaDiemDohang = VAE0LC0.K44.GetValue().ToString();
            TKMD.MaDiaDiemXepHang = VAE0LC0.K46.GetValue().ToString();
            TKMD.TenDiaDiemXepHang = VAE0LC0.K47.GetValue().ToString();
            TKMD.MaPTVC = VAE0LC0.K50.GetValue().ToString();
            TKMD.TenPTVC = VAE0LC0.K51.GetValue().ToString();
            TKMD.NgayHangDen = Convert.ToDateTime(VAE0LC0.K52.GetValue());
            TKMD.SoHieuKyHieu = VAE0LC0.K56.GetValue().ToString();
            #region Giấy phép
            List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
            if (VAE0LC0.A1.listAttribute != null && VAE0LC0.A1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE0LC0.A1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.GiayPhepCollection == null) TKMD.GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
                    KDT_VNACC_TK_GiayPhep giayPhep = new KDT_VNACC_TK_GiayPhep();
                    giayPhep.PhanLoai = VAE0LC0.A1.listAttribute[0].GetValueCollection(i).ToString();
                    giayPhep.SoGiayPhep = VAE0LC0.A1.listAttribute[1].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(giayPhep.SoGiayPhep))
                        listGiayPhep.Add(giayPhep);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_GiayPhep>(TKMD.GiayPhepCollection, listGiayPhep);
            #endregion
            TKMD.PhanLoaiHD = VAE0LC0.K58.GetValue().ToString();
            TKMD.SoHoaDon = VAE0LC0.K59.GetValue().ToString();
            TKMD.SoTiepNhanHD = Convert.ToDecimal(VAE0LC0.K60.GetValue(), cltInfo);
            TKMD.NgayPhatHanhHD = Convert.ToDateTime(VAE0LC0.IVD.GetValue());
            TKMD.PhuongThucTT = VAE0LC0.IVP.GetValue().ToString();
            TKMD.MaDieuKienGiaHD = VAE0LC0.K61.GetValue().ToString();
            TKMD.MaTTHoaDon = VAE0LC0.K62.GetValue().ToString();
            TKMD.TongTriGiaHD = Convert.ToDecimal(VAE0LC0.K63.GetValue(), cltInfo);
            TKMD.PhanLoaiGiaHD = VAE0LC0.K64.GetValue().ToString();
            TKMD.MaTTTriGiaTinhThue = VAE0LC0.K65.GetValue().ToString();
            TKMD.TriGiaTinhThue = Convert.ToDecimal(VAE0LC0.K66.GetValue(), cltInfo);
            #region Tỷ giá
            List<KDT_VNACC_TK_PhanHoi_TyGia> lTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
            if (VAE0LC0.RC1.listAttribute != null && VAE0LC0.RC1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE0LC0.RC1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TyGiaCollection == null) TKMD.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    KDT_VNACC_TK_PhanHoi_TyGia tygia = new KDT_VNACC_TK_PhanHoi_TyGia();
                    tygia.MaTTTyGiaTinhThue = VAE0LC0.RC1.listAttribute[0].GetValueCollection(i).ToString();
                    tygia.TyGiaTinhThue = Convert.ToDecimal(VAE0LC0.RC1.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(tygia.MaTTTyGiaTinhThue))
                    {
                        lTyGia.Add(tygia);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_TyGia>(TKMD.TyGiaCollection, lTyGia);
            #endregion
            //TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(VAE0LC0.K67.GetValue(), cltInfo);
            TKMD.MaPhanLoaiTongGiaCoBan = VAE0LC0.K68.GetValue().ToString();
            TKMD.PhanLoaiKhongQDVND = VAE0LC0.AD9.GetValue().ToString();
            TKMD.NguoiNopThue = VAE0LC0.TPM.GetValue().ToString();
            TKMD.PhanLoaiNopThue = VAE0LC0.PAY.GetValue().ToString();
            TKMD.TongSoTienThueXuatKhau = Convert.ToDecimal(VAE0LC0.ETA.GetValue(), cltInfo);
            TKMD.MaTTTongTienThueXuatKhau = VAE0LC0.ADA.GetValue().ToString();
            TKMD.TongSoTienLePhi = Convert.ToDecimal(VAE0LC0.TCO.GetValue(), cltInfo);
            TKMD.SoTienBaoLanh = Convert.ToDecimal(VAE0LC0.SAM.GetValue(), cltInfo);
            TKMD.MaTTCuaSoTienBaoLanh = VAE0LC0.ADB.GetValue().ToString();
            TKMD.TongSoTrangCuaToKhai = Convert.ToDecimal(VAE0LC0.K69.GetValue(), cltInfo);
            TKMD.TongSoDongHangCuaToKhai = Convert.ToDecimal(VAE0LC0.K70.GetValue(), cltInfo);
            #region Phân loại đính kèm khai báo điện tử
            List<KDT_VNACC_TK_DinhKemDienTu> lDinhKem = new List<KDT_VNACC_TK_DinhKemDienTu>();
            if (VAE0LC0.EA1.listAttribute != null && VAE0LC0.EA1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE0LC0.EA1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.DinhKemCollection == null) TKMD.DinhKemCollection = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    KDT_VNACC_TK_DinhKemDienTu dinhkem = new KDT_VNACC_TK_DinhKemDienTu();
                    dinhkem.PhanLoai = VAE0LC0.EA1.listAttribute[0].GetValueCollection(i).ToString();
                    dinhkem.SoDinhKemKhaiBaoDT = System.Convert.ToDecimal(VAE0LC0.EA1.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(dinhkem.PhanLoai) || dinhkem.SoDinhKemKhaiBaoDT > 0)
                    {
                        lDinhKem.Add(dinhkem);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_DinhKemDienTu>(TKMD.DinhKemCollection, lDinhKem);
            #endregion
            TKMD.GhiChu = VAE0LC0.K79.GetValue().ToString();
            TKMD.SoQuanLyNoiBoDN = VAE0LC0.K81.GetValue().ToString();
            TKMD.SoQuanLyNguoiSuDung = VAE0LC0.K82.GetValue().ToString();
            TKMD.MaNHTraThueThay = VAE0LC0.ADL.GetValue().ToString();
            TKMD.NamPhatHanhHM = Convert.ToDecimal(VAE0LC0.ADT.GetValue(), cltInfo);
            TKMD.KyHieuCTHanMuc = VAE0LC0.ADU.GetValue().ToString();
            TKMD.SoCTHanMuc = VAE0LC0.ADV.GetValue().ToString();
            TKMD.MaXDThoiHanNopThue = VAE0LC0.ADM.GetValue().ToString();
            TKMD.MaNHBaoLanh = VAE0LC0.ADO.GetValue().ToString();
            TKMD.NamPhatHanhBL = Convert.ToDecimal(VAE0LC0.ADP.GetValue(), cltInfo);
            TKMD.KyHieuCTBaoLanh = VAE0LC0.ADQ.GetValue().ToString();
            TKMD.SoCTBaoLanh = VAE0LC0.ADR.GetValue().ToString();
            TKMD.NgayHoanThanhKiemTra = Convert.ToDateTime(VAE0LC0.CCD.GetValue());
            TKMD.NgayHoanThanhKiemTra = HelperVNACCS.AddHoursToDateTime(TKMD.NgayHoanThanhKiemTra, VAE0LC0.AD4.GetValue().ToString());
            TKMD.NgayCapPhep = Convert.ToDateTime(VAE0LC0.K86.GetValue());
            TKMD.NgayCapPhep = HelperVNACCS.AddHoursToDateTime(TKMD.NgayCapPhep, VAE0LC0.AD5.GetValue().ToString());
            TKMD.NgayKhoiHanhVC = Convert.ToDateTime(VAE0LC0.K88.GetValue());
            #region Trung chuyen
            List<KDT_VNACC_TK_TrungChuyen> ltrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
            if (VAE0LC0.ST1.listAttribute != null && VAE0LC0.ST1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE0LC0.ST1.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TrungChuyenCollection == null) TKMD.TrungChuyenCollection = new List<KDT_VNACC_TK_TrungChuyen>();
                    KDT_VNACC_TK_TrungChuyen trungchuyen = new KDT_VNACC_TK_TrungChuyen();
                    trungchuyen.MaDiaDiem = VAE0LC0.ST1.listAttribute[0].GetValueCollection(i).ToString();
                    trungchuyen.NgayDen = System.Convert.ToDateTime(VAE0LC0.ST1.listAttribute[1].GetValueCollection(i));
                    trungchuyen.NgayKhoiHanh = System.Convert.ToDateTime(VAE0LC0.ST1.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(trungchuyen.MaDiaDiem))
                    {
                        ltrungChuyen.Add(trungchuyen);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_TrungChuyen>(TKMD.TrungChuyenCollection, ltrungChuyen);
            #endregion
            TKMD.DiaDiemDichVC = VAE0LC0.ARR.GetValue().ToString();
            TKMD.NgayDen = Convert.ToDateTime(VAE0LC0.K89.GetValue());
            #region  Container

            if (TKMD.ContainerTKMD == null) TKMD.ContainerTKMD = new KDT_VNACC_TK_Container();
            // Mã địa điểm xếp lên xe chở hàng trong vận đơn
            if (VAE0LC0.E1.listAttribute != null && VAE0LC0.E1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE0LC0.E1.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            TKMD.ContainerTKMD.MaDiaDiem1 = VAE0LC0.E1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            TKMD.ContainerTKMD.MaDiaDiem2 = VAE0LC0.E1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            TKMD.ContainerTKMD.MaDiaDiem3 = VAE0LC0.E1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            TKMD.ContainerTKMD.MaDiaDiem4 = VAE0LC0.E1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            TKMD.ContainerTKMD.MaDiaDiem5 = VAE0LC0.E1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            TKMD.ContainerTKMD.TenDiaDiem = VAE0LC0.K71.GetValue().ToString();
            TKMD.ContainerTKMD.DiaChiDiaDiem = VAE0LC0.K72.GetValue().ToString();
            List<KDT_VNACC_TK_SoContainer> listCont = new List<KDT_VNACC_TK_SoContainer>();
            if (VAE0LC0.C01.listAttribute != null && VAE0LC0.C01.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE0LC0.C01.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ContainerTKMD.SoContainerCollection == null) TKMD.ContainerTKMD.SoContainerCollection = new List<KDT_VNACC_TK_SoContainer>();
                    KDT_VNACC_TK_SoContainer SoCont = new KDT_VNACC_TK_SoContainer();
                    SoCont.SoTT = i + 1;
                    SoCont.SoContainer = VAE0LC0.C01.listAttribute[0].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(SoCont.SoContainer))
                        listCont.Add(SoCont);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_SoContainer>(TKMD.ContainerTKMD.SoContainerCollection, listCont);
            #endregion
            #region Chỉ thị HQ
            List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
            if (VAE0LC0.D01.listAttribute != null && VAE0LC0.D01.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE0LC0.D01.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ChiThiHQCollection == null) TKMD.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                    ChiThiHQ.PhanLoai = VAE0LC0.CCM.GetValue().ToString();
                    ChiThiHQ.Ngay = Convert.ToDateTime(VAE0LC0.D01.listAttribute[0].GetValueCollection(i));
                    ChiThiHQ.Ten = VAE0LC0.D01.listAttribute[1].GetValueCollection(i).ToString();
                    ChiThiHQ.NoiDung = VAE0LC0.D01.listAttribute[2].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(ChiThiHQ.Ten) || !string.IsNullOrEmpty(ChiThiHQ.NoiDung))
                        lChiThi.Add(ChiThiHQ);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_ChiThiHaiQuan>(TKMD.ChiThiHQCollection, lChiThi);
            #endregion

            if (TKMD.HangCollection != null)
            {
                if (TKMD.HangCollection.Count != VAE0LC0.HangMD.Count)
                {
                    KDT_VNACC_HangMauDich.DeleteCollection(TKMD.HangCollection);
                    TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                }
            }

            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {

                TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();

                for (int i = 0; i < VAE0LC0.HangMD.Count; i++)
                {
                    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        hmd.tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        hmd.tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    hmd = GetHMDFromVAE0LC0_Hang(VAE0LC0.HangMD[i], hmd);
                    if (hmd != null)
                        TKMD.HangCollection.Add(hmd);

                }
            }
            else
            {
               // if (TKMD.HangCollection == null) TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();

                for (int i = 0; i < VAE0LC0.HangMD.Count; i++)
                {
                    if (i < TKMD.HangCollection.Count)
                    {
                        //minhnd Check to khai nhập-xuất
                        if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                            TKMD.HangCollection[i].tkxuat = false;
                        else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                            TKMD.HangCollection[i].tkxuat = true;
                        //minhnd Check to khai nhập-xuất
                        TKMD.HangCollection[i] = GetHMDFromVAE0LC0_Hang(VAE0LC0.HangMD[i], TKMD.HangCollection[i]);
                    }
                }
            }
            return TKMD;
        }
        public static KDT_VNACC_HangMauDich GetHMDFromVAE0LC0_Hang(VAE0LC0_HANG VAE0LC0_Hang, KDT_VNACC_HangMauDich hmd)
        {
            hmd.SoDong = VAE0LC0_Hang.R01.GetValue().ToString();
            hmd.MaSoHang = VAE0LC0_Hang.R05.GetValue().ToString();
            hmd.MaQuanLy = VAE0LC0_Hang.ADS.GetValue().ToString();
            hmd.MaPhanLoaiTaiXacNhanGia = VAE0LC0_Hang.R03.GetValue().ToString();
            hmd.MaHangHoaKhaiBao = VAE0LC0_Hang.R04.GetValue().ToString();
            hmd.SoLuong1 = Convert.ToDecimal(VAE0LC0_Hang.R08.GetValue(), cltInfo);
            hmd.DVTLuong1 = VAE0LC0_Hang.R09.GetValue().ToString();
            hmd.SoLuong2 = Convert.ToDecimal(VAE0LC0_Hang.R11.GetValue(), cltInfo);
            hmd.DVTLuong2 = VAE0LC0_Hang.R12.GetValue().ToString();
            hmd.TriGiaHoaDon = Convert.ToDecimal(VAE0LC0_Hang.R13.GetValue(), cltInfo);
            hmd.DonGiaHoaDon = Convert.ToDecimal(VAE0LC0_Hang.UPR.GetValue(), cltInfo);
            hmd.MaTTDonGia = VAE0LC0_Hang.UPC.GetValue().ToString();
            hmd.DVTDonGia = VAE0LC0_Hang.TSC.GetValue().ToString();
            hmd.TriGiaTinhThueS = Convert.ToDecimal(VAE0LC0_Hang.R07.GetValue(), cltInfo);
            hmd.MaTTTriGiaTinhThueS = VAE0LC0_Hang.ADC.GetValue().ToString();
            hmd.MaTTTriGiaTinhThue = VAE0LC0_Hang.R14.GetValue().ToString();
            hmd.TriGiaTinhThue = Convert.ToDecimal(VAE0LC0_Hang.R15.GetValue(), cltInfo);
            hmd.SoLuongTinhThue = Convert.ToDecimal(VAE0LC0_Hang.TSQ.GetValue(), cltInfo);
            hmd.MaDVTDanhThue = VAE0LC0_Hang.TSU.GetValue().ToString();
            hmd.DonGiaTinhThue = Convert.ToDecimal(VAE0LC0_Hang.CVU.GetValue(), cltInfo);
            hmd.MaTTDonGiaTinhThue = VAE0LC0_Hang.ADD.GetValue().ToString();
            hmd.DV_SL_TrongDonGiaTinhThue = VAE0LC0_Hang.QCV.GetValue().ToString();
            hmd.ThueSuatThue = VAE0LC0_Hang.TRA.GetValue().ToString().Replace("%","");
            hmd.PhanLoaiThueSuatThue = VAE0LC0_Hang.TRM.GetValue().ToString();
            hmd.SoTienThue = Convert.ToDecimal(VAE0LC0_Hang.TAX.GetValue(), cltInfo);
            hmd.MaTTSoTienThueXuatKhau = VAE0LC0_Hang.ADE.GetValue().ToString();
            hmd.SoTienMienGiam = Convert.ToDecimal(VAE0LC0_Hang.TRP.GetValue(), cltInfo);
            hmd.MaTTSoTienMienGiam = VAE0LC0_Hang.ADF.GetValue().ToString();
            hmd.SoTTDongHangTKTNTX = VAE0LC0_Hang.TDN.GetValue().ToString();
            hmd.SoDMMienThue = VAE0LC0_Hang.ADG.GetValue().ToString();
            hmd.SoDongDMMienThue = VAE0LC0_Hang.ADH.GetValue().ToString();
            hmd.TienLePhi_DonGia = VAE0LC0_Hang.CUP.GetValue().ToString();
            hmd.TienBaoHiem_DonGia = VAE0LC0_Hang.IUP.GetValue().ToString();
            hmd.TienLePhi_SoLuong = Convert.ToDecimal(VAE0LC0_Hang.CQU.GetValue(), cltInfo);
            hmd.TienLePhi_MaDVSoLuong = VAE0LC0_Hang.CQC.GetValue().ToString();
            hmd.TienBaoHiem_SoLuong = Convert.ToDecimal(VAE0LC0_Hang.IQU.GetValue(), cltInfo);
            hmd.TienBaoHiem_MaDVSoLuong = VAE0LC0_Hang.IQC.GetValue().ToString();
            hmd.TienLePhi_KhoanTien = Convert.ToDecimal(VAE0LC0_Hang.CPR.GetValue(), cltInfo);
            hmd.TienBaoHiem_KhoanTien = Convert.ToDecimal(VAE0LC0_Hang.IPR.GetValue(), cltInfo);
            if (VAE0LC0_Hang.F1.listAttribute != null && VAE0LC0_Hang.F1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE0LC0_Hang.F1.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            hmd.MaVanBanPhapQuyKhac1 = VAE0LC0_Hang.F1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            hmd.MaVanBanPhapQuyKhac2 = VAE0LC0_Hang.F1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            hmd.MaVanBanPhapQuyKhac3 = VAE0LC0_Hang.F1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            hmd.MaVanBanPhapQuyKhac4 = VAE0LC0_Hang.F1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            hmd.MaVanBanPhapQuyKhac5 = VAE0LC0_Hang.F1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            hmd.MaMienGiam = VAE0LC0_Hang.TRC.GetValue().ToString();
            hmd.DieuKhoanMienGiam = VAE0LC0_Hang.TRL.GetValue().ToString();

            if (hmd.MaSoHang != null && !string.IsNullOrEmpty(hmd.MaSoHang))
                return hmd;
            else
                return null;
        }
        public static KDT_VNACC_ToKhaiMauDich GetTKMDFromVAE1LF0(VAE1LF0 VAE, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            TKMD.SoToKhai = System.Convert.ToDecimal(VAE.ECN.GetValue());
            TKMD.PhanLoaiBaoCaoSuaDoi = VAE.JKN.GetValue().ToString();
            TKMD.SoToKhaiDauTien = VAE.FIC.GetValue().ToString();
            TKMD.SoNhanhToKhai = System.Convert.ToDecimal(VAE.BNO.GetValue());
            TKMD.TongSoTKChiaNho = System.Convert.ToDecimal(VAE.DNO.GetValue());
            TKMD.SoToKhaiTNTX = System.Convert.ToDecimal(VAE.TDN.GetValue());
            TKMD.MaPhanLoaiKiemTra = VAE.K07.GetValue().ToString();
            TKMD.MaLoaiHinh = VAE.ECB.GetValue().ToString();
            TKMD.MaPhanLoaiHH = VAE.CCC.GetValue().ToString();
            TKMD.MaPhuongThucVT = VAE.MTC.GetValue().ToString();
            TKMD.MaSoThueDaiDien = VAE.K01.GetValue().ToString();
            TKMD.TenCoQuanHaiQuan = VAE.K08.GetValue().ToString();
            TKMD.NhomXuLyHS = VAE.CHB.GetValue().ToString();
            TKMD.NgayDangKy = System.Convert.ToDateTime(VAE.K10.GetValue().ToString());
            TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAE.AD1.GetValue().ToString());
            TKMD.NgayThayDoiDangKy = System.Convert.ToDateTime(VAE.AD2.GetValue().ToString());
            TKMD.NgayThayDoiDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAE.AD3.GetValue().ToString());
            TKMD.ThoiHanTaiNhapTaiXuat = System.Convert.ToDateTime(VAE.RID.GetValue().ToString());
            TKMD.BieuThiTruongHopHetHan = VAE.AAA.GetValue().ToString();
            TKMD.MaDonVi = VAE.EPC.GetValue().ToString();
            TKMD.TenDonVi = VAE.EPN.GetValue().ToString();
            TKMD.MaBuuChinhDonVi = VAE.EPP.GetValue().ToString();
            TKMD.DiaChiDonVi = VAE.EPA.GetValue().ToString();
            TKMD.SoDienThoaiDonVi = VAE.EPT.GetValue().ToString();
            TKMD.MaUyThac = VAE.EXC.GetValue().ToString();
            TKMD.TenUyThac = VAE.EXN.GetValue().ToString();
            TKMD.MaDoiTac = VAE.CGC.GetValue().ToString();
            TKMD.TenDoiTac = VAE.CGN.GetValue().ToString();
            TKMD.MaBuuChinhDoiTac = VAE.CGP.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAE.CGA.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAE.CAT.GetValue().ToString();
            TKMD.DiaChiDoiTac3 = VAE.CAC.GetValue().ToString();
            TKMD.DiaChiDoiTac4 = VAE.CAS.GetValue().ToString();
            TKMD.MaNuocDoiTac = VAE.CGK.GetValue().ToString();
            TKMD.MaDaiLyHQ = VAE.K32.GetValue().ToString();
            TKMD.TenDaiLyHaiQuan = VAE.K33.GetValue().ToString();
            TKMD.MaNhanVienHaiQuan = VAE.K34.GetValue().ToString();
            //TKMD. = VAE.EKN.GetValue().ToString();
            #region  Vận đơn
            string soVanDon = VAE.EKN.GetValue().ToString();
            if (!string.IsNullOrEmpty(soVanDon))
            {
                TKMD.VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
                TKMD.VanDonCollection.Add(new KDT_VNACC_TK_SoVanDon() { SoVanDon = soVanDon });
            }
            #endregion
            TKMD.SoLuong = System.Convert.ToDecimal(VAE.NO.GetValue(), cltInfo);
            TKMD.MaDVTSoLuong = VAE.NOT.GetValue().ToString();
            TKMD.TrongLuong = System.Convert.ToDecimal(VAE.GW.GetValue(), cltInfo);
            TKMD.MaDVTTrongLuong = VAE.GWT.GetValue().ToString();
            TKMD.MaDDLuuKho = VAE.ST.GetValue().ToString();
            TKMD.TenDDLuuKho = VAE.K42.GetValue().ToString();
            TKMD.MaDiaDiemDoHang = VAE.DSC.GetValue().ToString();
            TKMD.TenDiaDiemDohang = VAE.DSN.GetValue().ToString();
            TKMD.MaDiaDiemXepHang = VAE.PSC.GetValue().ToString();
            TKMD.TenDiaDiemXepHang = VAE.PSN.GetValue().ToString();
            TKMD.MaPTVC = VAE.VSC.GetValue().ToString();
            TKMD.TenPTVC = VAE.VSN.GetValue().ToString();
            TKMD.NgayHangDen = System.Convert.ToDateTime(VAE.SYM.GetValue().ToString());
            TKMD.SoHieuKyHieu = VAE.MRK.GetValue().ToString();
            #region Giấy phép
            List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
            if (VAE.SS_.listAttribute != null && VAE.SS_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.SS_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.GiayPhepCollection == null) TKMD.GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
                    KDT_VNACC_TK_GiayPhep giayPhep = new KDT_VNACC_TK_GiayPhep();
                    giayPhep.PhanLoai = VAE.SS_.listAttribute[0].GetValueCollection(i).ToString();
                    giayPhep.SoGiayPhep = VAE.SS_.listAttribute[1].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(giayPhep.SoGiayPhep))
                        listGiayPhep.Add(giayPhep);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_GiayPhep>(TKMD.GiayPhepCollection, listGiayPhep);
            #endregion
            TKMD.PhanLoaiHD = VAE.IV1.GetValue().ToString();
            TKMD.SoHoaDon = VAE.IV3.GetValue().ToString();
            TKMD.SoTiepNhanHD = System.Convert.ToDecimal(VAE.IV2.GetValue(), cltInfo);
            TKMD.NgayPhatHanhHD = System.Convert.ToDateTime(VAE.IVD.GetValue().ToString());
            TKMD.PhuongThucTT = VAE.IVP.GetValue().ToString();
            TKMD.MaDieuKienGiaHD = VAE.IP2.GetValue().ToString();
            TKMD.MaTTHoaDon = VAE.IP3.GetValue().ToString();
            TKMD.TongTriGiaHD = System.Convert.ToDecimal(VAE.IP4.GetValue(), cltInfo);
            TKMD.PhanLoaiGiaHD = VAE.IP1.GetValue().ToString();
            TKMD.MaTTTriGiaTinhThue = VAE.FCD.GetValue().ToString();
            TKMD.TriGiaTinhThue = System.Convert.ToDecimal(VAE.FKK.GetValue(), cltInfo);
            #region Tỷ giá
            List<KDT_VNACC_TK_PhanHoi_TyGia> lTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
            if (VAE.RC_.listAttribute != null && VAE.RC_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.RC_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TyGiaCollection == null) TKMD.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    KDT_VNACC_TK_PhanHoi_TyGia tygia = new KDT_VNACC_TK_PhanHoi_TyGia();
                    tygia.MaTTTyGiaTinhThue = VAE.RC_.listAttribute[0].GetValueCollection(i).ToString();
                    tygia.TyGiaTinhThue = Convert.ToDecimal(VAE.RC_.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(tygia.MaTTTyGiaTinhThue))
                    {
                        lTyGia.Add(tygia);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_TyGia>(TKMD.TyGiaCollection, lTyGia);
            #endregion
            TKMD.TongHeSoPhanBoTG = System.Convert.ToDecimal(VAE.TP.GetValue(), cltInfo);
            TKMD.MaPhanLoaiTongGiaCoBan = VAE.K68.GetValue().ToString();
            TKMD.PhanLoaiKhongQDVND = VAE.CNV.GetValue().ToString();
            TKMD.NguoiNopThue = VAE.TPM.GetValue().ToString();
            TKMD.MaXDThoiHanNopThue = VAE.ENC.GetValue().ToString();
            TKMD.PhanLoaiNopThue = VAE.PAY.GetValue().ToString();
            TKMD.TongSoTienThueXuatKhau = System.Convert.ToDecimal(VAE.ETA.GetValue(), cltInfo);
            TKMD.MaTTTongTienThueXuatKhau = VAE.AD7.GetValue().ToString();
            TKMD.TongSoTienLePhi = System.Convert.ToDecimal(VAE.TCO.GetValue(), cltInfo);
            TKMD.SoTienBaoLanh = System.Convert.ToDecimal(VAE.SAM.GetValue(), cltInfo);
            TKMD.MaTTCuaSoTienBaoLanh = VAE.AD8.GetValue().ToString();
            TKMD.TongSoTrangCuaToKhai = System.Convert.ToDecimal(VAE.K69.GetValue(), cltInfo);
            TKMD.TongSoDongHangCuaToKhai = System.Convert.ToDecimal(VAE.K70.GetValue(), cltInfo);
            #region Phân loại đính kèm khai báo điện tử
            List<KDT_VNACC_TK_DinhKemDienTu> lDinhKem = new List<KDT_VNACC_TK_DinhKemDienTu>();
            if (VAE.EA_.listAttribute != null && VAE.EA_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.EA_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.DinhKemCollection == null) TKMD.DinhKemCollection = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    KDT_VNACC_TK_DinhKemDienTu dinhkem = new KDT_VNACC_TK_DinhKemDienTu();
                    dinhkem.PhanLoai = VAE.EA_.listAttribute[0].GetValueCollection(i).ToString();
                    dinhkem.SoDinhKemKhaiBaoDT = System.Convert.ToDecimal(VAE.EA_.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(dinhkem.PhanLoai) || dinhkem.SoDinhKemKhaiBaoDT > 0)
                    {
                        lDinhKem.Add(dinhkem);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_DinhKemDienTu>(TKMD.DinhKemCollection, lDinhKem);
            #endregion
            TKMD.GhiChu = VAE.NT2.GetValue().ToString();
            TKMD.SoQuanLyNoiBoDN = VAE.REF.GetValue().ToString();
            TKMD.SoQuanLyNguoiSuDung = VAE.K82.GetValue().ToString();
            TKMD.NgayHoanThanhKiemTra = System.Convert.ToDateTime(VAE.CCD.GetValue().ToString());
            TKMD.NgayHoanThanhKiemTra = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAE.AD4.GetValue().ToString());
            TKMD.NgayCapPhep = System.Convert.ToDateTime(VAE.K86.GetValue().ToString());
            TKMD.NgayCapPhep = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAE.AD5.GetValue().ToString());
            TKMD.TenTruongDonViHaiQuan = VAE.K87.GetValue().ToString();
            TKMD.NgayKhoiHanhVC = System.Convert.ToDateTime(VAE.DPD.GetValue().ToString());
            #region Trung chuyen
            List<KDT_VNACC_TK_TrungChuyen> ltrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
            if (VAE.ST_.listAttribute != null && VAE.ST_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.ST_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TrungChuyenCollection == null) TKMD.TrungChuyenCollection = new List<KDT_VNACC_TK_TrungChuyen>();
                    KDT_VNACC_TK_TrungChuyen trungchuyen = new KDT_VNACC_TK_TrungChuyen();
                    trungchuyen.MaDiaDiem = VAE.ST_.listAttribute[0].GetValueCollection(i).ToString();
                    trungchuyen.NgayDen = System.Convert.ToDateTime(VAE.ST_.listAttribute[1].GetValueCollection(i));
                    trungchuyen.NgayKhoiHanh = System.Convert.ToDateTime(VAE.ST_.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(trungchuyen.MaDiaDiem))
                    {
                        ltrungChuyen.Add(trungchuyen);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_TrungChuyen>(TKMD.TrungChuyenCollection, ltrungChuyen);
            #endregion
            #region  Container

            if (TKMD.ContainerTKMD == null) TKMD.ContainerTKMD = new KDT_VNACC_TK_Container();
            // Mã địa điểm xếp lên xe chở hàng trong vận đơn
            if (VAE.VC_.listAttribute != null && VAE.VC_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.VC_.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            TKMD.ContainerTKMD.MaDiaDiem1 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            TKMD.ContainerTKMD.MaDiaDiem2 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            TKMD.ContainerTKMD.MaDiaDiem3 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            TKMD.ContainerTKMD.MaDiaDiem4 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            TKMD.ContainerTKMD.MaDiaDiem5 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            TKMD.ContainerTKMD.TenDiaDiem = VAE.VN.GetValue().ToString();
            TKMD.ContainerTKMD.DiaChiDiaDiem = VAE.K72.GetValue().ToString();
            List<KDT_VNACC_TK_SoContainer> listCont = new List<KDT_VNACC_TK_SoContainer>();

            if (VAE.C__.listAttribute != null && VAE.C__.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.C__.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ContainerTKMD.SoContainerCollection == null) TKMD.ContainerTKMD.SoContainerCollection = new List<KDT_VNACC_TK_SoContainer>();
                    KDT_VNACC_TK_SoContainer SoCont = new KDT_VNACC_TK_SoContainer();
                    SoCont.SoTT = i + 1;
                    SoCont.SoContainer = VAE.C__.listAttribute[0].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(SoCont.SoContainer))
                        listCont.Add(SoCont);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_SoContainer>(TKMD.ContainerTKMD.SoContainerCollection, listCont);
            #endregion

            #region Chỉ thị HQ
            List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
            if (VAE.D__.listAttribute != null && VAE.D__.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.D__.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ChiThiHQCollection == null) TKMD.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                    ChiThiHQ.PhanLoai = VAE.CCM.GetValue().ToString();
                    ChiThiHQ.Ngay = Convert.ToDateTime(VAE.D__.listAttribute[0].GetValueCollection(i));
                    ChiThiHQ.Ten = VAE.D__.listAttribute[1].GetValueCollection(i).ToString();
                    ChiThiHQ.NoiDung = VAE.D__.listAttribute[2].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(ChiThiHQ.Ten) || !string.IsNullOrEmpty(ChiThiHQ.NoiDung))
                        lChiThi.Add(ChiThiHQ);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_ChiThiHaiQuan>(TKMD.ChiThiHQCollection, lChiThi);
            #endregion

            if (TKMD.HangCollection != null)
            {
                if (TKMD.HangCollection.Count != VAE.HangMD.Count)
                {
                    KDT_VNACC_HangMauDich.DeleteCollection(TKMD.HangCollection);
                    TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                }
            }
            //if (TKMD.HangCollection == null) TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {

                TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();

                for (int i = 0; i < VAE.HangMD.Count; i++)
                {
                    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        hmd.tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        hmd.tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    hmd = GetHMDFromVAE1LF0_Hang(VAE.HangMD[i], hmd);
                    if (hmd != null)
                        TKMD.HangCollection.Add(hmd);

                }
            }
            else
            {
                for (int i = 0; i < VAE.HangMD.Count; i++)
                {
                    if (i < TKMD.HangCollection.Count)
                    {
                        //minhnd Check to khai nhập-xuất
                        if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                            TKMD.HangCollection[i].tkxuat = false;
                        else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                            TKMD.HangCollection[i].tkxuat = true;
                        //minhnd Check to khai nhập-xuất
                        TKMD.HangCollection[i] = GetHMDFromVAE1LF0_Hang(VAE.HangMD[i], TKMD.HangCollection[i]);
                    }
                }
            }
            return TKMD;
        }
        public static KDT_VNACC_HangMauDich GetHMDFromVAE1LF0_Hang(VAE1LF0_HANG VAE, KDT_VNACC_HangMauDich hmd)
        {
            hmd.SoDong = System.Convert.ToDecimal(VAE.R01.GetValue()).ToString();
            hmd.MaSoHang = VAE.CMD.GetValue().ToString();
            hmd.MaQuanLy = VAE.COC.GetValue().ToString();
            hmd.MaPhanLoaiTaiXacNhanGia = VAE.R03.GetValue().ToString();
            hmd.MaHangHoaKhaiBao = VAE.CMN.GetValue().ToString();
            hmd.SoLuong1 = System.Convert.ToDecimal(VAE.QN1.GetValue(), cltInfo);
            hmd.DVTLuong1 = VAE.QT1.GetValue().ToString();
            hmd.SoLuong2 = System.Convert.ToDecimal(VAE.QN2.GetValue(), cltInfo);
            hmd.DVTLuong2 = VAE.QT2.GetValue().ToString();
            hmd.TriGiaHoaDon = System.Convert.ToDecimal(VAE.BPR.GetValue(), cltInfo);
            hmd.DonGiaHoaDon = System.Convert.ToDecimal(VAE.UPR.GetValue(), cltInfo);
            hmd.MaTTDonGia = VAE.UPC.GetValue().ToString();
            hmd.DVTDonGia = VAE.TSC.GetValue().ToString();
            hmd.TriGiaTinhThueS = System.Convert.ToDecimal(VAE.R07.GetValue(), cltInfo);
            hmd.MaTTTriGiaTinhThueS = VAE.AD9.GetValue().ToString();
            hmd.MaTTTriGiaTinhThue = VAE.R14.GetValue().ToString();
            hmd.TriGiaTinhThue = System.Convert.ToDecimal(VAE.R15.GetValue(), cltInfo);
            hmd.SoLuongTinhThue = System.Convert.ToDecimal(VAE.TSQ.GetValue(), cltInfo);
            hmd.MaDVTDanhThue = VAE.TSU.GetValue().ToString();
            hmd.DonGiaTinhThue = System.Convert.ToDecimal(VAE.CVU.GetValue(), cltInfo);
            hmd.MaTTDonGiaTinhThue = VAE.ADA.GetValue().ToString();
            hmd.DV_SL_TrongDonGiaTinhThue = VAE.QCV.GetValue().ToString();
            hmd.ThueSuatThue = VAE.TRA.GetValue().ToString().Replace("%","");
            hmd.PhanLoaiThueSuatThue = VAE.TRM.GetValue().ToString();
            hmd.SoTienThue = System.Convert.ToDecimal(VAE.TAX.GetValue(), cltInfo);
            hmd.MaTTSoTienThueXuatKhau = VAE.ADB.GetValue().ToString();
            hmd.SoTienMienGiam = System.Convert.ToDecimal(VAE.REG.GetValue(), cltInfo);
            hmd.MaTTSoTienMienGiam = VAE.ADC.GetValue().ToString();
            hmd.SoTTDongHangTKTNTX = VAE.TDL.GetValue().ToString();
            hmd.SoDMMienThue = VAE.TXN.GetValue().ToString();
            hmd.SoDongDMMienThue = VAE.TXR.GetValue().ToString();
            hmd.TienLePhi_DonGia = VAE.CUP.GetValue().ToString();
            hmd.TienBaoHiem_DonGia = VAE.IUP.GetValue().ToString();
            hmd.TienLePhi_SoLuong = System.Convert.ToDecimal(VAE.CQU.GetValue(), cltInfo);
            hmd.TienLePhi_MaDVSoLuong = VAE.CQC.GetValue().ToString();
            hmd.TienBaoHiem_SoLuong = System.Convert.ToDecimal(VAE.IQU.GetValue(), cltInfo);
            hmd.TienBaoHiem_MaDVSoLuong = VAE.IQC.GetValue().ToString();
            hmd.TienLePhi_KhoanTien = System.Convert.ToDecimal(VAE.CPR.GetValue(), cltInfo);
            hmd.TienBaoHiem_KhoanTien = System.Convert.ToDecimal(VAE.IPR.GetValue(), cltInfo);
            if (VAE.OL_.listAttribute != null && VAE.OL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.OL_.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            hmd.MaVanBanPhapQuyKhac1 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            hmd.MaVanBanPhapQuyKhac2 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            hmd.MaVanBanPhapQuyKhac3 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            hmd.MaVanBanPhapQuyKhac4 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            hmd.MaVanBanPhapQuyKhac5 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            hmd.MaMienGiam = VAE.RE.GetValue().ToString();
            hmd.DieuKhoanMienGiam = VAE.TRL.GetValue().ToString();

            if (hmd.MaSoHang != null && !string.IsNullOrEmpty(hmd.MaSoHang))
                return hmd;
            else
                return null;
        }
        public static KDT_VNACC_ToKhaiMauDich GetTKMDFromVAE(VAE1LD0 VAE, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            TKMD.SoToKhai = System.Convert.ToDecimal(VAE.ECN.GetValue());
            TKMD.PhanLoaiBaoCaoSuaDoi = VAE.JKN.GetValue().ToString();
            TKMD.SoToKhaiDauTien = VAE.FIC.GetValue().ToString();
            TKMD.SoNhanhToKhai = System.Convert.ToDecimal(VAE.BNO.GetValue());
            TKMD.TongSoTKChiaNho = System.Convert.ToDecimal(VAE.DNO.GetValue());
            TKMD.SoToKhaiTNTX = System.Convert.ToDecimal(VAE.TDN.GetValue());
            TKMD.MaPhanLoaiKiemTra = VAE.K07.GetValue().ToString();
            TKMD.MaLoaiHinh = VAE.ECB.GetValue().ToString();
            TKMD.MaPhanLoaiHH = VAE.CCC.GetValue().ToString();
            TKMD.MaPhuongThucVT = VAE.MTC.GetValue().ToString();
            TKMD.MaSoThueDaiDien = VAE.K01.GetValue().ToString();
            TKMD.TenCoQuanHaiQuan = VAE.K08.GetValue().ToString();
            TKMD.NhomXuLyHS = VAE.CHB.GetValue().ToString();
            TKMD.NgayDangKy = System.Convert.ToDateTime(VAE.K10.GetValue());
            TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAE.AD1.GetValue().ToString());
            TKMD.NgayThayDoiDangKy = System.Convert.ToDateTime(VAE.AD2.GetValue());
            TKMD.NgayThayDoiDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayThayDoiDangKy, VAE.AD3.GetValue().ToString());
            TKMD.ThoiHanTaiNhapTaiXuat = System.Convert.ToDateTime(VAE.RID.GetValue());
            TKMD.BieuThiTruongHopHetHan = VAE.AAA.GetValue().ToString();
            TKMD.MaDonVi = VAE.EPC.GetValue().ToString();
            TKMD.TenDonVi = VAE.EPN.GetValue().ToString();
            TKMD.MaBuuChinhDonVi = VAE.EPP.GetValue().ToString();
            TKMD.DiaChiDonVi = VAE.EPA.GetValue().ToString();
            TKMD.SoDienThoaiDonVi = VAE.EPT.GetValue().ToString();
            TKMD.MaUyThac = VAE.EXC.GetValue().ToString();
            TKMD.TenUyThac = VAE.EXN.GetValue().ToString();
            TKMD.MaDoiTac = VAE.CGC.GetValue().ToString();
            TKMD.TenDoiTac = VAE.CGN.GetValue().ToString();
            TKMD.MaBuuChinhDoiTac = VAE.CGP.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAE.CGA.GetValue().ToString();
            TKMD.DiaChiDoiTac2 = VAE.CAT.GetValue().ToString();
            TKMD.DiaChiDoiTac3 = VAE.CAC.GetValue().ToString();
            TKMD.DiaChiDoiTac4 = VAE.CAS.GetValue().ToString();
            TKMD.MaNuocDoiTac = VAE.CGK.GetValue().ToString();
            TKMD.MaDaiLyHQ = VAE.K32.GetValue().ToString();
            TKMD.TenDaiLyHaiQuan = VAE.K33.GetValue().ToString();
            TKMD.MaNhanVienHaiQuan = VAE.K34.GetValue().ToString();
            #region  Vận đơn
            string soVanDon = VAE.EKN.GetValue().ToString();
            if (!string.IsNullOrEmpty(soVanDon))
            {
                TKMD.VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
                TKMD.VanDonCollection.Add(new KDT_VNACC_TK_SoVanDon() { SoVanDon = soVanDon });
            }
            #endregion

            TKMD.SoLuong = System.Convert.ToDecimal(VAE.NO.GetValue(), cltInfo);
            TKMD.MaDVTSoLuong = VAE.NOT.GetValue().ToString();
            TKMD.TrongLuong = System.Convert.ToDecimal(VAE.GW.GetValue(), cltInfo);
            TKMD.MaDVTTrongLuong = VAE.GWT.GetValue().ToString();
            TKMD.MaDDLuuKho = VAE.ST.GetValue().ToString();
            TKMD.TenDDLuuKho = VAE.K42.GetValue().ToString();
            TKMD.MaDiaDiemDoHang = VAE.DSC.GetValue().ToString();
            TKMD.TenDiaDiemDohang = VAE.DSN.GetValue().ToString();
            TKMD.MaDiaDiemXepHang = VAE.PSC.GetValue().ToString();
            TKMD.TenDiaDiemXepHang = VAE.PSN.GetValue().ToString();
            TKMD.MaPTVC = VAE.VSC.GetValue().ToString();
            TKMD.TenPTVC = VAE.VSN.GetValue().ToString();
            TKMD.NgayHangDen = System.Convert.ToDateTime(VAE.SYM.GetValue());
            TKMD.SoHieuKyHieu = VAE.MRK.GetValue().ToString();
            #region Giấy phép
            List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
            if (VAE.SS_.listAttribute != null && VAE.SS_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.SS_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.GiayPhepCollection == null) TKMD.GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
                    KDT_VNACC_TK_GiayPhep giayPhep = new KDT_VNACC_TK_GiayPhep();
                    giayPhep.PhanLoai = VAE.SS_.listAttribute[0].GetValueCollection(i).ToString();
                    giayPhep.SoGiayPhep = VAE.SS_.listAttribute[1].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(giayPhep.SoGiayPhep))
                        listGiayPhep.Add(giayPhep);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_GiayPhep>(TKMD.GiayPhepCollection, listGiayPhep);
            #endregion
            TKMD.PhanLoaiHD = VAE.IV1.GetValue().ToString();
            TKMD.SoHoaDon = VAE.IV3.GetValue().ToString();
            TKMD.SoTiepNhanHD = System.Convert.ToDecimal(VAE.IV2.GetValue());
            TKMD.NgayPhatHanhHD = System.Convert.ToDateTime(VAE.IVD.GetValue());
            TKMD.PhuongThucTT = VAE.IVP.GetValue().ToString();
            TKMD.MaDieuKienGiaHD = VAE.IP2.GetValue().ToString();
            TKMD.MaTTHoaDon = VAE.IP3.GetValue().ToString();
            TKMD.TongTriGiaHD = System.Convert.ToDecimal(VAE.IP4.GetValue(), cltInfo);
            TKMD.PhanLoaiGiaHD = VAE.IP1.GetValue().ToString();
            TKMD.MaTTTriGiaTinhThue = VAE.FCD.GetValue().ToString();
            TKMD.TriGiaTinhThue = System.Convert.ToDecimal(VAE.FKK.GetValue(), cltInfo);
            #region Tỷ giá
            List<KDT_VNACC_TK_PhanHoi_TyGia> lTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
            if (VAE.RC_.listAttribute != null && VAE.RC_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.RC_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TyGiaCollection == null) TKMD.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    KDT_VNACC_TK_PhanHoi_TyGia tygia = new KDT_VNACC_TK_PhanHoi_TyGia();
                    tygia.MaTTTyGiaTinhThue = VAE.RC_.listAttribute[0].GetValueCollection(i).ToString();
                    tygia.TyGiaTinhThue = Convert.ToDecimal(VAE.RC_.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(tygia.MaTTTyGiaTinhThue))
                    {
                        lTyGia.Add(tygia);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_PhanHoi_TyGia>(TKMD.TyGiaCollection, lTyGia);
            #endregion
            //TKMD.TongHeSoPhanBoTG = System.Convert.ToDecimal(VAE.TP.GetValue(), cltInfo);
            TKMD.MaPhanLoaiTongGiaCoBan = VAE.K68.GetValue().ToString();
            TKMD.PhanLoaiKhongQDVND = VAE.CNV.GetValue().ToString();
            TKMD.NguoiNopThue = VAE.TPM.GetValue().ToString();
            TKMD.MaXDThoiHanNopThue = VAE.ENC.GetValue().ToString();
            TKMD.PhanLoaiNopThue = VAE.PAY.GetValue().ToString();
            TKMD.TongSoTienThueXuatKhau = System.Convert.ToDecimal(VAE.ETA.GetValue(), cltInfo);
            TKMD.MaTTTongTienThueXuatKhau = VAE.AD7.GetValue().ToString();
            TKMD.TongSoTienLePhi = System.Convert.ToDecimal(VAE.TCO.GetValue(), cltInfo);
            TKMD.SoTienBaoLanh = System.Convert.ToDecimal(VAE.SAM.GetValue(), cltInfo);
            TKMD.MaTTCuaSoTienBaoLanh = VAE.AD8.GetValue().ToString();
            TKMD.TongSoTrangCuaToKhai = System.Convert.ToDecimal(VAE.K69.GetValue(), cltInfo);
            TKMD.TongSoDongHangCuaToKhai = System.Convert.ToDecimal(VAE.K70.GetValue(), cltInfo);
            #region Phân loại đính kèm khai báo điện tử
            List<KDT_VNACC_TK_DinhKemDienTu> lDinhKem = new List<KDT_VNACC_TK_DinhKemDienTu>();
            if (VAE.EA_.listAttribute != null && VAE.EA_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.EA_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.DinhKemCollection == null) TKMD.DinhKemCollection = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    KDT_VNACC_TK_DinhKemDienTu dinhkem = new KDT_VNACC_TK_DinhKemDienTu();
                    dinhkem.PhanLoai = VAE.EA_.listAttribute[0].GetValueCollection(i).ToString();
                    dinhkem.SoDinhKemKhaiBaoDT = System.Convert.ToDecimal(VAE.EA_.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(dinhkem.PhanLoai) || dinhkem.SoDinhKemKhaiBaoDT > 0)
                    {
                        lDinhKem.Add(dinhkem);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_DinhKemDienTu>(TKMD.DinhKemCollection, lDinhKem);
            #endregion
            TKMD.GhiChu = VAE.NT2.GetValue().ToString();
            TKMD.SoQuanLyNoiBoDN = VAE.REF.GetValue().ToString();
            TKMD.SoQuanLyNguoiSuDung = VAE.K82.GetValue().ToString();
            TKMD.NgayKhoiHanhVC = System.Convert.ToDateTime(VAE.DPD.GetValue());
            #region Trung chuyen
            List<KDT_VNACC_TK_TrungChuyen> ltrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
            if (VAE.ST_.listAttribute != null && VAE.ST_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.ST_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TrungChuyenCollection == null) TKMD.TrungChuyenCollection = new List<KDT_VNACC_TK_TrungChuyen>();
                    KDT_VNACC_TK_TrungChuyen trungchuyen = new KDT_VNACC_TK_TrungChuyen();
                    trungchuyen.MaDiaDiem = VAE.ST_.listAttribute[0].GetValueCollection(i).ToString();
                    trungchuyen.NgayDen = System.Convert.ToDateTime(VAE.ST_.listAttribute[1].GetValueCollection(i));
                    trungchuyen.NgayKhoiHanh = System.Convert.ToDateTime(VAE.ST_.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(trungchuyen.MaDiaDiem))
                    {
                        ltrungChuyen.Add(trungchuyen);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_TrungChuyen>(TKMD.TrungChuyenCollection, ltrungChuyen);
            #endregion
            TKMD.DiaDiemDichVC = VAE.ARP.GetValue().ToString();
            TKMD.NgayDen = System.Convert.ToDateTime(VAE.ADT.GetValue());
            #region  Container

            if (TKMD.ContainerTKMD == null) TKMD.ContainerTKMD = new KDT_VNACC_TK_Container();
            // Mã địa điểm xếp lên xe chở hàng trong vận đơn
            if (VAE.VC_.listAttribute != null && VAE.VC_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.VC_.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            TKMD.ContainerTKMD.MaDiaDiem1 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            TKMD.ContainerTKMD.MaDiaDiem2 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            TKMD.ContainerTKMD.MaDiaDiem3 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            TKMD.ContainerTKMD.MaDiaDiem4 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            TKMD.ContainerTKMD.MaDiaDiem5 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            TKMD.ContainerTKMD.TenDiaDiem = VAE.VN.GetValue().ToString();
            TKMD.ContainerTKMD.DiaChiDiaDiem = VAE.K72.GetValue().ToString();
            List<KDT_VNACC_TK_SoContainer> listCont = new List<KDT_VNACC_TK_SoContainer>();

            if (VAE.C__.listAttribute != null && VAE.C__.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.C__.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ContainerTKMD.SoContainerCollection == null) TKMD.ContainerTKMD.SoContainerCollection = new List<KDT_VNACC_TK_SoContainer>();
                    KDT_VNACC_TK_SoContainer SoCont = new KDT_VNACC_TK_SoContainer();
                    SoCont.SoTT = i + 1;
                    SoCont.SoContainer = VAE.C__.listAttribute[0].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(SoCont.SoContainer))
                        listCont.Add(SoCont);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_SoContainer>(TKMD.ContainerTKMD.SoContainerCollection, listCont);
            #endregion

            #region Chỉ thị HQ
            List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
            if (VAE.D__.listAttribute != null && VAE.D__.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.D__.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ChiThiHQCollection == null) TKMD.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                    ChiThiHQ.PhanLoai = VAE.CCM.GetValue().ToString();
                    ChiThiHQ.PhanLoai = VAE.CCM.GetValue().ToString();
                    ChiThiHQ.Ngay = Convert.ToDateTime(VAE.D__.listAttribute[0].GetValueCollection(i));
                    ChiThiHQ.Ten = VAE.D__.listAttribute[1].GetValueCollection(i).ToString();
                    ChiThiHQ.NoiDung = VAE.D__.listAttribute[2].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(ChiThiHQ.Ten) || !string.IsNullOrEmpty(ChiThiHQ.NoiDung))
                        lChiThi.Add(ChiThiHQ);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_ChiThiHaiQuan>(TKMD.ChiThiHQCollection, lChiThi);
            #endregion

            if (TKMD.HangCollection != null)
            {
                if (TKMD.HangCollection.Count != VAE.HangMD.Count)
                {
                    KDT_VNACC_HangMauDich.DeleteCollection(TKMD.HangCollection);
                    TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                }
            }
            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {

                TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();

                for (int i = 0; i < VAE.HangMD.Count; i++)
                {
                    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        hmd.tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        hmd.tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    hmd = GetHMDFromVAE_Hang(VAE.HangMD[i], hmd);
                    if (hmd != null)
                        TKMD.HangCollection.Add(hmd);

                }
            }
            else
            {
                //            if (TKMD.HangCollection == null) TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();

                for (int i = 0; i < VAE.HangMD.Count; i++)
                {
                    if (i < TKMD.HangCollection.Count)
                    {
                        //minhnd Check to khai nhập-xuất
                        if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                            TKMD.HangCollection[i].tkxuat = false;
                        else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                            TKMD.HangCollection[i].tkxuat = true;
                        //minhnd Check to khai nhập-xuất
                        TKMD.HangCollection[i] = GetHMDFromVAE_Hang(VAE.HangMD[i], TKMD.HangCollection[i]);
                    }
                }
            }
            return TKMD;
        }

        public static KDT_VNACC_HangMauDich GetHMDFromVAE_Hang(VAE1LD0_HANG VAE, KDT_VNACC_HangMauDich hmd)
        {
            hmd.SoDong = System.Convert.ToDecimal(VAE.R01.GetValue()).ToString();
            hmd.MaSoHang = VAE.CMD.GetValue().ToString();
            hmd.MaQuanLy = VAE.COC.GetValue().ToString();
            hmd.MaPhanLoaiTaiXacNhanGia = VAE.R03.GetValue().ToString();
            hmd.MaHangHoaKhaiBao = VAE.CMN.GetValue().ToString();
            hmd.SoLuong1 = System.Convert.ToDecimal(VAE.QN1.GetValue(), cltInfo);
            hmd.DVTLuong1 = VAE.QT1.GetValue().ToString();
            hmd.SoLuong2 = System.Convert.ToDecimal(VAE.QN2.GetValue(), cltInfo);
            hmd.DVTLuong2 = VAE.QT2.GetValue().ToString();
            hmd.TriGiaHoaDon = System.Convert.ToDecimal(VAE.BPR.GetValue(), cltInfo);
            hmd.DonGiaHoaDon = System.Convert.ToDecimal(VAE.UPR.GetValue(), cltInfo);
            hmd.MaTTDonGia = VAE.UPC.GetValue().ToString();
            hmd.DVTDonGia = VAE.TSC.GetValue().ToString();
            hmd.TriGiaTinhThueS = System.Convert.ToDecimal(VAE.R07.GetValue(), cltInfo);
            hmd.MaTTTriGiaTinhThueS = VAE.AD9.GetValue().ToString();
            hmd.MaTTTriGiaTinhThue = VAE.R14.GetValue().ToString();
            hmd.TriGiaTinhThue = System.Convert.ToDecimal(VAE.R15.GetValue(), cltInfo);
            hmd.SoLuongTinhThue = System.Convert.ToDecimal(VAE.TSQ.GetValue(), cltInfo);
            hmd.MaDVTDanhThue = VAE.TSU.GetValue().ToString();
            hmd.DonGiaTinhThue = System.Convert.ToDecimal(VAE.CVU.GetValue(), cltInfo);
            hmd.MaTTDonGiaTinhThue = VAE.ADA.GetValue().ToString();
            hmd.DV_SL_TrongDonGiaTinhThue = VAE.QCV.GetValue().ToString();
            hmd.ThueSuatThue = VAE.TRA.GetValue().ToString().Replace("%","");
            //hmd.PhanLoaiThueSuatThue = VAE.TRM.GetValue().ToString();
            hmd.SoTienThue = System.Convert.ToDecimal(VAE.TAX.GetValue(), cltInfo);
            hmd.MaTTSoTienThueXuatKhau = VAE.ADB.GetValue().ToString();
            hmd.SoTienMienGiam = System.Convert.ToDecimal(VAE.REG.GetValue(), cltInfo);
            hmd.MaTTSoTienMienGiam = VAE.ADC.GetValue().ToString();
            hmd.SoTTDongHangTKTNTX = VAE.TDL.GetValue().ToString();
            hmd.SoDMMienThue = VAE.TXN.GetValue().ToString();
            hmd.SoDongDMMienThue = VAE.TXR.GetValue().ToString();
            hmd.TienLePhi_DonGia = VAE.CUP.GetValue().ToString();
            hmd.TienBaoHiem_DonGia = VAE.IUP.GetValue().ToString();
            hmd.TienLePhi_SoLuong = System.Convert.ToDecimal(VAE.CQU.GetValue(), cltInfo);
            hmd.TienLePhi_MaDVSoLuong = VAE.CQC.GetValue().ToString();
            hmd.TienBaoHiem_SoLuong = System.Convert.ToDecimal(VAE.IQU.GetValue(), cltInfo);
            hmd.TienBaoHiem_MaDVSoLuong = VAE.IQC.GetValue().ToString();
            hmd.TienLePhi_KhoanTien = System.Convert.ToDecimal(VAE.CPR.GetValue(), cltInfo);
            hmd.TienBaoHiem_KhoanTien = System.Convert.ToDecimal(VAE.IPR.GetValue(), cltInfo);
            if (VAE.OL_.listAttribute != null && VAE.OL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.OL_.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            hmd.MaVanBanPhapQuyKhac1 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            hmd.MaVanBanPhapQuyKhac2 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            hmd.MaVanBanPhapQuyKhac3 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            hmd.MaVanBanPhapQuyKhac4 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            hmd.MaVanBanPhapQuyKhac5 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            hmd.MaMienGiam = VAE.RE.GetValue().ToString();
            hmd.DieuKhoanMienGiam = VAE.TRL.GetValue().ToString();

            if (hmd.MaSoHang != null && !string.IsNullOrEmpty(hmd.MaSoHang))
                return hmd;
            else
                return null;
        }


        public static KDT_VNACC_ToKhaiMauDich GetTKMDFromVAE4000(VAE4000 VAE, KDT_VNACC_ToKhaiMauDich TKMD)
        {
            TKMD.SoToKhai = System.Convert.ToDecimal(VAE.ECN.GetValue());
            //TKMD.PhanLoaiBaoCaoSuaDoi = VAE.JKN.GetValue().ToString();
            TKMD.SoToKhaiDauTien = VAE.FIC.GetValue().ToString();
            TKMD.SoNhanhToKhai = System.Convert.ToDecimal(VAE.BNO.GetValue());
            TKMD.TongSoTKChiaNho = System.Convert.ToDecimal(VAE.DNO.GetValue());
            TKMD.SoToKhaiTNTX = System.Convert.ToDecimal(VAE.TDN.GetValue());
            //TKMD.MaPhanLoaiKiemTra = VAE.K07.GetValue().ToString();
            TKMD.MaLoaiHinh = VAE.ECB.GetValue().ToString();
            TKMD.MaPhanLoaiHH = VAE.CCC.GetValue().ToString();
            TKMD.MaPhuongThucVT = VAE.MTC.GetValue().ToString();
            //TKMD.CoQuanHaiQuan = VAE.CH.GetValue().ToString();
            TKMD.NhomXuLyHS = VAE.CHB.GetValue().ToString();
            //TKMD.MaSoThueDaiDien = VAE.K01.GetValue().ToString();
            //TKMD.TenCoQuanHaiQuan = VAE.K08.GetValue().ToString();
            TKMD.NhomXuLyHS = VAE.CHB.GetValue().ToString();
            //TKMD.NgayDangKy = System.Convert.ToDateTime(VAE.K10.GetValue());
            //TKMD.NgayDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayDangKy, VAE.AD1.GetValue().ToString());
            //TKMD.NgayThayDoiDangKy = System.Convert.ToDateTime(VAE.AD2.GetValue());
            //TKMD.NgayThayDoiDangKy = HelperVNACCS.AddHoursToDateTime(TKMD.NgayThayDoiDangKy, VAE.AD3.GetValue().ToString());
            TKMD.ThoiHanTaiNhapTaiXuat = System.Convert.ToDateTime(VAE.RID.GetValue());
            //TKMD.BieuThiTruongHopHetHan = VAE.AAA.GetValue().ToString();
            TKMD.MaDonVi = VAE.EPC.GetValue().ToString();
            TKMD.TenDonVi = VAE.EPN.GetValue().ToString();
            TKMD.MaBuuChinhDonVi = VAE.EPP.GetValue().ToString();
            TKMD.DiaChiDonVi = VAE.EPA.GetValue().ToString();
            TKMD.SoDienThoaiDonVi = VAE.EPT.GetValue().ToString();
            TKMD.MaUyThac = VAE.EXC.GetValue().ToString();
            TKMD.TenUyThac = VAE.EXN.GetValue().ToString();
            TKMD.MaDoiTac = VAE.CGC.GetValue().ToString();
            TKMD.TenDoiTac = VAE.CGN.GetValue().ToString();
            TKMD.MaBuuChinhDoiTac = VAE.CGP.GetValue().ToString();
            TKMD.DiaChiDoiTac1 = VAE.CGA.GetValue().ToString();
            TKMD.DiaChiDoiTac2 = VAE.CAT.GetValue().ToString();
            TKMD.DiaChiDoiTac3 = VAE.CAC.GetValue().ToString();
            TKMD.DiaChiDoiTac4 = VAE.CAS.GetValue().ToString();
            TKMD.MaNuocDoiTac = VAE.CGK.GetValue().ToString();
            TKMD.MaDaiLyHQ = VAE.ECC.GetValue().ToString();
            //TKMD.TenDaiLyHaiQuan = VAE.K33.GetValue().ToString();
            //TKMD.MaNhanVienHaiQuan = VAE.K34.GetValue().ToString();
            #region  Vận đơn
            string soVanDon = VAE.EKN.GetValue().ToString();
            if (!string.IsNullOrEmpty(soVanDon))
            {
                TKMD.VanDonCollection = new List<KDT_VNACC_TK_SoVanDon>();
                TKMD.VanDonCollection.Add(new KDT_VNACC_TK_SoVanDon() { SoVanDon = soVanDon });
            }
            #endregion

            TKMD.SoLuong = System.Convert.ToDecimal(VAE.NO.GetValue(), cltInfo);
            TKMD.MaDVTSoLuong = VAE.NOT.GetValue().ToString();
            TKMD.TrongLuong = System.Convert.ToDecimal(VAE.GW.GetValue(), cltInfo);
            TKMD.MaDVTTrongLuong = VAE.GWT.GetValue().ToString();
            TKMD.MaDDLuuKho = VAE.ST.GetValue().ToString();
            //TKMD.TenDDLuuKho = VAE.K42.GetValue().ToString();
            TKMD.MaDiaDiemDoHang = VAE.DSC.GetValue().ToString();
            TKMD.TenDiaDiemDohang = VAE.DSN.GetValue().ToString();
            TKMD.MaDiaDiemXepHang = VAE.PSC.GetValue().ToString();
            TKMD.TenDiaDiemXepHang = VAE.PSN.GetValue().ToString();
            TKMD.MaPTVC = VAE.VSC.GetValue().ToString();
            TKMD.TenPTVC = VAE.VSN.GetValue().ToString();
            TKMD.NgayHangDen = System.Convert.ToDateTime(VAE.SYM.GetValue());
            TKMD.SoHieuKyHieu = VAE.MRK.GetValue().ToString();
            #region Giấy phép
            List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
            if (VAE.SS_.listAttribute != null && VAE.SS_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.SS_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.GiayPhepCollection == null) TKMD.GiayPhepCollection = new List<KDT_VNACC_TK_GiayPhep>();
                    KDT_VNACC_TK_GiayPhep giayPhep = new KDT_VNACC_TK_GiayPhep();
                    giayPhep.PhanLoai = VAE.SS_.listAttribute[0].GetValueCollection(i).ToString();
                    giayPhep.SoGiayPhep = VAE.SS_.listAttribute[1].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(giayPhep.SoGiayPhep))
                        listGiayPhep.Add(giayPhep);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_GiayPhep>(TKMD.GiayPhepCollection, listGiayPhep);
            #endregion
            TKMD.PhanLoaiHD = VAE.IV1.GetValue().ToString();
            TKMD.SoHoaDon = VAE.IV3.GetValue().ToString();
            TKMD.SoTiepNhanHD = System.Convert.ToDecimal(VAE.IV2.GetValue());
            TKMD.NgayPhatHanhHD = System.Convert.ToDateTime(VAE.IVD.GetValue());
            TKMD.PhuongThucTT = VAE.IVP.GetValue().ToString();
            TKMD.MaDieuKienGiaHD = VAE.IP2.GetValue().ToString();
            TKMD.MaTTHoaDon = VAE.IP3.GetValue().ToString();
            TKMD.TongTriGiaHD = System.Convert.ToDecimal(VAE.IP4.GetValue(), cltInfo);
            TKMD.PhanLoaiGiaHD = VAE.IP1.GetValue().ToString();
            TKMD.MaTTTriGiaTinhThue = VAE.FCD.GetValue().ToString();
            TKMD.TriGiaTinhThue = System.Convert.ToDecimal(VAE.FKK.GetValue(), cltInfo);

            //TKMD.TongHeSoPhanBoTG = System.Convert.ToDecimal(VAE.TP.GetValue(), cltInfo);
            //TKMD.MaPhanLoaiTongGiaCoBan = VAE.K68.GetValue().ToString();
            TKMD.PhanLoaiKhongQDVND = VAE.CNV.GetValue().ToString();
            TKMD.NguoiNopThue = VAE.TPM.GetValue().ToString();
            TKMD.MaXDThoiHanNopThue = VAE.ENC.GetValue().ToString();
            //TKMD.PhanLoaiNopThue = VAE.PAY.GetValue().ToString();
            //TKMD.TongSoTienThueXuatKhau = System.Convert.ToDecimal(VAE.ETA.GetValue());
            //TKMD.MaTTTongTienThueXuatKhau = VAE.AD7.GetValue().ToString();
            //TKMD.TongSoTienLePhi = System.Convert.ToDecimal(VAE.TCO.GetValue());
            //TKMD.SoTienBaoLanh = System.Convert.ToDecimal(VAE.SAM.GetValue());
            //TKMD.MaTTCuaSoTienBaoLanh = VAE.AD8.GetValue().ToString();
            //TKMD.TongSoTrangCuaToKhai = System.Convert.ToDecimal(VAE.K69.GetValue());
            //TKMD.TongSoDongHangCuaToKhai = System.Convert.ToDecimal(VAE.K70.GetValue());
            #region Phân loại đính kèm khai báo điện tử
            List<KDT_VNACC_TK_DinhKemDienTu> lDinhKem = new List<KDT_VNACC_TK_DinhKemDienTu>();
            if (VAE.EA_.listAttribute != null && VAE.EA_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.EA_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.DinhKemCollection == null) TKMD.DinhKemCollection = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    KDT_VNACC_TK_DinhKemDienTu dinhkem = new KDT_VNACC_TK_DinhKemDienTu();
                    dinhkem.PhanLoai = VAE.EA_.listAttribute[0].GetValueCollection(i).ToString();
                    dinhkem.SoDinhKemKhaiBaoDT = System.Convert.ToDecimal(VAE.EA_.listAttribute[1].GetValueCollection(i), cltInfo);
                    if (!string.IsNullOrEmpty(dinhkem.PhanLoai) || dinhkem.SoDinhKemKhaiBaoDT > 0)
                    {
                        lDinhKem.Add(dinhkem);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_DinhKemDienTu>(TKMD.DinhKemCollection, lDinhKem);
            #endregion
            TKMD.GhiChu = VAE.NT2.GetValue().ToString();
            TKMD.SoQuanLyNoiBoDN = VAE.REF.GetValue().ToString();
            //TKMD.SoQuanLyNguoiSuDung = VAE.K82.GetValue().ToString();
            TKMD.NgayKhoiHanhVC = System.Convert.ToDateTime(VAE.DPD.GetValue());
            #region Trung chuyen
            List<KDT_VNACC_TK_TrungChuyen> ltrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
            if (VAE.ST_.listAttribute != null && VAE.ST_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.ST_.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.TrungChuyenCollection == null) TKMD.TrungChuyenCollection = new List<KDT_VNACC_TK_TrungChuyen>();
                    KDT_VNACC_TK_TrungChuyen trungchuyen = new KDT_VNACC_TK_TrungChuyen();
                    trungchuyen.MaDiaDiem = VAE.ST_.listAttribute[0].GetValueCollection(i).ToString();
                    trungchuyen.NgayDen = System.Convert.ToDateTime(VAE.ST_.listAttribute[1].GetValueCollection(i));
                    trungchuyen.NgayKhoiHanh = System.Convert.ToDateTime(VAE.ST_.listAttribute[2].GetValueCollection(i));
                    if (!string.IsNullOrEmpty(trungchuyen.MaDiaDiem))
                    {
                        ltrungChuyen.Add(trungchuyen);
                    }
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_TrungChuyen>(TKMD.TrungChuyenCollection, ltrungChuyen);
            #endregion
            TKMD.DiaDiemDichVC = VAE.ARP.GetValue().ToString();
            TKMD.NgayDen = System.Convert.ToDateTime(VAE.ADT.GetValue());
            #region  Container

            if (TKMD.ContainerTKMD == null) TKMD.ContainerTKMD = new KDT_VNACC_TK_Container();
            // Mã địa điểm xếp lên xe chở hàng trong vận đơn
            if (VAE.VC_.listAttribute != null && VAE.VC_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.VC_.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            TKMD.ContainerTKMD.MaDiaDiem1 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            TKMD.ContainerTKMD.MaDiaDiem2 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            TKMD.ContainerTKMD.MaDiaDiem3 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            TKMD.ContainerTKMD.MaDiaDiem4 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            TKMD.ContainerTKMD.MaDiaDiem5 = VAE.VC_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            TKMD.ContainerTKMD.TenDiaDiem = VAE.VN.GetValue().ToString();
            TKMD.ContainerTKMD.DiaChiDiaDiem = VAE.VA.GetValue().ToString();
            List<KDT_VNACC_TK_SoContainer> listCont = new List<KDT_VNACC_TK_SoContainer>();

            if (VAE.C__.listAttribute != null && VAE.C__.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.C__.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ContainerTKMD.SoContainerCollection == null) TKMD.ContainerTKMD.SoContainerCollection = new List<KDT_VNACC_TK_SoContainer>();
                    KDT_VNACC_TK_SoContainer SoCont = new KDT_VNACC_TK_SoContainer();
                    SoCont.SoTT = i + 1;
                    SoCont.SoContainer = VAE.C__.listAttribute[0].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(SoCont.SoContainer))
                        listCont.Add(SoCont);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TK_SoContainer>(TKMD.ContainerTKMD.SoContainerCollection, listCont);
            #endregion

            #region Chỉ thị HQ
            List<KDT_VNACC_ChiThiHaiQuan> lChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
            if (VAE.D__.listAttribute != null && VAE.D__.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.D__.listAttribute[0].ListValue.Count; i++)
                {
                    if (TKMD.ChiThiHQCollection == null) TKMD.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    KDT_VNACC_ChiThiHaiQuan ChiThiHQ = new KDT_VNACC_ChiThiHaiQuan();
                    ChiThiHQ.PhanLoai = VAE.CCM.GetValue().ToString();
                    ChiThiHQ.PhanLoai = VAE.CCM.GetValue().ToString();
                    ChiThiHQ.Ngay = Convert.ToDateTime(VAE.D__.listAttribute[0].GetValueCollection(i));
                    ChiThiHQ.Ten = VAE.D__.listAttribute[1].GetValueCollection(i).ToString();
                    ChiThiHQ.NoiDung = VAE.D__.listAttribute[2].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(ChiThiHQ.Ten) || !string.IsNullOrEmpty(ChiThiHQ.NoiDung))
                        lChiThi.Add(ChiThiHQ);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_ChiThiHaiQuan>(TKMD.ChiThiHQCollection, lChiThi);
            #endregion

            if (TKMD.HangCollection != null)
            {
                if (TKMD.HangCollection.Count != VAE.HangMD.Count)
                {
                    KDT_VNACC_HangMauDich.DeleteCollection(TKMD.HangCollection);
                    TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();
                }
            }
            if (TKMD.HangCollection == null || TKMD.HangCollection.Count == 0)
            {

                TKMD.HangCollection = new List<KDT_VNACC_HangMauDich>();

                for (int i = 0; i < VAE.HangMD.Count; i++)
                {
                    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //minhnd Check to khai nhập-xuất
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                        hmd.tkxuat = false;
                    else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                        hmd.tkxuat = true;
                    //minhnd Check to khai nhập-xuất
                    hmd = GetHMDFromVAE4000_Hang(VAE.HangMD[i], hmd);
                    if (hmd != null)
                        TKMD.HangCollection.Add(hmd);

                }
            }
            else
            {
                for (int i = 0; i < VAE.HangMD.Count; i++)
                {
                    if (i < TKMD.HangCollection.Count)
                    {
                        //minhnd Check to khai nhập-xuất
                        if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                            TKMD.HangCollection[i].tkxuat = false;
                        else if (TKMD.SoToKhai.ToString().Substring(0, 1) == "3")
                            TKMD.HangCollection[i].tkxuat = true;
                        //minhnd Check to khai nhập-xuất
                        TKMD.HangCollection[i] = GetHMDFromVAE4000_Hang(VAE.HangMD[i], TKMD.HangCollection[i]);
                    }
                }
            }

            return TKMD;
        }

        public static KDT_VNACC_HangMauDich GetHMDFromVAE4000_Hang(VAE4000_HANG VAE, KDT_VNACC_HangMauDich hmd)
        {
            //hmd.SoDong = System.Convert.ToDecimal(VAE.R01.GetValue()).ToString();
            hmd.MaSoHang = VAE.CMD.GetValue().ToString();
            hmd.MaQuanLy = VAE.COC.GetValue().ToString();
            //hmd.MaPhanLoaiTaiXacNhanGia = VAE.R03.GetValue().ToString();
            hmd.MaHangHoaKhaiBao = VAE.CMN.GetValue().ToString();
            hmd.SoLuong1 = System.Convert.ToDecimal(VAE.QN1.GetValue(), cltInfo);
            hmd.DVTLuong1 = VAE.QT1.GetValue().ToString();
            hmd.SoLuong2 = System.Convert.ToDecimal(VAE.QN2.GetValue(), cltInfo);
            hmd.DVTLuong2 = VAE.QT2.GetValue().ToString();
            hmd.TriGiaHoaDon = System.Convert.ToDecimal(VAE.BPR.GetValue(), cltInfo);
            hmd.DonGiaHoaDon = System.Convert.ToDecimal(VAE.UPR.GetValue(), cltInfo);
            hmd.MaTTDonGia = VAE.UPC.GetValue().ToString();
            hmd.DVTDonGia = VAE.TSC.GetValue().ToString();
            //hmd.TriGiaTinhThueS = System.Convert.ToDecimal(VAE.R07.GetValue());
            //hmd.MaTTTriGiaTinhThueS = VAE.AD9.GetValue().ToString();
            //hmd.MaTTTriGiaTinhThue = VAE.R14.GetValue().ToString();
            //hmd.TriGiaTinhThue = System.Convert.ToDecimal(VAE.R15.GetValue());
            //hmd.SoLuongTinhThue = System.Convert.ToDecimal(VAE.TSQ.GetValue());
            //             hmd.MaDVTDanhThue = VAE.TSU.GetValue().ToString();
            //             hmd.DonGiaTinhThue = System.Convert.ToDecimal(VAE.CVU.GetValue());
            //             hmd.MaTTDonGiaTinhThue = VAE.ADA.GetValue().ToString();
            //             hmd.DV_SL_TrongDonGiaTinhThue = VAE.QCV.GetValue().ToString();
            //             hmd.ThueSuatThue = VAE.TRA.GetValue().ToString();
            //             hmd.PhanLoaiThueSuatThue = VAE.TRM.GetValue().ToString();
            //             hmd.SoTienThue = System.Convert.ToDecimal(VAE.TAX.GetValue());
            //             hmd.MaTTSoTienThueXuatKhau = VAE.ADB.GetValue().ToString();
            hmd.SoTienMienGiam = System.Convert.ToDecimal(VAE.REG.GetValue(), cltInfo);
            //  hmd.MaTTSoTienMienGiam = VAE.ADC.GetValue().ToString();
            hmd.SoTTDongHangTKTNTX = VAE.TDL.GetValue().ToString();
            hmd.SoDMMienThue = VAE.TXN.GetValue().ToString();
            hmd.SoDongDMMienThue = VAE.TXR.GetValue().ToString();
            //   hmd.TienLePhi_DonGia = VAE.CUP.GetValue().ToString();
            //   hmd.TienBaoHiem_DonGia = VAE.IUP.GetValue().ToString();
            //   hmd.TienLePhi_SoLuong = System.Convert.ToDecimal(VAE.CQU.GetValue());
            //   hmd.TienLePhi_MaDVSoLuong = VAE.CQC.GetValue().ToString();
            //   hmd.TienBaoHiem_SoLuong = System.Convert.ToDecimal(VAE.IQU.GetValue());
            //  hmd.TienBaoHiem_MaDVSoLuong = VAE.IQC.GetValue().ToString();
            //  hmd.TienLePhi_KhoanTien = System.Convert.ToDecimal(VAE.CPR.GetValue());
            //  hmd.TienBaoHiem_KhoanTien = System.Convert.ToDecimal(VAE.IPR.GetValue());
            if (VAE.OL_.listAttribute != null && VAE.OL_.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAE.OL_.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            hmd.MaVanBanPhapQuyKhac1 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            hmd.MaVanBanPhapQuyKhac2 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            hmd.MaVanBanPhapQuyKhac3 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            hmd.MaVanBanPhapQuyKhac4 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            hmd.MaVanBanPhapQuyKhac5 = VAE.OL_.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            hmd.MaMienGiam = VAE.RE.GetValue().ToString();
            hmd.ThueSuat = System.Convert.ToDecimal(VAE.TXA.GetValue(), cltInfo);
            hmd.ThueSuatTuyetDoi = System.Convert.ToDecimal(VAE.TXB.GetValue(), cltInfo);
            hmd.MaDVTTuyetDoi = VAE.TXC.GetValue().ToString();
            hmd.MaTTTuyetDoi = VAE.TXD.GetValue().ToString();
            hmd.TriGiaTinhThue = System.Convert.ToDecimal(VAE.DPR.GetValue(), cltInfo);
            hmd.MaTTTriGiaTinhThue = VAE.BPC.GetValue().ToString();
            //  hmd.DieuKhoanMienGiam = VAE.TRL.GetValue().ToString();

            if (hmd.MaSoHang != null && !string.IsNullOrEmpty(hmd.MaSoHang))
                return hmd;
            else
                return null;
        }
        public static KDT_VNACC_ToKhaiVanChuyen GetTKVCFromVAS5050(VAS5050 VAS, KDT_VNACC_ToKhaiVanChuyen TKVC)
        {
            TKVC.TenThongTinXuat = VAS.AB.GetValue().ToString();
            TKVC.MaCoBaoNiemPhong = VAS.KA.GetValue().ToString();
            TKVC.TenCoBaoNiemPhong = VAS.KB.GetValue().ToString();
            //TKVC.CoQuanHaiQuan = VAS.AD.GetValue().ToString();
            TKVC.CoBaoXuatNhapKhau = VAS.ED.GetValue().ToString();
            TKVC.SoToKhaiVC = System.Convert.ToDecimal(VAS.AE.GetValue());
            TKVC.MaNguoiVC = VAS.AL.GetValue().ToString();
            TKVC.TenNguoiVC = VAS.AM.GetValue().ToString();
            TKVC.DiaChiNguoiVC = VAS.AN.GetValue().ToString();
            TKVC.SoHopDongVC = VAS.AO.GetValue().ToString();
            TKVC.NgayHopDongVC = System.Convert.ToDateTime(VAS.AP.GetValue());
            TKVC.NgayHetHanHopDongVC = System.Convert.ToDateTime(VAS.AQ.GetValue());
            TKVC.MaPhuongTienVC = VAS.AR.GetValue().ToString();
            TKVC.TenPhuongTieVC = VAS.AS.GetValue().ToString();
            TKVC.MaMucDichVC = VAS.AT.GetValue().ToString();
            TKVC.TenMucDichVC = VAS.AU.GetValue().ToString();
            TKVC.LoaiHinhVanTai = VAS.AV.GetValue().ToString();
            TKVC.TenLoaiHinhVanTai = VAS.AW.GetValue().ToString();
            TKVC.MaDiaDiemXepHang = VAS.AX.GetValue().ToString();
            TKVC.MaViTriXepHang = VAS.AY.GetValue().ToString();
            TKVC.MaCangCuaKhauGaXepHang = VAS.AZ.GetValue().ToString();
            TKVC.MaCangXHKhongCo_HT = VAS.BA.GetValue().ToString();
            TKVC.DiaDiemXepHang = VAS.ZJ.GetValue().ToString();
            TKVC.NgayDenDiaDiem_XH = System.Convert.ToDateTime(VAS.KE.GetValue().ToString());


            if (TKVC.TrungChuyenCollection == null) TKVC.TrungChuyenCollection = new List<KDT_VNACC_TKVC_TrungChuyen>();
            List<KDT_VNACC_TKVC_TrungChuyen> ListTrungChuyen = new List<KDT_VNACC_TKVC_TrungChuyen>();
            if (VAS.KC.listAttribute != null && VAS.KC.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAS.KC.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TKVC_TrungChuyen trungChuyen = new KDT_VNACC_TKVC_TrungChuyen();
                    trungChuyen.MaDiaDiemTrungChuyen = VAS.KC.listAttribute[0].GetValueCollection(i).ToString();
                    trungChuyen.TenDiaDiemTrungChuyen = VAS.KC.listAttribute[1].GetValueCollection(i).ToString();
                    trungChuyen.NgayDenDiaDiem_TC = System.Convert.ToDateTime(VAS.KC.listAttribute[2].GetValueCollection(i).ToString());
                    trungChuyen.NgayDiDiaDiem_TC = System.Convert.ToDateTime(VAS.KC.listAttribute[3].GetValueCollection(i).ToString());

                    ListTrungChuyen.Add(trungChuyen);
                }
            }

            TKVC.MaDiaDiemDoHang = VAS.BC.GetValue().ToString();
            TKVC.MaViTriDoHang = VAS.BD.GetValue().ToString();
            TKVC.MaCangCuaKhauGaDoHang = VAS.BE.GetValue().ToString();
            TKVC.MaCangDHKhongCo_HT = VAS.BF.GetValue().ToString();
            TKVC.DiaDiemDoHang = VAS.BG.GetValue().ToString();
            TKVC.NgayDenDiaDiem_DH = System.Convert.ToDateTime(VAS.KH.GetValue().ToString());
            TKVC.TuyenDuongVC = VAS.BH.GetValue().ToString();
            TKVC.LoaiBaoLanh = VAS.FA.GetValue().ToString();
            TKVC.SoTienBaoLanh = System.Convert.ToDecimal(VAS.FB.GetValue(), cltInfo);
            TKVC.SoLuongCot_TK = System.Convert.ToInt32(VAS.BI.GetValue());
            TKVC.SoLuongContainer = System.Convert.ToInt32(VAS.BJ.GetValue());
            TKVC.GhiChu = VAS.BK.GetValue().ToString();

            #region  Vận đơn
            if (TKVC.VanDonCollection == null) TKVC.VanDonCollection = new List<KDT_VNACC_TKVC_VanDon>();
            List<KDT_VNACC_TKVC_VanDon> ListVanDon = new List<KDT_VNACC_TKVC_VanDon>();
            if (VAS.DG.listAttribute != null && VAS.DG.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAS.DG.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TKVC_VanDon VanDon = new KDT_VNACC_TKVC_VanDon();
                    VanDon.SoTTVanDon = VAS.DG.listAttribute[0].GetValueCollection(i).ToString();
                    VanDon.SoVanDon = VAS.DG.listAttribute[1].GetValueCollection(i).ToString();
                    VanDon.NgayPhatHanhVD = System.Convert.ToDateTime(VAS.DG.listAttribute[2].GetValueCollection(i).ToString());
                    VanDon.MoTaHangHoa = VAS.DG.listAttribute[3].GetValueCollection(i).ToString();
                    VanDon.MaHS = VAS.DG.listAttribute[4].GetValueCollection(i).ToString();
                    VanDon.KyHieuVaSoHieu = VAS.DG.listAttribute[5].GetValueCollection(i).ToString();
                    VanDon.NgayNhapKhoHQLanDau = System.Convert.ToDateTime(VAS.DG.listAttribute[6].GetValueCollection(i).ToString());
                    VanDon.PhanLoaiSanPhan = VAS.DG.listAttribute[7].GetValueCollection(i).ToString();
                    VanDon.MaNuocSanXuat = VAS.DG.listAttribute[8].GetValueCollection(i).ToString();
                    VanDon.TenNuocSanXuat = VAS.DG.listAttribute[9].GetValueCollection(i).ToString();
                    VanDon.MaDiaDiemXuatPhatVC = VAS.DG.listAttribute[10].GetValueCollection(i).ToString();
                    VanDon.TenDiaDiemXuatPhatVC = VAS.DG.listAttribute[11].GetValueCollection(i).ToString();
                    VanDon.MaDiaDiemDichVC = VAS.DG.listAttribute[12].GetValueCollection(i).ToString();
                    VanDon.TenDiaDiemDichVC = VAS.DG.listAttribute[13].GetValueCollection(i).ToString();
                    VanDon.LoaiHangHoa = VAS.DG.listAttribute[14].GetValueCollection(i).ToString();
                    VanDon.MaPhuongTienVC = VAS.DG.listAttribute[15].GetValueCollection(i).ToString();
                    VanDon.TenPhuongTienVC = VAS.DG.listAttribute[16].GetValueCollection(i).ToString();
                    VanDon.NgayHangDuKienDenDi = System.Convert.ToDateTime(VAS.DG.listAttribute[17].GetValueCollection(i).ToString());
                    VanDon.MaNguoiNhapKhau = VAS.DG.listAttribute[18].GetValueCollection(i).ToString();
                    VanDon.TenNguoiNhapKhau = VAS.DG.listAttribute[19].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiNhapKhau = VAS.DG.listAttribute[20].GetValueCollection(i).ToString();
                    VanDon.MaNguoiXuatKhua = VAS.DG.listAttribute[21].GetValueCollection(i).ToString();
                    VanDon.TenNguoiXuatKhau = VAS.DG.listAttribute[22].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiXuatKhau = VAS.DG.listAttribute[23].GetValueCollection(i).ToString();
                    VanDon.MaNguoiUyThac = VAS.DG.listAttribute[24].GetValueCollection(i).ToString();
                    VanDon.TenNguoiUyThac = VAS.DG.listAttribute[25].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiUyThac = VAS.DG.listAttribute[26].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat1 = VAS.DG.listAttribute[27].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat2 = VAS.DG.listAttribute[28].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat3 = VAS.DG.listAttribute[29].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat4 = VAS.DG.listAttribute[30].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat5 = VAS.DG.listAttribute[31].GetValueCollection(i).ToString();
                    VanDon.MaDVTTriGia = VAS.DG.listAttribute[32].GetValueCollection(i).ToString();
                    VanDon.TriGia = System.Convert.ToDecimal(VAS.DG.listAttribute[33].GetValueCollection(i), cltInfo);
                    VanDon.SoLuong = System.Convert.ToDecimal(VAS.DG.listAttribute[34].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTSoLuong = VAS.DG.listAttribute[35].GetValueCollection(i).ToString();
                    VanDon.TongTrongLuong = System.Convert.ToDecimal(VAS.DG.listAttribute[36].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTTrongLuong = VAS.DG.listAttribute[37].GetValueCollection(i).ToString();
                    VanDon.TheTich = System.Convert.ToDecimal(VAS.DG.listAttribute[38].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTTheTich = VAS.DG.listAttribute[39].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH1 = VAS.DG.listAttribute[40].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH2 = VAS.DG.listAttribute[41].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH3 = VAS.DG.listAttribute[42].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH4 = VAS.DG.listAttribute[43].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH5 = VAS.DG.listAttribute[44].GetValueCollection(i).ToString();
                    VanDon.SoGiayPhep = VAS.DG.listAttribute[45].GetValueCollection(i).ToString();
                    VanDon.NgayCapPhep = System.Convert.ToDateTime(VAS.DG.listAttribute[46].GetValueCollection(i).ToString());
                    VanDon.GhiChu = VAS.DG.listAttribute[47].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(VanDon.SoVanDon.Trim()))
                        ListVanDon.Add(VanDon);
                }


            }
            HelperVNACCS.UpdateList<KDT_VNACC_TKVC_VanDon>(TKVC.VanDonCollection, ListVanDon);
            #endregion

            TKVC.MaVach = System.Convert.ToDecimal(VAS.KJ.GetValue().ToString());
            TKVC.NgayPheDuyetVC = System.Convert.ToDateTime(VAS.KK.GetValue().ToString());
            TKVC.NgayDuKienBatDauVC = System.Convert.ToDateTime(VAS.CW.GetValue().ToString());
            TKVC.GioDuKienBatDauVC = VAS.CX.GetValue().ToString();
            TKVC.NgayDuKienKetThucVC = System.Convert.ToDateTime(VAS.CY.GetValue().ToString());
            TKVC.GioDuKienKetThucVC = VAS.CZ.GetValue().ToString();
            TKVC.MaBuuChinhHQ = VAS.KL.GetValue().ToString();
            TKVC.DiaChiBuuChinhHQ = VAS.KM.GetValue().ToString();
            TKVC.TenBuuChinhHQ = VAS.KN.GetValue().ToString();

            #region  SoTKXK
            if (TKVC.TKXuatCollection == null) TKVC.TKXuatCollection = new List<KDT_VNACC_TKVC_TKXuat>();
            List<KDT_VNACC_TKVC_TKXuat> ListToKhaiXK = new List<KDT_VNACC_TKVC_TKXuat>();
            if (VAS.DG.listAttribute != null && VAS.DG.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAS.Z01.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TKVC_TKXuat ToKhaiXK = new KDT_VNACC_TKVC_TKXuat();
                    ToKhaiXK.SoToKhaiXuat = System.Convert.ToDecimal(VAS.Z01.listAttribute[0].GetValueCollection(i), cltInfo);
                    if (ToKhaiXK.SoToKhaiXuat > 0)
                        ListToKhaiXK.Add(ToKhaiXK);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TKVC_TKXuat>(TKVC.TKXuatCollection, ListToKhaiXK);
            #endregion

            //             TKVC.ContainerCollection = new List<KDT_VNACC_TKVC_Container>();
            //             foreach (VAS5050_HANG item in VAS.HangHoa)
            //             {
            //                 TKVC.ContainerCollection.Add(GetTKVC_ContFromVAS5050(item));
            //             }
            if (TKVC.ContainerCollection == null) TKVC.ContainerCollection = new List<KDT_VNACC_TKVC_Container>();

            for (int i = 0; i < VAS.HangHoa.Count; i++)
            {
                if (i < TKVC.ContainerCollection.Count)
                    TKVC.ContainerCollection[i] = GetTKVC_ContFromVAS5050(VAS.HangHoa[i], TKVC.ContainerCollection[i]);
            }

            return TKVC;
        }
        public static KDT_VNACC_TKVC_Container GetTKVC_ContFromVAS5050(VAS5050_HANG cont, KDT_VNACC_TKVC_Container CONTAINER)
        {
            CONTAINER.SotieuDe = cont.DA.GetValue().ToString();
            CONTAINER.SoHieuContainer = cont.DB.GetValue().ToString();
            CONTAINER.SoDongHangTrenTK = cont.DC.GetValue().ToString();
            //CONTAINER.SoSeal1 = cont.DD.GetValue().ToString();
            if (cont.DD.listAttribute != null && cont.DD.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < cont.DD.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            CONTAINER.SoSeal1 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            CONTAINER.SoSeal2 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            CONTAINER.SoSeal3 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            CONTAINER.SoSeal4 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            CONTAINER.SoSeal5 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 5:
                            CONTAINER.SoSeal6 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            return CONTAINER;
        }

        public static KDT_VNACC_ToKhaiVanChuyen GetTKVCFromVAS5010(VAS501 VAS, KDT_VNACC_ToKhaiVanChuyen TKVC)
        {
            //TKVC.MaCoBaoNiemPhong = VAS.KA.GetValue().ToString();
            //TKVC.TenCoBaoNiemPhong = VAS.KB.GetValue().ToString();
            TKVC.TenThongTinXuat = VAS.AB.GetValue().ToString();
            //TKVC.CoQuanHaiQuan = VAS.AD.GetValue().ToString();
            TKVC.CoBaoXuatNhapKhau = VAS.ED.GetValue().ToString();
            TKVC.SoToKhaiVC = System.Convert.ToDecimal(VAS.AE.GetValue());
            TKVC.MaNguoiVC = VAS.AL.GetValue().ToString();
            TKVC.TenNguoiVC = VAS.AM.GetValue().ToString();
            TKVC.DiaChiNguoiVC = VAS.AN.GetValue().ToString();
            TKVC.SoHopDongVC = VAS.AO.GetValue().ToString();
            TKVC.NgayHopDongVC = System.Convert.ToDateTime(VAS.AP.GetValue());
            TKVC.NgayHetHanHopDongVC = System.Convert.ToDateTime(VAS.AQ.GetValue());
            TKVC.MaPhuongTienVC = VAS.AR.GetValue().ToString();
            TKVC.TenPhuongTieVC = VAS.AS.GetValue().ToString();
            TKVC.MaMucDichVC = VAS.AT.GetValue().ToString();
            TKVC.TenMucDichVC = VAS.AU.GetValue().ToString();
            TKVC.LoaiHinhVanTai = VAS.AV.GetValue().ToString();
            TKVC.TenLoaiHinhVanTai = VAS.AW.GetValue().ToString();
            TKVC.MaDiaDiemXepHang = VAS.AX.GetValue().ToString();
            TKVC.MaViTriXepHang = VAS.AY.GetValue().ToString();
            TKVC.MaCangCuaKhauGaXepHang = VAS.AZ.GetValue().ToString();
            TKVC.MaCangXHKhongCo_HT = VAS.BA.GetValue().ToString();
            TKVC.DiaDiemXepHang = VAS.ZJ.GetValue().ToString();
            //TKVC.NgayDenDiaDiem_XH = System.Convert.ToDateTime(VAS.KE.GetValue().ToString());


            //if (VAS.KC.listAttribute != null && VAS.KC.listAttribute[0].ListValue != null)
            //{
            //    for (int i = 0; i < VAS.KC.listAttribute[0].ListValue.Count; i++)
            //    {
            //        TKVC.MaDiaDiemTrungChuyen = VAS.KC.listAttribute[0].GetValueCollection(i).ToString();
            //        TKVC.TenDiaDiemTrungChuyen = VAS.KC.listAttribute[1].GetValueCollection(i).ToString();
            //        TKVC.NgayDenDiaDiem_TC = System.Convert.ToDateTime(VAS.KC.listAttribute[2].GetValueCollection(i).ToString());
            //        TKVC.NgayDiDiaDiem_TC = System.Convert.ToDateTime(VAS.KC.listAttribute[3].GetValueCollection(i).ToString());
            //    }
            //}

            TKVC.MaDiaDiemDoHang = VAS.BC.GetValue().ToString();
            TKVC.MaViTriDoHang = VAS.BD.GetValue().ToString();
            TKVC.MaCangCuaKhauGaDoHang = VAS.BE.GetValue().ToString();
            TKVC.MaCangDHKhongCo_HT = VAS.BF.GetValue().ToString();
            TKVC.DiaDiemDoHang = VAS.BG.GetValue().ToString();
            //TKVC.NgayDenDiaDiem_DH = System.Convert.ToDateTime(VAS.KH.GetValue().ToString());
            TKVC.TuyenDuongVC = VAS.BH.GetValue().ToString();
            TKVC.LoaiBaoLanh = VAS.FA.GetValue().ToString();
            TKVC.SoTienBaoLanh = System.Convert.ToDecimal(VAS.FB.GetValue(), cltInfo);
            TKVC.SoLuongCot_TK = System.Convert.ToInt32(VAS.BI.GetValue());
            TKVC.SoLuongContainer = System.Convert.ToInt32(VAS.BJ.GetValue());
            TKVC.GhiChu = VAS.BK.GetValue().ToString();

            #region  Vận đơn
            if (TKVC.VanDonCollection == null) TKVC.VanDonCollection = new List<KDT_VNACC_TKVC_VanDon>();
            List<KDT_VNACC_TKVC_VanDon> ListVanDon = new List<KDT_VNACC_TKVC_VanDon>();
            if (VAS.DG.listAttribute != null && VAS.DG.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAS.DG.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TKVC_VanDon VanDon = new KDT_VNACC_TKVC_VanDon();
                    VanDon.SoTTVanDon = VAS.DG.listAttribute[0].GetValueCollection(i).ToString();
                    VanDon.SoVanDon = VAS.DG.listAttribute[1].GetValueCollection(i).ToString();
                    VanDon.NgayPhatHanhVD = System.Convert.ToDateTime(VAS.DG.listAttribute[2].GetValueCollection(i).ToString());
                    VanDon.MoTaHangHoa = VAS.DG.listAttribute[3].GetValueCollection(i).ToString();
                    VanDon.MaHS = VAS.DG.listAttribute[4].GetValueCollection(i).ToString();
                    VanDon.KyHieuVaSoHieu = VAS.DG.listAttribute[5].GetValueCollection(i).ToString();
                    VanDon.NgayNhapKhoHQLanDau = System.Convert.ToDateTime(VAS.DG.listAttribute[6].GetValueCollection(i).ToString());
                    VanDon.PhanLoaiSanPhan = VAS.DG.listAttribute[7].GetValueCollection(i).ToString();
                    VanDon.MaNuocSanXuat = VAS.DG.listAttribute[8].GetValueCollection(i).ToString();
                    VanDon.TenNuocSanXuat = VAS.DG.listAttribute[9].GetValueCollection(i).ToString();
                    VanDon.MaDiaDiemXuatPhatVC = VAS.DG.listAttribute[10].GetValueCollection(i).ToString();
                    VanDon.TenDiaDiemXuatPhatVC = VAS.DG.listAttribute[11].GetValueCollection(i).ToString();
                    VanDon.MaDiaDiemDichVC = VAS.DG.listAttribute[12].GetValueCollection(i).ToString();
                    VanDon.TenDiaDiemDichVC = VAS.DG.listAttribute[13].GetValueCollection(i).ToString();
                    VanDon.LoaiHangHoa = VAS.DG.listAttribute[14].GetValueCollection(i).ToString();
                    VanDon.MaPhuongTienVC = VAS.DG.listAttribute[15].GetValueCollection(i).ToString();
                    VanDon.TenPhuongTienVC = VAS.DG.listAttribute[16].GetValueCollection(i).ToString();
                    VanDon.NgayHangDuKienDenDi = System.Convert.ToDateTime(VAS.DG.listAttribute[17].GetValueCollection(i).ToString());
                    VanDon.MaNguoiNhapKhau = VAS.DG.listAttribute[18].GetValueCollection(i).ToString();
                    VanDon.TenNguoiNhapKhau = VAS.DG.listAttribute[19].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiNhapKhau = VAS.DG.listAttribute[20].GetValueCollection(i).ToString();
                    VanDon.MaNguoiXuatKhua = VAS.DG.listAttribute[21].GetValueCollection(i).ToString();
                    VanDon.TenNguoiXuatKhau = VAS.DG.listAttribute[22].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiXuatKhau = VAS.DG.listAttribute[23].GetValueCollection(i).ToString();
                    VanDon.MaNguoiUyThac = VAS.DG.listAttribute[24].GetValueCollection(i).ToString();
                    VanDon.TenNguoiUyThac = VAS.DG.listAttribute[25].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiUyThac = VAS.DG.listAttribute[26].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat1 = VAS.DG.listAttribute[27].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat2 = VAS.DG.listAttribute[28].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat3 = VAS.DG.listAttribute[29].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat4 = VAS.DG.listAttribute[30].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat5 = VAS.DG.listAttribute[31].GetValueCollection(i).ToString();
                    VanDon.MaDVTTriGia = VAS.DG.listAttribute[32].GetValueCollection(i).ToString();
                    VanDon.TriGia = System.Convert.ToDecimal(VAS.DG.listAttribute[33].GetValueCollection(i), cltInfo);
                    VanDon.SoLuong = System.Convert.ToDecimal(VAS.DG.listAttribute[34].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTSoLuong = VAS.DG.listAttribute[35].GetValueCollection(i).ToString();
                    VanDon.TongTrongLuong = System.Convert.ToDecimal(VAS.DG.listAttribute[36].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTTrongLuong = VAS.DG.listAttribute[37].GetValueCollection(i).ToString();
                    VanDon.TheTich = System.Convert.ToDecimal(VAS.DG.listAttribute[38].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTTheTich = VAS.DG.listAttribute[39].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH1 = VAS.DG.listAttribute[40].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH2 = VAS.DG.listAttribute[41].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH3 = VAS.DG.listAttribute[42].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH4 = VAS.DG.listAttribute[43].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH5 = VAS.DG.listAttribute[44].GetValueCollection(i).ToString();
                    VanDon.SoGiayPhep = VAS.DG.listAttribute[45].GetValueCollection(i).ToString();
                    VanDon.NgayCapPhep = System.Convert.ToDateTime(VAS.DG.listAttribute[46].GetValueCollection(i).ToString());
                    VanDon.GhiChu = VAS.DG.listAttribute[47].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(VanDon.SoVanDon.Trim()))
                        ListVanDon.Add(VanDon);
                }


            }
            HelperVNACCS.UpdateList<KDT_VNACC_TKVC_VanDon>(TKVC.VanDonCollection, ListVanDon);
            #endregion

            //TKVC.MaVach = System.Convert.ToDecimal(VAS.KJ.GetValue().ToString());
            //TKVC.NgayPheDuyetVC = System.Convert.ToDateTime(VAS.KK.GetValue().ToString());
            TKVC.NgayDuKienBatDauVC = System.Convert.ToDateTime(VAS.CW.GetValue().ToString());
            TKVC.GioDuKienBatDauVC = VAS.CX.GetValue().ToString();
            TKVC.NgayDuKienKetThucVC = System.Convert.ToDateTime(VAS.CY.GetValue().ToString());
            TKVC.GioDuKienKetThucVC = VAS.CZ.GetValue().ToString();
            //TKVC.MaBuuChinhHQ = VAS.KL.GetValue().ToString();
            //TKVC.DiaChiBuuChinhHQ = VAS.KM.GetValue().ToString();
            //TKVC.TenBuuChinhHQ = VAS.KN.GetValue().ToString();

            #region  SoTKXK
            if (TKVC.TKXuatCollection == null) TKVC.TKXuatCollection = new List<KDT_VNACC_TKVC_TKXuat>();
            List<KDT_VNACC_TKVC_TKXuat> ListToKhaiXK = new List<KDT_VNACC_TKVC_TKXuat>();
            if (VAS.DG.listAttribute != null && VAS.DG.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAS.Z01.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TKVC_TKXuat ToKhaiXK = new KDT_VNACC_TKVC_TKXuat();
                    ToKhaiXK.SoToKhaiXuat = System.Convert.ToDecimal(VAS.Z01.listAttribute[0].GetValueCollection(i), cltInfo);
                    if (ToKhaiXK.SoToKhaiXuat > 0)
                        ListToKhaiXK.Add(ToKhaiXK);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TKVC_TKXuat>(TKVC.TKXuatCollection, ListToKhaiXK);

            if (TKVC.ContainerCollection == null) TKVC.ContainerCollection = new List<KDT_VNACC_TKVC_Container>();

            for (int i = 0; i < VAS.HangHoa.Count; i++)
            {
                if (i < TKVC.ContainerCollection.Count)
                    TKVC.ContainerCollection[i] = GetTKVC_ContFromVAS5010(VAS.HangHoa[i], TKVC.ContainerCollection[i]);
            }

            #endregion
            return TKVC;
        }
        public static KDT_VNACC_TKVC_Container GetTKVC_ContFromVAS5010(VAS501_HANG cont, KDT_VNACC_TKVC_Container CONTAINER)
        {
            CONTAINER.SotieuDe = cont.DA.GetValue().ToString();
            CONTAINER.SoHieuContainer = cont.DB.GetValue().ToString();
            CONTAINER.SoDongHangTrenTK = cont.DC.GetValue().ToString();
            //CONTAINER.SoSeal1 = cont.DD.GetValue().ToString();
            if (cont.DD.listAttribute != null && cont.DD.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < cont.DD.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            CONTAINER.SoSeal1 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            CONTAINER.SoSeal2 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            CONTAINER.SoSeal3 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            CONTAINER.SoSeal4 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            CONTAINER.SoSeal5 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 5:
                            CONTAINER.SoSeal6 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            return CONTAINER;
        }
        public static KDT_VNACC_ToKhaiVanChuyen GetTKVCFromVAS5130(VAS5130 VAS, KDT_VNACC_ToKhaiVanChuyen TKVC)
        {
            TKVC.TenThongTinXuat = VAS.AB.GetValue().ToString();
            TKVC.MaPhanLoaiXuLy = VAS.KA.GetValue().ToString();
            //TKVC.TenCoBaoNiemPhong = VAS.KB.GetValue().ToString();
            TKVC.CoQuanHaiQuan = VAS.AD.GetValue().ToString();
            TKVC.CoBaoXuatNhapKhau = VAS.ED.GetValue().ToString();
            TKVC.SoToKhaiVC = System.Convert.ToDecimal(VAS.AE.GetValue());
            TKVC.MaNguoiVC = VAS.AL.GetValue().ToString();
            TKVC.TenNguoiVC = VAS.AM.GetValue().ToString();
            TKVC.DiaChiNguoiVC = VAS.AN.GetValue().ToString();
            TKVC.SoHopDongVC = VAS.AO.GetValue().ToString();
            TKVC.NgayHopDongVC = System.Convert.ToDateTime(VAS.AP.GetValue());
            TKVC.NgayHetHanHopDongVC = System.Convert.ToDateTime(VAS.AQ.GetValue());
            TKVC.MaPhuongTienVC = VAS.AR.GetValue().ToString();
            TKVC.TenPhuongTieVC = VAS.AS.GetValue().ToString();
            TKVC.MaMucDichVC = VAS.AT.GetValue().ToString();
            TKVC.TenMucDichVC = VAS.AU.GetValue().ToString();
            TKVC.LoaiHinhVanTai = VAS.AV.GetValue().ToString();
            TKVC.TenLoaiHinhVanTai = VAS.AW.GetValue().ToString();
            TKVC.MaDiaDiemXepHang = VAS.AX.GetValue().ToString();
            TKVC.MaViTriXepHang = VAS.AY.GetValue().ToString();
            TKVC.MaCangCuaKhauGaXepHang = VAS.AZ.GetValue().ToString();
            TKVC.MaCangXHKhongCo_HT = VAS.BA.GetValue().ToString();
            TKVC.DiaDiemXepHang = VAS.ZJ.GetValue().ToString();
            TKVC.NgayDenDiaDiem_XH = System.Convert.ToDateTime(VAS.KE.GetValue().ToString());


            if (TKVC.TrungChuyenCollection == null) TKVC.TrungChuyenCollection = new List<KDT_VNACC_TKVC_TrungChuyen>();
            List<KDT_VNACC_TKVC_TrungChuyen> ListTrungChuyen = new List<KDT_VNACC_TKVC_TrungChuyen>();
            if (VAS.KC.listAttribute != null && VAS.KC.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAS.KC.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TKVC_TrungChuyen trungChuyen = new KDT_VNACC_TKVC_TrungChuyen();
                    trungChuyen.MaDiaDiemTrungChuyen = VAS.KC.listAttribute[0].GetValueCollection(i).ToString();
                    trungChuyen.TenDiaDiemTrungChuyen = VAS.KC.listAttribute[1].GetValueCollection(i).ToString();
                    trungChuyen.NgayDenDiaDiem_TC = System.Convert.ToDateTime(VAS.KC.listAttribute[2].GetValueCollection(i).ToString());
                    trungChuyen.NgayDiDiaDiem_TC = System.Convert.ToDateTime(VAS.KC.listAttribute[3].GetValueCollection(i).ToString());

                    ListTrungChuyen.Add(trungChuyen);
                }
            }

            TKVC.MaDiaDiemDoHang = VAS.BC.GetValue().ToString();
            TKVC.MaViTriDoHang = VAS.BD.GetValue().ToString();
            TKVC.MaCangCuaKhauGaDoHang = VAS.BE.GetValue().ToString();
            TKVC.MaCangDHKhongCo_HT = VAS.BF.GetValue().ToString();
            TKVC.DiaDiemDoHang = VAS.BG.GetValue().ToString();
            TKVC.NgayDenDiaDiem_DH = System.Convert.ToDateTime(VAS.KH.GetValue().ToString());
            TKVC.TuyenDuongVC = VAS.BH.GetValue().ToString();
            TKVC.LoaiBaoLanh = VAS.FA.GetValue().ToString();
            TKVC.SoTienBaoLanh = System.Convert.ToDecimal(VAS.FB.GetValue(), cltInfo);
            TKVC.SoLuongCot_TK = System.Convert.ToInt32(VAS.BI.GetValue());
            TKVC.SoLuongContainer = System.Convert.ToInt32(VAS.BJ.GetValue());
            TKVC.GhiChu = VAS.BK.GetValue().ToString();

            #region  Vận đơn
            if (TKVC.VanDonCollection == null) TKVC.VanDonCollection = new List<KDT_VNACC_TKVC_VanDon>();
            List<KDT_VNACC_TKVC_VanDon> ListVanDon = new List<KDT_VNACC_TKVC_VanDon>();
            if (VAS.DG.listAttribute != null && VAS.DG.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAS.DG.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TKVC_VanDon VanDon = new KDT_VNACC_TKVC_VanDon();
                    VanDon.SoTTVanDon = VAS.DG.listAttribute[0].GetValueCollection(i).ToString();
                    VanDon.SoVanDon = VAS.DG.listAttribute[1].GetValueCollection(i).ToString();
                    VanDon.NgayPhatHanhVD = System.Convert.ToDateTime(VAS.DG.listAttribute[2].GetValueCollection(i).ToString());
                    VanDon.MoTaHangHoa = VAS.DG.listAttribute[3].GetValueCollection(i).ToString();
                    VanDon.MaHS = VAS.DG.listAttribute[4].GetValueCollection(i).ToString();
                    VanDon.KyHieuVaSoHieu = VAS.DG.listAttribute[5].GetValueCollection(i).ToString();
                    VanDon.NgayNhapKhoHQLanDau = System.Convert.ToDateTime(VAS.DG.listAttribute[6].GetValueCollection(i).ToString());
                    VanDon.PhanLoaiSanPhan = VAS.DG.listAttribute[7].GetValueCollection(i).ToString();
                    VanDon.MaNuocSanXuat = VAS.DG.listAttribute[8].GetValueCollection(i).ToString();
                    VanDon.TenNuocSanXuat = VAS.DG.listAttribute[9].GetValueCollection(i).ToString();
                    VanDon.MaDiaDiemXuatPhatVC = VAS.DG.listAttribute[10].GetValueCollection(i).ToString();
                    VanDon.TenDiaDiemXuatPhatVC = VAS.DG.listAttribute[11].GetValueCollection(i).ToString();
                    VanDon.MaDiaDiemDichVC = VAS.DG.listAttribute[12].GetValueCollection(i).ToString();
                    VanDon.TenDiaDiemDichVC = VAS.DG.listAttribute[13].GetValueCollection(i).ToString();
                    VanDon.LoaiHangHoa = VAS.DG.listAttribute[14].GetValueCollection(i).ToString();
                    VanDon.MaPhuongTienVC = VAS.DG.listAttribute[15].GetValueCollection(i).ToString();
                    VanDon.TenPhuongTienVC = VAS.DG.listAttribute[16].GetValueCollection(i).ToString();
                    VanDon.NgayHangDuKienDenDi = System.Convert.ToDateTime(VAS.DG.listAttribute[17].GetValueCollection(i).ToString());
                    VanDon.MaNguoiNhapKhau = VAS.DG.listAttribute[18].GetValueCollection(i).ToString();
                    VanDon.TenNguoiNhapKhau = VAS.DG.listAttribute[19].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiNhapKhau = VAS.DG.listAttribute[20].GetValueCollection(i).ToString();
                    VanDon.MaNguoiXuatKhua = VAS.DG.listAttribute[21].GetValueCollection(i).ToString();
                    VanDon.TenNguoiXuatKhau = VAS.DG.listAttribute[22].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiXuatKhau = VAS.DG.listAttribute[23].GetValueCollection(i).ToString();
                    VanDon.MaNguoiUyThac = VAS.DG.listAttribute[24].GetValueCollection(i).ToString();
                    VanDon.TenNguoiUyThac = VAS.DG.listAttribute[25].GetValueCollection(i).ToString();
                    VanDon.DiaChiNguoiUyThac = VAS.DG.listAttribute[26].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat1 = VAS.DG.listAttribute[27].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat2 = VAS.DG.listAttribute[28].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat3 = VAS.DG.listAttribute[29].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat4 = VAS.DG.listAttribute[30].GetValueCollection(i).ToString();
                    VanDon.MaVanBanPhapLuat5 = VAS.DG.listAttribute[31].GetValueCollection(i).ToString();
                    VanDon.MaDVTTriGia = VAS.DG.listAttribute[32].GetValueCollection(i).ToString();
                    VanDon.TriGia = System.Convert.ToDecimal(VAS.DG.listAttribute[33].GetValueCollection(i), cltInfo);
                    VanDon.SoLuong = System.Convert.ToDecimal(VAS.DG.listAttribute[34].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTSoLuong = VAS.DG.listAttribute[35].GetValueCollection(i).ToString();
                    VanDon.TongTrongLuong = System.Convert.ToDecimal(VAS.DG.listAttribute[36].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTTrongLuong = VAS.DG.listAttribute[37].GetValueCollection(i).ToString();
                    VanDon.TheTich = System.Convert.ToDecimal(VAS.DG.listAttribute[38].GetValueCollection(i), cltInfo);
                    VanDon.MaDVTTheTich = VAS.DG.listAttribute[39].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH1 = VAS.DG.listAttribute[40].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH2 = VAS.DG.listAttribute[41].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH3 = VAS.DG.listAttribute[42].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH4 = VAS.DG.listAttribute[43].GetValueCollection(i).ToString();
                    VanDon.MaDanhDauDDKH5 = VAS.DG.listAttribute[44].GetValueCollection(i).ToString();
                    VanDon.SoGiayPhep = VAS.DG.listAttribute[45].GetValueCollection(i).ToString();
                    VanDon.NgayCapPhep = System.Convert.ToDateTime(VAS.DG.listAttribute[46].GetValueCollection(i).ToString());
                    VanDon.GhiChu = VAS.DG.listAttribute[47].GetValueCollection(i).ToString();
                    if (!string.IsNullOrEmpty(VanDon.SoVanDon.Trim()))
                        ListVanDon.Add(VanDon);
                }


            }
            HelperVNACCS.UpdateList<KDT_VNACC_TKVC_VanDon>(TKVC.VanDonCollection, ListVanDon);
            #endregion

            //TKVC.MaVach = System.Convert.ToDecimal(VAS.KJ.GetValue().ToString());
            //TKVC.NgayPheDuyetVC = System.Convert.ToDateTime(VAS.KK.GetValue().ToString());
            TKVC.NgayDuKienBatDauVC = System.Convert.ToDateTime(VAS.CW.GetValue().ToString());
            TKVC.GioDuKienBatDauVC = VAS.CX.GetValue().ToString();
            TKVC.NgayDuKienKetThucVC = System.Convert.ToDateTime(VAS.CY.GetValue().ToString());
            TKVC.GioDuKienKetThucVC = VAS.CZ.GetValue().ToString();
            //TKVC.MaBuuChinhHQ = VAS.KL.GetValue().ToString();
            //TKVC.DiaChiBuuChinhHQ = VAS.KM.GetValue().ToString();
            //TKVC.TenBuuChinhHQ = VAS.KN.GetValue().ToString();

            #region  SoTKXK
            if (TKVC.TKXuatCollection == null) TKVC.TKXuatCollection = new List<KDT_VNACC_TKVC_TKXuat>();
            List<KDT_VNACC_TKVC_TKXuat> ListToKhaiXK = new List<KDT_VNACC_TKVC_TKXuat>();
            if (VAS.DG.listAttribute != null && VAS.DG.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < VAS.Z01.listAttribute[0].ListValue.Count; i++)
                {
                    KDT_VNACC_TKVC_TKXuat ToKhaiXK = new KDT_VNACC_TKVC_TKXuat();
                    ToKhaiXK.SoToKhaiXuat = System.Convert.ToDecimal(VAS.Z01.listAttribute[0].GetValueCollection(i), cltInfo);
                    if (ToKhaiXK.SoToKhaiXuat > 0)
                        ListToKhaiXK.Add(ToKhaiXK);
                }
            }
            HelperVNACCS.UpdateList<KDT_VNACC_TKVC_TKXuat>(TKVC.TKXuatCollection, ListToKhaiXK);
            #endregion

            TKVC.ContainerCollection = new List<KDT_VNACC_TKVC_Container>();
            foreach (VAS5130_HANG item in VAS.HangHoa)
            {
                TKVC.ContainerCollection.Add(GetTKVC_ContFromVAS5130(item));
            }

            return TKVC;
        }
        public static KDT_VNACC_TKVC_Container GetTKVC_ContFromVAS5130(VAS5130_HANG cont)
        {
            KDT_VNACC_TKVC_Container CONTAINER = new KDT_VNACC_TKVC_Container();
            CONTAINER.SotieuDe = cont.DA.GetValue().ToString();
            CONTAINER.SoHieuContainer = cont.DB.GetValue().ToString();
            CONTAINER.SoDongHangTrenTK = cont.DC.GetValue().ToString();
            //CONTAINER.SoSeal1 = cont.DD.GetValue().ToString();
            if (cont.DD.listAttribute != null && cont.DD.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < cont.DD.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            CONTAINER.SoSeal1 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            CONTAINER.SoSeal2 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            CONTAINER.SoSeal3 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            CONTAINER.SoSeal4 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            CONTAINER.SoSeal5 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 5:
                            CONTAINER.SoSeal6 = cont.DD.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                    }
                }
            }
            return CONTAINER;
        }

        //public static KDT_VNACCS_TEA GetDMMTFromVAD8010(VAD8010 vad, KDT_VNACCS_TEA DMMT)
        //{
        //    DMMT.SoDanhMucMienThue = System.Convert.ToDecimal(vad.A01.GetValue().ToString());
        //    DMMT.MaSoQuanLyDSMT = vad.A02.GetValue().ToString();
        //    DMMT.PhanLoaiXuatNhapKhau = vad.A03.GetValue().ToString();
        //    DMMT.CoQuanHaiQuan = vad.A04.GetValue().ToString();

        //    DMMT.MaNguoiKhai = vad.A11.GetValue().ToString();

        //    DMMT.DiaChiCuaNguoiKhai = vad.A13.GetValue().ToString();
        //    DMMT.SDTCuaNguoiKhai = vad.A14.GetValue().ToString();
        //    DMMT.ThoiHanMienThue = System.Convert.ToDateTime(vad.A15.GetValue().ToString());
        //    DMMT.TenDuAnDauTu = vad.A16.GetValue().ToString();
        //    DMMT.DiaDiemXayDungDuAn = vad.A17.GetValue().ToString();
        //    DMMT.MucTieuDuAn = vad.A18.GetValue().ToString();
        //    DMMT.MaMienGiam = vad.A19.GetValue().ToString();

        //    DMMT.PhamViDangKyDMMT = vad.A21.GetValue().ToString();
        //    DMMT.NgayDuKienXNK = System.Convert.ToDateTime(vad.A22.GetValue().ToString());
        //    DMMT.GP_GCNDauTuSo = vad.A23.GetValue().ToString();
        //    DMMT.NgayChungNhan = System.Convert.ToDateTime(vad.A24.GetValue().ToString());
        //    DMMT.CapBoi = vad.A25.GetValue().ToString();

        //    if (DMMT.DieuChinhCollection == null) DMMT.DieuChinhCollection = new List<KDT_VNACCS_TEADieuChinh>();
        //    List<KDT_VNACCS_TEADieuChinh> ListDC = new List<KDT_VNACCS_TEADieuChinh>();
        //    if (vad.DA1.listAttribute != null && vad.DA1.listAttribute[0].ListValue != null)
        //    {
        //        for (int i = 0; i < vad.DA1.listAttribute[0].ListValue.Count; i++)
        //        {
        //            KDT_VNACCS_TEADieuChinh dieuchinh = new KDT_VNACCS_TEADieuChinh();
        //            dieuchinh.LanDieuChinhGP_GCN = System.Convert.ToInt32(vad.DA1.listAttribute[0].GetValueCollection(i).ToString());
        //            dieuchinh.ChungNhanDieuChinhSo = vad.DA1.listAttribute[1].GetValueCollection(i).ToString();
        //            dieuchinh.NgayChungNhanDieuChinh = System.Convert.ToDateTime(vad.DA1.listAttribute[2].GetValueCollection(i).ToString());
        //            dieuchinh.DieuChinhBoi = vad.DA1.listAttribute[3].GetValueCollection(i).ToString();
        //            ListDC.Add(dieuchinh);
        //        }
        //    }
        //    if (DMMT.NguoiXNKCollection == null) DMMT.NguoiXNKCollection = new List<KDT_VNACCS_TEANguoiXNK>();
        //    List<KDT_VNACCS_TEANguoiXNK> ListNguoiXNK = new List<KDT_VNACCS_TEANguoiXNK>();
        //    if (vad.B01.listAttribute != null && vad.B01.listAttribute[0].ListValue != null)
        //    {
        //        for (int i = 0; i < vad.DA1.listAttribute[0].ListValue.Count; i++)
        //        {
        //            KDT_VNACCS_TEANguoiXNK nguoiXNK = new KDT_VNACCS_TEANguoiXNK();
        //            nguoiXNK.MaNguoiXNK = vad.B01.listAttribute[0].GetValueCollection(i).ToString();
        //            nguoiXNK.TenNguoiXNK = vad.B01.listAttribute[1].GetValueCollection(i).ToString();

        //            ListNguoiXNK.Add(nguoiXNK);
        //        }
        //    }
        //    DMMT.HangCollection = new List<KDT_VNACCS_TEA_HangHoa>();
        //    foreach (VAD8010_HANG item in vad.HangHoa)
        //    {
        //        DMMT.HangCollection.Add(GetDMMT_HangFromVAD8010(item));
        //    }
        //    return DMMT;
        //}
        //public static KDT_VNACCS_TEA_HangHoa GetDMMT_HangFromVAD8010(VAD8010_HANG DMMT_hang)
        //{
        //    KDT_VNACCS_TEA_HangHoa hang = new KDT_VNACCS_TEA_HangHoa();
        //    //hang. = DMMT_hang.D01.GetValue().ToString();
        //    hang.MoTaHangHoa = DMMT_hang.D02.GetValue().ToString();
        //    hang.SoLuongDangKyMT = System.Convert.ToInt32(DMMT_hang.D03.GetValue().ToString());
        //    hang.DVTSoLuongDangKyMT = DMMT_hang.D04.GetValue().ToString();
        //    hang.SoLuongDaSuDung = System.Convert.ToInt32(DMMT_hang.D05.GetValue().ToString());
        //    hang.DVTSoLuongDaSuDung = DMMT_hang.D06.GetValue().ToString();
        //    //hang. = System.Convert.ToInt32(DMMT_hang.D07.GetValue().ToString());
        //    //hang. = DMMT_hang.D08.GetValue().ToString();           
        //    hang.TriGia = System.Convert.ToInt32(DMMT_hang.D09.GetValue().ToString());
        //    hang.TriGiaDuKien = System.Convert.ToInt32(DMMT_hang.D10.GetValue().ToString());

        //    return hang;
        //}

        #region E-payperment

        public static KDT_VNACC_ChungTuThanhToan GetCTTTFromVAF805(VAF8050 vaf805, KDT_VNACC_ChungTuThanhToan cttt)
        {
            cttt.LoaiHinhBaoLanh = vaf805.TYS.GetValue().ToString();
            cttt.MaNganHangCungCapBaoLanh = vaf805.A01.GetValue().ToString();
            cttt.TenNganHangCungCapBaoLanh = vaf805.A02.GetValue().ToString();
            cttt.NamPhatHanh = System.Convert.ToDecimal(vaf805.A03.GetValue());
            cttt.KiHieuChungTuPhatHanhBaoLanh = vaf805.A04.GetValue().ToString();
            cttt.SoChungTuPhatHanhBaoLanh = vaf805.A05.GetValue().ToString();
            cttt.SoToKhai = System.Convert.ToDecimal(vaf805.A06.GetValue());
            cttt.NgayDuDinhKhaiBao = System.Convert.ToDateTime(vaf805.A07.GetValue());
            cttt.MaLoaiHinh = vaf805.A08.GetValue().ToString();
            cttt.CoQuanHaiQuan = vaf805.A09.GetValue().ToString();
            cttt.TenCucHaiQuan = vaf805.A10.GetValue().ToString();
            //cttt.SoVanDon_1 = vaf805.CN1.GetValue().ToString();
            if (vaf805.CN1.listAttribute != null && vaf805.CN1.listAttribute[0].ListValue != null)
            {
                for (int i = 0; i < vaf805.CN1.listAttribute[0].ListValue.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            cttt.SoVanDon_1 = vaf805.CN1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 1:
                            cttt.SoVanDon_2 = vaf805.CN1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 2:
                            cttt.SoVanDon_3 = vaf805.CN1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 3:
                            cttt.SoVanDon_4 = vaf805.CN1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        case 4:
                            cttt.SoVanDon_5 = vaf805.CN1.listAttribute[0].GetValueCollection(i).ToString();
                            break;
                        default:
                            break;
                    }
                }
            }

            cttt.SoHoaDon = vaf805.A12.GetValue().ToString();
            cttt.MaDonViSuDungBaoLanh = vaf805.A13.GetValue().ToString();
            cttt.TenDonViSuDungBaoLanh = vaf805.A14.GetValue().ToString();
            cttt.MaDonViDaiDienSuDungBaoLanh = vaf805.A15.GetValue().ToString();
            cttt.TenDonViDaiDienSuDungBaoLanh = vaf805.A16.GetValue().ToString();
            cttt.NgayBatDauHieuLuc = System.Convert.ToDateTime(vaf805.A17.GetValue());
            cttt.NgayHetHieuLuc = System.Convert.ToDateTime(vaf805.A18.GetValue());
            cttt.SoTienDangKyBaoLanh = System.Convert.ToDecimal(vaf805.A19.GetValue());
            cttt.MaTienTe = vaf805.A20.GetValue().ToString();
            return cttt;
        }
        public static KDT_VNACC_ChungTuThanhToan GetCTTTFromVAF806(VAF8060 vaf806, KDT_VNACC_ChungTuThanhToan cttt)
        {
            cttt.CoBaoVoHieu = vaf806.A06.GetValue().ToString();
            cttt.MaDonViSuDungBaoLanh = vaf806.A07.GetValue().ToString();
            cttt.TenDonViSuDungBaoLanh = vaf806.A08.GetValue().ToString();
            cttt.MaDonViDaiDienSuDungBaoLanh = vaf806.A09.GetValue().ToString();
            cttt.TenDonViDaiDienSuDungBaoLanh = vaf806.A10.GetValue().ToString();
            cttt.NgayBatDauHieuLuc = System.Convert.ToDateTime(vaf806.A11.GetValue());
            cttt.NgayHetHieuLuc = System.Convert.ToDateTime(vaf806.A12.GetValue());
            cttt.SoTienDangKyBaoLanh = System.Convert.ToDecimal(vaf806.A13.GetValue());
            cttt.SoDuBaoLanh = System.Convert.ToDecimal(vaf806.A14.GetValue());
            cttt.MaTienTe = vaf806.CCC.GetValue().ToString();

            cttt.ChungTuChiTietCollection = new List<KDT_VNACC_ChungTuThanhToan_ChiTiet>();
            foreach (var item in vaf806.HangMD)
            {
                KDT_VNACC_ChungTuThanhToan_ChiTiet chitiet = new KDT_VNACC_ChungTuThanhToan_ChiTiet();
                chitiet = GetCTTT_ChiTietFromVAF806(item, chitiet);
                if (chitiet != null)
                    cttt.ChungTuChiTietCollection.Add(chitiet);
            }

            return cttt;
        }
        public static KDT_VNACC_ChungTuThanhToan_ChiTiet GetCTTT_ChiTietFromVAF806(VAF8060_HANG vaf806_hang, KDT_VNACC_ChungTuThanhToan_ChiTiet cttt_hang)
        {
            cttt_hang.SoThuTuGiaoDich = Convert.ToDecimal(vaf806_hang.B01.GetValue());
            cttt_hang.NgayTaoDuLieu = System.Convert.ToDateTime(vaf806_hang.B02.GetValue());
            cttt_hang.ThoiGianTaoDuLieu = System.Convert.ToDateTime(vaf806_hang.B03.GetValue());
            cttt_hang.SoTienTruLui = System.Convert.ToDecimal(vaf806_hang.B04.GetValue());
            cttt_hang.SoTienTangLen = System.Convert.ToDecimal(vaf806_hang.B05.GetValue());
            cttt_hang.SoToKhai = System.Convert.ToDecimal(vaf806_hang.B06.GetValue());
            cttt_hang.NgayDangKyToKhai = System.Convert.ToDateTime(vaf806_hang.B07.GetValue());
            cttt_hang.MaLoaiHinh = vaf806_hang.B08.GetValue().ToString();
            cttt_hang.CoQuanHaiQuan = vaf806_hang.B09.GetValue().ToString();
            cttt_hang.TenCucHaiQuan = vaf806_hang.B10.GetValue().ToString();

            if (cttt_hang.SoThuTuGiaoDich != null && cttt_hang.SoThuTuGiaoDich != 0)
                return cttt_hang;
            else
                return null;
        }

        public static KDT_VNACC_ChungTuThanhToan GetCTTTFromvaf807(VAF8070 vaf807, KDT_VNACC_ChungTuThanhToan cttt)
        {
            cttt.LoaiHinhBaoLanh = vaf807.TYS.GetValue().ToString();
            cttt.MaNganHangCungCapBaoLanh = vaf807.A01.GetValue().ToString();
            cttt.TenNganHangCungCapBaoLanh = vaf807.A02.GetValue().ToString();
            cttt.NamPhatHanh = System.Convert.ToDecimal(vaf807.A03.GetValue());
            cttt.KiHieuChungTuPhatHanhBaoLanh = vaf807.A04.GetValue().ToString();
            cttt.SoChungTuPhatHanhBaoLanh = vaf807.A05.GetValue().ToString();
            cttt.SoToKhai = System.Convert.ToDecimal(vaf807.A06.GetValue());
            cttt.CoQuanHaiQuan = vaf807.A07.GetValue().ToString();
            cttt.TenCucHaiQuan = vaf807.A08.GetValue().ToString();
            cttt.MaDonViSuDungBaoLanh = vaf807.A09.GetValue().ToString();
            cttt.TenDonViSuDungBaoLanh = vaf807.A10.GetValue().ToString();
            cttt.NgayBatDauHieuLuc = System.Convert.ToDateTime(vaf807.A11.GetValue());
            cttt.NgayHetHieuLuc = System.Convert.ToDateTime(vaf807.A12.GetValue());
            cttt.SoTienDangKyBaoLanh = System.Convert.ToDecimal(vaf807.A13.GetValue());


            return cttt;
        }

        public static KDT_VNACC_ChungTuThanhToan GetCTTTFromvaf808(VAF8080 vaf808, KDT_VNACC_ChungTuThanhToan cttt)
        {
            cttt.LoaiHinhBaoLanh = vaf808.TYS.GetValue().ToString();
            cttt.MaNganHangCungCapBaoLanh = vaf808.A01.GetValue().ToString();
            cttt.TenNganHangCungCapBaoLanh = vaf808.A02.GetValue().ToString();
            cttt.NamPhatHanh = System.Convert.ToDecimal(vaf808.A03.GetValue());
            cttt.KiHieuChungTuPhatHanhBaoLanh = vaf808.A04.GetValue().ToString();
            cttt.SoChungTuPhatHanhBaoLanh = vaf808.A05.GetValue().ToString();
            cttt.CoBaoVoHieu = System.Convert.ToDecimal(vaf808.A06.GetValue()).ToString();
            cttt.MaDonViSuDungBaoLanh = vaf808.A07.GetValue().ToString();
            cttt.TenDonViSuDungBaoLanh = vaf808.A08.GetValue().ToString();
            cttt.NgayBatDauHieuLuc = System.Convert.ToDateTime(vaf808.A09.GetValue());
            cttt.NgayHetHieuLuc = System.Convert.ToDateTime(vaf808.A10.GetValue());
            cttt.SoTienDangKyBaoLanh = System.Convert.ToDecimal(vaf808.A11.GetValue());
            return cttt;
        }

        public static KDT_VNACC_ChungTuThanhToan GetCTTTFromVAF511(VAF5110 vaf511, KDT_VNACC_ChungTuThanhToan cttt)
        {
            cttt.MaNganHangTraThay = vaf511.BRC.GetValue().ToString();
            cttt.TenNganHangTraThay = vaf511.A02.GetValue().ToString();
            cttt.MaDonViSuDungHanMuc = vaf511.UBP.GetValue().ToString();
            cttt.TenDonViSuDungHanMuc = vaf511.A04.GetValue().ToString();
            cttt.NamPhatHanh = System.Convert.ToDecimal(vaf511.RYA.GetValue());
            cttt.KiHieuChungTuPhatHanhHanMuc = vaf511.BPS.GetValue().ToString();
            cttt.SoHieuPhatHanhHanMuc = vaf511.BPN.GetValue().ToString();
            cttt.CoBaoVoHieu = vaf511.A08.GetValue().ToString();
            cttt.NgayBatDauHieuLuc = System.Convert.ToDateTime(vaf511.A09.GetValue());
            cttt.NgayHetHieuLuc = System.Convert.ToDateTime(vaf511.A10.GetValue());
            cttt.SoTienHanMucDangKi = System.Convert.ToDecimal(vaf511.A11.GetValue());
            cttt.SoDuHanMuc = System.Convert.ToDecimal(vaf511.A12.GetValue());
            cttt.MaTienTe = vaf511.CCC.GetValue().ToString();
            cttt.SoThuTuHanMuc = System.Convert.ToDecimal(vaf511.A14.GetValue()).ToString();
            cttt.ChungTuChiTietCollection = new List<KDT_VNACC_ChungTuThanhToan_ChiTiet>();
            foreach (var item in vaf511.HangMD)
            {
                KDT_VNACC_ChungTuThanhToan_ChiTiet chitiet = new KDT_VNACC_ChungTuThanhToan_ChiTiet();
                chitiet = GetCTTT_ChiTietFromVAF511(item, chitiet);
                if (chitiet != null)
                    cttt.ChungTuChiTietCollection.Add(chitiet);
            }
            return cttt;
        }
        public static KDT_VNACC_ChungTuThanhToan_ChiTiet GetCTTT_ChiTietFromVAF511(VAF5110_HANG vaf511_hang, KDT_VNACC_ChungTuThanhToan_ChiTiet cttt_chitiet)
        {
            cttt_chitiet.SoThuTuGiaoDich = System.Convert.ToDecimal(vaf511_hang.B01.GetValue());
            cttt_chitiet.NgayTaoDuLieu = System.Convert.ToDateTime(vaf511_hang.B02.GetValue());
            cttt_chitiet.ThoiGianTaoDuLieu = System.Convert.ToDateTime(vaf511_hang.B03.GetValue());
            cttt_chitiet.SoTienTruLui = System.Convert.ToDecimal(vaf511_hang.B04.GetValue());
            cttt_chitiet.SoTienTangLen = System.Convert.ToDecimal(vaf511_hang.B05.GetValue());
            cttt_chitiet.SoToKhai = System.Convert.ToDecimal(vaf511_hang.B06.GetValue());
            cttt_chitiet.NgayDangKyToKhai = System.Convert.ToDateTime(vaf511_hang.B07.GetValue());
            cttt_chitiet.MaLoaiHinh = vaf511_hang.B08.GetValue().ToString();
            cttt_chitiet.CoQuanHaiQuan = vaf511_hang.B09.GetValue().ToString();
            cttt_chitiet.TenCucHaiQuan = vaf511_hang.B10.GetValue().ToString();
            if (cttt_chitiet.SoThuTuGiaoDich != null && cttt_chitiet.SoThuTuGiaoDich != 0)
                return cttt_chitiet;
            else
                return null;
        }


        #endregion
    }
}