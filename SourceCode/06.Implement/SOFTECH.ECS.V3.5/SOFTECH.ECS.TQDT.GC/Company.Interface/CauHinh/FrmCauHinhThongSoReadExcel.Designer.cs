﻿namespace Company.Interface.CauHinh
{
    partial class FrmCauHinhThongSoReadExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCauHinhThongSoReadExcel));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.nmrDonGiaNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.nmrSoLuongNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.grbGC = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNguonCungCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtSoLuongNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkTCU = new Janus.Windows.EditControls.UICheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDonGiaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cbbSheetName = new Janus.Windows.EditControls.UIComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDVTNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRowNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.nmrDonGiaSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.nmrSoLuongSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.grbSP = new Janus.Windows.EditControls.UIGroupBox();
            this.txtLoaiSPGC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtSoLuongSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtDonGiaSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.uiComboBox1 = new Janus.Windows.EditControls.UIComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtDVTSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtRowSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.nmrDonGiaTB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.nmrSoLuongTB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiComboBox2 = new Janus.Windows.EditControls.UIComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.txtSoLuongTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonGiaTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtXuatXu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinhTrang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguyenTe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.txtDVTTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.txtRowTB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.nmrTLHH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.nmrDMSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiComboBox3 = new Janus.Windows.EditControls.UIComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.chkOverwrite = new Janus.Windows.EditControls.UICheckBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtNPLCungUng = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label73 = new System.Windows.Forms.Label();
            this.txtTLHH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDMSD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguyenPhuLieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaSanPham = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.txtRowDM = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage5 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.mnrTriGiaHH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.nmrDonGiaHH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.nmrSoLuong2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label98 = new System.Windows.Forms.Label();
            this.nmrSoLuong1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiComboBox4 = new Janus.Windows.EditControls.UIComboBox();
            this.txtXuatXuColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuongColumn2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label88 = new System.Windows.Forms.Label();
            this.txtTriGiaTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.txtDVTColumn2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label94 = new System.Windows.Forms.Label();
            this.txtDVTColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblXuatXu = new System.Windows.Forms.Label();
            this.txtSoLuongColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblThue = new System.Windows.Forms.Label();
            this.txtThueSuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label95 = new System.Windows.Forms.Label();
            this.txtBieuThueXNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label96 = new System.Windows.Forms.Label();
            this.txtDonGiaColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbGC)).BeginInit();
            this.grbGC.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbSP)).BeginInit();
            this.grbSP.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            this.uiTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1098, 450);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 401);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1098, 49);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSave.Location = new System.Drawing.Point(455, 17);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(95, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Lưu lại";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1015, 17);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(71, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1098, 401);
            this.uiTab1.TabIndex = 1;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4,
            this.uiTabPage5});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.uiGroupBox3);
            this.uiTabPage1.Controls.Add(this.uiGroupBox2);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 22);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1096, 378);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Cấu hình đọc Excel Nguyên phụ liệu";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.nmrDonGiaNPL);
            this.uiGroupBox3.Controls.Add(this.nmrSoLuongNPL);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Controls.Add(this.label100);
            this.uiGroupBox3.Controls.Add(this.label99);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 222);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1096, 156);
            this.uiGroupBox3.TabIndex = 3;
            this.uiGroupBox3.Text = "Cấu hình số thập phân";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // nmrDonGiaNPL
            // 
            this.nmrDonGiaNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrDonGiaNPL.Location = new System.Drawing.Point(106, 67);
            this.nmrDonGiaNPL.Name = "nmrDonGiaNPL";
            this.nmrDonGiaNPL.Size = new System.Drawing.Size(172, 22);
            this.nmrDonGiaNPL.TabIndex = 13;
            this.nmrDonGiaNPL.Text = "4";
            this.nmrDonGiaNPL.Value = 4;
            this.nmrDonGiaNPL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrDonGiaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // nmrSoLuongNPL
            // 
            this.nmrSoLuongNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrSoLuongNPL.Location = new System.Drawing.Point(106, 30);
            this.nmrSoLuongNPL.Name = "nmrSoLuongNPL";
            this.nmrSoLuongNPL.Size = new System.Drawing.Size(172, 22);
            this.nmrSoLuongNPL.TabIndex = 13;
            this.nmrSoLuongNPL.Text = "4";
            this.nmrSoLuongNPL.Value = 4;
            this.nmrSoLuongNPL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrSoLuongNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(15, 71);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 14);
            this.label21.TabIndex = 12;
            this.label21.Text = " Đơn giá";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(15, 34);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 14);
            this.label20.TabIndex = 12;
            this.label20.Text = "Số lượng";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.BackColor = System.Drawing.Color.Transparent;
            this.label100.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Red;
            this.label100.Location = new System.Drawing.Point(284, 71);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(74, 14);
            this.label100.TabIndex = 6;
            this.label100.Text = "Mặc định : 4";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.BackColor = System.Drawing.Color.Transparent;
            this.label99.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Red;
            this.label99.Location = new System.Drawing.Point(284, 35);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(74, 14);
            this.label99.TabIndex = 6;
            this.label99.Text = "Mặc định : 4";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.grbGC);
            this.uiGroupBox2.Controls.Add(this.cbbSheetName);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.txtDVTNPL);
            this.uiGroupBox2.Controls.Add(this.txtMaHSNPL);
            this.uiGroupBox2.Controls.Add(this.txtTenNPL);
            this.uiGroupBox2.Controls.Add(this.txtMaNPL);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtRowNPL);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1096, 222);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Thông số cấu hình";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grbGC
            // 
            this.grbGC.BorderColor = System.Drawing.Color.Transparent;
            this.grbGC.Controls.Add(this.txtNguonCungCap);
            this.grbGC.Controls.Add(this.label1);
            this.grbGC.Controls.Add(this.label16);
            this.grbGC.Controls.Add(this.txtSoLuongNPL);
            this.grbGC.Controls.Add(this.chkTCU);
            this.grbGC.Controls.Add(this.label15);
            this.grbGC.Controls.Add(this.label19);
            this.grbGC.Controls.Add(this.txtDonGiaNPL);
            this.grbGC.Controls.Add(this.label18);
            this.grbGC.Controls.Add(this.label17);
            this.grbGC.Location = new System.Drawing.Point(301, 45);
            this.grbGC.Margin = new System.Windows.Forms.Padding(0);
            this.grbGC.Name = "grbGC";
            this.grbGC.Size = new System.Drawing.Size(315, 149);
            this.grbGC.TabIndex = 19;
            this.grbGC.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtNguonCungCap
            // 
            this.txtNguonCungCap.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguonCungCap.Location = new System.Drawing.Point(13, 111);
            this.txtNguonCungCap.MaxLength = 1;
            this.txtNguonCungCap.Name = "txtNguonCungCap";
            this.txtNguonCungCap.Size = new System.Drawing.Size(270, 22);
            this.txtNguonCungCap.TabIndex = 10;
            this.txtNguonCungCap.Text = "Bên thuê gia công cung cấp";
            this.txtNguonCungCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Cột số lượng";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(10, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(101, 14);
            this.label16.TabIndex = 18;
            this.label16.Text = "Nguồn cung cấp:";
            // 
            // txtSoLuongNPL
            // 
            this.txtSoLuongNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongNPL.Location = new System.Drawing.Point(114, 10);
            this.txtSoLuongNPL.MaxLength = 1;
            this.txtSoLuongNPL.Name = "txtSoLuongNPL";
            this.txtSoLuongNPL.Size = new System.Drawing.Size(169, 22);
            this.txtSoLuongNPL.TabIndex = 7;
            this.txtSoLuongNPL.Text = "E";
            this.txtSoLuongNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chkTCU
            // 
            this.chkTCU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTCU.Location = new System.Drawing.Point(114, 75);
            this.chkTCU.Name = "chkTCU";
            this.chkTCU.Size = new System.Drawing.Size(87, 18);
            this.chkTCU.TabIndex = 9;
            this.chkTCU.Text = "Tự cung ứng";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(10, 46);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 14);
            this.label15.TabIndex = 12;
            this.label15.Text = "Cột đơn giá";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(286, 77);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(14, 14);
            this.label19.TabIndex = 5;
            this.label19.Text = "*";
            // 
            // txtDonGiaNPL
            // 
            this.txtDonGiaNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaNPL.Location = new System.Drawing.Point(114, 42);
            this.txtDonGiaNPL.MaxLength = 1;
            this.txtDonGiaNPL.Name = "txtDonGiaNPL";
            this.txtDonGiaNPL.Size = new System.Drawing.Size(169, 22);
            this.txtDonGiaNPL.TabIndex = 8;
            this.txtDonGiaNPL.Text = "F";
            this.txtDonGiaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(286, 46);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(14, 14);
            this.label18.TabIndex = 5;
            this.label18.Text = "*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(286, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 14);
            this.label17.TabIndex = 5;
            this.label17.Text = "*";
            // 
            // cbbSheetName
            // 
            this.cbbSheetName.DisplayMember = "Name";
            this.cbbSheetName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSheetName.Location = new System.Drawing.Point(106, 20);
            this.cbbSheetName.Name = "cbbSheetName";
            this.cbbSheetName.Size = new System.Drawing.Size(172, 22);
            this.cbbSheetName.TabIndex = 1;
            this.cbbSheetName.Tag = "PhuongThucThanhToan";
            this.cbbSheetName.ValueMember = "ID";
            this.cbbSheetName.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(587, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 14);
            this.label14.TabIndex = 5;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(281, 163);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 14);
            this.label13.TabIndex = 6;
            this.label13.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(281, 127);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 14);
            this.label12.TabIndex = 16;
            this.label12.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(281, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 14);
            this.label10.TabIndex = 15;
            this.label10.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(281, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 14);
            this.label8.TabIndex = 14;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(281, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 14);
            this.label7.TabIndex = 4;
            this.label7.Text = "*";
            // 
            // txtDVTNPL
            // 
            this.txtDVTNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTNPL.Location = new System.Drawing.Point(415, 20);
            this.txtDVTNPL.MaxLength = 1;
            this.txtDVTNPL.Name = "txtDVTNPL";
            this.txtDVTNPL.Size = new System.Drawing.Size(169, 22);
            this.txtDVTNPL.TabIndex = 6;
            this.txtDVTNPL.Text = "D";
            this.txtDVTNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHSNPL
            // 
            this.txtMaHSNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSNPL.Location = new System.Drawing.Point(106, 159);
            this.txtMaHSNPL.MaxLength = 1;
            this.txtMaHSNPL.Name = "txtMaHSNPL";
            this.txtMaHSNPL.Size = new System.Drawing.Size(172, 22);
            this.txtMaHSNPL.TabIndex = 5;
            this.txtMaHSNPL.Text = "C";
            this.txtMaHSNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNPL.Location = new System.Drawing.Point(106, 123);
            this.txtTenNPL.MaxLength = 1;
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.Size = new System.Drawing.Size(172, 22);
            this.txtTenNPL.TabIndex = 4;
            this.txtTenNPL.Text = "B";
            this.txtTenNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPL.Location = new System.Drawing.Point(106, 87);
            this.txtMaNPL.MaxLength = 1;
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(172, 22);
            this.txtMaNPL.TabIndex = 3;
            this.txtMaNPL.Text = "A";
            this.txtMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Sheet";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cột mã NPL";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(11, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 14);
            this.label11.TabIndex = 2;
            this.label11.Text = "Dòng đầu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cột tên NPL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cột mã HS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(311, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 14);
            this.label5.TabIndex = 10;
            this.label5.Text = "Cột ĐVT";
            // 
            // txtRowNPL
            // 
            this.txtRowNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRowNPL.Location = new System.Drawing.Point(106, 54);
            this.txtRowNPL.Name = "txtRowNPL";
            this.txtRowNPL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtRowNPL.Size = new System.Drawing.Size(172, 22);
            this.txtRowNPL.TabIndex = 2;
            this.txtRowNPL.Text = "2";
            this.txtRowNPL.Value = 2;
            this.txtRowNPL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRowNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.uiGroupBox5);
            this.uiTabPage2.Controls.Add(this.uiGroupBox4);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 22);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1096, 378);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Cấu hình đọc Excel Sản phẩm";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.label102);
            this.uiGroupBox5.Controls.Add(this.label101);
            this.uiGroupBox5.Controls.Add(this.nmrDonGiaSP);
            this.uiGroupBox5.Controls.Add(this.nmrSoLuongSP);
            this.uiGroupBox5.Controls.Add(this.label40);
            this.uiGroupBox5.Controls.Add(this.label42);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 222);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1096, 156);
            this.uiGroupBox5.TabIndex = 4;
            this.uiGroupBox5.Text = "Cấu hình số thập phân";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.BackColor = System.Drawing.Color.Transparent;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Red;
            this.label102.Location = new System.Drawing.Point(284, 72);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(74, 14);
            this.label102.TabIndex = 14;
            this.label102.Text = "Mặc định : 4";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.BackColor = System.Drawing.Color.Transparent;
            this.label101.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Red;
            this.label101.Location = new System.Drawing.Point(284, 35);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(74, 14);
            this.label101.TabIndex = 14;
            this.label101.Text = "Mặc định : 4";
            // 
            // nmrDonGiaSP
            // 
            this.nmrDonGiaSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrDonGiaSP.Location = new System.Drawing.Point(106, 67);
            this.nmrDonGiaSP.Name = "nmrDonGiaSP";
            this.nmrDonGiaSP.Size = new System.Drawing.Size(172, 22);
            this.nmrDonGiaSP.TabIndex = 13;
            this.nmrDonGiaSP.Text = "4";
            this.nmrDonGiaSP.Value = 4;
            this.nmrDonGiaSP.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrDonGiaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // nmrSoLuongSP
            // 
            this.nmrSoLuongSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrSoLuongSP.Location = new System.Drawing.Point(106, 30);
            this.nmrSoLuongSP.Name = "nmrSoLuongSP";
            this.nmrSoLuongSP.Size = new System.Drawing.Size(172, 22);
            this.nmrSoLuongSP.TabIndex = 13;
            this.nmrSoLuongSP.Text = "4";
            this.nmrSoLuongSP.Value = 4;
            this.nmrSoLuongSP.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrSoLuongSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(15, 71);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(48, 14);
            this.label40.TabIndex = 12;
            this.label40.Text = "Đơn giá";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(15, 34);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(56, 14);
            this.label42.TabIndex = 12;
            this.label42.Text = "Số lượng";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.grbSP);
            this.uiGroupBox4.Controls.Add(this.uiComboBox1);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.label27);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.label29);
            this.uiGroupBox4.Controls.Add(this.label30);
            this.uiGroupBox4.Controls.Add(this.label31);
            this.uiGroupBox4.Controls.Add(this.txtDVTSP);
            this.uiGroupBox4.Controls.Add(this.txtMaHSSP);
            this.uiGroupBox4.Controls.Add(this.txtTenSP);
            this.uiGroupBox4.Controls.Add(this.txtMaSP);
            this.uiGroupBox4.Controls.Add(this.label34);
            this.uiGroupBox4.Controls.Add(this.label35);
            this.uiGroupBox4.Controls.Add(this.label36);
            this.uiGroupBox4.Controls.Add(this.label37);
            this.uiGroupBox4.Controls.Add(this.label38);
            this.uiGroupBox4.Controls.Add(this.label39);
            this.uiGroupBox4.Controls.Add(this.txtRowSP);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1096, 222);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Thông số cấu hình";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grbSP
            // 
            this.grbSP.BorderColor = System.Drawing.Color.Transparent;
            this.grbSP.Controls.Add(this.txtLoaiSPGC);
            this.grbSP.Controls.Add(this.label33);
            this.grbSP.Controls.Add(this.txtSoLuongSP);
            this.grbSP.Controls.Add(this.label22);
            this.grbSP.Controls.Add(this.label32);
            this.grbSP.Controls.Add(this.label23);
            this.grbSP.Controls.Add(this.txtDonGiaSP);
            this.grbSP.Controls.Add(this.label24);
            this.grbSP.Controls.Add(this.label25);
            this.grbSP.Location = new System.Drawing.Point(301, 41);
            this.grbSP.Name = "grbSP";
            this.grbSP.Size = new System.Drawing.Size(313, 117);
            this.grbSP.TabIndex = 21;
            this.grbSP.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtLoaiSPGC
            // 
            this.txtLoaiSPGC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLoaiSPGC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoaiSPGC.Location = new System.Drawing.Point(114, 78);
            this.txtLoaiSPGC.MaxLength = 1;
            this.txtLoaiSPGC.Name = "txtLoaiSPGC";
            this.txtLoaiSPGC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtLoaiSPGC.Size = new System.Drawing.Size(169, 22);
            this.txtLoaiSPGC.TabIndex = 9;
            this.txtLoaiSPGC.Text = "G";
            this.txtLoaiSPGC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(10, 15);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(77, 14);
            this.label33.TabIndex = 12;
            this.label33.Text = "Cột số lượng";
            // 
            // txtSoLuongSP
            // 
            this.txtSoLuongSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongSP.Location = new System.Drawing.Point(114, 11);
            this.txtSoLuongSP.MaxLength = 1;
            this.txtSoLuongSP.Name = "txtSoLuongSP";
            this.txtSoLuongSP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSoLuongSP.Size = new System.Drawing.Size(169, 22);
            this.txtSoLuongSP.TabIndex = 7;
            this.txtSoLuongSP.Text = "E";
            this.txtSoLuongSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(10, 82);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 14);
            this.label22.TabIndex = 14;
            this.label22.Text = "Cột Loại SPGC";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(10, 46);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(70, 14);
            this.label32.TabIndex = 14;
            this.label32.Text = "Cột đơn giá";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(289, 82);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 14);
            this.label23.TabIndex = 19;
            this.label23.Text = "*";
            // 
            // txtDonGiaSP
            // 
            this.txtDonGiaSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaSP.Location = new System.Drawing.Point(114, 42);
            this.txtDonGiaSP.MaxLength = 1;
            this.txtDonGiaSP.Name = "txtDonGiaSP";
            this.txtDonGiaSP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDonGiaSP.Size = new System.Drawing.Size(169, 22);
            this.txtDonGiaSP.TabIndex = 8;
            this.txtDonGiaSP.Text = "F";
            this.txtDonGiaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(286, 47);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 14);
            this.label24.TabIndex = 19;
            this.label24.Text = "*";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(286, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 14);
            this.label25.TabIndex = 19;
            this.label25.Text = "*";
            // 
            // uiComboBox1
            // 
            this.uiComboBox1.DisplayMember = "Name";
            this.uiComboBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiComboBox1.Location = new System.Drawing.Point(106, 20);
            this.uiComboBox1.Name = "uiComboBox1";
            this.uiComboBox1.Size = new System.Drawing.Size(172, 22);
            this.uiComboBox1.TabIndex = 1;
            this.uiComboBox1.Tag = "PhuongThucThanhToan";
            this.uiComboBox1.ValueMember = "ID";
            this.uiComboBox1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(587, 24);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 14);
            this.label26.TabIndex = 18;
            this.label26.Text = "*";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(281, 163);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(14, 14);
            this.label27.TabIndex = 17;
            this.label27.Text = "*";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(281, 127);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(14, 14);
            this.label28.TabIndex = 17;
            this.label28.Text = "*";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(281, 92);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(14, 14);
            this.label29.TabIndex = 17;
            this.label29.Text = "*";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(281, 58);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(14, 14);
            this.label30.TabIndex = 16;
            this.label30.Text = "*";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(281, 24);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(14, 14);
            this.label31.TabIndex = 4;
            this.label31.Text = "*";
            // 
            // txtDVTSP
            // 
            this.txtDVTSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTSP.Location = new System.Drawing.Point(415, 20);
            this.txtDVTSP.MaxLength = 1;
            this.txtDVTSP.Name = "txtDVTSP";
            this.txtDVTSP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDVTSP.Size = new System.Drawing.Size(169, 22);
            this.txtDVTSP.TabIndex = 6;
            this.txtDVTSP.Text = "D";
            this.txtDVTSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHSSP
            // 
            this.txtMaHSSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSSP.Location = new System.Drawing.Point(106, 159);
            this.txtMaHSSP.MaxLength = 1;
            this.txtMaHSSP.Name = "txtMaHSSP";
            this.txtMaHSSP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMaHSSP.Size = new System.Drawing.Size(172, 22);
            this.txtMaHSSP.TabIndex = 5;
            this.txtMaHSSP.Text = "C";
            this.txtMaHSSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenSP
            // 
            this.txtTenSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenSP.Location = new System.Drawing.Point(106, 123);
            this.txtTenSP.MaxLength = 1;
            this.txtTenSP.Name = "txtTenSP";
            this.txtTenSP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTenSP.Size = new System.Drawing.Size(172, 22);
            this.txtTenSP.TabIndex = 4;
            this.txtTenSP.Text = "B";
            this.txtTenSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaSP
            // 
            this.txtMaSP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSP.Location = new System.Drawing.Point(106, 87);
            this.txtMaSP.MaxLength = 1;
            this.txtMaSP.Name = "txtMaSP";
            this.txtMaSP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMaSP.Size = new System.Drawing.Size(172, 22);
            this.txtMaSP.TabIndex = 3;
            this.txtMaSP.Text = "A";
            this.txtMaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(11, 24);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 14);
            this.label34.TabIndex = 0;
            this.label34.Text = "Tên Sheet";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(11, 91);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(64, 14);
            this.label35.TabIndex = 4;
            this.label35.Text = "Cột mã SP";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(11, 58);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(60, 14);
            this.label36.TabIndex = 2;
            this.label36.Text = "Dòng đầu";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(11, 127);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(67, 14);
            this.label37.TabIndex = 6;
            this.label37.Text = "Cột tên SP";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(11, 163);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 14);
            this.label38.TabIndex = 8;
            this.label38.Text = "Cột mã HS";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(311, 24);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 14);
            this.label39.TabIndex = 10;
            this.label39.Text = "Cột ĐVT";
            // 
            // txtRowSP
            // 
            this.txtRowSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRowSP.Location = new System.Drawing.Point(106, 54);
            this.txtRowSP.Name = "txtRowSP";
            this.txtRowSP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtRowSP.Size = new System.Drawing.Size(172, 22);
            this.txtRowSP.TabIndex = 2;
            this.txtRowSP.Text = "2";
            this.txtRowSP.Value = 2;
            this.txtRowSP.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRowSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.uiGroupBox7);
            this.uiTabPage3.Controls.Add(this.uiGroupBox6);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 22);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1096, 378);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Cấu hình đọc Excel Thiết bị";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.label104);
            this.uiGroupBox7.Controls.Add(this.label103);
            this.uiGroupBox7.Controls.Add(this.nmrDonGiaTB);
            this.uiGroupBox7.Controls.Add(this.nmrSoLuongTB);
            this.uiGroupBox7.Controls.Add(this.label65);
            this.uiGroupBox7.Controls.Add(this.label67);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 222);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1096, 156);
            this.uiGroupBox7.TabIndex = 4;
            this.uiGroupBox7.Text = "Cấu hình số thập phân";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.BackColor = System.Drawing.Color.Transparent;
            this.label104.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Red;
            this.label104.Location = new System.Drawing.Point(284, 71);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(74, 14);
            this.label104.TabIndex = 14;
            this.label104.Text = "Mặc định : 4";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.BackColor = System.Drawing.Color.Transparent;
            this.label103.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Red;
            this.label103.Location = new System.Drawing.Point(284, 34);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(74, 14);
            this.label103.TabIndex = 14;
            this.label103.Text = "Mặc định : 4";
            // 
            // nmrDonGiaTB
            // 
            this.nmrDonGiaTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrDonGiaTB.Location = new System.Drawing.Point(106, 67);
            this.nmrDonGiaTB.Name = "nmrDonGiaTB";
            this.nmrDonGiaTB.Size = new System.Drawing.Size(172, 22);
            this.nmrDonGiaTB.TabIndex = 13;
            this.nmrDonGiaTB.Text = "4";
            this.nmrDonGiaTB.Value = 4;
            this.nmrDonGiaTB.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrDonGiaTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // nmrSoLuongTB
            // 
            this.nmrSoLuongTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrSoLuongTB.Location = new System.Drawing.Point(106, 30);
            this.nmrSoLuongTB.Name = "nmrSoLuongTB";
            this.nmrSoLuongTB.Size = new System.Drawing.Size(172, 22);
            this.nmrSoLuongTB.TabIndex = 13;
            this.nmrSoLuongTB.Text = "4";
            this.nmrSoLuongTB.Value = 4;
            this.nmrSoLuongTB.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrSoLuongTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(15, 71);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(48, 14);
            this.label65.TabIndex = 12;
            this.label65.Text = "Đơn giá";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(15, 34);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(56, 14);
            this.label67.TabIndex = 12;
            this.label67.Text = "Số lượng";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.uiComboBox2);
            this.uiGroupBox6.Controls.Add(this.label43);
            this.uiGroupBox6.Controls.Add(this.label44);
            this.uiGroupBox6.Controls.Add(this.label45);
            this.uiGroupBox6.Controls.Add(this.label46);
            this.uiGroupBox6.Controls.Add(this.label47);
            this.uiGroupBox6.Controls.Add(this.label48);
            this.uiGroupBox6.Controls.Add(this.label49);
            this.uiGroupBox6.Controls.Add(this.label50);
            this.uiGroupBox6.Controls.Add(this.label51);
            this.uiGroupBox6.Controls.Add(this.label52);
            this.uiGroupBox6.Controls.Add(this.label53);
            this.uiGroupBox6.Controls.Add(this.txtSoLuongTB);
            this.uiGroupBox6.Controls.Add(this.txtDonGiaTB);
            this.uiGroupBox6.Controls.Add(this.txtXuatXu);
            this.uiGroupBox6.Controls.Add(this.txtTinhTrang);
            this.uiGroupBox6.Controls.Add(this.txtNguyenTe);
            this.uiGroupBox6.Controls.Add(this.label54);
            this.uiGroupBox6.Controls.Add(this.label55);
            this.uiGroupBox6.Controls.Add(this.label56);
            this.uiGroupBox6.Controls.Add(this.label57);
            this.uiGroupBox6.Controls.Add(this.label58);
            this.uiGroupBox6.Controls.Add(this.txtDVTTB);
            this.uiGroupBox6.Controls.Add(this.txtMaHSTB);
            this.uiGroupBox6.Controls.Add(this.txtTenTB);
            this.uiGroupBox6.Controls.Add(this.txtMaTB);
            this.uiGroupBox6.Controls.Add(this.label59);
            this.uiGroupBox6.Controls.Add(this.label60);
            this.uiGroupBox6.Controls.Add(this.label61);
            this.uiGroupBox6.Controls.Add(this.label62);
            this.uiGroupBox6.Controls.Add(this.label63);
            this.uiGroupBox6.Controls.Add(this.label64);
            this.uiGroupBox6.Controls.Add(this.txtRowTB);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1096, 222);
            this.uiGroupBox6.TabIndex = 1;
            this.uiGroupBox6.Text = "Thông số cấu hình";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiComboBox2
            // 
            this.uiComboBox2.DisplayMember = "Name";
            this.uiComboBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiComboBox2.Location = new System.Drawing.Point(106, 20);
            this.uiComboBox2.Name = "uiComboBox2";
            this.uiComboBox2.Size = new System.Drawing.Size(172, 22);
            this.uiComboBox2.TabIndex = 1;
            this.uiComboBox2.Tag = "PhuongThucThanhToan";
            this.uiComboBox2.ValueMember = "ID";
            this.uiComboBox2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Red;
            this.label43.Location = new System.Drawing.Point(590, 157);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(14, 14);
            this.label43.TabIndex = 3;
            this.label43.Text = "*";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(590, 123);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(14, 14);
            this.label44.TabIndex = 3;
            this.label44.Text = "*";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(590, 89);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(14, 14);
            this.label45.TabIndex = 27;
            this.label45.Text = "*";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(590, 54);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(14, 14);
            this.label46.TabIndex = 26;
            this.label46.Text = "*";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Red;
            this.label47.Location = new System.Drawing.Point(590, 24);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(14, 14);
            this.label47.TabIndex = 25;
            this.label47.Text = "*";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Red;
            this.label48.Location = new System.Drawing.Point(282, 58);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(14, 14);
            this.label48.TabIndex = 24;
            this.label48.Text = "*";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Red;
            this.label49.Location = new System.Drawing.Point(282, 193);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(14, 14);
            this.label49.TabIndex = 23;
            this.label49.Text = "*";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Location = new System.Drawing.Point(282, 163);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(14, 14);
            this.label50.TabIndex = 23;
            this.label50.Text = "*";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Red;
            this.label51.Location = new System.Drawing.Point(282, 125);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(14, 14);
            this.label51.TabIndex = 23;
            this.label51.Text = "*";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Red;
            this.label52.Location = new System.Drawing.Point(282, 91);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(14, 14);
            this.label52.TabIndex = 22;
            this.label52.Text = "*";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Red;
            this.label53.Location = new System.Drawing.Point(282, 24);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(14, 14);
            this.label53.TabIndex = 2;
            this.label53.Text = "*";
            // 
            // txtSoLuongTB
            // 
            this.txtSoLuongTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTB.Location = new System.Drawing.Point(415, 53);
            this.txtSoLuongTB.MaxLength = 1;
            this.txtSoLuongTB.Name = "txtSoLuongTB";
            this.txtSoLuongTB.Size = new System.Drawing.Size(169, 22);
            this.txtSoLuongTB.TabIndex = 8;
            this.txtSoLuongTB.Text = "F";
            this.txtSoLuongTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDonGiaTB
            // 
            this.txtDonGiaTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTB.Location = new System.Drawing.Point(415, 84);
            this.txtDonGiaTB.MaxLength = 1;
            this.txtDonGiaTB.Name = "txtDonGiaTB";
            this.txtDonGiaTB.Size = new System.Drawing.Size(169, 22);
            this.txtDonGiaTB.TabIndex = 9;
            this.txtDonGiaTB.Text = "G";
            this.txtDonGiaTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtXuatXu
            // 
            this.txtXuatXu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXuatXu.Location = new System.Drawing.Point(415, 20);
            this.txtXuatXu.Name = "txtXuatXu";
            this.txtXuatXu.Size = new System.Drawing.Size(169, 22);
            this.txtXuatXu.TabIndex = 7;
            this.txtXuatXu.Text = "E";
            this.txtXuatXu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTinhTrang
            // 
            this.txtTinhTrang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTinhTrang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTinhTrang.Location = new System.Drawing.Point(415, 152);
            this.txtTinhTrang.MaxLength = 1;
            this.txtTinhTrang.Name = "txtTinhTrang";
            this.txtTinhTrang.Size = new System.Drawing.Size(169, 22);
            this.txtTinhTrang.TabIndex = 11;
            this.txtTinhTrang.Text = "I";
            this.txtTinhTrang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNguyenTe
            // 
            this.txtNguyenTe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNguyenTe.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguyenTe.Location = new System.Drawing.Point(415, 118);
            this.txtNguyenTe.MaxLength = 1;
            this.txtNguyenTe.Name = "txtNguyenTe";
            this.txtNguyenTe.Size = new System.Drawing.Size(169, 22);
            this.txtNguyenTe.TabIndex = 10;
            this.txtNguyenTe.Text = "H";
            this.txtNguyenTe.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(311, 24);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(72, 14);
            this.label54.TabIndex = 12;
            this.label54.Text = "Cột xuất xứ";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(311, 91);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(70, 14);
            this.label55.TabIndex = 16;
            this.label55.Text = "Cột đơn giá";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(311, 58);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(56, 14);
            this.label56.TabIndex = 14;
            this.label56.Text = "Số lượng";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(311, 127);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(87, 14);
            this.label57.TabIndex = 18;
            this.label57.Text = "Cột nguyên tệ";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(311, 163);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(84, 14);
            this.label58.TabIndex = 20;
            this.label58.Text = "Cột tình trạng";
            // 
            // txtDVTTB
            // 
            this.txtDVTTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTTB.Location = new System.Drawing.Point(106, 189);
            this.txtDVTTB.MaxLength = 1;
            this.txtDVTTB.Name = "txtDVTTB";
            this.txtDVTTB.Size = new System.Drawing.Size(172, 22);
            this.txtDVTTB.TabIndex = 6;
            this.txtDVTTB.Text = "D";
            this.txtDVTTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHSTB
            // 
            this.txtMaHSTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSTB.Location = new System.Drawing.Point(106, 158);
            this.txtMaHSTB.MaxLength = 1;
            this.txtMaHSTB.Name = "txtMaHSTB";
            this.txtMaHSTB.Size = new System.Drawing.Size(172, 22);
            this.txtMaHSTB.TabIndex = 5;
            this.txtMaHSTB.Text = "C";
            this.txtMaHSTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenTB
            // 
            this.txtTenTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenTB.Location = new System.Drawing.Point(106, 122);
            this.txtTenTB.MaxLength = 1;
            this.txtTenTB.Name = "txtTenTB";
            this.txtTenTB.Size = new System.Drawing.Size(172, 22);
            this.txtTenTB.TabIndex = 4;
            this.txtTenTB.Text = "B";
            this.txtTenTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaTB
            // 
            this.txtMaTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaTB.Location = new System.Drawing.Point(106, 86);
            this.txtMaTB.MaxLength = 1;
            this.txtMaTB.Name = "txtMaTB";
            this.txtMaTB.Size = new System.Drawing.Size(172, 22);
            this.txtMaTB.TabIndex = 3;
            this.txtMaTB.Text = "A";
            this.txtMaTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(11, 24);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(66, 14);
            this.label59.TabIndex = 0;
            this.label59.Text = "Tên Sheet";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(11, 91);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(65, 14);
            this.label60.TabIndex = 4;
            this.label60.Text = "Cột mã TB";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(11, 58);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(60, 14);
            this.label61.TabIndex = 2;
            this.label61.Text = "Dòng đầu";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(11, 127);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(68, 14);
            this.label62.TabIndex = 6;
            this.label62.Text = "Cột tên TB";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(11, 163);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(65, 14);
            this.label63.TabIndex = 8;
            this.label63.Text = "Cột mã HS";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(11, 194);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(54, 14);
            this.label64.TabIndex = 10;
            this.label64.Text = "Cột ĐVT";
            // 
            // txtRowTB
            // 
            this.txtRowTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRowTB.Location = new System.Drawing.Point(106, 53);
            this.txtRowTB.Name = "txtRowTB";
            this.txtRowTB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtRowTB.Size = new System.Drawing.Size(172, 22);
            this.txtRowTB.TabIndex = 2;
            this.txtRowTB.Text = "2";
            this.txtRowTB.Value = 2;
            this.txtRowTB.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRowTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.uiGroupBox9);
            this.uiTabPage4.Controls.Add(this.uiGroupBox8);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 22);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1096, 378);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Cấu hình đọc Excel Định mức";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.label106);
            this.uiGroupBox9.Controls.Add(this.label105);
            this.uiGroupBox9.Controls.Add(this.nmrTLHH);
            this.uiGroupBox9.Controls.Add(this.nmrDMSD);
            this.uiGroupBox9.Controls.Add(this.label81);
            this.uiGroupBox9.Controls.Add(this.label82);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox9.Location = new System.Drawing.Point(0, 222);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(1096, 156);
            this.uiGroupBox9.TabIndex = 4;
            this.uiGroupBox9.Text = "Cấu hình số thập phân";
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.BackColor = System.Drawing.Color.Transparent;
            this.label106.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Red;
            this.label106.Location = new System.Drawing.Point(284, 71);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(74, 14);
            this.label106.TabIndex = 14;
            this.label106.Text = "Mặc định : 1";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.BackColor = System.Drawing.Color.Transparent;
            this.label105.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Red;
            this.label105.Location = new System.Drawing.Point(284, 34);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(74, 14);
            this.label105.TabIndex = 14;
            this.label105.Text = "Mặc định : 8";
            // 
            // nmrTLHH
            // 
            this.nmrTLHH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrTLHH.Location = new System.Drawing.Point(106, 67);
            this.nmrTLHH.Name = "nmrTLHH";
            this.nmrTLHH.Size = new System.Drawing.Size(172, 22);
            this.nmrTLHH.TabIndex = 13;
            this.nmrTLHH.Text = "1";
            this.nmrTLHH.Value = 1;
            this.nmrTLHH.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrTLHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // nmrDMSD
            // 
            this.nmrDMSD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrDMSD.Location = new System.Drawing.Point(106, 30);
            this.nmrDMSD.Name = "nmrDMSD";
            this.nmrDMSD.Size = new System.Drawing.Size(172, 22);
            this.nmrDMSD.TabIndex = 13;
            this.nmrDMSD.Text = "8";
            this.nmrDMSD.Value = 8;
            this.nmrDMSD.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrDMSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.BackColor = System.Drawing.Color.Transparent;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(15, 71);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(37, 14);
            this.label81.TabIndex = 12;
            this.label81.Text = "TLHH";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.BackColor = System.Drawing.Color.Transparent;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(15, 34);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(39, 14);
            this.label82.TabIndex = 12;
            this.label82.Text = "ĐMSD";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.uiComboBox3);
            this.uiGroupBox8.Controls.Add(this.label9);
            this.uiGroupBox8.Controls.Add(this.label41);
            this.uiGroupBox8.Controls.Add(this.label66);
            this.uiGroupBox8.Controls.Add(this.chkOverwrite);
            this.uiGroupBox8.Controls.Add(this.label68);
            this.uiGroupBox8.Controls.Add(this.label69);
            this.uiGroupBox8.Controls.Add(this.label70);
            this.uiGroupBox8.Controls.Add(this.label71);
            this.uiGroupBox8.Controls.Add(this.label72);
            this.uiGroupBox8.Controls.Add(this.txtNPLCungUng);
            this.uiGroupBox8.Controls.Add(this.label73);
            this.uiGroupBox8.Controls.Add(this.txtTLHH);
            this.uiGroupBox8.Controls.Add(this.txtDMSD);
            this.uiGroupBox8.Controls.Add(this.txtMaDD);
            this.uiGroupBox8.Controls.Add(this.txtMaNguyenPhuLieu);
            this.uiGroupBox8.Controls.Add(this.txtMaSanPham);
            this.uiGroupBox8.Controls.Add(this.label74);
            this.uiGroupBox8.Controls.Add(this.label75);
            this.uiGroupBox8.Controls.Add(this.label76);
            this.uiGroupBox8.Controls.Add(this.label77);
            this.uiGroupBox8.Controls.Add(this.label78);
            this.uiGroupBox8.Controls.Add(this.label79);
            this.uiGroupBox8.Controls.Add(this.label80);
            this.uiGroupBox8.Controls.Add(this.txtRowDM);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1096, 222);
            this.uiGroupBox8.TabIndex = 1;
            this.uiGroupBox8.Text = "Thông số cấu hình";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiComboBox3
            // 
            this.uiComboBox3.DisplayMember = "Name";
            this.uiComboBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiComboBox3.Location = new System.Drawing.Point(106, 20);
            this.uiComboBox3.Name = "uiComboBox3";
            this.uiComboBox3.Size = new System.Drawing.Size(177, 22);
            this.uiComboBox3.TabIndex = 1;
            this.uiComboBox3.Tag = "PhuongThucThanhToan";
            this.uiComboBox3.ValueMember = "ID";
            this.uiComboBox3.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(572, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "*";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(286, 57);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(14, 14);
            this.label41.TabIndex = 20;
            this.label41.Text = "*";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Red;
            this.label66.Location = new System.Drawing.Point(572, 24);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(13, 13);
            this.label66.TabIndex = 19;
            this.label66.Text = "*";
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.AutoSize = true;
            this.chkOverwrite.BackColor = System.Drawing.Color.Transparent;
            this.chkOverwrite.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOverwrite.Location = new System.Drawing.Point(415, 118);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(143, 19);
            this.chkOverwrite.TabIndex = 5;
            this.chkOverwrite.Text = "Thay thế định mức cũ ";
            this.chkOverwrite.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Red;
            this.label68.Location = new System.Drawing.Point(572, 90);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(13, 13);
            this.label68.TabIndex = 18;
            this.label68.Text = "*";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(311, 122);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(57, 14);
            this.label69.TabIndex = 10;
            this.label69.Text = "Thay thế";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Red;
            this.label70.Location = new System.Drawing.Point(286, 24);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(14, 14);
            this.label70.TabIndex = 17;
            this.label70.Text = "*";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.BackColor = System.Drawing.Color.Transparent;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Red;
            this.label71.Location = new System.Drawing.Point(286, 91);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(14, 14);
            this.label71.TabIndex = 16;
            this.label71.Text = "*";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.BackColor = System.Drawing.Color.Transparent;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Red;
            this.label72.Location = new System.Drawing.Point(286, 126);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(14, 14);
            this.label72.TabIndex = 2;
            this.label72.Text = "*";
            // 
            // txtNPLCungUng
            // 
            this.txtNPLCungUng.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNPLCungUng.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPLCungUng.Location = new System.Drawing.Point(415, 82);
            this.txtNPLCungUng.MaxLength = 1;
            this.txtNPLCungUng.Name = "txtNPLCungUng";
            this.txtNPLCungUng.Size = new System.Drawing.Size(153, 22);
            this.txtNPLCungUng.TabIndex = 8;
            this.txtNPLCungUng.Text = "E";
            this.txtNPLCungUng.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.BackColor = System.Drawing.Color.Transparent;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(311, 91);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(102, 14);
            this.label73.TabIndex = 14;
            this.label73.Text = "NPL tự cung ứng";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTLHH
            // 
            this.txtTLHH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTLHH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTLHH.Location = new System.Drawing.Point(415, 52);
            this.txtTLHH.MaxLength = 1;
            this.txtTLHH.Name = "txtTLHH";
            this.txtTLHH.Size = new System.Drawing.Size(153, 22);
            this.txtTLHH.TabIndex = 7;
            this.txtTLHH.Text = "D";
            this.txtTLHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDMSD
            // 
            this.txtDMSD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDMSD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDMSD.Location = new System.Drawing.Point(415, 20);
            this.txtDMSD.MaxLength = 1;
            this.txtDMSD.Name = "txtDMSD";
            this.txtDMSD.Size = new System.Drawing.Size(153, 22);
            this.txtDMSD.TabIndex = 6;
            this.txtDMSD.Text = "C";
            this.txtDMSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDD
            // 
            this.txtMaDD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaDD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDD.Location = new System.Drawing.Point(197, 158);
            this.txtMaDD.MaxLength = 1;
            this.txtMaDD.Name = "txtMaDD";
            this.txtMaDD.Size = new System.Drawing.Size(86, 22);
            this.txtMaDD.TabIndex = 4;
            this.txtMaDD.Text = "F";
            this.txtMaDD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNguyenPhuLieu
            // 
            this.txtMaNguyenPhuLieu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaNguyenPhuLieu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguyenPhuLieu.Location = new System.Drawing.Point(130, 122);
            this.txtMaNguyenPhuLieu.MaxLength = 1;
            this.txtMaNguyenPhuLieu.Name = "txtMaNguyenPhuLieu";
            this.txtMaNguyenPhuLieu.Size = new System.Drawing.Size(153, 22);
            this.txtMaNguyenPhuLieu.TabIndex = 4;
            this.txtMaNguyenPhuLieu.Text = "B";
            this.txtMaNguyenPhuLieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaSanPham
            // 
            this.txtMaSanPham.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaSanPham.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSanPham.Location = new System.Drawing.Point(106, 86);
            this.txtMaSanPham.MaxLength = 1;
            this.txtMaSanPham.Name = "txtMaSanPham";
            this.txtMaSanPham.Size = new System.Drawing.Size(177, 22);
            this.txtMaSanPham.TabIndex = 3;
            this.txtMaSanPham.Text = "A";
            this.txtMaSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.BackColor = System.Drawing.Color.Transparent;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(11, 24);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(66, 14);
            this.label74.TabIndex = 0;
            this.label74.Text = "Tên Sheet";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.BackColor = System.Drawing.Color.Transparent;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(11, 91);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(78, 14);
            this.label75.TabIndex = 4;
            this.label75.Text = "Mã sản phẩm";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.BackColor = System.Drawing.Color.Transparent;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(11, 163);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(180, 14);
            this.label76.TabIndex = 6;
            this.label76.Text = "Mã định danh của lệnh sản xuất";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.BackColor = System.Drawing.Color.Transparent;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(11, 58);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(82, 14);
            this.label77.TabIndex = 2;
            this.label77.Text = "Dòng bắt đầu";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.Transparent;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(11, 127);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(114, 14);
            this.label78.TabIndex = 6;
            this.label78.Text = "Mã nguyên phụ liệu";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.BackColor = System.Drawing.Color.Transparent;
            this.label79.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(311, 24);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(108, 14);
            this.label79.TabIndex = 10;
            this.label79.Text = "Định mức sử dụng";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.BackColor = System.Drawing.Color.Transparent;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(311, 58);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(81, 14);
            this.label80.TabIndex = 12;
            this.label80.Text = "Tỷ lệ hao hụt";
            // 
            // txtRowDM
            // 
            this.txtRowDM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRowDM.Location = new System.Drawing.Point(106, 53);
            this.txtRowDM.Name = "txtRowDM";
            this.txtRowDM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtRowDM.Size = new System.Drawing.Size(177, 22);
            this.txtRowDM.TabIndex = 2;
            this.txtRowDM.Text = "2";
            this.txtRowDM.Value = 2;
            this.txtRowDM.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRowDM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPage5
            // 
            this.uiTabPage5.Controls.Add(this.uiGroupBox10);
            this.uiTabPage5.Controls.Add(this.uiGroupBox11);
            this.uiTabPage5.Location = new System.Drawing.Point(1, 22);
            this.uiTabPage5.Name = "uiTabPage5";
            this.uiTabPage5.Size = new System.Drawing.Size(1096, 378);
            this.uiTabPage5.TabStop = true;
            this.uiTabPage5.Text = "Cấu hình  đọc Excel hàng hóa tờ khai";
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.label110);
            this.uiGroupBox10.Controls.Add(this.label109);
            this.uiGroupBox10.Controls.Add(this.label108);
            this.uiGroupBox10.Controls.Add(this.label107);
            this.uiGroupBox10.Controls.Add(this.mnrTriGiaHH);
            this.uiGroupBox10.Controls.Add(this.nmrDonGiaHH);
            this.uiGroupBox10.Controls.Add(this.nmrSoLuong2);
            this.uiGroupBox10.Controls.Add(this.label98);
            this.uiGroupBox10.Controls.Add(this.nmrSoLuong1);
            this.uiGroupBox10.Controls.Add(this.label97);
            this.uiGroupBox10.Controls.Add(this.label83);
            this.uiGroupBox10.Controls.Add(this.label84);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox10.Location = new System.Drawing.Point(0, 245);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(1096, 133);
            this.uiGroupBox10.TabIndex = 34;
            this.uiGroupBox10.Text = "Cấu hình số thập phân";
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.BackColor = System.Drawing.Color.Transparent;
            this.label110.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Red;
            this.label110.Location = new System.Drawing.Point(592, 70);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(74, 14);
            this.label110.TabIndex = 16;
            this.label110.Text = "Mặc định : 6";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.BackColor = System.Drawing.Color.Transparent;
            this.label109.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Red;
            this.label109.Location = new System.Drawing.Point(592, 33);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(74, 14);
            this.label109.TabIndex = 16;
            this.label109.Text = "Mặc định : 6";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.BackColor = System.Drawing.Color.Transparent;
            this.label108.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Red;
            this.label108.Location = new System.Drawing.Point(214, 70);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(74, 14);
            this.label108.TabIndex = 15;
            this.label108.Text = "Mặc định : 2";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.BackColor = System.Drawing.Color.Transparent;
            this.label107.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Red;
            this.label107.Location = new System.Drawing.Point(214, 33);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(74, 14);
            this.label107.TabIndex = 15;
            this.label107.Text = "Mặc định : 2";
            // 
            // mnrTriGiaHH
            // 
            this.mnrTriGiaHH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnrTriGiaHH.Location = new System.Drawing.Point(430, 66);
            this.mnrTriGiaHH.Name = "mnrTriGiaHH";
            this.mnrTriGiaHH.Size = new System.Drawing.Size(149, 22);
            this.mnrTriGiaHH.TabIndex = 13;
            this.mnrTriGiaHH.Text = "6";
            this.mnrTriGiaHH.Value = 6;
            this.mnrTriGiaHH.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.mnrTriGiaHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // nmrDonGiaHH
            // 
            this.nmrDonGiaHH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrDonGiaHH.Location = new System.Drawing.Point(430, 29);
            this.nmrDonGiaHH.Name = "nmrDonGiaHH";
            this.nmrDonGiaHH.Size = new System.Drawing.Size(149, 22);
            this.nmrDonGiaHH.TabIndex = 13;
            this.nmrDonGiaHH.Text = "6";
            this.nmrDonGiaHH.Value = 6;
            this.nmrDonGiaHH.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrDonGiaHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // nmrSoLuong2
            // 
            this.nmrSoLuong2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrSoLuong2.Location = new System.Drawing.Point(128, 66);
            this.nmrSoLuong2.Name = "nmrSoLuong2";
            this.nmrSoLuong2.Size = new System.Drawing.Size(77, 22);
            this.nmrSoLuong2.TabIndex = 13;
            this.nmrSoLuong2.Text = "2";
            this.nmrSoLuong2.Value = 2;
            this.nmrSoLuong2.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrSoLuong2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.BackColor = System.Drawing.Color.Transparent;
            this.label98.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(316, 70);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(40, 14);
            this.label98.TabIndex = 12;
            this.label98.Text = "Trị giá";
            // 
            // nmrSoLuong1
            // 
            this.nmrSoLuong1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nmrSoLuong1.Location = new System.Drawing.Point(128, 29);
            this.nmrSoLuong1.Name = "nmrSoLuong1";
            this.nmrSoLuong1.Size = new System.Drawing.Size(77, 22);
            this.nmrSoLuong1.TabIndex = 13;
            this.nmrSoLuong1.Text = "2";
            this.nmrSoLuong1.Value = 2;
            this.nmrSoLuong1.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.nmrSoLuong1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.BackColor = System.Drawing.Color.Transparent;
            this.label97.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(316, 33);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(48, 14);
            this.label97.TabIndex = 12;
            this.label97.Text = "Đơn giá";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.BackColor = System.Drawing.Color.Transparent;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(15, 70);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(67, 14);
            this.label83.TabIndex = 12;
            this.label83.Text = "Số lượng 2";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.BackColor = System.Drawing.Color.Transparent;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(15, 33);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(56, 14);
            this.label84.TabIndex = 12;
            this.label84.Text = "Số lượng";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.uiComboBox4);
            this.uiGroupBox11.Controls.Add(this.txtXuatXuColumn);
            this.uiGroupBox11.Controls.Add(this.txtSoLuongColumn2);
            this.uiGroupBox11.Controls.Add(this.label85);
            this.uiGroupBox11.Controls.Add(this.label86);
            this.uiGroupBox11.Controls.Add(this.label87);
            this.uiGroupBox11.Controls.Add(this.txtRow);
            this.uiGroupBox11.Controls.Add(this.label88);
            this.uiGroupBox11.Controls.Add(this.txtTriGiaTT);
            this.uiGroupBox11.Controls.Add(this.label89);
            this.uiGroupBox11.Controls.Add(this.label90);
            this.uiGroupBox11.Controls.Add(this.label91);
            this.uiGroupBox11.Controls.Add(this.label92);
            this.uiGroupBox11.Controls.Add(this.label93);
            this.uiGroupBox11.Controls.Add(this.txtDVTColumn2);
            this.uiGroupBox11.Controls.Add(this.label94);
            this.uiGroupBox11.Controls.Add(this.txtDVTColumn);
            this.uiGroupBox11.Controls.Add(this.lblXuatXu);
            this.uiGroupBox11.Controls.Add(this.txtSoLuongColumn);
            this.uiGroupBox11.Controls.Add(this.lblThue);
            this.uiGroupBox11.Controls.Add(this.txtThueSuat);
            this.uiGroupBox11.Controls.Add(this.label95);
            this.uiGroupBox11.Controls.Add(this.txtBieuThueXNK);
            this.uiGroupBox11.Controls.Add(this.label96);
            this.uiGroupBox11.Controls.Add(this.txtDonGiaColumn);
            this.uiGroupBox11.Controls.Add(this.txtMaHangColumn);
            this.uiGroupBox11.Controls.Add(this.txtMaHSColumn);
            this.uiGroupBox11.Controls.Add(this.txtTenHangColumn);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox11.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1096, 245);
            this.uiGroupBox11.TabIndex = 33;
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // uiComboBox4
            // 
            this.uiComboBox4.DisplayMember = "Name";
            this.uiComboBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiComboBox4.Location = new System.Drawing.Point(128, 20);
            this.uiComboBox4.Name = "uiComboBox4";
            this.uiComboBox4.Size = new System.Drawing.Size(151, 22);
            this.uiComboBox4.TabIndex = 31;
            this.uiComboBox4.Tag = "PhuongThucThanhToan";
            this.uiComboBox4.ValueMember = "ID";
            this.uiComboBox4.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtXuatXuColumn
            // 
            this.txtXuatXuColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtXuatXuColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXuatXuColumn.Location = new System.Drawing.Point(430, 21);
            this.txtXuatXuColumn.MaxLength = 1;
            this.txtXuatXuColumn.Name = "txtXuatXuColumn";
            this.txtXuatXuColumn.Size = new System.Drawing.Size(149, 22);
            this.txtXuatXuColumn.TabIndex = 8;
            this.txtXuatXuColumn.Text = "E";
            this.txtXuatXuColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuongColumn2
            // 
            this.txtSoLuongColumn2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongColumn2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongColumn2.Location = new System.Drawing.Point(129, 206);
            this.txtSoLuongColumn2.MaxLength = 1;
            this.txtSoLuongColumn2.Name = "txtSoLuongColumn2";
            this.txtSoLuongColumn2.Size = new System.Drawing.Size(150, 22);
            this.txtSoLuongColumn2.TabIndex = 7;
            this.txtSoLuongColumn2.Text = "D";
            this.txtSoLuongColumn2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.BackColor = System.Drawing.Color.Transparent;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(35, 179);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(77, 14);
            this.label85.TabIndex = 14;
            this.label85.Text = "Cột số lượng";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.BackColor = System.Drawing.Color.Transparent;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(35, 210);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(88, 14);
            this.label86.TabIndex = 30;
            this.label86.Text = "Cột số lượng 2";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.BackColor = System.Drawing.Color.Transparent;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(316, 55);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(70, 14);
            this.label87.TabIndex = 16;
            this.label87.Text = "Cột đơn giá";
            // 
            // txtRow
            // 
            this.txtRow.DecimalDigits = 0;
            this.txtRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRow.Location = new System.Drawing.Point(128, 51);
            this.txtRow.MaxLength = 6;
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(151, 22);
            this.txtRow.TabIndex = 2;
            this.txtRow.Text = "2";
            this.txtRow.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtRow.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.BackColor = System.Drawing.Color.Transparent;
            this.label88.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(316, 179);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(54, 14);
            this.label88.TabIndex = 12;
            this.label88.Text = "Cột ĐVT";
            // 
            // txtTriGiaTT
            // 
            this.txtTriGiaTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTriGiaTT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTT.Location = new System.Drawing.Point(430, 82);
            this.txtTriGiaTT.MaxLength = 1;
            this.txtTriGiaTT.Name = "txtTriGiaTT";
            this.txtTriGiaTT.Size = new System.Drawing.Size(149, 22);
            this.txtTriGiaTT.TabIndex = 10;
            this.txtTriGiaTT.Text = "H";
            this.txtTriGiaTT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.BackColor = System.Drawing.Color.Transparent;
            this.label89.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(35, 148);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(65, 14);
            this.label89.TabIndex = 8;
            this.label89.Text = "Cột mã HS";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.BackColor = System.Drawing.Color.Transparent;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(316, 86);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(60, 14);
            this.label90.TabIndex = 18;
            this.label90.Text = "Cột trị giá";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.BackColor = System.Drawing.Color.Transparent;
            this.label91.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(316, 210);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(61, 14);
            this.label91.TabIndex = 12;
            this.label91.Text = "Cột ĐVT2";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.BackColor = System.Drawing.Color.Transparent;
            this.label92.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(35, 117);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(80, 14);
            this.label92.TabIndex = 6;
            this.label92.Text = "Cột tên hàng";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.BackColor = System.Drawing.Color.Transparent;
            this.label93.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(35, 55);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(82, 14);
            this.label93.TabIndex = 2;
            this.label93.Text = "Dòng bắt đầu";
            // 
            // txtDVTColumn2
            // 
            this.txtDVTColumn2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTColumn2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTColumn2.Location = new System.Drawing.Point(430, 206);
            this.txtDVTColumn2.MaxLength = 1;
            this.txtDVTColumn2.Name = "txtDVTColumn2";
            this.txtDVTColumn2.Size = new System.Drawing.Size(149, 22);
            this.txtDVTColumn2.TabIndex = 14;
            this.txtDVTColumn2.Text = "F";
            this.txtDVTColumn2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.BackColor = System.Drawing.Color.Transparent;
            this.label94.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(35, 86);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(77, 14);
            this.label94.TabIndex = 4;
            this.label94.Text = "Cột mã hàng";
            // 
            // txtDVTColumn
            // 
            this.txtDVTColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTColumn.Location = new System.Drawing.Point(430, 175);
            this.txtDVTColumn.MaxLength = 1;
            this.txtDVTColumn.Name = "txtDVTColumn";
            this.txtDVTColumn.Size = new System.Drawing.Size(149, 22);
            this.txtDVTColumn.TabIndex = 13;
            this.txtDVTColumn.Text = "F";
            this.txtDVTColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblXuatXu
            // 
            this.lblXuatXu.AutoSize = true;
            this.lblXuatXu.BackColor = System.Drawing.Color.Transparent;
            this.lblXuatXu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXuatXu.Location = new System.Drawing.Point(316, 25);
            this.lblXuatXu.Name = "lblXuatXu";
            this.lblXuatXu.Size = new System.Drawing.Size(72, 14);
            this.lblXuatXu.TabIndex = 10;
            this.lblXuatXu.Text = "Cột xuất xứ";
            // 
            // txtSoLuongColumn
            // 
            this.txtSoLuongColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongColumn.Location = new System.Drawing.Point(129, 175);
            this.txtSoLuongColumn.MaxLength = 1;
            this.txtSoLuongColumn.Name = "txtSoLuongColumn";
            this.txtSoLuongColumn.Size = new System.Drawing.Size(149, 22);
            this.txtSoLuongColumn.TabIndex = 6;
            this.txtSoLuongColumn.Text = "D";
            this.txtSoLuongColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblThue
            // 
            this.lblThue.AutoSize = true;
            this.lblThue.BackColor = System.Drawing.Color.Transparent;
            this.lblThue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThue.Location = new System.Drawing.Point(316, 148);
            this.lblThue.Name = "lblThue";
            this.lblThue.Size = new System.Drawing.Size(109, 14);
            this.lblThue.TabIndex = 20;
            this.lblThue.Text = "Cột biểu thuế XNK";
            // 
            // txtThueSuat
            // 
            this.txtThueSuat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueSuat.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuat.Location = new System.Drawing.Point(430, 113);
            this.txtThueSuat.MaxLength = 1;
            this.txtThueSuat.Name = "txtThueSuat";
            this.txtThueSuat.Size = new System.Drawing.Size(149, 22);
            this.txtThueSuat.TabIndex = 12;
            this.txtThueSuat.Text = "I";
            this.txtThueSuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.BackColor = System.Drawing.Color.Transparent;
            this.label95.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(316, 117);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(83, 14);
            this.label95.TabIndex = 20;
            this.label95.Text = "Cột thuế suât";
            // 
            // txtBieuThueXNK
            // 
            this.txtBieuThueXNK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBieuThueXNK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBieuThueXNK.Location = new System.Drawing.Point(430, 141);
            this.txtBieuThueXNK.MaxLength = 1;
            this.txtBieuThueXNK.Name = "txtBieuThueXNK";
            this.txtBieuThueXNK.Size = new System.Drawing.Size(149, 22);
            this.txtBieuThueXNK.TabIndex = 11;
            this.txtBieuThueXNK.Text = "J";
            this.txtBieuThueXNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.BackColor = System.Drawing.Color.Transparent;
            this.label96.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(35, 25);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(66, 14);
            this.label96.TabIndex = 0;
            this.label96.Text = "Tên Sheet";
            // 
            // txtDonGiaColumn
            // 
            this.txtDonGiaColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaColumn.Location = new System.Drawing.Point(430, 51);
            this.txtDonGiaColumn.MaxLength = 1;
            this.txtDonGiaColumn.Name = "txtDonGiaColumn";
            this.txtDonGiaColumn.Size = new System.Drawing.Size(149, 22);
            this.txtDonGiaColumn.TabIndex = 9;
            this.txtDonGiaColumn.Text = "G";
            this.txtDonGiaColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHangColumn
            // 
            this.txtMaHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangColumn.Location = new System.Drawing.Point(128, 82);
            this.txtMaHangColumn.MaxLength = 1;
            this.txtMaHangColumn.Name = "txtMaHangColumn";
            this.txtMaHangColumn.Size = new System.Drawing.Size(151, 22);
            this.txtMaHangColumn.TabIndex = 3;
            this.txtMaHangColumn.Text = "A";
            this.txtMaHangColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHSColumn
            // 
            this.txtMaHSColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSColumn.Location = new System.Drawing.Point(129, 144);
            this.txtMaHSColumn.MaxLength = 1;
            this.txtMaHSColumn.Name = "txtMaHSColumn";
            this.txtMaHSColumn.Size = new System.Drawing.Size(149, 22);
            this.txtMaHSColumn.TabIndex = 5;
            this.txtMaHSColumn.Text = "C";
            this.txtMaHSColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenHangColumn
            // 
            this.txtTenHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangColumn.Location = new System.Drawing.Point(128, 113);
            this.txtTenHangColumn.MaxLength = 1;
            this.txtTenHangColumn.Name = "txtTenHangColumn";
            this.txtTenHangColumn.Size = new System.Drawing.Size(151, 22);
            this.txtTenHangColumn.TabIndex = 4;
            this.txtTenHangColumn.Text = "B";
            this.txtTenHangColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // FrmCauHinhThongSoReadExcel
            // 
            this.ClientSize = new System.Drawing.Size(1098, 450);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "FrmCauHinhThongSoReadExcel";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cấu hình Thông số mặc định khi Read Excel";
            this.Load += new System.EventHandler(this.FrmCauHinhThongSoReadExcel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbGC)).EndInit();
            this.grbGC.ResumeLayout(false);
            this.grbGC.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbSP)).EndInit();
            this.grbSP.ResumeLayout(false);
            this.grbSP.PerformLayout();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            this.uiGroupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            this.uiTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIComboBox cbbSheetName;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.EditControls.UICheckBox chkTCU;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaNPL;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongNPL;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguonCungCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRowNPL;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrDonGiaNPL;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrSoLuongNPL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIComboBox uiComboBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiSPGC;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaSP;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongSP;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTSP;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSSP;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenSP;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSP;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRowSP;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIComboBox uiComboBox2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongTB;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaTB;
        private Janus.Windows.GridEX.EditControls.EditBox txtXuatXu;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhTrang;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguyenTe;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTTB;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSTB;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenTB;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaTB;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRowTB;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIComboBox uiComboBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label66;
        private Janus.Windows.EditControls.UICheckBox chkOverwrite;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private Janus.Windows.GridEX.EditControls.EditBox txtNPLCungUng;
        private System.Windows.Forms.Label label73;
        private Janus.Windows.GridEX.EditControls.EditBox txtTLHH;
        private Janus.Windows.GridEX.EditControls.EditBox txtDMSD;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDD;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguyenPhuLieu;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSanPham;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRowDM;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrDonGiaSP;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrSoLuongSP;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrDonGiaTB;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrSoLuongTB;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label67;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrTLHH;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrDMSD;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.GridEX.EditControls.NumericEditBox mnrTriGiaHH;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrDonGiaHH;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrSoLuong2;
        private System.Windows.Forms.Label label98;
        private Janus.Windows.GridEX.EditControls.NumericEditBox nmrSoLuong1;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.EditControls.UIComboBox uiComboBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtXuatXuColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongColumn2;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label88;
        private Janus.Windows.GridEX.EditControls.EditBox txtTriGiaTT;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTColumn2;
        private System.Windows.Forms.Label label94;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTColumn;
        private System.Windows.Forms.Label lblXuatXu;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongColumn;
        private System.Windows.Forms.Label lblThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueSuat;
        private System.Windows.Forms.Label label95;
        private Janus.Windows.GridEX.EditControls.EditBox txtBieuThueXNK;
        private System.Windows.Forms.Label label96;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangColumn;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private Janus.Windows.EditControls.UIGroupBox grbGC;
        private Janus.Windows.EditControls.UIGroupBox grbSP;
    }
}
