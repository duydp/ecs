using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.GC
{
    public partial class ThanhKhoanHDGC : EntityBase
    {
        #region Private members.

        protected long _HopDong_ID;
        protected string _Ten = String.Empty;
        protected string _DVT = String.Empty;
        protected decimal _TongLuongNK;
        protected decimal _TongLuongCU;
        protected decimal _TongLuongXK;
        protected decimal _ChenhLech;
        protected string _KetLuanXLCL = String.Empty;
        protected long _OldHD_ID;
        protected int _STT;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long HopDong_ID
        {
            set { this._HopDong_ID = value; }
            get { return this._HopDong_ID; }
        }
        public string Ten
        {
            set { this._Ten = value; }
            get { return this._Ten; }
        }
        public string DVT
        {
            set { this._DVT = value; }
            get { return this._DVT; }
        }
        public decimal TongLuongNK
        {
            set { this._TongLuongNK = value; }
            get { return this._TongLuongNK; }
        }
        public decimal TongLuongCU
        {
            set { this._TongLuongCU = value; }
            get { return this._TongLuongCU; }
        }
        public decimal TongLuongXK
        {
            set { this._TongLuongXK = value; }
            get { return this._TongLuongXK; }
        }
        public decimal ChenhLech
        {
            set { this._ChenhLech = value; }
            get { return this._ChenhLech; }
        }
        public string KetLuanXLCL
        {
            set { this._KetLuanXLCL = value; }
            get { return this._KetLuanXLCL; }
        }
        public long OldHD_ID
        {
            set { this._OldHD_ID = value; }
            get { return this._OldHD_ID; }
        }
        public int STT
        {
            set { this._STT = value; }
            get { return this._STT; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_GC_ThanhKhoanHDGC_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) this._DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongNK"))) this._TongLuongNK = reader.GetDecimal(reader.GetOrdinal("TongLuongNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongCU"))) this._TongLuongCU = reader.GetDecimal(reader.GetOrdinal("TongLuongCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongXK"))) this._TongLuongXK = reader.GetDecimal(reader.GetOrdinal("TongLuongXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChenhLech"))) this._ChenhLech = reader.GetDecimal(reader.GetOrdinal("ChenhLech"));
                if (!reader.IsDBNull(reader.GetOrdinal("KetLuanXLCL"))) this._KetLuanXLCL = reader.GetString(reader.GetOrdinal("KetLuanXLCL"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) this._OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) this._STT = reader.GetInt32(reader.GetOrdinal("STT"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_GC_ThanhKhoanHDGC_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_GC_ThanhKhoanHDGC_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_ThanhKhoanHDGC_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_ThanhKhoanHDGC_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public ThanhKhoanHDGCCollection SelectCollectionAll()
        {
            ThanhKhoanHDGCCollection collection = new ThanhKhoanHDGCCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                ThanhKhoanHDGC entity = new ThanhKhoanHDGC();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongNK"))) entity.TongLuongNK = reader.GetDecimal(reader.GetOrdinal("TongLuongNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongCU"))) entity.TongLuongCU = reader.GetDecimal(reader.GetOrdinal("TongLuongCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongXK"))) entity.TongLuongXK = reader.GetDecimal(reader.GetOrdinal("TongLuongXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChenhLech"))) entity.ChenhLech = reader.GetDecimal(reader.GetOrdinal("ChenhLech"));
                if (!reader.IsDBNull(reader.GetOrdinal("KetLuanXLCL"))) entity.KetLuanXLCL = reader.GetString(reader.GetOrdinal("KetLuanXLCL"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) entity.OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public ThanhKhoanHDGCCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            ThanhKhoanHDGCCollection collection = new ThanhKhoanHDGCCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ThanhKhoanHDGC entity = new ThanhKhoanHDGC();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongNK"))) entity.TongLuongNK = reader.GetDecimal(reader.GetOrdinal("TongLuongNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongCU"))) entity.TongLuongCU = reader.GetDecimal(reader.GetOrdinal("TongLuongCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongLuongXK"))) entity.TongLuongXK = reader.GetDecimal(reader.GetOrdinal("TongLuongXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChenhLech"))) entity.ChenhLech = reader.GetDecimal(reader.GetOrdinal("ChenhLech"));
                if (!reader.IsDBNull(reader.GetOrdinal("KetLuanXLCL"))) entity.KetLuanXLCL = reader.GetString(reader.GetOrdinal("KetLuanXLCL"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) entity.OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_ThanhKhoanHDGC_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@TongLuongNK", SqlDbType.Decimal, this._TongLuongNK);
            this.db.AddInParameter(dbCommand, "@TongLuongCU", SqlDbType.Decimal, this._TongLuongCU);
            this.db.AddInParameter(dbCommand, "@TongLuongXK", SqlDbType.Decimal, this._TongLuongXK);
            this.db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, this._ChenhLech);
            this.db.AddInParameter(dbCommand, "@KetLuanXLCL", SqlDbType.NVarChar, this._KetLuanXLCL);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(ThanhKhoanHDGCCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ThanhKhoanHDGC item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, ThanhKhoanHDGCCollection collection)
        {
            foreach (ThanhKhoanHDGC item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_ThanhKhoanHDGC_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@TongLuongNK", SqlDbType.Decimal, this._TongLuongNK);
            this.db.AddInParameter(dbCommand, "@TongLuongCU", SqlDbType.Decimal, this._TongLuongCU);
            this.db.AddInParameter(dbCommand, "@TongLuongXK", SqlDbType.Decimal, this._TongLuongXK);
            this.db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, this._ChenhLech);
            this.db.AddInParameter(dbCommand, "@KetLuanXLCL", SqlDbType.NVarChar, this._KetLuanXLCL);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(ThanhKhoanHDGCCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ThanhKhoanHDGC item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_ThanhKhoanHDGC_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@TongLuongNK", SqlDbType.Decimal, this._TongLuongNK);
            this.db.AddInParameter(dbCommand, "@TongLuongCU", SqlDbType.Decimal, this._TongLuongCU);
            this.db.AddInParameter(dbCommand, "@TongLuongXK", SqlDbType.Decimal, this._TongLuongXK);
            this.db.AddInParameter(dbCommand, "@ChenhLech", SqlDbType.Decimal, this._ChenhLech);
            this.db.AddInParameter(dbCommand, "@KetLuanXLCL", SqlDbType.NVarChar, this._KetLuanXLCL);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(ThanhKhoanHDGCCollection collection, SqlTransaction transaction)
        {
            foreach (ThanhKhoanHDGC item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_ThanhKhoanHDGC_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(ThanhKhoanHDGCCollection collection, SqlTransaction transaction)
        {
            foreach (ThanhKhoanHDGC item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(ThanhKhoanHDGCCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ThanhKhoanHDGC item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion
    }
}