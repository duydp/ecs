using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public decimal SoToKhai { set; get; }
		public string SoToKhaiDauTien { set; get; }
		public decimal SoNhanhToKhai { set; get; }
		public decimal TongSoTKChiaNho { set; get; }
		public decimal SoToKhaiTNTX { set; get; }
		public string MaLoaiHinh { set; get; }
		public string MaPhanLoaiHH { set; get; }
		public string MaPhuongThucVT { set; get; }
		public string PhanLoaiToChuc { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public string NhomXuLyHS { set; get; }
		public DateTime ThoiHanTaiNhapTaiXuat { set; get; }
		public DateTime NgayDangKy { set; get; }
		public string MaDonVi { set; get; }
		public string TenDonVi { set; get; }
		public string MaBuuChinhDonVi { set; get; }
		public string DiaChiDonVi { set; get; }
		public string SoDienThoaiDonVi { set; get; }
		public string MaUyThac { set; get; }
		public string TenUyThac { set; get; }
		public string MaDoiTac { set; get; }
		public string TenDoiTac { set; get; }
		public string MaBuuChinhDoiTac { set; get; }
		public string DiaChiDoiTac1 { set; get; }
		public string DiaChiDoiTac2 { set; get; }
		public string DiaChiDoiTac3 { set; get; }
		public string DiaChiDoiTac4 { set; get; }
		public string MaNuocDoiTac { set; get; }
		public string NguoiUyThacXK { set; get; }
		public string MaDaiLyHQ { set; get; }
		public decimal SoLuong { set; get; }
		public string MaDVTSoLuong { set; get; }
		public decimal TrongLuong { set; get; }
		public string MaDVTTrongLuong { set; get; }
		public string MaDDLuuKho { set; get; }
		public string SoHieuKyHieu { set; get; }
		public string MaPTVC { set; get; }
		public string TenPTVC { set; get; }
		public DateTime NgayHangDen { set; get; }
		public string MaDiaDiemDoHang { set; get; }
		public string TenDiaDiemDohang { set; get; }
		public string MaDiaDiemXepHang { set; get; }
		public string TenDiaDiemXepHang { set; get; }
		public decimal SoLuongCont { set; get; }
		public string MaKetQuaKiemTra { set; get; }
		public string PhanLoaiHD { set; get; }
		public decimal SoTiepNhanHD { set; get; }
		public string SoHoaDon { set; get; }
		public DateTime NgayPhatHanhHD { set; get; }
		public string PhuongThucTT { set; get; }
		public string PhanLoaiGiaHD { set; get; }
		public string MaDieuKienGiaHD { set; get; }
		public string MaTTHoaDon { set; get; }
		public decimal TongTriGiaHD { set; get; }
		public string MaPhanLoaiTriGia { set; get; }
		public decimal SoTiepNhanTKTriGia { set; get; }
		public string MaTTHieuChinhTriGia { set; get; }
		public decimal GiaHieuChinhTriGia { set; get; }
		public string MaPhanLoaiPhiVC { set; get; }
		public string MaTTPhiVC { set; get; }
		public decimal PhiVanChuyen { set; get; }
		public string MaPhanLoaiPhiBH { set; get; }
		public string MaTTPhiBH { set; get; }
		public decimal PhiBaoHiem { set; get; }
		public string SoDangKyBH { set; get; }
		public string ChiTietKhaiTriGia { set; get; }
		public decimal TriGiaTinhThue { set; get; }
		public string PhanLoaiKhongQDVND { set; get; }
		public string MaTTTriGiaTinhThue { set; get; }
		public decimal TongHeSoPhanBoTG { set; get; }
		public string MaLyDoDeNghiBP { set; get; }
		public string NguoiNopThue { set; get; }
		public string MaNHTraThueThay { set; get; }
		public decimal NamPhatHanhHM { set; get; }
		public string KyHieuCTHanMuc { set; get; }
		public string SoCTHanMuc { set; get; }
		public string MaXDThoiHanNopThue { set; get; }
		public string MaNHBaoLanh { set; get; }
		public decimal NamPhatHanhBL { set; get; }
		public string KyHieuCTBaoLanh { set; get; }
		public string SoCTBaoLanh { set; get; }
		public DateTime NgayNhapKhoDau { set; get; }
		public DateTime NgayKhoiHanhVC { set; get; }
		public string DiaDiemDichVC { set; get; }
		public DateTime NgayDen { set; get; }
		public string GhiChu { set; get; }
		public string SoQuanLyNoiBoDN { set; get; }
		public string TrangThaiXuLy { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string PhanLoaiBaoCaoSuaDoi { set; get; }
		public string MaPhanLoaiKiemTra { set; get; }
		public string MaSoThueDaiDien { set; get; }
		public string TenCoQuanHaiQuan { set; get; }
		public DateTime NgayThayDoiDangKy { set; get; }
		public string BieuThiTruongHopHetHan { set; get; }
		public string TenDaiLyHaiQuan { set; get; }
		public string MaNhanVienHaiQuan { set; get; }
		public string TenDDLuuKho { set; get; }
		public string MaPhanLoaiTongGiaCoBan { set; get; }
		public string PhanLoaiCongThucChuan { set; get; }
		public string MaPhanLoaiDieuChinhTriGia { set; get; }
		public string PhuongPhapDieuChinhTriGia { set; get; }
		public decimal TongTienThuePhaiNop { set; get; }
		public decimal SoTienBaoLanh { set; get; }
		public string TenTruongDonViHaiQuan { set; get; }
		public DateTime NgayCapPhep { set; get; }
		public string PhanLoaiThamTraSauThongQuan { set; get; }
		public DateTime NgayPheDuyetBP { set; get; }
		public DateTime NgayHoanThanhKiemTraBP { set; get; }
		public decimal SoNgayDoiCapPhepNhapKhau { set; get; }
		public string TieuDe { set; get; }
		public string MaSacThueAnHan_VAT { set; get; }
		public string TenSacThueAnHan_VAT { set; get; }
		public DateTime HanNopThueSauKhiAnHan_VAT { set; get; }
		public string PhanLoaiNopThue { set; get; }
		public decimal TongSoTienThueXuatKhau { set; get; }
		public string MaTTTongTienThueXuatKhau { set; get; }
		public decimal TongSoTienLePhi { set; get; }
		public string MaTTCuaSoTienBaoLanh { set; get; }
		public string SoQuanLyNguoiSuDung { set; get; }
		public DateTime NgayHoanThanhKiemTra { set; get; }
		public decimal TongSoTrangCuaToKhai { set; get; }
		public decimal TongSoDongHangCuaToKhai { set; get; }
		public DateTime NgayKhaiBaoNopThue { set; get; }
		public string MaVanbanPhapQuy1 { set; get; }
		public string MaVanbanPhapQuy2 { set; get; }
		public string MaVanbanPhapQuy3 { set; get; }
		public string MaVanbanPhapQuy4 { set; get; }
		public string MaVanbanPhapQuy5 { set; get; }
		public long HopDong_ID { set; get; }
		public string HopDong_So { set; get; }
		public string LoaiHang { set; get; }
		public string Templ_1 { set; get; }
        public string SoDinhDanh { set; get; }
        public string VanDon { set; get; }
        public bool Is2061 { set; get; }
        public bool Is1593 { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiMauDich> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiMauDich> collection = new List<KDT_VNACC_ToKhaiMauDich>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiMauDich entity = new KDT_VNACC_ToKhaiMauDich();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiDauTien"))) entity.SoToKhaiDauTien = reader.GetString(reader.GetOrdinal("SoToKhaiDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhanhToKhai"))) entity.SoNhanhToKhai = reader.GetDecimal(reader.GetOrdinal("SoNhanhToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTKChiaNho"))) entity.TongSoTKChiaNho = reader.GetDecimal(reader.GetOrdinal("TongSoTKChiaNho"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTNTX"))) entity.SoToKhaiTNTX = reader.GetDecimal(reader.GetOrdinal("SoToKhaiTNTX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiHH"))) entity.MaPhanLoaiHH = reader.GetString(reader.GetOrdinal("MaPhanLoaiHH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhuongThucVT"))) entity.MaPhuongThucVT = reader.GetString(reader.GetOrdinal("MaPhuongThucVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiToChuc"))) entity.PhanLoaiToChuc = reader.GetString(reader.GetOrdinal("PhanLoaiToChuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomXuLyHS"))) entity.NhomXuLyHS = reader.GetString(reader.GetOrdinal("NhomXuLyHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanTaiNhapTaiXuat"))) entity.ThoiHanTaiNhapTaiXuat = reader.GetDateTime(reader.GetOrdinal("ThoiHanTaiNhapTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonVi"))) entity.MaDonVi = reader.GetString(reader.GetOrdinal("MaDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonVi"))) entity.TenDonVi = reader.GetString(reader.GetOrdinal("TenDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhDonVi"))) entity.MaBuuChinhDonVi = reader.GetString(reader.GetOrdinal("MaBuuChinhDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDonVi"))) entity.DiaChiDonVi = reader.GetString(reader.GetOrdinal("DiaChiDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiDonVi"))) entity.SoDienThoaiDonVi = reader.GetString(reader.GetOrdinal("SoDienThoaiDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaUyThac"))) entity.MaUyThac = reader.GetString(reader.GetOrdinal("MaUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenUyThac"))) entity.TenUyThac = reader.GetString(reader.GetOrdinal("TenUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoiTac"))) entity.MaDoiTac = reader.GetString(reader.GetOrdinal("MaDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoiTac"))) entity.TenDoiTac = reader.GetString(reader.GetOrdinal("TenDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhDoiTac"))) entity.MaBuuChinhDoiTac = reader.GetString(reader.GetOrdinal("MaBuuChinhDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac1"))) entity.DiaChiDoiTac1 = reader.GetString(reader.GetOrdinal("DiaChiDoiTac1"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac2"))) entity.DiaChiDoiTac2 = reader.GetString(reader.GetOrdinal("DiaChiDoiTac2"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac3"))) entity.DiaChiDoiTac3 = reader.GetString(reader.GetOrdinal("DiaChiDoiTac3"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac4"))) entity.DiaChiDoiTac4 = reader.GetString(reader.GetOrdinal("DiaChiDoiTac4"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocDoiTac"))) entity.MaNuocDoiTac = reader.GetString(reader.GetOrdinal("MaNuocDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiUyThacXK"))) entity.NguoiUyThacXK = reader.GetString(reader.GetOrdinal("NguoiUyThacXK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyHQ"))) entity.MaDaiLyHQ = reader.GetString(reader.GetOrdinal("MaDaiLyHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTSoLuong"))) entity.MaDVTSoLuong = reader.GetString(reader.GetOrdinal("MaDVTSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVTTrongLuong"))) entity.MaDVTTrongLuong = reader.GetString(reader.GetOrdinal("MaDVTTrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDDLuuKho"))) entity.MaDDLuuKho = reader.GetString(reader.GetOrdinal("MaDDLuuKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuKyHieu"))) entity.SoHieuKyHieu = reader.GetString(reader.GetOrdinal("SoHieuKyHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPTVC"))) entity.MaPTVC = reader.GetString(reader.GetOrdinal("MaPTVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenPTVC"))) entity.TenPTVC = reader.GetString(reader.GetOrdinal("TenPTVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHangDen"))) entity.NgayHangDen = reader.GetDateTime(reader.GetOrdinal("NgayHangDen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemDoHang"))) entity.MaDiaDiemDoHang = reader.GetString(reader.GetOrdinal("MaDiaDiemDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemDohang"))) entity.TenDiaDiemDohang = reader.GetString(reader.GetOrdinal("TenDiaDiemDohang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemXepHang"))) entity.MaDiaDiemXepHang = reader.GetString(reader.GetOrdinal("MaDiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemXepHang"))) entity.TenDiaDiemXepHang = reader.GetString(reader.GetOrdinal("TenDiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCont"))) entity.SoLuongCont = reader.GetDecimal(reader.GetOrdinal("SoLuongCont"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKetQuaKiemTra"))) entity.MaKetQuaKiemTra = reader.GetString(reader.GetOrdinal("MaKetQuaKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiHD"))) entity.PhanLoaiHD = reader.GetString(reader.GetOrdinal("PhanLoaiHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanHD"))) entity.SoTiepNhanHD = reader.GetDecimal(reader.GetOrdinal("SoTiepNhanHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatHanhHD"))) entity.NgayPhatHanhHD = reader.GetDateTime(reader.GetOrdinal("NgayPhatHanhHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongThucTT"))) entity.PhuongThucTT = reader.GetString(reader.GetOrdinal("PhuongThucTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiGiaHD"))) entity.PhanLoaiGiaHD = reader.GetString(reader.GetOrdinal("PhanLoaiGiaHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDieuKienGiaHD"))) entity.MaDieuKienGiaHD = reader.GetString(reader.GetOrdinal("MaDieuKienGiaHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTHoaDon"))) entity.MaTTHoaDon = reader.GetString(reader.GetOrdinal("MaTTHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaHD"))) entity.TongTriGiaHD = reader.GetDecimal(reader.GetOrdinal("TongTriGiaHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiTriGia"))) entity.MaPhanLoaiTriGia = reader.GetString(reader.GetOrdinal("MaPhanLoaiTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanTKTriGia"))) entity.SoTiepNhanTKTriGia = reader.GetDecimal(reader.GetOrdinal("SoTiepNhanTKTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTHieuChinhTriGia"))) entity.MaTTHieuChinhTriGia = reader.GetString(reader.GetOrdinal("MaTTHieuChinhTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiaHieuChinhTriGia"))) entity.GiaHieuChinhTriGia = reader.GetDecimal(reader.GetOrdinal("GiaHieuChinhTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiPhiVC"))) entity.MaPhanLoaiPhiVC = reader.GetString(reader.GetOrdinal("MaPhanLoaiPhiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTPhiVC"))) entity.MaTTPhiVC = reader.GetString(reader.GetOrdinal("MaTTPhiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiPhiBH"))) entity.MaPhanLoaiPhiBH = reader.GetString(reader.GetOrdinal("MaPhanLoaiPhiBH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTPhiBH"))) entity.MaTTPhiBH = reader.GetString(reader.GetOrdinal("MaTTPhiBH"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDangKyBH"))) entity.SoDangKyBH = reader.GetString(reader.GetOrdinal("SoDangKyBH"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiTietKhaiTriGia"))) entity.ChiTietKhaiTriGia = reader.GetString(reader.GetOrdinal("ChiTietKhaiTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThue"))) entity.TriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiKhongQDVND"))) entity.PhanLoaiKhongQDVND = reader.GetString(reader.GetOrdinal("PhanLoaiKhongQDVND"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTTriGiaTinhThue"))) entity.MaTTTriGiaTinhThue = reader.GetString(reader.GetOrdinal("MaTTTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongHeSoPhanBoTG"))) entity.TongHeSoPhanBoTG = reader.GetDecimal(reader.GetOrdinal("TongHeSoPhanBoTG"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLyDoDeNghiBP"))) entity.MaLyDoDeNghiBP = reader.GetString(reader.GetOrdinal("MaLyDoDeNghiBP"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiNopThue"))) entity.NguoiNopThue = reader.GetString(reader.GetOrdinal("NguoiNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNHTraThueThay"))) entity.MaNHTraThueThay = reader.GetString(reader.GetOrdinal("MaNHTraThueThay"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamPhatHanhHM"))) entity.NamPhatHanhHM = reader.GetDecimal(reader.GetOrdinal("NamPhatHanhHM"));
				if (!reader.IsDBNull(reader.GetOrdinal("KyHieuCTHanMuc"))) entity.KyHieuCTHanMuc = reader.GetString(reader.GetOrdinal("KyHieuCTHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCTHanMuc"))) entity.SoCTHanMuc = reader.GetString(reader.GetOrdinal("SoCTHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaXDThoiHanNopThue"))) entity.MaXDThoiHanNopThue = reader.GetString(reader.GetOrdinal("MaXDThoiHanNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNHBaoLanh"))) entity.MaNHBaoLanh = reader.GetString(reader.GetOrdinal("MaNHBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamPhatHanhBL"))) entity.NamPhatHanhBL = reader.GetDecimal(reader.GetOrdinal("NamPhatHanhBL"));
				if (!reader.IsDBNull(reader.GetOrdinal("KyHieuCTBaoLanh"))) entity.KyHieuCTBaoLanh = reader.GetString(reader.GetOrdinal("KyHieuCTBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCTBaoLanh"))) entity.SoCTBaoLanh = reader.GetString(reader.GetOrdinal("SoCTBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNhapKhoDau"))) entity.NgayNhapKhoDau = reader.GetDateTime(reader.GetOrdinal("NgayNhapKhoDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanhVC"))) entity.NgayKhoiHanhVC = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanhVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemDichVC"))) entity.DiaDiemDichVC = reader.GetString(reader.GetOrdinal("DiaDiemDichVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDen"))) entity.NgayDen = reader.GetDateTime(reader.GetOrdinal("NgayDen"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuanLyNoiBoDN"))) entity.SoQuanLyNoiBoDN = reader.GetString(reader.GetOrdinal("SoQuanLyNoiBoDN"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiBaoCaoSuaDoi"))) entity.PhanLoaiBaoCaoSuaDoi = reader.GetString(reader.GetOrdinal("PhanLoaiBaoCaoSuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiKiemTra"))) entity.MaPhanLoaiKiemTra = reader.GetString(reader.GetOrdinal("MaPhanLoaiKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoThueDaiDien"))) entity.MaSoThueDaiDien = reader.GetString(reader.GetOrdinal("MaSoThueDaiDien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCoQuanHaiQuan"))) entity.TenCoQuanHaiQuan = reader.GetString(reader.GetOrdinal("TenCoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayThayDoiDangKy"))) entity.NgayThayDoiDangKy = reader.GetDateTime(reader.GetOrdinal("NgayThayDoiDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("BieuThiTruongHopHetHan"))) entity.BieuThiTruongHopHetHan = reader.GetString(reader.GetOrdinal("BieuThiTruongHopHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyHaiQuan"))) entity.TenDaiLyHaiQuan = reader.GetString(reader.GetOrdinal("TenDaiLyHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNhanVienHaiQuan"))) entity.MaNhanVienHaiQuan = reader.GetString(reader.GetOrdinal("MaNhanVienHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDDLuuKho"))) entity.TenDDLuuKho = reader.GetString(reader.GetOrdinal("TenDDLuuKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiTongGiaCoBan"))) entity.MaPhanLoaiTongGiaCoBan = reader.GetString(reader.GetOrdinal("MaPhanLoaiTongGiaCoBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiCongThucChuan"))) entity.PhanLoaiCongThucChuan = reader.GetString(reader.GetOrdinal("PhanLoaiCongThucChuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiDieuChinhTriGia"))) entity.MaPhanLoaiDieuChinhTriGia = reader.GetString(reader.GetOrdinal("MaPhanLoaiDieuChinhTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongPhapDieuChinhTriGia"))) entity.PhuongPhapDieuChinhTriGia = reader.GetString(reader.GetOrdinal("PhuongPhapDieuChinhTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTienThuePhaiNop"))) entity.TongTienThuePhaiNop = reader.GetDecimal(reader.GetOrdinal("TongTienThuePhaiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienBaoLanh"))) entity.SoTienBaoLanh = reader.GetDecimal(reader.GetOrdinal("SoTienBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenTruongDonViHaiQuan"))) entity.TenTruongDonViHaiQuan = reader.GetString(reader.GetOrdinal("TenTruongDonViHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapPhep"))) entity.NgayCapPhep = reader.GetDateTime(reader.GetOrdinal("NgayCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiThamTraSauThongQuan"))) entity.PhanLoaiThamTraSauThongQuan = reader.GetString(reader.GetOrdinal("PhanLoaiThamTraSauThongQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPheDuyetBP"))) entity.NgayPheDuyetBP = reader.GetDateTime(reader.GetOrdinal("NgayPheDuyetBP"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhKiemTraBP"))) entity.NgayHoanThanhKiemTraBP = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhKiemTraBP"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayDoiCapPhepNhapKhau"))) entity.SoNgayDoiCapPhepNhapKhau = reader.GetDecimal(reader.GetOrdinal("SoNgayDoiCapPhepNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSacThueAnHan_VAT"))) entity.MaSacThueAnHan_VAT = reader.GetString(reader.GetOrdinal("MaSacThueAnHan_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSacThueAnHan_VAT"))) entity.TenSacThueAnHan_VAT = reader.GetString(reader.GetOrdinal("TenSacThueAnHan_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("HanNopThueSauKhiAnHan_VAT"))) entity.HanNopThueSauKhiAnHan_VAT = reader.GetDateTime(reader.GetOrdinal("HanNopThueSauKhiAnHan_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiNopThue"))) entity.PhanLoaiNopThue = reader.GetString(reader.GetOrdinal("PhanLoaiNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTienThueXuatKhau"))) entity.TongSoTienThueXuatKhau = reader.GetDecimal(reader.GetOrdinal("TongSoTienThueXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTTongTienThueXuatKhau"))) entity.MaTTTongTienThueXuatKhau = reader.GetString(reader.GetOrdinal("MaTTTongTienThueXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTienLePhi"))) entity.TongSoTienLePhi = reader.GetDecimal(reader.GetOrdinal("TongSoTienLePhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTCuaSoTienBaoLanh"))) entity.MaTTCuaSoTienBaoLanh = reader.GetString(reader.GetOrdinal("MaTTCuaSoTienBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuanLyNguoiSuDung"))) entity.SoQuanLyNguoiSuDung = reader.GetString(reader.GetOrdinal("SoQuanLyNguoiSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhKiemTra"))) entity.NgayHoanThanhKiemTra = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTrangCuaToKhai"))) entity.TongSoTrangCuaToKhai = reader.GetDecimal(reader.GetOrdinal("TongSoTrangCuaToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoDongHangCuaToKhai"))) entity.TongSoDongHangCuaToKhai = reader.GetDecimal(reader.GetOrdinal("TongSoDongHangCuaToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBaoNopThue"))) entity.NgayKhaiBaoNopThue = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBaoNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanbanPhapQuy1"))) entity.MaVanbanPhapQuy1 = reader.GetString(reader.GetOrdinal("MaVanbanPhapQuy1"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanbanPhapQuy2"))) entity.MaVanbanPhapQuy2 = reader.GetString(reader.GetOrdinal("MaVanbanPhapQuy2"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanbanPhapQuy3"))) entity.MaVanbanPhapQuy3 = reader.GetString(reader.GetOrdinal("MaVanbanPhapQuy3"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanbanPhapQuy4"))) entity.MaVanbanPhapQuy4 = reader.GetString(reader.GetOrdinal("MaVanbanPhapQuy4"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaVanbanPhapQuy5"))) entity.MaVanbanPhapQuy5 = reader.GetString(reader.GetOrdinal("MaVanbanPhapQuy5"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_So"))) entity.HopDong_So = reader.GetString(reader.GetOrdinal("HopDong_So"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHang"))) entity.LoaiHang = reader.GetString(reader.GetOrdinal("LoaiHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Templ_1"))) entity.Templ_1 = reader.GetString(reader.GetOrdinal("Templ_1"));
                // Thêm vận đơn vào danh sách hiển thị và xuất ra excel.
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("SoDinhDanh"))) entity.SoDinhDanh = reader.GetString(reader.GetOrdinal("SoDinhDanh"));
                }
                catch (Exception)
                {
                }
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiMauDich> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiMauDich item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich VALUES(@SoToKhai, @SoToKhaiDauTien, @SoNhanhToKhai, @TongSoTKChiaNho, @SoToKhaiTNTX, @MaLoaiHinh, @MaPhanLoaiHH, @MaPhuongThucVT, @PhanLoaiToChuc, @CoQuanHaiQuan, @NhomXuLyHS, @ThoiHanTaiNhapTaiXuat, @NgayDangKy, @MaDonVi, @TenDonVi, @MaBuuChinhDonVi, @DiaChiDonVi, @SoDienThoaiDonVi, @MaUyThac, @TenUyThac, @MaDoiTac, @TenDoiTac, @MaBuuChinhDoiTac, @DiaChiDoiTac1, @DiaChiDoiTac2, @DiaChiDoiTac3, @DiaChiDoiTac4, @MaNuocDoiTac, @NguoiUyThacXK, @MaDaiLyHQ, @SoLuong, @MaDVTSoLuong, @TrongLuong, @MaDVTTrongLuong, @MaDDLuuKho, @SoHieuKyHieu, @MaPTVC, @TenPTVC, @NgayHangDen, @MaDiaDiemDoHang, @TenDiaDiemDohang, @MaDiaDiemXepHang, @TenDiaDiemXepHang, @SoLuongCont, @MaKetQuaKiemTra, @PhanLoaiHD, @SoTiepNhanHD, @SoHoaDon, @NgayPhatHanhHD, @PhuongThucTT, @PhanLoaiGiaHD, @MaDieuKienGiaHD, @MaTTHoaDon, @TongTriGiaHD, @MaPhanLoaiTriGia, @SoTiepNhanTKTriGia, @MaTTHieuChinhTriGia, @GiaHieuChinhTriGia, @MaPhanLoaiPhiVC, @MaTTPhiVC, @PhiVanChuyen, @MaPhanLoaiPhiBH, @MaTTPhiBH, @PhiBaoHiem, @SoDangKyBH, @ChiTietKhaiTriGia, @TriGiaTinhThue, @PhanLoaiKhongQDVND, @MaTTTriGiaTinhThue, @TongHeSoPhanBoTG, @MaLyDoDeNghiBP, @NguoiNopThue, @MaNHTraThueThay, @NamPhatHanhHM, @KyHieuCTHanMuc, @SoCTHanMuc, @MaXDThoiHanNopThue, @MaNHBaoLanh, @NamPhatHanhBL, @KyHieuCTBaoLanh, @SoCTBaoLanh, @NgayNhapKhoDau, @NgayKhoiHanhVC, @DiaDiemDichVC, @NgayDen, @GhiChu, @SoQuanLyNoiBoDN, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag, @PhanLoaiBaoCaoSuaDoi, @MaPhanLoaiKiemTra, @MaSoThueDaiDien, @TenCoQuanHaiQuan, @NgayThayDoiDangKy, @BieuThiTruongHopHetHan, @TenDaiLyHaiQuan, @MaNhanVienHaiQuan, @TenDDLuuKho, @MaPhanLoaiTongGiaCoBan, @PhanLoaiCongThucChuan, @MaPhanLoaiDieuChinhTriGia, @PhuongPhapDieuChinhTriGia, @TongTienThuePhaiNop, @SoTienBaoLanh, @TenTruongDonViHaiQuan, @NgayCapPhep, @PhanLoaiThamTraSauThongQuan, @NgayPheDuyetBP, @NgayHoanThanhKiemTraBP, @SoNgayDoiCapPhepNhapKhau, @TieuDe, @MaSacThueAnHan_VAT, @TenSacThueAnHan_VAT, @HanNopThueSauKhiAnHan_VAT, @PhanLoaiNopThue, @TongSoTienThueXuatKhau, @MaTTTongTienThueXuatKhau, @TongSoTienLePhi, @MaTTCuaSoTienBaoLanh, @SoQuanLyNguoiSuDung, @NgayHoanThanhKiemTra, @TongSoTrangCuaToKhai, @TongSoDongHangCuaToKhai, @NgayKhaiBaoNopThue, @MaVanbanPhapQuy1, @MaVanbanPhapQuy2, @MaVanbanPhapQuy3, @MaVanbanPhapQuy4, @MaVanbanPhapQuy5, @HopDong_ID, @HopDong_So, @LoaiHang, @Templ_1)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich SET SoToKhai = @SoToKhai, SoToKhaiDauTien = @SoToKhaiDauTien, SoNhanhToKhai = @SoNhanhToKhai, TongSoTKChiaNho = @TongSoTKChiaNho, SoToKhaiTNTX = @SoToKhaiTNTX, MaLoaiHinh = @MaLoaiHinh, MaPhanLoaiHH = @MaPhanLoaiHH, MaPhuongThucVT = @MaPhuongThucVT, PhanLoaiToChuc = @PhanLoaiToChuc, CoQuanHaiQuan = @CoQuanHaiQuan, NhomXuLyHS = @NhomXuLyHS, ThoiHanTaiNhapTaiXuat = @ThoiHanTaiNhapTaiXuat, NgayDangKy = @NgayDangKy, MaDonVi = @MaDonVi, TenDonVi = @TenDonVi, MaBuuChinhDonVi = @MaBuuChinhDonVi, DiaChiDonVi = @DiaChiDonVi, SoDienThoaiDonVi = @SoDienThoaiDonVi, MaUyThac = @MaUyThac, TenUyThac = @TenUyThac, MaDoiTac = @MaDoiTac, TenDoiTac = @TenDoiTac, MaBuuChinhDoiTac = @MaBuuChinhDoiTac, DiaChiDoiTac1 = @DiaChiDoiTac1, DiaChiDoiTac2 = @DiaChiDoiTac2, DiaChiDoiTac3 = @DiaChiDoiTac3, DiaChiDoiTac4 = @DiaChiDoiTac4, MaNuocDoiTac = @MaNuocDoiTac, NguoiUyThacXK = @NguoiUyThacXK, MaDaiLyHQ = @MaDaiLyHQ, SoLuong = @SoLuong, MaDVTSoLuong = @MaDVTSoLuong, TrongLuong = @TrongLuong, MaDVTTrongLuong = @MaDVTTrongLuong, MaDDLuuKho = @MaDDLuuKho, SoHieuKyHieu = @SoHieuKyHieu, MaPTVC = @MaPTVC, TenPTVC = @TenPTVC, NgayHangDen = @NgayHangDen, MaDiaDiemDoHang = @MaDiaDiemDoHang, TenDiaDiemDohang = @TenDiaDiemDohang, MaDiaDiemXepHang = @MaDiaDiemXepHang, TenDiaDiemXepHang = @TenDiaDiemXepHang, SoLuongCont = @SoLuongCont, MaKetQuaKiemTra = @MaKetQuaKiemTra, PhanLoaiHD = @PhanLoaiHD, SoTiepNhanHD = @SoTiepNhanHD, SoHoaDon = @SoHoaDon, NgayPhatHanhHD = @NgayPhatHanhHD, PhuongThucTT = @PhuongThucTT, PhanLoaiGiaHD = @PhanLoaiGiaHD, MaDieuKienGiaHD = @MaDieuKienGiaHD, MaTTHoaDon = @MaTTHoaDon, TongTriGiaHD = @TongTriGiaHD, MaPhanLoaiTriGia = @MaPhanLoaiTriGia, SoTiepNhanTKTriGia = @SoTiepNhanTKTriGia, MaTTHieuChinhTriGia = @MaTTHieuChinhTriGia, GiaHieuChinhTriGia = @GiaHieuChinhTriGia, MaPhanLoaiPhiVC = @MaPhanLoaiPhiVC, MaTTPhiVC = @MaTTPhiVC, PhiVanChuyen = @PhiVanChuyen, MaPhanLoaiPhiBH = @MaPhanLoaiPhiBH, MaTTPhiBH = @MaTTPhiBH, PhiBaoHiem = @PhiBaoHiem, SoDangKyBH = @SoDangKyBH, ChiTietKhaiTriGia = @ChiTietKhaiTriGia, TriGiaTinhThue = @TriGiaTinhThue, PhanLoaiKhongQDVND = @PhanLoaiKhongQDVND, MaTTTriGiaTinhThue = @MaTTTriGiaTinhThue, TongHeSoPhanBoTG = @TongHeSoPhanBoTG, MaLyDoDeNghiBP = @MaLyDoDeNghiBP, NguoiNopThue = @NguoiNopThue, MaNHTraThueThay = @MaNHTraThueThay, NamPhatHanhHM = @NamPhatHanhHM, KyHieuCTHanMuc = @KyHieuCTHanMuc, SoCTHanMuc = @SoCTHanMuc, MaXDThoiHanNopThue = @MaXDThoiHanNopThue, MaNHBaoLanh = @MaNHBaoLanh, NamPhatHanhBL = @NamPhatHanhBL, KyHieuCTBaoLanh = @KyHieuCTBaoLanh, SoCTBaoLanh = @SoCTBaoLanh, NgayNhapKhoDau = @NgayNhapKhoDau, NgayKhoiHanhVC = @NgayKhoiHanhVC, DiaDiemDichVC = @DiaDiemDichVC, NgayDen = @NgayDen, GhiChu = @GhiChu, SoQuanLyNoiBoDN = @SoQuanLyNoiBoDN, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, PhanLoaiBaoCaoSuaDoi = @PhanLoaiBaoCaoSuaDoi, MaPhanLoaiKiemTra = @MaPhanLoaiKiemTra, MaSoThueDaiDien = @MaSoThueDaiDien, TenCoQuanHaiQuan = @TenCoQuanHaiQuan, NgayThayDoiDangKy = @NgayThayDoiDangKy, BieuThiTruongHopHetHan = @BieuThiTruongHopHetHan, TenDaiLyHaiQuan = @TenDaiLyHaiQuan, MaNhanVienHaiQuan = @MaNhanVienHaiQuan, TenDDLuuKho = @TenDDLuuKho, MaPhanLoaiTongGiaCoBan = @MaPhanLoaiTongGiaCoBan, PhanLoaiCongThucChuan = @PhanLoaiCongThucChuan, MaPhanLoaiDieuChinhTriGia = @MaPhanLoaiDieuChinhTriGia, PhuongPhapDieuChinhTriGia = @PhuongPhapDieuChinhTriGia, TongTienThuePhaiNop = @TongTienThuePhaiNop, SoTienBaoLanh = @SoTienBaoLanh, TenTruongDonViHaiQuan = @TenTruongDonViHaiQuan, NgayCapPhep = @NgayCapPhep, PhanLoaiThamTraSauThongQuan = @PhanLoaiThamTraSauThongQuan, NgayPheDuyetBP = @NgayPheDuyetBP, NgayHoanThanhKiemTraBP = @NgayHoanThanhKiemTraBP, SoNgayDoiCapPhepNhapKhau = @SoNgayDoiCapPhepNhapKhau, TieuDe = @TieuDe, MaSacThueAnHan_VAT = @MaSacThueAnHan_VAT, TenSacThueAnHan_VAT = @TenSacThueAnHan_VAT, HanNopThueSauKhiAnHan_VAT = @HanNopThueSauKhiAnHan_VAT, PhanLoaiNopThue = @PhanLoaiNopThue, TongSoTienThueXuatKhau = @TongSoTienThueXuatKhau, MaTTTongTienThueXuatKhau = @MaTTTongTienThueXuatKhau, TongSoTienLePhi = @TongSoTienLePhi, MaTTCuaSoTienBaoLanh = @MaTTCuaSoTienBaoLanh, SoQuanLyNguoiSuDung = @SoQuanLyNguoiSuDung, NgayHoanThanhKiemTra = @NgayHoanThanhKiemTra, TongSoTrangCuaToKhai = @TongSoTrangCuaToKhai, TongSoDongHangCuaToKhai = @TongSoDongHangCuaToKhai, NgayKhaiBaoNopThue = @NgayKhaiBaoNopThue, MaVanbanPhapQuy1 = @MaVanbanPhapQuy1, MaVanbanPhapQuy2 = @MaVanbanPhapQuy2, MaVanbanPhapQuy3 = @MaVanbanPhapQuy3, MaVanbanPhapQuy4 = @MaVanbanPhapQuy4, MaVanbanPhapQuy5 = @MaVanbanPhapQuy5, HopDong_ID = @HopDong_ID, HopDong_So = @HopDong_So, LoaiHang = @LoaiHang, Templ_1 = @Templ_1 WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiDauTien", SqlDbType.VarChar, "SoToKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhanhToKhai", SqlDbType.Decimal, "SoNhanhToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTKChiaNho", SqlDbType.Decimal, "TongSoTKChiaNho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiTNTX", SqlDbType.Decimal, "SoToKhaiTNTX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiHH", SqlDbType.VarChar, "MaPhanLoaiHH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhuongThucVT", SqlDbType.VarChar, "MaPhuongThucVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, "PhanLoaiToChuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHS", SqlDbType.VarChar, "NhomXuLyHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, "ThoiHanTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonVi", SqlDbType.VarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, "MaBuuChinhDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDonVi", SqlDbType.NVarChar, "DiaChiDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, "SoDienThoaiDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaUyThac", SqlDbType.VarChar, "MaUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenUyThac", SqlDbType.NVarChar, "TenUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoiTac", SqlDbType.VarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, "MaBuuChinhDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, "DiaChiDoiTac1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, "DiaChiDoiTac2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, "DiaChiDoiTac3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, "DiaChiDoiTac4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocDoiTac", SqlDbType.VarChar, "MaNuocDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiUyThacXK", SqlDbType.NVarChar, "NguoiUyThacXK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTSoLuong", SqlDbType.VarChar, "MaDVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, "MaDVTTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDDLuuKho", SqlDbType.VarChar, "MaDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuKyHieu", SqlDbType.VarChar, "SoHieuKyHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPTVC", SqlDbType.VarChar, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPTVC", SqlDbType.NVarChar, "TenPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHangDen", SqlDbType.DateTime, "NgayHangDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemDohang", SqlDbType.NVarChar, "TenDiaDiemDohang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCont", SqlDbType.Decimal, "SoLuongCont", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaKiemTra", SqlDbType.VarChar, "MaKetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiHD", SqlDbType.VarChar, "PhanLoaiHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhanHD", SqlDbType.Decimal, "SoTiepNhanHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanhHD", SqlDbType.DateTime, "NgayPhatHanhHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongThucTT", SqlDbType.VarChar, "PhuongThucTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, "PhanLoaiGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, "MaDieuKienGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTHoaDon", SqlDbType.VarChar, "MaTTHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaHD", SqlDbType.Decimal, "TongTriGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTriGia", SqlDbType.VarChar, "MaPhanLoaiTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhanTKTriGia", SqlDbType.Decimal, "SoTiepNhanTKTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTHieuChinhTriGia", SqlDbType.VarChar, "MaTTHieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaHieuChinhTriGia", SqlDbType.Decimal, "GiaHieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, "MaPhanLoaiPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTPhiVC", SqlDbType.VarChar, "MaTTPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, "MaPhanLoaiPhiBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTPhiBH", SqlDbType.VarChar, "MaTTPhiBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyBH", SqlDbType.VarChar, "SoDangKyBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChiTietKhaiTriGia", SqlDbType.NVarChar, "ChiTietKhaiTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiKhongQDVND", SqlDbType.VarChar, "PhanLoaiKhongQDVND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, "MaTTTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongHeSoPhanBoTG", SqlDbType.Decimal, "TongHeSoPhanBoTG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLyDoDeNghiBP", SqlDbType.VarChar, "MaLyDoDeNghiBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiNopThue", SqlDbType.VarChar, "NguoiNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNHTraThueThay", SqlDbType.VarChar, "MaNHTraThueThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhHM", SqlDbType.Decimal, "NamPhatHanhHM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuCTHanMuc", SqlDbType.VarChar, "KyHieuCTHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCTHanMuc", SqlDbType.VarChar, "SoCTHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaXDThoiHanNopThue", SqlDbType.VarChar, "MaXDThoiHanNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNHBaoLanh", SqlDbType.VarChar, "MaNHBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhBL", SqlDbType.Decimal, "NamPhatHanhBL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuCTBaoLanh", SqlDbType.VarChar, "KyHieuCTBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCTBaoLanh", SqlDbType.VarChar, "SoCTBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayNhapKhoDau", SqlDbType.DateTime, "NgayNhapKhoDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhoiHanhVC", SqlDbType.DateTime, "NgayKhoiHanhVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemDichVC", SqlDbType.VarChar, "DiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDen", SqlDbType.DateTime, "NgayDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, "SoQuanLyNoiBoDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, "PhanLoaiBaoCaoSuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, "MaPhanLoaiKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, "MaSoThueDaiDien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, "NgayThayDoiDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, "BieuThiTruongHopHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDDLuuKho", SqlDbType.VarChar, "TenDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, "MaPhanLoaiTongGiaCoBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, "PhanLoaiCongThucChuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, "MaPhanLoaiDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, "PhuongPhapDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, "TongTienThuePhaiNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, "PhanLoaiThamTraSauThongQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, "NgayPheDuyetBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, "NgayHoanThanhKiemTraBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, "SoNgayDoiCapPhepNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.VarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, "MaSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, "TenSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, "HanNopThueSauKhiAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, "TongSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, "MaTTTongTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienLePhi", SqlDbType.Decimal, "TongSoTienLePhi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, "MaTTCuaSoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, "SoQuanLyNguoiSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTrangCuaToKhai", SqlDbType.Decimal, "TongSoTrangCuaToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoDongHangCuaToKhai", SqlDbType.Decimal, "TongSoDongHangCuaToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBaoNopThue", SqlDbType.DateTime, "NgayKhaiBaoNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy1", SqlDbType.VarChar, "MaVanbanPhapQuy1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy2", SqlDbType.VarChar, "MaVanbanPhapQuy2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy3", SqlDbType.VarChar, "MaVanbanPhapQuy3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy4", SqlDbType.VarChar, "MaVanbanPhapQuy4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy5", SqlDbType.VarChar, "MaVanbanPhapQuy5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_So", SqlDbType.VarChar, "HopDong_So", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHang", SqlDbType.VarChar, "LoaiHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Templ_1", SqlDbType.VarChar, "Templ_1", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiDauTien", SqlDbType.VarChar, "SoToKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhanhToKhai", SqlDbType.Decimal, "SoNhanhToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTKChiaNho", SqlDbType.Decimal, "TongSoTKChiaNho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiTNTX", SqlDbType.Decimal, "SoToKhaiTNTX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiHH", SqlDbType.VarChar, "MaPhanLoaiHH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhuongThucVT", SqlDbType.VarChar, "MaPhuongThucVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, "PhanLoaiToChuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHS", SqlDbType.VarChar, "NhomXuLyHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, "ThoiHanTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonVi", SqlDbType.VarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, "MaBuuChinhDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDonVi", SqlDbType.NVarChar, "DiaChiDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, "SoDienThoaiDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaUyThac", SqlDbType.VarChar, "MaUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenUyThac", SqlDbType.NVarChar, "TenUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoiTac", SqlDbType.VarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, "MaBuuChinhDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, "DiaChiDoiTac1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, "DiaChiDoiTac2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, "DiaChiDoiTac3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, "DiaChiDoiTac4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocDoiTac", SqlDbType.VarChar, "MaNuocDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiUyThacXK", SqlDbType.NVarChar, "NguoiUyThacXK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTSoLuong", SqlDbType.VarChar, "MaDVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, "MaDVTTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDDLuuKho", SqlDbType.VarChar, "MaDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuKyHieu", SqlDbType.VarChar, "SoHieuKyHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPTVC", SqlDbType.VarChar, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPTVC", SqlDbType.NVarChar, "TenPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHangDen", SqlDbType.DateTime, "NgayHangDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemDohang", SqlDbType.NVarChar, "TenDiaDiemDohang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCont", SqlDbType.Decimal, "SoLuongCont", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaKiemTra", SqlDbType.VarChar, "MaKetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiHD", SqlDbType.VarChar, "PhanLoaiHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhanHD", SqlDbType.Decimal, "SoTiepNhanHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanhHD", SqlDbType.DateTime, "NgayPhatHanhHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongThucTT", SqlDbType.VarChar, "PhuongThucTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, "PhanLoaiGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, "MaDieuKienGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTHoaDon", SqlDbType.VarChar, "MaTTHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaHD", SqlDbType.Decimal, "TongTriGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTriGia", SqlDbType.VarChar, "MaPhanLoaiTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhanTKTriGia", SqlDbType.Decimal, "SoTiepNhanTKTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTHieuChinhTriGia", SqlDbType.VarChar, "MaTTHieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaHieuChinhTriGia", SqlDbType.Decimal, "GiaHieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, "MaPhanLoaiPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTPhiVC", SqlDbType.VarChar, "MaTTPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, "MaPhanLoaiPhiBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTPhiBH", SqlDbType.VarChar, "MaTTPhiBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyBH", SqlDbType.VarChar, "SoDangKyBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChiTietKhaiTriGia", SqlDbType.NVarChar, "ChiTietKhaiTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiKhongQDVND", SqlDbType.VarChar, "PhanLoaiKhongQDVND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, "MaTTTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongHeSoPhanBoTG", SqlDbType.Decimal, "TongHeSoPhanBoTG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLyDoDeNghiBP", SqlDbType.VarChar, "MaLyDoDeNghiBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiNopThue", SqlDbType.VarChar, "NguoiNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNHTraThueThay", SqlDbType.VarChar, "MaNHTraThueThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhHM", SqlDbType.Decimal, "NamPhatHanhHM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuCTHanMuc", SqlDbType.VarChar, "KyHieuCTHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCTHanMuc", SqlDbType.VarChar, "SoCTHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaXDThoiHanNopThue", SqlDbType.VarChar, "MaXDThoiHanNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNHBaoLanh", SqlDbType.VarChar, "MaNHBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhBL", SqlDbType.Decimal, "NamPhatHanhBL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuCTBaoLanh", SqlDbType.VarChar, "KyHieuCTBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCTBaoLanh", SqlDbType.VarChar, "SoCTBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayNhapKhoDau", SqlDbType.DateTime, "NgayNhapKhoDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhoiHanhVC", SqlDbType.DateTime, "NgayKhoiHanhVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemDichVC", SqlDbType.VarChar, "DiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDen", SqlDbType.DateTime, "NgayDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, "SoQuanLyNoiBoDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, "PhanLoaiBaoCaoSuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, "MaPhanLoaiKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, "MaSoThueDaiDien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, "NgayThayDoiDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, "BieuThiTruongHopHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDDLuuKho", SqlDbType.VarChar, "TenDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, "MaPhanLoaiTongGiaCoBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, "PhanLoaiCongThucChuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, "MaPhanLoaiDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, "PhuongPhapDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, "TongTienThuePhaiNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, "PhanLoaiThamTraSauThongQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, "NgayPheDuyetBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, "NgayHoanThanhKiemTraBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, "SoNgayDoiCapPhepNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.VarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, "MaSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, "TenSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, "HanNopThueSauKhiAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, "TongSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, "MaTTTongTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienLePhi", SqlDbType.Decimal, "TongSoTienLePhi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, "MaTTCuaSoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, "SoQuanLyNguoiSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTrangCuaToKhai", SqlDbType.Decimal, "TongSoTrangCuaToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoDongHangCuaToKhai", SqlDbType.Decimal, "TongSoDongHangCuaToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBaoNopThue", SqlDbType.DateTime, "NgayKhaiBaoNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy1", SqlDbType.VarChar, "MaVanbanPhapQuy1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy2", SqlDbType.VarChar, "MaVanbanPhapQuy2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy3", SqlDbType.VarChar, "MaVanbanPhapQuy3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy4", SqlDbType.VarChar, "MaVanbanPhapQuy4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy5", SqlDbType.VarChar, "MaVanbanPhapQuy5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_So", SqlDbType.VarChar, "HopDong_So", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHang", SqlDbType.VarChar, "LoaiHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Templ_1", SqlDbType.VarChar, "Templ_1", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich VALUES(@SoToKhai, @SoToKhaiDauTien, @SoNhanhToKhai, @TongSoTKChiaNho, @SoToKhaiTNTX, @MaLoaiHinh, @MaPhanLoaiHH, @MaPhuongThucVT, @PhanLoaiToChuc, @CoQuanHaiQuan, @NhomXuLyHS, @ThoiHanTaiNhapTaiXuat, @NgayDangKy, @MaDonVi, @TenDonVi, @MaBuuChinhDonVi, @DiaChiDonVi, @SoDienThoaiDonVi, @MaUyThac, @TenUyThac, @MaDoiTac, @TenDoiTac, @MaBuuChinhDoiTac, @DiaChiDoiTac1, @DiaChiDoiTac2, @DiaChiDoiTac3, @DiaChiDoiTac4, @MaNuocDoiTac, @NguoiUyThacXK, @MaDaiLyHQ, @SoLuong, @MaDVTSoLuong, @TrongLuong, @MaDVTTrongLuong, @MaDDLuuKho, @SoHieuKyHieu, @MaPTVC, @TenPTVC, @NgayHangDen, @MaDiaDiemDoHang, @TenDiaDiemDohang, @MaDiaDiemXepHang, @TenDiaDiemXepHang, @SoLuongCont, @MaKetQuaKiemTra, @PhanLoaiHD, @SoTiepNhanHD, @SoHoaDon, @NgayPhatHanhHD, @PhuongThucTT, @PhanLoaiGiaHD, @MaDieuKienGiaHD, @MaTTHoaDon, @TongTriGiaHD, @MaPhanLoaiTriGia, @SoTiepNhanTKTriGia, @MaTTHieuChinhTriGia, @GiaHieuChinhTriGia, @MaPhanLoaiPhiVC, @MaTTPhiVC, @PhiVanChuyen, @MaPhanLoaiPhiBH, @MaTTPhiBH, @PhiBaoHiem, @SoDangKyBH, @ChiTietKhaiTriGia, @TriGiaTinhThue, @PhanLoaiKhongQDVND, @MaTTTriGiaTinhThue, @TongHeSoPhanBoTG, @MaLyDoDeNghiBP, @NguoiNopThue, @MaNHTraThueThay, @NamPhatHanhHM, @KyHieuCTHanMuc, @SoCTHanMuc, @MaXDThoiHanNopThue, @MaNHBaoLanh, @NamPhatHanhBL, @KyHieuCTBaoLanh, @SoCTBaoLanh, @NgayNhapKhoDau, @NgayKhoiHanhVC, @DiaDiemDichVC, @NgayDen, @GhiChu, @SoQuanLyNoiBoDN, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag, @PhanLoaiBaoCaoSuaDoi, @MaPhanLoaiKiemTra, @MaSoThueDaiDien, @TenCoQuanHaiQuan, @NgayThayDoiDangKy, @BieuThiTruongHopHetHan, @TenDaiLyHaiQuan, @MaNhanVienHaiQuan, @TenDDLuuKho, @MaPhanLoaiTongGiaCoBan, @PhanLoaiCongThucChuan, @MaPhanLoaiDieuChinhTriGia, @PhuongPhapDieuChinhTriGia, @TongTienThuePhaiNop, @SoTienBaoLanh, @TenTruongDonViHaiQuan, @NgayCapPhep, @PhanLoaiThamTraSauThongQuan, @NgayPheDuyetBP, @NgayHoanThanhKiemTraBP, @SoNgayDoiCapPhepNhapKhau, @TieuDe, @MaSacThueAnHan_VAT, @TenSacThueAnHan_VAT, @HanNopThueSauKhiAnHan_VAT, @PhanLoaiNopThue, @TongSoTienThueXuatKhau, @MaTTTongTienThueXuatKhau, @TongSoTienLePhi, @MaTTCuaSoTienBaoLanh, @SoQuanLyNguoiSuDung, @NgayHoanThanhKiemTra, @TongSoTrangCuaToKhai, @TongSoDongHangCuaToKhai, @NgayKhaiBaoNopThue, @MaVanbanPhapQuy1, @MaVanbanPhapQuy2, @MaVanbanPhapQuy3, @MaVanbanPhapQuy4, @MaVanbanPhapQuy5, @HopDong_ID, @HopDong_So, @LoaiHang, @Templ_1)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich SET SoToKhai = @SoToKhai, SoToKhaiDauTien = @SoToKhaiDauTien, SoNhanhToKhai = @SoNhanhToKhai, TongSoTKChiaNho = @TongSoTKChiaNho, SoToKhaiTNTX = @SoToKhaiTNTX, MaLoaiHinh = @MaLoaiHinh, MaPhanLoaiHH = @MaPhanLoaiHH, MaPhuongThucVT = @MaPhuongThucVT, PhanLoaiToChuc = @PhanLoaiToChuc, CoQuanHaiQuan = @CoQuanHaiQuan, NhomXuLyHS = @NhomXuLyHS, ThoiHanTaiNhapTaiXuat = @ThoiHanTaiNhapTaiXuat, NgayDangKy = @NgayDangKy, MaDonVi = @MaDonVi, TenDonVi = @TenDonVi, MaBuuChinhDonVi = @MaBuuChinhDonVi, DiaChiDonVi = @DiaChiDonVi, SoDienThoaiDonVi = @SoDienThoaiDonVi, MaUyThac = @MaUyThac, TenUyThac = @TenUyThac, MaDoiTac = @MaDoiTac, TenDoiTac = @TenDoiTac, MaBuuChinhDoiTac = @MaBuuChinhDoiTac, DiaChiDoiTac1 = @DiaChiDoiTac1, DiaChiDoiTac2 = @DiaChiDoiTac2, DiaChiDoiTac3 = @DiaChiDoiTac3, DiaChiDoiTac4 = @DiaChiDoiTac4, MaNuocDoiTac = @MaNuocDoiTac, NguoiUyThacXK = @NguoiUyThacXK, MaDaiLyHQ = @MaDaiLyHQ, SoLuong = @SoLuong, MaDVTSoLuong = @MaDVTSoLuong, TrongLuong = @TrongLuong, MaDVTTrongLuong = @MaDVTTrongLuong, MaDDLuuKho = @MaDDLuuKho, SoHieuKyHieu = @SoHieuKyHieu, MaPTVC = @MaPTVC, TenPTVC = @TenPTVC, NgayHangDen = @NgayHangDen, MaDiaDiemDoHang = @MaDiaDiemDoHang, TenDiaDiemDohang = @TenDiaDiemDohang, MaDiaDiemXepHang = @MaDiaDiemXepHang, TenDiaDiemXepHang = @TenDiaDiemXepHang, SoLuongCont = @SoLuongCont, MaKetQuaKiemTra = @MaKetQuaKiemTra, PhanLoaiHD = @PhanLoaiHD, SoTiepNhanHD = @SoTiepNhanHD, SoHoaDon = @SoHoaDon, NgayPhatHanhHD = @NgayPhatHanhHD, PhuongThucTT = @PhuongThucTT, PhanLoaiGiaHD = @PhanLoaiGiaHD, MaDieuKienGiaHD = @MaDieuKienGiaHD, MaTTHoaDon = @MaTTHoaDon, TongTriGiaHD = @TongTriGiaHD, MaPhanLoaiTriGia = @MaPhanLoaiTriGia, SoTiepNhanTKTriGia = @SoTiepNhanTKTriGia, MaTTHieuChinhTriGia = @MaTTHieuChinhTriGia, GiaHieuChinhTriGia = @GiaHieuChinhTriGia, MaPhanLoaiPhiVC = @MaPhanLoaiPhiVC, MaTTPhiVC = @MaTTPhiVC, PhiVanChuyen = @PhiVanChuyen, MaPhanLoaiPhiBH = @MaPhanLoaiPhiBH, MaTTPhiBH = @MaTTPhiBH, PhiBaoHiem = @PhiBaoHiem, SoDangKyBH = @SoDangKyBH, ChiTietKhaiTriGia = @ChiTietKhaiTriGia, TriGiaTinhThue = @TriGiaTinhThue, PhanLoaiKhongQDVND = @PhanLoaiKhongQDVND, MaTTTriGiaTinhThue = @MaTTTriGiaTinhThue, TongHeSoPhanBoTG = @TongHeSoPhanBoTG, MaLyDoDeNghiBP = @MaLyDoDeNghiBP, NguoiNopThue = @NguoiNopThue, MaNHTraThueThay = @MaNHTraThueThay, NamPhatHanhHM = @NamPhatHanhHM, KyHieuCTHanMuc = @KyHieuCTHanMuc, SoCTHanMuc = @SoCTHanMuc, MaXDThoiHanNopThue = @MaXDThoiHanNopThue, MaNHBaoLanh = @MaNHBaoLanh, NamPhatHanhBL = @NamPhatHanhBL, KyHieuCTBaoLanh = @KyHieuCTBaoLanh, SoCTBaoLanh = @SoCTBaoLanh, NgayNhapKhoDau = @NgayNhapKhoDau, NgayKhoiHanhVC = @NgayKhoiHanhVC, DiaDiemDichVC = @DiaDiemDichVC, NgayDen = @NgayDen, GhiChu = @GhiChu, SoQuanLyNoiBoDN = @SoQuanLyNoiBoDN, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, PhanLoaiBaoCaoSuaDoi = @PhanLoaiBaoCaoSuaDoi, MaPhanLoaiKiemTra = @MaPhanLoaiKiemTra, MaSoThueDaiDien = @MaSoThueDaiDien, TenCoQuanHaiQuan = @TenCoQuanHaiQuan, NgayThayDoiDangKy = @NgayThayDoiDangKy, BieuThiTruongHopHetHan = @BieuThiTruongHopHetHan, TenDaiLyHaiQuan = @TenDaiLyHaiQuan, MaNhanVienHaiQuan = @MaNhanVienHaiQuan, TenDDLuuKho = @TenDDLuuKho, MaPhanLoaiTongGiaCoBan = @MaPhanLoaiTongGiaCoBan, PhanLoaiCongThucChuan = @PhanLoaiCongThucChuan, MaPhanLoaiDieuChinhTriGia = @MaPhanLoaiDieuChinhTriGia, PhuongPhapDieuChinhTriGia = @PhuongPhapDieuChinhTriGia, TongTienThuePhaiNop = @TongTienThuePhaiNop, SoTienBaoLanh = @SoTienBaoLanh, TenTruongDonViHaiQuan = @TenTruongDonViHaiQuan, NgayCapPhep = @NgayCapPhep, PhanLoaiThamTraSauThongQuan = @PhanLoaiThamTraSauThongQuan, NgayPheDuyetBP = @NgayPheDuyetBP, NgayHoanThanhKiemTraBP = @NgayHoanThanhKiemTraBP, SoNgayDoiCapPhepNhapKhau = @SoNgayDoiCapPhepNhapKhau, TieuDe = @TieuDe, MaSacThueAnHan_VAT = @MaSacThueAnHan_VAT, TenSacThueAnHan_VAT = @TenSacThueAnHan_VAT, HanNopThueSauKhiAnHan_VAT = @HanNopThueSauKhiAnHan_VAT, PhanLoaiNopThue = @PhanLoaiNopThue, TongSoTienThueXuatKhau = @TongSoTienThueXuatKhau, MaTTTongTienThueXuatKhau = @MaTTTongTienThueXuatKhau, TongSoTienLePhi = @TongSoTienLePhi, MaTTCuaSoTienBaoLanh = @MaTTCuaSoTienBaoLanh, SoQuanLyNguoiSuDung = @SoQuanLyNguoiSuDung, NgayHoanThanhKiemTra = @NgayHoanThanhKiemTra, TongSoTrangCuaToKhai = @TongSoTrangCuaToKhai, TongSoDongHangCuaToKhai = @TongSoDongHangCuaToKhai, NgayKhaiBaoNopThue = @NgayKhaiBaoNopThue, MaVanbanPhapQuy1 = @MaVanbanPhapQuy1, MaVanbanPhapQuy2 = @MaVanbanPhapQuy2, MaVanbanPhapQuy3 = @MaVanbanPhapQuy3, MaVanbanPhapQuy4 = @MaVanbanPhapQuy4, MaVanbanPhapQuy5 = @MaVanbanPhapQuy5, HopDong_ID = @HopDong_ID, HopDong_So = @HopDong_So, LoaiHang = @LoaiHang, Templ_1 = @Templ_1 WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiDauTien", SqlDbType.VarChar, "SoToKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhanhToKhai", SqlDbType.Decimal, "SoNhanhToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTKChiaNho", SqlDbType.Decimal, "TongSoTKChiaNho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiTNTX", SqlDbType.Decimal, "SoToKhaiTNTX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiHH", SqlDbType.VarChar, "MaPhanLoaiHH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhuongThucVT", SqlDbType.VarChar, "MaPhuongThucVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, "PhanLoaiToChuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHS", SqlDbType.VarChar, "NhomXuLyHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, "ThoiHanTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonVi", SqlDbType.VarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, "MaBuuChinhDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDonVi", SqlDbType.NVarChar, "DiaChiDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, "SoDienThoaiDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaUyThac", SqlDbType.VarChar, "MaUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenUyThac", SqlDbType.NVarChar, "TenUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoiTac", SqlDbType.VarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, "MaBuuChinhDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, "DiaChiDoiTac1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, "DiaChiDoiTac2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, "DiaChiDoiTac3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, "DiaChiDoiTac4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocDoiTac", SqlDbType.VarChar, "MaNuocDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiUyThacXK", SqlDbType.NVarChar, "NguoiUyThacXK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTSoLuong", SqlDbType.VarChar, "MaDVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, "MaDVTTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDDLuuKho", SqlDbType.VarChar, "MaDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuKyHieu", SqlDbType.VarChar, "SoHieuKyHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPTVC", SqlDbType.VarChar, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPTVC", SqlDbType.NVarChar, "TenPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHangDen", SqlDbType.DateTime, "NgayHangDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemDohang", SqlDbType.NVarChar, "TenDiaDiemDohang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCont", SqlDbType.Decimal, "SoLuongCont", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaKiemTra", SqlDbType.VarChar, "MaKetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiHD", SqlDbType.VarChar, "PhanLoaiHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhanHD", SqlDbType.Decimal, "SoTiepNhanHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanhHD", SqlDbType.DateTime, "NgayPhatHanhHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongThucTT", SqlDbType.VarChar, "PhuongThucTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, "PhanLoaiGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, "MaDieuKienGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTHoaDon", SqlDbType.VarChar, "MaTTHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaHD", SqlDbType.Decimal, "TongTriGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTriGia", SqlDbType.VarChar, "MaPhanLoaiTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhanTKTriGia", SqlDbType.Decimal, "SoTiepNhanTKTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTHieuChinhTriGia", SqlDbType.VarChar, "MaTTHieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaHieuChinhTriGia", SqlDbType.Decimal, "GiaHieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, "MaPhanLoaiPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTPhiVC", SqlDbType.VarChar, "MaTTPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, "MaPhanLoaiPhiBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTPhiBH", SqlDbType.VarChar, "MaTTPhiBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyBH", SqlDbType.VarChar, "SoDangKyBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChiTietKhaiTriGia", SqlDbType.NVarChar, "ChiTietKhaiTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiKhongQDVND", SqlDbType.VarChar, "PhanLoaiKhongQDVND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, "MaTTTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongHeSoPhanBoTG", SqlDbType.Decimal, "TongHeSoPhanBoTG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLyDoDeNghiBP", SqlDbType.VarChar, "MaLyDoDeNghiBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiNopThue", SqlDbType.VarChar, "NguoiNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNHTraThueThay", SqlDbType.VarChar, "MaNHTraThueThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhHM", SqlDbType.Decimal, "NamPhatHanhHM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuCTHanMuc", SqlDbType.VarChar, "KyHieuCTHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCTHanMuc", SqlDbType.VarChar, "SoCTHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaXDThoiHanNopThue", SqlDbType.VarChar, "MaXDThoiHanNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNHBaoLanh", SqlDbType.VarChar, "MaNHBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhBL", SqlDbType.Decimal, "NamPhatHanhBL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuCTBaoLanh", SqlDbType.VarChar, "KyHieuCTBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCTBaoLanh", SqlDbType.VarChar, "SoCTBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayNhapKhoDau", SqlDbType.DateTime, "NgayNhapKhoDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhoiHanhVC", SqlDbType.DateTime, "NgayKhoiHanhVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemDichVC", SqlDbType.VarChar, "DiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDen", SqlDbType.DateTime, "NgayDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, "SoQuanLyNoiBoDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, "PhanLoaiBaoCaoSuaDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, "MaPhanLoaiKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, "MaSoThueDaiDien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, "NgayThayDoiDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, "BieuThiTruongHopHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDDLuuKho", SqlDbType.VarChar, "TenDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, "MaPhanLoaiTongGiaCoBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, "PhanLoaiCongThucChuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, "MaPhanLoaiDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, "PhuongPhapDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, "TongTienThuePhaiNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, "PhanLoaiThamTraSauThongQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, "NgayPheDuyetBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, "NgayHoanThanhKiemTraBP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, "SoNgayDoiCapPhepNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.VarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, "MaSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, "TenSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, "HanNopThueSauKhiAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, "TongSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, "MaTTTongTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienLePhi", SqlDbType.Decimal, "TongSoTienLePhi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, "MaTTCuaSoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, "SoQuanLyNguoiSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTrangCuaToKhai", SqlDbType.Decimal, "TongSoTrangCuaToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoDongHangCuaToKhai", SqlDbType.Decimal, "TongSoDongHangCuaToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBaoNopThue", SqlDbType.DateTime, "NgayKhaiBaoNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy1", SqlDbType.VarChar, "MaVanbanPhapQuy1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy2", SqlDbType.VarChar, "MaVanbanPhapQuy2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy3", SqlDbType.VarChar, "MaVanbanPhapQuy3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy4", SqlDbType.VarChar, "MaVanbanPhapQuy4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaVanbanPhapQuy5", SqlDbType.VarChar, "MaVanbanPhapQuy5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_So", SqlDbType.VarChar, "HopDong_So", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHang", SqlDbType.VarChar, "LoaiHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Templ_1", SqlDbType.VarChar, "Templ_1", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiDauTien", SqlDbType.VarChar, "SoToKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhanhToKhai", SqlDbType.Decimal, "SoNhanhToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTKChiaNho", SqlDbType.Decimal, "TongSoTKChiaNho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiTNTX", SqlDbType.Decimal, "SoToKhaiTNTX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiHH", SqlDbType.VarChar, "MaPhanLoaiHH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhuongThucVT", SqlDbType.VarChar, "MaPhuongThucVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, "PhanLoaiToChuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHS", SqlDbType.VarChar, "NhomXuLyHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, "ThoiHanTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonVi", SqlDbType.VarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, "MaBuuChinhDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDonVi", SqlDbType.NVarChar, "DiaChiDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, "SoDienThoaiDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaUyThac", SqlDbType.VarChar, "MaUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenUyThac", SqlDbType.NVarChar, "TenUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoiTac", SqlDbType.VarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, "MaBuuChinhDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, "DiaChiDoiTac1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, "DiaChiDoiTac2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, "DiaChiDoiTac3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, "DiaChiDoiTac4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocDoiTac", SqlDbType.VarChar, "MaNuocDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiUyThacXK", SqlDbType.NVarChar, "NguoiUyThacXK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTSoLuong", SqlDbType.VarChar, "MaDVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, "MaDVTTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDDLuuKho", SqlDbType.VarChar, "MaDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuKyHieu", SqlDbType.VarChar, "SoHieuKyHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPTVC", SqlDbType.VarChar, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPTVC", SqlDbType.NVarChar, "TenPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHangDen", SqlDbType.DateTime, "NgayHangDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemDohang", SqlDbType.NVarChar, "TenDiaDiemDohang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCont", SqlDbType.Decimal, "SoLuongCont", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaKiemTra", SqlDbType.VarChar, "MaKetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiHD", SqlDbType.VarChar, "PhanLoaiHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhanHD", SqlDbType.Decimal, "SoTiepNhanHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanhHD", SqlDbType.DateTime, "NgayPhatHanhHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongThucTT", SqlDbType.VarChar, "PhuongThucTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, "PhanLoaiGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, "MaDieuKienGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTHoaDon", SqlDbType.VarChar, "MaTTHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaHD", SqlDbType.Decimal, "TongTriGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTriGia", SqlDbType.VarChar, "MaPhanLoaiTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhanTKTriGia", SqlDbType.Decimal, "SoTiepNhanTKTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTHieuChinhTriGia", SqlDbType.VarChar, "MaTTHieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaHieuChinhTriGia", SqlDbType.Decimal, "GiaHieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, "MaPhanLoaiPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTPhiVC", SqlDbType.VarChar, "MaTTPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, "MaPhanLoaiPhiBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTPhiBH", SqlDbType.VarChar, "MaTTPhiBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyBH", SqlDbType.VarChar, "SoDangKyBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChiTietKhaiTriGia", SqlDbType.NVarChar, "ChiTietKhaiTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiKhongQDVND", SqlDbType.VarChar, "PhanLoaiKhongQDVND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, "MaTTTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongHeSoPhanBoTG", SqlDbType.Decimal, "TongHeSoPhanBoTG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLyDoDeNghiBP", SqlDbType.VarChar, "MaLyDoDeNghiBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiNopThue", SqlDbType.VarChar, "NguoiNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNHTraThueThay", SqlDbType.VarChar, "MaNHTraThueThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhHM", SqlDbType.Decimal, "NamPhatHanhHM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuCTHanMuc", SqlDbType.VarChar, "KyHieuCTHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCTHanMuc", SqlDbType.VarChar, "SoCTHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaXDThoiHanNopThue", SqlDbType.VarChar, "MaXDThoiHanNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNHBaoLanh", SqlDbType.VarChar, "MaNHBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhBL", SqlDbType.Decimal, "NamPhatHanhBL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuCTBaoLanh", SqlDbType.VarChar, "KyHieuCTBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCTBaoLanh", SqlDbType.VarChar, "SoCTBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayNhapKhoDau", SqlDbType.DateTime, "NgayNhapKhoDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhoiHanhVC", SqlDbType.DateTime, "NgayKhoiHanhVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemDichVC", SqlDbType.VarChar, "DiaDiemDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDen", SqlDbType.DateTime, "NgayDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, "SoQuanLyNoiBoDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, "PhanLoaiBaoCaoSuaDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, "MaPhanLoaiKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, "MaSoThueDaiDien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, "TenCoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, "NgayThayDoiDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, "BieuThiTruongHopHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDDLuuKho", SqlDbType.VarChar, "TenDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, "MaPhanLoaiTongGiaCoBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, "PhanLoaiCongThucChuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, "MaPhanLoaiDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, "PhuongPhapDieuChinhTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, "TongTienThuePhaiNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienBaoLanh", SqlDbType.Decimal, "SoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, "PhanLoaiThamTraSauThongQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, "NgayPheDuyetBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, "NgayHoanThanhKiemTraBP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, "SoNgayDoiCapPhepNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.VarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, "MaSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, "TenSacThueAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, "HanNopThueSauKhiAnHan_VAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, "TongSoTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, "MaTTTongTienThueXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienLePhi", SqlDbType.Decimal, "TongSoTienLePhi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, "MaTTCuaSoTienBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, "SoQuanLyNguoiSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTrangCuaToKhai", SqlDbType.Decimal, "TongSoTrangCuaToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoDongHangCuaToKhai", SqlDbType.Decimal, "TongSoDongHangCuaToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBaoNopThue", SqlDbType.DateTime, "NgayKhaiBaoNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy1", SqlDbType.VarChar, "MaVanbanPhapQuy1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy2", SqlDbType.VarChar, "MaVanbanPhapQuy2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy3", SqlDbType.VarChar, "MaVanbanPhapQuy3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy4", SqlDbType.VarChar, "MaVanbanPhapQuy4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaVanbanPhapQuy5", SqlDbType.VarChar, "MaVanbanPhapQuy5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_So", SqlDbType.VarChar, "HopDong_So", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHang", SqlDbType.VarChar, "LoaiHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Templ_1", SqlDbType.VarChar, "Templ_1", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
        public static string CheckBillAndInvoice(string Bill, string Invoice ,decimal SoToKhai)
        {
            string sql = "";
            if (Invoice == "''")
            {
                sql = "SELECT SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich LEFT JOIN dbo.t_KDT_VNACC_TK_SoVanDon ON t_KDT_VNACC_TK_SoVanDon.TKMD_ID = t_KDT_VNACC_ToKhaiMauDich.ID WHERE SoHoaDon = '" + Bill + "' AND SoToKhai NOT IN " + "(" + SoToKhai + ")";
            }
            else
            {
                sql = "SELECT SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich LEFT JOIN dbo.t_KDT_VNACC_TK_SoVanDon ON t_KDT_VNACC_TK_SoVanDon.TKMD_ID = t_KDT_VNACC_ToKhaiMauDich.ID WHERE SoHoaDon = '" + Bill + "' AND SoDinhDanh IN " + "(" + Invoice + ") AND SoToKhai NOT IN " + "(" + SoToKhai + ")";
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.NVarChar, Bill);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return null;
            else
                return obj.ToString();
        }
		public static KDT_VNACC_ToKhaiMauDich Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiMauDich> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicVoucher(string whereCondition, string orderByExpression)
        {
//            string Sql = string.Format(@"SELECT TKMD.ID,
//		SoToKhai,
//		NgayDangKy,
//        TKMD.MaLoaiHinh,
//	    SoHoaDon,
//		CASE WHEN TKMD.HopDong_ID > 0 THEN (SELECT TOP 1 SoHopDong FROM dbo.t_KDT_GC_HopDong WHERE TKMD.HopDong_ID=ID) ELSE NULL END AS HopDong_ID,
//		MaPhanLoaiKiemTra,
//		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_CertificateOfOrigin WHERE TKMD_ID = TKMD.ID ) AS CertificateOfOrigin,
//		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_BillOfLading WHERE TKMD_ID = TKMD.ID ) AS BillOfLading,
//		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_ContractDocument WHERE TKMD_ID = TKMD.ID ) AS ContractDocument,
//		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_CommercialInvoice WHERE TKMD_ID = TKMD.ID ) AS CommercialInvoice,
//		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_License WHERE TKMD_ID =TKMD.ID ) AS License,
//		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_Container_Details WHERE TKMD_ID = TKMD.ID ) AS Container,
//		(SELECT COUNT(*) FROM dbo.t_KDT_VNACCS_AdditionalDocument WHERE TKMD_ID = TKMD.ID ) AS AdditionalDocument
//		FROM dbo.t_KDT_VNACC_ToKhaiMauDich TKMD 
//LEFT JOIN t_KDT_VNACCS_CertificateOfOrigin CO ON CO.TKMD_ID = TKMD.ID
//LEFT JOIN t_KDT_VNACCS_BillOfLading BL ON BL.TKMD_ID = TKMD.ID
//LEFT JOIN t_KDT_VNACCS_ContractDocument CT ON CT.TKMD_ID = TKMD.ID
//LEFT JOIN t_KDT_VNACCS_CommercialInvoice CM ON CM.TKMD_ID = TKMD.ID
//LEFT JOIN t_KDT_VNACCS_License LC ON LC.TKMD_ID = TKMD.ID
//LEFT JOIN t_KDT_VNACCS_Container_Details CTD ON CTD.TKMD_ID = TKMD.ID
//LEFT JOIN t_KDT_VNACCS_AdditionalDocument AD ON AD.TKMD_ID = TKMD.ID WHERE {0} GROUP BY TKMD.ID,TKMD.SoToKhai,TKMD.NgayDangKy,TKMD.MaLoaiHinh,TKMD.SoHoaDon,TKMD.HopDong_ID,TKMD.MaPhanLoaiKiemTra ", whereCondition);
//            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
//            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
//            return db.ExecuteDataSet(dbCommand);

            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamicVouchers]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            //db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
        //
        public static DataSet BaoCaoChiTietHHXNK(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoChiTietHHXNK]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
        public static DataSet GetAllVouchers()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_Voucher]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteDataSet(dbCommand);
        }
        public static DataSet ThongKeToKhaiXNK(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThongKeToKhaiXNK]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        public static DataSet BaoCaoTongHopHHXNK(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoTongHopHHXNK]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@GroupByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiMauDich(decimal soToKhai, string soToKhaiDauTien, decimal soNhanhToKhai, decimal tongSoTKChiaNho, decimal soToKhaiTNTX, string maLoaiHinh, string maPhanLoaiHH, string maPhuongThucVT, string phanLoaiToChuc, string coQuanHaiQuan, string nhomXuLyHS, DateTime thoiHanTaiNhapTaiXuat, DateTime ngayDangKy, string maDonVi, string tenDonVi, string maBuuChinhDonVi, string diaChiDonVi, string soDienThoaiDonVi, string maUyThac, string tenUyThac, string maDoiTac, string tenDoiTac, string maBuuChinhDoiTac, string diaChiDoiTac1, string diaChiDoiTac2, string diaChiDoiTac3, string diaChiDoiTac4, string maNuocDoiTac, string nguoiUyThacXK, string maDaiLyHQ, decimal soLuong, string maDVTSoLuong, decimal trongLuong, string maDVTTrongLuong, string maDDLuuKho, string soHieuKyHieu, string maPTVC, string tenPTVC, DateTime ngayHangDen, string maDiaDiemDoHang, string tenDiaDiemDohang, string maDiaDiemXepHang, string tenDiaDiemXepHang, decimal soLuongCont, string maKetQuaKiemTra, string phanLoaiHD, decimal soTiepNhanHD, string soHoaDon, DateTime ngayPhatHanhHD, string phuongThucTT, string phanLoaiGiaHD, string maDieuKienGiaHD, string maTTHoaDon, decimal tongTriGiaHD, string maPhanLoaiTriGia, decimal soTiepNhanTKTriGia, string maTTHieuChinhTriGia, decimal giaHieuChinhTriGia, string maPhanLoaiPhiVC, string maTTPhiVC, decimal phiVanChuyen, string maPhanLoaiPhiBH, string maTTPhiBH, decimal phiBaoHiem, string soDangKyBH, string chiTietKhaiTriGia, decimal triGiaTinhThue, string phanLoaiKhongQDVND, string maTTTriGiaTinhThue, decimal tongHeSoPhanBoTG, string maLyDoDeNghiBP, string nguoiNopThue, string maNHTraThueThay, decimal namPhatHanhHM, string kyHieuCTHanMuc, string soCTHanMuc, string maXDThoiHanNopThue, string maNHBaoLanh, decimal namPhatHanhBL, string kyHieuCTBaoLanh, string soCTBaoLanh, DateTime ngayNhapKhoDau, DateTime ngayKhoiHanhVC, string diaDiemDichVC, DateTime ngayDen, string ghiChu, string soQuanLyNoiBoDN, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag, string phanLoaiBaoCaoSuaDoi, string maPhanLoaiKiemTra, string maSoThueDaiDien, string tenCoQuanHaiQuan, DateTime ngayThayDoiDangKy, string bieuThiTruongHopHetHan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string tenDDLuuKho, string maPhanLoaiTongGiaCoBan, string phanLoaiCongThucChuan, string maPhanLoaiDieuChinhTriGia, string phuongPhapDieuChinhTriGia, decimal tongTienThuePhaiNop, decimal soTienBaoLanh, string tenTruongDonViHaiQuan, DateTime ngayCapPhep, string phanLoaiThamTraSauThongQuan, DateTime ngayPheDuyetBP, DateTime ngayHoanThanhKiemTraBP, decimal soNgayDoiCapPhepNhapKhau, string tieuDe, string maSacThueAnHan_VAT, string tenSacThueAnHan_VAT, DateTime hanNopThueSauKhiAnHan_VAT, string phanLoaiNopThue, decimal tongSoTienThueXuatKhau, string maTTTongTienThueXuatKhau, decimal tongSoTienLePhi, string maTTCuaSoTienBaoLanh, string soQuanLyNguoiSuDung, DateTime ngayHoanThanhKiemTra, decimal tongSoTrangCuaToKhai, decimal tongSoDongHangCuaToKhai, DateTime ngayKhaiBaoNopThue, string maVanbanPhapQuy1, string maVanbanPhapQuy2, string maVanbanPhapQuy3, string maVanbanPhapQuy4, string maVanbanPhapQuy5, long hopDong_ID, string hopDong_So, string loaiHang, string templ_1)
		{
			KDT_VNACC_ToKhaiMauDich entity = new KDT_VNACC_ToKhaiMauDich();	
			entity.SoToKhai = soToKhai;
			entity.SoToKhaiDauTien = soToKhaiDauTien;
			entity.SoNhanhToKhai = soNhanhToKhai;
			entity.TongSoTKChiaNho = tongSoTKChiaNho;
			entity.SoToKhaiTNTX = soToKhaiTNTX;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaPhanLoaiHH = maPhanLoaiHH;
			entity.MaPhuongThucVT = maPhuongThucVT;
			entity.PhanLoaiToChuc = phanLoaiToChuc;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHS = nhomXuLyHS;
			entity.ThoiHanTaiNhapTaiXuat = thoiHanTaiNhapTaiXuat;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.MaBuuChinhDonVi = maBuuChinhDonVi;
			entity.DiaChiDonVi = diaChiDonVi;
			entity.SoDienThoaiDonVi = soDienThoaiDonVi;
			entity.MaUyThac = maUyThac;
			entity.TenUyThac = tenUyThac;
			entity.MaDoiTac = maDoiTac;
			entity.TenDoiTac = tenDoiTac;
			entity.MaBuuChinhDoiTac = maBuuChinhDoiTac;
			entity.DiaChiDoiTac1 = diaChiDoiTac1;
			entity.DiaChiDoiTac2 = diaChiDoiTac2;
			entity.DiaChiDoiTac3 = diaChiDoiTac3;
			entity.DiaChiDoiTac4 = diaChiDoiTac4;
			entity.MaNuocDoiTac = maNuocDoiTac;
			entity.NguoiUyThacXK = nguoiUyThacXK;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.SoLuong = soLuong;
			entity.MaDVTSoLuong = maDVTSoLuong;
			entity.TrongLuong = trongLuong;
			entity.MaDVTTrongLuong = maDVTTrongLuong;
			entity.MaDDLuuKho = maDDLuuKho;
			entity.SoHieuKyHieu = soHieuKyHieu;
			entity.MaPTVC = maPTVC;
			entity.TenPTVC = tenPTVC;
			entity.NgayHangDen = ngayHangDen;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDohang = tenDiaDiemDohang;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.SoLuongCont = soLuongCont;
			entity.MaKetQuaKiemTra = maKetQuaKiemTra;
			entity.PhanLoaiHD = phanLoaiHD;
			entity.SoTiepNhanHD = soTiepNhanHD;
			entity.SoHoaDon = soHoaDon;
			entity.NgayPhatHanhHD = ngayPhatHanhHD;
			entity.PhuongThucTT = phuongThucTT;
			entity.PhanLoaiGiaHD = phanLoaiGiaHD;
			entity.MaDieuKienGiaHD = maDieuKienGiaHD;
			entity.MaTTHoaDon = maTTHoaDon;
			entity.TongTriGiaHD = tongTriGiaHD;
			entity.MaPhanLoaiTriGia = maPhanLoaiTriGia;
			entity.SoTiepNhanTKTriGia = soTiepNhanTKTriGia;
			entity.MaTTHieuChinhTriGia = maTTHieuChinhTriGia;
			entity.GiaHieuChinhTriGia = giaHieuChinhTriGia;
			entity.MaPhanLoaiPhiVC = maPhanLoaiPhiVC;
			entity.MaTTPhiVC = maTTPhiVC;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaPhanLoaiPhiBH = maPhanLoaiPhiBH;
			entity.MaTTPhiBH = maTTPhiBH;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.SoDangKyBH = soDangKyBH;
			entity.ChiTietKhaiTriGia = chiTietKhaiTriGia;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.PhanLoaiKhongQDVND = phanLoaiKhongQDVND;
			entity.MaTTTriGiaTinhThue = maTTTriGiaTinhThue;
			entity.TongHeSoPhanBoTG = tongHeSoPhanBoTG;
			entity.MaLyDoDeNghiBP = maLyDoDeNghiBP;
			entity.NguoiNopThue = nguoiNopThue;
			entity.MaNHTraThueThay = maNHTraThueThay;
			entity.NamPhatHanhHM = namPhatHanhHM;
			entity.KyHieuCTHanMuc = kyHieuCTHanMuc;
			entity.SoCTHanMuc = soCTHanMuc;
			entity.MaXDThoiHanNopThue = maXDThoiHanNopThue;
			entity.MaNHBaoLanh = maNHBaoLanh;
			entity.NamPhatHanhBL = namPhatHanhBL;
			entity.KyHieuCTBaoLanh = kyHieuCTBaoLanh;
			entity.SoCTBaoLanh = soCTBaoLanh;
			entity.NgayNhapKhoDau = ngayNhapKhoDau;
			entity.NgayKhoiHanhVC = ngayKhoiHanhVC;
			entity.DiaDiemDichVC = diaDiemDichVC;
			entity.NgayDen = ngayDen;
			entity.GhiChu = ghiChu;
			entity.SoQuanLyNoiBoDN = soQuanLyNoiBoDN;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.PhanLoaiBaoCaoSuaDoi = phanLoaiBaoCaoSuaDoi;
			entity.MaPhanLoaiKiemTra = maPhanLoaiKiemTra;
			entity.MaSoThueDaiDien = maSoThueDaiDien;
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.NgayThayDoiDangKy = ngayThayDoiDangKy;
			entity.BieuThiTruongHopHetHan = bieuThiTruongHopHetHan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.TenDDLuuKho = tenDDLuuKho;
			entity.MaPhanLoaiTongGiaCoBan = maPhanLoaiTongGiaCoBan;
			entity.PhanLoaiCongThucChuan = phanLoaiCongThucChuan;
			entity.MaPhanLoaiDieuChinhTriGia = maPhanLoaiDieuChinhTriGia;
			entity.PhuongPhapDieuChinhTriGia = phuongPhapDieuChinhTriGia;
			entity.TongTienThuePhaiNop = tongTienThuePhaiNop;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayCapPhep = ngayCapPhep;
			entity.PhanLoaiThamTraSauThongQuan = phanLoaiThamTraSauThongQuan;
			entity.NgayPheDuyetBP = ngayPheDuyetBP;
			entity.NgayHoanThanhKiemTraBP = ngayHoanThanhKiemTraBP;
			entity.SoNgayDoiCapPhepNhapKhau = soNgayDoiCapPhepNhapKhau;
			entity.TieuDe = tieuDe;
			entity.MaSacThueAnHan_VAT = maSacThueAnHan_VAT;
			entity.TenSacThueAnHan_VAT = tenSacThueAnHan_VAT;
			entity.HanNopThueSauKhiAnHan_VAT = hanNopThueSauKhiAnHan_VAT;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.TongSoTienThueXuatKhau = tongSoTienThueXuatKhau;
			entity.MaTTTongTienThueXuatKhau = maTTTongTienThueXuatKhau;
			entity.TongSoTienLePhi = tongSoTienLePhi;
			entity.MaTTCuaSoTienBaoLanh = maTTCuaSoTienBaoLanh;
			entity.SoQuanLyNguoiSuDung = soQuanLyNguoiSuDung;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			entity.TongSoTrangCuaToKhai = tongSoTrangCuaToKhai;
			entity.TongSoDongHangCuaToKhai = tongSoDongHangCuaToKhai;
			entity.NgayKhaiBaoNopThue = ngayKhaiBaoNopThue;
			entity.MaVanbanPhapQuy1 = maVanbanPhapQuy1;
			entity.MaVanbanPhapQuy2 = maVanbanPhapQuy2;
			entity.MaVanbanPhapQuy3 = maVanbanPhapQuy3;
			entity.MaVanbanPhapQuy4 = maVanbanPhapQuy4;
			entity.MaVanbanPhapQuy5 = maVanbanPhapQuy5;
			entity.HopDong_ID = hopDong_ID;
			entity.HopDong_So = hopDong_So;
			entity.LoaiHang = loaiHang;
			entity.Templ_1 = templ_1;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@SoToKhaiDauTien", SqlDbType.VarChar, SoToKhaiDauTien);
			db.AddInParameter(dbCommand, "@SoNhanhToKhai", SqlDbType.Decimal, SoNhanhToKhai);
			db.AddInParameter(dbCommand, "@TongSoTKChiaNho", SqlDbType.Decimal, TongSoTKChiaNho);
			db.AddInParameter(dbCommand, "@SoToKhaiTNTX", SqlDbType.Decimal, SoToKhaiTNTX);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPhanLoaiHH", SqlDbType.VarChar, MaPhanLoaiHH);
			db.AddInParameter(dbCommand, "@MaPhuongThucVT", SqlDbType.VarChar, MaPhuongThucVT);
			db.AddInParameter(dbCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, PhanLoaiToChuc);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHS", SqlDbType.VarChar, NhomXuLyHS);
			db.AddInParameter(dbCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, ThoiHanTaiNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.VarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, MaBuuChinhDonVi);
			db.AddInParameter(dbCommand, "@DiaChiDonVi", SqlDbType.NVarChar, DiaChiDonVi);
			db.AddInParameter(dbCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, SoDienThoaiDonVi);
			db.AddInParameter(dbCommand, "@MaUyThac", SqlDbType.VarChar, MaUyThac);
			db.AddInParameter(dbCommand, "@TenUyThac", SqlDbType.NVarChar, TenUyThac);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.VarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, MaBuuChinhDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, DiaChiDoiTac1);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, DiaChiDoiTac2);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, DiaChiDoiTac3);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, DiaChiDoiTac4);
			db.AddInParameter(dbCommand, "@MaNuocDoiTac", SqlDbType.VarChar, MaNuocDoiTac);
			db.AddInParameter(dbCommand, "@NguoiUyThacXK", SqlDbType.NVarChar, NguoiUyThacXK);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@MaDVTSoLuong", SqlDbType.VarChar, MaDVTSoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, MaDVTTrongLuong);
			db.AddInParameter(dbCommand, "@MaDDLuuKho", SqlDbType.VarChar, MaDDLuuKho);
			db.AddInParameter(dbCommand, "@SoHieuKyHieu", SqlDbType.VarChar, SoHieuKyHieu);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.VarChar, MaPTVC);
			db.AddInParameter(dbCommand, "@TenPTVC", SqlDbType.NVarChar, TenPTVC);
			db.AddInParameter(dbCommand, "@NgayHangDen", SqlDbType.DateTime, NgayHangDen.Year <= 1753 ? DBNull.Value : (object) NgayHangDen);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDohang", SqlDbType.NVarChar, TenDiaDiemDohang);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@SoLuongCont", SqlDbType.Decimal, SoLuongCont);
			db.AddInParameter(dbCommand, "@MaKetQuaKiemTra", SqlDbType.VarChar, MaKetQuaKiemTra);
			db.AddInParameter(dbCommand, "@PhanLoaiHD", SqlDbType.VarChar, PhanLoaiHD);
			db.AddInParameter(dbCommand, "@SoTiepNhanHD", SqlDbType.Decimal, SoTiepNhanHD);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayPhatHanhHD", SqlDbType.DateTime, NgayPhatHanhHD.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhHD);
			db.AddInParameter(dbCommand, "@PhuongThucTT", SqlDbType.VarChar, PhuongThucTT);
			db.AddInParameter(dbCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, PhanLoaiGiaHD);
			db.AddInParameter(dbCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, MaDieuKienGiaHD);
			db.AddInParameter(dbCommand, "@MaTTHoaDon", SqlDbType.VarChar, MaTTHoaDon);
			db.AddInParameter(dbCommand, "@TongTriGiaHD", SqlDbType.Decimal, TongTriGiaHD);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTriGia", SqlDbType.VarChar, MaPhanLoaiTriGia);
			db.AddInParameter(dbCommand, "@SoTiepNhanTKTriGia", SqlDbType.Decimal, SoTiepNhanTKTriGia);
			db.AddInParameter(dbCommand, "@MaTTHieuChinhTriGia", SqlDbType.VarChar, MaTTHieuChinhTriGia);
			db.AddInParameter(dbCommand, "@GiaHieuChinhTriGia", SqlDbType.Decimal, GiaHieuChinhTriGia);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, MaPhanLoaiPhiVC);
			db.AddInParameter(dbCommand, "@MaTTPhiVC", SqlDbType.VarChar, MaTTPhiVC);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, MaPhanLoaiPhiBH);
			db.AddInParameter(dbCommand, "@MaTTPhiBH", SqlDbType.VarChar, MaTTPhiBH);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoDangKyBH", SqlDbType.VarChar, SoDangKyBH);
			db.AddInParameter(dbCommand, "@ChiTietKhaiTriGia", SqlDbType.NVarChar, ChiTietKhaiTriGia);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@PhanLoaiKhongQDVND", SqlDbType.VarChar, PhanLoaiKhongQDVND);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, MaTTTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@TongHeSoPhanBoTG", SqlDbType.Decimal, TongHeSoPhanBoTG);
			db.AddInParameter(dbCommand, "@MaLyDoDeNghiBP", SqlDbType.VarChar, MaLyDoDeNghiBP);
			db.AddInParameter(dbCommand, "@NguoiNopThue", SqlDbType.VarChar, NguoiNopThue);
			db.AddInParameter(dbCommand, "@MaNHTraThueThay", SqlDbType.VarChar, MaNHTraThueThay);
			db.AddInParameter(dbCommand, "@NamPhatHanhHM", SqlDbType.Decimal, NamPhatHanhHM);
			db.AddInParameter(dbCommand, "@KyHieuCTHanMuc", SqlDbType.VarChar, KyHieuCTHanMuc);
			db.AddInParameter(dbCommand, "@SoCTHanMuc", SqlDbType.VarChar, SoCTHanMuc);
			db.AddInParameter(dbCommand, "@MaXDThoiHanNopThue", SqlDbType.VarChar, MaXDThoiHanNopThue);
			db.AddInParameter(dbCommand, "@MaNHBaoLanh", SqlDbType.VarChar, MaNHBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBL", SqlDbType.Decimal, NamPhatHanhBL);
			db.AddInParameter(dbCommand, "@KyHieuCTBaoLanh", SqlDbType.VarChar, KyHieuCTBaoLanh);
			db.AddInParameter(dbCommand, "@SoCTBaoLanh", SqlDbType.VarChar, SoCTBaoLanh);
			db.AddInParameter(dbCommand, "@NgayNhapKhoDau", SqlDbType.DateTime, NgayNhapKhoDau.Year <= 1753 ? DBNull.Value : (object) NgayNhapKhoDau);
			db.AddInParameter(dbCommand, "@NgayKhoiHanhVC", SqlDbType.DateTime, NgayKhoiHanhVC.Year <= 1753 ? DBNull.Value : (object) NgayKhoiHanhVC);
			db.AddInParameter(dbCommand, "@DiaDiemDichVC", SqlDbType.VarChar, DiaDiemDichVC);
			db.AddInParameter(dbCommand, "@NgayDen", SqlDbType.DateTime, NgayDen.Year <= 1753 ? DBNull.Value : (object) NgayDen);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, SoQuanLyNoiBoDN);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, PhanLoaiBaoCaoSuaDoi);
			db.AddInParameter(dbCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, MaPhanLoaiKiemTra);
			db.AddInParameter(dbCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, MaSoThueDaiDien);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, NgayThayDoiDangKy.Year <= 1753 ? DBNull.Value : (object) NgayThayDoiDangKy);
			db.AddInParameter(dbCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, BieuThiTruongHopHetHan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@TenDDLuuKho", SqlDbType.VarChar, TenDDLuuKho);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, MaPhanLoaiTongGiaCoBan);
			db.AddInParameter(dbCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, PhanLoaiCongThucChuan);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, MaPhanLoaiDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, PhuongPhapDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, TongTienThuePhaiNop);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, PhanLoaiThamTraSauThongQuan);
			db.AddInParameter(dbCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, NgayPheDuyetBP.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetBP);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, NgayHoanThanhKiemTraBP.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraBP);
			db.AddInParameter(dbCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, SoNgayDoiCapPhepNhapKhau);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.VarChar, TieuDe);
			db.AddInParameter(dbCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, MaSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, TenSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, HanNopThueSauKhiAnHan_VAT.Year <= 1753 ? DBNull.Value : (object) HanNopThueSauKhiAnHan_VAT);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, TongSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, MaTTTongTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TongSoTienLePhi", SqlDbType.Decimal, TongSoTienLePhi);
			db.AddInParameter(dbCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, MaTTCuaSoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, SoQuanLyNguoiSuDung);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@TongSoTrangCuaToKhai", SqlDbType.Decimal, TongSoTrangCuaToKhai);
			db.AddInParameter(dbCommand, "@TongSoDongHangCuaToKhai", SqlDbType.Decimal, TongSoDongHangCuaToKhai);
			db.AddInParameter(dbCommand, "@NgayKhaiBaoNopThue", SqlDbType.DateTime, NgayKhaiBaoNopThue.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBaoNopThue);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy1", SqlDbType.VarChar, MaVanbanPhapQuy1);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy2", SqlDbType.VarChar, MaVanbanPhapQuy2);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy3", SqlDbType.VarChar, MaVanbanPhapQuy3);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy4", SqlDbType.VarChar, MaVanbanPhapQuy4);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy5", SqlDbType.VarChar, MaVanbanPhapQuy5);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@HopDong_So", SqlDbType.VarChar, HopDong_So);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.VarChar, LoaiHang);
			db.AddInParameter(dbCommand, "@Templ_1", SqlDbType.VarChar, Templ_1);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiMauDich(long id, decimal soToKhai, string soToKhaiDauTien, decimal soNhanhToKhai, decimal tongSoTKChiaNho, decimal soToKhaiTNTX, string maLoaiHinh, string maPhanLoaiHH, string maPhuongThucVT, string phanLoaiToChuc, string coQuanHaiQuan, string nhomXuLyHS, DateTime thoiHanTaiNhapTaiXuat, DateTime ngayDangKy, string maDonVi, string tenDonVi, string maBuuChinhDonVi, string diaChiDonVi, string soDienThoaiDonVi, string maUyThac, string tenUyThac, string maDoiTac, string tenDoiTac, string maBuuChinhDoiTac, string diaChiDoiTac1, string diaChiDoiTac2, string diaChiDoiTac3, string diaChiDoiTac4, string maNuocDoiTac, string nguoiUyThacXK, string maDaiLyHQ, decimal soLuong, string maDVTSoLuong, decimal trongLuong, string maDVTTrongLuong, string maDDLuuKho, string soHieuKyHieu, string maPTVC, string tenPTVC, DateTime ngayHangDen, string maDiaDiemDoHang, string tenDiaDiemDohang, string maDiaDiemXepHang, string tenDiaDiemXepHang, decimal soLuongCont, string maKetQuaKiemTra, string phanLoaiHD, decimal soTiepNhanHD, string soHoaDon, DateTime ngayPhatHanhHD, string phuongThucTT, string phanLoaiGiaHD, string maDieuKienGiaHD, string maTTHoaDon, decimal tongTriGiaHD, string maPhanLoaiTriGia, decimal soTiepNhanTKTriGia, string maTTHieuChinhTriGia, decimal giaHieuChinhTriGia, string maPhanLoaiPhiVC, string maTTPhiVC, decimal phiVanChuyen, string maPhanLoaiPhiBH, string maTTPhiBH, decimal phiBaoHiem, string soDangKyBH, string chiTietKhaiTriGia, decimal triGiaTinhThue, string phanLoaiKhongQDVND, string maTTTriGiaTinhThue, decimal tongHeSoPhanBoTG, string maLyDoDeNghiBP, string nguoiNopThue, string maNHTraThueThay, decimal namPhatHanhHM, string kyHieuCTHanMuc, string soCTHanMuc, string maXDThoiHanNopThue, string maNHBaoLanh, decimal namPhatHanhBL, string kyHieuCTBaoLanh, string soCTBaoLanh, DateTime ngayNhapKhoDau, DateTime ngayKhoiHanhVC, string diaDiemDichVC, DateTime ngayDen, string ghiChu, string soQuanLyNoiBoDN, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag, string phanLoaiBaoCaoSuaDoi, string maPhanLoaiKiemTra, string maSoThueDaiDien, string tenCoQuanHaiQuan, DateTime ngayThayDoiDangKy, string bieuThiTruongHopHetHan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string tenDDLuuKho, string maPhanLoaiTongGiaCoBan, string phanLoaiCongThucChuan, string maPhanLoaiDieuChinhTriGia, string phuongPhapDieuChinhTriGia, decimal tongTienThuePhaiNop, decimal soTienBaoLanh, string tenTruongDonViHaiQuan, DateTime ngayCapPhep, string phanLoaiThamTraSauThongQuan, DateTime ngayPheDuyetBP, DateTime ngayHoanThanhKiemTraBP, decimal soNgayDoiCapPhepNhapKhau, string tieuDe, string maSacThueAnHan_VAT, string tenSacThueAnHan_VAT, DateTime hanNopThueSauKhiAnHan_VAT, string phanLoaiNopThue, decimal tongSoTienThueXuatKhau, string maTTTongTienThueXuatKhau, decimal tongSoTienLePhi, string maTTCuaSoTienBaoLanh, string soQuanLyNguoiSuDung, DateTime ngayHoanThanhKiemTra, decimal tongSoTrangCuaToKhai, decimal tongSoDongHangCuaToKhai, DateTime ngayKhaiBaoNopThue, string maVanbanPhapQuy1, string maVanbanPhapQuy2, string maVanbanPhapQuy3, string maVanbanPhapQuy4, string maVanbanPhapQuy5, long hopDong_ID, string hopDong_So, string loaiHang, string templ_1)
		{
			KDT_VNACC_ToKhaiMauDich entity = new KDT_VNACC_ToKhaiMauDich();			
			entity.ID = id;
			entity.SoToKhai = soToKhai;
			entity.SoToKhaiDauTien = soToKhaiDauTien;
			entity.SoNhanhToKhai = soNhanhToKhai;
			entity.TongSoTKChiaNho = tongSoTKChiaNho;
			entity.SoToKhaiTNTX = soToKhaiTNTX;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaPhanLoaiHH = maPhanLoaiHH;
			entity.MaPhuongThucVT = maPhuongThucVT;
			entity.PhanLoaiToChuc = phanLoaiToChuc;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHS = nhomXuLyHS;
			entity.ThoiHanTaiNhapTaiXuat = thoiHanTaiNhapTaiXuat;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.MaBuuChinhDonVi = maBuuChinhDonVi;
			entity.DiaChiDonVi = diaChiDonVi;
			entity.SoDienThoaiDonVi = soDienThoaiDonVi;
			entity.MaUyThac = maUyThac;
			entity.TenUyThac = tenUyThac;
			entity.MaDoiTac = maDoiTac;
			entity.TenDoiTac = tenDoiTac;
			entity.MaBuuChinhDoiTac = maBuuChinhDoiTac;
			entity.DiaChiDoiTac1 = diaChiDoiTac1;
			entity.DiaChiDoiTac2 = diaChiDoiTac2;
			entity.DiaChiDoiTac3 = diaChiDoiTac3;
			entity.DiaChiDoiTac4 = diaChiDoiTac4;
			entity.MaNuocDoiTac = maNuocDoiTac;
			entity.NguoiUyThacXK = nguoiUyThacXK;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.SoLuong = soLuong;
			entity.MaDVTSoLuong = maDVTSoLuong;
			entity.TrongLuong = trongLuong;
			entity.MaDVTTrongLuong = maDVTTrongLuong;
			entity.MaDDLuuKho = maDDLuuKho;
			entity.SoHieuKyHieu = soHieuKyHieu;
			entity.MaPTVC = maPTVC;
			entity.TenPTVC = tenPTVC;
			entity.NgayHangDen = ngayHangDen;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDohang = tenDiaDiemDohang;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.SoLuongCont = soLuongCont;
			entity.MaKetQuaKiemTra = maKetQuaKiemTra;
			entity.PhanLoaiHD = phanLoaiHD;
			entity.SoTiepNhanHD = soTiepNhanHD;
			entity.SoHoaDon = soHoaDon;
			entity.NgayPhatHanhHD = ngayPhatHanhHD;
			entity.PhuongThucTT = phuongThucTT;
			entity.PhanLoaiGiaHD = phanLoaiGiaHD;
			entity.MaDieuKienGiaHD = maDieuKienGiaHD;
			entity.MaTTHoaDon = maTTHoaDon;
			entity.TongTriGiaHD = tongTriGiaHD;
			entity.MaPhanLoaiTriGia = maPhanLoaiTriGia;
			entity.SoTiepNhanTKTriGia = soTiepNhanTKTriGia;
			entity.MaTTHieuChinhTriGia = maTTHieuChinhTriGia;
			entity.GiaHieuChinhTriGia = giaHieuChinhTriGia;
			entity.MaPhanLoaiPhiVC = maPhanLoaiPhiVC;
			entity.MaTTPhiVC = maTTPhiVC;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaPhanLoaiPhiBH = maPhanLoaiPhiBH;
			entity.MaTTPhiBH = maTTPhiBH;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.SoDangKyBH = soDangKyBH;
			entity.ChiTietKhaiTriGia = chiTietKhaiTriGia;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.PhanLoaiKhongQDVND = phanLoaiKhongQDVND;
			entity.MaTTTriGiaTinhThue = maTTTriGiaTinhThue;
			entity.TongHeSoPhanBoTG = tongHeSoPhanBoTG;
			entity.MaLyDoDeNghiBP = maLyDoDeNghiBP;
			entity.NguoiNopThue = nguoiNopThue;
			entity.MaNHTraThueThay = maNHTraThueThay;
			entity.NamPhatHanhHM = namPhatHanhHM;
			entity.KyHieuCTHanMuc = kyHieuCTHanMuc;
			entity.SoCTHanMuc = soCTHanMuc;
			entity.MaXDThoiHanNopThue = maXDThoiHanNopThue;
			entity.MaNHBaoLanh = maNHBaoLanh;
			entity.NamPhatHanhBL = namPhatHanhBL;
			entity.KyHieuCTBaoLanh = kyHieuCTBaoLanh;
			entity.SoCTBaoLanh = soCTBaoLanh;
			entity.NgayNhapKhoDau = ngayNhapKhoDau;
			entity.NgayKhoiHanhVC = ngayKhoiHanhVC;
			entity.DiaDiemDichVC = diaDiemDichVC;
			entity.NgayDen = ngayDen;
			entity.GhiChu = ghiChu;
			entity.SoQuanLyNoiBoDN = soQuanLyNoiBoDN;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.PhanLoaiBaoCaoSuaDoi = phanLoaiBaoCaoSuaDoi;
			entity.MaPhanLoaiKiemTra = maPhanLoaiKiemTra;
			entity.MaSoThueDaiDien = maSoThueDaiDien;
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.NgayThayDoiDangKy = ngayThayDoiDangKy;
			entity.BieuThiTruongHopHetHan = bieuThiTruongHopHetHan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.TenDDLuuKho = tenDDLuuKho;
			entity.MaPhanLoaiTongGiaCoBan = maPhanLoaiTongGiaCoBan;
			entity.PhanLoaiCongThucChuan = phanLoaiCongThucChuan;
			entity.MaPhanLoaiDieuChinhTriGia = maPhanLoaiDieuChinhTriGia;
			entity.PhuongPhapDieuChinhTriGia = phuongPhapDieuChinhTriGia;
			entity.TongTienThuePhaiNop = tongTienThuePhaiNop;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayCapPhep = ngayCapPhep;
			entity.PhanLoaiThamTraSauThongQuan = phanLoaiThamTraSauThongQuan;
			entity.NgayPheDuyetBP = ngayPheDuyetBP;
			entity.NgayHoanThanhKiemTraBP = ngayHoanThanhKiemTraBP;
			entity.SoNgayDoiCapPhepNhapKhau = soNgayDoiCapPhepNhapKhau;
			entity.TieuDe = tieuDe;
			entity.MaSacThueAnHan_VAT = maSacThueAnHan_VAT;
			entity.TenSacThueAnHan_VAT = tenSacThueAnHan_VAT;
			entity.HanNopThueSauKhiAnHan_VAT = hanNopThueSauKhiAnHan_VAT;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.TongSoTienThueXuatKhau = tongSoTienThueXuatKhau;
			entity.MaTTTongTienThueXuatKhau = maTTTongTienThueXuatKhau;
			entity.TongSoTienLePhi = tongSoTienLePhi;
			entity.MaTTCuaSoTienBaoLanh = maTTCuaSoTienBaoLanh;
			entity.SoQuanLyNguoiSuDung = soQuanLyNguoiSuDung;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			entity.TongSoTrangCuaToKhai = tongSoTrangCuaToKhai;
			entity.TongSoDongHangCuaToKhai = tongSoDongHangCuaToKhai;
			entity.NgayKhaiBaoNopThue = ngayKhaiBaoNopThue;
			entity.MaVanbanPhapQuy1 = maVanbanPhapQuy1;
			entity.MaVanbanPhapQuy2 = maVanbanPhapQuy2;
			entity.MaVanbanPhapQuy3 = maVanbanPhapQuy3;
			entity.MaVanbanPhapQuy4 = maVanbanPhapQuy4;
			entity.MaVanbanPhapQuy5 = maVanbanPhapQuy5;
			entity.HopDong_ID = hopDong_ID;
			entity.HopDong_So = hopDong_So;
			entity.LoaiHang = loaiHang;
			entity.Templ_1 = templ_1;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@SoToKhaiDauTien", SqlDbType.VarChar, SoToKhaiDauTien);
			db.AddInParameter(dbCommand, "@SoNhanhToKhai", SqlDbType.Decimal, SoNhanhToKhai);
			db.AddInParameter(dbCommand, "@TongSoTKChiaNho", SqlDbType.Decimal, TongSoTKChiaNho);
			db.AddInParameter(dbCommand, "@SoToKhaiTNTX", SqlDbType.Decimal, SoToKhaiTNTX);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPhanLoaiHH", SqlDbType.VarChar, MaPhanLoaiHH);
			db.AddInParameter(dbCommand, "@MaPhuongThucVT", SqlDbType.VarChar, MaPhuongThucVT);
			db.AddInParameter(dbCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, PhanLoaiToChuc);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHS", SqlDbType.VarChar, NhomXuLyHS);
			db.AddInParameter(dbCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, ThoiHanTaiNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.VarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, MaBuuChinhDonVi);
			db.AddInParameter(dbCommand, "@DiaChiDonVi", SqlDbType.NVarChar, DiaChiDonVi);
			db.AddInParameter(dbCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, SoDienThoaiDonVi);
			db.AddInParameter(dbCommand, "@MaUyThac", SqlDbType.VarChar, MaUyThac);
			db.AddInParameter(dbCommand, "@TenUyThac", SqlDbType.NVarChar, TenUyThac);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.VarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, MaBuuChinhDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, DiaChiDoiTac1);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, DiaChiDoiTac2);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, DiaChiDoiTac3);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, DiaChiDoiTac4);
			db.AddInParameter(dbCommand, "@MaNuocDoiTac", SqlDbType.VarChar, MaNuocDoiTac);
			db.AddInParameter(dbCommand, "@NguoiUyThacXK", SqlDbType.NVarChar, NguoiUyThacXK);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@MaDVTSoLuong", SqlDbType.VarChar, MaDVTSoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, MaDVTTrongLuong);
			db.AddInParameter(dbCommand, "@MaDDLuuKho", SqlDbType.VarChar, MaDDLuuKho);
			db.AddInParameter(dbCommand, "@SoHieuKyHieu", SqlDbType.VarChar, SoHieuKyHieu);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.VarChar, MaPTVC);
			db.AddInParameter(dbCommand, "@TenPTVC", SqlDbType.NVarChar, TenPTVC);
			db.AddInParameter(dbCommand, "@NgayHangDen", SqlDbType.DateTime, NgayHangDen.Year <= 1753 ? DBNull.Value : (object) NgayHangDen);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDohang", SqlDbType.NVarChar, TenDiaDiemDohang);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@SoLuongCont", SqlDbType.Decimal, SoLuongCont);
			db.AddInParameter(dbCommand, "@MaKetQuaKiemTra", SqlDbType.VarChar, MaKetQuaKiemTra);
			db.AddInParameter(dbCommand, "@PhanLoaiHD", SqlDbType.VarChar, PhanLoaiHD);
			db.AddInParameter(dbCommand, "@SoTiepNhanHD", SqlDbType.Decimal, SoTiepNhanHD);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayPhatHanhHD", SqlDbType.DateTime, NgayPhatHanhHD.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhHD);
			db.AddInParameter(dbCommand, "@PhuongThucTT", SqlDbType.VarChar, PhuongThucTT);
			db.AddInParameter(dbCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, PhanLoaiGiaHD);
			db.AddInParameter(dbCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, MaDieuKienGiaHD);
			db.AddInParameter(dbCommand, "@MaTTHoaDon", SqlDbType.VarChar, MaTTHoaDon);
			db.AddInParameter(dbCommand, "@TongTriGiaHD", SqlDbType.Decimal, TongTriGiaHD);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTriGia", SqlDbType.VarChar, MaPhanLoaiTriGia);
			db.AddInParameter(dbCommand, "@SoTiepNhanTKTriGia", SqlDbType.Decimal, SoTiepNhanTKTriGia);
			db.AddInParameter(dbCommand, "@MaTTHieuChinhTriGia", SqlDbType.VarChar, MaTTHieuChinhTriGia);
			db.AddInParameter(dbCommand, "@GiaHieuChinhTriGia", SqlDbType.Decimal, GiaHieuChinhTriGia);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, MaPhanLoaiPhiVC);
			db.AddInParameter(dbCommand, "@MaTTPhiVC", SqlDbType.VarChar, MaTTPhiVC);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, MaPhanLoaiPhiBH);
			db.AddInParameter(dbCommand, "@MaTTPhiBH", SqlDbType.VarChar, MaTTPhiBH);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoDangKyBH", SqlDbType.VarChar, SoDangKyBH);
			db.AddInParameter(dbCommand, "@ChiTietKhaiTriGia", SqlDbType.NVarChar, ChiTietKhaiTriGia);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@PhanLoaiKhongQDVND", SqlDbType.VarChar, PhanLoaiKhongQDVND);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, MaTTTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@TongHeSoPhanBoTG", SqlDbType.Decimal, TongHeSoPhanBoTG);
			db.AddInParameter(dbCommand, "@MaLyDoDeNghiBP", SqlDbType.VarChar, MaLyDoDeNghiBP);
			db.AddInParameter(dbCommand, "@NguoiNopThue", SqlDbType.VarChar, NguoiNopThue);
			db.AddInParameter(dbCommand, "@MaNHTraThueThay", SqlDbType.VarChar, MaNHTraThueThay);
			db.AddInParameter(dbCommand, "@NamPhatHanhHM", SqlDbType.Decimal, NamPhatHanhHM);
			db.AddInParameter(dbCommand, "@KyHieuCTHanMuc", SqlDbType.VarChar, KyHieuCTHanMuc);
			db.AddInParameter(dbCommand, "@SoCTHanMuc", SqlDbType.VarChar, SoCTHanMuc);
			db.AddInParameter(dbCommand, "@MaXDThoiHanNopThue", SqlDbType.VarChar, MaXDThoiHanNopThue);
			db.AddInParameter(dbCommand, "@MaNHBaoLanh", SqlDbType.VarChar, MaNHBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBL", SqlDbType.Decimal, NamPhatHanhBL);
			db.AddInParameter(dbCommand, "@KyHieuCTBaoLanh", SqlDbType.VarChar, KyHieuCTBaoLanh);
			db.AddInParameter(dbCommand, "@SoCTBaoLanh", SqlDbType.VarChar, SoCTBaoLanh);
			db.AddInParameter(dbCommand, "@NgayNhapKhoDau", SqlDbType.DateTime, NgayNhapKhoDau.Year <= 1753 ? DBNull.Value : (object) NgayNhapKhoDau);
			db.AddInParameter(dbCommand, "@NgayKhoiHanhVC", SqlDbType.DateTime, NgayKhoiHanhVC.Year <= 1753 ? DBNull.Value : (object) NgayKhoiHanhVC);
			db.AddInParameter(dbCommand, "@DiaDiemDichVC", SqlDbType.VarChar, DiaDiemDichVC);
			db.AddInParameter(dbCommand, "@NgayDen", SqlDbType.DateTime, NgayDen.Year <= 1753 ? DBNull.Value : (object) NgayDen);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, SoQuanLyNoiBoDN);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, PhanLoaiBaoCaoSuaDoi);
			db.AddInParameter(dbCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, MaPhanLoaiKiemTra);
			db.AddInParameter(dbCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, MaSoThueDaiDien);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, NgayThayDoiDangKy.Year <= 1753 ? DBNull.Value : (object) NgayThayDoiDangKy);
			db.AddInParameter(dbCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, BieuThiTruongHopHetHan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@TenDDLuuKho", SqlDbType.VarChar, TenDDLuuKho);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, MaPhanLoaiTongGiaCoBan);
			db.AddInParameter(dbCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, PhanLoaiCongThucChuan);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, MaPhanLoaiDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, PhuongPhapDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, TongTienThuePhaiNop);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, PhanLoaiThamTraSauThongQuan);
			db.AddInParameter(dbCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, NgayPheDuyetBP.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetBP);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, NgayHoanThanhKiemTraBP.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraBP);
			db.AddInParameter(dbCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, SoNgayDoiCapPhepNhapKhau);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.VarChar, TieuDe);
			db.AddInParameter(dbCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, MaSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, TenSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, HanNopThueSauKhiAnHan_VAT.Year <= 1753 ? DBNull.Value : (object) HanNopThueSauKhiAnHan_VAT);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, TongSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, MaTTTongTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TongSoTienLePhi", SqlDbType.Decimal, TongSoTienLePhi);
			db.AddInParameter(dbCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, MaTTCuaSoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, SoQuanLyNguoiSuDung);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@TongSoTrangCuaToKhai", SqlDbType.Decimal, TongSoTrangCuaToKhai);
			db.AddInParameter(dbCommand, "@TongSoDongHangCuaToKhai", SqlDbType.Decimal, TongSoDongHangCuaToKhai);
			db.AddInParameter(dbCommand, "@NgayKhaiBaoNopThue", SqlDbType.DateTime, NgayKhaiBaoNopThue.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBaoNopThue);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy1", SqlDbType.VarChar, MaVanbanPhapQuy1);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy2", SqlDbType.VarChar, MaVanbanPhapQuy2);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy3", SqlDbType.VarChar, MaVanbanPhapQuy3);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy4", SqlDbType.VarChar, MaVanbanPhapQuy4);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy5", SqlDbType.VarChar, MaVanbanPhapQuy5);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@HopDong_So", SqlDbType.VarChar, HopDong_So);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.VarChar, LoaiHang);
			db.AddInParameter(dbCommand, "@Templ_1", SqlDbType.VarChar, Templ_1);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiMauDich(long id, decimal soToKhai, string soToKhaiDauTien, decimal soNhanhToKhai, decimal tongSoTKChiaNho, decimal soToKhaiTNTX, string maLoaiHinh, string maPhanLoaiHH, string maPhuongThucVT, string phanLoaiToChuc, string coQuanHaiQuan, string nhomXuLyHS, DateTime thoiHanTaiNhapTaiXuat, DateTime ngayDangKy, string maDonVi, string tenDonVi, string maBuuChinhDonVi, string diaChiDonVi, string soDienThoaiDonVi, string maUyThac, string tenUyThac, string maDoiTac, string tenDoiTac, string maBuuChinhDoiTac, string diaChiDoiTac1, string diaChiDoiTac2, string diaChiDoiTac3, string diaChiDoiTac4, string maNuocDoiTac, string nguoiUyThacXK, string maDaiLyHQ, decimal soLuong, string maDVTSoLuong, decimal trongLuong, string maDVTTrongLuong, string maDDLuuKho, string soHieuKyHieu, string maPTVC, string tenPTVC, DateTime ngayHangDen, string maDiaDiemDoHang, string tenDiaDiemDohang, string maDiaDiemXepHang, string tenDiaDiemXepHang, decimal soLuongCont, string maKetQuaKiemTra, string phanLoaiHD, decimal soTiepNhanHD, string soHoaDon, DateTime ngayPhatHanhHD, string phuongThucTT, string phanLoaiGiaHD, string maDieuKienGiaHD, string maTTHoaDon, decimal tongTriGiaHD, string maPhanLoaiTriGia, decimal soTiepNhanTKTriGia, string maTTHieuChinhTriGia, decimal giaHieuChinhTriGia, string maPhanLoaiPhiVC, string maTTPhiVC, decimal phiVanChuyen, string maPhanLoaiPhiBH, string maTTPhiBH, decimal phiBaoHiem, string soDangKyBH, string chiTietKhaiTriGia, decimal triGiaTinhThue, string phanLoaiKhongQDVND, string maTTTriGiaTinhThue, decimal tongHeSoPhanBoTG, string maLyDoDeNghiBP, string nguoiNopThue, string maNHTraThueThay, decimal namPhatHanhHM, string kyHieuCTHanMuc, string soCTHanMuc, string maXDThoiHanNopThue, string maNHBaoLanh, decimal namPhatHanhBL, string kyHieuCTBaoLanh, string soCTBaoLanh, DateTime ngayNhapKhoDau, DateTime ngayKhoiHanhVC, string diaDiemDichVC, DateTime ngayDen, string ghiChu, string soQuanLyNoiBoDN, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag, string phanLoaiBaoCaoSuaDoi, string maPhanLoaiKiemTra, string maSoThueDaiDien, string tenCoQuanHaiQuan, DateTime ngayThayDoiDangKy, string bieuThiTruongHopHetHan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string tenDDLuuKho, string maPhanLoaiTongGiaCoBan, string phanLoaiCongThucChuan, string maPhanLoaiDieuChinhTriGia, string phuongPhapDieuChinhTriGia, decimal tongTienThuePhaiNop, decimal soTienBaoLanh, string tenTruongDonViHaiQuan, DateTime ngayCapPhep, string phanLoaiThamTraSauThongQuan, DateTime ngayPheDuyetBP, DateTime ngayHoanThanhKiemTraBP, decimal soNgayDoiCapPhepNhapKhau, string tieuDe, string maSacThueAnHan_VAT, string tenSacThueAnHan_VAT, DateTime hanNopThueSauKhiAnHan_VAT, string phanLoaiNopThue, decimal tongSoTienThueXuatKhau, string maTTTongTienThueXuatKhau, decimal tongSoTienLePhi, string maTTCuaSoTienBaoLanh, string soQuanLyNguoiSuDung, DateTime ngayHoanThanhKiemTra, decimal tongSoTrangCuaToKhai, decimal tongSoDongHangCuaToKhai, DateTime ngayKhaiBaoNopThue, string maVanbanPhapQuy1, string maVanbanPhapQuy2, string maVanbanPhapQuy3, string maVanbanPhapQuy4, string maVanbanPhapQuy5, long hopDong_ID, string hopDong_So, string loaiHang, string templ_1)
		{
			KDT_VNACC_ToKhaiMauDich entity = new KDT_VNACC_ToKhaiMauDich();			
			entity.ID = id;
			entity.SoToKhai = soToKhai;
			entity.SoToKhaiDauTien = soToKhaiDauTien;
			entity.SoNhanhToKhai = soNhanhToKhai;
			entity.TongSoTKChiaNho = tongSoTKChiaNho;
			entity.SoToKhaiTNTX = soToKhaiTNTX;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaPhanLoaiHH = maPhanLoaiHH;
			entity.MaPhuongThucVT = maPhuongThucVT;
			entity.PhanLoaiToChuc = phanLoaiToChuc;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHS = nhomXuLyHS;
			entity.ThoiHanTaiNhapTaiXuat = thoiHanTaiNhapTaiXuat;
			entity.NgayDangKy = ngayDangKy;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.MaBuuChinhDonVi = maBuuChinhDonVi;
			entity.DiaChiDonVi = diaChiDonVi;
			entity.SoDienThoaiDonVi = soDienThoaiDonVi;
			entity.MaUyThac = maUyThac;
			entity.TenUyThac = tenUyThac;
			entity.MaDoiTac = maDoiTac;
			entity.TenDoiTac = tenDoiTac;
			entity.MaBuuChinhDoiTac = maBuuChinhDoiTac;
			entity.DiaChiDoiTac1 = diaChiDoiTac1;
			entity.DiaChiDoiTac2 = diaChiDoiTac2;
			entity.DiaChiDoiTac3 = diaChiDoiTac3;
			entity.DiaChiDoiTac4 = diaChiDoiTac4;
			entity.MaNuocDoiTac = maNuocDoiTac;
			entity.NguoiUyThacXK = nguoiUyThacXK;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.SoLuong = soLuong;
			entity.MaDVTSoLuong = maDVTSoLuong;
			entity.TrongLuong = trongLuong;
			entity.MaDVTTrongLuong = maDVTTrongLuong;
			entity.MaDDLuuKho = maDDLuuKho;
			entity.SoHieuKyHieu = soHieuKyHieu;
			entity.MaPTVC = maPTVC;
			entity.TenPTVC = tenPTVC;
			entity.NgayHangDen = ngayHangDen;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDohang = tenDiaDiemDohang;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.SoLuongCont = soLuongCont;
			entity.MaKetQuaKiemTra = maKetQuaKiemTra;
			entity.PhanLoaiHD = phanLoaiHD;
			entity.SoTiepNhanHD = soTiepNhanHD;
			entity.SoHoaDon = soHoaDon;
			entity.NgayPhatHanhHD = ngayPhatHanhHD;
			entity.PhuongThucTT = phuongThucTT;
			entity.PhanLoaiGiaHD = phanLoaiGiaHD;
			entity.MaDieuKienGiaHD = maDieuKienGiaHD;
			entity.MaTTHoaDon = maTTHoaDon;
			entity.TongTriGiaHD = tongTriGiaHD;
			entity.MaPhanLoaiTriGia = maPhanLoaiTriGia;
			entity.SoTiepNhanTKTriGia = soTiepNhanTKTriGia;
			entity.MaTTHieuChinhTriGia = maTTHieuChinhTriGia;
			entity.GiaHieuChinhTriGia = giaHieuChinhTriGia;
			entity.MaPhanLoaiPhiVC = maPhanLoaiPhiVC;
			entity.MaTTPhiVC = maTTPhiVC;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaPhanLoaiPhiBH = maPhanLoaiPhiBH;
			entity.MaTTPhiBH = maTTPhiBH;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.SoDangKyBH = soDangKyBH;
			entity.ChiTietKhaiTriGia = chiTietKhaiTriGia;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.PhanLoaiKhongQDVND = phanLoaiKhongQDVND;
			entity.MaTTTriGiaTinhThue = maTTTriGiaTinhThue;
			entity.TongHeSoPhanBoTG = tongHeSoPhanBoTG;
			entity.MaLyDoDeNghiBP = maLyDoDeNghiBP;
			entity.NguoiNopThue = nguoiNopThue;
			entity.MaNHTraThueThay = maNHTraThueThay;
			entity.NamPhatHanhHM = namPhatHanhHM;
			entity.KyHieuCTHanMuc = kyHieuCTHanMuc;
			entity.SoCTHanMuc = soCTHanMuc;
			entity.MaXDThoiHanNopThue = maXDThoiHanNopThue;
			entity.MaNHBaoLanh = maNHBaoLanh;
			entity.NamPhatHanhBL = namPhatHanhBL;
			entity.KyHieuCTBaoLanh = kyHieuCTBaoLanh;
			entity.SoCTBaoLanh = soCTBaoLanh;
			entity.NgayNhapKhoDau = ngayNhapKhoDau;
			entity.NgayKhoiHanhVC = ngayKhoiHanhVC;
			entity.DiaDiemDichVC = diaDiemDichVC;
			entity.NgayDen = ngayDen;
			entity.GhiChu = ghiChu;
			entity.SoQuanLyNoiBoDN = soQuanLyNoiBoDN;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.PhanLoaiBaoCaoSuaDoi = phanLoaiBaoCaoSuaDoi;
			entity.MaPhanLoaiKiemTra = maPhanLoaiKiemTra;
			entity.MaSoThueDaiDien = maSoThueDaiDien;
			entity.TenCoQuanHaiQuan = tenCoQuanHaiQuan;
			entity.NgayThayDoiDangKy = ngayThayDoiDangKy;
			entity.BieuThiTruongHopHetHan = bieuThiTruongHopHetHan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.TenDDLuuKho = tenDDLuuKho;
			entity.MaPhanLoaiTongGiaCoBan = maPhanLoaiTongGiaCoBan;
			entity.PhanLoaiCongThucChuan = phanLoaiCongThucChuan;
			entity.MaPhanLoaiDieuChinhTriGia = maPhanLoaiDieuChinhTriGia;
			entity.PhuongPhapDieuChinhTriGia = phuongPhapDieuChinhTriGia;
			entity.TongTienThuePhaiNop = tongTienThuePhaiNop;
			entity.SoTienBaoLanh = soTienBaoLanh;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayCapPhep = ngayCapPhep;
			entity.PhanLoaiThamTraSauThongQuan = phanLoaiThamTraSauThongQuan;
			entity.NgayPheDuyetBP = ngayPheDuyetBP;
			entity.NgayHoanThanhKiemTraBP = ngayHoanThanhKiemTraBP;
			entity.SoNgayDoiCapPhepNhapKhau = soNgayDoiCapPhepNhapKhau;
			entity.TieuDe = tieuDe;
			entity.MaSacThueAnHan_VAT = maSacThueAnHan_VAT;
			entity.TenSacThueAnHan_VAT = tenSacThueAnHan_VAT;
			entity.HanNopThueSauKhiAnHan_VAT = hanNopThueSauKhiAnHan_VAT;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.TongSoTienThueXuatKhau = tongSoTienThueXuatKhau;
			entity.MaTTTongTienThueXuatKhau = maTTTongTienThueXuatKhau;
			entity.TongSoTienLePhi = tongSoTienLePhi;
			entity.MaTTCuaSoTienBaoLanh = maTTCuaSoTienBaoLanh;
			entity.SoQuanLyNguoiSuDung = soQuanLyNguoiSuDung;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			entity.TongSoTrangCuaToKhai = tongSoTrangCuaToKhai;
			entity.TongSoDongHangCuaToKhai = tongSoDongHangCuaToKhai;
			entity.NgayKhaiBaoNopThue = ngayKhaiBaoNopThue;
			entity.MaVanbanPhapQuy1 = maVanbanPhapQuy1;
			entity.MaVanbanPhapQuy2 = maVanbanPhapQuy2;
			entity.MaVanbanPhapQuy3 = maVanbanPhapQuy3;
			entity.MaVanbanPhapQuy4 = maVanbanPhapQuy4;
			entity.MaVanbanPhapQuy5 = maVanbanPhapQuy5;
			entity.HopDong_ID = hopDong_ID;
			entity.HopDong_So = hopDong_So;
			entity.LoaiHang = loaiHang;
			entity.Templ_1 = templ_1;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@SoToKhaiDauTien", SqlDbType.VarChar, SoToKhaiDauTien);
			db.AddInParameter(dbCommand, "@SoNhanhToKhai", SqlDbType.Decimal, SoNhanhToKhai);
			db.AddInParameter(dbCommand, "@TongSoTKChiaNho", SqlDbType.Decimal, TongSoTKChiaNho);
			db.AddInParameter(dbCommand, "@SoToKhaiTNTX", SqlDbType.Decimal, SoToKhaiTNTX);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPhanLoaiHH", SqlDbType.VarChar, MaPhanLoaiHH);
			db.AddInParameter(dbCommand, "@MaPhuongThucVT", SqlDbType.VarChar, MaPhuongThucVT);
			db.AddInParameter(dbCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, PhanLoaiToChuc);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHS", SqlDbType.VarChar, NhomXuLyHS);
			db.AddInParameter(dbCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, ThoiHanTaiNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.VarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, MaBuuChinhDonVi);
			db.AddInParameter(dbCommand, "@DiaChiDonVi", SqlDbType.NVarChar, DiaChiDonVi);
			db.AddInParameter(dbCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, SoDienThoaiDonVi);
			db.AddInParameter(dbCommand, "@MaUyThac", SqlDbType.VarChar, MaUyThac);
			db.AddInParameter(dbCommand, "@TenUyThac", SqlDbType.NVarChar, TenUyThac);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.VarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, MaBuuChinhDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, DiaChiDoiTac1);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, DiaChiDoiTac2);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, DiaChiDoiTac3);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, DiaChiDoiTac4);
			db.AddInParameter(dbCommand, "@MaNuocDoiTac", SqlDbType.VarChar, MaNuocDoiTac);
			db.AddInParameter(dbCommand, "@NguoiUyThacXK", SqlDbType.NVarChar, NguoiUyThacXK);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@MaDVTSoLuong", SqlDbType.VarChar, MaDVTSoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@MaDVTTrongLuong", SqlDbType.VarChar, MaDVTTrongLuong);
			db.AddInParameter(dbCommand, "@MaDDLuuKho", SqlDbType.VarChar, MaDDLuuKho);
			db.AddInParameter(dbCommand, "@SoHieuKyHieu", SqlDbType.VarChar, SoHieuKyHieu);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.VarChar, MaPTVC);
			db.AddInParameter(dbCommand, "@TenPTVC", SqlDbType.NVarChar, TenPTVC);
			db.AddInParameter(dbCommand, "@NgayHangDen", SqlDbType.DateTime, NgayHangDen.Year <= 1753 ? DBNull.Value : (object) NgayHangDen);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDohang", SqlDbType.NVarChar, TenDiaDiemDohang);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@SoLuongCont", SqlDbType.Decimal, SoLuongCont);
			db.AddInParameter(dbCommand, "@MaKetQuaKiemTra", SqlDbType.VarChar, MaKetQuaKiemTra);
			db.AddInParameter(dbCommand, "@PhanLoaiHD", SqlDbType.VarChar, PhanLoaiHD);
			db.AddInParameter(dbCommand, "@SoTiepNhanHD", SqlDbType.Decimal, SoTiepNhanHD);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayPhatHanhHD", SqlDbType.DateTime, NgayPhatHanhHD.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanhHD);
			db.AddInParameter(dbCommand, "@PhuongThucTT", SqlDbType.VarChar, PhuongThucTT);
			db.AddInParameter(dbCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, PhanLoaiGiaHD);
			db.AddInParameter(dbCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, MaDieuKienGiaHD);
			db.AddInParameter(dbCommand, "@MaTTHoaDon", SqlDbType.VarChar, MaTTHoaDon);
			db.AddInParameter(dbCommand, "@TongTriGiaHD", SqlDbType.Decimal, TongTriGiaHD);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTriGia", SqlDbType.VarChar, MaPhanLoaiTriGia);
			db.AddInParameter(dbCommand, "@SoTiepNhanTKTriGia", SqlDbType.Decimal, SoTiepNhanTKTriGia);
			db.AddInParameter(dbCommand, "@MaTTHieuChinhTriGia", SqlDbType.VarChar, MaTTHieuChinhTriGia);
			db.AddInParameter(dbCommand, "@GiaHieuChinhTriGia", SqlDbType.Decimal, GiaHieuChinhTriGia);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, MaPhanLoaiPhiVC);
			db.AddInParameter(dbCommand, "@MaTTPhiVC", SqlDbType.VarChar, MaTTPhiVC);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, MaPhanLoaiPhiBH);
			db.AddInParameter(dbCommand, "@MaTTPhiBH", SqlDbType.VarChar, MaTTPhiBH);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoDangKyBH", SqlDbType.VarChar, SoDangKyBH);
			db.AddInParameter(dbCommand, "@ChiTietKhaiTriGia", SqlDbType.NVarChar, ChiTietKhaiTriGia);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@PhanLoaiKhongQDVND", SqlDbType.VarChar, PhanLoaiKhongQDVND);
			db.AddInParameter(dbCommand, "@MaTTTriGiaTinhThue", SqlDbType.VarChar, MaTTTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@TongHeSoPhanBoTG", SqlDbType.Decimal, TongHeSoPhanBoTG);
			db.AddInParameter(dbCommand, "@MaLyDoDeNghiBP", SqlDbType.VarChar, MaLyDoDeNghiBP);
			db.AddInParameter(dbCommand, "@NguoiNopThue", SqlDbType.VarChar, NguoiNopThue);
			db.AddInParameter(dbCommand, "@MaNHTraThueThay", SqlDbType.VarChar, MaNHTraThueThay);
			db.AddInParameter(dbCommand, "@NamPhatHanhHM", SqlDbType.Decimal, NamPhatHanhHM);
			db.AddInParameter(dbCommand, "@KyHieuCTHanMuc", SqlDbType.VarChar, KyHieuCTHanMuc);
			db.AddInParameter(dbCommand, "@SoCTHanMuc", SqlDbType.VarChar, SoCTHanMuc);
			db.AddInParameter(dbCommand, "@MaXDThoiHanNopThue", SqlDbType.VarChar, MaXDThoiHanNopThue);
			db.AddInParameter(dbCommand, "@MaNHBaoLanh", SqlDbType.VarChar, MaNHBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBL", SqlDbType.Decimal, NamPhatHanhBL);
			db.AddInParameter(dbCommand, "@KyHieuCTBaoLanh", SqlDbType.VarChar, KyHieuCTBaoLanh);
			db.AddInParameter(dbCommand, "@SoCTBaoLanh", SqlDbType.VarChar, SoCTBaoLanh);
			db.AddInParameter(dbCommand, "@NgayNhapKhoDau", SqlDbType.DateTime, NgayNhapKhoDau.Year <= 1753 ? DBNull.Value : (object) NgayNhapKhoDau);
			db.AddInParameter(dbCommand, "@NgayKhoiHanhVC", SqlDbType.DateTime, NgayKhoiHanhVC.Year <= 1753 ? DBNull.Value : (object) NgayKhoiHanhVC);
			db.AddInParameter(dbCommand, "@DiaDiemDichVC", SqlDbType.VarChar, DiaDiemDichVC);
			db.AddInParameter(dbCommand, "@NgayDen", SqlDbType.DateTime, NgayDen.Year <= 1753 ? DBNull.Value : (object) NgayDen);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, SoQuanLyNoiBoDN);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSuaDoi", SqlDbType.VarChar, PhanLoaiBaoCaoSuaDoi);
			db.AddInParameter(dbCommand, "@MaPhanLoaiKiemTra", SqlDbType.VarChar, MaPhanLoaiKiemTra);
			db.AddInParameter(dbCommand, "@MaSoThueDaiDien", SqlDbType.VarChar, MaSoThueDaiDien);
			db.AddInParameter(dbCommand, "@TenCoQuanHaiQuan", SqlDbType.VarChar, TenCoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NgayThayDoiDangKy", SqlDbType.DateTime, NgayThayDoiDangKy.Year <= 1753 ? DBNull.Value : (object) NgayThayDoiDangKy);
			db.AddInParameter(dbCommand, "@BieuThiTruongHopHetHan", SqlDbType.VarChar, BieuThiTruongHopHetHan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.VarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@TenDDLuuKho", SqlDbType.VarChar, TenDDLuuKho);
			db.AddInParameter(dbCommand, "@MaPhanLoaiTongGiaCoBan", SqlDbType.VarChar, MaPhanLoaiTongGiaCoBan);
			db.AddInParameter(dbCommand, "@PhanLoaiCongThucChuan", SqlDbType.VarChar, PhanLoaiCongThucChuan);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDieuChinhTriGia", SqlDbType.VarChar, MaPhanLoaiDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@PhuongPhapDieuChinhTriGia", SqlDbType.VarChar, PhuongPhapDieuChinhTriGia);
			db.AddInParameter(dbCommand, "@TongTienThuePhaiNop", SqlDbType.Decimal, TongTienThuePhaiNop);
			db.AddInParameter(dbCommand, "@SoTienBaoLanh", SqlDbType.Decimal, SoTienBaoLanh);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@PhanLoaiThamTraSauThongQuan", SqlDbType.VarChar, PhanLoaiThamTraSauThongQuan);
			db.AddInParameter(dbCommand, "@NgayPheDuyetBP", SqlDbType.DateTime, NgayPheDuyetBP.Year <= 1753 ? DBNull.Value : (object) NgayPheDuyetBP);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraBP", SqlDbType.DateTime, NgayHoanThanhKiemTraBP.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraBP);
			db.AddInParameter(dbCommand, "@SoNgayDoiCapPhepNhapKhau", SqlDbType.Decimal, SoNgayDoiCapPhepNhapKhau);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.VarChar, TieuDe);
			db.AddInParameter(dbCommand, "@MaSacThueAnHan_VAT", SqlDbType.VarChar, MaSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@TenSacThueAnHan_VAT", SqlDbType.NVarChar, TenSacThueAnHan_VAT);
			db.AddInParameter(dbCommand, "@HanNopThueSauKhiAnHan_VAT", SqlDbType.DateTime, HanNopThueSauKhiAnHan_VAT.Year <= 1753 ? DBNull.Value : (object) HanNopThueSauKhiAnHan_VAT);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@TongSoTienThueXuatKhau", SqlDbType.Decimal, TongSoTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@MaTTTongTienThueXuatKhau", SqlDbType.VarChar, MaTTTongTienThueXuatKhau);
			db.AddInParameter(dbCommand, "@TongSoTienLePhi", SqlDbType.Decimal, TongSoTienLePhi);
			db.AddInParameter(dbCommand, "@MaTTCuaSoTienBaoLanh", SqlDbType.VarChar, MaTTCuaSoTienBaoLanh);
			db.AddInParameter(dbCommand, "@SoQuanLyNguoiSuDung", SqlDbType.VarChar, SoQuanLyNguoiSuDung);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@TongSoTrangCuaToKhai", SqlDbType.Decimal, TongSoTrangCuaToKhai);
			db.AddInParameter(dbCommand, "@TongSoDongHangCuaToKhai", SqlDbType.Decimal, TongSoDongHangCuaToKhai);
			db.AddInParameter(dbCommand, "@NgayKhaiBaoNopThue", SqlDbType.DateTime, NgayKhaiBaoNopThue.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBaoNopThue);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy1", SqlDbType.VarChar, MaVanbanPhapQuy1);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy2", SqlDbType.VarChar, MaVanbanPhapQuy2);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy3", SqlDbType.VarChar, MaVanbanPhapQuy3);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy4", SqlDbType.VarChar, MaVanbanPhapQuy4);
			db.AddInParameter(dbCommand, "@MaVanbanPhapQuy5", SqlDbType.VarChar, MaVanbanPhapQuy5);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@HopDong_So", SqlDbType.VarChar, HopDong_So);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.VarChar, LoaiHang);
			db.AddInParameter(dbCommand, "@Templ_1", SqlDbType.VarChar, Templ_1);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiMauDich(long id)
		{
			KDT_VNACC_ToKhaiMauDich entity = new KDT_VNACC_ToKhaiMauDich();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiMauDich> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}