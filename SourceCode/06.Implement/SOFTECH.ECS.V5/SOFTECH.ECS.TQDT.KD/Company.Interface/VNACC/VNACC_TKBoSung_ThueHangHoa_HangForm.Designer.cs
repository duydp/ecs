﻿namespace Company.Interface
{
    partial class VNACC_TKBoSung_ThueHangHoa_HangForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout gridThueThuKhac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TKBoSung_ThueHangHoa_HangForm));
            Janus.Windows.GridEX.GridEXLayout grdHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnAddThuKhac = new Janus.Windows.EditControls.UIButton();
            this.btnDelThuKhac = new Janus.Windows.EditControls.UIButton();
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatSauKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuongTinhThueSauKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoTienThueTruocKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienThueSauKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaSoHangHoaSauKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatTruocKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaSoHangHoaTruocKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatSauKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThueSauKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gridThueThuKhac = new Janus.Windows.GridEX.GridEX();
            this.ucMaNuocXuatXuSauKhiKhaiBoSung = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtSoThuTuDongHangTrenToKhaiGoc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.txtMoTaHangHoaSauKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMoTaHangHoaTruocKhiKhaiBoSung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grdHang = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridThueThuKhac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 605), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 605);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 581);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 581);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grdHang);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(785, 605);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.btnAddThuKhac);
            this.uiGroupBox1.Controls.Add(this.btnDelThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtSoTienThueTruocKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtSoTienThueSauKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtThueSuatTruocKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtThueSuatSauKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac);
            this.uiGroupBox1.Controls.Add(this.label21);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaTinhThueTruocKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongTinhThueTruocKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongTinhThueSauKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtSoTienThueTruocKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtSoTienThueSauKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtMaSoHangHoaSauKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtThueSuatTruocKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtMaSoHangHoaTruocKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtThueSuatSauKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaTinhThueSauKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.label16);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.gridThueThuKhac);
            this.uiGroupBox1.Controls.Add(this.ucMaNuocXuatXuSauKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.ucMaNuocXuatXuTruocKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtSoThuTuDongHangTrenToKhaiGoc);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.txtMoTaHangHoaSauKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.txtMoTaHangHoaTruocKhiKhaiBoSung);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(785, 487);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac
            // 
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.DecimalDigits = 3;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(118, 285);
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.MaxLength = 17;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Name = "txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac";
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(144, 21);
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.TabIndex = 17;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Text = "0.000";
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 290);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 86;
            this.label7.Text = "(Trước khi khai báo)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(10, 317);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 13);
            this.label15.TabIndex = 87;
            this.label15.Text = "(Sau khi khai báo)";
            // 
            // btnAddThuKhac
            // 
            this.btnAddThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddThuKhac.Image = ((System.Drawing.Image)(resources.GetObject("btnAddThuKhac.Image")));
            this.btnAddThuKhac.ImageIndex = 4;
            this.btnAddThuKhac.Location = new System.Drawing.Point(118, 339);
            this.btnAddThuKhac.Name = "btnAddThuKhac";
            this.btnAddThuKhac.Size = new System.Drawing.Size(26, 23);
            this.btnAddThuKhac.TabIndex = 29;
            this.btnAddThuKhac.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnDelThuKhac
            // 
            this.btnDelThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelThuKhac.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelThuKhac.Icon")));
            this.btnDelThuKhac.Location = new System.Drawing.Point(118, 368);
            this.btnDelThuKhac.Name = "btnDelThuKhac";
            this.btnDelThuKhac.Size = new System.Drawing.Size(26, 23);
            this.btnDelThuKhac.TabIndex = 30;
            this.btnDelThuKhac.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac
            // 
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.DecimalDigits = 0;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(268, 285);
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.MaxLength = 12;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Name = "txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac";
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(118, 21);
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.TabIndex = 18;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Text = "0";
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac
            // 
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Appearance.Options.UseBackColor = true;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Code = "";
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.IsValidate = false;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(392, 309);
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Name = "ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac";
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Name_VN = "";
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.ShowColumnCode = true;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.ShowColumnName = false;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(53, 26);
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.TabIndex = 25;
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.WhereCondition = "";
            this.ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac
            // 
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.DecimalDigits = 0;
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(268, 312);
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.MaxLength = 12;
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Name = "txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac";
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(118, 21);
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.TabIndex = 24;
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Text = "0";
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac
            // 
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Appearance.Options.UseBackColor = true;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Code = "";
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.IsValidate = false;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(392, 282);
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Name = "ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac";
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Name_VN = "";
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.ShowColumnCode = true;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.ShowColumnName = false;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(53, 26);
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.TabIndex = 19;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.WhereCondition = "";
            this.ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoTienThueTruocKhiKhaiBoSungThuKhac
            // 
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.DecimalDigits = 3;
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(641, 285);
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.MaxLength = 16;
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.Name = "txtSoTienThueTruocKhiKhaiBoSungThuKhac";
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(131, 21);
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.TabIndex = 22;
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = "0.000";
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoTienThueTruocKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienThueSauKhiKhaiBoSungThuKhac
            // 
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.DecimalDigits = 3;
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(641, 312);
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.MaxLength = 16;
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.Name = "txtSoTienThueSauKhiKhaiBoSungThuKhac";
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(131, 21);
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.TabIndex = 28;
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = "0.000";
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoTienThueSauKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac
            // 
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(451, 312);
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.MaxLength = 12;
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Name = "txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac";
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(97, 21);
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.TabIndex = 26;
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatTruocKhiKhaiBoSungThuKhac
            // 
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.DecimalDigits = 3;
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(554, 285);
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.MaxLength = 30;
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.Name = "txtThueSuatTruocKhiKhaiBoSungThuKhac";
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(81, 21);
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.TabIndex = 21;
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.Text = "0.000";
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtThueSuatTruocKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac
            // 
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(451, 285);
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.MaxLength = 12;
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Name = "txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac";
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(97, 21);
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.TabIndex = 20;
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatSauKhiKhaiBoSungThuKhac
            // 
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.DecimalDigits = 3;
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(554, 312);
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.MaxLength = 30;
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.Name = "txtThueSuatSauKhiKhaiBoSungThuKhac";
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(81, 21);
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.TabIndex = 27;
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.Text = "0.000";
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtThueSuatSauKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac
            // 
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.DecimalDigits = 3;
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Location = new System.Drawing.Point(118, 312);
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.MaxLength = 17;
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Name = "txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac";
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Size = new System.Drawing.Size(144, 21);
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.TabIndex = 23;
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Text = "0.000";
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(12, 251);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(259, 13);
            this.label21.TabIndex = 88;
            this.label21.Text = "Thuế và thu khác (Cho phép thêm 5 bản ghi):";
            // 
            // txtTriGiaTinhThueTruocKhiKhaiBoSung
            // 
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.DecimalDigits = 3;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.Location = new System.Drawing.Point(118, 194);
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.MaxLength = 17;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.Name = "txtTriGiaTinhThueTruocKhiKhaiBoSung";
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.Size = new System.Drawing.Size(144, 21);
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.TabIndex = 5;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.Text = "0.000";
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTriGiaTinhThueTruocKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 199);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 63;
            this.label12.Text = "(Trước khi khai báo)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 226);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 61;
            this.label13.Text = "(Sau khi khai báo)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(265, 269);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 13);
            this.label20.TabIndex = 57;
            this.label20.Text = "Số lượng tính thuế";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(265, 178);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 13);
            this.label9.TabIndex = 57;
            this.label9.Text = "Số lượng tính thuế";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(389, 269);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 13);
            this.label19.TabIndex = 58;
            this.label19.Text = "ĐVT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(389, 178);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 58;
            this.label10.Text = "ĐVT";
            // 
            // txtSoLuongTinhThueTruocKhiKhaiBoSung
            // 
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.DecimalDigits = 0;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.Location = new System.Drawing.Point(268, 194);
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.MaxLength = 12;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.Name = "txtSoLuongTinhThueTruocKhiKhaiBoSung";
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.Size = new System.Drawing.Size(118, 21);
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.TabIndex = 6;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.Text = "0";
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongTinhThueTruocKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung
            // 
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Appearance.Options.UseBackColor = true;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Code = "";
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.IsValidate = false;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Location = new System.Drawing.Point(392, 218);
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Name = "ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung";
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Name_VN = "";
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.ShowColumnCode = true;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.ShowColumnName = false;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Size = new System.Drawing.Size(53, 26);
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.TabIndex = 13;
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.WhereCondition = "";
            this.ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoLuongTinhThueSauKhiKhaiBoSung
            // 
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.DecimalDigits = 0;
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.Location = new System.Drawing.Point(268, 221);
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.MaxLength = 12;
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.Name = "txtSoLuongTinhThueSauKhiKhaiBoSung";
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.Size = new System.Drawing.Size(118, 21);
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.TabIndex = 12;
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.Text = "0";
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongTinhThueSauKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung
            // 
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Appearance.Options.UseBackColor = true;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Code = "";
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.IsValidate = false;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Location = new System.Drawing.Point(392, 191);
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Name = "ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung";
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Name_VN = "";
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.ShowColumnCode = true;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.ShowColumnName = false;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Size = new System.Drawing.Size(53, 26);
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.TabIndex = 7;
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.WhereCondition = "";
            this.ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoTienThueTruocKhiKhaiBoSung
            // 
            this.txtSoTienThueTruocKhiKhaiBoSung.DecimalDigits = 3;
            this.txtSoTienThueTruocKhiKhaiBoSung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThueTruocKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThueTruocKhiKhaiBoSung.Location = new System.Drawing.Point(641, 194);
            this.txtSoTienThueTruocKhiKhaiBoSung.MaxLength = 16;
            this.txtSoTienThueTruocKhiKhaiBoSung.Name = "txtSoTienThueTruocKhiKhaiBoSung";
            this.txtSoTienThueTruocKhiKhaiBoSung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThueTruocKhiKhaiBoSung.Size = new System.Drawing.Size(131, 21);
            this.txtSoTienThueTruocKhiKhaiBoSung.TabIndex = 10;
            this.txtSoTienThueTruocKhiKhaiBoSung.Text = "0.000";
            this.txtSoTienThueTruocKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThueTruocKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoTienThueTruocKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienThueSauKhiKhaiBoSung
            // 
            this.txtSoTienThueSauKhiKhaiBoSung.DecimalDigits = 3;
            this.txtSoTienThueSauKhiKhaiBoSung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThueSauKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThueSauKhiKhaiBoSung.Location = new System.Drawing.Point(641, 221);
            this.txtSoTienThueSauKhiKhaiBoSung.MaxLength = 16;
            this.txtSoTienThueSauKhiKhaiBoSung.Name = "txtSoTienThueSauKhiKhaiBoSung";
            this.txtSoTienThueSauKhiKhaiBoSung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThueSauKhiKhaiBoSung.Size = new System.Drawing.Size(131, 21);
            this.txtSoTienThueSauKhiKhaiBoSung.TabIndex = 16;
            this.txtSoTienThueSauKhiKhaiBoSung.Text = "0.000";
            this.txtSoTienThueSauKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThueSauKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoTienThueSauKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaSoHangHoaSauKhiKhaiBoSung
            // 
            this.txtMaSoHangHoaSauKhiKhaiBoSung.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSoHangHoaSauKhiKhaiBoSung.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSoHangHoaSauKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSoHangHoaSauKhiKhaiBoSung.Location = new System.Drawing.Point(451, 221);
            this.txtMaSoHangHoaSauKhiKhaiBoSung.MaxLength = 12;
            this.txtMaSoHangHoaSauKhiKhaiBoSung.Name = "txtMaSoHangHoaSauKhiKhaiBoSung";
            this.txtMaSoHangHoaSauKhiKhaiBoSung.Size = new System.Drawing.Size(97, 21);
            this.txtMaSoHangHoaSauKhiKhaiBoSung.TabIndex = 14;
            this.txtMaSoHangHoaSauKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSoHangHoaSauKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatTruocKhiKhaiBoSung
            // 
            this.txtThueSuatTruocKhiKhaiBoSung.DecimalDigits = 3;
            this.txtThueSuatTruocKhiKhaiBoSung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuatTruocKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatTruocKhiKhaiBoSung.Location = new System.Drawing.Point(554, 194);
            this.txtThueSuatTruocKhiKhaiBoSung.MaxLength = 30;
            this.txtThueSuatTruocKhiKhaiBoSung.Name = "txtThueSuatTruocKhiKhaiBoSung";
            this.txtThueSuatTruocKhiKhaiBoSung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuatTruocKhiKhaiBoSung.Size = new System.Drawing.Size(81, 21);
            this.txtThueSuatTruocKhiKhaiBoSung.TabIndex = 9;
            this.txtThueSuatTruocKhiKhaiBoSung.Text = "0.000";
            this.txtThueSuatTruocKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuatTruocKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtThueSuatTruocKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaSoHangHoaTruocKhiKhaiBoSung
            // 
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.Location = new System.Drawing.Point(451, 194);
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.MaxLength = 12;
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.Name = "txtMaSoHangHoaTruocKhiKhaiBoSung";
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.Size = new System.Drawing.Size(97, 21);
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.TabIndex = 8;
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSoHangHoaTruocKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatSauKhiKhaiBoSung
            // 
            this.txtThueSuatSauKhiKhaiBoSung.DecimalDigits = 3;
            this.txtThueSuatSauKhiKhaiBoSung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuatSauKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatSauKhiKhaiBoSung.Location = new System.Drawing.Point(554, 221);
            this.txtThueSuatSauKhiKhaiBoSung.MaxLength = 30;
            this.txtThueSuatSauKhiKhaiBoSung.Name = "txtThueSuatSauKhiKhaiBoSung";
            this.txtThueSuatSauKhiKhaiBoSung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuatSauKhiKhaiBoSung.Size = new System.Drawing.Size(81, 21);
            this.txtThueSuatSauKhiKhaiBoSung.TabIndex = 15;
            this.txtThueSuatSauKhiKhaiBoSung.Text = "0.000";
            this.txtThueSuatSauKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuatSauKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtThueSuatSauKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThueSauKhiKhaiBoSung
            // 
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.DecimalDigits = 3;
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.Location = new System.Drawing.Point(118, 221);
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.MaxLength = 17;
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.Name = "txtTriGiaTinhThueSauKhiKhaiBoSung";
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.Size = new System.Drawing.Size(144, 21);
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.TabIndex = 11;
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.Text = "0.000";
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTriGiaTinhThueSauKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(638, 269);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 13);
            this.label23.TabIndex = 68;
            this.label23.Text = "Tiền thuế phải nộp";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(638, 178);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 13);
            this.label14.TabIndex = 68;
            this.label14.Text = "Tiền thuế phải nộp";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(435, 269);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 13);
            this.label17.TabIndex = 69;
            this.label17.Text = "Mã xác định thuế suất";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(553, 269);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 13);
            this.label22.TabIndex = 67;
            this.label22.Text = "Thuế suất (%)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(451, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 69;
            this.label4.Text = "Mã số hàng hóa";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(553, 178);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 67;
            this.label11.Text = "Thuế suất (%)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(115, 269);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 70;
            this.label18.Text = "Trị giá tính thuế";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(12, 162);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 13);
            this.label16.TabIndex = 65;
            this.label16.Text = "Thuế xuất nhập khẩu:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(115, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Trị giá tính thuế";
            // 
            // gridThueThuKhac
            // 
            this.gridThueThuKhac.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridThueThuKhac.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridThueThuKhac.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridThueThuKhac.ColumnAutoResize = true;
            gridThueThuKhac_DesignTimeLayout.LayoutString = resources.GetString("gridThueThuKhac_DesignTimeLayout.LayoutString");
            this.gridThueThuKhac.DesignTimeLayout = gridThueThuKhac_DesignTimeLayout;
            this.gridThueThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridThueThuKhac.GroupByBoxVisible = false;
            this.gridThueThuKhac.Location = new System.Drawing.Point(152, 341);
            this.gridThueThuKhac.Name = "gridThueThuKhac";
            this.gridThueThuKhac.RecordNavigator = true;
            this.gridThueThuKhac.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridThueThuKhac.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridThueThuKhac.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridThueThuKhac.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridThueThuKhac.Size = new System.Drawing.Size(621, 110);
            this.gridThueThuKhac.TabIndex = 31;
            this.gridThueThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridThueThuKhac.VisualStyleManager = this.vsmMain;
            this.gridThueThuKhac.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdThueThuKhac_RowDoubleClick);
            // 
            // ucMaNuocXuatXuSauKhiKhaiBoSung
            // 
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.Code = "";
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.Location = new System.Drawing.Point(647, 104);
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.Name = "ucMaNuocXuatXuSauKhiKhaiBoSung";
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.Name_VN = "";
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.SetValidate = false;
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.ShowColumnCode = true;
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.ShowColumnName = true;
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.Size = new System.Drawing.Size(131, 26);
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.TabIndex = 4;
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.WhereCondition = "";
            this.ucMaNuocXuatXuSauKhiKhaiBoSung.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucMaNuocXuatXuTruocKhiKhaiBoSung
            // 
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.Code = "";
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.Location = new System.Drawing.Point(647, 52);
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.Name = "ucMaNuocXuatXuTruocKhiKhaiBoSung";
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.Name_VN = "";
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.SetValidate = false;
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.ShowColumnCode = true;
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.ShowColumnName = true;
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.Size = new System.Drawing.Size(131, 26);
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.TabIndex = 2;
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.WhereCondition = "";
            this.ucMaNuocXuatXuTruocKhiKhaiBoSung.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoThuTuDongHangTrenToKhaiGoc
            // 
            this.txtSoThuTuDongHangTrenToKhaiGoc.DecimalDigits = 0;
            this.txtSoThuTuDongHangTrenToKhaiGoc.Location = new System.Drawing.Point(207, 21);
            this.txtSoThuTuDongHangTrenToKhaiGoc.MaxLength = 2;
            this.txtSoThuTuDongHangTrenToKhaiGoc.Name = "txtSoThuTuDongHangTrenToKhaiGoc";
            this.txtSoThuTuDongHangTrenToKhaiGoc.Size = new System.Drawing.Size(68, 21);
            this.txtSoThuTuDongHangTrenToKhaiGoc.TabIndex = 0;
            this.txtSoThuTuDongHangTrenToKhaiGoc.Text = "0";
            this.txtSoThuTuDongHangTrenToKhaiGoc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(697, 457);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 34;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = global::Company.Interface.Properties.Resources.disk;
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(530, 457);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 32;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(614, 457);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 33;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtMoTaHangHoaSauKhiKhaiBoSung
            // 
            this.txtMoTaHangHoaSauKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoTaHangHoaSauKhiKhaiBoSung.Location = new System.Drawing.Point(120, 104);
            this.txtMoTaHangHoaSauKhiKhaiBoSung.MaxLength = 255;
            this.txtMoTaHangHoaSauKhiKhaiBoSung.Multiline = true;
            this.txtMoTaHangHoaSauKhiKhaiBoSung.Name = "txtMoTaHangHoaSauKhiKhaiBoSung";
            this.txtMoTaHangHoaSauKhiKhaiBoSung.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMoTaHangHoaSauKhiKhaiBoSung.Size = new System.Drawing.Size(521, 46);
            this.txtMoTaHangHoaSauKhiKhaiBoSung.TabIndex = 3;
            this.txtMoTaHangHoaSauKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMoTaHangHoaSauKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMoTaHangHoaTruocKhiKhaiBoSung
            // 
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.Location = new System.Drawing.Point(120, 52);
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.MaxLength = 255;
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.Multiline = true;
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.Name = "txtMoTaHangHoaTruocKhiKhaiBoSung";
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.Size = new System.Drawing.Size(521, 46);
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.TabIndex = 1;
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMoTaHangHoaTruocKhiKhaiBoSung.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Số thứ tự dòng/ hàng trên tờ khai gốc";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "(Sau khi khai báo)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "(Trước khi khai báo)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(644, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Mã nước xuất xứ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(323, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Mô tả hàng hóa";
            // 
            // grdHang
            // 
            this.grdHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHang.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.ColumnAutoResize = true;
            grdHang_DesignTimeLayout.LayoutString = resources.GetString("grdHang_DesignTimeLayout.LayoutString");
            this.grdHang.DesignTimeLayout = grdHang_DesignTimeLayout;
            this.grdHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdHang.GroupByBoxVisible = false;
            this.grdHang.Location = new System.Drawing.Point(0, 487);
            this.grdHang.Name = "grdHang";
            this.grdHang.RecordNavigator = true;
            this.grdHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdHang.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHang.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHang.Size = new System.Drawing.Size(785, 118);
            this.grdHang.TabIndex = 1;
            this.grdHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdHang.VisualStyleManager = this.vsmMain;
            this.grdHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdHang_RowDoubleClick);
            // 
            // VNACC_TKBoSung_ThueHangHoa_HangForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 611);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_TKBoSung_ThueHangHoa_HangForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hàng hóa";
            this.Load += new System.EventHandler(this.VNACC_TKBoSung_ThueHangHoa_HangForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridThueThuKhac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grdHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtMoTaHangHoaTruocKhiKhaiBoSung;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoThuTuDongHangTrenToKhaiGoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMoTaHangHoaSauKhiKhaiBoSung;
        private System.Windows.Forms.Label label5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucMaNuocXuatXuSauKhiKhaiBoSung;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucMaNuocXuatXuTruocKhiKhaiBoSung;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.GridEX gridThueThuKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIButton btnAddThuKhac;
        private Janus.Windows.EditControls.UIButton btnDelThuKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThueTruocKhiKhaiBoSungThuKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThueSauKhiKhaiBoSungThuKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuatTruocKhiKhaiBoSungThuKhac;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuatSauKhiKhaiBoSungThuKhac;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueTruocKhiKhaiBoSung;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongTinhThueTruocKhiKhaiBoSung;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongTinhThueSauKhiKhaiBoSung;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThueTruocKhiKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThueSauKhiKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSoHangHoaSauKhiKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuatTruocKhiKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSoHangHoaTruocKhiKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuatSauKhiKhaiBoSung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueSauKhiKhaiBoSung;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac;
    }
}