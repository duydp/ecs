using System;
using System.Data;
using System.Data.SqlClient;
#if GC_V3
using Company.GC.BLL;
#elif KD_V3
using Company.KD.BLL.KDT;
#endif
namespace Company.KD.BLL.KDT
{
    public partial class ToKhaiTriGiaPP23 : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected long _TKMD_ID;
        protected int _MaToKhaiTriGia;
        protected string _LyDo = String.Empty;
        protected string _TenHang = String.Empty;
        protected int _STTHang;
        protected DateTime _NgayXuat = new DateTime(1900, 01, 01);
        protected int _STTHangTT;
        protected string _TenHangTT = String.Empty;
        protected DateTime _NgayXuatTT = new DateTime(1900, 01, 01);
        protected long _SoTKHangTT;
        protected DateTime _NgayDangKyHangTT = new DateTime(1900, 01, 01);
        protected string _MaLoaiHinhHangTT = String.Empty;
        protected string _MaHaiQuanHangTT = String.Empty;
        protected double _TriGiaNguyenTeHangTT;
        protected double _DieuChinhCongThuongMai;
        protected double _DieuChinhCongSoLuong;
        protected double _DieuChinhCongKhoanGiamGiaKhac;
        protected double _DieuChinhCongChiPhiVanTai;
        protected double _DieuChinhCongChiPhiBaoHiem;
        protected double _TriGiaNguyenTeHang;
        protected double _TriGiaTTHang;
        protected int _ToSo;
        protected double _DieuChinhTruCapDoThuongMai;
        protected double _DieuChinhTruSoLuong;
        protected double _DieuChinhTruKhoanGiamGiaKhac;
        protected double _DieuChinhTruChiPhiVanTai;
        protected double _DieuChinhTruChiPhiBaoHiem;
        protected string _GiaiTrinh = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long TKMD_ID
        {
            set { this._TKMD_ID = value; }
            get { return this._TKMD_ID; }
        }
        public int MaToKhaiTriGia
        {
            set { this._MaToKhaiTriGia = value; }
            get { return this._MaToKhaiTriGia; }
        }
        public string LyDo
        {
            set { this._LyDo = value; }
            get { return this._LyDo; }
        }
        public string TenHang
        {
            set { this._TenHang = value; }
            get { return this._TenHang; }
        }
        public int STTHang
        {
            set { this._STTHang = value; }
            get { return this._STTHang; }
        }
        public DateTime NgayXuat
        {
            set { this._NgayXuat = value; }
            get { return this._NgayXuat; }
        }
        public int STTHangTT
        {
            set { this._STTHangTT = value; }
            get { return this._STTHangTT; }
        }
        public string TenHangTT
        {
            set { this._TenHangTT = value; }
            get { return this._TenHangTT; }
        }
        public DateTime NgayXuatTT
        {
            set { this._NgayXuatTT = value; }
            get { return this._NgayXuatTT; }
        }
        public long SoTKHangTT
        {
            set { this._SoTKHangTT = value; }
            get { return this._SoTKHangTT; }
        }
        public DateTime NgayDangKyHangTT
        {
            set { this._NgayDangKyHangTT = value; }
            get { return this._NgayDangKyHangTT; }
        }
        public string MaLoaiHinhHangTT
        {
            set { this._MaLoaiHinhHangTT = value; }
            get { return this._MaLoaiHinhHangTT; }
        }
        public string MaHaiQuanHangTT
        {
            set { this._MaHaiQuanHangTT = value; }
            get { return this._MaHaiQuanHangTT; }
        }
        public double TriGiaNguyenTeHangTT
        {
            set { this._TriGiaNguyenTeHangTT = value; }
            get { return this._TriGiaNguyenTeHangTT; }
        }
        public double DieuChinhCongThuongMai
        {
            set { this._DieuChinhCongThuongMai = value; }
            get { return this._DieuChinhCongThuongMai; }
        }
        public double DieuChinhCongSoLuong
        {
            set { this._DieuChinhCongSoLuong = value; }
            get { return this._DieuChinhCongSoLuong; }
        }
        public double DieuChinhCongKhoanGiamGiaKhac
        {
            set { this._DieuChinhCongKhoanGiamGiaKhac = value; }
            get { return this._DieuChinhCongKhoanGiamGiaKhac; }
        }
        public double DieuChinhCongChiPhiVanTai
        {
            set { this._DieuChinhCongChiPhiVanTai = value; }
            get { return this._DieuChinhCongChiPhiVanTai; }
        }
        public double DieuChinhCongChiPhiBaoHiem
        {
            set { this._DieuChinhCongChiPhiBaoHiem = value; }
            get { return this._DieuChinhCongChiPhiBaoHiem; }
        }
        public double TriGiaNguyenTeHang
        {
            set { this._TriGiaNguyenTeHang = value; }
            get { return this._TriGiaNguyenTeHang; }
        }
        public double TriGiaTTHang
        {
            set { this._TriGiaTTHang = value; }
            get { return this._TriGiaTTHang; }
        }
        public int ToSo
        {
            set { this._ToSo = value; }
            get { return this._ToSo; }
        }
        public double DieuChinhTruCapDoThuongMai
        {
            set { this._DieuChinhTruCapDoThuongMai = value; }
            get { return this._DieuChinhTruCapDoThuongMai; }
        }
        public double DieuChinhTruSoLuong
        {
            set { this._DieuChinhTruSoLuong = value; }
            get { return this._DieuChinhTruSoLuong; }
        }
        public double DieuChinhTruKhoanGiamGiaKhac
        {
            set { this._DieuChinhTruKhoanGiamGiaKhac = value; }
            get { return this._DieuChinhTruKhoanGiamGiaKhac; }
        }
        public double DieuChinhTruChiPhiVanTai
        {
            set { this._DieuChinhTruChiPhiVanTai = value; }
            get { return this._DieuChinhTruChiPhiVanTai; }
        }
        public double DieuChinhTruChiPhiBaoHiem
        {
            set { this._DieuChinhTruChiPhiBaoHiem = value; }
            get { return this._DieuChinhTruChiPhiBaoHiem; }
        }
        public string GiaiTrinh
        {
            set { this._GiaiTrinh = value; }
            get { return this._GiaiTrinh; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaToKhaiTriGia"))) this._MaToKhaiTriGia = reader.GetInt32(reader.GetOrdinal("MaToKhaiTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDo"))) this._LyDo = reader.GetString(reader.GetOrdinal("LyDo"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuat"))) this._NgayXuat = reader.GetDateTime(reader.GetOrdinal("NgayXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHangTT"))) this._STTHangTT = reader.GetInt32(reader.GetOrdinal("STTHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangTT"))) this._TenHangTT = reader.GetString(reader.GetOrdinal("TenHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatTT"))) this._NgayXuatTT = reader.GetDateTime(reader.GetOrdinal("NgayXuatTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTKHangTT"))) this._SoTKHangTT = reader.GetInt64(reader.GetOrdinal("SoTKHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyHangTT"))) this._NgayDangKyHangTT = reader.GetDateTime(reader.GetOrdinal("NgayDangKyHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhHangTT"))) this._MaLoaiHinhHangTT = reader.GetString(reader.GetOrdinal("MaLoaiHinhHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanHangTT"))) this._MaHaiQuanHangTT = reader.GetString(reader.GetOrdinal("MaHaiQuanHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTeHangTT"))) this._TriGiaNguyenTeHangTT = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTeHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongThuongMai"))) this._DieuChinhCongThuongMai = reader.GetDouble(reader.GetOrdinal("DieuChinhCongThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongSoLuong"))) this._DieuChinhCongSoLuong = reader.GetDouble(reader.GetOrdinal("DieuChinhCongSoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongKhoanGiamGiaKhac"))) this._DieuChinhCongKhoanGiamGiaKhac = reader.GetDouble(reader.GetOrdinal("DieuChinhCongKhoanGiamGiaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongChiPhiVanTai"))) this._DieuChinhCongChiPhiVanTai = reader.GetDouble(reader.GetOrdinal("DieuChinhCongChiPhiVanTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongChiPhiBaoHiem"))) this._DieuChinhCongChiPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("DieuChinhCongChiPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTeHang"))) this._TriGiaNguyenTeHang = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTeHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTTHang"))) this._TriGiaTTHang = reader.GetDouble(reader.GetOrdinal("TriGiaTTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToSo"))) this._ToSo = reader.GetInt32(reader.GetOrdinal("ToSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruCapDoThuongMai"))) this._DieuChinhTruCapDoThuongMai = reader.GetDouble(reader.GetOrdinal("DieuChinhTruCapDoThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruSoLuong"))) this._DieuChinhTruSoLuong = reader.GetDouble(reader.GetOrdinal("DieuChinhTruSoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruKhoanGiamGiaKhac"))) this._DieuChinhTruKhoanGiamGiaKhac = reader.GetDouble(reader.GetOrdinal("DieuChinhTruKhoanGiamGiaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruChiPhiVanTai"))) this._DieuChinhTruChiPhiVanTai = reader.GetDouble(reader.GetOrdinal("DieuChinhTruChiPhiVanTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruChiPhiBaoHiem"))) this._DieuChinhTruChiPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("DieuChinhTruChiPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaiTrinh"))) this._GiaiTrinh = reader.GetString(reader.GetOrdinal("GiaiTrinh"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiTriGiaPP23Collection SelectCollectionBy_TKMD_ID()
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            ToKhaiTriGiaPP23Collection collection = new ToKhaiTriGiaPP23Collection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiTriGiaPP23 entity = new ToKhaiTriGiaPP23();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaToKhaiTriGia"))) entity.MaToKhaiTriGia = reader.GetInt32(reader.GetOrdinal("MaToKhaiTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDo"))) entity.LyDo = reader.GetString(reader.GetOrdinal("LyDo"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuat"))) entity.NgayXuat = reader.GetDateTime(reader.GetOrdinal("NgayXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHangTT"))) entity.STTHangTT = reader.GetInt32(reader.GetOrdinal("STTHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangTT"))) entity.TenHangTT = reader.GetString(reader.GetOrdinal("TenHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatTT"))) entity.NgayXuatTT = reader.GetDateTime(reader.GetOrdinal("NgayXuatTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTKHangTT"))) entity.SoTKHangTT = reader.GetInt64(reader.GetOrdinal("SoTKHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyHangTT"))) entity.NgayDangKyHangTT = reader.GetDateTime(reader.GetOrdinal("NgayDangKyHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhHangTT"))) entity.MaLoaiHinhHangTT = reader.GetString(reader.GetOrdinal("MaLoaiHinhHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanHangTT"))) entity.MaHaiQuanHangTT = reader.GetString(reader.GetOrdinal("MaHaiQuanHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTeHangTT"))) entity.TriGiaNguyenTeHangTT = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTeHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongThuongMai"))) entity.DieuChinhCongThuongMai = reader.GetDouble(reader.GetOrdinal("DieuChinhCongThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongSoLuong"))) entity.DieuChinhCongSoLuong = reader.GetDouble(reader.GetOrdinal("DieuChinhCongSoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongKhoanGiamGiaKhac"))) entity.DieuChinhCongKhoanGiamGiaKhac = reader.GetDouble(reader.GetOrdinal("DieuChinhCongKhoanGiamGiaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongChiPhiVanTai"))) entity.DieuChinhCongChiPhiVanTai = reader.GetDouble(reader.GetOrdinal("DieuChinhCongChiPhiVanTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongChiPhiBaoHiem"))) entity.DieuChinhCongChiPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("DieuChinhCongChiPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTeHang"))) entity.TriGiaNguyenTeHang = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTeHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTTHang"))) entity.TriGiaTTHang = reader.GetDouble(reader.GetOrdinal("TriGiaTTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToSo"))) entity.ToSo = reader.GetInt32(reader.GetOrdinal("ToSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruCapDoThuongMai"))) entity.DieuChinhTruCapDoThuongMai = reader.GetDouble(reader.GetOrdinal("DieuChinhTruCapDoThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruSoLuong"))) entity.DieuChinhTruSoLuong = reader.GetDouble(reader.GetOrdinal("DieuChinhTruSoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruKhoanGiamGiaKhac"))) entity.DieuChinhTruKhoanGiamGiaKhac = reader.GetDouble(reader.GetOrdinal("DieuChinhTruKhoanGiamGiaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruChiPhiVanTai"))) entity.DieuChinhTruChiPhiVanTai = reader.GetDouble(reader.GetOrdinal("DieuChinhTruChiPhiVanTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruChiPhiBaoHiem"))) entity.DieuChinhTruChiPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("DieuChinhTruChiPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaiTrinh"))) entity.GiaiTrinh = reader.GetString(reader.GetOrdinal("GiaiTrinh"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_TKMD_ID()
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiTriGiaPP23Collection SelectCollectionAll()
        {
            ToKhaiTriGiaPP23Collection collection = new ToKhaiTriGiaPP23Collection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                ToKhaiTriGiaPP23 entity = new ToKhaiTriGiaPP23();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaToKhaiTriGia"))) entity.MaToKhaiTriGia = reader.GetInt32(reader.GetOrdinal("MaToKhaiTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDo"))) entity.LyDo = reader.GetString(reader.GetOrdinal("LyDo"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuat"))) entity.NgayXuat = reader.GetDateTime(reader.GetOrdinal("NgayXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHangTT"))) entity.STTHangTT = reader.GetInt32(reader.GetOrdinal("STTHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangTT"))) entity.TenHangTT = reader.GetString(reader.GetOrdinal("TenHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatTT"))) entity.NgayXuatTT = reader.GetDateTime(reader.GetOrdinal("NgayXuatTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTKHangTT"))) entity.SoTKHangTT = reader.GetInt64(reader.GetOrdinal("SoTKHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyHangTT"))) entity.NgayDangKyHangTT = reader.GetDateTime(reader.GetOrdinal("NgayDangKyHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhHangTT"))) entity.MaLoaiHinhHangTT = reader.GetString(reader.GetOrdinal("MaLoaiHinhHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanHangTT"))) entity.MaHaiQuanHangTT = reader.GetString(reader.GetOrdinal("MaHaiQuanHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTeHangTT"))) entity.TriGiaNguyenTeHangTT = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTeHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongThuongMai"))) entity.DieuChinhCongThuongMai = reader.GetDouble(reader.GetOrdinal("DieuChinhCongThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongSoLuong"))) entity.DieuChinhCongSoLuong = reader.GetDouble(reader.GetOrdinal("DieuChinhCongSoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongKhoanGiamGiaKhac"))) entity.DieuChinhCongKhoanGiamGiaKhac = reader.GetDouble(reader.GetOrdinal("DieuChinhCongKhoanGiamGiaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongChiPhiVanTai"))) entity.DieuChinhCongChiPhiVanTai = reader.GetDouble(reader.GetOrdinal("DieuChinhCongChiPhiVanTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongChiPhiBaoHiem"))) entity.DieuChinhCongChiPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("DieuChinhCongChiPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTeHang"))) entity.TriGiaNguyenTeHang = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTeHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTTHang"))) entity.TriGiaTTHang = reader.GetDouble(reader.GetOrdinal("TriGiaTTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToSo"))) entity.ToSo = reader.GetInt32(reader.GetOrdinal("ToSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruCapDoThuongMai"))) entity.DieuChinhTruCapDoThuongMai = reader.GetDouble(reader.GetOrdinal("DieuChinhTruCapDoThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruSoLuong"))) entity.DieuChinhTruSoLuong = reader.GetDouble(reader.GetOrdinal("DieuChinhTruSoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruKhoanGiamGiaKhac"))) entity.DieuChinhTruKhoanGiamGiaKhac = reader.GetDouble(reader.GetOrdinal("DieuChinhTruKhoanGiamGiaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruChiPhiVanTai"))) entity.DieuChinhTruChiPhiVanTai = reader.GetDouble(reader.GetOrdinal("DieuChinhTruChiPhiVanTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruChiPhiBaoHiem"))) entity.DieuChinhTruChiPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("DieuChinhTruChiPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaiTrinh"))) entity.GiaiTrinh = reader.GetString(reader.GetOrdinal("GiaiTrinh"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiTriGiaPP23Collection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            ToKhaiTriGiaPP23Collection collection = new ToKhaiTriGiaPP23Collection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiTriGiaPP23 entity = new ToKhaiTriGiaPP23();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaToKhaiTriGia"))) entity.MaToKhaiTriGia = reader.GetInt32(reader.GetOrdinal("MaToKhaiTriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDo"))) entity.LyDo = reader.GetString(reader.GetOrdinal("LyDo"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuat"))) entity.NgayXuat = reader.GetDateTime(reader.GetOrdinal("NgayXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHangTT"))) entity.STTHangTT = reader.GetInt32(reader.GetOrdinal("STTHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangTT"))) entity.TenHangTT = reader.GetString(reader.GetOrdinal("TenHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatTT"))) entity.NgayXuatTT = reader.GetDateTime(reader.GetOrdinal("NgayXuatTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTKHangTT"))) entity.SoTKHangTT = reader.GetInt64(reader.GetOrdinal("SoTKHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyHangTT"))) entity.NgayDangKyHangTT = reader.GetDateTime(reader.GetOrdinal("NgayDangKyHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhHangTT"))) entity.MaLoaiHinhHangTT = reader.GetString(reader.GetOrdinal("MaLoaiHinhHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanHangTT"))) entity.MaHaiQuanHangTT = reader.GetString(reader.GetOrdinal("MaHaiQuanHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTeHangTT"))) entity.TriGiaNguyenTeHangTT = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTeHangTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongThuongMai"))) entity.DieuChinhCongThuongMai = reader.GetDouble(reader.GetOrdinal("DieuChinhCongThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongSoLuong"))) entity.DieuChinhCongSoLuong = reader.GetDouble(reader.GetOrdinal("DieuChinhCongSoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongKhoanGiamGiaKhac"))) entity.DieuChinhCongKhoanGiamGiaKhac = reader.GetDouble(reader.GetOrdinal("DieuChinhCongKhoanGiamGiaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongChiPhiVanTai"))) entity.DieuChinhCongChiPhiVanTai = reader.GetDouble(reader.GetOrdinal("DieuChinhCongChiPhiVanTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhCongChiPhiBaoHiem"))) entity.DieuChinhCongChiPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("DieuChinhCongChiPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTeHang"))) entity.TriGiaNguyenTeHang = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTeHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTTHang"))) entity.TriGiaTTHang = reader.GetDouble(reader.GetOrdinal("TriGiaTTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToSo"))) entity.ToSo = reader.GetInt32(reader.GetOrdinal("ToSo"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruCapDoThuongMai"))) entity.DieuChinhTruCapDoThuongMai = reader.GetDouble(reader.GetOrdinal("DieuChinhTruCapDoThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruSoLuong"))) entity.DieuChinhTruSoLuong = reader.GetDouble(reader.GetOrdinal("DieuChinhTruSoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruKhoanGiamGiaKhac"))) entity.DieuChinhTruKhoanGiamGiaKhac = reader.GetDouble(reader.GetOrdinal("DieuChinhTruKhoanGiamGiaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruChiPhiVanTai"))) entity.DieuChinhTruChiPhiVanTai = reader.GetDouble(reader.GetOrdinal("DieuChinhTruChiPhiVanTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhTruChiPhiBaoHiem"))) entity.DieuChinhTruChiPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("DieuChinhTruChiPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaiTrinh"))) entity.GiaiTrinh = reader.GetString(reader.GetOrdinal("GiaiTrinh"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@MaToKhaiTriGia", SqlDbType.Int, this._MaToKhaiTriGia);
            this.db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, this._LyDo);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@NgayXuat", SqlDbType.DateTime, this._NgayXuat);
            this.db.AddInParameter(dbCommand, "@STTHangTT", SqlDbType.Int, this._STTHangTT);
            this.db.AddInParameter(dbCommand, "@TenHangTT", SqlDbType.NVarChar, this._TenHangTT);
            this.db.AddInParameter(dbCommand, "@NgayXuatTT", SqlDbType.DateTime, this._NgayXuatTT);
            this.db.AddInParameter(dbCommand, "@SoTKHangTT", SqlDbType.BigInt, this._SoTKHangTT);
            this.db.AddInParameter(dbCommand, "@NgayDangKyHangTT", SqlDbType.DateTime, this._NgayDangKyHangTT);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhHangTT", SqlDbType.VarChar, this._MaLoaiHinhHangTT);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanHangTT", SqlDbType.VarChar, this._MaHaiQuanHangTT);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTeHangTT", SqlDbType.Float, this._TriGiaNguyenTeHangTT);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongThuongMai", SqlDbType.Float, this._DieuChinhCongThuongMai);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongSoLuong", SqlDbType.Float, this._DieuChinhCongSoLuong);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongKhoanGiamGiaKhac", SqlDbType.Float, this._DieuChinhCongKhoanGiamGiaKhac);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongChiPhiVanTai", SqlDbType.Float, this._DieuChinhCongChiPhiVanTai);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongChiPhiBaoHiem", SqlDbType.Float, this._DieuChinhCongChiPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTeHang", SqlDbType.Float, this._TriGiaNguyenTeHang);
            this.db.AddInParameter(dbCommand, "@TriGiaTTHang", SqlDbType.Float, this._TriGiaTTHang);
            this.db.AddInParameter(dbCommand, "@ToSo", SqlDbType.Int, this._ToSo);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruCapDoThuongMai", SqlDbType.Float, this._DieuChinhTruCapDoThuongMai);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruSoLuong", SqlDbType.Float, this._DieuChinhTruSoLuong);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruKhoanGiamGiaKhac", SqlDbType.Float, this._DieuChinhTruKhoanGiamGiaKhac);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruChiPhiVanTai", SqlDbType.Float, this._DieuChinhTruChiPhiVanTai);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruChiPhiBaoHiem", SqlDbType.Float, this._DieuChinhTruChiPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@GiaiTrinh", SqlDbType.NVarChar, this._GiaiTrinh);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(ToKhaiTriGiaPP23Collection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiTriGiaPP23 item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, ToKhaiTriGiaPP23Collection collection)
        {
            foreach (ToKhaiTriGiaPP23 item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@MaToKhaiTriGia", SqlDbType.Int, this._MaToKhaiTriGia);
            this.db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, this._LyDo);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@NgayXuat", SqlDbType.DateTime, this._NgayXuat);
            this.db.AddInParameter(dbCommand, "@STTHangTT", SqlDbType.Int, this._STTHangTT);
            this.db.AddInParameter(dbCommand, "@TenHangTT", SqlDbType.NVarChar, this._TenHangTT);
            this.db.AddInParameter(dbCommand, "@NgayXuatTT", SqlDbType.DateTime, this._NgayXuatTT);
            this.db.AddInParameter(dbCommand, "@SoTKHangTT", SqlDbType.BigInt, this._SoTKHangTT);
            this.db.AddInParameter(dbCommand, "@NgayDangKyHangTT", SqlDbType.DateTime, this._NgayDangKyHangTT);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhHangTT", SqlDbType.VarChar, this._MaLoaiHinhHangTT);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanHangTT", SqlDbType.VarChar, this._MaHaiQuanHangTT);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTeHangTT", SqlDbType.Float, this._TriGiaNguyenTeHangTT);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongThuongMai", SqlDbType.Float, this._DieuChinhCongThuongMai);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongSoLuong", SqlDbType.Float, this._DieuChinhCongSoLuong);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongKhoanGiamGiaKhac", SqlDbType.Float, this._DieuChinhCongKhoanGiamGiaKhac);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongChiPhiVanTai", SqlDbType.Float, this._DieuChinhCongChiPhiVanTai);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongChiPhiBaoHiem", SqlDbType.Float, this._DieuChinhCongChiPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTeHang", SqlDbType.Float, this._TriGiaNguyenTeHang);
            this.db.AddInParameter(dbCommand, "@TriGiaTTHang", SqlDbType.Float, this._TriGiaTTHang);
            this.db.AddInParameter(dbCommand, "@ToSo", SqlDbType.Int, this._ToSo);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruCapDoThuongMai", SqlDbType.Float, this._DieuChinhTruCapDoThuongMai);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruSoLuong", SqlDbType.Float, this._DieuChinhTruSoLuong);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruKhoanGiamGiaKhac", SqlDbType.Float, this._DieuChinhTruKhoanGiamGiaKhac);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruChiPhiVanTai", SqlDbType.Float, this._DieuChinhTruChiPhiVanTai);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruChiPhiBaoHiem", SqlDbType.Float, this._DieuChinhTruChiPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@GiaiTrinh", SqlDbType.NVarChar, this._GiaiTrinh);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(ToKhaiTriGiaPP23Collection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiTriGiaPP23 item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@MaToKhaiTriGia", SqlDbType.Int, this._MaToKhaiTriGia);
            this.db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, this._LyDo);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@NgayXuat", SqlDbType.DateTime, this._NgayXuat);
            this.db.AddInParameter(dbCommand, "@STTHangTT", SqlDbType.Int, this._STTHangTT);
            this.db.AddInParameter(dbCommand, "@TenHangTT", SqlDbType.NVarChar, this._TenHangTT);
            this.db.AddInParameter(dbCommand, "@NgayXuatTT", SqlDbType.DateTime, this._NgayXuatTT);
            this.db.AddInParameter(dbCommand, "@SoTKHangTT", SqlDbType.BigInt, this._SoTKHangTT);
            this.db.AddInParameter(dbCommand, "@NgayDangKyHangTT", SqlDbType.DateTime, this._NgayDangKyHangTT);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinhHangTT", SqlDbType.VarChar, this._MaLoaiHinhHangTT);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanHangTT", SqlDbType.VarChar, this._MaHaiQuanHangTT);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTeHangTT", SqlDbType.Float, this._TriGiaNguyenTeHangTT);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongThuongMai", SqlDbType.Float, this._DieuChinhCongThuongMai);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongSoLuong", SqlDbType.Float, this._DieuChinhCongSoLuong);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongKhoanGiamGiaKhac", SqlDbType.Float, this._DieuChinhCongKhoanGiamGiaKhac);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongChiPhiVanTai", SqlDbType.Float, this._DieuChinhCongChiPhiVanTai);
            this.db.AddInParameter(dbCommand, "@DieuChinhCongChiPhiBaoHiem", SqlDbType.Float, this._DieuChinhCongChiPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTeHang", SqlDbType.Float, this._TriGiaNguyenTeHang);
            this.db.AddInParameter(dbCommand, "@TriGiaTTHang", SqlDbType.Float, this._TriGiaTTHang);
            this.db.AddInParameter(dbCommand, "@ToSo", SqlDbType.Int, this._ToSo);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruCapDoThuongMai", SqlDbType.Float, this._DieuChinhTruCapDoThuongMai);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruSoLuong", SqlDbType.Float, this._DieuChinhTruSoLuong);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruKhoanGiamGiaKhac", SqlDbType.Float, this._DieuChinhTruKhoanGiamGiaKhac);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruChiPhiVanTai", SqlDbType.Float, this._DieuChinhTruChiPhiVanTai);
            this.db.AddInParameter(dbCommand, "@DieuChinhTruChiPhiBaoHiem", SqlDbType.Float, this._DieuChinhTruChiPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@GiaiTrinh", SqlDbType.NVarChar, this._GiaiTrinh);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(ToKhaiTriGiaPP23Collection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiTriGiaPP23 item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(ToKhaiTriGiaPP23Collection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiTriGiaPP23 item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(ToKhaiTriGiaPP23Collection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiTriGiaPP23 item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleteBy_TKMD_ID()
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_DeleteBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_TKMD_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_KD_ToKhaiTriGiaPP23_DeleteBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        #endregion
    }
}