using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_CX_NPLXinHuyDangKy : ICloneable
	{
        private List<KDT_CX_NPLXinHuy> _ListXinHuy = new List<KDT_CX_NPLXinHuy>();

        public List<KDT_CX_NPLXinHuy> XinHuyList
        {
            set { this._ListXinHuy = value; }
            get { return this._ListXinHuy; }
        }

        public static KDT_CX_NPLXinHuyDangKy LoadFull(long id)
        {
            try
            {
                KDT_CX_NPLXinHuyDangKy XHDK = new KDT_CX_NPLXinHuyDangKy();
                XHDK = KDT_CX_NPLXinHuyDangKy.Load(id);
                List<KDT_CX_NPLXinHuy> ListTX = new List<KDT_CX_NPLXinHuy>();
                ListTX =  KDT_CX_NPLXinHuy.SelectCollectionDynamic( " Master_id="+XHDK.ID,"");
                if (ListTX.Count > 0)
                {
                    XHDK.XinHuyList = ListTX;
                }
                return XHDK;

            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public bool InsertUpdatFull()
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);
                    foreach (KDT_CX_NPLXinHuy item in this.XinHuyList)
                    {
                        item.Master_id = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                    throw;
                }
            }
            return ret;
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            KDT_CX_NPLXinHuy.DeleteDynamic("Master_ID = " + id);
            this.Delete();
        }
            

	}	
}