using System;
using System.Data;
using System.Data.SqlClient;
#if GC_V3
using Company.GC.BLL;
#elif KD_V3
using Company.KD.BLL.KDT;
#endif
namespace Company.KD.BLL.KDT
{
    public partial class HangTriGia : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected string _TenHang = String.Empty;
        protected int _STTHang;
        protected double _GiaTrenHoaDon;
        protected double _KhoanThanhToanGianTiep;
        protected double _TraTruoc;
        protected double _TongCong1;
        protected double _HoaHong;
        protected double _ChiPhiBaoBi;
        protected double _ChiPhiDongGoi;
        protected double _TroGiup;
        protected double _VatLieu;
        protected double _CongCu;
        protected double _NguyenLieu;
        protected double _ThietKe;
        protected double _BanQuyen;
        protected double _TienTraSuDung;
        protected double _ChiPhiVanChuyen;
        protected double _CuocPhiXepHang;
        protected double _CuocPhiBaoHiem;
        protected double _TongCong2;
        protected double _PhiBaoHiem;
        protected double _ChiPhiPhatSinh;
        protected double _TienLai;
        protected double _TienThue;
        protected double _GiamGia;
        protected double _ChiPhiKhongTang;
        protected double _TriGiaNguyenTe;
        protected double _TriGiaVND;
        protected double _NgayTruyen;
        protected long _TKTG_ID;
        protected double _ChietKhau;
        protected double _TongCong3;
        protected double _ChiPhiNoiDia;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public string TenHang
        {
            set { this._TenHang = value; }
            get { return this._TenHang; }
        }
        public int STTHang
        {
            set { this._STTHang = value; }
            get { return this._STTHang; }
        }
        public double GiaTrenHoaDon
        {
            set { this._GiaTrenHoaDon = value; }
            get { return this._GiaTrenHoaDon; }
        }
        public double KhoanThanhToanGianTiep
        {
            set { this._KhoanThanhToanGianTiep = value; }
            get { return this._KhoanThanhToanGianTiep; }
        }
        public double TraTruoc
        {
            set { this._TraTruoc = value; }
            get { return this._TraTruoc; }
        }
        public double TongCong1
        {
            set { this._TongCong1 = value; }
            get { return this._TongCong1; }
        }
        public double HoaHong
        {
            set { this._HoaHong = value; }
            get { return this._HoaHong; }
        }
        public double ChiPhiBaoBi
        {
            set { this._ChiPhiBaoBi = value; }
            get { return this._ChiPhiBaoBi; }
        }
        public double ChiPhiDongGoi
        {
            set { this._ChiPhiDongGoi = value; }
            get { return this._ChiPhiDongGoi; }
        }
        public double TroGiup
        {
            set { this._TroGiup = value; }
            get { return this._TroGiup; }
        }
        public double VatLieu
        {
            set { this._VatLieu = value; }
            get { return this._VatLieu; }
        }
        public double CongCu
        {
            set { this._CongCu = value; }
            get { return this._CongCu; }
        }
        public double NguyenLieu
        {
            set { this._NguyenLieu = value; }
            get { return this._NguyenLieu; }
        }
        public double ThietKe
        {
            set { this._ThietKe = value; }
            get { return this._ThietKe; }
        }
        public double BanQuyen
        {
            set { this._BanQuyen = value; }
            get { return this._BanQuyen; }
        }
        public double TienTraSuDung
        {
            set { this._TienTraSuDung = value; }
            get { return this._TienTraSuDung; }
        }
        public double ChiPhiVanChuyen
        {
            set { this._ChiPhiVanChuyen = value; }
            get { return this._ChiPhiVanChuyen; }
        }
        public double CuocPhiXepHang
        {
            set { this._CuocPhiXepHang = value; }
            get { return this._CuocPhiXepHang; }
        }
        public double CuocPhiBaoHiem
        {
            set { this._CuocPhiBaoHiem = value; }
            get { return this._CuocPhiBaoHiem; }
        }
        public double TongCong2
        {
            set { this._TongCong2 = value; }
            get { return this._TongCong2; }
        }
        public double PhiBaoHiem
        {
            set { this._PhiBaoHiem = value; }
            get { return this._PhiBaoHiem; }
        }
        public double ChiPhiPhatSinh
        {
            set { this._ChiPhiPhatSinh = value; }
            get { return this._ChiPhiPhatSinh; }
        }
        public double TienLai
        {
            set { this._TienLai = value; }
            get { return this._TienLai; }
        }
        public double TienThue
        {
            set { this._TienThue = value; }
            get { return this._TienThue; }
        }
        public double GiamGia
        {
            set { this._GiamGia = value; }
            get { return this._GiamGia; }
        }
        public double ChiPhiKhongTang
        {
            set { this._ChiPhiKhongTang = value; }
            get { return this._ChiPhiKhongTang; }
        }
        public double TriGiaNguyenTe
        {
            set { this._TriGiaNguyenTe = value; }
            get { return this._TriGiaNguyenTe; }
        }
        public double TriGiaVND
        {
            set { this._TriGiaVND = value; }
            get { return this._TriGiaVND; }
        }
        public double NgayTruyen
        {
            set { this._NgayTruyen = value; }
            get { return this._NgayTruyen; }
        }
        public long TKTG_ID
        {
            set { this._TKTG_ID = value; }
            get { return this._TKTG_ID; }
        }
        public double ChietKhau
        {
            set { this._ChietKhau = value; }
            get { return this._ChietKhau; }
        }
        public double TongCong3
        {
            set { this._TongCong3 = value; }
            get { return this._TongCong3; }
        }
        public double ChiPhiNoiDia
        {
            set { this._ChiPhiNoiDia = value; }
            get { return this._ChiPhiNoiDia; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_SXXK_HangTKTG_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaTrenHoaDon"))) this._GiaTrenHoaDon = reader.GetDouble(reader.GetOrdinal("GiaTrenHoaDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("KhoanThanhToanGianTiep"))) this._KhoanThanhToanGianTiep = reader.GetDouble(reader.GetOrdinal("KhoanThanhToanGianTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TraTruoc"))) this._TraTruoc = reader.GetDouble(reader.GetOrdinal("TraTruoc"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong1"))) this._TongCong1 = reader.GetDouble(reader.GetOrdinal("TongCong1"));
                if (!reader.IsDBNull(reader.GetOrdinal("HoaHong"))) this._HoaHong = reader.GetDouble(reader.GetOrdinal("HoaHong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiBaoBi"))) this._ChiPhiBaoBi = reader.GetDouble(reader.GetOrdinal("ChiPhiBaoBi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiDongGoi"))) this._ChiPhiDongGoi = reader.GetDouble(reader.GetOrdinal("ChiPhiDongGoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TroGiup"))) this._TroGiup = reader.GetDouble(reader.GetOrdinal("TroGiup"));
                if (!reader.IsDBNull(reader.GetOrdinal("VatLieu"))) this._VatLieu = reader.GetDouble(reader.GetOrdinal("VatLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("CongCu"))) this._CongCu = reader.GetDouble(reader.GetOrdinal("CongCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenLieu"))) this._NguyenLieu = reader.GetDouble(reader.GetOrdinal("NguyenLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThietKe"))) this._ThietKe = reader.GetDouble(reader.GetOrdinal("ThietKe"));
                if (!reader.IsDBNull(reader.GetOrdinal("BanQuyen"))) this._BanQuyen = reader.GetDouble(reader.GetOrdinal("BanQuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienTraSuDung"))) this._TienTraSuDung = reader.GetDouble(reader.GetOrdinal("TienTraSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiVanChuyen"))) this._ChiPhiVanChuyen = reader.GetDouble(reader.GetOrdinal("ChiPhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuocPhiXepHang"))) this._CuocPhiXepHang = reader.GetDouble(reader.GetOrdinal("CuocPhiXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuocPhiBaoHiem"))) this._CuocPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("CuocPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong2"))) this._TongCong2 = reader.GetDouble(reader.GetOrdinal("TongCong2"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDouble(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiPhatSinh"))) this._ChiPhiPhatSinh = reader.GetDouble(reader.GetOrdinal("ChiPhiPhatSinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienLai"))) this._TienLai = reader.GetDouble(reader.GetOrdinal("TienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) this._TienThue = reader.GetDouble(reader.GetOrdinal("TienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiamGia"))) this._GiamGia = reader.GetDouble(reader.GetOrdinal("GiamGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiKhongTang"))) this._ChiPhiKhongTang = reader.GetDouble(reader.GetOrdinal("ChiPhiKhongTang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTe"))) this._TriGiaNguyenTe = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaVND"))) this._TriGiaVND = reader.GetDouble(reader.GetOrdinal("TriGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTruyen"))) this._NgayTruyen = reader.GetDouble(reader.GetOrdinal("NgayTruyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKTG_ID"))) this._TKTG_ID = reader.GetInt64(reader.GetOrdinal("TKTG_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChietKhau"))) this._ChietKhau = reader.GetDouble(reader.GetOrdinal("ChietKhau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong3"))) this._TongCong3 = reader.GetDouble(reader.GetOrdinal("TongCong3"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiNoiDia"))) this._ChiPhiNoiDia = reader.GetDouble(reader.GetOrdinal("ChiPhiNoiDia"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public HangTriGiaCollection SelectCollectionBy_TKTG_ID()
        {
            string spName = "p_KDT_SXXK_HangTKTG_SelectBy_TKTG_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKTG_ID", SqlDbType.BigInt, this._TKTG_ID);

            HangTriGiaCollection collection = new HangTriGiaCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangTriGia entity = new HangTriGia();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaTrenHoaDon"))) entity.GiaTrenHoaDon = reader.GetDouble(reader.GetOrdinal("GiaTrenHoaDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("KhoanThanhToanGianTiep"))) entity.KhoanThanhToanGianTiep = reader.GetDouble(reader.GetOrdinal("KhoanThanhToanGianTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TraTruoc"))) entity.TraTruoc = reader.GetDouble(reader.GetOrdinal("TraTruoc"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong1"))) entity.TongCong1 = reader.GetDouble(reader.GetOrdinal("TongCong1"));
                if (!reader.IsDBNull(reader.GetOrdinal("HoaHong"))) entity.HoaHong = reader.GetDouble(reader.GetOrdinal("HoaHong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiBaoBi"))) entity.ChiPhiBaoBi = reader.GetDouble(reader.GetOrdinal("ChiPhiBaoBi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiDongGoi"))) entity.ChiPhiDongGoi = reader.GetDouble(reader.GetOrdinal("ChiPhiDongGoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TroGiup"))) entity.TroGiup = reader.GetDouble(reader.GetOrdinal("TroGiup"));
                if (!reader.IsDBNull(reader.GetOrdinal("VatLieu"))) entity.VatLieu = reader.GetDouble(reader.GetOrdinal("VatLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("CongCu"))) entity.CongCu = reader.GetDouble(reader.GetOrdinal("CongCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenLieu"))) entity.NguyenLieu = reader.GetDouble(reader.GetOrdinal("NguyenLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThietKe"))) entity.ThietKe = reader.GetDouble(reader.GetOrdinal("ThietKe"));
                if (!reader.IsDBNull(reader.GetOrdinal("BanQuyen"))) entity.BanQuyen = reader.GetDouble(reader.GetOrdinal("BanQuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienTraSuDung"))) entity.TienTraSuDung = reader.GetDouble(reader.GetOrdinal("TienTraSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiVanChuyen"))) entity.ChiPhiVanChuyen = reader.GetDouble(reader.GetOrdinal("ChiPhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuocPhiXepHang"))) entity.CuocPhiXepHang = reader.GetDouble(reader.GetOrdinal("CuocPhiXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuocPhiBaoHiem"))) entity.CuocPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("CuocPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong2"))) entity.TongCong2 = reader.GetDouble(reader.GetOrdinal("TongCong2"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDouble(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiPhatSinh"))) entity.ChiPhiPhatSinh = reader.GetDouble(reader.GetOrdinal("ChiPhiPhatSinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienLai"))) entity.TienLai = reader.GetDouble(reader.GetOrdinal("TienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) entity.TienThue = reader.GetDouble(reader.GetOrdinal("TienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiamGia"))) entity.GiamGia = reader.GetDouble(reader.GetOrdinal("GiamGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiKhongTang"))) entity.ChiPhiKhongTang = reader.GetDouble(reader.GetOrdinal("ChiPhiKhongTang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTe"))) entity.TriGiaNguyenTe = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaVND"))) entity.TriGiaVND = reader.GetDouble(reader.GetOrdinal("TriGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTruyen"))) entity.NgayTruyen = reader.GetDouble(reader.GetOrdinal("NgayTruyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKTG_ID"))) entity.TKTG_ID = reader.GetInt64(reader.GetOrdinal("TKTG_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChietKhau"))) entity.ChietKhau = reader.GetDouble(reader.GetOrdinal("ChietKhau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong3"))) entity.TongCong3 = reader.GetDouble(reader.GetOrdinal("TongCong3"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiNoiDia"))) entity.ChiPhiNoiDia = reader.GetDouble(reader.GetOrdinal("ChiPhiNoiDia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_TKTG_ID()
        {
            string spName = "p_KDT_SXXK_HangTKTG_SelectBy_TKTG_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKTG_ID", SqlDbType.BigInt, this._TKTG_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_HangTKTG_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_HangTKTG_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_HangTKTG_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_HangTKTG_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public HangTriGiaCollection SelectCollectionAll()
        {
            HangTriGiaCollection collection = new HangTriGiaCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                HangTriGia entity = new HangTriGia();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaTrenHoaDon"))) entity.GiaTrenHoaDon = reader.GetDouble(reader.GetOrdinal("GiaTrenHoaDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("KhoanThanhToanGianTiep"))) entity.KhoanThanhToanGianTiep = reader.GetDouble(reader.GetOrdinal("KhoanThanhToanGianTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TraTruoc"))) entity.TraTruoc = reader.GetDouble(reader.GetOrdinal("TraTruoc"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong1"))) entity.TongCong1 = reader.GetDouble(reader.GetOrdinal("TongCong1"));
                if (!reader.IsDBNull(reader.GetOrdinal("HoaHong"))) entity.HoaHong = reader.GetDouble(reader.GetOrdinal("HoaHong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiBaoBi"))) entity.ChiPhiBaoBi = reader.GetDouble(reader.GetOrdinal("ChiPhiBaoBi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiDongGoi"))) entity.ChiPhiDongGoi = reader.GetDouble(reader.GetOrdinal("ChiPhiDongGoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TroGiup"))) entity.TroGiup = reader.GetDouble(reader.GetOrdinal("TroGiup"));
                if (!reader.IsDBNull(reader.GetOrdinal("VatLieu"))) entity.VatLieu = reader.GetDouble(reader.GetOrdinal("VatLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("CongCu"))) entity.CongCu = reader.GetDouble(reader.GetOrdinal("CongCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenLieu"))) entity.NguyenLieu = reader.GetDouble(reader.GetOrdinal("NguyenLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThietKe"))) entity.ThietKe = reader.GetDouble(reader.GetOrdinal("ThietKe"));
                if (!reader.IsDBNull(reader.GetOrdinal("BanQuyen"))) entity.BanQuyen = reader.GetDouble(reader.GetOrdinal("BanQuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienTraSuDung"))) entity.TienTraSuDung = reader.GetDouble(reader.GetOrdinal("TienTraSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiVanChuyen"))) entity.ChiPhiVanChuyen = reader.GetDouble(reader.GetOrdinal("ChiPhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuocPhiXepHang"))) entity.CuocPhiXepHang = reader.GetDouble(reader.GetOrdinal("CuocPhiXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuocPhiBaoHiem"))) entity.CuocPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("CuocPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong2"))) entity.TongCong2 = reader.GetDouble(reader.GetOrdinal("TongCong2"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDouble(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiPhatSinh"))) entity.ChiPhiPhatSinh = reader.GetDouble(reader.GetOrdinal("ChiPhiPhatSinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienLai"))) entity.TienLai = reader.GetDouble(reader.GetOrdinal("TienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) entity.TienThue = reader.GetDouble(reader.GetOrdinal("TienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiamGia"))) entity.GiamGia = reader.GetDouble(reader.GetOrdinal("GiamGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiKhongTang"))) entity.ChiPhiKhongTang = reader.GetDouble(reader.GetOrdinal("ChiPhiKhongTang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTe"))) entity.TriGiaNguyenTe = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaVND"))) entity.TriGiaVND = reader.GetDouble(reader.GetOrdinal("TriGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTruyen"))) entity.NgayTruyen = reader.GetDouble(reader.GetOrdinal("NgayTruyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKTG_ID"))) entity.TKTG_ID = reader.GetInt64(reader.GetOrdinal("TKTG_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChietKhau"))) entity.ChietKhau = reader.GetDouble(reader.GetOrdinal("ChietKhau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong3"))) entity.TongCong3 = reader.GetDouble(reader.GetOrdinal("TongCong3"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiNoiDia"))) entity.ChiPhiNoiDia = reader.GetDouble(reader.GetOrdinal("ChiPhiNoiDia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public HangTriGiaCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            HangTriGiaCollection collection = new HangTriGiaCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                HangTriGia entity = new HangTriGia();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaTrenHoaDon"))) entity.GiaTrenHoaDon = reader.GetDouble(reader.GetOrdinal("GiaTrenHoaDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("KhoanThanhToanGianTiep"))) entity.KhoanThanhToanGianTiep = reader.GetDouble(reader.GetOrdinal("KhoanThanhToanGianTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TraTruoc"))) entity.TraTruoc = reader.GetDouble(reader.GetOrdinal("TraTruoc"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong1"))) entity.TongCong1 = reader.GetDouble(reader.GetOrdinal("TongCong1"));
                if (!reader.IsDBNull(reader.GetOrdinal("HoaHong"))) entity.HoaHong = reader.GetDouble(reader.GetOrdinal("HoaHong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiBaoBi"))) entity.ChiPhiBaoBi = reader.GetDouble(reader.GetOrdinal("ChiPhiBaoBi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiDongGoi"))) entity.ChiPhiDongGoi = reader.GetDouble(reader.GetOrdinal("ChiPhiDongGoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TroGiup"))) entity.TroGiup = reader.GetDouble(reader.GetOrdinal("TroGiup"));
                if (!reader.IsDBNull(reader.GetOrdinal("VatLieu"))) entity.VatLieu = reader.GetDouble(reader.GetOrdinal("VatLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("CongCu"))) entity.CongCu = reader.GetDouble(reader.GetOrdinal("CongCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenLieu"))) entity.NguyenLieu = reader.GetDouble(reader.GetOrdinal("NguyenLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThietKe"))) entity.ThietKe = reader.GetDouble(reader.GetOrdinal("ThietKe"));
                if (!reader.IsDBNull(reader.GetOrdinal("BanQuyen"))) entity.BanQuyen = reader.GetDouble(reader.GetOrdinal("BanQuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienTraSuDung"))) entity.TienTraSuDung = reader.GetDouble(reader.GetOrdinal("TienTraSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiVanChuyen"))) entity.ChiPhiVanChuyen = reader.GetDouble(reader.GetOrdinal("ChiPhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuocPhiXepHang"))) entity.CuocPhiXepHang = reader.GetDouble(reader.GetOrdinal("CuocPhiXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuocPhiBaoHiem"))) entity.CuocPhiBaoHiem = reader.GetDouble(reader.GetOrdinal("CuocPhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong2"))) entity.TongCong2 = reader.GetDouble(reader.GetOrdinal("TongCong2"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDouble(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiPhatSinh"))) entity.ChiPhiPhatSinh = reader.GetDouble(reader.GetOrdinal("ChiPhiPhatSinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienLai"))) entity.TienLai = reader.GetDouble(reader.GetOrdinal("TienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThue"))) entity.TienThue = reader.GetDouble(reader.GetOrdinal("TienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiamGia"))) entity.GiamGia = reader.GetDouble(reader.GetOrdinal("GiamGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiKhongTang"))) entity.ChiPhiKhongTang = reader.GetDouble(reader.GetOrdinal("ChiPhiKhongTang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNguyenTe"))) entity.TriGiaNguyenTe = reader.GetDouble(reader.GetOrdinal("TriGiaNguyenTe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaVND"))) entity.TriGiaVND = reader.GetDouble(reader.GetOrdinal("TriGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTruyen"))) entity.NgayTruyen = reader.GetDouble(reader.GetOrdinal("NgayTruyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKTG_ID"))) entity.TKTG_ID = reader.GetInt64(reader.GetOrdinal("TKTG_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChietKhau"))) entity.ChietKhau = reader.GetDouble(reader.GetOrdinal("ChietKhau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongCong3"))) entity.TongCong3 = reader.GetDouble(reader.GetOrdinal("TongCong3"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiNoiDia"))) entity.ChiPhiNoiDia = reader.GetDouble(reader.GetOrdinal("ChiPhiNoiDia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HangTKTG_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@GiaTrenHoaDon", SqlDbType.Float, this._GiaTrenHoaDon);
            this.db.AddInParameter(dbCommand, "@KhoanThanhToanGianTiep", SqlDbType.Float, this._KhoanThanhToanGianTiep);
            this.db.AddInParameter(dbCommand, "@TraTruoc", SqlDbType.Float, this._TraTruoc);
            this.db.AddInParameter(dbCommand, "@TongCong1", SqlDbType.Float, this._TongCong1);
            this.db.AddInParameter(dbCommand, "@HoaHong", SqlDbType.Float, this._HoaHong);
            this.db.AddInParameter(dbCommand, "@ChiPhiBaoBi", SqlDbType.Float, this._ChiPhiBaoBi);
            this.db.AddInParameter(dbCommand, "@ChiPhiDongGoi", SqlDbType.Float, this._ChiPhiDongGoi);
            this.db.AddInParameter(dbCommand, "@TroGiup", SqlDbType.Float, this._TroGiup);
            this.db.AddInParameter(dbCommand, "@VatLieu", SqlDbType.Float, this._VatLieu);
            this.db.AddInParameter(dbCommand, "@CongCu", SqlDbType.Float, this._CongCu);
            this.db.AddInParameter(dbCommand, "@NguyenLieu", SqlDbType.Float, this._NguyenLieu);
            this.db.AddInParameter(dbCommand, "@ThietKe", SqlDbType.Float, this._ThietKe);
            this.db.AddInParameter(dbCommand, "@BanQuyen", SqlDbType.Float, this._BanQuyen);
            this.db.AddInParameter(dbCommand, "@TienTraSuDung", SqlDbType.Float, this._TienTraSuDung);
            this.db.AddInParameter(dbCommand, "@ChiPhiVanChuyen", SqlDbType.Float, this._ChiPhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@CuocPhiXepHang", SqlDbType.Float, this._CuocPhiXepHang);
            this.db.AddInParameter(dbCommand, "@CuocPhiBaoHiem", SqlDbType.Float, this._CuocPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@TongCong2", SqlDbType.Float, this._TongCong2);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Float, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@ChiPhiPhatSinh", SqlDbType.Float, this._ChiPhiPhatSinh);
            this.db.AddInParameter(dbCommand, "@TienLai", SqlDbType.Float, this._TienLai);
            this.db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Float, this._TienThue);
            this.db.AddInParameter(dbCommand, "@GiamGia", SqlDbType.Float, this._GiamGia);
            this.db.AddInParameter(dbCommand, "@ChiPhiKhongTang", SqlDbType.Float, this._ChiPhiKhongTang);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTe", SqlDbType.Float, this._TriGiaNguyenTe);
            this.db.AddInParameter(dbCommand, "@TriGiaVND", SqlDbType.Float, this._TriGiaVND);
            this.db.AddInParameter(dbCommand, "@NgayTruyen", SqlDbType.Float, this._NgayTruyen);
            this.db.AddInParameter(dbCommand, "@TKTG_ID", SqlDbType.BigInt, this._TKTG_ID);
            this.db.AddInParameter(dbCommand, "@ChietKhau", SqlDbType.Float, this._ChietKhau);
            this.db.AddInParameter(dbCommand, "@TongCong3", SqlDbType.Float, this._TongCong3);
            this.db.AddInParameter(dbCommand, "@ChiPhiNoiDia", SqlDbType.Float, this._ChiPhiNoiDia);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(HangTriGiaCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangTriGia item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, HangTriGiaCollection collection)
        {
            foreach (HangTriGia item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HangTKTG_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@GiaTrenHoaDon", SqlDbType.Float, this._GiaTrenHoaDon);
            this.db.AddInParameter(dbCommand, "@KhoanThanhToanGianTiep", SqlDbType.Float, this._KhoanThanhToanGianTiep);
            this.db.AddInParameter(dbCommand, "@TraTruoc", SqlDbType.Float, this._TraTruoc);
            this.db.AddInParameter(dbCommand, "@TongCong1", SqlDbType.Float, this._TongCong1);
            this.db.AddInParameter(dbCommand, "@HoaHong", SqlDbType.Float, this._HoaHong);
            this.db.AddInParameter(dbCommand, "@ChiPhiBaoBi", SqlDbType.Float, this._ChiPhiBaoBi);
            this.db.AddInParameter(dbCommand, "@ChiPhiDongGoi", SqlDbType.Float, this._ChiPhiDongGoi);
            this.db.AddInParameter(dbCommand, "@TroGiup", SqlDbType.Float, this._TroGiup);
            this.db.AddInParameter(dbCommand, "@VatLieu", SqlDbType.Float, this._VatLieu);
            this.db.AddInParameter(dbCommand, "@CongCu", SqlDbType.Float, this._CongCu);
            this.db.AddInParameter(dbCommand, "@NguyenLieu", SqlDbType.Float, this._NguyenLieu);
            this.db.AddInParameter(dbCommand, "@ThietKe", SqlDbType.Float, this._ThietKe);
            this.db.AddInParameter(dbCommand, "@BanQuyen", SqlDbType.Float, this._BanQuyen);
            this.db.AddInParameter(dbCommand, "@TienTraSuDung", SqlDbType.Float, this._TienTraSuDung);
            this.db.AddInParameter(dbCommand, "@ChiPhiVanChuyen", SqlDbType.Float, this._ChiPhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@CuocPhiXepHang", SqlDbType.Float, this._CuocPhiXepHang);
            this.db.AddInParameter(dbCommand, "@CuocPhiBaoHiem", SqlDbType.Float, this._CuocPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@TongCong2", SqlDbType.Float, this._TongCong2);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Float, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@ChiPhiPhatSinh", SqlDbType.Float, this._ChiPhiPhatSinh);
            this.db.AddInParameter(dbCommand, "@TienLai", SqlDbType.Float, this._TienLai);
            this.db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Float, this._TienThue);
            this.db.AddInParameter(dbCommand, "@GiamGia", SqlDbType.Float, this._GiamGia);
            this.db.AddInParameter(dbCommand, "@ChiPhiKhongTang", SqlDbType.Float, this._ChiPhiKhongTang);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTe", SqlDbType.Float, this._TriGiaNguyenTe);
            this.db.AddInParameter(dbCommand, "@TriGiaVND", SqlDbType.Float, this._TriGiaVND);
            this.db.AddInParameter(dbCommand, "@NgayTruyen", SqlDbType.Float, this._NgayTruyen);
            this.db.AddInParameter(dbCommand, "@TKTG_ID", SqlDbType.BigInt, this._TKTG_ID);
            this.db.AddInParameter(dbCommand, "@ChietKhau", SqlDbType.Float, this._ChietKhau);
            this.db.AddInParameter(dbCommand, "@TongCong3", SqlDbType.Float, this._TongCong3);
            this.db.AddInParameter(dbCommand, "@ChiPhiNoiDia", SqlDbType.Float, this._ChiPhiNoiDia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(HangTriGiaCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangTriGia item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HangTKTG_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@GiaTrenHoaDon", SqlDbType.Float, this._GiaTrenHoaDon);
            this.db.AddInParameter(dbCommand, "@KhoanThanhToanGianTiep", SqlDbType.Float, this._KhoanThanhToanGianTiep);
            this.db.AddInParameter(dbCommand, "@TraTruoc", SqlDbType.Float, this._TraTruoc);
            this.db.AddInParameter(dbCommand, "@TongCong1", SqlDbType.Float, this._TongCong1);
            this.db.AddInParameter(dbCommand, "@HoaHong", SqlDbType.Float, this._HoaHong);
            this.db.AddInParameter(dbCommand, "@ChiPhiBaoBi", SqlDbType.Float, this._ChiPhiBaoBi);
            this.db.AddInParameter(dbCommand, "@ChiPhiDongGoi", SqlDbType.Float, this._ChiPhiDongGoi);
            this.db.AddInParameter(dbCommand, "@TroGiup", SqlDbType.Float, this._TroGiup);
            this.db.AddInParameter(dbCommand, "@VatLieu", SqlDbType.Float, this._VatLieu);
            this.db.AddInParameter(dbCommand, "@CongCu", SqlDbType.Float, this._CongCu);
            this.db.AddInParameter(dbCommand, "@NguyenLieu", SqlDbType.Float, this._NguyenLieu);
            this.db.AddInParameter(dbCommand, "@ThietKe", SqlDbType.Float, this._ThietKe);
            this.db.AddInParameter(dbCommand, "@BanQuyen", SqlDbType.Float, this._BanQuyen);
            this.db.AddInParameter(dbCommand, "@TienTraSuDung", SqlDbType.Float, this._TienTraSuDung);
            this.db.AddInParameter(dbCommand, "@ChiPhiVanChuyen", SqlDbType.Float, this._ChiPhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@CuocPhiXepHang", SqlDbType.Float, this._CuocPhiXepHang);
            this.db.AddInParameter(dbCommand, "@CuocPhiBaoHiem", SqlDbType.Float, this._CuocPhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@TongCong2", SqlDbType.Float, this._TongCong2);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Float, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@ChiPhiPhatSinh", SqlDbType.Float, this._ChiPhiPhatSinh);
            this.db.AddInParameter(dbCommand, "@TienLai", SqlDbType.Float, this._TienLai);
            this.db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Float, this._TienThue);
            this.db.AddInParameter(dbCommand, "@GiamGia", SqlDbType.Float, this._GiamGia);
            this.db.AddInParameter(dbCommand, "@ChiPhiKhongTang", SqlDbType.Float, this._ChiPhiKhongTang);
            this.db.AddInParameter(dbCommand, "@TriGiaNguyenTe", SqlDbType.Float, this._TriGiaNguyenTe);
            this.db.AddInParameter(dbCommand, "@TriGiaVND", SqlDbType.Float, this._TriGiaVND);
            this.db.AddInParameter(dbCommand, "@NgayTruyen", SqlDbType.Float, this._NgayTruyen);
            this.db.AddInParameter(dbCommand, "@TKTG_ID", SqlDbType.BigInt, this._TKTG_ID);
            this.db.AddInParameter(dbCommand, "@ChietKhau", SqlDbType.Float, this._ChietKhau);
            this.db.AddInParameter(dbCommand, "@TongCong3", SqlDbType.Float, this._TongCong3);
            this.db.AddInParameter(dbCommand, "@ChiPhiNoiDia", SqlDbType.Float, this._ChiPhiNoiDia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(HangTriGiaCollection collection, SqlTransaction transaction)
        {
            foreach (HangTriGia item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HangTKTG_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(HangTriGiaCollection collection, SqlTransaction transaction)
        {
            foreach (HangTriGia item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(HangTriGiaCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangTriGia item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleteBy_TKTG_ID()
        {
            string spName = "p_KDT_SXXK_HangTKTG_DeleteBy_TKTG_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKTG_ID", SqlDbType.BigInt, this._TKTG_ID);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_TKTG_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_HangTKTG_DeleteBy_TKTG_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKTG_ID", SqlDbType.BigInt, this._TKTG_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        #endregion
    }
}