
/****** Object:  StoredProcedure [dbo].[p_NPLCungUngDaDangKy]    Script Date: 10/08/2012 10:01:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Huynh Ngoc Khanh>  
-- Create date: <Create Date,,01/09/2012>  
-- Description: <Description,,Theo doi NPL cung ung>  
-- =============================================  
ALTER PROCEDURE [dbo].[p_NPLCungUngDaDangKy]  
@HopDongID int
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 -- p_NPLTuCungUng '0400101556',23,676  
 SET NOCOUNT ON;   
  
--Declare @MaDoanhNghiep varchar(50);  
--set @MaDoanhNghiep='0100101308'  
SELECT Ma,MAX(TenNPL)AS TenNPL,SUM(LuongCung)LuongCung,MAX(DonViTinh) AS DonViTinh,STK,MAX(TKMD_ID) AS TKMD_ID ,MAX(TKCT_ID)AS TKCT_ID
FROM
(SELECT     t_KDT_GC_NguyenPhuLieu.Ma, t_KDT_GC_NguyenPhuLieu.Ten AS TenNPL, t_KDT_GC_NguyenPhuLieu.MaHS, 
                       t_View_KDT_NPLCungUng.LuongCung,
                          (SELECT     Ten
                            FROM          t_HaiQuan_DonViTinh
                            WHERE      (ID = t_KDT_GC_NguyenPhuLieu.DVT_ID)) AS DonViTinh, 
                           CASE WHEN t_View_KDT_NPLCungUng.TKMD_ID > 0 THEN
									(SELECT   
										CASE	WHEN  t_KDT_ToKhaiMauDich.SoToKhai > 0 THEN
												CONVERT(VARCHAR(7), SoToKhai) + '/' + MaLoaiHinh + '/' + CONVERT(VARCHAR(10), NamDK)
												ELSE
												N'Tờ khai chưa được cấp số (ID = '+CONVERT(VARCHAR(6),t_KDT_ToKhaiMauDich.ID)+')'
												END
									FROM    dbo.t_KDT_ToKhaiMauDich
									WHERE      ID = t_View_KDT_NPLCungUng.TKMD_ID) 
							     WHEN	t_View_KDT_NPLCungUng.TKMD_ID = 0 AND t_View_KDT_NPLCungUng.TKCT_ID > 0 THEN
									(SELECT     
										CASE	WHEN tkct.SoToKhai > 0 THEN
												CONVERT(VARCHAR(7), tkct.SoToKhai) + '/' + tkct.MaLoaiHinh + '/' + CONVERT(VARCHAR(4), tkct.NamDK)
												ELSE
												N'Tờ khai chuyển tiếp xuất chưa được cấp số (ID = '+CONVERT(VARCHAR(6),tkct.ID)+')'
												END
									FROM          dbo.t_KDT_GC_ToKhaiChuyenTiep tkct
									WHERE      tkct.ID = t_View_KDT_NPLCungUng.TKCT_ID)
                            END AS STK,
                             t_View_KDT_NPLCungUng.TKMD_ID AS TKMD_ID, t_View_KDT_NPLCungUng.TKCT_ID AS TKCT_ID
FROM         t_KDT_GC_NguyenPhuLieu INNER JOIN
                      t_View_KDT_NPLCungUng ON t_KDT_GC_NguyenPhuLieu.Ma = t_View_KDT_NPLCungUng.MaNguyenPhuLieu
WHERE     (t_KDT_GC_NguyenPhuLieu.HopDong_ID = @HopDongID )) AS t_view_NPLCungUng
GROUP BY STK,Ma
ORDER BY Ma


END  

GO

if( (select count(*) from dbo.t_HaiQuan_Version where [Version] ='5.1') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('5.1', getdate(), N'Cập nhật dữ liệu view định mức đăng ký')
	end 