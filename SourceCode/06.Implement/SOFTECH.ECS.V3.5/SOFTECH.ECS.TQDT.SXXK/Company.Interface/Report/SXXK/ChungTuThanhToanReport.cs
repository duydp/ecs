using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SXXK;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu.CTTT;

namespace Company.Interface.Report.SXXK
{
    public partial class ChungTuThanhToanReport : DevExpress.XtraReports.UI.XtraReport
    {
        public int SoHSTK;
        public int LanThanhLy;
        public ChungTuThanhToanCollection CTCollection = new ChungTuThanhToanCollection();
        private int STT = 0;
        private string ToKhaiXuat = "";
        private DateTime NgayDangKyXuat = new DateTime(1900, 1, 1);
        public DataTable Data_Source = new DataTable();
        
        public ChungTuThanhToanReport()
        {
            InitializeComponent();
        }
        public void BindReport(string loaiChungTu, bool InTriGiaHd)
        {
            string strLoaiCT = string.Empty;
            if (loaiChungTu == enumLoaiChungTuTT.Nhap)
                strLoaiCT = " AND (ToKhaiXuat like '%E31%' or ToKhaiXuat like '%/N%')";
            else
                strLoaiCT = " AND (ToKhaiXuat like '%E62%' or  ToKhaiXuat like '%B13%' or ToKhaiXuat like '%/X%')";
            DataSet dsSource = new ChungTuThanhToan().SelectDynamic(" LanThanhLy = " + LanThanhLy + strLoaiCT, " NgayDangKyXuat asc,ToKhaiXuat asc, SoNgayChungTu asc");

            this.DataSource = dsSource.Tables[0]; //this.CTCollection;

            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (SoHSTK > 0)
                lblSHSTK.Text = SoHSTK + "";
            if (loaiChungTu == enumLoaiChungTuTT.Nhap)
            {
                lblLoaiHopDong.Text = "Hợp đồng nhập khẩu";
                lblLoaiMaHang.Text = "Mã hàng nhập";
                lblloaiTriGia.Text = "Trị giá hàng thực nhập khẩu";
            }
            lblToKhaiXuat.DataBindings.Add("Text", this.DataSource, "ToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, "NgayDangKyXuat", "{0:dd/MM/yy}");
            lblSoNgayHopDong.DataBindings.Add("Text", this.DataSource, "SoNgayHopDong");
            lblMaHangXuat.DataBindings.Add("Text", this.DataSource, "MaHangXuat");
            lblTriGiaHopDong.DataBindings.Add("Text", this.DataSource, "TriGiaHopDong", "{0:N2}");
            lblTriGiaHangThucXuat.DataBindings.Add("Text", this.DataSource, "TriGiaHangThucXuat", "{0:N2}");
            lblSoNgayChungTu.DataBindings.Add("Text", this.DataSource, "SoNgayChungTu");
            lblTriGiaCT.DataBindings.Add("Text", this.DataSource, "TriGiaCT", "{0:N2}");
            lblHinhThucThanhToan.DataBindings.Add("Text", this.DataSource, "HinhThucThanhToan");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");
            xrLabel76.Text = xrLabel30.Text = GlobalSettings.TieudeNgay;
        }
        private DataTable TaoDuLieu(ChungTuThanhToanCollection CTCollection)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("ToKhaiXuat", typeof(string));
                dt.Columns.Add("NgayDangKyXuat", typeof(string));
                dt.Columns.Add("SoNgayHopDong", typeof(string));
                dt.Columns.Add("MaHangXuat", typeof(string));
                dt.Columns.Add("TriGiaHopDong", typeof(string));
                dt.Columns.Add("TriGiaHangThucXuat", typeof(string));
                dt.Columns.Add("SoNgayChungTu", typeof(string));
                dt.Columns.Add("TriGiaCT", typeof(string));
                dt.Columns.Add("HinhThucThanhToan", typeof(string));
                dt.Columns.Add("GhiChu", typeof(string));

                foreach (ChungTuThanhToan item in CTCollection)
                {
                    DataRow dr = dt.NewRow();
                    if (dt.Rows.Count > 0)
                    {
                        DataRow[] drTrungToKhai;
                        drTrungToKhai = dt.Select("ToKhaiXuat = '" + item.ToKhaiXuat + "'");
                        if (drTrungToKhai.Length > 0)
                        {
                            dr["SoNgayChungTu"] = dr["SoNgayChungTu"].ToString() + "\r\n" + item.SoNgayChungTu;
                            continue;
                        }

                    }
                    dr["ToKhaiXuat"] = item.ToKhaiXuat;
                    dr["NgayDangKyXuat"] = item.NgayDangKyXuat;
                    dr["SoNgayHopDong"] = item.SoNgayHopDong;
                    dr["MaHangXuat"] = item.MaHangXuat;
                    dr["TriGiaHopDong"] = item.TriGiaHopDong;
                    dr["TriGiaHangThucXuat"] = item.TriGiaHangThucXuat;
                    dr["SoNgayChungTu"] = item.SoNgayChungTu;
                    dr["TriGiaCT"] = item.TriGiaCT;
                    dr["HinhThucThanhToan"] = item.HinhThucThanhToan;
                    dr["GhiChu"] = item.GhiChu;
                    dt.Rows.Add(dr);
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return dt;
        }
        private bool KiemTraTrung()
        {
            if (GetCurrentColumnValue("ToKhaiXuat") != null)
            {
                if (ToKhaiXuat == GetCurrentColumnValue("ToKhaiXuat").ToString() && NgayDangKyXuat == Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyXuat"))) return true;

                else return false;
            }
            else
                return false;
        }
        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (!KiemTraTrung())
            {
                STT++;
                lblSTT.Text = STT + "";
                XRControl control = (XRControl)sender;
                control.Borders = DevExpress.XtraPrinting.BorderSide.Left |
        DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
            }
            else
            {
                lblSTT.Text = "";
                XRControl control = (XRControl)sender;
                control.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom; 
            }
        }
        private void lblToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (KiemTraTrung())
            {
                XRControl control = (XRControl)sender;
                control.Text = "";
                control.Borders = DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom; 
            }
            else
            {
                XRControl control = (XRControl)sender;
                control.Borders = DevExpress.XtraPrinting.BorderSide.Left |
       DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom;
            }
        }
        private void Detail_AfterPrint(object sender, EventArgs e)
        {
            ToKhaiXuat = GetCurrentColumnValue("ToKhaiXuat").ToString();
            NgayDangKyXuat = Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyXuat"));
        }

    }
}
