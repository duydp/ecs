﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.Messages.Send;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class OLA : BasicVNACC
    {
        public List<OLA_HANG> listCont { get; set; }

        public StringBuilder BuilEdiMessagesOLA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<OLA>(StrBuild, true, GlobalVNACC.PathConfig, EnumNghiepVu.OLA);
            foreach (OLA_HANG item in listCont)
            {
                item.BuildEdiMessages<OLA_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "OLA_HANG");
            }
            return str;
        }

    }
}
