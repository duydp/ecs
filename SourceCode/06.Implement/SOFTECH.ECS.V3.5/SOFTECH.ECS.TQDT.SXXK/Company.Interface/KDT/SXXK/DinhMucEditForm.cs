﻿using System;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.SXXK;
using NguyenPhuLieu = Company.BLL.SXXK.NguyenPhuLieu;
using SanPham = Company.BLL.SXXK.SanPham;
using Janus.Windows.GridEX;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.BLL.SXXK.ToKhai;
using Company.QuanTri;
using Company.Interface.KDT.GC;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucEditForm : BaseForm
    {
        ToKhaiMauDichCollection TKNColl = new ToKhaiMauDichCollection();

        public DinhMuc DMDetail = new DinhMuc();
        public DinhMucDangKy dmDangKy;
        public string maSP = "";
        SelectSanPhamDMForm fsp = new SelectSanPhamDMForm();
        SelectNguyenPhuLieuDMForm fnpl = new SelectNguyenPhuLieuDMForm();
        List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = new List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu>();
        List<Company.BLL.KDT.SXXK.SXXK_SanPham> SPCollection = new List<Company.BLL.KDT.SXXK.SXXK_SanPham>();
        List<KDT_LenhSanXuat> LenhSanXuatCollection = new List<KDT_LenhSanXuat>();
        List<KDT_LenhSanXuat> LenhSanXuatSPCollection = new List<KDT_LenhSanXuat>();
        public bool isEdit = false;
        public bool isCancel = false;
        public LogHistory Log = new LogHistory();
        public DinhMucEditForm()
        {
            InitializeComponent();
            fsp.status = true;
            fnpl.status = true;
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {

        }
        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            //try
            //{
            //    if (fsp == null)
            //    {
            //        fsp = new SelectSanPhamDMForm();
            //        fsp.status = true;
            //    }
            //    fsp.CalledForm = this.Name;
            //    fsp.ShowDialog(this);

            //    if (fsp.SanPhamSelected.Ma.Length > 0)
            //    {

            //        txtMaSP.Text = fsp.SanPhamSelected.Ma.Trim();
            //        txtTenSP.Text = fsp.SanPhamSelected.Ten.Trim();
            //        txtMaHSSP.Text = fsp.SanPhamSelected.MaHS.Trim();
            //        txtDonViTinhSP.Text = DonViTinh.GetName(fsp.SanPhamSelected.DVT_ID);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            //try
            //{
            //    fnpl.CalledForm = this.Name;
            //    fnpl.ShowDialog(this);
            //    if (fnpl.NguyenPhuLieuSelected.Ma.Length > 0)
            //    {

            //        txtMaNPL.Text = fnpl.NguyenPhuLieuSelected.Ma.Trim();
            //        txtTenNPL.Text = fnpl.NguyenPhuLieuSelected.Ten.Trim();
            //        txtMaHSNPL.Text = fnpl.NguyenPhuLieuSelected.MaHS.Trim();
            //        txtDonViTinhNPL.Text = DonViTinh.GetName(fnpl.NguyenPhuLieuSelected.DVT_ID);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
            //if (fnpl == null)
            //{
            //    fnpl = new SelectNguyenPhuLieuDMForm();
            //    fnpl.status = true;
            //}
        }
        private void SetCommandStatus()
        {
            if (this.OpenType == OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = true;
                btnDelete.Enabled = true;
            }
        }
        private void LoadDataDefault()
        {
            try
            {
                NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                cbbMaNPL.DataSource = NPLCollection;
                cbbMaNPL.DisplayMember = "Ma";
                cbbMaNPL.ValueMember = "Ma";

                SPCollection = Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                cbbMaSP.DataSource = SPCollection;
                cbbMaSP.DisplayMember = "Ma";
                cbbMaSP.ValueMember = "Ma";

                LenhSanXuatCollection = KDT_LenhSanXuat.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void DinhMucEditForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            txtDinhMuc.DecimalDigits = GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["DinhMucChung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;

            txtTyLeHH.DecimalDigits = GlobalSettings.SoThapPhan.TLHH;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            LoadDataDefault();

            //SanPham spSXXK = new SanPham();
            //spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //spSXXK.Ma = DMDetail.MaSanPham;
            //if (spSXXK.Load())
            //{                
            //    cbbMaSP.Value = spSXXK.Ma.Trim();
            //    txtTenSP.Text = spSXXK.Ten.Trim();
            //    txtMaHSSP.Text = spSXXK.MaHS;
            //    txtDonViTinhSP.Text = DonViTinh.GetName(spSXXK.DVT_ID);
            //}

            //NguyenPhuLieu nplSXXK = new NguyenPhuLieu();
            //nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //nplSXXK.Ma = DMDetail.MaNguyenPhuLieu;
            //if (nplSXXK.Load())
            //{                
            //    cbbMaNPL.Value = nplSXXK.Ma.Trim();
            //    txtTenNPL.Text = nplSXXK.Ten.Trim();
            //    txtMaHSNPL.Text = nplSXXK.MaHS;
            //    txtDonViTinhNPL.Text = DonViTinh.GetName(nplSXXK.DVT_ID);
            //}
            cbbMaSP.Value = DMDetail.MaSanPham;
            cbbMaNPL.Value = DMDetail.MaNguyenPhuLieu;
            cbbLenhSanXuat.Value = DMDetail.MaDinhDanhLenhSX;
            cbbMaSP_ValueChanged(null, null);
            cbbMaNPL_ValueChanged(null, null);

            txtDinhMuc.Value = DMDetail.DinhMucSuDung;
            txtTyLeHH.Value = DMDetail.TyLeHaoHut;
            txtGhiChu.Text = DMDetail.GhiChu;
            BindData();
            if (this.OpenType == OpenFormType.Edit)
            {
                btnDelete.Enabled = true;
                btnAdd.Enabled = true;
                btnCopy.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            else
            {
                btnDelete.Enabled = false;
                btnAdd.Enabled = false;
                btnCopy.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            SetCommandStatus();
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            //try
            //{
            //    SanPham spSXXK = new SanPham();
            //    spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    spSXXK.Ma = txtMaSP.Text;
            //    if (spSXXK.Load())
            //    {
            //        error.SetError(txtMaNPL, string.Empty);

            //        txtMaSP.Text = spSXXK.Ma.Trim();
            //        txtTenSP.Text = spSXXK.Ten.Trim();
            //        txtMaHSSP.Text = spSXXK.MaHS;
            //        txtDonViTinhSP.Text = DonViTinh.GetName(spSXXK.DVT_ID);
            //        error.SetError(txtMaSP, string.Empty);
            //    }
            //    else
            //    {
            //        error.SetIconPadding(txtMaSP, -8);
            //        if (GlobalSettings.NGON_NGU == "0")
            //        {
            //            error.SetError(txtMaSP, "KHÔNG TỒN TẠI SẢN PHẨM NÀY.");
            //        }
            //        else
            //        {
            //            error.SetError(txtMaSP, "THIS PRODUCT DOES NOT EXIST.");
            //        }

            //        txtTenSP.Text = txtMaHSSP.Text = txtDonViTinhSP.Text = string.Empty;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkDM(string masp, string manpl)
        {
            foreach (DinhMuc dm in dmDangKy.DMCollection)
            {
                if (dm.MaSanPham.Trim() == cbbMaSP.Value.ToString().Trim() && dm.MaNguyenPhuLieu.Trim() == cbbMaNPL.Value.ToString().Trim())
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY ĐÃ ĐƯỢC KHAI BÁO TRÊN LƯỚI.", false);
                    if (DMDetail.MaSanPham.Trim() != "")
                        this.dmDangKy.DMCollection.Add(DMDetail);
                    return true;
                }
            }
            return false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (cbbLenhSanXuat.Value == null)
            {
                errorProvider.SetError(cbbLenhSanXuat, "Mã định danh của Lệnh sản xuất không được để trống");
                return;
            }
            //SanPham spSXXK = new SanPham();
            //spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //spSXXK.Ma = cbbMaSP.Value.ToString();
            //if (!spSXXK.Load())
            //{
            //    error.SetIconPadding(cbbMaSP, -8);
            //    if (GlobalSettings.NGON_NGU == "0")
            //    {
            //        error.SetError(cbbMaSP, "KHÔNG TỒN TẠI SẢN PHẨM NÀY.");
            //    }
            //    else
            //    {
            //        error.SetError(cbbMaSP, "THIS PRODUCT HAVEN'T EXIST.");
            //    }

            //    txtTenSP.Text = txtMaHSSP.Text = txtDonViTinhSP.Text = string.Empty;
            //    return;
            //}
            //NguyenPhuLieu nplSXXK = new NguyenPhuLieu();
            //nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //nplSXXK.Ma = cbbMaNPL.Value.ToString();
            //if (!nplSXXK.Load())
            //{
            //    error.SetIconPadding(cbbMaNPL, -8);
            //    if (GlobalSettings.NGON_NGU == "0")
            //    {
            //        error.SetError(cbbMaNPL, "KHÔNG TỒN TẠI NGUYÊN PHỤ LIỆU NÀY.");
            //    }
            //    else
            //    {
            //        error.SetError(cbbMaNPL, "THIS MATERIAL HAVEN'T EXIST.");
            //    }
            //    txtTenNPL.Text = txtMaHSNPL.Text = txtDonViTinhNPL.Text = string.Empty;
            //}

            if (Convert.ToDecimal(txtDinhMuc.Text) <= 0)
            {
                error.SetIconPadding(txtDinhMuc, -8);
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtDinhMuc, "ĐỊNH MỨC PHẢI LỚN HƠN 0.");
                }
                else
                {
                    error.SetError(txtDinhMuc, "THE NORM MUST BE GREATER THAN ZERO.");
                }
                return;
            }
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                //long id = DinhMuc.GetIDDinhMucExit(cbbMaSP.Value.ToString().Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                long id = DinhMuc.GetIDDinhMucExit(cbbMaSP.Value.ToString().Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, cbbLenhSanXuat.Value.ToString());
                if (id > 0)
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY ĐÃ ĐƯỢC KHAI BÁO TRONG DANH SÁCH ĐỊNH MỨC KHÁC CÓ ID = " + id, false);
                    return;
                }
            }

            Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
            ttdm.MaHaiQuan = dmDangKy.MaHaiQuan;
            ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            ttdm.MaSanPham = cbbMaSP.Value.ToString().Trim();
            ttdm.LenhSanXuat_ID = Convert.ToInt32(KDT_LenhSanXuat.GetID(cbbLenhSanXuat.Value.ToString()));
            if (ttdm.Load())
            {
                this.dmDangKy.DMCollection.Remove(DMDetail);
                foreach (DinhMuc dm in dmDangKy.DMCollection)
                {
                    if (dm.MaSanPham == cbbMaSP.Value.ToString().Trim() && dm.MaNguyenPhuLieu == cbbMaNPL.Value.ToString().Trim())
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY ĐÃ ĐƯỢC KHAI BÁO TRÊN LƯỚI.", false);
                        if (DMDetail.MaSanPham.Trim() != "")
                            this.dmDangKy.DMCollection.Add(DMDetail);
                        return;
                    }
                }
            }
            this.dmDangKy.DMCollection.Remove(DMDetail);
            foreach (DinhMuc dm in dmDangKy.DMCollection)
            {
                if (dm.MaSanPham == cbbMaSP.Value.ToString().Trim() && dm.MaNguyenPhuLieu == cbbMaNPL.Value.ToString().Trim())
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY ĐÃ ĐƯỢC KHAI BÁO TRÊN LƯỚI.", false);
                    if (DMDetail.MaSanPham.Trim() != "")
                        this.dmDangKy.DMCollection.Add(DMDetail);
                    return;
                }
            }

            //foreach (KDT_LenhSanXuat item in LenhSanXuatCollection)
            //{
            //    bool isExits = false;
            //    item.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
            //    foreach (KDT_LenhSanXuat_SP ite in item.SPCollection)
            //    {
            //        if (ite.MaSanPham == cbbMaSP.Value.ToString())
            //        {
            //            cbbLenhSanXuat.Value = item.SoLenhSanXuat.ToString();
            //            isExits = true;
            //            break;
            //        }
            //    }
            //    if (!isExits)
            //    {
            //        errorProvider.SetError(cbbLenhSanXuat, "Mã định danh của Lệnh sản xuất  này chưa được đăng ký hoặc cập nhật");
            //        return;
            //    }
            //}
            if (DMDetail == null || DMDetail.ID == 0)
                this.DMDetail = new DinhMuc();
            this.DMDetail.MaSanPham = cbbMaSP.Value.ToString().Trim();
            this.DMDetail.MaNguyenPhuLieu = cbbMaNPL.Value.ToString().Trim();
            this.DMDetail.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Value);
            this.DMDetail.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Value);
            this.DMDetail.DVT_ID = this.DonViTinh_GetID(txtDonViTinhNPL.Text.Trim());
            this.DMDetail.TenNPL = txtTenNPL.Text;
            this.DMDetail.MaDinhDanhLenhSX = cbbLenhSanXuat.Value.ToString();
            this.DMDetail.GhiChu = txtGhiChu.Text;
            this.dmDangKy.DMCollection.Add(DMDetail);
            BindData();
            cbbMaNPL.Text = "";
            txtMaHSNPL.Text = "";
            txtTenNPL.Text = "";
            txtDinhMuc.Value = 0;
            txtTyLeHH.Value = 0;

            this.DMDetail = new DinhMuc();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "DOANH NGHIỆP CÓ MUỐN XÓA ĐỊNH MỨC NÀY KHÔNG?", true) == "Yes")
                {
                    DinhMucCollection dmcoll = new DinhMucCollection();
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
                            // Thực hiện xóa trong CSDL.
                            if (dmSelected.ID > 0)
                            {
                                dmSelected.Delete();
                            }
                            dmDangKy.DMCollection.Remove(dmSelected);
                            dmcoll.Add(dmSelected);
                        }
                    }
                    BindData();
                    ShowMessage("XÓA THÀNH CÔNG", false);
                    Log.LogDinhMuc(dmDangKy, dmcoll, MessageTitle.XoaDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DMDetail = (DinhMuc)e.Row.DataRow;
                    cbbMaSP.Value = DMDetail.MaSanPham.Trim();
                    cbbMaNPL.Value = DMDetail.MaNguyenPhuLieu.Trim();
                    txtDinhMuc.Text = DMDetail.DinhMucSuDung.ToString();
                    txtTyLeHH.Text = DMDetail.TyLeHaoHut.ToString();
                    cbbLenhSanXuat.Value = DMDetail.MaDinhDanhLenhSX;
                    txtMaSP_Leave(null, null);
                    txtMaNPL_Leave(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            try
            {
                #region Code Old
                //Company.Interface.Controls.ScreenShoot.CaptureImage("Test Bug");
                //if (cbbMaSP.Value.ToString().Trim() == "")
                //{
                //    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "DOANH NGHIỆP PHẢI CHỌN MÃ SẢN PHẨM CẦN KHAI BÁO ĐỊNH MỨC TRƯỚC.", false);
                //    return;
                //}
                //if (DinhMuc.checkDinhMucExit(cbbMaSP.Value.ToString().Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI))
                //{
                //    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY ĐÃ ĐƯỢC KHAI BÁO TRONG DANH SÁCH ĐỊNH MỨC KHÁC.", false);
                //    return;
                //}
                //Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                //ttdm.MaHaiQuan = dmDangKy.MaHaiQuan;
                //ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //ttdm.MaSanPham = cbbMaSP.Value.ToString().Trim();
                //if (ttdm.Load())
                //{
                //    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "SẢN PHẨM NÀY ĐÃ ĐƯỢC ĐĂNG KÝ ĐỊNH MỨC.", false);
                //    return;
                //}
                //SaoChepDinhMucForm f = new SaoChepDinhMucForm();
                //f.dmdk = dmDangKy;
                //f.MaSP = cbbMaSP.Value.ToString().Trim();
                //f.ShowDialog(this);
                //{
                //    foreach (BLL.SXXK.DinhMuc dmSaoChep in f.collectionCopy)
                //    {
                //        DinhMuc dm = new DinhMuc();
                //        dm.MaSanPham = cbbMaSP.Value.ToString().Trim();
                //        dm.MaNguyenPhuLieu = dmSaoChep.MaNguyenPhuLieu;
                //        dm.DinhMucSuDung = dmSaoChep.DinhMucSuDung;
                //        dm.TyLeHaoHut = dmSaoChep.TyLeHaoHut;
                //        NguyenPhuLieu NPL = new NguyenPhuLieu();
                //        NPL.Ma = dm.MaNguyenPhuLieu;
                //        NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //        NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                //        NPL.Load();
                //        dm.TenNPL = NPL.Ten;
                //        dmDangKy.DMCollection.Add(dm);
                //    }

                //}
                //BindData();
                #endregion

                if (cbbMaSP.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN SẢN PHẨM ĐỂ SAO CHÉP ĐỊNH MỨC", false);
                    return;
                }
                if (cbbLenhSanXuat.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT CỦA SẢN PHẨM .", false);
                    return;
                }
                if (!isEdit)
                {
                    if (DinhMuc.checkDinhMucExit(cbbMaSP.Value.ToString().Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI))
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY ĐÃ ĐƯỢC KHAI BÁO TRONG DANH SÁCH ĐỊNH MỨC KHÁC.", false);
                        return;
                    }
                    Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                    ttdm.MaHaiQuan = dmDangKy.MaHaiQuan;
                    ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    ttdm.MaSanPham = cbbMaSP.Value.ToString().Trim();
                    ttdm.LenhSanXuat_ID = Convert.ToInt32(KDT_LenhSanXuat.GetID(cbbLenhSanXuat.Value.ToString()));
                    if (ttdm.Load())
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "SẢN PHẨM NÀY ĐÃ ĐƯỢC ĐĂNG KÝ ĐỊNH MỨC.", false);
                        return;
                    }
                }
                List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                List<Company.BLL.KDT.SXXK.SXXK_SanPham> SPCollection = Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                CoppyDinhMucForm f = new CoppyDinhMucForm();
                f.MaSanPham = cbbMaSP.Value.ToString();
                f.dmDangKy = dmDangKy;
                f.ShowDialog(this);
                if (f.DMCollectionCopy.Count > 0)
                {
                    foreach (DinhMuc item in f.DMCollectionCopy)
                    {
                        DinhMuc DM = new DinhMuc();
                        DM.MaSanPham = item.MaSanPham;
                        foreach (Company.BLL.KDT.SXXK.SXXK_SanPham ite in SPCollection)
                        {
                            if (ite.Ma == item.MaSanPham)
                            {
                                DM.TenSP = ite.Ten.ToString();
                                break;
                            }
                        }
                        DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu.ToString();
                        foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu ite in NPLCollection)
                        {
                            if (ite.Ma == item.MaNguyenPhuLieu)
                            {
                                DM.TenNPL = ite.Ten.ToString();
                                break;
                            }
                        }
                        DM.DinhMucSuDung = item.DinhMucSuDung;
                        DM.TyLeHaoHut = item.TyLeHaoHut;
                        DM.MaDinhDanhLenhSX = cbbLenhSanXuat.Value.ToString();
                        DM.GhiChu = item.GhiChu;
                        dmDangKy.DMCollection.Add(DM);
                    }
                    ShowMessageTQDT("Sao chép thành công", false);
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCopyMenu_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRow().RowType == RowType.GroupHeader)
                {
                    SaoChepDinhMucMenuForm f = new SaoChepDinhMucMenuForm();
                    f.DMDangKy = dmDangKy;
                    DinhMuc dmSelect = (DinhMuc)dgList.GetRow().GetChildRows()[0].DataRow;
                    f.MaSanPhamCopy = dmSelect.MaSanPham;
                    f.ShowDialog(this);
                    foreach (DinhMuc dm in f.collectionCopy)
                    {
                        DinhMuc dmCopy = new DinhMuc();
                        dmCopy.MaSanPham = dm.MaSanPham;
                        dmCopy.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
                        dmCopy.DinhMucSuDung = dm.DinhMucSuDung;
                        dmCopy.TyLeHaoHut = dm.TyLeHaoHut;
                        NguyenPhuLieu NPL = new NguyenPhuLieu();
                        NPL.Ma = dmCopy.MaNguyenPhuLieu;
                        NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        NPL.Load();
                        dmCopy.TenNPL = NPL.Ten;
                        dmDangKy.DMCollection.Add(dmCopy);
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                //dgList.Refresh();
                dgList.DataSource = dmDangKy.DMCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DinhMucCollection DMCollectionTMP = new DinhMucCollection();
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "DOANH NGHIỆP CÓ MUỐN XÓA ĐỊNH MỨC NÀY KHÔNG?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
                            if (dmSelected.ID > 0)
                            {
                                dmSelected.Delete();
                                DMCollectionTMP.Add(dmSelected);
                            }
                            else
                            {
                                DMCollectionTMP.Add(dmSelected);
                            }
                        }
                    }
                }
                else
                {
                    return;
                }
                foreach (DinhMuc dm in DMCollectionTMP)
                {
                    dmDangKy.DMCollection.Remove(dm);
                }
                BindData();
                ShowMessage("XÓA THÀNH CÔNG", false);
                Log.LogDinhMuc(dmDangKy, DMCollectionTMP, MessageTitle.XoaDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaSP.Value.ToString().Trim().Length == 0)
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "CHƯA CHỌN MÃ SẢN PHẨM.", false);
                    return;
                }
                foreach (DinhMuc dmtmp in dmDangKy.DMCollection)
                {
                    if (dmtmp.MaSanPham.Trim().ToUpper() == cbbMaSP.Value.ToString().Trim().ToUpper())
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐÃ NHẬP ĐỊNH MỨC CỦA  MÃ SẢN PHẨM NÀY TRÊN LƯỚI.", false);
                        return;
                    }
                }

                if (DinhMuc.checkDinhMucExit(cbbMaSP.Value.ToString().Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI))
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY ĐÃ ĐƯỢC KHAI BÁO TRONG DANH SÁCH ĐỊNH MỨC KHÁC.", false);
                    return;
                }
                Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                ttdm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                ttdm.MaSanPham = cbbMaSP.Value.ToString().Trim();
                if (ttdm.Load())
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "SẢN PHẨM NÀY ĐÃ ĐƯỢC ĐĂNG KÝ ĐỊNH MỨC.", false);
                    return;
                }
                DataSet ds = Company.BLL.SXXK.ThanhKhoan.NPLNhapTon.SelectTongNPLbyTenChuHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, cbbMaSP.Value.ToString().Trim());
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    DinhMuc dm = new DinhMuc();
                    dm.DinhMucSuDung = Convert.ToDecimal(row["Ton"]);
                    dm.MaSanPham = cbbMaSP.Value.ToString().Trim();
                    dm.MaNguyenPhuLieu = row["Ma"].ToString();
                    dm.DVT_ID = row["DVT_ID"].ToString();
                    dm.TenNPL = row["Ten"].ToString();
                    dmDangKy.DMCollection.Add(dm);
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                string dmsd = e.Row.Cells["DinhMucSuDung"].Text;
                string tlhh = e.Row.Cells["TyLeHaoHut"].Text;
                dmsd = dmsd == "" ? "0" : dmsd;
                tlhh = tlhh == "" ? "0" : tlhh;

                decimal dmChung = Convert.ToDecimal(dmsd) * (Convert.ToDecimal(tlhh) / 100 + 1);
                e.Row.Cells["DinhMucChung"].Text = dmChung.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaNPL_TextChanged(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value == null)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT ĐỂ SAO CHÉP ", false);
                    return;
                }
                foreach (KDT_LenhSanXuat item in LenhSanXuatCollection)
                {
                    bool isExits = false;
                    item.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
                    foreach (KDT_LenhSanXuat_SP ite in item.SPCollection)
                    {
                        if (ite.MaSanPham == cbbMaSP.Value.ToString())
                        {
                            cbbLenhSanXuat.Text = item.SoLenhSanXuat.ToString();
                            isExits = true;
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        errorProvider.SetError(cbbLenhSanXuat, "MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT  NÀY CHƯA ĐƯỢC ĐĂNG KÝ HOẶC CẬP NHẬT .");
                        return;
                    }
                }
                foreach (DinhMuc dm in dmDangKy.DMCollection)
                {
                    if (cbbMaSP.Value.ToString() == dm.MaSanPham)
                    {
                        dm.MaDinhDanhLenhSX = cbbLenhSanXuat.Value.ToString();
                    }
                }
                ShowMessageTQDT("Thông báo từ hệ thống", "SAO CHÉP THÀNH CÔNG ", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            cbbMaSP.ValueChanged -= new EventHandler(cbbMaSP_ValueChanged);
                            cbbMaNPL.ValueChanged -= new EventHandler(cbbMaNPL_ValueChanged);
                            cbbLenhSanXuat.ValueChanged -= new EventHandler(cbbLenhSanXuat_ValueChanged);

                            DMDetail = (DinhMuc)i.GetRow().DataRow;
                            cbbMaSP.Value = DMDetail.MaSanPham.Trim();
                            cbbMaNPL.Value = DMDetail.MaNguyenPhuLieu.Trim();
                            txtDinhMuc.Text = DMDetail.DinhMucSuDung.ToString();
                            txtTyLeHH.Text = DMDetail.TyLeHaoHut.ToString();
                            cbbLenhSanXuat.Value = DMDetail.MaDinhDanhLenhSX;
                            //txtMaSP_Leave(null, null);
                            //txtMaNPL_Leave(null, null);

                            cbbMaSP.ValueChanged += new EventHandler(cbbMaSP_ValueChanged);
                            cbbMaNPL.ValueChanged += new EventHandler(cbbMaNPL_ValueChanged);
                            cbbLenhSanXuat.ValueChanged += new EventHandler(cbbLenhSanXuat_ValueChanged);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaSP_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaSP.Value != null)
                {
                    LenhSanXuatSPCollection = new List<KDT_LenhSanXuat>();
                    foreach (Company.BLL.KDT.SXXK.SXXK_SanPham item in SPCollection)
                    {
                        if (item.Ma == cbbMaSP.Value.ToString())
                        {
                            txtTenSP.Text = item.Ten.ToString();
                            txtDonViTinhSP.Text = DonViTinh.GetName(item.DVT_ID);
                            txtMaHSSP.Text = item.MaHS;
                            foreach (KDT_LenhSanXuat it in LenhSanXuatCollection)
                            {
                                it.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(it.ID);
                                foreach (KDT_LenhSanXuat_SP ite in it.SPCollection)
                                {
                                    if (ite.MaSanPham == cbbMaSP.Value.ToString())
                                    {
                                        LenhSanXuatSPCollection.Add(it);
                                    }
                                }
                            }
                        }
                    }
                    cbbLenhSanXuat.DataSource = LenhSanXuatSPCollection;
                    if (LenhSanXuatSPCollection.Count >= 1)
                    {
                        cbbLenhSanXuat.SelectedIndex = 0;
                    }
                    cbbLenhSanXuat.DisplayMember = "SoLenhSanXuat";
                    cbbLenhSanXuat.ValueMember = "SoLenhSanXuat";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaNPL_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaNPL.Value != null)
                {
                    foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu item in NPLCollection)
                    {
                        if (item.Ma == cbbMaNPL.Value.ToString())
                        {
                            txtTenNPL.Text = item.Ten.ToString();
                            txtDonViTinhNPL.Text = DonViTinh.GetName(item.DVT_ID);
                            txtMaHSNPL.Text = item.MaHS;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static List<String> RemoveDuplicatesIterative(List<String> items)
        {
            List<String> result = new List<String>();
            for (int i = 0; i < items.Count; i++)
            {
                // Assume not duplicate.
                bool duplicate = false;
                for (int z = 0; z < i; z++)
                {
                    if (items[z] == items[i])
                    {
                        // This is a duplicate.
                        duplicate = true;
                        break;
                    }
                }
                // If not duplicate, add to result.
                if (!duplicate)
                {
                    result.Add(items[i]);
                }
            }
            return result;
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT CỦA SẢN PHẨM .", false);
                    return;
                }
                long LenhSanXuat_ID = Convert.ToInt64(KDT_LenhSanXuat.GetID(cbbLenhSanXuat.Value.ToString()));
                string where = " LenhSanXuat_ID = " + LenhSanXuat_ID + " AND MaSanPham NOT IN ('',";
                KDT_LenhSanXuat lenhsanxuat = KDT_LenhSanXuat.Load(LenhSanXuat_ID);
                SanPhamLenhSanXuatRegistedForm SPRegistedForm = new SanPhamLenhSanXuatRegistedForm();
                List<String> ListSP = new List<String>();
                foreach (DinhMuc item in dmDangKy.DMCollection)
                {
                    ListSP.Add(item.MaSanPham);
                }
                List<String> ListSPDistic = RemoveDuplicatesIterative(ListSP);
                foreach (String item in ListSPDistic)
                {
                    where += "'" + item + "',";
                }
                where = where.Substring(0, where.Length - 1);
                where += ")";
                SPRegistedForm.whereCondition = where;
                SPRegistedForm.lenhsanxuat = lenhsanxuat;
                SPRegistedForm.ShowDialog(this);
                if (SPRegistedForm.SanPhamSelectCollection.Count >= 1)
                {
                    List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                    List<Company.BLL.KDT.SXXK.SXXK_SanPham> SPCollection = Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                    foreach (KDT_LenhSanXuat_SP item in SPRegistedForm.SanPhamSelectCollection)
                    {
                        List<SXXK_DinhMuc> dmCollection = SXXK_DinhMuc.SelectCollectionDynamic(" MaSanPham = '" + item.MaSanPham + "' AND LenhSanXuat_ID = " + LenhSanXuat_ID + "", "");
                        foreach (SXXK_DinhMuc dinhmuc in dmCollection)
                        {
                            DinhMuc DM = new DinhMuc();
                            DM.MaSanPham = dinhmuc.MaSanPham;
                            foreach (Company.BLL.KDT.SXXK.SXXK_SanPham sp in SPCollection)
                            {
                                if (DM.MaSanPham == sp.Ma)
                                {
                                    DM.TenSP = sp.Ten;
                                    break;
                                }
                            }
                            DM.MaNguyenPhuLieu = dinhmuc.MaNguyenPhuLieu;
                            foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu npl in NPLCollection)
                            {
                                if (DM.MaNguyenPhuLieu == npl.Ma)
                                {
                                    DM.TenNPL = npl.Ten;
                                    break;
                                }
                            }
                            DM.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                            DM.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                            DM.MaDinhDanhLenhSX = cbbLenhSanXuat.Value.ToString();
                            DM.GhiChu = dinhmuc.GhiChu;
                            dmDangKy.DMCollection.Add(DM);
                        }
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchSP_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["MaNguyenPhuLieu"], ConditionOperator.Contains, txtMaNPL.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLenhSanXuat_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value != null)
                {
                    List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                    List<Company.BLL.KDT.SXXK.SXXK_SanPham> SPCollection = Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");

                    List<SXXK_DinhMuc> DMCollection = new List<SXXK_DinhMuc>();
                    DMCollection = new List<SXXK_DinhMuc>();
                    long LenhSanXuat_ID = KDT_LenhSanXuat.GetID(cbbLenhSanXuat.Value.ToString());
                    DMCollection = SXXK_DinhMuc.SelectCollectionDynamic("MaSanPham = '" + cbbMaSP.Value.ToString() + "' AND LenhSanXuat_ID =" + LenhSanXuat_ID + "", "");
                    bool isExits = false;
                    if (DMCollection.Count > 0)
                    {
                        foreach (DinhMuc item in dmDangKy.DMCollection)
                        {
                            if (item.MaSanPham == cbbMaSP.Value.ToString())
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            if (ShowMessageTQDT("Mã sản phẩm : [" + cbbMaSP.Value.ToString() + "] này đã đăng ký thông tin định mức với lệnh sản xuất : [" + cbbLenhSanXuat.Value.ToString() + "] trước đó .\n Bạn có muốn Lấy danh sách định mức của mã sản phẩm này không ?", true) == "Yes")
                            {
                                foreach (SXXK_DinhMuc ite in DMCollection)
                                {
                                    DMDetail = new DinhMuc();
                                    DMDetail.MaSanPham = ite.MaSanPham;
                                    DMDetail.MaDinhDanhLenhSX = cbbLenhSanXuat.Value.ToString();
                                    DMDetail.MaNguyenPhuLieu = ite.MaNguyenPhuLieu;
                                    DMDetail.TyLeHaoHut = ite.TyLeHaoHut;
                                    DMDetail.DinhMucSuDung = ite.DinhMucSuDung;
                                    //DMDetail.DVT_ID = ite.;
                                    foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu item in NPLCollection)
                                    {
                                        if (DMDetail.MaNguyenPhuLieu == item.Ma)
                                        {
                                            DMDetail.TenNPL = item.Ten;
                                            break;
                                        }
                                    }
                                    foreach (Company.BLL.KDT.SXXK.SXXK_SanPham item in SPCollection)
                                    {
                                        if (DMDetail.MaSanPham == item.Ma)
                                        {
                                            DMDetail.TenSP = item.Ten;
                                            break;
                                        }
                                    }
                                    DMDetail.GhiChu = ite.GhiChu;
                                    dmDangKy.DMCollection.Add(DMDetail);
                                }
                            }
                        }
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCoppyRegisted_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaSP.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN SẢN PHẨM ĐỂ SAO CHÉP ĐỊNH MỨC", false);
                    return;
                }
                if (cbbLenhSanXuat.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT CỦA SẢN PHẨM .", false);
                    return;
                }
                if (!isEdit)
                {
                    if (DinhMuc.checkDinhMucExit(cbbMaSP.Value.ToString().Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI))
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "ĐỊNH MỨC NÀY ĐÃ ĐƯỢC KHAI BÁO TRONG DANH SÁCH ĐỊNH MỨC KHÁC.", false);
                        return;
                    }
                    Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                    ttdm.MaHaiQuan = dmDangKy.MaHaiQuan;
                    ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    ttdm.MaSanPham = cbbMaSP.Value.ToString().Trim();
                    ttdm.LenhSanXuat_ID = Convert.ToInt32(KDT_LenhSanXuat.GetID(cbbLenhSanXuat.Value.ToString()));
                    if (ttdm.Load())
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "SẢN PHẨM NÀY ĐÃ ĐƯỢC ĐĂNG KÝ ĐỊNH MỨC.", false);
                        return;
                    }
                }
                List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                List<Company.BLL.KDT.SXXK.SXXK_SanPham> SPCollection = Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                CoppyDinhMucRegistedForm f = new CoppyDinhMucRegistedForm();
                f.MaSanPham = cbbMaSP.Value.ToString();
                f.dmDangKy = dmDangKy;
                f.ShowDialog(this);
                if (f.DMCollectionCopy.Count > 0)
                {
                    foreach (DinhMuc item in f.DMCollectionCopy)
                    {
                        DinhMuc DM = new DinhMuc();
                        DM.MaSanPham = item.MaSanPham;
                        foreach (Company.BLL.KDT.SXXK.SXXK_SanPham ite in SPCollection)
                        {
                            if (ite.Ma == item.MaSanPham)
                            {
                                DM.TenSP = ite.Ten.ToString();
                                break;
                            }
                        }
                        DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu.ToString();
                        foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu ite in NPLCollection)
                        {
                            if (ite.Ma == item.MaNguyenPhuLieu)
                            {
                                DM.TenNPL = ite.Ten.ToString();
                                break;
                            }
                        }
                        DM.DinhMucSuDung = item.DinhMucSuDung;
                        DM.TyLeHaoHut = item.TyLeHaoHut;
                        DM.MaDinhDanhLenhSX = cbbLenhSanXuat.Value.ToString();
                        DM.GhiChu = item.GhiChu;
                        dmDangKy.DMCollection.Add(DM);
                    }
                    ShowMessageTQDT("Sao chép thành công", false);
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "DANH SÁCH ĐỊNH MỨC_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
            if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
            {

                Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                gridEXExporter1.GridEX = dgList;
                try
                {
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();
                    if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }

    }
}