﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using Company.GC.BLL.KDT;
namespace Company.Interface.KDT.GC
{
    public partial class HangThanhKhoanHDForm : BaseForm
    {
        public ThanhKhoan KhaiBaoTK;
        public HangThanhKhoan HangTK = new HangThanhKhoan();
        private HopDong HD = new HopDong();
        bool isEdit = false;
        public HangThanhKhoanHDForm()
        {
            InitializeComponent();
        }

        private void Set()
        {
            txtMaHang.Text = HangTK.MaHang;
            txtTenHang.Text = HangTK.TenHang;
            txtMaHS.Text = HangTK.MaHS;
            cbDonViTinh.SelectedValue = HangTK.DVT_ID;
            txtLuongDu.Value = HangTK.SoLuong;
            cbLoaiHang.SelectedValue = HangTK.LoaiHang;
        }
        private void Get()
        {
            if (!isEdit)
                HangTK = new HangThanhKhoan();

            HangTK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            HangTK.MaHang = txtMaHang.Text.Trim();
            HangTK.TenHang = txtTenHang.Text.Trim();
            HangTK.MaHS = txtMaHS.Text.Trim();
            HangTK.SoLuong = Convert.ToDecimal(txtLuongDu.Value);
            HangTK.LoaiHang = Convert.ToInt16(cbLoaiHang.SelectedValue);

        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["LoaiHang"].Text = (int)e.Row.Cells["LoaiHang"].Value == 1 ? "Nguyên phụ liệu" : "Thiết bị";
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {

            bool isValid = true;
            if (string.IsNullOrEmpty(txtLuongDu.Text))
                txtLuongDu.Value = 0;
            isValid &= Globals.ValidateNull(txtMaHang, error, "Mã hàng");
            if (!isValid) return;
            Get();
            if (!isEdit)
            {
                if (IsExist(HangTK.MaHang, HangTK.LoaiHang))
                {
                    ShowMessage("Hàng này đã được thêm", false);
                    return;
                }
                KhaiBaoTK.HangCollection.Add(HangTK);
            }
            else
                KhaiBaoTK.HangCollection[KhaiBaoTK.HangCollection.IndexOf(HangTK)] = HangTK;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            isEdit = false;
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            if (cbLoaiHang.SelectedValue.ToString() == "1")
            {
                NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.NguyenPhuLieuSelected.HopDong_ID = KhaiBaoTK.HopDong_ID;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.NguyenPhuLieuSelected.Ma))
                {
                    txtMaHang.Text = f.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = f.NguyenPhuLieuSelected.Ten;
                    txtMaHS.Text = f.NguyenPhuLieuSelected.MaHS;
                    cbDonViTinh.SelectedValue = f.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                }
            }
            else if (cbLoaiHang.SelectedValue.ToString() == "3")
            {
                ThietBiRegistedForm f = new ThietBiRegistedForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.ThietBiSelected.HopDong_ID = KhaiBaoTK.HopDong_ID;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.ThietBiSelected.Ma))
                {
                    txtMaHang.Text = f.ThietBiSelected.Ma;
                    txtTenHang.Text = f.ThietBiSelected.Ten;
                    txtMaHS.Text = f.ThietBiSelected.MaHS;
                    cbDonViTinh.SelectedValue = f.ThietBiSelected.DVT_ID.PadRight(3);
                }
            }
            else
                ShowMessage("Bạn chưa chọn loại hàng", false);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                if (dgList.GetRow() == null) return;
                else HangTK = (HangThanhKhoan)dgList.GetRow().DataRow;
                if (HangTK.ID > 0)
                {
                    HangTK.Delete();
                }
                KhaiBaoTK.HangCollection.Remove(HangTK);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(ex.Message, KhaiBaoTK.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            //Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            //npl.HopDong_ID = TKMD.IDHopDong;
            //npl.Ma = txtMaHang.Text.Trim();
            //if (npl.Load())
            //{
            //    txtMaHang.Text = npl.Ma;
            //    if (txtMaHS.Text.Trim().Length == 0)
            //        txtMaHS.Text = npl.MaHS;
            //    txtTenHang.Text = npl.Ten;
            //    cbDonViTinh.SelectedValue = npl.DVT_ID;

            //    error.SetError(txtMaHang, null);
            //    error.SetError(txtTenHang, null);
            //    error.SetError(txtMaHS, null);

            //}
            //else
            //{
            //    error.SetError(txtMaHang, setText("Không tồn tại mã này.", "This value is not exist"));
            //    txtTenHang.Text = txtMaHS.Text = string.Empty;
            //    cbDonViTinh.SelectedValue = "";
            //}
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.DataRow == null) return;
            HangTK = (HangThanhKhoan)e.Row.DataRow;
            Set();
            isEdit = true;
        }

        private void ThanhKhoanHDHangForm_Load(object sender, EventArgs e)
        {
            if (KhaiBaoTK.HangCollection == null)
                KhaiBaoTK.HangCollection = new List<HangThanhKhoan>();
            dgList.DataSource = KhaiBaoTK.HangCollection;
            cbLoaiHang.SelectedIndex = 0;
            cbDonViTinh.DataSource = DonViTinh.SelectAll().Tables[0];
        }

        private void btnChonHang_Click(object sender, EventArgs e)
        {
            if (cbLoaiHang.SelectedValue.ToString() == "1")
            {
                NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.NguyenPhuLieuSelected.HopDong_ID = KhaiBaoTK.HopDong_ID;
                f.isChonNhieu = true;
                f.ShowDialog();
                if (f.NPLThanhKhoan.Count > 0)
                {
                    getHangFromNPL(f.NPLThanhKhoan);
                }
            }
            else if (cbLoaiHang.SelectedValue.ToString() == "3")
            {
                ThietBiRegistedForm f = new ThietBiRegistedForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.ThietBiSelected.HopDong_ID = KhaiBaoTK.HopDong_ID;
                f.isChonNhieu = true;
                f.ShowDialog();
                if (f.TBThanhKhoan.Count > 0)
                {
                    getHangFromThietBi(f.TBThanhKhoan);
                }
            }
            dgList.DataSource = KhaiBaoTK.HangCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }
        private void getHangFromNPL(List<Company.GC.BLL.GC.NguyenPhuLieu> nplCo)
        {
            foreach (Company.GC.BLL.GC.NguyenPhuLieu item in nplCo)
            {
                if (!IsExist(item.Ma, 1))
                {
                    HangThanhKhoan htk = new HangThanhKhoan
                    {
                        MaHang = item.Ma,
                        LoaiHang = 1,
                        MaHS = item.MaHS,
                        SoLuong = item.SoLuongConLai,
                        TenHang = item.Ten,
                        DVT_ID = item.DVT_ID,

                    };
                    KhaiBaoTK.HangCollection.Add(htk);
                }
            }

        }
        private void getHangFromThietBi(List<Company.GC.BLL.GC.ThietBi> thietBiCo)
        {
            foreach (Company.GC.BLL.GC.ThietBi item in thietBiCo)
            {
                if (!IsExist(item.Ma, 3))
                {
                    HangThanhKhoan htk = new HangThanhKhoan
                    {
                        MaHang = item.Ma,
                        LoaiHang = 3,
                        MaHS = item.MaHS,
                        SoLuong = item.SoLuongConLai,
                        TenHang = item.Ten,
                        DVT_ID = item.DVT_ID,
                    };
                    KhaiBaoTK.HangCollection.Add(htk);
                }
            }

        }
        private bool IsExist(string MaHang, int LoaiHang)
        {
            bool isExist = false;
            foreach (HangThanhKhoan item in KhaiBaoTK.HangCollection)
                if (MaHang.Equals(item.MaHang) && item.LoaiHang == LoaiHang)
                    isExist = true;
            return isExist;
        }

        private void btnSelectTK_Click(object sender, EventArgs e)
        {
            if (cbLoaiHang.SelectedValue.ToString() == "1")
            {
                NguyenPhuLieuThanhKhoanForm f = new NguyenPhuLieuThanhKhoanForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.NguyenPhuLieuSelected.HopDong_ID = KhaiBaoTK.HopDong_ID;
                f.isChonNhieu = true;
                f.ShowDialog();
                if (f.NPLThanhKhoan.Count > 0)
                {
                    getHangFromNPL(f.NPLThanhKhoan);
                }
            }
            else if (cbLoaiHang.SelectedValue.ToString() == "3")
            {
                ThietBiThanhKhoanForm f = new ThietBiThanhKhoanForm();
                f.isBrower = true;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.ThietBiSelected.HopDong_ID = KhaiBaoTK.HopDong_ID;
                f.isChonNhieu = true;
                f.ShowDialog();
                if (f.TBThanhKhoan.Count > 0)
                {
                    getHangFromThietBi(f.TBThanhKhoan);
                }
            }
            dgList.DataSource = KhaiBaoTK.HangCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

        }

    }
}
