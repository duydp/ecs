-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]
	@ThuePhaiThu_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@TienThue numeric(12, 0),
	@SoTienMienThue numeric(11, 0),
	@SoTienGiamThue numeric(11, 0),
	@SoThuePhaiNop numeric(12, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
(
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
)
VALUES 
(
	@ThuePhaiThu_ID,
	@TenSacThue,
	@TieuMuc,
	@TienThue,
	@SoTienMienThue,
	@SoTienGiamThue,
	@SoThuePhaiNop
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]
	@ID bigint,
	@ThuePhaiThu_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@TienThue numeric(12, 0),
	@SoTienMienThue numeric(11, 0),
	@SoTienGiamThue numeric(11, 0),
	@SoThuePhaiNop numeric(12, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
SET
	[ThuePhaiThu_ID] = @ThuePhaiThu_ID,
	[TenSacThue] = @TenSacThue,
	[TieuMuc] = @TieuMuc,
	[TienThue] = @TienThue,
	[SoTienMienThue] = @SoTienMienThue,
	[SoTienGiamThue] = @SoTienGiamThue,
	[SoThuePhaiNop] = @SoThuePhaiNop
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]
	@ID bigint,
	@ThuePhaiThu_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@TienThue numeric(12, 0),
	@SoTienMienThue numeric(11, 0),
	@SoTienGiamThue numeric(11, 0),
	@SoThuePhaiNop numeric(12, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] 
		SET
			[ThuePhaiThu_ID] = @ThuePhaiThu_ID,
			[TenSacThue] = @TenSacThue,
			[TieuMuc] = @TieuMuc,
			[TienThue] = @TienThue,
			[SoTienMienThue] = @SoTienMienThue,
			[SoTienGiamThue] = @SoTienGiamThue,
			[SoThuePhaiNop] = @SoThuePhaiNop
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
		(
			[ThuePhaiThu_ID],
			[TenSacThue],
			[TieuMuc],
			[TienThue],
			[SoTienMienThue],
			[SoTienGiamThue],
			[SoThuePhaiNop]
		)
		VALUES 
		(
			@ThuePhaiThu_ID,
			@TenSacThue,
			@TieuMuc,
			@TienThue,
			@SoTienMienThue,
			@SoTienGiamThue,
			@SoThuePhaiNop
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]
	@ThuePhaiThu_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
WHERE
	[ThuePhaiThu_ID] = @ThuePhaiThu_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]
	@ThuePhaiThu_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
WHERE
	[ThuePhaiThu_ID] = @ThuePhaiThu_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]	

GO

