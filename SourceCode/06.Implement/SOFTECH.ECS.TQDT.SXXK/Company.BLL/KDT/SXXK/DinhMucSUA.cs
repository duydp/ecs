﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.BLL.KDT.SXXK
{
    public partial class DinhMucSUA
    {
        public static DinhMucSUA SearchBy(List<DinhMucSUA> dmSUAList, string maSP, string maNPL)
        {
            foreach (DinhMucSUA item in dmSUAList)
            {
                if (item.MaSanPham.ToLower() == maSP.ToLower() && item.MaNguyenPhuLieu.ToLower() == maNPL.ToLower())
                {
                    return item;
                }
            }

            return null;
        }
    }
}
