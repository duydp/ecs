/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 08/13/2012 4:06:16 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] DROP CONSTRAINT [PK_t_HaiQuan_CuaKhau]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[t_HaiQuan_Cuc]'
GO
ALTER TABLE [dbo].[t_HaiQuan_Cuc] DROP CONSTRAINT [DF_t_HaiQuan_Cuc_IsDefault]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ALTER COLUMN [ID] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_CuaKhau] on [dbo].[t_HaiQuan_CuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_CuaKhau] ADD CONSTRAINT [PK_t_HaiQuan_CuaKhau] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_NguyenTe]'
GO
ALTER TABLE [dbo].[t_HaiQuan_NguyenTe] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_DonViTinh]'
GO
ALTER TABLE [dbo].[t_HaiQuan_DonViTinh] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_MaHS]'
GO
ALTER TABLE [dbo].[t_HaiQuan_MaHS] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_PhuongThucThanhToan]'
GO
ALTER TABLE [dbo].[t_HaiQuan_PhuongThucThanhToan] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_Nuoc]'
GO
ALTER TABLE [dbo].[t_HaiQuan_Nuoc] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_DonViHaiQuan]'
GO
ALTER TABLE [dbo].[t_HaiQuan_DonViHaiQuan] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_Cuc]'
GO
ALTER TABLE [dbo].[t_HaiQuan_Cuc] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[t_HaiQuan_Cuc] ALTER COLUMN [IsDefault] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_DieuKienGiaoHang]'
GO
ALTER TABLE [dbo].[t_HaiQuan_DieuKienGiaoHang] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_PhuongThucVanTai]'
GO
ALTER TABLE [dbo].[t_HaiQuan_PhuongThucVanTai] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[t_HaiQuan_Cuc]'
GO
ALTER TABLE [dbo].[t_HaiQuan_Cuc] ADD CONSTRAINT [DF_t_HaiQuan_Cuc_IsDefault_1] DEFAULT ('V2') FOR [IsDefault]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.9', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.9', getdate(), null)
	end 
