﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    partial class ToKhaiMauDichManageForm
    {
        private UIGroupBox uiGroupBox1;
        private GridEX dgList;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichManageForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cbUserKB = new Janus.Windows.EditControls.UIComboBox();
            this.lblUserKB = new System.Windows.Forms.Label();
            this.chkDate = new System.Windows.Forms.CheckBox();
            this.txtSoTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.lblSoTK = new System.Windows.Forms.Label();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.dtDenNgay = new System.Windows.Forms.DateTimePicker();
            this.lblDenNgay = new System.Windows.Forms.Label();
            this.dtTuNgay = new System.Windows.Forms.DateTimePicker();
            this.lblTuNgay = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboPhanLuong = new Janus.Windows.EditControls.UIComboBox();
            this.btnXemNPLDK = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNamTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SaoChepCha = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepTK = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepHH = new System.Windows.Forms.ToolStripMenuItem();
            this.print = new System.Windows.Forms.ToolStripMenuItem();
            this.PhieuTNMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToKhaiMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToKhaiA4MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCSDaDuyet = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImportTK = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImportMSG = new System.Windows.Forms.ToolStripMenuItem();
            this.mniSuaToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.mniHuyToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSingleDownload2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.cmdCancel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.XacNhan1 = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.SaoChep3 = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.cmdXuatToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.cmdCSDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdPrint2 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSingleDownload = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.SaoChep = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.SaoChepToKhaiHang1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.SaoChepALL1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepALL = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepToKhaiHang = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.ToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.cmdInA41 = new Janus.Windows.UI.CommandBars.UICommand("cmdInA4");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.Export = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.Import = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.cmdCSDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.ToKhaiViet = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdInA4 = new Janus.Windows.UI.CommandBars.UICommand("cmdInA4");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.mnuCapNhatLaiTK = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(942, 361);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.cbUserKB);
            this.uiGroupBox1.Controls.Add(this.lblUserKB);
            this.uiGroupBox1.Controls.Add(this.chkDate);
            this.uiGroupBox1.Controls.Add(this.txtSoTK);
            this.uiGroupBox1.Controls.Add(this.lblSoTK);
            this.uiGroupBox1.Controls.Add(this.btnExportExcel);
            this.uiGroupBox1.Controls.Add(this.dtDenNgay);
            this.uiGroupBox1.Controls.Add(this.lblDenNgay);
            this.uiGroupBox1.Controls.Add(this.dtTuNgay);
            this.uiGroupBox1.Controls.Add(this.lblTuNgay);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.cboPhanLuong);
            this.uiGroupBox1.Controls.Add(this.btnXemNPLDK);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnSearch);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.cbStatus);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.txtNamTiepNhan);
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 32);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(942, 361);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.uiButton1.ImageIndex = 5;
            this.uiButton1.ImageList = this.ImageList1;
            this.uiButton1.Location = new System.Drawing.Point(519, 328);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(106, 23);
            this.uiButton1.TabIndex = 294;
            this.uiButton1.Text = "Kết quả xử lý";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.WordWrap = false;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "page_excel.png");
            this.ImageList1.Images.SetKeyName(5, "report.png");
            // 
            // cbUserKB
            // 
            this.cbUserKB.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbUserKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Chưa khai báo";
            uiComboBoxItem1.Value = -1;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.FormatStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Chờ duyệt";
            uiComboBoxItem2.Value = 0;
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Đã duyệt";
            uiComboBoxItem3.Value = 1;
            this.cbUserKB.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3});
            this.cbUserKB.Location = new System.Drawing.Point(612, 65);
            this.cbUserKB.Name = "cbUserKB";
            this.cbUserKB.Size = new System.Drawing.Size(118, 21);
            this.cbUserKB.TabIndex = 9;
            this.cbUserKB.VisualStyleManager = this.vsmMain;
            // 
            // lblUserKB
            // 
            this.lblUserKB.AutoSize = true;
            this.lblUserKB.BackColor = System.Drawing.Color.Transparent;
            this.lblUserKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserKB.Location = new System.Drawing.Point(609, 46);
            this.lblUserKB.Name = "lblUserKB";
            this.lblUserKB.Size = new System.Drawing.Size(90, 13);
            this.lblUserKB.TabIndex = 293;
            this.lblUserKB.Text = "Người khai báo";
            // 
            // chkDate
            // 
            this.chkDate.AutoSize = true;
            this.chkDate.BackColor = System.Drawing.Color.Transparent;
            this.chkDate.Location = new System.Drawing.Point(146, 70);
            this.chkDate.Name = "chkDate";
            this.chkDate.Size = new System.Drawing.Size(94, 17);
            this.chkDate.TabIndex = 6;
            this.chkDate.Text = "Tìm theo ngày";
            this.chkDate.UseVisualStyleBackColor = false;
            this.chkDate.CheckedChanged += new System.EventHandler(this.chkDate_CheckedChanged);
            // 
            // txtSoTK
            // 
            this.txtSoTK.DecimalDigits = 0;
            this.txtSoTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTK.FormatString = "#####";
            this.txtSoTK.Location = new System.Drawing.Point(97, 66);
            this.txtSoTK.MaxLength = 5;
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Size = new System.Drawing.Size(43, 21);
            this.txtSoTK.TabIndex = 5;
            this.txtSoTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTK.Value = ((ulong)(0ul));
            this.txtSoTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTK.VisualStyleManager = this.vsmMain;
            // 
            // lblSoTK
            // 
            this.lblSoTK.AutoSize = true;
            this.lblSoTK.BackColor = System.Drawing.Color.Transparent;
            this.lblSoTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTK.Location = new System.Drawing.Point(13, 71);
            this.lblSoTK.Name = "lblSoTK";
            this.lblSoTK.Size = new System.Drawing.Size(63, 13);
            this.lblSoTK.TabIndex = 290;
            this.lblSoTK.Text = "Số tờ khai";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnExportExcel.ImageIndex = 4;
            this.btnExportExcel.ImageList = this.ImageList1;
            this.btnExportExcel.Location = new System.Drawing.Point(736, 64);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(99, 23);
            this.btnExportExcel.TabIndex = 11;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyleManager = this.vsmMain;
            this.btnExportExcel.WordWrap = false;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // dtDenNgay
            // 
            this.dtDenNgay.CustomFormat = "dd/MM/yyyy";
            this.dtDenNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtDenNgay.Location = new System.Drawing.Point(522, 65);
            this.dtDenNgay.Name = "dtDenNgay";
            this.dtDenNgay.Size = new System.Drawing.Size(81, 21);
            this.dtDenNgay.TabIndex = 8;
            // 
            // lblDenNgay
            // 
            this.lblDenNgay.AutoSize = true;
            this.lblDenNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblDenNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDenNgay.Location = new System.Drawing.Point(429, 71);
            this.lblDenNgay.Name = "lblDenNgay";
            this.lblDenNgay.Size = new System.Drawing.Size(61, 13);
            this.lblDenNgay.TabIndex = 287;
            this.lblDenNgay.Text = "Đến ngày";
            // 
            // dtTuNgay
            // 
            this.dtTuNgay.CustomFormat = "dd/MM/yyyy";
            this.dtTuNgay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTuNgay.Location = new System.Drawing.Point(296, 65);
            this.dtTuNgay.Name = "dtTuNgay";
            this.dtTuNgay.Size = new System.Drawing.Size(118, 21);
            this.dtTuNgay.TabIndex = 7;
            // 
            // lblTuNgay
            // 
            this.lblTuNgay.AutoSize = true;
            this.lblTuNgay.BackColor = System.Drawing.Color.Transparent;
            this.lblTuNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTuNgay.Location = new System.Drawing.Point(237, 71);
            this.lblTuNgay.Name = "lblTuNgay";
            this.lblTuNgay.Size = new System.Drawing.Size(53, 13);
            this.lblTuNgay.TabIndex = 285;
            this.lblTuNgay.Text = "Từ ngày";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(221, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 284;
            this.label1.Text = "Phần luồng";
            // 
            // cboPhanLuong
            // 
            this.cboPhanLuong.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Chưa khai báo";
            uiComboBoxItem4.Value = -1;
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.FormatStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Chờ duyệt";
            uiComboBoxItem5.Value = 0;
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Đã duyệt";
            uiComboBoxItem6.Value = 1;
            this.cboPhanLuong.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cboPhanLuong.Location = new System.Drawing.Point(296, 37);
            this.cboPhanLuong.Name = "cboPhanLuong";
            this.cboPhanLuong.Size = new System.Drawing.Size(118, 21);
            this.cboPhanLuong.TabIndex = 3;
            this.cboPhanLuong.VisualStyleManager = this.vsmMain;
            this.cboPhanLuong.SelectedValueChanged += new System.EventHandler(this.cboPhanLuong_SelectedValueChanged);
            // 
            // btnXemNPLDK
            // 
            this.btnXemNPLDK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXemNPLDK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemNPLDK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXemNPLDK.Icon")));
            this.btnXemNPLDK.Location = new System.Drawing.Point(631, 328);
            this.btnXemNPLDK.Name = "btnXemNPLDK";
            this.btnXemNPLDK.Size = new System.Drawing.Size(142, 23);
            this.btnXemNPLDK.TabIndex = 14;
            this.btnXemNPLDK.Text = "Xem NPL cung ứng";
            this.btnXemNPLDK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXemNPLDK.Click += new System.EventHandler(this.btnXemNPLDK_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXoa.Location = new System.Drawing.Point(778, 328);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(74, 23);
            this.btnXoa.TabIndex = 15;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.WordWrap = false;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(97, 10);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(317, 21);
            this.txtSoHopDong.TabIndex = 0;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 277;
            this.label6.Text = "Số hợp đồng";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(12, 333);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Hướng dẫn: Kích đôi để xem chi tiết";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(858, 328);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(72, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSearch.Icon")));
            this.btnSearch.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearch.Location = new System.Drawing.Point(736, 35);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(99, 23);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            this.btnSearch.WordWrap = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 273;
            this.label4.Text = "Trạng thái";
            // 
            // cbStatus
            // 
            this.cbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.FormatStyle.ForeColor = System.Drawing.Color.Red;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Chưa khai báo";
            uiComboBoxItem7.Value = -1;
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.FormatStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Chờ duyệt";
            uiComboBoxItem8.Value = 0;
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Đã duyệt";
            uiComboBoxItem9.Value = 1;
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Không phê duyệt";
            uiComboBoxItem10.Value = 2;
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Sửa tờ khai";
            uiComboBoxItem11.Value = 5;
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Đã hủy";
            uiComboBoxItem12.Value = 10;
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Chờ hủy";
            uiComboBoxItem13.Value = 11;
            this.cbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10,
            uiComboBoxItem11,
            uiComboBoxItem12,
            uiComboBoxItem13});
            this.cbStatus.Location = new System.Drawing.Point(97, 37);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(118, 21);
            this.cbStatus.TabIndex = 2;
            this.cbStatus.VisualStyleManager = this.vsmMain;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.cbStatus_SelectedValueChanged);
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(522, 11);
            this.txtSoTiepNhan.MaxLength = 5;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(81, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(429, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 269;
            this.label3.Text = "Năm tiếp nhận";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(429, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 267;
            this.label2.Text = "Số tiếp nhận";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtNamTiepNhan
            // 
            this.txtNamTiepNhan.DecimalDigits = 0;
            this.txtNamTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamTiepNhan.FormatString = "####";
            this.txtNamTiepNhan.Location = new System.Drawing.Point(522, 38);
            this.txtNamTiepNhan.MaxLength = 4;
            this.txtNamTiepNhan.Name = "txtNamTiepNhan";
            this.txtNamTiepNhan.Size = new System.Drawing.Size(81, 21);
            this.txtNamTiepNhan.TabIndex = 4;
            this.txtNamTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamTiepNhan.Value = ((short)(0));
            this.txtNamTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtNamTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(12, 105);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(918, 212);
            this.dgList.TabIndex = 12;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaoChepCha,
            this.print,
            this.mnuCSDaDuyet,
            this.mniImportTK,
            this.mniImportMSG,
            this.mniSuaToKhai,
            this.mniHuyToKhai,
            this.mnuCapNhatLaiTK});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(177, 202);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // SaoChepCha
            // 
            this.SaoChepCha.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saoChepTK,
            this.saoChepHH});
            this.SaoChepCha.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepCha.Image")));
            this.SaoChepCha.Name = "SaoChepCha";
            this.SaoChepCha.Size = new System.Drawing.Size(176, 22);
            this.SaoChepCha.Text = "Sao chép";
            this.SaoChepCha.Click += new System.EventHandler(this.SaoChepCha_Click);
            // 
            // saoChepTK
            // 
            this.saoChepTK.Image = ((System.Drawing.Image)(resources.GetObject("saoChepTK.Image")));
            this.saoChepTK.Name = "saoChepTK";
            this.saoChepTK.Size = new System.Drawing.Size(154, 22);
            this.saoChepTK.Text = "Tờ khai";
            this.saoChepTK.Click += new System.EventHandler(this.saoChepTK_Click);
            // 
            // saoChepHH
            // 
            this.saoChepHH.Image = ((System.Drawing.Image)(resources.GetObject("saoChepHH.Image")));
            this.saoChepHH.Name = "saoChepHH";
            this.saoChepHH.Size = new System.Drawing.Size(154, 22);
            this.saoChepHH.Text = "Tờ khai + hàng";
            this.saoChepHH.Click += new System.EventHandler(this.saoChepHH_Click);
            // 
            // print
            // 
            this.print.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PhieuTNMenuItem,
            this.ToKhaiMenuItem,
            this.ToKhaiA4MenuItem});
            this.print.Image = ((System.Drawing.Image)(resources.GetObject("print.Image")));
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(176, 22);
            this.print.Text = "In ";
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // PhieuTNMenuItem
            // 
            this.PhieuTNMenuItem.Name = "PhieuTNMenuItem";
            this.PhieuTNMenuItem.Size = new System.Drawing.Size(218, 22);
            this.PhieuTNMenuItem.Text = "Phiếu tiếp nhận";
            this.PhieuTNMenuItem.Click += new System.EventHandler(this.PhieuTNMenuItem_Click);
            // 
            // ToKhaiMenuItem
            // 
            this.ToKhaiMenuItem.Name = "ToKhaiMenuItem";
            this.ToKhaiMenuItem.Size = new System.Drawing.Size(218, 22);
            this.ToKhaiMenuItem.Text = "Tờ khai";
            this.ToKhaiMenuItem.Visible = false;
            this.ToKhaiMenuItem.Click += new System.EventHandler(this.ToKhaiMenuItem_Click);
            // 
            // ToKhaiA4MenuItem
            // 
            this.ToKhaiA4MenuItem.Name = "ToKhaiA4MenuItem";
            this.ToKhaiA4MenuItem.Size = new System.Drawing.Size(218, 22);
            this.ToKhaiA4MenuItem.Text = "Tờ khai thông quan điện tử";
            this.ToKhaiA4MenuItem.Click += new System.EventHandler(this.ToKhaiA4MenuItem_Click);
            // 
            // mnuCSDaDuyet
            // 
            this.mnuCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("mnuCSDaDuyet.Image")));
            this.mnuCSDaDuyet.Name = "mnuCSDaDuyet";
            this.mnuCSDaDuyet.Size = new System.Drawing.Size(176, 22);
            this.mnuCSDaDuyet.Text = "Chuyển Trạng Thái";
            this.mnuCSDaDuyet.Visible = false;
            this.mnuCSDaDuyet.Click += new System.EventHandler(this.mnuCSDaDuyet_Click);
            // 
            // mniImportTK
            // 
            this.mniImportTK.Name = "mniImportTK";
            this.mniImportTK.Size = new System.Drawing.Size(176, 22);
            this.mniImportTK.Text = "Import tờ khai";
            // 
            // mniImportMSG
            // 
            this.mniImportMSG.Name = "mniImportMSG";
            this.mniImportMSG.Size = new System.Drawing.Size(176, 22);
            this.mniImportMSG.Text = "Import MSG";
            // 
            // mniSuaToKhai
            // 
            this.mniSuaToKhai.Name = "mniSuaToKhai";
            this.mniSuaToKhai.Size = new System.Drawing.Size(176, 22);
            this.mniSuaToKhai.Text = "Sửa tờ khai";
            this.mniSuaToKhai.Click += new System.EventHandler(this.mniSuaToKhai_Click);
            // 
            // mniHuyToKhai
            // 
            this.mniHuyToKhai.Name = "mniHuyToKhai";
            this.mniHuyToKhai.Size = new System.Drawing.Size(176, 22);
            this.mniHuyToKhai.Text = "Hủy tờ khai";
            this.mniHuyToKhai.Click += new System.EventHandler(this.mniHuyToKhai_Click);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSingleDownload,
            this.cmdCancel,
            this.SaoChep,
            this.SaoChepALL,
            this.SaoChepToKhaiHang,
            this.cmdPrint,
            this.XacNhan,
            this.Export,
            this.Import,
            this.cmdCSDaDuyet,
            this.cmdXuatToKhai,
            this.ToKhaiViet,
            this.InPhieuTN2,
            this.cmdInA4});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend2,
            this.cmdSingleDownload2,
            this.cmdCancel2,
            this.XacNhan1,
            this.SaoChep3,
            this.cmdXuatToKhai1,
            this.cmdCSDaDuyet1,
            this.cmdPrint2});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.MergeRowOrder = 1;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(748, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandBar1_CommandClick);
            // 
            // cmdSend2
            // 
            this.cmdSend2.Key = "cmdSend";
            this.cmdSend2.Name = "cmdSend2";
            this.cmdSend2.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
            this.cmdSend2.Text = "&Khai báo";
            // 
            // cmdSingleDownload2
            // 
            this.cmdSingleDownload2.Key = "cmdSingleDownload";
            this.cmdSingleDownload2.Name = "cmdSingleDownload2";
            this.cmdSingleDownload2.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.cmdSingleDownload2.Text = "&Nhận dữ liệu";
            this.cmdSingleDownload2.ToolTipText = " ";
            // 
            // cmdCancel2
            // 
            this.cmdCancel2.Key = "cmdCancel";
            this.cmdCancel2.Name = "cmdCancel2";
            this.cmdCancel2.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.cmdCancel2.Text = "&Hủy khai báo";
            this.cmdCancel2.ToolTipText = " ";
            // 
            // XacNhan1
            // 
            this.XacNhan1.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan1.Icon")));
            this.XacNhan1.Key = "XacNhan";
            this.XacNhan1.Name = "XacNhan1";
            this.XacNhan1.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.XacNhan1.Text = "&Xác nhận";
            this.XacNhan1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // SaoChep3
            // 
            this.SaoChep3.Key = "SaoChep";
            this.SaoChep3.Name = "SaoChep3";
            this.SaoChep3.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.SaoChep3.Text = "&Sao chép";
            // 
            // cmdXuatToKhai1
            // 
            this.cmdXuatToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdXuatToKhai1.Icon")));
            this.cmdXuatToKhai1.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai1.Name = "cmdXuatToKhai1";
            this.cmdXuatToKhai1.Text = "X&uất dữ liệu cho phòng khai";
            // 
            // cmdCSDaDuyet1
            // 
            this.cmdCSDaDuyet1.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet1.Name = "cmdCSDaDuyet1";
            this.cmdCSDaDuyet1.Text = "&Chuyển Trạng Thái";
            // 
            // cmdPrint2
            // 
            this.cmdPrint2.ImageIndex = 2;
            this.cmdPrint2.Key = "cmdPrint";
            this.cmdPrint2.Name = "cmdPrint2";
            this.cmdPrint2.Text = "&In";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSingleDownload
            // 
            this.cmdSingleDownload.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSingleDownload.Icon")));
            this.cmdSingleDownload.Key = "cmdSingleDownload";
            this.cmdSingleDownload.Name = "cmdSingleDownload";
            this.cmdSingleDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdSingleDownload.Text = "Nhận dữ liệu";
            this.cmdSingleDownload.ToolTipText = "Nhận dữ liệu mới của tờ khai đang chọn (Ctrl + D)";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCancel.Icon")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdCancel.Text = "Hủy khai báo";
            this.cmdCancel.ToolTipText = "Hủy khai báo (Ctrl + S)";
            // 
            // SaoChep
            // 
            this.SaoChep.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.SaoChepToKhaiHang1,
            this.SaoChepALL1});
            this.SaoChep.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChep.Icon")));
            this.SaoChep.Key = "SaoChep";
            this.SaoChep.Name = "SaoChep";
            this.SaoChep.Text = "Sao chép";
            // 
            // SaoChepToKhaiHang1
            // 
            this.SaoChepToKhaiHang1.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChepToKhaiHang1.Icon")));
            this.SaoChepToKhaiHang1.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang1.Name = "SaoChepToKhaiHang1";
            this.SaoChepToKhaiHang1.Text = "Tờ khai";
            // 
            // SaoChepALL1
            // 
            this.SaoChepALL1.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChepALL1.Icon")));
            this.SaoChepALL1.Key = "SaoChepALL";
            this.SaoChepALL1.Name = "SaoChepALL1";
            this.SaoChepALL1.Text = "Tờ khai + hàng";
            // 
            // SaoChepALL
            // 
            this.SaoChepALL.Key = "SaoChepALL";
            this.SaoChepALL.Name = "SaoChepALL";
            this.SaoChepALL.Text = "Tờ khai + hàng";
            // 
            // SaoChepToKhaiHang
            // 
            this.SaoChepToKhaiHang.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Name = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Text = "Tờ khai + hàng";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.InPhieuTN1,
            this.ToKhai1,
            this.cmdInA41});
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.ImageIndex = 2;
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // ToKhai1
            // 
            this.ToKhai1.ImageIndex = 2;
            this.ToKhai1.Key = "ToKhaiViet";
            this.ToKhai1.Name = "ToKhai1";
            this.ToKhai1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdInA41
            // 
            this.cmdInA41.ImageIndex = 2;
            this.cmdInA41.Key = "cmdInA4";
            this.cmdInA41.Name = "cmdInA41";
            this.cmdInA41.Text = "In tờ khai thông quan điện tử";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận";
            // 
            // Export
            // 
            this.Export.Key = "Export";
            this.Export.Name = "Export";
            this.Export.Text = "Export dữ liệu";
            // 
            // Import
            // 
            this.Import.Key = "Import";
            this.Import.Name = "Import";
            this.Import.Text = "Import dữ liệu";
            // 
            // cmdCSDaDuyet
            // 
            this.cmdCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet.Image")));
            this.cmdCSDaDuyet.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Name = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdCSDaDuyet.Text = "Chuyển Trạng Thái";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // ToKhaiViet
            // 
            this.ToKhaiViet.Key = "ToKhaiViet";
            this.ToKhaiViet.Name = "ToKhaiViet";
            this.ToKhaiViet.Text = "Tờ khai";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            this.InPhieuTN2.Text = "Phiếu tiếp nhận";
            // 
            // cmdInA4
            // 
            this.cmdInA4.Key = "cmdInA4";
            this.cmdInA4.Name = "cmdInA4";
            this.cmdInA4.Text = "In Trang A4";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(942, 32);
            // 
            // uiRebar1
            // 
            this.uiRebar1.CommandManager = this.cmMain;
            this.uiRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiRebar1.Location = new System.Drawing.Point(0, 0);
            this.uiRebar1.Name = "uiRebar1";
            this.uiRebar1.Size = new System.Drawing.Size(810, 0);
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "ECS files|*.ECS";
            this.saveFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // mnuCapNhatLaiTK
            // 
            this.mnuCapNhatLaiTK.Name = "mnuCapNhatLaiTK";
            this.mnuCapNhatLaiTK.Size = new System.Drawing.Size(176, 22);
            this.mnuCapNhatLaiTK.Text = "Cập nhật lại tờ khai";
            this.mnuCapNhatLaiTK.Click += new System.EventHandler(this.mnuCapNhatLaiTK_Click);
            // 
            // ToKhaiMauDichManageForm
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(942, 393);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichManageForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "ToKhaiMauDichManageForm";
            this.Text = "Theo dõi tờ khai mậu dịch";
            this.Load += new System.EventHandler(this.ToKhaiMauDichManageForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UIButton btnSearch;
        private Label label4;
        private UIComboBox cbStatus;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private Label label3;
        private Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamTiepNhan;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar uiRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend2;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload2;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel2;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep3;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem SaoChepCha;
        private ToolStripMenuItem saoChepTK;
        private ToolStripMenuItem saoChepHH;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private ToolStripMenuItem print;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan1;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private Janus.Windows.UI.CommandBars.UICommand Export;
        private Janus.Windows.UI.CommandBars.UICommand Import;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private Label label5;
        private UIButton btnClose;
        private Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private UIButton btnXoa;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet;
        private ToolStripMenuItem mnuCSDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatToKhai;
        private UIButton btnXemNPLDK;
        private ToolStripMenuItem ToKhaiMenuItem;
        private ToolStripMenuItem PhieuTNMenuItem;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint2;
        private Janus.Windows.UI.CommandBars.UICommand ToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand ToKhai;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN2;
        private Janus.Windows.UI.CommandBars.UICommand cmdInA4;
        private Janus.Windows.UI.CommandBars.UICommand cmdInA41;
        private ToolStripMenuItem ToKhaiA4MenuItem;
        private ColorDialog colorDialog1;
        private Label label1;
        private UIComboBox cboPhanLuong;
        private ToolStripMenuItem mniImportTK;
        private ToolStripMenuItem mniImportMSG;
        private ToolStripMenuItem mniSuaToKhai;
        private ToolStripMenuItem mniHuyToKhai;
        private Company.KDT.SHARE.Components.WS.KDTService kdtService1;
        private DateTimePicker dtTuNgay;
        private Label lblTuNgay;
        private DateTimePicker dtDenNgay;
        private Label lblDenNgay;
        private UIButton btnExportExcel;
        private CheckBox chkDate;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTK;
        private Label lblSoTK;
        private Label lblUserKB;
        private UIComboBox cbUserKB;
        private UIButton uiButton1;
        private Janus.Windows.UI.CommandBars.UICommand ToKhaiViet;
        private ImageList ImageList1;
        private ToolStripMenuItem mnuCapNhatLaiTK;
    }
}
