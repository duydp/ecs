using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangGiayPhep_SMA : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GiayPhep_ID { set; get; }
		public string TenThuocQuyCachDongGoi { set; get; }
		public string MaSoHangHoa { set; get; }
		public string TenHoatChatGayNghien { set; get; }
		public string HoatChat { set; get; }
		public string TieuChuanChatLuong { set; get; }
		public string SoDangKy { set; get; }
		public DateTime HanDung { set; get; }
		public decimal SoLuong { set; get; }
		public string DonVitinhSoLuong { set; get; }
		public string CongDung { set; get; }
		public decimal TongSoKhoiLuongHoatChatGayNghien { set; get; }
		public string DonVitinhKhoiLuong { set; get; }
		public decimal GiaNhapKhauVND { set; get; }
		public decimal GiaBanBuonVND { set; get; }
		public decimal GiaBanLeVND { set; get; }
		public string MaThuongNhanXuatKhau { set; get; }
		public string TenThuongNhanXuatKhau { set; get; }
		public string MaBuuChinhXuatKhau { set; get; }
		public string SoNhaTenDuongXuatKhau { set; get; }
		public string PhuongXaXuatKhau { set; get; }
		public string QuanHuyenXuatKhau { set; get; }
		public string TinhThanhPhoXuatKhau { set; get; }
		public string MaQuocGiaXuatKhau { set; get; }
		public string MaCongTySanXuat { set; get; }
		public string TenCongTySanXuat { set; get; }
		public string MaBuuChinhCongTySanXuat { set; get; }
		public string SoNhaTenDuongCongTySanXuat { set; get; }
		public string PhuongXaCongTySanXuat { set; get; }
		public string QuanHuyenCongTySanXuat { set; get; }
		public string TinhThanhPhoCongTySanXuat { set; get; }
		public string MaQuocGiaCongTySanXuat { set; get; }
		public string MaCongTyCungCap { set; get; }
		public string TenCongTyCungCap { set; get; }
		public string MaBuuChinhCongTyCungCap { set; get; }
		public string SoNhaTenDuongCongTyCungCap { set; get; }
		public string PhuongXaCongTyCungCap { set; get; }
		public string QuanHuyenCongTyCungCap { set; get; }
		public string TinhThanhPhoCongTyCungCap { set; get; }
		public string MaQuocGiaCongTyCungCap { set; get; }
		public string MaDonViUyThac { set; get; }
		public string TenDonViUyThac { set; get; }
		public string MaBuuChinhDonViUyThac { set; get; }
		public string SoNhaTenDuongDonViUyThac { set; get; }
		public string PhuongXaDonViUyThac { set; get; }
		public string QuanHuyenDonViUyThac { set; get; }
		public string TinhThanhPhoDonViUyThac { set; get; }
		public string MaQuocGiaDonViUyThac { set; get; }
		public decimal LuongTonKhoKyTruoc { set; get; }
		public string DonViTinhLuongTonKyTruoc { set; get; }
		public decimal LuongNhapTrongKy { set; get; }
		public decimal TongSo { set; get; }
		public decimal TongSoXuatTrongKy { set; get; }
		public decimal SoLuongTonKhoDenNgay { set; get; }
		public decimal HuHao { set; get; }
		public string GhiChuChiTiet { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangGiayPhep_SMA> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangGiayPhep_SMA> collection = new List<KDT_VNACC_HangGiayPhep_SMA>();
			while (reader.Read())
			{
				KDT_VNACC_HangGiayPhep_SMA entity = new KDT_VNACC_HangGiayPhep_SMA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayPhep_ID"))) entity.GiayPhep_ID = reader.GetInt64(reader.GetOrdinal("GiayPhep_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuocQuyCachDongGoi"))) entity.TenThuocQuyCachDongGoi = reader.GetString(reader.GetOrdinal("TenThuocQuyCachDongGoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHangHoa"))) entity.MaSoHangHoa = reader.GetString(reader.GetOrdinal("MaSoHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHoatChatGayNghien"))) entity.TenHoatChatGayNghien = reader.GetString(reader.GetOrdinal("TenHoatChatGayNghien"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoatChat"))) entity.HoatChat = reader.GetString(reader.GetOrdinal("HoatChat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuChuanChatLuong"))) entity.TieuChuanChatLuong = reader.GetString(reader.GetOrdinal("TieuChuanChatLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDangKy"))) entity.SoDangKy = reader.GetString(reader.GetOrdinal("SoDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("HanDung"))) entity.HanDung = reader.GetDateTime(reader.GetOrdinal("HanDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonVitinhSoLuong"))) entity.DonVitinhSoLuong = reader.GetString(reader.GetOrdinal("DonVitinhSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("CongDung"))) entity.CongDung = reader.GetString(reader.GetOrdinal("CongDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoKhoiLuongHoatChatGayNghien"))) entity.TongSoKhoiLuongHoatChatGayNghien = reader.GetDecimal(reader.GetOrdinal("TongSoKhoiLuongHoatChatGayNghien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonVitinhKhoiLuong"))) entity.DonVitinhKhoiLuong = reader.GetString(reader.GetOrdinal("DonVitinhKhoiLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiaNhapKhauVND"))) entity.GiaNhapKhauVND = reader.GetDecimal(reader.GetOrdinal("GiaNhapKhauVND"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiaBanBuonVND"))) entity.GiaBanBuonVND = reader.GetDecimal(reader.GetOrdinal("GiaBanBuonVND"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiaBanLeVND"))) entity.GiaBanLeVND = reader.GetDecimal(reader.GetOrdinal("GiaBanLeVND"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaThuongNhanXuatKhau"))) entity.MaThuongNhanXuatKhau = reader.GetString(reader.GetOrdinal("MaThuongNhanXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuongNhanXuatKhau"))) entity.TenThuongNhanXuatKhau = reader.GetString(reader.GetOrdinal("TenThuongNhanXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhXuatKhau"))) entity.MaBuuChinhXuatKhau = reader.GetString(reader.GetOrdinal("MaBuuChinhXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhaTenDuongXuatKhau"))) entity.SoNhaTenDuongXuatKhau = reader.GetString(reader.GetOrdinal("SoNhaTenDuongXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongXaXuatKhau"))) entity.PhuongXaXuatKhau = reader.GetString(reader.GetOrdinal("PhuongXaXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuanHuyenXuatKhau"))) entity.QuanHuyenXuatKhau = reader.GetString(reader.GetOrdinal("QuanHuyenXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhThanhPhoXuatKhau"))) entity.TinhThanhPhoXuatKhau = reader.GetString(reader.GetOrdinal("TinhThanhPhoXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaXuatKhau"))) entity.MaQuocGiaXuatKhau = reader.GetString(reader.GetOrdinal("MaQuocGiaXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCongTySanXuat"))) entity.MaCongTySanXuat = reader.GetString(reader.GetOrdinal("MaCongTySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCongTySanXuat"))) entity.TenCongTySanXuat = reader.GetString(reader.GetOrdinal("TenCongTySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhCongTySanXuat"))) entity.MaBuuChinhCongTySanXuat = reader.GetString(reader.GetOrdinal("MaBuuChinhCongTySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhaTenDuongCongTySanXuat"))) entity.SoNhaTenDuongCongTySanXuat = reader.GetString(reader.GetOrdinal("SoNhaTenDuongCongTySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongXaCongTySanXuat"))) entity.PhuongXaCongTySanXuat = reader.GetString(reader.GetOrdinal("PhuongXaCongTySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuanHuyenCongTySanXuat"))) entity.QuanHuyenCongTySanXuat = reader.GetString(reader.GetOrdinal("QuanHuyenCongTySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhThanhPhoCongTySanXuat"))) entity.TinhThanhPhoCongTySanXuat = reader.GetString(reader.GetOrdinal("TinhThanhPhoCongTySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaCongTySanXuat"))) entity.MaQuocGiaCongTySanXuat = reader.GetString(reader.GetOrdinal("MaQuocGiaCongTySanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCongTyCungCap"))) entity.MaCongTyCungCap = reader.GetString(reader.GetOrdinal("MaCongTyCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCongTyCungCap"))) entity.TenCongTyCungCap = reader.GetString(reader.GetOrdinal("TenCongTyCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhCongTyCungCap"))) entity.MaBuuChinhCongTyCungCap = reader.GetString(reader.GetOrdinal("MaBuuChinhCongTyCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhaTenDuongCongTyCungCap"))) entity.SoNhaTenDuongCongTyCungCap = reader.GetString(reader.GetOrdinal("SoNhaTenDuongCongTyCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongXaCongTyCungCap"))) entity.PhuongXaCongTyCungCap = reader.GetString(reader.GetOrdinal("PhuongXaCongTyCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuanHuyenCongTyCungCap"))) entity.QuanHuyenCongTyCungCap = reader.GetString(reader.GetOrdinal("QuanHuyenCongTyCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhThanhPhoCongTyCungCap"))) entity.TinhThanhPhoCongTyCungCap = reader.GetString(reader.GetOrdinal("TinhThanhPhoCongTyCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaCongTyCungCap"))) entity.MaQuocGiaCongTyCungCap = reader.GetString(reader.GetOrdinal("MaQuocGiaCongTyCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUyThac"))) entity.MaDonViUyThac = reader.GetString(reader.GetOrdinal("MaDonViUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUyThac"))) entity.TenDonViUyThac = reader.GetString(reader.GetOrdinal("TenDonViUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhDonViUyThac"))) entity.MaBuuChinhDonViUyThac = reader.GetString(reader.GetOrdinal("MaBuuChinhDonViUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhaTenDuongDonViUyThac"))) entity.SoNhaTenDuongDonViUyThac = reader.GetString(reader.GetOrdinal("SoNhaTenDuongDonViUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongXaDonViUyThac"))) entity.PhuongXaDonViUyThac = reader.GetString(reader.GetOrdinal("PhuongXaDonViUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuanHuyenDonViUyThac"))) entity.QuanHuyenDonViUyThac = reader.GetString(reader.GetOrdinal("QuanHuyenDonViUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhThanhPhoDonViUyThac"))) entity.TinhThanhPhoDonViUyThac = reader.GetString(reader.GetOrdinal("TinhThanhPhoDonViUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaDonViUyThac"))) entity.MaQuocGiaDonViUyThac = reader.GetString(reader.GetOrdinal("MaQuocGiaDonViUyThac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTonKhoKyTruoc"))) entity.LuongTonKhoKyTruoc = reader.GetDecimal(reader.GetOrdinal("LuongTonKhoKyTruoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhLuongTonKyTruoc"))) entity.DonViTinhLuongTonKyTruoc = reader.GetString(reader.GetOrdinal("DonViTinhLuongTonKyTruoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhapTrongKy"))) entity.LuongNhapTrongKy = reader.GetDecimal(reader.GetOrdinal("LuongNhapTrongKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSo"))) entity.TongSo = reader.GetDecimal(reader.GetOrdinal("TongSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoXuatTrongKy"))) entity.TongSoXuatTrongKy = reader.GetDecimal(reader.GetOrdinal("TongSoXuatTrongKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTonKhoDenNgay"))) entity.SoLuongTonKhoDenNgay = reader.GetDecimal(reader.GetOrdinal("SoLuongTonKhoDenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuHao"))) entity.HuHao = reader.GetDecimal(reader.GetOrdinal("HuHao"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuChiTiet"))) entity.GhiChuChiTiet = reader.GetString(reader.GetOrdinal("GhiChuChiTiet"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangGiayPhep_SMA> collection, long id)
        {
            foreach (KDT_VNACC_HangGiayPhep_SMA item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangGiayPhep_SMA VALUES(@GiayPhep_ID, @TenThuocQuyCachDongGoi, @MaSoHangHoa, @TenHoatChatGayNghien, @HoatChat, @TieuChuanChatLuong, @SoDangKy, @HanDung, @SoLuong, @DonVitinhSoLuong, @CongDung, @TongSoKhoiLuongHoatChatGayNghien, @DonVitinhKhoiLuong, @GiaNhapKhauVND, @GiaBanBuonVND, @GiaBanLeVND, @MaThuongNhanXuatKhau, @TenThuongNhanXuatKhau, @MaBuuChinhXuatKhau, @SoNhaTenDuongXuatKhau, @PhuongXaXuatKhau, @QuanHuyenXuatKhau, @TinhThanhPhoXuatKhau, @MaQuocGiaXuatKhau, @MaCongTySanXuat, @TenCongTySanXuat, @MaBuuChinhCongTySanXuat, @SoNhaTenDuongCongTySanXuat, @PhuongXaCongTySanXuat, @QuanHuyenCongTySanXuat, @TinhThanhPhoCongTySanXuat, @MaQuocGiaCongTySanXuat, @MaCongTyCungCap, @TenCongTyCungCap, @MaBuuChinhCongTyCungCap, @SoNhaTenDuongCongTyCungCap, @PhuongXaCongTyCungCap, @QuanHuyenCongTyCungCap, @TinhThanhPhoCongTyCungCap, @MaQuocGiaCongTyCungCap, @MaDonViUyThac, @TenDonViUyThac, @MaBuuChinhDonViUyThac, @SoNhaTenDuongDonViUyThac, @PhuongXaDonViUyThac, @QuanHuyenDonViUyThac, @TinhThanhPhoDonViUyThac, @MaQuocGiaDonViUyThac, @LuongTonKhoKyTruoc, @DonViTinhLuongTonKyTruoc, @LuongNhapTrongKy, @TongSo, @TongSoXuatTrongKy, @SoLuongTonKhoDenNgay, @HuHao, @GhiChuChiTiet, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HangGiayPhep_SMA SET GiayPhep_ID = @GiayPhep_ID, TenThuocQuyCachDongGoi = @TenThuocQuyCachDongGoi, MaSoHangHoa = @MaSoHangHoa, TenHoatChatGayNghien = @TenHoatChatGayNghien, HoatChat = @HoatChat, TieuChuanChatLuong = @TieuChuanChatLuong, SoDangKy = @SoDangKy, HanDung = @HanDung, SoLuong = @SoLuong, DonVitinhSoLuong = @DonVitinhSoLuong, CongDung = @CongDung, TongSoKhoiLuongHoatChatGayNghien = @TongSoKhoiLuongHoatChatGayNghien, DonVitinhKhoiLuong = @DonVitinhKhoiLuong, GiaNhapKhauVND = @GiaNhapKhauVND, GiaBanBuonVND = @GiaBanBuonVND, GiaBanLeVND = @GiaBanLeVND, MaThuongNhanXuatKhau = @MaThuongNhanXuatKhau, TenThuongNhanXuatKhau = @TenThuongNhanXuatKhau, MaBuuChinhXuatKhau = @MaBuuChinhXuatKhau, SoNhaTenDuongXuatKhau = @SoNhaTenDuongXuatKhau, PhuongXaXuatKhau = @PhuongXaXuatKhau, QuanHuyenXuatKhau = @QuanHuyenXuatKhau, TinhThanhPhoXuatKhau = @TinhThanhPhoXuatKhau, MaQuocGiaXuatKhau = @MaQuocGiaXuatKhau, MaCongTySanXuat = @MaCongTySanXuat, TenCongTySanXuat = @TenCongTySanXuat, MaBuuChinhCongTySanXuat = @MaBuuChinhCongTySanXuat, SoNhaTenDuongCongTySanXuat = @SoNhaTenDuongCongTySanXuat, PhuongXaCongTySanXuat = @PhuongXaCongTySanXuat, QuanHuyenCongTySanXuat = @QuanHuyenCongTySanXuat, TinhThanhPhoCongTySanXuat = @TinhThanhPhoCongTySanXuat, MaQuocGiaCongTySanXuat = @MaQuocGiaCongTySanXuat, MaCongTyCungCap = @MaCongTyCungCap, TenCongTyCungCap = @TenCongTyCungCap, MaBuuChinhCongTyCungCap = @MaBuuChinhCongTyCungCap, SoNhaTenDuongCongTyCungCap = @SoNhaTenDuongCongTyCungCap, PhuongXaCongTyCungCap = @PhuongXaCongTyCungCap, QuanHuyenCongTyCungCap = @QuanHuyenCongTyCungCap, TinhThanhPhoCongTyCungCap = @TinhThanhPhoCongTyCungCap, MaQuocGiaCongTyCungCap = @MaQuocGiaCongTyCungCap, MaDonViUyThac = @MaDonViUyThac, TenDonViUyThac = @TenDonViUyThac, MaBuuChinhDonViUyThac = @MaBuuChinhDonViUyThac, SoNhaTenDuongDonViUyThac = @SoNhaTenDuongDonViUyThac, PhuongXaDonViUyThac = @PhuongXaDonViUyThac, QuanHuyenDonViUyThac = @QuanHuyenDonViUyThac, TinhThanhPhoDonViUyThac = @TinhThanhPhoDonViUyThac, MaQuocGiaDonViUyThac = @MaQuocGiaDonViUyThac, LuongTonKhoKyTruoc = @LuongTonKhoKyTruoc, DonViTinhLuongTonKyTruoc = @DonViTinhLuongTonKyTruoc, LuongNhapTrongKy = @LuongNhapTrongKy, TongSo = @TongSo, TongSoXuatTrongKy = @TongSoXuatTrongKy, SoLuongTonKhoDenNgay = @SoLuongTonKhoDenNgay, HuHao = @HuHao, GhiChuChiTiet = @GhiChuChiTiet, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangGiayPhep_SMA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuocQuyCachDongGoi", SqlDbType.NVarChar, "TenThuocQuyCachDongGoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHoatChatGayNghien", SqlDbType.NVarChar, "TenHoatChatGayNghien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoatChat", SqlDbType.VarChar, "HoatChat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuChuanChatLuong", SqlDbType.VarChar, "TieuChuanChatLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKy", SqlDbType.VarChar, "SoDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HanDung", SqlDbType.DateTime, "HanDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, "DonVitinhSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CongDung", SqlDbType.VarChar, "CongDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoKhoiLuongHoatChatGayNghien", SqlDbType.Decimal, "TongSoKhoiLuongHoatChatGayNghien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, "DonVitinhKhoiLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaNhapKhauVND", SqlDbType.Decimal, "GiaNhapKhauVND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaBanBuonVND", SqlDbType.Decimal, "GiaBanBuonVND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaBanLeVND", SqlDbType.Decimal, "GiaBanLeVND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCongTySanXuat", SqlDbType.VarChar, "MaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCongTySanXuat", SqlDbType.NVarChar, "TenCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhCongTySanXuat", SqlDbType.VarChar, "MaBuuChinhCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongCongTySanXuat", SqlDbType.VarChar, "SoNhaTenDuongCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaCongTySanXuat", SqlDbType.NVarChar, "PhuongXaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenCongTySanXuat", SqlDbType.NVarChar, "QuanHuyenCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoCongTySanXuat", SqlDbType.NVarChar, "TinhThanhPhoCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaCongTySanXuat", SqlDbType.VarChar, "MaQuocGiaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCongTyCungCap", SqlDbType.VarChar, "MaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCongTyCungCap", SqlDbType.NVarChar, "TenCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhCongTyCungCap", SqlDbType.VarChar, "MaBuuChinhCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongCongTyCungCap", SqlDbType.VarChar, "SoNhaTenDuongCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaCongTyCungCap", SqlDbType.NVarChar, "PhuongXaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenCongTyCungCap", SqlDbType.NVarChar, "QuanHuyenCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoCongTyCungCap", SqlDbType.NVarChar, "TinhThanhPhoCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaCongTyCungCap", SqlDbType.VarChar, "MaQuocGiaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViUyThac", SqlDbType.VarChar, "MaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViUyThac", SqlDbType.NVarChar, "TenDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDonViUyThac", SqlDbType.VarChar, "MaBuuChinhDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongDonViUyThac", SqlDbType.VarChar, "SoNhaTenDuongDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaDonViUyThac", SqlDbType.NVarChar, "PhuongXaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenDonViUyThac", SqlDbType.NVarChar, "QuanHuyenDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoDonViUyThac", SqlDbType.NVarChar, "TinhThanhPhoDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaDonViUyThac", SqlDbType.VarChar, "MaQuocGiaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonKhoKyTruoc", SqlDbType.Decimal, "LuongTonKhoKyTruoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhLuongTonKyTruoc", SqlDbType.VarChar, "DonViTinhLuongTonKyTruoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, "LuongNhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSo", SqlDbType.Decimal, "TongSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoXuatTrongKy", SqlDbType.Decimal, "TongSoXuatTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTonKhoDenNgay", SqlDbType.Decimal, "SoLuongTonKhoDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuHao", SqlDbType.Decimal, "HuHao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuChiTiet", SqlDbType.NVarChar, "GhiChuChiTiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuocQuyCachDongGoi", SqlDbType.NVarChar, "TenThuocQuyCachDongGoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHoatChatGayNghien", SqlDbType.NVarChar, "TenHoatChatGayNghien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoatChat", SqlDbType.VarChar, "HoatChat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuChuanChatLuong", SqlDbType.VarChar, "TieuChuanChatLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKy", SqlDbType.VarChar, "SoDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HanDung", SqlDbType.DateTime, "HanDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, "DonVitinhSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CongDung", SqlDbType.VarChar, "CongDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoKhoiLuongHoatChatGayNghien", SqlDbType.Decimal, "TongSoKhoiLuongHoatChatGayNghien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, "DonVitinhKhoiLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaNhapKhauVND", SqlDbType.Decimal, "GiaNhapKhauVND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaBanBuonVND", SqlDbType.Decimal, "GiaBanBuonVND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaBanLeVND", SqlDbType.Decimal, "GiaBanLeVND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCongTySanXuat", SqlDbType.VarChar, "MaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCongTySanXuat", SqlDbType.NVarChar, "TenCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhCongTySanXuat", SqlDbType.VarChar, "MaBuuChinhCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongCongTySanXuat", SqlDbType.VarChar, "SoNhaTenDuongCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaCongTySanXuat", SqlDbType.NVarChar, "PhuongXaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenCongTySanXuat", SqlDbType.NVarChar, "QuanHuyenCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoCongTySanXuat", SqlDbType.NVarChar, "TinhThanhPhoCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaCongTySanXuat", SqlDbType.VarChar, "MaQuocGiaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCongTyCungCap", SqlDbType.VarChar, "MaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCongTyCungCap", SqlDbType.NVarChar, "TenCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhCongTyCungCap", SqlDbType.VarChar, "MaBuuChinhCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongCongTyCungCap", SqlDbType.VarChar, "SoNhaTenDuongCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaCongTyCungCap", SqlDbType.NVarChar, "PhuongXaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenCongTyCungCap", SqlDbType.NVarChar, "QuanHuyenCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoCongTyCungCap", SqlDbType.NVarChar, "TinhThanhPhoCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaCongTyCungCap", SqlDbType.VarChar, "MaQuocGiaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViUyThac", SqlDbType.VarChar, "MaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViUyThac", SqlDbType.NVarChar, "TenDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDonViUyThac", SqlDbType.VarChar, "MaBuuChinhDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongDonViUyThac", SqlDbType.VarChar, "SoNhaTenDuongDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaDonViUyThac", SqlDbType.NVarChar, "PhuongXaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenDonViUyThac", SqlDbType.NVarChar, "QuanHuyenDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoDonViUyThac", SqlDbType.NVarChar, "TinhThanhPhoDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaDonViUyThac", SqlDbType.VarChar, "MaQuocGiaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonKhoKyTruoc", SqlDbType.Decimal, "LuongTonKhoKyTruoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhLuongTonKyTruoc", SqlDbType.VarChar, "DonViTinhLuongTonKyTruoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, "LuongNhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSo", SqlDbType.Decimal, "TongSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoXuatTrongKy", SqlDbType.Decimal, "TongSoXuatTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTonKhoDenNgay", SqlDbType.Decimal, "SoLuongTonKhoDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuHao", SqlDbType.Decimal, "HuHao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuChiTiet", SqlDbType.NVarChar, "GhiChuChiTiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangGiayPhep_SMA VALUES(@GiayPhep_ID, @TenThuocQuyCachDongGoi, @MaSoHangHoa, @TenHoatChatGayNghien, @HoatChat, @TieuChuanChatLuong, @SoDangKy, @HanDung, @SoLuong, @DonVitinhSoLuong, @CongDung, @TongSoKhoiLuongHoatChatGayNghien, @DonVitinhKhoiLuong, @GiaNhapKhauVND, @GiaBanBuonVND, @GiaBanLeVND, @MaThuongNhanXuatKhau, @TenThuongNhanXuatKhau, @MaBuuChinhXuatKhau, @SoNhaTenDuongXuatKhau, @PhuongXaXuatKhau, @QuanHuyenXuatKhau, @TinhThanhPhoXuatKhau, @MaQuocGiaXuatKhau, @MaCongTySanXuat, @TenCongTySanXuat, @MaBuuChinhCongTySanXuat, @SoNhaTenDuongCongTySanXuat, @PhuongXaCongTySanXuat, @QuanHuyenCongTySanXuat, @TinhThanhPhoCongTySanXuat, @MaQuocGiaCongTySanXuat, @MaCongTyCungCap, @TenCongTyCungCap, @MaBuuChinhCongTyCungCap, @SoNhaTenDuongCongTyCungCap, @PhuongXaCongTyCungCap, @QuanHuyenCongTyCungCap, @TinhThanhPhoCongTyCungCap, @MaQuocGiaCongTyCungCap, @MaDonViUyThac, @TenDonViUyThac, @MaBuuChinhDonViUyThac, @SoNhaTenDuongDonViUyThac, @PhuongXaDonViUyThac, @QuanHuyenDonViUyThac, @TinhThanhPhoDonViUyThac, @MaQuocGiaDonViUyThac, @LuongTonKhoKyTruoc, @DonViTinhLuongTonKyTruoc, @LuongNhapTrongKy, @TongSo, @TongSoXuatTrongKy, @SoLuongTonKhoDenNgay, @HuHao, @GhiChuChiTiet, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HangGiayPhep_SMA SET GiayPhep_ID = @GiayPhep_ID, TenThuocQuyCachDongGoi = @TenThuocQuyCachDongGoi, MaSoHangHoa = @MaSoHangHoa, TenHoatChatGayNghien = @TenHoatChatGayNghien, HoatChat = @HoatChat, TieuChuanChatLuong = @TieuChuanChatLuong, SoDangKy = @SoDangKy, HanDung = @HanDung, SoLuong = @SoLuong, DonVitinhSoLuong = @DonVitinhSoLuong, CongDung = @CongDung, TongSoKhoiLuongHoatChatGayNghien = @TongSoKhoiLuongHoatChatGayNghien, DonVitinhKhoiLuong = @DonVitinhKhoiLuong, GiaNhapKhauVND = @GiaNhapKhauVND, GiaBanBuonVND = @GiaBanBuonVND, GiaBanLeVND = @GiaBanLeVND, MaThuongNhanXuatKhau = @MaThuongNhanXuatKhau, TenThuongNhanXuatKhau = @TenThuongNhanXuatKhau, MaBuuChinhXuatKhau = @MaBuuChinhXuatKhau, SoNhaTenDuongXuatKhau = @SoNhaTenDuongXuatKhau, PhuongXaXuatKhau = @PhuongXaXuatKhau, QuanHuyenXuatKhau = @QuanHuyenXuatKhau, TinhThanhPhoXuatKhau = @TinhThanhPhoXuatKhau, MaQuocGiaXuatKhau = @MaQuocGiaXuatKhau, MaCongTySanXuat = @MaCongTySanXuat, TenCongTySanXuat = @TenCongTySanXuat, MaBuuChinhCongTySanXuat = @MaBuuChinhCongTySanXuat, SoNhaTenDuongCongTySanXuat = @SoNhaTenDuongCongTySanXuat, PhuongXaCongTySanXuat = @PhuongXaCongTySanXuat, QuanHuyenCongTySanXuat = @QuanHuyenCongTySanXuat, TinhThanhPhoCongTySanXuat = @TinhThanhPhoCongTySanXuat, MaQuocGiaCongTySanXuat = @MaQuocGiaCongTySanXuat, MaCongTyCungCap = @MaCongTyCungCap, TenCongTyCungCap = @TenCongTyCungCap, MaBuuChinhCongTyCungCap = @MaBuuChinhCongTyCungCap, SoNhaTenDuongCongTyCungCap = @SoNhaTenDuongCongTyCungCap, PhuongXaCongTyCungCap = @PhuongXaCongTyCungCap, QuanHuyenCongTyCungCap = @QuanHuyenCongTyCungCap, TinhThanhPhoCongTyCungCap = @TinhThanhPhoCongTyCungCap, MaQuocGiaCongTyCungCap = @MaQuocGiaCongTyCungCap, MaDonViUyThac = @MaDonViUyThac, TenDonViUyThac = @TenDonViUyThac, MaBuuChinhDonViUyThac = @MaBuuChinhDonViUyThac, SoNhaTenDuongDonViUyThac = @SoNhaTenDuongDonViUyThac, PhuongXaDonViUyThac = @PhuongXaDonViUyThac, QuanHuyenDonViUyThac = @QuanHuyenDonViUyThac, TinhThanhPhoDonViUyThac = @TinhThanhPhoDonViUyThac, MaQuocGiaDonViUyThac = @MaQuocGiaDonViUyThac, LuongTonKhoKyTruoc = @LuongTonKhoKyTruoc, DonViTinhLuongTonKyTruoc = @DonViTinhLuongTonKyTruoc, LuongNhapTrongKy = @LuongNhapTrongKy, TongSo = @TongSo, TongSoXuatTrongKy = @TongSoXuatTrongKy, SoLuongTonKhoDenNgay = @SoLuongTonKhoDenNgay, HuHao = @HuHao, GhiChuChiTiet = @GhiChuChiTiet, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangGiayPhep_SMA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuocQuyCachDongGoi", SqlDbType.NVarChar, "TenThuocQuyCachDongGoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHoatChatGayNghien", SqlDbType.NVarChar, "TenHoatChatGayNghien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoatChat", SqlDbType.VarChar, "HoatChat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuChuanChatLuong", SqlDbType.VarChar, "TieuChuanChatLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKy", SqlDbType.VarChar, "SoDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HanDung", SqlDbType.DateTime, "HanDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, "DonVitinhSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CongDung", SqlDbType.VarChar, "CongDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoKhoiLuongHoatChatGayNghien", SqlDbType.Decimal, "TongSoKhoiLuongHoatChatGayNghien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, "DonVitinhKhoiLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaNhapKhauVND", SqlDbType.Decimal, "GiaNhapKhauVND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaBanBuonVND", SqlDbType.Decimal, "GiaBanBuonVND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaBanLeVND", SqlDbType.Decimal, "GiaBanLeVND", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCongTySanXuat", SqlDbType.VarChar, "MaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCongTySanXuat", SqlDbType.NVarChar, "TenCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhCongTySanXuat", SqlDbType.VarChar, "MaBuuChinhCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongCongTySanXuat", SqlDbType.VarChar, "SoNhaTenDuongCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaCongTySanXuat", SqlDbType.NVarChar, "PhuongXaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenCongTySanXuat", SqlDbType.NVarChar, "QuanHuyenCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoCongTySanXuat", SqlDbType.NVarChar, "TinhThanhPhoCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaCongTySanXuat", SqlDbType.VarChar, "MaQuocGiaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCongTyCungCap", SqlDbType.VarChar, "MaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCongTyCungCap", SqlDbType.NVarChar, "TenCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhCongTyCungCap", SqlDbType.VarChar, "MaBuuChinhCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongCongTyCungCap", SqlDbType.VarChar, "SoNhaTenDuongCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaCongTyCungCap", SqlDbType.NVarChar, "PhuongXaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenCongTyCungCap", SqlDbType.NVarChar, "QuanHuyenCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoCongTyCungCap", SqlDbType.NVarChar, "TinhThanhPhoCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaCongTyCungCap", SqlDbType.VarChar, "MaQuocGiaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViUyThac", SqlDbType.VarChar, "MaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViUyThac", SqlDbType.NVarChar, "TenDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDonViUyThac", SqlDbType.VarChar, "MaBuuChinhDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongDonViUyThac", SqlDbType.VarChar, "SoNhaTenDuongDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaDonViUyThac", SqlDbType.NVarChar, "PhuongXaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenDonViUyThac", SqlDbType.NVarChar, "QuanHuyenDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoDonViUyThac", SqlDbType.NVarChar, "TinhThanhPhoDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaDonViUyThac", SqlDbType.VarChar, "MaQuocGiaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonKhoKyTruoc", SqlDbType.Decimal, "LuongTonKhoKyTruoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhLuongTonKyTruoc", SqlDbType.VarChar, "DonViTinhLuongTonKyTruoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, "LuongNhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSo", SqlDbType.Decimal, "TongSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoXuatTrongKy", SqlDbType.Decimal, "TongSoXuatTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTonKhoDenNgay", SqlDbType.Decimal, "SoLuongTonKhoDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuHao", SqlDbType.Decimal, "HuHao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuChiTiet", SqlDbType.NVarChar, "GhiChuChiTiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuocQuyCachDongGoi", SqlDbType.NVarChar, "TenThuocQuyCachDongGoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHoatChatGayNghien", SqlDbType.NVarChar, "TenHoatChatGayNghien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoatChat", SqlDbType.VarChar, "HoatChat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuChuanChatLuong", SqlDbType.VarChar, "TieuChuanChatLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKy", SqlDbType.VarChar, "SoDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HanDung", SqlDbType.DateTime, "HanDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, "DonVitinhSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CongDung", SqlDbType.VarChar, "CongDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoKhoiLuongHoatChatGayNghien", SqlDbType.Decimal, "TongSoKhoiLuongHoatChatGayNghien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, "DonVitinhKhoiLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaNhapKhauVND", SqlDbType.Decimal, "GiaNhapKhauVND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaBanBuonVND", SqlDbType.Decimal, "GiaBanBuonVND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaBanLeVND", SqlDbType.Decimal, "GiaBanLeVND", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCongTySanXuat", SqlDbType.VarChar, "MaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCongTySanXuat", SqlDbType.NVarChar, "TenCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhCongTySanXuat", SqlDbType.VarChar, "MaBuuChinhCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongCongTySanXuat", SqlDbType.VarChar, "SoNhaTenDuongCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaCongTySanXuat", SqlDbType.NVarChar, "PhuongXaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenCongTySanXuat", SqlDbType.NVarChar, "QuanHuyenCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoCongTySanXuat", SqlDbType.NVarChar, "TinhThanhPhoCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaCongTySanXuat", SqlDbType.VarChar, "MaQuocGiaCongTySanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCongTyCungCap", SqlDbType.VarChar, "MaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCongTyCungCap", SqlDbType.NVarChar, "TenCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhCongTyCungCap", SqlDbType.VarChar, "MaBuuChinhCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongCongTyCungCap", SqlDbType.VarChar, "SoNhaTenDuongCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaCongTyCungCap", SqlDbType.NVarChar, "PhuongXaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenCongTyCungCap", SqlDbType.NVarChar, "QuanHuyenCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoCongTyCungCap", SqlDbType.NVarChar, "TinhThanhPhoCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaCongTyCungCap", SqlDbType.VarChar, "MaQuocGiaCongTyCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViUyThac", SqlDbType.VarChar, "MaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViUyThac", SqlDbType.NVarChar, "TenDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDonViUyThac", SqlDbType.VarChar, "MaBuuChinhDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongDonViUyThac", SqlDbType.VarChar, "SoNhaTenDuongDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaDonViUyThac", SqlDbType.NVarChar, "PhuongXaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenDonViUyThac", SqlDbType.NVarChar, "QuanHuyenDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoDonViUyThac", SqlDbType.NVarChar, "TinhThanhPhoDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaDonViUyThac", SqlDbType.VarChar, "MaQuocGiaDonViUyThac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonKhoKyTruoc", SqlDbType.Decimal, "LuongTonKhoKyTruoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhLuongTonKyTruoc", SqlDbType.VarChar, "DonViTinhLuongTonKyTruoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, "LuongNhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSo", SqlDbType.Decimal, "TongSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoXuatTrongKy", SqlDbType.Decimal, "TongSoXuatTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTonKhoDenNgay", SqlDbType.Decimal, "SoLuongTonKhoDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuHao", SqlDbType.Decimal, "HuHao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuChiTiet", SqlDbType.NVarChar, "GhiChuChiTiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangGiayPhep_SMA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangGiayPhep_SMA> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangGiayPhep_SMA> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangGiayPhep_SMA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_HangGiayPhep_SMA> SelectCollectionBy_GiayPhep_ID(long giayPhep_ID)
		{
            IDataReader reader = SelectReaderBy_GiayPhep_ID(giayPhep_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangGiayPhep_SMA(long giayPhep_ID, string tenThuocQuyCachDongGoi, string maSoHangHoa, string tenHoatChatGayNghien, string hoatChat, string tieuChuanChatLuong, string soDangKy, DateTime hanDung, decimal soLuong, string donVitinhSoLuong, string congDung, decimal tongSoKhoiLuongHoatChatGayNghien, string donVitinhKhoiLuong, decimal giaNhapKhauVND, decimal giaBanBuonVND, decimal giaBanLeVND, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string maCongTySanXuat, string tenCongTySanXuat, string maBuuChinhCongTySanXuat, string soNhaTenDuongCongTySanXuat, string phuongXaCongTySanXuat, string quanHuyenCongTySanXuat, string tinhThanhPhoCongTySanXuat, string maQuocGiaCongTySanXuat, string maCongTyCungCap, string tenCongTyCungCap, string maBuuChinhCongTyCungCap, string soNhaTenDuongCongTyCungCap, string phuongXaCongTyCungCap, string quanHuyenCongTyCungCap, string tinhThanhPhoCongTyCungCap, string maQuocGiaCongTyCungCap, string maDonViUyThac, string tenDonViUyThac, string maBuuChinhDonViUyThac, string soNhaTenDuongDonViUyThac, string phuongXaDonViUyThac, string quanHuyenDonViUyThac, string tinhThanhPhoDonViUyThac, string maQuocGiaDonViUyThac, decimal luongTonKhoKyTruoc, string donViTinhLuongTonKyTruoc, decimal luongNhapTrongKy, decimal tongSo, decimal tongSoXuatTrongKy, decimal soLuongTonKhoDenNgay, decimal huHao, string ghiChuChiTiet, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep_SMA entity = new KDT_VNACC_HangGiayPhep_SMA();	
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenThuocQuyCachDongGoi = tenThuocQuyCachDongGoi;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.TenHoatChatGayNghien = tenHoatChatGayNghien;
			entity.HoatChat = hoatChat;
			entity.TieuChuanChatLuong = tieuChuanChatLuong;
			entity.SoDangKy = soDangKy;
			entity.HanDung = hanDung;
			entity.SoLuong = soLuong;
			entity.DonVitinhSoLuong = donVitinhSoLuong;
			entity.CongDung = congDung;
			entity.TongSoKhoiLuongHoatChatGayNghien = tongSoKhoiLuongHoatChatGayNghien;
			entity.DonVitinhKhoiLuong = donVitinhKhoiLuong;
			entity.GiaNhapKhauVND = giaNhapKhauVND;
			entity.GiaBanBuonVND = giaBanBuonVND;
			entity.GiaBanLeVND = giaBanLeVND;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.MaCongTySanXuat = maCongTySanXuat;
			entity.TenCongTySanXuat = tenCongTySanXuat;
			entity.MaBuuChinhCongTySanXuat = maBuuChinhCongTySanXuat;
			entity.SoNhaTenDuongCongTySanXuat = soNhaTenDuongCongTySanXuat;
			entity.PhuongXaCongTySanXuat = phuongXaCongTySanXuat;
			entity.QuanHuyenCongTySanXuat = quanHuyenCongTySanXuat;
			entity.TinhThanhPhoCongTySanXuat = tinhThanhPhoCongTySanXuat;
			entity.MaQuocGiaCongTySanXuat = maQuocGiaCongTySanXuat;
			entity.MaCongTyCungCap = maCongTyCungCap;
			entity.TenCongTyCungCap = tenCongTyCungCap;
			entity.MaBuuChinhCongTyCungCap = maBuuChinhCongTyCungCap;
			entity.SoNhaTenDuongCongTyCungCap = soNhaTenDuongCongTyCungCap;
			entity.PhuongXaCongTyCungCap = phuongXaCongTyCungCap;
			entity.QuanHuyenCongTyCungCap = quanHuyenCongTyCungCap;
			entity.TinhThanhPhoCongTyCungCap = tinhThanhPhoCongTyCungCap;
			entity.MaQuocGiaCongTyCungCap = maQuocGiaCongTyCungCap;
			entity.MaDonViUyThac = maDonViUyThac;
			entity.TenDonViUyThac = tenDonViUyThac;
			entity.MaBuuChinhDonViUyThac = maBuuChinhDonViUyThac;
			entity.SoNhaTenDuongDonViUyThac = soNhaTenDuongDonViUyThac;
			entity.PhuongXaDonViUyThac = phuongXaDonViUyThac;
			entity.QuanHuyenDonViUyThac = quanHuyenDonViUyThac;
			entity.TinhThanhPhoDonViUyThac = tinhThanhPhoDonViUyThac;
			entity.MaQuocGiaDonViUyThac = maQuocGiaDonViUyThac;
			entity.LuongTonKhoKyTruoc = luongTonKhoKyTruoc;
			entity.DonViTinhLuongTonKyTruoc = donViTinhLuongTonKyTruoc;
			entity.LuongNhapTrongKy = luongNhapTrongKy;
			entity.TongSo = tongSo;
			entity.TongSoXuatTrongKy = tongSoXuatTrongKy;
			entity.SoLuongTonKhoDenNgay = soLuongTonKhoDenNgay;
			entity.HuHao = huHao;
			entity.GhiChuChiTiet = ghiChuChiTiet;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenThuocQuyCachDongGoi", SqlDbType.NVarChar, TenThuocQuyCachDongGoi);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@TenHoatChatGayNghien", SqlDbType.NVarChar, TenHoatChatGayNghien);
			db.AddInParameter(dbCommand, "@HoatChat", SqlDbType.VarChar, HoatChat);
			db.AddInParameter(dbCommand, "@TieuChuanChatLuong", SqlDbType.VarChar, TieuChuanChatLuong);
			db.AddInParameter(dbCommand, "@SoDangKy", SqlDbType.VarChar, SoDangKy);
			db.AddInParameter(dbCommand, "@HanDung", SqlDbType.DateTime, HanDung.Year <= 1753 ? DBNull.Value : (object) HanDung);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, DonVitinhSoLuong);
			db.AddInParameter(dbCommand, "@CongDung", SqlDbType.VarChar, CongDung);
			db.AddInParameter(dbCommand, "@TongSoKhoiLuongHoatChatGayNghien", SqlDbType.Decimal, TongSoKhoiLuongHoatChatGayNghien);
			db.AddInParameter(dbCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, DonVitinhKhoiLuong);
			db.AddInParameter(dbCommand, "@GiaNhapKhauVND", SqlDbType.Decimal, GiaNhapKhauVND);
			db.AddInParameter(dbCommand, "@GiaBanBuonVND", SqlDbType.Decimal, GiaBanBuonVND);
			db.AddInParameter(dbCommand, "@GiaBanLeVND", SqlDbType.Decimal, GiaBanLeVND);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@MaCongTySanXuat", SqlDbType.VarChar, MaCongTySanXuat);
			db.AddInParameter(dbCommand, "@TenCongTySanXuat", SqlDbType.NVarChar, TenCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaBuuChinhCongTySanXuat", SqlDbType.VarChar, MaBuuChinhCongTySanXuat);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongCongTySanXuat", SqlDbType.VarChar, SoNhaTenDuongCongTySanXuat);
			db.AddInParameter(dbCommand, "@PhuongXaCongTySanXuat", SqlDbType.NVarChar, PhuongXaCongTySanXuat);
			db.AddInParameter(dbCommand, "@QuanHuyenCongTySanXuat", SqlDbType.NVarChar, QuanHuyenCongTySanXuat);
			db.AddInParameter(dbCommand, "@TinhThanhPhoCongTySanXuat", SqlDbType.NVarChar, TinhThanhPhoCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaQuocGiaCongTySanXuat", SqlDbType.VarChar, MaQuocGiaCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaCongTyCungCap", SqlDbType.VarChar, MaCongTyCungCap);
			db.AddInParameter(dbCommand, "@TenCongTyCungCap", SqlDbType.NVarChar, TenCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaBuuChinhCongTyCungCap", SqlDbType.VarChar, MaBuuChinhCongTyCungCap);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongCongTyCungCap", SqlDbType.VarChar, SoNhaTenDuongCongTyCungCap);
			db.AddInParameter(dbCommand, "@PhuongXaCongTyCungCap", SqlDbType.NVarChar, PhuongXaCongTyCungCap);
			db.AddInParameter(dbCommand, "@QuanHuyenCongTyCungCap", SqlDbType.NVarChar, QuanHuyenCongTyCungCap);
			db.AddInParameter(dbCommand, "@TinhThanhPhoCongTyCungCap", SqlDbType.NVarChar, TinhThanhPhoCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaQuocGiaCongTyCungCap", SqlDbType.VarChar, MaQuocGiaCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaDonViUyThac", SqlDbType.VarChar, MaDonViUyThac);
			db.AddInParameter(dbCommand, "@TenDonViUyThac", SqlDbType.NVarChar, TenDonViUyThac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonViUyThac", SqlDbType.VarChar, MaBuuChinhDonViUyThac);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongDonViUyThac", SqlDbType.VarChar, SoNhaTenDuongDonViUyThac);
			db.AddInParameter(dbCommand, "@PhuongXaDonViUyThac", SqlDbType.NVarChar, PhuongXaDonViUyThac);
			db.AddInParameter(dbCommand, "@QuanHuyenDonViUyThac", SqlDbType.NVarChar, QuanHuyenDonViUyThac);
			db.AddInParameter(dbCommand, "@TinhThanhPhoDonViUyThac", SqlDbType.NVarChar, TinhThanhPhoDonViUyThac);
			db.AddInParameter(dbCommand, "@MaQuocGiaDonViUyThac", SqlDbType.VarChar, MaQuocGiaDonViUyThac);
			db.AddInParameter(dbCommand, "@LuongTonKhoKyTruoc", SqlDbType.Decimal, LuongTonKhoKyTruoc);
			db.AddInParameter(dbCommand, "@DonViTinhLuongTonKyTruoc", SqlDbType.VarChar, DonViTinhLuongTonKyTruoc);
			db.AddInParameter(dbCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, LuongNhapTrongKy);
			db.AddInParameter(dbCommand, "@TongSo", SqlDbType.Decimal, TongSo);
			db.AddInParameter(dbCommand, "@TongSoXuatTrongKy", SqlDbType.Decimal, TongSoXuatTrongKy);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoDenNgay", SqlDbType.Decimal, SoLuongTonKhoDenNgay);
			db.AddInParameter(dbCommand, "@HuHao", SqlDbType.Decimal, HuHao);
			db.AddInParameter(dbCommand, "@GhiChuChiTiet", SqlDbType.NVarChar, GhiChuChiTiet);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangGiayPhep_SMA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep_SMA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangGiayPhep_SMA(long id, long giayPhep_ID, string tenThuocQuyCachDongGoi, string maSoHangHoa, string tenHoatChatGayNghien, string hoatChat, string tieuChuanChatLuong, string soDangKy, DateTime hanDung, decimal soLuong, string donVitinhSoLuong, string congDung, decimal tongSoKhoiLuongHoatChatGayNghien, string donVitinhKhoiLuong, decimal giaNhapKhauVND, decimal giaBanBuonVND, decimal giaBanLeVND, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string maCongTySanXuat, string tenCongTySanXuat, string maBuuChinhCongTySanXuat, string soNhaTenDuongCongTySanXuat, string phuongXaCongTySanXuat, string quanHuyenCongTySanXuat, string tinhThanhPhoCongTySanXuat, string maQuocGiaCongTySanXuat, string maCongTyCungCap, string tenCongTyCungCap, string maBuuChinhCongTyCungCap, string soNhaTenDuongCongTyCungCap, string phuongXaCongTyCungCap, string quanHuyenCongTyCungCap, string tinhThanhPhoCongTyCungCap, string maQuocGiaCongTyCungCap, string maDonViUyThac, string tenDonViUyThac, string maBuuChinhDonViUyThac, string soNhaTenDuongDonViUyThac, string phuongXaDonViUyThac, string quanHuyenDonViUyThac, string tinhThanhPhoDonViUyThac, string maQuocGiaDonViUyThac, decimal luongTonKhoKyTruoc, string donViTinhLuongTonKyTruoc, decimal luongNhapTrongKy, decimal tongSo, decimal tongSoXuatTrongKy, decimal soLuongTonKhoDenNgay, decimal huHao, string ghiChuChiTiet, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep_SMA entity = new KDT_VNACC_HangGiayPhep_SMA();			
			entity.ID = id;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenThuocQuyCachDongGoi = tenThuocQuyCachDongGoi;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.TenHoatChatGayNghien = tenHoatChatGayNghien;
			entity.HoatChat = hoatChat;
			entity.TieuChuanChatLuong = tieuChuanChatLuong;
			entity.SoDangKy = soDangKy;
			entity.HanDung = hanDung;
			entity.SoLuong = soLuong;
			entity.DonVitinhSoLuong = donVitinhSoLuong;
			entity.CongDung = congDung;
			entity.TongSoKhoiLuongHoatChatGayNghien = tongSoKhoiLuongHoatChatGayNghien;
			entity.DonVitinhKhoiLuong = donVitinhKhoiLuong;
			entity.GiaNhapKhauVND = giaNhapKhauVND;
			entity.GiaBanBuonVND = giaBanBuonVND;
			entity.GiaBanLeVND = giaBanLeVND;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.MaCongTySanXuat = maCongTySanXuat;
			entity.TenCongTySanXuat = tenCongTySanXuat;
			entity.MaBuuChinhCongTySanXuat = maBuuChinhCongTySanXuat;
			entity.SoNhaTenDuongCongTySanXuat = soNhaTenDuongCongTySanXuat;
			entity.PhuongXaCongTySanXuat = phuongXaCongTySanXuat;
			entity.QuanHuyenCongTySanXuat = quanHuyenCongTySanXuat;
			entity.TinhThanhPhoCongTySanXuat = tinhThanhPhoCongTySanXuat;
			entity.MaQuocGiaCongTySanXuat = maQuocGiaCongTySanXuat;
			entity.MaCongTyCungCap = maCongTyCungCap;
			entity.TenCongTyCungCap = tenCongTyCungCap;
			entity.MaBuuChinhCongTyCungCap = maBuuChinhCongTyCungCap;
			entity.SoNhaTenDuongCongTyCungCap = soNhaTenDuongCongTyCungCap;
			entity.PhuongXaCongTyCungCap = phuongXaCongTyCungCap;
			entity.QuanHuyenCongTyCungCap = quanHuyenCongTyCungCap;
			entity.TinhThanhPhoCongTyCungCap = tinhThanhPhoCongTyCungCap;
			entity.MaQuocGiaCongTyCungCap = maQuocGiaCongTyCungCap;
			entity.MaDonViUyThac = maDonViUyThac;
			entity.TenDonViUyThac = tenDonViUyThac;
			entity.MaBuuChinhDonViUyThac = maBuuChinhDonViUyThac;
			entity.SoNhaTenDuongDonViUyThac = soNhaTenDuongDonViUyThac;
			entity.PhuongXaDonViUyThac = phuongXaDonViUyThac;
			entity.QuanHuyenDonViUyThac = quanHuyenDonViUyThac;
			entity.TinhThanhPhoDonViUyThac = tinhThanhPhoDonViUyThac;
			entity.MaQuocGiaDonViUyThac = maQuocGiaDonViUyThac;
			entity.LuongTonKhoKyTruoc = luongTonKhoKyTruoc;
			entity.DonViTinhLuongTonKyTruoc = donViTinhLuongTonKyTruoc;
			entity.LuongNhapTrongKy = luongNhapTrongKy;
			entity.TongSo = tongSo;
			entity.TongSoXuatTrongKy = tongSoXuatTrongKy;
			entity.SoLuongTonKhoDenNgay = soLuongTonKhoDenNgay;
			entity.HuHao = huHao;
			entity.GhiChuChiTiet = ghiChuChiTiet;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenThuocQuyCachDongGoi", SqlDbType.NVarChar, TenThuocQuyCachDongGoi);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@TenHoatChatGayNghien", SqlDbType.NVarChar, TenHoatChatGayNghien);
			db.AddInParameter(dbCommand, "@HoatChat", SqlDbType.VarChar, HoatChat);
			db.AddInParameter(dbCommand, "@TieuChuanChatLuong", SqlDbType.VarChar, TieuChuanChatLuong);
			db.AddInParameter(dbCommand, "@SoDangKy", SqlDbType.VarChar, SoDangKy);
			db.AddInParameter(dbCommand, "@HanDung", SqlDbType.DateTime, HanDung.Year <= 1753 ? DBNull.Value : (object) HanDung);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, DonVitinhSoLuong);
			db.AddInParameter(dbCommand, "@CongDung", SqlDbType.VarChar, CongDung);
			db.AddInParameter(dbCommand, "@TongSoKhoiLuongHoatChatGayNghien", SqlDbType.Decimal, TongSoKhoiLuongHoatChatGayNghien);
			db.AddInParameter(dbCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, DonVitinhKhoiLuong);
			db.AddInParameter(dbCommand, "@GiaNhapKhauVND", SqlDbType.Decimal, GiaNhapKhauVND);
			db.AddInParameter(dbCommand, "@GiaBanBuonVND", SqlDbType.Decimal, GiaBanBuonVND);
			db.AddInParameter(dbCommand, "@GiaBanLeVND", SqlDbType.Decimal, GiaBanLeVND);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@MaCongTySanXuat", SqlDbType.VarChar, MaCongTySanXuat);
			db.AddInParameter(dbCommand, "@TenCongTySanXuat", SqlDbType.NVarChar, TenCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaBuuChinhCongTySanXuat", SqlDbType.VarChar, MaBuuChinhCongTySanXuat);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongCongTySanXuat", SqlDbType.VarChar, SoNhaTenDuongCongTySanXuat);
			db.AddInParameter(dbCommand, "@PhuongXaCongTySanXuat", SqlDbType.NVarChar, PhuongXaCongTySanXuat);
			db.AddInParameter(dbCommand, "@QuanHuyenCongTySanXuat", SqlDbType.NVarChar, QuanHuyenCongTySanXuat);
			db.AddInParameter(dbCommand, "@TinhThanhPhoCongTySanXuat", SqlDbType.NVarChar, TinhThanhPhoCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaQuocGiaCongTySanXuat", SqlDbType.VarChar, MaQuocGiaCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaCongTyCungCap", SqlDbType.VarChar, MaCongTyCungCap);
			db.AddInParameter(dbCommand, "@TenCongTyCungCap", SqlDbType.NVarChar, TenCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaBuuChinhCongTyCungCap", SqlDbType.VarChar, MaBuuChinhCongTyCungCap);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongCongTyCungCap", SqlDbType.VarChar, SoNhaTenDuongCongTyCungCap);
			db.AddInParameter(dbCommand, "@PhuongXaCongTyCungCap", SqlDbType.NVarChar, PhuongXaCongTyCungCap);
			db.AddInParameter(dbCommand, "@QuanHuyenCongTyCungCap", SqlDbType.NVarChar, QuanHuyenCongTyCungCap);
			db.AddInParameter(dbCommand, "@TinhThanhPhoCongTyCungCap", SqlDbType.NVarChar, TinhThanhPhoCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaQuocGiaCongTyCungCap", SqlDbType.VarChar, MaQuocGiaCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaDonViUyThac", SqlDbType.VarChar, MaDonViUyThac);
			db.AddInParameter(dbCommand, "@TenDonViUyThac", SqlDbType.NVarChar, TenDonViUyThac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonViUyThac", SqlDbType.VarChar, MaBuuChinhDonViUyThac);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongDonViUyThac", SqlDbType.VarChar, SoNhaTenDuongDonViUyThac);
			db.AddInParameter(dbCommand, "@PhuongXaDonViUyThac", SqlDbType.NVarChar, PhuongXaDonViUyThac);
			db.AddInParameter(dbCommand, "@QuanHuyenDonViUyThac", SqlDbType.NVarChar, QuanHuyenDonViUyThac);
			db.AddInParameter(dbCommand, "@TinhThanhPhoDonViUyThac", SqlDbType.NVarChar, TinhThanhPhoDonViUyThac);
			db.AddInParameter(dbCommand, "@MaQuocGiaDonViUyThac", SqlDbType.VarChar, MaQuocGiaDonViUyThac);
			db.AddInParameter(dbCommand, "@LuongTonKhoKyTruoc", SqlDbType.Decimal, LuongTonKhoKyTruoc);
			db.AddInParameter(dbCommand, "@DonViTinhLuongTonKyTruoc", SqlDbType.VarChar, DonViTinhLuongTonKyTruoc);
			db.AddInParameter(dbCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, LuongNhapTrongKy);
			db.AddInParameter(dbCommand, "@TongSo", SqlDbType.Decimal, TongSo);
			db.AddInParameter(dbCommand, "@TongSoXuatTrongKy", SqlDbType.Decimal, TongSoXuatTrongKy);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoDenNgay", SqlDbType.Decimal, SoLuongTonKhoDenNgay);
			db.AddInParameter(dbCommand, "@HuHao", SqlDbType.Decimal, HuHao);
			db.AddInParameter(dbCommand, "@GhiChuChiTiet", SqlDbType.NVarChar, GhiChuChiTiet);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangGiayPhep_SMA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep_SMA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangGiayPhep_SMA(long id, long giayPhep_ID, string tenThuocQuyCachDongGoi, string maSoHangHoa, string tenHoatChatGayNghien, string hoatChat, string tieuChuanChatLuong, string soDangKy, DateTime hanDung, decimal soLuong, string donVitinhSoLuong, string congDung, decimal tongSoKhoiLuongHoatChatGayNghien, string donVitinhKhoiLuong, decimal giaNhapKhauVND, decimal giaBanBuonVND, decimal giaBanLeVND, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string maCongTySanXuat, string tenCongTySanXuat, string maBuuChinhCongTySanXuat, string soNhaTenDuongCongTySanXuat, string phuongXaCongTySanXuat, string quanHuyenCongTySanXuat, string tinhThanhPhoCongTySanXuat, string maQuocGiaCongTySanXuat, string maCongTyCungCap, string tenCongTyCungCap, string maBuuChinhCongTyCungCap, string soNhaTenDuongCongTyCungCap, string phuongXaCongTyCungCap, string quanHuyenCongTyCungCap, string tinhThanhPhoCongTyCungCap, string maQuocGiaCongTyCungCap, string maDonViUyThac, string tenDonViUyThac, string maBuuChinhDonViUyThac, string soNhaTenDuongDonViUyThac, string phuongXaDonViUyThac, string quanHuyenDonViUyThac, string tinhThanhPhoDonViUyThac, string maQuocGiaDonViUyThac, decimal luongTonKhoKyTruoc, string donViTinhLuongTonKyTruoc, decimal luongNhapTrongKy, decimal tongSo, decimal tongSoXuatTrongKy, decimal soLuongTonKhoDenNgay, decimal huHao, string ghiChuChiTiet, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep_SMA entity = new KDT_VNACC_HangGiayPhep_SMA();			
			entity.ID = id;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenThuocQuyCachDongGoi = tenThuocQuyCachDongGoi;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.TenHoatChatGayNghien = tenHoatChatGayNghien;
			entity.HoatChat = hoatChat;
			entity.TieuChuanChatLuong = tieuChuanChatLuong;
			entity.SoDangKy = soDangKy;
			entity.HanDung = hanDung;
			entity.SoLuong = soLuong;
			entity.DonVitinhSoLuong = donVitinhSoLuong;
			entity.CongDung = congDung;
			entity.TongSoKhoiLuongHoatChatGayNghien = tongSoKhoiLuongHoatChatGayNghien;
			entity.DonVitinhKhoiLuong = donVitinhKhoiLuong;
			entity.GiaNhapKhauVND = giaNhapKhauVND;
			entity.GiaBanBuonVND = giaBanBuonVND;
			entity.GiaBanLeVND = giaBanLeVND;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.MaCongTySanXuat = maCongTySanXuat;
			entity.TenCongTySanXuat = tenCongTySanXuat;
			entity.MaBuuChinhCongTySanXuat = maBuuChinhCongTySanXuat;
			entity.SoNhaTenDuongCongTySanXuat = soNhaTenDuongCongTySanXuat;
			entity.PhuongXaCongTySanXuat = phuongXaCongTySanXuat;
			entity.QuanHuyenCongTySanXuat = quanHuyenCongTySanXuat;
			entity.TinhThanhPhoCongTySanXuat = tinhThanhPhoCongTySanXuat;
			entity.MaQuocGiaCongTySanXuat = maQuocGiaCongTySanXuat;
			entity.MaCongTyCungCap = maCongTyCungCap;
			entity.TenCongTyCungCap = tenCongTyCungCap;
			entity.MaBuuChinhCongTyCungCap = maBuuChinhCongTyCungCap;
			entity.SoNhaTenDuongCongTyCungCap = soNhaTenDuongCongTyCungCap;
			entity.PhuongXaCongTyCungCap = phuongXaCongTyCungCap;
			entity.QuanHuyenCongTyCungCap = quanHuyenCongTyCungCap;
			entity.TinhThanhPhoCongTyCungCap = tinhThanhPhoCongTyCungCap;
			entity.MaQuocGiaCongTyCungCap = maQuocGiaCongTyCungCap;
			entity.MaDonViUyThac = maDonViUyThac;
			entity.TenDonViUyThac = tenDonViUyThac;
			entity.MaBuuChinhDonViUyThac = maBuuChinhDonViUyThac;
			entity.SoNhaTenDuongDonViUyThac = soNhaTenDuongDonViUyThac;
			entity.PhuongXaDonViUyThac = phuongXaDonViUyThac;
			entity.QuanHuyenDonViUyThac = quanHuyenDonViUyThac;
			entity.TinhThanhPhoDonViUyThac = tinhThanhPhoDonViUyThac;
			entity.MaQuocGiaDonViUyThac = maQuocGiaDonViUyThac;
			entity.LuongTonKhoKyTruoc = luongTonKhoKyTruoc;
			entity.DonViTinhLuongTonKyTruoc = donViTinhLuongTonKyTruoc;
			entity.LuongNhapTrongKy = luongNhapTrongKy;
			entity.TongSo = tongSo;
			entity.TongSoXuatTrongKy = tongSoXuatTrongKy;
			entity.SoLuongTonKhoDenNgay = soLuongTonKhoDenNgay;
			entity.HuHao = huHao;
			entity.GhiChuChiTiet = ghiChuChiTiet;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenThuocQuyCachDongGoi", SqlDbType.NVarChar, TenThuocQuyCachDongGoi);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@TenHoatChatGayNghien", SqlDbType.NVarChar, TenHoatChatGayNghien);
			db.AddInParameter(dbCommand, "@HoatChat", SqlDbType.VarChar, HoatChat);
			db.AddInParameter(dbCommand, "@TieuChuanChatLuong", SqlDbType.VarChar, TieuChuanChatLuong);
			db.AddInParameter(dbCommand, "@SoDangKy", SqlDbType.VarChar, SoDangKy);
			db.AddInParameter(dbCommand, "@HanDung", SqlDbType.DateTime, HanDung.Year <= 1753 ? DBNull.Value : (object) HanDung);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, DonVitinhSoLuong);
			db.AddInParameter(dbCommand, "@CongDung", SqlDbType.VarChar, CongDung);
			db.AddInParameter(dbCommand, "@TongSoKhoiLuongHoatChatGayNghien", SqlDbType.Decimal, TongSoKhoiLuongHoatChatGayNghien);
			db.AddInParameter(dbCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, DonVitinhKhoiLuong);
			db.AddInParameter(dbCommand, "@GiaNhapKhauVND", SqlDbType.Decimal, GiaNhapKhauVND);
			db.AddInParameter(dbCommand, "@GiaBanBuonVND", SqlDbType.Decimal, GiaBanBuonVND);
			db.AddInParameter(dbCommand, "@GiaBanLeVND", SqlDbType.Decimal, GiaBanLeVND);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@MaCongTySanXuat", SqlDbType.VarChar, MaCongTySanXuat);
			db.AddInParameter(dbCommand, "@TenCongTySanXuat", SqlDbType.NVarChar, TenCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaBuuChinhCongTySanXuat", SqlDbType.VarChar, MaBuuChinhCongTySanXuat);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongCongTySanXuat", SqlDbType.VarChar, SoNhaTenDuongCongTySanXuat);
			db.AddInParameter(dbCommand, "@PhuongXaCongTySanXuat", SqlDbType.NVarChar, PhuongXaCongTySanXuat);
			db.AddInParameter(dbCommand, "@QuanHuyenCongTySanXuat", SqlDbType.NVarChar, QuanHuyenCongTySanXuat);
			db.AddInParameter(dbCommand, "@TinhThanhPhoCongTySanXuat", SqlDbType.NVarChar, TinhThanhPhoCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaQuocGiaCongTySanXuat", SqlDbType.VarChar, MaQuocGiaCongTySanXuat);
			db.AddInParameter(dbCommand, "@MaCongTyCungCap", SqlDbType.VarChar, MaCongTyCungCap);
			db.AddInParameter(dbCommand, "@TenCongTyCungCap", SqlDbType.NVarChar, TenCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaBuuChinhCongTyCungCap", SqlDbType.VarChar, MaBuuChinhCongTyCungCap);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongCongTyCungCap", SqlDbType.VarChar, SoNhaTenDuongCongTyCungCap);
			db.AddInParameter(dbCommand, "@PhuongXaCongTyCungCap", SqlDbType.NVarChar, PhuongXaCongTyCungCap);
			db.AddInParameter(dbCommand, "@QuanHuyenCongTyCungCap", SqlDbType.NVarChar, QuanHuyenCongTyCungCap);
			db.AddInParameter(dbCommand, "@TinhThanhPhoCongTyCungCap", SqlDbType.NVarChar, TinhThanhPhoCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaQuocGiaCongTyCungCap", SqlDbType.VarChar, MaQuocGiaCongTyCungCap);
			db.AddInParameter(dbCommand, "@MaDonViUyThac", SqlDbType.VarChar, MaDonViUyThac);
			db.AddInParameter(dbCommand, "@TenDonViUyThac", SqlDbType.NVarChar, TenDonViUyThac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonViUyThac", SqlDbType.VarChar, MaBuuChinhDonViUyThac);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongDonViUyThac", SqlDbType.VarChar, SoNhaTenDuongDonViUyThac);
			db.AddInParameter(dbCommand, "@PhuongXaDonViUyThac", SqlDbType.NVarChar, PhuongXaDonViUyThac);
			db.AddInParameter(dbCommand, "@QuanHuyenDonViUyThac", SqlDbType.NVarChar, QuanHuyenDonViUyThac);
			db.AddInParameter(dbCommand, "@TinhThanhPhoDonViUyThac", SqlDbType.NVarChar, TinhThanhPhoDonViUyThac);
			db.AddInParameter(dbCommand, "@MaQuocGiaDonViUyThac", SqlDbType.VarChar, MaQuocGiaDonViUyThac);
			db.AddInParameter(dbCommand, "@LuongTonKhoKyTruoc", SqlDbType.Decimal, LuongTonKhoKyTruoc);
			db.AddInParameter(dbCommand, "@DonViTinhLuongTonKyTruoc", SqlDbType.VarChar, DonViTinhLuongTonKyTruoc);
			db.AddInParameter(dbCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, LuongNhapTrongKy);
			db.AddInParameter(dbCommand, "@TongSo", SqlDbType.Decimal, TongSo);
			db.AddInParameter(dbCommand, "@TongSoXuatTrongKy", SqlDbType.Decimal, TongSoXuatTrongKy);
			db.AddInParameter(dbCommand, "@SoLuongTonKhoDenNgay", SqlDbType.Decimal, SoLuongTonKhoDenNgay);
			db.AddInParameter(dbCommand, "@HuHao", SqlDbType.Decimal, HuHao);
			db.AddInParameter(dbCommand, "@GhiChuChiTiet", SqlDbType.NVarChar, GhiChuChiTiet);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangGiayPhep_SMA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep_SMA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangGiayPhep_SMA(long id)
		{
			KDT_VNACC_HangGiayPhep_SMA entity = new KDT_VNACC_HangGiayPhep_SMA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangGiayPhep_SMA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep_SMA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}