﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_ToKhaiMauDichXuatForm_OLD : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        KDT_VNACC_TK_SoVanDon vd = new KDT_VNACC_TK_SoVanDon();
        public VNACC_ToKhaiMauDichXuatForm_OLD()
        {
            InitializeComponent();
        }


        private void VNACC_ToKhaiMauDichXuatForm_Load(object sender, EventArgs e)
        {
            if (TKMD.ID != 0)
            {
                SetToKhai();
                cmbMain.Commands["cmdChiThiHQ"].Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            LoadGrid();
        }

        private void LoadGrid()
        {
            grListHang.DataSource = TKMD.HangCollection;
            grListChiThi.DataSource = TKMD.ChiThiHQCollection;
        }

        private void GetToKhai()
        {
            TKMD.SoToKhai = Convert.ToInt32(txtSoToKhai.Value);
            TKMD.SoToKhaiDauTien = txtSoToKhaiDauTien.Text;
            TKMD.SoNhanhToKhai = Convert.ToInt32(txtSoNhanhToKhai.Value);
            TKMD.TongSoTKChiaNho = Convert.ToInt32(txtTongSoTKChiaNho.Value);
            TKMD.SoToKhaiTNTX = Convert.ToInt32(txtSoToKhaiTNTX.Value);
            TKMD.MaLoaiHinh = ctrMaLoaiHinh.Code;
            TKMD.MaPhanLoaiHH = ctrMaLoaiHinh.Code;
            TKMD.CoQuanHaiQuan = txtCoQuanHaiQuan.Text;
            TKMD.NhomXuLyHS = ctrNhomXuLyHS.Code;
            TKMD.ThoiHanTaiNhapTaiXuat = clcThoiHanTaiNhap.Value;
            TKMD.NgayDangKy = clcNgayDangKy.Value;
            TKMD.MaPhuongThucVT = ctrMaPhuongThucVT.Code;
            //don vi
            TKMD.MaDonVi = txtMaDonVi.Text;
            TKMD.TenDonVi = txtTenDonVi.Text;
            TKMD.MaBuuChinhDonVi = txtMaBuuChinhDonVi.Text;
            TKMD.DiaChiDonVi = txtDiaChiDonVi.Text;
            TKMD.SoDienThoaiDonVi = txtSoDienThoaiDonVi.Text;
            TKMD.MaUyThac = txtMaUyThac.Text;
            TKMD.TenUyThac = txtTenUyThac.Text;
            // doi tac
            TKMD.MaDoiTac = txtMaDoiTac.Text;
            TKMD.TenDoiTac = txtTenDoiTac.Text;
            TKMD.MaBuuChinhDoiTac = txtMaBuuChinhDoiTac.Text;
            TKMD.DiaChiDoiTac1 = txtDiaChiDoiTac1.Text;
            TKMD.DiaChiDoiTac2 = txtDiaChiDoiTac2.Text;
            TKMD.DiaChiDoiTac3 = txtDiaChiDoiTac3.Text;
            TKMD.DiaChiDoiTac4 = txtDiaChiDoiTac4.Text;
            //uy thac
            TKMD.MaNuocDoiTac = ctrMaNuoc.Code;
            TKMD.MaDaiLyHQ = txtMaDaiLyHQ.Text;
            //so kien va trong luong
            TKMD.SoLuong = Convert.ToInt32(txtSoLuong.Value);
            TKMD.MaDVTSoLuong = ctrMaDVTSoLuong.Code;
            TKMD.TrongLuong = Convert.ToInt32(txtTrongLuong.Value);
            TKMD.MaDVTTrongLuong = ctrMaDVTTrongLuong.Code;
            // van don
            TKMD.MaDDLuuKho = txtMaDDLuuKho.Text;
            TKMD.SoHieuKyHieu = txtSoHieuKyHieu.Text;
            TKMD.MaPTVC = txtMaPTVC.Text;
            TKMD.TenPTVC = txtTenPTVC.Text;
            TKMD.NgayHangDen = clcNgayHangDen.Value;
            TKMD.MaDiaDiemDoHang = ctrDiaDiemNhanHang.Code;
            TKMD.TenDiaDiemDohang = ctrDiaDiemNhanHang.Name_VN;
            TKMD.MaDiaDiemXepHang = ctrDiaDiemXepHang.Name_VN;
            TKMD.TenDiaDiemXepHang = ctrDiaDiemXepHang.Name_VN;
            //hoa don
            TKMD.PhanLoaiHD = ctrPhanLoaiHD.Code;//(ctrPhanLoaiHD.SelectedValue != null) ? cbbPhanLoaiHD.SelectedValue.ToString() : string.Empty;
            TKMD.SoTiepNhanHD = Convert.ToInt32(txtSoTiepNhanHD.Value);
            TKMD.SoHoaDon = txtSoHoaDon.Text;
            TKMD.NgayPhatHanhHD = clcNgayPhatHanhHD.Value;
            TKMD.PhuongThucTT = txtPhuongThucTT.Text;
            TKMD.PhanLoaiGiaHD = txtPhanLoaiGiaHD.Text;
            TKMD.MaDieuKienGiaHD = txtMaDieuKienGiaHD.Text;
            TKMD.TongTriGiaHD = Convert.ToDecimal(txtTongTriGiaHD.Value);
            TKMD.MaTTHoaDon = ctrMaTTHoaDon.Code;
            //tri gia tinh thue
            TKMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
            TKMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Code;
            TKMD.PhanLoaiKhongQDVND = txtPhanLoaiKhongQDVND.Text;
            //han muc
            TKMD.NguoiNopThue = ctrNguoiNopThue.Code;//(cbbNguoiNopThue.SelectedValue != null) ? cbbNguoiNopThue.SelectedValue.ToString() : string.Empty;
            TKMD.MaNHTraThueThay = txtMaNHTraThueThay.Text;
            TKMD.NamPhatHanhHM = Convert.ToInt32(txtNanPhatHanhHM.Value);
            TKMD.KyHieuCTHanMuc = txtKyHieuCTHanMuc.Text;
            TKMD.SoCTHanMuc = txtSoCTHanMuc.Text;
            //bao lanh thue
            TKMD.MaXDThoiHanNopThue = ctrMaXDThoiHanNopThue.Code;
            TKMD.MaNHBaoLanh = txtMaNHBaoLanh.Text;
            TKMD.NamPhatHanhBL = Convert.ToInt32(txtNamPhatHanhBL.Value);
            TKMD.KyHieuCTBaoLanh = txtKyHieuCTBaoLanh.Text;
            TKMD.SoCTBaoLanh = txtSoCTBaoLanh.Text;
            //
            TKMD.NgayKhoiHanhVC = clcNgayKhoiHanhVC.Value;
            TKMD.DiaDiemDichVC = txtDiaDiemDichVC.Text;
            TKMD.NgayDen = clcNgayDen.Value;
            TKMD.GhiChu = txtGhiChu.Text;
            TKMD.SoQuanLyNoiBoDN = txtSoQuanLyNoiBoDN.Text;
            //Van don
            if (txtSoVanDon.Text.Trim() != "")
            {
                if (TKMD.VanDonCollection.Count == 0)
                    TKMD.VanDonCollection.Add(vd);
                TKMD.VanDonCollection[0].SoTT = 1;
                TKMD.VanDonCollection[0].SoVanDon = txtSoVanDon.Text;
            }
            else
            {
                if (TKMD.VanDonCollection.Count > 0)
                {
                    TKMD.VanDonCollection[0].Delete();
                    TKMD.VanDonCollection.Clear();
                }
            }

        }

        private void SetToKhai()
        {
            txtSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
            txtSoToKhaiDauTien.Text = TKMD.SoToKhaiDauTien;
            txtSoNhanhToKhai.Text = Convert.ToString(TKMD.SoNhanhToKhai);
            txtTongSoTKChiaNho.Text = Convert.ToString(TKMD.TongSoTKChiaNho);
            txtSoToKhaiTNTX.Text = Convert.ToString(TKMD.SoToKhaiTNTX);
            ctrMaLoaiHinh.Code = TKMD.MaLoaiHinh;
            // cbbMaLoaiHinh.SelectedValue = TKMD.MaLoaiHinh;
            ctrMaPhanLoaiHH.Code = TKMD.MaPhanLoaiHH;
            txtCoQuanHaiQuan.Text = TKMD.CoQuanHaiQuan;
            ctrNhomXuLyHS.Code = TKMD.NhomXuLyHS;
            clcThoiHanTaiNhap.Value = TKMD.ThoiHanTaiNhapTaiXuat;
            clcNgayDangKy.Value = TKMD.NgayDangKy;
            ctrMaPhuongThucVT.Code =TKMD.MaPhuongThucVT;
            //Don vi
            txtMaDonVi.Text = TKMD.MaDonVi;
            txtTenDonVi.Text = TKMD.TenDonVi;
            txtMaBuuChinhDonVi.Text = TKMD.MaBuuChinhDonVi;
            txtDiaChiDonVi.Text = TKMD.DiaChiDonVi;
            txtSoDienThoaiDonVi.Text = TKMD.SoDienThoaiDonVi;
            //uy thac
            txtMaUyThac.Text = TKMD.MaUyThac;
            txtTenUyThac.Text = TKMD.TenUyThac;
            //doi tac
            txtMaDoiTac.Text = TKMD.MaDoiTac;
            txtTenDoiTac.Text = TKMD.TenDoiTac;
            txtMaBuuChinhDoiTac.Text = TKMD.MaBuuChinhDoiTac;
            txtDiaChiDoiTac1.Text = TKMD.DiaChiDoiTac1;
            txtDiaChiDoiTac2.Text = TKMD.DiaChiDoiTac2;
            txtDiaChiDoiTac3.Text = TKMD.DiaChiDoiTac3;
            txtDiaChiDoiTac4.Text = TKMD.DiaChiDoiTac4;
            ctrMaNuoc.Code = TKMD.MaNuocDoiTac;
            txtMaDaiLyHQ.Text = TKMD.MaDaiLyHQ;
            //so kien va trong luong
            txtSoLuong.Text = Convert.ToString(TKMD.SoLuong);
            ctrMaDVTSoLuong.Code = TKMD.MaDVTSoLuong;
            txtTrongLuong.Text = Convert.ToString(TKMD.TrongLuong);
            ctrMaDVTTrongLuong.Code = TKMD.MaDVTTrongLuong;
            //van don
            txtMaDDLuuKho.Text = TKMD.MaDDLuuKho;
            txtSoHieuKyHieu.Text = TKMD.SoHieuKyHieu;
            txtMaPTVC.Text = TKMD.MaPTVC;
            txtTenPTVC.Text = TKMD.TenPTVC;
            clcNgayHangDen.Value = TKMD.NgayHangDen;
            ctrDiaDiemNhanHang.Code = TKMD.MaDiaDiemDoHang;
            //ctrDiaDiemNhanHang.Name_VN = TKMD.TenDiaDiemDohang;
            ctrDiaDiemXepHang.Code = TKMD.MaDiaDiemXepHang;
            //ctrDiaDiemXepHang.Ten = TKMD.TenDiaDiemXepHang;
            //hoa don
            ctrPhanLoaiHD.Code = TKMD.PhanLoaiHD;
            txtSoTiepNhanHD.Text = Convert.ToString(TKMD.SoTiepNhanHD);
            txtSoHoaDon.Text = TKMD.SoHoaDon;
            clcNgayPhatHanhHD.Value = TKMD.NgayPhatHanhHD;
            txtPhuongThucTT.Text = TKMD.PhuongThucTT;
            txtPhanLoaiGiaHD.Text = TKMD.PhanLoaiGiaHD;
            txtMaDieuKienGiaHD.Text = TKMD.MaDieuKienGiaHD;
            ctrMaTTHoaDon.Code = TKMD.MaTTHoaDon;
            txtTongTriGiaHD.Text = Convert.ToString(TKMD.TongTriGiaHD);
            txtTriGiaTinhThue.Text = Convert.ToString(TKMD.TriGiaTinhThue);
            txtPhanLoaiKhongQDVND.Text = TKMD.PhanLoaiKhongQDVND;
            ctrMaTTTriGiaTinhThue.Code = TKMD.MaTTTriGiaTinhThue;
            //han muc
            ctrNguoiNopThue.Code = TKMD.NguoiNopThue;
            txtMaNHTraThueThay.Text = TKMD.MaNHTraThueThay;
            txtNanPhatHanhHM.Text = Convert.ToString(TKMD.NamPhatHanhHM);
            txtKyHieuCTHanMuc.Text = TKMD.KyHieuCTHanMuc;
            txtSoCTHanMuc.Text = TKMD.SoCTHanMuc;
            //bao lanh thue
            ctrMaXDThoiHanNopThue.Code = TKMD.MaXDThoiHanNopThue;
            txtMaNHBaoLanh.Text = TKMD.MaNHBaoLanh;
            txtNamPhatHanhBL.Text = Convert.ToString(TKMD.NamPhatHanhBL);
            txtKyHieuCTBaoLanh.Text = TKMD.KyHieuCTBaoLanh;
            txtSoCTBaoLanh.Text = TKMD.SoCTBaoLanh;
            //----
            clcNgayKhoiHanhVC.Value = TKMD.NgayKhoiHanhVC;
            txtDiaDiemDichVC.Text = TKMD.DiaDiemDichVC;
            clcNgayDen.Value = TKMD.NgayDen;
            txtGhiChu.Text = TKMD.GhiChu;
            txtSoQuanLyNoiBoDN.Text = TKMD.SoQuanLyNoiBoDN;
            //van don

            if (TKMD.VanDonCollection.Count == 1)
                txtSoVanDon.Text = TKMD.VanDonCollection[0].SoVanDon;

        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    showThemHang();
                    break;
                case "cmdChiThiHQ":
                    showChiThiHQ();
                    break;
                case "cmdGiayPhep":
                    showGiayPhep();
                    break;
                case "cmdDinhKemDT":
                    showDinhKem();
                    break;
                case "cmdTrungChuyen":
                    showTrungChuyen();
                    break;
                case "cmdContainer":
                    showContainer();
                    break;
                case "cmdLuu":
                    saveToKhai();

                    break;

            }
        }

        private void showThemHang()
        {
            VNACC_HangMauDichXuatForm f = new VNACC_HangMauDichXuatForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
        }

        private void showChiThiHQ()
        {
            if (TKMD.SoToKhai != 0)
            {
                VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
                f.Master_ID = this.TKMD.ID;
                f.LoaiThongTin = ELoaiThongTin.TK;
                f.ShowDialog();
            }
        }

        private void showGiayPhep()
        {
            VNACC_TK_GiayPhepForm f = new VNACC_TK_GiayPhepForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
        }

        private void showDinhKem()
        {
            VNACC_TK_DinhKiemDTForm f = new VNACC_TK_DinhKiemDTForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
        }

        private void showTrungChuyen()
        {
            VNACC_TK_TrungChuyenForm f = new VNACC_TK_TrungChuyenForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();

        }

        private void showContainer()
        {
            if (TKMD.ID != 0)
            {
                VNACC_TK_Container f = new VNACC_TK_Container();
                f.TKMD = this.TKMD;
                f.ShowDialog();
            }
            else
                ShowMessage("Lưu tờ khai trước thêm Container", false);
        }

        private void saveToKhai()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                GetToKhai();

                TKMD.InsertUpdateFull();
                ShowMessage("Lưu tờ khai thành công", false);
                LoadGrid();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

    }
}
