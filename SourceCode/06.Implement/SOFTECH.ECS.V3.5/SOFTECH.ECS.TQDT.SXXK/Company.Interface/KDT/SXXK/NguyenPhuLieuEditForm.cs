﻿using System;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuEditForm : BaseForm
    {
        public NguyenPhuLieu NPLDetail;
        public NguyenPhuLieuCollection NPLCollection;
        public NguyenPhuLieuDangKy nplDangKy;
        private string MoTa = "";
        public NguyenPhuLieuEditForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.Insert)
                    this.NPLDetail = null;
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            // Kiểm tra tính hợp lệ của mã HS.
            if (!MaHS.Validate(txtMaHS.Text, 8))
            {
                error.SetIconPadding(txtMaHS, -8);
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    error.SetError(txtMaHS, "HS code is invalid");
                }
                return;
            }

            if (!cvError.IsValid) return;

            if (checkNPLExit(txtMa.Text.Trim()))
            {
                MLMessages("Nguyên phụ liệu này đã được khai báo trên lưới.", "MSG_PUB10", "", false);
                return;
            }
            if (this.OpenType == OpenFormType.Insert)
            {
                // Kiểm tra xem NPL(SXXK) đã tồn tại hay chưa?
                BLL.SXXK.NguyenPhuLieu nplSXXK = new BLL.SXXK.NguyenPhuLieu();
                nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                nplSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                nplSXXK.Ma = txtMa.Text.Trim();
                if (nplSXXK.Load())
                {
                    MLMessages("Nguyên phụ liệu này đã được đăng ký.", "MSG_PUB06", "", false);
                    return;
                }

                this.NPLDetail = new NguyenPhuLieu();
                this.NPLDetail.Ma = txtMa.Text.Trim();
                this.NPLDetail.Ten = txtTen.Text.Trim();
                this.NPLDetail.MaHS = txtMaHS.Text.Trim();
                this.NPLDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            }
            else if (this.OpenType == OpenFormType.Edit)
            {
                if (txtMa.Text.Trim().ToUpper() != NPLDetail.Ma.Trim().ToUpper())
                {
                    BLL.SXXK.NguyenPhuLieu nplSXXK = new BLL.SXXK.NguyenPhuLieu();
                    nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                    nplSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                    nplSXXK.Ma = txtMa.Text.Trim();
                    if (nplSXXK.Load())
                    {
                        MLMessages("Nguyên phụ liệu này đã được đăng ký.", "MSG_PUB06", "", false);
                        return;
                    }
                }
                string maSP = txtMa.Text.Trim();
                string tenSP = txtTen.Text.Trim();
                string mahs = txtMaHS.Text.Trim();
                string dvt = cbDonViTinh.SelectedValue.ToString();

                if (maSP != NPLDetail.Ma || tenSP != NPLDetail.Ten || NPLDetail.MaHS != mahs || NPLDetail.DVT_ID != dvt)
                {
                    string maCu = NPLDetail.Ma;
                    this.NPLDetail.Ma = txtMa.Text.Trim();
                    this.NPLDetail.Ten = txtTen.Text.Trim();
                    this.NPLDetail.MaHS = txtMaHS.Text.Trim();
                    this.NPLDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    if (NPLDetail.ID > 0)
                    {
                        this.NPLDetail.Update();
                        List<Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung> nplBSListKDT = (List<Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung>)Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung.SelectCollectionBy_NPL_ID(NPLDetail.ID);
                        if (nplBSListKDT != null && nplBSListKDT.Count > 0)
                        {
                            Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung nplBs = nplBSListKDT[0];

                            nplBs.NPLChinh = NPLDetail.NPLChinh;
                            nplBs.Update();
                        }
                        else
                        {
                            Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung nplBs = new Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung();
                            nplBs.NPL_ID = NPLDetail.ID;
                            nplBs.Ma = NPLDetail.Ma;
                            nplBs.NPLChinh = NPLDetail.NPLChinh;
                            nplBs.Insert();
                        }

                        //Cap nhat NPL da dang ky
                        BLL.SXXK.NguyenPhuLieu nplSXXK = new BLL.SXXK.NguyenPhuLieu();
                        nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                        nplSXXK.MaHaiQuan = this.MaHaiQuan.Trim();
                        nplSXXK.Ma = maCu;
                        nplSXXK.Delete();

                        nplSXXK.Ma = NPLDetail.Ma;
                        nplSXXK.MaHS = NPLDetail.MaHS;
                        nplSXXK.Ten = NPLDetail.Ten;
                        nplSXXK.DVT_ID = NPLDetail.DVT_ID;
                        nplSXXK.Insert();

                        //TODO: Update NPLChinh trong SXXK
                        Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung nplBSObjectSXXK = Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung.Load(this.MaHaiQuan.Trim(), this.MaDoanhNghiep, NPLDetail.Ma);
                        if (nplBSObjectSXXK != null)
                        {
                            nplBSObjectSXXK.NPLChinh = NPLDetail.NPLChinh;
                            nplBSObjectSXXK.Update();
                        }
                        else
                        {
                            Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung nplBSObjectSXXKNew = new Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung();
                            nplBSObjectSXXKNew.MaHaiQuan = this.MaHaiQuan.Trim();
                            nplBSObjectSXXKNew.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplBSObjectSXXKNew.Ma = NPLDetail.Ma;
                            nplBSObjectSXXKNew.NPLChinh = NPLDetail.NPLChinh;
                            nplBSObjectSXXKNew.Insert();
                        }
                    }
                }
            }
            this.Close();
        }
        private bool checkNPLExit(string maNPL)
        {
            try
            {
                foreach (NguyenPhuLieu npl in NPLCollection)
                {
                    if (npl.Ma == maNPL) return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void NguyenPhuLieuEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = MaHS.SelectAll();
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaHS.AutoCompleteCustomSource = col;
                txtMa.Focus();
                this._DonViTinh =  DonViTinh.SelectAll().Tables[0];
                cbDonViTinh.DataSource = this._DonViTinh;
                cbDonViTinh.DisplayMember = "Ten";
                cbDonViTinh.ValueMember = "ID";
                cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;

                if (this.OpenType == OpenFormType.Insert)
                {
                    cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
                }
                else if (this.OpenType == OpenFormType.Edit)
                {
                    if (this.NPLDetail != null)
                    {
                        txtMa.Text = this.NPLDetail.Ma;
                        txtTen.Text = this.NPLDetail.Ten;
                        txtMaHS.Text = this.NPLDetail.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLDetail.DVT_ID;
                    }
                }
                else 
                {
                    txtMa.Text = this.NPLDetail.Ma;
                    txtTen.Text = this.NPLDetail.Ten;
                    txtMaHS.Text = this.NPLDetail.MaHS;
                    cbDonViTinh.SelectedValue = this.NPLDetail.DVT_ID;
                    btnUpdate.Enabled = false;
                }

            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!MaHS.Validate(txtMaHS.Text, 10))
                {
                    error.SetIconPadding(txtMaHS, -8);
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                    }
                    else
                    {
                        error.SetError(txtMaHS, "HS code is invalid");
                    }
                }
                else
                {
                    error.SetError(txtMaHS, string.Empty);
                }
                this.MoTa = MaHS.CheckExist(txtMaHS.Text);
                if (this.MoTa == "")
                {
                    error.SetIconPadding(txtMaHS, -8);
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        error.SetError(txtMaHS, "Mã HS không có trong danh mục mã HS.");
                    }
                    else
                    {
                        error.SetError(txtMaHS, "HS code haven't exist in HS list.");
                    }
                }
                else
                {
                    error.SetError(txtMaHS, string.Empty);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }
    }
}