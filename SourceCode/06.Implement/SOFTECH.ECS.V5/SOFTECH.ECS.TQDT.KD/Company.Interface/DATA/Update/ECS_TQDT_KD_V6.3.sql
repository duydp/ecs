IF (SELECT COUNT(*) FROM [dbo].[t_HaiQuan_BieuThue] WHERE [MaBieuThue] = 'B51') = 0
begin
	INSERT INTO [dbo].[t_HaiQuan_BieuThue]  ([MaBieuThue]      ,[TenBieuThue]        ,[MoTaKhac])
	VALUES ('B51', N'Biểu thuế môi trường', 'MT')
END

IF (SELECT COUNT(*) FROM [dbo].[t_HaiQuan_BieuThue] WHERE [MaBieuThue] = 'B61') = 0
begin
  INSERT INTO [dbo].[t_HaiQuan_BieuThue]  ([MaBieuThue]      ,[TenBieuThue]        ,[MoTaKhac])
  VALUES ('B61', N'Biểu thuế khác', 'KHAC')
END

UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '6.3'
      ,[Date] = getdate()
      ,[Notes] = ''
