using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GiayPhep_ID { set; get; }
		public decimal TongSoToKhai { set; get; }
		public string TinhTrangThamTra { set; get; }
		public decimal PhuongThucXuLyCuoiCung { set; get; }
		public decimal SoDonXinCapPhep { set; get; }
		public string MaNhanVienKiemTra { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public string MaNguoiKhai { set; get; }
		public string TenNguoiKhai { set; get; }
		public DateTime NgayGiaoViec { set; get; }
		public DateTime GioGiaoViec { set; get; }
		public string NguoiGiaoViec { set; get; }
		public string NoiDungGiaoViec { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> collection = new List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra>();
			while (reader.Read())
			{
				KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra entity = new KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayPhep_ID"))) entity.GiayPhep_ID = reader.GetInt64(reader.GetOrdinal("GiayPhep_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoToKhai"))) entity.TongSoToKhai = reader.GetDecimal(reader.GetOrdinal("TongSoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhTrangThamTra"))) entity.TinhTrangThamTra = reader.GetString(reader.GetOrdinal("TinhTrangThamTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongThucXuLyCuoiCung"))) entity.PhuongThucXuLyCuoiCung = reader.GetDecimal(reader.GetOrdinal("PhuongThucXuLyCuoiCung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDonXinCapPhep"))) entity.SoDonXinCapPhep = reader.GetDecimal(reader.GetOrdinal("SoDonXinCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNhanVienKiemTra"))) entity.MaNhanVienKiemTra = reader.GetString(reader.GetOrdinal("MaNhanVienKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhai"))) entity.MaNguoiKhai = reader.GetString(reader.GetOrdinal("MaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhai"))) entity.TenNguoiKhai = reader.GetString(reader.GetOrdinal("TenNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaoViec"))) entity.NgayGiaoViec = reader.GetDateTime(reader.GetOrdinal("NgayGiaoViec"));
				if (!reader.IsDBNull(reader.GetOrdinal("GioGiaoViec"))) entity.GioGiaoViec = reader.GetDateTime(reader.GetOrdinal("GioGiaoViec"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiGiaoViec"))) entity.NguoiGiaoViec = reader.GetString(reader.GetOrdinal("NguoiGiaoViec"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungGiaoViec"))) entity.NoiDungGiaoViec = reader.GetString(reader.GetOrdinal("NoiDungGiaoViec"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> collection, long id)
        {
            foreach (KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra VALUES(@GiayPhep_ID, @TongSoToKhai, @TinhTrangThamTra, @PhuongThucXuLyCuoiCung, @SoDonXinCapPhep, @MaNhanVienKiemTra, @NgayKhaiBao, @MaNguoiKhai, @TenNguoiKhai, @NgayGiaoViec, @GioGiaoViec, @NguoiGiaoViec, @NoiDungGiaoViec, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra SET GiayPhep_ID = @GiayPhep_ID, TongSoToKhai = @TongSoToKhai, TinhTrangThamTra = @TinhTrangThamTra, PhuongThucXuLyCuoiCung = @PhuongThucXuLyCuoiCung, SoDonXinCapPhep = @SoDonXinCapPhep, MaNhanVienKiemTra = @MaNhanVienKiemTra, NgayKhaiBao = @NgayKhaiBao, MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, NgayGiaoViec = @NgayGiaoViec, GioGiaoViec = @GioGiaoViec, NguoiGiaoViec = @NguoiGiaoViec, NoiDungGiaoViec = @NoiDungGiaoViec, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoToKhai", SqlDbType.Decimal, "TongSoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrangThamTra", SqlDbType.VarChar, "TinhTrangThamTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongThucXuLyCuoiCung", SqlDbType.Decimal, "PhuongThucXuLyCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhanVienKiemTra", SqlDbType.VarChar, "MaNhanVienKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGiaoViec", SqlDbType.DateTime, "NgayGiaoViec", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioGiaoViec", SqlDbType.DateTime, "GioGiaoViec", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiGiaoViec", SqlDbType.VarChar, "NguoiGiaoViec", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungGiaoViec", SqlDbType.NVarChar, "NoiDungGiaoViec", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoToKhai", SqlDbType.Decimal, "TongSoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrangThamTra", SqlDbType.VarChar, "TinhTrangThamTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongThucXuLyCuoiCung", SqlDbType.Decimal, "PhuongThucXuLyCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhanVienKiemTra", SqlDbType.VarChar, "MaNhanVienKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGiaoViec", SqlDbType.DateTime, "NgayGiaoViec", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioGiaoViec", SqlDbType.DateTime, "GioGiaoViec", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiGiaoViec", SqlDbType.VarChar, "NguoiGiaoViec", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungGiaoViec", SqlDbType.NVarChar, "NoiDungGiaoViec", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra VALUES(@GiayPhep_ID, @TongSoToKhai, @TinhTrangThamTra, @PhuongThucXuLyCuoiCung, @SoDonXinCapPhep, @MaNhanVienKiemTra, @NgayKhaiBao, @MaNguoiKhai, @TenNguoiKhai, @NgayGiaoViec, @GioGiaoViec, @NguoiGiaoViec, @NoiDungGiaoViec, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra SET GiayPhep_ID = @GiayPhep_ID, TongSoToKhai = @TongSoToKhai, TinhTrangThamTra = @TinhTrangThamTra, PhuongThucXuLyCuoiCung = @PhuongThucXuLyCuoiCung, SoDonXinCapPhep = @SoDonXinCapPhep, MaNhanVienKiemTra = @MaNhanVienKiemTra, NgayKhaiBao = @NgayKhaiBao, MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, NgayGiaoViec = @NgayGiaoViec, GioGiaoViec = @GioGiaoViec, NguoiGiaoViec = @NguoiGiaoViec, NoiDungGiaoViec = @NoiDungGiaoViec, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoToKhai", SqlDbType.Decimal, "TongSoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhTrangThamTra", SqlDbType.VarChar, "TinhTrangThamTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongThucXuLyCuoiCung", SqlDbType.Decimal, "PhuongThucXuLyCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhanVienKiemTra", SqlDbType.VarChar, "MaNhanVienKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGiaoViec", SqlDbType.DateTime, "NgayGiaoViec", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioGiaoViec", SqlDbType.DateTime, "GioGiaoViec", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiGiaoViec", SqlDbType.VarChar, "NguoiGiaoViec", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungGiaoViec", SqlDbType.NVarChar, "NoiDungGiaoViec", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoToKhai", SqlDbType.Decimal, "TongSoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhTrangThamTra", SqlDbType.VarChar, "TinhTrangThamTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongThucXuLyCuoiCung", SqlDbType.Decimal, "PhuongThucXuLyCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhanVienKiemTra", SqlDbType.VarChar, "MaNhanVienKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGiaoViec", SqlDbType.DateTime, "NgayGiaoViec", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioGiaoViec", SqlDbType.DateTime, "GioGiaoViec", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiGiaoViec", SqlDbType.VarChar, "NguoiGiaoViec", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungGiaoViec", SqlDbType.NVarChar, "NoiDungGiaoViec", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> SelectCollectionBy_GiayPhep_ID(long giayPhep_ID)
		{
            IDataReader reader = SelectReaderBy_GiayPhep_ID(giayPhep_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_GiayPhep_SAA_TinhTrangThamTra(long giayPhep_ID, decimal tongSoToKhai, string tinhTrangThamTra, decimal phuongThucXuLyCuoiCung, decimal soDonXinCapPhep, string maNhanVienKiemTra, DateTime ngayKhaiBao, string maNguoiKhai, string tenNguoiKhai, DateTime ngayGiaoViec, DateTime gioGiaoViec, string nguoiGiaoViec, string noiDungGiaoViec, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra entity = new KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra();	
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TongSoToKhai = tongSoToKhai;
			entity.TinhTrangThamTra = tinhTrangThamTra;
			entity.PhuongThucXuLyCuoiCung = phuongThucXuLyCuoiCung;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.MaNhanVienKiemTra = maNhanVienKiemTra;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.NgayGiaoViec = ngayGiaoViec;
			entity.GioGiaoViec = gioGiaoViec;
			entity.NguoiGiaoViec = nguoiGiaoViec;
			entity.NoiDungGiaoViec = noiDungGiaoViec;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TongSoToKhai", SqlDbType.Decimal, TongSoToKhai);
			db.AddInParameter(dbCommand, "@TinhTrangThamTra", SqlDbType.VarChar, TinhTrangThamTra);
			db.AddInParameter(dbCommand, "@PhuongThucXuLyCuoiCung", SqlDbType.Decimal, PhuongThucXuLyCuoiCung);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@MaNhanVienKiemTra", SqlDbType.VarChar, MaNhanVienKiemTra);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@NgayGiaoViec", SqlDbType.DateTime, NgayGiaoViec.Year <= 1753 ? DBNull.Value : (object) NgayGiaoViec);
			db.AddInParameter(dbCommand, "@GioGiaoViec", SqlDbType.DateTime, GioGiaoViec.Year <= 1753 ? DBNull.Value : (object) GioGiaoViec);
			db.AddInParameter(dbCommand, "@NguoiGiaoViec", SqlDbType.VarChar, NguoiGiaoViec);
			db.AddInParameter(dbCommand, "@NoiDungGiaoViec", SqlDbType.NVarChar, NoiDungGiaoViec);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_GiayPhep_SAA_TinhTrangThamTra(long id, long giayPhep_ID, decimal tongSoToKhai, string tinhTrangThamTra, decimal phuongThucXuLyCuoiCung, decimal soDonXinCapPhep, string maNhanVienKiemTra, DateTime ngayKhaiBao, string maNguoiKhai, string tenNguoiKhai, DateTime ngayGiaoViec, DateTime gioGiaoViec, string nguoiGiaoViec, string noiDungGiaoViec, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra entity = new KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra();			
			entity.ID = id;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TongSoToKhai = tongSoToKhai;
			entity.TinhTrangThamTra = tinhTrangThamTra;
			entity.PhuongThucXuLyCuoiCung = phuongThucXuLyCuoiCung;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.MaNhanVienKiemTra = maNhanVienKiemTra;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.NgayGiaoViec = ngayGiaoViec;
			entity.GioGiaoViec = gioGiaoViec;
			entity.NguoiGiaoViec = nguoiGiaoViec;
			entity.NoiDungGiaoViec = noiDungGiaoViec;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TongSoToKhai", SqlDbType.Decimal, TongSoToKhai);
			db.AddInParameter(dbCommand, "@TinhTrangThamTra", SqlDbType.VarChar, TinhTrangThamTra);
			db.AddInParameter(dbCommand, "@PhuongThucXuLyCuoiCung", SqlDbType.Decimal, PhuongThucXuLyCuoiCung);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@MaNhanVienKiemTra", SqlDbType.VarChar, MaNhanVienKiemTra);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@NgayGiaoViec", SqlDbType.DateTime, NgayGiaoViec.Year <= 1753 ? DBNull.Value : (object) NgayGiaoViec);
			db.AddInParameter(dbCommand, "@GioGiaoViec", SqlDbType.DateTime, GioGiaoViec.Year <= 1753 ? DBNull.Value : (object) GioGiaoViec);
			db.AddInParameter(dbCommand, "@NguoiGiaoViec", SqlDbType.VarChar, NguoiGiaoViec);
			db.AddInParameter(dbCommand, "@NoiDungGiaoViec", SqlDbType.NVarChar, NoiDungGiaoViec);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_GiayPhep_SAA_TinhTrangThamTra(long id, long giayPhep_ID, decimal tongSoToKhai, string tinhTrangThamTra, decimal phuongThucXuLyCuoiCung, decimal soDonXinCapPhep, string maNhanVienKiemTra, DateTime ngayKhaiBao, string maNguoiKhai, string tenNguoiKhai, DateTime ngayGiaoViec, DateTime gioGiaoViec, string nguoiGiaoViec, string noiDungGiaoViec, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra entity = new KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra();			
			entity.ID = id;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TongSoToKhai = tongSoToKhai;
			entity.TinhTrangThamTra = tinhTrangThamTra;
			entity.PhuongThucXuLyCuoiCung = phuongThucXuLyCuoiCung;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.MaNhanVienKiemTra = maNhanVienKiemTra;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.NgayGiaoViec = ngayGiaoViec;
			entity.GioGiaoViec = gioGiaoViec;
			entity.NguoiGiaoViec = nguoiGiaoViec;
			entity.NoiDungGiaoViec = noiDungGiaoViec;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TongSoToKhai", SqlDbType.Decimal, TongSoToKhai);
			db.AddInParameter(dbCommand, "@TinhTrangThamTra", SqlDbType.VarChar, TinhTrangThamTra);
			db.AddInParameter(dbCommand, "@PhuongThucXuLyCuoiCung", SqlDbType.Decimal, PhuongThucXuLyCuoiCung);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@MaNhanVienKiemTra", SqlDbType.VarChar, MaNhanVienKiemTra);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@NgayGiaoViec", SqlDbType.DateTime, NgayGiaoViec.Year <= 1753 ? DBNull.Value : (object) NgayGiaoViec);
			db.AddInParameter(dbCommand, "@GioGiaoViec", SqlDbType.DateTime, GioGiaoViec.Year <= 1753 ? DBNull.Value : (object) GioGiaoViec);
			db.AddInParameter(dbCommand, "@NguoiGiaoViec", SqlDbType.VarChar, NguoiGiaoViec);
			db.AddInParameter(dbCommand, "@NoiDungGiaoViec", SqlDbType.NVarChar, NoiDungGiaoViec);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_GiayPhep_SAA_TinhTrangThamTra(long id)
		{
			KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra entity = new KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_GiayPhep_ID(long giayPhep_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}