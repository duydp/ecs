﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseNPLMaperManagement : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_NGUYENPHULIEU NPL = new T_KHOKETOAN_NGUYENPHULIEU();
        public T_KHOKETOAN_NGUYENPHULIEU_MAP NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
        public WareHouseNPLMaperManagement()
        {
            InitializeComponent();

        }
        private void BinDataNPL()
        {
            try
            {
                grListNPL.Refetch();
                grListNPL.DataSource = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionAll();
                grListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);              
            }
        }
        private void BindDataNPLMap()
        {
            try
            {
                grListNPLMap.Refetch();
                grListNPLMap.DataSource = NPL.NPLMapCollection;
                grListNPLMap.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void WareHouseNPLMaperManagement_Load(object sender, EventArgs e)
        {
            BinDataNPL();
        }

        private void btnSearchNPL_Click(object sender, EventArgs e)
        {
            try
            {
                grListNPL.Refetch();
                grListNPL.DataSource = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("MANPL LIKE '%" + txtNPL.Text.ToString() + "%'", "");
                grListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchNPLMap_Click(object sender, EventArgs e)
        {
            try
            {
                grListNPLMap.Refetch();
                grListNPLMap.DataSource = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPLMAP LIKE '%" + txtNPLMap.Text.ToString() + "%'", "");
                grListNPLMap.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPL_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPLMap_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPLMap_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                NPLMap = (T_KHOKETOAN_NGUYENPHULIEU_MAP)e.Row.DataRow;
                NPL = T_KHOKETOAN_NGUYENPHULIEU.Load(NPLMap.KHONPL_ID);
                NPL.NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionBy_KHONPL_ID(NPL.ID);
                WareHouseNPLMaper f = new WareHouseNPLMaper();
                f.NPL = NPL;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPL_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                NPL = new T_KHOKETOAN_NGUYENPHULIEU();
                NPL = (T_KHOKETOAN_NGUYENPHULIEU)e.Row.DataRow;
                NPL.NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionBy_KHONPL_ID(NPL.ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            BinDataNPL();
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            try
            {
               
                WareHouseNPLAutoMaperForm f = new WareHouseNPLAutoMaperForm();
                f.Type = "NPL";
                f.ShowDialog(this);
                BinDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                WareHouseNPLMaper f = new WareHouseNPLMaper();
                f.ShowDialog(this);
                BinDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPL_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListNPL.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NPL = (T_KHOKETOAN_NGUYENPHULIEU)i.GetRow().DataRow;
                        NPL.NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionBy_KHONPL_ID(NPL.ID);
                    }
                }
                BindDataNPLMap();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
