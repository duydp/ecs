﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.DuLieuChuan;


namespace Company.Interface.Report.GC
{
    public partial class PhieuXuatKho : DevExpress.XtraReports.UI.XtraReport
    {
        public List<KDT_GC_PhieuXuatKho_Hang> ListHang = new List<KDT_GC_PhieuXuatKho_Hang>();
        //private int STT = 0;
        public PhieuXuatKho()
        {
            InitializeComponent();
        }
        public void BindReport(KDT_GC_PhieuXuatKho PXK)
        {
            lblTenDN.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblEmail.Text = GlobalSettings.MailDoanhNghiep;
            lblSoDT.Text = GlobalSettings.SoDienThoaiDN;
            lblFax.Text = GlobalSettings.SoFaxDN;
            lblNgay.Text = "Ngày " + PXK.NgayXuatKho.Day.ToString() + " tháng " + PXK.NgayXuatKho.Month.ToString() + " năm " + PXK.NgayXuatKho.Year.ToString();

            lblSoHD.Text = PXK.SoPhieu.ToString() + " / " + PXK.SoHopDong;
            lblKhachHang.Text = PXK.KhachHang;
            lblDonViNhan.Text = PXK.DonViNhan;
            lblStyle.Text = PXK.Style;
            lblXuatTaiKho.Text = PXK.XuatTaiKho;
            lblPO.Text = PXK.PO;
            lblSoLuong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(2, false), Convert.ToDecimal(PXK.SoLuong.ToString().Replace(".", ",")));
            lblDVT.Text = PXK.DVTSoLuong;
            DataTable dt = new DataTable();
            this.DataSource = PXK.HangXuatCollection;
            lblMaNPL1.DataBindings.Add("Text", this.DataSource, "MaNPL2");
            //lblMaNPL2.DataBindings.Add("Text", this.DataSource, "MaNPL2");
            lblMoTaNPL.DataBindings.Add("Text", this.DataSource, "TenHang");
            //lblArt.DataBindings.Add("Text", this.DataSource, "Art");
            //lblNCC.DataBindings.Add("Text", this.DataSource, "NCC");
            //lblSize.DataBindings.Add("Text", this.DataSource, "Size");
            //lblColor.DataBindings.Add("Text", this.DataSource, "Color");
            lblSL.DataBindings.Add("Text", this.DataSource, "SoLuong1", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            
            lblDonVi.DataBindings.Add("Text", this.DataSource, "DVTLuong1");
            lblDM.DataBindings.Add("Text", this.DataSource, "DM", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblPhanTram.DataBindings.Add("Text", this.DataSource, "PhanTram", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblNhuCau.DataBindings.Add("Text", this.DataSource, "NhuCau", Company.KDT.SHARE.Components.Globals.FormatNumber(6, false));
            lblThucNhan.DataBindings.Add("Text", this.DataSource, "ThucNhan", Company.KDT.SHARE.Components.Globals.FormatNumber(6, false));
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");
        }
        int stt = 0;

        private void lblNo_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            stt += 1;
            lblNo.Text = stt.ToString();

        }

        private void lblDonVi_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string dvt = lblDonVi.Text;
            //lblDonVi.Text = DonViTinh.GetName(dvt);
        }
    }
}
