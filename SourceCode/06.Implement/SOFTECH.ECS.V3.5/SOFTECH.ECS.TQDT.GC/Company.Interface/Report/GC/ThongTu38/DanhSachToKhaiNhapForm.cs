﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Logger;
using Company.GC.BLL.KDT.GC;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Janus.Windows.GridEX;

namespace Company.Interface.Report.GC.ThongTu38
{
    public partial class DanhSachToKhaiXuatForm_1 : BaseForm
    {
        public HopDong HopDong = new HopDong();
        public string where = "";

        public DanhSachToKhaiXuatForm_1()
        {
            InitializeComponent();
        }

        private void DanhSachToKhaiNhap_Load(object sender, EventArgs e)
        {
            dgList.DataSource = GetDataSource();
        }

        private DataTable GetDataSource()
        {
            try
            {
                const string spName = "[p_GC_DanhSachToKhaiNhapDuaVaoPhanBo]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, HopDong.ID);
                db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.VarChar, GlobalSettings.MA_HAI_QUAN);
                db.AddInParameter(dbCommand, "@MaDN", SqlDbType.VarChar, GlobalSettings.MA_DON_VI);

                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void btnBKToKhaiNhap_Click(object sender, EventArgs e)
        {
            BKToKhaiNhapTT38 bangKe = new BKToKhaiNhapTT38();
            bangKe.HopDong = this.HopDong;
            bangKe.DSTK = LayDSToKhaiDuocChon();
            bangKe.BindReport();
            bangKe.ShowPreview();
        }

        private string LayDSToKhaiDuocChon()
        {
            string temp = "";
            foreach (var row in dgList.GetCheckedRows())
            {
                string soTK = row.Cells["SoToKhai"].Value.ToString();
                temp += soTK + ", ";
            }
            return temp.Substring(0, temp.Length - 2);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //, ConditionOperator.Equal, Convert.ToDecimal(txtSoToKhaiTL.Value
                if (txtSoToKhai.Text != "0")
                {
                    if (dgList.FindAll(dgList.Tables[0].Columns["LoaiVanDon"], Janus.Windows.GridEX.ConditionOperator.Equal, Convert.ToDecimal(txtSoToKhai.Value)) > 0)
                    {

                        foreach (GridEXSelectedItem item in dgList.SelectedItems)
                            item.GetRow().IsChecked = true;
                    }
                    else
                    {
                        MLMessages("Không có tờ khai này trong danh sách", "MSG_THK79", "", false);
                    }
                }
                else
                {                
                   
                    DateTime fromDate = ccFromDate.Value;
                    DateTime toDate = ccToDate.Value;
                    if (fromDate > toDate)
                    {
                        MLMessages("Từ ngày phải nhỏ hơn hoặc bằng đến ngày.", "MSG_SEA01", "", false);
                        return;
                    }
                    fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                    toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);
                    
                    int soLuongToKhai = 0;
                    foreach (GridEXRow row in dgList.GetRows())
                    {
                        if (Convert.ToDateTime(row.Cells["NgayDangKy"].Value) >= fromDate && Convert.ToDateTime(row.Cells["NgayDangKy"].Value) <= toDate)
                        {
                            row.IsChecked = true;
                            soLuongToKhai ++;
                        }                       
                    }
                    if (soLuongToKhai > 0)
                    { 
                    }
                    else
                    {
                        MLMessages("Không có tờ khai nào được đăng ký trong thời gian đã cho", "MSG_THK79", "", false);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
