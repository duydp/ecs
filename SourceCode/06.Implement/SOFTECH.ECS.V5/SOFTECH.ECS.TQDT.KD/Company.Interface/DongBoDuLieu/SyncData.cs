﻿
using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
#if KD_V3 || KD_V4
using Company.Interface.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif GC_V3
using Company.Interface.GC;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
#endif
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
using System.Threading;
using KetQuaXuLy = Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3.KetQuaXuLy;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.DongBoDuLieu
{
    class SyncData
    {
        ISyncData wsSyncData = WebService.SyncService();
        public event EventHandler<SyncEventArgs> SyncEventArgs;
        private void OnSynce(SyncEventArgs e)
        {
            if (SyncEventArgs != null)
            {
                SyncEventArgs(this, e);
            }
        }
        private void SaveLogError(Detail chitiet, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = chitiet.SoToKhai;
            logError.MaDoanhNghiep = chitiet.MaDoanhNghiep;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = chitiet.GUIDSTR;
            logError.TrangThai = 2;
            logError.InsertUpdate();
        }
        public void KD_SendData(List<ToKhaiMauDich> tokhaiSelect, string user, string password)
        {
            try
            {


                int total = tokhaiSelect.Count;
                int pos = 0;
                int valSend = 0;
                string msgSend = string.Empty;
                foreach (ToKhaiMauDich tkmd in tokhaiSelect)
                {

                    pos++;
                    try
                    {
                        tkmd.LoadHMDCollection();
                        tkmd.LoadChungTuHaiQuan();
                        if (string.IsNullOrEmpty(tkmd.GUIDSTR))
                        {
                            tkmd.GUIDSTR = Guid.NewGuid().ToString();
                            tkmd.Update();
                        }
                        SyncDaTa syncDaTa = new SyncDaTa()
                        {
                            Type = "TKMD",
                            Declaration = new List<SyncDaTaDetail>()
                        };
                        syncDaTa.Declaration.Add(new SyncDaTaDetail
                        {
                            Issuer = user,
                            Business = tkmd.MaDoanhNghiep,
                            Reference = tkmd.GUIDSTR,
                            CustomsReference = tkmd.SoTiepNhan.ToString(),
                            Acceptance = tkmd.NgayDangKy.ToString(),
                            Number = tkmd.SoToKhai.ToString(),
                            NatureOfTransaction = tkmd.MaLoaiHinh,
                            DeclarationOffice = tkmd.MaHaiQuan,
                            StreamlineContent = tkmd.HUONGDAN,
                            Type = "KD",
                        });
                        string content = Helpers.Serializer(tkmd);
                        content = Helpers.ConvertToBase64(content);
                        syncDaTa.Content = new Content { Text = content };
                        msgSend = Helpers.Serializer(syncDaTa);
                        msgSend = wsSyncData.SendDaiLy(msgSend, user, password);
                        if (!string.IsNullOrEmpty(msgSend))
                        {
                            //Warring info
                            if (msgSend == "MK")
                            {
                                OnSynce(new SyncEventArgs(new Exception("Sai mật khẩu kết nối")));
                                GlobalSettings.USERNAME_DONGBO = string.Empty;
                                GlobalSettings.PASSWOR_DONGBO = string.Empty;
                                break;
                            }
                            else OnSynce(new SyncEventArgs(new Exception(msgSend)));

                        }
                        else
                        {
                            Status sttk = new Status();
                            sttk.GUIDSTR = tkmd.GUIDSTR;
                            sttk.MSG_STATUS = 1;
                            if (tkmd.MaLoaiHinh.Substring(0, 1).ToUpper() == "N")
                                sttk.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.KD_TOKHAI_NHAP);
                            else
                                sttk.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.KD_TOKHAI_XUAT);
                            sttk.CREATE_TIME = DateTime.Now;
                            sttk.InsertUpdate();
                            valSend++;
                            string msg = string.Format("Đã gửi thông tin {0}/{1}/{2}", tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NamDK);
                            int percent = (int)((pos * 100) / total);
                            OnSynce(new SyncEventArgs(msg, percent));
                        }
                    }
                    catch (Exception ex)
                    {

                        OnSynce(new SyncEventArgs(ex));
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
                OnSynce(new SyncEventArgs(string.Format("Đã gửi được {0}/{1} tờ khai", valSend, total), (int)((pos * 100) / total)));
            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));
            }
        }
        public void KD_SearchData(string user, string password, DateTime fromDate, DateTime toDate, bool timTatCa)
        {
            try
            {
                OnSynce(new SyncEventArgs("Bắt đầu tìm kiếm...", 0));

                SyncDaTa syncdata = new SyncDaTa();
                syncdata.issuer = GlobalSettings.MA_DON_VI;
                syncdata.FromDate = fromDate.ToString("yyyy-MM-dd");
                syncdata.ToDate = toDate.ToString("yyyy-MM-dd");
                syncdata.Type = "KD";
                syncdata.Status = timTatCa ? "1" : "0";
                Thread.Sleep(200);
                OnSynce(new SyncEventArgs("Truyền thông tin yêu cầu...", 0));
                string msg = Helpers.ConvertToBase64(Helpers.Serializer(syncdata));
                string msgFeedback = string.Empty;
                msgFeedback = wsSyncData.ReceivesViews(user, password, msg);

                if (msgFeedback == "MK")
                {
                    OnSynce(new SyncEventArgs(new Exception("Sai mật khẩu kết nối")));
                    GlobalSettings.USERNAME_DONGBO = string.Empty;
                    GlobalSettings.PASSWOR_DONGBO = string.Empty;
                    return;
                }
                else
                {
                    if (string.IsNullOrEmpty(msgFeedback))
                    {
                        OnSynce(new SyncEventArgs(new Exception("Không tìm thấy tờ khai")));
                        return;
                    }
                    else
                    {
                        SyncDaTa feedback = new SyncDaTa();
                        try
                        {
                            feedback = Helpers.Deserialize<SyncDaTa>(msgFeedback);
                        }
                        catch (System.Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            OnSynce(new SyncEventArgs(new Exception("Có lỗi trả về từ server: " + msgFeedback)));
                            return;
                        }
                        List<Detail> chitietCO = MapperDBDL.MapperView(feedback.Declaration);
                        OnSynce(new SyncEventArgs("Đã tải dữ liệu từ đại lý về", 100, null, chitietCO));
                    }
                }
            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));

            }
        }
        public void KD_GetData(string user, string password, List<Detail> detailSelect)
        {
            try
            {
                int pos = 0;
                int total = detailSelect.Count;
                int valDownload = 0;
                foreach (Detail item in detailSelect)
                {
                    string sfmtToKhai = string.Format("{0}/{1}/{2}", item.SoToKhai, item.MaLoaiHinh, item.MaHaiQuan);
                    OnSynce(new SyncEventArgs("Đang tải " + sfmtToKhai, (int)((pos++ * 100) / total)));
                    string msgFeedback = wsSyncData.SendDoanhNghiep(item.GUIDSTR, user, password);
                    if (msgFeedback == "MK")
                    {
                        OnSynce(new SyncEventArgs(new Exception("Sai tên đăng nhập hoặc mật khẩu ")));
                        GlobalSettings.USERNAME_DONGBO = string.Empty;
                        GlobalSettings.PASSWOR_DONGBO = string.Empty;
                        break;
                    }
                    SyncDaTa xml = new SyncDaTa();

                    try
                    {
                        xml = Helpers.Deserialize<SyncDaTa>(msgFeedback);
                        string content = Helpers.ConvertFromBase64(xml.Content.Text);
                        ToKhaiMauDich tk = Helpers.Deserialize<ToKhaiMauDich>(content);
                        string error = tk.InsertFullFromISyncDaTa();
                        tk.TransgferDataToSXXK();
                        if (!string.IsNullOrEmpty(error))
                        {
                            SaveLogError(item, error);
                            OnSynce(new SyncEventArgs(new Exception(string.Format("Cập nhật dữ liệu {0} không thành công!", sfmtToKhai))));
                        }
                        valDownload++;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        OnSynce(new SyncEventArgs(ex));
                    }
                }
                OnSynce(new SyncEventArgs(string.Format("Đã tải được {0}/{1} tờ khai", valDownload, total), (int)((pos * 100) / total)));

            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));
            }
        }
    }
}
