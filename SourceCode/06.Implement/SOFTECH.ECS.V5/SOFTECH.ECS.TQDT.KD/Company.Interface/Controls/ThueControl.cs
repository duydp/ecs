﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;


namespace Company.Interface.Controls
{
    public partial class ThueControl : UserControl
    {
        public ThueControl()
        {
            InitializeComponent();
        }

//         private void ThueControl_Load(object sender, EventArgs e)
//         {
// 
//         }
//         private void txtDGNT_Leave(object sender, EventArgs e)
//         {
//             if (GlobalSettings.TinhThueTGNT == "0")
//             {
//                 this.luong = Convert.ToDecimal(txtLuong.Value);
//                 if (luong != 0)
//                 {
//                     this.dgnt = Convert.ToDouble(txtDGNT.Value);// Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));
//                     txtTGNT.Value = Helpers.FormatNumeric(this.dgnt * Convert.ToDouble(luong), GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
//                     this.tgnt = Convert.ToDouble(Convert.ToDecimal(txtTGNT.Text));
//                     if (this.NhomLoaiHinh.Contains("N"))
//                         tinhthue();
//                     else
//                         tinhthue3();
//                 }
// 
//             }
// 
//         }
//         private void txtTS_NK_Leave(object sender, EventArgs e)
//         {
//             this.ts_nk = Convert.ToDouble(txtTS_NK.Value) / 100;
//             if (this.NhomLoaiHinh.Contains("N"))
//                 tinhthue2();
//             else
//                 tinhthue3();
//         }
// 
//         private void txtTS_TTDB_Leave(object sender, EventArgs e)
//         {
//             this.ts_ttdb = Convert.ToDouble(txtTS_TTDB.Value) / 100;
//             if (this.NhomLoaiHinh.Contains("N"))
//                 tinhthue2();
//             else
//                 tinhthue3();
//         }
//         private void txtTS_GTGT_Leave(object sender, EventArgs e)
//         {
//             this.ts_gtgt = Convert.ToDouble(txtTS_GTGT.Value) / 100;
//             if (this.NhomLoaiHinh.Contains("N"))
//                 tinhthue2();
//             else
//                 tinhthue3();
//         }
//         private void txtTS_CLG_Leave(object sender, EventArgs e)
//         {
//             this.tl_clg = Convert.ToDouble(txtTL_CLG.Value) / 100;
//             if (this.NhomLoaiHinh.Contains("N"))
//                 tinhthue2();
//             else
//                 tinhthue3();
//         }
//         private void txtTGTT_NK_Leave(object sender, EventArgs e)
//         {
//             if (this.NhomLoaiHinh.Contains("N"))
//                 tinhthue2();
//             else
//                 tinhthue3();
//             txtTriGiaThuKhac.Value = (decimal.Parse(txtTGTT_NK.Text) / 100) * decimal.Parse(txtTyLeThuKhac.Text);
// 
//         }
//         private void txtTyLeThuKhac_Leave(object sender, EventArgs e)
//         {
//             txtTriGiaThuKhac.Value = (Convert.ToDecimal(txtTGTT_NK.Value) / 100) * Convert.ToDecimal(txtTyLeThuKhac.Value);
//         }
//         private void txtTienThue_NK_Leave(object sender, EventArgs e)
//         {
//             if (this.NhomLoaiHinh.Contains("N"))
//                 tinhthue2();
//             else
//                 tinhthue3();
//         }
//         private void txtDonGiaTuyetDoi_Leave(object sender, EventArgs e)
//         {
//             if (this.NhomLoaiHinh.Contains("N"))
//                 tinhthue();
//             else
//                 tinhthue3();
//         }
//         private void txtTGNT_Leave(object sender, EventArgs e)
//         {
//             if (GlobalSettings.TinhThueTGNT == "1")
//             {
//                 this.luong = Convert.ToDecimal(txtLuong.Value);
//                 if (luong != 0)
//                 {
//                     this.tgnt = Convert.ToDouble(txtTGNT.Value);// Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));
//                     txtDGNT.Value = Helpers.FormatNumeric(tgnt / Convert.ToDouble(luong), GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
//                     this.dgnt = Convert.ToDouble(Convert.ToDecimal(txtDGNT.Text));
//                     if (this.NhomLoaiHinh.Contains("N"))
//                         tinhthue();
//                     else
//                         tinhthue3();
//                 }
//             }
// 
//         }
//         private double tinhthue()
//         {
//             this.tgtt_nk = this.tgnt * this.TyGiaTT;
//             if (!chkThueTuyetDoi.Checked)
//                 this.tt_nk = this.tgtt_nk * this.ts_nk;
//             else
//             {
//                 double dgntTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
//                 this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDouble(luong));
//             }
//             this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
// 
//             this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;
// 
//             this.clg = this.tgtt_nk;
//             this.st_clg = this.clg * this.tl_clg;
// 
//             this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb + this.st_clg;
//             this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;
// 
//             txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
// 
//             //txtTGNT.Value = this.tgnt;
//             txtTGTT_NK.Value = this.tgtt_nk;
//             txtTGTT_TTDB.Value = this.tgtt_ttdb;
//             txtTGTT_GTGT.Value = this.tgtt_gtgt;
//             txtCLG.Value = this.clg;
// 
//             txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
//             txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
//             txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
//             txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);
// 
//             txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
// 
//             return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
//         }
//         private double tinhthue2()
//         {
// 
//             if (!chkThueTuyetDoi.Checked)
//             {
//                 this.ts_nk = Convert.ToDouble(txtTS_NK.Value) / 100;
//             }
// 
//             this.ts_ttdb = Convert.ToDouble(txtTS_TTDB.Value) / 100;
//             this.ts_gtgt = Convert.ToDouble(txtTS_GTGT.Value) / 100;
//             this.tl_clg = Convert.ToDouble(txtTL_CLG.Value) / 100;
// 
//             //this.tgnt = Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));
// 
//             this.tgtt_nk = Convert.ToDouble(txtTGTT_NK.Value);
//             if (tgtt_nk == 0)
//             {
//                 tgtt_nk = tgnt * TyGiaTT;
//             }
//             if (!chkThueTuyetDoi.Checked)
//                 this.tt_nk = this.tgtt_nk * this.ts_nk;
//             else
//             {
//                 double dgntTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
//                 this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDouble(luong));
//             }
//             this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
//             this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;
// 
//             this.clg = this.tgtt_nk;
//             this.st_clg = this.clg * this.tl_clg;
// 
//             this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb + this.st_clg;
//             this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;
// 
//             txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
//             //txtTGNT.Value = this.tgnt;
//             txtTGTT_NK.Value = this.tgtt_nk;
//             txtTGTT_TTDB.Value = this.tgtt_ttdb;
//             txtTGTT_GTGT.Value = this.tgtt_gtgt;
//             txtCLG.Value = this.clg;
// 
//             txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
// 
//             txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
//             txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
//             txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);
// 
//             txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
// 
//             return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
//         }
//         private double tinhthue3()
//         {
// 
//             decimal tgntkophi = (decimal)tgnt;
//             if (TKMD.DKGH_ID.Trim() == "CNF" || TKMD.DKGH_ID.Trim() == "CFR") tgntkophi = (decimal)this.tgnt - TKMD.PhiVanChuyen;
// 
//             this.tgtt_nk = (double)tgntkophi * this.TyGiaTT;
// 
//             this.tt_nk = this.tgtt_nk * this.ts_nk;
// 
//             this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
//             this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;
// 
//             this.clg = this.tgtt_nk;
//             this.st_clg = this.clg * this.tl_clg;
// 
//             this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb + this.st_clg;
//             this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;
// 
//             txtTriGiaKB.Value = tgnt * this.TyGiaTT;
//             //txtTGNT.Value = this.tgnt;
//             txtTGTT_NK.Value = this.tgtt_nk;
//             txtTGTT_TTDB.Value = this.tgtt_ttdb;
//             txtTGTT_GTGT.Value = this.tgtt_gtgt;
//             txtCLG.Value = this.clg;
// 
//             txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
//             txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
//             txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
//             txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);
// 
//             txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
// 
//             return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
//         }
    }
}
