/*
Run this script on:

        192.168.72.100\ECSEXPRESS.ECS_TQDT_KD_V5    -  This database will be modified

to synchronize it with:

        192.168.72.100\ECSEXPRESS.ECS_TQDT_KD_VNACCS

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 02/13/2014 9:02:30 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]'
GO
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [TriGiaTinhThueTruocKhiKhaiBoSung] [numeric] (21, 6) NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [TriGiaTinhThueSauKhiKhaiBoSung] [numeric] (21, 6) NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [SoLuongTinhThueTruocKhiKhaiBoSung] [numeric] (21, 6) NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [SoLuongTinhThueSauKhiKhaiBoSung] [numeric] (21, 6) NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [ThueSuatTruocKhiKhaiBoSung] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [SoTienThueTruocKhiKhaiBoSung] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [SoTienThueSauKhiKhaiBoSung] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ALTER COLUMN [SoTienTangGiamThue] [numeric] (16, 6) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]'
GO
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] ALTER COLUMN [TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] [numeric] (21, 6) NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] ALTER COLUMN [SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] [numeric] (16, 6) NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] ALTER COLUMN [SoTienThueTruocKhiKhaiBoSungThuKhac] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] ALTER COLUMN [SoTienThueSauKhiKhaiBoSungThuKhac] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(4),
	@ThueSuatTruocKhiKhaiBoSung varchar(30),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung varchar(16),
	@SoTienThueSauKhiKhaiBoSung varchar(16),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
(
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
)
VALUES 
(
	@TKMDBoSung_ID,
	@SoDong,
	@SoThuTuDongHangTrenToKhaiGoc,
	@MoTaHangHoaTruocKhiKhaiBoSung,
	@MoTaHangHoaSauKhiKhaiBoSung,
	@MaSoHangHoaTruocKhiKhaiBoSung,
	@MaSoHangHoaSauKhiKhaiBoSung,
	@MaNuocXuatXuTruocKhiKhaiBoSung,
	@MaNuocXuatXuSauKhiKhaiBoSung,
	@TriGiaTinhThueTruocKhiKhaiBoSung,
	@TriGiaTinhThueSauKhiKhaiBoSung,
	@SoLuongTinhThueTruocKhiKhaiBoSung,
	@SoLuongTinhThueSauKhiKhaiBoSung,
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
	@ThueSuatTruocKhiKhaiBoSung,
	@ThueSuatSauKhiKhaiBoSung,
	@SoTienThueTruocKhiKhaiBoSung,
	@SoTienThueSauKhiKhaiBoSung,
	@HienThiMienThueTruocKhiKhaiBoSung,
	@HienThiMienThueSauKhiKhaiBoSung,
	@HienThiSoTienTangGiamThueXuatNhapKhau,
	@SoTienTangGiamThue
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(4),
	@ThueSuatTruocKhiKhaiBoSung varchar(30),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung varchar(16),
	@SoTienThueSauKhiKhaiBoSung varchar(16),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 6)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
SET
	[TKMDBoSung_ID] = @TKMDBoSung_ID,
	[SoDong] = @SoDong,
	[SoThuTuDongHangTrenToKhaiGoc] = @SoThuTuDongHangTrenToKhaiGoc,
	[MoTaHangHoaTruocKhiKhaiBoSung] = @MoTaHangHoaTruocKhiKhaiBoSung,
	[MoTaHangHoaSauKhiKhaiBoSung] = @MoTaHangHoaSauKhiKhaiBoSung,
	[MaSoHangHoaTruocKhiKhaiBoSung] = @MaSoHangHoaTruocKhiKhaiBoSung,
	[MaSoHangHoaSauKhiKhaiBoSung] = @MaSoHangHoaSauKhiKhaiBoSung,
	[MaNuocXuatXuTruocKhiKhaiBoSung] = @MaNuocXuatXuTruocKhiKhaiBoSung,
	[MaNuocXuatXuSauKhiKhaiBoSung] = @MaNuocXuatXuSauKhiKhaiBoSung,
	[TriGiaTinhThueTruocKhiKhaiBoSung] = @TriGiaTinhThueTruocKhiKhaiBoSung,
	[TriGiaTinhThueSauKhiKhaiBoSung] = @TriGiaTinhThueSauKhiKhaiBoSung,
	[SoLuongTinhThueTruocKhiKhaiBoSung] = @SoLuongTinhThueTruocKhiKhaiBoSung,
	[SoLuongTinhThueSauKhiKhaiBoSung] = @SoLuongTinhThueSauKhiKhaiBoSung,
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung] = @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
	[ThueSuatTruocKhiKhaiBoSung] = @ThueSuatTruocKhiKhaiBoSung,
	[ThueSuatSauKhiKhaiBoSung] = @ThueSuatSauKhiKhaiBoSung,
	[SoTienThueTruocKhiKhaiBoSung] = @SoTienThueTruocKhiKhaiBoSung,
	[SoTienThueSauKhiKhaiBoSung] = @SoTienThueSauKhiKhaiBoSung,
	[HienThiMienThueTruocKhiKhaiBoSung] = @HienThiMienThueTruocKhiKhaiBoSung,
	[HienThiMienThueSauKhiKhaiBoSung] = @HienThiMienThueSauKhiKhaiBoSung,
	[HienThiSoTienTangGiamThueXuatNhapKhau] = @HienThiSoTienTangGiamThueXuatNhapKhau,
	[SoTienTangGiamThue] = @SoTienTangGiamThue
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(4),
	@ThueSuatTruocKhiKhaiBoSung varchar(30),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung varchar(16),
	@SoTienThueSauKhiKhaiBoSung varchar(16),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] 
		SET
			[TKMDBoSung_ID] = @TKMDBoSung_ID,
			[SoDong] = @SoDong,
			[SoThuTuDongHangTrenToKhaiGoc] = @SoThuTuDongHangTrenToKhaiGoc,
			[MoTaHangHoaTruocKhiKhaiBoSung] = @MoTaHangHoaTruocKhiKhaiBoSung,
			[MoTaHangHoaSauKhiKhaiBoSung] = @MoTaHangHoaSauKhiKhaiBoSung,
			[MaSoHangHoaTruocKhiKhaiBoSung] = @MaSoHangHoaTruocKhiKhaiBoSung,
			[MaSoHangHoaSauKhiKhaiBoSung] = @MaSoHangHoaSauKhiKhaiBoSung,
			[MaNuocXuatXuTruocKhiKhaiBoSung] = @MaNuocXuatXuTruocKhiKhaiBoSung,
			[MaNuocXuatXuSauKhiKhaiBoSung] = @MaNuocXuatXuSauKhiKhaiBoSung,
			[TriGiaTinhThueTruocKhiKhaiBoSung] = @TriGiaTinhThueTruocKhiKhaiBoSung,
			[TriGiaTinhThueSauKhiKhaiBoSung] = @TriGiaTinhThueSauKhiKhaiBoSung,
			[SoLuongTinhThueTruocKhiKhaiBoSung] = @SoLuongTinhThueTruocKhiKhaiBoSung,
			[SoLuongTinhThueSauKhiKhaiBoSung] = @SoLuongTinhThueSauKhiKhaiBoSung,
			[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung] = @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
			[ThueSuatTruocKhiKhaiBoSung] = @ThueSuatTruocKhiKhaiBoSung,
			[ThueSuatSauKhiKhaiBoSung] = @ThueSuatSauKhiKhaiBoSung,
			[SoTienThueTruocKhiKhaiBoSung] = @SoTienThueTruocKhiKhaiBoSung,
			[SoTienThueSauKhiKhaiBoSung] = @SoTienThueSauKhiKhaiBoSung,
			[HienThiMienThueTruocKhiKhaiBoSung] = @HienThiMienThueTruocKhiKhaiBoSung,
			[HienThiMienThueSauKhiKhaiBoSung] = @HienThiMienThueSauKhiKhaiBoSung,
			[HienThiSoTienTangGiamThueXuatNhapKhau] = @HienThiSoTienTangGiamThueXuatNhapKhau,
			[SoTienTangGiamThue] = @SoTienTangGiamThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
		(
			[TKMDBoSung_ID],
			[SoDong],
			[SoThuTuDongHangTrenToKhaiGoc],
			[MoTaHangHoaTruocKhiKhaiBoSung],
			[MoTaHangHoaSauKhiKhaiBoSung],
			[MaSoHangHoaTruocKhiKhaiBoSung],
			[MaSoHangHoaSauKhiKhaiBoSung],
			[MaNuocXuatXuTruocKhiKhaiBoSung],
			[MaNuocXuatXuSauKhiKhaiBoSung],
			[TriGiaTinhThueTruocKhiKhaiBoSung],
			[TriGiaTinhThueSauKhiKhaiBoSung],
			[SoLuongTinhThueTruocKhiKhaiBoSung],
			[SoLuongTinhThueSauKhiKhaiBoSung],
			[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
			[ThueSuatTruocKhiKhaiBoSung],
			[ThueSuatSauKhiKhaiBoSung],
			[SoTienThueTruocKhiKhaiBoSung],
			[SoTienThueSauKhiKhaiBoSung],
			[HienThiMienThueTruocKhiKhaiBoSung],
			[HienThiMienThueSauKhiKhaiBoSung],
			[HienThiSoTienTangGiamThueXuatNhapKhau],
			[SoTienTangGiamThue]
		)
		VALUES 
		(
			@TKMDBoSung_ID,
			@SoDong,
			@SoThuTuDongHangTrenToKhaiGoc,
			@MoTaHangHoaTruocKhiKhaiBoSung,
			@MoTaHangHoaSauKhiKhaiBoSung,
			@MaSoHangHoaTruocKhiKhaiBoSung,
			@MaSoHangHoaSauKhiKhaiBoSung,
			@MaNuocXuatXuTruocKhiKhaiBoSung,
			@MaNuocXuatXuSauKhiKhaiBoSung,
			@TriGiaTinhThueTruocKhiKhaiBoSung,
			@TriGiaTinhThueSauKhiKhaiBoSung,
			@SoLuongTinhThueTruocKhiKhaiBoSung,
			@SoLuongTinhThueSauKhiKhaiBoSung,
			@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
			@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
			@ThueSuatTruocKhiKhaiBoSung,
			@ThueSuatSauKhiKhaiBoSung,
			@SoTienThueTruocKhiKhaiBoSung,
			@SoTienThueSauKhiKhaiBoSung,
			@HienThiMienThueTruocKhiKhaiBoSung,
			@HienThiMienThueSauKhiKhaiBoSung,
			@HienThiSoTienTangGiamThueXuatNhapKhau,
			@SoTienTangGiamThue
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]
	@TKMDBoSung_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[TKMDBoSung_ID] = @TKMDBoSung_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]
	@TKMDBoSung_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[TKMDBoSung_ID] = @TKMDBoSung_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(16, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac varchar(16),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueSauKhiKhaiBoSungThuKhac varchar(16),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
(
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
)
VALUES 
(
	@HMDBoSung_ID,
	@SoDong,
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
	@ThueSuatTruocKhiKhaiBoSungThuKhac,
	@SoTienThueTruocKhiKhaiBoSungThuKhac,
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
	@ThueSuatSauKhiKhaiBoSungThuKhac,
	@SoTienThueSauKhiKhaiBoSungThuKhac,
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
	@HienThiSoTienTangGiamThueVaThuKhac,
	@SoTienTangGiamThuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]
	@ID bigint,
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(16, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac varchar(16),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueSauKhiKhaiBoSungThuKhac varchar(16),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
SET
	[HMDBoSung_ID] = @HMDBoSung_ID,
	[SoDong] = @SoDong,
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] = @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac] = @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
	[ThueSuatTruocKhiKhaiBoSungThuKhac] = @ThueSuatTruocKhiKhaiBoSungThuKhac,
	[SoTienThueTruocKhiKhaiBoSungThuKhac] = @SoTienThueTruocKhiKhaiBoSungThuKhac,
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac] = @TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac] = @MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
	[ThueSuatSauKhiKhaiBoSungThuKhac] = @ThueSuatSauKhiKhaiBoSungThuKhac,
	[SoTienThueSauKhiKhaiBoSungThuKhac] = @SoTienThueSauKhiKhaiBoSungThuKhac,
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung] = @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung] = @HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
	[HienThiSoTienTangGiamThueVaThuKhac] = @HienThiSoTienTangGiamThueVaThuKhac,
	[SoTienTangGiamThuKhac] = @SoTienTangGiamThuKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]
	@ID bigint,
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(16, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac varchar(16),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueSauKhiKhaiBoSungThuKhac varchar(16),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] 
		SET
			[HMDBoSung_ID] = @HMDBoSung_ID,
			[SoDong] = @SoDong,
			[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] = @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
			[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac] = @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
			[ThueSuatTruocKhiKhaiBoSungThuKhac] = @ThueSuatTruocKhiKhaiBoSungThuKhac,
			[SoTienThueTruocKhiKhaiBoSungThuKhac] = @SoTienThueTruocKhiKhaiBoSungThuKhac,
			[TriGiaTinhThueSauKhiKhaiBoSungThuKhac] = @TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
			[SoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			[MaApDungThueSuatSauKhiKhaiBoSungThuKhac] = @MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
			[ThueSuatSauKhiKhaiBoSungThuKhac] = @ThueSuatSauKhiKhaiBoSungThuKhac,
			[SoTienThueSauKhiKhaiBoSungThuKhac] = @SoTienThueSauKhiKhaiBoSungThuKhac,
			[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung] = @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
			[HienThiMienThueVaThuKhacSauKhiKhaiBoSung] = @HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
			[HienThiSoTienTangGiamThueVaThuKhac] = @HienThiSoTienTangGiamThueVaThuKhac,
			[SoTienTangGiamThuKhac] = @SoTienTangGiamThuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
		(
			[HMDBoSung_ID],
			[SoDong],
			[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
			[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
			[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
			[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
			[ThueSuatTruocKhiKhaiBoSungThuKhac],
			[SoTienThueTruocKhiKhaiBoSungThuKhac],
			[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
			[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
			[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
			[ThueSuatSauKhiKhaiBoSungThuKhac],
			[SoTienThueSauKhiKhaiBoSungThuKhac],
			[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
			[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
			[HienThiSoTienTangGiamThueVaThuKhac],
			[SoTienTangGiamThuKhac]
		)
		VALUES 
		(
			@HMDBoSung_ID,
			@SoDong,
			@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
			@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
			@ThueSuatTruocKhiKhaiBoSungThuKhac,
			@SoTienThueTruocKhiKhaiBoSungThuKhac,
			@TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
			@SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			@MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
			@ThueSuatSauKhiKhaiBoSungThuKhac,
			@SoTienThueSauKhiKhaiBoSungThuKhac,
			@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
			@HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
			@HienThiSoTienTangGiamThueVaThuKhac,
			@SoTienTangGiamThuKhac
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]
	@HMDBoSung_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[HMDBoSung_ID] = @HMDBoSung_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]
	@HMDBoSung_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[HMDBoSung_ID] = @HMDBoSung_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]	

GO

UPDATE [t_VNACC_Category_Common] SET Name_VN = N'Đường không'
WHERE ReferenceDB = 'E005' AND code = 1

GO

           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '14.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('14.2', GETDATE(), N'Cap nhat to khai sua doi bo sung')
END	