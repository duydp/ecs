using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_TIA_HangHoa : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string MaSoHangHoa { set; get; }
		public decimal SoLuongBanDau { set; get; }
		public string DVTSoLuongBanDau { set; get; }
		public decimal SoLuongDaTaiNhapTaiXuat { set; get; }
		public string DVTSoLuongDaTaiNhapTaiXuat { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TIA_HangHoa> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TIA_HangHoa> collection = new List<KDT_VNACCS_TIA_HangHoa>();
			while (reader.Read())
			{
				KDT_VNACCS_TIA_HangHoa entity = new KDT_VNACCS_TIA_HangHoa();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHangHoa"))) entity.MaSoHangHoa = reader.GetString(reader.GetOrdinal("MaSoHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongBanDau"))) entity.SoLuongBanDau = reader.GetDecimal(reader.GetOrdinal("SoLuongBanDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTSoLuongBanDau"))) entity.DVTSoLuongBanDau = reader.GetString(reader.GetOrdinal("DVTSoLuongBanDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaTaiNhapTaiXuat"))) entity.SoLuongDaTaiNhapTaiXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongDaTaiNhapTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTSoLuongDaTaiNhapTaiXuat"))) entity.DVTSoLuongDaTaiNhapTaiXuat = reader.GetString(reader.GetOrdinal("DVTSoLuongDaTaiNhapTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TIA_HangHoa> collection, long id)
        {
            foreach (KDT_VNACCS_TIA_HangHoa item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TIA_HangHoa VALUES(@Master_ID, @MaSoHangHoa, @SoLuongBanDau, @DVTSoLuongBanDau, @SoLuongDaTaiNhapTaiXuat, @DVTSoLuongDaTaiNhapTaiXuat, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TIA_HangHoa SET Master_ID = @Master_ID, MaSoHangHoa = @MaSoHangHoa, SoLuongBanDau = @SoLuongBanDau, DVTSoLuongBanDau = @DVTSoLuongBanDau, SoLuongDaTaiNhapTaiXuat = @SoLuongDaTaiNhapTaiXuat, DVTSoLuongDaTaiNhapTaiXuat = @DVTSoLuongDaTaiNhapTaiXuat, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TIA_HangHoa WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongBanDau", SqlDbType.Decimal, "SoLuongBanDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSoLuongBanDau", SqlDbType.VarChar, "DVTSoLuongBanDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDaTaiNhapTaiXuat", SqlDbType.Decimal, "SoLuongDaTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSoLuongDaTaiNhapTaiXuat", SqlDbType.VarChar, "DVTSoLuongDaTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongBanDau", SqlDbType.Decimal, "SoLuongBanDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSoLuongBanDau", SqlDbType.VarChar, "DVTSoLuongBanDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDaTaiNhapTaiXuat", SqlDbType.Decimal, "SoLuongDaTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSoLuongDaTaiNhapTaiXuat", SqlDbType.VarChar, "DVTSoLuongDaTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TIA_HangHoa VALUES(@Master_ID, @MaSoHangHoa, @SoLuongBanDau, @DVTSoLuongBanDau, @SoLuongDaTaiNhapTaiXuat, @DVTSoLuongDaTaiNhapTaiXuat, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TIA_HangHoa SET Master_ID = @Master_ID, MaSoHangHoa = @MaSoHangHoa, SoLuongBanDau = @SoLuongBanDau, DVTSoLuongBanDau = @DVTSoLuongBanDau, SoLuongDaTaiNhapTaiXuat = @SoLuongDaTaiNhapTaiXuat, DVTSoLuongDaTaiNhapTaiXuat = @DVTSoLuongDaTaiNhapTaiXuat, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TIA_HangHoa WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongBanDau", SqlDbType.Decimal, "SoLuongBanDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSoLuongBanDau", SqlDbType.VarChar, "DVTSoLuongBanDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDaTaiNhapTaiXuat", SqlDbType.Decimal, "SoLuongDaTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSoLuongDaTaiNhapTaiXuat", SqlDbType.VarChar, "DVTSoLuongDaTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongBanDau", SqlDbType.Decimal, "SoLuongBanDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSoLuongBanDau", SqlDbType.VarChar, "DVTSoLuongBanDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDaTaiNhapTaiXuat", SqlDbType.Decimal, "SoLuongDaTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSoLuongDaTaiNhapTaiXuat", SqlDbType.VarChar, "DVTSoLuongDaTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TIA_HangHoa Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TIA_HangHoa> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TIA_HangHoa> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TIA_HangHoa> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TIA_HangHoa(long master_ID, string maSoHangHoa, decimal soLuongBanDau, string dVTSoLuongBanDau, decimal soLuongDaTaiNhapTaiXuat, string dVTSoLuongDaTaiNhapTaiXuat, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TIA_HangHoa entity = new KDT_VNACCS_TIA_HangHoa();	
			entity.Master_ID = master_ID;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.SoLuongBanDau = soLuongBanDau;
			entity.DVTSoLuongBanDau = dVTSoLuongBanDau;
			entity.SoLuongDaTaiNhapTaiXuat = soLuongDaTaiNhapTaiXuat;
			entity.DVTSoLuongDaTaiNhapTaiXuat = dVTSoLuongDaTaiNhapTaiXuat;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@SoLuongBanDau", SqlDbType.Decimal, SoLuongBanDau);
			db.AddInParameter(dbCommand, "@DVTSoLuongBanDau", SqlDbType.VarChar, DVTSoLuongBanDau);
			db.AddInParameter(dbCommand, "@SoLuongDaTaiNhapTaiXuat", SqlDbType.Decimal, SoLuongDaTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@DVTSoLuongDaTaiNhapTaiXuat", SqlDbType.VarChar, DVTSoLuongDaTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TIA_HangHoa> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TIA_HangHoa item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TIA_HangHoa(long id, long master_ID, string maSoHangHoa, decimal soLuongBanDau, string dVTSoLuongBanDau, decimal soLuongDaTaiNhapTaiXuat, string dVTSoLuongDaTaiNhapTaiXuat, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TIA_HangHoa entity = new KDT_VNACCS_TIA_HangHoa();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.SoLuongBanDau = soLuongBanDau;
			entity.DVTSoLuongBanDau = dVTSoLuongBanDau;
			entity.SoLuongDaTaiNhapTaiXuat = soLuongDaTaiNhapTaiXuat;
			entity.DVTSoLuongDaTaiNhapTaiXuat = dVTSoLuongDaTaiNhapTaiXuat;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TIA_HangHoa_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@SoLuongBanDau", SqlDbType.Decimal, SoLuongBanDau);
			db.AddInParameter(dbCommand, "@DVTSoLuongBanDau", SqlDbType.VarChar, DVTSoLuongBanDau);
			db.AddInParameter(dbCommand, "@SoLuongDaTaiNhapTaiXuat", SqlDbType.Decimal, SoLuongDaTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@DVTSoLuongDaTaiNhapTaiXuat", SqlDbType.VarChar, DVTSoLuongDaTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TIA_HangHoa> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TIA_HangHoa item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TIA_HangHoa(long id, long master_ID, string maSoHangHoa, decimal soLuongBanDau, string dVTSoLuongBanDau, decimal soLuongDaTaiNhapTaiXuat, string dVTSoLuongDaTaiNhapTaiXuat, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TIA_HangHoa entity = new KDT_VNACCS_TIA_HangHoa();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.SoLuongBanDau = soLuongBanDau;
			entity.DVTSoLuongBanDau = dVTSoLuongBanDau;
			entity.SoLuongDaTaiNhapTaiXuat = soLuongDaTaiNhapTaiXuat;
			entity.DVTSoLuongDaTaiNhapTaiXuat = dVTSoLuongDaTaiNhapTaiXuat;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@SoLuongBanDau", SqlDbType.Decimal, SoLuongBanDau);
			db.AddInParameter(dbCommand, "@DVTSoLuongBanDau", SqlDbType.VarChar, DVTSoLuongBanDau);
			db.AddInParameter(dbCommand, "@SoLuongDaTaiNhapTaiXuat", SqlDbType.Decimal, SoLuongDaTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@DVTSoLuongDaTaiNhapTaiXuat", SqlDbType.VarChar, DVTSoLuongDaTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TIA_HangHoa> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TIA_HangHoa item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TIA_HangHoa(long id)
		{
			KDT_VNACCS_TIA_HangHoa entity = new KDT_VNACCS_TIA_HangHoa();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_HangHoa_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TIA_HangHoa> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TIA_HangHoa item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

	}	
}