﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface
{
    public partial class VNACC_TIA_ManagerForm : BaseForm
    {
        private string where = "";
        private List<KDT_VNACCS_TIA> listTIA = new List<KDT_VNACCS_TIA>();
        private DateTime dayMin = new DateTime(1900, 1, 1);
        public VNACC_TIA_ManagerForm()
        {
            InitializeComponent();

            this.Text = "Theo dõi hàng tạm nhập/ tái xuất";
        }

        private void VNACC_TIA_ManagerForm_Load(object sender, EventArgs e)
        {
            cbbTrangThaiXuLy.SelectedValue = "0";
            //where = "TrangThaiXuLy='-1'";
            //LoadData(where);
        }
        //private void LoadData(string where)
        //{
        //    listTIA = KDT_VNACCS_TIA.SelectCollectionDynamic(where, "");
        //    grList.DataSource = listTIA;
        //}
        private KDT_VNACCS_TIA getTIA(long id)
        {

            foreach (KDT_VNACCS_TIA TIA in listTIA)
            {
                if (TIA.ID == id)
                {
                    return TIA;
                }
            }

            return null;
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
            if (id != 0)
            {
                VNACC_TIAForm f = new VNACC_TIAForm();
                KDT_VNACCS_TIA TIA = getTIA(id);
                f.TIA = TIA;
                f.ShowDialog();
                btnTimKiem_Click(null, null);
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soTN = txtSoGiayPhepDauTu.Text.Trim();
                string loaiHinh = (cbbPhanLoaiXuatNhap.SelectedValue.ToString() == "0") ? "" : cbbPhanLoaiXuatNhap.SelectedValue.ToString();
                string trangThai = (cbbTrangThaiXuLy.SelectedValue == null) ? "0" : cbbTrangThaiXuLy.SelectedValue.ToString();
                string where = String.Empty;
                if (soTN != "")
                    where = "GP_GCNDauTuSo='" + soTN + "' and ";
                if (loaiHinh != "")
                    where = where + "PhanLoaiXuatNhapKhau='" + loaiHinh + "' and ";
                if (trangThai != "")
                    where = where + "TrangThaiXuLy='" + trangThai + "'";
                listTIA.Clear();
                listTIA = KDT_VNACCS_TIA.SelectCollectionDynamic(where, "ID");

                grList.DataSource = listTIA;
                grList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void cbbTrangThaiXuLy_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }
    }
}
