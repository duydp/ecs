﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Company.KD.BLL.KDT;
using Infragistics.Excel;

namespace Company.Interface
{
    public partial class ReadExcelForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        private string ConnectionStringExcel = "Driver={Microsoft Excel Driver(*.xls)};DriverId=790;Dbq=";
        public ReadExcelForm()
        {
            InitializeComponent();
        }

        private void editBox8_TextChanged(object sender, EventArgs e)
        {

        }
        //Hien thi Dialog box 
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        
        private int checkNPLExit(string maNPL)
        {
            for (int i = 0; i < this.TKMD.HMDCollection.Count; i++)
            {
                if (this.TKMD.HMDCollection[i].MaPhu.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }
      
        private void txtTriGiaColumn_TextChanged(object sender, EventArgs e)
        {

        }


        private void tinhLaiThue()
        {            
            this.TKMD.TongTriGiaKhaiBao = 0;
            double Phi = Convert.ToDouble(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            double TongTriGiaHang = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
                this.TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
            }
            double TriGiaTTMotDong = Phi / TongTriGiaHang;
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                hmd.TriGiaTT = Math.Round((TriGiaTTMotDong * hmd.TriGiaKB + hmd.TriGiaKB) * Convert.ToDouble(TKMD.TyGiaTinhThue), 0);
                this.TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);

                hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDouble(hmd.SoLuong);
                hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100,MidpointRounding.AwayFromZero);
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
                hmd.TriGiaThuKhac = Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero);
            }            
        }

        private void tinhTongTriGiaKhaiBao()
        {            
            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                this.TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
                this.TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                error.SetIconPadding(txtRow, 8);
                return;

            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"MSG_EXC01","",txtSheet.Text, false);
                return;
            }
            //editing :
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int maHangCol = ConvertCharToInt(maHangColumn);

            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int tenHangCol = ConvertCharToInt(tenHangColumn);

            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text.Trim());
            int maHSCol = ConvertCharToInt(maHSColumn);

            char dVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int dVTCol = ConvertCharToInt(dVTColumn);

            char soLuongColumn = Convert.ToChar(txtSoLuongColumn.Text.Trim());
            int soLuongCol = ConvertCharToInt(soLuongColumn);

            char donGiaColumn = Convert.ToChar(txtDonGiaColumn.Text.Trim());
            int donGiaCol = ConvertCharToInt(donGiaColumn);

            char xuatXuColumn = Convert.ToChar(txtXuatXuColumn.Text.Trim());
            int xuatXuCol = ConvertCharToInt(xuatXuColumn);

            char TriGiaTTColumn = Convert.ToChar(txtTriGiaTT.Text);
            int TriGiaTTCol = ConvertCharToInt(TriGiaTTColumn);

            char tsXNKColumn = Convert.ToChar(txtTSXNK.Text.Trim());
            int tsXNKCol = ConvertCharToInt(tsXNKColumn);

            char tsTSDBColumn = Convert.ToChar(txtTSTTDB.Text.Trim());
            int tsTSDBCol = ConvertCharToInt(tsTSDBColumn);

            char tsTSVATColumn = Convert.ToChar(txtTSVAT.Text.Trim());
            int tsTSVATCol = ConvertCharToInt(tsTSVATColumn);

            char tsTyLeCLColumn = Convert.ToChar(txtCLGia.Text.Trim());
            int tsTyLeCLCol = ConvertCharToInt(tsTyLeCLColumn);

            bool tcvn3 = ckTCVN3.Checked;
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        //NguyenPhuLieu npl = new NguyenPhuLieu();                       
                        Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
                        hmd.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                        if (hmd.MaHS.Trim().Length == 0)
                        {
                            MLMessages("Mã HS ở dòng " + (wsr.Index + 1) + " không được để trống.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        hmd.MaPhu = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        if (!tcvn3)
                            hmd.TenHang = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                        else
                            hmd.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(wsr.Cells[tenHangCol].Value.ToString());
                        if (hmd.TenHang.Trim().Length == 0)
                            continue;
                        try
                        {
                            string idNuocXX = Convert.ToString(wsr.Cells[xuatXuCol].Value).Trim();
                            string tenNuocXX = Nuoc_GetName(idNuocXX);
                            hmd.NuocXX_ID = idNuocXX;
                            if (Company.KD.BLL.DuLieuChuan.Nuoc.GetID(hmd.NuocXX_ID).Equals(string.Empty))
                            {
                                MLMessages("Nước xuất xứ ở dòng " + (wsr.Index + 1) + " không hợp lệ.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
                        }
                        catch
                        {
                            MLMessages("Nước xuất xứ ở dòng " + (wsr.Index + 1) + " không hợp lệ.","MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        try
                        {
                            hmd.DVT_ID = Company.KD.BLL.DuLieuChuan.DonViTinh.GetID(Convert.ToString(wsr.Cells[dVTCol].Value).Trim());
                            if (hmd.DVT_ID.Equals(""))
                            {
                                MLMessages("Đơn vị tính ở dòng " + (wsr.Index + 1) + " không hợp lệ.", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
                        }
                        catch
                        {
                            MLMessages("Đơn vị tính ở dòng " + (wsr.Index + 1)+" không hợp lệ.","MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                            return;
                        }
                        hmd.DonGiaKB = Convert.ToDouble(Convert.ToDecimal(wsr.Cells[donGiaCol].Value));
                        hmd.SoLuong = Convert.ToDecimal(wsr.Cells[soLuongCol].Value);
                        hmd.TriGiaKB = Convert.ToDouble(Convert.ToDecimal(hmd.DonGiaKB * Convert.ToDouble(hmd.SoLuong)));
                        hmd.TriGiaTT =  Convert.ToDouble(Convert.ToDecimal(wsr.Cells[TriGiaTTCol].Value));
                        hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDouble(hmd.SoLuong);
                        hmd.ThueSuatXNK = Convert.ToDouble(wsr.Cells[tsXNKCol].Value);
                        hmd.ThueSuatTTDB = Convert.ToDouble(wsr.Cells[tsTSDBCol].Value);
                        hmd.ThueSuatGTGT = Convert.ToDouble(wsr.Cells[tsTSVATCol].Value);
                        hmd.TyLeThuKhac = Convert.ToDouble(wsr.Cells[tsTyLeCLCol].Value);
                        //hmd.TriGiaKB_VND = Convert.ToDouble(hmd.TriGiaKB) * Convert.ToDouble(TKMD.TyGiaTinhThue);
                        hmd.ThueXNK = Math.Round(hmd.ThueSuatXNK * hmd.TriGiaTT / 100,MidpointRounding.AwayFromZero);
                        hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                        hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
                        hmd.TriGiaThuKhac = Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero);
                        //int i = checkNPLExit(hmd.MaPhu);
                        //if (i >= 0)
                        //{
                        //    if (ShowMessage("Sản phẩm có mã \"" + hmd.MaPhu + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế sản phẩm trên lưới bằng sản phẩm này?", true) == "Yes")
                        //        this.TKMD.HMDCollection[i] = hmd;

                        //}
                        //else 
                        this.TKMD.HMDCollection.Add(hmd);
                        this.SaveDefault();

                    }
                    catch
                    {
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?","MSG_EXC06",Convert.ToString(wsr.Index + 1), true) != "Yes")
                        {
                            return;
                        }
                        this.SaveDefault();
                    }
                }
            }
            if (TKMD.HMDCollection.Count > 0)
            {
                if (ckTuDongTT.Checked)
                    tinhLaiThue();
                else
                    tinhTongTriGiaKhaiBao();
                MLMessages("Nhập hàng từ Excel thành công ","MSG_WRN34","", false);
            }
            this.SaveDefault();
            this.Close();

        }
        private void SaveDefault()
        {
            DocExcel docExcel = new DocExcel();
            docExcel.insertThongSoHangTK(txtSheet.Text.Trim(),
                                                                  txtRow.Text.Trim(),
                                                                  txtMaHangColumn.Text.Trim(),
                                                                  txtTenHangColumn.Text.Trim(),
                                                                  txtMaHSColumn.Text.Trim(),
                                                                  txtDVTColumn.Text.Trim(),
                                                                  txtSoLuongColumn.Text.Trim(),
                                                                  txtDonGiaColumn.Text.Trim(),
                                                                  txtXuatXuColumn.Text.Trim(),
                                                                  txtTriGiaTT.Text.Trim(),
                                                                  txtTSXNK.Text,
                                                                  txtTSVAT.Text,
                                                                  txtTSTTDB.Text,
                                                                  txtCLGia.Text

                                                                  );

        }
        private void ReadDefault()
        {
            DocExcel docExcel = new DocExcel();
            docExcel.ReadDefault(txtSheet,
                                                                  txtRow,
                                                                  txtMaHangColumn,
                                                                  txtTenHangColumn,
                                                                  txtMaHSColumn,
                                                                  txtDVTColumn,
                                                                  txtSoLuongColumn,
                                                                  txtDonGiaColumn,
                                                                  txtXuatXuColumn,
                                                                  txtTriGiaTT,
                                                                  txtTSXNK,
                                                                  txtTSVAT,
                                                                  txtTSTTDB,
                                                                  txtCLGia
                                                                  );
        }

        private void ReadExcelForm_Load(object sender, EventArgs e)
        {
            //openFileDialog1.InitialDirectory = Application.StartupPath;
            //ReadDefault();
        }

        private void txtXuatXuColumn_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMaHSColumn_TextChanged(object sender, EventArgs e)
        {

        }
    }
}