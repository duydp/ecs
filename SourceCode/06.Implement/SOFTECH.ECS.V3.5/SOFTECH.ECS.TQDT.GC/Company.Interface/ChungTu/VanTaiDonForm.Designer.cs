﻿namespace Company.Interface
{
    partial class VanTaiDonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VanTaiDonForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayVanDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.chkQuocTich = new Janus.Windows.EditControls.UICheckBox();
            this.txtNoiDi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHieuChuyenDi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.ccNgayKhoiHanh = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label21 = new System.Windows.Forms.Label();
            this.txtTenHangVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkHangRoi = new Janus.Windows.EditControls.UICheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaHangVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ctrQuocTichPTVT = new Company.Interface.Controls.NuocHControl();
            this.label5 = new System.Windows.Forms.Label();
            this.ctrNuocXuat = new Company.Interface.Controls.NuocHControl();
            this.txtTenPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoHieuPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ccNgayDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenNguoiNhanHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMaNguoiNhanHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenNguoiGiaoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMaNguoiGiaoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtCuaKhauXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.cuaKhauControl1 = new Company.Interface.Controls.CuaKhauControl();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenNguoiNhanHangTG = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaNguoiNhanHangTG = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.grbDiaDiemXepHang = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaDiemGiaoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMaDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel0Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.TaoContainer1 = new Janus.Windows.UI.CommandBars.UICommand("TaoContainer");
            this.ThemContainerExcel1 = new Janus.Windows.UI.CommandBars.UICommand("ThemContainerExcel");
            this.Xoa1 = new Janus.Windows.UI.CommandBars.UICommand("Xoa");
            this.Ghi1 = new Janus.Windows.UI.CommandBars.UICommand("Ghi");
            this.TaoContainer = new Janus.Windows.UI.CommandBars.UICommand("TaoContainer");
            this.ThemContainerExcel = new Janus.Windows.UI.CommandBars.UICommand("ThemContainerExcel");
            this.Xoa = new Janus.Windows.UI.CommandBars.UICommand("Xoa");
            this.Ghi = new Janus.Windows.UI.CommandBars.UICommand("Ghi");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMaDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayVanDon = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoVanDon = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoHieuPTVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayDen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenPTVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenhangVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenNguoiNhan = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenNguoiGiao = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvCuaKhauNhap = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvCuaKhauXuat = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDKGH = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDiaDiemGiaoHang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDiaDiemDoHang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDiaDiemXepHang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNoiDi = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoHieuChuyenDi = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayKhoiHanh = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).BeginInit();
            this.grbDiaDiemXepHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            this.uiPanel0Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayVanDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoVanDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenPTVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenhangVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNguoiNhan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNguoiGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvCuaKhauNhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvCuaKhauXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDKGH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemGiaoHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaDiemDoHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaDiemXepHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNoiDi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuChuyenDi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayKhoiHanh)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox8);
            this.grbMain.Controls.Add(this.grbDiaDiemXepHang);
            this.grbMain.Controls.Add(this.uiGroupBox16);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Location = new System.Drawing.Point(3, 35);
            this.grbMain.Size = new System.Drawing.Size(781, 429);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ccNgayVanDon);
            this.uiGroupBox2.Controls.Add(this.chkQuocTich);
            this.uiGroupBox2.Controls.Add(this.txtNoiDi);
            this.uiGroupBox2.Controls.Add(this.txtSoHieuChuyenDi);
            this.uiGroupBox2.Controls.Add(this.label19);
            this.uiGroupBox2.Controls.Add(this.label20);
            this.uiGroupBox2.Controls.Add(this.ccNgayKhoiHanh);
            this.uiGroupBox2.Controls.Add(this.label21);
            this.uiGroupBox2.Controls.Add(this.txtTenHangVT);
            this.uiGroupBox2.Controls.Add(this.chkHangRoi);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtMaHangVT);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.ctrQuocTichPTVT);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.ctrNuocXuat);
            this.uiGroupBox2.Controls.Add(this.txtTenPTVT);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtSoHieuPTVT);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.ccNgayDen);
            this.uiGroupBox2.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(11, 15);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(767, 196);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayVanDon
            // 
            // 
            // 
            // 
            this.ccNgayVanDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayVanDon.DropDownCalendar.Name = "";
            this.ccNgayVanDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayVanDon.IsNullDate = true;
            this.ccNgayVanDon.Location = new System.Drawing.Point(477, 22);
            this.ccNgayVanDon.Name = "ccNgayVanDon";
            this.ccNgayVanDon.Nullable = true;
            this.ccNgayVanDon.NullButtonText = "Xóa";
            this.ccNgayVanDon.ShowNullButton = true;
            this.ccNgayVanDon.Size = new System.Drawing.Size(267, 21);
            this.ccNgayVanDon.TabIndex = 2;
            this.ccNgayVanDon.TodayButtonText = "Hôm nay";
            this.ccNgayVanDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayVanDon.VisualStyleManager = this.vsmMain;
            // 
            // chkQuocTich
            // 
            this.chkQuocTich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkQuocTich.Location = new System.Drawing.Point(683, 76);
            this.chkQuocTich.Name = "chkQuocTich";
            this.chkQuocTich.Size = new System.Drawing.Size(61, 23);
            this.chkQuocTich.TabIndex = 7;
            this.chkQuocTich.Text = "Chưa rõ";
            this.chkQuocTich.CheckedChanged += new System.EventHandler(this.chkQuocTich_CheckedChanged);
            // 
            // txtNoiDi
            // 
            this.txtNoiDi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiDi.Location = new System.Drawing.Point(87, 157);
            this.txtNoiDi.MaxLength = 255;
            this.txtNoiDi.Name = "txtNoiDi";
            this.txtNoiDi.Size = new System.Drawing.Size(193, 21);
            this.txtNoiDi.TabIndex = 12;
            this.txtNoiDi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNoiDi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNoiDi.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHieuChuyenDi
            // 
            this.txtSoHieuChuyenDi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuChuyenDi.Location = new System.Drawing.Point(395, 157);
            this.txtSoHieuChuyenDi.MaxLength = 255;
            this.txtSoHieuChuyenDi.Name = "txtSoHieuChuyenDi";
            this.txtSoHieuChuyenDi.Size = new System.Drawing.Size(158, 21);
            this.txtSoHieuChuyenDi.TabIndex = 13;
            this.txtSoHieuChuyenDi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuChuyenDi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHieuChuyenDi.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(563, 162);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "Ngày khởi hành";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(290, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Số hiệu chuyến đi";
            // 
            // ccNgayKhoiHanh
            // 
            // 
            // 
            // 
            this.ccNgayKhoiHanh.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayKhoiHanh.DropDownCalendar.Name = "";
            this.ccNgayKhoiHanh.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKhoiHanh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKhoiHanh.IsNullDate = true;
            this.ccNgayKhoiHanh.Location = new System.Drawing.Point(650, 157);
            this.ccNgayKhoiHanh.Name = "ccNgayKhoiHanh";
            this.ccNgayKhoiHanh.Nullable = true;
            this.ccNgayKhoiHanh.NullButtonText = "Xóa";
            this.ccNgayKhoiHanh.ShowNullButton = true;
            this.ccNgayKhoiHanh.Size = new System.Drawing.Size(94, 21);
            this.ccNgayKhoiHanh.TabIndex = 14;
            this.ccNgayKhoiHanh.TodayButtonText = "Hôm nay";
            this.ccNgayKhoiHanh.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKhoiHanh.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(8, 162);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = "Nơi đi";
            // 
            // txtTenHangVT
            // 
            this.txtTenHangVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangVT.Location = new System.Drawing.Point(87, 130);
            this.txtTenHangVT.MaxLength = 255;
            this.txtTenHangVT.Name = "txtTenHangVT";
            this.txtTenHangVT.Size = new System.Drawing.Size(294, 21);
            this.txtTenHangVT.TabIndex = 10;
            this.txtTenHangVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenHangVT.VisualStyleManager = this.vsmMain;
            // 
            // chkHangRoi
            // 
            this.chkHangRoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHangRoi.Location = new System.Drawing.Point(477, 130);
            this.chkHangRoi.Name = "chkHangRoi";
            this.chkHangRoi.Size = new System.Drawing.Size(76, 23);
            this.chkHangRoi.TabIndex = 11;
            this.chkHangRoi.Text = "Hàng rời";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Tên hãng VT";
            // 
            // txtMaHangVT
            // 
            this.txtMaHangVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangVT.Location = new System.Drawing.Point(87, 102);
            this.txtMaHangVT.MaxLength = 255;
            this.txtMaHangVT.Name = "txtMaHangVT";
            this.txtMaHangVT.Size = new System.Drawing.Size(294, 21);
            this.txtMaHangVT.TabIndex = 8;
            this.txtMaHangVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHangVT.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Mã hãng VT";
            // 
            // ctrQuocTichPTVT
            // 
            this.ctrQuocTichPTVT.BackColor = System.Drawing.Color.Transparent;
            this.ctrQuocTichPTVT.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrQuocTichPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrQuocTichPTVT.Location = new System.Drawing.Point(477, 76);
            this.ctrQuocTichPTVT.Ma = "";
            this.ctrQuocTichPTVT.Name = "ctrQuocTichPTVT";
            this.ctrQuocTichPTVT.ReadOnly = false;
            this.ctrQuocTichPTVT.Size = new System.Drawing.Size(200, 22);
            this.ctrQuocTichPTVT.TabIndex = 6;
            this.ctrQuocTichPTVT.VisualStyleManager = null;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(392, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Nước xuất";
            // 
            // ctrNuocXuat
            // 
            this.ctrNuocXuat.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuat.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrNuocXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXuat.Location = new System.Drawing.Point(477, 101);
            this.ctrNuocXuat.Ma = "";
            this.ctrNuocXuat.Name = "ctrNuocXuat";
            this.ctrNuocXuat.ReadOnly = false;
            this.ctrNuocXuat.Size = new System.Drawing.Size(283, 22);
            this.ctrNuocXuat.TabIndex = 9;
            this.ctrNuocXuat.VisualStyleManager = null;
            // 
            // txtTenPTVT
            // 
            this.txtTenPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPTVT.Location = new System.Drawing.Point(87, 74);
            this.txtTenPTVT.MaxLength = 255;
            this.txtTenPTVT.Name = "txtTenPTVT";
            this.txtTenPTVT.Size = new System.Drawing.Size(294, 21);
            this.txtTenPTVT.TabIndex = 5;
            this.txtTenPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(392, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Quốc tịch PTVT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Tên PTVT";
            // 
            // txtSoHieuPTVT
            // 
            this.txtSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuPTVT.Location = new System.Drawing.Point(87, 47);
            this.txtSoHieuPTVT.MaxLength = 255;
            this.txtSoHieuPTVT.Name = "txtSoHieuPTVT";
            this.txtSoHieuPTVT.Size = new System.Drawing.Size(294, 21);
            this.txtSoHieuPTVT.TabIndex = 3;
            this.txtSoHieuPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHieuPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(392, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ngày đến";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Số hiệu PTVT";
            // 
            // ccNgayDen
            // 
            // 
            // 
            // 
            this.ccNgayDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDen.DropDownCalendar.Name = "";
            this.ccNgayDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDen.IsNullDate = true;
            this.ccNgayDen.Location = new System.Drawing.Point(477, 49);
            this.ccNgayDen.Name = "ccNgayDen";
            this.ccNgayDen.Nullable = true;
            this.ccNgayDen.NullButtonText = "Xóa";
            this.ccNgayDen.ShowNullButton = true;
            this.ccNgayDen.Size = new System.Drawing.Size(267, 21);
            this.ccNgayDen.TabIndex = 4;
            this.ccNgayDen.TodayButtonText = "Hôm nay";
            this.ccNgayDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDen.VisualStyleManager = this.vsmMain;
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon.Location = new System.Drawing.Point(87, 20);
            this.txtSoVanDon.MaxLength = 255;
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(294, 21);
            this.txtSoVanDon.TabIndex = 1;
            this.txtSoVanDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoVanDon.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(392, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày vận đơn";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(61, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số vận đơn";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtTenNguoiNhanHang);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.txtMaNguoiNhanHang);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(10, 223);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(185, 99);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.Text = "Người nhận hàng";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNguoiNhanHang
            // 
            this.txtTenNguoiNhanHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiNhanHang.Location = new System.Drawing.Point(33, 44);
            this.txtTenNguoiNhanHang.MaxLength = 255;
            this.txtTenNguoiNhanHang.Multiline = true;
            this.txtTenNguoiNhanHang.Name = "txtTenNguoiNhanHang";
            this.txtTenNguoiNhanHang.Size = new System.Drawing.Size(146, 49);
            this.txtTenNguoiNhanHang.TabIndex = 16;
            this.txtTenNguoiNhanHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiNhanHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenNguoiNhanHang.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(4, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Tên";
            // 
            // txtMaNguoiNhanHang
            // 
            this.txtMaNguoiNhanHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiNhanHang.Location = new System.Drawing.Point(33, 17);
            this.txtMaNguoiNhanHang.MaxLength = 255;
            this.txtMaNguoiNhanHang.Name = "txtMaNguoiNhanHang";
            this.txtMaNguoiNhanHang.Size = new System.Drawing.Size(146, 21);
            this.txtMaNguoiNhanHang.TabIndex = 15;
            this.txtMaNguoiNhanHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiNhanHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaNguoiNhanHang.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(4, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Mã";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtTenNguoiGiaoHang);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.txtMaNguoiGiaoHang);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(207, 223);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(185, 99);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.Text = "Người giao hàng";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNguoiGiaoHang
            // 
            this.txtTenNguoiGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiGiaoHang.Location = new System.Drawing.Point(33, 44);
            this.txtTenNguoiGiaoHang.MaxLength = 255;
            this.txtTenNguoiGiaoHang.Multiline = true;
            this.txtTenNguoiGiaoHang.Name = "txtTenNguoiGiaoHang";
            this.txtTenNguoiGiaoHang.Size = new System.Drawing.Size(148, 49);
            this.txtTenNguoiGiaoHang.TabIndex = 18;
            this.txtTenNguoiGiaoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiGiaoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenNguoiGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tên";
            // 
            // txtMaNguoiGiaoHang
            // 
            this.txtMaNguoiGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiGiaoHang.Location = new System.Drawing.Point(33, 17);
            this.txtMaNguoiGiaoHang.MaxLength = 255;
            this.txtMaNguoiGiaoHang.Name = "txtMaNguoiGiaoHang";
            this.txtMaNguoiGiaoHang.Size = new System.Drawing.Size(148, 21);
            this.txtMaNguoiGiaoHang.TabIndex = 17;
            this.txtMaNguoiGiaoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiGiaoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaNguoiGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Mã";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtCuaKhauXuat);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(593, 224);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(185, 98);
            this.uiGroupBox4.TabIndex = 4;
            this.uiGroupBox4.Text = "Cửa khẩu xuất";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtCuaKhauXuat
            // 
            this.txtCuaKhauXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCuaKhauXuat.Location = new System.Drawing.Point(9, 16);
            this.txtCuaKhauXuat.MaxLength = 255;
            this.txtCuaKhauXuat.Multiline = true;
            this.txtCuaKhauXuat.Name = "txtCuaKhauXuat";
            this.txtCuaKhauXuat.Size = new System.Drawing.Size(170, 76);
            this.txtCuaKhauXuat.TabIndex = 20;
            this.txtCuaKhauXuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCuaKhauXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtCuaKhauXuat.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.cuaKhauControl1);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(402, 224);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(185, 98);
            this.uiGroupBox5.TabIndex = 3;
            this.uiGroupBox5.Text = "Cửa khẩu nhập";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // cuaKhauControl1
            // 
            this.cuaKhauControl1.BackColor = System.Drawing.Color.Transparent;
            this.cuaKhauControl1.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.cuaKhauControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuaKhauControl1.Location = new System.Drawing.Point(12, 16);
            this.cuaKhauControl1.Ma = "";
            this.cuaKhauControl1.Name = "cuaKhauControl1";
            this.cuaKhauControl1.ReadOnly = false;
            this.cuaKhauControl1.Size = new System.Drawing.Size(181, 76);
            this.cuaKhauControl1.TabIndex = 19;
            this.cuaKhauControl1.VisualStyleManager = null;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.txtTenNguoiNhanHangTG);
            this.uiGroupBox6.Controls.Add(this.label10);
            this.uiGroupBox6.Controls.Add(this.txtMaNguoiNhanHangTG);
            this.uiGroupBox6.Controls.Add(this.label11);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(10, 328);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(185, 101);
            this.uiGroupBox6.TabIndex = 5;
            this.uiGroupBox6.Text = "Người nhận hàng trung gian";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNguoiNhanHangTG
            // 
            this.txtTenNguoiNhanHangTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiNhanHangTG.Location = new System.Drawing.Point(33, 47);
            this.txtTenNguoiNhanHangTG.MaxLength = 255;
            this.txtTenNguoiNhanHangTG.Multiline = true;
            this.txtTenNguoiNhanHangTG.Name = "txtTenNguoiNhanHangTG";
            this.txtTenNguoiNhanHangTG.Size = new System.Drawing.Size(146, 48);
            this.txtTenNguoiNhanHangTG.TabIndex = 22;
            this.txtTenNguoiNhanHangTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiNhanHangTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenNguoiNhanHangTG.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(2, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Tên";
            // 
            // txtMaNguoiNhanHangTG
            // 
            this.txtMaNguoiNhanHangTG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiNhanHangTG.Location = new System.Drawing.Point(33, 20);
            this.txtMaNguoiNhanHangTG.MaxLength = 255;
            this.txtMaNguoiNhanHangTG.Name = "txtMaNguoiNhanHangTG";
            this.txtMaNguoiNhanHangTG.Size = new System.Drawing.Size(146, 21);
            this.txtMaNguoiNhanHangTG.TabIndex = 21;
            this.txtMaNguoiNhanHangTG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiNhanHangTG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaNguoiNhanHangTG.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(4, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Mã";
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox16.Controls.Add(this.cbDKGH);
            this.uiGroupBox16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox16.Location = new System.Drawing.Point(207, 328);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(185, 41);
            this.uiGroupBox16.TabIndex = 6;
            this.uiGroupBox16.Text = "Điều kiện giao hàng";
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox16.VisualStyleManager = this.vsmMain;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(7, 16);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(174, 21);
            this.cbDKGH.TabIndex = 23;
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // grbDiaDiemXepHang
            // 
            this.grbDiaDiemXepHang.BackColor = System.Drawing.Color.Transparent;
            this.grbDiaDiemXepHang.Controls.Add(this.txtDiaDiemGiaoHang);
            this.grbDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemXepHang.Location = new System.Drawing.Point(207, 369);
            this.grbDiaDiemXepHang.Name = "grbDiaDiemXepHang";
            this.grbDiaDiemXepHang.Size = new System.Drawing.Size(185, 60);
            this.grbDiaDiemXepHang.TabIndex = 7;
            this.grbDiaDiemXepHang.Text = "Địa điểm giao hàng";
            this.grbDiaDiemXepHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaDiemGiaoHang
            // 
            this.txtDiaDiemGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemGiaoHang.Location = new System.Drawing.Point(8, 16);
            this.txtDiaDiemGiaoHang.MaxLength = 255;
            this.txtDiaDiemGiaoHang.Multiline = true;
            this.txtDiaDiemGiaoHang.Name = "txtDiaDiemGiaoHang";
            this.txtDiaDiemGiaoHang.Size = new System.Drawing.Size(170, 38);
            this.txtDiaDiemGiaoHang.TabIndex = 24;
            this.txtDiaDiemGiaoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemGiaoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaDiemGiaoHang.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.txtTenDiaDiemXepHang);
            this.uiGroupBox8.Controls.Add(this.label12);
            this.uiGroupBox8.Controls.Add(this.txtMaDiaDiemXepHang);
            this.uiGroupBox8.Controls.Add(this.label13);
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.Location = new System.Drawing.Point(592, 328);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(185, 101);
            this.uiGroupBox8.TabIndex = 9;
            this.uiGroupBox8.Text = "Địa điểm xếp hàng";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDiaDiemXepHang
            // 
            this.txtTenDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDiaDiemXepHang.Location = new System.Drawing.Point(31, 46);
            this.txtTenDiaDiemXepHang.MaxLength = 255;
            this.txtTenDiaDiemXepHang.Multiline = true;
            this.txtTenDiaDiemXepHang.Name = "txtTenDiaDiemXepHang";
            this.txtTenDiaDiemXepHang.Size = new System.Drawing.Size(148, 49);
            this.txtTenDiaDiemXepHang.TabIndex = 28;
            this.txtTenDiaDiemXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(25, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Tên";
            // 
            // txtMaDiaDiemXepHang
            // 
            this.txtMaDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDiaDiemXepHang.Location = new System.Drawing.Point(31, 19);
            this.txtMaDiaDiemXepHang.MaxLength = 255;
            this.txtMaDiaDiemXepHang.Name = "txtMaDiaDiemXepHang";
            this.txtMaDiaDiemXepHang.Size = new System.Drawing.Size(148, 21);
            this.txtMaDiaDiemXepHang.TabIndex = 27;
            this.txtMaDiaDiemXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã";
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.uiPanel0.Id = new System.Guid("2b7a958a-cf32-4a55-82fb-3ac003b3f28a");
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("2b7a958a-cf32-4a55-82fb-3ac003b3f28a"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(781, 230), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("2b7a958a-cf32-4a55-82fb-3ac003b3f28a"), new System.Drawing.Point(106, 220), new System.Drawing.Size(200, 200), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(106, 220);
            this.uiPanel0.InnerContainer = this.uiPanel0Container;
            this.uiPanel0.Location = new System.Drawing.Point(3, 252);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(781, 230);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Thông tin Container";
            // 
            // uiPanel0Container
            // 
            this.uiPanel0Container.Controls.Add(this.dgList);
            this.uiPanel0Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel0Container.Name = "uiPanel0Container";
            this.uiPanel0Container.Size = new System.Drawing.Size(779, 202);
            this.uiPanel0Container.TabIndex = 0;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(779, 202);
            this.dgList.TabIndex = 3;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            this.dgList.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_FormattingRow);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.TaoContainer,
            this.ThemContainerExcel,
            this.Xoa,
            this.Ghi});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.TaoContainer1,
            this.ThemContainerExcel1,
            this.Xoa1,
            this.Ghi1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(454, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // TaoContainer1
            // 
            this.TaoContainer1.Key = "TaoContainer";
            this.TaoContainer1.Name = "TaoContainer1";
            // 
            // ThemContainerExcel1
            // 
            this.ThemContainerExcel1.Key = "ThemContainerExcel";
            this.ThemContainerExcel1.Name = "ThemContainerExcel1";
            // 
            // Xoa1
            // 
            this.Xoa1.Key = "Xoa";
            this.Xoa1.Name = "Xoa1";
            // 
            // Ghi1
            // 
            this.Ghi1.Key = "Ghi";
            this.Ghi1.Name = "Ghi1";
            // 
            // TaoContainer
            // 
            this.TaoContainer.Icon = ((System.Drawing.Icon)(resources.GetObject("TaoContainer.Icon")));
            this.TaoContainer.Key = "TaoContainer";
            this.TaoContainer.Name = "TaoContainer";
            this.TaoContainer.Text = "Thêm Container";
            // 
            // ThemContainerExcel
            // 
            this.ThemContainerExcel.Icon = ((System.Drawing.Icon)(resources.GetObject("ThemContainerExcel.Icon")));
            this.ThemContainerExcel.Key = "ThemContainerExcel";
            this.ThemContainerExcel.Name = "ThemContainerExcel";
            this.ThemContainerExcel.Text = "Thêm Container Excel";
            // 
            // Xoa
            // 
            this.Xoa.Icon = ((System.Drawing.Icon)(resources.GetObject("Xoa.Icon")));
            this.Xoa.Key = "Xoa";
            this.Xoa.Name = "Xoa";
            this.Xoa.Text = "Xóa Container";
            // 
            // Ghi
            // 
            this.Ghi.Icon = ((System.Drawing.Icon)(resources.GetObject("Ghi.Icon")));
            this.Ghi.Key = "Ghi";
            this.Ghi.Name = "Ghi";
            this.Ghi.Text = "Ghi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(787, 32);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.txtTenDiaDiemDoHang);
            this.uiGroupBox7.Controls.Add(this.label16);
            this.uiGroupBox7.Controls.Add(this.txtMaDiaDiemDoHang);
            this.uiGroupBox7.Controls.Add(this.label18);
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(402, 329);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(185, 100);
            this.uiGroupBox7.TabIndex = 8;
            this.uiGroupBox7.Text = "Địa điểm dỡ hàng";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDiaDiemDoHang
            // 
            this.txtTenDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDiaDiemDoHang.Location = new System.Drawing.Point(31, 45);
            this.txtTenDiaDiemDoHang.MaxLength = 255;
            this.txtTenDiaDiemDoHang.Multiline = true;
            this.txtTenDiaDiemDoHang.Name = "txtTenDiaDiemDoHang";
            this.txtTenDiaDiemDoHang.Size = new System.Drawing.Size(148, 49);
            this.txtTenDiaDiemDoHang.TabIndex = 26;
            this.txtTenDiaDiemDoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(25, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Tên";
            // 
            // txtMaDiaDiemDoHang
            // 
            this.txtMaDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDiaDiemDoHang.Location = new System.Drawing.Point(31, 18);
            this.txtMaDiaDiemDoHang.MaxLength = 255;
            this.txtMaDiaDiemDoHang.Name = "txtMaDiaDiemDoHang";
            this.txtMaDiaDiemDoHang.Size = new System.Drawing.Size(148, 21);
            this.txtMaDiaDiemDoHang.TabIndex = 25;
            this.txtMaDiaDiemDoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(21, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Mã";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.HostingForm = this;
            // 
            // rfvNgayVanDon
            // 
            this.rfvNgayVanDon.ControlToValidate = this.ccNgayVanDon;
            this.rfvNgayVanDon.ErrorMessage = "\"Ngày vận đơn\" không được bỏ trống.";
            this.rfvNgayVanDon.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayVanDon.Icon")));
            this.rfvNgayVanDon.Tag = "rfvNgayVanDon";
            // 
            // rfvSoVanDon
            // 
            this.rfvSoVanDon.ControlToValidate = this.txtSoVanDon;
            this.rfvSoVanDon.ErrorMessage = "\"Số vận đơn\" không được để trống.";
            this.rfvSoVanDon.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoVanDon.Icon")));
            this.rfvSoVanDon.Tag = "rfvSoVanDon";
            // 
            // rfvSoHieuPTVT
            // 
            this.rfvSoHieuPTVT.ControlToValidate = this.txtSoHieuPTVT;
            this.rfvSoHieuPTVT.ErrorMessage = "\"Số hiệu PTVT\" không được để trống.";
            this.rfvSoHieuPTVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHieuPTVT.Icon")));
            this.rfvSoHieuPTVT.Tag = "rfvSoVanDon";
            // 
            // rfvNgayDen
            // 
            this.rfvNgayDen.ControlToValidate = this.ccNgayDen;
            this.rfvNgayDen.ErrorMessage = "\"Ngày đến PTVT\" không được để trống.";
            this.rfvNgayDen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayDen.Icon")));
            this.rfvNgayDen.Tag = "rfvNgayDen";
            // 
            // rfvTenPTVT
            // 
            this.rfvTenPTVT.ControlToValidate = this.txtTenPTVT;
            this.rfvTenPTVT.ErrorMessage = "\"Tên PTVT\" không được để trống.";
            this.rfvTenPTVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenPTVT.Icon")));
            this.rfvTenPTVT.Tag = "rfvTenPTVT";
            // 
            // rfvTenhangVT
            // 
            this.rfvTenhangVT.ControlToValidate = this.txtTenHangVT;
            this.rfvTenhangVT.ErrorMessage = "\"Tên hãng  vận tải\" không được để trống.";
            this.rfvTenhangVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenhangVT.Icon")));
            this.rfvTenhangVT.Tag = "rfvTenhangVT";
            // 
            // rfvTenNguoiNhan
            // 
            this.rfvTenNguoiNhan.ControlToValidate = this.txtTenNguoiNhanHang;
            this.rfvTenNguoiNhan.ErrorMessage = "\"Tên người nhận hàng\" không được để trống.";
            this.rfvTenNguoiNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenNguoiNhan.Icon")));
            this.rfvTenNguoiNhan.Tag = "rfvTenhangVT";
            // 
            // rfvTenNguoiGiao
            // 
            this.rfvTenNguoiGiao.ControlToValidate = this.txtTenNguoiGiaoHang;
            this.rfvTenNguoiGiao.ErrorMessage = "\"Tên người giao hàng\" không được để trống.";
            this.rfvTenNguoiGiao.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenNguoiGiao.Icon")));
            this.rfvTenNguoiGiao.Tag = "rfvTenNguoiGiao";
            // 
            // rfvCuaKhauNhap
            // 
            this.rfvCuaKhauNhap.ControlToValidate = this.cuaKhauControl1;
            this.rfvCuaKhauNhap.ErrorMessage = "\"Cửa khẩu nhập\" không được để trống.";
            this.rfvCuaKhauNhap.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvCuaKhauNhap.Icon")));
            this.rfvCuaKhauNhap.Tag = "rfvCuaKhauNhap";
            // 
            // rfvCuaKhauXuat
            // 
            this.rfvCuaKhauXuat.ControlToValidate = this.txtCuaKhauXuat;
            this.rfvCuaKhauXuat.ErrorMessage = "\"Cửa khẩu Xuất\" không được để trống.";
            this.rfvCuaKhauXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvCuaKhauXuat.Icon")));
            this.rfvCuaKhauXuat.Tag = "rfvCuaKhauXuat";
            // 
            // rfvDKGH
            // 
            this.rfvDKGH.ControlToValidate = this.cbDKGH;
            this.rfvDKGH.ErrorMessage = "\"Điều kiện giao hàng\" không được để trống.";
            this.rfvDKGH.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDKGH.Icon")));
            this.rfvDKGH.Tag = "rfvDKGH";
            // 
            // rfvDiaDiemGiaoHang
            // 
            this.rfvDiaDiemGiaoHang.ControlToValidate = this.txtDiaDiemGiaoHang;
            this.rfvDiaDiemGiaoHang.ErrorMessage = "\"Địa điểm giao hàng\" không được để trống.";
            this.rfvDiaDiemGiaoHang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaDiemGiaoHang.Icon")));
            this.rfvDiaDiemGiaoHang.Tag = "rfvDiaDiemGiaoHang";
            // 
            // rfvTenDiaDiemDoHang
            // 
            this.rfvTenDiaDiemDoHang.ControlToValidate = this.txtTenDiaDiemDoHang;
            this.rfvTenDiaDiemDoHang.ErrorMessage = "\"Tên địa điểm dỡ hàng\" không được để trống.";
            this.rfvTenDiaDiemDoHang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDiaDiemDoHang.Icon")));
            this.rfvTenDiaDiemDoHang.Tag = "rfvTenDiaDiemDoHang";
            // 
            // rfvTenDiaDiemXepHang
            // 
            this.rfvTenDiaDiemXepHang.ControlToValidate = this.txtTenDiaDiemXepHang;
            this.rfvTenDiaDiemXepHang.ErrorMessage = "\"Tên địa điểm xếp hàng\" không được để trống.";
            this.rfvTenDiaDiemXepHang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDiaDiemXepHang.Icon")));
            this.rfvTenDiaDiemXepHang.Tag = "rfvTenDiaDiemXepHang";
            // 
            // rfvNoiDi
            // 
            this.rfvNoiDi.ControlToValidate = this.txtNoiDi;
            this.rfvNoiDi.ErrorMessage = "\"Nơi đi\" không được để trống.";
            this.rfvNoiDi.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNoiDi.Icon")));
            this.rfvNoiDi.Tag = "rfvNoiDi";
            // 
            // rfvSoHieuChuyenDi
            // 
            this.rfvSoHieuChuyenDi.ControlToValidate = this.txtSoHieuChuyenDi;
            this.rfvSoHieuChuyenDi.ErrorMessage = "\"Số hiệu chuyến đi\" không được để trống.";
            this.rfvSoHieuChuyenDi.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHieuChuyenDi.Icon")));
            this.rfvSoHieuChuyenDi.Tag = "rfvSoHieuChuyenDi";
            // 
            // rfvNgayKhoiHanh
            // 
            this.rfvNgayKhoiHanh.ControlToValidate = this.ccNgayKhoiHanh;
            this.rfvNgayKhoiHanh.ErrorMessage = "\"Ngày khởi hành\" không được để trống.";
            this.rfvNgayKhoiHanh.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayKhoiHanh.Icon")));
            this.rfvNgayKhoiHanh.Tag = "rfvNgayKhoiHanh";
            // 
            // VanTaiDonForm
            // 
            this.ClientSize = new System.Drawing.Size(787, 485);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VanTaiDonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin vận tải đơn";
            this.Load += new System.EventHandler(this.VanTaiDonForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).EndInit();
            this.grbDiaDiemXepHang.ResumeLayout(false);
            this.grbDiaDiemXepHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            this.uiPanel0Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayVanDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoVanDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenPTVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenhangVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNguoiNhan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNguoiGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvCuaKhauNhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvCuaKhauXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDKGH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemGiaoHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaDiemDoHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaDiemXepHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNoiDi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuChuyenDi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayKhoiHanh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private Company.Interface.Controls.NuocHControl ctrNuocXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenPTVT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuPTVT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDen;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiNhanHang;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiNhanHang;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangVT;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangVT;
        private System.Windows.Forms.Label label7;
        private Company.Interface.Controls.NuocHControl ctrQuocTichPTVT;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtCuaKhauXuat;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Company.Interface.Controls.CuaKhauControl cuaKhauControl1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiGiaoHang;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiGiaoHang;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiNhanHangTG;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiNhanHangTG;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox16;
        private Janus.Windows.EditControls.UIComboBox cbDKGH;
        private Janus.Windows.EditControls.UIGroupBox grbDiaDiemXepHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemGiaoHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemXepHang;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemXepHang;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel0;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel0Container;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand TaoContainer;
        private Janus.Windows.UI.CommandBars.UICommand Xoa;
        private Janus.Windows.UI.CommandBars.UICommand ThemContainerExcel;
        private Janus.Windows.UI.CommandBars.UICommand TaoContainer1;
        private Janus.Windows.UI.CommandBars.UICommand Xoa1;
        private Janus.Windows.UI.CommandBars.UICommand ThemContainerExcel1;
        private Janus.Windows.EditControls.UICheckBox chkHangRoi;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemDoHang;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemDoHang;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.UI.CommandBars.UICommand Ghi;
        private Janus.Windows.UI.CommandBars.UICommand Ghi1;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayVanDon;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoVanDon;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoHieuPTVT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayDen;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenPTVT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenhangVT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenNguoiNhan;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenNguoiGiao;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvCuaKhauNhap;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvCuaKhauXuat;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDKGH;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaDiemGiaoHang;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDiaDiemDoHang;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDiaDiemXepHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDi;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuChuyenDi;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKhoiHanh;
        private System.Windows.Forms.Label label21;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNoiDi;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoHieuChuyenDi;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayKhoiHanh;
        private Janus.Windows.EditControls.UICheckBox chkQuocTich;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayVanDon;
    }
}
