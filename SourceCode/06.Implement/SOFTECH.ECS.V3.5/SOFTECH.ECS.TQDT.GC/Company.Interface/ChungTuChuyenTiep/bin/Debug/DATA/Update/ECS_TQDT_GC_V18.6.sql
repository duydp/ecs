
/****** Object:  StoredProcedure [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]    Script Date: 07/23/2014 15:54:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Huynh Ngoc Khanh    
-- Create date:     18/04/2013
-- Description:     View BC 09/HSTK-TT117
-- =============================================    
ALTER PROCEDURE [dbo].[p_GC_BC09HSTK_GC_TT117_NEW]     
 -- Add the parameters for the stored procedure here    
 @IDHopDong BIGINT,    
 @MaHaiQuan CHAR(6),    
 @MaDoanhNghiep VARCHAR(14)     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;   
  
 --DECLARE
 --@IDHopDong BIGINT,    
 --@MaHaiQuan CHAR(6),    
 --@MaDoanhNghiep VARCHAR(14) 
 --SET @IDHopDong = 444
 --SET @MaHaiQuan = 'C34C'
 --SET @MaDoanhNghiep = '0400101556'
 SELECT  BangKe.SoNgayToKhai, BangKe.TenHang, BangKe.SoLuong, BangKe.TriGia, t_KDT_GC_HopDong.SoHopDong + ', ' + CONVERT(NVARCHAR(20),     
                      t_KDT_GC_HopDong.NgayKy, 103) + ', ' + CONVERT(NVARCHAR(20), t_KDT_GC_HopDong.NgayHetHan, 103) AS HdPhuLuc, BangKe.CuaKhau,     
                      BangKe.SoNgayBL, t_KDT_GC_HopDong.ID AS IDHopDong, t_KDT_GC_HopDong.MaHaiQuan, t_KDT_GC_HopDong.MaDoanhNghiep     FROM
 (
 (SELECT  CONVERT(NVARCHAR(20), 
 (case when TKMD.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = TKMD.SoToKhai) else TKMD.SoToKhai end )) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKMD.NgayDangKy, 103) AS SoNgayToKhai, t_KDT_HangMauDich.TenHang, t_KDT_HangMauDich.SoLuong, 
								  t_KDT_HangMauDich.TriGiaKB AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau,
								   CASE PTVT_ID 
								  WHEN '001' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  WHEN '005' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103) 
								  ELSE CASE TKMD.SoVanDon 
								  WHEN '' THEN t_GC_BC08TT74_VanDonToKhaiXuat.SoVanDon + ', ' + CONVERT(NVARCHAR(20), t_GC_BC08TT74_VanDonToKhaiXuat.NgayVanDon, 103)
								  ELSE
								   TKMD.SoVanDon + ', ' + CONVERT(NVARCHAR(20), TKMD.NgayVanDon, 103) 
								       END 
								   END AS SoNgayBL
			    FROM (SELECT ID,SoToKhai,NgayDangKy,MaLoaiHinh,CuaKhau_ID,SoVanDon,NgayVanDon,PTVT_ID
			            FROM t_KDT_ToKhaiMauDich  
				WHERE t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' 
				AND t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong AND t_KDT_ToKhaiMauDich.MaDoanhNghiep = @MaDoanhNghiep 
				AND t_KDT_ToKhaiMauDich.MaHaiQuan = @MaHaiQuan) AS TKMD
	   LEFT JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKMD.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKMD.MaLoaiHinh
	   INNER JOIN t_KDT_HangMauDich ON TKMD.ID = t_KDT_HangMauDich.TKMD_ID
	   LEFT  JOIN ( select * from t_GC_BC08TT74_VanDonToKhaiXuat WHERE t_GC_BC08TT74_VanDonToKhaiXuat.IDHopDong = @IDHopDong) t_GC_BC08TT74_VanDonToKhaiXuat  ON t_GC_BC08TT74_VanDonToKhaiXuat.TKMD_ID = TKMD.ID)
 UNION
 (SELECT CONVERT(NVARCHAR(20),
  (case when TKCT.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = TKCT.SoToKhai) else TKCT.SoToKhai end )) + N'/' + t_HaiQuan_LoaiHinhMauDich.Ten_VT + N', ' + CONVERT(NVARCHAR(20), 
								  TKCT.NgayDangKy, 103) AS SoNgayToKhai,t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.SoLuong, 
								  t_KDT_GC_HangChuyenTiep.TriGia AS TriGia, t_HaiQuan_CuaKhau.Ten AS CuaKhau, '' AS SoNgayBL 
  FROM (SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep 
                WHERE  t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE 'X%' OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE '%X')
						AND  t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep = @MaDoanhNghiep AND t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan = @MaHaiQuan) AS TKCT
	   LEFT JOIN t_HaiQuan_CuaKhau ON t_HaiQuan_CuaKhau.ID = TKCT.CuaKhau_ID
	   INNER JOIN t_HaiQuan_LoaiHinhMauDich ON t_HaiQuan_LoaiHinhMauDich.ID = TKCT.MaLoaiHinh
	   INNER JOIN t_KDT_GC_HangChuyenTiep ON TKCT.ID = t_KDT_GC_HangChuyenTiep.Master_ID)
)	
AS BangKe    
INNER JOIN t_KDT_GC_HopDong ON t_KDT_GC_HopDong.ID = @IDHopDong
		 
END

GO

/****** Object:  StoredProcedure [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectToKhai]    Script Date: 07/23/2014 16:05:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 16, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_BC08TT74_VanDonToKhaiXuat_SelectToKhai]
	@HopDongID bigint
AS

SELECT TKMD.TKMD_ID,TKMD.SoToKhai,TKMD.SoToKhaiVNACCS,TKMD.MaLoaiHinh,TKMD.NgayDangKy,VanDonXuat.SoVanDon,VanDonXuat.NgayVanDon
FROM
	(SELECT * FROM  t_GC_BC08TT74_VanDonToKhaiXuat WHERE IDHopDong = @HopDongID)  VanDonXuat 
	RIGHT JOIN
	(SELECT t_KDT_ToKhaiMauDich.ID AS TKMD_ID, t_KDT_ToKhaiMauDich.SoToKhai AS SoToKhai,t_KDT_ToKhaiMauDich.LoaiVanDon AS SoToKhaiVNACCS,t_KDT_ToKhaiMauDich.MaLoaiHinh AS MaLoaiHinh,t_KDT_ToKhaiMauDich.NgayDangKy AS NgayDangKy
	   FROM t_KDT_ToKhaiMauDich  WHERE t_KDT_ToKhaiMauDich.IDHopDong = @HopDongID AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%') TKMD   
	ON TKMD.TKMD_ID = VanDonXuat.TKMD_ID 
	 

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.6',GETDATE(), N' Cập nhật bao cao 09')
END