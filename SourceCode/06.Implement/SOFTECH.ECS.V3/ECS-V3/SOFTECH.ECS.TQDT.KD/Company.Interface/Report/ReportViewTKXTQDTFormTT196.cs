﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System.Diagnostics;

namespace Company.Interface.Report
{

    public partial class ReportViewTKXTQDTFormTT196 : BaseForm
    {
        public Company.Interface.Report.TQDTToKhaiXK_TT196 ToKhaiChinhReport = new Company.Interface.Report.TQDTToKhaiXK_TT196();
        public Company.Interface.Report.TQDTPhuLucToKhaiXuat_TT196 PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiXuat_TT196();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public XRControl Cell = new XRControl();
        int index = 0;
        private int soDongHang = 7; //Mac dinh = 9 dong hang tren phu luc to khai.



        public ReportViewTKXTQDTFormTT196()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            //DATLMQ bổ sung thiết lập margin: 25/02/2011
//             this.ToKhaiChinhReport.Margins.Top = 0;
//             this.ToKhaiChinhReport.Margins.Bottom = 0;
//             this.PhuLucReport.Margins.Top = 0;
//             this.PhuLucReport.Margins.Bottom = 0;
            int soContainer;
            if (this.TKMD.VanTaiDon != null)
                soContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
            else
                soContainer = 0;
            if (this.TKMD.HMDCollection.Count > 3 || soContainer > 4)
            {
                int countHang = (this.TKMD.HMDCollection.Count - 1) / 7 + 1;
                int countContainer = (soContainer - 1) / 4;
                int count;
                if (GlobalSettings.IsKhongDungBangKeCont)
                    count = countHang >= countContainer ? countHang : countContainer;
                else
                {
                    if (this.TKMD.HMDCollection.Count > 3)
                        count = countHang;
                    else
                        count = 0;
                }
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            /*if (IsHaveTax()) cboToKhai.Items.Add("Phụ lục hàng xuất có thuế", cboToKhai.Items.Count);*/
            cboToKhai.SelectedIndex = 0;


        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //                 if (char.IsDigit(txtSoDongHang.Text, 0))
                //                 {
                //                     soDongHang = Convert.ToInt32(txtSoDongHang.Text);
                //                 }
                //                 else
                //                 {
                //                     Globals.ShowMessage("Số dòng hàng không phải là kiểu số.", false);
                //                     txtSoDongHang.Focus();
                //                     return;
                //                 }

                if (cboToKhai.SelectedIndex == 0)
                {

                    this.ToKhaiChinhReport.TKMD = this.TKMD;
                    //this.ToKhaiChinhReport.report = this;
                    this.ToKhaiChinhReport.inMaHang = chkInMaHang.Checked;
                    this.ToKhaiChinhReport.inTriGiaTT = chkInTriGiaTinhThue.Checked;
                    this.ToKhaiChinhReport.BindReport();
                    printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                    this.ToKhaiChinhReport.CreateDocument();

                }
                else
                {

                    try
                    {
                        index = cboToKhai.SelectedIndex;
                    }
                    catch { index = 1; }
                    txtTenNhomHang.Text = "";
                    txtDeXuatKhac.Text = "";
                    List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                    //int begin = (cboToKhai.SelectedIndex - 1) * 9; //Comment by Hungtq
                    //int end = cboToKhai.SelectedIndex * 9;  //Comment by Hungtq
                    int begin = (cboToKhai.SelectedIndex - 1) * 7;
                    int end = cboToKhai.SelectedIndex * 7;
                    //edit by Khanhhn
                    if (begin <= this.TKMD.HMDCollection.Count - 1 && this.TKMD.HMDCollection.Count > 3)
                    {
                        if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                        for (int i = begin; i < end; i++)
                            HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                    }
                    List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
                    int beginContainer = (cboToKhai.SelectedIndex) * 4;
                    int endContainer = (cboToKhai.SelectedIndex + 1) * 4;
                    if (this.TKMD.VanTaiDon != null && this.TKMD.VanTaiDon.ContainerCollection.Count > 0)
                    {
                        if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count - 1)
                        {
                            if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
                                endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
                            for (int j = beginContainer; j < endContainer; j++)
                                ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
                        }
                    }
                    this.PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiXuat_TT196();
                    //this.PhuLucReport.report = this;
                    this.PhuLucReport.TKMD = this.TKMD;
                    if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                        this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                    this.PhuLucReport.ContainerCo = ContainerReportCo;
                    this.PhuLucReport.HMDCollection = HMDReportCollection;
                    this.PhuLucReport.soDongHang = soDongHang;
                    this.PhuLucReport.inMaHang = chkInMaHang.Checked;
                    this.PhuLucReport.inTriGiaTT = chkInTriGiaTinhThue.Checked;
                    this.PhuLucReport.BindReport(index.ToString());
                    printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                    this.PhuLucReport.CreateDocument();

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {

                //this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                //this.ToKhaiChinhReport.CreateDocument();
            }
            else

            {
                // this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                // this.PhuLucReport.CreateDocument();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
        }


        private void uiButton3_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                //datlmq update 29072010
                //if (txtTenNhomHang.Text != "")
                //    this.ToKhaiChinhReport.setThongTin(this.Cell, txtTenNhomHang.Text);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                //this.PhuLucReport.setThongTin(this.Cell, txtTenNhomHang.Text);
                //this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                //this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                //this.PhuLucReport.setVisibleImage(true);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                if (chkInMaHang.Checked)
                    this.ToKhaiChinhReport.inMaHang = true;
                else
                    this.ToKhaiChinhReport.inMaHang = false;

                this.ToKhaiChinhReport.BindReport();
                this.ToKhaiChinhReport.inTriGiaTT = chkInTriGiaTinhThue.Checked;
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                //                 if (chkInMaHang.Checked)
                //                     this.PhuLucReport.inMaHang = true;
                //                 else
                //                     this.PhuLucReport.inMaHang = false;

                try
                {
                    index = cboToKhai.SelectedIndex;
                }
                catch { index = 1; }
                txtTenNhomHang.Text = "";
                txtDeXuatKhac.Text = "";
                List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                //int begin = (cboToKhai.SelectedIndex - 1) * 9; //Comment by Hungtq
                //int end = cboToKhai.SelectedIndex * 9;  //Comment by Hungtq
                int begin = (cboToKhai.SelectedIndex - 1) * 7;
                int end = cboToKhai.SelectedIndex * 7;
                //edit by Khanhhn
                if (begin <= this.TKMD.HMDCollection.Count - 1)
                {
                    if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                    for (int i = begin; i < end; i++)
                        HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                }
                List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
                int beginContainer = (cboToKhai.SelectedIndex - 1) * 4;
                int endContainer = cboToKhai.SelectedIndex * 4;
                if (this.TKMD.VanTaiDon != null && this.TKMD.VanTaiDon.ContainerCollection.Count > 0)
                {
                    if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count - 1)
                    {
                        if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
                            endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
                        for (int j = beginContainer; j < endContainer; j++)
                            ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
                    }
                }
                this.PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiXuat_TT196();
                //this.PhuLucReport.report = this;
                this.PhuLucReport.TKMD = this.TKMD;
                if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.ContainerCo = ContainerReportCo;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.soDongHang = soDongHang;
                this.PhuLucReport.inMaHang = chkInMaHang.Checked;
                this.PhuLucReport.inTriGiaTT = chkInTriGiaTinhThue.Checked;
                this.PhuLucReport.BindReport(index.ToString());
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkHinhNen_CheckedChanged(object sender, EventArgs e)
        {

        }



        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                //this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.PhuLucReport.BindReport(index.ToString());
                this.PhuLucReport.CreateDocument();

            }
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        private void chkMienThue1_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                // this.ToKhaiChinhReport.MienThue1 = chkMienThue1.Checked;
                // this.ToKhaiChinhReport.MienThue2 = chkMienThue2.Checked;
                this.ToKhaiChinhReport.BindReport();
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                // this.PhuLucReport.MienThue1 = chkMienThue1.Checked;
                // this.PhuLucReport.MienThue2 = chkMienThue2.Checked;
                this.PhuLucReport.BindReport(cboToKhai.Items.Count.ToString());
                this.PhuLucReport.CreateDocument();

            }
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {

                this.ToKhaiChinhReport.CreateDocument();
                this.ToKhaiChinhReport.Margins.Top = 0;
                this.ToKhaiChinhReport.Margins.Bottom = 0;

                // BEGIN: SAVE FILE TO PDF.
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.InitialDirectory = "C:\\";
                dlg.RestoreDirectory = true;
                dlg.Filter = "pdf files (*.pdf)|*.pdf";

                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    if (string.IsNullOrEmpty(dlg.FileName) == false)
                    {
                        string filePath = dlg.FileName;
                        this.ToKhaiChinhReport.PrintingSystem.ExportToPdf(filePath);
                        try
                        {
                            // Open file pdf.
                            Process.Start(filePath);
                        }
                        catch { }
                    }
                }
                // END: SAVE FILE TO PDF.
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.CreateDocument();
                this.PhuLucReport.Margins.Top = 0;
                this.PhuLucReport.Margins.Bottom = 0;

                // BEGIN: SAVE FILE TO PDF.
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.InitialDirectory = "C:\\";
                dlg.RestoreDirectory = true;
                dlg.Filter = "pdf files (*.pdf)|*.pdf";

                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    if (string.IsNullOrEmpty(dlg.FileName) == false)
                    {
                        string filePath = dlg.FileName;
                        this.PhuLucReport.PrintingSystem.ExportToPdf(filePath);
                        try
                        {
                            // Open file pdf.
                            Process.Start(filePath);
                        }
                        catch { }
                    }
                }
                // END: SAVE FILE TO PDF.
                this.PhuLucReport.CreateDocument();
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            //if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
            //{
            //    Globals.ExportExcel(Globals.ToKhaiType.Xuat, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThue1.Checked, chkMienThue2.Checked, "");
            //}
            //             if (this.cboToKhai.SelectedIndex == 0)
            //             {
            //                 Globals.ExportExcel(Globals.ToKhaiType.Xuat, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThue1.Checked, chkMienThue2.Checked, "");
            //             }
            //             else
            //             {
            //                 Globals.ExportExcel(Globals.ToKhaiType.PhuLucTKX, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThue1.Checked, chkMienThue2.Checked, cboToKhai.SelectedIndex.ToString());
            //             }
            string thongBao = "Lưu ý: \r\n";
            thongBao += "          Doanh nghiệp chịu hoàn toàn trách nhiệm trước pháp luật khi thay đổi nội dung tờ khai trên file Excel.";
            if (ShowMessage(thongBao, true) == "Yes")
            {
                Company.KDT.SHARE.Components.Globals.SaveMessage("Xuat Excel", TKMD.ID, "Xuất tờ khai ra Excel", thongBao);
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Excel 2003 |*.xls";
                save.CheckPathExists = true;
                save.ShowDialog(this);
                XlsExportOptions optionToKhai = new XlsExportOptions();
                if (cboToKhai.SelectedIndex == 0)
                {

//                     this.ToKhaiChinhReport.TKMD = this.TKMD;
//                     this.ToKhaiChinhReport.report = this;
//                     this.ToKhaiChinhReport.BindReport();
//                     printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
//                     this.ToKhaiChinhReport.CreateDocument();
                    optionToKhai.SheetName = "To khai chinh";
                    optionToKhai.TextExportMode = TextExportMode.Text;
                    this.ToKhaiChinhReport.ExportToXls(save.FileName, optionToKhai);

                }
                else
                {

//                     try
//                     {
//                         index = cboToKhai.SelectedIndex;
//                     }
//                     catch { index = 1; }
//                     txtTenNhomHang.Text = "";
//                     txtDeXuatKhac.Text = "";
//                     List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
//                     //int begin = (cboToKhai.SelectedIndex - 1) * 9; //Comment by Hungtq
//                     //int end = cboToKhai.SelectedIndex * 9;  //Comment by Hungtq
//                     int begin = (cboToKhai.SelectedIndex - 1) * 7;
//                     int end = cboToKhai.SelectedIndex * 7;
//                     //edit by Khanhhn
//                     if (begin <= this.TKMD.HMDCollection.Count - 1)
//                     {
//                         if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
//                         for (int i = begin; i < end; i++)
//                             HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
//                     }
//                     List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
//                     int beginContainer = (cboToKhai.SelectedIndex - 1) * 4;
//                     int endContainer = cboToKhai.SelectedIndex * 4;
// 
//                     if (this.TKMD.VanTaiDon != null && this.TKMD.VanTaiDon.ContainerCollection.Count == 0)
//                     {
//                         if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count - 1)
//                         {
//                             if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
//                                 endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
//                             for (int j = beginContainer; j < endContainer; j++)
//                                 ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
//                         }
//                     }
// 
//                     this.PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiXuat_TT196();
//                     this.PhuLucReport.report = this;
//                     this.PhuLucReport.TKMD = this.TKMD;
//                     if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
//                         this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
//                     this.PhuLucReport.ContainerCo = ContainerReportCo;
//                     this.PhuLucReport.HMDCollection = HMDReportCollection;
//                     this.PhuLucReport.soDongHang = soDongHang;
//                     this.PhuLucReport.BindReport(index.ToString());
//                     printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
//                     this.PhuLucReport.CreateDocument();
                    optionToKhai.SheetName = "Phu luc " + index.ToString();
                    optionToKhai.TextExportMode = TextExportMode.Text;
                    PhuLucReport.ExportToXls(save.FileName, optionToKhai);
                }
                Process.Start(save.FileName);
            }
        }


    }
}