using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data.SqlClient;
using Logger;

namespace Company.Interface.Report.SXXK
{
    public partial class BKToKhaiNhapXinHoan_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        public bool bk_Hoan_KhongThu = true;
        public string WhereCondition = string.Empty;
        public int LanThanhLy;
        private int STT = 1;
        //private int STTBanLuu = 0;
        private decimal TongTienThueTKTiep = 0;
        //private decimal TongTienThueHoan = 0;
        //private decimal TongLuongSuDung = 0;
        private decimal TongTienThueHoanTatCa = 0;
        //private decimal TongTienThueHoanCuaToKhai = 0;
        //private decimal TongThueNKPhaiNopCuaToKhai = 0;
        //private decimal TongTienThueTKTiepCuaToKhai = 0;
        //private decimal TongThueTKKoThu = 0;
        //private decimal TongTienTKTiep = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        public int NamThanhLy;
        public string SoTK = "SoTK";
        public string ToKhai = "1";
        public string TenNPL = "1";
        public string MaDoanhNghiep;
        public decimal tien = 0;
        public decimal tienKT = 0;
        static double num = 0.0;

        public BKToKhaiNhapXinHoan_TT38()
        {
            InitializeComponent();
            //lblThueTheoDoiTiep.AfterPrint += new EventHandler(lblTongTienThueTKTiep_AfterPrint);
            lblTongThueKhongThu.BeforePrint += new System.Drawing.Printing.PrintEventHandler(lblTongThueKhongThu_BeforePrint);
            lblTongThueLanSauTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(lblTongThueLanSauTK_BeforePrint);
            
        }

        void lblTongThueLanSauTK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongThueLanSauTK.Text = TongTienThueTKTiep.ToString("N0");
        }

        void lblTongThueKhongThu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongThueKhongThu.Text = TongTienThueHoanTatCa.ToString("N0");
        }

        void lblTongTienThueTKTiep_AfterPrint(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(((DevExpress.XtraReports.UI.XRTableCell)sender).Text))
                TongTienThueTKTiep = TongTienThueTKTiep + System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
        }

        public void BindReport(string where)
        {
            if (bk_Hoan_KhongThu == false)
            {
                lbTieuDe.Text = "BẢNG KÊ TỜ KHAI NHẬP KHẨU KHÔNG THU THUẾ";
                lbTienThue.Text = "Tổng số thuế NPL nhập khẩu không thu(VND):";
                lbTableThue.Text = "Số tiền thuế không thu";
            }
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);
            DataTable dataSource = new DataTable();
            dataSource = GetDataSource();

            int i = 0;
            DateTime ngayDangKyNhap = DateTime.Today;
            string soToKhaiNhap = "";
            foreach (DataRow dr in dataSource.Rows)
            {
                
                if (ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == dr["SoToKhaiNhap"].ToString())
                {
                    dr["STT"] = i;
                }
                else
                {
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = dr["SoToKhaiNhap"].ToString();
                    i++;
                    dr["STT"] = i;
                }
            }

            DetailReport.DataSource = dataSource;
            this.GroupHeader1.Visible = false;
            //if (GlobalSettings.MA_DON_VI.Trim() == "0400101556")
            //{
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                //new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
              //new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
              
                //new DevExpress.XtraReports.UI.GroupField("SoHopDongNhap")
            });
            //}
            //else 
            //{
            //    this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            //    new DevExpress.XtraReports.UI.GroupField("STT"),
            //    new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
            //    new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
            //    //new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
            //  // new DevExpress.XtraReports.UI.GroupField("DonGiaTT")});
              
            //    new DevExpress.XtraReports.UI.GroupField("SoHopDongNhap")
            //});
            //}
            

            

            lblSTT_G_F.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblSoToKhaiNhap_D.DataBindings.Add("Text", DetailReport.DataSource, "SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", DetailReport.DataSource, "NgayDangKyNhap");
            lblNgayDangKyNhap_G_F.DataBindings.Add("Text", DetailReport.DataSource, "NgayDangKyNhap");
            lblTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
            lblTenDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
            lblLuongNhapTK.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhapTK", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            //lblLuongTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhapTonDauKy", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblLuongNPLSuDung.DataBindings.Add("Text", DetailReport.DataSource, "LuongNPLSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            //lblLuongTonCuoi.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            
            //lblTienThueDauKy.DataBindings.Add("Text", DetailReport.DataSource, "ThueTonDauKy",Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblThueKhongThu.DataBindings.Add("Text", DetailReport.DataSource, "ThueKhongThu", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            //lblThueTheoDoiTiep.DataBindings.Add("Text", DetailReport.DataSource, "ThueTheoDoiTiep", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            //lblSo_CTTT_N.DataBindings.Add("Text", DetailReport.DataSource, "SoCTTT_N");
            //lblTriGiaHang_G_F.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaHangThucXuat", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            //lblPhanBo.DataBindings.Add("Text", DetailReport.DataSource, "GhiChu");

            if (GlobalSettings.MA_DON_VI.Trim() == "0400101556")
                lblGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDongNhap");
            else
                lblGhiChu_G_F.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDongNhap");

            

            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            if (SoHoSo > 0)
            {
                // khanhhn 20/02/2012 - Xóa Từ Xin Hoàn (Theo yêu cầu của Grozbecker)
                lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                lblSHSTK.Width = 200;
            }
        
            //HQ
            //xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            xrLabel5.Text = GlobalSettings.TieudeNgay;
        }
        private DataTable GetDataSource()
        {
            try
            {

                string spName = "[p_SXXK_BKToKhaiNhapXinHoan_TT38]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@LanTL", SqlDbType.BigInt, LanThanhLy);
                db.AddInParameter(dbCommand, "@NamTL", SqlDbType.Int, NamThanhLy);
                db.AddInParameter(dbCommand, "@MaDN", SqlDbType.VarChar, MaDoanhNghiep);
                db.AddInParameter(dbCommand, "@Distinct1", SqlDbType.Int, 0);
                db.AddInParameter(dbCommand, "@ListTKN", SqlDbType.NVarChar, WhereCondition);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void lblTongTienThueHoan_AfterPrint(object sender, EventArgs e)
        {
            
            if (TenNPL != "" && GlobalSettings.MA_DON_VI.Trim() == "0400101556")
            {
                
                if (!string.IsNullOrEmpty(((DevExpress.XtraReports.UI.XRTableCell)sender).Text))
                    TongTienThueHoanTatCa = TongTienThueHoanTatCa + System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
            }else
                if (!string.IsNullOrEmpty(((DevExpress.XtraReports.UI.XRTableCell)sender).Text))
                    TongTienThueHoanTatCa = TongTienThueHoanTatCa + System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
            try
            {
                
                if (TenNPL != "")
                    tien += System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
                else
                    lblThueKhongThu.Text = "";
                
            }
            catch (Exception)
            {

                //throw;
            }

           
                
        }

        private void lblSoToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (test == "")
            //{
            //    test = lblSoToKhaiNhap.Text;
            //}
            //else if (test != lblSoToKhaiNhap.Text)
            //{
            //    test = lblSoToKhaiNhap.Text;
            //}
            //else
            //    lblSoToKhaiNhap.Text = "";

        }

        private void lblSoToKhai_Group_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string stt = lblSTT_Group.Text;
            //string sotokhai = lblSoToKhai_Group.Text;
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
           
        }

        private void lblSTT_Group_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
            //var x = GetCurrentColumnValue("STT");
            //lblSTT_Group.Text = GetCurrentColumnValue("STT").ToString();
        }

        private void lblSTT_Group_AfterPrint(object sender, EventArgs e)
        {
            //var x = GetCurrentColumnValue("STT");
            //lblSTT_Group.Text = GetCurrentColumnValue("STT").ToString();
        }

        private void lblSoToKhaiNhap_Group_AfterPrint(object sender, EventArgs e)
        {
            SoTK = lblSoToKhaiNhap_Group.Text;
            STT += 1;
        }

        private void lblNgayDangKyNhap_Group_AfterPrint(object sender, EventArgs e)
        {
            STT += 1;
        }

        private void lblSoHopDong_Group_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string[] split = lblSoHopDong_Group.Text.Split('.',',');
            if (split.Length > 0) 
            {
                lblSoHopDong_Group.Text = split[0];
            }
            
        }

        private void lblTenNPL_AfterPrint(object sender, EventArgs e)
        {
            if(TenNPL == "")
            lblTenNPL.Text = "";
        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == lblTenNPL.Text && ToKhai == lblSoToKhaiNhap_D.Text)
            {
                TenNPL = lblTenNPL.Text = "";
            }
            else 
            {
                TenNPL = lblTenNPL.Text;
                ToKhai = lblSoToKhaiNhap_D.Text;
            }
                
        }

        private void lblSoToKhaiNhap_Group_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //SoTK = lbl
        }

        private void lblSoToKhaiNhapG_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if(STT!=0)
                lblSoToKhaiNhapG_F.Text = "TK: "+lblSoToKhaiNhap_D.Text;
            
        }

        private void lblTongThueHoanKhongThu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if(TenNPL=="")
            lblThueKhongThu.Text = "";
            
            lblTongThueHoanKhongThu.Text = tien.ToString("N");
            tien = 0;
        }

        private void lblGhiChu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblGhiChu.Text = "";
            else 
            {
                string[] split = lblGhiChu.Text.Split('.', ',');
                if (split.Length > 0)
                {
                    lblGhiChu.Text = split[0];
                }
            }
            
        }

        private void lblTongThueTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblTongThueTiep.Text = tienKT.ToString("N");

            tienKT = 0;
        }

        private void lblTenDVT_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL == "")
                lblTenDVT.Text = "";
        }

        private void lblLuongNhapTK_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL == "")
                lblLuongNhapTK.Text = "";
        }

        private void lblLuongTonDauKy_AfterPrint(object sender, EventArgs e)
        {
            //if (TenNPL == "")
                //lblLuongTonDauKy.Text = "";
        }

        private void lblLuongNPLSuDung_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL == "")
                lblLuongNPLSuDung.Text = "";
        }

        private void lblLuongTonCuoi_AfterPrint(object sender, EventArgs e)
        {
            //if (TenNPL == "")
                //lblLuongTonCuoi.Text = "";
        }

        private void lblTenDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblTenDVT.Text = "";
        }

        private void lblLuongNhapTK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblLuongNhapTK.Text = "";
        }

        private void lblLuongTonDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (TenNPL == "")
                //lblLuongTonDauKy.Text = "";
        }

        private void lblLuongNPLSuDung_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (TenNPL == "")
                lblLuongNPLSuDung.Text = "";
        }

        private void lblLuongTonCuoi_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (TenNPL == "")
                //lblLuongTonCuoi.Text = "";
        }

        private void lblTienThueDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (TenNPL == "")
                //lblTienThueDauKy.Text = "";
        }

        private void lblThueTheoDoiTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (TenNPL == "")
                //lblThueTheoDoiTiep.Text = "";
        }

        private void lblThueTheoDoiTiep_AfterPrint(object sender, EventArgs e)
        {
            if (TenNPL != "")
                tienKT += System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
            //else
                //lblThueTheoDoiTiep.Text = "";
        }

        private void lblSoToKhaiNhap_D_AfterPrint(object sender, EventArgs e)
        {
            //if()
            //SoTK = lblSoToKhaiNhap_Group.Text;
            //STT += 1;
        }

        private void lblSTT_G_F_AfterPrint(object sender, EventArgs e)
        {
            SoTK = lblSoToKhaiNhap_D.Text;
            STT += 1;
        }

        private static string Chu(string gNumber)
        {
            string result = "";
            switch (gNumber)
            {
                case "0":
                    result = "không";
                    break;
                case "1":
                    result = "một";
                    break;
                case "2":
                    result = "hai";
                    break;
                case "3":
                    result = "ba";
                    break;
                case "4":
                    result = "bốn";
                    break;
                case "5":
                    result = "năm";
                    break;
                case "6":
                    result = "sáu";
                    break;
                case "7":
                    result = "bảy";
                    break;
                case "8":
                    result = "tám";
                    break;
                case "9":
                    result = "chín";
                    break;
            }
            return result;
        }
        private static string Donvi(string so)
        {
            string Kdonvi = "";

            if (so.Equals("1"))
                Kdonvi = "";
            if (so.Equals("2"))
                Kdonvi = "nghìn";
            if (so.Equals("3"))
                Kdonvi = "triệu";
            if (so.Equals("4"))
                Kdonvi = "tỷ";
            if (so.Equals("5"))
                Kdonvi = "nghìn tỷ";
            if (so.Equals("6"))
                Kdonvi = "triệu tỷ";
            if (so.Equals("7"))
                Kdonvi = "tỷ tỷ";

            return Kdonvi;
        }
        private static string Tach(string tach3)
        {
            string Ktach = "";
            if (tach3.Equals("000"))
                return "";
            if (tach3.Length == 3)
            {
                string tr = tach3.Trim().Substring(0, 1).ToString().Trim();
                string ch = tach3.Trim().Substring(1, 1).ToString().Trim();
                string dv = tach3.Trim().Substring(2, 1).ToString().Trim();
                if (tr.Equals("0") && ch.Equals("0"))
                    Ktach = " không trăm lẻ " + Chu(dv.ToString().Trim()) + " ";
                if (!tr.Equals("0") && ch.Equals("0") && dv.Equals("0"))
                    Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm ";
                if (!tr.Equals("0") && ch.Equals("0") && !dv.Equals("0"))
                    Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm lẻ " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
                if (tr.Equals("0") && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm mười " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("0"))
                    Ktach = " không trăm mười ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("5"))
                    Ktach = " không trăm mười lăm ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười " + Chu(dv.Trim()).Trim() + " ";

                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("0"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười lăm ";

            }


            return Ktach;

        }
        public static string So_chu(double gNum)
        {
            if (gNum == 0)
                return "Không đồng";

            string lso_chu = "";
            string tach_mod = "";
            string tach_conlai = "";
            double Num = Math.Round(gNum, 0);
            string gN = Convert.ToString(Num);
            int m = Convert.ToInt32(gN.Length / 3);
            int mod = gN.Length - m * 3;
            string dau = "[+]";

            // Dau [+ , - ]
            if (gNum < 0)
                dau = "[-]";
            dau = "";

            // Tach hang lon nhat
            if (mod.Equals(1))
                tach_mod = "00" + Convert.ToString(Num.ToString().Trim().Substring(0, 1)).Trim();
            if (mod.Equals(2))
                tach_mod = "0" + Convert.ToString(Num.ToString().Trim().Substring(0, 2)).Trim();
            if (mod.Equals(0))
                tach_mod = "000";
            // Tach hang con lai sau mod :
            if (Num.ToString().Length > 2)
                tach_conlai = Convert.ToString(Num.ToString().Trim().Substring(mod, Num.ToString().Length - mod)).Trim();

            ///don vi hang mod
            int im = m + 1;
            if (mod > 0)
                lso_chu = Tach(tach_mod).ToString().Trim() + " " + Donvi(im.ToString().Trim());
            /// Tach 3 trong tach_conlai

            int i = m;
            int _m = m;
            int j = 1;
            string tach3 = "";
            string tach3_ = "";

            while (i > 0)
            {
                tach3 = tach_conlai.Trim().Substring(0, 3).Trim();
                tach3_ = tach3;
                lso_chu = lso_chu.Trim() + " " + Tach(tach3.Trim()).Trim();
                m = _m + 1 - j;
                if (!tach3_.Equals("000"))
                    lso_chu = lso_chu.Trim() + " " + Donvi(m.ToString().Trim()).Trim();
                tach_conlai = tach_conlai.Trim().Substring(3, tach_conlai.Trim().Length - 3);

                i = i - 1;
                j = j + 1;
            }
            if (lso_chu.Trim().Substring(0, 1).Equals("k"))
                lso_chu = lso_chu.Trim().Substring(10, lso_chu.Trim().Length - 10).Trim();
            if (lso_chu.Trim().Substring(0, 1).Equals("l"))
                lso_chu = lso_chu.Trim().Substring(2, lso_chu.Trim().Length - 2).Trim();
            if (lso_chu.Trim().Length > 0)
                lso_chu = dau.Trim() + " " + lso_chu.Trim().Substring(0, 1).Trim().ToUpper() + lso_chu.Trim().Substring(1, lso_chu.Trim().Length - 1).Trim() + " đồng chẵn.";

            return lso_chu.ToString().Trim();

        }

        private void lblTongThueKhongThu_AfterPrint(object sender, EventArgs e)
        {
            //num = Convert.ToDouble(lblTongThueHoanKhongThu.Text);
            //lbTien_Chu.Text = "(Bằng chữ: " + So_chu(num) + " )";
        }

        private void lbTien_Chu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //decimal t = tien = tienKT;
            
            double num = Convert.ToDouble(TongTienThueHoanTatCa);
            lbTien_Chu.Text = "(Bằng chữ: " + So_chu(num) + " )";
        }

       
       
    }
}
