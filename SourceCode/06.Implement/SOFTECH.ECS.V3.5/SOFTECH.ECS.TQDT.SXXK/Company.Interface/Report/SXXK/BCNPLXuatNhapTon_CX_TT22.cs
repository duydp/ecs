using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Logger;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon_CX_TT22 : DevExpress.XtraReports.UI.XtraReport
    {
        public int BangKeHSTL_ID;
        public int LanThanhLy;
        private int STT = 0;
        private int STTBanLuu = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        private int SoToKhaiNhap = 0;
        private int SoToKhaiXuat = 0;
        private decimal temp = 0;

        private string MaNPLTemp = "";
        private string MaNPLBanLuu = "";

        private string MaSPTemp = "";
        private string MaSPBanLuu = "";

        private decimal _TongLuongNhap = 0;
        private decimal _TongLuongTonDau = 0;
        private decimal _TongLuongNPLSuDung = 0;
        private decimal _TongLuongTonCuoi = 0;
        private decimal _TongLuongNPLTaiXuat = 0;
        private float fontReport = 6;
        private string DATETIME_FORMAT = "dd/MM/yyyy";

        public int NamThanhLy;
        public string MaDoanhNghiep;
        DataTable dataSource = new DataTable();

        public BCNPLXuatNhapTon_CX_TT22()
        {
            InitializeComponent();

            fontReport = GlobalSettings.FontBCXNT;
        }
        public void BindingReport()
        {
            try
            {
                DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);

                lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
                {
                    lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
                }
                lblSHSTK.Width = 300;

                Company.BLL.KDT.SXXK.BCXuatNhapTon bcxnt = new Company.BLL.KDT.SXXK.BCXuatNhapTon();
                dataSource = bcxnt.GetBC07_KCX_XNTByLanThanhLy(MaDoanhNghiep, LanThanhLy, GlobalSettings.MA_HAI_QUAN,BangKeHSTL_ID);

                DetailReport.DataSource = dataSource;
               // lblSoTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                lblMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MaNPL");
                lblTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
                lblDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
                lblLuongTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                //lblLuongTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblLuongNhap.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhap", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                //lblLuongNPLSuDung.DataBindings.Add("Text", DetailReport.DataSource, "LuongSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblLuongNPLSuDung.DataBindings.Add("Text", DetailReport.DataSource, "LuongNPLSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblLuongNPLTaiXuat.DataBindings.Add("Text", DetailReport.DataSource, "LuongTaiXuatVaTieuHuy", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblLuongTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
           
                //lblGiaiTrinh.DataBindings.Add("Text", DetailReport.DataSource, "XuLy");
                //HQ
                lblNgayThangHQ.Text = Properties.Settings.Default.TieudeNgay;
                //DN
                lblNgayThangDN.Text = GlobalSettings.TieudeNgay;

            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

        private void lblMaHS_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //lblMaSP.Text = GetCurrentColumnValue("TenSP").ToString();
            //if (GetCurrentColumnValue("MaSP").ToString().Trim().Length > 0)
            //    lblMaSP.Text = lblMaSP.Text + " / " + GetCurrentColumnValue("MaSP").ToString();
            NguyenPhuLieu npl = new NguyenPhuLieu();
            npl.Ma= lblMaNPL.Text;
            npl.MaDoanhNghiep = MaDoanhNghiep;
            npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            if (npl.Load())
            {
                lblMaHS.Text = npl.MaHS;
                
            }
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
                if (!String.IsNullOrEmpty(VNACCS_Mapper.GetCodeVNACC(lblDVT.Text.Trim())))
                    lblDVT.Text = VNACCS_Mapper.GetCodeVNACC(lblDVT.Text.Trim());
                    
        }
        int stt = 0;
        private void lblSoTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            stt++;
            lblSoTT.Text = "" + stt;
        }
      
    }
}

     