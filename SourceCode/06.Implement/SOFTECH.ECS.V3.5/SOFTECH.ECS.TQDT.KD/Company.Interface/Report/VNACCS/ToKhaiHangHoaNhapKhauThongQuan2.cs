using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using BarcodeLib;

namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiHangHoaNhapKhauThongQuan2 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiHangHoaNhapKhauThongQuan2()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 30,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindingReport(VAD1AG0 vad1ag,string maNV)
        {
            lblTenThongTinXuat.Text = EnumThongBao.GetTenNV(maNV);
            lblSoTrang.Text = "2/" + vad1ag.B12.GetValue().ToString().ToUpper();
            lblSoToKhai.Text = vad1ag.ICN.GetValue().ToString().ToUpper();
            pictureBox1.Image = GenerateBarCode(vad1ag.ICN.GetValue().ToString());

            lblSoToKhaiDauTien.Text = vad1ag.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad1ag.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad1ag.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad1ag.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad1ag.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad1ag.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad1ag.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad1ag.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad1ag.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad1ag.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad1ag.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad1ag.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ag.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad1ag.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";
            }
            if (vad1ag.AD1.GetValue().ToString().ToUpper() != "" && vad1ag.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad1ag.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ag.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad1ag.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad1ag.AD3.GetValue().ToString().ToUpper() != "" && vad1ag.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad1ag.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ag.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad1ag.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }
            lblBieuThiTHHetHan.Text = vad1ag.AAA.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ag.EA_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanLoaiDinhKemKhaiBaoDienTu1.Text = vad1ag.EA_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoDinhKemKhaiBaoDienTu1.Text = vad1ag.EA_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblPhanLoaiDinhKemKhaiBaoDienTu2.Text = vad1ag.EA_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoDinhKemKhaiBaoDienTu2.Text = vad1ag.EA_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblPhanLoaiDinhKemKhaiBaoDienTu3.Text = vad1ag.EA_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoDinhKemKhaiBaoDienTu3.Text = vad1ag.EA_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblPhanGhiChu.Text = vad1ag.NT2.GetValue().ToString().ToUpper();
            lblSoQuanLyNoiBoDN.Text = vad1ag.REF.GetValue().ToString().ToUpper();
            lblSoQuanLyNSD.Text = vad1ag.B16.GetValue().ToString().ToUpper();
            lblPhanLoaiChiThiHQ.Text = vad1ag.CCM.GetValue().ToString().ToUpper();

            lblTenTruongDvHQ.Text = vad1ag.C20.GetValue().ToString();
            if (Convert.ToDateTime(vad1ag.C21.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayCapPhep.Text = Convert.ToDateTime(vad1ag.C21.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayCapPhep.Text = "";
            if (vad1ag.AD4.GetValue().ToString().ToUpper() != "" && vad1ag.AD4.GetValue().ToString().ToUpper() != "0")
                lblGioCapPhep.Text = vad1ag.AD4.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.AD4.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.AD4.GetValue().ToString().ToUpper().Substring(4, 2);
            else
                lblGioCapPhep.Text = "";
            if (Convert.ToDateTime(vad1ag.C22.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayHoanThanhKT.Text = Convert.ToDateTime(vad1ag.C22.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayHoanThanhKT.Text = "";
            if (vad1ag.AD5.GetValue().ToString().ToUpper() != "" && vad1ag.AD5.GetValue().ToString().ToUpper() != "0")
                lblGioHoanThanhKT.Text = vad1ag.AD5.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.AD5.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.AD5.GetValue().ToString().ToUpper().Substring(4, 2);
            lblPhanLoaiThamTra.Text = vad1ag.C23.GetValue().ToString();
            if (Convert.ToDateTime(vad1ag.C24.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayPheDuyet.Text = Convert.ToDateTime(vad1ag.C24.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayPheDuyet.Text = "";
            if (vad1ag.AD6.GetValue().ToString().ToUpper() != "" && vad1ag.AD6.GetValue().ToString().ToUpper() != "0")
                lblGioPheDuyet.Text = vad1ag.AD6.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.AD6.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.AD6.GetValue().ToString().ToUpper().Substring(4, 2);
            else
                lblGioPheDuyet.Text = "";
            if (Convert.ToDateTime(vad1ag.C25.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayHoanThanhKiemTraBP.Text = Convert.ToDateTime(vad1ag.C25.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayHoanThanhKiemTraBP.Text = "";
            if (vad1ag.AD7.GetValue().ToString().ToUpper() != "" && vad1ag.AD7.GetValue().ToString().ToUpper() != "0")
                lblGioHoanThanhKiemTraBP.Text = vad1ag.AD7.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.AD7.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.AD7.GetValue().ToString().ToUpper().Substring(4, 2);
            else
                lblGioHoanThanhKiemTraBP.Text = "";
            lblSoNgayMongDoi.Text = vad1ag.C26.GetValue().ToString();

            lblMaSacThueAnHanVAT.Text = vad1ag.ADX.GetValue().ToString();
            lblTenSacThueAnHanVAT.Text = vad1ag.ADY.GetValue().ToString();
            if (Convert.ToDateTime(vad1ag.ADZ.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblHanNopThueVAT.Text = Convert.ToDateTime(vad1ag.ADZ.GetValue()).ToString("dd/MM/yyyy");
            else
                lblHanNopThueVAT.Text = "";



            //if (Convert.ToDateTime(vad1ag.ADY.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            //{
            //    lblNgayKhaiBaoNopThue.Text = Convert.ToDateTime(vad1ag.ADY.GetValue()).ToString("dd/MM/yyyy");
            //}
            //else
            //{
            //    lblNgayKhaiBaoNopThue.Text = "";
            //}
            //if (vad1ag.ADZ.GetValue().ToString().ToUpper() != "" && vad1ag.ADZ.GetValue().ToString().ToUpper() != "0")
            //{
            //    lblGioKhaiBaoNT.Text = vad1ag.ADZ.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.ADZ.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.ADZ.GetValue().ToString().ToUpper().Substring(4, 2);
            //}
            //else
            //{
            //    lblGioKhaiBaoNT.Text = "";
            //}
            lblTieuDe.Text = vad1ag.B23.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ag.KN1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaSacThueAnHan1.Text = vad1ag.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan1.Text = vad1ag.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue1.Text = Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue1.Text = "";
                        }
                        break;
                    case 1:
                        lblMaSacThueAnHan2.Text = vad1ag.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan2.Text = vad1ag.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue2.Text = Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue2.Text = "";
                        }
                        break;
                    case 2:
                        lblMaSacThueAnHan3.Text = vad1ag.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan3.Text = vad1ag.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue3.Text = Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue3.Text = "";
                        }
                        break;
                    case 3:
                        lblMaSacThueAnHan4.Text = vad1ag.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan4.Text = vad1ag.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue4.Text = Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue4.Text = "";
                        }
                        break;
                    case 4:
                        lblMaSacThueAnHan5.Text = vad1ag.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan5.Text = vad1ag.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue5.Text = Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue5.Text = "";
                        }
                        break;
                    case 5:
                        lblMaSacThueAnHan6.Text = vad1ag.KN1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThueAnHan6.Text = vad1ag.KN1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblHanNopThue6.Text = Convert.ToDateTime(vad1ag.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblHanNopThue6.Text = "";
                        }
                        break;
                }
            }
            if (Convert.ToDateTime(vad1ag.DPD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhoiHanhVanChuyen.Text = Convert.ToDateTime(vad1ag.DPD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhoiHanhVanChuyen.Text = "";
            }
            for (int i = 0; i < vad1ag.ST_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblDiaDiemTrungChuyen1.Text = vad1ag.ST_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDuKienDenDDTC1.Text = Convert.ToDateTime(vad1ag.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDuKienDenDDTC1.Text = "";
                        }
                        if (Convert.ToDateTime(vad1ag.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayKhoiHanh1.Text = Convert.ToDateTime(vad1ag.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayKhoiHanh1.Text = "";
                        }
                        break;
                    case 1:
                        lblDiaDiemTrungChuyen2.Text = vad1ag.ST_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDuKienDenDDTC2.Text = Convert.ToDateTime(vad1ag.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDuKienDenDDTC2.Text = "";
                        }
                        if (Convert.ToDateTime(vad1ag.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayKhoiHanh2.Text = Convert.ToDateTime(vad1ag.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayKhoiHanh2.Text = "";
                        }
                        break;
                    case 2:
                        lblDiaDiemTrungChuyen3.Text = vad1ag.ST_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        if (Convert.ToDateTime(vad1ag.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayDuKienDenDDTC3.Text = Convert.ToDateTime(vad1ag.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayDuKienDenDDTC3.Text = "";
                        }
                        if (Convert.ToDateTime(vad1ag.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgayKhoiHanh3.Text = Convert.ToDateTime(vad1ag.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgayKhoiHanh3.Text = "";
                        }
                        break;
                }
            }
            lblDiaDiemDichVanChuyen.Text = vad1ag.ARP.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ag.ADT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuKienDen.Text = Convert.ToDateTime(vad1ag.ADT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuKienDen.Text = "";
            }
            BindReportChiThiHQ(vad1ag);
        }
        public void BindReportChiThiHQ(VAD1AG0 vad1ag)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Ten", typeof(string));
                dt.Columns.Add("NoiDung", typeof(string));
                dt.Columns.Add("STT", typeof(string));
                int STT = 0;

                for (int i = 0; i < vad1ag.D__.listAttribute[0].ListValue.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    if (Convert.ToDateTime(vad1ag.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy") != ("01/01/1900"))
                    {
                        dr["Date"] = Convert.ToDateTime(vad1ag.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy");
                    }
                    dr["Ten"] = vad1ag.D__.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                    dr["STT"] = STT.ToString().ToUpper();
                    dr["NoiDung"] = vad1ag.D__.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 10)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["Date"] = "";
                    dr["Ten"] = "";
                    dr["NoiDung"] = "";
                    dr["STT"] = STT.ToString().ToUpper();
                    dt.Rows.Add(dr);
                }
                DetailReport.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "Date");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "Ten");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "NoiDung");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }
    }
}
