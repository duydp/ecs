﻿namespace DongBo.Controls
{
    partial class ucToKhaiMauDich
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgVanDon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucToKhaiMauDich));
            Janus.Windows.GridEX.GridEXLayout dgHoaDon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgGiayPhep_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgCo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgChungTuAnh_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgToKhai_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgChuyenCK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.btnGetToKhai = new System.Windows.Forms.Button();
            this.tabChungTu = new System.Windows.Forms.TabControl();
            this.TabVanDon = new System.Windows.Forms.TabPage();
            this.dgVanDon = new Janus.Windows.GridEX.GridEX();
            this.TabHoaDon = new System.Windows.Forms.TabPage();
            this.dgHoaDon = new Janus.Windows.GridEX.GridEX();
            this.tabHopDong = new System.Windows.Forms.TabPage();
            this.dgHopDong = new Janus.Windows.GridEX.GridEX();
            this.tabGiayPhep = new System.Windows.Forms.TabPage();
            this.dgGiayPhep = new Janus.Windows.GridEX.GridEX();
            this.tabCO = new System.Windows.Forms.TabPage();
            this.dgCo = new Janus.Windows.GridEX.GridEX();
            this.tabChungTuAnh = new System.Windows.Forms.TabPage();
            this.dgChungTuAnh = new Janus.Windows.GridEX.GridEX();
            this.tabToKhai = new System.Windows.Forms.TabPage();
            this.dgToKhai = new Janus.Windows.GridEX.GridEX();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.GetTTChungTu = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.tabChuyenCK = new System.Windows.Forms.TabPage();
            this.dgChuyenCK = new Janus.Windows.GridEX.GridEX();
            this.tabChungTu.SuspendLayout();
            this.TabVanDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVanDon)).BeginInit();
            this.TabHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHoaDon)).BeginInit();
            this.tabHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHopDong)).BeginInit();
            this.tabGiayPhep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgGiayPhep)).BeginInit();
            this.tabCO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCo)).BeginInit();
            this.tabChungTuAnh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgChungTuAnh)).BeginInit();
            this.tabToKhai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhai)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabChuyenCK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgChuyenCK)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGetToKhai
            // 
            this.btnGetToKhai.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGetToKhai.Location = new System.Drawing.Point(7, 161);
            this.btnGetToKhai.Name = "btnGetToKhai";
            this.btnGetToKhai.Size = new System.Drawing.Size(91, 23);
            this.btnGetToKhai.TabIndex = 25;
            this.btnGetToKhai.Text = "Lấy TT Tờ Khai";
            this.btnGetToKhai.UseVisualStyleBackColor = true;
            this.btnGetToKhai.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabChungTu
            // 
            this.tabChungTu.Controls.Add(this.TabVanDon);
            this.tabChungTu.Controls.Add(this.TabHoaDon);
            this.tabChungTu.Controls.Add(this.tabHopDong);
            this.tabChungTu.Controls.Add(this.tabGiayPhep);
            this.tabChungTu.Controls.Add(this.tabCO);
            this.tabChungTu.Controls.Add(this.tabChungTuAnh);
            this.tabChungTu.Controls.Add(this.tabChuyenCK);
            this.tabChungTu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabChungTu.Location = new System.Drawing.Point(0, 190);
            this.tabChungTu.Name = "tabChungTu";
            this.tabChungTu.SelectedIndex = 0;
            this.tabChungTu.Size = new System.Drawing.Size(668, 226);
            this.tabChungTu.TabIndex = 28;
            // 
            // TabVanDon
            // 
            this.TabVanDon.Controls.Add(this.dgVanDon);
            this.TabVanDon.Location = new System.Drawing.Point(4, 22);
            this.TabVanDon.Name = "TabVanDon";
            this.TabVanDon.Padding = new System.Windows.Forms.Padding(3);
            this.TabVanDon.Size = new System.Drawing.Size(660, 200);
            this.TabVanDon.TabIndex = 0;
            this.TabVanDon.Text = "Vận Đơn";
            this.TabVanDon.UseVisualStyleBackColor = true;
            // 
            // dgVanDon
            // 
            this.dgVanDon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgVanDon.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgVanDon.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgVanDon_DesignTimeLayout.LayoutString = resources.GetString("dgVanDon_DesignTimeLayout.LayoutString");
            this.dgVanDon.DesignTimeLayout = dgVanDon_DesignTimeLayout;
            this.dgVanDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgVanDon.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgVanDon.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgVanDon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgVanDon.GroupByBoxVisible = false;
            this.dgVanDon.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgVanDon.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgVanDon.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgVanDon.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgVanDon.Hierarchical = true;
            this.dgVanDon.Location = new System.Drawing.Point(3, 3);
            this.dgVanDon.Name = "dgVanDon";
            this.dgVanDon.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgVanDon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgVanDon.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgVanDon.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgVanDon.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgVanDon.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgVanDon.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgVanDon.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgVanDon.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgVanDon.Size = new System.Drawing.Size(654, 194);
            this.dgVanDon.TabIndex = 24;
            this.dgVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // TabHoaDon
            // 
            this.TabHoaDon.Controls.Add(this.dgHoaDon);
            this.TabHoaDon.Location = new System.Drawing.Point(4, 22);
            this.TabHoaDon.Name = "TabHoaDon";
            this.TabHoaDon.Padding = new System.Windows.Forms.Padding(3);
            this.TabHoaDon.Size = new System.Drawing.Size(660, 200);
            this.TabHoaDon.TabIndex = 1;
            this.TabHoaDon.Text = "Hóa Đơn TM";
            this.TabHoaDon.UseVisualStyleBackColor = true;
            // 
            // dgHoaDon
            // 
            this.dgHoaDon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgHoaDon.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgHoaDon.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgHoaDon_DesignTimeLayout.LayoutString = resources.GetString("dgHoaDon_DesignTimeLayout.LayoutString");
            this.dgHoaDon.DesignTimeLayout = dgHoaDon_DesignTimeLayout;
            this.dgHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgHoaDon.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgHoaDon.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgHoaDon.GroupByBoxVisible = false;
            this.dgHoaDon.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHoaDon.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgHoaDon.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHoaDon.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgHoaDon.Hierarchical = true;
            this.dgHoaDon.Location = new System.Drawing.Point(3, 3);
            this.dgHoaDon.Name = "dgHoaDon";
            this.dgHoaDon.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgHoaDon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgHoaDon.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgHoaDon.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHoaDon.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgHoaDon.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgHoaDon.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHoaDon.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgHoaDon.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgHoaDon.Size = new System.Drawing.Size(654, 194);
            this.dgHoaDon.TabIndex = 25;
            this.dgHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabHopDong
            // 
            this.tabHopDong.Controls.Add(this.dgHopDong);
            this.tabHopDong.Location = new System.Drawing.Point(4, 22);
            this.tabHopDong.Name = "tabHopDong";
            this.tabHopDong.Padding = new System.Windows.Forms.Padding(3);
            this.tabHopDong.Size = new System.Drawing.Size(660, 200);
            this.tabHopDong.TabIndex = 3;
            this.tabHopDong.Text = "Hợp Đồng TM";
            this.tabHopDong.UseVisualStyleBackColor = true;
            // 
            // dgHopDong
            // 
            this.dgHopDong.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgHopDong.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgHopDong.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgHopDong_DesignTimeLayout.LayoutString = resources.GetString("dgHopDong_DesignTimeLayout.LayoutString");
            this.dgHopDong.DesignTimeLayout = dgHopDong_DesignTimeLayout;
            this.dgHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgHopDong.FlatBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.dgHopDong.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgHopDong.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgHopDong.GroupByBoxVisible = false;
            this.dgHopDong.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHopDong.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgHopDong.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHopDong.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgHopDong.Hierarchical = true;
            this.dgHopDong.Location = new System.Drawing.Point(3, 3);
            this.dgHopDong.Name = "dgHopDong";
            this.dgHopDong.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgHopDong.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgHopDong.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgHopDong.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHopDong.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgHopDong.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgHopDong.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHopDong.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgHopDong.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgHopDong.Size = new System.Drawing.Size(654, 194);
            this.dgHopDong.TabIndex = 25;
            this.dgHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabGiayPhep
            // 
            this.tabGiayPhep.Controls.Add(this.dgGiayPhep);
            this.tabGiayPhep.Location = new System.Drawing.Point(4, 22);
            this.tabGiayPhep.Name = "tabGiayPhep";
            this.tabGiayPhep.Padding = new System.Windows.Forms.Padding(3);
            this.tabGiayPhep.Size = new System.Drawing.Size(660, 200);
            this.tabGiayPhep.TabIndex = 5;
            this.tabGiayPhep.Text = "Giấy Phép";
            this.tabGiayPhep.UseVisualStyleBackColor = true;
            // 
            // dgGiayPhep
            // 
            this.dgGiayPhep.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgGiayPhep.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgGiayPhep.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgGiayPhep_DesignTimeLayout.LayoutString = resources.GetString("dgGiayPhep_DesignTimeLayout.LayoutString");
            this.dgGiayPhep.DesignTimeLayout = dgGiayPhep_DesignTimeLayout;
            this.dgGiayPhep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgGiayPhep.FlatBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.dgGiayPhep.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgGiayPhep.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgGiayPhep.GroupByBoxVisible = false;
            this.dgGiayPhep.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgGiayPhep.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgGiayPhep.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgGiayPhep.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgGiayPhep.Hierarchical = true;
            this.dgGiayPhep.Location = new System.Drawing.Point(3, 3);
            this.dgGiayPhep.Name = "dgGiayPhep";
            this.dgGiayPhep.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgGiayPhep.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgGiayPhep.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgGiayPhep.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgGiayPhep.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgGiayPhep.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgGiayPhep.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgGiayPhep.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgGiayPhep.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgGiayPhep.Size = new System.Drawing.Size(654, 194);
            this.dgGiayPhep.TabIndex = 26;
            this.dgGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabCO
            // 
            this.tabCO.Controls.Add(this.dgCo);
            this.tabCO.Location = new System.Drawing.Point(4, 22);
            this.tabCO.Name = "tabCO";
            this.tabCO.Padding = new System.Windows.Forms.Padding(3);
            this.tabCO.Size = new System.Drawing.Size(660, 200);
            this.tabCO.TabIndex = 6;
            this.tabCO.Text = "CO";
            this.tabCO.UseVisualStyleBackColor = true;
            // 
            // dgCo
            // 
            this.dgCo.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgCo.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgCo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgCo_DesignTimeLayout.LayoutString = resources.GetString("dgCo_DesignTimeLayout.LayoutString");
            this.dgCo.DesignTimeLayout = dgCo_DesignTimeLayout;
            this.dgCo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgCo.FlatBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.dgCo.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgCo.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgCo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgCo.GroupByBoxVisible = false;
            this.dgCo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgCo.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgCo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgCo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgCo.Hierarchical = true;
            this.dgCo.Location = new System.Drawing.Point(3, 3);
            this.dgCo.Name = "dgCo";
            this.dgCo.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgCo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgCo.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgCo.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgCo.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgCo.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgCo.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgCo.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgCo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgCo.Size = new System.Drawing.Size(654, 194);
            this.dgCo.TabIndex = 27;
            this.dgCo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabChungTuAnh
            // 
            this.tabChungTuAnh.Controls.Add(this.dgChungTuAnh);
            this.tabChungTuAnh.Location = new System.Drawing.Point(4, 22);
            this.tabChungTuAnh.Name = "tabChungTuAnh";
            this.tabChungTuAnh.Padding = new System.Windows.Forms.Padding(3);
            this.tabChungTuAnh.Size = new System.Drawing.Size(660, 200);
            this.tabChungTuAnh.TabIndex = 7;
            this.tabChungTuAnh.Text = "Chứng Từ Dạng Ảnh";
            this.tabChungTuAnh.UseVisualStyleBackColor = true;
            // 
            // dgChungTuAnh
            // 
            this.dgChungTuAnh.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgChungTuAnh.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgChungTuAnh.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgChungTuAnh_DesignTimeLayout.LayoutString = resources.GetString("dgChungTuAnh_DesignTimeLayout.LayoutString");
            this.dgChungTuAnh.DesignTimeLayout = dgChungTuAnh_DesignTimeLayout;
            this.dgChungTuAnh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgChungTuAnh.FlatBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.dgChungTuAnh.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgChungTuAnh.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChungTuAnh.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgChungTuAnh.GroupByBoxVisible = false;
            this.dgChungTuAnh.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChungTuAnh.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgChungTuAnh.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChungTuAnh.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgChungTuAnh.Hierarchical = true;
            this.dgChungTuAnh.Location = new System.Drawing.Point(3, 3);
            this.dgChungTuAnh.Name = "dgChungTuAnh";
            this.dgChungTuAnh.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgChungTuAnh.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgChungTuAnh.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgChungTuAnh.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChungTuAnh.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgChungTuAnh.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgChungTuAnh.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChungTuAnh.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgChungTuAnh.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgChungTuAnh.Size = new System.Drawing.Size(654, 194);
            this.dgChungTuAnh.TabIndex = 28;
            this.dgChungTuAnh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabToKhai
            // 
            this.tabToKhai.Controls.Add(this.dgToKhai);
            this.tabToKhai.Location = new System.Drawing.Point(4, 22);
            this.tabToKhai.Name = "tabToKhai";
            this.tabToKhai.Padding = new System.Windows.Forms.Padding(3);
            this.tabToKhai.Size = new System.Drawing.Size(660, 129);
            this.tabToKhai.TabIndex = 3;
            this.tabToKhai.Text = "Tờ khai";
            this.tabToKhai.UseVisualStyleBackColor = true;
            // 
            // dgToKhai
            // 
            this.dgToKhai.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgToKhai.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgToKhai.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgToKhai_DesignTimeLayout.LayoutString = resources.GetString("dgToKhai_DesignTimeLayout.LayoutString");
            this.dgToKhai.DesignTimeLayout = dgToKhai_DesignTimeLayout;
            this.dgToKhai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgToKhai.FlatBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.dgToKhai.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgToKhai.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgToKhai.GroupByBoxVisible = false;
            this.dgToKhai.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhai.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgToKhai.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhai.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgToKhai.Hierarchical = true;
            this.dgToKhai.Location = new System.Drawing.Point(3, 3);
            this.dgToKhai.Name = "dgToKhai";
            this.dgToKhai.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgToKhai.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgToKhai.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgToKhai.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhai.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgToKhai.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgToKhai.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhai.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgToKhai.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgToKhai.Size = new System.Drawing.Size(654, 123);
            this.dgToKhai.TabIndex = 25;
            this.dgToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabToKhai);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(668, 155);
            this.tabControl1.TabIndex = 29;
            // 
            // GetTTChungTu
            // 
            this.GetTTChungTu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.GetTTChungTu.Location = new System.Drawing.Point(104, 161);
            this.GetTTChungTu.Name = "GetTTChungTu";
            this.GetTTChungTu.Size = new System.Drawing.Size(107, 23);
            this.GetTTChungTu.TabIndex = 25;
            this.GetTTChungTu.Text = "Lấy TT Chứng từ";
            this.GetTTChungTu.UseVisualStyleBackColor = true;
            this.GetTTChungTu.Click += new System.EventHandler(this.GetTTChungTu_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(577, 161);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(84, 23);
            this.btnUpdate.TabIndex = 25;
            this.btnUpdate.Text = "Cập nhật TK";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // tabChuyenCK
            // 
            this.tabChuyenCK.Controls.Add(this.dgChuyenCK);
            this.tabChuyenCK.Location = new System.Drawing.Point(4, 22);
            this.tabChuyenCK.Name = "tabChuyenCK";
            this.tabChuyenCK.Padding = new System.Windows.Forms.Padding(3);
            this.tabChuyenCK.Size = new System.Drawing.Size(660, 200);
            this.tabChuyenCK.TabIndex = 8;
            this.tabChuyenCK.Text = "Chuyển cửa  khẩu";
            this.tabChuyenCK.UseVisualStyleBackColor = true;
            // 
            // dgChuyenCK
            // 
            this.dgChuyenCK.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgChuyenCK.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgChuyenCK.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgChuyenCK_DesignTimeLayout.LayoutString = resources.GetString("dgChuyenCK_DesignTimeLayout.LayoutString");
            this.dgChuyenCK.DesignTimeLayout = dgChuyenCK_DesignTimeLayout;
            this.dgChuyenCK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgChuyenCK.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgChuyenCK.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChuyenCK.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgChuyenCK.GroupByBoxVisible = false;
            this.dgChuyenCK.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChuyenCK.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgChuyenCK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChuyenCK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgChuyenCK.Hierarchical = true;
            this.dgChuyenCK.Location = new System.Drawing.Point(3, 3);
            this.dgChuyenCK.Name = "dgChuyenCK";
            this.dgChuyenCK.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgChuyenCK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgChuyenCK.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgChuyenCK.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChuyenCK.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgChuyenCK.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgChuyenCK.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgChuyenCK.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgChuyenCK.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgChuyenCK.Size = new System.Drawing.Size(654, 194);
            this.dgChuyenCK.TabIndex = 25;
            this.dgChuyenCK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // ucToKhaiMauDich
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tabChungTu);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.GetTTChungTu);
            this.Controls.Add(this.btnGetToKhai);
            this.Name = "ucToKhaiMauDich";
            this.Size = new System.Drawing.Size(668, 416);
            this.Load += new System.EventHandler(this.ucToKhaiMauDich_Load);
            this.tabChungTu.ResumeLayout(false);
            this.TabVanDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVanDon)).EndInit();
            this.TabHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgHoaDon)).EndInit();
            this.tabHopDong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgHopDong)).EndInit();
            this.tabGiayPhep.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgGiayPhep)).EndInit();
            this.tabCO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCo)).EndInit();
            this.tabChungTuAnh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgChungTuAnh)).EndInit();
            this.tabToKhai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhai)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabChuyenCK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgChuyenCK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGetToKhai;
        private System.Windows.Forms.TabControl tabChungTu;
        private System.Windows.Forms.TabPage TabVanDon;
        private Janus.Windows.GridEX.GridEX dgVanDon;
        private System.Windows.Forms.TabPage TabHoaDon;
        private Janus.Windows.GridEX.GridEX dgHoaDon;
        private System.Windows.Forms.TabPage tabHopDong;
        private Janus.Windows.GridEX.GridEX dgHopDong;
        private System.Windows.Forms.TabPage tabToKhai;
        private Janus.Windows.GridEX.GridEX dgToKhai;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabGiayPhep;
        private Janus.Windows.GridEX.GridEX dgGiayPhep;
        private System.Windows.Forms.TabPage tabCO;
        private Janus.Windows.GridEX.GridEX dgCo;
        private System.Windows.Forms.TabPage tabChungTuAnh;
        private Janus.Windows.GridEX.GridEX dgChungTuAnh;
        private System.Windows.Forms.Button GetTTChungTu;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TabPage tabChuyenCK;
        private Janus.Windows.GridEX.GridEX dgChuyenCK;
    }
}
