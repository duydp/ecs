﻿namespace PDFSignedDigital
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListFileSign_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkFolder = new System.Windows.Forms.CheckBox();
            this.btnFolderSigned = new System.Windows.Forms.Button();
            this.btnFolderSign = new System.Windows.Forms.Button();
            this.txtFolderSigned = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtFolderSign = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListFileSign = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbSigns = new System.Windows.Forms.ComboBox();
            this.chkPassword = new System.Windows.Forms.CheckBox();
            this.chkCertiface = new System.Windows.Forms.CheckBox();
            this.btnSign = new System.Windows.Forms.Button();
            this.btnOpenFolder = new System.Windows.Forms.Button();
            this.btnCheck = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lblProduct = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPassword = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListFileSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.panel1);
            this.grbMain.Size = new System.Drawing.Size(607, 564);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(191)))), ((int)(((byte)(165)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(607, 60);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(155, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(276, 32);
            this.label3.TabIndex = 9;
            this.label3.Text = "SIGN DIGITAL CERTIFACE";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.White;
            this.uiGroupBox1.Controls.Add(this.chkFolder);
            this.uiGroupBox1.Controls.Add(this.btnFolderSigned);
            this.uiGroupBox1.Controls.Add(this.btnFolderSign);
            this.uiGroupBox1.Controls.Add(this.txtFolderSigned);
            this.uiGroupBox1.Controls.Add(this.txtFolderSign);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 60);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(607, 123);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.Text = "1.1 KÝ FILE PDF";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkFolder
            // 
            this.chkFolder.AutoSize = true;
            this.chkFolder.Checked = true;
            this.chkFolder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFolder.Location = new System.Drawing.Point(160, 80);
            this.chkFolder.Name = "chkFolder";
            this.chkFolder.Size = new System.Drawing.Size(95, 17);
            this.chkFolder.TabIndex = 5;
            this.chkFolder.Text = "LƯU THƯ MỤC";
            this.chkFolder.UseVisualStyleBackColor = true;
            this.chkFolder.CheckedChanged += new System.EventHandler(this.chkFolder_CheckedChanged);
            // 
            // btnFolderSigned
            // 
            this.btnFolderSigned.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnFolderSigned.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFolderSigned.ForeColor = System.Drawing.Color.White;
            this.btnFolderSigned.Location = new System.Drawing.Point(454, 51);
            this.btnFolderSigned.Name = "btnFolderSigned";
            this.btnFolderSigned.Size = new System.Drawing.Size(75, 23);
            this.btnFolderSigned.TabIndex = 4;
            this.btnFolderSigned.Text = "CHỌN";
            this.btnFolderSigned.UseVisualStyleBackColor = false;
            this.btnFolderSigned.Click += new System.EventHandler(this.btnFolderSigned_Click);
            // 
            // btnFolderSign
            // 
            this.btnFolderSign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnFolderSign.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFolderSign.ForeColor = System.Drawing.Color.White;
            this.btnFolderSign.Location = new System.Drawing.Point(454, 23);
            this.btnFolderSign.Name = "btnFolderSign";
            this.btnFolderSign.Size = new System.Drawing.Size(75, 23);
            this.btnFolderSign.TabIndex = 2;
            this.btnFolderSign.Text = "CHỌN";
            this.btnFolderSign.UseVisualStyleBackColor = false;
            this.btnFolderSign.Click += new System.EventHandler(this.btnFolderSign_Click);
            // 
            // txtFolderSigned
            // 
            this.txtFolderSigned.Location = new System.Drawing.Point(160, 53);
            this.txtFolderSigned.Name = "txtFolderSigned";
            this.txtFolderSigned.Size = new System.Drawing.Size(288, 21);
            this.txtFolderSigned.TabIndex = 3;
            this.txtFolderSigned.VisualStyleManager = this.vsmMain;
            // 
            // txtFolderSign
            // 
            this.txtFolderSign.Location = new System.Drawing.Point(160, 25);
            this.txtFolderSign.Name = "txtFolderSign";
            this.txtFolderSign.Size = new System.Drawing.Size(288, 21);
            this.txtFolderSign.TabIndex = 1;
            this.txtFolderSign.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "THƯ MỤC LƯU FILE ĐÃ KÝ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "THƯ MỤC LƯU FILE CẦN KÝ ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(155, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(366, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Phần mềm hỗ trợ ký File có định dạng .docx hoặc .xlsx hoặc .pdf ";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.White;
            this.uiGroupBox5.Controls.Add(this.dgListFileSign);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 337);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(607, 227);
            this.uiGroupBox5.TabIndex = 23;
            this.uiGroupBox5.Text = "DANH SÁCH FILE KÝ";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListFileSign
            // 
            this.dgListFileSign.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListFileSign.ColumnAutoResize = true;
            dgListFileSign_DesignTimeLayout.LayoutString = resources.GetString("dgListFileSign_DesignTimeLayout.LayoutString");
            this.dgListFileSign.DesignTimeLayout = dgListFileSign_DesignTimeLayout;
            this.dgListFileSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListFileSign.GroupByBoxVisible = false;
            this.dgListFileSign.Location = new System.Drawing.Point(3, 17);
            this.dgListFileSign.Name = "dgListFileSign";
            this.dgListFileSign.RecordNavigator = true;
            this.dgListFileSign.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListFileSign.Size = new System.Drawing.Size(601, 207);
            this.dgListFileSign.TabIndex = 22;
            this.dgListFileSign.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListFileSign.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.White;
            this.uiGroupBox4.Controls.Add(this.cbSigns);
            this.uiGroupBox4.Controls.Add(this.chkPassword);
            this.uiGroupBox4.Controls.Add(this.chkCertiface);
            this.uiGroupBox4.Controls.Add(this.btnSign);
            this.uiGroupBox4.Controls.Add(this.btnOpenFolder);
            this.uiGroupBox4.Controls.Add(this.btnCheck);
            this.uiGroupBox4.Controls.Add(this.btnRefresh);
            this.uiGroupBox4.Controls.Add(this.lblProduct);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.txtPassword);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 183);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(607, 154);
            this.uiGroupBox4.TabIndex = 13;
            this.uiGroupBox4.Text = "1.2 THÔNG TIN CHỮ KÝ SỐ";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbSigns
            // 
            this.cbSigns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.cbSigns.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSigns.ForeColor = System.Drawing.Color.White;
            this.cbSigns.FormattingEnabled = true;
            this.cbSigns.Location = new System.Drawing.Point(160, 22);
            this.cbSigns.Name = "cbSigns";
            this.cbSigns.Size = new System.Drawing.Size(288, 21);
            this.cbSigns.TabIndex = 37;
            this.cbSigns.TextChanged += new System.EventHandler(this.cbSigns_TextChanged);
            // 
            // chkPassword
            // 
            this.chkPassword.AutoSize = true;
            this.chkPassword.Checked = true;
            this.chkPassword.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPassword.Location = new System.Drawing.Point(308, 104);
            this.chkPassword.Name = "chkPassword";
            this.chkPassword.Size = new System.Drawing.Size(84, 17);
            this.chkPassword.TabIndex = 19;
            this.chkPassword.Text = "LƯU MÃ PIN";
            this.chkPassword.UseVisualStyleBackColor = true;
            this.chkPassword.CheckedChanged += new System.EventHandler(this.chkPassword_CheckedChanged);
            // 
            // chkCertiface
            // 
            this.chkCertiface.AutoSize = true;
            this.chkCertiface.Checked = true;
            this.chkCertiface.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCertiface.Location = new System.Drawing.Point(159, 103);
            this.chkCertiface.Name = "chkCertiface";
            this.chkCertiface.Size = new System.Drawing.Size(103, 17);
            this.chkCertiface.TabIndex = 17;
            this.chkCertiface.Text = "LƯU CHỮ KÝ SỐ";
            this.chkCertiface.UseVisualStyleBackColor = true;
            this.chkCertiface.CheckedChanged += new System.EventHandler(this.chkCertiface_CheckedChanged);
            // 
            // btnSign
            // 
            this.btnSign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnSign.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSign.ForeColor = System.Drawing.Color.White;
            this.btnSign.Location = new System.Drawing.Point(158, 125);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(143, 23);
            this.btnSign.TabIndex = 20;
            this.btnSign.Text = "KÝ";
            this.btnSign.UseVisualStyleBackColor = false;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // btnOpenFolder
            // 
            this.btnOpenFolder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnOpenFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenFolder.ForeColor = System.Drawing.Color.White;
            this.btnOpenFolder.Location = new System.Drawing.Point(305, 125);
            this.btnOpenFolder.Name = "btnOpenFolder";
            this.btnOpenFolder.Size = new System.Drawing.Size(143, 23);
            this.btnOpenFolder.TabIndex = 21;
            this.btnOpenFolder.Text = "MỞ THƯ MỤC KÝ";
            this.btnOpenFolder.UseVisualStyleBackColor = false;
            this.btnOpenFolder.Click += new System.EventHandler(this.btnOpenFolder_Click);
            // 
            // btnCheck
            // 
            this.btnCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnCheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheck.ForeColor = System.Drawing.Color.White;
            this.btnCheck.Location = new System.Drawing.Point(305, 75);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(143, 23);
            this.btnCheck.TabIndex = 16;
            this.btnCheck.Text = "KIỂM TRA";
            this.btnCheck.UseVisualStyleBackColor = false;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.ForeColor = System.Drawing.Color.White;
            this.btnRefresh.Location = new System.Drawing.Point(454, 20);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 16;
            this.btnRefresh.Text = "LÀM MỚI";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefersh_Click);
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduct.ForeColor = System.Drawing.Color.Red;
            this.lblProduct.Location = new System.Drawing.Point(158, 53);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(0, 16);
            this.lblProduct.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "NHÀ CUNG CẤP";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "CHỮ KÝ SỐ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 84);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "MÃ PIN ";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(159, 76);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(143, 21);
            this.txtPassword.TabIndex = 18;
            this.txtPassword.VisualStyleManager = this.vsmMain;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 564);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(623, 603);
            this.MinimumSize = new System.Drawing.Size(623, 603);
            this.Name = "Main";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "KÝ FILE";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListFileSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnFolderSigned;
        private System.Windows.Forms.Button btnFolderSign;
        private Janus.Windows.GridEX.EditControls.EditBox txtFolderSigned;
        private Janus.Windows.GridEX.EditControls.EditBox txtFolderSign;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.GridEX dgListFileSign;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Button btnSign;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.EditBox txtPassword;
        private System.Windows.Forms.Button btnOpenFolder;
        private System.Windows.Forms.CheckBox chkFolder;
        private System.Windows.Forms.CheckBox chkPassword;
        private System.Windows.Forms.CheckBox chkCertiface;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbSigns;
        private System.Windows.Forms.Label lblProduct;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Label label5;
    }
}