﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.KDT.SHARE.Components
{
    public partial class KDT_ContainerDangKy
    {
        List<KDT_ContainerBS> _ListCont = new List<KDT_ContainerBS>();
        
        public List<KDT_ContainerBS> ListCont
        {
            set { this._ListCont = value; }
            get { return this._ListCont; }
        }


        public static KDT_ContainerDangKy LoadFull(long id)
        {
            KDT_ContainerDangKy ContDK = new KDT_ContainerDangKy();
            ContDK = KDT_ContainerDangKy.Load(id);
            if (ContDK.ID == id)
            {
                List<KDT_ContainerBS> lisLoaiPK = new List<KDT_ContainerBS>();
                lisLoaiPK = KDT_ContainerBS.SelectCollectionDynamic("Master_id = " + ContDK.ID, "");
                foreach (KDT_ContainerBS Cont in lisLoaiPK)
                {
                    ContDK.ListCont.Add(Cont);
                }
                return ContDK;
            }
            else
                return null;
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            ListCont = KDT_ContainerBS.SelectCollectionDynamic("Master_id =" + this.ID, "");
            foreach (KDT_ContainerBS item in ListCont)
            {
                item.Delete();
            }
            this.Delete();
        }
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (KDT_ContainerBS item in ListCont)
                    {
                        item.Master_id = this.ID;
                        if (item.ID == 0)
                        {
                          item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
      
    }
}
