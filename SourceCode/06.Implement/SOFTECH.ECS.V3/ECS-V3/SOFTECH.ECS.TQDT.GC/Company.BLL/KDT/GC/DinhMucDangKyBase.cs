using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.KDT.GC
{
	public partial class DinhMucDangKy  : EntityBase
	{
		#region Private members.
		
		protected long _ID;
		protected long _SoTiepNhan;
		protected int _TrangThaiXuLy=-1;
		protected DateTime _NgayTiepNhan = new DateTime(1900, 01, 01);
		protected long _ID_HopDong;
		protected string _MaHaiQuan = String.Empty;
		protected string _MaDoanhNghiep = String.Empty;
		protected string _GUIDSTR = String.Empty;
		protected string _DeXuatKhac = String.Empty;
		protected string _LyDoSua = String.Empty;
		protected int _ActionStatus;
		protected string _GuidReference = String.Empty;
		protected string _NamTN = String.Empty;
		protected string _HUONGDAN = String.Empty;
		protected string _PhanLuong = String.Empty;
		protected string _Huongdan_PL = String.Empty;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public long SoTiepNhan
		{
			set {this._SoTiepNhan = value;}
			get {return this._SoTiepNhan;}
		}
		public int TrangThaiXuLy
		{
			set {this._TrangThaiXuLy = value;}
			get {return this._TrangThaiXuLy;}
		}
		public DateTime NgayTiepNhan
		{
			set {this._NgayTiepNhan = value;}
			get {return this._NgayTiepNhan;}
		}
		public long ID_HopDong
		{
			set {this._ID_HopDong = value;}
			get {return this._ID_HopDong;}
		}
		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value.Trim();}
			get {return this._MaHaiQuan.Trim();}
		}
		public string MaDoanhNghiep
		{
            set { this._MaDoanhNghiep = value.Trim(); }
            get { return this._MaDoanhNghiep.Trim(); }
		}
		public string GUIDSTR
		{
			set {this._GUIDSTR = value;}
			get {return this._GUIDSTR;}
		}
		public string DeXuatKhac
		{
			set {this._DeXuatKhac = value;}
			get {return this._DeXuatKhac;}
		}
		public string LyDoSua
		{
			set {this._LyDoSua = value;}
			get {return this._LyDoSua;}
		}
		public int ActionStatus
		{
			set {this._ActionStatus = value;}
			get {return this._ActionStatus;}
		}
		public string GuidReference
		{
			set {this._GuidReference = value;}
			get {return this._GuidReference;}
		}
		public string NamTN
		{
			set {this._NamTN = value;}
			get {return this._NamTN;}
		}
		public string HUONGDAN
		{
			set {this._HUONGDAN = value;}
			get {return this._HUONGDAN;}
		}
		public string PhanLuong
		{
			set {this._PhanLuong = value;}
			get {return this._PhanLuong;}
		}
		public string Huongdan_PL
		{
			set {this._Huongdan_PL = value;}
			get {return this._Huongdan_PL;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_KDT_GC_DinhMucDangKy_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) this._ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) this._NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) this._Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				reader.Close();
				return true;
			}
			reader.Close();
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public DinhMucDangKyCollection SelectCollectionBy_ID_HopDong()
		{
			string spName = "p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
			
			DinhMucDangKyCollection collection = new DinhMucDangKyCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				DinhMucDangKy  entity = new DinhMucDangKy ();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_ID_HopDong()
		{
			string spName = "p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_KDT_GC_DinhMucDangKy_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_GC_DinhMucDangKy_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_GC_DinhMucDangKy_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_GC_DinhMucDangKy_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public DinhMucDangKyCollection SelectCollectionAll()
		{
			DinhMucDangKyCollection collection = new DinhMucDangKyCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				DinhMucDangKy  entity = new DinhMucDangKy ();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public DinhMucDangKyCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			DinhMucDangKyCollection collection = new DinhMucDangKyCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				DinhMucDangKy  entity = new DinhMucDangKy ();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetString(reader.GetOrdinal("NamTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_GC_DinhMucDangKy_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
			this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
			this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
			this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
			this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
			this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
			this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
			this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
			this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, this._NamTN);
			this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
			this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
			this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);
			
			if (transaction != null)
			{
				this.db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				this.db.ExecuteNonQuery(dbCommand);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(DinhMucDangKyCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (DinhMucDangKy  item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, DinhMucDangKyCollection collection)
        {
            foreach (DinhMucDangKy  item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_GC_DinhMucDangKy_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
			this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
			this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
			this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
			this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
			this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
			this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
			this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
			this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, this._NamTN);
			this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
			this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
			this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(DinhMucDangKyCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (DinhMucDangKy item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_GC_DinhMucDangKy_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
			this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
			this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
			this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
			this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
			this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
			this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
			this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
			this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.NVarChar, this._NamTN);
			this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
			this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
			this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(DinhMucDangKyCollection collection, SqlTransaction transaction)
        {
            foreach (DinhMucDangKy  item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_GC_DinhMucDangKy_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(DinhMucDangKyCollection collection, SqlTransaction transaction)
        {
            foreach (DinhMucDangKy item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(DinhMucDangKyCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (DinhMucDangKy item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		public int DeleteBy_ID_HopDong()
		{
			string spName = "p_KDT_GC_DinhMucDangKy_DeleteBy_ID_HopDong";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
						
            return this.db.ExecuteNonQuery(dbCommand);			
		}
		public int DeleteBy_ID_HopDong(SqlTransaction transaction)
		{
			string spName = "p_KDT_GC_DinhMucDangKy_DeleteBy_ID_HopDong";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, this._ID_HopDong);
						
            return this.db.ExecuteNonQuery(dbCommand,transaction);			
		}
		#endregion
	}	
}