﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Company.Interface.Report
{
    public partial class CauHinhInForm : Company.Interface.BaseForm
    {
        public CauHinhInForm()
        {
            InitializeComponent();
        }
        public int versionHD = 0;
        public Label lblDonGiaNT { get { return _lblDonGiaNT; } }
        public Label lblTriGiaNT { get { return _lblTriGiaNT; } }
        public Label lblLuongSP { get { return _lblLuongSp; } }
        public List<Label> Listlable = new List<Label>();
        private void CauHinhInForm_Load(object sender, EventArgs e)
        {
            
            
            XmlDocument doc = new XmlDocument();
            doc.Load(Application.StartupPath + "\\Config.xml");
            XmlNode node = doc.SelectSingleNode("Root/Type");
            versionHD = Convert.ToInt32(node.InnerText);


            if (Listlable.Count > 0)
            {
                timer1.Interval = 500;
                timer1.Start();
            }
            try
            {
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                //
                txtDinhMuc.Value = GlobalSettings.SoThapPhan.DinhMuc;
                txtLuongNPL.Value = GlobalSettings.SoThapPhan.LuongNPL;
                txtLuongSP.Value = GlobalSettings.SoThapPhan.LuongSP;
                txtTLHH.Value = GlobalSettings.SoThapPhan.TLHH;

                /*Updated by Hungtq 14/01/2011*/

                cbbSapXep.SelectedValue = GlobalSettings.SoThapPhan.SapXepTheoTK;
                cbNPLKoTK.SelectedValue = GlobalSettings.SoThapPhan.NPLKoTK;
                cbMauBC01.SelectedValue = GlobalSettings.SoThapPhan.MauBC01;
                cbTachLam2.SelectedValue = GlobalSettings.SoThapPhan.TachLam2;
                cbMauBC04.SelectedValue = GlobalSettings.SoThapPhan.MauBC04;

                txtTriGiaNT.Value = GlobalSettings.TriGiaNT;
                txtDonGiaNT.Value = GlobalSettings.DonGiaNT;
                //update by KHANHHN - cập nhật format số thập phân của tổng trị giá
                // 24-04-2012
                txtTongTriGiaNT.Value = GlobalSettings.TongTriGiaNT;
                //txtBCTK.Value = GlobalSettings.SoThapPhan_BaoCaoThanhKhoan;

                txtDiaPhuong.Text = GlobalSettings.DiaPhuong;
                rdNgayTuDong.Checked = GlobalSettings.NgayHeThong;
                rdNgayTuNhap.Checked = !rdNgayTuDong.Checked;
                cbbAmTKTiep.SelectedValue = GlobalSettings.AmTKTiep;
                cboChenhLech.SelectedValue = GlobalSettings.CHENHLECH_THN_THX;

                cboSTNDT.SelectedValue = GlobalSettings.InToKhai.SoTNDKDT;
                cboTGTT.SelectedValue = GlobalSettings.InToKhai.TriGiTT;
                cboLoaiHinh.SelectedValue = GlobalSettings.InToKhai.LoaiHinh;
                cboDVT.SelectedValue = GlobalSettings.InToKhai.DVTCon;

                cboTKX.SelectedValue = GlobalSettings.TKX;
                cboTKhaiKoTK.SelectedValue = GlobalSettings.ToKhaiKoTK;
                cboHienThiHD.SelectedValue = GlobalSettings.HienThiHD;
                cboChiPhi.SelectedValue = GlobalSettings.ChiPhiKhac;
                cboKCX.SelectedValue = GlobalSettings.TKhoanKCX;
                numericUpDown1.Value = (decimal)GlobalSettings.FontDongHang;
                numericFontBCXNT.Value = (decimal)GlobalSettings.FontBCXNT;

                //minhnd 06/10/2014
                numFontHuongDan.Value = Convert.ToDecimal(GlobalSettings.fontsize);

                //phiph 25/10/2012
                numericFontTK.Value = (decimal)GlobalSettings.FontToKhai;
                cboChenhLech.ComboStyle = Janus.Windows.EditControls.ComboStyle.Combo;
                cboXuongDongThongTinDiaChi.SelectedValue = GlobalSettings.WordWrap;
                ch07DMDK.Checked = GlobalSettings.M07DMDK;

                string delay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TimeDelay");
                numTimeDelay.Value = Convert.ToDecimal(delay);
                cmbInDinhMuc.SelectedValue = GlobalSettings.InDinhMucTT196;
                chkKhongDungBKContainer.Checked = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsKhongDungBangKeCont"));
                chkInCV3742.Checked = Company.KDT.SHARE.Components.Globals.InCV3742;
                txtNoiDungSoHSTK.Text = GlobalSettings.NoiDungSoHoSoThanhKhoan.Trim().ToUpper();
                chkThanhKhoanNhieuChiCuc.Checked = GlobalSettings.ThanhKhoanNhieuChiCuc;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string dp = "";
                cvError.Validate();
                if (!cvError.IsValid) return;
                else
                {

                    if (versionHD == 1)
                    {
                        Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();
                        htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                        htpk.PassWord = "0";
                        htpk.Key_Config = "LuongSP";
                        htpk.Value_Config = txtLuongSP.Text;
                        htpk.InsertUpdate();
                    }
                    GlobalSettings.Luu_SoThapPhan((int)txtDinhMuc.Value, (int)txtLuongNPL.Value, (int)txtLuongSP.Value, (int)cbbSapXep.SelectedValue, (int)cbNPLKoTK.SelectedValue, (int)cbMauBC01.SelectedValue, (int)cbTachLam2.SelectedValue, (int)cbMauBC04.SelectedValue, (int)txtTLHH.Value);
                    GlobalSettings.Luu_InToKhai((int)cboSTNDT.SelectedValue, (int)cboTGTT.SelectedValue, (int)cboLoaiHinh.SelectedValue, (int)cboDVT.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VisibleHD", cboHienThiHD.SelectedValue);

                    //Hungtq 22/12/2010. Luu cau hinh
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaPhuong", txtDiaPhuong.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgayHeThong", rdNgayTuDong.Checked.ToString());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("AmTKTiep", cbbAmTKTiep.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CL_THN_THX", cboChenhLech.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TKX", cboTKX.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ToKhaiKoTK", (int)cboTKhaiKoTK.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ChiPhiKhac", (int)cboChiPhi.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontTenHang", (float)numericUpDown1.Value);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TKhoanKCX", (int)cboKCX.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontBCXNT", (float)numericFontBCXNT.Value);
                    //phiph 25/10/2012
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontToKhai", (float)numericFontTK.Value);
                    //xuống dòng thông tin địa chỉ
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WordWrap", cboXuongDongThongTinDiaChi.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("07DMDK", ch07DMDK.Checked);

                    //minhnd 06/10/2014
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontHuongDan",numFontHuongDan.Value);

                    if (txtDiaPhuong.Text.Trim() == "")
                        dp = ".................";
                    else
                        dp = txtDiaPhuong.Text;

                    if (rdNgayTuDong.Checked == true)
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", dp + " ,ngày " + DateTime.Now.Day.ToString("00") + " tháng " + DateTime.Now.Month.ToString("00") + " năm " + DateTime.Now.Year.ToString("0000"));
                    else
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TieudeNgay", dp + " ,ngày........tháng........năm........ ");

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TriGiaNT", txtTriGiaNT.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DonGiaNT", txtDonGiaNT.Text.Trim());
                    // Update KhanhHN
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TongTriGiaNT", txtTongTriGiaNT.Text.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("BCTK", txtBCTK.Text.Trim());

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TimeDelay", numTimeDelay.Value.ToString());
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("InDinhMucTT196", cmbInDinhMuc.SelectedValue);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NoiDungSoHSTK", txtNoiDungSoHSTK.Text.Trim());
                    GlobalSettings.RefreshKey();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsKhongDungBangKeCont", chkKhongDungBKContainer.Checked.ToString());
                    Company.KDT.SHARE.Components.Globals.TriGiaNT = Convert.ToInt32(txtTriGiaNT.Value);
                    Company.KDT.SHARE.Components.Globals.DonGiaNT = Convert.ToInt32(txtDonGiaNT.Value);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("InCV3742", chkInCV3742.Checked.ToString());
                    Company.KDT.SHARE.Components.Globals.InCV3742 = chkInCV3742.Checked;
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ThanhKhoanNhieuChiCuc", chkThanhKhoanNhieuChiCuc.Checked.ToString());
                    GlobalSettings.ThanhKhoanNhieuChiCuc = chkThanhKhoanNhieuChiCuc.Checked;
                    MLMessages("Lưu thông tin cấu hình thành công", "MSG_SAV03", "", false);
                    this.Close();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (Label lable in Listlable)
            {
                lable.ForeColor = lable.ForeColor == Color.Black ? Color.Red : Color.Black;
            }
        }

    }
}

