﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Text;


namespace SignRemote
{
    public class ServiceAPI
    {
        public T Deserialize<T>(string sfmtObjectXml)
        {
            if (string.IsNullOrEmpty(sfmtObjectXml)) throw new ArgumentNullException("Deserialize parameter ->sfmtObjectXml không được rỗng");
            using (StringReader reader = new StringReader(sfmtObjectXml))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }
        public string Serializer(object obj, Encoding encoding, bool removeDeclartion, bool removeNameSpace)
        {

            XmlWriterSettings xmlWS = new XmlWriterSettings();
            xmlWS.OmitXmlDeclaration = true;
            xmlWS.Encoding = encoding;
            StringWriter sWriter = new StringWriter();
            using (XmlWriter xmlWriter = XmlWriter.Create(sWriter, xmlWS))
            {
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                namespaces.Add("", "");//Remove NameSpace               
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(xmlWriter, obj, namespaces);
                return sWriter.ToString();
            }
        }
        public string Client(MessageSend msgSend)
        {
            MessageSend messageReturn = null;
            try
            {


                if (msgSend.SendStatus == SendStatusInfo.Send || msgSend.SendStatus == SendStatusInfo.SendVNACCS)
                {
                    INBOX inbox = new INBOX()
                    {
                        CREATE_TIME = DateTime.Now,
                        MSG_FROM = msgSend.From,
                        MSG_TO = msgSend.To,
                        MSG_ORIGIN = msgSend.Message.Content,
                        GUIDSTR = msgSend.Message.GuidStr,
                        STATUS = msgSend.SendStatus == SendStatusInfo.Send ? SendStatusInfo.NoSign : SendStatusInfo.NoSignVNACCS
                    };
                    inbox.Insert();
                    messageReturn = new MessageSend()
                    {
                        Warrning = "Đã gửi thông tin",
                        SendStatus = SendStatusInfo.Wait
                    };
                }
                else if (msgSend.SendStatus == SendStatusInfo.Request || msgSend.SendStatus == SendStatusInfo.RequestVNACCS)
                {
                    List<INBOX> items = INBOX.SelectCollectionDynamic("GuidStr='" + msgSend.Message.GuidStr + "'", "");
                    if (items.Count == 0)
                        messageReturn = new MessageSend()
                        {
                            SendStatus = SendStatusInfo.Error,
                            Warrning = string.Format("Thông tin {0} chưa được gửi đến hệ thống", msgSend.Message.GuidStr)
                        };
                    else
                    {
                        string sfmtWhere = string.Format("MSG_TO='{0}' and  GuidStr='{1}'", msgSend.From, msgSend.Message.GuidStr);
                        List<OUTBOX> outMessages = OUTBOX.SelectCollectionDynamic(sfmtWhere, "");
                        if (outMessages.Count > 0)
                        {
                            OUTBOX item = outMessages[0];
                            if (item.STATUS == SendStatusInfo.Sign || item.STATUS == SendStatusInfo.SignVNACCS)
                            {
                                messageReturn = new MessageSend()
                                {
                                    SendStatus = item.STATUS,
                                    From = item.MSG_FROM,
                                    To = item.MSG_TO,
                                    Message = new Message()
                                    {
                                        SignCert = item.MSG_SIGN_CERT,
                                        SignData = item.MSG_SIGN_DATA
                                    }
                                };
                                item.Delete();
                                items[0].Delete();
                            }
                            else messageReturn = new MessageSend()
                            {
                                SendStatus = SendStatusInfo.Wait,
                                Warrning = string.Format("Hệ thống chưa ký thông tin {0}", msgSend.Message.GuidStr)
                            };


                        }
                        else
                        {
                            messageReturn = new MessageSend()
                            {
                                SendStatus = SendStatusInfo.Wait,
                                Warrning = string.Format("Hệ thống chưa ký thông tin {0}", msgSend.Message.GuidStr)
                            };
                        }
                    }

                }
                else throw new Exception("Không xác định thông tin gửi");


            }
            catch (Exception ex)
            {
                messageReturn = new MessageSend()
                {
                    SendStatus = SendStatusInfo.Error,
                    Warrning = ex.Message
                };
            }
            return Serializer(messageReturn, Encoding.UTF8, true, true);

        }
        public string Server(MessageSend msgSend)
        {
            MessageSend messageReturn = null;
            try
            {


                if (msgSend.SendStatus == SendStatusInfo.Send || msgSend.SendStatus == SendStatusInfo.SendVNACCS)
                {
                    OUTBOX outbox = new OUTBOX()
                    {
                        CREATE_TIME = DateTime.Now,
                        MSG_FROM = msgSend.From,
                        MSG_TO = msgSend.To,
                        MSG_SIGN_DATA = msgSend.Message.SignData,
                        MSG_SIGN_CERT = msgSend.Message.SignCert,
                        GUIDSTR = msgSend.Message.GuidStr,
                        STATUS = msgSend.SendStatus == SendStatusInfo.Send ? SendStatusInfo.Sign : SendStatusInfo.SignVNACCS
                    };
                    outbox.Insert();
                    messageReturn = new MessageSend()
                    {
                        Warrning = "Đã gửi thông tin",
                        SendStatus = SendStatusInfo.Wait
                    };
                }
                else if (msgSend.SendStatus == SendStatusInfo.Request || msgSend.SendStatus == SendStatusInfo.RequestVNACCS)
                {
                    string sfmtWhere = string.Format("MSG_FROM='{0}' and ([STATUS]={1} OR [STATUS] = {2}) ", msgSend.From, SendStatusInfo.NoSign, SendStatusInfo.NoSignVNACCS);
                    INBOX item = INBOX.SelectTop1(sfmtWhere, "");
                    if (item != null && (item.STATUS == SendStatusInfo.NoSign || item.STATUS == SendStatusInfo.NoSignVNACCS))
                    {
                        messageReturn = new MessageSend()
                        {
                            SendStatus = item.STATUS,
                            From = item.MSG_FROM,
                            To = item.MSG_TO,
                            Message = new Message()
                            {
                                Content = item.MSG_ORIGIN,
                                GuidStr = item.GUIDSTR
                            }

                        };
                        item.STATUS = item.STATUS == SendStatusInfo.NoSign ? SendStatusInfo.Sign : SendStatusInfo.SignVNACCS;
                        //item.Delete();
                        item.Update();
                    }
                    else
                    {
                        messageReturn = new MessageSend()
                        {
                            SendStatus = SendStatusInfo.Wait,
                            Warrning = "Chưa có thông tin yêu cầu"
                        };
                    }

                }
                else throw new Exception("Không xác định thông tin gửi");


            }
            catch (Exception ex)
            {
                messageReturn = new MessageSend()
                {
                    SendStatus = SendStatusInfo.Error,
                    Warrning = ex.Message
                };
            }
            return Serializer(messageReturn, Encoding.UTF8, true, true);

        }

        public string FileClient(MessageSendFile msgSend)
        {
            MessageSendFile messageReturn = null;
            try
            {


                if (msgSend.SendStatus == SendStatusInfo.Send || msgSend.SendStatus == SendStatusInfo.SendVNACCS)
                {
                    MSG_FILE_INBOX  FILE_INBOX= new MSG_FILE_INBOX()
                    {
                        MSG_FROM = msgSend.From,
                        MSG_TO = msgSend.To,
                        CREATE_TIME = DateTime.Now,
                        GUIDSTR = msgSend.Message.GuidStr,
                        STATUS = msgSend.SendStatus == SendStatusInfo.Send ? SendStatusInfo.NoSign : SendStatusInfo.NoSignVNACCS,
                        FILE_NAME = msgSend.Message.FileName,
                        FILE_DATA = msgSend.Message.FileData
                    };
                    FILE_INBOX.InsertUpdate();
                    messageReturn = new MessageSendFile()
                    {
                        Warrning = "Đã gửi thông tin",
                        SendStatus = SendStatusInfo.Wait
                    };
                }
                else if (msgSend.SendStatus == SendStatusInfo.Request || msgSend.SendStatus == SendStatusInfo.RequestVNACCS)
                {
                    List<MSG_FILE_INBOX> items = MSG_FILE_INBOX.SelectCollectionDynamic(" STATUS=" + SendStatusInfo.Sign + "", "");
                    if (items.Count == 0)
                        messageReturn = new MessageSendFile()
                        {
                            SendStatus = SendStatusInfo.Error,
                            Warrning = string.Format("Thông tin {0} chưa được gửi đến hệ thống", msgSend.Message.GuidStr)
                        };
                    else
                    {
                        string sfmtWhere = string.Format("MSG_TO='{0}' and  STATUS={1}", msgSend.From, SendStatusInfo.NoSign);
                        List<MSG_FILE_OUTBOX> outMessages = MSG_FILE_OUTBOX.SelectCollectionDynamic(sfmtWhere, "CREATE_TIME DESC");
                        if (outMessages.Count > 0)
                        {
                            MSG_FILE_OUTBOX item = outMessages[0];
                            if (item.STATUS == SendStatusInfo.NoSign || item.STATUS == SendStatusInfo.NoSignVNACCS)
                            {
                                messageReturn = new MessageSendFile()
                                {
                                    SendStatus = item.STATUS,
                                    From = item.MSG_FROM,
                                    To = item.MSG_TO,
                                    Message = new MessageFile()
                                    {
                                        FileName = item.FILE_NAME,
                                        FileData = item.FILE_DATA
                                    }
                                };
                                item.Delete();
                                items[0].Delete();
                            }
                            else messageReturn = new MessageSendFile()
                            {
                                SendStatus = SendStatusInfo.Wait,
                                Warrning = string.Format("Hệ thống chưa ký thông tin {0}", msgSend.Message.GuidStr)
                            };


                        }
                        else
                        {
                            messageReturn = new MessageSendFile()
                            {
                                SendStatus = SendStatusInfo.Wait,
                                Warrning = string.Format("Hệ thống chưa ký thông tin {0}", msgSend.Message.GuidStr)
                            };
                        }
                    }

                }
                else throw new Exception("Không xác định thông tin gửi");


            }
            catch (Exception ex)
            {
                messageReturn = new MessageSendFile()
                {
                    SendStatus = SendStatusInfo.Error,
                    Warrning = ex.Message
                };
            }
            return Serializer(messageReturn, Encoding.UTF8, true, true);

        }
        public string FileServer(MessageSendFile msgSend)
        {
            MessageSendFile messageReturn = null;
            try
            {


                if (msgSend.SendStatus == SendStatusInfo.Send || msgSend.SendStatus == SendStatusInfo.SendVNACCS)
                {
                    MSG_FILE_OUTBOX FILE_OUTBOX = new MSG_FILE_OUTBOX()
                    {
                        MSG_FROM = msgSend.From,
                        MSG_TO = msgSend.To,
                        CREATE_TIME = DateTime.Now,
                        GUIDSTR = msgSend.Message.GuidStr,
                        STATUS = msgSend.SendStatus == SendStatusInfo.Send ? SendStatusInfo.NoSign : SendStatusInfo.NoSignVNACCS,
                        FILE_NAME = msgSend.Message.FileName,
                        FILE_DATA = msgSend.Message.FileData
                    };
                    FILE_OUTBOX.InsertUpdate();
                    messageReturn = new MessageSendFile()
                    {
                        Warrning = "Đã gửi thông tin",
                        SendStatus = SendStatusInfo.Wait
                    };
                }
                else if (msgSend.SendStatus == SendStatusInfo.Request || msgSend.SendStatus == SendStatusInfo.RequestVNACCS)
                {
                    string sfmtWhere = string.Format("MSG_FROM='{0}' and ([STATUS]={1} OR [STATUS] = {2}) ", msgSend.From, SendStatusInfo.NoSign, SendStatusInfo.NoSignVNACCS);
                    List<MSG_FILE_INBOX> items = MSG_FILE_INBOX.SelectCollectionDynamic(sfmtWhere, "");
                    foreach (MSG_FILE_INBOX item in items)
                    {
                        if (item != null && (item.STATUS == SendStatusInfo.NoSign || item.STATUS == SendStatusInfo.NoSignVNACCS))
                        {
                            messageReturn = new MessageSendFile()
                            {
                                SendStatus = item.STATUS,
                                From = item.MSG_FROM,
                                To = item.MSG_TO,
                                Message = new MessageFile()
                                {
                                    FileName = item.FILE_NAME,
                                    FileData = item.FILE_DATA
                                }

                            };
                            item.STATUS = item.STATUS == SendStatusInfo.NoSign ? SendStatusInfo.Sign : SendStatusInfo.SignVNACCS;
                            item.Update();
                        }
                        else
                        {
                            messageReturn = new MessageSendFile()
                            {
                                SendStatus = SendStatusInfo.Wait,
                                Warrning = "Chưa có thông tin yêu cầu"
                            };
                        }   
                    }

                }
                else throw new Exception("Không xác định thông tin gửi");


            }
            catch (Exception ex)
            {
                messageReturn = new MessageSendFile()
                {
                    SendStatus = SendStatusInfo.Error,
                    Warrning = ex.Message
                };
            }
            return Serializer(messageReturn, Encoding.UTF8, true, true);

        }
    }
}
