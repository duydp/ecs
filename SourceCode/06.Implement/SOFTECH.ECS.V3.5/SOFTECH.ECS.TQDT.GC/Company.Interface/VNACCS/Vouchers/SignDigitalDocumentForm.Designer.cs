﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class SignDigitalDocumentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListFileSign_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignDigitalDocumentForm));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkFolder = new System.Windows.Forms.CheckBox();
            this.btnFolderSigned = new Janus.Windows.EditControls.UIButton();
            this.btnFolderSign = new Janus.Windows.EditControls.UIButton();
            this.txtFolderSigned = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtFolderSign = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkHDSD = new System.Windows.Forms.LinkLabel();
            this.btnRefresh = new Janus.Windows.EditControls.UIButton();
            this.btnOpenFolder = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnSign = new Janus.Windows.EditControls.UIButton();
            this.btnCheckConnect = new Janus.Windows.EditControls.UIButton();
            this.txtPassword = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chIsRemember = new Janus.Windows.EditControls.UICheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkIsUseSign = new Janus.Windows.EditControls.UICheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblNhaCungCap = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbSigns = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListFileSign = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListFileSign)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Size = new System.Drawing.Size(741, 559);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.chkFolder);
            this.uiGroupBox3.Controls.Add(this.btnFolderSigned);
            this.uiGroupBox3.Controls.Add(this.btnFolderSign);
            this.uiGroupBox3.Controls.Add(this.txtFolderSigned);
            this.uiGroupBox3.Controls.Add(this.txtFolderSign);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(741, 123);
            this.uiGroupBox3.TabIndex = 4;
            this.uiGroupBox3.Text = "Thông tin cấu hình Thư mục ký File";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // chkFolder
            // 
            this.chkFolder.AutoSize = true;
            this.chkFolder.Checked = true;
            this.chkFolder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFolder.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFolder.Location = new System.Drawing.Point(187, 80);
            this.chkFolder.Name = "chkFolder";
            this.chkFolder.Size = new System.Drawing.Size(86, 17);
            this.chkFolder.TabIndex = 5;
            this.chkFolder.Text = "Lưu thư mục";
            this.chkFolder.UseVisualStyleBackColor = true;
            // 
            // btnFolderSigned
            // 
            this.btnFolderSigned.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFolderSigned.Image = ((System.Drawing.Image)(resources.GetObject("btnFolderSigned.Image")));
            this.btnFolderSigned.ImageSize = new System.Drawing.Size(20, 20);
            this.btnFolderSigned.Location = new System.Drawing.Point(627, 53);
            this.btnFolderSigned.Name = "btnFolderSigned";
            this.btnFolderSigned.Size = new System.Drawing.Size(75, 22);
            this.btnFolderSigned.TabIndex = 6;
            this.btnFolderSigned.Text = "Chọn";
            this.btnFolderSigned.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnFolderSigned.Click += new System.EventHandler(this.btnFolderSigned_Click);
            // 
            // btnFolderSign
            // 
            this.btnFolderSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFolderSign.Image = ((System.Drawing.Image)(resources.GetObject("btnFolderSign.Image")));
            this.btnFolderSign.ImageSize = new System.Drawing.Size(20, 20);
            this.btnFolderSign.Location = new System.Drawing.Point(627, 24);
            this.btnFolderSign.Name = "btnFolderSign";
            this.btnFolderSign.Size = new System.Drawing.Size(75, 22);
            this.btnFolderSign.TabIndex = 6;
            this.btnFolderSign.Text = "Chọn";
            this.btnFolderSign.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnFolderSign.Click += new System.EventHandler(this.btnFolderSign_Click);
            // 
            // txtFolderSigned
            // 
            this.txtFolderSigned.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolderSigned.Location = new System.Drawing.Point(187, 53);
            this.txtFolderSigned.Name = "txtFolderSigned";
            this.txtFolderSigned.Size = new System.Drawing.Size(434, 21);
            this.txtFolderSigned.TabIndex = 3;
            this.txtFolderSigned.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtFolderSign
            // 
            this.txtFolderSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolderSign.Location = new System.Drawing.Point(187, 25);
            this.txtFolderSign.Name = "txtFolderSign";
            this.txtFolderSign.Size = new System.Drawing.Size(434, 21);
            this.txtFolderSign.TabIndex = 1;
            this.txtFolderSign.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(170, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Chọn thư mục chứa File sau khi ký";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chọn thư mục chứa File cần ký";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(185, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(482, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Phần mềm hỗ trợ ký File có định dạng .docx hoặc .xlsx (Office 2007 trở lên) hoặc " +
                ".pdf ";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.linkHDSD);
            this.uiGroupBox6.Controls.Add(this.btnRefresh);
            this.uiGroupBox6.Controls.Add(this.btnOpenFolder);
            this.uiGroupBox6.Controls.Add(this.uiButton1);
            this.uiGroupBox6.Controls.Add(this.btnSign);
            this.uiGroupBox6.Controls.Add(this.btnCheckConnect);
            this.uiGroupBox6.Controls.Add(this.txtPassword);
            this.uiGroupBox6.Controls.Add(this.chIsRemember);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.chkIsUseSign);
            this.uiGroupBox6.Controls.Add(this.label9);
            this.uiGroupBox6.Controls.Add(this.label8);
            this.uiGroupBox6.Controls.Add(this.lblNhaCungCap);
            this.uiGroupBox6.Controls.Add(this.label2);
            this.uiGroupBox6.Controls.Add(this.label4);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.cbSigns);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 123);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(741, 272);
            this.uiGroupBox6.TabIndex = 5;
            this.uiGroupBox6.Text = "Thông tin chữ ký số";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // linkHDSD
            // 
            this.linkHDSD.AutoSize = true;
            this.linkHDSD.Location = new System.Drawing.Point(182, 224);
            this.linkHDSD.Name = "linkHDSD";
            this.linkHDSD.Size = new System.Drawing.Size(291, 13);
            this.linkHDSD.TabIndex = 8;
            this.linkHDSD.TabStop = true;
            this.linkHDSD.Text = "Hướng dẫn Sử dụng Ký chữ ký số cho File đính kèm";
            this.linkHDSD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHDSD_LinkClicked);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.ImageSize = new System.Drawing.Size(20, 20);
            this.btnRefresh.Location = new System.Drawing.Point(481, 43);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 22);
            this.btnRefresh.TabIndex = 6;
            this.btnRefresh.Text = "Làm mới";
            this.btnRefresh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnOpenFolder
            // 
            this.btnOpenFolder.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenFolder.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFolder.Image")));
            this.btnOpenFolder.ImageSize = new System.Drawing.Size(20, 20);
            this.btnOpenFolder.Location = new System.Drawing.Point(266, 169);
            this.btnOpenFolder.Name = "btnOpenFolder";
            this.btnOpenFolder.Size = new System.Drawing.Size(209, 22);
            this.btnOpenFolder.TabIndex = 6;
            this.btnOpenFolder.Text = "Mở thư mục chứa File đã ký";
            this.btnOpenFolder.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnOpenFolder.Click += new System.EventHandler(this.btnOpenFolder_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton1.Location = new System.Drawing.Point(187, 244);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 22);
            this.uiButton1.TabIndex = 6;
            this.uiButton1.Text = "Nén File";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // btnSign
            // 
            this.btnSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSign.Image = ((System.Drawing.Image)(resources.GetObject("btnSign.Image")));
            this.btnSign.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSign.Location = new System.Drawing.Point(185, 169);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(75, 22);
            this.btnSign.TabIndex = 6;
            this.btnSign.Text = "Ký";
            this.btnSign.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // btnCheckConnect
            // 
            this.btnCheckConnect.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckConnect.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckConnect.Image")));
            this.btnCheckConnect.ImageSize = new System.Drawing.Size(20, 20);
            this.btnCheckConnect.Location = new System.Drawing.Point(481, 98);
            this.btnCheckConnect.Name = "btnCheckConnect";
            this.btnCheckConnect.Size = new System.Drawing.Size(75, 22);
            this.btnCheckConnect.TabIndex = 6;
            this.btnCheckConnect.Text = "Kiểm tra";
            this.btnCheckConnect.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCheckConnect.Click += new System.EventHandler(this.btnCheckConnect_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(185, 99);
            this.txtPassword.MaxLength = 100;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(290, 21);
            this.txtPassword.TabIndex = 5;
            this.txtPassword.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPassword.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chIsRemember
            // 
            this.chIsRemember.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsRemember.Location = new System.Drawing.Point(185, 143);
            this.chIsRemember.Name = "chIsRemember";
            this.chIsRemember.Size = new System.Drawing.Size(116, 18);
            this.chIsRemember.TabIndex = 7;
            this.chIsRemember.Text = "Ghi nhớ mật khẩu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mã PIN :";
            // 
            // chkIsUseSign
            // 
            this.chkIsUseSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsUseSign.Location = new System.Drawing.Point(185, 20);
            this.chkIsUseSign.Name = "chkIsUseSign";
            this.chkIsUseSign.Size = new System.Drawing.Size(128, 18);
            this.chkIsUseSign.TabIndex = 1;
            this.chkIsUseSign.Text = "Sử dụng chữ ký số";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(182, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(520, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Hướng dẫn : Click đôi danh sách File ở phía dưới để Xem File trong thư mục chứa F" +
                "ile cần ký .";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(184, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(449, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Sau khi nhập Mã PIN phải nhấn nút Kiểm tra để kiểm tra Mã PIN của Chữ ký số .";
            // 
            // lblNhaCungCap
            // 
            this.lblNhaCungCap.AutoSize = true;
            this.lblNhaCungCap.BackColor = System.Drawing.Color.Transparent;
            this.lblNhaCungCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhaCungCap.ForeColor = System.Drawing.Color.Red;
            this.lblNhaCungCap.Location = new System.Drawing.Point(185, 74);
            this.lblNhaCungCap.Name = "lblNhaCungCap";
            this.lblNhaCungCap.Size = new System.Drawing.Size(86, 13);
            this.lblNhaCungCap.TabIndex = 3;
            this.lblNhaCungCap.Text = "Chưa xác định";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nhà cung cấp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Cho phép";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Chữ ký số";
            // 
            // cbSigns
            // 
            this.cbSigns.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbSigns.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSigns.Location = new System.Drawing.Point(185, 44);
            this.cbSigns.Name = "cbSigns";
            this.cbSigns.Size = new System.Drawing.Size(290, 21);
            this.cbSigns.TabIndex = 2;
            this.cbSigns.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbSigns.SelectedValueChanged += new System.EventHandler(this.cbSigns_SelectedValueChanged);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgListFileSign);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 395);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(741, 164);
            this.uiGroupBox2.TabIndex = 6;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // dgListFileSign
            // 
            this.dgListFileSign.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListFileSign.ColumnAutoResize = true;
            dgListFileSign_DesignTimeLayout.LayoutString = resources.GetString("dgListFileSign_DesignTimeLayout.LayoutString");
            this.dgListFileSign.DesignTimeLayout = dgListFileSign_DesignTimeLayout;
            this.dgListFileSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListFileSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListFileSign.GroupByBoxVisible = false;
            this.dgListFileSign.Location = new System.Drawing.Point(3, 8);
            this.dgListFileSign.Name = "dgListFileSign";
            this.dgListFileSign.RecordNavigator = true;
            this.dgListFileSign.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListFileSign.Size = new System.Drawing.Size(735, 153);
            this.dgListFileSign.TabIndex = 23;
            this.dgListFileSign.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListFileSign.VisualStyleManager = this.vsmMain;
            this.dgListFileSign.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListFileSign_RowDoubleClick);
            // 
            // SignDigitalDocumentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 559);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "SignDigitalDocumentForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Ký chữ ký số cho File đính kèm";
            this.Load += new System.EventHandler(this.SignDigitalDocumentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListFileSign)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.CheckBox chkFolder;
        private Janus.Windows.GridEX.EditControls.EditBox txtFolderSigned;
        private Janus.Windows.GridEX.EditControls.EditBox txtFolderSign;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnRefresh;
        private Janus.Windows.EditControls.UIButton btnCheckConnect;
        private Janus.Windows.GridEX.EditControls.EditBox txtPassword;
        private Janus.Windows.EditControls.UICheckBox chIsRemember;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UICheckBox chkIsUseSign;
        private System.Windows.Forms.Label lblNhaCungCap;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbSigns;
        private Janus.Windows.EditControls.UIButton btnFolderSigned;
        private Janus.Windows.EditControls.UIButton btnFolderSign;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgListFileSign;
        private Janus.Windows.EditControls.UIButton btnOpenFolder;
        private Janus.Windows.EditControls.UIButton btnSign;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.LinkLabel linkHDSD;
        private Janus.Windows.EditControls.UIButton uiButton1;
    }
}