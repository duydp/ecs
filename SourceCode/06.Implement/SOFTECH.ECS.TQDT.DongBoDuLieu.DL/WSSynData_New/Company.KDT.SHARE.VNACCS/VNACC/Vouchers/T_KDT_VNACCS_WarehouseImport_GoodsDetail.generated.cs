﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.GC.BLL.KDT.GC
{
	public partial class T_KDT_VNACCS_WarehouseImport_GoodsDetail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long WarehouseImport_Details_ID { set; get; }
		public decimal STT { set; get; }
		public string TenHangHoa { set; get; }
		public string MaHangHoa { set; get; }
		public decimal LoaiHangHoa { set; get; }
		public string MaDinhDanhSX { set; get; }
		public decimal NguonNhap { set; get; }
		public decimal SoLuongDuKienNhap { set; get; }
		public decimal SoLuongThucNhap { set; get; }
		public string DVT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> collection = new List<T_KDT_VNACCS_WarehouseImport_GoodsDetail>();
			while (reader.Read())
			{
				T_KDT_VNACCS_WarehouseImport_GoodsDetail entity = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("WarehouseImport_Details_ID"))) entity.WarehouseImport_Details_ID = reader.GetInt64(reader.GetOrdinal("WarehouseImport_Details_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetDecimal(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangHoa"))) entity.TenHangHoa = reader.GetString(reader.GetOrdinal("TenHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.MaHangHoa = reader.GetString(reader.GetOrdinal("MaHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetDecimal(reader.GetOrdinal("LoaiHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanhSX"))) entity.MaDinhDanhSX = reader.GetString(reader.GetOrdinal("MaDinhDanhSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguonNhap"))) entity.NguonNhap = reader.GetDecimal(reader.GetOrdinal("NguonNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDuKienNhap"))) entity.SoLuongDuKienNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDuKienNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongThucNhap"))) entity.SoLuongThucNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongThucNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        protected static List<CustomsGoodsItem> ConvertToCollectionNew(IDataReader reader)
        {
            List<CustomsGoodsItem> collection = new List<CustomsGoodsItem>();
            int i = 0;
            while (reader.Read())
            {
                //DeclarationDocument add = new DeclarationDocument()
                //{
                //    Type="2",
                //    Reference="CTXK01001",
                //    //Reference = warehouseImport.SoTKChungTu.ToString(),
                //    //Type = warehouseImport.Loai.ToString()
                //};                
                CustomsGoodsItem entity = new CustomsGoodsItem()
                {
                    Commodity = new Commodity(),
                    GoodsMeasure = new GoodsMeasure(),
                    DeclarationDocument = new DeclarationDocument()

                };
                entity.DeclarationDocument.Reference = "CTXK01001";
                entity.DeclarationDocument.Type = "2";
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.Commodity.Sequence = i++.ToString(); //reader.GetDecimal(reader.GetOrdinal("STT")).ToString();
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangHoa"))) entity.Commodity.Description = reader.GetString(reader.GetOrdinal("TenHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.Commodity.Identification = reader.GetString(reader.GetOrdinal("MaHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.Commodity.Type = reader.GetDecimal(reader.GetOrdinal("LoaiHangHoa")).ToString();
                if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanhSX"))) entity.Commodity.ProductCtrlNo = reader.GetString(reader.GetOrdinal("MaDinhDanhSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguonNhap"))) entity.Commodity.Origin = reader.GetDecimal(reader.GetOrdinal("NguonNhap")).ToString();
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDuKienNhap"))) entity.GoodsMeasure.DocQuantity = Decimal.Round(reader.GetDecimal(reader.GetOrdinal("SoLuongDuKienNhap")), 4, MidpointRounding.AwayFromZero).ToString().Replace(",", ".");
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongThucNhap"))) entity.GoodsMeasure.ActualQuantity = Decimal.Round(reader.GetDecimal(reader.GetOrdinal("SoLuongThucNhap")), 4, MidpointRounding.AwayFromZero).ToString().Replace(",", ".");
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.GoodsMeasure.MeasureUnit = reader.GetString(reader.GetOrdinal("DVT"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        protected static List<PNK_CustomsGoodsItem> ConvertToCollectionUpdate(IDataReader reader, T_KDT_VNACCS_WarehouseImport warehouseImport)
        {
            List<PNK_CustomsGoodsItem> collection = new List<PNK_CustomsGoodsItem>();
            int i = 1;
            while (reader.Read())
            {
                PNK_CustomsGoodsItem entity = new PNK_CustomsGoodsItem()
                {
                    Commodity = new PNK_Commodity(),
                    GoodsMeasure = new PNK_GoodsMeasure(),
                    DeclarationDocument = new PNK_DeclarationDocument(),
                    ContractReference = new PNK_ContractReference()
                };
                entity.DeclarationDocument.Reference = warehouseImport.SoTKChungTu.ToString();
                entity.DeclarationDocument.Type = warehouseImport.Loai.ToString();
#if GC_V4
                entity.ContractReference.Reference = warehouseImport.SoHopDong.ToString();
                entity.ContractReference.Issue = warehouseImport.NgayHopDong.ToString("yyyy-MM-dd");
                entity.ContractReference.DeclarationOffice = warehouseImport.MaHQTiepNhanHD;
                entity.ContractReference.Expire = warehouseImport.NgayHetHanHD.ToString("yyyy-MM-dd");
#endif
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.Commodity.Sequence = reader.GetDecimal(reader.GetOrdinal("STT")).ToString();
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangHoa"))) entity.Commodity.Description = reader.GetString(reader.GetOrdinal("TenHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.Commodity.Identification = reader.GetString(reader.GetOrdinal("MaHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.Commodity.Type = reader.GetDecimal(reader.GetOrdinal("LoaiHangHoa")).ToString();
                if (!reader.IsDBNull(reader.GetOrdinal("MaDinhDanhSX"))) entity.Commodity.ProductCtrlNo = reader.GetString(reader.GetOrdinal("MaDinhDanhSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguonNhap"))) entity.Commodity.Origin = reader.GetDecimal(reader.GetOrdinal("NguonNhap")).ToString();
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDuKienNhap"))) entity.GoodsMeasure.DocQuantity = Decimal.Round(reader.GetDecimal(reader.GetOrdinal("SoLuongDuKienNhap")), 4, MidpointRounding.AwayFromZero).ToString().Replace(",", ".");
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongThucNhap"))) entity.GoodsMeasure.ActualQuantity = Decimal.Round(reader.GetDecimal(reader.GetOrdinal("SoLuongThucNhap")), 4, MidpointRounding.AwayFromZero).ToString().Replace(",", ".");
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.GoodsMeasure.MeasureUnit = VNACCS_Mapper.GetCodeVNACC(reader.GetString(reader.GetOrdinal("DVT")));
                i++;
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
		public static bool Find(List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> collection, long id)
        {
            foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_VNACCS_WarehouseImport_GoodsDetails VALUES(@WarehouseImport_Details_ID, @STT, @TenHangHoa, @MaHangHoa, @LoaiHangHoa, @MaDinhDanhSX, @NguonNhap, @SoLuongDuKienNhap, @SoLuongThucNhap, @DVT)";
            string update = "UPDATE T_KDT_VNACCS_WarehouseImport_GoodsDetails SET WarehouseImport_Details_ID = @WarehouseImport_Details_ID, STT = @STT, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, LoaiHangHoa = @LoaiHangHoa, MaDinhDanhSX = @MaDinhDanhSX, NguonNhap = @NguonNhap, SoLuongDuKienNhap = @SoLuongDuKienNhap, SoLuongThucNhap = @SoLuongThucNhap, DVT = @DVT WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_VNACCS_WarehouseImport_GoodsDetails WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, "WarehouseImport_Details_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangHoa", SqlDbType.Decimal, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDinhDanhSX", SqlDbType.NVarChar, "MaDinhDanhSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguonNhap", SqlDbType.Decimal, "NguonNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDuKienNhap", SqlDbType.Decimal, "SoLuongDuKienNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThucNhap", SqlDbType.Decimal, "SoLuongThucNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, "WarehouseImport_Details_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangHoa", SqlDbType.Decimal, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDinhDanhSX", SqlDbType.NVarChar, "MaDinhDanhSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguonNhap", SqlDbType.Decimal, "NguonNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDuKienNhap", SqlDbType.Decimal, "SoLuongDuKienNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThucNhap", SqlDbType.Decimal, "SoLuongThucNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_VNACCS_WarehouseImport_GoodsDetails VALUES(@WarehouseImport_Details_ID, @STT, @TenHangHoa, @MaHangHoa, @LoaiHangHoa, @MaDinhDanhSX, @NguonNhap, @SoLuongDuKienNhap, @SoLuongThucNhap, @DVT)";
            string update = "UPDATE T_KDT_VNACCS_WarehouseImport_GoodsDetails SET WarehouseImport_Details_ID = @WarehouseImport_Details_ID, STT = @STT, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, LoaiHangHoa = @LoaiHangHoa, MaDinhDanhSX = @MaDinhDanhSX, NguonNhap = @NguonNhap, SoLuongDuKienNhap = @SoLuongDuKienNhap, SoLuongThucNhap = @SoLuongThucNhap, DVT = @DVT WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_VNACCS_WarehouseImport_GoodsDetails WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, "WarehouseImport_Details_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHangHoa", SqlDbType.Decimal, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDinhDanhSX", SqlDbType.NVarChar, "MaDinhDanhSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguonNhap", SqlDbType.Decimal, "NguonNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDuKienNhap", SqlDbType.Decimal, "SoLuongDuKienNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThucNhap", SqlDbType.Decimal, "SoLuongThucNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, "WarehouseImport_Details_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHangHoa", SqlDbType.Decimal, "LoaiHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDinhDanhSX", SqlDbType.NVarChar, "MaDinhDanhSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguonNhap", SqlDbType.Decimal, "NguonNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDuKienNhap", SqlDbType.Decimal, "SoLuongDuKienNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThucNhap", SqlDbType.Decimal, "SoLuongThucNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_VNACCS_WarehouseImport_GoodsDetail Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> SelectCollectionBy_WarehouseImport_Details_ID(long warehouseImport_Details_ID)
		{
            IDataReader reader = SelectReaderBy_WarehouseImport_Details_ID(warehouseImport_Details_ID);
			return ConvertToCollection(reader);	
		}
        public static List<CustomsGoodsItem> SelectCollectionBy_WarehouseImport_Details_ID_New(long warehouseImport_Details_ID)
        {
            IDataReader reader = SelectReaderBy_WarehouseImport_Details_ID(warehouseImport_Details_ID);
            return ConvertToCollectionNew(reader);
        }
        public static List<PNK_CustomsGoodsItem> SelectCollectionBy_WarehouseImport_Details_ID_Update(long warehouseImport_Details_ID, T_KDT_VNACCS_WarehouseImport warehouseImport)
        {
            IDataReader reader = SelectReaderBy_WarehouseImport_Details_ID(warehouseImport_Details_ID);
            return ConvertToCollectionUpdate(reader,warehouseImport);
        }
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_WarehouseImport_Details_ID(long warehouseImport_Details_ID)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectBy_WarehouseImport_Details_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, warehouseImport_Details_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_WarehouseImport_Details_ID(long warehouseImport_Details_ID)
		{
			const string spName = "p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectBy_WarehouseImport_Details_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, warehouseImport_Details_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_VNACCS_WarehouseImport_GoodsDetail(long warehouseImport_Details_ID, decimal sTT, string tenHangHoa, string maHangHoa, decimal loaiHangHoa, string maDinhDanhSX, decimal nguonNhap, decimal soLuongDuKienNhap, decimal soLuongThucNhap, string dVT)
		{
			T_KDT_VNACCS_WarehouseImport_GoodsDetail entity = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();	
			entity.WarehouseImport_Details_ID = warehouseImport_Details_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.MaDinhDanhSX = maDinhDanhSX;
			entity.NguonNhap = nguonNhap;
			entity.SoLuongDuKienNhap = soLuongDuKienNhap;
			entity.SoLuongThucNhap = soLuongThucNhap;
			entity.DVT = dVT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, WarehouseImport_Details_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Decimal, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@MaDinhDanhSX", SqlDbType.NVarChar, MaDinhDanhSX);
			db.AddInParameter(dbCommand, "@NguonNhap", SqlDbType.Decimal, NguonNhap);
			db.AddInParameter(dbCommand, "@SoLuongDuKienNhap", SqlDbType.Decimal, SoLuongDuKienNhap);
			db.AddInParameter(dbCommand, "@SoLuongThucNhap", SqlDbType.Decimal, SoLuongThucNhap);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_VNACCS_WarehouseImport_GoodsDetail(long id, long warehouseImport_Details_ID, decimal sTT, string tenHangHoa, string maHangHoa, decimal loaiHangHoa, string maDinhDanhSX, decimal nguonNhap, decimal soLuongDuKienNhap, decimal soLuongThucNhap, string dVT)
		{
			T_KDT_VNACCS_WarehouseImport_GoodsDetail entity = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();			
			entity.ID = id;
			entity.WarehouseImport_Details_ID = warehouseImport_Details_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.MaDinhDanhSX = maDinhDanhSX;
			entity.NguonNhap = nguonNhap;
			entity.SoLuongDuKienNhap = soLuongDuKienNhap;
			entity.SoLuongThucNhap = soLuongThucNhap;
			entity.DVT = dVT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, WarehouseImport_Details_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Decimal, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@MaDinhDanhSX", SqlDbType.NVarChar, MaDinhDanhSX);
			db.AddInParameter(dbCommand, "@NguonNhap", SqlDbType.Decimal, NguonNhap);
			db.AddInParameter(dbCommand, "@SoLuongDuKienNhap", SqlDbType.Decimal, SoLuongDuKienNhap);
			db.AddInParameter(dbCommand, "@SoLuongThucNhap", SqlDbType.Decimal, SoLuongThucNhap);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_VNACCS_WarehouseImport_GoodsDetail(long id, long warehouseImport_Details_ID, decimal sTT, string tenHangHoa, string maHangHoa, decimal loaiHangHoa, string maDinhDanhSX, decimal nguonNhap, decimal soLuongDuKienNhap, decimal soLuongThucNhap, string dVT)
		{
			T_KDT_VNACCS_WarehouseImport_GoodsDetail entity = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();			
			entity.ID = id;
			entity.WarehouseImport_Details_ID = warehouseImport_Details_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.LoaiHangHoa = loaiHangHoa;
			entity.MaDinhDanhSX = maDinhDanhSX;
			entity.NguonNhap = nguonNhap;
			entity.SoLuongDuKienNhap = soLuongDuKienNhap;
			entity.SoLuongThucNhap = soLuongThucNhap;
			entity.DVT = dVT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, WarehouseImport_Details_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Decimal, LoaiHangHoa);
			db.AddInParameter(dbCommand, "@MaDinhDanhSX", SqlDbType.NVarChar, MaDinhDanhSX);
			db.AddInParameter(dbCommand, "@NguonNhap", SqlDbType.Decimal, NguonNhap);
			db.AddInParameter(dbCommand, "@SoLuongDuKienNhap", SqlDbType.Decimal, SoLuongDuKienNhap);
			db.AddInParameter(dbCommand, "@SoLuongThucNhap", SqlDbType.Decimal, SoLuongThucNhap);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_VNACCS_WarehouseImport_GoodsDetail(long id)
		{
			T_KDT_VNACCS_WarehouseImport_GoodsDetail entity = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_WarehouseImport_Details_ID(long warehouseImport_Details_ID)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteBy_WarehouseImport_Details_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@WarehouseImport_Details_ID", SqlDbType.BigInt, warehouseImport_Details_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseImport_GoodsDetail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}