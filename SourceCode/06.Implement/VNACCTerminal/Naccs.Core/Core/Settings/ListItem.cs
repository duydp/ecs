﻿namespace Naccs.Core.Settings
{
    using System;
    using System.Threading;

    public class ListItem
    {
        private EventHandler _OnChange;

        public event EventHandler OnChange
        {
            add
            {
                EventHandler handler2;
                EventHandler onChange = this._OnChange;
                do
                {
                    handler2 = onChange;
                    EventHandler handler3 = (EventHandler) Delegate.Combine(handler2, value);
                    onChange = Interlocked.CompareExchange<EventHandler>(ref this._OnChange, handler3, handler2);
                }
                while (onChange != handler2);
            }
            remove
            {
                EventHandler handler2;
                EventHandler onChange = this._OnChange;
                do
                {
                    handler2 = onChange;
                    EventHandler handler3 = (EventHandler) Delegate.Remove(handler2, value);
                    onChange = Interlocked.CompareExchange<EventHandler>(ref this._OnChange, handler3, handler2);
                }
                while (onChange != handler2);
            }
        }

        public void Change()
        {
            if (this._OnChange != null)
            {
                this._OnChange(this, new EventArgs());
            }
        }
    }
}

