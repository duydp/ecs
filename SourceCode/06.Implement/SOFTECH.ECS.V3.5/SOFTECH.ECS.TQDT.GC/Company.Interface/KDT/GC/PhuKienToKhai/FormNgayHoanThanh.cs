﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class FormNgayHoanThanh : BaseForm
    {
        public KDT_SXXK_LoaiPhuKien loaiPK = new KDT_SXXK_LoaiPhuKien();
        public KDT_SXXK_PhuKienDetail pkdt = new KDT_SXXK_PhuKienDetail();
       

        public DateTime NgayHoanThanh;
        public FormNgayHoanThanh()
        {
            InitializeComponent();
        }
        bool isAdd = false;
        private void FormNgayHoanThanh_Load(object sender, EventArgs e)
        {
            if (loaiPK.ListPhuKienDeTail.Count > 0)
            {
                pkdt = loaiPK.ListPhuKienDeTail[0];
            }
            if (pkdt != null && pkdt.ID > 0)
            {
                clcNgayHoanThanhCu.Value = pkdt.NgayTKHeThong;
                clcNgayHoanThanHMoi.Value = pkdt.NgayTKSuaDoi;
                isAdd = false;
            }
            else
            {
                //clcNgayHoanThanhCu.Value = NgayHoanThanh;
                isAdd = true;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                pkdt.NgayTKHeThong = clcNgayHoanThanhCu.Value;
                pkdt.NgayTKSuaDoi = clcNgayHoanThanHMoi.Value;
                if (isAdd)
                {
                    pkdt.Master_ID = loaiPK.ID;
                    pkdt.Insert();
                    loaiPK.ListPhuKienDeTail.Add(pkdt);
                }
                else
                {
                    pkdt.Update();
                }
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                
            }
          
        }
     
    }
}
