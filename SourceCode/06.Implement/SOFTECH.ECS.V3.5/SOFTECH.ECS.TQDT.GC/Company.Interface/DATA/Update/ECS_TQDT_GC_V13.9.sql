  ALTER TABLE t_KDT_VNACC_ToKhaiMauDich
   alter column SoToKhaiDauTien varchar(12)
           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '13.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('13.9', GETDATE(), N'Cap nhat table t_KDT_VNACC_ToKhaiMauDich,t_KDT_VNACC_HangMauDich')
END	
