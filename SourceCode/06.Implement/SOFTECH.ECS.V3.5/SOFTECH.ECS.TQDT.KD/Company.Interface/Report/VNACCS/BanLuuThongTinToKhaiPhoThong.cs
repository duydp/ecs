using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanLuuThongTinToKhaiPhoThong : DevExpress.XtraReports.UI.XtraReport
    {
        public BanLuuThongTinToKhaiPhoThong()
        {
            InitializeComponent();
        }

        public void BindingReport(VAL0020 val0020)
        {
            lblSoToKhai.Text = val0020.JNO.GetValue().ToString();
            lblPhanLoaiKhaiBao.Text = val0020.TSB.GetValue().ToString();
            lblTenThuTucKB.Text = val0020.TNM.GetValue().ToString();
            lblCoQuanHQ.Text = val0020.CH.GetValue().ToString();
            lblNhomXuLyHS.Text = val0020.CHB.GetValue().ToString();
            if (Convert.ToDateTime(val0020.YMD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(val0020.YMD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            //lblNgayKhaiBao.Text = Convert.ToDateTime(val0020.YMD.GetValue()).ToString("dd/MM/yyyy");
            lblTenNguoiKB.Text = val0020.SNM.GetValue().ToString();
            lblDiaChiNguoiKB.Text = val0020.SAD.GetValue().ToString();
            lblSoDienThoaiNguoiKB.Text = val0020.STL.GetValue().ToString();
            lblSoQuanLyNoiBo.Text = val0020.REF.GetValue().ToString();
            lblGhiChu.Text = val0020.KIJ.GetValue().ToString();


        }
    }
}
