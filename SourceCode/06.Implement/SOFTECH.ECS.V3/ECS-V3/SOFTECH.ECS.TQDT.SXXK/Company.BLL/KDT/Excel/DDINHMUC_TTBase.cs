using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.ExportToExcel
{
	public partial class DDINHMUC_TT 
	{
		#region Private members.
		
		protected string _MA_SP = String.Empty;
		protected string _MA_DV = String.Empty;
		protected string _MA_HQ = String.Empty;
		protected int _SO_DM;
		protected DateTime _NGAY_DK = new DateTime(1900, 01, 01);
		protected DateTime _NGAY_AD = new DateTime(1900, 01, 01);
		protected DateTime _NGAY_HH = new DateTime(1900, 01, 01);
		protected int _THANH_LY;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public string MA_SP
		{
			set {this._MA_SP = value;}
			get {return this._MA_SP;}
		}
		public string MA_DV
		{
			set {this._MA_DV = value;}
			get {return this._MA_DV;}
		}
		public string MA_HQ
		{
			set {this._MA_HQ = value;}
			get {return this._MA_HQ;}
		}
		public int SO_DM
		{
			set {this._SO_DM = value;}
			get {return this._SO_DM;}
		}
		public DateTime NGAY_DK
		{
			set {this._NGAY_DK = value;}
			get {return this._NGAY_DK;}
		}
		public DateTime NGAY_AD
		{
			set {this._NGAY_AD = value;}
			get {return this._NGAY_AD;}
		}
		public DateTime NGAY_HH
		{
			set {this._NGAY_HH = value;}
			get {return this._NGAY_HH;}
		}
		public int THANH_LY
		{
			set {this._THANH_LY = value;}
			get {return this._THANH_LY;}
		}
		
		//---------------------------------------------------------------------------------------------

		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		
		//---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string maHaiQuan, string maDoanhNghiep)
        {
            string sql = "SELECT * FROM t_SXXK_ThongTinDinhMuc  WHERE MaHaiQuan = @MaHQ AND MaDoanhNghiep =@MaDV";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, maDoanhNghiep);

            return db.ExecuteDataSet(dbCommand);
        }
		
		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------

	}	
}