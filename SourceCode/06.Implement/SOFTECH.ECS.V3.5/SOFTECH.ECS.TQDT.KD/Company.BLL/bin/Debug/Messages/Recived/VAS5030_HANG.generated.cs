using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAS5030_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute DA { get; set; }
public PropertiesAttribute DB { get; set; }
public PropertiesAttribute DC { get; set; }
public GroupAttribute DD { get; set; }

public VAS5030_HANG()
        {
DA = new PropertiesAttribute(3, typeof(string));
DB = new PropertiesAttribute(17, typeof(string));
DC = new PropertiesAttribute(5, typeof(string));
#region DD
List<PropertiesAttribute> listDD = new List<PropertiesAttribute>();
listDD.Add(new PropertiesAttribute(15, 6, EnumGroupID.VAS5030_HANG_DD1, typeof(string)));
DD = new GroupAttribute("DD", 6, listDD);
#endregion DD
TongSoByte = 133;
}

}public partial class EnumGroupID
    {
public static readonly string VAS5030_HANG_DD1 = "VAS5030_HANG_DD1";
}

}
