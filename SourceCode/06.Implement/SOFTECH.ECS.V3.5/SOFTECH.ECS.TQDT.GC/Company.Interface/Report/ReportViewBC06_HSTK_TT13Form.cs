﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;
using Company.GC.BLL.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC06_HSTK_TT13Form : BaseForm
    {
        private BangKe06_HSTK_TT13 BC06;
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        // Bổ sung NPL Tái xuất
        //Table NPL tái xuất
        public DataTable dtTaiXuat = new DataTable();
        public XRLabel Label;
        public ReportViewBC06_HSTK_TT13Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            Label = new XRLabel();

            BC06 = new BangKe06_HSTK_TT13();
            this.dsBK.Tables[0].Columns.Add("TongLuongXKTemp", typeof(decimal));
            this.dsBK.Tables[0].Columns.Add("MaPhu", typeof(string));
            this.dsBK.Tables[0].Columns.Add("TongXuatTra", typeof(decimal));
            //ThongTu113
            // Bổ sung lượng NPL tái xuất
            

           // this.dsBK.Tables[0].Columns.Add("NguyenLieuVatTuDuThua", typeof(decimal));
            foreach (DataRow dr in this.dsBK.Tables[0].Rows)
            {
                dr["TongLuongXKTemp"] = dr["TongLuongXK"];
               // dr["NguyenLieuVatTuDuThua"] = decimal.Parse(dr[3].ToString()) + decimal.Parse(dr[4].ToString()) - decimal.Parse(dr[5].ToString());
                string TenHang = dr["Ten"].ToString();
                string maHang = "";
                if (TenHang.Contains("&#!"))
                {

                    maHang = TenHang.Substring(TenHang.IndexOf("&#!") + 3);
                    maHang = maHang.Substring(0, maHang.IndexOf("&"));
                }
                else
                {
                    string[] arr = TenHang.Split(new char[1] { '(' });
                    maHang = arr[arr.Length - 1].Replace(")", "");
                }

                dr["MaPhu"] = maHang;
                DataRow[] listRow = dtTaiXuat.Select(string.Format("MaPhu = '{0}'", dr["MaPhu"].ToString()));
                if (listRow.Length > 0)
                {
                    dr["TongXuatTra"] = listRow[0]["SoLuongTaiXuat"];
                    dr["TongLuongXK"] = System.Convert.ToDecimal(dr["TongLuongXK"]) - System.Convert.ToDecimal(dr["TongXuatTra"]);
                }
                
            }
            //this.BC06.report = this;
            this.BC06.dsBK = this.dsBK;
            this.BC06.HD = this.HD;
            this.BC06.BindReport( );
            printControl1.PrintingSystem = this.BC06.PrintingSystem;
            this.BC06.CreateDocument();
            
        }

        

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }   
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BC06.setText(this.Label, txtName.Text);
            this.BC06.CreateDocument();
        }

        private void txtChenhLech_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtChenhLech_Leave(object sender, EventArgs e)
        {
            decimal tyLeCL = Convert.ToDecimal(txtChenhLech.Value);
            foreach (DataRow dr in this.BC06.dsBK.Tables[0].Rows)
            {
                if (Math.Abs(Convert.ToDecimal(dr["TongLuongNK"]) + Convert.ToDecimal(dr["TongLuongCU"]) - Convert.ToDecimal(dr["TongLuongXK"])) < (Convert.ToDecimal(dr["TongLuongNK"]) + Convert.ToDecimal(dr["TongLuongCU"])) * tyLeCL / 100)
                {
                    dr["ChenhLech"] = 0;
                    dr["TongLuongXK"] = Convert.ToDecimal(dr["TongLuongNK"]) + Convert.ToDecimal(dr["TongLuongCU"]);
                }
                else
                {
                    dr["TongLuongXK"] = Convert.ToDecimal(dr["TongLuongXKTemp"]);
                    dr["ChenhLech"] = Convert.ToDecimal(dr["TongLuongNK"]) + Convert.ToDecimal(dr["TongLuongCU"]) - Convert.ToDecimal(dr["TongLuongXK"]);
                }
            }
            this.BC06.BindReport();
            printControl1.PrintingSystem = this.BC06.PrintingSystem;
            this.BC06.CreateDocument();
        }

   
    }
}