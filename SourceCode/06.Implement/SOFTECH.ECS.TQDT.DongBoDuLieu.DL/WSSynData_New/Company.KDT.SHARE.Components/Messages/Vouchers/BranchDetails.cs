﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class BranchDetails
    {
        /// <summary>
        /// Thông tin chi tiết các vận đơn nhánh 
        /// </summary>
       [XmlElement("BranchDetail")]
       public List<BranchDetail> BranchDetail { get; set; }
    }
}
