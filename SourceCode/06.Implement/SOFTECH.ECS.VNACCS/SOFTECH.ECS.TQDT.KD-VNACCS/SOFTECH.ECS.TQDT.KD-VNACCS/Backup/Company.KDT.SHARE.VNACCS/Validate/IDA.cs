﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

namespace Company.KDT.SHARE.VNACCS.Validate
{

    public class IDA
    {
        public DataTable IDAValidateTable { get; set; }

        public IDA()
        {
            IDAValidateTable = HelperVNACCS.LoadDataValidate("IDA");
        }
        private DataTable GenerateTable()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = HelperVNACCS.GeneratorTableValidate();



                return dt;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return HelperVNACCS.GeneratorTableValidate();
            }
            
        }

    }
}
