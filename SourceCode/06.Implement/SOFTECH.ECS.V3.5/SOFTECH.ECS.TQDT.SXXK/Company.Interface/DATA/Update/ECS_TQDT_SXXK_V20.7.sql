  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]  
-- Database: Haiquan  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, July 01, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
alter PROCEDURE [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]  
 @WhereCondition nvarchar(max),  
 @OrderByExpression nvarchar(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(max)  
  
SET @SQL = 'SELECT a.ID,  
 a.STT,  
 a.LanThanhLy,  
 a.NamThanhLy,  
 a.MaDoanhNghiep,  
 Case When MaLoaiHinhNhap like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiNhap]) else [SoToKhaiNhap] end as SoToKhaiNhap ,  
 a.NgayDangKyNhap,  
 Case When MaLoaiHinhNhap like ''%V%'' Then Substring(MaLoaiHinhNhap,3,3) else MaLoaiHinhNhap end as MaLoaiHinhNhap,  
 --a.NgayThucNhap,
 case WHEN year(a.NgayThucNhap)<=1900 then (select NgayHoanThanh from t_sxxk_tokhaimaudich where sotokhai = a.sotokhainhap and MaLoaiHinh= a.MaLoaiHinhNhap and Ngaydangky= a.Ngaydangkynhap ) else a.NgayThucNhap end as NgayThucNhap,  
 UPPER(a.MaNPL) AS MaNPL,  
 case WHEN a.TenNPL is NULL OR a.TenNPL ='''' then b.Ten else a.TenNPL end as TenNPL,  
 a.LuongNhap,  
 a.TenDVT_NPL,  
 a.DonGiaTT,  
 a.TyGiaTT,  
 a.ThueSuat,  
 a.ThueNKNop,  
 Case When MaLoaiHinhXuat like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiXuat]) else [SoToKhaiXuat] end as SoToKhaiXuat,  
 a.NgayDangKyXuat,  
 case WHEN year(a.NgayThucXuat)<=1900 then null else a.NgayThucXuat end as NgayThucXuat,  
 Case When MaLoaiHinhXuat like ''%V%'' Then Substring(MaLoaiHinhXuat,3,3) else MaLoaiHinhXuat end as MaLoaiHinhXuat,  
 a.LuongNPLSuDung,  
 a.LuongNPLTon,  
 a.TienThueHoan,  
 a.TienThueTKTiep,  
 a.GhiChu,  
 a.ThueXNK,  
 a.Luong  
 FROM t_KDT_SXXK_BCThueXNK a  
 INNER JOIN t_SXXK_NguyenPhuLieu b  
 ON a.MaNPL  = b.Ma AND a.MaDoanhNghiep = b.MaDoanhNghiep WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
  
  GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.7',GETDATE(), N' Cap nhat store p_KDT_SXXK_BCThueXNK_SelectDynamic  bao cáo 04')
END