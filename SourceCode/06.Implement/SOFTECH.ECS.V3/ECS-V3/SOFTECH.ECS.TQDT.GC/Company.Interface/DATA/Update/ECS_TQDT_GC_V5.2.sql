

/****** Object:  Table [dbo].[t_HaiQuan_NhomCuaKhau]    Script Date: 12/14/2012 14:39:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_HaiQuan_NhomCuaKhau]') AND type in (N'U'))
begin

CREATE TABLE [dbo].[t_HaiQuan_NhomCuaKhau](
	[Cuc_ID] [varchar](10) NOT NULL,
	[CuaKhau_ID] [varchar](10) NOT NULL,
 CONSTRAINT [PK_t_HaiQuan_NhomCuaKhau] PRIMARY KEY CLUSTERED 
(
	[Cuc_ID] ASC,
	[CuaKhau_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

end

go


SET ANSI_PADDING OFF
GO


/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_Delete]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhau_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_Insert]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhau_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_Load]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhau_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_Update]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhau_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhauGetDonVi]    Script Date: 12/14/2012 14:39:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_NhomCuaKhauGetDonVi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhauGetDonVi]
GO

--USE [ECS_TQDT_KD_V3]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_Delete]    Script Date: 12/14/2012 14:39:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_Delete]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Delete]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_NhomCuaKhau]
WHERE
	[Cuc_ID] = @Cuc_ID
	AND [CuaKhau_ID] = @CuaKhau_ID


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]    Script Date: 12/14/2012 14:39:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_NhomCuaKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_Insert]    Script Date: 12/14/2012 14:39:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_Insert]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Insert]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS
INSERT INTO [dbo].[t_HaiQuan_NhomCuaKhau]
(
	[Cuc_ID],
	[CuaKhau_ID]
)
VALUES
(
	@Cuc_ID,
	@CuaKhau_ID
)


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]    Script Date: 12/14/2012 14:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS
IF EXISTS(SELECT [Cuc_ID], [CuaKhau_ID] FROM [dbo].[t_HaiQuan_NhomCuaKhau] WHERE [Cuc_ID] = @Cuc_ID AND [CuaKhau_ID] = @CuaKhau_ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_NhomCuaKhau] 
		SET
		[Cuc_ID] = @Cuc_ID,
		[CuaKhau_ID] = @CuaKhau_ID
		WHERE
			[Cuc_ID] = @Cuc_ID
			AND [CuaKhau_ID] = @CuaKhau_ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_NhomCuaKhau]
	(
			[Cuc_ID],
			[CuaKhau_ID]
	)
	VALUES
	(
			@Cuc_ID,
			@CuaKhau_ID
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_Load]    Script Date: 12/14/2012 14:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_Load]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Load]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Cuc_ID],
	[CuaKhau_ID]
FROM
	[dbo].[t_HaiQuan_NhomCuaKhau]
WHERE
	[Cuc_ID] = @Cuc_ID
	AND [CuaKhau_ID] = @CuaKhau_ID

GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]    Script Date: 12/14/2012 14:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Cuc_ID],
	[CuaKhau_ID]
FROM
	[dbo].[t_HaiQuan_NhomCuaKhau]	


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]    Script Date: 12/14/2012 14:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[Cuc_ID],
	[CuaKhau_ID]
FROM [dbo].[t_HaiQuan_NhomCuaKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhau_Update]    Script Date: 12/14/2012 14:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_Update]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Update]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS

UPDATE
	[dbo].[t_HaiQuan_NhomCuaKhau]
SET
[Cuc_ID] = @Cuc_ID,
[CuaKhau_ID] = @CuaKhau_ID
WHERE
	[Cuc_ID] = @Cuc_ID
	AND [CuaKhau_ID] = @CuaKhau_ID


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]    Script Date: 12/14/2012 14:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]          
 -- Add the parameters for the stored procedure here          
 @Cuc_ID nvarchar(10)          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
    -- Insert statements for procedure here          
  SELECT     t_HaiQuan_CuaKhau.ID, t_HaiQuan_CuaKhau.Ten, t_HaiQuan_NhomCuaKhau.Cuc_ID    
  FROM         t_HaiQuan_NhomCuaKhau INNER JOIN          
         t_HaiQuan_CuaKhau ON t_HaiQuan_NhomCuaKhau.CuaKhau_ID = t_HaiQuan_CuaKhau.ID          
 WHERE     (t_HaiQuan_NhomCuaKhau.Cuc_ID like '%' +@Cuc_ID +'%')         
          
END 
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_NhomCuaKhauGetDonVi]    Script Date: 12/14/2012 14:39:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[p_HaiQuan_NhomCuaKhauGetDonVi]
	-- Add the parameters for the stored procedure here
	@CuaKhau_ID NVARCHAR(10)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT     t_HaiQuan_DonViHaiQuan.Ten
FROM         t_HaiQuan_DonViHaiQuan INNER JOIN
                      t_HaiQuan_NhomCuaKhau ON t_HaiQuan_DonViHaiQuan.ID = t_HaiQuan_NhomCuaKhau.Cuc_ID
WHERE     (t_HaiQuan_NhomCuaKhau.CuaKhau_ID = @CuaKhau_ID)
END

GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='5.2', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('5.2', getdate(), null)
	end 



