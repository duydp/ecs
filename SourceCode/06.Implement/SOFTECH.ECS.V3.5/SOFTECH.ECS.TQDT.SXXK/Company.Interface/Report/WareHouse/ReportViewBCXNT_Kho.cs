using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;

namespace Company.Interface.Report.WareHouse
{
    public partial class ReportViewBCXNT_Kho : DevExpress.XtraReports.UI.XtraReport
    {
        private DataTable dt = new DataTable();
        private string MaNPLGroup = "";
        private string TenNPLGroup = "";
        private string MaNPLTempGroup = "";
        private string MaNPLKeTiepGroup = "";
        private decimal SumTongLuongNhap = 0;
        private decimal SumTongLuongTonDau = 0;
        private decimal SumTongLuongTonCuoi = 0;
        private decimal SumTongLuongXuat = 0;

        private decimal SumTongTriGiaNhap = 0;
        private decimal SumTongTriGiaTonDau = 0;
        private decimal SumTongTriGiaTonCuoi = 0;
        private decimal SumTongTriGiaXuat = 0;
        public ReportViewBCXNT_Kho()
        {
            InitializeComponent();
        }

        public void BindReport(string SoHopDong,DateTime DateFrom,DateTime DateTo)
        {
            try
            {
                dt = KDT_KhoCFS_DangKy.SelectDynamicBCXNT_Kho(SoHopDong,DateFrom,DateTo).Tables[0];
                dt.TableName = "BCXuatNhapTon_Kho";
                this.DataSource = dt;
 
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("TenHang")
                });
                //lblTongNhap.Summary.Running = SummaryRunning.Group;
                //lblTongNhap.Summary.Func = SummaryFunc.Sum;
                //lblTongNhap.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                //lblTongTriGiaNhap.Summary.Running = SummaryRunning.Group;
                //lblTongTriGiaNhap.Summary.Func = SummaryFunc.Sum;
                //lblTongTriGiaNhap.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                //lblTongTonDau.Summary.Running = SummaryRunning.Group;
                //lblTongTonDau.Summary.Func = SummaryFunc.Sum;
                //lblTongTonDau.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                //lblTongTriGiaTonDau.Summary.Running = SummaryRunning.Group;
                //lblTongTriGiaTonDau.Summary.Func = SummaryFunc.Sum;
                //lblTongTriGiaTonDau.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                //lblTongTriGiaTonCuoi.Summary.Running = SummaryRunning.Group;
                //lblTongTriGiaTonCuoi.Summary.Func = SummaryFunc.Sum;
                //lblTongTriGiaTonCuoi.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                //lblTongXuat.Summary.Running = SummaryRunning.Group;
                //lblTongXuat.Summary.Func = SummaryFunc.Sum;
                //lblTongXuat.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                //lblTongTriGiaXuat.Summary.Running = SummaryRunning.Group;
                //lblTongTriGiaXuat.Summary.Func = SummaryFunc.Sum;
                //lblTongTriGiaXuat.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                //lblTongTonCuoi.Summary.Running = SummaryRunning.Group;
                //lblTongTonCuoi.Summary.Func = SummaryFunc.Sum;
                //lblTongTonCuoi.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

                //lblTongTriGiaTonCuoi.Summary.Running = SummaryRunning.Group;
                //lblTongTriGiaTonCuoi.Summary.Func = SummaryFunc.Sum;
                //lblTongTriGiaTonCuoi.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
                lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                lblDiaChi.Text = GlobalSettings.DIA_CHI;

                lblTuNgay.Text = DateFrom.ToString("dd/MM/yyyy");
                lblDenNgay.Text = DateTo.ToString("dd/MM/yyyy");

                //lblMaKho.Text = MaKho.ToString();
                //lblTenKho.Text = TenKho.ToString(); 

                lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
                lblNgayXNK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayXNK", "{0:dd/MM/yyyy}");
                lblSoPhieu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".PhieuXNKho");
                lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
                lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenHang");
                lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");

                lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDauKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTriGiaTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaTonDauKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

                lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhapKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTriGiaNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaNhapKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

                lblLuongXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongXuatKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTriGiaXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaXuatKho", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

                lblLuongTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                lblTriGiaTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TriGiaTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (GetCurrentColumnValue("MaNPL").ToString() == "") e.Cancel = true;

                string TenNPL = GetCurrentColumnValue("TenHang").ToString();

                if (GetCurrentColumnValue("MaNPL").ToString() != MaNPLGroup
                    && TenNPL != TenNPLGroup)
                {
                    SumTongLuongTonDau = 0;
                    SumTongTriGiaTonDau = 0;
                    SumTongLuongNhap = 0;
                    SumTongTriGiaNhap=0;
                    SumTongLuongXuat = 0;
                    SumTongTriGiaXuat = 0;
                    SumTongLuongTonCuoi = 0;
                    SumTongTriGiaTonCuoi = 0;
                    MaNPLGroup = GetCurrentColumnValue("MaNPL").ToString();
                    TenNPLGroup = TenNPL;
                    SumTongLuongTonDau += System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonDauKho"));
                    SumTongTriGiaTonDau += System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaTonDauKho"));

                    SumTongLuongNhap += System.Convert.ToDecimal(GetCurrentColumnValue("LuongNhapKho"));
                    SumTongTriGiaNhap += System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaNhapKho"));

                    SumTongLuongXuat += System.Convert.ToDecimal(GetCurrentColumnValue("LuongXuatKho"));
                    SumTongTriGiaXuat += System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaXuatKho"));

                    SumTongLuongTonCuoi += System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
                    SumTongLuongTonCuoi += System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaTonCuoi"));
                }
                else
                {
                    MaNPLGroup = GetCurrentColumnValue("MaNPL").ToString();
                    TenNPLGroup = TenNPL;
                    SumTongLuongTonDau += System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonDauKho"));
                    SumTongTriGiaTonDau += System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaTonDauKho"));

                    SumTongLuongNhap += System.Convert.ToDecimal(GetCurrentColumnValue("LuongNhapKho"));
                    SumTongTriGiaNhap += System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaNhapKho"));

                    SumTongLuongXuat += System.Convert.ToDecimal(GetCurrentColumnValue("LuongXuatKho"));
                    SumTongTriGiaXuat += System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaXuatKho"));

                    SumTongLuongTonCuoi += System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
                    SumTongLuongTonCuoi += System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaTonCuoi"));
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                lblTongNhap.Text = SumTongLuongNhap.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                lblTongTriGiaNhap.Text = SumTongTriGiaNhap.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);

                lblTongTonDau.Text = SumTongLuongTonDau.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                lblTongTriGiaTonDau.Text = SumTongTriGiaTonDau.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);

                lblTongXuat.Text = SumTongLuongXuat.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                lblTongTriGiaXuat.Text = SumTongTriGiaXuat.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);

                lblTongTonCuoi.Text = (SumTongLuongTonDau + SumTongLuongNhap - SumTongLuongXuat).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
                lblTongTriGiaTonCuoi.Text = (SumTongTriGiaTonDau + SumTongTriGiaNhap - SumTongTriGiaXuat).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void SetTextLabel(string maNPL, string tenNPL, decimal luongNhap,decimal trigiaNhap, decimal luongTonDau,decimal trigiaTon ,decimal luongTonCuoi, decimal trigiaTonCuoi,decimal luongXuat,decimal triGiaXuat)
        {
            lblTongTonCuoi.Text = (luongTonDau + luongNhap - luongXuat).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.LuongNPL));
            lblTongTriGiaTonCuoi.Text = (trigiaTon + trigiaNhap - triGiaXuat).ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.LuongNPL));
        }
        private void AssignSumGroup()
        {
            SumTongLuongNhap = System.Convert.ToDecimal(GetCurrentColumnValue("LuongNhapKho"));
            SumTongTriGiaNhap = System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaNhapKho"));

            SumTongLuongTonDau = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonDauKho"));
            SumTongLuongTonDau = System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaTonDauKho"));

            SumTongLuongTonCuoi = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
            SumTongLuongTonCuoi = System.Convert.ToDecimal(GetCurrentColumnValue("TriGiaTonCuoi"));
        }

        private void CalculateSumGroup()
        {
            SumTongLuongNhap += System.Convert.ToDecimal(GetCurrentColumnValue("LuongNhap"));
            SumTongLuongTonDau += System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            SumTongLuongTonCuoi = System.Convert.ToDecimal(GetCurrentColumnValue("LuongTonCuoi"));
        }
    }
}
