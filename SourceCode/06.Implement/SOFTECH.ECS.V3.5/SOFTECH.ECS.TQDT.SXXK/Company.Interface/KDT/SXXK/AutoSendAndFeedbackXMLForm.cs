﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Logger;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class AutoSendAndFeedbackXMLForm : Company.Interface.BaseForm
    {
        public AutoSendAndFeedbackXMLForm()
        {
            InitializeComponent();
        }
        public NguyenPhuLieuDangKy nplDangKy = new NguyenPhuLieuDangKy();
        public SanPhamDangKy spDangKy = new SanPhamDangKy();
        public DinhMucDangKy dmDangKy = new DinhMucDangKy();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public string LoaiChungTu = "";
        public bool isFeedBack = true;
        private bool isStop;
        public DataTable dtFeedBack;
        public string Type = "";
        public int Counter;
        private void AutoSendAndFeedbackXMLForm_Load(object sender, EventArgs e)
        {
            switch (Type)
            {
                case "NPL":
                    LoadDataNPL();
                    break;
                case "SP":
                    LoadDataSP();
                    break;
                case "DM":
                    LoadDataDM();
                    break;
                default:
                    break;
            }
            dtFeedBack = new DataTable();

            DataColumn dtColSoToKhai = new DataColumn("ID");
            dtColSoToKhai.DataType = typeof(System.String);
            DataColumn dtColNgayDangKy = new DataColumn("SoTiepNhan");
            dtColNgayDangKy.DataType = typeof(System.DateTime);
            DataColumn dtColSoTN = new DataColumn("NgayTiepNhan");
            dtColSoTN.DataType = typeof(System.String);
            DataColumn dtColNgayTN = new DataColumn("MaHaiQuan");
            dtColNgayTN.DataType = typeof(System.DateTime);
            DataColumn dtColLoaiChungTu = new DataColumn("MaDoanhNghiep");
            dtColLoaiChungTu.DataType = typeof(System.String);

            dtFeedBack.Columns.Add(dtColSoToKhai);
            dtFeedBack.Columns.Add(dtColNgayDangKy);
            dtFeedBack.Columns.Add(dtColSoTN);
            dtFeedBack.Columns.Add(dtColNgayTN);
            dtFeedBack.Columns.Add(dtColLoaiChungTu);
        }
        private void LoadDataNPL()
        {
            try
            {
                grList.Refresh();
                grList.DataSource = NguyenPhuLieuDangKy.SelectCollectionDynamic("TrangThaiXuLy IN (-1,-3)", "ID");
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadDataSP()
        {
            try
            {
                grList.Refresh();
                grList.DataSource = SanPhamDangKy.SelectCollectionDynamic("TrangThaiXuLy IN (-1,-3)", "ID");
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadDataDM()
        {
            try
            {
                grList.Refresh();
                grList.DataSource = DinhMucDangKy.SelectCollectionDynamic("TrangThaiXuLy IN (-1,-3)", "ID");
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void btnThucHien_Click(object sender, EventArgs e)
        {
            btnThucHien.Enabled = false;
            btnDong.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void DoWork(object obj)
        {
            try
            {

                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    switch (Type)
                    {
                        case "NPL":
                            nplDangKy = new NguyenPhuLieuDangKy();
                            nplDangKy = (NguyenPhuLieuDangKy)item.DataRow;
                            nplDangKy.LoadNPLCollection();
                            SendV5NPL(false);
                            break;
                        case "SP":
                            spDangKy = new SanPhamDangKy();
                            spDangKy = (SanPhamDangKy)item.DataRow;
                            spDangKy.LoadSPCollection();
                            SendV5SP(false);
                            break;
                        case "DM":
                            dmDangKy = new DinhMucDangKy();
                            dmDangKy = (DinhMucDangKy)item.DataRow;
                            dmDangKy.LoadDMCollection();
                            SendV5DM(false);
                            break;
                        default:
                            break;
                    }
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        switch (Type)
                        {
                            case "NPL":
                                LoadDataNPL();
                                break;
                            case "SP":
                                LoadDataSP();
                                break;
                            case "DM":
                                LoadDataDM();
                                break;
                            default:
                                break;
                        }
                        btnThucHien.Enabled = true; btnDong.Enabled = true;
                        btnStop.Enabled = true;

                    }));
                }
                else
                {
                    switch (Type)
                    {
                        case "NPL":
                            LoadDataNPL();
                            break;
                        case "SP":
                            LoadDataSP();
                            break;
                        case "DM":
                            LoadDataDM();
                            break;
                        default:
                            break;
                    }
                    btnThucHien.Enabled = true; btnDong.Enabled = true; btnStop.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");

            }

            catch (System.Exception ex)
            {
                SetError(string.Empty);
                LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    btnStop.Enabled = true;
                    btnThucHien.Enabled = true;
                    isFeedBack = false;
                    isStop = true;
                }));
            }
            else
            {
                btnStop.Enabled = true;
                btnThucHien.Enabled = true;
                isFeedBack = false;
                isStop = true;
            }
        }
        private void BinDataFeedBack()
        {
            try
            {
                grListResult.Refresh();
                grListResult.DataSource = dtFeedBack;
                grListResult.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        #region Nguyên phụ liệu
        private void SendV5NPL(bool IsCancel)
        {
            try
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
                sendXML.master_id = nplDangKy.ID;
                if (sendXML.Load())
                {
                    if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        FeedBackV5NPL();
                    }
                }
                nplDangKy.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.SXXK_NguyenPhuLieu npl = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_NPL(nplDangKy, IsCancel, GlobalSettings.TEN_DON_VI);
                ObjectSend msgSend = new ObjectSend(
                          new Company.KDT.SHARE.Components.NameBase()
                          {
                              Name = GlobalSettings.TEN_DON_VI,
                              Identity = nplDangKy.MaDoanhNghiep
                          }
                            , new Company.KDT.SHARE.Components.NameBase()
                            {
                                Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(nplDangKy.MaHaiQuan),
                                Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplDangKy.MaHaiQuan).Trim() : nplDangKy.MaHaiQuan
                            }
                         ,

                           new Company.KDT.SHARE.Components.SubjectBase()
                           {
                               Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_NPL,
                               Function = npl.Function,
                               Reference = nplDangKy.GUIDSTR,
                           }
                           ,
                           npl);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessageNPL;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(nplDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoNguyenPhuLieu);
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
                    sendXML.master_id = nplDangKy.ID;
                    sendXML.func = 1;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        DataRow dtRow = dtFeedBack.NewRow();
                        dtRow["ID"] = nplDangKy.ID.ToString();
                        dtRow["SoTiepNhan"] = nplDangKy.SoTiepNhan.ToString();
                        dtRow["NgayTiepNhan"] = nplDangKy.NgayTiepNhan.ToString();
                        dtRow["MaHaiQuan"] = nplDangKy.MaHaiQuan.ToString();
                        dtRow["MaDoanhNghiep"] = nplDangKy.MaDoanhNghiep.ToString();
                        dtFeedBack.Rows.Add(dtRow);
                        nplDangKy.TransgferDataToSXXK();
                        BinDataFeedBack();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackV5NPL();
                    }
                }
            }


            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        void SendMessageNPL(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandlerNPL),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandlerNPL(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.NguyenPhuLieuSendHandler(nplDangKy, ref msgInfor, e);
        }
        private void FeedBackV5NPL()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = nplDangKy.GUIDSTR;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_NPL,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_NPL,
                };
                subjectBase.Type = DeclarationIssuer.SXXK_NPL;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = nplDangKy.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(nplDangKy.MaHaiQuan.Trim())),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplDangKy.MaHaiQuan).Trim() : nplDangKy.MaHaiQuan
                                              }, subjectBase, null);
                if (nplDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(nplDangKy.MaHaiQuan));
                    msgSend.To.Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(nplDangKy.MaHaiQuan).Trim();
                }
                while (isFeedBack)
                {
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessageNPL;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    count++;
                    if (isFeedBack)
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY && count <= Counter)
                        {
                            isFeedBack = true;
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                        {
                            DataRow dtRow = dtFeedBack.NewRow();
                            dtRow["ID"] = nplDangKy.ID.ToString();
                            dtRow["SoTiepNhan"] = nplDangKy.SoTiepNhan.ToString();
                            dtRow["NgayTiepNhan"] = nplDangKy.NgayTiepNhan.ToString();
                            dtRow["MaHaiQuan"] = nplDangKy.MaHaiQuan.ToString();
                            dtRow["MaDoanhNghiep"] = nplDangKy.MaDoanhNghiep.ToString();
                            dtFeedBack.Rows.Add(dtRow);
                            nplDangKy.TransgferDataToSXXK();
                            BinDataFeedBack();
                            isFeedBack = false;
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            nplDangKy.Update();
                            isFeedBack = false;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                }
            }
        }
        #endregion

        #region Sản phẩm
        private void SendV5SP(bool isCancel)
        {
            this.spDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN.Trim();
            this.spDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI.Trim();
            this.spDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY.Trim();
            try
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.SanPham;
                sendXML.master_id = spDangKy.ID;
                if (sendXML.Load())
                {
                    if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        FeedBackV5SP();
                    }
                }
                spDangKy.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham();
                if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                {
                    sanpham = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_DNCX_HangDuaRa(spDangKy, "", isCancel, GlobalSettings.TEN_DON_VI);
                }
                else
                {

                    sanpham = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_SXXK_SP(spDangKy, isCancel, GlobalSettings.TEN_DON_VI);
                }
                ObjectSend msgSend = new ObjectSend(new NameBase()
                {
                    Name = GlobalSettings.TEN_DON_VI,
                    Identity = spDangKy.MaDoanhNghiep,
                },
                                new NameBase()
                                {
                                    Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan),
                                    Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(spDangKy.MaHaiQuan).Trim() : spDangKy.MaHaiQuan
                                },
                                  new SubjectBase()
                                  {
                                      Type = DeclarationIssuer.SXXK_SP,
                                      Function = sanpham.Function,
                                      Reference = spDangKy.GUIDSTR,
                                  },
                                sanpham);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessageSP;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(spDangKy.ID, MessageTitle.KhaiBaoSanPham);
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.SanPham;
                    sendXML.func = 1;
                    sendXML.master_id = spDangKy.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        DataRow dtRow = dtFeedBack.NewRow();
                        dtRow["ID"] = nplDangKy.ID.ToString();
                        dtRow["SoTiepNhan"] = nplDangKy.SoTiepNhan.ToString();
                        dtRow["NgayTiepNhan"] = nplDangKy.NgayTiepNhan.ToString();
                        dtRow["MaHaiQuan"] = nplDangKy.MaHaiQuan.ToString();
                        dtRow["MaDoanhNghiep"] = nplDangKy.MaDoanhNghiep.ToString();
                        dtFeedBack.Rows.Add(dtRow);
                        spDangKy.TransgferDataToSXXK();
                        BinDataFeedBack();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackV5SP();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void FeedBackV5SP()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = spDangKy.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_SP,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_SP,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = spDangKy.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan.Trim()),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(spDangKy.MaHaiQuan).Trim() : spDangKy.MaHaiQuan
                                              }, subjectBase, null);
                while (isFeedBack)
                {
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessageSP;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    count++;
                    if (isFeedBack)
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY && count <= Counter)
                        {
                            isFeedBack = true;
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                        {
                            DataRow dtRow = dtFeedBack.NewRow();
                            dtRow["ID"] = nplDangKy.ID.ToString();
                            dtRow["SoTiepNhan"] = nplDangKy.SoTiepNhan.ToString();
                            dtRow["NgayTiepNhan"] = nplDangKy.NgayTiepNhan.ToString();
                            dtRow["MaHaiQuan"] = nplDangKy.MaHaiQuan.ToString();
                            dtRow["MaDoanhNghiep"] = nplDangKy.MaDoanhNghiep.ToString();
                            dtFeedBack.Rows.Add(dtRow);
                            spDangKy.TransgferDataToSXXK();
                            BinDataFeedBack();
                            isFeedBack = false;
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            spDangKy.Update();
                            isFeedBack = false;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                }
            }
        }
        void SendMessageSP(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandlerSP),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandlerSP(object sender, SendEventArgs e)
        {
            {
                feedbackContent = SingleMessage.SanPhamSendHandler(spDangKy, ref msgInfor, e);
            }
        }

        #endregion
        #region Định mức
        private void SendV5DM(bool isCancel)
        {
            bool isEdit = false;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (sendXML.Load())
            {
                if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    FeedBackV5DM();
                }
            }
            try
            {
                this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                string returnMessage = string.Empty;
                dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                SXXK_DinhMucSP dmSp = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc(dmDangKy, isCancel, isEdit, GlobalSettings.TEN_DON_VI);
                ObjectSend msgSend = new ObjectSend(
                     new NameBase()
                     {
                         Name = GlobalSettings.TEN_DON_VI,
                         Identity = dmDangKy.MaDoanhNghiep
                     },
                      new NameBase()
                      {
                          Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                          Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan
                      },
                      new SubjectBase()
                      {
                          Type = dmSp.Issuer,
                          Function = DeclarationFunction.KHAI_BAO,
                          Reference = dmDangKy.GUIDSTR,
                      },
                      dmSp
                    );

                msgSend.Subject.Function = Company.KDT.SHARE.Components.DeclarationFunction.KHAI_BAO; ;
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessageDM;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.KhaiBaoDinhMuc);
                    sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                    sendXML.master_id = dmDangKy.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        DataRow dtRow = dtFeedBack.NewRow();
                        dtRow["ID"] = nplDangKy.ID.ToString();
                        dtRow["SoTiepNhan"] = nplDangKy.SoTiepNhan.ToString();
                        dtRow["NgayTiepNhan"] = nplDangKy.NgayTiepNhan.ToString();
                        dtRow["MaHaiQuan"] = nplDangKy.MaHaiQuan.ToString();
                        dtRow["MaDoanhNghiep"] = nplDangKy.MaDoanhNghiep.ToString();
                        dtFeedBack.Rows.Add(dtRow);
                        dmDangKy.TransferDataToSXXK();
                        BinDataFeedBack();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackV5DM();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        void SendMessageDM(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandlerDM),
                sender, e);
        }
        private void FeedBackV5DM()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = dmDangKy.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_DINH_MUC,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = dmDangKy.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan.Trim()),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan
                                                  //Identity = dmDangKy.MaHaiQuan
                                              }, subjectBase, null);
                while (isFeedBack)
                {
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessageDM;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    count++;
                    if (isFeedBack)
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY && count <= Counter)
                        {
                            isFeedBack = true;
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                        {
                            DataRow dtRow = dtFeedBack.NewRow();
                            dtRow["ID"] = nplDangKy.ID.ToString();
                            dtRow["SoTiepNhan"] = nplDangKy.SoTiepNhan.ToString();
                            dtRow["NgayTiepNhan"] = nplDangKy.NgayTiepNhan.ToString();
                            dtRow["MaHaiQuan"] = nplDangKy.MaHaiQuan.ToString();
                            dtRow["MaDoanhNghiep"] = nplDangKy.MaDoanhNghiep.ToString();
                            dtFeedBack.Rows.Add(dtRow);
                            spDangKy.TransgferDataToSXXK();
                            BinDataFeedBack();
                            isFeedBack = false;
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            dmDangKy.Update();
                            isFeedBack = false;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                }

            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandlerDM(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dmDangKy, ref msgInfor, e);
        }
        #endregion
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Type = cbStatus.SelectedValue.ToString();
                switch (Type)
                {
                    case "NPL":
                        LoadDataNPL();
                        break;
                    case "SP":
                        LoadDataSP();
                        break;
                    case "DM":
                        LoadDataDM();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtNumber_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Counter = Convert.ToInt32(txtNumber.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TrangThaiXuLy"].Value != null)
                {
                    string pl = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    if (pl == "-1")
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    else if (pl == "-3")
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                }
            }
        }
    }
}
