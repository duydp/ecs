DELETE dbo.t_VNACC_Category_Common WHERE ReferenceDB='E002' AND code IN ('E44','E64','H21')
DELETE dbo.t_VNACC_Category_Common WHERE ReferenceDB='E001' AND code IN ('A44','A43','E25','E33','H11')
Go
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất kinh doanh, Xuất khẩu của doanh nghiệp đầu tư' WHERE ReferenceDB='E002' AND Code='B11'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất sau khi đã tạm xuất' WHERE ReferenceDB='E002' AND Code='B12'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất trả hàng nhập khẩu' WHERE ReferenceDB='E002' AND Code='B13'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất sản phẩm của DNCX' WHERE ReferenceDB='E002' AND Code='E42'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Hàng của DNCX vào nội địa để GC' WHERE ReferenceDB='E002' AND Code='E46'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất các sản phẩm GC cho thương nhân nước ngoài' WHERE ReferenceDB='E002' AND Code='E52'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất nguyên phụ liệu gia công cho hợp đồng khác' WHERE ReferenceDB='E002' AND Code='E54'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất sản phẩm GC vào nội địa' WHERE ReferenceDB='E002' AND Code='E56'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất sản phẩm SXXK' WHERE ReferenceDB='E002' AND Code='E62'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Xuất nguyên liệu thuê GC ở nước ngoài' WHERE ReferenceDB='E002' AND Code='E82'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tái xuất hàng kinh doanh TNTX' WHERE ReferenceDB='E002' AND Code='G21'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tái xuất thiết bị, máy móc thuê phục vụ dự án có thời hạn' WHERE ReferenceDB='E002' AND Code='G22'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tái xuất hàng miễn thuế tạm nhập' WHERE ReferenceDB='E002' AND Code='G23'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tái xuất khác' WHERE ReferenceDB='E002' AND Code='G24'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tạm xuất hàng hóa' WHERE ReferenceDB='E002' AND Code='G61'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Hàng xuất kho ngoại quan' WHERE ReferenceDB='E002' AND Code='C12'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Hàng đưa ra khỏi khu phi thuế quan' WHERE ReferenceDB='E002' AND Code='C22'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Danh  nghiệp ưu tiên AEO' WHERE ReferenceDB='E002' AND Code='AEO'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập kinh doanh tiêu dùng' WHERE ReferenceDB='E001' AND Code='A11'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập kinh doanh sản xuất' WHERE ReferenceDB='E001' AND Code='A12'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Chuyển tiêu thụ nội địa từ nguồn tạm nhập' WHERE ReferenceDB='E001' AND Code='A21'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập hàng XK bị trả lại' WHERE ReferenceDB='E001' AND Code='A31'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập kinh doanh của doanh nghiệp đầu tư' WHERE ReferenceDB='E001' AND Code='A41'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Chuyển tiêu thụ nội địa khác' WHERE ReferenceDB='E001' AND Code='A42'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập nguyên liệu của DNCX từ nước ngoài' WHERE ReferenceDB='E001' AND Code='E11'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập tạo tài sản cố định của doanh nghiệp chế xuất' WHERE ReferenceDB='E001' AND Code='E13'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập nguyên liệu của doanh nghiệp chế xuất từ nội địa' WHERE ReferenceDB='E001' AND Code='E15'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập nguyên liệu để gia công' WHERE ReferenceDB='E001' AND Code='E21'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập nguyên liệu gia công từ hợp đồng khác chuyển sang' WHERE ReferenceDB='E001' AND Code='E23'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập nguyên liệu sản xuất xuất khẩu' WHERE ReferenceDB='E001' AND Code='E31'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập sản phẩm thuê gia công ở nước ngoài' WHERE ReferenceDB='E001' AND Code='E41'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tạm nhập hàng kinh doanh tạm nhập tái xuất' WHERE ReferenceDB='E001' AND Code='G11'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tạm nhập máy móc, thiết bị phục vụ thực hiện các dự án có thời hạn' WHERE ReferenceDB='E001' AND Code='G12'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tạm nhập miễn thuế' WHERE ReferenceDB='E001' AND Code='G13'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tạm nhập khác' WHERE ReferenceDB='E001' AND Code='G14'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Tái nhập hàng đã tạm xuất' WHERE ReferenceDB='E001' AND Code='G51'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Hàng gửi kho ngoại quan' WHERE ReferenceDB='E001' AND Code='C11'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Hàng đưa vào khu phi thuế quan' WHERE ReferenceDB='E001' AND Code='C21'
UPDATE dbo.t_VNACC_Category_Common SET Name_VN=N'Nhập khẩu của doang nghiệp AEO' WHERE ReferenceDB='E001' AND Code='AEO'
GO
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','51C2S03','CANG TH THI VAI')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','51C1SC1','CCHQ PHU MY - HHKNQ')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','51C2SC2','CCHQ PHU MY - SPPSA')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','51C1OZZ','DIEM LUU HH XK 51C1')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','51C2OZZ','DIEM LUU HH XK 51C2')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1D02','MIEN THUE SASCO - XK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S03','CT MTV CANG BEN NGHE')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1C01','CTY SAFI')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B4W01','KNQ SCSC - NK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B4A01','KHO SCSC - NK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B4OZZ','DIEM LUU HH XK 02B2')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B4C01','KHO CFS SCSC - NK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1W04','KNQ WF41 - XK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1W03','KNQ WF41 - NK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B4W02','KNQ SCSC - XK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1W02','KNQ TCS - XK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1W01','KNQ TCS - NK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H3S01','LD PT TIEP VAN SO 1')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1F01','BAO THUE CATERING NK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1D01','MIEN THUE SASCO - NK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B4A02','KHO SCSC - XK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1A02','KHO TCS - XK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1A01','KHO TCS - NK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02DSC10','KHO HOP NHAT')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02DSC09','KHO KERRY')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02F1C03','SAGAWA EXPRESS VN 2')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02F1W02','SAGAWA EXPRESS VN 1')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02F1W01','GN KV BINH MINH XFW')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02F1C02','GN KV BINH MINH XFC')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02F1C01','CTY VAN TAI BIEN SG')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02F3XF3','KCX LINH TRUNG III')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B1F02','BAO THUE CATERING XK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S01','CASTROL BP PETRO LTD')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1W01','CT MTV CANG SG CHW 1')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1W04','KNQ CTY CANG BENNGHE')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1W03','CTY LD BONG SEN CHW')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S04','KHO VK102/CUC HC/QK7')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02B4C02','KHO CFS SCSC - XK')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S07','CTY LD BONG SEN CHS')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S05','MTV DAU KHI TP.HCM 1')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1W02','CT MTV CANG SG CHW 2')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S07','TM SP HD LAMTAICHANH')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S09','XANG DAU NHA BE')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S03','HOA DAU PETROLIMEX')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S11','TONG CTY VOCARIMEX')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S04','CT MTV CANG SG CHS 1')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S02','CANG TAN THUAN DONG')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S06','NHUA DUONG PERTRO')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S06','CTY CANG RAU QUA')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S02','CTY GAS PETROLIMEX')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S01','CANG BIEN DONG')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S05','CT MTV CANG SG CHS 2')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S08','CTY TAU THUY SG')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S10','SAI GON SHIPMARIN')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H1S09','CTY TOTALGAZ VN')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','02H2S08','TONG CTY DAU VN')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','20CDC16','CTY NHUA MINH TUONG')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','20CFC03','CTY XNK THUY SAN QN')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','20CFS02','CANG XI MANG HA LONG')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','20CFS03','CANG KHACH HON GAI')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','20CFS04','CANG KHACH TUAN CHAU')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','20CFS05','CANG XANG DAU B12')
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Cargo WHERE BondedAreaCode='')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) VALUES('A202A','20CGS04','CANG XI MANG CAM PHA')
GO
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='DIEM LUU HH XK 02H2' WHERE BondedAreaCode='02H2OZZ'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='DIEM LUU HH XK 02H3' WHERE BondedAreaCode='02H3OZZ'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='DIEM LUU HH XK 02F2' WHERE BondedAreaCode='02F2OZZ'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='DIEM LUU HH XK 02F1' WHERE BondedAreaCode='02F1OZZ'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='DIEM LUU HH XK 02B1' WHERE BondedAreaCode='02B1OZZ'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='DIEM LUU HH XK 02H1' WHERE BondedAreaCode='02H1OZZ'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='CCHQCK CANG SG KV I' WHERE BondedAreaCode='02CIRCI'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='CTY XI MANG CHINFON' WHERE BondedAreaCode='02CVS02'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='XI MANG FICO TAYNINH' WHERE BondedAreaCode='02CVS03'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='KHO DHL' WHERE BondedAreaCode='02DSC05'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='KHO D/NGHIEP KHAC' WHERE BondedAreaCode='02DSC08'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='KNQ PHUC LONG' WHERE BondedAreaCode='02IKW04'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='DIEM LUU HH XK 02F3' WHERE BondedAreaCode='02F3OZZ'
UPDATE t_VNACC_Category_Cargo SET BondedAreaName='KHO B.CUC NGOAI DICH' WHERE BondedAreaCode='02DSP01'
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '17.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('17.4',GETDATE(), N' Cập nhật mã điểm lưu kho, tên loại hình mậu dich')
END	


