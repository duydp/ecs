﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;
namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class BoSungChungTu : DeclarationBase
    {
        [XmlElement("DeclarationDocument")]
        public DeclarationBase DeclarationDocument { get; set; }
        /// <summary>
        /// Giấy phép
        /// </summary>
        [XmlArray("Licenses")]
        [XmlArrayItem("License")]
        public List<License> Licenses { get; set; }

        /// <summary>
        /// CO
        /// </summary>
        [XmlArray("CertificateOfOrigins")]
        [XmlArrayItem("CertificateOfOrigin")]
        public List<CertificateOfOrigin> CertificateOfOrigins { get; set; }
        /// <summary>
        /// Đề nghị chuyển cửa khẩu
        /// </summary>
        [XmlElement("CustomsOfficeChangedRequest")]
        public CustomsOfficeChangedRequest CustomsOfficeChangedRequest { get; set; }
        /// <summary>
        /// File đính kèm
        /// </summary>
        [XmlArray("AttachDocuments")]
        [XmlArrayItem("AttachDocumentItem")]
        public List<AttachDocumentItem> AttachDocuments { get; set; }
        /// <summary>
        /// Hóa đơn thương mại
        /// </summary>
        [XmlArray("CommercialInvoices")]
        [XmlArrayItem("CommercialInvoice")]
        public List<CommercialInvoice> CommercialInvoices { get; set; }

        /// <summary>
        /// Hợp đồng thương mại
        /// </summary>
        [XmlArray("ContractDocuments")]
        [XmlArrayItem("ContractDocument")]
        public List<ContractDocument> ContractDocuments { get; set; }

        //Thông tin giấy nộp tiền
        [XmlElement("Receipt")]
        public Receipt Receipt { get; set; }

        //Thông tin giấy nộp tiền
        [XmlElement("ExaminationRegistration")]
        public ExaminationRegistration ExaminationRegistration { get; set; }

        //Thông tin giấy nộp tiền
        [XmlElement("ExaminationResult")]
        public ExaminationResult ExaminationResult { get; set; }

        /// <summary>
        /// Thông tin chứng thư giám định
        /// </summary>
        [XmlElement("CertificateOfInspection")]
        public CertificateOfInspection CertificateOfInspection { get; set; }
    }
}
