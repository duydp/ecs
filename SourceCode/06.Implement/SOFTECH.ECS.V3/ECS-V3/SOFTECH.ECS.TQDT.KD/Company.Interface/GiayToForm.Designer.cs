﻿using System.ComponentModel;
using System.Windows.Forms;
using Company.Interface.Controls;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface
{
    partial class GiayToForm
    {
        private UIGroupBox uiGroupBox1;
        private Label label2;
        private Label lblHoaDon;
        private Label label4;
        private Label label27;
        private UIGroupBox uiGroupBox2;
        private UIButton btnAddNew;
        private ToolTip toolTip1;
        private ErrorProvider epError;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayToForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.FileLoaiKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.FileBanKe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.FileVanTai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.FileHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.FileHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLoaiKhac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtBSLoaiKhac1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtBCLoaiKhac1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtBSBanKe = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtBCBanKe = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.lblBanKe = new System.Windows.Forms.Label();
            this.txtBSVanTai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtBCVanTai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.lblVanTai = new System.Windows.Forms.Label();
            this.txtBSHopDong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtBCHopDong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.lblHopDong = new System.Windows.Forms.Label();
            this.txtBSHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtBCHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblHoaDon = new System.Windows.Forms.Label();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(431, 235);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.btnAddNew);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(431, 235);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnClose.Location = new System.Drawing.Point(349, 204);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.FileLoaiKhac);
            this.uiGroupBox2.Controls.Add(this.FileBanKe);
            this.uiGroupBox2.Controls.Add(this.FileVanTai);
            this.uiGroupBox2.Controls.Add(this.FileHopDong);
            this.uiGroupBox2.Controls.Add(this.FileHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtLoaiKhac1);
            this.uiGroupBox2.Controls.Add(this.txtBSLoaiKhac1);
            this.uiGroupBox2.Controls.Add(this.txtBCLoaiKhac1);
            this.uiGroupBox2.Controls.Add(this.txtBSBanKe);
            this.uiGroupBox2.Controls.Add(this.txtBCBanKe);
            this.uiGroupBox2.Controls.Add(this.lblBanKe);
            this.uiGroupBox2.Controls.Add(this.txtBSVanTai);
            this.uiGroupBox2.Controls.Add(this.txtBCVanTai);
            this.uiGroupBox2.Controls.Add(this.lblVanTai);
            this.uiGroupBox2.Controls.Add(this.txtBSHopDong);
            this.uiGroupBox2.Controls.Add(this.txtBCHopDong);
            this.uiGroupBox2.Controls.Add(this.lblHopDong);
            this.uiGroupBox2.Controls.Add(this.txtBSHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtBCHoaDon);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.lblHoaDon);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(411, 190);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // FileLoaiKhac
            // 
            this.FileLoaiKhac.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.TextButton;
            this.FileLoaiKhac.ButtonText = "...";
            this.FileLoaiKhac.Location = new System.Drawing.Point(276, 156);
            this.FileLoaiKhac.Name = "FileLoaiKhac";
            this.FileLoaiKhac.Size = new System.Drawing.Size(122, 21);
            this.FileLoaiKhac.TabIndex = 22;
            this.FileLoaiKhac.ButtonClick += new System.EventHandler(this.editBox1_ButtonClick);
            // 
            // FileBanKe
            // 
            this.FileBanKe.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.TextButton;
            this.FileBanKe.ButtonText = "...";
            this.FileBanKe.Location = new System.Drawing.Point(276, 129);
            this.FileBanKe.Name = "FileBanKe";
            this.FileBanKe.Size = new System.Drawing.Size(122, 21);
            this.FileBanKe.TabIndex = 18;
            this.FileBanKe.ButtonClick += new System.EventHandler(this.editBox1_ButtonClick);
            // 
            // FileVanTai
            // 
            this.FileVanTai.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.TextButton;
            this.FileVanTai.ButtonText = "...";
            this.FileVanTai.Location = new System.Drawing.Point(276, 102);
            this.FileVanTai.Name = "FileVanTai";
            this.FileVanTai.Size = new System.Drawing.Size(122, 21);
            this.FileVanTai.TabIndex = 14;
            this.FileVanTai.ButtonClick += new System.EventHandler(this.editBox1_ButtonClick);
            // 
            // FileHopDong
            // 
            this.FileHopDong.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.TextButton;
            this.FileHopDong.ButtonText = "...";
            this.FileHopDong.Location = new System.Drawing.Point(276, 76);
            this.FileHopDong.Name = "FileHopDong";
            this.FileHopDong.Size = new System.Drawing.Size(122, 21);
            this.FileHopDong.TabIndex = 10;
            this.FileHopDong.ButtonClick += new System.EventHandler(this.editBox1_ButtonClick);
            // 
            // FileHoaDon
            // 
            this.FileHoaDon.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.TextButton;
            this.FileHoaDon.ButtonText = "...";
            this.FileHoaDon.Location = new System.Drawing.Point(276, 51);
            this.FileHoaDon.Name = "FileHoaDon";
            this.FileHoaDon.Size = new System.Drawing.Size(122, 21);
            this.FileHoaDon.TabIndex = 6;
            this.FileHoaDon.ButtonClick += new System.EventHandler(this.editBox1_ButtonClick);
            // 
            // txtLoaiKhac1
            // 
            this.txtLoaiKhac1.Location = new System.Drawing.Point(6, 157);
            this.txtLoaiKhac1.MaxLength = 100;
            this.txtLoaiKhac1.Name = "txtLoaiKhac1";
            this.txtLoaiKhac1.Size = new System.Drawing.Size(122, 21);
            this.txtLoaiKhac1.TabIndex = 19;
            // 
            // txtBSLoaiKhac1
            // 
            this.txtBSLoaiKhac1.DecimalDigits = 0;
            this.txtBSLoaiKhac1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSLoaiKhac1.Location = new System.Drawing.Point(213, 156);
            this.txtBSLoaiKhac1.MaxLength = 3;
            this.txtBSLoaiKhac1.Name = "txtBSLoaiKhac1";
            this.txtBSLoaiKhac1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBSLoaiKhac1.Size = new System.Drawing.Size(41, 21);
            this.txtBSLoaiKhac1.TabIndex = 21;
            this.txtBSLoaiKhac1.Text = "0";
            this.txtBSLoaiKhac1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBSLoaiKhac1.Value = ((uint)(0u));
            this.txtBSLoaiKhac1.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBSLoaiKhac1.VisualStyleManager = this.vsmMain;
            // 
            // txtBCLoaiKhac1
            // 
            this.txtBCLoaiKhac1.DecimalDigits = 0;
            this.txtBCLoaiKhac1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBCLoaiKhac1.Location = new System.Drawing.Point(137, 156);
            this.txtBCLoaiKhac1.MaxLength = 3;
            this.txtBCLoaiKhac1.Name = "txtBCLoaiKhac1";
            this.txtBCLoaiKhac1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBCLoaiKhac1.Size = new System.Drawing.Size(41, 21);
            this.txtBCLoaiKhac1.TabIndex = 20;
            this.txtBCLoaiKhac1.Text = "0";
            this.txtBCLoaiKhac1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBCLoaiKhac1.Value = ((uint)(0u));
            this.txtBCLoaiKhac1.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBCLoaiKhac1.VisualStyleManager = this.vsmMain;
            // 
            // txtBSBanKe
            // 
            this.txtBSBanKe.DecimalDigits = 0;
            this.txtBSBanKe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSBanKe.Location = new System.Drawing.Point(213, 129);
            this.txtBSBanKe.MaxLength = 3;
            this.txtBSBanKe.Name = "txtBSBanKe";
            this.txtBSBanKe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBSBanKe.Size = new System.Drawing.Size(41, 21);
            this.txtBSBanKe.TabIndex = 17;
            this.txtBSBanKe.Text = "0";
            this.txtBSBanKe.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBSBanKe.Value = ((uint)(0u));
            this.txtBSBanKe.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBSBanKe.VisualStyleManager = this.vsmMain;
            // 
            // txtBCBanKe
            // 
            this.txtBCBanKe.DecimalDigits = 0;
            this.txtBCBanKe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBCBanKe.Location = new System.Drawing.Point(137, 129);
            this.txtBCBanKe.MaxLength = 3;
            this.txtBCBanKe.Name = "txtBCBanKe";
            this.txtBCBanKe.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBCBanKe.Size = new System.Drawing.Size(41, 21);
            this.txtBCBanKe.TabIndex = 16;
            this.txtBCBanKe.Text = "0";
            this.txtBCBanKe.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBCBanKe.Value = ((uint)(0u));
            this.txtBCBanKe.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBCBanKe.VisualStyleManager = this.vsmMain;
            // 
            // lblBanKe
            // 
            this.lblBanKe.AutoSize = true;
            this.lblBanKe.BackColor = System.Drawing.Color.Transparent;
            this.lblBanKe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanKe.Location = new System.Drawing.Point(6, 137);
            this.lblBanKe.Name = "lblBanKe";
            this.lblBanKe.Size = new System.Drawing.Size(74, 13);
            this.lblBanKe.TabIndex = 15;
            this.lblBanKe.Text = "Bản kê chi tiết";
            // 
            // txtBSVanTai
            // 
            this.txtBSVanTai.DecimalDigits = 0;
            this.txtBSVanTai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSVanTai.Location = new System.Drawing.Point(213, 102);
            this.txtBSVanTai.MaxLength = 3;
            this.txtBSVanTai.Name = "txtBSVanTai";
            this.txtBSVanTai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBSVanTai.Size = new System.Drawing.Size(41, 21);
            this.txtBSVanTai.TabIndex = 13;
            this.txtBSVanTai.Text = "0";
            this.txtBSVanTai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBSVanTai.Value = ((uint)(0u));
            this.txtBSVanTai.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBSVanTai.VisualStyleManager = this.vsmMain;
            // 
            // txtBCVanTai
            // 
            this.txtBCVanTai.DecimalDigits = 0;
            this.txtBCVanTai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBCVanTai.Location = new System.Drawing.Point(137, 102);
            this.txtBCVanTai.MaxLength = 3;
            this.txtBCVanTai.Name = "txtBCVanTai";
            this.txtBCVanTai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBCVanTai.Size = new System.Drawing.Size(41, 21);
            this.txtBCVanTai.TabIndex = 12;
            this.txtBCVanTai.Text = "0";
            this.txtBCVanTai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBCVanTai.Value = ((uint)(0u));
            this.txtBCVanTai.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBCVanTai.VisualStyleManager = this.vsmMain;
            // 
            // lblVanTai
            // 
            this.lblVanTai.AutoSize = true;
            this.lblVanTai.BackColor = System.Drawing.Color.Transparent;
            this.lblVanTai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVanTai.Location = new System.Drawing.Point(6, 110);
            this.lblVanTai.Name = "lblVanTai";
            this.lblVanTai.Size = new System.Drawing.Size(61, 13);
            this.lblVanTai.TabIndex = 11;
            this.lblVanTai.Text = "Vận tải đơn";
            // 
            // txtBSHopDong
            // 
            this.txtBSHopDong.DecimalDigits = 0;
            this.txtBSHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSHopDong.Location = new System.Drawing.Point(213, 75);
            this.txtBSHopDong.MaxLength = 3;
            this.txtBSHopDong.Name = "txtBSHopDong";
            this.txtBSHopDong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBSHopDong.Size = new System.Drawing.Size(41, 21);
            this.txtBSHopDong.TabIndex = 9;
            this.txtBSHopDong.Text = "0";
            this.txtBSHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBSHopDong.Value = ((uint)(0u));
            this.txtBSHopDong.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBSHopDong.VisualStyleManager = this.vsmMain;
            // 
            // txtBCHopDong
            // 
            this.txtBCHopDong.DecimalDigits = 0;
            this.txtBCHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBCHopDong.Location = new System.Drawing.Point(137, 75);
            this.txtBCHopDong.MaxLength = 3;
            this.txtBCHopDong.Name = "txtBCHopDong";
            this.txtBCHopDong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBCHopDong.Size = new System.Drawing.Size(41, 21);
            this.txtBCHopDong.TabIndex = 8;
            this.txtBCHopDong.Text = "0";
            this.txtBCHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBCHopDong.Value = ((uint)(0u));
            this.txtBCHopDong.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBCHopDong.VisualStyleManager = this.vsmMain;
            // 
            // lblHopDong
            // 
            this.lblHopDong.AutoSize = true;
            this.lblHopDong.BackColor = System.Drawing.Color.Transparent;
            this.lblHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHopDong.Location = new System.Drawing.Point(6, 83);
            this.lblHopDong.Name = "lblHopDong";
            this.lblHopDong.Size = new System.Drawing.Size(110, 13);
            this.lblHopDong.TabIndex = 7;
            this.lblHopDong.Text = "Hợp đồng thương mại";
            // 
            // txtBSHoaDon
            // 
            this.txtBSHoaDon.DecimalDigits = 0;
            this.txtBSHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSHoaDon.Location = new System.Drawing.Point(213, 50);
            this.txtBSHoaDon.MaxLength = 3;
            this.txtBSHoaDon.Name = "txtBSHoaDon";
            this.txtBSHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBSHoaDon.Size = new System.Drawing.Size(41, 21);
            this.txtBSHoaDon.TabIndex = 5;
            this.txtBSHoaDon.Text = "0";
            this.txtBSHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBSHoaDon.Value = ((uint)(0u));
            this.txtBSHoaDon.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBSHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // txtBCHoaDon
            // 
            this.txtBCHoaDon.DecimalDigits = 0;
            this.txtBCHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBCHoaDon.Location = new System.Drawing.Point(137, 50);
            this.txtBCHoaDon.MaxLength = 3;
            this.txtBCHoaDon.Name = "txtBCHoaDon";
            this.txtBCHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBCHoaDon.Size = new System.Drawing.Size(41, 21);
            this.txtBCHoaDon.TabIndex = 4;
            this.txtBCHoaDon.Text = "0";
            this.txtBCHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtBCHoaDon.Value = ((uint)(0u));
            this.txtBCHoaDon.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtBCHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(194, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Số bản sao";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(120, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số bản chính";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(8, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(72, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Tên chứng từ";
            // 
            // lblHoaDon
            // 
            this.lblHoaDon.AutoSize = true;
            this.lblHoaDon.BackColor = System.Drawing.Color.Transparent;
            this.lblHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoaDon.Location = new System.Drawing.Point(6, 58);
            this.lblHoaDon.Name = "lblHoaDon";
            this.lblHoaDon.Size = new System.Drawing.Size(104, 13);
            this.lblHoaDon.TabIndex = 3;
            this.lblHoaDon.Text = "Hóa đơn thương mại";
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(268, 204);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 1;
            this.btnAddNew.Text = "Thêm";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // openFile
            // 
            this.openFile.FileName = "openFileDialog1";
            this.openFile.Filter = "Image(*.jpg)|*.jpg";
            this.openFile.RestoreDirectory = true;
            // 
            // GiayToForm
            // 
            this.AcceptButton = this.btnAddNew;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(431, 235);
            this.Controls.Add(this.uiGroupBox1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "GiayToForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin chứng từ";
            this.Load += new System.EventHandler(this.ChungTuForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion        

        private ImageList ImageList1;
        private UIButton btnClose;
        private NumericEditBox txtBSHoaDon;
        private NumericEditBox txtBCHoaDon;
        private NumericEditBox txtBSLoaiKhac1;
        private NumericEditBox txtBCLoaiKhac1;
        private NumericEditBox txtBSBanKe;
        private NumericEditBox txtBCBanKe;
        private Label lblBanKe;
        private NumericEditBox txtBSVanTai;
        private NumericEditBox txtBCVanTai;
        private Label lblVanTai;
        private NumericEditBox txtBSHopDong;
        private NumericEditBox txtBCHopDong;
        private Label lblHopDong;
        private EditBox txtLoaiKhac1;
        private EditBox FileLoaiKhac;
        private EditBox FileBanKe;
        private EditBox FileVanTai;
        private EditBox FileHopDong;
        private EditBox FileHoaDon;
        private OpenFileDialog openFile;
    }
}
