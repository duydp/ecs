﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class ErrorInformation
    {
        /// <summary>
        /// Thông tin lỗi định dạng Xml từ hải quan trả về
        /// </summary>        
        [XmlElement("CHECKNODENOTEXIST")]
        public ErrorDescription CheckNoDeNotExist { get; set; }
        /// <summary>
        /// Thông tin bắt buộc phải có
        /// </summary>
        [XmlElement("REQUIRED")]
        public ErrorDescription ReQuired { get; set; }
        /// <summary>
        /// Độ dài tối đa cho phép
        /// </summary>
        [XmlElement("MAXLENGTH")]
        public ErrorDescription MaxLength { get; set; }
        /// <summary>
        /// Kiểu số thực dương
        /// </summary>
        [XmlElement("POSITIVE_DOUBLE")]
        public ErrorDescription PositiveDouble { get; set; }

        [XmlElement("IN")]
        public ErrorDescription IN { get; set; }
        [XmlElement("DATETIME")]
        public ErrorDescription DATETIME { get; set; }

        [XmlElement("LESS_OR_EQUAL_CURRENT_DATETIME")]
        public ErrorDescription LE_CURRENT_DATETIME { get; set; }
        
        [XmlElement("GREATER_OR_EQUAL_CURRENT_DATETIME")]
        public ErrorDescription GE_CURRENT_DATETIME { get; set; }


    }
}
