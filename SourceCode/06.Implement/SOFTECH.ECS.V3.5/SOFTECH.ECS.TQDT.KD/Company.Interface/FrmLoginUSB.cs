﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface
{
    public partial class FrmLoginUSB : BaseForm
    {
        public FrmLoginUSB()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.PasswordSign = txtMatKhau.Text;
                Company.KDT.SHARE.Components.Helpers.GetSignature("Test");
                Company.KDT.SHARE.Components.Globals.IsRememberSignOnline = chIsRemember.Checked;
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
        }
    }
}
