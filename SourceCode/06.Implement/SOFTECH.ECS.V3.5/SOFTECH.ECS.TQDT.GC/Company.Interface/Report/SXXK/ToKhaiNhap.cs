﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiNhap : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewTKNForm report;
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public int temp = 1;
        public bool BanLuuHaiQuan = false;
        public ToKhaiNhap()
        {
            InitializeComponent();
        }
        public void BindReport(bool inMaHang)
        {
            if (this.BanLuuHaiQuan)
            {
                lblBanLuuHaiQuan.Text = "Bản lưu Hải quan";
            }
            else
            {
                lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            }
            xrLabel1.Text = GlobalSettings.TieuDeInDinhMuc;
            lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTriGiaTT = 0;
            DateTime minDate = new DateTime(1900,1,1);
            if (this.TKMD.SoTiepNhan != 0)
                this.lblSoTiepNhan.Text = "Số TNDKDT: "  + this.TKMD.SoTiepNhan;
            lblMaHaiQuan.Text = "";
            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai + "";
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            if (this.TKMD.SoLuongPLTK > 0)
                lblSoPLTK.Text = this.TKMD.SoLuongPLTK.ToString();
            lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep);
            lblTenDoanhNghiep.Text = this.TKMD.TenDoanhNghiep.ToUpper() + "\r\n" + GlobalSettings.DIA_CHI.ToUpper();
            
            lblTenDoiTac.Text = this.TKMD.TenDonViDoiTac;
            lblNguoiUyThac.Text = "";
            lblMaNguoiUyThac.Text = "";
            lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
            lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;
            lblSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            lblSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            lblSoHoaDon.Text = this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai > minDate)
                lblNgayHoaDon.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
            lblSoPTVT.Text = this.TKMD.SoHieuPTVT;
            if (this.TKMD.NgayDenPTVT > minDate)
                lblNgayDenPTVT.Text = this.TKMD.NgayDenPTVT.ToString("dd/MM/yyyy");
            lblSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.NgayVanDon > minDate)
                lblNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
            lblMaNuoc.Text = ToStringForReport(this.TKMD.NuocXK_ID);
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocXK_ID);
            lblDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            lblMaDiaDiemDoHang.Text = ToStringForReport(this.TKMD.CuaKhau_ID);
            lblDiaDiemDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblDKGH.Text = this.TKMD.DKGH_ID;
            lblNgoaiTe.Text = ToStringForReport(this.TKMD.NguyenTe_ID);
            lblTyGiaTT.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
            lblPTTT.Text = this.TKMD.PTTT_ID;
            string st = "";
            if(this.TKMD.PhiBaoHiem > 0)
                st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
            if (this.TKMD.PhiVanChuyen > 0)
                st += " F = " + this.TKMD.PhiVanChuyen.ToString("N2");
            if (this.TKMD.PhiKhac > 0)
                st += " Phí khác = " + this.TKMD.PhiKhac.ToString("N2");
            lblPhiBaoHiem.Text = st;
            lblTrongLuong.Text = "TC " + this.TKMD.SoKien.ToString("n0") + " kiện  " + this.TKMD.TrongLuong + " kg";
            if (this.TKMD.HMDCollection.Count <= 3)
            {
                if (this.TKMD.HMDCollection.Count >= 1)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[0];
                    TenHang1.Text = hmd.TenHang;
                    if (inMaHang) TenHang1.Text += " / " + hmd.MaPhu;
                    MaHS1.Text = hmd.MaHS;
                    XuatXu1.Text = hmd.NuocXX_ID;
                    Luong1.Text = hmd.SoLuong.ToString("G15");
                    DVT1.Text = DonViTinh.GetName(hmd.DVT_ID);
                    DonGiaNT1.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT1.Text = hmd.TriGiaKB.ToString("N2");
                    //TriGiaTT1.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueSuatXNKGiam.Trim() == "")
                    //    ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK1.Text = hmd.ThueSuatXNKGiam;
                    //TienThueXNK1.Text = hmd.ThueXNK.ToString("N0");
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    //tongTienThueXNK += hmd.ThueXNK;
                }
                if (this.TKMD.HMDCollection.Count >= 2)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[1];
                    TenHang2.Text = hmd.TenHang;
                    if (inMaHang) TenHang2.Text += " / " + hmd.MaPhu;
                    MaHS2.Text = hmd.MaHS;
                    XuatXu2.Text = hmd.NuocXX_ID;
                    Luong2.Text = hmd.SoLuong.ToString("G15");
                    DVT2.Text = DonViTinh.GetName(hmd.DVT_ID);
                    DonGiaNT2.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT2.Text = hmd.TriGiaKB.ToString("N2");
                    //TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueSuatXNKGiam.Trim() == "")
                    //    ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK1.Text = hmd.ThueSuatXNKGiam;
                    //TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    //tongTienThueXNK += hmd.ThueXNK;
                }
                if (this.TKMD.HMDCollection.Count == 3)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[2];
                    TenHang3.Text = hmd.TenHang;
                    if (inMaHang) TenHang3.Text += " / " + hmd.MaPhu;
                    MaHS3.Text = hmd.MaHS;
                    XuatXu3.Text = hmd.NuocXX_ID;
                    Luong3.Text = hmd.SoLuong.ToString("G15");
                    DVT3.Text = DonViTinh.GetName(hmd.DVT_ID);
                    DonGiaNT3.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT3.Text = hmd.TriGiaKB.ToString("N2");
                    //TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueSuatXNKGiam.Trim() == "")
                    //    ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK1.Text = hmd.ThueSuatXNKGiam;
                    //TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    //tongTienThueXNK += hmd.ThueXNK;
                }
            }
            else
            {
                TenHang1.Text = "NGUYÊN PHỤ LIỆU GIA CÔNG";
                TenHang2.Text = "(CÓ PHỤ LỤC ĐÍNH KÈM)";
                foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                {
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongTienThueXNK += hmd.ThueXNK;
                    tongTriGiaTT += hmd.TriGiaTT;
                }
                TriGiaNT1.Text = tongTriGiaNT.ToString("N2");
                //TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                //TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
            }
            tongTriGiaNT += this.TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen;
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            //lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
            //lblTongThueXNKSo.Text = this.TinhTongThueHMD().ToString("N0");
            //string s = Company.GC.BLL.Utils.VNCurrency.ToString(this.TinhTongThueHMD()).Trim();
            //s = s[0].ToString().ToUpper() + s.Substring(1);
            //lblTongThueXNKChu.Text = s.Replace("  ", " ");

            XRControl control = new XRControl();
            int i = 5;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.LoaiCT < 5)
                {
                    control = this.Detail.Controls["lblSoBanChinh" + ct.LoaiCT];
                    control.Text = ct.SoBanChinh + "";
                    control = this.Detail.Controls["lblSoBanSao" + ct.LoaiCT];
                    control.Text = ct.SoBanSao + "";
                }
                else
                {
                    if (i == 7) return;
                    control = this.Detail.Controls["lblTenChungTu" + i];
                    control.Text = ct.TenChungTu;
                    control = this.Detail.Controls["lblSoBanChinh" + i];
                    control.Text = ct.SoBanChinh + "";
                    control = this.Detail.Controls["lblSoBanSao" + i];
                    control.Text = ct.SoBanSao + "";
                    i++;
                }
            }
        }
        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                tong += hmd.ThueXNK;
            return tong;
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + "   ";
            temp += s[s.Length-1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            ptbImage.Visible = t;
            lblBanLuuHaiQuan.Visible = t;
        }
        public void setNhomHang(XRTableCell cell, string tenHang)
        {
            cell.Text = tenHang;            
        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

    }
}
