﻿namespace Company.Interface
{
    partial class VNACC_HangMauDichNhapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HangMauDichNhapForm));
            Janus.Windows.GridEX.GridEXLayout grListThueThuKhac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.btnDVT2 = new Janus.Windows.EditControls.UIButton();
            this.linkCopyDongTienThanhToan = new System.Windows.Forms.LinkLabel();
            this.btnDVT1 = new Janus.Windows.EditControls.UIButton();
            this.ctrMaThueNKTheoLuong = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtSoMucKhaiKhoanDC5 = new System.Windows.Forms.NumericUpDown();
            this.txtSoMucKhaiKhoanDC4 = new System.Windows.Forms.NumericUpDown();
            this.txtSoMucKhaiKhoanDC3 = new System.Windows.Forms.NumericUpDown();
            this.txtSoMucKhaiKhoanDC2 = new System.Windows.Forms.NumericUpDown();
            this.txtSoMucKhaiKhoanDC1 = new System.Windows.Forms.NumericUpDown();
            this.ctrNuocXuatXu = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.TabThueNhapKhau = new Janus.Windows.UI.Tab.UITabPage();
            this.ckboxNhapThue = new System.Windows.Forms.CheckBox();
            this.CopyMaMienGiam = new System.Windows.Forms.LinkLabel();
            this.CopyDanhMuc = new System.Windows.Forms.LinkLabel();
            this.CopyBieuThue = new System.Windows.Forms.LinkLabel();
            this.ctrMaBieuThueNK = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtMaHanNgach = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.ctrDV_SL_TrongDonGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDVTDanhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoTTDongHangTKTNTX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDMMienThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDongDMMienThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrMaMienGiam = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTTriGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDVTTuyetDoi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTTuyetDoi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label31 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoLuongTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtThueSuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtThueSuatTuyetDoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThueS = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDieuKhoanMienGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienMienGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.TabThueVaThuKhac = new Janus.Windows.UI.Tab.UITabPage();
            this.grListThueThuKhac = new Janus.Windows.GridEX.GridEX();
            this.ctrDVTDonGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTDonGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaSoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTriGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaQuanLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblMaHang = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkSuaSoLuong = new System.Windows.Forms.LinkLabel();
            this.CopyThueVaThuKhac = new System.Windows.Forms.LinkLabel();
            this.btnXuatExcel = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.TabThueNhapKhau.SuspendLayout();
            this.TabThueVaThuKhac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListThueThuKhac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 661), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 661);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 637);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 637);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(786, 661);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 483);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(786, 178);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Danh sách hàng";
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(0, 17);
            this.grList.Name = "grList";
            this.grList.NewRowPosition = Janus.Windows.GridEX.NewRowPosition.BottomRow;
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.grList.Size = new System.Drawing.Size(786, 158);
            this.grList.TabIndex = 1;
            this.grList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DoubleClick += new System.EventHandler(this.grList_DoubleClick);
            this.grList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_LoadingRow);
            this.grList.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_FormattingRow);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.linkLabel5);
            this.uiGroupBox2.Controls.Add(this.linkLabel4);
            this.uiGroupBox2.Controls.Add(this.linkLabel2);
            this.uiGroupBox2.Controls.Add(this.linkLabel3);
            this.uiGroupBox2.Controls.Add(this.linkLabel1);
            this.uiGroupBox2.Controls.Add(this.btnDVT2);
            this.uiGroupBox2.Controls.Add(this.linkCopyDongTienThanhToan);
            this.uiGroupBox2.Controls.Add(this.btnDVT1);
            this.uiGroupBox2.Controls.Add(this.ctrMaThueNKTheoLuong);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC5);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC4);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC3);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC2);
            this.uiGroupBox2.Controls.Add(this.txtSoMucKhaiKhoanDC1);
            this.uiGroupBox2.Controls.Add(this.ctrNuocXuatXu);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox2.Controls.Add(this.ctrDVTDonGia);
            this.uiGroupBox2.Controls.Add(this.ctrMaTTDonGia);
            this.uiGroupBox2.Controls.Add(this.ctrDVTLuong2);
            this.uiGroupBox2.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox2.Controls.Add(this.ctrMaSoHang);
            this.uiGroupBox2.Controls.Add(this.txtTriGiaHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtDonGiaHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong2);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong1);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label32);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label19);
            this.uiGroupBox2.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox2.Controls.Add(this.txtMaQuanLy);
            this.uiGroupBox2.Controls.Add(this.label34);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.lblMaHang);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(786, 455);
            this.uiGroupBox2.TabIndex = 0;
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel5.Location = new System.Drawing.Point(267, 50);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(95, 13);
            this.linkLabel5.TabIndex = 83;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Chi tiết Mã HS này";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel4.Location = new System.Drawing.Point(388, 108);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(32, 13);
            this.linkLabel4.TabIndex = 83;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Copy";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel2.Location = new System.Drawing.Point(273, 130);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(78, 13);
            this.linkLabel2.TabIndex = 82;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Xoá SL2+DVT2";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel3.Location = new System.Drawing.Point(659, 79);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(87, 12);
            this.linkLabel3.TabIndex = 81;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Cấu hình tính TG";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Location = new System.Drawing.Point(160, 22);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(32, 13);
            this.linkLabel1.TabIndex = 73;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Copy";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // btnDVT2
            // 
            this.btnDVT2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDVT2.ImageIndex = 4;
            this.btnDVT2.Location = new System.Drawing.Point(363, 104);
            this.btnDVT2.Name = "btnDVT2";
            this.btnDVT2.Size = new System.Drawing.Size(21, 20);
            this.btnDVT2.TabIndex = 76;
            this.btnDVT2.Text = "...";
            this.btnDVT2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDVT2.Click += new System.EventHandler(this.btnDVT2_Click);
            // 
            // linkCopyDongTienThanhToan
            // 
            this.linkCopyDongTienThanhToan.AutoSize = true;
            this.linkCopyDongTienThanhToan.BackColor = System.Drawing.Color.Transparent;
            this.linkCopyDongTienThanhToan.Location = new System.Drawing.Point(732, 107);
            this.linkCopyDongTienThanhToan.Name = "linkCopyDongTienThanhToan";
            this.linkCopyDongTienThanhToan.Size = new System.Drawing.Size(32, 13);
            this.linkCopyDongTienThanhToan.TabIndex = 73;
            this.linkCopyDongTienThanhToan.TabStop = true;
            this.linkCopyDongTienThanhToan.Text = "Copy";
            this.linkCopyDongTienThanhToan.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkCopyDongTienThanhToan_LinkClicked);
            // 
            // btnDVT1
            // 
            this.btnDVT1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDVT1.ImageIndex = 4;
            this.btnDVT1.Location = new System.Drawing.Point(363, 75);
            this.btnDVT1.Name = "btnDVT1";
            this.btnDVT1.Size = new System.Drawing.Size(21, 21);
            this.btnDVT1.TabIndex = 76;
            this.btnDVT1.Text = "...";
            this.btnDVT1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDVT1.Click += new System.EventHandler(this.btnDVT1_Click);
            // 
            // ctrMaThueNKTheoLuong
            // 
            this.ctrMaThueNKTheoLuong.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaThueNKTheoLuong.Appearance.Options.UseBackColor = true;
            this.ctrMaThueNKTheoLuong.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A402;
            this.ctrMaThueNKTheoLuong.Code = "";
            this.ctrMaThueNKTheoLuong.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaThueNKTheoLuong.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaThueNKTheoLuong.IsOnlyWarning = false;
            this.ctrMaThueNKTheoLuong.IsValidate = true;
            this.ctrMaThueNKTheoLuong.Location = new System.Drawing.Point(520, 155);
            this.ctrMaThueNKTheoLuong.Name = "ctrMaThueNKTheoLuong";
            this.ctrMaThueNKTheoLuong.Name_VN = "";
            this.ctrMaThueNKTheoLuong.SetOnlyWarning = false;
            this.ctrMaThueNKTheoLuong.SetValidate = false;
            this.ctrMaThueNKTheoLuong.ShowColumnCode = true;
            this.ctrMaThueNKTheoLuong.ShowColumnName = true;
            this.ctrMaThueNKTheoLuong.Size = new System.Drawing.Size(206, 21);
            this.ctrMaThueNKTheoLuong.TabIndex = 14;
            this.ctrMaThueNKTheoLuong.TagCode = "";
            this.ctrMaThueNKTheoLuong.TagName = "";
            this.ctrMaThueNKTheoLuong.Where = null;
            this.ctrMaThueNKTheoLuong.WhereCondition = "";
            // 
            // txtSoMucKhaiKhoanDC5
            // 
            this.txtSoMucKhaiKhoanDC5.Location = new System.Drawing.Point(315, 155);
            this.txtSoMucKhaiKhoanDC5.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC5.Name = "txtSoMucKhaiKhoanDC5";
            this.txtSoMucKhaiKhoanDC5.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC5.TabIndex = 16;
            // 
            // txtSoMucKhaiKhoanDC4
            // 
            this.txtSoMucKhaiKhoanDC4.Location = new System.Drawing.Point(276, 155);
            this.txtSoMucKhaiKhoanDC4.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC4.Name = "txtSoMucKhaiKhoanDC4";
            this.txtSoMucKhaiKhoanDC4.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC4.TabIndex = 15;
            // 
            // txtSoMucKhaiKhoanDC3
            // 
            this.txtSoMucKhaiKhoanDC3.Location = new System.Drawing.Point(237, 155);
            this.txtSoMucKhaiKhoanDC3.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC3.Name = "txtSoMucKhaiKhoanDC3";
            this.txtSoMucKhaiKhoanDC3.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC3.TabIndex = 14;
            // 
            // txtSoMucKhaiKhoanDC2
            // 
            this.txtSoMucKhaiKhoanDC2.Location = new System.Drawing.Point(198, 155);
            this.txtSoMucKhaiKhoanDC2.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC2.Name = "txtSoMucKhaiKhoanDC2";
            this.txtSoMucKhaiKhoanDC2.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC2.TabIndex = 13;
            // 
            // txtSoMucKhaiKhoanDC1
            // 
            this.txtSoMucKhaiKhoanDC1.Location = new System.Drawing.Point(159, 155);
            this.txtSoMucKhaiKhoanDC1.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtSoMucKhaiKhoanDC1.Name = "txtSoMucKhaiKhoanDC1";
            this.txtSoMucKhaiKhoanDC1.Size = new System.Drawing.Size(33, 21);
            this.txtSoMucKhaiKhoanDC1.TabIndex = 12;
            // 
            // ctrNuocXuatXu
            // 
            this.ctrNuocXuatXu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuatXu.Appearance.Options.UseBackColor = true;
            this.ctrNuocXuatXu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrNuocXuatXu.Code = "";
            this.ctrNuocXuatXu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNuocXuatXu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNuocXuatXu.IsOnlyWarning = false;
            this.ctrNuocXuatXu.IsValidate = true;
            this.ctrNuocXuatXu.Location = new System.Drawing.Point(99, 130);
            this.ctrNuocXuatXu.Name = "ctrNuocXuatXu";
            this.ctrNuocXuatXu.Name_VN = "";
            this.ctrNuocXuatXu.SetOnlyWarning = false;
            this.ctrNuocXuatXu.SetValidate = false;
            this.ctrNuocXuatXu.ShowColumnCode = true;
            this.ctrNuocXuatXu.ShowColumnName = true;
            this.ctrNuocXuatXu.Size = new System.Drawing.Size(159, 21);
            this.ctrNuocXuatXu.TabIndex = 10;
            this.ctrNuocXuatXu.TagCode = "";
            this.ctrNuocXuatXu.TagName = "";
            this.ctrNuocXuatXu.Where = null;
            this.ctrNuocXuatXu.WhereCondition = "";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.uiTab1);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 192);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(780, 260);
            this.uiGroupBox5.TabIndex = 75;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 8);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(780, 249);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.TabThueNhapKhau,
            this.TabThueVaThuKhac});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            this.uiTab1.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTab1_SelectedTabChanged_1);
            // 
            // TabThueNhapKhau
            // 
            this.TabThueNhapKhau.Controls.Add(this.ckboxNhapThue);
            this.TabThueNhapKhau.Controls.Add(this.CopyMaMienGiam);
            this.TabThueNhapKhau.Controls.Add(this.CopyDanhMuc);
            this.TabThueNhapKhau.Controls.Add(this.CopyBieuThue);
            this.TabThueNhapKhau.Controls.Add(this.ctrMaBieuThueNK);
            this.TabThueNhapKhau.Controls.Add(this.txtMaHanNgach);
            this.TabThueNhapKhau.Controls.Add(this.txtDonGiaTinhThue);
            this.TabThueNhapKhau.Controls.Add(this.label29);
            this.TabThueNhapKhau.Controls.Add(this.ctrDV_SL_TrongDonGiaTinhThue);
            this.TabThueNhapKhau.Controls.Add(this.ctrMaDVTDanhThue);
            this.TabThueNhapKhau.Controls.Add(this.txtSoTTDongHangTKTNTX);
            this.TabThueNhapKhau.Controls.Add(this.txtSoDMMienThue);
            this.TabThueNhapKhau.Controls.Add(this.txtSoDongDMMienThue);
            this.TabThueNhapKhau.Controls.Add(this.ctrMaMienGiam);
            this.TabThueNhapKhau.Controls.Add(this.ctrMaTTTriGiaTinhThue);
            this.TabThueNhapKhau.Controls.Add(this.ctrMaDVTTuyetDoi);
            this.TabThueNhapKhau.Controls.Add(this.ctrMaTTTuyetDoi);
            this.TabThueNhapKhau.Controls.Add(this.label31);
            this.TabThueNhapKhau.Controls.Add(this.label9);
            this.TabThueNhapKhau.Controls.Add(this.txtSoLuongTinhThue);
            this.TabThueNhapKhau.Controls.Add(this.label12);
            this.TabThueNhapKhau.Controls.Add(this.txtThueSuat);
            this.TabThueNhapKhau.Controls.Add(this.txtThueSuatTuyetDoi);
            this.TabThueNhapKhau.Controls.Add(this.txtTriGiaTinhThueS);
            this.TabThueNhapKhau.Controls.Add(this.txtTriGiaTinhThue);
            this.TabThueNhapKhau.Controls.Add(this.label27);
            this.TabThueNhapKhau.Controls.Add(this.label26);
            this.TabThueNhapKhau.Controls.Add(this.label16);
            this.TabThueNhapKhau.Controls.Add(this.label17);
            this.TabThueNhapKhau.Controls.Add(this.label35);
            this.TabThueNhapKhau.Controls.Add(this.label33);
            this.TabThueNhapKhau.Controls.Add(this.label15);
            this.TabThueNhapKhau.Controls.Add(this.label24);
            this.TabThueNhapKhau.Controls.Add(this.label22);
            this.TabThueNhapKhau.Controls.Add(this.label13);
            this.TabThueNhapKhau.Controls.Add(this.label23);
            this.TabThueNhapKhau.Controls.Add(this.label25);
            this.TabThueNhapKhau.Controls.Add(this.label21);
            this.TabThueNhapKhau.Controls.Add(this.label10);
            this.TabThueNhapKhau.Controls.Add(this.txtDieuKhoanMienGiam);
            this.TabThueNhapKhau.Controls.Add(this.label14);
            this.TabThueNhapKhau.Controls.Add(this.label28);
            this.TabThueNhapKhau.Controls.Add(this.label20);
            this.TabThueNhapKhau.Controls.Add(this.txtSoTienThue);
            this.TabThueNhapKhau.Controls.Add(this.txtSoTienMienGiam);
            this.TabThueNhapKhau.Location = new System.Drawing.Point(1, 21);
            this.TabThueNhapKhau.Name = "TabThueNhapKhau";
            this.TabThueNhapKhau.Size = new System.Drawing.Size(778, 227);
            this.TabThueNhapKhau.TabStop = true;
            this.TabThueNhapKhau.Text = "Thuế nhập khẩu";
            // 
            // ckboxNhapThue
            // 
            this.ckboxNhapThue.AutoSize = true;
            this.ckboxNhapThue.BackColor = System.Drawing.Color.LightPink;
            this.ckboxNhapThue.Location = new System.Drawing.Point(533, 6);
            this.ckboxNhapThue.Name = "ckboxNhapThue";
            this.ckboxNhapThue.Size = new System.Drawing.Size(122, 17);
            this.ckboxNhapThue.TabIndex = 74;
            this.ckboxNhapThue.Text = "Nhập thuế bằng tay";
            this.ckboxNhapThue.UseVisualStyleBackColor = false;
            this.ckboxNhapThue.CheckedChanged += new System.EventHandler(this.ckboxNhapThue_CheckedChanged);
            // 
            // CopyMaMienGiam
            // 
            this.CopyMaMienGiam.AutoSize = true;
            this.CopyMaMienGiam.BackColor = System.Drawing.Color.Transparent;
            this.CopyMaMienGiam.Location = new System.Drawing.Point(338, 182);
            this.CopyMaMienGiam.Name = "CopyMaMienGiam";
            this.CopyMaMienGiam.Size = new System.Drawing.Size(32, 13);
            this.CopyMaMienGiam.TabIndex = 73;
            this.CopyMaMienGiam.TabStop = true;
            this.CopyMaMienGiam.Text = "Copy";
            this.CopyMaMienGiam.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CopyMienGiam_LinkkClicked);
            // 
            // CopyDanhMuc
            // 
            this.CopyDanhMuc.AutoSize = true;
            this.CopyDanhMuc.BackColor = System.Drawing.Color.Transparent;
            this.CopyDanhMuc.Location = new System.Drawing.Point(338, 128);
            this.CopyDanhMuc.Name = "CopyDanhMuc";
            this.CopyDanhMuc.Size = new System.Drawing.Size(32, 13);
            this.CopyDanhMuc.TabIndex = 73;
            this.CopyDanhMuc.TabStop = true;
            this.CopyDanhMuc.Text = "Copy";
            this.CopyDanhMuc.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CopyDanhMuc_LinkkClicked);
            // 
            // CopyBieuThue
            // 
            this.CopyBieuThue.AutoSize = true;
            this.CopyBieuThue.BackColor = System.Drawing.Color.Transparent;
            this.CopyBieuThue.Location = new System.Drawing.Point(455, 6);
            this.CopyBieuThue.Name = "CopyBieuThue";
            this.CopyBieuThue.Size = new System.Drawing.Size(32, 13);
            this.CopyBieuThue.TabIndex = 73;
            this.CopyBieuThue.TabStop = true;
            this.CopyBieuThue.Text = "Copy";
            this.CopyBieuThue.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CopyBieuThue_LinkClicked);
            // 
            // ctrMaBieuThueNK
            // 
            this.ctrMaBieuThueNK.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaBieuThueNK.Appearance.Options.UseBackColor = true;
            this.ctrMaBieuThueNK.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A404;
            this.ctrMaBieuThueNK.Code = "";
            this.ctrMaBieuThueNK.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaBieuThueNK.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaBieuThueNK.IsOnlyWarning = false;
            this.ctrMaBieuThueNK.IsValidate = true;
            this.ctrMaBieuThueNK.Location = new System.Drawing.Point(138, 2);
            this.ctrMaBieuThueNK.Name = "ctrMaBieuThueNK";
            this.ctrMaBieuThueNK.Name_VN = "";
            this.ctrMaBieuThueNK.SetOnlyWarning = false;
            this.ctrMaBieuThueNK.SetValidate = false;
            this.ctrMaBieuThueNK.ShowColumnCode = true;
            this.ctrMaBieuThueNK.ShowColumnName = true;
            this.ctrMaBieuThueNK.Size = new System.Drawing.Size(313, 21);
            this.ctrMaBieuThueNK.TabIndex = 0;
            this.ctrMaBieuThueNK.TagCode = "";
            this.ctrMaBieuThueNK.TagName = "";
            this.ctrMaBieuThueNK.Where = null;
            this.ctrMaBieuThueNK.WhereCondition = "";
            // 
            // txtMaHanNgach
            // 
            this.txtMaHanNgach.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHanNgach.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHanNgach.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHanNgach.Location = new System.Drawing.Point(478, 51);
            this.txtMaHanNgach.MaxLength = 12;
            this.txtMaHanNgach.Name = "txtMaHanNgach";
            this.txtMaHanNgach.Size = new System.Drawing.Size(138, 21);
            this.txtMaHanNgach.TabIndex = 7;
            this.txtMaHanNgach.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHanNgach.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDonGiaTinhThue
            // 
            this.txtDonGiaTinhThue.DecimalDigits = 20;
            this.txtDonGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaTinhThue.Location = new System.Drawing.Point(478, 76);
            this.txtDonGiaTinhThue.MaxLength = 15;
            this.txtDonGiaTinhThue.Name = "txtDonGiaTinhThue";
            this.txtDonGiaTinhThue.ReadOnly = true;
            this.txtDonGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaTinhThue.Size = new System.Drawing.Size(138, 21);
            this.txtDonGiaTinhThue.TabIndex = 9;
            this.txtDonGiaTinhThue.Text = "0";
            this.txtDonGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(11, 128);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(169, 13);
            this.label29.TabIndex = 51;
            this.label29.Text = "Số danh mục miễn thuế xuất khẩu";
            // 
            // ctrDV_SL_TrongDonGiaTinhThue
            // 
            this.ctrDV_SL_TrongDonGiaTinhThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDV_SL_TrongDonGiaTinhThue.Appearance.Options.UseBackColor = true;
            this.ctrDV_SL_TrongDonGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDV_SL_TrongDonGiaTinhThue.Code = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrDV_SL_TrongDonGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDV_SL_TrongDonGiaTinhThue.Enabled = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDV_SL_TrongDonGiaTinhThue.IsOnlyWarning = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.IsValidate = true;
            this.ctrDV_SL_TrongDonGiaTinhThue.Location = new System.Drawing.Point(643, 77);
            this.ctrDV_SL_TrongDonGiaTinhThue.Name = "ctrDV_SL_TrongDonGiaTinhThue";
            this.ctrDV_SL_TrongDonGiaTinhThue.Name_VN = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.SetOnlyWarning = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.SetValidate = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.ShowColumnCode = true;
            this.ctrDV_SL_TrongDonGiaTinhThue.ShowColumnName = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.Size = new System.Drawing.Size(72, 20);
            this.ctrDV_SL_TrongDonGiaTinhThue.TabIndex = 10;
            this.ctrDV_SL_TrongDonGiaTinhThue.TagName = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.Where = null;
            this.ctrDV_SL_TrongDonGiaTinhThue.WhereCondition = "";
            // 
            // ctrMaDVTDanhThue
            // 
            this.ctrMaDVTDanhThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTDanhThue.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTDanhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrMaDVTDanhThue.Code = "";
            this.ctrMaDVTDanhThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTDanhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTDanhThue.Enabled = false;
            this.ctrMaDVTDanhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTDanhThue.IsOnlyWarning = false;
            this.ctrMaDVTDanhThue.IsValidate = true;
            this.ctrMaDVTDanhThue.Location = new System.Drawing.Point(643, 127);
            this.ctrMaDVTDanhThue.Name = "ctrMaDVTDanhThue";
            this.ctrMaDVTDanhThue.Name_VN = "";
            this.ctrMaDVTDanhThue.SetOnlyWarning = false;
            this.ctrMaDVTDanhThue.SetValidate = false;
            this.ctrMaDVTDanhThue.ShowColumnCode = true;
            this.ctrMaDVTDanhThue.ShowColumnName = false;
            this.ctrMaDVTDanhThue.Size = new System.Drawing.Size(72, 20);
            this.ctrMaDVTDanhThue.TabIndex = 15;
            this.ctrMaDVTDanhThue.TagName = "";
            this.ctrMaDVTDanhThue.Where = null;
            this.ctrMaDVTDanhThue.WhereCondition = "";
            // 
            // txtSoTTDongHangTKTNTX
            // 
            this.txtSoTTDongHangTKTNTX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTTDongHangTKTNTX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTTDongHangTKTNTX.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoTTDongHangTKTNTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTTDongHangTKTNTX.Location = new System.Drawing.Point(194, 100);
            this.txtSoTTDongHangTKTNTX.MaxLength = 12;
            this.txtSoTTDongHangTKTNTX.Name = "txtSoTTDongHangTKTNTX";
            this.txtSoTTDongHangTKTNTX.Size = new System.Drawing.Size(138, 21);
            this.txtSoTTDongHangTKTNTX.TabIndex = 8;
            this.txtSoTTDongHangTKTNTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTTDongHangTKTNTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTTDongHangTKTNTX.ButtonClick += new System.EventHandler(this.txtSoTTDongHangTKTNTX_ButtonClick);
            // 
            // txtSoDMMienThue
            // 
            this.txtSoDMMienThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDMMienThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDMMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDMMienThue.Location = new System.Drawing.Point(194, 125);
            this.txtSoDMMienThue.MaxLength = 12;
            this.txtSoDMMienThue.Name = "txtSoDMMienThue";
            this.txtSoDMMienThue.Size = new System.Drawing.Size(138, 21);
            this.txtSoDMMienThue.TabIndex = 9;
            this.txtSoDMMienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDMMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoDongDMMienThue
            // 
            this.txtSoDongDMMienThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDongDMMienThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDongDMMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDongDMMienThue.Location = new System.Drawing.Point(194, 151);
            this.txtSoDongDMMienThue.MaxLength = 12;
            this.txtSoDongDMMienThue.Name = "txtSoDongDMMienThue";
            this.txtSoDongDMMienThue.Size = new System.Drawing.Size(138, 21);
            this.txtSoDongDMMienThue.TabIndex = 10;
            this.txtSoDongDMMienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDongDMMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaMienGiam
            // 
            this.ctrMaMienGiam.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaMienGiam.Appearance.Options.UseBackColor = true;
            this.ctrMaMienGiam.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A520;
            this.ctrMaMienGiam.Code = "";
            this.ctrMaMienGiam.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaMienGiam.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaMienGiam.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaMienGiam.IsOnlyWarning = false;
            this.ctrMaMienGiam.IsValidate = true;
            this.ctrMaMienGiam.Location = new System.Drawing.Point(228, 178);
            this.ctrMaMienGiam.Name = "ctrMaMienGiam";
            this.ctrMaMienGiam.Name_VN = "";
            this.ctrMaMienGiam.SetOnlyWarning = false;
            this.ctrMaMienGiam.SetValidate = false;
            this.ctrMaMienGiam.ShowColumnCode = true;
            this.ctrMaMienGiam.ShowColumnName = false;
            this.ctrMaMienGiam.Size = new System.Drawing.Size(104, 21);
            this.ctrMaMienGiam.TabIndex = 11;
            this.ctrMaMienGiam.TagName = "";
            this.ctrMaMienGiam.Where = null;
            this.ctrMaMienGiam.WhereCondition = "";
            this.ctrMaMienGiam.Leave += new System.EventHandler(this.ctrMaMienGiam_Leave);
            // 
            // ctrMaTTTriGiaTinhThue
            // 
            this.ctrMaTTTriGiaTinhThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTTriGiaTinhThue.Appearance.Options.UseBackColor = true;
            this.ctrMaTTTriGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTriGiaTinhThue.Code = "";
            this.ctrMaTTTriGiaTinhThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTTriGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTriGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTriGiaTinhThue.IsOnlyWarning = false;
            this.ctrMaTTTriGiaTinhThue.IsValidate = true;
            this.ctrMaTTTriGiaTinhThue.Location = new System.Drawing.Point(279, 51);
            this.ctrMaTTTriGiaTinhThue.Name = "ctrMaTTTriGiaTinhThue";
            this.ctrMaTTTriGiaTinhThue.Name_VN = "";
            this.ctrMaTTTriGiaTinhThue.SetOnlyWarning = false;
            this.ctrMaTTTriGiaTinhThue.SetValidate = false;
            this.ctrMaTTTriGiaTinhThue.ShowColumnCode = true;
            this.ctrMaTTTriGiaTinhThue.ShowColumnName = false;
            this.ctrMaTTTriGiaTinhThue.Size = new System.Drawing.Size(79, 21);
            this.ctrMaTTTriGiaTinhThue.TabIndex = 6;
            this.ctrMaTTTriGiaTinhThue.TagName = "";
            this.ctrMaTTTriGiaTinhThue.Where = null;
            this.ctrMaTTTriGiaTinhThue.WhereCondition = "";
            // 
            // ctrMaDVTTuyetDoi
            // 
            this.ctrMaDVTTuyetDoi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTTuyetDoi.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTTuyetDoi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrMaDVTTuyetDoi.Code = "";
            this.ctrMaDVTTuyetDoi.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTTuyetDoi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTTuyetDoi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTTuyetDoi.IsOnlyWarning = false;
            this.ctrMaDVTTuyetDoi.IsValidate = true;
            this.ctrMaDVTTuyetDoi.Location = new System.Drawing.Point(460, 25);
            this.ctrMaDVTTuyetDoi.Name = "ctrMaDVTTuyetDoi";
            this.ctrMaDVTTuyetDoi.Name_VN = "";
            this.ctrMaDVTTuyetDoi.SetOnlyWarning = false;
            this.ctrMaDVTTuyetDoi.SetValidate = false;
            this.ctrMaDVTTuyetDoi.ShowColumnCode = true;
            this.ctrMaDVTTuyetDoi.ShowColumnName = false;
            this.ctrMaDVTTuyetDoi.Size = new System.Drawing.Size(79, 21);
            this.ctrMaDVTTuyetDoi.TabIndex = 3;
            this.ctrMaDVTTuyetDoi.TagName = "";
            this.ctrMaDVTTuyetDoi.Where = null;
            this.ctrMaDVTTuyetDoi.WhereCondition = "";
            // 
            // ctrMaTTTuyetDoi
            // 
            this.ctrMaTTTuyetDoi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTTuyetDoi.Appearance.Options.UseBackColor = true;
            this.ctrMaTTTuyetDoi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTuyetDoi.Code = "";
            this.ctrMaTTTuyetDoi.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTTuyetDoi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTuyetDoi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTuyetDoi.IsOnlyWarning = false;
            this.ctrMaTTTuyetDoi.IsValidate = true;
            this.ctrMaTTTuyetDoi.Location = new System.Drawing.Point(619, 25);
            this.ctrMaTTTuyetDoi.Name = "ctrMaTTTuyetDoi";
            this.ctrMaTTTuyetDoi.Name_VN = "";
            this.ctrMaTTTuyetDoi.SetOnlyWarning = false;
            this.ctrMaTTTuyetDoi.SetValidate = false;
            this.ctrMaTTTuyetDoi.ShowColumnCode = true;
            this.ctrMaTTTuyetDoi.ShowColumnName = false;
            this.ctrMaTTTuyetDoi.Size = new System.Drawing.Size(79, 21);
            this.ctrMaTTTuyetDoi.TabIndex = 4;
            this.ctrMaTTTuyetDoi.TagName = "";
            this.ctrMaTTTuyetDoi.Where = null;
            this.ctrMaTTTuyetDoi.WhereCondition = "";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(11, 155);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(161, 13);
            this.label31.TabIndex = 72;
            this.label31.Text = "Số dòng tương ứng trong DMMT";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(165, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "STT dòng hàng trên tờ khai TNTX";
            // 
            // txtSoLuongTinhThue
            // 
            this.txtSoLuongTinhThue.DecimalDigits = 20;
            this.txtSoLuongTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongTinhThue.Location = new System.Drawing.Point(478, 126);
            this.txtSoLuongTinhThue.MaxLength = 15;
            this.txtSoLuongTinhThue.Name = "txtSoLuongTinhThue";
            this.txtSoLuongTinhThue.ReadOnly = true;
            this.txtSoLuongTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTinhThue.Size = new System.Drawing.Size(138, 21);
            this.txtSoLuongTinhThue.TabIndex = 14;
            this.txtSoLuongTinhThue.Text = "0";
            this.txtSoLuongTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(147, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Mức thuế tuyệt đối";
            // 
            // txtThueSuat
            // 
            this.txtThueSuat.DecimalDigits = 20;
            this.txtThueSuat.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuat.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThueSuat.Location = new System.Drawing.Point(62, 26);
            this.txtThueSuat.MaxLength = 15;
            this.txtThueSuat.Name = "txtThueSuat";
            this.txtThueSuat.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtThueSuat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuat.Size = new System.Drawing.Size(63, 21);
            this.txtThueSuat.TabIndex = 2;
            this.txtThueSuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuat.Value = null;
            this.txtThueSuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatTuyetDoi
            // 
            this.txtThueSuatTuyetDoi.DecimalDigits = 20;
            this.txtThueSuatTuyetDoi.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuatTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatTuyetDoi.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThueSuatTuyetDoi.Location = new System.Drawing.Point(245, 26);
            this.txtThueSuatTuyetDoi.MaxLength = 15;
            this.txtThueSuatTuyetDoi.Name = "txtThueSuatTuyetDoi";
            this.txtThueSuatTuyetDoi.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtThueSuatTuyetDoi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuatTuyetDoi.Size = new System.Drawing.Size(114, 21);
            this.txtThueSuatTuyetDoi.TabIndex = 2;
            this.txtThueSuatTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuatTuyetDoi.Value = null;
            this.txtThueSuatTuyetDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThueS
            // 
            this.txtTriGiaTinhThueS.DecimalDigits = 20;
            this.txtTriGiaTinhThueS.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueS.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThueS.Location = new System.Drawing.Point(478, 99);
            this.txtTriGiaTinhThueS.MaxLength = 15;
            this.txtTriGiaTinhThueS.Name = "txtTriGiaTinhThueS";
            this.txtTriGiaTinhThueS.ReadOnly = true;
            this.txtTriGiaTinhThueS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueS.Size = new System.Drawing.Size(138, 21);
            this.txtTriGiaTinhThueS.TabIndex = 12;
            this.txtTriGiaTinhThueS.Text = "0";
            this.txtTriGiaTinhThueS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueS.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThueS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.DecimalDigits = 20;
            this.txtTriGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(117, 51);
            this.txtTriGiaTinhThue.MaxLength = 15;
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(159, 21);
            this.txtTriGiaTinhThue.TabIndex = 5;
            this.txtTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThue.Value = null;
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(616, 130);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(27, 13);
            this.label27.TabIndex = 68;
            this.label27.Text = "ĐVT";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(377, 130);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(95, 13);
            this.label26.TabIndex = 65;
            this.label26.Text = "Số lượng tính thuế";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(122, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Mã biểu thuế nhập khẩu";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(377, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(103, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Mã ngoài hạn ngạch";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(552, 29);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(58, 13);
            this.label35.TabIndex = 29;
            this.label35.Text = "Mã tiền tệ ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(360, 30);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(98, 13);
            this.label33.TabIndex = 29;
            this.label33.Text = "ĐVT thuế tuyệt đối";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(10, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Thuế suất";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(616, 157);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(28, 13);
            this.label24.TabIndex = 20;
            this.label24.Text = "VNĐ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(616, 80);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 13);
            this.label22.TabIndex = 20;
            this.label22.Text = "VNĐ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(131, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "%";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(377, 157);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 13);
            this.label23.TabIndex = 50;
            this.label23.Text = "Số tiền thuế";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(377, 103);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 13);
            this.label25.TabIndex = 67;
            this.label25.Text = "Trị giá tính thuế(S)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(11, 80);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(141, 13);
            this.label21.TabIndex = 50;
            this.label21.Text = "Số tiền giảm thuế xuất khẩu";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 67;
            this.label10.Text = "Trị giá tính thuế(M)";
            // 
            // txtDieuKhoanMienGiam
            // 
            this.txtDieuKhoanMienGiam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDieuKhoanMienGiam.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDieuKhoanMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDieuKhoanMienGiam.Location = new System.Drawing.Point(332, 204);
            this.txtDieuKhoanMienGiam.MaxLength = 12;
            this.txtDieuKhoanMienGiam.Name = "txtDieuKhoanMienGiam";
            this.txtDieuKhoanMienGiam.ReadOnly = true;
            this.txtDieuKhoanMienGiam.Size = new System.Drawing.Size(366, 21);
            this.txtDieuKhoanMienGiam.TabIndex = 12;
            this.txtDieuKhoanMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDieuKhoanMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(377, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 52;
            this.label14.Text = "Đơn giá tính thuế";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(11, 208);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(315, 13);
            this.label28.TabIndex = 52;
            this.label28.Text = "Điều khoản miễn / Giảm / Không chịu thuế nhập khẩu (Pháp luật)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(11, 182);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(209, 13);
            this.label20.TabIndex = 52;
            this.label20.Text = "Mã miễn/ Giảm Không chịu thuế nhập khẩu";
            // 
            // txtSoTienThue
            // 
            this.txtSoTienThue.DecimalDigits = 20;
            this.txtSoTienThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienThue.Location = new System.Drawing.Point(478, 153);
            this.txtSoTienThue.MaxLength = 15;
            this.txtSoTienThue.Name = "txtSoTienThue";
            this.txtSoTienThue.ReadOnly = true;
            this.txtSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThue.Size = new System.Drawing.Size(138, 21);
            this.txtSoTienThue.TabIndex = 17;
            this.txtSoTienThue.Text = "0";
            this.txtSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienMienGiam
            // 
            this.txtSoTienMienGiam.DecimalDigits = 20;
            this.txtSoTienMienGiam.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienMienGiam.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienMienGiam.Location = new System.Drawing.Point(194, 76);
            this.txtSoTienMienGiam.MaxLength = 15;
            this.txtSoTienMienGiam.Name = "txtSoTienMienGiam";
            this.txtSoTienMienGiam.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtSoTienMienGiam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienMienGiam.Size = new System.Drawing.Size(138, 21);
            this.txtSoTienMienGiam.TabIndex = 7;
            this.txtSoTienMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienMienGiam.Value = null;
            this.txtSoTienMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // TabThueVaThuKhac
            // 
            this.TabThueVaThuKhac.Controls.Add(this.grListThueThuKhac);
            this.TabThueVaThuKhac.Location = new System.Drawing.Point(1, 21);
            this.TabThueVaThuKhac.Name = "TabThueVaThuKhac";
            this.TabThueVaThuKhac.Size = new System.Drawing.Size(778, 227);
            this.TabThueVaThuKhac.TabStop = true;
            this.TabThueVaThuKhac.Text = "Thuế và thu khác";
            // 
            // grListThueThuKhac
            // 
            this.grListThueThuKhac.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListThueThuKhac.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            grListThueThuKhac_DesignTimeLayout.LayoutString = resources.GetString("grListThueThuKhac_DesignTimeLayout.LayoutString");
            this.grListThueThuKhac.DesignTimeLayout = grListThueThuKhac_DesignTimeLayout;
            this.grListThueThuKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListThueThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListThueThuKhac.FrozenColumns = 4;
            this.grListThueThuKhac.GroupByBoxVisible = false;
            this.grListThueThuKhac.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListThueThuKhac.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListThueThuKhac.Location = new System.Drawing.Point(0, 0);
            this.grListThueThuKhac.Name = "grListThueThuKhac";
            this.grListThueThuKhac.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListThueThuKhac.Size = new System.Drawing.Size(778, 227);
            this.grListThueThuKhac.TabIndex = 0;
            this.grListThueThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListThueThuKhac.VisualStyleManager = this.vsmMain;
            this.grListThueThuKhac.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grListThueThuKhac_DeletingRecord);
            this.grListThueThuKhac.AddingRecord += new System.ComponentModel.CancelEventHandler(this.grListThueThuKhac_AddingRecord);
            this.grListThueThuKhac.SelectionChanged += new System.EventHandler(this.grListThueThuKhac_SelectionChanged);
            this.grListThueThuKhac.DropDown += new Janus.Windows.GridEX.ColumnActionEventHandler(this.grListThueThuKhac_DropDown);
            this.grListThueThuKhac.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListThueThuKhac_LoadingRow);
            // 
            // ctrDVTDonGia
            // 
            this.ctrDVTDonGia.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrDVTDonGia.Appearance.Options.UseBackColor = true;
            this.ctrDVTDonGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTDonGia.Code = "";
            this.ctrDVTDonGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTDonGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTDonGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTDonGia.IsOnlyWarning = false;
            this.ctrDVTDonGia.IsValidate = true;
            this.ctrDVTDonGia.Location = new System.Drawing.Point(520, 130);
            this.ctrDVTDonGia.Name = "ctrDVTDonGia";
            this.ctrDVTDonGia.Name_VN = "";
            this.ctrDVTDonGia.SetOnlyWarning = false;
            this.ctrDVTDonGia.SetValidate = false;
            this.ctrDVTDonGia.ShowColumnCode = true;
            this.ctrDVTDonGia.ShowColumnName = false;
            this.ctrDVTDonGia.Size = new System.Drawing.Size(206, 21);
            this.ctrDVTDonGia.TabIndex = 9;
            this.ctrDVTDonGia.TagName = "";
            this.ctrDVTDonGia.Where = null;
            this.ctrDVTDonGia.WhereCondition = "";
            this.ctrDVTDonGia.Leave += new System.EventHandler(this.ctrDVTDonGia_Leave);
            // 
            // ctrMaTTDonGia
            // 
            this.ctrMaTTDonGia.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaTTDonGia.Appearance.Options.UseBackColor = true;
            this.ctrMaTTDonGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTDonGia.Code = "";
            this.ctrMaTTDonGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTDonGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTDonGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTDonGia.IsOnlyWarning = false;
            this.ctrMaTTDonGia.IsValidate = true;
            this.ctrMaTTDonGia.Location = new System.Drawing.Point(661, 104);
            this.ctrMaTTDonGia.Name = "ctrMaTTDonGia";
            this.ctrMaTTDonGia.Name_VN = "";
            this.ctrMaTTDonGia.SetOnlyWarning = false;
            this.ctrMaTTDonGia.SetValidate = false;
            this.ctrMaTTDonGia.ShowColumnCode = true;
            this.ctrMaTTDonGia.ShowColumnName = false;
            this.ctrMaTTDonGia.Size = new System.Drawing.Size(65, 21);
            this.ctrMaTTDonGia.TabIndex = 8;
            this.ctrMaTTDonGia.TagName = "";
            this.ctrMaTTDonGia.Where = null;
            this.ctrMaTTDonGia.WhereCondition = "";
            // 
            // ctrDVTLuong2
            // 
            this.ctrDVTLuong2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVTLuong2.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong2.Code = "";
            this.ctrDVTLuong2.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong2.IsOnlyWarning = false;
            this.ctrDVTLuong2.IsValidate = true;
            this.ctrDVTLuong2.Location = new System.Drawing.Point(286, 104);
            this.ctrDVTLuong2.Name = "ctrDVTLuong2";
            this.ctrDVTLuong2.Name_VN = "";
            this.ctrDVTLuong2.SetOnlyWarning = false;
            this.ctrDVTLuong2.SetValidate = false;
            this.ctrDVTLuong2.ShowColumnCode = true;
            this.ctrDVTLuong2.ShowColumnName = false;
            this.ctrDVTLuong2.Size = new System.Drawing.Size(79, 19);
            this.ctrDVTLuong2.TabIndex = 12;
            this.ctrDVTLuong2.TagName = "";
            this.ctrDVTLuong2.Where = null;
            this.ctrDVTLuong2.WhereCondition = "";
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrDVTLuong1.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsOnlyWarning = false;
            this.ctrDVTLuong1.IsValidate = true;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(286, 75);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.SetOnlyWarning = false;
            this.ctrDVTLuong1.SetValidate = false;
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(79, 21);
            this.ctrDVTLuong1.TabIndex = 5;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.Where = null;
            this.ctrDVTLuong1.WhereCondition = "";
            this.ctrDVTLuong1.Leave += new System.EventHandler(this.ctrDVTLuong1_Leave);
            // 
            // ctrMaSoHang
            // 
            this.ctrMaSoHang.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaSoHang.Appearance.Options.UseBackColor = true;
            this.ctrMaSoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaSoHang.Code = "";
            this.ctrMaSoHang.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaSoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaSoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaSoHang.IsOnlyWarning = false;
            this.ctrMaSoHang.IsValidate = true;
            this.ctrMaSoHang.Location = new System.Drawing.Point(98, 46);
            this.ctrMaSoHang.Name = "ctrMaSoHang";
            this.ctrMaSoHang.Name_VN = "";
            this.ctrMaSoHang.SetOnlyWarning = false;
            this.ctrMaSoHang.SetValidate = false;
            this.ctrMaSoHang.ShowColumnCode = true;
            this.ctrMaSoHang.ShowColumnName = false;
            this.ctrMaSoHang.Size = new System.Drawing.Size(160, 21);
            this.ctrMaSoHang.TabIndex = 3;
            this.ctrMaSoHang.TagName = "";
            this.ctrMaSoHang.Where = null;
            this.ctrMaSoHang.WhereCondition = "";
            // 
            // txtTriGiaHoaDon
            // 
            this.txtTriGiaHoaDon.BackColor = System.Drawing.SystemColors.Info;
            this.txtTriGiaHoaDon.DecimalDigits = 20;
            this.txtTriGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaHoaDon.FormatString = "G20";
            this.txtTriGiaHoaDon.Location = new System.Drawing.Point(520, 75);
            this.txtTriGiaHoaDon.Name = "txtTriGiaHoaDon";
            this.txtTriGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaHoaDon.Size = new System.Drawing.Size(128, 21);
            this.txtTriGiaHoaDon.TabIndex = 6;
            this.txtTriGiaHoaDon.Text = "0";
            this.txtTriGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaHoaDon.Leave += new System.EventHandler(this.txtTriGiaHoaDon_Leave);
            // 
            // txtDonGiaHoaDon
            // 
            this.txtDonGiaHoaDon.BackColor = System.Drawing.SystemColors.Info;
            this.txtDonGiaHoaDon.DecimalDigits = 20;
            this.txtDonGiaHoaDon.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaHoaDon.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaHoaDon.Location = new System.Drawing.Point(520, 103);
            this.txtDonGiaHoaDon.MaxLength = 15;
            this.txtDonGiaHoaDon.Name = "txtDonGiaHoaDon";
            this.txtDonGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaHoaDon.Size = new System.Drawing.Size(128, 21);
            this.txtDonGiaHoaDon.TabIndex = 7;
            this.txtDonGiaHoaDon.Text = "0";
            this.txtDonGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonGiaHoaDon.Leave += new System.EventHandler(this.txtDonGiaHoaDon_Leave);
            // 
            // txtSoLuong2
            // 
            this.txtSoLuong2.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoLuong2.DecimalDigits = 20;
            this.txtSoLuong2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong2.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong2.Location = new System.Drawing.Point(99, 103);
            this.txtSoLuong2.MaxLength = 20;
            this.txtSoLuong2.Name = "txtSoLuong2";
            this.txtSoLuong2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong2.Size = new System.Drawing.Size(119, 21);
            this.txtSoLuong2.TabIndex = 11;
            this.txtSoLuong2.Text = "0";
            this.txtSoLuong2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuong1
            // 
            this.txtSoLuong1.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoLuong1.DecimalDigits = 20;
            this.txtSoLuong1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong1.Location = new System.Drawing.Point(99, 76);
            this.txtSoLuong1.MaxLength = 20;
            this.txtSoLuong1.Name = "txtSoLuong1";
            this.txtSoLuong1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong1.Size = new System.Drawing.Size(119, 21);
            this.txtSoLuong1.TabIndex = 4;
            this.txtSoLuong1.Text = "0";
            this.txtSoLuong1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong1.Leave += new System.EventHandler(this.txtSoLuong1_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(428, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Trị giá hóa đơn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(224, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 69;
            this.label3.Text = "Đơn vị tính";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(428, 134);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 13);
            this.label32.TabIndex = 66;
            this.label32.Text = "ĐVT đơn giá HĐ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(428, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 66;
            this.label7.Text = "Đơn giá hóa đơn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(224, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "Đơn vị tính";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 65;
            this.label5.Text = "Số lượng (2)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 64;
            this.label6.Text = "Số lượng (1)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(14, 157);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(144, 13);
            this.label19.TabIndex = 53;
            this.label19.Text = "Số của mục khoản điều chỉnh";
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHangHoa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHangHoa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangHoa.Location = new System.Drawing.Point(245, 17);
            this.txtMaHangHoa.MaxLength = 50;
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(122, 21);
            this.txtMaHangHoa.TabIndex = 1;
            this.txtMaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHangHoa.TextChanged += new System.EventHandler(this.txtMaHangHoa_TextChanged);
            this.txtMaHangHoa.ButtonClick += new System.EventHandler(this.txtMaQuanLy_ButtonClick);
            this.txtMaHangHoa.Leave += new System.EventHandler(this.txtMaHangHoa_Leave);
            // 
            // txtMaQuanLy
            // 
            this.txtMaQuanLy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaQuanLy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaQuanLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaQuanLy.Location = new System.Drawing.Point(98, 18);
            this.txtMaQuanLy.MaxLength = 12;
            this.txtMaQuanLy.Name = "txtMaQuanLy";
            this.txtMaQuanLy.Size = new System.Drawing.Size(60, 21);
            this.txtMaQuanLy.TabIndex = 0;
            this.txtMaQuanLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaQuanLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaQuanLy.ButtonClick += new System.EventHandler(this.txtMaQuanLy_ButtonClick);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(355, 158);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(160, 13);
            this.label34.TabIndex = 29;
            this.label34.Text = "Mã áp dụng mức thuế tuyệt đối ";
            // 
            // txtTenHang
            // 
            this.txtTenHang.BackColor = System.Drawing.SystemColors.Info;
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(431, 18);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Multiline = true;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(333, 49);
            this.txtTenHang.TabIndex = 2;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(373, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Tên hàng";
            // 
            // lblMaHang
            // 
            this.lblMaHang.AutoSize = true;
            this.lblMaHang.BackColor = System.Drawing.Color.Transparent;
            this.lblMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHang.Location = new System.Drawing.Point(195, 22);
            this.lblMaHang.Name = "lblMaHang";
            this.lblMaHang.Size = new System.Drawing.Size(48, 13);
            this.lblMaHang.TabIndex = 27;
            this.lblMaHang.Text = "Mã hàng";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(13, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Mã số hàng hóa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Mã quản lý riêng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(13, 134);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Nước xuất xứ";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(702, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(612, 5);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 2;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(529, 5);
            this.btnGhi.Name = "btnGhi";
            this.helpProvider1.SetShowHelp(this.btnGhi, true);
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.linkSuaSoLuong);
            this.uiGroupBox3.Controls.Add(this.CopyThueVaThuKhac);
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnXuatExcel);
            this.uiGroupBox3.Controls.Add(this.btnGhi);
            this.uiGroupBox3.Controls.Add(this.btnXoa);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 455);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(786, 28);
            this.uiGroupBox3.TabIndex = 1;
            // 
            // linkSuaSoLuong
            // 
            this.linkSuaSoLuong.AutoSize = true;
            this.linkSuaSoLuong.BackColor = System.Drawing.Color.Transparent;
            this.linkSuaSoLuong.Location = new System.Drawing.Point(133, 10);
            this.linkSuaSoLuong.Name = "linkSuaSoLuong";
            this.linkSuaSoLuong.Size = new System.Drawing.Size(153, 13);
            this.linkSuaSoLuong.TabIndex = 74;
            this.linkSuaSoLuong.TabStop = true;
            this.linkSuaSoLuong.Text = "Điều chỉnh Thông tin hàng hóa";
            this.linkSuaSoLuong.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSuaSoLuong_LinkClicked);
            // 
            // CopyThueVaThuKhac
            // 
            this.CopyThueVaThuKhac.AutoSize = true;
            this.CopyThueVaThuKhac.BackColor = System.Drawing.Color.Transparent;
            this.CopyThueVaThuKhac.Location = new System.Drawing.Point(6, 10);
            this.CopyThueVaThuKhac.Name = "CopyThueVaThuKhac";
            this.CopyThueVaThuKhac.Size = new System.Drawing.Size(116, 13);
            this.CopyThueVaThuKhac.TabIndex = 74;
            this.CopyThueVaThuKhac.TabStop = true;
            this.CopyThueVaThuKhac.Text = "Copy thuế và thu khác";
            this.CopyThueVaThuKhac.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CopyThuKhac_LinkkClicked);
            // 
            // btnXuatExcel
            // 
            this.btnXuatExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuatExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatExcel.Image")));
            this.btnXuatExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXuatExcel.Location = new System.Drawing.Point(391, 5);
            this.btnXuatExcel.Name = "btnXuatExcel";
            this.btnXuatExcel.Size = new System.Drawing.Size(129, 23);
            this.btnXuatExcel.TabIndex = 0;
            this.btnXuatExcel.Text = "Xuất excel hàng";
            this.btnXuatExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatExcel.Click += new System.EventHandler(this.btnXuatExcel_Click);
            // 
            // VNACC_HangMauDichNhapForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(992, 667);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_HangMauDichNhapForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Thông tin hàng hóa";
            this.Load += new System.EventHandler(this.VNACC_HangMauDichNhapForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoMucKhaiKhoanDC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.TabThueNhapKhau.ResumeLayout(false);
            this.TabThueNhapKhau.PerformLayout();
            this.TabThueVaThuKhac.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListThueThuKhac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        public Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label18;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaQuanLy;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label12;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaHanNgach;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuatTuyetDoi;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label16;
        public Janus.Windows.EditControls.UIButton btnClose;
        public Janus.Windows.EditControls.UIButton btnGhi;
        public Janus.Windows.EditControls.UIButton btnXoa;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label19;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienMienGiam;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.Label label29;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaHoaDon;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaHoaDon;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong2;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong1;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label10;
        public Janus.Windows.GridEX.GridEX grList;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTDonGia;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong2;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaSoHang;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTuyetDoi;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTriGiaTinhThue;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoDongDMMienThue;
        public System.Windows.Forms.Label label31;
        public Janus.Windows.GridEX.GridEX grListThueThuKhac;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        public Janus.Windows.UI.Tab.UITab uiTab1;
        public Janus.Windows.UI.Tab.UITabPage TabThueNhapKhau;
        public Janus.Windows.UI.Tab.UITabPage TabThueVaThuKhac;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaTinhThue;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTDanhThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongTinhThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueS;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label label14;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThue;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDV_SL_TrongDonGiaTinhThue;
        public Janus.Windows.GridEX.EditControls.EditBox txtDieuKhoanMienGiam;
        public System.Windows.Forms.Label label28;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrNuocXuatXu;
        public System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC1;
        public System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC5;
        public System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC4;
        public System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC3;
        public System.Windows.Forms.NumericUpDown txtSoMucKhaiKhoanDC2;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaMienGiam;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTDonGia;
        public System.Windows.Forms.Label label32;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaBieuThueNK;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTTuyetDoi;
        public System.Windows.Forms.Label label33;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaThueNKTheoLuong;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label34;
        public Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
        public System.Windows.Forms.Label lblMaHang;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoTTDongHangTKTNTX;
        public Janus.Windows.GridEX.EditControls.EditBox txtSoDMMienThue;
        public Janus.Windows.EditControls.UIButton btnDVT2;
        public Janus.Windows.EditControls.UIButton btnDVT1;
        public System.Windows.Forms.LinkLabel CopyMaMienGiam;
        public System.Windows.Forms.LinkLabel CopyDanhMuc;
        public System.Windows.Forms.LinkLabel CopyBieuThue;
        public System.Windows.Forms.LinkLabel CopyThueVaThuKhac;
        public System.Windows.Forms.LinkLabel linkLabel1;
        public System.Windows.Forms.LinkLabel linkCopyDongTienThanhToan;
        public Janus.Windows.EditControls.UIButton btnXuatExcel;
        public System.Windows.Forms.LinkLabel linkLabel3;
        public System.Windows.Forms.LinkLabel linkLabel2;
        public System.Windows.Forms.LinkLabel linkLabel4;
        public System.Windows.Forms.CheckBox ckboxNhapThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuat;
        public System.Windows.Forms.LinkLabel linkSuaSoLuong;
        public System.Windows.Forms.LinkLabel linkLabel5;
    }
}
