﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX.EditControls;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif
namespace Company.Interface
{
    partial class AddContainerForm
    {
        private UIGroupBox uiGroupBox1;
        private UIButton btnSave;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private EditBox txtSoHieuContainer;
        private UIGroupBox uiGroupBox2;
        private UIComboBox cbTrangThai;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddContainerForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTrongLuongTinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDiaDiemDongHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSaveNew = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbLoaiContainer = new Janus.Windows.EditControls.UIComboBox();
            this.txtSealNo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cmbTinhChatCont = new Janus.Windows.EditControls.UIComboBox();
            this.cbTrangThai = new Janus.Windows.EditControls.UIComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoHieuContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvSoHieu = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSealNo = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSealNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(393, 348);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.btnSaveNew);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(393, 348);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtSoKien);
            this.uiGroupBox3.Controls.Add(this.txtTrongLuongTinh);
            this.uiGroupBox3.Controls.Add(this.txtTrongLuong);
            this.uiGroupBox3.Controls.Add(this.txtDiaDiemDongHang);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(1, 163);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(385, 147);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Kiện hàng trong container";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtSoKien
            // 
            this.txtSoKien.DecimalDigits = 2;
            this.txtSoKien.Location = new System.Drawing.Point(161, 83);
            this.txtSoKien.MaxLength = 10;
            this.txtSoKien.Name = "txtSoKien";
            this.txtSoKien.Size = new System.Drawing.Size(215, 21);
            this.txtSoKien.TabIndex = 2;
            this.txtSoKien.Tag = "TrongLuongTinh";
            this.txtSoKien.Text = "0,00";
            this.txtSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoKien.Value = 0;
            this.txtSoKien.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTrongLuongTinh
            // 
            this.txtTrongLuongTinh.DecimalDigits = 4;
            this.txtTrongLuongTinh.Location = new System.Drawing.Point(161, 56);
            this.txtTrongLuongTinh.MaxLength = 10;
            this.txtTrongLuongTinh.Name = "txtTrongLuongTinh";
            this.txtTrongLuongTinh.Size = new System.Drawing.Size(215, 21);
            this.txtTrongLuongTinh.TabIndex = 1;
            this.txtTrongLuongTinh.Tag = "TrongLuongTinh";
            this.txtTrongLuongTinh.Text = "0,0000";
            this.txtTrongLuongTinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuongTinh.Value = 0;
            this.txtTrongLuongTinh.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTrongLuongTinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 4;
            this.txtTrongLuong.Location = new System.Drawing.Point(161, 26);
            this.txtTrongLuong.MaxLength = 10;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.Size = new System.Drawing.Size(215, 21);
            this.txtTrongLuong.TabIndex = 0;
            this.txtTrongLuong.Tag = "TrongLuongTinh";
            this.txtTrongLuong.Text = "0,0000";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuong.Value = 0;
            this.txtTrongLuong.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTrongLuong.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaDiemDongHang
            // 
            this.txtDiaDiemDongHang.Location = new System.Drawing.Point(161, 110);
            this.txtDiaDiemDongHang.MaxLength = 35;
            this.txtDiaDiemDongHang.Name = "txtDiaDiemDongHang";
            this.txtDiaDiemDongHang.Size = new System.Drawing.Size(215, 21);
            this.txtDiaDiemDongHang.TabIndex = 3;
            this.txtDiaDiemDongHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemDongHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Địa điểm đóng hàng";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Số kiện trong container";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Trọng lượng tịnh hàng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Trọng Lượng hàng";
            // 
            // btnSaveNew
            // 
            this.btnSaveNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSaveNew.Icon")));
            this.btnSaveNew.Location = new System.Drawing.Point(98, 316);
            this.btnSaveNew.Name = "btnSaveNew";
            this.btnSaveNew.Size = new System.Drawing.Size(125, 23);
            this.btnSaveNew.TabIndex = 2;
            this.btnSaveNew.Text = "Lưu và tạo mới";
            this.btnSaveNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSaveNew.VisualStyleManager = this.vsmMain;
            this.btnSaveNew.Click += new System.EventHandler(this.btnSaveNew_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(315, 316);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbLoaiContainer);
            this.uiGroupBox2.Controls.Add(this.txtSealNo);
            this.uiGroupBox2.Controls.Add(this.cmbTinhChatCont);
            this.uiGroupBox2.Controls.Add(this.cbTrangThai);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtSoHieuContainer);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(1, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(385, 154);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // cbLoaiContainer
            // 
            this.cbLoaiContainer.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbLoaiContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Container 20";
            uiComboBoxItem1.Value = "2";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Container 40";
            uiComboBoxItem2.Value = "4";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Container 45";
            uiComboBoxItem3.Value = "5";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Loại container khác";
            uiComboBoxItem4.Value = "9";
            this.cbLoaiContainer.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbLoaiContainer.Location = new System.Drawing.Point(161, 43);
            this.cbLoaiContainer.Name = "cbLoaiContainer";
            this.cbLoaiContainer.Size = new System.Drawing.Size(215, 21);
            this.cbLoaiContainer.TabIndex = 1;
            this.cbLoaiContainer.ValueMember = "ID";
            this.cbLoaiContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbLoaiContainer.VisualStyleManager = this.vsmMain;
            // 
            // txtSealNo
            // 
            this.txtSealNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSealNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSealNo.Location = new System.Drawing.Point(161, 70);
            this.txtSealNo.Name = "txtSealNo";
            this.txtSealNo.Size = new System.Drawing.Size(215, 21);
            this.txtSealNo.TabIndex = 2;
            this.txtSealNo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSealNo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSealNo.VisualStyleManager = this.vsmMain;
            // 
            // cmbTinhChatCont
            // 
            this.cmbTinhChatCont.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cmbTinhChatCont.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Cont thường";
            uiComboBoxItem5.Value = ((sbyte)(1));
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Cont lạnh";
            uiComboBoxItem6.Value = "2";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Cont khác";
            uiComboBoxItem7.Value = ((short)(9));
            this.cmbTinhChatCont.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7});
            this.cmbTinhChatCont.Location = new System.Drawing.Point(161, 124);
            this.cmbTinhChatCont.Name = "cmbTinhChatCont";
            this.cmbTinhChatCont.Size = new System.Drawing.Size(215, 21);
            this.cmbTinhChatCont.TabIndex = 3;
            this.cmbTinhChatCont.ValueMember = "ID";
            this.cmbTinhChatCont.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // cbTrangThai
            // 
            this.cbTrangThai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Đầy";
            uiComboBoxItem8.Value = "1";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Rỗng";
            uiComboBoxItem9.Value = "0";
            this.cbTrangThai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem8,
            uiComboBoxItem9});
            this.cbTrangThai.Location = new System.Drawing.Point(161, 97);
            this.cbTrangThai.Name = "cbTrangThai";
            this.cbTrangThai.Size = new System.Drawing.Size(215, 21);
            this.cbTrangThai.TabIndex = 3;
            this.cbTrangThai.ValueMember = "ID";
            this.cbTrangThai.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbTrangThai.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Seal no";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Loại Container";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 129);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Tính chất";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số hiệu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Trạng thái";
            // 
            // txtSoHieuContainer
            // 
            this.txtSoHieuContainer.Location = new System.Drawing.Point(161, 16);
            this.txtSoHieuContainer.Name = "txtSoHieuContainer";
            this.txtSoHieuContainer.Size = new System.Drawing.Size(215, 21);
            this.txtSoHieuContainer.TabIndex = 0;
            this.txtSoHieuContainer.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHieuContainer.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(229, 316);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvSoHieu
            // 
            this.rfvSoHieu.ControlToValidate = this.txtSoHieuContainer;
            this.rfvSoHieu.ErrorMessage = "\"Số hiệu\" bắt buộc phải nhập.";
            this.rfvSoHieu.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHieu.Icon")));
            this.rfvSoHieu.Tag = "rfvSoHieu";
            // 
            // rfvSealNo
            // 
            this.rfvSealNo.ControlToValidate = this.txtSealNo;
            this.rfvSealNo.ErrorMessage = "\"Seal No\" bắt buộc phải nhập.";
            this.rfvSealNo.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSealNo.Icon")));
            this.rfvSealNo.Tag = "rfvSealNo";
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // AddContainerForm
            // 
            this.AcceptButton = this.btnSaveNew;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(393, 348);
            this.Controls.Add(this.uiGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(399, 365);
            this.Name = "AddContainerForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin Container";
            this.Load += new System.EventHandler(this.AddContainerForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSealNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoHieu;
        private UIButton btnClose;
        private EditBox txtSealNo;
        private IContainer components;
        private ErrorProvider error;
        public Company.Controls.CustomValidation.RequiredFieldValidator rfvSealNo;
        private UIComboBox cbLoaiContainer;
        private UIButton btnSaveNew;
        private UIGroupBox uiGroupBox3;
        private EditBox txtDiaDiemDongHang;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label label5;
        private NumericEditBox txtSoKien;
        private NumericEditBox txtTrongLuongTinh;
        private NumericEditBox txtTrongLuong;
        private UIComboBox cmbTinhChatCont;
        private Label label9;
    }
}
