﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;

namespace Company.GC.BLL.SXXK.ToKhai
{
    public partial class ThongTinDieuChinh
    {
        public static void updateDatabase(DataSet ds, SqlTransaction trangsaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ThongTinDieuChinh tt = new ThongTinDieuChinh();
                tt.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                tt.MaHaiQuan = (row["MA_HQ"].ToString());
                tt.MaLoaiHinh = (row["MA_LH"].ToString());
                tt.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                if(row["NGAY_HL"].ToString()!="")
                    tt.NGAY_HL = Convert.ToDateTime(row["NGAY_HL"].ToString());
                if (row["SNAHAN"].ToString() != "")
                    tt.SNAHAN = Convert.ToInt32(row["SNAHAN"].ToString());
                if (row["SO_CT"].ToString() != "")
                    tt.SoChungTu = (row["SO_CT"].ToString());
                tt.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                tt.THANH_LY = Convert.ToInt32(row["THANH_LY"].ToString());
                tt.InsertUpdateTransaction(trangsaction);
            }
        }
        public static void updateDatabaseMoi(DataSet ds, SqlTransaction trangsaction,DataSet dsToKhaiCu)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();            
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ThongTinDieuChinh tt = new ThongTinDieuChinh();
                tt.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                tt.MaHaiQuan = (row["MA_HQ"].ToString());
                tt.MaLoaiHinh = (row["MA_LH"].ToString());
                tt.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                if (row["NGAY_HL"].ToString() != "")
                    tt.NGAY_HL = Convert.ToDateTime(row["NGAY_HL"].ToString());
                if (row["SNAHAN"].ToString() != "")
                    tt.SNAHAN = Convert.ToInt32(row["SNAHAN"].ToString());
                if (row["SO_CT"].ToString() != "")
                    tt.SoChungTu = (row["SO_CT"].ToString());
                tt.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                tt.THANH_LY = Convert.ToInt32(row["THANH_LY"].ToString());               
                try
                {
                    DataRow[] rowTTDM = dsToKhaiCu.Tables[0].Select("MaLoaiHinh='" + tt.MaLoaiHinh + "' and NamDangKy=" + tt.NamDangKy + " and MaHaiQuan='" + tt.MaHaiQuan + "' and  SoToKhai=" + tt.SoToKhai);
                    if (rowTTDM == null || rowTTDM.Length == 0)
                    tt.InsertTransaction(trangsaction);
                }
                catch { }
            }
        }
        //public void WSGetDanhSachDieuChinhOfToKhai()
        //{
        //    WS.SXXK.SXXKService sxxk = new WS.SXXK.SXXKService();
        //    string[] stXML = sxxk.DieuChinh_GetDanhSachDieuChinhOfToKhai(this.MaHaiQuan, this.MaLoaiHinh, this.SoToKhai, this.NamDangKy);
        //    //load thong tin to khai
        //    DataSet ds = new DataSet();
        //    if(stXML==null)
        //        throw new Exception("Chưa có danh sách điều chỉnh của tờ khai này");
        //    //load thong tin dieu chinh
        //    System.IO.StringReader xmlSR = new System.IO.StringReader(stXML[3]);
        //    ds.ReadXmlSchema(xmlSR);
        //    xmlSR = new System.IO.StringReader(stXML[0]);
        //    ds.ReadXml(xmlSR);
        //    //load thong tin hang dieu chinh
        //    DataSet dsHangDC = new DataSet();
        //    xmlSR = new System.IO.StringReader(stXML[4]);
        //    dsHangDC.ReadXmlSchema(xmlSR);
        //    xmlSR = new System.IO.StringReader(stXML[1]);
        //    dsHangDC.ReadXml(xmlSR);
        //    //load thong tin hagn dieu chinh chi tiet
        //    DataSet dsHangDCCT = new DataSet();
        //    xmlSR = new System.IO.StringReader(stXML[5]);
        //    dsHangDCCT.ReadXmlSchema(xmlSR);
        //    xmlSR = new System.IO.StringReader(stXML[2]);
        //    dsHangDCCT.ReadXml(xmlSR);
        //   SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //   using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //   {
        //       connection.Open();
        //       SqlTransaction transaction = connection.BeginTransaction();
        //       try
        //       {
        //           ThongTinDieuChinh.updateDatabase(ds, transaction);
        //           HangMauDichDieuChinh.updateDatabase(dsHangDC, transaction);
        //           HangMauDichDieuChinhCT.updateDatabase(dsHangDCCT, transaction);
        //           transaction.Commit();
        //       }
        //       catch (Exception ex)
        //       {
        //           transaction.Rollback();
        //           throw new Exception(ex.Message);
        //       }
        //       finally
        //       {
        //           connection.Close();
        //       }
        //   }
        //}

        public static void WSGetDanhSachDieuChinh(string maHaiQuan,string maDoanhNghiep,DataSet ds,DataSet dsHangDC)
        {
           
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    ThongTinDieuChinh.updateDatabase(ds, transaction);
                    HangMauDichDieuChinh.updateDatabase(dsHangDC, transaction);
                    //HangMauDichDieuChinhCT.updateDatabase(dsHangDCCT, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public static void WSGetDanhSachDieuChinhMoi(string maHaiQuan, string maDoanhNghiep, DataSet ds, DataSet dsHangDC)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            DataSet dsToKhaiCu = tkmd.SelectDynamic("madoanhnghiep='" + maDoanhNghiep + "' and mahaiquan='" + maHaiQuan + "'", "");
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    ThongTinDieuChinh.updateDatabaseMoi(ds, transaction,dsToKhaiCu);
                    HangMauDichDieuChinh.updateDatabaseMoi(dsHangDC, transaction,dsToKhaiCu);
                    //HangMauDichDieuChinhCT.updateDatabase(dsHangDCCT, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        
    }
}
