using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class PhuLucToKhaiNhap : DevExpress.XtraReports.UI.XtraReport
    {
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public int SoToKhai;
        public DateTime NgayDangKy;
        public bool trang = false;
        public int _SoTrang;
        /// <summary>
        /// Mien thue NK
        /// </summary>
        public bool MienThue1 = false;
        /// <summary>
        /// Mien thue GTGT
        /// </summary>
        public bool MienThue2 = false;
        public ToKhaiMauDich TKMD;

        public PhuLucToKhaiNhap()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            if (this.TKMD.MaLoaiHinh.Contains("KD") || this.TKMD.MaLoaiHinh.Contains("DT"))
            {
                xrLabel3.Text = this.TKMD.MaLoaiHinh.Substring(1, 2);
            }
            else
                xrLabel3.Text = "";
            this.PrintingSystem.ShowMarginsWarning = false;
            double tongTriGiaNT = 0;
            double tongThueXNK = 0;
            double tongThueGTGT = 0;
            double tongTriGiaThuKhac = 0;
            //lblMaHaiQuan.Text = GlobalSettings.MA_HAI_QUAN;
            if(NgayDangKy > new DateTime(1900,1,1))
                lblNgayDangKy.Text = NgayDangKy.ToString("dd/MM/yyyy");
            if(SoToKhai!=0)
                lblSoToKhai.Text = SoToKhai + "";

            lblMienThue2.Text = GlobalSettings.DonViBaoCao;
            lblMienThue1.Text = GlobalSettings.TieuDeInDinhMuc;
            lblMienThue1.Visible = MienThue1;
            lblMienThue2.Visible = MienThue2;

            // KhanhHN - Biến phân biệt tờ khai có cả 2 loại thuế GTGT và TTDB
            bool TypeTax = false;
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                if (hmd.ThueSuatGTGT > 0 && hmd.ThueSuatTTDB > 0)
                {
                    TypeTax = true;
                    break;
                }
            }    

            for (int i = 0; i < this.HMDCollection.Count; i++)
            {
                XRControl control = new XRControl();
                HangMauDich hmd = this.HMDCollection[i];
                control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                if (hmd.MaPhu != "")
                    control.Text = hmd.TenHang + " /" + hmd.MaPhu;
                else
                    control.Text = hmd.TenHang;

                control = this.xrTable1.Rows[i].Controls["MaHS" + (i + 1)];
                control.Text = hmd.MaHS;

                control = this.xrTable1.Rows[i].Controls["XuatXu" + (i + 1)];
                control.Text = hmd.NuocXX_ID;

                control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                control.Text = hmd.SoLuong.ToString("G20");

                control = this.xrTable1.Rows[i].Controls["DVT" + (i + 1)];
                control.Text = DonViTinh.GetName((object)hmd.DVT_ID);

                if (hmd.FOC)
                {
                    control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("G10");

                    control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                    control.Text = "F.O.C";

                    control = this.xrTable2.Rows[i].Controls["TriGiaTT" + (i + 1)];
                    control.Text = "";


                    control = this.xrTable2.Rows[i].Controls["ThueSuatXNK" + (i + 1)];
                    control.Text = "";


                    control = this.xrTable2.Rows[i].Controls["TienThueXNK" + (i + 1)];
                    control.Text = "";


                    control = this.xrTable2.Rows[i].Controls["TriGiaTTGTGT" + (i + 1)];
                    double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB;
                    control.Text = "";


                    control = this.xrTable2.Rows[i].Controls["ThueSuatGTGT" + (i + 1)];
                    control.Text = "";


                    control = this.xrTable2.Rows[i].Controls["TienThueGTGT" + (i + 1)];
                    control.Text = "";



                    control = this.xrTable2.Rows[i].Controls["TyLeThuKhac" + (i + 1)];

                    control.Text = "";

                    control = this.xrTable2.Rows[i].Controls["TriGiaThuKhac" + (i + 1)];

                    control.Text = "";
                }
                else 
                {
                    control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("G10");

                    control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hmd.TriGiaKB.ToString("N2");

                    control = this.xrTable2.Rows[i].Controls["TriGiaTT" + (i + 1)];
                    control.Text = hmd.TriGiaTT.ToString("N0");
                    if (MienThue1) control.Text = "";

                    control = this.xrTable2.Rows[i].Controls["ThueSuatXNK" + (i + 1)];
                    if (hmd.ThueTuyetDoi)
                    {
                        control.Text = "";
                    }
                    else
                    {
                        if (hmd.ThueSuatXNKGiam.Length == 0)
                            control.Text = hmd.ThueSuatXNK.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatXNKGiam;

                    }
                    if (MienThue1) control.Text = "";
                    control = this.xrTable2.Rows[i].Controls["TienThueXNK" + (i + 1)];
                    control.Text = hmd.ThueXNK.ToString("N0");
                    if (MienThue1) control.Text = "";
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        control = this.xrTable2.Rows[i].Controls["TriGiaTTGTGT" + (i + 1)];
                        double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i].Controls["ThueSuatGTGT" + (i + 1)];
                        if (hmd.ThueSuatVATGiam.Length == 0)
                            control.Text = hmd.ThueSuatGTGT.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatVATGiam;
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i].Controls["TyLeThuKhac" + (i + 1)];

                        control.Text = hmd.TyLeThuKhac.ToString("N0");

                        control = this.xrTable2.Rows[i].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.TriGiaThuKhac.ToString("N0");
                    }
                    else if (hmd.ThueGTGT == 0 && hmd.ThueTTDB > 0)
                    {
                        control = this.xrTable2.Rows[i].Controls["TriGiaTTGTGT" + (i + 1)];
                        double TriGiaTTTTDB= hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i].Controls["ThueSuatGTGT" + (i + 1)];
                        if (hmd.ThueSuatTTDBGiam.Length == 0)
                            control.Text = hmd.ThueSuatTTDB.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatTTDBGiam;
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i].Controls["TyLeThuKhac" + (i + 1)];

                        control.Text = hmd.TyLeThuKhac.ToString("N0");

                        control = this.xrTable2.Rows[i].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.TriGiaThuKhac.ToString("N0");
                    }
                    else if (TypeTax)
                    {
                        control = this.xrTable2.Rows[i].Controls["TriGiaTTGTGT" + (i + 1)];
                        double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i].Controls["ThueSuatGTGT" + (i + 1)];
                        if (hmd.ThueSuatTTDBGiam.Length == 0)
                            control.Text = hmd.ThueSuatTTDB.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatTTDBGiam;
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i].Controls["TyLeThuKhac" + (i + 1)];

                        if (hmd.ThueSuatVATGiam.Length == 0)
                            control.Text = hmd.ThueSuatGTGT.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatVATGiam;
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.ThueGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";
                    }
                }

                if (!hmd.FOC)
                {
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongThueXNK += hmd.ThueXNK;
                    if (TypeTax)
                    {
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                        
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {

                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                }
            }

            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
            lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
            if (tongTriGiaThuKhac > 0)
                lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            else
                lblTongTriGiaThuKhac.Text = "";
            if (MienThue1) lblTongTienThueXNK.Text = "";
            if (MienThue2) lblTongTienThueGTGT.Text = "";

        }
        public void setVisibleImage(bool t)
        {
            xrPictureBox1.Visible = t;
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            lblMienThue2.Visible = t;
            xrTable2.Visible = !t;
            lblTongTienThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = !t;
        }

        private void xrLabel2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (trang)
            {
                xrLabel2.Text = "Trang " + _SoTrang;
            }
        }
    }
}
