﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.VNACCS;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.BLL.KDT.SXXK;
using Company.Interface.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.Interface.Report.GC.TT39;

namespace Company.Interface.KDT.SXXK
{
    public partial class BaoCaoQuyetToanForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
        DataTable dtTotal = new DataTable();
        public bool IsActive = false;
        public bool IsReadExcel = false;
        public int NamQuyetToanOld;
        public int NamQuyetToan;
        public DateTime NgayBatDauBC;
        public DateTime NgayKetThucBC;
        public string NPL;
        public string NPLTemp;
        public string MaNPL;
        public string MaNPLTemp;
        public decimal LuongNhap;
        public decimal LuongTon;
        public decimal LuongXuat;
        public decimal LuongXuatTemp;
        public decimal LuongTonCuoiTemp;
        public decimal LuongXuatTotal;
        public int k = 0;

        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;

        public string LoaiHangHoa = "NPL";

        public BaoCaoQuyetToanForm()
        {
            InitializeComponent();
        }

        private void BaoCaoQuyetToanForm_Load(object sender, EventArgs e)
        {
            dgList.HideColumnsWhenGrouped = InheritableBoolean.True;
            cbbLoaiHinhBaoCao.SelectedIndex = 0;
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            cbbType.SelectedIndex = 0;
            if (IsActive)
            {
                SetGoodItem();
                BindDataGoodItem();
            }
            if (goodItem.ID == 0)
            {
                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            SetCommandStatus();
        }
        private void SetCommandStatus()
        {
            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaBCQT.Enabled = cmdSuaBCQT1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                clcTuNgay.Enabled = false;
                clcDenNgay.Enabled = false;
                btnProcess.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaBCQT.Enabled = cmdSuaBCQT1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = goodItem.SoTN.ToString();
                clcNgayTN.Value = goodItem.NgayTN;
                lblTrangThaiXuLy.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || goodItem.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaBCQT.Enabled = cmdSuaBCQT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                clcTuNgay.Enabled = true;
                clcDenNgay.Enabled = true;
                btnProcess.Enabled = true;
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTN.Value = DateTime.Now;
                lblTrangThaiXuLy.Text = goodItem.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaBCQT.Enabled = cmdSuaBCQT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                clcTuNgay.Enabled = false;
                clcDenNgay.Enabled = false;
                btnProcess.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTN.Text = goodItem.SoTN.ToString();
                clcNgayTN.Value = goodItem.NgayTN;
                lblTrangThaiXuLy.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || goodItem.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || goodItem.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaBCQT.Enabled = cmdSuaBCQT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                clcTuNgay.Enabled = false;
                clcDenNgay.Enabled = false;
                btnProcess.Enabled = false;

                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThaiXuLy.Text = goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaBCQT.Enabled = cmdSuaBCQT.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                clcTuNgay.Enabled = true;
                clcDenNgay.Enabled = true;
                btnProcess.Enabled = true;
                txtSoTN.Text = goodItem.SoTN.ToString();
                clcNgayTN.Value = goodItem.NgayTN;
                lblTrangThaiXuLy.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaBCQT.Enabled = cmdSuaBCQT.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                clcTuNgay.Enabled = true;
                clcDenNgay.Enabled = true;
                btnProcess.Enabled = true;
                txtSoTN.Text = goodItem.SoTN.ToString();
                clcNgayTN.Value = goodItem.NgayTN;
                lblTrangThaiXuLy.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaBCQT.Enabled = cmdSuaBCQT.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                clcTuNgay.Enabled = false;
                clcDenNgay.Enabled = false;
                btnProcess.Enabled = false;

                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = goodItem.SoTN.ToString();
                clcNgayTN.Value = goodItem.NgayTN;
                lblTrangThaiXuLy.Text = goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Not declared") : setText("Chờ duyệt Sửa / Hủy", "Wait for approval");
                this.OpenType = OpenFormType.View;
            }


        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send("Send");
                    break;
                case "cmdEdit":
                    Send("Edit");
                    break;
                case "cmdCancel":
                    if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN KHAI BÁO HỦY BÁO CÁO QUYẾT TOÁN NÀY ĐẾN HQ KHÔNG ? ", true) == "Yes")
                    {
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        goodItem.Update();
                        SetCommandStatus();
                        Send("Cancel");
                    }
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedBack":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    UpdateGuidString();
                    break;
                case "cmdAdd":
                    Add();
                    break;
                case "cmdSuaBCQT":
                    ChuyenTT();
                    break;
                default:
                    break;
            }
        }
        private void ChuyenTT()
        {
            try
            {
                if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN CHUYỂN BÁO CÁO QUYẾT TOÁN NÀY SANG KHAI BÁO SỬA KHÔNG ? ", true) == "Yes")
                {
                    goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    goodItem.Update();
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Add()
        {
            BaoCaoQuyetToan_HangHoaForm f = new BaoCaoQuyetToan_HangHoaForm();
            f.goodItem = goodItem;
            f.LoaiHangHoa = LoaiHangHoa;
            f.OpenType = this.OpenType;
            f.ShowDialog(this);
            BindDataGoodItem();
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(cbbLoaiHinhBaoCao, errorProvider, "LOẠI HÌNH BÁO CÁO", isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(clcNgayBatDauBC, errorProvider, "NGÀY BẮT ĐẦU BÁO CÁO", isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(clcNgayKetThucBC, errorProvider, "NGÀY KẾT THÚC BÁO CÁO", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void GetGoodItem()
        {
            if (goodItem.ID == 0)
                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            goodItem.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            goodItem.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
            goodItem.NgayBatDauBC = clcNgayBatDauBC.Value;
            goodItem.NgayKetThucBC = clcNgayKetThucBC.Value;
            if (!String.IsNullOrEmpty(cbbType.SelectedValue.ToString()))
            {
                goodItem.LoaiSua = Convert.ToInt32(cbbType.SelectedValue.ToString());
            }
            else
            {
                goodItem.LoaiSua = 0;
            }
            goodItem.LoaiBaoCao = Convert.ToDecimal(cbbLoaiHinhBaoCao.SelectedValue.ToString());
            goodItem.GhiChuKhac = txtGhiChuKhac.Text.ToString();
        }
        public void SetGoodItem()
        {
            string TrangThai = goodItem.TrangThaiXuLy.ToString();
            switch (TrangThai)
            {
                case "0":
                    lblTrangThaiXuLy.Text = "Chờ duyệt";
                    break;
                case "-1":
                    lblTrangThaiXuLy.Text = "Chưa khai báo";
                    break;
                case "1":
                    lblTrangThaiXuLy.Text = "Đã duyệt";
                    break;
                case "2":
                    lblTrangThaiXuLy.Text = "Không phê duyệt";
                    break;
                case "5":
                    lblTrangThaiXuLy.Text = "Đang sửa";
                    break;
                case "-3":
                    lblTrangThaiXuLy.Text = "Đã khai báo";
                    break;
                case "10":
                    lblTrangThaiXuLy.Text = "Đã hủy";
                    break;
                case "4":
                    lblTrangThaiXuLy.Text = "Đã khai báo sửa";
                    break;
                case "-2":
                    lblTrangThaiXuLy.Text = "Chờ duyệt khai báo sửa";
                    break;
                case "-4":
                    lblTrangThaiXuLy.Text = "Đã khai báo hủy";
                    break;
                case "11":
                    lblTrangThaiXuLy.Text = "Chờ duyệt khai báo hủy";
                    break;

            }
            txtSoTN.Text = goodItem.SoTN.ToString();
            clcNgayTN.Value = goodItem.NgayTN;
            ctrCoQuanHaiQuan.Code = goodItem.MaHQ;
            cbbLoaiHinhBaoCao.SelectedValue = goodItem.LoaiBaoCao.ToString();
            cbbType.SelectedValue = goodItem.LoaiSua.ToString();
            clcNgayBatDauBC.Value = goodItem.NgayBatDauBC;
            clcNgayKetThucBC.Value = goodItem.NgayKetThucBC;
            txtGhiChuKhac.Text = goodItem.GhiChuKhac;
            goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
        }
        public void Save()
        {
            if (!ValidateForm(false))
                return;
            if (goodItem.GoodItemsCollection.Count < 0)
            {
                ShowMessage("DOANH NGHIỆP CHƯA XỬ LÝ BÁO CÁO ", false);
                return;
            }
            GetGoodItem();
            if (goodItem != null && goodItem.ID > 0)
            {
                goodItemDetail.DeleteBy_GoodItem_ID(goodItem.ID);
            }
            goodItem.InsertUpdateFull();
            BindDataGoodItem();
            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "LƯU THÀNH CÔNG", false);
        }
        private void UpdateGuidString()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New", "", Convert.ToInt32(goodItem.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (cbbLoaiHinhBaoCao.SelectedValue.ToString() == "1")
            {
                ProcessDataNPL();
                GetDataGoodItem();
            }
            else
            {
                ProcessDataSP();
            }
            BindDataGoodItem();
        }
        private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        {
            //Return its own if the column names are empty
            if (columnNamesInDt == string.Empty || groupByColumnNames == string.Empty)
            {
                return dt;
            }

            //Once the columns are added find the distinct rows and group it bu the numbet
            DataTable _dt = dt.DefaultView.ToTable(true, groupByColumnNames);

            //The column names in data table
            string[] _columnNamesInDt = columnNamesInDt.Split(',');

            for (int i = 0; i < _columnNamesInDt.Length; i = i + 1)
            {
                if (_columnNamesInDt[i] != groupByColumnNames)
                {
                    _dt.Columns.Add(_columnNamesInDt[i]);
                }
            }

            //Gets the collection and send it back
            for (int i = 0; i < _dt.Rows.Count; i = i + 1)
            {
                for (int j = 0; j < _columnNamesInDt.Length; j = j + 1)
                {
                    if (_columnNamesInDt[j] != groupByColumnNames)
                    {
                        _dt.Rows[i][j] = dt.Compute(typeOfCalculation + "(" + _columnNamesInDt[j] + ")", groupByColumnNames + " = '" + _dt.Rows[i][groupByColumnNames].ToString() + "'");
                    }
                }
            }

            return _dt;
        }
        public void ProcessDataSP()
        {
            try
            {

                NgayBatDauBC = clcNgayBatDauBC.Value;
                NgayKetThucBC = clcNgayKetThucBC.Value;
                dtTotal = KDT_VNACC_HangMauDich.SelectDynamicGroup(NgayBatDauBC, NgayKetThucBC).Tables[0];
                for (int i = 0; i < dtTotal.Rows.Count; i++)
                {
                    KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                    goodItemDetail.STT = i;
                    goodItemDetail.MaHangHoa = dtTotal.Rows[i]["MAHANGHOA"].ToString();
                    goodItemDetail.TenHangHoa = dtTotal.Rows[i]["TENHANGHOA"].ToString();
                    goodItemDetail.DVT = dtTotal.Rows[i]["DVT"].ToString();
                    goodItemDetail.TonDauKy = 0;
                    goodItemDetail.NhapTrongKy = Convert.ToDecimal(dtTotal.Rows[i]["LUONGNHAP"].ToString());
                    goodItemDetail.TaiXuat = 0;
                    goodItemDetail.ChuyenMDSD = 0;
                    goodItemDetail.XuatKhac = 0;
                    goodItemDetail.XuatTrongKy = Convert.ToDecimal(dtTotal.Rows[i]["LUONGXUAT"].ToString());
                    goodItemDetail.TonCuoiKy = 0;
                    goodItemDetail.GhiChu = "";
                    goodItem.GoodItemsCollection.Add(goodItemDetail);

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ProcessDataNPL()
        {
            int Percen;
            int TotalPercen;
            NamQuyetToan = clcTuNgay.Value.Year;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    T_SXXK_NPL_QUYETTOAN NPLQuyetToan = new T_SXXK_NPL_QUYETTOAN();
                    List<T_SXXK_NPL_QUYETTOAN> NPLQTCollection = new List<T_SXXK_NPL_QUYETTOAN>();
                    T_SXXK_NPL_QUYETTOAN_CHITIET NPLQTChiTiet = new T_SXXK_NPL_QUYETTOAN_CHITIET();
                    List<T_SXXK_NPL_QUYETTOAN_CHITIET> NPLQTChiTietCollection = new List<T_SXXK_NPL_QUYETTOAN_CHITIET>();
                    DataSet ds = new DataSet();
                    dtTotal = new DataTable();
                    DataColumn dtColMANPL = new DataColumn("MANPL");
                    dtColMANPL.DataType = typeof(System.String);
                    DataColumn dtColTENNPL = new DataColumn("TENNPL");
                    dtColTENNPL.DataType = typeof(System.String);
                    DataColumn dtColDVT = new DataColumn("DVT");
                    dtColDVT.DataType = typeof(System.String);
                    DataColumn dtColLUONGTONDK = new DataColumn("LUONGTONDK");
                    dtColLUONGTONDK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGNHAPTK = new DataColumn("LUONGNHAPTK");
                    dtColLUONGNHAPTK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGTAIXUAT = new DataColumn("LUONGTAIXUAT");
                    dtColLUONGTAIXUAT.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONCHUYENMDSD = new DataColumn("LUONGCHUYENMDSD");
                    dtColLUONCHUYENMDSD.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGXUATTK = new DataColumn("LUONGXUATTK");
                    dtColLUONGXUATTK.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGXUATKHAC = new DataColumn("LUONGXUATKHAC");
                    dtColLUONGXUATKHAC.DataType = typeof(System.Decimal);
                    DataColumn dtColLUONGTONCK = new DataColumn("LUONGTONCK");
                    dtColLUONGTONCK.DataType = typeof(System.Decimal);
                    DataColumn dtColNGAYBATDAU = new DataColumn("NGAYBATDAU");
                    dtColNGAYBATDAU.DataType = typeof(System.DateTime);
                    DataColumn dtColNGAYKETTHUC = new DataColumn("NGAYKETTHUC");
                    dtColNGAYKETTHUC.DataType = typeof(System.DateTime);

                    dtTotal.Columns.Add(dtColMANPL);
                    dtTotal.Columns.Add(dtColTENNPL);
                    dtTotal.Columns.Add(dtColDVT);
                    dtTotal.Columns.Add(dtColLUONGTONDK);
                    dtTotal.Columns.Add(dtColLUONGNHAPTK);
                    dtTotal.Columns.Add(dtColLUONGTAIXUAT);
                    dtTotal.Columns.Add(dtColLUONCHUYENMDSD);
                    dtTotal.Columns.Add(dtColLUONGXUATTK);
                    dtTotal.Columns.Add(dtColLUONGTONCK);
                    dtTotal.Columns.Add(dtColLUONGXUATKHAC);
                    dtTotal.Columns.Add(dtColNGAYBATDAU);
                    dtTotal.Columns.Add(dtColNGAYKETTHUC);
                    //Xử lý lượng tồn đầu kỳ
                    #region Xử lý lượng tồn đầu kỳ
                    //NamQuyetToan = Convert.ToInt32(txtNamQT.Text.ToString());
                    //NamQuyetToanOld = NamQuyetToan - 1;
                    NgayBatDauBC = clcNgayBatDauBC.Value;
                    NgayKetThucBC = clcNgayKetThucBC.Value;
                    DataTable dtbNPLQTOld = NPLQuyetToan.SelectDynamic("NGAYKETTHUC ='" + NgayBatDauBC.ToString("yyyy-MM-dd 00:00:00") + "'", "MANPL").Tables[0];
                    DataTable dtbNPLQuyeToan = NPLQuyetToan.SelectDynamic("NGAYBATDAU >='" + NgayBatDauBC.ToString("yyyy-MM-dd 00:00:00") + "' AND NGAYKETTHUC <= '" + NgayKetThucBC.ToString("yyyy-MM-dd 23:59:59") + "'", "MANPL").Tables[0];
                    // Kiểm tra quyết toán là lần đầu tiên hay lần tiếp theo
                    if (dtbNPLQTOld.Rows.Count == 0)
                    {
                        // Lần đầu tiên thì tính toán số liệu tồn đầu kỳ
                        DataTable dtNPLTon = NPLNhapTon.SelectTotalNPLTonByTimes(NgayBatDauBC, NgayKetThucBC).Tables[0];

                        foreach (DataRow dr in dtNPLTon.Rows)
                        {
                            try
                            {

                                DataRow drNPLTon = dtTotal.NewRow();
                                drNPLTon["MANPL"] = dr["MANPL"];
                                drNPLTon["TENNPL"] = dr["TENNPL"].ToString();
                                drNPLTon["DVT"] = dr["DVT"].ToString();
                                drNPLTon["LUONGTONDK"] = dr["LUONGTONDK"];
                                drNPLTon["LUONGNHAPTK"] = 0;
                                drNPLTon["LUONGTAIXUAT"] = 0;
                                drNPLTon["LUONGCHUYENMDSD"] = 0;
                                drNPLTon["LUONGXUATTK"] = 0;
                                drNPLTon["LUONGXUATKHAC"] = 0;
                                drNPLTon["LUONGTONCK"] = 0;
                                drNPLTon["NGAYBATDAU"] = NgayBatDauBC;
                                drNPLTon["NGAYKETTHUC"] = NgayKetThucBC;
                                dtTotal.Rows.Add(drNPLTon);
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                    else
                    {
                        foreach (DataRow dr in dtbNPLQuyeToan.Rows)
                        {
                            try
                            {

                                DataRow drNPLTon = dtTotal.NewRow();
                                drNPLTon["MANPL"] = dr["MANPL"];
                                drNPLTon["TENNPL"] = dr["TENNPL"].ToString();
                                drNPLTon["DVT"] = dr["DVT"].ToString();
                                //Gán lượng tồn cuối kỳ năm QT trước cho tồn đầu kỳ năm QT này
                                drNPLTon["LUONGTONDK"] = drNPLTon["LUONGTONCK"];
                                drNPLTon["LUONGNHAPTK"] = 0;
                                drNPLTon["LUONGTAIXUAT"] = 0;
                                drNPLTon["LUONGCHUYENMDSD"] = 0;
                                drNPLTon["LUONGXUATTK"] = 0;
                                drNPLTon["LUONGXUATKHAC"] = 0;
                                drNPLTon["LUONGTONCK"] = 0;
                                drNPLTon["NGAYBATDAU"] = NgayBatDauBC;
                                drNPLTon["NGAYKETTHUC"] = NgayKetThucBC;
                                dtTotal.Rows.Add(drNPLTon);
                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                    #endregion
                    #region Xử lý lượng nhập trong kỳ
                    DataTable dtNPLNhap = NPLNhapTon.SelectTotalNPLNhapByYear(NamQuyetToan).Tables[0];
                    int j = 0; //Biến đánh dấu Mã NPL nhập có tồn tại trong Table NPL Tổng
                    for (int i = 0; i < dtNPLNhap.Rows.Count; i++)
                    {
                        MaNPL = dtNPLNhap.Rows[i]["MANPL"].ToString();
                        for (int k = 0; k < dtTotal.Rows.Count; k++)
                        {
                            MaNPLTemp = dtTotal.Rows[k]["MANPL"].ToString();
                            if (MaNPL == MaNPLTemp)
                            {
                                j = 1;
                                dtTotal.Rows[k]["LUONGNHAPTK"] = dtNPLNhap.Rows[i]["LUONGNHAPTK"].ToString();
                            }
                        }
                        if (j == 0)
                        {
                            try
                            {
                                DataRow drNPLNhap = dtTotal.NewRow();
                                drNPLNhap["MANPL"] = dtNPLNhap.Rows[i]["MANPL"].ToString();
                                drNPLNhap["TENNPL"] = dtNPLNhap.Rows[i]["TENNPL"].ToString();
                                drNPLNhap["DVT"] = dtNPLNhap.Rows[i]["DVT"].ToString();
                                drNPLNhap["LUONGTONDK"] = 0;
                                drNPLNhap["LUONGNHAPTK"] = dtNPLNhap.Rows[i]["LUONGNHAPTK"].ToString();
                                drNPLNhap["LUONGTAIXUAT"] = 0;
                                drNPLNhap["LUONGCHUYENMDSD"] = 0;
                                drNPLNhap["LUONGXUATKHAC"] = 0;
                                drNPLNhap["LUONGXUATTK"] = 0;
                                drNPLNhap["LUONGTONCK"] = 0;
                                drNPLNhap["NGAYBATDAU"] = NgayBatDauBC;
                                drNPLNhap["NGAYKETTHUC"] = NgayKetThucBC;
                                dtTotal.Rows.Add(drNPLNhap);

                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                    #endregion
                    #region Xử lý NPL xuất trong kỳ chi tiết và tổng hợp
                    DataTable dtNPLXuat = BCXuatNhapTon.SelectNPLXuatDetailByYear(NamQuyetToan);
                    DataTable dtNPLXuatToTal = dtTotal.Clone();
                    DataTable dtNPLXuatDetail = dtNPLXuat.Clone();
                    //Biến đánh dấu NPL bị âm (1 : Âm , 0 : Dương )
                    for (int i = 0; i < dtNPLXuat.Rows.Count; i++)
                    {
                        try
                        {
                            MaNPL = dtNPLXuat.Rows[i]["MANPL"].ToString();
                            if (i + 1 < dtNPLXuat.Rows.Count)
                            {
                                MaNPLTemp = dtNPLXuat.Rows[i + 1]["MANPL"].ToString();
                            }
                            else
                            {
                                MaNPLTemp = dtNPLXuat.Rows[i - 1]["MANPL"].ToString();
                            }
                            LuongNhap = Convert.ToDecimal(dtNPLXuat.Rows[i]["LUONGNHAP"].ToString());
                            LuongXuat = Convert.ToDecimal(dtNPLXuat.Rows[i]["LUONGXUAT"].ToString());
                            LuongTon = Convert.ToDecimal(dtNPLXuat.Rows[i]["LUONGTONCUOI"].ToString());
                            if (MaNPLTemp == MaNPL)
                            {
                                if (LuongTon >= 0)
                                {
                                    DataRow dr = dtNPLXuatDetail.NewRow();
                                    dr["TOKHAIXUAT"] = dtNPLXuat.Rows[i]["TOKHAIXUAT"].ToString();
                                    dr["TOKHAINHAP"] = dtNPLXuat.Rows[i]["TOKHAINHAP"].ToString();
                                    dr["MANPL"] = MaNPL;
                                    dr["TENNPL"] = dtNPLXuat.Rows[i]["TENNPL"].ToString();
                                    dr["DVT_NPL"] = dtNPLXuat.Rows[i]["DVT_NPL"].ToString();
                                    dr["MASP"] = dtNPLXuat.Rows[i]["MASP"].ToString(); ;
                                    dr["TENSP"] = dtNPLXuat.Rows[i]["TENSP"].ToString();
                                    dr["DVT_SP"] = dtNPLXuat.Rows[i]["DVT_SP"].ToString();
                                    dr["LUONGNHAP"] = LuongNhap;
                                    if (k == 0)
                                    {
                                        dr["LUONGXUAT"] = LuongXuat;

                                    }
                                    else
                                    {
                                        dr["LUONGXUAT"] = LuongTonCuoiTemp;//LuongXuat - LuongXuatTemp;
                                    }
                                    dr["LUONGTONCUOI"] = LuongTon;
                                    dtNPLXuatDetail.Rows.Add(dr);
                                    LuongXuatTemp = LuongTon;
                                    k = 0;
                                }
                                else
                                {
                                    DataRow dr = dtNPLXuatDetail.NewRow();
                                    dr["TOKHAIXUAT"] = dtNPLXuat.Rows[i]["TOKHAIXUAT"].ToString();
                                    dr["TOKHAINHAP"] = dtNPLXuat.Rows[i]["TOKHAINHAP"].ToString();
                                    dr["MANPL"] = MaNPL;
                                    dr["TENNPL"] = dtNPLXuat.Rows[i]["TENNPL"].ToString();
                                    dr["DVT_NPL"] = dtNPLXuat.Rows[i]["DVT_NPL"].ToString();
                                    dr["MASP"] = dtNPLXuat.Rows[i]["MASP"].ToString(); ;
                                    dr["TENSP"] = dtNPLXuat.Rows[i]["TENSP"].ToString();
                                    dr["DVT_SP"] = dtNPLXuat.Rows[i]["DVT_SP"].ToString();
                                    dr["LUONGNHAP"] = LuongNhap;
                                    dr["LUONGXUAT"] = LuongXuatTemp;
                                    dr["LUONGTONCUOI"] = 0;
                                    k = 1;//Đánh dấu NPL bị âm 
                                    dtNPLXuatDetail.Rows.Add(dr);
                                    LuongTonCuoiTemp = LuongTon * -1;
                                }
                            }
                            else
                            {
                                if (LuongTon >= 0)
                                {
                                    DataRow dr = dtNPLXuatDetail.NewRow();
                                    dr["TOKHAIXUAT"] = dtNPLXuat.Rows[i]["TOKHAIXUAT"].ToString();
                                    dr["TOKHAINHAP"] = dtNPLXuat.Rows[i]["TOKHAINHAP"].ToString();
                                    dr["MANPL"] = MaNPL;
                                    dr["TENNPL"] = dtNPLXuat.Rows[i]["TENNPL"].ToString();
                                    dr["DVT_NPL"] = dtNPLXuat.Rows[i]["DVT_NPL"].ToString();
                                    dr["MASP"] = dtNPLXuat.Rows[i]["MASP"].ToString(); ;
                                    dr["TENSP"] = dtNPLXuat.Rows[i]["TENSP"].ToString();
                                    dr["DVT_SP"] = dtNPLXuat.Rows[i]["DVT_SP"].ToString();
                                    dr["LUONGNHAP"] = LuongNhap;
                                    dr["LUONGXUAT"] = LuongXuat;
                                    dr["LUONGTONCUOI"] = LuongTon;
                                    dtNPLXuatDetail.Rows.Add(dr);
                                    k = 0;
                                }
                                else
                                {
                                    DataRow dr = dtNPLXuatDetail.NewRow();
                                    dr["TOKHAIXUAT"] = dtNPLXuat.Rows[i]["TOKHAIXUAT"].ToString();
                                    dr["TOKHAINHAP"] = dtNPLXuat.Rows[i]["TOKHAINHAP"].ToString();
                                    dr["MANPL"] = MaNPL;
                                    dr["TENNPL"] = dtNPLXuat.Rows[i]["TENNPL"].ToString();
                                    dr["DVT_NPL"] = dtNPLXuat.Rows[i]["DVT_NPL"].ToString();
                                    dr["MASP"] = dtNPLXuat.Rows[i]["MASP"].ToString(); ;
                                    dr["TENSP"] = dtNPLXuat.Rows[i]["TENSP"].ToString();
                                    dr["DVT_SP"] = dtNPLXuat.Rows[i]["DVT_SP"].ToString();
                                    dr["LUONGNHAP"] = LuongNhap;
                                    dr["LUONGXUAT"] = LuongTon;
                                    dr["LUONGTONCUOI"] = 0;
                                    k = 1; // Đánh dấu dòng NPL bị âm
                                    LuongXuatTemp = LuongTon;
                                    dtNPLXuatDetail.Rows.Add(dr);
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            StreamWriter write = File.AppendText("Error.txt");
                            write.WriteLine("--------------------------------");
                            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                            write.WriteLine(ex.StackTrace);
                            write.WriteLine("Lỗi là : ");
                            write.WriteLine(ex.Message);
                            write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + MaNPL.ToString());
                            write.WriteLine("--------------------------------");
                            write.Flush();
                            write.Close();
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                    dgListNPLXuatDetails.Refresh();
                    dgListNPLXuatDetails.DataSource = dtNPLXuatDetail;
                    dgListNPLXuatDetails.Refetch();
                    dtNPLXuatToTal = GetGroupedBy(dtNPLXuatDetail, "MANPL,LUONGXUAT", "MANPL", "Sum");
                    // Xử lý lượng xuất trong kỳ
                    for (int i = 0; i < dtNPLXuatToTal.Rows.Count; i++)
                    {
                        MaNPL = dtNPLXuatToTal.Rows[i]["MANPL"].ToString();
                        for (int l = 0; l < dtTotal.Rows.Count; l++)
                        {
                            MaNPLTemp = dtTotal.Rows[l]["MANPL"].ToString();
                            if (MaNPL == MaNPLTemp)
                            {
                                j = 1;
                                dtTotal.Rows[l]["LUONGXUATTK"] = dtNPLXuatToTal.Rows[i]["LUONGXUAT"].ToString();
                            }
                        }
                        if (j == 0)
                        {
                            try
                            {
                                DataRow drNPLXuat = dtTotal.NewRow();
                                drNPLXuat["MANPL"] = dtNPLXuatToTal.Rows[i]["MANPL"].ToString();
                                for (int m = 0; m < dtNPLXuatDetail.Rows.Count; m++)
                                {
                                    MaNPLTemp = dtNPLXuatDetail.Rows[m]["MANPL"].ToString();
                                    if (MaNPL == MaNPLTemp)
                                    {
                                        drNPLXuat["TENNPL"] = dtNPLXuatDetail.Rows[m]["TENNPL"].ToString();
                                        drNPLXuat["DVT"] = dtNPLXuatDetail.Rows[m]["DVT_NPL"].ToString();
                                    }
                                }
                                drNPLXuat["LUONGTONDK"] = 0;
                                drNPLXuat["LUONGNHAPTK"] = 0;
                                drNPLXuat["LUONGTAIXUAT"] = 0;
                                drNPLXuat["LUONGCHUYENMDSD"] = 0;
                                drNPLXuat["LUONGXUATKHAC"] = 0;
                                drNPLXuat["LUONGXUATTK"] = dtNPLXuatToTal.Rows[i]["LUONGXUAT"].ToString();
                                drNPLXuat["LUONGTONCK"] = 0;
                                drNPLXuat["NGAYBATDAU"] = NgayBatDauBC;
                                drNPLXuat["NGAYKETTHUC"] = NgayKetThucBC;
                                dtTotal.Rows.Add(drNPLXuat);

                            }
                            catch (Exception ex)
                            {
                                StreamWriter write = File.AppendText("Error.txt");
                                write.WriteLine("--------------------------------");
                                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                                write.WriteLine(ex.StackTrace);
                                write.WriteLine("Lỗi là : ");
                                write.WriteLine(ex.Message);
                                write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                                write.WriteLine("--------------------------------");
                                write.Flush();
                                write.Close();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                    #endregion
                    this.Cursor = Cursors.Default;
                    transaction.Commit();
                    MLMessages("XỬ LÝ DỮ LIỆU THÀNH CÔNG ", "MSG_WRN34", "", false);
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    transaction.Rollback();
                    MLMessages("XỬ LÝ DỮ LIỆU KHÔNG THÀNH CÔNG ", "MSG_WRN34", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void GetDataGoodItem()
        {
            goodItem.GoodItemsCollection.Clear();
            DataRow[] drTotal = new DataRow[dtTotal.Rows.Count];
            dtTotal.Rows.CopyTo(drTotal, 0);
            int k = 0;
            for (int i = 0; i < dtTotal.Rows.Count; i++)
            {
                goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();

                goodItemDetail.TenHangHoa = drTotal[i]["TENNPL"].ToString();
                goodItemDetail.MaHangHoa = drTotal[i]["MANPL"].ToString();
                goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(drTotal[i]["DVT"].ToString()));
                decimal TonDauKy = 0;
                decimal NhapTrongKy = 0;
                decimal TaiXuat = 0;
                decimal ChuyenMDSD = 0;
                decimal XuatKhac = 0;
                decimal XuatTrongKy = 0;
                decimal TonCuoiKy = 0;
                TonDauKy = Convert.ToDecimal(drTotal[i]["LUONGTONDK"].ToString());
                NhapTrongKy = Convert.ToDecimal(drTotal[i]["LUONGNHAPTK"].ToString());
                TaiXuat = Convert.ToDecimal(drTotal[i]["LUONGTAIXUAT"].ToString());
                ChuyenMDSD = Convert.ToDecimal(drTotal[i]["LUONGCHUYENMDSD"].ToString());
                XuatKhac = Convert.ToDecimal(drTotal[i]["LUONGXUATKHAC"].ToString());
                XuatTrongKy = Convert.ToDecimal(drTotal[i]["LUONGXUATTK"].ToString());
                TonCuoiKy = Convert.ToDecimal(drTotal[i]["LUONGTONCK"].ToString());

                goodItemDetail.TonDauKy = TonDauKy;
                goodItemDetail.NhapTrongKy = NhapTrongKy;
                goodItemDetail.TaiXuat = TaiXuat;
                goodItemDetail.ChuyenMDSD = ChuyenMDSD;
                goodItemDetail.XuatKhac = XuatKhac;
                goodItemDetail.XuatTrongKy = XuatTrongKy;
                goodItemDetail.TonCuoiKy = TonDauKy + NhapTrongKy - TaiXuat - ChuyenMDSD - XuatKhac - XuatTrongKy;

                goodItemDetail.GhiChu = "";//dr["GHICHU"].ToString();
                if (TonDauKy == 0 && NhapTrongKy == 0 && XuatTrongKy == 0)
                {
                    k++;
                }
                else
                {
                    goodItemDetail.STT = i + 1 - k;
                    goodItem.GoodItemsCollection.Add(goodItemDetail);
                }
            }
        }
        public void BindDataGoodItem()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItem.GoodItemsCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (LoaiHangHoa == "NPL")
            {
                Report_15_BCQT_NVL_GSQL report = new Report_15_BCQT_NVL_GSQL();
                report.BindReport(goodItem);
                report.ShowPreview();
            }
            else
            {
                Report_15a_BCQT_SP_GSQL report = new Report_15a_BCQT_SP_GSQL();
                report.BindReport(goodItem);
                report.ShowPreview();
            }
        }
        public void Send(string Status)
        {
            if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "DOANH NGHIỆP CÓ MUỐN KHAI BÁO CÁO QUYẾT TOÁN NÀY ĐẾN HQ KHÔNG ?", true) == "Yes")
            {
                if (goodItem.ID == 0)
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "DOANH NGHIỆP HÃY LƯU THÔNG TIN TRƯỚC KHI KHAI BÁO ", false);
                    return;
                }
                else
                {
                    goodItem = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New.Load(goodItem.ID);
                    goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                sendXML.master_id = goodItem.ID;

                if (sendXML.Load())
                {
                    if (Status == "Send")
                    {
                        if (goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                        {
                            ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "THÔNG TIN ĐÃ ĐƯỢC GỬI ĐẾN HẢI QUAN. NHẤN [LẤY PHẢN HỒI] ĐỂ LẤY THÔNG TIN", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    goodItem.GuidStr = Guid.NewGuid().ToString();
                    GoodsItems_VNACCS goodsItem_VNACCS = new GoodsItems_VNACCS();
                    goodsItem_VNACCS = Mapper_V4.ToDataTransferGoodsItemNew(goodItem, goodItemDetail, GlobalSettings.DIA_CHI, GlobalSettings.TEN_DON_VI, Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = goodItem.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ)),
                              Identity = goodItem.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = goodsItem_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = goodItem.GuidStr,
                          },
                          goodsItem_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem);
                        cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                        sendXML.master_id = goodItem.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            goodItem.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            SetCommandStatus();
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            goodItem.Update();
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem);
                        ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
            sendXML.master_id = goodItem.ID;
            if (!sendXML.Load())
            {

                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN HOẶC HỆ THỐNG HQ BỊ QUÁ TẢI KHÔNG NHẬN ĐƯỢC PHẢN HỒI . NẾU TRƯỜNG HỢP HỆ THỐNG HQ BỊ QUÁ TẢI BẠN CHỌN YES ĐỂ TẠO MESSAGE NHẬN PHẢN HỒI . CHỌN NO ĐỂ BỎ QUA", true) == "Yes")
                {
                    if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                    {
                        sendXML.func = Convert.ToInt32(DeclarationFunction.HOI_TRANG_THAI);
                    }
                    else
                    {
                        sendXML.msg = String.Empty;
                        sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                        sendXML.InsertUpdate();
                    }
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
                    return;
                }
            }
            while (isFeedBack)
            {
                string reference = goodItem.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.GoodsItem_SXXK,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.GoodsItem_SXXK,
                };
                subjectBase.Type = DeclarationIssuer.GoodsItem_SXXK;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = goodItem.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ.Trim())),
                                                  Identity = goodItem.MaHQ
                                              }, subjectBase, null);
                if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                while (isFeedBack)
                {
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    if (isFeedBack)
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            goodItem.Update();
                            isFeedBack = true;
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            goodItem.Update();
                            isFeedBack = false;
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            goodItem.Update();
                            isFeedBack = false;
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG", true) == "Yes";
                        }
                    }
                }
            }
        }
        public void Result()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = goodItem.ID;
            form.DeclarationIssuer = DeclarationIssuer.GoodsItem_SXXK;
            form.ShowDialog(this);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.GoodItemSendHandlerNew(goodItem, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void btnReadExcel_Click(object sender, EventArgs e)
        {
            IsReadExcel = true;
            ReadExcelFormNPL f = new ReadExcelFormNPL();
            f.goodItemNew = goodItem;
            f.ShowDialog(this);
            BindDataGoodItem();
        }

        private void uiTab1_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {

        }
        private void ExportExcel(string FileName, GridEX dg)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = FileName + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dg;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG ", "DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void mnuExportBCQT_Click(object sender, EventArgs e)
        {
            ExportExcel("TỔNG HỢP SỐ LIỆU BÁO CÁO QUYẾT TOÁN ", dgListTotal);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            ExportExcel("TỔNG HỢP SỐ LIỆU CHI TIẾT XUẤT NPL TRONG KỲ ", dgListNPLXuatDetails);
        }

        private void cbbLoaiHinhBaoCao_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbbLoaiHinhBaoCao.SelectedValue.ToString() == "1")
            {
                LoaiHangHoa = "NPL";
            }
            else
            {
                LoaiHangHoa = "SP";
            }
        }

        private void linkLabel1_Leave(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(Application.StartupPath + "\\ExcelTemplate\\Mau 15, 15a, 15b, 15c - BCQT.xlsx");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgListTotal_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            Add();
        }
    }
}
