using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class SelectSanPhamInDMForm : BaseForm
    {
        public BLL.SXXK.SanPhamCollection spCollectionSelect = new SanPhamCollection();
        public SanPhamCollection spCollection = new SanPhamCollection();
        SanPhamCollection spTMPCollection = new SanPhamCollection();
        BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
        public SelectSanPhamInDMForm()
        {
            InitializeComponent();
        }
        private void SelectSanPhamInDMForm_Load(object sender, EventArgs e)
        {
            spCollection = sp.getSanPhamCoDinhMuc(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            dgList.DataSource = spCollection;
            dgList.Refetch();
            dgListSelect.DataSource = spCollectionSelect;
            dgListSelect.Refetch();
        }
        private void RemoveSP(SanPham sp, SanPhamCollection spCollection)
        {
            foreach (SanPham spDelete in spCollection)
            {
                if (spDelete.Ma == sp.Ma)
                {
                    spCollection.Remove(spDelete);
                    break;
                }
            }
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            spTMPCollection.Clear();
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    SanPham sp = (SanPham)row.DataRow;
                    SanPham spSelect = new SanPham();
                    spSelect.Ma = sp.Ma;
                    spSelect.Ten = sp.Ten;
                    spSelect.DVT_ID = sp.DVT_ID;
                    spSelect.MaHS = sp.MaHS;
                    spCollectionSelect.Add(spSelect);
                    spTMPCollection.Add(spSelect);
                }
            }
            foreach (SanPham sp in spTMPCollection)
            {
                RemoveSP(sp, spCollection);
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

            dgListSelect.Refetch();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            spTMPCollection.Clear();
            foreach (GridEXRow row in dgListSelect.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    SanPham sp = (SanPham)row.DataRow;
                    SanPham spSelect = new SanPham();
                    spSelect.Ma = sp.Ma;
                    spSelect.Ten = sp.Ten;
                    spSelect.DVT_ID = sp.DVT_ID;
                    spSelect.MaHS = sp.MaHS;
                    spCollection.Add(spSelect);
                    spTMPCollection.Add(spSelect);
                }
            }
            foreach (SanPham sp in spTMPCollection)
            {
                RemoveSP(sp, spCollectionSelect);
            }
            try
            {
                dgListSelect.Refetch();
            }
            catch
            {
                dgListSelect.Refresh();
            }

            dgList.Refetch();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void dgListSelect_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
             this.Close();
        }

     
    }
}
