-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@ImportTaxClassificationCodeForFTA1 varchar(10),
	@ImportTaxClassificationCodeForFTA2 varchar(10),
	@ImportTaxClassificationCodeForFTA3 varchar(10),
	@ImportTaxClassificationCodeForFTA4 varchar(10),
	@ImportTaxClassificationCodeForFTA5 varchar(10),
	@ImportTaxClassificationCodeForFTA6 varchar(10),
	@ImportTaxClassificationCodeForFTA7 varchar(10),
	@ImportTaxClassificationCodeForFTA8 varchar(10),
	@ImportTaxClassificationCodeForFTA9 varchar(10),
	@ImportTaxClassificationCodeForFTA10 varchar(10),
	@ImportTaxClassificationCodeForFTA11 varchar(10),
	@ImportTaxClassificationCodeForFTA12 varchar(10),
	@ImportTaxClassificationCodeForFTA13 varchar(10),
	@ImportTaxClassificationCodeForFTA14 varchar(10),
	@ImportTaxClassificationCodeForFTA15 varchar(10),
	@ImportTaxClassificationCodeForFTA16 varchar(10),
	@ImportTaxClassificationCodeForFTA17 varchar(10),
	@ImportTaxClassificationCodeForFTA18 varchar(10),
	@ImportTaxClassificationCodeForFTA19 varchar(10),
	@ImportTaxClassificationCodeForFTA20 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Nation]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[ImportTaxClassificationCodeForFTA1],
	[ImportTaxClassificationCodeForFTA2],
	[ImportTaxClassificationCodeForFTA3],
	[ImportTaxClassificationCodeForFTA4],
	[ImportTaxClassificationCodeForFTA5],
	[ImportTaxClassificationCodeForFTA6],
	[ImportTaxClassificationCodeForFTA7],
	[ImportTaxClassificationCodeForFTA8],
	[ImportTaxClassificationCodeForFTA9],
	[ImportTaxClassificationCodeForFTA10],
	[ImportTaxClassificationCodeForFTA11],
	[ImportTaxClassificationCodeForFTA12],
	[ImportTaxClassificationCodeForFTA13],
	[ImportTaxClassificationCodeForFTA14],
	[ImportTaxClassificationCodeForFTA15],
	[ImportTaxClassificationCodeForFTA16],
	[ImportTaxClassificationCodeForFTA17],
	[ImportTaxClassificationCodeForFTA18],
	[ImportTaxClassificationCodeForFTA19],
	[ImportTaxClassificationCodeForFTA20],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@NationCode,
	@GenerationManagementIndication,
	@DateOfMaintenanceUpdated,
	@CountryShortName,
	@ApplicationStartDate,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry,
	@ImportTaxClassificationCodeForFTA1,
	@ImportTaxClassificationCodeForFTA2,
	@ImportTaxClassificationCodeForFTA3,
	@ImportTaxClassificationCodeForFTA4,
	@ImportTaxClassificationCodeForFTA5,
	@ImportTaxClassificationCodeForFTA6,
	@ImportTaxClassificationCodeForFTA7,
	@ImportTaxClassificationCodeForFTA8,
	@ImportTaxClassificationCodeForFTA9,
	@ImportTaxClassificationCodeForFTA10,
	@ImportTaxClassificationCodeForFTA11,
	@ImportTaxClassificationCodeForFTA12,
	@ImportTaxClassificationCodeForFTA13,
	@ImportTaxClassificationCodeForFTA14,
	@ImportTaxClassificationCodeForFTA15,
	@ImportTaxClassificationCodeForFTA16,
	@ImportTaxClassificationCodeForFTA17,
	@ImportTaxClassificationCodeForFTA18,
	@ImportTaxClassificationCodeForFTA19,
	@ImportTaxClassificationCodeForFTA20,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@ImportTaxClassificationCodeForFTA1 varchar(10),
	@ImportTaxClassificationCodeForFTA2 varchar(10),
	@ImportTaxClassificationCodeForFTA3 varchar(10),
	@ImportTaxClassificationCodeForFTA4 varchar(10),
	@ImportTaxClassificationCodeForFTA5 varchar(10),
	@ImportTaxClassificationCodeForFTA6 varchar(10),
	@ImportTaxClassificationCodeForFTA7 varchar(10),
	@ImportTaxClassificationCodeForFTA8 varchar(10),
	@ImportTaxClassificationCodeForFTA9 varchar(10),
	@ImportTaxClassificationCodeForFTA10 varchar(10),
	@ImportTaxClassificationCodeForFTA11 varchar(10),
	@ImportTaxClassificationCodeForFTA12 varchar(10),
	@ImportTaxClassificationCodeForFTA13 varchar(10),
	@ImportTaxClassificationCodeForFTA14 varchar(10),
	@ImportTaxClassificationCodeForFTA15 varchar(10),
	@ImportTaxClassificationCodeForFTA16 varchar(10),
	@ImportTaxClassificationCodeForFTA17 varchar(10),
	@ImportTaxClassificationCodeForFTA18 varchar(10),
	@ImportTaxClassificationCodeForFTA19 varchar(10),
	@ImportTaxClassificationCodeForFTA20 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Nation]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[GenerationManagementIndication] = @GenerationManagementIndication,
	[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
	[CountryShortName] = @CountryShortName,
	[ApplicationStartDate] = @ApplicationStartDate,
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry] = @PlaceOfOriginIsMFNTaxRatesApplicationCountry,
	[ImportTaxClassificationCodeForFTA1] = @ImportTaxClassificationCodeForFTA1,
	[ImportTaxClassificationCodeForFTA2] = @ImportTaxClassificationCodeForFTA2,
	[ImportTaxClassificationCodeForFTA3] = @ImportTaxClassificationCodeForFTA3,
	[ImportTaxClassificationCodeForFTA4] = @ImportTaxClassificationCodeForFTA4,
	[ImportTaxClassificationCodeForFTA5] = @ImportTaxClassificationCodeForFTA5,
	[ImportTaxClassificationCodeForFTA6] = @ImportTaxClassificationCodeForFTA6,
	[ImportTaxClassificationCodeForFTA7] = @ImportTaxClassificationCodeForFTA7,
	[ImportTaxClassificationCodeForFTA8] = @ImportTaxClassificationCodeForFTA8,
	[ImportTaxClassificationCodeForFTA9] = @ImportTaxClassificationCodeForFTA9,
	[ImportTaxClassificationCodeForFTA10] = @ImportTaxClassificationCodeForFTA10,
	[ImportTaxClassificationCodeForFTA11] = @ImportTaxClassificationCodeForFTA11,
	[ImportTaxClassificationCodeForFTA12] = @ImportTaxClassificationCodeForFTA12,
	[ImportTaxClassificationCodeForFTA13] = @ImportTaxClassificationCodeForFTA13,
	[ImportTaxClassificationCodeForFTA14] = @ImportTaxClassificationCodeForFTA14,
	[ImportTaxClassificationCodeForFTA15] = @ImportTaxClassificationCodeForFTA15,
	[ImportTaxClassificationCodeForFTA16] = @ImportTaxClassificationCodeForFTA16,
	[ImportTaxClassificationCodeForFTA17] = @ImportTaxClassificationCodeForFTA17,
	[ImportTaxClassificationCodeForFTA18] = @ImportTaxClassificationCodeForFTA18,
	[ImportTaxClassificationCodeForFTA19] = @ImportTaxClassificationCodeForFTA19,
	[ImportTaxClassificationCodeForFTA20] = @ImportTaxClassificationCodeForFTA20,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[NationCode] = @NationCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@ImportTaxClassificationCodeForFTA1 varchar(10),
	@ImportTaxClassificationCodeForFTA2 varchar(10),
	@ImportTaxClassificationCodeForFTA3 varchar(10),
	@ImportTaxClassificationCodeForFTA4 varchar(10),
	@ImportTaxClassificationCodeForFTA5 varchar(10),
	@ImportTaxClassificationCodeForFTA6 varchar(10),
	@ImportTaxClassificationCodeForFTA7 varchar(10),
	@ImportTaxClassificationCodeForFTA8 varchar(10),
	@ImportTaxClassificationCodeForFTA9 varchar(10),
	@ImportTaxClassificationCodeForFTA10 varchar(10),
	@ImportTaxClassificationCodeForFTA11 varchar(10),
	@ImportTaxClassificationCodeForFTA12 varchar(10),
	@ImportTaxClassificationCodeForFTA13 varchar(10),
	@ImportTaxClassificationCodeForFTA14 varchar(10),
	@ImportTaxClassificationCodeForFTA15 varchar(10),
	@ImportTaxClassificationCodeForFTA16 varchar(10),
	@ImportTaxClassificationCodeForFTA17 varchar(10),
	@ImportTaxClassificationCodeForFTA18 varchar(10),
	@ImportTaxClassificationCodeForFTA19 varchar(10),
	@ImportTaxClassificationCodeForFTA20 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [NationCode] FROM [dbo].[t_VNACC_Category_Nation] WHERE [NationCode] = @NationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Nation] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[GenerationManagementIndication] = @GenerationManagementIndication,
			[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
			[CountryShortName] = @CountryShortName,
			[ApplicationStartDate] = @ApplicationStartDate,
			[PlaceOfOriginIsMFNTaxRatesApplicationCountry] = @PlaceOfOriginIsMFNTaxRatesApplicationCountry,
			[ImportTaxClassificationCodeForFTA1] = @ImportTaxClassificationCodeForFTA1,
			[ImportTaxClassificationCodeForFTA2] = @ImportTaxClassificationCodeForFTA2,
			[ImportTaxClassificationCodeForFTA3] = @ImportTaxClassificationCodeForFTA3,
			[ImportTaxClassificationCodeForFTA4] = @ImportTaxClassificationCodeForFTA4,
			[ImportTaxClassificationCodeForFTA5] = @ImportTaxClassificationCodeForFTA5,
			[ImportTaxClassificationCodeForFTA6] = @ImportTaxClassificationCodeForFTA6,
			[ImportTaxClassificationCodeForFTA7] = @ImportTaxClassificationCodeForFTA7,
			[ImportTaxClassificationCodeForFTA8] = @ImportTaxClassificationCodeForFTA8,
			[ImportTaxClassificationCodeForFTA9] = @ImportTaxClassificationCodeForFTA9,
			[ImportTaxClassificationCodeForFTA10] = @ImportTaxClassificationCodeForFTA10,
			[ImportTaxClassificationCodeForFTA11] = @ImportTaxClassificationCodeForFTA11,
			[ImportTaxClassificationCodeForFTA12] = @ImportTaxClassificationCodeForFTA12,
			[ImportTaxClassificationCodeForFTA13] = @ImportTaxClassificationCodeForFTA13,
			[ImportTaxClassificationCodeForFTA14] = @ImportTaxClassificationCodeForFTA14,
			[ImportTaxClassificationCodeForFTA15] = @ImportTaxClassificationCodeForFTA15,
			[ImportTaxClassificationCodeForFTA16] = @ImportTaxClassificationCodeForFTA16,
			[ImportTaxClassificationCodeForFTA17] = @ImportTaxClassificationCodeForFTA17,
			[ImportTaxClassificationCodeForFTA18] = @ImportTaxClassificationCodeForFTA18,
			[ImportTaxClassificationCodeForFTA19] = @ImportTaxClassificationCodeForFTA19,
			[ImportTaxClassificationCodeForFTA20] = @ImportTaxClassificationCodeForFTA20,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[NationCode] = @NationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Nation]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[NationCode],
			[GenerationManagementIndication],
			[DateOfMaintenanceUpdated],
			[CountryShortName],
			[ApplicationStartDate],
			[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
			[ImportTaxClassificationCodeForFTA1],
			[ImportTaxClassificationCodeForFTA2],
			[ImportTaxClassificationCodeForFTA3],
			[ImportTaxClassificationCodeForFTA4],
			[ImportTaxClassificationCodeForFTA5],
			[ImportTaxClassificationCodeForFTA6],
			[ImportTaxClassificationCodeForFTA7],
			[ImportTaxClassificationCodeForFTA8],
			[ImportTaxClassificationCodeForFTA9],
			[ImportTaxClassificationCodeForFTA10],
			[ImportTaxClassificationCodeForFTA11],
			[ImportTaxClassificationCodeForFTA12],
			[ImportTaxClassificationCodeForFTA13],
			[ImportTaxClassificationCodeForFTA14],
			[ImportTaxClassificationCodeForFTA15],
			[ImportTaxClassificationCodeForFTA16],
			[ImportTaxClassificationCodeForFTA17],
			[ImportTaxClassificationCodeForFTA18],
			[ImportTaxClassificationCodeForFTA19],
			[ImportTaxClassificationCodeForFTA20],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@NationCode,
			@GenerationManagementIndication,
			@DateOfMaintenanceUpdated,
			@CountryShortName,
			@ApplicationStartDate,
			@PlaceOfOriginIsMFNTaxRatesApplicationCountry,
			@ImportTaxClassificationCodeForFTA1,
			@ImportTaxClassificationCodeForFTA2,
			@ImportTaxClassificationCodeForFTA3,
			@ImportTaxClassificationCodeForFTA4,
			@ImportTaxClassificationCodeForFTA5,
			@ImportTaxClassificationCodeForFTA6,
			@ImportTaxClassificationCodeForFTA7,
			@ImportTaxClassificationCodeForFTA8,
			@ImportTaxClassificationCodeForFTA9,
			@ImportTaxClassificationCodeForFTA10,
			@ImportTaxClassificationCodeForFTA11,
			@ImportTaxClassificationCodeForFTA12,
			@ImportTaxClassificationCodeForFTA13,
			@ImportTaxClassificationCodeForFTA14,
			@ImportTaxClassificationCodeForFTA15,
			@ImportTaxClassificationCodeForFTA16,
			@ImportTaxClassificationCodeForFTA17,
			@ImportTaxClassificationCodeForFTA18,
			@ImportTaxClassificationCodeForFTA19,
			@ImportTaxClassificationCodeForFTA20,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Delete]
	@NationCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Nation]
WHERE
	[NationCode] = @NationCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Nation] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Load]
	@NationCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[ImportTaxClassificationCodeForFTA1],
	[ImportTaxClassificationCodeForFTA2],
	[ImportTaxClassificationCodeForFTA3],
	[ImportTaxClassificationCodeForFTA4],
	[ImportTaxClassificationCodeForFTA5],
	[ImportTaxClassificationCodeForFTA6],
	[ImportTaxClassificationCodeForFTA7],
	[ImportTaxClassificationCodeForFTA8],
	[ImportTaxClassificationCodeForFTA9],
	[ImportTaxClassificationCodeForFTA10],
	[ImportTaxClassificationCodeForFTA11],
	[ImportTaxClassificationCodeForFTA12],
	[ImportTaxClassificationCodeForFTA13],
	[ImportTaxClassificationCodeForFTA14],
	[ImportTaxClassificationCodeForFTA15],
	[ImportTaxClassificationCodeForFTA16],
	[ImportTaxClassificationCodeForFTA17],
	[ImportTaxClassificationCodeForFTA18],
	[ImportTaxClassificationCodeForFTA19],
	[ImportTaxClassificationCodeForFTA20],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Nation]
WHERE
	[NationCode] = @NationCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[ImportTaxClassificationCodeForFTA1],
	[ImportTaxClassificationCodeForFTA2],
	[ImportTaxClassificationCodeForFTA3],
	[ImportTaxClassificationCodeForFTA4],
	[ImportTaxClassificationCodeForFTA5],
	[ImportTaxClassificationCodeForFTA6],
	[ImportTaxClassificationCodeForFTA7],
	[ImportTaxClassificationCodeForFTA8],
	[ImportTaxClassificationCodeForFTA9],
	[ImportTaxClassificationCodeForFTA10],
	[ImportTaxClassificationCodeForFTA11],
	[ImportTaxClassificationCodeForFTA12],
	[ImportTaxClassificationCodeForFTA13],
	[ImportTaxClassificationCodeForFTA14],
	[ImportTaxClassificationCodeForFTA15],
	[ImportTaxClassificationCodeForFTA16],
	[ImportTaxClassificationCodeForFTA17],
	[ImportTaxClassificationCodeForFTA18],
	[ImportTaxClassificationCodeForFTA19],
	[ImportTaxClassificationCodeForFTA20],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Nation] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[ImportTaxClassificationCodeForFTA1],
	[ImportTaxClassificationCodeForFTA2],
	[ImportTaxClassificationCodeForFTA3],
	[ImportTaxClassificationCodeForFTA4],
	[ImportTaxClassificationCodeForFTA5],
	[ImportTaxClassificationCodeForFTA6],
	[ImportTaxClassificationCodeForFTA7],
	[ImportTaxClassificationCodeForFTA8],
	[ImportTaxClassificationCodeForFTA9],
	[ImportTaxClassificationCodeForFTA10],
	[ImportTaxClassificationCodeForFTA11],
	[ImportTaxClassificationCodeForFTA12],
	[ImportTaxClassificationCodeForFTA13],
	[ImportTaxClassificationCodeForFTA14],
	[ImportTaxClassificationCodeForFTA15],
	[ImportTaxClassificationCodeForFTA16],
	[ImportTaxClassificationCodeForFTA17],
	[ImportTaxClassificationCodeForFTA18],
	[ImportTaxClassificationCodeForFTA19],
	[ImportTaxClassificationCodeForFTA20],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Nation]	

GO

