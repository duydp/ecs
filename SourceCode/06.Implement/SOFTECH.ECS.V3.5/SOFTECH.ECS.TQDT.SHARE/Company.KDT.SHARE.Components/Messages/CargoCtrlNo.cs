﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
   public class CargoCtrlNo
    {
        // <summary>
        /// Số định danh hàng hóa
        /// </summary>
       [XmlElement("reference")]
       public string Reference { get; set; }

        /// <summary>
        /// Ngày cấp
        /// </summary>
       [XmlElement("issue")]
       public string Issue { get; set; }

        /// <summary>
        /// Mã QRCODE
        /// </summary>
       [XmlElement("codeContent")]
       public string CodeContent { get; set; }
    }
}
