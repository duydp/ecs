﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Janus.Windows.GridEX;
using Company.BLL;
using Company.BLL.SXXK.ToKhai;
using Company.KDT.SHARE.Components.DuLieuChuan;
namespace Company.Interface.SXXK
{
    public partial class NhanNhoForm : BaseForm
    {
        public DataSet ds;
        public NhanNhoForm()
        {
            InitializeComponent();
        }

        private void TheoDoiHoSoThanhLyForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = ds.Tables[0];
            dgList.Refetch();

            GetSumToKhai();
        }

        private void GetSumToKhai()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ds == null || ds.Tables.Count == 0)
                    return;

                DataRow[] filters = ds.Tables[0].Select("SoNgay > 0");

                lblGanDenHanThanhKhoan.Text = "" + (filters != null ? filters.Length : 0);

                lblHetHanThanhKhoan.Text = "" + (ds != null && ds.Tables.Count > 0 ? ds.Tables[0].Rows.Count - (filters != null ? filters.Length : 0) : 0);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                DateTime time = DateTime.Now;
                try
                {
                    time = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                }
                catch { }
                if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim().Contains("V"))
                { 
                    e.Row.Cells["MaLoaiHinh"].Text =e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim();
                }
                else
                {
                    e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                }
                 e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value);
                e.Row.Cells["SoToKhaiVNACCS"].Text = e.Row.Cells["SoToKhaiVNACCS"].Text + "/" + e.Row.Cells["MaHaiQuan"].Value.ToString() + "/" + time.Year.ToString();
                
                switch (e.Row.Cells["TrangThaiThanhKhoan"].Value.ToString())
                {
                    case "S":
                        // e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã được tính thuế sau kiểm hóa";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã được tính thuế sau kiểm hóa";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiThanhKhoan"].Text = "Calculated tax after checking";
                        }
                        break;
                    case "D":
                        //e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiThanhKhoan"].Text = "Approved";
                        }
                        break;
                    case "K":
                        // e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã được kiểm hóa";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã được kiểm hóa";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiThanhKhoan"].Text = "Checked tax";
                        }
                        break;
                    case "C":
                        //e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã được điều chỉnh thuế";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã được điều chỉnh thuế";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiThanhKhoan"].Text = "Adjusted tax";
                        }
                        break;
                }

                string phanluong = e.Row.Cells["PhanLuong"].Text;
                if (phanluong == "1" || phanluong == "3" || phanluong == "5")
                    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng xanh";
                else if (phanluong == "2" || phanluong == "4" || phanluong == "6" || phanluong == "10")
                {
                    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng đỏ";
                }
                else if (phanluong == "8" || phanluong == "12")
                    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng vàng";


                DateTime ngayTNX = Convert.ToDateTime(e.Row.Cells["NGAY_THN_THX"].Value.ToString());
                DateTime ngayDK = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value.ToString());
                DateTime today = DateTime.Today;
                int ngay = Convert.ToInt32(e.Row.Cells["songay"].Value.ToString());
                //if (ngay > 0)
                //    e.Row.Cells["songay"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                //else if (ngay == 0)
                //    e.Row.Cells["songay"].Text = "Hết hạn thanh khoản trong ngày hôm nay";
                //else
                //    e.Row.Cells["songay"].Text = "Đã hết hạn thanh khoản";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    if (ngay > 0)
                        e.Row.Cells["songay"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                    else if (ngay == 0)
                        e.Row.Cells["songay"].Text = "Hết hạn thanh khoản trong ngày hôm nay";
                    else
                        e.Row.Cells["songay"].Text = "Đã hết hạn thanh khoản";
                }
                else
                {
                    if (ngay > 0)
                        e.Row.Cells["songay"].Text = "Remaining " + ngay.ToString() + " days have to liquidate";
                    else if (ngay == 0)
                        e.Row.Cells["songay"].Text = "Expired liquidation at today";
                    else
                        e.Row.Cells["songay"].Text = "Expired liquidation";

                }
                if (ngayTNX.Year <= 1900)
                {
                    e.Row.Cells["NGAY_THN_THX"].Text = "";
                }
            }

        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
