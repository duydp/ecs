﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

using Janus.Windows.EditControls;
using Janus.Windows.ExplorerBar;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using Company.Interface.SXXK;

namespace Company.Interface
{
    partial class MainForm
    {
        private UICommandManager cmMain;
        private UIRebar TopRebar1;
        private UICommand cmdHeThong;
        private UICommand cmdHeThong1;
        private UICommand cmdThoat;
        private UICommand cmdThoat1;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private UIPanelManager pmMain;
        private UICommandBar cmbMenu;
        private UICommand cmdLoaiHinh;
        private UIPanel pnlSXXK;
        private UIPanelInnerContainer pnlSXXKContainer;
        private UIPanel pnlGiaCong;
        private UIPanelInnerContainer pnlGiaCongContainer;
        private UIPanel pnlKinhDoanh;
        private UIPanelInnerContainer pnlKinhDoanhContainer;
        private UIPanel pnlDauTu;
        internal ImageList ilSmall;
        internal ImageList ilMedium;
        internal ImageList ilLarge;
        private UICommand cmdReceiveAll;
        private Janus.Windows.ExplorerBar.ExplorerBar expSXXK;
        private Janus.Windows.ExplorerBar.ExplorerBar expGiaCong;
        private Janus.Windows.ExplorerBar.ExplorerBar expKD;
        private Janus.Windows.UI.CommandBars.UICommand cmdThoat2;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup21 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem58 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem59 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup22 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem60 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem61 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem62 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem63 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup23 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem64 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem65 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem66 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem67 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup24 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem68 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem69 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem70 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup1 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem1 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup2 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem5 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem6 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup3 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem7 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem8 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem9 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup4 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem10 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem11 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem12 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem13 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem71 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup25 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem72 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem73 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem74 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem75 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup5 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem14 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem15 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem16 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup6 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem17 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem18 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem19 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup7 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem20 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem21 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem22 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup8 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem23 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem24 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup9 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem25 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem26 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem27 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup10 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem28 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem29 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup11 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem30 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem31 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup12 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem32 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem33 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup13 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem34 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem35 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup14 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem36 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem37 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup15 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem38 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem39 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel4 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel5 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel6 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbMenu = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdHeThong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdDanhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.MaHS1 = new Janus.Windows.UI.CommandBars.UICommand("MaHS");
            this.QuanTri1 = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.Command11 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.Command01 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdHeThong = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdThietLapCHDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapCHDN");
            this.cmdXuatDuLieuChoPhongKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuChoPhongKhai");
            this.cmdDongBoPhongKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDongBoPhongKhai");
            this.cmdDataVersion1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdRestore1 = new Janus.Windows.UI.CommandBars.UICommand("cmdRestore");
            this.cmdBackUp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdCauHinh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.cmdQuery1 = new Janus.Windows.UI.CommandBars.UICommand("cmdQuery");
            this.QuanLyMess1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyMess");
            this.cmdLog1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.LoginUser1 = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.cmdThoat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdThoat = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdLoaiHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiHinh");
            this.cmdReceiveAll = new Janus.Windows.UI.CommandBars.UICommand("cmdReceiveAll");
            this.NhacNho = new Janus.Windows.UI.CommandBars.UICommand("NhacNho");
            this.DongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdImport = new Janus.Windows.UI.CommandBars.UICommand("cmdImport");
            this.cmdImportNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.cmdImportNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportTTDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportTTDM");
            this.cmdImportToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.Command1 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.cmd20071 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd20031 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.cmdVN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEng");
            this.cmd2007 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd2003 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.Command0 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdHelp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAutoUpdate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdActivate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdAbout1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAbout = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdNPLNhapTon = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdDanhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNuoc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNguyenTe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.DonViDoiTac1 = new Janus.Windows.UI.CommandBars.UICommand("DonViDoiTac");
            this.cmdMaHS = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNuoc = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNguyenTe = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdBackUp = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdRestore = new Janus.Windows.UI.CommandBars.UICommand("cmdRestore");
            this.ThongSoKetNoi = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdThietLapIn = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdNhapToKhaiDauTu = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiDauTu");
            this.cmdNhapToKhaiKinhDoanh = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiKinhDoanh");
            this.cmdDongBoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdDongBoPhongKhai");
            this.cmdImportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiKinhDoanh");
            this.cmdExportExccel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiDauTu");
            this.cmdCauHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.ThongSoKetNoi1 = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ1 = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdCauHinhToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.cmdThietLapIn1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdCauHinhToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.QuanTri = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.QuanLyNguoiDung1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.QuanLyNguoiDung = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.LoginUser = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.cmdThietLapCHDN = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapCHDN");
            this.MaHS = new Janus.Windows.UI.CommandBars.UICommand("MaHS");
            this.DonViDoiTac = new Janus.Windows.UI.CommandBars.UICommand("DonViDoiTac");
            this.cmdAutoUpdate = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.cmdXuatDuLieuChoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuChoPhongKhai");
            this.cmdXuatToKhaiDauTu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhaiDauTu");
            this.cmdXuatToKhaiKinhDoanh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhaiKinhDoanh");
            this.cmdXuatToKhaiDauTu = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhaiDauTu");
            this.cmdXuatToKhaiKinhDoanh = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhaiKinhDoanh");
            this.cmdEng = new Janus.Windows.UI.CommandBars.UICommand("cmdEng");
            this.cmdVN = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdActivate = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdCloseMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.cmdCloseAllButMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdCloseAll = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.QuanLyMess = new Janus.Windows.UI.CommandBars.UICommand("QuanLyMess");
            this.cmdQuery = new Janus.Windows.UI.CommandBars.UICommand("cmdQuery");
            this.cmdLog = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdDataVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdnhomcuakhau = new Janus.Windows.UI.CommandBars.UICommand("cmdnhomcuakhau");
            this.mnuRightClick = new Janus.Windows.UI.CommandBars.UIContextMenu();
            this.cmdCloseMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdCloseAllButMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdCloseAll1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.ilSmall = new System.Windows.Forms.ImageList(this.components);
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdThoat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.pmMain = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBar1 = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSXXK = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSXXKContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expSXXK = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlGiaCong = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlGiaCongContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expGiaCong = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlKinhDoanh = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlKinhDoanhContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKD = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlDauTu = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlDauTuContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expDT = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSend = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSendContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKhaiBao_TheoDoi = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.ilMedium = new System.Windows.Forms.ImageList(this.components);
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.statusBar = new Janus.Windows.UI.StatusBar.UIStatusBar();
            this.cmdThoat3 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.cmdnhomcuakhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdnhomcuakhau");
            this.cmdnhomcuakhau2 = new Janus.Windows.UI.CommandBars.UICommand("cmdnhomcuakhau");
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).BeginInit();
            this.pnlSXXK.SuspendLayout();
            this.pnlSXXKContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).BeginInit();
            this.pnlGiaCong.SuspendLayout();
            this.pnlGiaCongContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).BeginInit();
            this.pnlKinhDoanh.SuspendLayout();
            this.pnlKinhDoanhContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).BeginInit();
            this.pnlDauTu.SuspendLayout();
            this.pnlDauTuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).BeginInit();
            this.pnlSend.SuspendLayout();
            this.pnlSendContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.Control;
            this.grbMain.Location = new System.Drawing.Point(203, 29);
            this.grbMain.Size = new System.Drawing.Size(622, 488);
            this.grbMain.Visible = false;
            // 
            // cmMain
            // 
            this.cmMain.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowMerge = false;
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong,
            this.cmdThoat,
            this.cmdLoaiHinh,
            this.cmdReceiveAll,
            this.NhacNho,
            this.DongBoDuLieu,
            this.cmdImport,
            this.cmdImportNPL,
            this.cmdImportSP,
            this.cmdImportDM,
            this.cmdImportTTDM,
            this.cmdImportToKhai,
            this.cmdImportHangHoa,
            this.Command1,
            this.cmd2007,
            this.cmd2003,
            this.Command0,
            this.cmdHelp,
            this.cmdAbout,
            this.cmdNPLNhapTon,
            this.cmdDanhMuc,
            this.cmdMaHS,
            this.cmdNuoc,
            this.cmdHaiQuan,
            this.cmdNguyenTe,
            this.cmdDVT,
            this.cmdPTTT,
            this.cmdPTVT,
            this.cmdDKGH,
            this.cmdCuaKhau,
            this.cmdBackUp,
            this.cmdRestore,
            this.ThongSoKetNoi,
            this.TLThongTinDNHQ,
            this.cmdThietLapIn,
            this.cmdNhapToKhaiDauTu,
            this.cmdNhapToKhaiKinhDoanh,
            this.cmdDongBoPhongKhai,
            this.cmdCauHinh,
            this.cmdCauHinhToKhai,
            this.QuanTri,
            this.QuanLyNguoiDung,
            this.QuanLyNhom,
            this.LoginUser,
            this.cmdChangePass,
            this.cmdThietLapCHDN,
            this.MaHS,
            this.DonViDoiTac,
            this.cmdAutoUpdate,
            this.cmdXuatDuLieuChoPhongKhai,
            this.cmdXuatToKhaiDauTu,
            this.cmdXuatToKhaiKinhDoanh,
            this.cmdEng,
            this.cmdVN,
            this.cmdActivate,
            this.cmdCloseMe,
            this.cmdCloseAllButMe,
            this.cmdCloseAll,
            this.QuanLyMess,
            this.cmdQuery,
            this.cmdLog,
            this.cmdDataVersion,
            this.cmdnhomcuakhau});
            this.cmMain.ContainerControl = this;
            this.cmMain.ContextMenus.AddRange(new Janus.Windows.UI.CommandBars.UIContextMenu[] {
            this.mnuRightClick});
            this.cmMain.Id = new System.Guid("eae49f54-3bfa-4a6a-8b46-89b443ba80cd");
            this.cmMain.ImageList = this.ilSmall;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbMenu
            // 
            this.cmbMenu.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowMerge = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.CommandBarType = Janus.Windows.UI.CommandBars.CommandBarType.Menu;
            this.cmbMenu.CommandManager = this.cmMain;
            this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong1,
            this.cmdDanhMuc1,
            this.MaHS1,
            this.QuanTri1,
            this.Command11,
            this.Command01});
            this.cmbMenu.Key = "cmbMenu";
            this.cmbMenu.Location = new System.Drawing.Point(0, 0);
            this.cmbMenu.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbMenu.MergeRowOrder = 0;
            this.cmbMenu.Name = "cmbMenu";
            this.cmbMenu.RowIndex = 0;
            this.cmbMenu.Size = new System.Drawing.Size(828, 26);
            this.cmbMenu.Text = "cmbMenu";
            this.cmbMenu.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMenu_CommandClick);
            // 
            // cmdHeThong1
            // 
            this.cmdHeThong1.Key = "cmdHeThong";
            this.cmdHeThong1.Name = "cmdHeThong1";
            // 
            // cmdDanhMuc1
            // 
            this.cmdDanhMuc1.Key = "cmdDanhMuc";
            this.cmdDanhMuc1.Name = "cmdDanhMuc1";
            this.cmdDanhMuc1.Text = "&Danh mục";
            // 
            // MaHS1
            // 
            this.MaHS1.Key = "MaHS";
            this.MaHS1.Name = "MaHS1";
            // 
            // QuanTri1
            // 
            this.QuanTri1.Key = "QuanTri";
            this.QuanTri1.Name = "QuanTri1";
            // 
            // Command11
            // 
            this.Command11.Key = "Command1";
            this.Command11.Name = "Command11";
            this.Command11.Text = "&Giao diện";
            // 
            // Command01
            // 
            this.Command01.Key = "Command0";
            this.Command01.Name = "Command01";
            this.Command01.Text = "&Trợ giúp";
            // 
            // cmdHeThong
            // 
            this.cmdHeThong.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThietLapCHDN1,
            this.cmdXuatDuLieuChoPhongKhai1,
            this.cmdDongBoPhongKhai1,
            this.cmdDataVersion1,
            this.cmdRestore1,
            this.cmdBackUp1,
            this.cmdCauHinh1,
            this.cmdQuery1,
            this.QuanLyMess1,
            this.cmdLog1,
            this.LoginUser1,
            this.cmdChangePass1,
            this.cmdThoat2});
            this.cmdHeThong.Key = "cmdHeThong";
            this.cmdHeThong.Name = "cmdHeThong";
            this.cmdHeThong.Text = "&Hệ thống";
            // 
            // cmdThietLapCHDN1
            // 
            this.cmdThietLapCHDN1.ImageIndex = 37;
            this.cmdThietLapCHDN1.Key = "cmdThietLapCHDN";
            this.cmdThietLapCHDN1.Name = "cmdThietLapCHDN1";
            // 
            // cmdXuatDuLieuChoPhongKhai1
            // 
            this.cmdXuatDuLieuChoPhongKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdXuatDuLieuChoPhongKhai1.Icon")));
            this.cmdXuatDuLieuChoPhongKhai1.Key = "cmdXuatDuLieuChoPhongKhai";
            this.cmdXuatDuLieuChoPhongKhai1.Name = "cmdXuatDuLieuChoPhongKhai1";
            // 
            // cmdDongBoPhongKhai1
            // 
            this.cmdDongBoPhongKhai1.Key = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai1.Name = "cmdDongBoPhongKhai1";
            // 
            // cmdDataVersion1
            // 
            this.cmdDataVersion1.Key = "cmdDataVersion";
            this.cmdDataVersion1.Name = "cmdDataVersion1";
            // 
            // cmdRestore1
            // 
            this.cmdRestore1.Key = "cmdRestore";
            this.cmdRestore1.Name = "cmdRestore1";
            this.cmdRestore1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdBackUp1
            // 
            this.cmdBackUp1.Key = "cmdBackUp";
            this.cmdBackUp1.Name = "cmdBackUp1";
            // 
            // cmdCauHinh1
            // 
            this.cmdCauHinh1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCauHinh1.Icon")));
            this.cmdCauHinh1.Key = "cmdCauHinh";
            this.cmdCauHinh1.Name = "cmdCauHinh1";
            // 
            // cmdQuery1
            // 
            this.cmdQuery1.Key = "cmdQuery";
            this.cmdQuery1.Name = "cmdQuery1";
            // 
            // QuanLyMess1
            // 
            this.QuanLyMess1.Key = "QuanLyMess";
            this.QuanLyMess1.Name = "QuanLyMess1";
            // 
            // cmdLog1
            // 
            this.cmdLog1.ImageIndex = 35;
            this.cmdLog1.Key = "cmdLog";
            this.cmdLog1.Name = "cmdLog1";
            // 
            // LoginUser1
            // 
            this.LoginUser1.Icon = ((System.Drawing.Icon)(resources.GetObject("LoginUser1.Icon")));
            this.LoginUser1.Key = "LoginUser";
            this.LoginUser1.Name = "LoginUser1";
            // 
            // cmdChangePass1
            // 
            this.cmdChangePass1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChangePass1.Icon")));
            this.cmdChangePass1.Key = "cmdChangePass";
            this.cmdChangePass1.Name = "cmdChangePass1";
            // 
            // cmdThoat2
            // 
            this.cmdThoat2.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThoat2.Icon")));
            this.cmdThoat2.Key = "cmdThoat";
            this.cmdThoat2.Name = "cmdThoat2";
            // 
            // cmdThoat
            // 
            this.cmdThoat.Key = "cmdThoat";
            this.cmdThoat.Name = "cmdThoat";
            this.cmdThoat.Text = "Thoát";
            // 
            // cmdLoaiHinh
            // 
            this.cmdLoaiHinh.Key = "cmdLoaiHinh";
            this.cmdLoaiHinh.Name = "cmdLoaiHinh";
            this.cmdLoaiHinh.Text = "Loại hình";
            // 
            // cmdReceiveAll
            // 
            this.cmdReceiveAll.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdReceiveAll.Icon")));
            this.cmdReceiveAll.Key = "cmdReceiveAll";
            this.cmdReceiveAll.Name = "cmdReceiveAll";
            this.cmdReceiveAll.Text = "Cập nhật thông tin";
            // 
            // NhacNho
            // 
            this.NhacNho.Key = "NhacNho";
            this.NhacNho.Name = "NhacNho";
            this.NhacNho.Text = "Nhắc nhở";
            // 
            // DongBoDuLieu
            // 
            this.DongBoDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("DongBoDuLieu.Icon")));
            this.DongBoDuLieu.Key = "DongBoDuLieu";
            this.DongBoDuLieu.Name = "DongBoDuLieu";
            this.DongBoDuLieu.Text = "Đồng bộ dữ liệu";
            // 
            // cmdImport
            // 
            this.cmdImport.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportNPL1,
            this.cmdImportSP1,
            this.cmdImportDM1,
            this.cmdImportToKhai1,
            this.cmdImportHangHoa1});
            this.cmdImport.Key = "cmdImport";
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Text = "Import from Excel";
            // 
            // cmdImportNPL1
            // 
            this.cmdImportNPL1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportNPL1.Icon")));
            this.cmdImportNPL1.Key = "cmdImportNPL";
            this.cmdImportNPL1.Name = "cmdImportNPL1";
            this.cmdImportNPL1.Text = "Nguyên phụ liệu";
            // 
            // cmdImportSP1
            // 
            this.cmdImportSP1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportSP1.Icon")));
            this.cmdImportSP1.Key = "cmdImportSP";
            this.cmdImportSP1.Name = "cmdImportSP1";
            this.cmdImportSP1.Text = "Sản phẩm";
            // 
            // cmdImportDM1
            // 
            this.cmdImportDM1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportDM1.Icon")));
            this.cmdImportDM1.Key = "cmdImportDM";
            this.cmdImportDM1.Name = "cmdImportDM1";
            this.cmdImportDM1.Text = "Định mức";
            // 
            // cmdImportToKhai1
            // 
            this.cmdImportToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportToKhai1.Icon")));
            this.cmdImportToKhai1.Key = "cmdImportToKhai";
            this.cmdImportToKhai1.Name = "cmdImportToKhai1";
            this.cmdImportToKhai1.Text = "Tờ khai";
            // 
            // cmdImportHangHoa1
            // 
            this.cmdImportHangHoa1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportHangHoa1.Icon")));
            this.cmdImportHangHoa1.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa1.Name = "cmdImportHangHoa1";
            this.cmdImportHangHoa1.Text = "Hàng của tờ khai";
            // 
            // cmdImportNPL
            // 
            this.cmdImportNPL.Key = "cmdImportNPL";
            this.cmdImportNPL.Name = "cmdImportNPL";
            this.cmdImportNPL.Text = "Import NPL";
            // 
            // cmdImportSP
            // 
            this.cmdImportSP.Key = "cmdImportSP";
            this.cmdImportSP.Name = "cmdImportSP";
            this.cmdImportSP.Text = "Import Sản phẩm";
            // 
            // cmdImportDM
            // 
            this.cmdImportDM.Key = "cmdImportDM";
            this.cmdImportDM.Name = "cmdImportDM";
            this.cmdImportDM.Text = "Import Định mức";
            // 
            // cmdImportTTDM
            // 
            this.cmdImportTTDM.Key = "cmdImportTTDM";
            this.cmdImportTTDM.Name = "cmdImportTTDM";
            this.cmdImportTTDM.Text = "Import thông tin định mức";
            // 
            // cmdImportToKhai
            // 
            this.cmdImportToKhai.Key = "cmdImportToKhai";
            this.cmdImportToKhai.Name = "cmdImportToKhai";
            this.cmdImportToKhai.Text = "Import tờ khai";
            // 
            // cmdImportHangHoa
            // 
            this.cmdImportHangHoa.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa.Name = "cmdImportHangHoa";
            this.cmdImportHangHoa.Text = "Import hàng tờ khai";
            // 
            // Command1
            // 
            this.Command1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmd20071,
            this.cmd20031,
            this.cmdVN1,
            this.cmdEng1});
            this.Command1.Key = "Command1";
            this.Command1.Name = "Command1";
            this.Command1.Text = "Giao diện";
            // 
            // cmd20071
            // 
            this.cmd20071.Key = "cmd2007";
            this.cmd20071.Name = "cmd20071";
            // 
            // cmd20031
            // 
            this.cmd20031.Key = "cmd2003";
            this.cmd20031.Name = "cmd20031";
            // 
            // cmdVN1
            // 
            this.cmdVN1.ImageIndex = 40;
            this.cmdVN1.Key = "cmdVN";
            this.cmdVN1.Name = "cmdVN1";
            // 
            // cmdEng1
            // 
            this.cmdEng1.ImageIndex = 39;
            this.cmdEng1.Key = "cmdEng";
            this.cmdEng1.Name = "cmdEng1";
            // 
            // cmd2007
            // 
            this.cmd2007.Key = "cmd2007";
            this.cmd2007.Name = "cmd2007";
            this.cmd2007.Text = "Office 2007";
            // 
            // cmd2003
            // 
            this.cmd2003.Key = "cmd2003";
            this.cmd2003.Name = "cmd2003";
            this.cmd2003.Text = "Office 2003";
            // 
            // Command0
            // 
            this.Command0.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHelp1,
            this.cmdAutoUpdate1,
            this.Separator1,
            this.cmdActivate1,
            this.cmdAbout1});
            this.Command0.Key = "Command0";
            this.Command0.Name = "Command0";
            this.Command0.Text = "Trợ giúp";
            // 
            // cmdHelp1
            // 
            this.cmdHelp1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdHelp1.Icon")));
            this.cmdHelp1.Key = "cmdHelp";
            this.cmdHelp1.Name = "cmdHelp1";
            this.cmdHelp1.Text = "&Hướng dẫn sử dụng";
            // 
            // cmdAutoUpdate1
            // 
            this.cmdAutoUpdate1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAutoUpdate1.Icon")));
            this.cmdAutoUpdate1.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate1.Name = "cmdAutoUpdate1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdActivate1
            // 
            this.cmdActivate1.Key = "cmdActivate";
            this.cmdActivate1.Name = "cmdActivate1";
            // 
            // cmdAbout1
            // 
            this.cmdAbout1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAbout1.Icon")));
            this.cmdAbout1.Key = "cmdAbout";
            this.cmdAbout1.Name = "cmdAbout1";
            this.cmdAbout1.Text = "&Thông tin sản phẩm";
            // 
            // cmdHelp
            // 
            this.cmdHelp.Key = "cmdHelp";
            this.cmdHelp.Name = "cmdHelp";
            this.cmdHelp.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.cmdHelp.Text = "Hướng dẫn sử dụng";
            // 
            // cmdAbout
            // 
            this.cmdAbout.Key = "cmdAbout";
            this.cmdAbout.Name = "cmdAbout";
            this.cmdAbout.Text = "Thông tin sản phẩm";
            // 
            // cmdNPLNhapTon
            // 
            this.cmdNPLNhapTon.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Name = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Text = "NPL nhập tồn";
            // 
            // cmdDanhMuc
            // 
            this.cmdDanhMuc.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHaiQuan1,
            this.cmdNuoc1,
            this.cmdMaHS1,
            this.cmdNguyenTe1,
            this.cmdDVT1,
            this.cmdPTTT1,
            this.cmdPTVT1,
            this.cmdDKGH1,
            this.cmdnhomcuakhau2,
            this.cmdCuaKhau1,
            this.DonViDoiTac1});
            this.cmdDanhMuc.Key = "cmdDanhMuc";
            this.cmdDanhMuc.Name = "cmdDanhMuc";
            this.cmdDanhMuc.Text = "DanhMuc";
            // 
            // cmdHaiQuan1
            // 
            this.cmdHaiQuan1.ImageIndex = 38;
            this.cmdHaiQuan1.Key = "cmdHaiQuan";
            this.cmdHaiQuan1.Name = "cmdHaiQuan1";
            // 
            // cmdNuoc1
            // 
            this.cmdNuoc1.ImageIndex = 38;
            this.cmdNuoc1.Key = "cmdNuoc";
            this.cmdNuoc1.Name = "cmdNuoc1";
            // 
            // cmdMaHS1
            // 
            this.cmdMaHS1.ImageIndex = 38;
            this.cmdMaHS1.Key = "cmdMaHS";
            this.cmdMaHS1.Name = "cmdMaHS1";
            // 
            // cmdNguyenTe1
            // 
            this.cmdNguyenTe1.ImageIndex = 38;
            this.cmdNguyenTe1.Key = "cmdNguyenTe";
            this.cmdNguyenTe1.Name = "cmdNguyenTe1";
            // 
            // cmdDVT1
            // 
            this.cmdDVT1.ImageIndex = 38;
            this.cmdDVT1.Key = "cmdDVT";
            this.cmdDVT1.Name = "cmdDVT1";
            // 
            // cmdPTTT1
            // 
            this.cmdPTTT1.ImageIndex = 38;
            this.cmdPTTT1.Key = "cmdPTTT";
            this.cmdPTTT1.Name = "cmdPTTT1";
            // 
            // cmdPTVT1
            // 
            this.cmdPTVT1.ImageIndex = 38;
            this.cmdPTVT1.Key = "cmdPTVT";
            this.cmdPTVT1.Name = "cmdPTVT1";
            // 
            // cmdDKGH1
            // 
            this.cmdDKGH1.ImageIndex = 38;
            this.cmdDKGH1.Key = "cmdDKGH";
            this.cmdDKGH1.Name = "cmdDKGH1";
            // 
            // cmdCuaKhau1
            // 
            this.cmdCuaKhau1.ImageIndex = 38;
            this.cmdCuaKhau1.Key = "cmdCuaKhau";
            this.cmdCuaKhau1.Name = "cmdCuaKhau1";
            // 
            // DonViDoiTac1
            // 
            this.DonViDoiTac1.ImageIndex = 38;
            this.DonViDoiTac1.Key = "DonViDoiTac";
            this.DonViDoiTac1.Name = "DonViDoiTac1";
            // 
            // cmdMaHS
            // 
            this.cmdMaHS.Key = "cmdMaHS";
            this.cmdMaHS.Name = "cmdMaHS";
            this.cmdMaHS.Text = "Mã HS";
            // 
            // cmdNuoc
            // 
            this.cmdNuoc.Key = "cmdNuoc";
            this.cmdNuoc.Name = "cmdNuoc";
            this.cmdNuoc.Text = "Nước";
            // 
            // cmdHaiQuan
            // 
            this.cmdHaiQuan.Key = "cmdHaiQuan";
            this.cmdHaiQuan.Name = "cmdHaiQuan";
            this.cmdHaiQuan.Text = "Đơn vị Hải quan";
            // 
            // cmdNguyenTe
            // 
            this.cmdNguyenTe.Key = "cmdNguyenTe";
            this.cmdNguyenTe.Name = "cmdNguyenTe";
            this.cmdNguyenTe.Text = "Nguyên tệ";
            // 
            // cmdDVT
            // 
            this.cmdDVT.Key = "cmdDVT";
            this.cmdDVT.Name = "cmdDVT";
            this.cmdDVT.Text = "Đơn vị tính";
            // 
            // cmdPTTT
            // 
            this.cmdPTTT.Key = "cmdPTTT";
            this.cmdPTTT.Name = "cmdPTTT";
            this.cmdPTTT.Text = "Phương thức thanh toán";
            // 
            // cmdPTVT
            // 
            this.cmdPTVT.Key = "cmdPTVT";
            this.cmdPTVT.Name = "cmdPTVT";
            this.cmdPTVT.Text = "Phương thức vận tải";
            // 
            // cmdDKGH
            // 
            this.cmdDKGH.Key = "cmdDKGH";
            this.cmdDKGH.Name = "cmdDKGH";
            this.cmdDKGH.Text = "Điều kiện giao hàng";
            // 
            // cmdCuaKhau
            // 
            this.cmdCuaKhau.Key = "cmdCuaKhau";
            this.cmdCuaKhau.Name = "cmdCuaKhau";
            this.cmdCuaKhau.Text = "Cửa khẩu";
            // 
            // cmdBackUp
            // 
            this.cmdBackUp.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBackUp.Icon")));
            this.cmdBackUp.Key = "cmdBackUp";
            this.cmdBackUp.Name = "cmdBackUp";
            this.cmdBackUp.Text = "Sao lưu dữ liệu";
            // 
            // cmdRestore
            // 
            this.cmdRestore.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdRestore.Icon")));
            this.cmdRestore.Key = "cmdRestore";
            this.cmdRestore.Name = "cmdRestore";
            this.cmdRestore.Text = "Phục hồi dữ liệu";
            // 
            // ThongSoKetNoi
            // 
            this.ThongSoKetNoi.Icon = ((System.Drawing.Icon)(resources.GetObject("ThongSoKetNoi.Icon")));
            this.ThongSoKetNoi.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi.Name = "ThongSoKetNoi";
            this.ThongSoKetNoi.Text = "Thiết lập thông số kết nối";
            // 
            // TLThongTinDNHQ
            // 
            this.TLThongTinDNHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("TLThongTinDNHQ.Icon")));
            this.TLThongTinDNHQ.ImageIndex = 37;
            this.TLThongTinDNHQ.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Name = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Text = "Thiết lập thông tin doanh nghiệp và hải quan";
            // 
            // cmdThietLapIn
            // 
            this.cmdThietLapIn.Key = "cmdThietLapIn";
            this.cmdThietLapIn.Name = "cmdThietLapIn";
            this.cmdThietLapIn.Text = "Thiết lập thông số in báo cáo";
            // 
            // cmdNhapToKhaiDauTu
            // 
            this.cmdNhapToKhaiDauTu.Key = "cmdNhapToKhaiDauTu";
            this.cmdNhapToKhaiDauTu.Name = "cmdNhapToKhaiDauTu";
            this.cmdNhapToKhaiDauTu.Text = "Tờ khai kinh doanh";
            // 
            // cmdNhapToKhaiKinhDoanh
            // 
            this.cmdNhapToKhaiKinhDoanh.Key = "cmdNhapToKhaiKinhDoanh";
            this.cmdNhapToKhaiKinhDoanh.Name = "cmdNhapToKhaiKinhDoanh";
            this.cmdNhapToKhaiKinhDoanh.Text = "Tờ khai kinh doanh";
            // 
            // cmdDongBoPhongKhai
            // 
            this.cmdDongBoPhongKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportExcel1,
            this.cmdExportExccel1});
            this.cmdDongBoPhongKhai.Key = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Name = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Text = "Nhập dữ liệu từ doanh nghiệp";
            // 
            // cmdImportExcel1
            // 
            this.cmdImportExcel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportExcel1.Icon")));
            this.cmdImportExcel1.Key = "cmdNhapToKhaiKinhDoanh";
            this.cmdImportExcel1.Name = "cmdImportExcel1";
            // 
            // cmdExportExccel1
            // 
            this.cmdExportExccel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdExportExccel1.Icon")));
            this.cmdExportExccel1.Key = "cmdNhapToKhaiDauTu";
            this.cmdExportExccel1.Name = "cmdExportExccel1";
            // 
            // cmdCauHinh
            // 
            this.cmdCauHinh.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThongSoKetNoi1,
            this.TLThongTinDNHQ1,
            this.cmdCauHinhToKhai1,
            this.cmdThietLapIn1});
            this.cmdCauHinh.Key = "cmdCauHinh";
            this.cmdCauHinh.Name = "cmdCauHinh";
            this.cmdCauHinh.Text = "Cấu hình hệ thống";
            // 
            // ThongSoKetNoi1
            // 
            this.ThongSoKetNoi1.Icon = ((System.Drawing.Icon)(resources.GetObject("ThongSoKetNoi1.Icon")));
            this.ThongSoKetNoi1.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi1.Name = "ThongSoKetNoi1";
            this.ThongSoKetNoi1.Text = "Cấu hình thông số kết nối";
            // 
            // TLThongTinDNHQ1
            // 
            this.TLThongTinDNHQ1.Icon = ((System.Drawing.Icon)(resources.GetObject("TLThongTinDNHQ1.Icon")));
            this.TLThongTinDNHQ1.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ1.Name = "TLThongTinDNHQ1";
            this.TLThongTinDNHQ1.Text = "Cấu hình thông tin doanh nghiệp và hải quan";
            // 
            // cmdCauHinhToKhai1
            // 
            this.cmdCauHinhToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCauHinhToKhai1.Icon")));
            this.cmdCauHinhToKhai1.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai1.Name = "cmdCauHinhToKhai1";
            // 
            // cmdThietLapIn1
            // 
            this.cmdThietLapIn1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThietLapIn1.Icon")));
            this.cmdThietLapIn1.Key = "cmdThietLapIn";
            this.cmdThietLapIn1.Name = "cmdThietLapIn1";
            this.cmdThietLapIn1.Text = "Cấu hình thông số in báo cáo";
            // 
            // cmdCauHinhToKhai
            // 
            this.cmdCauHinhToKhai.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Name = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Text = "Cấu hình tham số mặc định của tờ khai";
            // 
            // QuanTri
            // 
            this.QuanTri.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.QuanLyNguoiDung1,
            this.QuanLyNhom1});
            this.QuanTri.Key = "QuanTri";
            this.QuanTri.Name = "QuanTri";
            this.QuanTri.Text = "Quản trị";
            // 
            // QuanLyNguoiDung1
            // 
            this.QuanLyNguoiDung1.Icon = ((System.Drawing.Icon)(resources.GetObject("QuanLyNguoiDung1.Icon")));
            this.QuanLyNguoiDung1.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung1.Name = "QuanLyNguoiDung1";
            // 
            // QuanLyNhom1
            // 
            this.QuanLyNhom1.Icon = ((System.Drawing.Icon)(resources.GetObject("QuanLyNhom1.Icon")));
            this.QuanLyNhom1.Key = "QuanLyNhom";
            this.QuanLyNhom1.Name = "QuanLyNhom1";
            // 
            // QuanLyNguoiDung
            // 
            this.QuanLyNguoiDung.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Name = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Text = "Quản lý người dùng";
            // 
            // QuanLyNhom
            // 
            this.QuanLyNhom.Key = "QuanLyNhom";
            this.QuanLyNhom.Name = "QuanLyNhom";
            this.QuanLyNhom.Text = "Quản lý nhóm người dùng";
            // 
            // LoginUser
            // 
            this.LoginUser.Icon = ((System.Drawing.Icon)(resources.GetObject("LoginUser.Icon")));
            this.LoginUser.Key = "LoginUser";
            this.LoginUser.Name = "LoginUser";
            this.LoginUser.Text = "Đăng nhập người dùng khác";
            // 
            // cmdChangePass
            // 
            this.cmdChangePass.Key = "cmdChangePass";
            this.cmdChangePass.Name = "cmdChangePass";
            this.cmdChangePass.Text = "Đổi mật khẩu";
            // 
            // cmdThietLapCHDN
            // 
            this.cmdThietLapCHDN.Key = "cmdThietLapCHDN";
            this.cmdThietLapCHDN.Name = "cmdThietLapCHDN";
            this.cmdThietLapCHDN.Text = "Thiết lập thông tin Doanh Nghiệp";
            // 
            // MaHS
            // 
            this.MaHS.Key = "MaHS";
            this.MaHS.Name = "MaHS";
            this.MaHS.Text = "Tra cứu mã HS";
            // 
            // DonViDoiTac
            // 
            this.DonViDoiTac.Key = "DonViDoiTac";
            this.DonViDoiTac.Name = "DonViDoiTac";
            this.DonViDoiTac.Text = "Đơn vị đối tác";
            // 
            // cmdAutoUpdate
            // 
            this.cmdAutoUpdate.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate.Name = "cmdAutoUpdate";
            this.cmdAutoUpdate.Text = "&Cập nhật chương trình";
            // 
            // cmdXuatDuLieuChoPhongKhai
            // 
            this.cmdXuatDuLieuChoPhongKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdXuatToKhaiDauTu1,
            this.cmdXuatToKhaiKinhDoanh1});
            this.cmdXuatDuLieuChoPhongKhai.Key = "cmdXuatDuLieuChoPhongKhai";
            this.cmdXuatDuLieuChoPhongKhai.Name = "cmdXuatDuLieuChoPhongKhai";
            this.cmdXuatDuLieuChoPhongKhai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdXuatToKhaiDauTu1
            // 
            this.cmdXuatToKhaiDauTu1.Key = "cmdXuatToKhaiDauTu";
            this.cmdXuatToKhaiDauTu1.Name = "cmdXuatToKhaiDauTu1";
            // 
            // cmdXuatToKhaiKinhDoanh1
            // 
            this.cmdXuatToKhaiKinhDoanh1.Key = "cmdXuatToKhaiKinhDoanh";
            this.cmdXuatToKhaiKinhDoanh1.Name = "cmdXuatToKhaiKinhDoanh1";
            // 
            // cmdXuatToKhaiDauTu
            // 
            this.cmdXuatToKhaiDauTu.Key = "cmdXuatToKhaiDauTu";
            this.cmdXuatToKhaiDauTu.Name = "cmdXuatToKhaiDauTu";
            this.cmdXuatToKhaiDauTu.Text = "Tờ khai đầu tư";
            // 
            // cmdXuatToKhaiKinhDoanh
            // 
            this.cmdXuatToKhaiKinhDoanh.Key = "cmdXuatToKhaiKinhDoanh";
            this.cmdXuatToKhaiKinhDoanh.Name = "cmdXuatToKhaiKinhDoanh";
            this.cmdXuatToKhaiKinhDoanh.Text = "Tờ khai kinh doanh";
            // 
            // cmdEng
            // 
            this.cmdEng.Key = "cmdEng";
            this.cmdEng.Name = "cmdEng";
            this.cmdEng.Text = "Tiếng Anh";
            // 
            // cmdVN
            // 
            this.cmdVN.Key = "cmdVN";
            this.cmdVN.Name = "cmdVN";
            this.cmdVN.Text = "Tiếng Việt";
            // 
            // cmdActivate
            // 
            this.cmdActivate.Image = ((System.Drawing.Image)(resources.GetObject("cmdActivate.Image")));
            this.cmdActivate.Key = "cmdActivate";
            this.cmdActivate.Name = "cmdActivate";
            this.cmdActivate.Shortcut = System.Windows.Forms.Shortcut.CtrlF10;
            this.cmdActivate.Text = "Kích hoạt phần mềm";
            // 
            // cmdCloseMe
            // 
            this.cmdCloseMe.Key = "cmdCloseMe";
            this.cmdCloseMe.Name = "cmdCloseMe";
            this.cmdCloseMe.Text = "Đóng cửa sổ này";
            // 
            // cmdCloseAllButMe
            // 
            this.cmdCloseAllButMe.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Name = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Text = "Đóng các cửa sổ khác";
            // 
            // cmdCloseAll
            // 
            this.cmdCloseAll.Key = "cmdCloseAll";
            this.cmdCloseAll.Name = "cmdCloseAll";
            this.cmdCloseAll.Text = "Đóng hết các cửa sổ";
            // 
            // QuanLyMess
            // 
            this.QuanLyMess.ImageIndex = 33;
            this.QuanLyMess.Key = "QuanLyMess";
            this.QuanLyMess.Name = "QuanLyMess";
            this.QuanLyMess.Text = "Quản lý Message khai báo";
            // 
            // cmdQuery
            // 
            this.cmdQuery.ImageIndex = 36;
            this.cmdQuery.Key = "cmdQuery";
            this.cmdQuery.Name = "cmdQuery";
            this.cmdQuery.Text = "Truy vấn Query";
            // 
            // cmdLog
            // 
            this.cmdLog.ImageIndex = 37;
            this.cmdLog.Key = "cmdLog";
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Text = "Nhật ký chương trình";
            // 
            // cmdDataVersion
            // 
            this.cmdDataVersion.ImageIndex = 36;
            this.cmdDataVersion.Key = "cmdDataVersion";
            this.cmdDataVersion.Name = "cmdDataVersion";
            this.cmdDataVersion.Text = "Dữ liệu phiên bản: ?";
            // 
            // cmdnhomcuakhau
            // 
            this.cmdnhomcuakhau.ImageIndex = 38;
            this.cmdnhomcuakhau.Key = "cmdnhomcuakhau";
            this.cmdnhomcuakhau.Name = "cmdnhomcuakhau";
            this.cmdnhomcuakhau.Text = "Nhóm cửa khẩu";
            // 
            // mnuRightClick
            // 
            this.mnuRightClick.CommandManager = this.cmMain;
            this.mnuRightClick.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCloseMe1,
            this.Separator2,
            this.cmdCloseAllButMe1,
            this.cmdCloseAll1});
            this.mnuRightClick.Key = "mnuRightClick";
            // 
            // cmdCloseMe1
            // 
            this.cmdCloseMe1.Key = "cmdCloseMe";
            this.cmdCloseMe1.Name = "cmdCloseMe1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdCloseAllButMe1
            // 
            this.cmdCloseAllButMe1.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe1.Name = "cmdCloseAllButMe1";
            // 
            // cmdCloseAll1
            // 
            this.cmdCloseAll1.Key = "cmdCloseAll";
            this.cmdCloseAll1.Name = "cmdCloseAll1";
            // 
            // ilSmall
            // 
            this.ilSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmall.ImageStream")));
            this.ilSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmall.Images.SetKeyName(0, "");
            this.ilSmall.Images.SetKeyName(1, "");
            this.ilSmall.Images.SetKeyName(2, "");
            this.ilSmall.Images.SetKeyName(3, "");
            this.ilSmall.Images.SetKeyName(4, "");
            this.ilSmall.Images.SetKeyName(5, "");
            this.ilSmall.Images.SetKeyName(6, "");
            this.ilSmall.Images.SetKeyName(7, "");
            this.ilSmall.Images.SetKeyName(8, "");
            this.ilSmall.Images.SetKeyName(9, "");
            this.ilSmall.Images.SetKeyName(10, "");
            this.ilSmall.Images.SetKeyName(11, "");
            this.ilSmall.Images.SetKeyName(12, "");
            this.ilSmall.Images.SetKeyName(13, "");
            this.ilSmall.Images.SetKeyName(14, "");
            this.ilSmall.Images.SetKeyName(15, "");
            this.ilSmall.Images.SetKeyName(16, "");
            this.ilSmall.Images.SetKeyName(17, "");
            this.ilSmall.Images.SetKeyName(18, "");
            this.ilSmall.Images.SetKeyName(19, "");
            this.ilSmall.Images.SetKeyName(20, "");
            this.ilSmall.Images.SetKeyName(21, "");
            this.ilSmall.Images.SetKeyName(22, "");
            this.ilSmall.Images.SetKeyName(23, "");
            this.ilSmall.Images.SetKeyName(24, "");
            this.ilSmall.Images.SetKeyName(25, "");
            this.ilSmall.Images.SetKeyName(26, "");
            this.ilSmall.Images.SetKeyName(27, "");
            this.ilSmall.Images.SetKeyName(28, "");
            this.ilSmall.Images.SetKeyName(29, "");
            this.ilSmall.Images.SetKeyName(30, "");
            this.ilSmall.Images.SetKeyName(31, "");
            this.ilSmall.Images.SetKeyName(32, "");
            this.ilSmall.Images.SetKeyName(33, "");
            this.ilSmall.Images.SetKeyName(34, "");
            this.ilSmall.Images.SetKeyName(35, "file_temp.png");
            this.ilSmall.Images.SetKeyName(36, "RightDatabase32.gif");
            this.ilSmall.Images.SetKeyName(37, "shell32_279.ico");
            this.ilSmall.Images.SetKeyName(38, "folder_page.png");
            this.ilSmall.Images.SetKeyName(39, "en-US.gif");
            this.ilSmall.Images.SetKeyName(40, "vi-VN.gif");
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 24);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 494);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbMenu);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(828, 26);
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExccel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdThoat1
            // 
            this.cmdThoat1.Key = "cmdThoat";
            this.cmdThoat1.Name = "cmdThoat1";
            // 
            // pmMain
            // 
            this.pmMain.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.pmMain.ContainerControl = this;
            this.pmMain.DefaultPanelSettings.ActiveCaptionMode = Janus.Windows.UI.Dock.ActiveCaptionMode.Never;
            this.pmMain.DefaultPanelSettings.CaptionDisplayMode = Janus.Windows.UI.Dock.PanelCaptionDisplayMode.Text;
            this.pmMain.DefaultPanelSettings.CaptionHeight = 30;
            this.pmMain.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.pmMain.DefaultPanelSettings.DarkCaptionFormatStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.pmMain.DefaultPanelSettings.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.Window;
            this.pmMain.DefaultPanelSettings.TabStateStyles.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pmMain.TabbedMdi = true;
            this.pmMain.TabbedMdiSettings.TabStateStyles.SelectedFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.pmMain.Tag = null;
            this.pmMain.VisualStyleManager = this.vsmMain;
            this.pmMain.MdiTabMouseDown += new Janus.Windows.UI.Dock.MdiTabMouseEventHandler(this.pmMain_MdiTabMouseDown);
            this.uiPanel0.Id = new System.Guid("68a872fc-c8e7-4413-ab63-47771a460c96");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("d3cc0238-54e6-4f57-94ab-6819514dfdf9");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.pmMain.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.pmMain.BeginPanelInfo();
            this.pmMain.AddDockPanelInfo(new System.Guid("68a872fc-c8e7-4413-ab63-47771a460c96"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(200, 488), true);
            this.pmMain.AddDockPanelInfo(new System.Guid("d3cc0238-54e6-4f57-94ab-6819514dfdf9"), new System.Guid("68a872fc-c8e7-4413-ab63-47771a460c96"), -1, true);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(88, 116), new System.Drawing.Size(0, 6), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("2b3e5f09-9a24-4b99-bf7e-8ee886f8383d"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("d5e59413-5184-45bc-bbc5-9b40a268e6ec"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("52dc898d-e5c5-4c3e-964e-6134d41411e2"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("038f8df0-b141-4aac-bb44-6015ce71b26f"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("ab8bee13-8397-4584-b8e9-5cfc2b506e10"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("adc6599d-0d45-4f54-a9f5-4903d12e3180"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("68a872fc-c8e7-4413-ab63-47771a460c96"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("d3cc0238-54e6-4f57-94ab-6819514dfdf9"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.uiPanel0.Location = new System.Drawing.Point(3, 29);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.SelectedPanel = this.uiPanel1;
            this.uiPanel0.Size = new System.Drawing.Size(200, 488);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Panel 0";
            // 
            // uiPanel1
            // 
            this.uiPanel1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiPanel1.Icon")));
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(196, 416);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Loai hình KD - ĐT";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.explorerBar1);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(194, 385);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // explorerBar1
            // 
            this.explorerBar1.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar1.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem58.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem58.Icon")));
            explorerBarItem58.Key = "HangHoaNhap";
            explorerBarItem58.Text = "Hàng hóa nhập";
            explorerBarItem59.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem59.Icon")));
            explorerBarItem59.Key = "HangHoaXuat";
            explorerBarItem59.Text = "Hàng hóa xuất";
            explorerBarGroup21.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem58,
            explorerBarItem59});
            explorerBarGroup21.Key = "grpHangHoa";
            explorerBarGroup21.Text = "Hàng hóa";
            explorerBarItem60.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem60.Icon")));
            explorerBarItem60.Key = "tkNhapKhau";
            explorerBarItem60.Text = "Nhập khẩu";
            explorerBarItem61.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem61.Icon")));
            explorerBarItem61.Key = "tkXuatKhau";
            explorerBarItem61.Text = "Xuất khẩu";
            explorerBarItem62.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem62.Icon")));
            explorerBarItem62.Key = "TheoDoiTKSXXK";
            explorerBarItem62.Text = "Theo dõi ";
            explorerBarItem63.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem63.Icon")));
            explorerBarItem63.Key = "ToKhaiSXXKDangKy";
            explorerBarItem63.Text = "Đã đăng ký";
            explorerBarGroup22.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem60,
            explorerBarItem61,
            explorerBarItem62,
            explorerBarItem63});
            explorerBarGroup22.Key = "grpToKhai";
            explorerBarGroup22.Text = "Tờ khai kinh doanh";
            explorerBarItem64.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem64.Icon")));
            explorerBarItem64.Key = "tkNhapDT";
            explorerBarItem64.Text = "Tờ khai nhập";
            explorerBarItem65.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem65.Icon")));
            explorerBarItem65.Key = "tkXuatDT";
            explorerBarItem65.Text = "Tờ khai xuất";
            explorerBarItem66.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem66.Icon")));
            explorerBarItem66.Key = "tkTheoDoiDT";
            explorerBarItem66.Text = "Theo dõi";
            explorerBarItem67.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem67.Icon")));
            explorerBarItem67.Key = "tkDTDangKy";
            explorerBarItem67.Text = "Đã đăng ký";
            explorerBarGroup23.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem64,
            explorerBarItem65,
            explorerBarItem66,
            explorerBarItem67});
            explorerBarGroup23.Key = "grpDauTu";
            explorerBarGroup23.Text = "Tờ khai đầu tư";
            explorerBarItem68.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem68.Icon")));
            explorerBarItem68.Key = "TriGiaXK";
            explorerBarItem68.Text = "Báo cáo xuất khẩu";
            explorerBarItem69.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem69.Icon")));
            explorerBarItem69.Key = "ThongKeTK";
            explorerBarItem69.Text = "Thống kê tờ khai ";
            explorerBarItem70.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem70.Icon")));
            explorerBarItem70.Key = "ThongKeKimNgachNuoc_MatHang";
            explorerBarItem70.Text = "Kim ngạch xuất khẩu theo từng nước, đối tác";
            explorerBarGroup24.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem68,
            explorerBarItem69,
            explorerBarItem70});
            explorerBarGroup24.Key = "grpbaocao";
            explorerBarGroup24.Text = "Báo cáo-Thống kê";
            this.explorerBar1.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup21,
            explorerBarGroup22,
            explorerBarGroup23,
            explorerBarGroup24});
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBar1.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBar1.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBar1.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBar1.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBar1.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBar1.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBar1.Location = new System.Drawing.Point(0, 0);
            this.explorerBar1.Name = "explorerBar1";
            this.explorerBar1.Size = new System.Drawing.Size(194, 385);
            this.explorerBar1.TabIndex = 1;
            this.explorerBar1.Text = "explorerBar1";
            this.explorerBar1.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.explorerBar1.VisualStyleManager = this.vsmMain;
            this.explorerBar1.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expSXXK_ItemClick);
            // 
            // pnlSXXK
            // 
            this.pnlSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSXXK.Icon")));
            this.pnlSXXK.InnerContainer = this.pnlSXXKContainer;
            this.pnlSXXK.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXK.Name = "pnlSXXK";
            this.pnlSXXK.Size = new System.Drawing.Size(209, 225);
            this.pnlSXXK.TabIndex = 4;
            this.pnlSXXK.Text = "Loại hình SXXK";
            // 
            // pnlSXXKContainer
            // 
            this.pnlSXXKContainer.Controls.Add(this.expSXXK);
            this.pnlSXXKContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXKContainer.Name = "pnlSXXKContainer";
            this.pnlSXXKContainer.Size = new System.Drawing.Size(209, 225);
            this.pnlSXXKContainer.TabIndex = 0;
            // 
            // expSXXK
            // 
            this.expSXXK.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expSXXK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expSXXK.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarGroup1.Expanded = false;
            explorerBarItem1.Key = "nplKhaiBao";
            explorerBarItem1.Text = "Khai báo ";
            explorerBarItem2.Key = "nplTheoDoi";
            explorerBarItem2.Text = "Theo dõi";
            explorerBarItem3.Key = "nplDaDangKy";
            explorerBarItem3.Text = "Đã đăng ký";
            explorerBarGroup1.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem1,
            explorerBarItem2,
            explorerBarItem3});
            explorerBarGroup1.Key = "grpNguyenPhuLieu";
            explorerBarGroup1.Text = "Nguyên phụ liệu";
            explorerBarGroup2.Expanded = false;
            explorerBarItem4.Key = "spKhaiBao";
            explorerBarItem4.Text = "Khai báo";
            explorerBarItem5.Key = "spTheoDoi";
            explorerBarItem5.Text = "Theo dõi";
            explorerBarItem6.Key = "spDaDangKy";
            explorerBarItem6.Text = "Đã đăng ký";
            explorerBarGroup2.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem4,
            explorerBarItem5,
            explorerBarItem6});
            explorerBarGroup2.Key = "grpSanPham";
            explorerBarGroup2.Text = "Sản phẩm";
            explorerBarItem7.Key = "dmKhaiBao";
            explorerBarItem7.Text = "Khai báo";
            explorerBarItem8.Key = "dmTheoDoi";
            explorerBarItem8.Text = "Theo dõi";
            explorerBarItem9.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem9.Icon")));
            explorerBarItem9.Key = "dmDaDangKy";
            explorerBarItem9.Text = "Đã đăng ký";
            explorerBarGroup3.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem7,
            explorerBarItem8,
            explorerBarItem9});
            explorerBarGroup3.Key = "grpDinhMuc";
            explorerBarGroup3.Text = "Định mức";
            explorerBarItem10.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem10.Icon")));
            explorerBarItem10.Key = "tkNhapKhau";
            explorerBarItem10.Text = "Nhập khẩu";
            explorerBarItem11.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem11.Icon")));
            explorerBarItem11.Key = "tkXuatKhau";
            explorerBarItem11.Text = "Xuất khẩu";
            explorerBarItem12.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem12.Icon")));
            explorerBarItem12.Key = "TheoDoiTKSXXK";
            explorerBarItem12.Text = "Theo dõi ";
            explorerBarItem13.Key = "ToKhaiSXXKDangKy";
            explorerBarItem13.Text = "Đã đăng ký";
            explorerBarItem71.Key = "tkHetHan";
            explorerBarItem71.Text = "Săp hết hạn TK";
            explorerBarGroup4.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem10,
            explorerBarItem11,
            explorerBarItem12,
            explorerBarItem13,
            explorerBarItem71});
            explorerBarGroup4.Key = "grpToKhai";
            explorerBarGroup4.Text = "Tờ khai";
            explorerBarItem72.Key = "AddHSTL";
            explorerBarItem72.Text = "Tạo mới";
            explorerBarItem73.Key = "UpdateHSTL";
            explorerBarItem73.Text = "Cập nhật";
            explorerBarItem74.Key = "HSTLManage";
            explorerBarItem74.Text = "Theo dõi";
            explorerBarItem75.Key = "HSTLClosed";
            explorerBarItem75.Text = "Đã đóng";
            explorerBarGroup25.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem72,
            explorerBarItem73,
            explorerBarItem74,
            explorerBarItem75});
            explorerBarGroup25.Key = "grpThanhLy";
            explorerBarGroup25.Text = "Hồ sơ thanh lý";
            this.expSXXK.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup1,
            explorerBarGroup2,
            explorerBarGroup3,
            explorerBarGroup4,
            explorerBarGroup25});
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expSXXK.ImageSize = new System.Drawing.Size(16, 16);
            this.expSXXK.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expSXXK.Location = new System.Drawing.Point(0, 0);
            this.expSXXK.Name = "expSXXK";
            this.expSXXK.Size = new System.Drawing.Size(209, 225);
            this.expSXXK.TabIndex = 0;
            this.expSXXK.Text = "explorerBar1";
            this.expSXXK.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expSXXK.VisualStyleManager = this.vsmMain;
            this.expSXXK.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expSXXK_ItemClick);
            // 
            // pnlGiaCong
            // 
            this.pnlGiaCong.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlGiaCong.Icon")));
            this.pnlGiaCong.InnerContainer = this.pnlGiaCongContainer;
            this.pnlGiaCong.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCong.Name = "pnlGiaCong";
            this.pnlGiaCong.Size = new System.Drawing.Size(209, 225);
            this.pnlGiaCong.TabIndex = 4;
            this.pnlGiaCong.Text = "Loại hình gia công";
            // 
            // pnlGiaCongContainer
            // 
            this.pnlGiaCongContainer.Controls.Add(this.expGiaCong);
            this.pnlGiaCongContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCongContainer.Name = "pnlGiaCongContainer";
            this.pnlGiaCongContainer.Size = new System.Drawing.Size(209, 225);
            this.pnlGiaCongContainer.TabIndex = 0;
            // 
            // expGiaCong
            // 
            this.expGiaCong.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expGiaCong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem14.Key = "hdgcNhap";
            explorerBarItem14.Text = "Khai báo";
            explorerBarItem15.Key = "hdgcManage";
            explorerBarItem15.Text = "Theo dõi";
            explorerBarItem16.Key = "hdgcRegisted";
            explorerBarItem16.Text = "Đã đăng ký";
            explorerBarGroup5.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem14,
            explorerBarItem15,
            explorerBarItem16});
            explorerBarGroup5.Key = "grpHopDong";
            explorerBarGroup5.Text = "Hợp đồng";
            explorerBarItem17.Key = "dmSend";
            explorerBarItem17.Text = "Khai báo";
            explorerBarItem18.Key = "dmManage";
            explorerBarItem18.Text = "Theo dõi";
            explorerBarItem19.Key = "dmRegisted";
            explorerBarItem19.Text = "Đã đăng ký";
            explorerBarGroup6.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem17,
            explorerBarItem18,
            explorerBarItem19});
            explorerBarGroup6.Key = "grpDinhMuc";
            explorerBarGroup6.Text = "Định mức";
            explorerBarItem20.Key = "pkgcNhap";
            explorerBarItem20.Text = "Khai báo";
            explorerBarItem21.Key = "pkgcManage";
            explorerBarItem21.Text = "Theo dõi";
            explorerBarItem22.Key = "pkgcRegisted";
            explorerBarItem22.Text = "Đã đăng ký";
            explorerBarGroup7.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem20,
            explorerBarItem21,
            explorerBarItem22});
            explorerBarGroup7.Key = "grpPhuKien";
            explorerBarGroup7.Text = "Phụ kiện";
            explorerBarItem23.Key = "tkNhapKhau_GC";
            explorerBarItem23.Text = "Nhập khẩu";
            explorerBarItem24.Key = "tkXuatKhau_GC";
            explorerBarItem24.Text = "Xuất khẩu";
            explorerBarGroup8.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem23,
            explorerBarItem24});
            explorerBarGroup8.Key = "grpToKhai";
            explorerBarGroup8.Text = "Tờ khai";
            explorerBarItem25.Key = "tkGCCTNhap";
            explorerBarItem25.Text = "Tờ khai GCCT nhập";
            explorerBarItem26.Key = "tkGCCTXuat";
            explorerBarItem26.Text = "Tờ khai GCCT xuất";
            explorerBarItem27.Key = "theodoiTKCT";
            explorerBarItem27.Text = "Theo dõi";
            explorerBarGroup9.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem25,
            explorerBarItem26,
            explorerBarItem27});
            explorerBarGroup9.Key = "grpGCCT";
            explorerBarGroup9.Text = "Tờ khai GCCT";
            this.expGiaCong.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup5,
            explorerBarGroup6,
            explorerBarGroup7,
            explorerBarGroup8,
            explorerBarGroup9});
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expGiaCong.ImageSize = new System.Drawing.Size(16, 16);
            this.expGiaCong.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expGiaCong.Location = new System.Drawing.Point(0, 0);
            this.expGiaCong.Name = "expGiaCong";
            this.expGiaCong.Size = new System.Drawing.Size(209, 225);
            this.expGiaCong.TabIndex = 1;
            this.expGiaCong.Text = "explorerBar2";
            this.expGiaCong.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expGiaCong.VisualStyleManager = this.vsmMain;
            // 
            // pnlKinhDoanh
            // 
            this.pnlKinhDoanh.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlKinhDoanh.Icon")));
            this.pnlKinhDoanh.InnerContainer = this.pnlKinhDoanhContainer;
            this.pnlKinhDoanh.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanh.Name = "pnlKinhDoanh";
            this.pnlKinhDoanh.Size = new System.Drawing.Size(209, 225);
            this.pnlKinhDoanh.TabIndex = 4;
            this.pnlKinhDoanh.Text = "Loại hình kinh doanh";
            // 
            // pnlKinhDoanhContainer
            // 
            this.pnlKinhDoanhContainer.Controls.Add(this.expKD);
            this.pnlKinhDoanhContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanhContainer.Name = "pnlKinhDoanhContainer";
            this.pnlKinhDoanhContainer.Size = new System.Drawing.Size(209, 225);
            this.pnlKinhDoanhContainer.TabIndex = 0;
            // 
            // expKD
            // 
            this.expKD.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKD.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem28.Key = "tkNhapKhau_KD";
            explorerBarItem28.Text = "Nhập khẩu";
            explorerBarItem29.Key = "tkXuatKhau_KD";
            explorerBarItem29.Text = "Xuất khẩu";
            explorerBarGroup10.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem28,
            explorerBarItem29});
            explorerBarGroup10.Key = "grpToKhai";
            explorerBarGroup10.Text = "Tờ khai";
            this.expKD.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup10});
            this.expKD.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKD.ImageSize = new System.Drawing.Size(16, 16);
            this.expKD.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKD.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKD.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKD.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKD.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKD.Location = new System.Drawing.Point(0, 0);
            this.expKD.Name = "expKD";
            this.expKD.Size = new System.Drawing.Size(209, 225);
            this.expKD.TabIndex = 1;
            this.expKD.Text = "explorerBar1";
            this.expKD.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expKD.VisualStyleManager = this.vsmMain;
            // 
            // pnlDauTu
            // 
            this.pnlDauTu.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlDauTu.Icon")));
            this.pnlDauTu.InnerContainer = this.pnlDauTuContainer;
            this.pnlDauTu.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTu.Name = "pnlDauTu";
            this.pnlDauTu.Size = new System.Drawing.Size(209, 225);
            this.pnlDauTu.TabIndex = 4;
            this.pnlDauTu.Text = "Loại hình đầu tư";
            // 
            // pnlDauTuContainer
            // 
            this.pnlDauTuContainer.Controls.Add(this.expDT);
            this.pnlDauTuContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTuContainer.Name = "pnlDauTuContainer";
            this.pnlDauTuContainer.Size = new System.Drawing.Size(209, 225);
            this.pnlDauTuContainer.TabIndex = 0;
            // 
            // expDT
            // 
            this.expDT.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expDT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expDT.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem30.Key = "tkNhapKhau_DT";
            explorerBarItem30.Text = "Nhập khẩu";
            explorerBarItem31.Key = "tkXuatKhau_DT";
            explorerBarItem31.Text = "Xuất khẩu";
            explorerBarGroup11.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem30,
            explorerBarItem31});
            explorerBarGroup11.Key = "grpToKhai";
            explorerBarGroup11.Text = "Tờ khai";
            this.expDT.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup11});
            this.expDT.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expDT.ImageSize = new System.Drawing.Size(16, 16);
            this.expDT.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expDT.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expDT.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expDT.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expDT.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expDT.Location = new System.Drawing.Point(0, 0);
            this.expDT.Name = "expDT";
            this.expDT.Size = new System.Drawing.Size(209, 225);
            this.expDT.TabIndex = 2;
            this.expDT.Text = "explorerBar1";
            this.expDT.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expDT.VisualStyleManager = this.vsmMain;
            // 
            // pnlSend
            // 
            this.pnlSend.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSend.Icon")));
            this.pnlSend.InnerContainer = this.pnlSendContainer;
            this.pnlSend.Location = new System.Drawing.Point(0, 0);
            this.pnlSend.Name = "pnlSend";
            this.pnlSend.Size = new System.Drawing.Size(209, 225);
            this.pnlSend.TabIndex = 4;
            this.pnlSend.Text = "Khai báo / Theo dõi tờ khai";
            // 
            // pnlSendContainer
            // 
            this.pnlSendContainer.Controls.Add(this.expKhaiBao_TheoDoi);
            this.pnlSendContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSendContainer.Name = "pnlSendContainer";
            this.pnlSendContainer.Size = new System.Drawing.Size(209, 225);
            this.pnlSendContainer.TabIndex = 0;
            // 
            // expKhaiBao_TheoDoi
            // 
            this.expKhaiBao_TheoDoi.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKhaiBao_TheoDoi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKhaiBao_TheoDoi.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem32.Key = "tkManage";
            explorerBarItem32.Text = "Theo dõi tờ khai";
            explorerBarItem33.Key = "tkDaDangKy";
            explorerBarItem33.Text = "Tờ khai đã đăng ký";
            explorerBarGroup12.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem32,
            explorerBarItem33});
            explorerBarGroup12.Key = "grpToKhai";
            explorerBarGroup12.Text = "Tờ khai";
            explorerBarItem34.Key = "tdNPLton";
            explorerBarItem34.Text = "Theo dõi NPL Tồn";
            explorerBarItem35.Key = "tkNPLton";
            explorerBarItem35.Text = "Thống kê NPL tồn";
            explorerBarGroup13.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem34,
            explorerBarItem35});
            explorerBarGroup13.Key = "grpNPLTon";
            explorerBarGroup13.Text = "Nguyên Phụ Liệu Tồn";
            explorerBarItem36.Key = "tkNhap";
            explorerBarItem36.Text = "Theo tờ khai nhập";
            explorerBarItem37.Key = "tkXuat";
            explorerBarItem37.Text = "Theo tờ khai xuất";
            explorerBarGroup14.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem36,
            explorerBarItem37});
            explorerBarGroup14.Key = "grpPhanBo";
            explorerBarGroup14.Text = "Theo dõi phân bổ NPL";
            explorerBarGroup14.Visible = false;
            explorerBarItem38.Key = "tkLHKNhap";
            explorerBarItem38.Text = "Tờ Khai Nhập";
            explorerBarItem39.Key = "tkLHKXuat";
            explorerBarItem39.Text = "Tờ Khai Xuất";
            explorerBarGroup15.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem38,
            explorerBarItem39});
            explorerBarGroup15.Key = "grpTKLoaiHinhKhac";
            explorerBarGroup15.Text = "Tờ khai nhập từ hệ thống khác";
            this.expKhaiBao_TheoDoi.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup12,
            explorerBarGroup13,
            explorerBarGroup14,
            explorerBarGroup15});
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKhaiBao_TheoDoi.ImageSize = new System.Drawing.Size(16, 16);
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKhaiBao_TheoDoi.Location = new System.Drawing.Point(0, 0);
            this.expKhaiBao_TheoDoi.Name = "expKhaiBao_TheoDoi";
            this.expKhaiBao_TheoDoi.Size = new System.Drawing.Size(209, 225);
            this.expKhaiBao_TheoDoi.TabIndex = 1;
            this.expKhaiBao_TheoDoi.Text = "explorerBar1";
            this.expKhaiBao_TheoDoi.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expKhaiBao_TheoDoi.VisualStyleManager = this.vsmMain;
            // 
            // ilMedium
            // 
            this.ilMedium.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilMedium.ImageStream")));
            this.ilMedium.TransparentColor = System.Drawing.Color.Transparent;
            this.ilMedium.Images.SetKeyName(0, "");
            this.ilMedium.Images.SetKeyName(1, "");
            this.ilMedium.Images.SetKeyName(2, "");
            this.ilMedium.Images.SetKeyName(3, "");
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "");
            this.ilLarge.Images.SetKeyName(1, "");
            this.ilLarge.Images.SetKeyName(2, "");
            this.ilLarge.Images.SetKeyName(3, "");
            this.ilLarge.Images.SetKeyName(4, "");
            this.ilLarge.Images.SetKeyName(5, "");
            this.ilLarge.Images.SetKeyName(6, "");
            this.ilLarge.Images.SetKeyName(7, "");
            this.ilLarge.Images.SetKeyName(8, "");
            this.ilLarge.Images.SetKeyName(9, "");
            this.ilLarge.Images.SetKeyName(10, "");
            this.ilLarge.Images.SetKeyName(11, "");
            this.ilLarge.Images.SetKeyName(12, "");
            this.ilLarge.Images.SetKeyName(13, "");
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 520);
            this.statusBar.Name = "statusBar";
            uiStatusBarPanel4.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel4.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel4.Key = "DoanhNghiep";
            uiStatusBarPanel4.ProgressBarValue = 0;
            uiStatusBarPanel4.Width = 352;
            uiStatusBarPanel5.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel5.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel5.Key = "HaiQuan";
            uiStatusBarPanel5.ProgressBarValue = 0;
            uiStatusBarPanel5.ToggleKeyValue = Janus.Windows.UI.StatusBar.ToggleKeyValue.NumLock;
            uiStatusBarPanel5.Width = 351;
            uiStatusBarPanel6.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel6.Key = "System";
            uiStatusBarPanel6.PanelType = Janus.Windows.UI.StatusBar.StatusBarPanelType.ProgressBar;
            uiStatusBarPanel6.ProgressBarValue = 0;
            this.statusBar.Panels.AddRange(new Janus.Windows.UI.StatusBar.UIStatusBarPanel[] {
            uiStatusBarPanel4,
            uiStatusBarPanel5,
            uiStatusBarPanel6});
            this.statusBar.PanelsBorderColor = System.Drawing.SystemColors.ControlDark;
            this.statusBar.Size = new System.Drawing.Size(828, 23);
            this.statusBar.TabIndex = 7;
            this.statusBar.VisualStyleManager = this.vsmMain;
            // 
            // cmdThoat3
            // 
            this.cmdThoat3.Key = "cmdThoat";
            this.cmdThoat3.Name = "cmdThoat3";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // cmdnhomcuakhau1
            // 
            this.cmdnhomcuakhau1.Key = "cmdnhomcuakhau";
            this.cmdnhomcuakhau1.Name = "cmdnhomcuakhau1";
            // 
            // cmdnhomcuakhau2
            // 
            this.cmdnhomcuakhau2.Key = "cmdnhomcuakhau";
            this.cmdnhomcuakhau2.Name = "cmdnhomcuakhau2";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(828, 543);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "SOFTECH.ECS.TQĐT.KD 2.0 ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.statusBar, 0);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).EndInit();
            this.pnlSXXK.ResumeLayout(false);
            this.pnlSXXKContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).EndInit();
            this.pnlGiaCong.ResumeLayout(false);
            this.pnlGiaCongContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).EndInit();
            this.pnlKinhDoanh.ResumeLayout(false);
            this.pnlKinhDoanhContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).EndInit();
            this.pnlDauTu.ResumeLayout(false);
            this.pnlDauTuContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).EndInit();
            this.pnlSend.ResumeLayout(false);
            this.pnlSendContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UICommand cmdThoat3;
        private UIPanelInnerContainer pnlDauTuContainer;
        private ExplorerBar expDT;
        private UIPanel pnlSend;
        private UIPanelInnerContainer pnlSendContainer;
        private ExplorerBar expKhaiBao_TheoDoi;
        private NotifyIcon notifyIcon1;
        private UICommand NhacNho;
        private UICommand DongBoDuLieu;
        private UICommand cmdImport;
        private UICommand cmdImportNPL1;
        private UICommand cmdImportSP1;
        private UICommand cmdImportNPL;
        private UICommand cmdImportSP;
        private UICommand cmdImportDM1;
        private UICommand cmdImportDM;
        private UICommand cmdImportToKhai1;
        private UICommand cmdImportTTDM;
        private UICommand cmdImportToKhai;
        private UICommand cmdImportHangHoa1;
        private UICommand cmdImportHangHoa;
        private UICommand Command01;
        private UICommand Command0;
        private UICommand cmdHelp1;
        private UICommand cmdAbout1;
        private UICommand cmdHelp;
        private UICommand cmdAbout;
        private UICommand Command11;
        private UICommand Command1;
        private UICommand cmd20071;
        private UICommand cmd2007;
        private UICommand cmd2003;
        private UICommand cmd20031;
        private UICommand cmdNPLNhapTon;
        private UICommand cmdDanhMuc1;
        private UICommand cmdDanhMuc;
        private UICommand cmdMaHS;
        private UICommand cmdHaiQuan1;
        private UICommand cmdNuoc1;
        private UICommand cmdMaHS1;
        private UICommand cmdNguyenTe1;
        private UICommand cmdDVT1;
        private UICommand cmdPTTT1;
        private UICommand cmdPTVT1;
        private UICommand cmdDKGH1;
        private UICommand cmdCuaKhau1;
        private UICommand cmdNuoc;
        private UICommand cmdHaiQuan;
        private UICommand cmdNguyenTe;
        private UICommand cmdDVT;
        private UICommand cmdPTTT;
        private UICommand cmdPTVT;
        private UICommand cmdDKGH;
        private UICommand cmdCuaKhau;
        private UICommand cmdRestore1;
        private UICommand cmdBackUp1;
        private UICommand cmdBackUp;
        private UICommand cmdRestore;
        private UICommand ThongSoKetNoi;
        private UICommand TLThongTinDNHQ;
        private UICommand cmdThietLapIn;
        private UICommand cmdNhapToKhaiDauTu;
        private UICommand cmdExportExcel1;
        private UICommand cmdNhapToKhaiKinhDoanh;
        private UICommand cmdDongBoPhongKhai;
        private UICommand cmdImportExcel1;
        private UICommand cmdExportExccel1;
        private UICommand cmdCauHinh1;
        private UICommand cmdCauHinh;
        private UICommand ThongSoKetNoi1;
        private UICommand TLThongTinDNHQ1;
        private UICommand cmdCauHinhToKhai1;
        private UICommand cmdThietLapIn1;
        private UICommand cmdCauHinhToKhai;
        public Janus.Windows.UI.StatusBar.UIStatusBar statusBar;
        private UICommand QuanTri1;
        private UICommand QuanTri;
        private UICommand QuanLyNguoiDung1;
        private UICommand QuanLyNhom1;
        private UICommand QuanLyNguoiDung;
        private UICommand QuanLyNhom;
        private UICommand LoginUser1;
        private UICommand LoginUser;
        private UICommand cmdChangePass1;
        private UICommand cmdChangePass;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private ExplorerBar explorerBar1;
        private UICommand cmdThietLapCHDN;
        private UICommand MaHS1;
        private UICommand MaHS;
        private UICommand DonViDoiTac1;
        private UICommand DonViDoiTac;
        private UICommand cmdAutoUpdate1;
        private UICommand cmdAutoUpdate;
        private UICommand cmdXuatDuLieuChoPhongKhai1;
        private UICommand cmdXuatDuLieuChoPhongKhai;
        private UICommand cmdXuatToKhaiDauTu;
        private UICommand cmdXuatToKhaiDauTu1;
        private UICommand cmdXuatToKhaiKinhDoanh1;
        private UICommand cmdXuatToKhaiKinhDoanh;
        private UICommand cmdEng;
        private UICommand cmdVN;
        private UICommand Separator1;
        private UICommand cmdActivate1;
        private UICommand cmdActivate;
        private UICommand cmdVN1;
        private UICommand cmdEng1;
        private UIContextMenu mnuRightClick;
        private UICommand cmdCloseMe;
        private UICommand cmdCloseAllButMe;
        private UICommand cmdCloseAll;
        private UICommand cmdCloseMe1;
        private UICommand Separator2;
        private UICommand cmdCloseAllButMe1;
        private UICommand cmdCloseAll1;
        private BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private UICommand cmdDongBoPhongKhai1;
        private UICommand QuanLyMess;
        private UICommand QuanLyMess1;
        private OpenFileDialog openFileDialog1;
        private UICommand cmdThietLapCHDN1;
        private UICommand cmdQuery1;
        private UICommand cmdQuery;
        private UICommand cmdLog;
        private UICommand cmdLog1;
        private UICommand cmdDataVersion;
        private UICommand cmdDataVersion1;
        private UICommand cmdnhomcuakhau;
        private UICommand cmdnhomcuakhau1;
        private UICommand cmdnhomcuakhau2;
    }
}
