using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Collections.Generic;

namespace Company.Interface.Report.GC.TT39
{
    public partial class Report_15a_BCQT_SP_GSQL : DevExpress.XtraReports.UI.XtraReport
    {
        public Report_15a_BCQT_SP_GSQL()
        {
            InitializeComponent();
        }
        public void BindReport(KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New BCQTTT39)
        {

            DetailReport.DataSource = BCQTTT39.GoodItemsCollection;

            lblTuNgay.Text = BCQTTT39.NgayBatDauBC.ToString("dd/MM/yyyy");
            lblDenNgay.Text = BCQTTT39.NgayKetThucBC.ToString("dd/MM/yyyy");
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            txtTenHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "TenHangHoa");
            txtMaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaHangHoa");
            txtDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
            txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "TonDauKy", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "NhapTrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtChuyenMDSD.DataBindings.Add("Text", DetailReport.DataSource, "ChuyenMDSD", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtXuatKhac.DataBindings.Add("Text", DetailReport.DataSource, "XuatKhac", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "XuatTrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TonCuoiKy", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GhiChu");
        }
    }
}
