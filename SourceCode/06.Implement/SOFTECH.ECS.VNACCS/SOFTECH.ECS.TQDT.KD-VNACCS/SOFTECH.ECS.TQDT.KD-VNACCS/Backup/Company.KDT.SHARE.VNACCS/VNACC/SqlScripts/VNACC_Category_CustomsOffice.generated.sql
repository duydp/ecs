-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_CustomsOffice]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@CustomsCode,
	@CustomsProvinceCode,
	@OfficeIndicationOfExport,
	@OfficeIndicationOfImport,
	@OfficeIndicationOfBondedRelated,
	@OfficeIndicationOfSupervision,
	@CustomsOfficeName,
	@CustomsOfficeNameInVietnamese,
	@NameOfHeadOfCustomsOfficeInVietnamese,
	@Postcode,
	@AddressInVietnamese,
	@NameOfHeadOfCustomsOfficeInRomanAlphabet,
	@CustomsProvinceName,
	@CustomsBranchName,
	@DestinationCode_1,
	@DestinationCode_2,
	@DestinationCode_3,
	@DestinationCode_4,
	@DestinationCode_5,
	@DestinationCode_6,
	@DestinationCode_7,
	@DestinationCode_8,
	@DestinationCode_9,
	@DestinationCode_10,
	@InspectAtInspectionSite,
	@InspectUsingLargeXrayScanner,
	@TransportationKindCode,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CustomsOffice]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CustomsProvinceCode] = @CustomsProvinceCode,
	[OfficeIndicationOfExport] = @OfficeIndicationOfExport,
	[OfficeIndicationOfImport] = @OfficeIndicationOfImport,
	[OfficeIndicationOfBondedRelated] = @OfficeIndicationOfBondedRelated,
	[OfficeIndicationOfSupervision] = @OfficeIndicationOfSupervision,
	[CustomsOfficeName] = @CustomsOfficeName,
	[CustomsOfficeNameInVietnamese] = @CustomsOfficeNameInVietnamese,
	[NameOfHeadOfCustomsOfficeInVietnamese] = @NameOfHeadOfCustomsOfficeInVietnamese,
	[Postcode] = @Postcode,
	[AddressInVietnamese] = @AddressInVietnamese,
	[NameOfHeadOfCustomsOfficeInRomanAlphabet] = @NameOfHeadOfCustomsOfficeInRomanAlphabet,
	[CustomsProvinceName] = @CustomsProvinceName,
	[CustomsBranchName] = @CustomsBranchName,
	[DestinationCode_1] = @DestinationCode_1,
	[DestinationCode_2] = @DestinationCode_2,
	[DestinationCode_3] = @DestinationCode_3,
	[DestinationCode_4] = @DestinationCode_4,
	[DestinationCode_5] = @DestinationCode_5,
	[DestinationCode_6] = @DestinationCode_6,
	[DestinationCode_7] = @DestinationCode_7,
	[DestinationCode_8] = @DestinationCode_8,
	[DestinationCode_9] = @DestinationCode_9,
	[DestinationCode_10] = @DestinationCode_10,
	[InspectAtInspectionSite] = @InspectAtInspectionSite,
	[InspectUsingLargeXrayScanner] = @InspectUsingLargeXrayScanner,
	[TransportationKindCode] = @TransportationKindCode,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[CustomsCode] = @CustomsCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [CustomsCode] FROM [dbo].[t_VNACC_Category_CustomsOffice] WHERE [CustomsCode] = @CustomsCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CustomsOffice] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CustomsProvinceCode] = @CustomsProvinceCode,
			[OfficeIndicationOfExport] = @OfficeIndicationOfExport,
			[OfficeIndicationOfImport] = @OfficeIndicationOfImport,
			[OfficeIndicationOfBondedRelated] = @OfficeIndicationOfBondedRelated,
			[OfficeIndicationOfSupervision] = @OfficeIndicationOfSupervision,
			[CustomsOfficeName] = @CustomsOfficeName,
			[CustomsOfficeNameInVietnamese] = @CustomsOfficeNameInVietnamese,
			[NameOfHeadOfCustomsOfficeInVietnamese] = @NameOfHeadOfCustomsOfficeInVietnamese,
			[Postcode] = @Postcode,
			[AddressInVietnamese] = @AddressInVietnamese,
			[NameOfHeadOfCustomsOfficeInRomanAlphabet] = @NameOfHeadOfCustomsOfficeInRomanAlphabet,
			[CustomsProvinceName] = @CustomsProvinceName,
			[CustomsBranchName] = @CustomsBranchName,
			[DestinationCode_1] = @DestinationCode_1,
			[DestinationCode_2] = @DestinationCode_2,
			[DestinationCode_3] = @DestinationCode_3,
			[DestinationCode_4] = @DestinationCode_4,
			[DestinationCode_5] = @DestinationCode_5,
			[DestinationCode_6] = @DestinationCode_6,
			[DestinationCode_7] = @DestinationCode_7,
			[DestinationCode_8] = @DestinationCode_8,
			[DestinationCode_9] = @DestinationCode_9,
			[DestinationCode_10] = @DestinationCode_10,
			[InspectAtInspectionSite] = @InspectAtInspectionSite,
			[InspectUsingLargeXrayScanner] = @InspectUsingLargeXrayScanner,
			[TransportationKindCode] = @TransportationKindCode,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[CustomsCode] = @CustomsCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_CustomsOffice]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[CustomsCode],
			[CustomsProvinceCode],
			[OfficeIndicationOfExport],
			[OfficeIndicationOfImport],
			[OfficeIndicationOfBondedRelated],
			[OfficeIndicationOfSupervision],
			[CustomsOfficeName],
			[CustomsOfficeNameInVietnamese],
			[NameOfHeadOfCustomsOfficeInVietnamese],
			[Postcode],
			[AddressInVietnamese],
			[NameOfHeadOfCustomsOfficeInRomanAlphabet],
			[CustomsProvinceName],
			[CustomsBranchName],
			[DestinationCode_1],
			[DestinationCode_2],
			[DestinationCode_3],
			[DestinationCode_4],
			[DestinationCode_5],
			[DestinationCode_6],
			[DestinationCode_7],
			[DestinationCode_8],
			[DestinationCode_9],
			[DestinationCode_10],
			[InspectAtInspectionSite],
			[InspectUsingLargeXrayScanner],
			[TransportationKindCode],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@CustomsCode,
			@CustomsProvinceCode,
			@OfficeIndicationOfExport,
			@OfficeIndicationOfImport,
			@OfficeIndicationOfBondedRelated,
			@OfficeIndicationOfSupervision,
			@CustomsOfficeName,
			@CustomsOfficeNameInVietnamese,
			@NameOfHeadOfCustomsOfficeInVietnamese,
			@Postcode,
			@AddressInVietnamese,
			@NameOfHeadOfCustomsOfficeInRomanAlphabet,
			@CustomsProvinceName,
			@CustomsBranchName,
			@DestinationCode_1,
			@DestinationCode_2,
			@DestinationCode_3,
			@DestinationCode_4,
			@DestinationCode_5,
			@DestinationCode_6,
			@DestinationCode_7,
			@DestinationCode_8,
			@DestinationCode_9,
			@DestinationCode_10,
			@InspectAtInspectionSite,
			@InspectUsingLargeXrayScanner,
			@TransportationKindCode,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Delete]
	@CustomsCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CustomsOffice]
WHERE
	[CustomsCode] = @CustomsCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CustomsOffice] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Load]
	@CustomsCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsOffice]
WHERE
	[CustomsCode] = @CustomsCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CustomsOffice] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsOffice]	

GO

