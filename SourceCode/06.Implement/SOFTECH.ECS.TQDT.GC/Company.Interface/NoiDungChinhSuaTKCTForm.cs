﻿using System;
using System.Collections.Generic;
using Janus.Windows.GridEX;

using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;


namespace Company.Interface
{
    public partial class NoiDungChinhSuaTKCTForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiChuyenTiep TKCT;
        public ToKhaiMauDich TKMD;
        public NoiDungDieuChinhTK noiDungDCTK = new NoiDungDieuChinhTK();
        public List<NoiDungDieuChinhTK> ListNoiDungDieuChinhTK = new List<NoiDungDieuChinhTK>();
        public List<NoiDungDieuChinhTKDetail> ListNoiDungDieuChinhTKChiTiet = new List<NoiDungDieuChinhTKDetail>();
        public static int i;
        //-----------------------------------------------------------------------------------------

        public NoiDungChinhSuaTKCTForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        public void BindData()
        {
            dgList.DataSource = ListNoiDungDieuChinhTK;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                switch (Convert.ToInt32(e.Row.Cells["TrangThai"].Value))
                { 
                    case -1:
                        e.Row.Cells["TrangThai"].Text = setText("Chưa khai báo", "Not declared yet");
                        break;
                    case 0:
                        e.Row.Cells["TrangThai"].Text = setText("Chờ duyệt", "Wait for approval");
                        break;
                    case 1:
                        e.Row.Cells["TrangThai"].Text = setText("Đã duyệt", "Approved");
                        break;
                }
            }
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa nội dung này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NoiDungDieuChinhTK nddctk = new NoiDungDieuChinhTK();
                        nddctk = (NoiDungDieuChinhTK)i.GetRow().DataRow;
                        if (nddctk == null) continue;

                        if (nddctk.ID > 0)
                        {
                            NoiDungDieuChinhTKDetail.DeleteBy_Id_DieuChinh(nddctk.ID);
                            nddctk.Delete();
                        }

                        TKCT.NoiDungDieuChinhTKCollection.Remove(nddctk);
                    }
                }
            }
            //BindData();
            dgList.DataSource = TKCT.NoiDungDieuChinhTKCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
              
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            i++;
            NoiDungChinhSuaTKCTDetailForm ndForm = new NoiDungChinhSuaTKCTDetailForm();
            ndForm.TKCT = TKCT;
            ndForm.isKhaiBoSung = this.isKhaiBoSung;
            ndForm.noiDungDieuChinhTK.SoDieuChinh = i;
            ndForm.ShowDialog();

            dgList.DataSource = TKCT.NoiDungDieuChinhTKCollection;
            try
            { dgList.Refetch(); }
            catch
            { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            NoiDungChinhSuaTKCTDetailForm noiDungChinhSuaTKDetailForm = new NoiDungChinhSuaTKCTDetailForm();
            noiDungChinhSuaTKDetailForm.TKCT = TKCT;
            noiDungChinhSuaTKDetailForm.noiDungDieuChinhTK.ID = Convert.ToInt32(e.Row.Cells["ID"].Value);
            noiDungChinhSuaTKDetailForm.noiDungDieuChinhTK.TrangThai = Convert.ToInt32(e.Row.Cells["TrangThai"].Value);
            noiDungChinhSuaTKDetailForm.noiDungDieuChinhTK.SoDieuChinh = Convert.ToInt32(e.Row.Cells["SoDieuChinh"].Value);
            noiDungChinhSuaTKDetailForm.ShowDialog();

            dgList.DataSource = TKCT.NoiDungDieuChinhTKCollection;
            try
            { dgList.Refetch(); }
            catch
            { dgList.Refresh(); }
        }

        private void NoiDungChinhSuaTKForm_Load(object sender, EventArgs e)
        {
            i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKCT.ID,TKCT.MaLoaiHinh);
            string WHERE = "TKMD_ID= " + TKCT.ID +" AND MALOAIHINH = '" + TKCT.MaLoaiHinh.Trim() +"' ";
            TKCT.NoiDungDieuChinhTKCollection = NoiDungDieuChinhTK.SelectCollectionDynamic(WHERE,"");
            dgList.DataSource = TKCT.NoiDungDieuChinhTKCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            if (noiDungDCTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || noiDungDCTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                btnTaoMoi.Enabled = false;
                btnXoa.Enabled = false;
            }
            //BindData();
        }
   
    }
}
