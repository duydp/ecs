﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
    public class FileAttached
    {
        /// <summary>
        /// Tên File
        /// </summary>
        [XmlElement("fileName")]
        public string FileName { get; set; }
        /// <summary>
        /// Nội dung
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }
    }
}
