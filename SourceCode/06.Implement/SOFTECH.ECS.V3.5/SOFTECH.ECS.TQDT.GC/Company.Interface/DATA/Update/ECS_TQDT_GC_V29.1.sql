-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapNPL]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapNPL]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiTaiXuatNPL]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiTaiXuatNPL]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepNPL]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepNPL]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuat]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuat]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatSanPham]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatSanPham]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepSP]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepSP]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapTB]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapTB]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatTB]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatTB]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepTB]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepTB]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapHM]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapHM]

IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatHM]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatHM]
GO
ALTER VIEW [dbo].[t_View_LuongNPLTrongTKXuat]    
AS    
    SELECT  CASE WHEN t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%V%'    
                 THEN t_KDT_ToKhaiMauDich.LoaiVanDon    
                 ELSE CONVERT(VARCHAR(12), t_KDT_ToKhaiMauDich.SoToKhai)    
            END AS SoToKhaiVNACCS ,    
            dbo.t_KDT_ToKhaiMauDich.MaHaiQuan ,    
            dbo.t_KDT_ToKhaiMauDich.SoToKhai ,    
            dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh ,    
   dbo.t_KDT_ToKhaiMauDich.NgayDangKy ,    
            dbo.t_KDT_HangMauDich.MaPhu AS MaSP ,    
            dbo.t_KDT_HangMauDich.TenHang ,    
      dbo.t_KDT_HangMauDich.DVT_ID,    
            dbo.t_KDT_HangMauDich.SoLuong ,    
            dbo.t_GC_DinhMuc.MaNguyenPhuLieu ,    
            dbo.t_GC_NguyenPhuLieu.Ten ,    
            dbo.t_KDT_HangMauDich.SoLuong * ( dbo.t_GC_DinhMuc.DinhMucSuDung    
                                              / 100    
                                              * dbo.t_GC_DinhMuc.TyLeHaoHut    
                                              + dbo.t_GC_DinhMuc.DinhMucSuDung ) AS LuongNPL ,  
   TriGiaTT,  
   DonGiaTT,    
            dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep ,    
            dbo.t_KDT_ToKhaiMauDich.IDHopDong ,    
            dbo.t_KDT_ToKhaiMauDich.ID ,    
            dbo.t_GC_DinhMuc.DinhMucSuDung ,    
            dbo.t_GC_DinhMuc.TyLeHaoHut ,    
            YEAR(dbo.t_KDT_ToKhaiMauDich.NgayDangKy) AS NamDK    
    FROM    dbo.t_KDT_ToKhaiMauDich    
            INNER JOIN dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID    
            INNER JOIN dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = dbo.t_KDT_HangMauDich.MaPhu    
                                           AND dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_KDT_ToKhaiMauDich.IDHopDong    
            INNER JOIN dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID    
                                                 AND dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma    
    WHERE   ( dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' AND TrangThaiXuLy NOT IN (10,11) );   
  
  

  GO
-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapNPL]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai nhập NPL
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapNPL]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.LoaiVanDon
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaPhu ,
        HMD.SoLuong
FROM    t_KDT_ToKhaiMauDich TKMD
        INNER JOIN t_KDT_HangMauDich HMD ON TKMD.ID = HMD.TKMD_ID
WHERE   HMD.MaPhu = ''' + @MaPhu + ''' AND TKMD.MaLoaiHinh LIKE ''N%''
        AND TKMD.LoaiHangHoa = ''N''
        AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiTaiXuatNPL]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai tái xuất NPL
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiTaiXuatNPL]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.LoaiVanDon
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaPhu ,
        HMD.SoLuong
FROM    t_KDT_ToKhaiMauDich TKMD
        INNER JOIN t_KDT_HangMauDich HMD ON TKMD.ID = HMD.TKMD_ID
WHERE   HMD.MaPhu = ''' + @MaPhu + ''' AND TKMD.MaLoaiHinh LIKE ''X%''
        AND TKMD.LoaiHangHoa = ''N''
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepNPL]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai chuyển tiếp NPL
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepNPL]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.Huongdan_PL
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaHang AS MaPhu ,
        HMD.SoLuong
FROM    t_KDT_GC_ToKhaiChuyenTiep TKMD
        INNER JOIN t_KDT_GC_HangChuyenTiep HMD ON TKMD.ID = HMD.Master_ID
WHERE   HMD.MaHang = ''' + @MaPhu + '''
        AND ( TKMD.MaLoaiHinh LIKE ''%PL%''
              OR TKMD.MaLoaiHinh LIKE ''%18%''
              OR TKMD.MaLoaiHinh LIKE ''%V%''
            )
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO
-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuat]
-- Database: ECS_TQDT_GC_V4_TAT
-- Lượng NPL Quy đổi từ Tờ khai xuất
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuat]
	@HopDong_ID BIGINT,
	@MaNguyenPhuLieu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT *
 FROM   t_View_LuongNPLTrongTKXuat
 WHERE  IDHopDong = '''+ CAST(@HopDong_ID AS NVARCHAR(12))  + '''
        AND MaNguyenPhuLieu = ''' + @MaNguyenPhuLieu + ''''

EXEC sp_executesql @SQL

GO
-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatSanPham]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai xuất sản phẩm
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatSanPham]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.LoaiVanDon
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaPhu ,
        HMD.SoLuong
FROM    t_KDT_ToKhaiMauDich TKMD
        INNER JOIN t_KDT_HangMauDich HMD ON TKMD.ID = HMD.TKMD_ID
WHERE   HMD.MaPhu = ''' + @MaPhu + ''' AND TKMD.MaLoaiHinh LIKE ''X%''
        AND TKMD.LoaiHangHoa = ''S''
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO
-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepSP]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai chuyển tiếp Sản phẩm
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepSP]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.Huongdan_PL
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaHang AS MaPhu ,
        HMD.SoLuong
FROM    t_KDT_GC_ToKhaiChuyenTiep TKMD
        INNER JOIN t_KDT_GC_HangChuyenTiep HMD ON TKMD.ID = HMD.Master_ID
WHERE   HMD.MaHang = ''' + @MaPhu + '''
        AND ( TKMD.MaLoaiHinh LIKE ''%SP%''
              OR TKMD.MaLoaiHinh LIKE ''%19%''
            )
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO
-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapTB]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai nhập TB
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapTB]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.LoaiVanDon
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaPhu ,
        HMD.SoLuong
FROM    t_KDT_ToKhaiMauDich TKMD
        INNER JOIN t_KDT_HangMauDich HMD ON TKMD.ID = HMD.TKMD_ID
WHERE   HMD.MaPhu = ''' + @MaPhu + ''' AND TKMD.MaLoaiHinh LIKE ''N%''
        AND TKMD.LoaiHangHoa = ''T''
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatTB]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai  xuất TB
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatTB]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.LoaiVanDon
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaPhu ,
        HMD.SoLuong
FROM    t_KDT_ToKhaiMauDich TKMD
        INNER JOIN t_KDT_HangMauDich HMD ON TKMD.ID = HMD.TKMD_ID
WHERE   HMD.MaPhu = ''' + @MaPhu + ''' AND TKMD.MaLoaiHinh LIKE ''X%''
        AND TKMD.LoaiHangHoa = ''T''
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepTB]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai chuyển tiếp TB
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepTB]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.Huongdan_PL
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaHang AS MaPhu ,
        HMD.SoLuong
FROM    t_KDT_GC_ToKhaiChuyenTiep TKMD
        INNER JOIN t_KDT_GC_HangChuyenTiep HMD ON TKMD.ID = HMD.Master_ID
WHERE   HMD.MaHang = ''' + @MaPhu + '''
        AND ( TKMD.MaLoaiHinh LIKE ''%TB%''
              OR TKMD.MaLoaiHinh LIKE ''%20%''
            )
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO
-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapHM]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai nhập HM
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapHM]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.LoaiVanDon
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaPhu ,
        HMD.SoLuong
FROM    t_KDT_ToKhaiMauDich TKMD
        INNER JOIN t_KDT_HangMauDich HMD ON TKMD.ID = HMD.TKMD_ID
WHERE   HMD.MaPhu = ''' + @MaPhu + ''' AND TKMD.MaLoaiHinh LIKE ''N%''
        AND TKMD.LoaiHangHoa = ''HM''
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatHM]
-- Database: ECS_TQDT_GC_V4_TAT
-- Tờ khai xuất HM
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatHM]
	@IDHopDong BIGINT,
	@MaPhu NVARCHAR(500)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT  TKMD.ID ,
        CASE WHEN TKMD.MaLoaiHinh LIKE ''%V%'' THEN TKMD.LoaiVanDon
             ELSE CONVERT(NVARCHAR(12), TKMD.SoToKhai)
        END AS SoToKhaiVNACCS ,
        TKMD.SoToKhai ,
        TKMD.SoTiepNhan ,
        TKMD.MaLoaiHinh ,
        TKMD.NgayDangKy ,
        TKMD.NgayTiepNhan ,
        HMD.MaPhu ,
        HMD.SoLuong
FROM    t_KDT_ToKhaiMauDich TKMD
        INNER JOIN t_KDT_HangMauDich HMD ON TKMD.ID = HMD.TKMD_ID
WHERE   HMD.MaPhu = ''' + @MaPhu + ''' AND TKMD.MaLoaiHinh LIKE ''X%''
        AND TKMD.LoaiHangHoa = ''HM''
		AND TKMD.IDHopDong = '''+ CAST(@IDHopDong AS NVARCHAR(12))  + '''
        AND TKMD.TrangThaiXuLy NOT IN ( 10, 11 )'

EXEC sp_executesql @SQL

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '29.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('29.1',GETDATE(), N'CẬP NHẬT PROCEDURE CHI TIẾT HỢP ĐỒNG ĐÃ ĐĂNG KÝ')
END