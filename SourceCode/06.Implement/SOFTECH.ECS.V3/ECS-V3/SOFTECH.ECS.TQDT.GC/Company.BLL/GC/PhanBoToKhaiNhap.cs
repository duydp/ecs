using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.GC.BLL.GC
{
	public partial class PhanBoToKhaiNhap
	{
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKMD(long ID_TKMD, int SoThapPhanNPL)
        {

            string spName = "select * from t_GC_PhanBoToKhaiNhap where TKXuat_ID in " +
                            "( select id from t_GC_PhanBoToKhaiXuat where  ID_TKMD=@ID_TKMD AND MaDoanhNghiep LIKE 'XGC%')";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);


            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;

        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKCT(long ID_TKMD, int SoThapPhanNPL)
        {

            string spName = "select * from t_GC_PhanBoToKhaiNhap where TKXuat_ID in " +
                            "( select id from t_GC_PhanBoToKhaiXuat where  ID_TKMD=@ID_TKMD AND MaDoanhNghiep LIKE 'PH%')";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);


            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;

        }
        public static DataSet SelectDanhSachNPLPhanBoToKhai(long ID_TKMD, long HopDong_ID)
        {

            string spName = "SELECT DISTINCT a.MaNPL, b.Ten AS TenNPL, b.DVT_ID FROM v_GC_PhanBo_TKMD a " +
                            "INNER JOIN (SELECT * FROM t_GC_NguyenPhuLieu WHERE HopDong_ID = " + HopDong_ID +") b ON a.MaNPL = b.Ma " +
                            "WHERE a.ID_TKMD=" + ID_TKMD;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoTKCT(long ID_TKMD, long HopDong_ID)
        {

            string spName = "SELECT DISTINCT a.MaNPL, b.Ten AS TenNPL, b.DVT_ID FROM v_GC_PhanBo_TKCT a " +
                            "INNER JOIN (SELECT * FROM t_GC_NguyenPhuLieu WHERE HopDong_ID = " + HopDong_ID + ") b ON a.MaNPL = b.Ma " +
                            "WHERE a.ID_TKMD=" + ID_TKMD;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoCungUngTK(long ID)
        {

            string spName = "SELECT ID_TKMD,MaNPL, Sum(LuongPhanBo) as LuongPhanBo FROM (SELECT a.ID_TKMD, a.MaNPL, ISNULL(CASE SUBSTRING(a.MaLoaiHInhNhap,1,3) WHEN 'NSX' THEN sum(a.LuongPhanBo) END,0) + sum(a.LuongCungUng) as LuongPhanBo FROM v_GC_PhanBo_TKMD a " +
                            "WHERE a.IDHopDong = " + ID + "  AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)GROUP BY a.MaNPL, a.ID_TKMD, a.MaLoaiHinhNhap) b GROUP BY ID_TKMD, MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoCungUngTKCT(long ID)
        {

            string spName = "SELECT ID_TKMD,MaNPL, Sum(LuongPhanBo) as LuongPhanBo FROM (SELECT a.ID_TKMD, a.MaNPL, ISNULL(CASE SUBSTRING(a.MaLoaiHInhNhap,1,3) WHEN 'NSX' THEN sum(a.LuongPhanBo) END,0) + sum(a.LuongCungUng) as LuongPhanBo FROM v_GC_PhanBo_TKCT a " +
                            "WHERE a.IDHopDong = " + ID + " AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)GROUP BY a.MaNPL, a.ID_TKMD, a.MaLoaiHinhNhap )b GROUP BY ID_TKMD, MaNPL ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            DataSet ds=new DataSet();
            db.LoadDataSet(dbCommand, ds, "dd");
            return ds;

        }
        public static DataSet SelectDanhSachNPLPhanBoCuaSanPham(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT a.* FROM v_GC_PhanBo_TKMD a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD+ " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)ORDER BY a.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            return db.ExecuteDataSet(dbCommand);

        }
        public static bool CheckSanPhamCungUng(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT count(*) FROM v_GC_PhanBo_TKMD a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return Convert.ToInt32(db.ExecuteScalar(dbCommand)) > 0;

        }
        public static bool CheckSanPhamCungUngTKCT(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT count(*) FROM v_GC_PhanBo_TKCT a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return Convert.ToInt32(db.ExecuteScalar(dbCommand)) > 0;

        }
        public static DataSet SelectDanhSachNPLPhanBoCuaSanPhamTKCT(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT a.* FROM v_GC_PhanBo_TKCT a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)ORDER BY a.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoCuaSanPhamGroupByMaNPL(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT a.MaNPL,a.SoToKhaiNhap,a.NgayDangKyNhap,a.MaLoaiHinhNhap, ISNULL(CASE SUBSTRING(a.MaLoaiHInhNhap,1,3) WHEN 'NSX' THEN sum(a.LuongPhanBo) END,0) + sum(a.LuongCungUng) as LuongPhanBo FROM v_GC_PhanBo_TKMD a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)GROUP BY a.MaNPL,a.SoToKhaiNhap,a.NgayDangKyNhap,a.MaLoaiHinhNhap ORDER BY a.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoCuaSanPhamGroupByMaNPLTKCT(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT a.MaNPL,a.SoToKhaiNhap,a.NgayDangKyNhap,a.MaLoaiHinhNhap, ISNULL(CASE SUBSTRING(a.MaLoaiHInhNhap,1,3) WHEN 'NSX' THEN sum(a.LuongPhanBo) END,0) + sum(a.LuongCungUng) as LuongPhanBo FROM v_GC_PhanBo_TKCT a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)GROUP BY a.MaNPL,a.SoToKhaiNhap,a.NgayDangKyNhap,a.MaLoaiHinhNhap ORDER BY a.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSP(long ID_TKMD, string MaSP, int SoThapPhanNPL)
        {
            string spName = "select * from t_GC_PhanBoToKhaiNhap where TKXuat_ID in " +
                           "( select id from t_GC_PhanBoToKhaiXuat where  ID_TKMD=@ID_TKMD AND a.MaDoanhNghiep LIKE 'XGC%   and MaSP=@MaSP)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);

            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSPTKCT(long ID_TKMD, string MaSP, int SoThapPhanNPL)
        {
            string spName = "select * from t_GC_PhanBoToKhaiNhap where TKXuat_ID in " +
                           "( select id from t_GC_PhanBoToKhaiXuat where  ID_TKMD=@ID_TKMD AND a.MaDoanhNghiep LIKE 'PH%    and MaSP=@MaSP)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);

            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
        public static bool CheckPhanBoToKhaiNhap(int SoToKhai,string MaLoaiHinh,string MaHaiQuan,short NamDangKy, long idHopDong)        
        {
            string sql = "select IDHopDong  from t_GC_PhanBoToKhaiNhap pbnhap inner join t_GC_PhanBoToKhaiXuat pbx on pbx.id=pbnhap.TKXuat_ID inner join t_KDT_ToKhaiMauDich tkmd " +
                        " on tkmd.id=pbx.ID_TKMD  " +
                        " where SoToKhaiNhap=@SoToKhaiNhap and MaLoaiHinhNhap=@MaLoaiHinhNhap and MaHaiQuanNhap=@MaHaiQuanNhap and NamDangKyNhap=@NamDangKyNhap " +
                        " and pbnhap.ID = (select max(id) from  t_GC_PhanBoToKhaiNhap where SoToKhaiNhap=@SoToKhaiNhap and MaLoaiHinhNhap=@MaLoaiHinhNhap and MaHaiQuanNhap=@MaHaiQuanNhap and NamDangKyNhap=@NamDangKyNhap)";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuanNhap", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKyNhap", SqlDbType.SmallInt, NamDangKy);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null) return false;

            if (Convert.ToInt64(o) == idHopDong)
                return true;
            sql = "select IDHopDong  from t_GC_PhanBoToKhaiNhap pbnhap inner join t_GC_PhanBoToKhaiXuat pbx on pbx.id=pbnhap.TKXuat_ID inner join t_KDT_GC_ToKhaiChuyenTiep tkct " +
                        " on tkct.id=pbx.ID_TKMD  " +
                        " where SoToKhaiNhap=@SoToKhaiNhap and MaLoaiHinhNhap=@MaLoaiHinhNhap and MaHaiQuanNhap=@MaHaiQuanNhap and NamDangKyNhap=@NamDangKyNhap "+
                        " and pbnhap.ID = (select max(id) from  t_GC_PhanBoToKhaiNhap where SoToKhaiNhap=@SoToKhaiNhap and MaLoaiHinhNhap=@MaLoaiHinhNhap and MaHaiQuanNhap=@MaHaiQuanNhap and NamDangKyNhap=@NamDangKyNhap)";
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuanNhap", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKyNhap", SqlDbType.SmallInt, NamDangKy);
            o = db.ExecuteScalar(dbCommand);

            if (o == null) return false;
            if (Convert.ToInt64(o) == idHopDong)
                return true;
            else
                return false;
            return true;
        }
	}	
}