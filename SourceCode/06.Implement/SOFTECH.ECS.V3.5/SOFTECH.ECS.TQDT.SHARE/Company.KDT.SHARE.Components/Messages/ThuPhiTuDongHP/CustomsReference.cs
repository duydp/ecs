﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class CustomsReference
    {
        /// <summary>
        /// Số tờ khai
        /// </summary>
       [XmlElement("customsDeclare")]
       public string CustomsDeclare { get; set; }

        /// <summary>
        /// Ngày tờ khai YYYY-MM-DD
        /// </summary>
       [XmlElement("customsDeclareDate")]
       public string CustomsDeclareDate { get; set; }

        /// <summary>
       /// Mã loại hình tờ khai
        /// </summary>
       [XmlElement("customsDeclareType")]
       public string CustomsDeclareType { get; set; }

       /// <summary>
       /// Mã cơ quan Hải quan tiếp nhận tờ khai
       /// </summary>
       [XmlElement("customsCode")]
       public string CustomsCode { get; set; }

       /// <summary>
       /// Mã nhóm loại hình hàng hóa
       /// 1: Hàng tạm nhập, tái xuất, hàng chuyển khẩu, hàng gửi kho ngoại quan
       /// 2: Đối với hàng quá cảnh
       /// 0: Đối với hàng hóa nhập khẩu, hàng hóa xuất khẩu
       /// </summary>
       [XmlElement("tariffTypeCode")]
       public string TariffTypeCode { get; set; }

       /// <summary>
       /// Mã phương tiện vận chuyển
       /// </summary>
       [XmlElement("transportCode")]
       public string TransportCode { get; set; }

       /// <summary>
       /// Mã địa điểm lưu kho chờ thông quan
       /// </summary>
       [XmlElement("destinationCode")]
       public string DestinationCode { get; set; }

    }
}
