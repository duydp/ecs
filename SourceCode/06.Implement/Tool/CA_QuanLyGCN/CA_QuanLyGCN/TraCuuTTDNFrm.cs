﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CA_QuanLyGCN
{
    public partial class TraCuuTTDNFrm : Form
    {
        //private LoiPhat loiphat = new LoiPhat();
        //private DoanhNghiep dn = new DoanhNghiep();
        public TraCuuTTDNFrm()
        {
            InitializeComponent();
        }





        private void gdList_DoubleClick(object sender, EventArgs e)
        {
            //try
            //{
                
            //    ThongTinDoanhNghiepfrm frm = new ThongTinDoanhNghiepfrm();
            //    //frm.doanhnghiep = ;
            //    frm.ShowDialog();
            //    btnTim_Click(null, null);
            //}
            //catch (System.Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    MessageBox.Show("Lưu thông lỗi:  " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }



        private void btnThem_Click(object sender, EventArgs e)
        {
            MaDoanhNghiemFrm maDN = new MaDoanhNghiemFrm();
            maDN.ShowDialog(this);
            if (maDN.DialogResult == DialogResult.OK)
            {
                ThongTinDoanhNghiepfrm frm = new ThongTinDoanhNghiepfrm();
                frm.doanhnghiep = new DoanhNghiep();
                frm.trangThai = ThongTinDoanhNghiepfrm.TrangThai.addNew;
                frm.MaDoanhNghiep = maDN.MaDoanhNghiep;
                frm.Text = maDN.MaDoanhNghiep;
                frm.ShowDialog();
            }
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            string tenDoanhNghiep = txtTenDoanhNghiep.Text.Trim();
            string tenGiamDoc = txtTenGiamDoc.Text.Trim();
            string diaChi = txtDiaChi.Text.Trim();
            string TenCoSoKD = txtTenCoSo.Text.Trim();
            string diachiCoSo = txtDiaChiCoSo.Text.Trim();
            string where = "1=1 ";
            if (tenDoanhNghiep != "")
            {
                where = where + " and TENDN like'%" + tenDoanhNghiep + "%' ";
            }
            if (diaChi != "")
            {
                where =  where + " and DiaChi like'%" + diaChi + "%' ";
            }
            if (tenGiamDoc != "")
            {
                where =  where + " and TenNguoiDiaDien like'%" + diaChi + "%' ";
            }
            if (TenCoSoKD != "")
            {
                where = where + "And TenCoSo like '%" + TenCoSoKD + "%'";
            }
            if(diachiCoSo != "")
            {
                where = where + "AND b.DiaChi like '%" + diachiCoSo + "%'";
            }
            if(txtNgayCap.Value > 0)
            {
                where = where + "AND Day(NgayChungNhan) = " + txtNgayCap.Value;
            }
            if (txtThangCap.Value > 0)
            {
                where = where + "AND Month(NgayChungNhan) = " + txtThangCap.Value;
            }
            if (txtNamCap.Value > 2000)
            {
                where = where + "AND Year(NgayChungNhan) = " + txtNamCap.Value;
            }
            gdList.DataSource = DoanhNghiep.SelectDynamicList(where, "ID");
            gdList.Refresh();
        }

        private void gdList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
            {
                //DataTable dt = gridEX1.DataSource as DataTable;
                DoanhNghiep dr = (DoanhNghiep)e.Row.DataRow;
                ThongTinDoanhNghiepfrm f = new ThongTinDoanhNghiepfrm();
                f.doanhnghiep = dr;
                //f.loiphat = dr;
                f.ShowDialog(this);
                btnTim_Click(null,null);
            }
        }

        private void gdList_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {

                if (MessageBox.Show("Bạn có muốn xóa thông tin các doanh nghiệp này", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    Janus.Windows.GridEX.GridEXSelectedItemCollection list = gdList.SelectedItems;
                    if (list.Count > 0)
                    {
                        foreach (Janus.Windows.GridEX.GridEXSelectedItem item in list)
                        {
                            if (item.RowType == Janus.Windows.GridEX.RowType.Record)
                            {
                                //DataTable dt = gridEX1.DataSource as DataTable;
                                DoanhNghiep dr = (DoanhNghiep)item.GetRow().DataRow;
                                dr.Delete();
                                //Int32 id = System.Convert.ToInt32(dr["ID"]);
                                //NganhNghe nn = new NganhNghe();
                                //nn.Id = id;
                                //nn.Delete();
                            }
                        }
                        btnTim_Click(null, null);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}
