﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class FeedBackContent:IssueBase
    {
        /// <summary>
        /// Nội dung gửi từ hải quan.
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public List<AdditionalInformation> AdditionalInformations { get; set; }

        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

        [XmlElement("MessageContent")]
        public string MessageContent { get; set; }

        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        [XmlElement("AdditionalDocument")]
        public AdditionalDocument AdditionalDocument { get; set; }

        [XmlElement("CargoCtrlNo")]
        public CargoCtrlNo CargoCtrlNo { get; set; }

        //[XmlElement("CustomsGoodsItems")]
        //public Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.CustomsGoodsItems CustomsGoodsItems { get; set; }

        //[XmlElement("FileAttachs")]
        //public Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.FileAttachs FileAttachs { get; set; }

        //[XmlElement("ReceiptOfPayment")]
        //public Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.ReceiptOfPayment ReceiptOfPayment { get; set; }
    }
}
