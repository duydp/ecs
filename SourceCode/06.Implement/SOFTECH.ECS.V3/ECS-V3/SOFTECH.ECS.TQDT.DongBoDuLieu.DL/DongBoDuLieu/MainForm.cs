﻿using System;
using System.Windows.Forms;
using Company.BLL;
using System.IO;
using System.Xml.Serialization;
using System.Threading;
using System.Globalization;
using System.Xml;
using Janus.Windows.ExplorerBar;
using Company.QuanTri;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;
using DongBoDuLieu.QuanTri;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace DongBoDuLieu
{
    public partial class MainForm : BaseForm
    {
        public static int flag = 0;
        public static bool isLoginSuccess = false;
        HtmlDocument docHTML = null;
        WebBrowser wbManin = new WebBrowser();
        public static int versionHD = 0;
        public static int typeLogin = 0;
        public static string password = "";
        public static string user = "";
        private FrmDongBoDuLieu dongBoForm;
        private static string PhanHe = string.Empty;
        

        #region Quản trị
        public static ECSPrincipal EcsQuanTri;
        QuanLyNguoiDung NguoiDung;

        #endregion Quản trị

        public static void LoadECSPrincipal(long id)
        {
            EcsQuanTri = new ECSPrincipal(id);
        }

        public MainForm(string[] args)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            timer1.Enabled = false;
            timer1.Stop();
            #region truyền cấu hình

            if (args.Length > 5)
                {
                isLoginSuccess = args[0].ToUpper() == "T" ? true : false;
                PhanHe = args[1].ToUpper();
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DATABASE_NAME", args[2]);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PASS", args[3]);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SERVER_NAME", args[4]);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("USER", args[5]);
                }
            else
                isLoginSuccess = false;
#endregion

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            #region truyền cấu hình
            uiPanel1.Text = "Phân hệ " + PhanHe;
            #endregion
            string strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.Text += " - Build " + strVersion;
            string formType = (Helps.DecryptString(WebService.LoadConfigure("FormType")).ToUpper().Contains(FormType.CLIENT) ? FormType.CLIENT : FormType.SERVER);
            if (formType == FormType.SERVER)
                QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
            else
                QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
            this.BackgroundImage = System.Drawing.Image.FromFile(Application.StartupPath + "//ecsv.jpg");
            this.BackgroundImageLayout = ImageLayout.Stretch;
            if(!isLoginSuccess)
            {
                this.Hide();
                Login login = new Login();
                login.ShowDialog();

                if (isLoginSuccess)
                {
                    this.Show();

                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    backgroundWorker1.RunWorkerAsync();
                }

                else
                    Application.Exit();
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThoat":
                    {
                       // this.Close();
                        Application.Exit();
                    }
                    break;
                case "ThongSoKetNoi":
                    ShowThietLapThongSoKetNoi();
                    break;
                case "TLThongTinDNHQ":
                    ShowThietLapThongTinDNAndHQ();
                    break;
                case "QuanLyNguoiDung":
                    ShowQuanLyNguoiDung();
                    break;
                case "LoginUser":
                    LoginUserKhac();
                    break;
                case "cmdChangePass":
                    ChangePassForm();
                    break;
                case "cmdCloseAll":
                    CloseAll();
                    break;
                case "cmdCloseAllButThis":
                    CloseAllButThis();
                    break;
            }
        }

        private void CloseAllButThis()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (!forms[i].Name.ToString().Equals(this.ActiveMdiChild.Name))
                {
                    forms[i].Close();
                    forms[i].Dispose();
                }
            }
        }

        private void CloseAll()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
                forms[i].Dispose();
            }
        }

        private void ChangePassForm()
        {
            ChangePassForm f = new ChangePassForm();
            f.ShowDialog();
        }
        private void LoginUserKhac()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
            MainForm.EcsQuanTri = null;
            isLoginSuccess = false;
            if (versionHD == 0)
            {
                Login login = new Login();
                login.ShowDialog();
            }

            if (isLoginSuccess)
            {
                this.Show();
                if (versionHD == 0)
                {
                    //if (MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                    //{
                    //    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    //    ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    //    TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    //}
                    //else
                    //{
                    //    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    //    ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //    TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //}
                    //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTuNhap)))
                    //{
                    //    cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //}
                    //else
                    //    cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
            }
            else
                Application.Exit();
        }
        private void ShowQuanLyNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguoiDung = new QuanLyNguoiDung();
            NguoiDung.MdiParent = this;
            NguoiDung.Show();
        }

        private void ShowThietLapThongTinDNAndHQ()
        {
            ThongTinDNAndHQForm f = new ThongTinDNAndHQForm();
            f.ShowDialog();
        }
        private void ShowThietLapThongSoKetNoi()
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();

        }

        private void UpdateStyleForAllForm(string style)
        {
            this.vsmMain.DefaultColorScheme = style;
        }

        private void closeAllButThisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }

        private void expDongBo_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                // Khai báo NPL.
                case "itemDongBo":
                    this.show_FrmDongBoDuLieu();
                    break;
            }
        }

        private void show_FrmDongBoDuLieu()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FrmDongBoDuLieu"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            dongBoForm = new FrmDongBoDuLieu();
            dongBoForm.MdiParent = this;
            dongBoForm.Show();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }


    }
}