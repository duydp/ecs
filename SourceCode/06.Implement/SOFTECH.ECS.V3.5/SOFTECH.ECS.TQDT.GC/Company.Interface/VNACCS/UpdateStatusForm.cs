﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
#if SXXK_V4
using Company.BLL.KDT;
#endif
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT;
#if KD_V4
using Company.KD.BLL.KDT;
#endif

namespace Company.Interface.VNACCS
{
    public partial class UpdateStatusForm : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public UpdateStatusForm()
        {
            InitializeComponent();
        }

        private void UpdateStatusForm_Load(object sender, EventArgs e)
        {
            cbbTrangThai.SelectedValue = 1;
            if (TKMD!=null)
            {
                txtSoTK.Text = TKMD.SoToKhai.ToString();
                txtMaLoaiHinh.Text = TKMD.MaLoaiHinh;
                txtChiCucHQ.Text = TKMD.CoQuanHaiQuan;
                clcNgayDK.Value = TKMD.NgayDangKy;
                cbbTrangThai.SelectedValue = TKMD.TrangThaiXuLy;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                TKMD.TrangThaiXuLy = cbbTrangThai.SelectedValue.ToString();
                TKMD.Update();
                decimal sotk = System.Convert.ToDecimal(TKMD.SoToKhai.ToString().Substring(0, 11));
#if SSXK_V4
                ToKhaiMauDich tkmd = ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
                if (tkmd != null)
                {
                    if (cbbTrangThai.SelectedValue.ToString()== "6")
                    {
                        tkmd.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                        tkmd.Update();      
                    }
                }
#endif
#if GC_V4
                ToKhaiMauDich tkmd = ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
                if (tkmd != null)
                {
                    if (cbbTrangThai.SelectedValue.ToString() == "6")
                    {
                        tkmd.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                        tkmd.Update();
                    }
                }
#endif
                ShowMessage("Cập nhật thành công ",false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
