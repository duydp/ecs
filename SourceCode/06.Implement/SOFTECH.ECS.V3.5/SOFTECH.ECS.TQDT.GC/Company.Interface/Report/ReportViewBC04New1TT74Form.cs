﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;
using Company.GC.BLL.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC04New1TT74Form : BaseForm
    {
        public DataSet ds;
        public HopDong HD = new HopDong();
        private BangKe04New1TT74 BK04;
        public XRLabel Label;
        public bool Is117 = false;
        public ReportViewBC04New1TT74Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            ds = new DataSet();
            BK04 = new BangKe04New1TT74();
            Label = new XRLabel();
            this.BK04.report = this;
            //ds = this.CreatSchemaDataSetRP04();
            ds = this.CreatSchemaDataSetRP04TT74();
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            cbPage.SelectedIndex = 0;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BK04.HD = this.HD;
            this.BK04.First = (cbPage.SelectedIndex == 0);
            this.BK04.xrLabel9.Visible = !Is117;
            if (cbPage.SelectedIndex == cbPage.Items.Count - 1)
            {
                BK04.Last = true;
            }
            else
            {
                BK04.Last = false;
            }
            this.BK04.BindReport(this.ds.Tables[cbPage.SelectedIndex]);
            printControl1.PrintingSystem = this.BK04.PrintingSystem;
            this.BK04.CreateDocument();
        }

        public DataSet CreatSchemaDataSetRP04()
        {

            DataSet dsSTK = new DataSet();
            ToKhaiMauDichCollection tkmdColl = HD.GetTKXKPhanBo();//Lấy danh sách tờ khai xuất cung ứng
            List<ToKhaiChuyenTiep> tkctColl = HD.GetTKCTNhapNPLPhanBo();// Lấy danh sách tờ khai chuyển tiếp xuất cung ứng
            Company.GC.BLL.GC.NguyenPhuLieuCollection nplColl = this.HD.GetNPLCungUngPhanBo();// Lấy danh sách npl cung ứng

            DataTable dt = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCungUngTK(HD.ID).Tables[0];
            DataTable dt1 = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCungUngTKCT(HD.ID).Tables[0];
            int soTable = (nplColl.Count - 1) / 7 + 1;//Tổng số table  được lưu vào biến soTable

            DataTable dtTemp;
            for (int i = 0; i < soTable; i++)
            {
                //Tạo cấu trúc bảng
                dtTemp = new DataTable("tbl_" + i.ToString());
                dtTemp.Columns.Add("MaTKX", Type.GetType("System.Int64"));
                dtTemp.Columns.Add("SoTKX", Type.GetType("System.String"));
                for (int j = i * 7; j < (i + 1) * 7; j++)
                {
                    DataColumn col = new DataColumn();
                    if (j < nplColl.Count)
                    {
                        col.ColumnName = nplColl[j].Ma;
                        col.DataType = typeof(decimal);
                        col.Caption = "Tên: " + nplColl[j].Ten + "\nMã: " + nplColl[j].Ma + "\nĐV tính: " + DonViTinh_GetName(nplColl[j].DVT_ID);
                    }
                    else
                    {
                        col.ColumnName = "col_" + j.ToString();
                        col.Caption = "Tên: \nMã \nĐV tính: ";
                    }
                    dtTemp.Columns.Add(col);
                }
                dtTemp.Columns.Add("HTCU", Type.GetType("System.String"));
                //string strID= "";
                //foreach (ToKhaiMauDich tkmd in tkmdColl)
                //{
                //    strID += tkmd.ID + ",";
                //}
                //if (strID.Length > 0) strID = strID.Remove(strID.Length - 1);
                //if (strID != "")
                {

                    
                    foreach (ToKhaiMauDich tkmd in tkmdColl)
                    {
                        //Đưa từng dòng dữ liệu cung ứng của tờ khai mậu dịch vào bảng dtTemp
                        DataRow dr = dtTemp.NewRow();
                        dr[0] = tkmd.ID;
                        dr[1] = tkmd.SoToKhai.ToString() + "-" + tkmd.MaLoaiHinh + "-" + tkmd.NgayDangKy.Year.ToString();
                        DataRow[] dtRow = dt.Select(" ID_TKMD = " + tkmd.ID);

                        for (int k = 2; k < dtTemp.Columns.Count; k++)
                        {

                            foreach (DataRow row in dtRow)
                            {
                                if (row["MaNPL"].ToString().Trim().ToUpper() == dtTemp.Columns[k].ColumnName.Trim().ToUpper())
                                {
                                    dr[k] = Convert.ToDecimal(row["LuongPhanBo"]);
                                    continue;
                                }
                            }

                        }
                        dtTemp.Rows.Add(dr);
                    }
                }
                

                //strID = "";
                //foreach (ToKhaiChuyenTiep tkct in tkctColl)
                //{
                //    strID += tkct.ID + ",";
                //}
                //if (strID.Length > 0) strID = strID.Remove(strID.Length - 1);
                //if (strID != "")
                {
                    
                    foreach (ToKhaiChuyenTiep tkct in tkctColl)
                    {
                        //Đưa từng dòng dữ liệu cung ứng của tờ khai chuyển tiếp vào bảng dtTemp
                        DataRow dr = dtTemp.NewRow();
                        dr[0] = tkct.ID;
                        dr[1] = tkct.SoToKhai.ToString() + "-" + tkct.MaLoaiHinh + "-" + tkct.NgayDangKy.Year.ToString();
                        DataRow[] dtRow = dt1.Select(" ID_TKMD = " + tkct.ID);

                        for (int k = 2; k < dtTemp.Columns.Count; k++)
                        {

                            foreach (DataRow row in dtRow)
                            {
                                if (row["MaNPL"].ToString().Trim().ToUpper() == dtTemp.Columns[k].ColumnName.Trim().ToUpper())
                                {
                                    dr[k] = Convert.ToDecimal(row["LuongPhanBo"]);
                                    continue;
                                }
                            }

                        }
                        dtTemp.Rows.Add(dr);
                    }
                }
                dsSTK.Tables.Add(dtTemp);//Add bảng dtTemp vào Dataset dsSTK
            }

            return dsSTK;
        }

        public DataSet CreatSchemaDataSetRP04TT74()
        {

            DataSet dsSTK = new DataSet();
            ToKhaiMauDichCollection tkmdColl = HD.GetTKXKPhanBo();//Lấy danh sách tờ khai xuất cung ứng
            List<ToKhaiChuyenTiep> tkctColl = HD.GetTKCTNhapNPLPhanBo();// Lấy danh sách tờ khai chuyển tiếp xuất cung ứng
            Company.GC.BLL.GC.NguyenPhuLieuCollection nplColl = this.HD.GetNPLCungUngPhanBo();// Lấy danh sách npl cung ứng

            DataTable dt = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCungUngTK(HD.ID).Tables[0];// DS NPL 
            DataTable dt1 = PhanBoToKhaiNhap.SelectDanhSachNPLPhanBoCungUngTKCT(HD.ID).Tables[0];// DS NPL
            
            DataTable dtTemp = new DataTable("dtBangNguyenPhuLieu");
            DataColumn[] dcCol = new DataColumn[8];

            dcCol[0] = new DataColumn("STT", typeof(string));
            dcCol[0].Caption = "STT";

            dcCol[1] = new DataColumn();
            dcCol[1].ColumnName = "Tên NPL";
            dcCol[1].DataType = typeof(string);
            dcCol[1].Caption = "Tên nguyên liệu, vật tư";

            dcCol[2] = new DataColumn();
            dcCol[2].ColumnName = "DVTinh";
            dcCol[2].DataType = typeof(string);
            dcCol[2].Caption = "Đơn vị tính";

            dcCol[3] = new DataColumn();
            dcCol[3].ColumnName = "Số lượng";
            dcCol[3].DataType = typeof(decimal);
            dcCol[3].Caption = "Số lượng";

            dcCol[4] = new DataColumn();
            dcCol[4].ColumnName = "Đơn giá";
            dcCol[4].DataType = typeof(decimal);
            dcCol[4].Caption = "Đơn giá";

            dcCol[5] = new DataColumn();
            dcCol[5].ColumnName = "Tổng trị giá";
            dcCol[5].DataType = typeof(decimal);
            dcCol[5].Caption = "Tổng trị giá";

            dcCol[6] = new DataColumn();
            dcCol[6].ColumnName = "Hình thức cung ứng";
            dcCol[6].DataType = typeof(string);
            dcCol[6].Caption = "Hình thức cung ứng";

            dcCol[7] = new DataColumn();
            dcCol[7].ColumnName = "Ghi chú";
            dcCol[7].DataType = typeof(string);
            dcCol[7].Caption = "Ghi chú";

            dtTemp.Columns.AddRange(dcCol);
            int stt = 1;

            foreach (Company.GC.BLL.GC.NguyenPhuLieu npl in nplColl) {
                
                decimal tg = 0;
                decimal donGia = 0;
                decimal tongTriGia = 0;
                string hinhThucCungUng = "";
                decimal TriGia = 0;
                foreach (ToKhaiMauDich tkmd in tkmdColl)
                {
                    DataTable dtA = tkmd.GetNPLCungUngTK(tkmd.ID).Tables[0];// Lấy NPL theo mẫu 4 ko phân bổ
                    DataRow[] dtRowA = dtA.Select(" TKMD_ID = " + tkmd.ID + " and MaNguyenPhuLieu = '" + npl.Ma + "'");

                    DataRow[] dtRow = dt.Select(" ID_TKMD = " + tkmd.ID);
                    foreach (DataRow row in dtRow)
                    {
                        if (row["MaNPL"].ToString().Trim().ToUpper() == npl.Ma.Trim().ToUpper())
                           //&& Decimal.Parse(row["DonGiaTT"].ToString()) == Decimal.Parse(npl.DonGia.ToString()))
                        {

                            if (dtRowA.Length > 0)
                                hinhThucCungUng = dtRowA[0]["HinhThuCungUng"].ToString();
                            TriGia += Decimal.Parse(row["TriGia"].ToString());
                            tongTriGia += TriGia;
                            donGia = 0;// Decimal.Parse(row["DonGiaTT"].ToString()); ;
                        }
                    }
                }
                foreach (ToKhaiChuyenTiep tkct in tkctColl)
                {
                    DataTable dtB = tkct.GetNPLCungUngTKCT(tkct.ID).Tables[0];// Lấy NPL theo mẫu 4 ko phân bổ
                    DataRow[] dtRowB = dtB.Select(" TKCT_ID = " + tkct.ID + " and MaNguyenPhuLieu = '" + npl.Ma + "'");

                    DataRow[] dtRow = dt1.Select(" TKCT_ID = " + tkct.ID);

                    foreach (DataRow row in dtRow)
                    {
                        if (row["MaNPL"].ToString().Trim().ToUpper() == npl.Ma.Trim().ToUpper())
                            //&& Decimal.Parse(row["DonGia"].ToString()) == Decimal.Parse(npl.DonGia.ToString()))
                        {
                            if (dtRowB.Length > 0)
                                hinhThucCungUng = dtRowB[0]["HinhThuCungUng"].ToString();
                            tg += Convert.ToDecimal(row["LuongPhanBo"]);
                            tongTriGia += Decimal.Parse(row["TriGia"].ToString());
                            donGia = 0;// Decimal.Parse(row["DonGiaTT"].ToString()); ;
                        }
                    }
                }
                if (tg > 0)
                {
                    DataRow drData = dtTemp.NewRow();
                    drData[0] = stt;//STT
                    drData[1] = npl.Ten + " / " + npl.Ma;//Tên nguyên liệu, vật tư
                    drData[2] = DonViTinh_GetName(npl.DVT_ID);//Đơn vị tính
                    drData[3] = tg;//Số lượng
                    drData[4] = tongTriGia / tg;//donGia;//Đơn giá 
                    drData[5] = tongTriGia; //tg* donGia;//Tổng trị giá
                    drData[6] = hinhThucCungUng;//Hình thức cung ứng
                    drData[7] = "";//Ghi chú
                    dtTemp.Rows.Add(drData);
                    stt++;
                }
            }
                

            //foreach (ToKhaiMauDich tkmd in tkmdColl)
            //{
            //    //Đưa từng dòng dữ liệu cung ứng của tờ khai mậu dịch vào bảng dtTemp
            //    DataRow dr = dtTemp.NewRow();
            //    dr[0] = tkmd.ID;
            //    dr[1] = tkmd.SoToKhai.ToString() + "-" + tkmd.MaLoaiHinh + "-" + tkmd.NgayDangKy.Year.ToString();
            //    DataRow[] dtRow = dt.Select(" ID_TKMD = " + tkmd.ID);

            //    for (int k = 2; k < dtTemp.Columns.Count; k++)
            //    {

            //        foreach (DataRow row in dtRow)
            //        {
            //            if (row["MaNPL"].ToString().Trim().ToUpper() == dtTemp.Columns[k].ColumnName.Trim().ToUpper())
            //            {
            //                dr[k] = Convert.ToDecimal(row["LuongPhanBo"]);
            //                continue;
            //            }
            //        }

            //    }
            //    dtTemp.Rows.Add(dr);
            //}
        

            //foreach (ToKhaiChuyenTiep tkct in tkctColl)
            //{
            //    //Đưa từng dòng dữ liệu cung ứng của tờ khai chuyển tiếp vào bảng dtTemp
            //    DataRow dr = dtTemp.NewRow();
            //    dr[0] = tkct.ID;
            //    dr[1] = tkct.SoToKhai.ToString() + "-" + tkct.MaLoaiHinh + "-" + tkct.NgayDangKy.Year.ToString();
            //    DataRow[] dtRow = dt1.Select(" ID_TKMD = " + tkct.ID);

            //    for (int k = 2; k < dtTemp.Columns.Count; k++)
            //    {

            //        foreach (DataRow row in dtRow)
            //        {
            //            if (row["MaNPL"].ToString().Trim().ToUpper() == dtTemp.Columns[k].ColumnName.Trim().ToUpper())
            //            {
            //                dr[k] = Convert.ToDecimal(row["LuongPhanBo"]);
            //                continue;
            //            }
            //        }

            //    }
            //    dtTemp.Rows.Add(dr);
            //}
            dsSTK.Tables.Add(dtTemp);//Add bảng dtTemp vào Dataset dsSTK
            
            return dsSTK;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
            }   
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BK04.setText(this.Label, txtName.Text);
            this.BK04.CreateDocument();
        }
   
    }
}