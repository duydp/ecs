﻿
using System;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
using System.Threading;
using KetQuaXuLy = Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3.KetQuaXuLy;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.DongBoDuLieu
{
    class SyncData
    {
        ISyncData wsSyncData = WebService.SyncService();
        public event EventHandler<SyncEventArgs> SyncEventArgs;
        private void OnSynce(SyncEventArgs e)
        {
            if (SyncEventArgs != null)
            {
                SyncEventArgs(this, e);
            }
        }
        #region save Log error
        private void SaveLogError(Detail chitiet, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = chitiet.SoToKhai;
            logError.MaDoanhNghiep = chitiet.MaDoanhNghiep;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = chitiet.GUIDSTR;
            logError.TrangThai = 2;
            logError.InsertUpdate();
        }
        private void SaveLogError(ToKhaiMauDich chitiet, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = chitiet.SoToKhai;
            logError.MaDoanhNghiep = chitiet.MaDoanhNghiep;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = chitiet.GUIDSTR;
            logError.TrangThai = 1;
            logError.InsertUpdate();
        }
        private void SaveLogError(string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = 0;
            logError.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.TrangThai = 0;
            logError.InsertUpdate();
        }
        #endregion
        public void SXXK_SendData(List<ToKhaiMauDich> tokhaiSelect, string user, string password)
        {
            try
            {


                int total = tokhaiSelect.Count;
                int pos = 0;
                int valSend = 0;
                string msgSend = string.Empty;
                foreach (ToKhaiMauDich tkmd in tokhaiSelect)
                {
                    pos++;
                    try
                    {
                        tkmd.LoadHMDCollection();
                        tkmd.LoadChungTuHaiQuan();
                        if (string.IsNullOrEmpty(tkmd.GUIDSTR))
                        {
                            tkmd.GUIDSTR = Guid.NewGuid().ToString();
                            tkmd.Update();
                        }
                        if (tkmd.MaLoaiHinh.Substring(0, 1).ToUpper() != "N")
                            this.SXXK_SendDataDinhMuc(tkmd, user, password);
                        SyncDaTa syncDaTa = new SyncDaTa()
                        {
                            Type = "TKMD",
                            Declaration = new List<SyncDaTaDetail>()
                        };
                        syncDaTa.Declaration.Add(new SyncDaTaDetail
                        {
                            Issuer = user,
                            Business = tkmd.MaDoanhNghiep,
                            Reference = tkmd.GUIDSTR,
                            CustomsReference = tkmd.SoTiepNhan.ToString(),
                            Acceptance = tkmd.NgayDangKy.ToString(),
                            Number = tkmd.SoToKhai.ToString(),
                            NatureOfTransaction = tkmd.MaLoaiHinh,
                            DeclarationOffice = tkmd.MaHaiQuan,
                            StreamlineContent = tkmd.HUONGDAN,
                            Type = "SXXK",
                        });
                        string content = Helpers.Serializer(tkmd);
                        content = Helpers.ConvertToBase64(content);
                        syncDaTa.Content = new Content { Text = content };
                        msgSend = Helpers.Serializer(syncDaTa);
                        msgSend = wsSyncData.SendDaiLy(msgSend, user, password);
                        if (!string.IsNullOrEmpty(msgSend))
                        {
                            //Warring info
                            if (msgSend == "MK")
                            {
                                OnSynce(new SyncEventArgs(new Exception("Sai mật khẩu kết nối")));
                                GlobalSettings.USERNAME_DONGBO = string.Empty;
                                GlobalSettings.PASSWOR_DONGBO = string.Empty;
                                break;
                            }
                            else OnSynce(new SyncEventArgs(new Exception(msgSend)));

                        }
                        else
                        {
                            Status sttk = new Status();
                            sttk.GUIDSTR = tkmd.GUIDSTR;
                            sttk.MSG_STATUS = 1;
                            if (tkmd.MaLoaiHinh.Substring(0, 1).ToUpper() == "N")
                                sttk.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.KD_TOKHAI_NHAP);
                            else
                                sttk.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.KD_TOKHAI_XUAT);
                            sttk.CREATE_TIME = DateTime.Now;
                            sttk.InsertUpdate();
                            valSend++;
                            string msg = string.Format("Đã gửi thông tin {0}/{1}/{2}", tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NamDK);
                            int percent = (int)((pos * 100) / total);
                            OnSynce(new SyncEventArgs(msg, percent));
                        }
                    }
                    catch (Exception ex)
                    {

                        OnSynce(new SyncEventArgs(ex));
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }

                }
                OnSynce(new SyncEventArgs(string.Format("Đã gửi được {0}/{1} tờ khai", valSend, total), (int)((pos * 100) / total)));
            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));
            }
        }
        public void SXXK_SearchData(string user, string password, DateTime fromDate, DateTime toDate, bool timTatCa)
        {
            try
            {
                OnSynce(new SyncEventArgs("Bắt đầu tìm kiếm...", 0));

                SyncDaTa syncdata = new SyncDaTa();
                syncdata.issuer = GlobalSettings.MA_DON_VI;
                syncdata.FromDate = fromDate.ToString("yyyy-MM-dd");
                syncdata.ToDate = toDate.ToString("yyyy-MM-dd");
                syncdata.Type = "SXXK";
                syncdata.Status = timTatCa ? "1" : "0";
                Thread.Sleep(200);
                OnSynce(new SyncEventArgs("Truyền thông tin yêu cầu...", 0));
                string msg = Helpers.ConvertToBase64(Helpers.Serializer(syncdata));
                string msgFeedback = string.Empty;
                msgFeedback = wsSyncData.ReceivesViews(user, password, msg);

                if (msgFeedback == "MK")
                {
                    OnSynce(new SyncEventArgs(new Exception("Sai mật khẩu kết nối")));
                    GlobalSettings.USERNAME_DONGBO = string.Empty;
                    GlobalSettings.PASSWOR_DONGBO = string.Empty;
                    return;
                }
                else
                {
                    if (string.IsNullOrEmpty(msgFeedback))
                    {
                        OnSynce(new SyncEventArgs(new Exception("Không tìm thấy tờ khai")));
                        return;
                    }
                    else
                    {
                        SyncDaTa feedback = new SyncDaTa();
                        try
                        {
                            feedback = Helpers.Deserialize<SyncDaTa>(msgFeedback);
                        }
                        catch (System.Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            OnSynce(new SyncEventArgs(new Exception("Có lỗi trả về từ server: " + msgFeedback)));
                            return;
                        }
                        List<Detail> chitietCO = MapperDBDL.MapperView(feedback.Declaration);
                        OnSynce(new SyncEventArgs("Đã tải dữ liệu từ đại lý về", 100, null, chitietCO));
                    }
                }
            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));

            }
        }
        public void SXXK_GetData(string user, string password, List<Detail> detailSelect)
        {
            try
            {
                int pos = 0;
                int total = detailSelect.Count;
                int valDownload = 0;
                this.SXXK_GetDataDinhMuc(user, password);
                foreach (Detail item in detailSelect)
                {
                    string sfmtToKhai = string.Format("{0}/{1}/{2}", item.SoToKhai, item.MaLoaiHinh, item.MaHaiQuan);
                    OnSynce(new SyncEventArgs("Đang tải " + sfmtToKhai, (int)((pos++ * 100) / total)));
                    string msgFeedback = wsSyncData.SendDoanhNghiep(item.GUIDSTR, user, password);
                    if (msgFeedback == "MK")
                    {
                        OnSynce(new SyncEventArgs(new Exception("Sai tên đăng nhập hoặc mật khẩu ")));
                        GlobalSettings.USERNAME_DONGBO = string.Empty;
                        GlobalSettings.PASSWOR_DONGBO = string.Empty;
                        break;
                    }
                    SyncDaTa xml = new SyncDaTa();

                    try
                    {
                        xml = Helpers.Deserialize<SyncDaTa>(msgFeedback);
                        string content = Helpers.ConvertFromBase64(xml.Content.Text);
                        ToKhaiMauDich tk = Helpers.Deserialize<ToKhaiMauDich>(content);
                        string error = tk.InsertFullFromISyncDaTa();
                        tk.TransgferDataToSXXK();
                        if (!string.IsNullOrEmpty(error))
                        {
                            SaveLogError(item, error);
                            OnSynce(new SyncEventArgs(new Exception(string.Format("Cập nhật dữ liệu {0} không thành công!", sfmtToKhai))));
                        }
                        else
                        {
                            #region Insert Nguyên Phụ Liệu - Sản Phẩm
                            if (tk.MaLoaiHinh.Substring(0,1).ToUpper() == "N")
                            {
                                foreach (Company.BLL.KDT.HangMauDich hmd in tk.HMDCollection)
                                {
                                    Company.BLL.SXXK.NguyenPhuLieu npl = Company.BLL.SXXK.NguyenPhuLieu.getNguyenPhuLieu(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, hmd.MaPhu);
                                    if (npl == null)
                                    {
                                        npl = new Company.BLL.SXXK.NguyenPhuLieu()
                                        {
                                            Ma = hmd.MaPhu,
                                            MaHS = hmd.MaHS,
                                            DVT_ID = hmd.DVT_ID,
                                            MaDoanhNghiep = GlobalSettings.MA_DON_VI,
                                            MaHaiQuan = GlobalSettings.MA_HAI_QUAN,
                                            Ten = hmd.TenHang,

                                        };
                                        npl.Insert();
                                    }
                                }
                            } 
                            else
                            {
                                foreach (Company.BLL.KDT.HangMauDich hmd in tk.HMDCollection)
                                {
                                    Company.BLL.SXXK.SanPham sp = Company.BLL.SXXK.SanPham.getSanPham(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, hmd.MaPhu);
                                    if (sp == null)
                                    {
                                        sp = new Company.BLL.SXXK.SanPham()
                                        {
                                            Ma = hmd.MaPhu,
                                            MaHS = hmd.MaHS,
                                            DVT_ID = hmd.DVT_ID,
                                            MaDoanhNghiep = GlobalSettings.MA_DON_VI,
                                            MaHaiQuan = GlobalSettings.MA_HAI_QUAN,
                                            Ten = hmd.TenHang,

                                        };
                                        sp.Insert();
                                    }
                                }
                            }
                            #endregion
                        }
                            valDownload++;
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        OnSynce(new SyncEventArgs(ex));
                    }
                }
                OnSynce(new SyncEventArgs(string.Format("Đã tải được {0}/{1} tờ khai", valDownload, total), (int)((pos * 100) / total)));

            }
            catch (Exception ex)
            {
                OnSynce(new SyncEventArgs(ex));
            }
        }
        #region Định mức
        private bool SXXK_SendDataDinhMuc(ToKhaiMauDich tkmd, string user, string password)
        {
            int pos = 1;
            foreach (Company.BLL.KDT.HangMauDich hmd in tkmd.HMDCollection)
            {
                OnSynce(new SyncEventArgs("Đồng bộ định mức", (int)((pos++ * 100) / tkmd.HMDCollection.Count)));
                DinhMucDangKy dmdk = DinhMucDangKy.LoadbyMaSanPham(hmd.MaPhu);
                if (dmdk == null)
                {
                    SaveLogError("Không tìm thấy định mức của sản phẩm " + hmd.MaPhu + ":");
                    return false;
                }
                else
                {
                    if (string.IsNullOrEmpty(dmdk.GUIDSTR))
                    {
                        dmdk.GUIDSTR = Guid.NewGuid().ToString();
                        dmdk.Update();
                    }
                    if (dmdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        Status stdm = Status.Load(dmdk.GUIDSTR);
                        if (stdm == null || stdm.MSG_STATUS == 0)
                        {
                            try
                            {
                                dmdk.LoadDMCollection();
                                string msgDM = Helpers.Serializer(dmdk);
                                msgDM = Helpers.ConvertToBase64(msgDM);
                                SyncDaTa syn = new SyncDaTa();
                                syn.Content = new Content { Text = msgDM };
                                syn.Declaration = new List<SyncDaTaDetail>();
                                syn.Declaration.Add(new SyncDaTaDetail
                                {
                                    Business = GlobalSettings.MA_DON_VI,
                                    Reference = dmdk.GUIDSTR,
                                    Type = "SXXK",
                                });
                                syn.Type = "DMSXXK";
                                string msgSendDM = Helpers.Serializer(syn);
                                string DMfeedback = wsSyncData.SendGC(msgSendDM, user, password);
                                if (!string.IsNullOrEmpty(DMfeedback))
                                {
                                    SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức số " + dmdk.ID + ":" + DMfeedback);
                                    return false;
                                }
                                else
                                {
                                    stdm = new Status();
                                    stdm.GUIDSTR = dmdk.GUIDSTR;
                                    stdm.MSG_STATUS = 1;
                                    stdm.MSG_TYPE = Convert.ToInt16(DeclarationIssuer.GC_DINH_MUC);
                                    stdm.CREATE_TIME = DateTime.Now;
                                    stdm.InsertUpdate();
                                }
                            }
                            catch (System.Exception ex)
                            {
                                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức số " + dmdk.ID + ":" + ex.Message);
                                return false;
                            }
                            Thread.Sleep(3000);
                        }
                    }
                    else
                    {
                        SaveLogError("Định mức của sản phẩm " + hmd.MaPhu + "Chưa được HQ phê duyệt");
                        return false;
                    }

                }
            }
            return true;
        }

        private bool SXXK_GetDataDinhMuc(string user, string password)
        {
            try
            {
                int pos = 1;
                string msgFeedBackDM = wsSyncData.ReceivesGC("DMSXXK", user, password);
                while (!string.IsNullOrEmpty(msgFeedBackDM))
                {
                    if (pos == 99)
                        pos = 2;
                    OnSynce(new SyncEventArgs("Đang tải định mức ", pos++));
                    try
                    {
                        if (msgFeedBackDM == "MK")
                        {
                            OnSynce(new SyncEventArgs(new Exception("Sai tên đăng nhập hoặc mật khẩu ")));
                            GlobalSettings.USERNAME_DONGBO = string.Empty;
                            GlobalSettings.PASSWOR_DONGBO = string.Empty;
                            return false;
                        }
                        else
                        {
                            SyncDaTa xml = Helpers.Deserialize<SyncDaTa>(msgFeedBackDM);
                            DinhMucDangKy Dm = Helpers.Deserialize<DinhMucDangKy>(Helpers.ConvertFromBase64(xml.Content.Text));
                            string error = Dm.InsertFullFromISyncDaTa();
                            if (!string.IsNullOrEmpty(error))
                            {
                                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức : " + error);
                                OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu định mức: " + error)));
                                return false;
                            }
                            else
                                Dm.TransferDataToSXXK();
                            Thread.Sleep(3000);
                            msgFeedBackDM = wsSyncData.ReceivesGC("DMSXXK", user, password);
                        }
                    }
                    catch
                    {
                        SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức : " + msgFeedBackDM);
                        OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu định mức: " + msgFeedBackDM)));
                        return false;
                    }
                }
                OnSynce(new SyncEventArgs("Hoàn thành tải định mức", 100));
            }
            catch (System.Exception ex)
            {
                SaveLogError("Lỗi xảy ra khi đồng bộ dữ liệu định mức : " + ex.Message);
                OnSynce(new SyncEventArgs(new Exception("Lỗi xảy ra khi đồng bộ dữ liệu định mức: " + ex.Message)));
                return false;
            }
            return true;
        }
        #endregion

    }
}
