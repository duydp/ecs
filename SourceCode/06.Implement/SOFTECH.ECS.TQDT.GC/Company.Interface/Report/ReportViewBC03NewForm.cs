﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC03NewForm : BaseForm
    {
        ToKhaiMauDich TKMD;
        ToKhaiChuyenTiep TKCT;
        DataSet dsNPL;
        DataSet dsNPLTemp;
        public XRLabel label;
        public DataSet dsNguyenPhuLieu;
        public DataTable dtBangNguyenPhuLieu;
        private BangKe03New BC03;
        public HopDong HD = new HopDong();
        Company.GC.BLL.GC.NguyenPhuLieu NPL;

        int sotable;
        int soLuongNPL;
        public ReportViewBC03NewForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            TKCT = new ToKhaiChuyenTiep();
            TKMD = new ToKhaiMauDich();
            dtBangNguyenPhuLieu = new DataTable();
            BC03 = new BangKe03New();
            label = new XRLabel();
            dsNguyenPhuLieu = new DataSet();
            NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
            dsNPL = this.HD.GetNPLMauDichVaChuyenTiepHopDong();
            soLuongNPL = dsNPL.Tables[0].Rows.Count;
            sotable = (soLuongNPL - 1) / 6 + 1;
            BC03.report = this;
            for (int i = 0; i < this.sotable; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            if(cbPage.Items.Count > 0)
                cbPage.SelectedIndex = 0;

            btnNext.Enabled = !this.BC03.Last;
            btnPrivious.Enabled = !this.BC03.First;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BC03.First = (cbPage.SelectedIndex == 0);
            this.BC03.Last = (cbPage.SelectedIndex == (sotable - 1));
            this.BC03.HD = this.HD;

            btnNext.Enabled = !this.BC03.Last;
            btnPrivious.Enabled = !this.BC03.First;

            if (dsNguyenPhuLieu.Tables["dtBangNguyenPhuLieu" + cbPage.SelectedIndex] != null)
                this.BC03.BindReport(this.dsNguyenPhuLieu.Tables["dtBangNguyenPhuLieu" + cbPage.SelectedIndex]);
            else
            {
                dtBangNguyenPhuLieu = new DataTable("dtBangNguyenPhuLieu" + cbPage.SelectedIndex);
                dtBangNguyenPhuLieu = CreatSchemaDataSetNewRP03(cbPage.SelectedIndex);
                this.BC03.BindReport(this.dtBangNguyenPhuLieu);
                dsNguyenPhuLieu.Tables.Add(dtBangNguyenPhuLieu);
                
            }

            printControl1.PrintingSystem = this.BC03.PrintingSystem;
            this.BC03.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }


        public DataTable CreatSchemaDataSetNewRP03(int BangSo)
        {
            #region Tạo cấu trúc table
            DataTable dttemp = new DataTable("dtBangNguyenPhuLieu" + BangSo.ToString());
            DataColumn[] dcCol = new DataColumn[8];

            dcCol[0] = new DataColumn("TKXK", typeof(string));
            dcCol[0].Caption = "TKXK";

            int t = 1;
            for (int k = BangSo * 6; k < (BangSo + 1) * 6; k++)
            {

                if (k < soLuongNPL)
                {
                    dcCol[t] = new DataColumn();
                    string Ma = dsNPL.Tables[0].Rows[k].ItemArray[1].ToString();
                    dcCol[t].ColumnName = "Mã " + Ma;
                    dcCol[t].DataType = typeof(decimal);

                    /* DataSet tạm để lấy tên và đơn vị tính */
                    dsNPLTemp = NPL.GetNPL(Ma, this.HD.ID);

                    string Ten = dsNPLTemp.Tables[0].Rows[0].ItemArray[0].ToString();
                    string DVT_ID = dsNPLTemp.Tables[0].Rows[0].ItemArray[1].ToString();
                    dcCol[t].Caption = " Tên : " + Ten + "\r\n Mã : " + Ma + "\r\n ĐV tính : " + DonViTinh_GetName(DVT_ID);
                    t++;
                }
                else
                {
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "";
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = "";
                    t++;
                }
            }

            dcCol[t] = new DataColumn();
            dcCol[t].ColumnName = "GhiChu";
            dcCol[t].DataType = typeof(string);
            dcCol[t].Caption = "GhiChu";
            dttemp.Columns.AddRange(dcCol);
            #endregion Tạo cấu trúc table


            ToKhaiMauDichCollection TKMDCol = this.HD.GetTKNKBC03New();

            foreach (ToKhaiMauDich TKMD in TKMDCol)
            {
                TKMD.LoadHMDCollection();
                DataRow drData = dttemp.NewRow();
                foreach (HangMauDich HMD in TKMD.HMDCollection)
                {
                    drData[0] = TKMD.SoToKhai + "/" + LoaiHinhMauDich_GetTenVT(TKMD.MaLoaiHinh) + "/" + TKMD.MaHaiQuan + "\r\n" + TKMD.NgayDangKy.ToShortDateString();
                    for (int n = 1; n <= 6; n++)
                    {
                        string ColName = "Mã " + HMD.MaPhu;
                        if (dttemp.Columns[n].ColumnName == ColName)
                            drData[n] = HMD.SoLuong;
                    }
                }
                for (int n = 1; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }

            }

            ToKhaiChuyenTiepCollection TKCTCol = this.HD.GetTKCTXuatNPL();

            foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
            {
                TKCT.LoadHCTCollection();
                DataRow drData = dttemp.NewRow();
                foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                {
                    drData[0] = TKCT.SoToKhai + "/" + LoaiHinhMauDich_GetTenVT(TKCT.MaLoaiHinh) + "/" + TKCT.MaHaiQuanTiepNhan + "\r\n" + TKCT.NgayDangKy.ToShortDateString();
                    for (int n = 1; n <= 6; n++)
                    {
                        string ColName = "Mã " + HCT.MaHang;
                        if (dttemp.Columns[n].ColumnName == ColName)
                            drData[n] = HCT.SoLuong;
                    }
                }
                for (int n = 1; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }

            }
            return dttemp;
        }

        private void btnPrivious_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex--;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex++;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BC03.setText(this.label, txtName.Text);
            this.BC03.CreateDocument();
        }
    }
}