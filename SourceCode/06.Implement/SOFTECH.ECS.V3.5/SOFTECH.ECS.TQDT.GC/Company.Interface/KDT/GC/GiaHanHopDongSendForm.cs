﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using Company.Interface.KDT.SXXK;
using Company.Interface.GC;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Send;
namespace Company.Interface.KDT.GC
{
    public partial class GiaHanHopDongSendForm : BaseForm
    {

        public HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public GiaHanThanhKhoan giaHanThanhKhoan = new GiaHanThanhKhoan() { TrangThaiXuLy = -1 };

        public GiaHanHopDongSendForm()
        {
            InitializeComponent();


        }

        private void Set(GiaHanThanhKhoan ghThanhKhoan)
        {
            txtSoNgay.Value = ghThanhKhoan.SoNgayGiaHan;
            txtLyDo.Text = ghThanhKhoan.LyDo;
            txtGhiChu.Text = ghThanhKhoan.GhiChu;
        }
        private void Get(GiaHanThanhKhoan ghThanhKhoan)
        {
            ghThanhKhoan.SoNgayGiaHan = Convert.ToInt32(txtSoNgay.Value);
            ghThanhKhoan.LyDo = txtLyDo.Text;
            ghThanhKhoan.GhiChu = txtGhiChu.Text;
            ghThanhKhoan.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            ghThanhKhoan.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            ghThanhKhoan.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
            ghThanhKhoan.HopDong_ID = HD.ID;
            if (ghThanhKhoan.NgayTiepNhan.Year < 1900) ghThanhKhoan.NgayTiepNhan = new DateTime(1900, 1, 1);
            if (ghThanhKhoan.NgayDangKy.Year < 1900) ghThanhKhoan.NgayDangKy = new DateTime(1900, 1, 1);

        }
        private void GiaHanHopDongSendForm_Load(object sender, EventArgs e)
        {
            txtSoNgay.Focus();
            if (giaHanThanhKhoan == null)
                giaHanThanhKhoan = new GiaHanThanhKhoan();
            Set(giaHanThanhKhoan);
            if (giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GiaHanThanhKhoan;
                sendXML.master_id = giaHanThanhKhoan.ID;
                if (sendXML.Load())
                {
                    cmdSend.Enabled = cmdSend1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdDelete.Enabled = cmdDelete1.Enabled =
                        HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    lblTrangThai.Text = "Đã khai báo nhưng chưa nhận được phản hồi";
                }
                else
                    setCommandStatus();
            }
            else
            {
                if (OpenType == OpenFormType.Insert)
                {
                    giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
                setCommandStatus();
            }
        }
        public void setCommandStatus()
        {
            txtSoHopDong.Text = HD.SoHopDong;
            txtSoTiepNhan.Text = giaHanThanhKhoan.SoTiepNhan != 0 ? giaHanThanhKhoan.SoTiepNhan.ToString() : string.Empty;
            ccNgayHD.Value = HD.NgayKy;
            ccNgayHetHanHD.Value = HD.NgayHetHan;
            if (giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {

                cmdChonHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdDelete.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
            }
            else if (giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdDelete.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChonHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else
            {
                lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChonHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdDelete.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                
            }
        }

        private void Xoa()
        {
            try
            {
                if (ShowMessage("Bạn có thật sự muốn xóa không?", true) != "Yes") return;
                giaHanThanhKhoan.Delete();
                giaHanThanhKhoan = new GiaHanThanhKhoan();
                Set(giaHanThanhKhoan);
                ShowMessage("Xóa thành công", false);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(HD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdDelete":
                    Xoa();
                    break;
                case "cmdSave":
                    this.save();
                    break;
                case "cmdSend":
                    this.SendV3();
                    break;
                case "HuyKhaiBao":
                    this.CanceldV3();
                    break;
                case "NhanDuLieu":
                    this.FeedBackV3();
                    break;

            }
        }
        private bool CheckValid()
        {
            bool isValid = true;
            int songayGH = int.Parse(txtSoNgay.Text);
            if (songayGH < 1 || songayGH > 30)
            {
                error.SetError(txtSoNgay, "Số ngày gia hạn không hợp lệ");
                txtSoNgay.Focus();
                isValid = false;
            }
            return isValid;
        }
        private void save()
        {
            error.Clear();
            if (!CheckValid()) return;
            Get(giaHanThanhKhoan);
            try
            {
                if (string.IsNullOrEmpty(giaHanThanhKhoan.GUIDSTR))
                    giaHanThanhKhoan.GUIDSTR = Guid.NewGuid().ToString();

                if (giaHanThanhKhoan.ID == 0)
                    giaHanThanhKhoan.Insert();
                else giaHanThanhKhoan.Update();
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(HD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {

            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = giaHanThanhKhoan.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_GIA_HAN_THANH_KHOAN;
            form.ShowDialog(this);
        }

        #region V3
        private void SendV3()
        {
            if (giaHanThanhKhoan.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.GiaHanThanhKhoan;
            sendXML.master_id = giaHanThanhKhoan.ID;

            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            try
            {
                giaHanThanhKhoan.GUIDSTR = Guid.NewGuid().ToString();
                GC_GiaHanThanhKhoan ghThanhKhoan = Mapper.ToDataTransferGiaHanThanhKhoan(giaHanThanhKhoan, HD);
                ObjectSend msgSend = new ObjectSend(
                               new NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = giaHanThanhKhoan.MaDoanhNghiep
                               }
                                 , new NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(giaHanThanhKhoan.MaHaiQuan),
                                     Identity = giaHanThanhKhoan.MaHaiQuan
                                 }
                              ,
                                new SubjectBase()
                                {
                                    Type = ghThanhKhoan.Issuer,
                                    Function = ghThanhKhoan.Function,
                                    Reference = giaHanThanhKhoan.GUIDSTR,
                                }
                                ,
                                ghThanhKhoan);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.KhaiBaoGiaHanTK);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = cmdSend1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdDelete.Enabled = cmdDelete1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    sendXML.LoaiHS = LoaiKhaiBao.GiaHanThanhKhoan;
                    sendXML.master_id = giaHanThanhKhoan.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    giaHanThanhKhoan.Update();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                   // sendForm.Message.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.TuChoiGiaHanTK);
                    ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(ex.Message, giaHanThanhKhoan.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.GiaHanThanhKhoan(giaHanThanhKhoan, ref msgInfor, e);

        }
        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                Reference = giaHanThanhKhoan.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,

            };
            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = giaHanThanhKhoan.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(giaHanThanhKhoan.MaHaiQuan.Trim()),
                                              Identity = giaHanThanhKhoan.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else if (giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                            this.setCommandStatus();
                        }
                        count--;
                    }
                    else if (giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || giaHanThanhKhoan.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                        this.setCommandStatus();
                    }
                    else isFeedBack = false;
                }
            }
        }
        private void CanceldV3()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.GiaHanThanhKhoan;
            sendXML.master_id = giaHanThanhKhoan.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            DeclarationBase giahanTK = Mapper.HuyKhaiBao(DeclarationIssuer.GC_GIA_HAN_THANH_KHOAN, giaHanThanhKhoan.GUIDSTR, giaHanThanhKhoan.SoTiepNhan, giaHanThanhKhoan.MaHaiQuan, giaHanThanhKhoan.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = giaHanThanhKhoan.MaDoanhNghiep
                           }
                             , new NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(giaHanThanhKhoan.MaHaiQuan),
                                 Identity = giaHanThanhKhoan.MaHaiQuan
                             }
                          ,
                            new SubjectBase()
                            {
                                Type = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                                Function = DeclarationFunction.HUY,
                                Reference = giaHanThanhKhoan.GUIDSTR,
                            }
                            ,
                            giahanTK);
            SendMessageForm sendForm = new SendMessageForm();
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
            {
                giaHanThanhKhoan.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                sendForm.Message.XmlSaveMessage(giaHanThanhKhoan.ID, MessageTitle.HQHuyKhaiBaoToKhai);
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                giaHanThanhKhoan.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }

        #endregion

    }
}