-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoThueKhaiBao numeric(11, 0),
	@TongSoThueAnDinh numeric(11, 0),
	@TongSoThueChenhLech numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
(
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[SoToKhai],
	[NgayDangKyToKhai],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoThueKhaiBao],
	[TongSoThueAnDinh],
	[TongSoThueChenhLech],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
)
VALUES 
(
	@TenCoQuanHaiQuan,
	@TenChiCucHaiQuanNoiMoToKhai,
	@SoChungTu,
	@SoToKhai,
	@NgayDangKyToKhai,
	@TenDonViXuatNhapKhau,
	@MaDonViXuatNhapKhau,
	@MaBuuChinh,
	@DiaChiNguoiXuatNhapKhau,
	@SoDienThoaiNguoiXuatNhapKhau,
	@TenNganHangBaoLanh,
	@MaNganHangBaoLanh,
	@KiHieuChungTuBaoLanh,
	@SoChungTuBaoLanh,
	@LoaiBaoLanh,
	@TenNganHangTraThay,
	@MaNganHangTraThay,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoHieuPhatHanhHanMuc,
	@TongSoThueKhaiBao,
	@TongSoThueAnDinh,
	@TongSoThueChenhLech,
	@MaTienTe,
	@TyGiaQuyDoi,
	@SoNgayDuocAnHan,
	@NgayHetHieuLucTamNhapTaiXuat,
	@SoTaiKhoanKhoBac,
	@TenKhoBac,
	@LaiSuatPhatChamNop,
	@NgayPhatHanhChungTu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoThueKhaiBao numeric(11, 0),
	@TongSoThueAnDinh numeric(11, 0),
	@TongSoThueChenhLech numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
SET
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[TenChiCucHaiQuanNoiMoToKhai] = @TenChiCucHaiQuanNoiMoToKhai,
	[SoChungTu] = @SoChungTu,
	[SoToKhai] = @SoToKhai,
	[NgayDangKyToKhai] = @NgayDangKyToKhai,
	[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
	[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
	[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
	[TenNganHangBaoLanh] = @TenNganHangBaoLanh,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[KiHieuChungTuBaoLanh] = @KiHieuChungTuBaoLanh,
	[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
	[LoaiBaoLanh] = @LoaiBaoLanh,
	[TenNganHangTraThay] = @TenNganHangTraThay,
	[MaNganHangTraThay] = @MaNganHangTraThay,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
	[TongSoThueKhaiBao] = @TongSoThueKhaiBao,
	[TongSoThueAnDinh] = @TongSoThueAnDinh,
	[TongSoThueChenhLech] = @TongSoThueChenhLech,
	[MaTienTe] = @MaTienTe,
	[TyGiaQuyDoi] = @TyGiaQuyDoi,
	[SoNgayDuocAnHan] = @SoNgayDuocAnHan,
	[NgayHetHieuLucTamNhapTaiXuat] = @NgayHetHieuLucTamNhapTaiXuat,
	[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
	[TenKhoBac] = @TenKhoBac,
	[LaiSuatPhatChamNop] = @LaiSuatPhatChamNop,
	[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoThueKhaiBao numeric(11, 0),
	@TongSoThueAnDinh numeric(11, 0),
	@TongSoThueChenhLech numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] 
		SET
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[TenChiCucHaiQuanNoiMoToKhai] = @TenChiCucHaiQuanNoiMoToKhai,
			[SoChungTu] = @SoChungTu,
			[SoToKhai] = @SoToKhai,
			[NgayDangKyToKhai] = @NgayDangKyToKhai,
			[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
			[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
			[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
			[TenNganHangBaoLanh] = @TenNganHangBaoLanh,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[KiHieuChungTuBaoLanh] = @KiHieuChungTuBaoLanh,
			[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
			[LoaiBaoLanh] = @LoaiBaoLanh,
			[TenNganHangTraThay] = @TenNganHangTraThay,
			[MaNganHangTraThay] = @MaNganHangTraThay,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
			[TongSoThueKhaiBao] = @TongSoThueKhaiBao,
			[TongSoThueAnDinh] = @TongSoThueAnDinh,
			[TongSoThueChenhLech] = @TongSoThueChenhLech,
			[MaTienTe] = @MaTienTe,
			[TyGiaQuyDoi] = @TyGiaQuyDoi,
			[SoNgayDuocAnHan] = @SoNgayDuocAnHan,
			[NgayHetHieuLucTamNhapTaiXuat] = @NgayHetHieuLucTamNhapTaiXuat,
			[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
			[TenKhoBac] = @TenKhoBac,
			[LaiSuatPhatChamNop] = @LaiSuatPhatChamNop,
			[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
		(
			[TenCoQuanHaiQuan],
			[TenChiCucHaiQuanNoiMoToKhai],
			[SoChungTu],
			[SoToKhai],
			[NgayDangKyToKhai],
			[TenDonViXuatNhapKhau],
			[MaDonViXuatNhapKhau],
			[MaBuuChinh],
			[DiaChiNguoiXuatNhapKhau],
			[SoDienThoaiNguoiXuatNhapKhau],
			[TenNganHangBaoLanh],
			[MaNganHangBaoLanh],
			[KiHieuChungTuBaoLanh],
			[SoChungTuBaoLanh],
			[LoaiBaoLanh],
			[TenNganHangTraThay],
			[MaNganHangTraThay],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoHieuPhatHanhHanMuc],
			[TongSoThueKhaiBao],
			[TongSoThueAnDinh],
			[TongSoThueChenhLech],
			[MaTienTe],
			[TyGiaQuyDoi],
			[SoNgayDuocAnHan],
			[NgayHetHieuLucTamNhapTaiXuat],
			[SoTaiKhoanKhoBac],
			[TenKhoBac],
			[LaiSuatPhatChamNop],
			[NgayPhatHanhChungTu]
		)
		VALUES 
		(
			@TenCoQuanHaiQuan,
			@TenChiCucHaiQuanNoiMoToKhai,
			@SoChungTu,
			@SoToKhai,
			@NgayDangKyToKhai,
			@TenDonViXuatNhapKhau,
			@MaDonViXuatNhapKhau,
			@MaBuuChinh,
			@DiaChiNguoiXuatNhapKhau,
			@SoDienThoaiNguoiXuatNhapKhau,
			@TenNganHangBaoLanh,
			@MaNganHangBaoLanh,
			@KiHieuChungTuBaoLanh,
			@SoChungTuBaoLanh,
			@LoaiBaoLanh,
			@TenNganHangTraThay,
			@MaNganHangTraThay,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoHieuPhatHanhHanMuc,
			@TongSoThueKhaiBao,
			@TongSoThueAnDinh,
			@TongSoThueChenhLech,
			@MaTienTe,
			@TyGiaQuyDoi,
			@SoNgayDuocAnHan,
			@NgayHetHieuLucTamNhapTaiXuat,
			@SoTaiKhoanKhoBac,
			@TenKhoBac,
			@LaiSuatPhatChamNop,
			@NgayPhatHanhChungTu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[SoToKhai],
	[NgayDangKyToKhai],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoThueKhaiBao],
	[TongSoThueAnDinh],
	[TongSoThueChenhLech],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[SoToKhai],
	[NgayDangKyToKhai],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoThueKhaiBao],
	[TongSoThueAnDinh],
	[TongSoThueChenhLech],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[SoToKhai],
	[NgayDangKyToKhai],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoThueKhaiBao],
	[TongSoThueAnDinh],
	[TongSoThueChenhLech],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]	

GO

