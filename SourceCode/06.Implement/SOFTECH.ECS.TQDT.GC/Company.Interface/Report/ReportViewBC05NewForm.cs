﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC05NewForm : BaseForm
    {
        public DataSet ds = new DataSet();
        private BangKe05New BC05;
        public HopDong HD = new HopDong();
        public XRLabel Label;
        public ReportViewBC05NewForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            BC05 = new BangKe05New();
            Label = new XRLabel();
            this.BC05.report = this;
            for (int i = 0; i < this.ds.Tables.Count; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            if(this.ds.Tables.Count > 0)
                cbPage.SelectedIndex = 0;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BC05.First = (cbPage.SelectedIndex == 0);
            this.BC05.Last = (cbPage.SelectedIndex == (ds.Tables.Count - 1));
            this.BC05.HD = this.HD;
            this.BC05.BindReport(this.ds.Tables[cbPage.SelectedIndex]);
            printControl1.PrintingSystem = this.BC05.PrintingSystem;
            this.BC05.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                showMsg("MSG_PRI03");
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                showMsg("MSG_PRI03");
            }  
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BC05.setText(this.Label, txtName.Text);
            this.BC05.CreateDocument();
        }

   
    }
}