﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.KD
{
    class NodeHoaDonThuongMai
    {
        public const string Declaration = "Declaration";
        
        /// <summary>
        /// Loại chứng từ (=380)  1	 an..3 Danh mục chuẩn
        /// </summary>
        public const string issuer = "issuer";
        
        /// <summary>
        /// Số tham chiếu chứng từ
        /// </summary>
        public const string reference = "reference";
        
        /// <summary>
        /// Ngày khai chứng từ 1 an19 YYYY-MM-DD HH:mm:ss || Số TK ||Số hoá đơn thương mại
        /// </summary>
        public const string issue = "issue";

        /// <summary>
        /// Chức năng của chứng từ(=8) 1 n..2 Danh mục chuẩn || Ngày TK || Ngày phát hành hoá đơn thương mại
        /// </summary>
        public const string function = "function";
        
        /// <summary>
        /// Nơi khai báo	0	an..60
        /// </summary>
        public const string issueLocation = "issueLocation";
        
        /// <summary>
        /// Trạng thái của chứng từ	1	an..3	Danh mục chuẩn
        /// </summary>
        public const string status = "status";
        
        /// <summary>
        /// Số đăng ký chứng từ  || Trạng thái đại lý	
        /// </summary>
        public const string customsReference = "customsReference";
        
        /// <summary>
        /// Ngày đăng ký chứng từ  0  an 19	YYYY-MM-DD HH:mm:ss
        /// </summary>
        public const string acceptance = "acceptance";
        
        /// <summary>
        /// Hải quan tiếp nhận chứng từ  1 an..6 Danh mục chuẩn
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        
        /// <summary>
        /// Người khai hải quan || Mã hải quan 
        /// </summary>
        public const string Agent = "Agent";
        
        /// <summary>
        /// Tên người khai hải quan || Tên người bán hàng || Tên người mua hàng
        /// </summary>
        public const string name = "name";
        
        /// <summary>
        /// Mã người khai hải quan || Mã người bán hàng || Mã người mua hàng 
        /// </summary>
        public const string identity = "identity";
        
        /// <summary>
        /// Mã LH	1   an..10	Danh mục chuẩn
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";
        /// <summary>
        /// Phương thức thanh toán	1	a..10	Danh mục chuẩn
        /// </summary>
        public const string method = "method";
        
        /// <summary>
        /// Đồng tiền thanh toán	1	a3	Danh mục chuẩn
        /// </summary>
         public const string currencyType = "currencyType";		
	    
        /// <summary>
         /// Điều kiện giao hàng	1	an..7	Danh mục chuẩn
	    /// </summary>
		 public const string condition = "condition";	
        
        /// <summary>
         /// Số thứ tự hàng	1	n..5
        /// </summary>
		 public const string sequence = "sequence";
		/// <summary>
         /// Đơn giá	1	n..16,2
		/// </summary>
		public const string unitPrice = "unitPrice";
	    /// <summary>
        /// Trị giá tính thuế	1	n..16
	    /// </summary>
		public const string statisticalValue = "statisticalValue";
		
        /// <summary>
        ///Xuất xứ hàng hoá	1	a2	Danh mục chuẩn
		/// </summary>
		public const string originCountry = "originCountry";
		
        /// <summary>
        /// Mô tả hàng hóa	1	an..256
		/// </summary>
		public const string description = "description";
		/// <summary>
        /// Mã hàng do doanh nghiệp khai	1	an..30
		/// </summary>
		public const string identification = "identification";

        /// <summary>
        /// Chi tiết hàng
        /// </summary>
        public const string Commodity = "Commodity";						
        
        /// <summary>
        /// Thông tin hàng trên hóa đơn
        /// </summary>
        public const string CommercialInvoiceItem = "CommercialInvoiceItem";

        public const string CommercialInvoice = "CommercialInvoice";
        public const string CommercialInvoices = "CommercialInvoices";
        /// <summary>
        /// Chứng từ liên quan
        /// </summary>
        public const string AdditionalDocument = "AdditionalDocument";
        public const string Buyer = "Buyer";
        public const string Seller = "Seller";		
        public const string CurrencyExchange = "CurrencyExchange";
        public const string Payment = "Payment";
        public const string TradeTerm = "TradeTerm";

        /// <summary>
        ///Thông tin hàng hóa trên hóa đơn thương mại	 chuyển từ ngoài vào trong
        /// </summary>
        public const string Origin = "Origin";						

        public const string GoodsMeasure = "GoodsMeasure";
        
        /// <summary>
        /// Số lượng	1	n..14,3
		/// </summary>
		public const string quantity = "quantity";	
        
        /// <summary>
        /// Mã đơn vị tính	1	an..3	Danh mục chuẩn
        /// </summary>
        public const string measureUnit = "measureUnit";					
		
        /// <summary>
        /// Các khoản điều chỉnh
        /// </summary>
		public const string ValuationAdjustment = "ValuationAdjustment";

        /// <summary>
        /// Các khoản cộng vào trị giá	1	n..16,2	Không có thì ghi giá trị 0
        /// </summary>
        public const string addition = "addition";	

        /// <summary>
        /// Các khoản giảm giá	1	n..16,2	Không có thì ghi giá trị 0
		/// </summary>
		public const string deduction = "deduction";

        public const string AdditionalInformation = "AdditionalInformation";
        
        /// <summary>
        /// Ghi chú khác	0	an..2000
		/// </summary>
	    public const string content = "content";				
    }
}
