using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
namespace Company.Interface.KDT.SXXK
{
    public partial class BoSungSanPhamReadExcelForm : BaseForm
    {
        public PhuKienDangKy PKDangKy;
        public Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        public BoSungSanPhamReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private bool checkSPExit(string maSP)
        {
            foreach (HangPhuKien sp in LoaiPK.HPKCollection)
            {
                if (sp.MaHang.Trim().ToUpper() == maSP.ToUpper().Trim())
                    return true;
            }
            return false;
        }
    
        private bool EmptyCheck(string st)
        {
            if (st == "") return true;
            return false;
        }
        private bool CheckNumber(decimal so)
        {
            if (so < 0) return true;
            return false;
        }
        private bool CheckNameDVT(string varcheck)
        {
            DataTable dt = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Trim().ToUpper().Equals(drcheck["Ten"].ToString().Trim().ToUpper()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if(!cvError.IsValid)return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
           
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                showMsg("MSG_0203008");
                //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                return;
            }
            try
            {
               ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn ;
            int maHangCol = 0;
            char tenHangColumn ;
            int tenHangCol = 0;
            char maHSColumn ;
            int maHSCol = 0;
            char dvtColumn;
            int dvtCol = 0;
            char SoLuongColumn ;
            int SoLuongCol = 0;
            char LoaiSPColumn ;
            int LoaiSPCol = 0;
            try{
                 maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
                 maHangCol = ConvertCharToInt(maHangColumn);
                 tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
                 tenHangCol = ConvertCharToInt(tenHangColumn);
                 maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
                 maHSCol = ConvertCharToInt(maHSColumn);
                 dvtColumn = Convert.ToChar(txtDVTColumn.Text);
                 dvtCol = ConvertCharToInt(dvtColumn);
                 SoLuongColumn = Convert.ToChar(txtSoLuong.Text);
                 SoLuongCol = ConvertCharToInt(SoLuongColumn);
                 LoaiSPColumn = Convert.ToChar(txtLoaiSPGC.Text);
                 LoaiSPCol = ConvertCharToInt(LoaiSPColumn);
            }
            catch { return; }
            int j =0;
            foreach (WorksheetRow wsr in wsrc)
            {                   
                if (wsr.Index >= beginRow)
                {
                    j = wsr.Index + 1;
                    try
                    {
                        HangPhuKien sp = new HangPhuKien();
                        //if (CheckNumber(Convert.ToDecimal(wsr.Cells[SoLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHangCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[SoLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHSCol].Value)) || !CheckNameDVT(Convert.ToString(wsr.Cells[dvtCol].Value)))
                        //{
                        //    ShowMessage(" Dữ liệu tài dòng " + (wsr.Index + 1) + " sai, được bỏ qua", false);
                        //    continue;
                        //}
                        sp.MaHang = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        Company.GC.BLL.GC.SanPham spKiemTra = new Company.GC.BLL.GC.SanPham();
                        spKiemTra.HopDong_ID = PKDangKy.HopDong_ID;
                        spKiemTra.Ma = sp.MaHang;
                        
                        if (spKiemTra.Load())
                        {
                            showMsg("MSG_0203009", sp.MaHang);
                            //MLMessages("Sản phẩm : "+sp.MaHang +" đã được khai báo trong hợp đồng này nên sẽ được bỏ qua.","MSG_WRN06",sp.MaHang, false);
                            continue;
                        }
                        sp.TenHang = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                        sp.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                        try
                        {
                            sp.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[dvtCol].Value));
                        }
                        catch
                        {
                            showMsg("MSG_0203010", Convert.ToString(wsr.Cells[dvtCol].Value));
                            //MLMessages("Đơn vị tính : " + Convert.ToString(wsr.Cells[dvtCol].Value) + " không có trong hệ thống.","MSG_WRN10",Convert.ToString(wsr.Cells[dvtCol].Value), false);
                            break;
                        }
                        sp.SoLuong = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value);
                        sp.NhomSP = Convert.ToString(wsr.Cells[LoaiSPCol].Value).Trim();
                        try
                        {
                            Company.GC.BLL.GC.NhomSanPham nhomsp = new Company.GC.BLL.GC.NhomSanPham();
                            nhomsp.MaSanPham=(sp.NhomSP);
                            nhomsp.HopDong_ID = PKDangKy.HopDong_ID;

                            if (!nhomsp.Load())
                            {
                                showMsg("MSG_0203011", sp.NhomSP);
                                //MLMessages("Loại sản phẩm gia công : " + sp.NhomSP + " chưa được khai báo trong hợp đồng.","MSG_WRN10",sp.NhomSP, false);
                                break;
                            }
                        }
                        catch
                        {
                            if (showMsg("MSG_0203012", true) == "Yes")
                            {
                                break;
                            }
                            else
                                continue;

                        }
                        if (checkSPExit(sp.MaHang))
                        {
                            int dong = wsr.Index + 1;
                            showMsg("MSG_0203013", new String[] { sp.MaHang, dong.ToString()});
                            //MLMessages("Sản phẩm có mã \"" + sp.MaHang + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới nên sẽ được bỏ qua.","MSG_EXC12",j.ToString(), false);
                        }
                        else this.LoaiPK.HPKCollection.Add(sp);
                    }
                    catch
                    {
                        if (showMsg("MSG_0203014", wsr.Index + 1, true) != "Yes")
                        //if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?","MSG_EXC06", j.ToString(), true) != "Yes")
                        {
                            break;
                        }
                    }
                }
            }
            if (LoaiPK.HPKCollection.Count > 0)
            {
                try
                {
                    LoaiPK.Master_ID = PKDangKy.ID;
                    LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                    LoaiPK.InsertUpdateBoSungSP(PKDangKy.HopDong_ID);
                }
                catch(Exception ex)
                {
                    showMsg("MSG_SAV022", ex.Message);
                    //MLMessages("Có lỗi khi lưu thông tin : "+ex.Message,"MSG_SAV01",ex.Message, false);
                }
            }
            this.Close();
        }

        private void SanPhamReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
            //foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in PKDangKy.PKCollection)
            //{
            //    if (pk.MaPhuKien.Trim() == "S13")
            //    {
            //        LoaiPK = pk;
            //        break;
            //    }
            //}
            LoaiPK.MaPhuKien = "S13";
        }
    }
}