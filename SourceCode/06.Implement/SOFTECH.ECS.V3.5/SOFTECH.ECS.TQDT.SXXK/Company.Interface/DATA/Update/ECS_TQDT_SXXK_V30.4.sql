   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_SXXK_ThanhLy_NPLNhapTon_SelectDynamic]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, August 28, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
Alter PROCEDURE [dbo].[p_SXXK_ThanhLy_NPLNhapTon_SelectDynamic]  
 @WhereCondition nvarchar(500),  
 @OrderByExpression nvarchar(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(3250)  
  
SET @SQL = 'SELECT  [SoToKhai],  
 CASE WHEN MaLoaiHinh LIKE ''%V%'' THEN(SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhai) END AS SoTKVNACCS,  
 [SoToKhai],  
 [MaLoaiHinh],  
 [NamDangKy],  
 [MaHaiQuan],  
 [MaNPL],  
 [MaDoanhNghiep],  
 [Luong],  
 [Ton],  
 [ThueXNK],  
 [ThueTTDB],  
 [ThueVAT],  
 [PhuThu],  
 [ThueCLGia],  
 [ThueXNKTon],  
 Case when MaLoaiHinh like ''%V%'' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai =(select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_SXXK_ThanhLy_NPLNhapTon.SoToKhai) ) else (select top 1 SoHopDong from t_SXXK_ToKhaiMauDich where SoToKhai = t_SXXK_ThanhLy_NPLNhapTon.SoToKhai)  end AS SoHopDong  
 FROM [dbo].[t_SXXK_ThanhLy_NPLNhapTon] WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
  GO
  Go
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.4',GETDATE(), N' Cập nhật các store [p_SXXK_ThanhLy_NPLNhapTon_SelectDynamic]')
END	
