﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Category
{
    public partial class VNACC_Category_HSCode : BaseForm
    {
        public Company.KDT.SHARE.VNACCS.VNACC_Category_HSCode hSCode;
        public string where = "";
        public VNACC_Category_HSCode()
        {
            InitializeComponent();
        }

        private void VNACC_Category_HSCode_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = Company.KDT.SHARE.VNACCS.VNACC_Category_HSCode.SelectCollectionDynamic(GetSearchWhere(), null);
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ClearText()
        {
            //txtCode.Text = String.Empty;
            //txtName.Text = String.Empty;
            //txtName_EN.Text = String.Empty;
            txtNotes.Text = String.Empty;
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtCode, errorProvider, "Mã HS  ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNotes, errorProvider, "Ghi chú ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                hSCode.HSCode = txtCode.Text.Trim().ToUpper();
                hSCode.Notes = txtNotes.Text.Trim().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void Set()
        {
            try
            {
                txtCode.Text = hSCode.HSCode;
                txtNotes.Text = hSCode.Notes;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtCode.Text))
                    where += " AND HSCode = '" + txtCode.Text.Trim() + "'";
                if (!String.IsNullOrEmpty(txtNotes.Text))
                    where += " AND Notes LIKE N'%" + txtNotes.Text.Trim() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return where;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                Get();
                hSCode.TableID = "A506A";
                hSCode.InsertUpdate();
                ShowMessage("Lưu thành công ", false);
                hSCode = new Company.KDT.SHARE.VNACCS.VNACC_Category_HSCode();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                if (ShowMessage("Bạn có muốn xóa dòng này không ? ", true) == "Yes")
                {
                    hSCode.Delete();
                }
                ShowMessage("Xóa thành công ", false);
                hSCode = new Company.KDT.SHARE.VNACCS.VNACC_Category_HSCode();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {

                if (e.Row.RowType == RowType.Record)
                {
                    hSCode = new Company.KDT.SHARE.VNACCS.VNACC_Category_HSCode();
                    hSCode = (Company.KDT.SHARE.VNACCS.VNACC_Category_HSCode)e.Row.DataRow;
                    Set();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            VNACC_UpdateHSCodeForm f = new VNACC_UpdateHSCodeForm();
            f.ShowDialog(this);
        }
    }
}
