using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Windows.Forms;
using System.IO;
namespace Company.GC.BLL
{
    public class EntityBase
    {

        protected SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

      
        public void SetDabaseMoi(string nameDatabase)
        {
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
        }
        public static string GetPathProgram()
        {
            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
            return fileInfo.DirectoryName;
        }
    }
}
//Dùng cho tơ khai trị giá từ KD
//namespace Company.KD.BLL.KDT
//{
//    public class EntityBase
//    {

//        protected SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();


//        public void SetDabaseMoi(string nameDatabase)
//        {
//            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
//        }
//        public static string GetPathProgram()
//        {
//            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
//            return fileInfo.DirectoryName;
//        }
//    }
//}