﻿using System.ComponentModel;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Company.Interface.Controls;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface
{
    partial class HangMauDichEditForm
    {
        private UIGroupBox uiGroupBox1;
        private UIButton btnGhi;
        private ToolTip toolTip1;
        private ErrorProvider epError;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HangMauDichEditForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblTyGiaTT = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtPhuThu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtTyLeThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTSXNKGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTienThue_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTS_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTriGiaKB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblNguyenTe_TGNT = new System.Windows.Forms.Label();
            this.lblNguyenTe_DGNT = new System.Windows.Forms.Label();
            this.txtDGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbDVT_HTS = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoLuong_HTS = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.lblDVT_HTS = new System.Windows.Forms.Label();
            this.txtMa_HTS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblSoLuong_HTS = new System.Windows.Forms.Label();
            this.lblMa_HTS = new System.Windows.Forms.Label();
            this.cbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrNuocXX = new Company.Interface.Controls.NuocHControl();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.cvDonGia = new Company.Controls.CustomValidation.CompareValidator();
            this.cvTSNK = new Company.Controls.CustomValidation.CompareValidator();
            this.cvTriGiaTT = new Company.Controls.CustomValidation.CompareValidator();
            this.cvSoLuongHTS = new Company.Controls.CustomValidation.CompareValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTSNK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTriGiaTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuongHTS)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(787, 314);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.lblTyGiaTT);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(787, 314);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.AutoSize = true;
            this.lblTyGiaTT.BackColor = System.Drawing.Color.Transparent;
            this.lblTyGiaTT.Location = new System.Drawing.Point(12, 285);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Size = new System.Drawing.Size(58, 13);
            this.lblTyGiaTT.TabIndex = 2;
            this.lblTyGiaTT.Text = "Tỷ giá TT: ";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtPhuThu);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.txtTyLeThuKhac);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Controls.Add(this.txtTSXNKGiam);
            this.uiGroupBox4.Controls.Add(this.label20);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.txtTienThue_NK);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.label17);
            this.uiGroupBox4.Controls.Add(this.txtTS_NK);
            this.uiGroupBox4.Controls.Add(this.txtTGTT_NK);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Controls.Add(this.label16);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaKB);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_TGNT);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_DGNT);
            this.uiGroupBox4.Controls.Add(this.txtDGNT);
            this.uiGroupBox4.Controls.Add(this.txtTGNT);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(13, 114);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(761, 160);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtPhuThu
            // 
            this.txtPhuThu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPhuThu.DecimalDigits = 0;
            this.txtPhuThu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuThu.Location = new System.Drawing.Point(481, 100);
            this.txtPhuThu.Name = "txtPhuThu";
            this.txtPhuThu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPhuThu.Size = new System.Drawing.Size(129, 21);
            this.txtPhuThu.TabIndex = 18;
            this.txtPhuThu.TabStop = false;
            this.txtPhuThu.Text = "0";
            this.txtPhuThu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhuThu.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPhuThu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtPhuThu.VisualStyleManager = this.vsmMain;
            this.txtPhuThu.Leave += new System.EventHandler(this.txtPhuThu_Leave);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(364, 103);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 13);
            this.label26.TabIndex = 17;
            this.label26.Text = "Tiền thu khác";
            // 
            // txtTyLeThuKhac
            // 
            this.txtTyLeThuKhac.DecimalDigits = 8;
            this.txtTyLeThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyLeThuKhac.Location = new System.Drawing.Point(481, 73);
            this.txtTyLeThuKhac.Name = "txtTyLeThuKhac";
            this.txtTyLeThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyLeThuKhac.Size = new System.Drawing.Size(129, 21);
            this.txtTyLeThuKhac.TabIndex = 16;
            this.txtTyLeThuKhac.TabStop = false;
            this.txtTyLeThuKhac.Text = "0.00000000";
            this.txtTyLeThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyLeThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            524288});
            this.txtTyLeThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyLeThuKhac.VisualStyleManager = this.vsmMain;
            this.txtTyLeThuKhac.Leave += new System.EventHandler(this.txtTyLeThuKhac_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(364, 76);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 13);
            this.label25.TabIndex = 15;
            this.label25.Text = "Tỷ lệ thu khác";
            // 
            // txtTSXNKGiam
            // 
            this.txtTSXNKGiam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTSXNKGiam.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTSXNKGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSXNKGiam.Location = new System.Drawing.Point(480, 18);
            this.txtTSXNKGiam.Name = "txtTSXNKGiam";
            this.txtTSXNKGiam.Size = new System.Drawing.Size(129, 21);
            this.txtTSXNKGiam.TabIndex = 12;
            this.txtTSXNKGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSXNKGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTSXNKGiam.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(363, 23);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "Thuế suất NK giảm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(257, 127);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "(%)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(616, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "(VNĐ)";
            // 
            // txtTienThue_NK
            // 
            this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_NK.DecimalDigits = 0;
            this.txtTienThue_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_NK.Location = new System.Drawing.Point(481, 45);
            this.txtTienThue_NK.Name = "txtTienThue_NK";
            this.txtTienThue_NK.ReadOnly = true;
            this.txtTienThue_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_NK.Size = new System.Drawing.Size(129, 21);
            this.txtTienThue_NK.TabIndex = 14;
            this.txtTienThue_NK.TabStop = false;
            this.txtTienThue_NK.Text = "0";
            this.txtTienThue_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTienThue_NK.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(257, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "(VNĐ)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(363, 50);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "Tiền thuế NK";
            // 
            // txtTS_NK
            // 
            this.txtTS_NK.DecimalDigits = 0;
            this.txtTS_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_NK.Location = new System.Drawing.Point(121, 122);
            this.txtTS_NK.MaxLength = 5;
            this.txtTS_NK.Name = "txtTS_NK";
            this.txtTS_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_NK.Size = new System.Drawing.Size(128, 21);
            this.txtTS_NK.TabIndex = 10;
            this.txtTS_NK.Text = "0";
            this.txtTS_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTS_NK.VisualStyleManager = this.vsmMain;
            this.txtTS_NK.Leave += new System.EventHandler(this.txtTS_NK_Leave);
            // 
            // txtTGTT_NK
            // 
            this.txtTGTT_NK.BackColor = System.Drawing.Color.White;
            this.txtTGTT_NK.DecimalDigits = 0;
            this.txtTGTT_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_NK.Location = new System.Drawing.Point(121, 95);
            this.txtTGTT_NK.Name = "txtTGTT_NK";
            this.txtTGTT_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_NK.Size = new System.Drawing.Size(129, 21);
            this.txtTGTT_NK.TabIndex = 8;
            this.txtTGTT_NK.TabStop = false;
            this.txtTGTT_NK.Text = "0";
            this.txtTGTT_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGTT_NK.VisualStyleManager = this.vsmMain;
            this.txtTGTT_NK.Leave += new System.EventHandler(this.txtTGTT_NK_Leave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(257, 73);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "(VNĐ)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(4, 127);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 13);
            this.label19.TabIndex = 9;
            this.label19.Text = "Thuế suất NK";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(4, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "Trị giá tính thuế NK";
            // 
            // txtTriGiaKB
            // 
            this.txtTriGiaKB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTriGiaKB.DecimalDigits = 0;
            this.txtTriGiaKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaKB.Location = new System.Drawing.Point(121, 68);
            this.txtTriGiaKB.Name = "txtTriGiaKB";
            this.txtTriGiaKB.ReadOnly = true;
            this.txtTriGiaKB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaKB.Size = new System.Drawing.Size(128, 21);
            this.txtTriGiaKB.TabIndex = 6;
            this.txtTriGiaKB.TabStop = false;
            this.txtTriGiaKB.Text = "0";
            this.txtTriGiaKB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTriGiaKB, "Trị giá khai báo");
            this.txtTriGiaKB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaKB.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(4, 73);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "Trị giá khai báo";
            // 
            // lblNguyenTe_TGNT
            // 
            this.lblNguyenTe_TGNT.AutoSize = true;
            this.lblNguyenTe_TGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_TGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_TGNT.Location = new System.Drawing.Point(257, 46);
            this.lblNguyenTe_TGNT.Name = "lblNguyenTe_TGNT";
            this.lblNguyenTe_TGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_TGNT.TabIndex = 5;
            this.lblNguyenTe_TGNT.Text = "Mã nguyên tệ";
            // 
            // lblNguyenTe_DGNT
            // 
            this.lblNguyenTe_DGNT.AutoSize = true;
            this.lblNguyenTe_DGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_DGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_DGNT.Location = new System.Drawing.Point(257, 19);
            this.lblNguyenTe_DGNT.Name = "lblNguyenTe_DGNT";
            this.lblNguyenTe_DGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_DGNT.TabIndex = 2;
            this.lblNguyenTe_DGNT.Text = "Mã nguyên tệ";
            // 
            // txtDGNT
            // 
            this.txtDGNT.DecimalDigits = 18;
            this.txtDGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDGNT.Location = new System.Drawing.Point(121, 15);
            this.txtDGNT.MaxLength = 25;
            this.txtDGNT.Name = "txtDGNT";
            this.txtDGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDGNT.Size = new System.Drawing.Size(128, 21);
            this.txtDGNT.TabIndex = 2;
            this.txtDGNT.Text = "0.000000000000000000";
            this.txtDGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtDGNT, "Đơn giá khai báo");
            this.txtDGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            1179648});
            this.txtDGNT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDGNT.VisualStyleManager = this.vsmMain;
            this.txtDGNT.Leave += new System.EventHandler(this.txtDGNT_Leave);
            // 
            // txtTGNT
            // 
            this.txtTGNT.BackColor = System.Drawing.Color.White;
            this.txtTGNT.DecimalDigits = 15;
            this.txtTGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGNT.FormatString = "G15";
            this.txtTGNT.Location = new System.Drawing.Point(121, 41);
            this.txtTGNT.Name = "txtTGNT";
            this.txtTGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGNT.Size = new System.Drawing.Size(128, 21);
            this.txtTGNT.TabIndex = 4;
            this.txtTGNT.Text = "0";
            this.txtTGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTGNT, "Trị giá khai báo");
            this.txtTGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGNT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGNT.VisualStyleManager = this.vsmMain;
            this.txtTGNT.Click += new System.EventHandler(this.txtTGNT_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(4, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Đơn giá nguyên tệ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(4, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Trị giá nguyên tệ";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbbDVT_HTS);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong_HTS);
            this.uiGroupBox2.Controls.Add(this.lblDVT_HTS);
            this.uiGroupBox2.Controls.Add(this.txtMa_HTS);
            this.uiGroupBox2.Controls.Add(this.lblSoLuong_HTS);
            this.uiGroupBox2.Controls.Add(this.lblMa_HTS);
            this.uiGroupBox2.Controls.Add(this.cbDonViTinh);
            this.uiGroupBox2.Controls.Add(this.txtMaHang);
            this.uiGroupBox2.Controls.Add(this.ctrNuocXX);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtLuong);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.txtMaHS);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(759, 105);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // cbbDVT_HTS
            // 
            this.cbbDVT_HTS.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbDVT_HTS.DisplayMember = "Ten";
            this.cbbDVT_HTS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbDVT_HTS.Location = new System.Drawing.Point(626, 15);
            this.cbbDVT_HTS.Name = "cbbDVT_HTS";
            this.cbbDVT_HTS.Size = new System.Drawing.Size(122, 21);
            this.cbbDVT_HTS.TabIndex = 11;
            this.cbbDVT_HTS.TabStop = false;
            this.cbbDVT_HTS.ValueMember = "ID";
            this.cbbDVT_HTS.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbbDVT_HTS.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuong_HTS
            // 
            this.txtSoLuong_HTS.DecimalDigits = 3;
            this.txtSoLuong_HTS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong_HTS.Location = new System.Drawing.Point(654, 70);
            this.txtSoLuong_HTS.MaxLength = 15;
            this.txtSoLuong_HTS.Name = "txtSoLuong_HTS";
            this.txtSoLuong_HTS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong_HTS.Size = new System.Drawing.Size(94, 21);
            this.txtSoLuong_HTS.TabIndex = 17;
            this.txtSoLuong_HTS.Text = "0.000";
            this.txtSoLuong_HTS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong_HTS.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoLuong_HTS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoLuong_HTS.VisualStyleManager = this.vsmMain;
            // 
            // lblDVT_HTS
            // 
            this.lblDVT_HTS.AutoSize = true;
            this.lblDVT_HTS.BackColor = System.Drawing.Color.Transparent;
            this.lblDVT_HTS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT_HTS.Location = new System.Drawing.Point(576, 20);
            this.lblDVT_HTS.Name = "lblDVT_HTS";
            this.lblDVT_HTS.Size = new System.Drawing.Size(49, 13);
            this.lblDVT_HTS.TabIndex = 10;
            this.lblDVT_HTS.Text = "ĐVT HTS";
            // 
            // txtMa_HTS
            // 
            this.txtMa_HTS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMa_HTS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMa_HTS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa_HTS.Location = new System.Drawing.Point(236, 69);
            this.txtMa_HTS.Name = "txtMa_HTS";
            this.txtMa_HTS.Size = new System.Drawing.Size(122, 21);
            this.txtMa_HTS.TabIndex = 7;
            this.txtMa_HTS.TabStop = false;
            this.txtMa_HTS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa_HTS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMa_HTS.VisualStyleManager = this.vsmMain;
            // 
            // lblSoLuong_HTS
            // 
            this.lblSoLuong_HTS.AutoSize = true;
            this.lblSoLuong_HTS.BackColor = System.Drawing.Color.Transparent;
            this.lblSoLuong_HTS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong_HTS.Location = new System.Drawing.Point(564, 75);
            this.lblSoLuong_HTS.Name = "lblSoLuong_HTS";
            this.lblSoLuong_HTS.Size = new System.Drawing.Size(71, 13);
            this.lblSoLuong_HTS.TabIndex = 16;
            this.lblSoLuong_HTS.Text = "Số lượng HTS";
            // 
            // lblMa_HTS
            // 
            this.lblMa_HTS.AutoSize = true;
            this.lblMa_HTS.BackColor = System.Drawing.Color.Transparent;
            this.lblMa_HTS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMa_HTS.Location = new System.Drawing.Point(186, 74);
            this.lblMa_HTS.Name = "lblMa_HTS";
            this.lblMa_HTS.Size = new System.Drawing.Size(43, 13);
            this.lblMa_HTS.TabIndex = 6;
            this.lblMa_HTS.Text = "Mã HTS";
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDonViTinh.DisplayMember = "Ten";
            this.cbDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDonViTinh.Location = new System.Drawing.Point(481, 15);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.Size = new System.Drawing.Size(90, 21);
            this.cbDonViTinh.TabIndex = 9;
            this.cbDonViTinh.TabStop = false;
            this.cbDonViTinh.ValueMember = "ID";
            this.cbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHang
            // 
            this.txtMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHang.Location = new System.Drawing.Point(91, 15);
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(267, 21);
            this.txtMaHang.TabIndex = 1;
            this.txtMaHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHang.VisualStyleManager = this.vsmMain;
            this.txtMaHang.ButtonClick += new System.EventHandler(this.txtMaHang_ButtonClick);
            this.txtMaHang.Leave += new System.EventHandler(this.txtMaHang_Leave);
            // 
            // ctrNuocXX
            // 
            this.ctrNuocXX.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXX.ErrorMessage = "\"Nước xuất xứ\" không được bỏ trống.";
            this.ctrNuocXX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXX.Location = new System.Drawing.Point(481, 42);
            this.ctrNuocXX.Ma = "";
            this.ctrNuocXX.Name = "ctrNuocXX";
            this.ctrNuocXX.ReadOnly = false;
            this.ctrNuocXX.Size = new System.Drawing.Size(281, 22);
            this.ctrNuocXX.TabIndex = 13;
            this.ctrNuocXX.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã HS";
            // 
            // txtLuong
            // 
            this.txtLuong.DecimalDigits = 3;
            this.txtLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuong.Location = new System.Drawing.Point(481, 70);
            this.txtLuong.MaxLength = 15;
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLuong.Size = new System.Drawing.Size(78, 21);
            this.txtLuong.TabIndex = 15;
            this.txtLuong.Text = "0.000";
            this.txtLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtLuong.VisualStyleManager = this.vsmMain;
            this.txtLuong.Leave += new System.EventHandler(this.txtLuong_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên hàng";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(4, 20);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã hàng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(364, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 12;
            this.label18.Text = "Nước xuất xứ";
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(91, 42);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(267, 21);
            this.txtTenHang.TabIndex = 3;
            this.txtTenHang.TabStop = false;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHS
            // 
            this.txtMaHS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHS.Location = new System.Drawing.Point(91, 69);
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(90, 21);
            this.txtMaHS.TabIndex = 5;
            this.txtMaHS.TabStop = false;
            this.txtMaHS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHS.VisualStyleManager = this.vsmMain;
            this.txtMaHS.Leave += new System.EventHandler(this.txtMaHS_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(364, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Đơn vị tính";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(364, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Số lượng";
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(696, 280);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(615, 280);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 2;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaHang;
            this.rfvMa.ErrorMessage = "\"Mã hàng\" không được để trống.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenHang;
            this.rfvTen.ErrorMessage = "\"Tên hàng\" không được để trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            this.rfvTen.Tag = "rfvTen";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHS;
            this.rfvMaHS.ErrorMessage = "\"Mã HS\" không được để trống.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToValidate = this.txtLuong;
            this.cvSoLuong.ErrorMessage = "\"Số lượng\" không hợp lệ.";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Tag = "cvSoLuong";
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // cvDonGia
            // 
            this.cvDonGia.ControlToValidate = this.txtDGNT;
            this.cvDonGia.ErrorMessage = "\"Đơn giá\" không hợp lệ.";
            this.cvDonGia.Icon = ((System.Drawing.Icon)(resources.GetObject("cvDonGia.Icon")));
            this.cvDonGia.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvDonGia.Tag = "cvDonGia";
            this.cvDonGia.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvDonGia.ValueToCompare = "0";
            // 
            // cvTSNK
            // 
            this.cvTSNK.ControlToValidate = this.txtTS_NK;
            this.cvTSNK.ErrorMessage = "\"Thuế suất NK\" không hợp lệ.";
            this.cvTSNK.Icon = ((System.Drawing.Icon)(resources.GetObject("cvTSNK.Icon")));
            this.cvTSNK.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvTSNK.Tag = "cvTSNK";
            this.cvTSNK.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvTSNK.ValueToCompare = "0";
            // 
            // cvTriGiaTT
            // 
            this.cvTriGiaTT.ControlToValidate = this.txtTGTT_NK;
            this.cvTriGiaTT.ErrorMessage = "\"Trị giá tính thuế NK\" không hợp lệ.";
            this.cvTriGiaTT.Icon = ((System.Drawing.Icon)(resources.GetObject("cvTriGiaTT.Icon")));
            this.cvTriGiaTT.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvTriGiaTT.Tag = "cvTriGiaTT";
            this.cvTriGiaTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvTriGiaTT.ValueToCompare = "0";
            // 
            // cvSoLuongHTS
            // 
            this.cvSoLuongHTS.ControlToValidate = this.txtSoLuong_HTS;
            this.cvSoLuongHTS.ErrorMessage = "\"Số lượng HTS\" không hợp lệ.";
            this.cvSoLuongHTS.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuongHTS.Icon")));
            this.cvSoLuongHTS.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvSoLuongHTS.Tag = "cvSoLuongHTS";
            this.cvSoLuongHTS.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuongHTS.ValueToCompare = "0";
            // 
            // HangMauDichEditForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(787, 314);
            this.Controls.Add(this.uiGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HangMauDichEditForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sửa thông tin hàng hóa";
            this.Load += new System.EventHandler(this.HangMauDichEditForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTSNK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTriGiaTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuongHTS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion        

        private ImageList ImageList1;
        private UIButton btnClose;
        private ContainerValidator cvError;
        private ListValidationSummary lvsError;
        private RequiredFieldValidator rfvMa;
        private RequiredFieldValidator rfvTen;
        private RequiredFieldValidator rfvMaHS;
        private CompareValidator cvSoLuong;
        private CompareValidator cvDonGia;
        private UIGroupBox uiGroupBox2;
        private UIComboBox cbbDVT_HTS;
        private NumericEditBox txtSoLuong_HTS;
        private Label lblDVT_HTS;
        private EditBox txtMa_HTS;
        private Label lblSoLuong_HTS;
        private Label lblMa_HTS;
        private UIComboBox cbDonViTinh;
        private EditBox txtMaHang;
        private NuocHControl ctrNuocXX;
        private Label label4;
        private NumericEditBox txtLuong;
        private Label label2;
        private Label label27;
        private Label label18;
        private EditBox txtTenHang;
        private EditBox txtMaHS;
        private Label label3;
        private Label label6;
        private UIGroupBox uiGroupBox4;
        private Label label9;
        private Label label5;
        private NumericEditBox txtTienThue_NK;
        private Label label1;
        private Label label17;
        private NumericEditBox txtTS_NK;
        private NumericEditBox txtTGTT_NK;
        private Label label23;
        private Label label19;
        private Label label16;
        private NumericEditBox txtTriGiaKB;
        private Label label22;
        private Label lblNguyenTe_TGNT;
        private Label lblNguyenTe_DGNT;
        private NumericEditBox txtDGNT;
        private NumericEditBox txtTGNT;
        private Label label7;
        private Label label8;
        private Label lblTyGiaTT;
        private CompareValidator cvTSNK;
        private CompareValidator cvTriGiaTT;
        private CompareValidator cvSoLuongHTS;
        private EditBox txtTSXNKGiam;
        private Label label20;
        private NumericEditBox txtPhuThu;
        private Label label26;
        private NumericEditBox txtTyLeThuKhac;
        private Label label25;
    }
}
