--DANH MUC VNACCS

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_ApplicationProcedureType]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_ApplicationProcedureType](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[ApplicationProcedureType] [varchar](5) NOT NULL,
	[ApplicationProcedureName] [nvarchar](250) NULL,
	[Section] [int] NULL,
	[IndicationOfGeneralDepartmentOfCustoms] [int] NULL,
	[IndicationOfDisapproval] [int] NULL,
	[IndicationOfCancellation] [int] NULL,
	[IndicationOfCompletionOfExamination] [int] NULL,
	[NumberOfYearsForSavingTheOriginalDocumentData] [int] NULL,
	[ExpiryDate] [datetime] NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ApplicationProcedureType] PRIMARY KEY CLUSTERED 
(
	[ApplicationProcedureType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_BorderGate]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF  not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_BorderGate]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_BorderGate](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[BorderGateCode] [varchar](10) NOT NULL,
	[BorderGateName] [nvarchar](250) NULL,
	[CustomsOfficeCode] [varchar](5) NULL,
	[ImmigrationBureauUserCode] [varchar](5) NULL,
	[ArrivalDepartureForImmigration] [int] NULL,
	[PassengerForImmigration] [int] NULL,
	[QuarantineOfficeUserCode] [varchar](5) NULL,
	[ArrivalDepartureForQuarantine] [int] NULL,
	[PassengerForQuarantine] [int] NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_BorderGate] PRIMARY KEY CLUSTERED 
(
	[BorderGateCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_Cargo]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_Cargo]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_Cargo](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[BondedAreaCode] [varchar](10) NOT NULL,
	[UserCode] [varchar](10) NULL,
	[BondedAreaName] [nvarchar](250) NULL,
	[BondedAreaDemarcation] [char](1) NULL,
	[NecessityIndicationOfBondedAreaName] [int] NULL,
	[CustomsOfficeCodeForImportDeclaration] [varchar](5) NULL,
	[CustomsOfficeCodeForExportDeclaration] [varchar](5) NULL,
	[CustomsOfficeCodeForDeclarationOnTransportation] [varchar](5) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_Cargo] PRIMARY KEY CLUSTERED 
(
	[BondedAreaCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_CityUNLOCODE]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_CityUNLOCODE]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_CityUNLOCODE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[LOCODE] [varchar](10) NULL,
	[CityCode] [varchar](5) NULL,
	[CountryCode] [varchar](5) NULL,
	[NecessityIndicationOfInputName] [int] NULL,
	[AirportPortClassification] [int] NULL,
	[CityNameOrStateName] [nvarchar](250) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_City_UNLOCODE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[t_VNACC_Category_Common]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_Common]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_Common](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceDB] [varchar](5) NULL,
	[Code] [varchar](5) NULL,
	[Name_VN] [nvarchar](max) NULL,
	[Name_EN] [varchar](max) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_ContainerSize]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_ContainerSize]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_VNACC_Category_ContainerSize](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[ContainerSizeCode] [varchar](5) NOT NULL,
	[ContainerSizeName] [nvarchar](250) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ContainerSize_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_CurrencyExchange]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_CurrencyExchange]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_VNACC_Category_CurrencyExchange](
	[ErrorCode] [char](25) NULL,
	[PGNumber] [char](2) NULL,
	[TableID] [char](5) NULL,
	[ProcessClassification] [char](1) NULL,
	[MakerClassification] [char](1) NULL,
	[NumberOfKeyItems] [char](2) NULL,
	[CurrencyCode] [char](3) NOT NULL,
	[CurrencyName] [nvarchar](250) NULL,
	[GenerationManagementIndication] [char](1) NULL,
	[DateOfMaintenanceUpdated] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartDate_1] [datetime] NULL,
	[ExchangeRate_1] [decimal](9, 2) NULL,
	[StartDate_2] [datetime] NULL,
	[ExchangeRate_2] [decimal](9, 2) NULL,
	[StartDate_3] [datetime] NULL,
	[ExchangeRate_3] [decimal](9, 2) NULL,
	[StartDate_4] [datetime] NULL,
	[ExchangeRate_4] [decimal](9, 2) NULL,
	[StartDate_5] [datetime] NULL,
	[ExchangeRate_5] [decimal](9, 2) NULL,
	[StartDate_6] [datetime] NULL,
	[ExchangeRate_6] [decimal](9, 2) NULL,
	[StartDate_7] [datetime] NULL,
	[ExchangeRate_7] [decimal](9, 2) NULL,
	[StartDate_8] [datetime] NULL,
	[ExchangeRate_8] [decimal](9, 2) NULL,
	[StartDate_9] [datetime] NULL,
	[ExchangeRate_9] [decimal](9, 2) NULL,
	[StartDate_10] [datetime] NULL,
	[ExchangeRate_10] [decimal](9, 2) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_CurrencyExchange] PRIMARY KEY CLUSTERED 
(
	[CurrencyCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[t_VNACC_Category_CustomsOffice]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_CustomsOffice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_VNACC_Category_CustomsOffice](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[CustomsCode] [varchar](10) NOT NULL,
	[CustomsProvinceCode] [varchar](5) NULL,
	[OfficeIndicationOfExport] [int] NULL,
	[OfficeIndicationOfImport] [int] NULL,
	[OfficeIndicationOfBondedRelated] [int] NULL,
	[OfficeIndicationOfSupervision] [int] NULL,
	[CustomsOfficeName] [varchar](100) NULL,
	[CustomsOfficeNameInVietnamese] [nvarchar](250) NULL,
	[NameOfHeadOfCustomsOfficeInVietnamese] [nvarchar](250) NULL,
	[Postcode] [varchar](10) NULL,
	[AddressInVietnamese] [nvarchar](250) NULL,
	[NameOfHeadOfCustomsOfficeInRomanAlphabet] [varchar](100) NULL,
	[CustomsProvinceName] [nvarchar](250) NULL,
	[CustomsBranchName] [nvarchar](250) NULL,
	[DestinationCode_1] [varchar](10) NULL,
	[DestinationCode_2] [varchar](10) NULL,
	[DestinationCode_3] [varchar](10) NULL,
	[DestinationCode_4] [varchar](10) NULL,
	[DestinationCode_5] [varchar](10) NULL,
	[DestinationCode_6] [varchar](10) NULL,
	[DestinationCode_7] [varchar](10) NULL,
	[DestinationCode_8] [varchar](10) NULL,
	[DestinationCode_9] [varchar](10) NULL,
	[DestinationCode_10] [varchar](10) NULL,
	[InspectAtInspectionSite] [nvarchar](250) NULL,
	[InspectUsingLargeXrayScanner] [nvarchar](250) NULL,
	[TransportationKindCode] [int] NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_CustomsOffice_1] PRIMARY KEY CLUSTERED 
(
	[CustomsCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_CustomsSubSection]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_CustomsSubSection]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_VNACC_Category_CustomsSubSection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[CustomsCode] [varchar](10) NULL,
	[CustomsSubSectionCode] [varchar](5) NULL,
	[ImportExportClassification] [varchar](5) NULL,
	[Name] [nvarchar](250) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_CustomsSubSection] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_HSCode]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_HSCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_VNACC_Category_HSCode](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[HSCode] [varchar](12) NOT NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HSCode] PRIMARY KEY CLUSTERED 
(
	[HSCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_Nation]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_Nation]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_Nation](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[NationCode] [varchar](10) NOT NULL,
	[GenerationManagementIndication] [varchar](5) NULL,
	[DateOfMaintenanceUpdated] [datetime] NULL,
	[CountryShortName] [varchar](20) NULL,
	[ApplicationStartDate] [datetime] NULL,
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry] [int] NULL,
	[ImportTaxClassificationCodeForFTA1] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA2] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA3] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA4] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA5] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA6] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA7] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA8] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA9] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA10] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA11] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA12] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA13] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA14] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA15] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA16] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA17] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA18] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA19] [varchar](10) NULL,
	[ImportTaxClassificationCodeForFTA20] [varchar](10) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_Nation_1] PRIMARY KEY CLUSTERED 
(
	[NationCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[t_VNACC_Category_OGAUser]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_OGAUser]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_OGAUser](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[OfficeOfApplicationCode] [varchar](10) NOT NULL,
	[OfficeOfApplicationName] [nvarchar](250) NULL,
	[DestinationCode_1] [varchar](10) NULL,
	[DestinationCode_2] [varchar](10) NULL,
	[DestinationCode_3] [varchar](10) NULL,
	[DestinationCode_4] [varchar](10) NULL,
	[DestinationCode_5] [varchar](10) NULL,
	[DestinationCode_6] [varchar](10) NULL,
	[DestinationCode_7] [varchar](10) NULL,
	[DestinationCode_8] [varchar](10) NULL,
	[DestinationCode_9] [varchar](10) NULL,
	[DestinationCode_10] [varchar](10) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_OGA_USER] PRIMARY KEY CLUSTERED 
(
	[OfficeOfApplicationCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_PackagesUnit]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_PackagesUnit]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_PackagesUnit](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[NumberOfPackagesUnitCode] [varchar](5) NOT NULL,
	[NumberOfPackagesUnitName] [nvarchar](250) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_PackagesUnit] PRIMARY KEY CLUSTERED 
(
	[NumberOfPackagesUnitCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_VNACC_Category_QuantityUnit]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_QuantityUnit]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_QuantityUnit](
	[TableID] [varchar](10) NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Notes] [nvarchar](150) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_QuantityUnit] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_Stations]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_Stations]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_Stations](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[StationCode] [varchar](5) NOT NULL,
	[StationName] [nvarchar](250) NULL,
	[CountryCode] [varchar](5) NULL,
	[NecessityIndicationOfInputName] [int] NULL,
	[CustomsOfficeCode] [varchar](5) NULL,
	[CustomsOfficeCodeRM] [varchar](5) NULL,
	[UserCodeOfImmigrationBureau] [varchar](5) NULL,
	[ManifestForImmigration] [int] NULL,
	[ArrivalDepartureForImmigration] [int] NULL,
	[PassengerForImmigration] [int] NULL,
	[CrewForImmigration] [int] NULL,
	[UserCodeOfQuarantineStation] [varchar](5) NULL,
	[ManifestForQuarantine] [int] NULL,
	[ArrivalDepartureForQuarantine] [int] NULL,
	[PassengerForQuarantine] [int] NULL,
	[CrewForQuarantine] [int] NULL,
	[UserCodeOfRailwayStationAuthority] [varchar](5) NULL,
	[ManifestForRailwayStationAuthority] [int] NULL,
	[ArrivalDepartureForRailwayStationAuthority] [int] NULL,
	[PassengerForRailwayStationAuthority] [int] NULL,
	[CrewForRailwayStationAuthority] [int] NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_Stations] PRIMARY KEY CLUSTERED 
(
	[StationCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_TaxClassificationCode]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_TaxClassificationCode]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_TaxClassificationCode](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[TaxCode] [varchar](5) NOT NULL,
	[TaxName] [nvarchar](250) NULL,
	[IndicationOfCommonTax] [decimal](5, 2) NULL,
	[IndicationOfPreferantialTax] [decimal](5, 2) NULL,
	[IndicationOfMFNTax] [decimal](5, 2) NULL,
	[IndicationOfFTATax] [decimal](5, 2) NULL,
	[IndicationOfOutOfQuota] [decimal](5, 2) NULL,
	[IndicationOfSpecificDuty] [decimal](5, 2) NULL,
	[IndicationOfSpecificDutyAndAdValoremDuty] [decimal](5, 2) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TaxClassificationCode] PRIMARY KEY CLUSTERED 
(
	[TaxCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_TransportMeans]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_TransportMeans]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_TransportMeans](
	[ResultCode] [varchar](50) NULL,
	[PGNumber] [int] NULL,
	[TableID] [varchar](10) NULL,
	[ProcessClassification] [varchar](5) NULL,
	[CreatorClassification] [int] NULL,
	[NumberOfKeyItems] [varchar](5) NULL,
	[TransportMeansCode] [varchar](5) NOT NULL,
	[TransportMeansName] [nvarchar](250) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_VNACC_Category_TransportMeans] PRIMARY KEY CLUSTERED 
(
	[TransportMeansCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_VNACC_Category_Type]    Script Date: 11/11/2013 10:57:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_Type]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_VNACC_Category_Type](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[English] [varchar](250) NULL,
	[Vietnamese] [nvarchar](250) NULL,
	[ReferenceDB] [varchar](4) NULL,
	[Notes] [nvarchar](150) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_CategoryType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '10.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('10.2', GETDATE(), N'Them moi cac danh muc VNACCS')
END	
