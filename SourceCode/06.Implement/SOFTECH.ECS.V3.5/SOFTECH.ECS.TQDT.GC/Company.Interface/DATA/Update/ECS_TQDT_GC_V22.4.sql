﻿  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_SelectDynamic_Full]  
-- Database: ECS_GC  
-- Author: Nguyen Dinh Minh  
-- Time created: 21/01/2016 
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectDynamic_Full]  
 @WhereCondition NVARCHAR(500)
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(3250)  
  
SET @SQL =   
'SELECT   
 nl.[HopDong_ID],  
 nl.[Ma],  
 nl.[Ten],  
 nl.[MaHS],  
 nl.[DVT_ID],  
 nl.[SoLuongDangKy],  
 nl.[SoLuongDaNhap],  
 nl.[SoLuongDaDung],  
 nl.[SoLuongCungUng],  
 nl.[TongNhuCau],  
 nl.[STTHang],  
 nl.[TrangThai],  
 nl.[DonGia],
 t.NPLChuaHH,
 t.NPLHH,
 t.NPLCoHH  
FROM [dbo].[t_GC_NguyenPhuLieu] AS nl LEFT JOIN (SELECT npl.MaNguyenPhuLieu,MAX(npl.TenNPL) AS TenNPL, SUM(NPLChuaHH) AS NPLChuaHH, SUM(NPLHH) AS NPLHH, SUM(NPLCoHH) AS NPLCoHH FROM 
(
SELECT  sp.Ma ,
        sp.SoLuongDaXuat ,
        dm.MaNguyenPhuLieu ,
        dm.TenNPL ,
        dm.DinhMucSuDung ,
        dm.TyLeHaoHut,
        CONVERT(DECIMAL(24,6),sp.SoLuongDaXuat*dm.DinhMucSuDung) AS NPLChuaHH,
        CONVERT(DECIMAL(24,6),(sp.SoLuongDaXuat*dm.DinhMucSuDung)*(TyLeHaoHut/100)) AS NPLHH,
        CONVERT(DECIMAL(24,6),(sp.SoLuongDaXuat*dm.DinhMucSuDung)+(sp.SoLuongDaXuat*dm.DinhMucSuDung)*(TyLeHaoHut/100)) AS NPLCoHH
FROM    dbo.t_GC_SanPham AS sp
        JOIN dbo.t_GC_DinhMuc AS dm ON sp.HopDong_ID = dm.HopDong_ID
                                       AND sp.Ma = dm.MaSanPham
WHERE   sp.HopDong_ID = '+ @WhereCondition + '
) AS npl
GROUP BY npl.MaNguyenPhuLieu) AS t ON t.MaNguyenPhuLieu = nl.Ma WHERE nl.HopDong_ID = '+ @WhereCondition +'

ORDER BY nl.Ma'  
   
  
EXEC sp_executesql @SQL  
  

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '22.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('22.3',GETDATE(), N'Fix lại NPLHH')
END	