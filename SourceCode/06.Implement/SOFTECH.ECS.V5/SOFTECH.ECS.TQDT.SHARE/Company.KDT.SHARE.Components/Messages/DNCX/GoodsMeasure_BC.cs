﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.DNCX
{
    public class GoodsMeasure_BC
    {
        /// <summary>
        /// so luong    
        /// </summary>
        [XmlElement("quantity")]
        public List<String> Quantitys { get; set; }

        /// <summary>
        /// Đơn vị tính
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }

    }
}
