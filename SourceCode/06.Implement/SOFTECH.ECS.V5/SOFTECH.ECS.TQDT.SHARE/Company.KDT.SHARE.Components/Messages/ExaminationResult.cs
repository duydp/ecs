﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    /// <summary>
    /// Giấy đăng ký kiểm tra 
    /// </summary>
    public class ExaminationResult : IssueBase
    {

        /// <summary>
        /// Người được cấp
        /// </summary>
        [XmlElement("Register")]
        public NameBase Register { get; set; }

        /// <summary>
        /// Thông tin hàng hóa
        /// </summary>
        [XmlElement("GoodsItem")]
        public List<GoodsItem> GoodsItem { get; set; }
    }
}
