-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Insert]
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten nvarchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChiThiHaiQuan]
(
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@LoaiThongTin,
	@Master_ID,
	@PhanLoai,
	@Ngay,
	@Ten,
	@NoiDung,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Update]
	@ID bigint,
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten nvarchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
SET
	[LoaiThongTin] = @LoaiThongTin,
	[Master_ID] = @Master_ID,
	[PhanLoai] = @PhanLoai,
	[Ngay] = @Ngay,
	[Ten] = @Ten,
	[NoiDung] = @NoiDung,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_InsertUpdate]
	@ID bigint,
	@LoaiThongTin varchar(10),
	@Master_ID bigint,
	@PhanLoai varchar(1),
	@Ngay datetime,
	@Ten nvarchar(500),
	@NoiDung nvarchar(max),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChiThiHaiQuan] 
		SET
			[LoaiThongTin] = @LoaiThongTin,
			[Master_ID] = @Master_ID,
			[PhanLoai] = @PhanLoai,
			[Ngay] = @Ngay,
			[Ten] = @Ten,
			[NoiDung] = @NoiDung,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChiThiHaiQuan]
		(
			[LoaiThongTin],
			[Master_ID],
			[PhanLoai],
			[Ngay],
			[Ten],
			[NoiDung],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@LoaiThongTin,
			@Master_ID,
			@PhanLoai,
			@Ngay,
			@Ten,
			@NoiDung,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_ChiThiHaiQuan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Saturday, April 19, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChiThiHaiQuan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiThongTin],
	[Master_ID],
	[PhanLoai],
	[Ngay],
	[Ten],
	[NoiDung],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ChiThiHaiQuan]	

GO

