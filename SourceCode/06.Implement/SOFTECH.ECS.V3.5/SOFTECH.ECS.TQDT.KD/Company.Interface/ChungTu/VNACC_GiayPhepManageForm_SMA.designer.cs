﻿namespace Company.Interface
{
    partial class VNACC_GiayPhepManageForm_SMA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhepManageForm_SMA));
            Janus.Windows.GridEX.GridEXLayout grdList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnTimKiem = new Janus.Windows.EditControls.UIButton();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.grdList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDel = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(816, 424);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnTimKiem);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtSoGiayPhep);
            this.uiGroupBox1.Controls.Add(this.clcNgayTiepNhan);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(816, 79);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Tìm kiếm";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Image = ((System.Drawing.Image)(resources.GetObject("btnTimKiem.Image")));
            this.btnTimKiem.ImageIndex = 4;
            this.btnTimKiem.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTimKiem.Location = new System.Drawing.Point(445, 20);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(90, 23);
            this.btnTimKiem.TabIndex = 8;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTimKiem.VisualStyleManager = this.vsmMain;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Location = new System.Drawing.Point(142, 20);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(115, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.Location = new System.Drawing.Point(142, 47);
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(115, 21);
            this.txtSoGiayPhep.TabIndex = 5;
            this.txtSoGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayTiepNhan
            // 
            // 
            // 
            // 
            this.clcNgayTiepNhan.DropDownCalendar.Name = "";
            this.clcNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayTiepNhan.IsNullDate = true;
            this.clcNgayTiepNhan.Location = new System.Drawing.Point(340, 20);
            this.clcNgayTiepNhan.Name = "clcNgayTiepNhan";
            this.clcNgayTiepNhan.Nullable = true;
            this.clcNgayTiepNhan.Size = new System.Drawing.Size(90, 21);
            this.clcNgayTiepNhan.TabIndex = 3;
            this.clcNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(74, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Số tiếp nhận";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(74, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Số giấy phép";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(263, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Ngày tiếp nhận";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.grdList);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 79);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(816, 311);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Danh sách giấy phép";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // grdList
            // 
            this.grdList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdList.ColumnAutoResize = true;
            grdList_DesignTimeLayout.LayoutString = resources.GetString("grdList_DesignTimeLayout.LayoutString");
            this.grdList.DesignTimeLayout = grdList_DesignTimeLayout;
            this.grdList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdList.GroupByBoxVisible = false;
            this.grdList.Location = new System.Drawing.Point(3, 17);
            this.grdList.Name = "grdList";
            this.grdList.RecordNavigator = true;
            this.grdList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdList.Size = new System.Drawing.Size(810, 291);
            this.grdList.TabIndex = 0;
            this.grdList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdList.VisualStyleManager = this.vsmMain;
            this.grdList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grdList_DeletingRecord);
            this.grdList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridEX1_RowDoubleClick);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnDel);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 390);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(816, 34);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(729, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDel
            // 
            this.btnDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.Image = ((System.Drawing.Image)(resources.GetObject("btnDel.Image")));
            this.btnDel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDel.Location = new System.Drawing.Point(646, 6);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(75, 23);
            this.btnDel.TabIndex = 4;
            this.btnDel.Text = "Xoá";
            this.btnDel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDel.VisualStyleManager = this.vsmMain;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // VNACC_GiayPhepManageForm_SMA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 424);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_GiayPhepManageForm_SMA";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Theo dõi giấy phép SMA";
            this.Load += new System.EventHandler(this.VNACC_GiayPhep_TheoDoi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX grdList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnTimKiem;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayPhep;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTiepNhan;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnDel;
    }
}