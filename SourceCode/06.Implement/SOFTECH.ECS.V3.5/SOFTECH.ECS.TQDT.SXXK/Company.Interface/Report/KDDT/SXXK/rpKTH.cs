﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class rpKTH : DevExpress.XtraReports.UI.XtraReport
    {
        //private ToKhaiMauDich TKMD = null;

        public rpKTH(ToKhaiMauDich tkmd)
        {
            InitializeComponent();
            lblCuc1.Text = DonViHaiQuan.GetName(tkmd.MaHaiQuan).ToUpper();
            lblNgaythang.Text = GlobalSettings.TieudeNgay;
            lblChiCuc1.Text = "- " + DonViHaiQuan.GetName(tkmd.MaHaiQuan).ToUpper();
            lblDoanhNghiep.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lblDoanhNghiep.Text += ", ĐỊA CHỈ: " + GlobalSettings.DIA_CHI.ToUpper();
            this.dispReport(tkmd);           
        }

        private void dispReport(ToKhaiMauDich TKMD)
        {
            TKMD.LoadHMDCollection();
            if (TKMD.HMDCollection.Count > 0)
            {
                HangMauDich hmd = TKMD.HMDCollection[0];
                lblMatHang.Text = hmd.TenHang;
                lblSoLuong.Text = hmd.SoLuong.ToString() + " " + DonViTinh.GetName(hmd.DVT_ID);
                lblSoTK.Text = TKMD.SoToKhai.ToString();
                lblSoHD.Text = TKMD.SoHopDong;
                lblNgayHD.Text = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            }
        }

        private void lbl_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            if (sender.GetType().ToString() == "DevExpress.XtraReports.UI.XRLabel")
            {
                XRLabel lbl = (XRLabel)sender;
                DoiNoiDung obj = new DoiNoiDung(lbl.Text);
                obj.ShowDialog();
                if (obj.NoiDungMoi != "")
                {
                    lbl.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                    this.CreateDocument();
                }
            }
            else if (sender.GetType().ToString() == "DevExpress.XtraReports.UI.XRTableCell")
            {
                XRTableCell cell = (XRTableCell)sender;
                DoiNoiDung obj = new DoiNoiDung(cell.Text);
                obj.ShowDialog();
                if (obj.NoiDungMoi != "")
                {
                    cell.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                    this.CreateDocument();
                }
            }
        }

        public void dispBackColor(bool val)
        {
            if (val)
            {
                lblCuc1.BackColor = Color.Silver;
                lblCuc2.BackColor = Color.Silver;
                cellSoCV.BackColor = Color.Silver;
                xrTableCell1.BackColor = Color.Silver;
                lblNgaythang.BackColor = Color.Silver;             
                lblChiCuc1.BackColor = Color.Silver;
                lblDoanhNghiep.BackColor = Color.Silver;
                xrLabel2.BackColor = Color.Silver;
                lblMatHang.BackColor = Color.Silver;
                lblSoLuong.BackColor = Color.Silver;
                lblSoTK.BackColor = Color.Silver;
                lblSoHD.BackColor = Color.Silver;
                lblNgayHD.BackColor = Color.Silver;
                xrLabel7.BackColor = Color.Silver;
                xrLabel13.BackColor = Color.Silver;
                xrLabel14.BackColor = Color.Silver;
                xrLabel11.BackColor = Color.Silver;
                lblChuKy.BackColor = Color.Silver;
                xrLabel16.BackColor = Color.Silver;
                xrLabel12.BackColor = Color.Silver;
            }
            else
            {
                lblCuc1.BackColor = Color.Transparent;
                lblCuc2.BackColor = Color.Transparent;
                cellSoCV.BackColor = Color.Transparent;
                xrTableCell1.BackColor = Color.Transparent;
                lblNgaythang.BackColor = Color.Transparent;                
                lblChiCuc1.BackColor = Color.Transparent;
                lblDoanhNghiep.BackColor = Color.Transparent;
                xrLabel2.BackColor = Color.Transparent;
                lblMatHang.BackColor = Color.Transparent;
                lblSoLuong.BackColor = Color.Transparent;
                lblSoTK.BackColor = Color.Transparent;
                lblSoHD.BackColor = Color.Transparent;
                lblNgayHD.BackColor = Color.Transparent;
                xrLabel7.BackColor = Color.Transparent;
                xrLabel13.BackColor = Color.Transparent;
                xrLabel14.BackColor = Color.Transparent;
                xrLabel11.BackColor = Color.Transparent;
                lblChuKy.BackColor = Color.Transparent;
                xrLabel16.BackColor = Color.Transparent;
                xrLabel12.BackColor = Color.Transparent;
            }
            this.CreateDocument();
        }
    }
}
