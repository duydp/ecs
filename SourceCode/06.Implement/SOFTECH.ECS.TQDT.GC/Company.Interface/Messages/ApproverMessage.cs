﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Messages
{
  public  abstract class ApproverMessage
    {
        public delegate string MessageHandler(object sender, MessageXmlEventArgs e);
        public MessageHandler MessageHome;  
        public abstract string MessageXmlHandler(object sender, MessageXmlEventArgs e);

        public ApproverMessage()
        {  
            MessageHome += new MessageHandler(MessageXmlHandler);
        }
        public string ProcessRequest(MessageXml messagexml)
        {
            MessageXmlEventArgs e = new MessageXmlEventArgs { MessageXml = messagexml };
            return OnMessage(e);
        }
        public virtual string OnMessage(MessageXmlEventArgs e)
        {
            if (MessageHome != null)
              return  MessageHome(this, e);
            else
            return string.Empty;
        }
        
        public ApproverMessage Successor { get; set; }
    }
}
