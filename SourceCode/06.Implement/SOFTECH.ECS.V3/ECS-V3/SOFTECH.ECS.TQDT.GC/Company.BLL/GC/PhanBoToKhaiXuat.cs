﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;


namespace Company.GC.BLL.GC
{
	public partial class PhanBoToKhaiXuat
	{
        private List<PhanBoToKhaiNhap> _PhanBoToKhaiNhapCollection;
        public List<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection
        {
            set { _PhanBoToKhaiNhapCollection = value; }
            get { return _PhanBoToKhaiNhapCollection; }
        }
        public void LoadPhanBoToKhaiNhapCollection()
        {
            _PhanBoToKhaiNhapCollection = (List<PhanBoToKhaiNhap>)PhanBoToKhaiNhap.SelectCollectionBy_TKXuat_ID(this.ID);
        }

        public static void InsertUpdateFull(ToKhaiMauDich TKMD, DataTable tableTon)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    try
                    {
                        TKMD.TrangThaiPhanBo = 1;//=1 da thuc hien phan bo
                        TKMD.UpdateTransaction(transaction);
                        foreach (PhanBoToKhaiXuat pbToKhaiXuat in TKMD.PhanBoToKhaiXuatCollection)
                        {
                            if (pbToKhaiXuat.ID == 0)
                                pbToKhaiXuat.Insert(transaction);
                            else
                                pbToKhaiXuat.Update(transaction);
                            foreach (PhanBoToKhaiNhap pbTKNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                            {
                                pbTKNhap.TKXuat_ID = pbToKhaiXuat.ID;
                                if (pbTKNhap.ID == 0)
                                    pbTKNhap.Insert(transaction);
                                else
                                    pbTKNhap.Update(transaction);
                            }
                        }
                        foreach (DataRow row in tableTon.Rows)
                        {
                            NPLNhapTonThucTe npl = new NPLNhapTonThucTe();
                            npl.ID = Convert.ToInt64(row["id"]);
                            npl.Luong = Convert.ToDecimal(row["Luong"]);
                            npl.MaDoanhNghiep = (row["MaDoanhNghiep"].ToString());
                            npl.MaHaiQuan = (row["MaHaiQuan"].ToString());
                            npl.MaLoaiHinh = (row["MaLoaiHinh"].ToString());
                            npl.MaNPL = (row["MaNPL"].ToString());
                            npl.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                            npl.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"]);
                            npl.SoToKhai = Convert.ToInt32(row["SoToKhai"]);
                            npl.Ton = Convert.ToDecimal(row["Ton"]);
                            npl.ID_HopDong = Convert.ToInt64(row["ID_HopDong"]);
                            npl.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public static void InsertUpdateFull(ToKhaiChuyenTiep TKCT, DataTable tableTon)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    try
                    {
                        //TKCT.TrangThaiPhanBo = 1;//=1 da thuc hien phan bo
                        TKCT.Update(transaction);
                        foreach (PhanBoToKhaiXuat pbToKhaiXuat in TKCT.PhanBoToKhaiXuatCollection)
                        {
                            if (pbToKhaiXuat.ID == 0)
                                pbToKhaiXuat.Insert(transaction);
                            else
                                pbToKhaiXuat.Update(transaction);
                            foreach (PhanBoToKhaiNhap pbTKNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                            {
                                pbTKNhap.TKXuat_ID = pbToKhaiXuat.ID;
                                if (pbTKNhap.ID == 0)
                                    pbTKNhap.Insert(transaction);
                                else
                                    pbTKNhap.Update(transaction);
                            }
                        }
                        foreach (DataRow row in tableTon.Rows)
                        {
                            NPLNhapTonThucTe npl = new NPLNhapTonThucTe();
                            npl.ID = Convert.ToInt64(row["id"]);
                            npl.Luong = Convert.ToDecimal(row["Luong"]);
                            npl.MaDoanhNghiep = (row["MaDoanhNghiep"].ToString());
                            npl.MaHaiQuan = (row["MaHaiQuan"].ToString());
                            npl.MaLoaiHinh = (row["MaLoaiHinh"].ToString());
                            npl.MaNPL = (row["MaNPL"].ToString());
                            npl.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                            npl.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"]);
                            npl.SoToKhai = Convert.ToInt32(row["SoToKhai"]);
                            npl.Ton = Convert.ToDecimal(row["Ton"]);
                            npl.ID_HopDong = Convert.ToInt64(row["ID_HopDong"]);
                            npl.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public static DataSet SelectViewPhanBoToKhaiXuatOfTKMDAndMaNPL(long idTKX, string MaNPL)
        {
            string spName = "select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo from v_GC_PhanBo where ID_TKMD = " + idTKX +" AND MaLoaiHinhXuat LIKE 'XGC%' AND MaNPL='" + MaNPL + "'" +
                 " GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }
        public static DataSet SelectViewPhanBoToKhaiXuatOfTKCTAndMaNPL(long idTKX, string MaNPL)
        {
            string spName = "select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo from v_GC_PhanBo where ID_TKMD = " + idTKX + " AND MaLoaiHinhXuat LIKE 'PH%' AND MaNPL='" + MaNPL + "'" +
                 " GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKMD(long ID_TKMD)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("ID_TKMD=" + ID_TKMD + " AND MaDoanhNghiep LIKE 'XGC%'", "");
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection();
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKCT(long ID_TKMD)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("ID_TKMD=" + ID_TKMD + " AND MaDoanhNghiep LIKE 'PH%'", "");
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection();
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKMD(long ID_TKMD, SqlTransaction transaction)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("ID_TKMD=" + ID_TKMD + " AND MaDoanhNghiep LIKE 'XGC%'", "", transaction);
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection(transaction);
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IList<PhanBoToKhaiXuat> SelectPhanBoToKhaiXuatOfTKCT(long ID_TKMD, SqlTransaction transaction)
        {
            IList<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            PhanBoToKhaiXuatCollection = PhanBoToKhaiXuat.SelectCollectionDynamic("ID_TKMD=" + ID_TKMD + " AND MaDoanhNghiep LIKE 'PH%'", "", transaction);
            foreach (PhanBoToKhaiXuat pbTKXuat in PhanBoToKhaiXuatCollection)
            {
                pbTKXuat.LoadPhanBoToKhaiNhapCollection(transaction);
            }
            return PhanBoToKhaiXuatCollection;
        }
        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_GC_PhanBoToKhaiXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand, transaction);
        }
        public static IList<PhanBoToKhaiXuat> SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction)
        {
            IList<PhanBoToKhaiXuat> collection = new List<PhanBoToKhaiXuat>();

            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression, transaction);
            while (reader.Read())
            {
                PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_TKMD"))) entity.ID_TKMD = reader.GetInt64(reader.GetOrdinal("ID_TKMD"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
        public void DeletePhanBo(SqlTransaction transaction)
        {
            foreach (PhanBoToKhaiNhap pbToKhaiNhap in this.PhanBoToKhaiNhapCollection)
            {
                if (pbToKhaiNhap.SoToKhaiNhap > 0)
                {
                    NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load(pbToKhaiNhap.SoToKhaiNhap, pbToKhaiNhap.MaLoaiHinhNhap, (short)pbToKhaiNhap.NamDangKyNhap, pbToKhaiNhap.MaHaiQuanNhap, pbToKhaiNhap.MaNPL, transaction);
                    if (nplTon == null)
                    {
                        throw new Exception("Không có tờ khai : " + pbToKhaiNhap.SoToKhaiNhap + "/" + pbToKhaiNhap.MaLoaiHinhNhap + "/" + pbToKhaiNhap.NamDangKyNhap + "không có trong hệ thống.");
                    }
                    nplTon.Ton += Convert.ToDecimal(pbToKhaiNhap.LuongPhanBo);
                    nplTon.Update(transaction);
                }
                pbToKhaiNhap.Delete(transaction);

            }
            this.Delete(transaction);
        }
        public void LoadPhanBoToKhaiNhapCollection(SqlTransaction transaction)
        {
            PhanBoToKhaiNhapCollection = (List<PhanBoToKhaiNhap>)PhanBoToKhaiNhap.SelectCollectionBy_TKXuat_ID(this.ID,transaction);
        }
        public static void PhanBoChoToKhaiXuatGC(ToKhaiMauDich TKMD,int SoThapPhanNPL, int SoThapPhanDM)
        {
            if (TKMD.HMDCollection.Count == 0)
            {
                TKMD.LoadHMDCollection();
            }
            
            //TKMD.HMDCollection.Sort(new SortHangMauDich());
            TKMD.PhanBoToKhaiXuatCollection=new List<PhanBoToKhaiXuat>();
            DataTable tableNPLTonThucTe = NPLNhapTonThucTe.SelectNPLTonThucTeNgayDangKyNhoHonTKXuat(TKMD.NgayDangKy, TKMD.IDHopDong);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            foreach (HangMauDich HMD in TKMD.HMDCollection)
            {
                PhanBoToKhaiXuat pbTKXuat = new PhanBoToKhaiXuat();
                pbTKXuat.MaDoanhNghiep = TKMD.MaLoaiHinh;
                pbTKXuat.MaSP = HMD.MaPhu.Trim();
                pbTKXuat.SoLuongXuat = HMD.SoLuong;
                pbTKXuat.ID_TKMD = TKMD.ID;
                pbTKXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
                TKMD.PhanBoToKhaiXuatCollection.Add(pbTKXuat);
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKMD.IDHopDong;
                sp.Ma = HMD.MaPhu.Trim();
                if (!sp.Load())
                {
                    throw new Exception("Mã sản phẩm : " + HMD.MaPhu.Trim() + " trong tờ khai xuất số : " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NgayDangKy.Year + " không có trong hợp đồng này.Hãy kiểm tra lại mã sản phẩm này.");
                }
                DataTable dsDinhMucSanPham = DinhMuc.getDinhMuc(TKMD.IDHopDong, HMD.MaPhu.Trim(), HMD.SoLuong,SoThapPhanNPL, SoThapPhanDM);
                if (dsDinhMucSanPham.Rows.Count == 0)
                {
                    throw new Exception("Mã sản phẩm : " + HMD.MaPhu.Trim() + " trong tờ khai xuất số : " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NgayDangKy.Year + " chưa có định mức trong hợp đồng này.Hãy kiểm tra lại định mức của mã sản phẩm này.");
                }
                foreach (DataRow rowDinhMuc in dsDinhMucSanPham.Rows)
                {
                    string MaNPL = rowDinhMuc["MaNguyenPhuLieu"].ToString();
                    decimal NhuCau = Convert.ToDecimal(rowDinhMuc["NhuCau"]);
                    DataRow[] RowToKhaiCollection = tableNPLTonThucTe.Select("MaNPL='" + MaNPL + "' and Ton>0", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan");
                    if (RowToKhaiCollection.Length == 0)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(rowDinhMuc["DinhMucChung"]);
                        pbToKhaiNhap.LuongCungUng = Convert.ToDouble(NhuCau);
                        pbToKhaiNhap.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                        pbToKhaiNhap.MaNPL = MaNPL;
                        pbTKXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                        continue;
                    }
                    foreach (DataRow rowTK in RowToKhaiCollection)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        decimal LuongTon = Convert.ToDecimal(rowTK["Ton"].ToString());
                        if (LuongTon > 0)
                        {
                            pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(rowDinhMuc["DinhMucChung"]);
                            pbToKhaiNhap.LuongCungUng = 0;
                            pbToKhaiNhap.LuongTonDau = Convert.ToDouble(LuongTon);
                            bool ok = true;//true la du --> thoat vong lap//false la thieu phai dung to khai khac
                            if (NhuCau > LuongTon)
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(LuongTon);
                                NhuCau -= LuongTon;
                                rowTK["Ton"] = 0;
                                ok = false;
                                pbToKhaiNhap.LuongTonCuoi = 0;
                            }
                            else
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(NhuCau);
                                rowTK["Ton"] = (LuongTon - NhuCau);
                                NhuCau = 0;
                                pbToKhaiNhap.LuongTonCuoi = Convert.ToDouble(rowTK["Ton"]);
                            }
                            pbToKhaiNhap.MaDoanhNghiep = rowTK["MaDoanhNghiep"].ToString();
                            pbToKhaiNhap.MaHaiQuanNhap = rowTK["MaHaiQuan"].ToString();
                            pbToKhaiNhap.MaLoaiHinhNhap = rowTK["MaLoaiHinh"].ToString();
                            pbToKhaiNhap.MaNPL = MaNPL;
                            pbToKhaiNhap.NamDangKyNhap = Convert.ToInt16(rowTK["NamDangKy"]);
                            pbToKhaiNhap.SoToKhaiNhap = Convert.ToInt32(rowTK["SoToKhai"]);
                            //if(pbToKhaiNhap.SoToKhaiNhap==)
                            //{

                            //}
                            pbTKXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                            if (ok)
                                break;
                        }
                    }
                    if (NhuCau > 0)
                    {
                        pbTKXuat.PhanBoToKhaiNhapCollection[pbTKXuat.PhanBoToKhaiNhapCollection.Count - 1].LuongCungUng = Convert.ToDouble(NhuCau);
                    }
                }
                
            }
            InsertUpdateFull(TKMD, tableNPLTonThucTe);
        }

        public static void PhanBoChoToKhaiChuyenTiep(ToKhaiChuyenTiep TKCT, int SoThapPhanNPL, int SoThapPhanDM)
        {
            if (TKCT.HCTCollection.Count == 0)
            {
                TKCT.LoadHCTCollection();
            }

            //TKMD.HMDCollection.Sort(new SortHangMauDich());
            TKCT.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            DataTable tableNPLTonThucTe = NPLNhapTonThucTe.SelectNPLTonThucTeNgayDangKyNhoHonTKXuat(TKCT.NgayDangKy, TKCT.IDHopDong);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
            {
                PhanBoToKhaiXuat pbTKXuat = new PhanBoToKhaiXuat();
                pbTKXuat.MaDoanhNghiep = TKCT.MaLoaiHinh;
                pbTKXuat.MaSP = HCT.MaHang.Trim();
                pbTKXuat.SoLuongXuat = HCT.SoLuong;
                pbTKXuat.ID_TKMD = TKCT.ID;
                pbTKXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
                TKCT.PhanBoToKhaiXuatCollection.Add(pbTKXuat);
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKCT.IDHopDong;
                sp.Ma = HCT.MaHang.Trim();
                if (!sp.Load())
                {
                    throw new Exception("Mã sản phẩm : " + HCT.MaHang.Trim() + " trong tờ khai xuất số : " + TKCT.SoToKhai + "/" + TKCT.MaLoaiHinh + "/" + TKCT.NgayDangKy.Year + " không có trong hợp đồng này.Hãy kiểm tra lại mã sản phẩm này.");
                }
                DataTable dsDinhMucSanPham = DinhMuc.getDinhMuc(TKCT.IDHopDong, HCT.MaHang.Trim(), HCT.SoLuong, SoThapPhanNPL, SoThapPhanDM);
                if (dsDinhMucSanPham.Rows.Count == 0)
                {
                    throw new Exception("Mã sản phẩm : " + HCT.MaHang.Trim() + " trong tờ khai xuất số : " + TKCT.SoToKhai + "/" + TKCT.MaLoaiHinh + "/" + TKCT.NgayDangKy.Year + " chưa có định mức trong hợp đồng này.Hãy kiểm tra lại định mức của mã sản phẩm này.");
                }
                foreach (DataRow rowDinhMuc in dsDinhMucSanPham.Rows)
                {
                    string MaNPL = rowDinhMuc["MaNguyenPhuLieu"].ToString();
                    decimal NhuCau = Convert.ToDecimal(rowDinhMuc["NhuCau"]);
                    DataRow[] RowToKhaiCollection = tableNPLTonThucTe.Select("MaNPL='" + MaNPL + "' and Ton>0", "NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan");
                    if (RowToKhaiCollection.Length == 0)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(rowDinhMuc["DinhMucChung"]);
                        pbToKhaiNhap.LuongCungUng = Convert.ToDouble(NhuCau);
                        pbToKhaiNhap.MaDoanhNghiep = TKCT.MaDoanhNghiep;
                        pbToKhaiNhap.MaNPL = MaNPL;
                        pbTKXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                        continue;
                    }
                    foreach (DataRow rowTK in RowToKhaiCollection)
                    {
                        PhanBoToKhaiNhap pbToKhaiNhap = new PhanBoToKhaiNhap();
                        decimal LuongTon = Convert.ToDecimal(rowTK["Ton"].ToString());
                        if (LuongTon > 0)
                        {
                            pbToKhaiNhap.DinhMucChung = Convert.ToDecimal(rowDinhMuc["DinhMucChung"]);
                            pbToKhaiNhap.LuongCungUng = 0;
                            pbToKhaiNhap.LuongTonDau = Convert.ToDouble(LuongTon);
                            bool ok = true;//true la du --> thoat vong lap//false la thieu phai dung to khai khac
                            if (NhuCau > LuongTon)
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(LuongTon);
                                NhuCau -= LuongTon;
                                rowTK["Ton"] = 0;
                                ok = false;
                                pbToKhaiNhap.LuongTonCuoi = 0;
                            }
                            else
                            {
                                pbToKhaiNhap.LuongPhanBo = Convert.ToDouble(NhuCau);
                                rowTK["Ton"] = (LuongTon - NhuCau);
                                NhuCau = 0;
                                pbToKhaiNhap.LuongTonCuoi = Convert.ToDouble(rowTK["Ton"]);
                            }
                            pbToKhaiNhap.MaDoanhNghiep = rowTK["MaDoanhNghiep"].ToString();
                            pbToKhaiNhap.MaHaiQuanNhap = rowTK["MaHaiQuan"].ToString();
                            pbToKhaiNhap.MaLoaiHinhNhap = rowTK["MaLoaiHinh"].ToString();
                            pbToKhaiNhap.MaNPL = MaNPL;
                            pbToKhaiNhap.NamDangKyNhap = Convert.ToInt16(rowTK["NamDangKy"]);
                            pbToKhaiNhap.SoToKhaiNhap = Convert.ToInt32(rowTK["SoToKhai"]);
                            //if(pbToKhaiNhap.SoToKhaiNhap==)
                            //{

                            //}
                            pbTKXuat.PhanBoToKhaiNhapCollection.Add(pbToKhaiNhap);
                            if (ok)
                                break;
                        }
                    }
                    if (NhuCau > 0)
                    {
                        pbTKXuat.PhanBoToKhaiNhapCollection[pbTKXuat.PhanBoToKhaiNhapCollection.Count - 1].LuongCungUng = Convert.ToDouble(NhuCau);
                    }
                }

            }
            InsertUpdateFull(TKCT, tableNPLTonThucTe);
        }

        public void XoaPhanBo(SqlTransaction transaction)
        {
            foreach (PhanBoToKhaiNhap pbToKhaiNhap in this.PhanBoToKhaiNhapCollection)
            {
                if (pbToKhaiNhap.SoToKhaiNhap > 0)
                {
                    NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load(pbToKhaiNhap.SoToKhaiNhap, pbToKhaiNhap.MaLoaiHinhNhap, (short)pbToKhaiNhap.NamDangKyNhap, pbToKhaiNhap.MaHaiQuanNhap, pbToKhaiNhap.MaNPL, transaction);
                    if (nplTon == null)
                    {
                        throw new Exception("Không có tờ khai : " + pbToKhaiNhap.SoToKhaiNhap + "/" + pbToKhaiNhap.MaLoaiHinhNhap + "/" + pbToKhaiNhap.NamDangKyNhap + "không có trong hệ thống.");
                    }
                    nplTon.Ton += Convert.ToDecimal(pbToKhaiNhap.LuongPhanBo);
                    nplTon.Update(transaction);
                }
                pbToKhaiNhap.Delete(transaction);
            }
            this.Delete(transaction);
        }

        public static void XoaPhanBoToKhaiXuatGC(ToKhaiMauDich TKMD)
        {
            if (TKMD.PhanBoToKhaiXuatCollection.Count == 0)
            {
                TKMD.LoadPhanBoToKhaiXuatCollection();
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiXuat item in TKMD.PhanBoToKhaiXuatCollection)
                        {
                            item.XoaPhanBo(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

        }

        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(string maHaiQuan, string MaDoanhNghiep, string MaNPL)
        {
            string sql = "select * from t_GC_NPLNhapTonThucTe where MaDoanhNghiep='" + MaDoanhNghiep + "' and MaHaiQuan='" + maHaiQuan + "' and Ton>0 and ngaydangky<@ngaydangky and MaNPL=@MaNPL order by NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand);
        }
        public static void PhanBoChoToKhaiTaiXuat(ToKhaiMauDich TKMD,int SoThapPhanNPL)
        {
            if (TKMD.HMDCollection.Count == 0)
            {
                TKMD.LoadHMDCollection();
            }
            //TKMD.HMDCollection.Sort(new SortHangMauDich());


            DataTable tableTK = NPLNhapTonThucTe.SelectBy_HopDongAndTonHonO(TKMD.IDHopDong, TKMD.NgayDangKy, SoThapPhanNPL).Tables[0];


            foreach (PhanBoToKhaiXuat pbToKhaiXuat in TKMD.PhanBoToKhaiXuatCollection)
            {
                foreach (PhanBoToKhaiNhap pbToKhaiNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    DataRow rowToKhaiNhap = tableTK.Select("SoToKhai=" + pbToKhaiNhap.SoToKhaiNhap + " and MaLoaiHinh='" + pbToKhaiNhap.MaLoaiHinhNhap + "' and NamDangKy=" + pbToKhaiNhap.NamDangKyNhap + " and MaHaiQuan='" + pbToKhaiNhap.MaHaiQuanNhap + "' and MaNPL='" + pbToKhaiNhap.MaNPL.Trim() + "'")[0];
                    pbToKhaiNhap.LuongTonDau = Convert.ToDouble(rowToKhaiNhap["Ton"]);
                    pbToKhaiNhap.LuongTonCuoi = pbToKhaiNhap.LuongTonDau - pbToKhaiNhap.LuongPhanBo;
                    rowToKhaiNhap["Ton"] = pbToKhaiNhap.LuongTonCuoi;
                }
            }
            InsertUpdateFull(TKMD, tableTK);
        }
        public static void PhanBoChoToKhaiTaiXuat(ToKhaiChuyenTiep TKCT, int SoThapPhanNPL)
        {
            if (TKCT.HCTCollection.Count == 0)
            {
                TKCT.LoadHCTCollection();
            }
            //TKCT.HCTCollection.Sort(new SortHangMauDich());


            DataTable tableTK = NPLNhapTonThucTe.SelectBy_HopDongAndTonHonO(TKCT.IDHopDong, TKCT.NgayDangKy, SoThapPhanNPL).Tables[0];


            foreach (PhanBoToKhaiXuat pbToKhaiXuat in TKCT.PhanBoToKhaiXuatCollection)
            {
                foreach (PhanBoToKhaiNhap pbToKhaiNhap in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    DataRow rowToKhaiNhap = tableTK.Select("SoToKhai=" + pbToKhaiNhap.SoToKhaiNhap + " and MaLoaiHinh='" + pbToKhaiNhap.MaLoaiHinhNhap + "' and NamDangKy=" + pbToKhaiNhap.NamDangKyNhap + " and MaHaiQuan='" + pbToKhaiNhap.MaHaiQuanNhap + "' and MaNPL='" + pbToKhaiNhap.MaNPL.Trim() + "'")[0];
                    pbToKhaiNhap.LuongTonDau = Convert.ToDouble(rowToKhaiNhap["Ton"]);
                    pbToKhaiNhap.LuongTonCuoi = pbToKhaiNhap.LuongTonDau - pbToKhaiNhap.LuongPhanBo;
                    rowToKhaiNhap["Ton"] = pbToKhaiNhap.LuongTonCuoi;
                }
            }
            InsertUpdateFull(TKCT, tableTK);
        }

        public static bool CheckPhanBoToKhaiXuat(long ID_TKMD)
        {
            string sql = "select id  from t_GC_PhanBoToKhaiXuat where ID_TKMD=@ID_TKMD AND MaDoanhNghiep lIKE 'XGC%'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null) return false;
            return true;
        }
        public static bool CheckPhanBoToKhaiChuyenTiepXuat(long ID_TKMD)
        {
            string sql = "select id  from t_GC_PhanBoToKhaiXuat where ID_TKMD=@ID_TKMD AND MaDoanhNghiep lIKE 'PH%'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null) return false;
            return true;
        }

       
	}	
}