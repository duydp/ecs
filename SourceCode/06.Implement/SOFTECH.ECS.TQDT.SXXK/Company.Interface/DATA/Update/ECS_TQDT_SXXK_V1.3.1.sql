/*
Run this script on:

        192.168.72.151.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.151.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 12/19/2011 9:54:40 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(35,18),
	@DonGiaKB float,
	@TriGiaKB float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HopDongThuongMaiDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HopDongThuongMaiDetail] 
		SET
			[HMD_ID] = @HMD_ID,
			[HopDongTM_ID] = @HopDongTM_ID,
			[GhiChu] = @GhiChu,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HopDongThuongMaiDetail]
		(
			[HMD_ID],
			[HopDongTM_ID],
			[GhiChu],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB]
		)
		VALUES 
		(
			@HMD_ID,
			@HopDongTM_ID,
			@GhiChu,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB
		)		
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HopDongThuongMaiDetail_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_Update]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_Update]
	@ID bigint,
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(35,18),
	@DonGiaKB float,
	@TriGiaKB float
AS

UPDATE
	[dbo].[t_KDT_HopDongThuongMaiDetail]
SET
	[HMD_ID] = @HMD_ID,
	[HopDongTM_ID] = @HopDongTM_ID,
	[GhiChu] = @GhiChu,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, June 08, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HopDongThuongMaiDetail_Insert]
	@HMD_ID bigint,
	@HopDongTM_ID bigint,
	@GhiChu nvarchar(255),
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(35,18),
	@DonGiaKB float,
	@TriGiaKB float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HopDongThuongMaiDetail]
(
	[HMD_ID],
	[HopDongTM_ID],
	[GhiChu],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB]
)
VALUES 
(
	@HMD_ID,
	@HopDongTM_ID,
	@GhiChu,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_Version]'
GO
ALTER TABLE [dbo].[t_HaiQuan_Version] ALTER COLUMN [Version] [decimal] (5, 2) NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.3.1', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.3.1', getdate(), null)
	end