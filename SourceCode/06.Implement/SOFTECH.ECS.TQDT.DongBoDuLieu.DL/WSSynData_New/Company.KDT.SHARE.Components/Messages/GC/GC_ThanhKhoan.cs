﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{   
    [XmlRoot("Declaration")]
    public class GC_ThanhKhoan : DeclarationBase
    { 
      
        /// <summary>
        /// Thông tin thanh khoản
        /// </summary>
        [XmlElement("ContractReference")]
        public ContractDocument ContractReference { get; set; }
        /// <summary>
        /// Hàng thanh khoản
        /// </summary>
        [XmlElement("GoodsItems")]
        public List<GoodsItem> GoodsItems { set; get; }

        ///// <summary>
        ///// Hàng thanh khoản
        ///// </summary>
        //[XmlElement("AdditionalDocument")]
        //public List<AdditionalDocument> AdditionalDocument { set; get; }

        /// <summary>
        /// De nghi cua doanh nghiep
        /// </summary>
        [XmlElement("suggest")]
        public string Suggest { set; get; }
        


    }
}
