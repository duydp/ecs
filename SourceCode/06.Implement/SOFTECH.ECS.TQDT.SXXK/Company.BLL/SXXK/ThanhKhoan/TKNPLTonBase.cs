using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Company.BLL.SXXK.ThanhKhoan
{
   public partial class TKNPLTon : EntityBase 
    {
        #region Private members.

        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected short _NamDangKy;
        protected string _MaHaiQuan = String.Empty;
        protected string _MaNPL = String.Empty;
        protected string _TenNguyenPhuLieu = String.Empty;
        protected decimal _NPLThieu;
        protected decimal _NPLYeuCau;
        //protected double _ThueXNK;
        //protected double _ThueTTDB;
        //protected double _ThueVAT;
        //protected double _PhuThu;
        //protected double _ThueCLGia;
        //protected double _ThueTon;
        protected decimal _TotalNPL;
        protected string _MaSP;
        protected decimal _SoLuongCan;
        protected decimal _SoLuongDapUngt;
        protected decimal _DinhMucChung;
        protected string _MaNguyenPhuLieu = String.Empty;



        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public short NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
       public string TenNPL
       {
           set { this._TenNguyenPhuLieu = value; }
           get { return this._TenNguyenPhuLieu; }
       }

        public decimal TotalNPL
        {
            set { this._TotalNPL = value; }
            get { return this._TotalNPL; }
        }


       public decimal NPLYeuCau
       {
           set { this._NPLYeuCau = value; }
           get { return this._NPLYeuCau; }
       }

        public decimal DinhMucChung
        {
            set { this._DinhMucChung = value; }
            get { return this._DinhMucChung; }
        }
       public decimal SoLuongCan
       {
           set { this._SoLuongCan  = value; }
           get { return this._SoLuongCan; }
       }
       public decimal SoLuongDapUng
       {
           set { this._SoLuongDapUngt  = value; }
           get { return this._SoLuongDapUngt; }
       }
       public decimal NPLThieu
        {
            set { this._NPLThieu = value; }
            get { return this._NPLThieu; }
        }

        public string MaNguyenPhuLieu
        {
            set { this._MaNguyenPhuLieu = value; }
            get { return this._MaNguyenPhuLieu; }
        }

        public string MaSP
        {
            set { this._MaSP = value; }
            get { return this._MaSP; }
        }
        #endregion
       
       
       
        #region Dinhmuc
        public DataTable DinhMuc(string maSP)
           {
               StringBuilder strBD = new StringBuilder(string.Empty);               
               strBD.Append(" SELECT  ");
               strBD.Append(" t_SXXK_DinhMuc.MaNguyenPhuLieu, ");
               strBD.Append(" t_SXXK_DinhMuc.DinhMucChung,  ");
               strBD.Append(" t_SXXK_DinhMuc.MaSanPham  ");
               strBD.Append(" FROM  ");
               strBD.Append(" t_SXXK_DinhMuc  ");              
               strBD.Append(" WHERE t_SXXK_DinhMuc.MaSanPham =@MaSanPham  "); 
               SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strBD.ToString());
               this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, maSP);

               return this.db.ExecuteDataSet(dbCommand).Tables[0];
           }
        #endregion

        #region Tongton
        public DataTable TongTon( )
           {
               StringBuilder strBD = new StringBuilder(string.Empty);

               
               strBD.Append(" SELECT  ");
               strBD.Append(" v_ThongKeNPLTon.MaNPL,  ");
               strBD.Append(" v_ThongKeNPLTon.TenHang,  ");
               strBD.Append("  SUM (v_ThongKeNPLTon.Ton) as TotalNPL  ");
               strBD.Append(" FROM  ");
               strBD.Append(" v_ThongKeNPLTon  ");
               strBD.Append("  GROUP BY ");
               strBD.Append(" v_ThongKeNPLTon.MaNPL, ");
               strBD.Append(" v_ThongKeNPLTon.TenHang  ");
               strBD.Append(" HAVING SUM (v_ThongKeNPLTon.Ton) >=0 ");
               SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strBD.ToString());
               return this.db.ExecuteDataSet(dbCommand).Tables[0];
           }
        #endregion
        
        #region Kiem tra MaSP
               public DataTable  MaSanPham ()
               {
                   StringBuilder strBD = new StringBuilder(string.Empty);
                   strBD.Append(" SELECT  ");
                   strBD.Append(" t_SXXK_SanPham.Ma  "); 
                   strBD.Append(" FROM  "); 
                   strBD.Append(" t_SXXK_SanPham   ");
                   SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strBD.ToString());
                   return this.db.ExecuteDataSet(dbCommand).Tables[0];
               }
        #endregion

       }
}
