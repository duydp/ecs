------- Cập nhật vận đơn 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name IN ('SoVanDonChu','NamVanDonChu'))
	ALTER TABLE dbo.t_KDT_VNACC_TK_SoVanDon
	ADD  
	   SoVanDonChu VARCHAR(200) NULL,
	   NamVanDonChu  VARCHAR(200) NULL;
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name IN ('LoaiDinhDanh'))
    ALTER TABLE dbo.t_KDT_VNACC_TK_SoVanDon
	ADD   LoaiDinhDanh VARCHAR(200) NULL;
GO

IF NOT EXISTS(
  SELECT TOP 1 1
  FROM INFORMATION_SCHEMA.COLUMNS
  WHERE 
    [TABLE_NAME] = 't_KDT_VNACC_TK_SoVanDon'
    AND [COLUMN_NAME] = 'NgayVanDon')
BEGIN
    ALTER TABLE dbo.t_KDT_VNACC_TK_SoVanDon
	ADD   NgayVanDon DATETIME NULL;
END
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoVanDonChu varchar(200),
	@NamVanDonChu varchar(200),
	@LoaiDinhDanh varchar(200),
	@NgayVanDon datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_SoVanDon]
(
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoVanDonChu],
	[NamVanDonChu],
	[LoaiDinhDanh],
	[NgayVanDon]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@SoVanDon,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@SoVanDonChu,
	@NamVanDonChu,
	@LoaiDinhDanh,
	@NgayVanDon
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoVanDonChu varchar(200),
	@NamVanDonChu varchar(200),
	@LoaiDinhDanh varchar(200),
	@NgayVanDon datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[SoVanDon] = @SoVanDon,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[SoVanDonChu] = @SoVanDonChu,
	[NamVanDonChu] = @NamVanDonChu,
	[LoaiDinhDanh] = @LoaiDinhDanh,
	[NgayVanDon] = @NgayVanDon
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoVanDonChu varchar(200),
	@NamVanDonChu varchar(200),
	@LoaiDinhDanh varchar(200),
	@NgayVanDon datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_SoVanDon] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[SoVanDon] = @SoVanDon,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[SoVanDonChu] = @SoVanDonChu,
			[NamVanDonChu] = @NamVanDonChu,
			[LoaiDinhDanh] = @LoaiDinhDanh,
			[NgayVanDon] = @NgayVanDon
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_SoVanDon]
		(
			[TKMD_ID],
			[SoTT],
			[SoVanDon],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[SoVanDonChu],
			[NamVanDonChu],
			[LoaiDinhDanh],
			[NgayVanDon]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@SoVanDon,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@SoVanDonChu,
			@NamVanDonChu,
			@LoaiDinhDanh,
			@NgayVanDon
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoVanDonChu],
	[NamVanDonChu],
	[LoaiDinhDanh],
	[NgayVanDon]
FROM
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoVanDonChu],
	[NamVanDonChu],
	[LoaiDinhDanh],
	[NgayVanDon]
FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoVanDonChu],
	[NamVanDonChu],
	[LoaiDinhDanh],
	[NgayVanDon]
FROM
	[dbo].[t_KDT_VNACC_TK_SoVanDon]	

GO

--------------- Cập nhật các Procedure dùng để xuất Excel----------------
GO
 IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_KhoCFS_DangKy_SelectDynamic') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_KhoCFS_DangKy_SelectDynamic
GO
------------ Cập nhật Procedure Kho CFS
-----------------------------------------------   
-- Stored procedure name: [dbo].[p_KDT_KhoCFS_DangKy_SelectDynamic]   
-- Database: ECS_TQDT_SXXK_V4_HT_2   
-----------------------------------------------   
   
CREATE PROCEDURE [dbo].[p_KDT_KhoCFS_DangKy_SelectDynamic]   
 @WhereCondition NVARCHAR(500),   
 @OrderByExpression NVARCHAR(250) = NULL   
AS   
   
SET NOCOUNT ON   
SET TRANSACTION ISOLATION LEVEL READ COMMITTED   
   
DECLARE @SQL NVARCHAR(MAX)   
   
SET @SQL =    
'SELECT    
 [ID],   
 [SoTiepNhan],   
 [NgayTiepNhan],   
 [MaHQ],   
 [MaKhoCFS],   
 [GuidStr],   
 [TrangThaiXuLy]   
FROM [dbo].[t_KDT_KhoCFS_DangKy] INNER JOIN dbo.t_KDT_KhoCFS_Details ON t_KDT_KhoCFS_DangKy.ID=t_KDT_KhoCFS_Details.Master_ID    
WHERE ' + @WhereCondition   
   
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0   
BEGIN   
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression   
END   
   
EXEC sp_executesql @SQL   
GO
ALTER TABLE t_VNACC_Category_Cargo DROP CONSTRAINT PK_t_KDT_VNACC_Cargo
GO
ALTER TABLE t_VNACC_Category_Cargo ALTER COLUMN BondedAreaCode [varchar] (250) NOT NULL
go
ALTER TABLE t_VNACC_Category_Cargo ALTER COLUMN BondedAreaName [nvarchar] (MAX) NOT NULL
go
GO

DELETE FROM t_VNACC_Category_Cargo WHERE TableID='A204A'
----------Cập nhật mã kho CFS khai báo 
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A204A','43CNC01_ARECO',N'CFS Areco - Cty CP Thương mại và Du lịch Bình Dương',N'9 ĐT743, Bình Thắng, An Bình, Dĩ An - Bình Dương')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A204A','43CNC01_Damco',N'CFS DAMCO -CTY TNHH Maersk Việt Nam',N'9 ĐT743, Bình Thắng, An Bình, Dĩ An - Bình Dương')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A204A','43NDC02',N'CTY TNHH Kerry Interrated Logistics VN (CFS)',N'20 Thống Nhất, KCN Sóng Thần, Bình Dương')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A204A','43IHC02',N'CTY TNHH MTV ICD Tân Cảng-Sóng Thần (CFS)',N'Kho 3, 5, 13, 14 ICD Sóng Thần, xã Bình Hòa, Thuận An, Bình Dương')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A204A','43NDC01',N'CTY CỔ PHẦN NAM LIÊN (CFS)',N'Đường Số 6, KCN Sóng Thần 1, Dĩ An,Bình Dương')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName,Notes) VALUES('A204A','47I1W06_ANHPHONG',N'Công ty TNHH Dịch vụ vận tải Anh Phong',N'Khu phố 1, phường Long Bình Tân, Tp. Biên Hòa, tỉnh Đồng Nai')
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamicFull') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamicFull
GO
CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamicFull]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT TKVC.ID ,
       TKVC.TKMD_ID ,
       TKVC.NgayDangKy ,
       TKVC.TenThongTinXuat ,
       TKVC.MaPhanLoaiXuLy ,
       TKVC.MaCoBaoNiemPhong ,
       TKVC.TenCoBaoNiemPhong ,
       TKVC.CoQuanHaiQuan ,
       TKVC.SoToKhaiVC ,
       TKVC.CoBaoXuatNhapKhau ,
       TKVC.NgayLapToKhai ,
       TKVC.MaNguoiKhai ,
       TKVC.TenNguoiKhai ,
       TKVC.DiaChiNguoiKhai ,
       TKVC.MaNguoiVC ,
       TKVC.TenNguoiVC ,
       TKVC.DiaChiNguoiVC ,
       TKVC.SoHopDongVC ,
       TKVC.NgayHopDongVC ,
       TKVC.NgayHetHanHopDongVC ,
       TKVC.MaPhuongTienVC ,
       TKVC.TenPhuongTieVC ,
       TKVC.MaMucDichVC ,
       TKVC.TenMucDichVC ,
       TKVC.LoaiHinhVanTai ,
       TKVC.TenLoaiHinhVanTai ,
       TKVC.MaDiaDiemXepHang ,
       TKVC.MaViTriXepHang ,
       TKVC.MaCangCuaKhauGaXepHang ,
       TKVC.MaCangXHKhongCo_HT ,
       TKVC.DiaDiemXepHang ,
       TKVC.NgayDenDiaDiem_XH ,
       TKVC.MaDiaDiemDoHang ,
       TKVC.MaViTriDoHang ,
       TKVC.MaCangCuaKhauGaDoHang ,
       TKVC.MaCangDHKhongCo_HT ,
       TKVC.DiaDiemDoHang ,
       TKVC.NgayDenDiaDiem_DH ,
       TKVC.TuyenDuongVC ,
       TKVC.LoaiBaoLanh ,
       TKVC.SoTienBaoLanh ,
       TKVC.SoLuongCot_TK ,
       TKVC.SoLuongContainer ,
       TKVC.MaNganHangBaoLanh ,
       TKVC.NamPhatHanhBaoLanh ,
       TKVC.KyHieuChungTuBaoLanh ,
       TKVC.SoChungTuBaoLanh ,
       TKVC.MaVach ,
       TKVC.NgayPheDuyetVC ,
       TKVC.NgayDuKienBatDauVC ,
       TKVC.GioDuKienBatDauVC ,
       TKVC.NgayDuKienKetThucVC ,
       TKVC.GioDuKienKetThucVC ,
       TKVC.MaBuuChinhHQ ,
       TKVC.DiaChiBuuChinhHQ ,
       TKVC.TenBuuChinhHQ ,
       TKVC.GhiChu ,
       TKVC.TrangThaiXuLy ,
       TKVC.InputMessageID ,
       TKVC.MessageTag ,
       TKVC.IndexTag ,
       t_KDT_VNACC_TKVC_VanDon.ID ,
       Master_ID ,
       SoTTVanDon ,
       SoVanDon ,
       NgayPhatHanhVD ,
       MoTaHangHoa ,
       MaHS ,
       KyHieuVaSoHieu ,
       NgayNhapKhoHQLanDau ,
       PhanLoaiSanPhan ,
       MaNuocSanXuat ,
       TenNuocSanXuat ,
       MaDiaDiemXuatPhatVC ,
       TenDiaDiemXuatPhatVC ,
       MaDiaDiemDichVC ,
       TenDiaDiemDichVC ,
       LoaiHangHoa ,
       t_KDT_VNACC_TKVC_VanDon.MaPhuongTienVC ,
       TenPhuongTienVC ,
       NgayHangDuKienDenDi ,
       MaNguoiNhapKhau ,
       TenNguoiNhapKhau ,
       DiaChiNguoiNhapKhau ,
       MaNguoiXuatKhua ,
       TenNguoiXuatKhau ,
       DiaChiNguoiXuatKhau ,
       MaNguoiUyThac ,
       TenNguoiUyThac ,
       DiaChiNguoiUyThac ,
       MaVanBanPhapLuat1 ,
       MaVanBanPhapLuat2 ,
       MaVanBanPhapLuat3 ,
       MaVanBanPhapLuat4 ,
       MaVanBanPhapLuat5 ,
       MaDVTTriGia ,
       TriGia ,
       SoLuong ,
       MaDVTSoLuong ,
       TongTrongLuong ,
       MaDVTTrongLuong ,
       TheTich ,
       MaDVTTheTich ,
       MaDanhDauDDKH1 ,
       MaDanhDauDDKH2 ,
       MaDanhDauDDKH3 ,
       MaDanhDauDDKH4 ,
       MaDanhDauDDKH5 ,
       SoGiayPhep ,
       NgayCapPhep ,
       NgayHetHanCapPhep ,
       t_KDT_VNACC_TKVC_VanDon.GhiChu FROM dbo.t_KDT_VNACC_ToKhaiVanChuyen TKVC INNER JOIN dbo.t_KDT_VNACC_TKVC_VanDon ON t_KDT_VNACC_TKVC_VanDon.Master_ID = TKVC.ID
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL

GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_VNACCS_TEA_SelectDynamicFull') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_VNACCS_TEA_SelectDynamicFull
GO
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectDynamicFull]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT t_KDT_VNACCS_TEA.ID ,
       SoDanhMucMienThue ,
       PhanLoaiXuatNhapKhau ,
       CoQuanHaiQuan ,
       DiaChiCuaNguoiKhai ,
       SDTCuaNguoiKhai ,
       ThoiHanMienThue ,
       TenDuAnDauTu ,
       DiaDiemXayDungDuAn ,
       MucTieuDuAn ,
       MaMienGiam ,
       PhamViDangKyDMMT ,
       NgayDuKienXNK ,
       GP_GCNDauTuSo ,
       NgayChungNhan ,
       CapBoi ,
       GhiChu ,
       CamKetSuDung ,
       MaNguoiKhai ,
       KhaiBaoTuNgay ,
       KhaiBaoDenNgay ,
       MaSoQuanLyDSMT ,
       PhanLoaiCapPhep ,
       TrangThaiXuLy ,
       t_KDT_VNACCS_TEA.InputMessageID ,
       t_KDT_VNACCS_TEA.MessageTag ,
       t_KDT_VNACCS_TEA.IndexTag ,
       t_KDT_VNACCS_TEA_HangHoa.ID ,
       Master_ID ,
       MoTaHangHoa ,
       SoLuongDangKyMT ,
       DVTSoLuongDangKyMT ,
       SoLuongDaSuDung ,
       DVTSoLuongDaSuDung ,
       TriGia ,
       TriGiaDuKien ,
       t_KDT_VNACCS_TEA_HangHoa.InputMessageID ,
       t_KDT_VNACCS_TEA_HangHoa.MessageTag ,
       t_KDT_VNACCS_TEA_HangHoa.IndexTag FROM dbo.t_KDT_VNACCS_TEA INNER JOIN dbo.t_KDT_VNACCS_TEA_HangHoa ON t_KDT_VNACCS_TEA_HangHoa.Master_ID = t_KDT_VNACCS_TEA.ID
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_VNACCS_TIA_SelectDynamicFull') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_VNACCS_TIA_SelectDynamicFull
GO
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectDynamicFull]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT t_KDT_VNACCS_TIA.ID ,
       SoToKhai ,
       CoBaoNhapKhauXuatKhau ,
       CoQuanHaiQuan ,
       MaNguoiKhaiDauTien ,
       TenNguoiKhaiDauTien ,
       MaNguoiXuatNhapKhau ,
       TenNguoiXuatNhapKhau ,
       ThoiHanTaiXuatNhap ,
       TrangThaiXuLy ,
       t_KDT_VNACCS_TIA.InputMessageID ,
       t_KDT_VNACCS_TIA.MessageTag ,
       t_KDT_VNACCS_TIA.IndexTag ,
       t_KDT_VNACCS_TIA_HangHoa.ID ,
       Master_ID ,
       MaSoHangHoa ,
       SoLuongBanDau ,
       DVTSoLuongBanDau ,
       SoLuongDaTaiNhapTaiXuat ,
       DVTSoLuongDaTaiNhapTaiXuat ,
       t_KDT_VNACCS_TIA_HangHoa.InputMessageID ,
       t_KDT_VNACCS_TIA_HangHoa.MessageTag ,
       t_KDT_VNACCS_TIA_HangHoa.IndexTag FROM dbo.t_KDT_VNACCS_TIA INNER JOIN dbo.t_KDT_VNACCS_TIA_HangHoa ON t_KDT_VNACCS_TIA_HangHoa.Master_ID = t_KDT_VNACCS_TIA.ID
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamicFull') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamicFull
GO
CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamicFull]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung.ID ,
       TKMD_ID ,
       SoToKhaiBoSung ,
       CoQuanHaiQuan ,
       NhomXuLyHoSo ,
       NhomXuLyHoSoID ,
       PhanLoaiXuatNhapKhau ,
       SoToKhai ,
       MaLoaiHinh ,
       NgayKhaiBao ,
       NgayCapPhep ,
       ThoiHanTaiNhapTaiXuat ,
       MaNguoiKhai ,
       TenNguoiKhai ,
       MaBuuChinh ,
       DiaChiNguoiKhai ,
       SoDienThoaiNguoiKhai ,
       MaLyDoKhaiBoSung ,
       MaTienTeTienThue ,
       MaNganHangTraThueThay ,
       NamPhatHanhHanMuc ,
       KiHieuChungTuPhatHanhHanMuc ,
       SoChungTuPhatHanhHanMuc ,
       MaXacDinhThoiHanNopThue ,
       MaNganHangBaoLanh ,
       NamPhatHanhBaoLanh ,
       KyHieuPhatHanhChungTuBaoLanh ,
       SoHieuPhatHanhChungTuBaoLanh ,
       MaTienTeTruocKhiKhaiBoSung ,
       TyGiaHoiDoaiTruocKhiKhaiBoSung ,
       MaTienTeSauKhiKhaiBoSung ,
       TyGiaHoiDoaiSauKhiKhaiBoSung ,
       SoQuanLyTrongNoiBoDoanhNghiep ,
       GhiChuNoiDungLienQuanTruocKhiKhaiBoSung ,
       GhiChuNoiDungLienQuanSauKhiKhaiBoSung ,
       SoTiepNhan ,
       NgayTiepNhan ,
       PhanLuong ,
       HuongDan ,
       Notes ,
       InputMessageID ,
       MessageTag ,
       IndexTag ,
       SoThongBao ,
       NgayHoanThanhKiemTra ,
       GioHoanThanhKiemTra ,
       NgayDangKyKhaiBoSung ,
       GioDangKyKhaiBoSung ,
       DauHieuBaoQua60Ngay ,
       MaHetThoiHan ,
       MaDaiLyHaiQuan ,
       TenDaiLyHaiQuan ,
       MaNhanVienHaiQuan ,
       PhanLoaiNopThue ,
       NgayHieuLucChungTu ,
       SoNgayNopThue ,
       SoNgayNopThueDanhChoVATHangHoaDacBiet ,
       HienThiTongSoTienTangGiamThueXuatNhapKhau ,
       TongSoTienTangGiamThueXuatNhapKhau ,
       MaTienTeTongSoTienTangGiamThueXuatNhapKhau ,
       TongSoTrangToKhaiBoSung ,
       TongSoDongHangToKhaiBoSung ,
       LyDo ,
       TenNguoiPhuTrach ,
       TenTruongDonViHaiQuan ,
       NgayDangKyDuLieu ,
       GioDangKyDuLieu ,
       TrangThaiXuLy ,
       t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue.ID ,
       TKMDBoSung_ID ,
       t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue.SoDong ,
       SoThuTuDongHangTrenToKhaiGoc ,
       MoTaHangHoaTruocKhiKhaiBoSung ,
       MoTaHangHoaSauKhiKhaiBoSung ,
       MaSoHangHoaTruocKhiKhaiBoSung ,
       MaSoHangHoaSauKhiKhaiBoSung ,
       MaNuocXuatXuTruocKhiKhaiBoSung ,
       MaNuocXuatXuSauKhiKhaiBoSung ,
       TriGiaTinhThueTruocKhiKhaiBoSung ,
       TriGiaTinhThueSauKhiKhaiBoSung ,
       SoLuongTinhThueTruocKhiKhaiBoSung ,
       SoLuongTinhThueSauKhiKhaiBoSung ,
       MaDonViTinhSoLuongTinhThueTruocKhaiBoSung ,
       MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung ,
       ThueSuatTruocKhiKhaiBoSung ,
       ThueSuatSauKhiKhaiBoSung ,
       SoTienThueTruocKhiKhaiBoSung ,
       SoTienThueSauKhiKhaiBoSung ,
       HienThiMienThueTruocKhiKhaiBoSung ,
       HienThiMienThueSauKhiKhaiBoSung ,
       HienThiSoTienTangGiamThueXuatNhapKhau ,
       SoTienTangGiamThue ,
       t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac.ID ,
       HMDBoSung_ID ,
       t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac.SoDong ,
       TriGiaTinhThueTruocKhiKhaiBoSungThuKhac ,
       TriGiaTinhThueSauKhiKhaiBoSungThuKhac ,
       SoLuongTinhThueTruocKhiKhaiBoSungThuKhac ,
       SoLuongTinhThueSauKhiKhaiBoSungThuKhac ,
       MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac ,
       MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac ,
       MaApDungThueSuatTruocKhiKhaiBoSungThuKhac ,
       MaApDungThueSuatSauKhiKhaiBoSungThuKhac ,
       ThueSuatTruocKhiKhaiBoSungThuKhac ,
       ThueSuatSauKhiKhaiBoSungThuKhac ,
       SoTienThueTruocKhiKhaiBoSungThuKhac ,
       SoTienThueSauKhiKhaiBoSungThuKhac ,
       HienThiMienThueVaThuKhacTruocKhiKhaiBoSung ,
       HienThiMienThueVaThuKhacSauKhiKhaiBoSung ,
       HienThiSoTienTangGiamThueVaThuKhac ,
       SoTienTangGiamThuKhac FROM dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung INNER JOIN dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue ON t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue.TKMDBoSung_ID = t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung.ID INNER JOIN dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac ON t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac.HMDBoSung_ID = dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue.ID
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectDynamicFull') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectDynamicFull
GO
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_SelectDynamicFull]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP.ID ,
       TrangThaiXuLy ,
       SoTN ,
       NgayTN ,
       MaHQ ,
       MaDoanhNghiep ,
       NamQuyetToan ,
       LoaiHinhBaoCao ,
       DVTBaoCao ,
       GhiChuKhac ,
       FileName ,
       Content ,
       GuidStr ,
       t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details.ID ,
       GoodItem_ID ,
       STT ,
       LoaiHangHoa ,
       TaiKhoan ,
       TenHangHoa ,
       MaHangHoa ,
       DVT ,
       TonDauKy ,
       NhapTrongKy ,
       XuatTrongKy ,
       TonCuoiKy ,
       GhiChu FROM dbo.t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP INNER JOIN dbo.t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details ON t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details.GoodItem_ID = t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP.ID
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL
GO
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_InsertUpdate]  
-- Database: ECS_TQDT_KD_V5  
-- Author: Ngo Thanh Tung  
-- Time created: Monday, November 11, 2013  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_VNACC_Category_Common_InsertUpdate]  
 @ID int,  
 @ReferenceDB varchar(5),  
 @Code varchar(10),  
 @Name_VN nvarchar(max),  
 @Name_EN varchar(max),  
 @Notes nvarchar(250),  
 @InputMessageID varchar(10),  
 @MessageTag varchar(26),  
 @IndexTag varchar(100)  
AS  
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Common] WHERE [ID] = @ID)  
 BEGIN  
  UPDATE  
   [dbo].[t_VNACC_Category_Common]   
  SET  
   [ReferenceDB] = @ReferenceDB,  
   [Code] = @Code,  
   [Name_VN] = @Name_VN,  
   [Name_EN] = @Name_EN,  
   [Notes] = @Notes,  
   [InputMessageID] = @InputMessageID,  
   [MessageTag] = @MessageTag,  
   [IndexTag] = @IndexTag  
  WHERE  
   [ID] = @ID  
 END  
ELSE  
 BEGIN  
    
  INSERT INTO [dbo].[t_VNACC_Category_Common]  
  (  
   [ReferenceDB],  
   [Code],  
   [Name_VN],  
   [Name_EN],  
   [Notes],  
   [InputMessageID],  
   [MessageTag],  
   [IndexTag]  
  )  
  VALUES   
  (  
   @ReferenceDB,  
   @Code,  
   @Name_VN,  
   @Name_EN,  
   @Notes,  
   @InputMessageID,  
   @MessageTag,  
   @IndexTag  
  )    
 END      
GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '34.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('34.9',GETDATE(), N'CẬP NHẬT KHAI BÁO VẬN ĐƠN VÀ PROCEDURE XUẤT EXCEL')
END