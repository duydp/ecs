using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class ThongTinToKhaiPhoThong : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongTinToKhaiPhoThong()
        {
            InitializeComponent();
        }

        public void BindingReport(VAL2800 val2800)
        {
            lblSoToKhai.Text = val2800.JNO.GetValue().ToString();
            lblPhanLoaiKhaiBao.Text = val2800.TSB.GetValue().ToString();
            lblTenThuTucKB.Text = val2800.TNM.GetValue().ToString();
            lblCoQuanHQ.Text = val2800.CH.GetValue().ToString();
            lblNhomXuLyHS.Text = val2800.CHB.GetValue().ToString();
            if (Convert.ToDateTime(val2800.YMD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(val2800.YMD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            lblTenNguoiKB.Text = val2800.SNM.GetValue().ToString();
            lblDiaChiNguoiKB.Text = val2800.SAD.GetValue().ToString();
            lblSoDienThoaiNguoiKB.Text = val2800.STL.GetValue().ToString();
            lblSoDeLayTepDinhKem.Text = val2800.STA.GetValue().ToString();
            lblSoQuanLyNoiBo.Text = val2800.REF.GetValue().ToString();
            lblGhiChu.Text = val2800.KIJ.GetValue().ToString();


        }
    }
}
