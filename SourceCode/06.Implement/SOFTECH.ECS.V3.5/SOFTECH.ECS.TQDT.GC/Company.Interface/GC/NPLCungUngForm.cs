using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class NPLCungUngForm : BaseForm
    {
        public long TKMD_ID = 0;
        private ToKhaiMauDich TKMD;
        private DataSet ds = new DataSet();
        public NPLCungUngForm()
        {
            InitializeComponent();
        }

        private void NPLCungUngForm_Load(object sender, EventArgs e)
        {          
            TKMD= new ToKhaiMauDich();  
            TKMD.ID = TKMD_ID;
            TKMD.Load(); 
            BindData();
        }

        private void BindData()
        {
            this.ds = new ToKhaiMauDich().GetNPLCungUngTK(this.TKMD_ID);
            dgList.DataSource = this.ds.Tables[0];
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                Company.GC.BLL.GC.NguyenPhuLieu NPL=new Company.GC.BLL.GC.NguyenPhuLieu();
                NPL.HopDong_ID=TKMD.IDHopDong;
                NPL.Ma= e.Row.Cells["MaNguyenPhuLieu"].Text.Trim();
                NPL.Load();
                e.Row.Cells["TenNPL"].Text = NPL.Ten;
            }
        }

        private void menuExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách Nguyên phụ liệu cung ứng của Tờ khai xuất _" + TKMD.LoaiVanDon + "_" + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgList;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình mở File" + ex.Message, false);
                }
            }
            else
            {
                ShowMessage("Xuất Excel không thành công", false);
            }
        }

    }
}