﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.SXXK
{
    public partial class MsgSend : ICloneable
	{
		#region Properties.
		
		public long master_id { set; get; }
		public int func { set; get; }
		public string LoaiHS { set; get; }
		public string msg { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<MsgSend> ConvertToCollection(IDataReader reader)
		{
			List<MsgSend> collection = new List<MsgSend>();
			while (reader.Read())
			{
				MsgSend entity = new MsgSend();
				if (!reader.IsDBNull(reader.GetOrdinal("master_id"))) entity.master_id = reader.GetInt64(reader.GetOrdinal("master_id"));
				if (!reader.IsDBNull(reader.GetOrdinal("func"))) entity.func = reader.GetInt32(reader.GetOrdinal("func"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHS"))) entity.LoaiHS = reader.GetString(reader.GetOrdinal("LoaiHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("msg"))) entity.msg = reader.GetString(reader.GetOrdinal("msg"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}

        public bool Load()
        {
            const string spName = "[dbo].[p_KDT_SXXK_MsgSend_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, master_id);
            db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, LoaiHS);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<MsgSend> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return true;
            }
            return false;
        }
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_MsgSend VALUES(@master_id, @func, @LoaiHS, @msg)";
            string update = "UPDATE t_MsgSend SET master_id = @master_id, func = @func, LoaiHS = @LoaiHS, msg = @msg WHERE ID = @ID";
            string delete = "DELETE FROM t_MsgSend WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@master_id", SqlDbType.BigInt, "master_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@func", SqlDbType.Int, "func", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHS", SqlDbType.VarChar, "LoaiHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@msg", SqlDbType.NVarChar, "msg", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@master_id", SqlDbType.BigInt, "master_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@func", SqlDbType.Int, "func", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHS", SqlDbType.VarChar, "LoaiHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@msg", SqlDbType.NVarChar, "msg", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@master_id", SqlDbType.BigInt, "master_id", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@LoaiHS", SqlDbType.VarChar, "LoaiHS", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_MsgSend VALUES(@master_id, @func, @LoaiHS, @msg)";
            string update = "UPDATE t_MsgSend SET master_id = @master_id, func = @func, LoaiHS = @LoaiHS, msg = @msg WHERE ID = @ID";
            string delete = "DELETE FROM t_MsgSend WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@master_id", SqlDbType.BigInt, "master_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@func", SqlDbType.Int, "func", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHS", SqlDbType.VarChar, "LoaiHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@msg", SqlDbType.NVarChar, "msg", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@master_id", SqlDbType.BigInt, "master_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@func", SqlDbType.Int, "func", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHS", SqlDbType.VarChar, "LoaiHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@msg", SqlDbType.NVarChar, "msg", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@master_id", SqlDbType.BigInt, "master_id", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@LoaiHS", SqlDbType.VarChar, "LoaiHS", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static MsgSend Load(long master_id, string loaiHS)
		{
			const string spName = "[dbo].[p_KDT_SXXK_MsgSend_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, master_id);
			db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, loaiHS);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<MsgSend> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<MsgSend> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<MsgSend> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_MsgSend_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_MsgSend_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertMsgSend(long master_id, int func, string loaiHS, string msg)
		{
			MsgSend entity = new MsgSend();	
			entity.master_id = master_id;
			entity.func = func;
			entity.LoaiHS = loaiHS;
			entity.msg = msg;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_MsgSend_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, master_id);
			db.AddInParameter(dbCommand, "@func", SqlDbType.Int, func);
			db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, LoaiHS);
			db.AddInParameter(dbCommand, "@msg", SqlDbType.NVarChar, msg);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<MsgSend> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MsgSend item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateMsgSend(long master_id, int func, string loaiHS, string msg)
		{
			MsgSend entity = new MsgSend();			
			entity.master_id = master_id;
			entity.func = func;
			entity.LoaiHS = loaiHS;
			entity.msg = msg;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_MsgSend_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, master_id);
			db.AddInParameter(dbCommand, "@func", SqlDbType.Int, func);
			db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, LoaiHS);
			db.AddInParameter(dbCommand, "@msg", SqlDbType.NVarChar, msg);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<MsgSend> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MsgSend item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateMsgSend(long master_id, int func, string loaiHS, string msg)
		{
			MsgSend entity = new MsgSend();			
			entity.master_id = master_id;
			entity.func = func;
			entity.LoaiHS = loaiHS;
			entity.msg = msg;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_MsgSend_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, master_id);
			db.AddInParameter(dbCommand, "@func", SqlDbType.Int, func);
			db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, LoaiHS);
			db.AddInParameter(dbCommand, "@msg", SqlDbType.NVarChar, msg);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<MsgSend> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MsgSend item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteMsgSend(long master_id, string loaiHS)
		{
			MsgSend entity = new MsgSend();
			entity.master_id = master_id;
			entity.LoaiHS = loaiHS;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_MsgSend_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, master_id);
			db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, LoaiHS);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_MsgSend_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<MsgSend> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MsgSend item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}