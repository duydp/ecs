using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class PhuongThucThanhToan : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_PhuongThucThanhToan ORDER BY ID";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }
        //Lypt update date 18/01/2010
        public static string getName(string id)
        {
            //string query = "SELECT GhiChu FROM t_HaiQuan_PhuongThucThanhToan where id='" + id + "'";
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //return db.ExecuteScalar(dbCommand).ToString();

            try
            {
                string query = string.Format("[ID] = '{0}'", id);

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["PhuongThucThanhToan"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["GhiChu"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(id, ex); }

            return string.Empty;
        }
        //
        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_PhuongThucThanhToan VALUES(@Id,@GhiChu,@DateCreated,@DateModified)";
            string update = "UPDATE t_HaiQuan_PhuongThucThanhToan SET GhiChu = @GhiChu, DateModified = @DateModified WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_PhuongThucThanhToan WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}