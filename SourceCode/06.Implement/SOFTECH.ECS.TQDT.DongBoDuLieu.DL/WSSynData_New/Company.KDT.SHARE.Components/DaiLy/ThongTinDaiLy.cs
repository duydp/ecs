﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public class ThongTinDaiLy
    {
        public string MaDL { get; set; }
        public string TenDL { get; set; }
        public string DiaChiDL { get; set; }
        public string SoDienThoaiDL { get; set; }
        public string NguoiLienHeDL { get; set; }
        public string EmailDL { get; set; }
        public string FaxDL { get; set; }


    }
}
