﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class BoSungHoaDonTM:DeclarationBase
    {
       [XmlElement("DeclarationDocument")]
       public DeclarationBase DeclarationDocument { get; set; }
       [XmlArray("CommercialInvoices")]
       [XmlArrayItem("CommercialInvoice")]
       public List<CommercialInvoice> CommercialInvoices { get; set; }
    }
}
