using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.Interface;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using DevExpress.XtraPrinting;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting;
#endif

namespace Company.Interface.Report
{
    public partial class ReportViewTKXTQDTFormTT196New : DevExpress.XtraEditors.XtraForm
    {
        public Company.Interface.Report.TQDTToKhaiXK_TT196 ToKhaiChinhReport = new Company.Interface.Report.TQDTToKhaiXK_TT196();
        public Company.Interface.Report.TQDTToKhaiXK_TaiCho_TT196 ToKhaiChinhTaiChoReport = new Company.Interface.Report.TQDTToKhaiXK_TaiCho_TT196();
        public Company.Interface.Report.TQDTPhuLucToKhaiXuat_TT196 PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiXuat_TT196();
        public Company.Interface.Report.TQDTPhuLucToKhaiXuat_TaiCho_TT196 PhuLucTaiChoReport = new Company.Interface.Report.TQDTPhuLucToKhaiXuat_TaiCho_TT196();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public bool InMaHang;
        public bool InTrigiaTT;
        public bool InTriGiaTTBVMT;
        int index = 0;
        private int soDongHang = 7;
        public List<ItemComboBoxRibbon> listBanIn = new List<ItemComboBoxRibbon>();
        private DevExpress.XtraPrinting.Native.ReflectorBar reflectorBar;
        private ProgressBarControl VisualProcessBar = new ProgressBarControl();
        public bool IsToKhaiXuatTaiCho { get; set; }
        string SoHoaDonGTGT = "";
        string NgayHoaDonGTGT = "";
        public ReportViewTKXTQDTFormTT196New(bool isToKhaiXuatTaiCho )
        {
            InitializeComponent();
            chkInMaHang_edit.CheckedChanged += new EventHandler(chkInMaHang_edit_CheckedChanged);
            chkInTriGiaTinhThue_edit.CheckedChanged += new EventHandler(chkInTriGiaTinhThue_edit_CheckedChanged);
            reflectorBar = new DevExpress.XtraPrinting.Native.ReflectorBar(VisualProcessBar);
            VisualProcessBar.PositionChanged += new EventHandler(VisualProcessBar_PositionChanged);
            btnExportAll_PDF.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ExportAll_Pdf_ItemClick);
            btnExportAll_XLSX.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ExportAll_Excel_ItemClick);
            IsToKhaiXuatTaiCho = isToKhaiXuatTaiCho;
            PGThongTinToKhaiTaiCho.Visible = isToKhaiXuatTaiCho;
        }

        void VisualProcessBar_PositionChanged(object sender, EventArgs e)
        {
            progressBarEditItem1.EditValue = VisualProcessBar.Position;
            if (VisualProcessBar.Position == 100 || VisualProcessBar.Position == 0)
                progressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            else
                progressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;

        }
        void chkInTriGiaTinhThue_edit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit editCheck = (CheckEdit)sender;
            InTrigiaTT = editCheck.Checked;
            cmbToKhai_EditValueChanged(null, null);
        }

        void chkInMaHang_edit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit editCheck = (CheckEdit)sender;
            InMaHang = editCheck.Checked;
            cmbToKhai_EditValueChanged(null, null);
        }
    
        private void ReportViewBC03_TT117NewForm_Load(object sender, EventArgs e)
        {
            try
            {
                
                listBanIn.Add(new ItemComboBoxRibbon("Tờ khai chính", 1, ItemComboBoxRibbon.TypeItems.TK));
                //DATLMQ bổ sung thiết lập margin: 25/02/2011
//                 this.ToKhaiChinhReport.Margins.Top = 0;
//                 this.ToKhaiChinhReport.Margins.Bottom = 0;
//                 this.PhuLucReport.Margins.Top = 0;
//                 this.PhuLucReport.Margins.Bottom = 0;
                int soContainer;
                if (this.TKMD.VanTaiDon != null)
                    soContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
                else
                    soContainer = 0;
                if (this.TKMD.HMDCollection.Count > 3 || soContainer > 4)
                {
                    int countHang = (this.TKMD.HMDCollection.Count - 1) / 7 + 1;
                    int countContainer = (soContainer - 1) / 4;
                    int count;
                    if (GlobalSettings.IsKhongDungBangKeCont)
                        count = countHang >= countContainer ? countHang : countContainer;
                    else
                    {
                        if (this.TKMD.HMDCollection.Count > 3)
                            count = countHang;
                        else
                            count = 0;
                    }
                    for (int i = 0; i < count; i++)
                        listBanIn.Add(new ItemComboBoxRibbon("Phụ lục tờ khai số " + (i + 1), (i + 1), ItemComboBoxRibbon.TypeItems.PL));
                }
                if (this.TKMD.VanTaiDon != null && this.TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count > 4)
                    listBanIn.Add(new ItemComboBoxRibbon("Bảng kê container", "", ItemComboBoxRibbon.TypeItems.BangKeCont));
                listBanIn.Add(new ItemComboBoxRibbon("-<CHỌN TẤT CẢ>-", ItemComboBoxRibbon.TypeItems.All));
                InMaHang = Convert.ToBoolean(chkInMaHang.EditValue);
                InTrigiaTT = Convert.ToBoolean(chkInTriGiaTinhThue.EditValue);
                cmbToKhaiEdit.DataSource = listBanIn;
                cmbToKhaiEdit.DisplayMember = "Caption";
                cmbToKhaiEdit.PopulateColumns();
                cmbToKhaiEdit.Columns["ValueItem"].Visible = false;
                cmbToKhaiEdit.Columns["Type"].Visible = false;
                cmbToKhai.EditValue = listBanIn[0];
                this.WindowState = FormWindowState.Maximized;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        
        }
       
        private void cmbToKhai_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                ItemComboBoxRibbon item = (ItemComboBoxRibbon)cmbToKhai.EditValue;
                if (item == null) return;
                //                 if (char.IsDigit(txtSoDongHang.Text, 0))
                //                 {
                //                     soDongHang = Convert.ToInt32(txtSoDongHang.Text);
                //                 }
                //                 else
                //                 {
                //                     Globals.ShowMessage("Số dòng hàng không phải là kiểu số.", false);
                //                     txtSoDongHang.Focus();
                //                     return;
                //                 }

                if (item.Type == ItemComboBoxRibbon.TypeItems.TK)
                {
                    if (IsToKhaiXuatTaiCho)
                    {
                        this.ToKhaiChinhTaiChoReport = (TQDTToKhaiXK_TaiCho_TT196)GetToKhaiChinh();
                        printControl1.PrintingSystem = ToKhaiChinhTaiChoReport.PrintingSystem;
                        this.ToKhaiChinhTaiChoReport.CreateDocument();
                    }
                    else
                    {
                        this.ToKhaiChinhReport = (TQDTToKhaiXK_TT196)GetToKhaiChinh();
                        printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                        this.ToKhaiChinhReport.CreateDocument();
                    }
                }
                else if(item.Type == ItemComboBoxRibbon.TypeItems.PL)
                {

                    try
                    {
                        index = (int)item.ValueItem;
                    }
                    catch { index = 1; }
//                     txtTenNhomHang.Text = "";
//                     txtDeXuatKhac.Text = "";
                    if (IsToKhaiXuatTaiCho)
                    {
                        PhuLucTaiChoReport = (TQDTPhuLucToKhaiXuat_TaiCho_TT196)GetPhuLuc();
                        printControl1.PrintingSystem = PhuLucTaiChoReport.PrintingSystem;
                        this.PhuLucTaiChoReport.CreateDocument();
                    }
                    else
                    {
                        PhuLucReport = (TQDTPhuLucToKhaiXuat_TT196) GetPhuLuc();
                        printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                        this.PhuLucReport.CreateDocument();
                    }

                }
                else if (item.Type == ItemComboBoxRibbon.TypeItems.BangKeCont)
                {
                    Company.Interface.Report.SXXK.BangkeSoContainer_TT196 f = new Company.Interface.Report.SXXK.BangkeSoContainer_TT196();
                    f.tkmd = TKMD;
                    f.BindReport();
                    printControl1.PrintingSystem = f.PrintingSystem;
                    f.PrintingSystem.ProgressReflector = reflectorBar;
                    f.CreateDocument(true);
                }
                else if (item.Type == ItemComboBoxRibbon.TypeItems.All)
                {
                    GetAllReport();
                }
                
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private object GetToKhaiChinh()
        {
            TQDTToKhaiXK_TT196 TKChinh;
            TQDTToKhaiXK_TaiCho_TT196 TKTaiCho;
            try
            {
                if (IsToKhaiXuatTaiCho)
                {
                    TKTaiCho = new TQDTToKhaiXK_TaiCho_TT196();
                    TKTaiCho.TKMD = this.TKMD;
                    //this.ToKhaiChinhReport.report = this;
                    TKTaiCho.inMaHang = InMaHang;
                    TKTaiCho.inTriGiaTT = InTrigiaTT;
                    TKTaiCho.SoHoaDonGTGT = SoHoaDonGTGT;
                    TKTaiCho.NgayHoaDonGTGT = NgayHoaDonGTGT;
                    TKTaiCho.BindReport();
                    TKTaiCho.PrintingSystem.ProgressReflector = reflectorBar;
                    return TKTaiCho;
                }
                else
                {
                    TKChinh = new TQDTToKhaiXK_TT196();
                    TKChinh.TKMD = this.TKMD;
                    //this.ToKhaiChinhReport.report = this;
                    TKChinh.inMaHang = InMaHang;
                    TKChinh.inTriGiaTT = InTrigiaTT;
                    TKChinh.BindReport();
                    TKChinh.PrintingSystem.ProgressReflector = reflectorBar;
                    return TKChinh;
                }
                
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
            
        }

        private object GetPhuLuc()
        {
            TQDTPhuLucToKhaiXuat_TT196 PhuLuc ;
            TQDTPhuLucToKhaiXuat_TaiCho_TT196 PhuLucTaiCho;
            try
            {
                List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                //int begin = (cboToKhai.SelectedIndex - 1) * 9; //Comment by Hungtq
                //int end = cboToKhai.SelectedIndex * 9;  //Comment by Hungtq
                int begin = (index - 1) * 7;
                int end = index * 7;
                //edit by Khanhhn
                if (begin <= this.TKMD.HMDCollection.Count - 1 && this.TKMD.HMDCollection.Count > 3)
                {
                    if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                    for (int i = begin; i < end; i++)
                        HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                }
                List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
                int beginContainer = (index) * 4;
                int endContainer = (index + 1) * 4;
                if (this.TKMD.VanTaiDon != null && this.TKMD.VanTaiDon.ContainerCollection.Count > 0)
                {
                    if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count - 1)
                    {
                        if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
                            endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
                        for (int j = beginContainer; j < endContainer; j++)
                            ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
                    }
                }
                if (IsToKhaiXuatTaiCho)
                {
                    PhuLucTaiCho = new Company.Interface.Report.TQDTPhuLucToKhaiXuat_TaiCho_TT196();
                    //this.PhuLucReport.report = this;
                    PhuLucTaiCho.TKMD = this.TKMD;
                    if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                        PhuLucTaiCho.NgayDangKy = this.TKMD.NgayDangKy;
                    PhuLucTaiCho.ContainerCo = ContainerReportCo;
                    PhuLucTaiCho.HMDCollection = HMDReportCollection;
                    PhuLucTaiCho.soDongHang = soDongHang;
                    PhuLucTaiCho.inMaHang = InMaHang;
                    PhuLucTaiCho.inTriGiaTT = InTrigiaTT;
                    PhuLucTaiCho.BindReport(index.ToString());
                    PhuLucTaiCho.PrintingSystem.ProgressReflector = reflectorBar;
                    return PhuLucTaiCho;
                }
                else
                {

                    PhuLuc = new Company.Interface.Report.TQDTPhuLucToKhaiXuat_TT196();
                    //this.PhuLucReport.report = this;
                    PhuLuc.TKMD = this.TKMD;
                    if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                        PhuLuc.NgayDangKy = this.TKMD.NgayDangKy;
                    PhuLuc.ContainerCo = ContainerReportCo;
                    PhuLuc.HMDCollection = HMDReportCollection;
                    PhuLuc.soDongHang = soDongHang;
                    PhuLuc.inMaHang = InMaHang;
                    PhuLuc.inTriGiaTT = InTrigiaTT;
                    PhuLuc.BindReport(index.ToString());
                    PhuLuc.PrintingSystem.ProgressReflector = reflectorBar;
                    return PhuLuc;
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void GetAllReport()
        {
            try
            {
                printingSystem1 = new PrintingSystem();
                printingSystem1.ContinuousPageNumbering = true;
                printingSystem1.ProgressReflector = reflectorBar;
                foreach (ItemComboBoxRibbon item in listBanIn)
                {

                    if (item.Type == ItemComboBoxRibbon.TypeItems.TK)
                    {
                        if (IsToKhaiXuatTaiCho)
                        {
                            this.ToKhaiChinhTaiChoReport = (TQDTToKhaiXK_TaiCho_TT196)GetToKhaiChinh();
                            this.ToKhaiChinhTaiChoReport.CreateDocument();
                            printingSystem1.Pages.AddRange(this.ToKhaiChinhTaiChoReport.Pages);
                        }
                        else
                        {
                            this.ToKhaiChinhReport = (TQDTToKhaiXK_TT196)GetToKhaiChinh();
                            this.ToKhaiChinhReport.CreateDocument();
                            printingSystem1.Pages.AddRange(this.ToKhaiChinhReport.Pages);
                        }
                    }
                    else if (item.Type == ItemComboBoxRibbon.TypeItems.PL)
                    {

                        try
                        {
                            index = (int)item.ValueItem;
                        }
                        catch { index = 1; }
                        //                     txtTenNhomHang.Text = "";
                        if (IsToKhaiXuatTaiCho)
                        {
                            PhuLucTaiChoReport = (TQDTPhuLucToKhaiXuat_TaiCho_TT196)GetPhuLuc();
                            this.PhuLucTaiChoReport.CreateDocument();
                            printingSystem1.Pages.AddRange(this.PhuLucTaiChoReport.Pages);
                        }
                        else
                        {
                            PhuLucReport = (TQDTPhuLucToKhaiXuat_TT196)GetPhuLuc();
                            this.PhuLucReport.CreateDocument();
                            printingSystem1.Pages.AddRange(this.PhuLucReport.Pages);
                        }
                    }
                    else if (item.Type == ItemComboBoxRibbon.TypeItems.BangKeCont)
                    {
                        Company.Interface.Report.SXXK.BangkeSoContainer_TT196 f = new Company.Interface.Report.SXXK.BangkeSoContainer_TT196();
                        f.tkmd = TKMD;
                        f.BindReport();
                        f.CreateDocument();
                        printingSystem1.Pages.AddRange(f.Pages);
                    }
                }
                printControl1.PrintingSystem = printingSystem1;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #region Export All
        void ExportAll_Pdf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "File PDF |*.pdf";
                save.CheckPathExists = true;
                save.ShowDialog(this);
                if (!string.IsNullOrEmpty(save.FileName))
                {
                    PdfExportOptions pdfOp = new PdfExportOptions();
                    printControl1.PrintingSystem.ExportToPdf(save.FileName, pdfOp);
                    System.Diagnostics.Process.Start(save.FileName);
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        void ExportAll_Excel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string thongBao = "Luu ý: \r\n";
                thongBao += "         Doanh nghiệp chịu hoàn toàn trách nhiệm trước pháp luật khi thay đổi nội dung tờ khai trên file Excel.";
                if (new BaseForm().ShowMessage(thongBao, true) == "Yes")
                {
                    Company.KDT.SHARE.Components.Globals.SaveMessage("Xuat Excel", TKMD.ID, "Xuất tờ khai excel", thongBao);
                    SaveFileDialog save = new SaveFileDialog();
                    save.Filter = "Excel 2007 -> 2013 |*.xlsx";
                    save.CheckPathExists = true;
                    save.ShowDialog(this);
                    if (!string.IsNullOrEmpty(save.FileName))
                    {
                        XlsxExportOptions xlsxOp = new XlsxExportOptions();
                        xlsxOp.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        printControl1.PrintingSystem.ExportToXlsx(save.FileName, xlsxOp);

                        #region Đổi tên sheet
//                         try
//                         {
//                             Infragistics.Excel.Workbook wb = new Infragistics.Excel.Workbook();
//                             wb = Infragistics.Excel.Workbook.Load(save.FileName);
//                             for (int i = 0; i < wb.Worksheets.Count - 1; i++)
//                             {
//                                 wb.Worksheets[i].Name = listBanIn[i].Caption;
//                             }
//                             wb.Save(save.FileName);
//                         }
//                         catch (System.Exception ex)
//                         {
//                             Logger.LocalLogger.Instance().WriteMessage(ex);
//                         }
                        #endregion

                        System.Diagnostics.Process.Start(save.FileName);
                    }
                }

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        #endregion

        private void btnApDungHoaDonGTGT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SoHoaDonGTGT = txtSoHoaDonGTGT.EditValue == null ? string.Empty : txtSoHoaDonGTGT.EditValue.ToString();
            NgayHoaDonGTGT = Convert.ToDateTime(txtNgayHoaDonGTGT.EditValue).ToString("dd/MM/yyyy");
            cmbToKhai_EditValueChanged(null, null);
        }

    }

}