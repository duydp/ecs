﻿namespace Company.Interface
{
    partial class VNACC_GiayPhepForm_SEA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhepForm_SEA));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChiThiHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHaiQuan");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdlayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdlayPhanHoi");
            this.cmdChiThiHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHaiQuan");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdToolBar = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucChucNangChungTu = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtLoaiGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cboLoaiHinhXNK = new Janus.Windows.EditControls.UIComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTenDonViCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoGiayPhepKQXL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtSoDonXinCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dtHieuLucDenNgayKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtHieuLucTuNgayKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayCapKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayKhaiBao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMaKQXL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtMaDonViCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.pnlHoSoKemTheo = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.txt10 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txt9 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txt8 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txt7 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txt6 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txt5 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txt4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txt3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txt2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txt1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucCuaKhauXuatNhap = new Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap();
            this.ucPhuongTienVanChuyen = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucMaQuocGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoGiayChungNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtQuyetDinhSo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtNgayCap = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMaBuuChinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNoiCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEmail = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtFax = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDienThoai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label38 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.clcTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label24 = new System.Windows.Forms.Label();
            this.clcNgay_HD_Ban = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label23 = new System.Windows.Forms.Label();
            this.clcNgay_HD_Mua = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDaiDienDonViCapGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenGiamDoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtHoSoLienQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuCoQuanCapGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblHoSoKemTheo = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtSoHopDongMua = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtSoHopDongBan = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).BeginInit();
            this.cmdToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.pnlHoSoKemTheo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 786), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 786);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 762);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 762);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(757, 786);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdlayPhanHoi,
            this.cmdChiThiHaiQuan});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.cmdToolBar;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdLuu1,
            this.Separator1,
            this.cmdKhaiBao1,
            this.Separator2,
            this.cmdChiThiHaiQuan1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(963, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdChiThiHaiQuan1
            // 
            this.cmdChiThiHaiQuan1.Key = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan1.Name = "cmdChiThiHaiQuan1";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdlayPhanHoi
            // 
            this.cmdlayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdlayPhanHoi.Icon")));
            this.cmdlayPhanHoi.Key = "cmdlayPhanHoi";
            this.cmdlayPhanHoi.Name = "cmdlayPhanHoi";
            this.cmdlayPhanHoi.Text = "Lấy phản hồi";
            // 
            // cmdChiThiHaiQuan
            // 
            this.cmdChiThiHaiQuan.Image = ((System.Drawing.Image)(resources.GetObject("cmdChiThiHaiQuan.Image")));
            this.cmdChiThiHaiQuan.Key = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan.Name = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan.Text = "Chỉ thị Hải quan";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmdToolBar
            // 
            this.cmdToolBar.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdToolBar.CommandManager = this.cmbMain;
            this.cmdToolBar.Controls.Add(this.uiCommandBar1);
            this.cmdToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmdToolBar.Name = "cmdToolBar";
            this.cmdToolBar.Size = new System.Drawing.Size(963, 32);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ucChucNangChungTu);
            this.uiGroupBox1.Controls.Add(this.txtLoaiGiayPhep);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.cboLoaiHinhXNK);
            this.uiGroupBox1.Controls.Add(this.label45);
            this.uiGroupBox1.Controls.Add(this.label44);
            this.uiGroupBox1.Controls.Add(this.label43);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.txtTenDonViCapPhep);
            this.uiGroupBox1.Controls.Add(this.txtTenNguoiKhai);
            this.uiGroupBox1.Controls.Add(this.txtMaNguoiKhai);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.txtSoGiayPhepKQXL);
            this.uiGroupBox1.Controls.Add(this.label42);
            this.uiGroupBox1.Controls.Add(this.txtSoDonXinCapPhep);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.dtHieuLucDenNgayKQXL);
            this.uiGroupBox1.Controls.Add(this.dtHieuLucTuNgayKQXL);
            this.uiGroupBox1.Controls.Add(this.dtNgayCapKQXL);
            this.uiGroupBox1.Controls.Add(this.dtNgayKhaiBao);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.txtMaKQXL);
            this.uiGroupBox1.Controls.Add(this.label41);
            this.uiGroupBox1.Controls.Add(this.txtMaDonViCapPhep);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(757, 183);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ucChucNangChungTu
            // 
            this.ucChucNangChungTu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E025;
            this.ucChucNangChungTu.Code = "";
            this.ucChucNangChungTu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucChucNangChungTu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucChucNangChungTu.IsValidate = true;
            this.ucChucNangChungTu.Location = new System.Drawing.Point(398, 38);
            this.ucChucNangChungTu.Name = "ucChucNangChungTu";
            this.ucChucNangChungTu.Name_VN = "";
            this.ucChucNangChungTu.SetValidate = false;
            this.ucChucNangChungTu.ShowColumnCode = true;
            this.ucChucNangChungTu.ShowColumnName = true;
            this.ucChucNangChungTu.Size = new System.Drawing.Size(100, 26);
            this.ucChucNangChungTu.TabIndex = 5;
            this.ucChucNangChungTu.TagName = "";
            this.ucChucNangChungTu.WhereCondition = "";
            this.ucChucNangChungTu.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtLoaiGiayPhep
            // 
            this.txtLoaiGiayPhep.Location = new System.Drawing.Point(623, 41);
            this.txtLoaiGiayPhep.Name = "txtLoaiGiayPhep";
            this.txtLoaiGiayPhep.Size = new System.Drawing.Size(108, 21);
            this.txtLoaiGiayPhep.TabIndex = 7;
            this.txtLoaiGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(267, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(126, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "Chức năng của chứng từ";
            // 
            // cboLoaiHinhXNK
            // 
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Xuất khẩu";
            uiComboBoxItem3.Value = "E";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Nhập khẩu";
            uiComboBoxItem4.Value = "I";
            this.cboLoaiHinhXNK.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cboLoaiHinhXNK.Location = new System.Drawing.Point(146, 68);
            this.cboLoaiHinhXNK.Name = "cboLoaiHinhXNK";
            this.cboLoaiHinhXNK.Size = new System.Drawing.Size(114, 21);
            this.cboLoaiHinhXNK.TabIndex = 9;
            this.cboLoaiHinhXNK.VisualStyleManager = this.vsmMain;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(443, 163);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(52, 13);
            this.label45.TabIndex = 10;
            this.label45.Text = "đến ngày";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(196, 163);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(86, 13);
            this.label44.TabIndex = 10;
            this.label44.Text = "Hiệu lực từ ngày";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(443, 132);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(52, 13);
            this.label43.TabIndex = 10;
            this.label43.Text = "Ngày cấp";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(317, 73);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 13);
            this.label26.TabIndex = 10;
            this.label26.Text = "Ngày khai báo";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(8, 73);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(132, 13);
            this.label20.TabIndex = 8;
            this.label20.Text = "Phân loại xuất/ nhập khẩu";
            // 
            // txtTenDonViCapPhep
            // 
            this.txtTenDonViCapPhep.Location = new System.Drawing.Point(266, 95);
            this.txtTenDonViCapPhep.Name = "txtTenDonViCapPhep";
            this.txtTenDonViCapPhep.Size = new System.Drawing.Size(465, 21);
            this.txtTenDonViCapPhep.TabIndex = 14;
            // 
            // txtTenNguoiKhai
            // 
            this.txtTenNguoiKhai.Location = new System.Drawing.Point(266, 14);
            this.txtTenNguoiKhai.Name = "txtTenNguoiKhai";
            this.txtTenNguoiKhai.Size = new System.Drawing.Size(465, 21);
            this.txtTenNguoiKhai.TabIndex = 2;
            // 
            // txtMaNguoiKhai
            // 
            this.txtMaNguoiKhai.Location = new System.Drawing.Point(146, 14);
            this.txtMaNguoiKhai.Name = "txtMaNguoiKhai";
            this.txtMaNguoiKhai.Size = new System.Drawing.Size(114, 21);
            this.txtMaNguoiKhai.TabIndex = 1;
            this.txtMaNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(67, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã người khai";
            // 
            // txtSoGiayPhepKQXL
            // 
            this.txtSoGiayPhepKQXL.Enabled = false;
            this.txtSoGiayPhepKQXL.Location = new System.Drawing.Point(288, 128);
            this.txtSoGiayPhepKQXL.Name = "txtSoGiayPhepKQXL";
            this.txtSoGiayPhepKQXL.Size = new System.Drawing.Size(109, 21);
            this.txtSoGiayPhepKQXL.TabIndex = 16;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(213, 132);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(69, 13);
            this.label42.TabIndex = 2;
            this.label42.Text = "Số giấy phép";
            // 
            // txtSoDonXinCapPhep
            // 
            this.txtSoDonXinCapPhep.Location = new System.Drawing.Point(146, 41);
            this.txtSoDonXinCapPhep.Name = "txtSoDonXinCapPhep";
            this.txtSoDonXinCapPhep.Size = new System.Drawing.Size(115, 21);
            this.txtSoDonXinCapPhep.TabIndex = 3;
            this.txtSoDonXinCapPhep.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Số đơn xin cấp phép";
            // 
            // dtHieuLucDenNgayKQXL
            // 
            // 
            // 
            // 
            this.dtHieuLucDenNgayKQXL.DropDownCalendar.Name = "";
            this.dtHieuLucDenNgayKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtHieuLucDenNgayKQXL.Enabled = false;
            this.dtHieuLucDenNgayKQXL.IsNullDate = true;
            this.dtHieuLucDenNgayKQXL.Location = new System.Drawing.Point(501, 155);
            this.dtHieuLucDenNgayKQXL.Name = "dtHieuLucDenNgayKQXL";
            this.dtHieuLucDenNgayKQXL.Size = new System.Drawing.Size(100, 21);
            this.dtHieuLucDenNgayKQXL.TabIndex = 19;
            this.dtHieuLucDenNgayKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtHieuLucTuNgayKQXL
            // 
            // 
            // 
            // 
            this.dtHieuLucTuNgayKQXL.DropDownCalendar.Name = "";
            this.dtHieuLucTuNgayKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtHieuLucTuNgayKQXL.Enabled = false;
            this.dtHieuLucTuNgayKQXL.IsNullDate = true;
            this.dtHieuLucTuNgayKQXL.Location = new System.Drawing.Point(288, 155);
            this.dtHieuLucTuNgayKQXL.Name = "dtHieuLucTuNgayKQXL";
            this.dtHieuLucTuNgayKQXL.Size = new System.Drawing.Size(109, 21);
            this.dtHieuLucTuNgayKQXL.TabIndex = 18;
            this.dtHieuLucTuNgayKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayCapKQXL
            // 
            // 
            // 
            // 
            this.dtNgayCapKQXL.DropDownCalendar.Name = "";
            this.dtNgayCapKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayCapKQXL.Enabled = false;
            this.dtNgayCapKQXL.IsNullDate = true;
            this.dtNgayCapKQXL.Location = new System.Drawing.Point(501, 128);
            this.dtNgayCapKQXL.Name = "dtNgayCapKQXL";
            this.dtNgayCapKQXL.Size = new System.Drawing.Size(100, 21);
            this.dtNgayCapKQXL.TabIndex = 17;
            this.dtNgayCapKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayKhaiBao
            // 
            // 
            // 
            // 
            this.dtNgayKhaiBao.DropDownCalendar.Name = "";
            this.dtNgayKhaiBao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayKhaiBao.Enabled = false;
            this.dtNgayKhaiBao.IsNullDate = true;
            this.dtNgayKhaiBao.Location = new System.Drawing.Point(398, 68);
            this.dtNgayKhaiBao.Name = "dtNgayKhaiBao";
            this.dtNgayKhaiBao.Size = new System.Drawing.Size(100, 21);
            this.dtNgayKhaiBao.TabIndex = 11;
            this.dtNgayKhaiBao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(525, 45);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Loại giấy phép";
            // 
            // txtMaKQXL
            // 
            this.txtMaKQXL.Enabled = false;
            this.txtMaKQXL.Location = new System.Drawing.Point(146, 128);
            this.txtMaKQXL.Name = "txtMaKQXL";
            this.txtMaKQXL.Size = new System.Drawing.Size(52, 21);
            this.txtMaKQXL.TabIndex = 15;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(53, 132);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(87, 13);
            this.label41.TabIndex = 12;
            this.label41.Text = "Mã kết quả xử lý";
            // 
            // txtMaDonViCapPhep
            // 
            this.txtMaDonViCapPhep.Location = new System.Drawing.Point(146, 95);
            this.txtMaDonViCapPhep.Name = "txtMaDonViCapPhep";
            this.txtMaDonViCapPhep.Size = new System.Drawing.Size(115, 21);
            this.txtMaDonViCapPhep.TabIndex = 13;
            this.txtMaDonViCapPhep.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(40, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Mã đơn vị cấp phép";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label58);
            this.uiGroupBox2.Controls.Add(this.pnlHoSoKemTheo);
            this.uiGroupBox2.Controls.Add(this.ucCuaKhauXuatNhap);
            this.uiGroupBox2.Controls.Add(this.ucPhuongTienVanChuyen);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox2.Controls.Add(this.clcDenNgay);
            this.uiGroupBox2.Controls.Add(this.label38);
            this.uiGroupBox2.Controls.Add(this.label25);
            this.uiGroupBox2.Controls.Add(this.clcTuNgay);
            this.uiGroupBox2.Controls.Add(this.label24);
            this.uiGroupBox2.Controls.Add(this.clcNgay_HD_Ban);
            this.uiGroupBox2.Controls.Add(this.label23);
            this.uiGroupBox2.Controls.Add(this.clcNgay_HD_Mua);
            this.uiGroupBox2.Controls.Add(this.label22);
            this.uiGroupBox2.Controls.Add(this.txtDaiDienDonViCapGiayPhep);
            this.uiGroupBox2.Controls.Add(this.txtTenGiamDoc);
            this.uiGroupBox2.Controls.Add(this.txtHoSoLienQuan);
            this.uiGroupBox2.Controls.Add(this.txtGhiChuCoQuanCapGiayPhep);
            this.uiGroupBox2.Controls.Add(this.txtGhiChu);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label18);
            this.uiGroupBox2.Controls.Add(this.label16);
            this.uiGroupBox2.Controls.Add(this.lblHoSoKemTheo);
            this.uiGroupBox2.Controls.Add(this.label47);
            this.uiGroupBox2.Controls.Add(this.label39);
            this.uiGroupBox2.Controls.Add(this.label37);
            this.uiGroupBox2.Controls.Add(this.txtSoHopDongMua);
            this.uiGroupBox2.Controls.Add(this.label46);
            this.uiGroupBox2.Controls.Add(this.label36);
            this.uiGroupBox2.Controls.Add(this.txtSoHopDongBan);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 183);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(757, 603);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(133, 593);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(16, 13);
            this.label58.TabIndex = 35;
            this.label58.Text = "   ";
            // 
            // pnlHoSoKemTheo
            // 
            this.pnlHoSoKemTheo.Controls.Add(this.label40);
            this.pnlHoSoKemTheo.Controls.Add(this.txt10);
            this.pnlHoSoKemTheo.Controls.Add(this.label35);
            this.pnlHoSoKemTheo.Controls.Add(this.txt9);
            this.pnlHoSoKemTheo.Controls.Add(this.label34);
            this.pnlHoSoKemTheo.Controls.Add(this.txt8);
            this.pnlHoSoKemTheo.Controls.Add(this.label33);
            this.pnlHoSoKemTheo.Controls.Add(this.txt7);
            this.pnlHoSoKemTheo.Controls.Add(this.label32);
            this.pnlHoSoKemTheo.Controls.Add(this.txt6);
            this.pnlHoSoKemTheo.Controls.Add(this.label31);
            this.pnlHoSoKemTheo.Controls.Add(this.txt5);
            this.pnlHoSoKemTheo.Controls.Add(this.label30);
            this.pnlHoSoKemTheo.Controls.Add(this.txt4);
            this.pnlHoSoKemTheo.Controls.Add(this.label29);
            this.pnlHoSoKemTheo.Controls.Add(this.txt3);
            this.pnlHoSoKemTheo.Controls.Add(this.label28);
            this.pnlHoSoKemTheo.Controls.Add(this.txt2);
            this.pnlHoSoKemTheo.Controls.Add(this.label27);
            this.pnlHoSoKemTheo.Controls.Add(this.txt1);
            this.pnlHoSoKemTheo.Enabled = false;
            this.pnlHoSoKemTheo.Location = new System.Drawing.Point(136, 395);
            this.pnlHoSoKemTheo.Name = "pnlHoSoKemTheo";
            this.pnlHoSoKemTheo.Size = new System.Drawing.Size(443, 27);
            this.pnlHoSoKemTheo.TabIndex = 11;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(376, 7);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(23, 13);
            this.label40.TabIndex = 1;
            this.label40.Text = "10.";
            // 
            // txt10
            // 
            this.txt10.Location = new System.Drawing.Point(399, 3);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(19, 21);
            this.txt10.TabIndex = 11;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(334, 7);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(17, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "9.";
            // 
            // txt9
            // 
            this.txt9.Location = new System.Drawing.Point(351, 3);
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(19, 21);
            this.txt9.TabIndex = 11;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(292, 7);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(17, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "8.";
            // 
            // txt8
            // 
            this.txt8.Location = new System.Drawing.Point(309, 3);
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(19, 21);
            this.txt8.TabIndex = 11;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(250, 7);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(17, 13);
            this.label33.TabIndex = 1;
            this.label33.Text = "7.";
            // 
            // txt7
            // 
            this.txt7.Location = new System.Drawing.Point(267, 3);
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(19, 21);
            this.txt7.TabIndex = 11;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(208, 7);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(17, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "6.";
            // 
            // txt6
            // 
            this.txt6.Location = new System.Drawing.Point(225, 3);
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(19, 21);
            this.txt6.TabIndex = 11;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(166, 7);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(17, 13);
            this.label31.TabIndex = 1;
            this.label31.Text = "5.";
            // 
            // txt5
            // 
            this.txt5.Location = new System.Drawing.Point(183, 3);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(19, 21);
            this.txt5.TabIndex = 11;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(126, 7);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "4.";
            // 
            // txt4
            // 
            this.txt4.Location = new System.Drawing.Point(143, 3);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(19, 21);
            this.txt4.TabIndex = 11;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(84, 7);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(17, 13);
            this.label29.TabIndex = 1;
            this.label29.Text = "3.";
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(101, 3);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(19, 21);
            this.txt3.TabIndex = 11;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(45, 7);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "2.";
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(62, 3);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(19, 21);
            this.txt2.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(3, 7);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "1.";
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(20, 3);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(19, 21);
            this.txt1.TabIndex = 11;
            // 
            // ucCuaKhauXuatNhap
            // 
            this.ucCuaKhauXuatNhap.Code = "";
            this.ucCuaKhauXuatNhap.CountryCode = null;
            this.ucCuaKhauXuatNhap.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCuaKhauXuatNhap.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCuaKhauXuatNhap.IsValidate = true;
            this.ucCuaKhauXuatNhap.Location = new System.Drawing.Point(438, 286);
            this.ucCuaKhauXuatNhap.Name = "ucCuaKhauXuatNhap";
            this.ucCuaKhauXuatNhap.Name_VN = "";
            this.ucCuaKhauXuatNhap.SetValidate = false;
            this.ucCuaKhauXuatNhap.ShowColumnCode = true;
            this.ucCuaKhauXuatNhap.ShowColumnName = true;
            this.ucCuaKhauXuatNhap.Size = new System.Drawing.Size(293, 26);
            this.ucCuaKhauXuatNhap.TabIndex = 7;
            this.ucCuaKhauXuatNhap.TagCode = "";
            this.ucCuaKhauXuatNhap.TagName = "";
            this.ucCuaKhauXuatNhap.WhereCondition = "";
            this.ucCuaKhauXuatNhap.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucPhuongTienVanChuyen
            // 
            this.ucPhuongTienVanChuyen.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ucPhuongTienVanChuyen.Code = "";
            this.ucPhuongTienVanChuyen.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucPhuongTienVanChuyen.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucPhuongTienVanChuyen.IsValidate = true;
            this.ucPhuongTienVanChuyen.Location = new System.Drawing.Point(136, 286);
            this.ucPhuongTienVanChuyen.Name = "ucPhuongTienVanChuyen";
            this.ucPhuongTienVanChuyen.Name_VN = "";
            this.ucPhuongTienVanChuyen.SetValidate = false;
            this.ucPhuongTienVanChuyen.ShowColumnCode = true;
            this.ucPhuongTienVanChuyen.ShowColumnName = true;
            this.ucPhuongTienVanChuyen.Size = new System.Drawing.Size(143, 26);
            this.ucPhuongTienVanChuyen.TabIndex = 6;
            this.ucPhuongTienVanChuyen.TagName = "";
            this.ucPhuongTienVanChuyen.WhereCondition = "";
            this.ucPhuongTienVanChuyen.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.ucMaQuocGia);
            this.uiGroupBox3.Controls.Add(this.txtSoGiayChungNhan);
            this.uiGroupBox3.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.txtTenDN);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.txtQuyetDinhSo);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.dtNgayCap);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.txtMaBuuChinh);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtNoiCap);
            this.uiGroupBox3.Controls.Add(this.txtDiaChi);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.txtEmail);
            this.uiGroupBox3.Controls.Add(this.txtFax);
            this.uiGroupBox3.Controls.Add(this.txtDienThoai);
            this.uiGroupBox3.Location = new System.Drawing.Point(2, 13);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(738, 210);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Doanh nghiệp xuất/Nhập khẩu";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ucMaQuocGia
            // 
            this.ucMaQuocGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGia.Code = "";
            this.ucMaQuocGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGia.IsValidate = true;
            this.ucMaQuocGia.Location = new System.Drawing.Point(115, 178);
            this.ucMaQuocGia.Name = "ucMaQuocGia";
            this.ucMaQuocGia.Name_VN = "";
            this.ucMaQuocGia.SetValidate = false;
            this.ucMaQuocGia.ShowColumnCode = true;
            this.ucMaQuocGia.ShowColumnName = false;
            this.ucMaQuocGia.Size = new System.Drawing.Size(58, 26);
            this.ucMaQuocGia.TabIndex = 8;
            this.ucMaQuocGia.TagName = "";
            this.ucMaQuocGia.WhereCondition = "";
            this.ucMaQuocGia.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            this.ucMaQuocGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(this.ucMaQuocGia_EditValueChanged);
            // 
            // txtSoGiayChungNhan
            // 
            this.txtSoGiayChungNhan.Location = new System.Drawing.Point(404, 72);
            this.txtSoGiayChungNhan.Name = "txtSoGiayChungNhan";
            this.txtSoGiayChungNhan.Size = new System.Drawing.Size(137, 21);
            this.txtSoGiayChungNhan.TabIndex = 3;
            this.txtSoGiayChungNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(115, 19);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(162, 21);
            this.txtMaDoanhNghiep.TabIndex = 0;
            this.txtMaDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã";
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(115, 46);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(614, 21);
            this.txtTenDN.TabIndex = 1;
            this.txtTenDN.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên";
            // 
            // txtQuyetDinhSo
            // 
            this.txtQuyetDinhSo.Location = new System.Drawing.Point(115, 72);
            this.txtQuyetDinhSo.Name = "txtQuyetDinhSo";
            this.txtQuyetDinhSo.Size = new System.Drawing.Size(162, 21);
            this.txtQuyetDinhSo.TabIndex = 2;
            this.txtQuyetDinhSo.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Quy định thành lập số";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(300, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Giấy chứng nhận số";
            // 
            // dtNgayCap
            // 
            // 
            // 
            // 
            this.dtNgayCap.DropDownCalendar.Name = "";
            this.dtNgayCap.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayCap.IsNullDate = true;
            this.dtNgayCap.Location = new System.Drawing.Point(639, 72);
            this.dtNgayCap.Name = "dtNgayCap";
            this.dtNgayCap.Size = new System.Drawing.Size(90, 21);
            this.dtNgayCap.TabIndex = 4;
            this.dtNgayCap.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayCap.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(580, 76);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Ngày cấp";
            // 
            // txtMaBuuChinh
            // 
            this.txtMaBuuChinh.Location = new System.Drawing.Point(115, 125);
            this.txtMaBuuChinh.Name = "txtMaBuuChinh";
            this.txtMaBuuChinh.Size = new System.Drawing.Size(162, 21);
            this.txtMaBuuChinh.TabIndex = 6;
            this.txtMaBuuChinh.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Nơi cấp";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(355, 183);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Số Fax";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Địa chỉ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(546, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Mã bưu chính";
            // 
            // txtNoiCap
            // 
            this.txtNoiCap.Location = new System.Drawing.Point(115, 99);
            this.txtNoiCap.Name = "txtNoiCap";
            this.txtNoiCap.Size = new System.Drawing.Size(614, 21);
            this.txtNoiCap.TabIndex = 5;
            this.txtNoiCap.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(115, 152);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(614, 21);
            this.txtDiaChi.TabIndex = 7;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(179, 183);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Điện thoại";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Mã quốc gia";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(583, 179);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(146, 21);
            this.txtEmail.TabIndex = 11;
            this.txtEmail.VisualStyleManager = this.vsmMain;
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(404, 179);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(137, 21);
            this.txtFax.TabIndex = 10;
            this.txtFax.VisualStyleManager = this.vsmMain;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Location = new System.Drawing.Point(236, 179);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(113, 21);
            this.txtDienThoai.TabIndex = 9;
            this.txtDienThoai.VisualStyleManager = this.vsmMain;
            // 
            // clcDenNgay
            // 
            // 
            // 
            // 
            this.clcDenNgay.DropDownCalendar.Name = "";
            this.clcDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.IsNullDate = true;
            this.clcDenNgay.Location = new System.Drawing.Point(294, 318);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.Size = new System.Drawing.Size(90, 21);
            this.clcDenNgay.TabIndex = 9;
            this.clcDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(289, 293);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(133, 13);
            this.label38.TabIndex = 4;
            this.label38.Text = "Cửa khẩu xuất/nhập hàng";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(236, 322);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "đến ngày";
            // 
            // clcTuNgay
            // 
            // 
            // 
            // 
            this.clcTuNgay.DropDownCalendar.Name = "";
            this.clcTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.IsNullDate = true;
            this.clcTuNgay.Location = new System.Drawing.Point(136, 318);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.Size = new System.Drawing.Size(90, 21);
            this.clcTuNgay.TabIndex = 8;
            this.clcTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(8, 322);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(125, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "Xuất/Nhập khẩu từ ngày";
            // 
            // clcNgay_HD_Ban
            // 
            // 
            // 
            // 
            this.clcNgay_HD_Ban.DropDownCalendar.Name = "";
            this.clcNgay_HD_Ban.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgay_HD_Ban.IsNullDate = true;
            this.clcNgay_HD_Ban.Location = new System.Drawing.Point(438, 260);
            this.clcNgay_HD_Ban.Name = "clcNgay_HD_Ban";
            this.clcNgay_HD_Ban.Size = new System.Drawing.Size(90, 21);
            this.clcNgay_HD_Ban.TabIndex = 5;
            this.clcNgay_HD_Ban.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(376, 264);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Ngày lập";
            // 
            // clcNgay_HD_Mua
            // 
            // 
            // 
            // 
            this.clcNgay_HD_Mua.DropDownCalendar.Name = "";
            this.clcNgay_HD_Mua.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgay_HD_Mua.IsNullDate = true;
            this.clcNgay_HD_Mua.Location = new System.Drawing.Point(438, 233);
            this.clcNgay_HD_Mua.Name = "clcNgay_HD_Mua";
            this.clcNgay_HD_Mua.Size = new System.Drawing.Size(90, 21);
            this.clcNgay_HD_Mua.TabIndex = 3;
            this.clcNgay_HD_Mua.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(376, 237);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Ngày lập";
            // 
            // txtDaiDienDonViCapGiayPhep
            // 
            this.txtDaiDienDonViCapGiayPhep.Enabled = false;
            this.txtDaiDienDonViCapGiayPhep.Location = new System.Drawing.Point(136, 570);
            this.txtDaiDienDonViCapGiayPhep.Name = "txtDaiDienDonViCapGiayPhep";
            this.txtDaiDienDonViCapGiayPhep.Size = new System.Drawing.Size(247, 21);
            this.txtDaiDienDonViCapGiayPhep.TabIndex = 15;
            // 
            // txtTenGiamDoc
            // 
            this.txtTenGiamDoc.Location = new System.Drawing.Point(136, 425);
            this.txtTenGiamDoc.Name = "txtTenGiamDoc";
            this.txtTenGiamDoc.Size = new System.Drawing.Size(247, 21);
            this.txtTenGiamDoc.TabIndex = 12;
            this.txtTenGiamDoc.VisualStyleManager = this.vsmMain;
            // 
            // txtHoSoLienQuan
            // 
            this.txtHoSoLienQuan.Location = new System.Drawing.Point(136, 345);
            this.txtHoSoLienQuan.Multiline = true;
            this.txtHoSoLienQuan.Name = "txtHoSoLienQuan";
            this.txtHoSoLienQuan.Size = new System.Drawing.Size(595, 44);
            this.txtHoSoLienQuan.TabIndex = 10;
            this.txtHoSoLienQuan.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChuCoQuanCapGiayPhep
            // 
            this.txtGhiChuCoQuanCapGiayPhep.Enabled = false;
            this.txtGhiChuCoQuanCapGiayPhep.Location = new System.Drawing.Point(136, 520);
            this.txtGhiChuCoQuanCapGiayPhep.Multiline = true;
            this.txtGhiChuCoQuanCapGiayPhep.Name = "txtGhiChuCoQuanCapGiayPhep";
            this.txtGhiChuCoQuanCapGiayPhep.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGhiChuCoQuanCapGiayPhep.Size = new System.Drawing.Size(595, 44);
            this.txtGhiChuCoQuanCapGiayPhep.TabIndex = 14;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(136, 450);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGhiChu.Size = new System.Drawing.Size(595, 44);
            this.txtGhiChu.TabIndex = 13;
            this.txtGhiChu.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 237);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(117, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Hợp đồng mua hàng số";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 293);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(124, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Phương tiện vận chuyển";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 264);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Hợp đồng bán hàng số";
            // 
            // lblHoSoKemTheo
            // 
            this.lblHoSoKemTheo.AutoSize = true;
            this.lblHoSoKemTheo.Location = new System.Drawing.Point(8, 402);
            this.lblHoSoKemTheo.Name = "lblHoSoKemTheo";
            this.lblHoSoKemTheo.Size = new System.Drawing.Size(81, 13);
            this.lblHoSoKemTheo.TabIndex = 12;
            this.lblHoSoKemTheo.Text = "Hồ sơ kèm theo";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(8, 565);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(81, 26);
            this.label47.TabIndex = 1;
            this.label47.Text = "Đại diện đơn vị \r\ncấp giấy phép";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 349);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(80, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "Hồ sơ liên quan";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(8, 429);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(70, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Tên giám đốc";
            // 
            // txtSoHopDongMua
            // 
            this.txtSoHopDongMua.Location = new System.Drawing.Point(136, 233);
            this.txtSoHopDongMua.Name = "txtSoHopDongMua";
            this.txtSoHopDongMua.Size = new System.Drawing.Size(216, 21);
            this.txtSoHopDongMua.TabIndex = 2;
            this.txtSoHopDongMua.VisualStyleManager = this.vsmMain;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(8, 504);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(186, 13);
            this.label46.TabIndex = 1;
            this.label46.Text = "Ghi chú (Dành cho cơ quan cấp phép)";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(8, 450);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(42, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "Ghi chú";
            // 
            // txtSoHopDongBan
            // 
            this.txtSoHopDongBan.Location = new System.Drawing.Point(136, 260);
            this.txtSoHopDongBan.Name = "txtSoHopDongBan";
            this.txtSoHopDongBan.Size = new System.Drawing.Size(216, 21);
            this.txtSoHopDongBan.TabIndex = 4;
            this.txtSoHopDongBan.VisualStyleManager = this.vsmMain;
            // 
            // VNACC_GiayPhepForm_SEA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 824);
            this.Controls.Add(this.cmdToolBar);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_GiayPhepForm_SEA";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin giấy phép (SEA: Application for Permission to export/import industrial " +
                "explosives)";
            this.Load += new System.EventHandler(this.VNACC_GiayPhepForm_Load);
            this.Controls.SetChildIndex(this.cmdToolBar, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).EndInit();
            this.cmdToolBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.pnlHoSoKemTheo.ResumeLayout(false);
            this.pnlHoSoKemTheo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar cmdToolBar;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdlayPhanHoi;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDongMua;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtFax;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuyetDinhSo;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiCap;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDongBan;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinh;
        private Janus.Windows.GridEX.EditControls.EditBox txtEmail;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayChungNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoai;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiKhai;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDonXinCapPhep;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonViCapPhep;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIComboBox cboLoaiHinhXNK;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDenNgay;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.CalendarCombo.CalendarCombo clcTuNgay;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgay_HD_Ban;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgay_HD_Mua;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayCap;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenGiamDoc;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.GridEX.EditControls.EditBox txtHoSoLienQuan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucChucNangChungTu;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucPhuongTienVanChuyen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap ucCuaKhauXuatNhap;
        private Janus.Windows.GridEX.EditControls.EditBox txt1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblHoSoKemTheo;
        private System.Windows.Forms.Panel pnlHoSoKemTheo;
        private System.Windows.Forms.Label label40;
        private Janus.Windows.GridEX.EditControls.EditBox txt10;
        private System.Windows.Forms.Label label35;
        private Janus.Windows.GridEX.EditControls.EditBox txt9;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.GridEX.EditControls.EditBox txt8;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.EditBox txt7;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txt6;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.GridEX.EditControls.EditBox txt5;
        private System.Windows.Forms.Label label30;
        private Janus.Windows.GridEX.EditControls.EditBox txt4;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.EditBox txt3;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.GridEX.EditControls.EditBox txt2;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiKhai;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayKhaiBao;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonViCapPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayPhepKQXL;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaKQXL;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private Janus.Windows.CalendarCombo.CalendarCombo dtHieuLucDenNgayKQXL;
        private Janus.Windows.CalendarCombo.CalendarCombo dtHieuLucTuNgayKQXL;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayCapKQXL;
        private Janus.Windows.GridEX.EditControls.EditBox txtDaiDienDonViCapGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuCoQuanCapGiayPhep;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHaiQuan;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHaiQuan1;
        private System.Windows.Forms.Label label58;
    }
}