using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Company.KDT.SHARE.Components;

namespace Company.GC.BLL.KDT.GC
{
    public partial class NguyenPhuLieu
    {
        //2 tieu chi nay dung de binding du lieu tren luoi. khong load du lieu cho 2 field nay.
        private string _nguonCungCap = "";
        private bool _tuCungUng = false;

        public string NguonCungCap
        {
            get { return _nguonCungCap; }
            set { _nguonCungCap = value; }
        }

        public bool TuCungUng
        {
            get { return _tuCungUng; }
            set { _tuCungUng = value; }
        }

        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

        public static bool CheckExitNPLinDinhMuc(string MaNguyenPhuLieu, string MaHaiQuan, string MaDoanhNghiep, long idHopDong)
        {
            string sql = " SELECT  MaNguyenPhuLieu FROM t_KDT_GC_DinhMuc inner join t_KDT_GC_DinhMucDangKy " +
                         " on t_KDT_GC_DinhMuc.Master_ID =t_KDT_GC_DinhMucDangKy.ID " +
                         " where  MaDoanhNghiep=@MaDoanhNghiep AND t_KDT_GC_DinhMucDangKy.ID_HopDong = @IDHopDong AND MaHaiQuan=@MaHaiQuan and MaNguyenPhuLieu=@MaNguyenPhuLieu";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, idHopDong);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
            return db.ExecuteScalar(dbCommand) != null;

        }

        public string GetDVTByMa(string Ma)
        {
            string sql = "Select DVT_ID from t_KDT_GC_NguyenPhuLieu where Ma = @Ma";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds.Tables[0].Rows[0].ItemArray[0].ToString();
        }

        //---------------------------------------------------------------------------------------------
        public void SetDabaseMoi(string nameDatabase)
        {
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
        }
        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_NguyenPhuLieu_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this.HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransactionKTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_NguyenPhuLieu_InsertUpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public long InsertTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_NguyenPhuLieu_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NguyenPhuLieu_Insert_ByKTX";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }
        public  List<NguyenPhuLieu> SelectCollectionBy_HopDong_ID()
        {
            return (List<NguyenPhuLieu>)NguyenPhuLieu.SelectCollectionBy_HopDong_ID(this.HopDong_ID);
        }
        public DataSet SelectBy_HopDong_ID()
        {
            return NguyenPhuLieu.SelectBy_HopDong_ID(this.HopDong_ID);
        }
        public int DeleteDynamic(string MaNPL, long IDHopDong, SqlTransaction transaction)
        {
            string sql = @"Delete t_KDT_GC_NguyenPhuLieu Where Ma = @MaNPL and HopDong_ID = @HopDong_ID ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@MaNPl", SqlDbType.VarChar, MaNPL);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        public int DeleteBy_ID_HopDong(long HopDong_ID)
        {
            string spName = "p_KDT_GC_NguyenPhuLieu_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_HopDong_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_NguyenPhuLieu_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        public long InsertTransactionTQDT(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NguyenPhuLieu_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
            db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, DonGia);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        public int DeleteByHopDongID(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NguyenPhuLieu_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }
        //---------------------------------------------------------------------------------------------

        public List<NguyenPhuLieu> SelectCollectionByHopDongIDByKTX(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 28/06/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NguyenPhuLieu_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            List<NguyenPhuLieu> collection = new List<NguyenPhuLieu>();
            IDataReader reader = null;
            if (transaction != null)
                reader = db.ExecuteReader(dbCommand, transaction);
            else
                reader = db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<MaterialsNorm> GetNPLFromDinhMuc(long IDDinhMuc, string MaSanPham, long IDHopDong)
        {
            string sql = string.Format(@"SELECT         t_GC_NguyenPhuLieu.MaHS, t_GC_NguyenPhuLieu.DVT_ID, t_GC_NguyenPhuLieu.Ma,t_GC_NguyenPhuLieu.Ten, t_KDT_GC_DinhMuc.DinhMucSuDung, t_KDT_GC_DinhMuc.TyLeHaoHut, t_KDT_GC_DinhMuc.GhiChu
                                           FROM         t_GC_NguyenPhuLieu INNER JOIN
                                                        t_KDT_GC_DinhMuc ON t_GC_NguyenPhuLieu.Ma = t_KDT_GC_DinhMuc.MaNguyenPhuLieu AND 
                                                        t_GC_NguyenPhuLieu.Ten = t_KDT_GC_DinhMuc.TenNPL INNER JOIN
                                                        t_KDT_GC_DinhMucDangKy ON t_KDT_GC_DinhMuc.Master_ID = t_KDT_GC_DinhMucDangKy.ID INNER JOIN
                                                        t_KDT_GC_HopDong ON t_GC_NguyenPhuLieu.HopDong_ID = t_KDT_GC_HopDong.ID AND t_KDT_GC_DinhMucDangKy.ID_HopDong = t_KDT_GC_HopDong.ID
                                          WHERE     (t_KDT_GC_DinhMucDangKy.ID = @IDDinhMuc) AND (t_KDT_GC_DinhMuc.MaSanPham = @MaSanPham ) AND (t_KDT_GC_HopDong.ID = @IDHopDong) ORDER BY t_GC_NguyenPhuLieu.Ma");

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDDinhMuc", SqlDbType.BigInt, IDDinhMuc);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<MaterialsNorm> NplCollection = new List<MaterialsNorm>();
            decimal _dinhMucSD = 0;
            decimal _tyLeHH = 0;
            while (reader.Read())
            {
                MaterialsNorm entity = new MaterialsNorm() { Material = new Product() { Commodity = new Commodity(), GoodsMeasure = new GoodsMeasure() } };
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.Material.Commodity.TariffClassification = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.Material.GoodsMeasure.MeasureUnit = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Material.Commodity.Identification = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Material.Commodity.Description = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) _dinhMucSD = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) _tyLeHH = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.Description = reader.GetString(reader.GetOrdinal("GhiChu"));
                entity.Norm = Helpers.FormatNumeric(_dinhMucSD, 8);
                entity.Loss = Helpers.FormatNumeric(_tyLeHH, 4);
                entity.RateExchange = "1";
                NplCollection.Add(entity);
            }
            reader.Close();
            return NplCollection;
        }
    }
}
