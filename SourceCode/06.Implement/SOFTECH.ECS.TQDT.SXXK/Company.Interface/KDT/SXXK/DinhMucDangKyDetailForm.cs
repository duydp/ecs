﻿// TEST TFS.
using System;
using Company.BLL;
using Company.BLL.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucDangKyDetailForm : BaseForm
    {
        public DinhMucDangKy DMDangKy = new DinhMucDangKy();
        private string maSP = "";
        //-----------------------------------------------------------------------------------------
        public DinhMucDangKyDetailForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        private void DinhMucDangKyDetailForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            this.DMDangKy.LoadDMCollection();
            dgList.DataSource = this.DMDangKy.DMCollection;
            switch (this.DMDangKy.TrangThaiXuLy)
            {                
                case -1:
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa gửi thông tin";
                    }
                    else
                    {
                        lblTrangThai.Text = "Information has not yet sent";
                    }
                    break;
                case 0:
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to approval";
                    }
                    
                    break;
                case 1:
                    //lblTrangThai.Text = "Đã duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Approved";
                    }
                    break;
                case 2:
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not Approved";
                    }
                   // lblTrangThai.Text = "Không phê duyệt";
                    break;
               

            }

            txtSoTiepNhan.Text = this.DMDangKy.SoTiepNhan.ToString();
            txtMaHaiQuan.Text = this.DMDangKy.MaHaiQuan;
            txtTenHaiQuan.Text = DonViHaiQuan.GetName(this.DMDangKy.MaHaiQuan);

            if (this.OpenType == OpenFormType.View)
            {
                TopRebar1.Visible = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            else
            {
                TopRebar1.Visible = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            MsgSend msg = new MsgSend();
            msg.master_id = this.DMDangKy.ID;
            msg.LoaiHS = "DM";
            if (msg.Load())
            {
                
               // lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
                if (GlobalSettings.NGON_NGU  == "0")
                {
                    lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
                }
                else
                {
                    lblTrangThai.Text = "Not Confirm information to Customs";
                }
                //TopRebar1.Visible = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleDinhMuc.KhaiDienTu)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    TopRebar1.Visible = false;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới nguyên phụ liệu.
        /// </summary>
        private void add()
        {
            DinhMucEditForm f = new DinhMucEditForm();            
            f.dmDangKy = DMDangKy;
            f.ShowDialog();
            DMDangKy.InsertUpdateFull();
            this.DMDangKy.LoadDMCollection();
            dgList.DataSource = this.DMDangKy.DMCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        //-----------------------------------------------------------------------------------------
        /*
        private void upload()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string[] danhsachDaDangKy = new string[0];

                WSForm wsForm = new WSForm();
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;

                int ret = 1;
               // int ret = this.SPDangKy.WSUpload(ref danhsachDaDangKy);

                // Thực hiện kiểm tra.
                switch(ret)
                {
                    case 0:
                        ShowMessage("Cập nhật không thành công!\nCó lỗi hệ thống.", false);
                        this.setNeedUpload(true);
                        break;
                    case 1:
                        ShowMessage("Cập nhật thành công!", false);
                        this.setNeedUpload(false);
                        break;
                    case 2:
                        if (ShowMessage("Cập nhật không thành công!\nDo chứng từ này đã được duyệt rồi.\nVậy bạn có muốn cập nhật dữ liệu từ Hải quan không?", true) == "Yes")
                        {
                            this.DMDangKy.WSDownload();
                        }
                        this.setNeedUpload(false);
                        break;
                    case 3:
                        ShowMessage("Cập nhật không thành công!\nCó một số nguyên phụ liệu đã được đăng ký rồi (các dòng màu đỏ).\nVui lòng kiểm tra lại!", false);
                        this.updateRowsOnGrid(danhsachDaDangKy);
                        this.setNeedUpload(true);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        */
        //-----------------------------------------------------------------------------------------

        /*
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.DMDangKy.LoadDMCollection();
                if (this.DMDangKy.DMCollection.Count == 0)
                {
                    ShowMessage("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
                    this.Cursor = Cursors.Default;
                    return;
                }

                string[] danhsachDaDangKy = new string[0];

                WSForm wsForm = new WSForm();
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;

               // this.SPDangKy.WSSend(ref danhsachDaDangKy);

                // Thực hiện kiểm tra.
                if (this.DMDangKy.SoTiepNhan == 0)
                {
                    this.updateRowsOnGrid(danhsachDaDangKy);
                    ShowMessage("Đăng ký không thành công!\nDo có các sản phẩm đã được đăng ký rồi.\nVui lòng kiểm tra lại các dòng màu đỏ!", false);
                }
                else
                {
                    ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + this.DMDangKy.SoTiepNhan, false);
                    cmdAdd.Enabled = cmdSend.Enabled = InheritableBoolean.False;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        */

        private int getPostion(DinhMuc dm)
        {
            for (int i = 0; i < this.DMDangKy.DMCollection.Count; i++)
            {
                if (this.DMDangKy.DMCollection[i].ID == dm.ID) return i;
            }
            return -1;
        }
        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            
            if (this.OpenType != OpenFormType.View)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucEditForm f = new DinhMucEditForm();                   
                    f.DMDetail = (DinhMuc) e.Row.DataRow;                    
                    f.dmDangKy = DMDangKy;
                    f.ShowDialog();
                    DMDangKy.InsertUpdateFull();
                    this.DMDangKy.LoadDMCollection();
                    dgList.DataSource = this.DMDangKy.DMCollection;
                   
                }
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
   
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    this.add();
                    break;
            }
        }
   
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (ShowMessage("Bạn có muốn xóa định mức này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa định mức này không?","MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMuc dm = (DinhMuc)i.GetRow().DataRow;
                        if (dm.ID > 0)
                        {
                            dm.Delete();
                        }
                    }
                }                
            }
            else
            {
                e.Cancel = true;
            }        
        }
    }
}