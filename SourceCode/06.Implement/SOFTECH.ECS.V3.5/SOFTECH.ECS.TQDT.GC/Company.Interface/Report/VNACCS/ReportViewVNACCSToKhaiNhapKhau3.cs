using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using BarcodeLib;
namespace Company.Interface.Report.VNACCS
{
    public partial class ReportViewVNACCSToKhaiNhapKhau3 : DevExpress.XtraReports.UI.XtraReport
    {
        public int count;
        public ReportViewVNACCSToKhaiNhapKhau3()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 30,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindingReport(KDT_VNACC_ToKhaiMauDich tkmd)
        {
            try
            {
                pictureBox1.Image = GenerateBarCode(tkmd.SoToKhai.ToString());
                lblTenThongTinXuat.Text = "Tờ khai hàng hóa nhập khẩu (thông quan)";
                int HangHoa = tkmd.HangCollection.Count;
                if (HangHoa <= 2)
                {
                    lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
                }
                else
                {
                    lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
                }
                lblSoToKhai.Text = tkmd.SoToKhai.ToString();
                lblSoToKhaiDauTien.Text = String.IsNullOrEmpty(tkmd.SoToKhaiDauTien) ? "" : tkmd.SoToKhaiDauTien.ToString();
                lblSoNhanhToKhaiChiaNho.Text = tkmd.SoNhanhToKhai.ToString();
                lblSoToKhaiTamNhapTaiXuat.Text = tkmd.SoToKhaiTNTX.ToString();
                lblMaPhanLoaiKiemTra.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiKiemTra) ? "" : tkmd.MaPhanLoaiKiemTra.ToString();
                lblMaLoaiHinh.Text = String.IsNullOrEmpty(tkmd.MaLoaiHinh) ? "" : tkmd.MaLoaiHinh.ToString();
                lblMaPhanLoaiHangHoa.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiHH) ? "" : tkmd.MaPhanLoaiHH.ToString();
                lblMaHieuPhuongThucVanChuyen.Text = String.IsNullOrEmpty(tkmd.MaPhuongThucVT) ? "" : tkmd.MaPhuongThucVT.ToString();
                lblPhanLoaiCaNhanToChuc.Text = String.IsNullOrEmpty(tkmd.PhanLoaiToChuc) ? "" : tkmd.PhanLoaiToChuc.ToString();
                //lblMaSoHangHoaDaiDienToKhai.Text = ;
                lblTenCoQuanHaiQuanTiepNhanToKhai.Text = String.IsNullOrEmpty(tkmd.CoQuanHaiQuan) ? "" : tkmd.CoQuanHaiQuan.ToString();
                //lblMaBoPhanXuLyToKhai.Text =
                if (tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayDangKy.Text = tkmd.NgayDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDangKy.Text = "";

                }
                if (tkmd.NgayDangKy.Hour.ToString() != "" && tkmd.NgayDangKy.Hour.ToString() != "0")
                {
                    lblGioDangKy.Text = tkmd.NgayDangKy.Hour.ToString() + ":" + tkmd.NgayDangKy.Minute.ToString() + ":" + tkmd.NgayDangKy.Second.ToString();// vad1ac.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioDangKy.Text = "";
                }
                if (tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayThayDoiDangKy.Text = tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayThayDoiDangKy.Text = "";
                }
                if (tkmd.NgayThayDoiDangKy.Hour.ToString() != "" && tkmd.NgayThayDoiDangKy.Hour.ToString().ToUpper() != "0")
                {
                    lblGioThayDoiDangKy.Text = tkmd.NgayThayDoiDangKy.Hour.ToString() + ":" + tkmd.NgayThayDoiDangKy.Minute.ToString() + ":" + tkmd.NgayThayDoiDangKy.Second.ToString();//vad1ac.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioThayDoiDangKy.Text = "";
                }
                if (tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblThoiHanTaiNhapTaiXuat.Text = tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblThoiHanTaiNhapTaiXuat.Text = "";
                }
                lblBieuThiTHHetHan.Text = String.IsNullOrEmpty(tkmd.BieuThiTruongHopHetHan) ? "" : tkmd.BieuThiTruongHopHetHan.ToString().ToUpper();
                //BindingReportHang(vad1ac.HangMD[spl]);
                DataTable dt = new DataTable();
                if (tkmd.HangCollection != null && tkmd.HangCollection.Count > 0)
                {
                    int STT = 0;
                    count = 0;
                    dt.Columns.Add("SoDong", typeof(string));
                    dt.Columns.Add("MaSoHH", typeof(string));
                    dt.Columns.Add("MaQuanLyRieng", typeof(string));
                    dt.Columns.Add("MaPhanLoaiTaiXN", typeof(string));
                    dt.Columns.Add("MoTaHH", typeof(string));
                    dt.Columns.Add("SoLuong1", typeof(decimal));
                    dt.Columns.Add("MaDVT1", typeof(string));
                    dt.Columns.Add("SoKhoanMucDC1", typeof(string));
                    dt.Columns.Add("SoKhoanMucDC2", typeof(string));
                    dt.Columns.Add("SoKhoanMucDC3", typeof(string));
                    dt.Columns.Add("SoKhoanMucDC4", typeof(string));
                    dt.Columns.Add("SoKhoanMucDC5", typeof(string));
                    dt.Columns.Add("SoLuong2", typeof(decimal));
                    dt.Columns.Add("MaDVT2", typeof(string));
                    dt.Columns.Add("TriGiaHD", typeof(decimal));
                    dt.Columns.Add("DonGiaHD", typeof(decimal));
                    dt.Columns.Add("MaDongTienCuaDonGia", typeof(string));
                    dt.Columns.Add("DonViCuaDonGiaVaSoLuong", typeof(string));
                    dt.Columns.Add("TriGiaTinhThueS", typeof(decimal));
                    dt.Columns.Add("MaDongTienCuaTGTT", typeof(string));
                    dt.Columns.Add("TriGiaTinhThueM", typeof(decimal));
                    dt.Columns.Add("SoLuongTinhThue", typeof(decimal));
                    dt.Columns.Add("MaDonViTinhChuanDanhThue", typeof(string));
                    dt.Columns.Add("DonGiaTinhThue", typeof(decimal));
                    dt.Columns.Add("DonViSoLuongTrongDonGiaTinhThue", typeof(string));
                    dt.Columns.Add("MaPhanLoaiThueSuat", typeof(string));
                    dt.Columns.Add("ThueSuatThueNhapKhau", typeof(string));
                    dt.Columns.Add("PhanLoaiNhapThueSuat", typeof(string));
                    dt.Columns.Add("MaXacDinhMucThueNhapKhau", typeof(string));
                    dt.Columns.Add("SoTienThueNK", typeof(decimal));
                    dt.Columns.Add("MaNuocXuatXu", typeof(string));
                    dt.Columns.Add("TenNoiXuatXu", typeof(string));
                    dt.Columns.Add("MaBieuThueNK", typeof(string));
                    dt.Columns.Add("SoTienGiamThueNK", typeof(decimal));
                    dt.Columns.Add("MaNgoaiHanNgach", typeof(string));
                    dt.Columns.Add("SoThuTuDongHangToKhai", typeof(string));
                    dt.Columns.Add("SoDKDanhMucMienThue", typeof(string));
                    dt.Columns.Add("SoDongTuongUngDMMienThue", typeof(string));
                    dt.Columns.Add("MaMienGiamThueNK", typeof(string));
                    dt.Columns.Add("DieuKhoanMienGiam", typeof(string));
                    dt.Columns.Add("TenKhoanMucThueVaThuKhac1", typeof(string));
                    dt.Columns.Add("MaApDungThueSuatThueVaThuKhac1", typeof(string));
                    dt.Columns.Add("TriGiaTinhThueVaThuKhac1", typeof(decimal));
                    dt.Columns.Add("SoLuongTinhThueVaThuKhac1", typeof(decimal));
                    dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac1", typeof(string));
                    dt.Columns.Add("ThueSuatThueVaThuKhac1", typeof(string));
                    dt.Columns.Add("SoTienThueVaThuKhac1", typeof(decimal));
                    dt.Columns.Add("MaMienGiamThueVaThuKhac1", typeof(string));
                    dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac1", typeof(string));
                    dt.Columns.Add("SoTienGiamThueVaThuKhac1", typeof(decimal));
                    dt.Columns.Add("TenKhoanMucThueVaThuKhac2", typeof(string));
                    dt.Columns.Add("MaApDungThueSuatThueVaThuKhac2", typeof(string));
                    dt.Columns.Add("TriGiaTinhThueVaThuKhac2", typeof(decimal));
                    dt.Columns.Add("SoLuongTinhThueVaThuKhac2", typeof(decimal));
                    dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac2", typeof(string));
                    dt.Columns.Add("ThueSuatThueVaThuKhac2", typeof(string));
                    dt.Columns.Add("SoTienThueVaThuKhac2", typeof(decimal));
                    dt.Columns.Add("MaMienGiamThueVaThuKhac2", typeof(string));
                    dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac2", typeof(string));
                    dt.Columns.Add("SoTienGiamThueVaThuKhac2", typeof(decimal));
                    dt.Columns.Add("TenKhoanMucThueVaThuKhac3", typeof(string));
                    dt.Columns.Add("MaApDungThueSuatThueVaThuKhac3", typeof(string));
                    dt.Columns.Add("TriGiaTinhThueVaThuKhac3", typeof(decimal));
                    dt.Columns.Add("SoLuongTinhThueVaThuKhac3", typeof(decimal));
                    dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac3", typeof(string));
                    dt.Columns.Add("ThueSuatThueVaThuKhac3", typeof(string));
                    dt.Columns.Add("SoTienThueVaThuKhac3", typeof(decimal));
                    dt.Columns.Add("MaMienGiamThueVaThuKhac3", typeof(string));
                    dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac3", typeof(string));
                    dt.Columns.Add("SoTienGiamThueVaThuKhac3", typeof(decimal));
                    dt.Columns.Add("TenKhoanMucThueVaThuKhac4", typeof(string));
                    dt.Columns.Add("MaApDungThueSuatThueVaThuKhac4", typeof(string));
                    dt.Columns.Add("TriGiaTinhThueVaThuKhac4", typeof(decimal));
                    dt.Columns.Add("SoLuongTinhThueVaThuKhac4", typeof(decimal));
                    dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac4", typeof(string));
                    dt.Columns.Add("ThueSuatThueVaThuKhac4", typeof(string));
                    dt.Columns.Add("SoTienThueVaThuKhac4", typeof(decimal));
                    dt.Columns.Add("MaMienGiamThueVaThuKhac4", typeof(string));
                    dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac4", typeof(string));
                    dt.Columns.Add("SoTienGiamThueVaThuKhac4", typeof(decimal));
                    dt.Columns.Add("TenKhoanMucThueVaThuKhac5", typeof(string));
                    dt.Columns.Add("MaApDungThueSuatThueVaThuKhac5", typeof(string));
                    dt.Columns.Add("TriGiaTinhThueVaThuKhac5", typeof(decimal));
                    dt.Columns.Add("SoLuongTinhThueVaThuKhac5", typeof(decimal));
                    dt.Columns.Add("MaDonViTinhChuanDanhThueVaThuKhac5", typeof(string));
                    dt.Columns.Add("ThueSuatThueVaThuKhac5", typeof(string));
                    dt.Columns.Add("SoTienThueVaThuKhac5", typeof(decimal));
                    dt.Columns.Add("MaMienGiamThueVaThuKhac5", typeof(string));
                    dt.Columns.Add("DieuKhoanMienGiamThueVaThuKhac5", typeof(string));
                    dt.Columns.Add("SoTienGiamThueVaThuKhac5", typeof(decimal));

                    for (int i = 0; i < tkmd.HangCollection.Count; i++)
                    {
                        STT++;
                        count++;
                        DataRow dr = dt.NewRow();

                        dr["SoDong"] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoDong) ? "" : tkmd.HangCollection[i].SoDong.ToString().Trim();
                        //dr["STT"] = STT.ToString().Trim();
                        dr["MaSoHH"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaSoHang) ? "" : tkmd.HangCollection[i].MaSoHang.ToString().Trim();
                        dr["MaQuanLyRieng"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaQuanLy) ? "" : tkmd.HangCollection[i].MaQuanLy.ToString().Trim();
                        dr["MaPhanLoaiTaiXN"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaPhanLoaiTaiXacNhanGia) ? String.Empty : tkmd.HangCollection[i].MaPhanLoaiTaiXacNhanGia.ToString().Trim();
                        dr["MoTaHH"] = String.IsNullOrEmpty(tkmd.HangCollection[i].TenHang) ? "" : tkmd.HangCollection[i].TenHang.ToString().Trim();
                        dr["SoLuong1"] = tkmd.HangCollection[i].SoLuong1.ToString().Replace(".", ",");
                        dr["MaDVT1"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DVTLuong1) ? "" : tkmd.HangCollection[i].DVTLuong1.ToString().Trim();
                        for (int j = 0; j < 5; j++)
                        {
                            dr["SoKhoanMucDC" + (j + 1)] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoMucKhaiKhoanDC) ? "" : tkmd.HangCollection[i].SoMucKhaiKhoanDC.ToString().Trim();
                        }
                        dr["SoLuong2"] = tkmd.HangCollection[i].SoLuong2.ToString().Replace(".", ",");
                        dr["MaDVT2"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DVTLuong2) ? "" : tkmd.HangCollection[i].DVTLuong2.ToString().Trim();
                        dr["TriGiaHD"] = tkmd.HangCollection[i].TriGiaHoaDon.ToString().Replace(".", ",");
                        dr["DonGiaHD"] = tkmd.HangCollection[i].DonGiaHoaDon.ToString().Replace(".", ",");
                        dr["MaDongTienCuaDonGia"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaTTDonGia) ? "" : tkmd.HangCollection[i].MaTTDonGia.ToString().Trim();
                        dr["DonViCuaDonGiaVaSoLuong"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DVTLuong1) ? "" : tkmd.HangCollection[i].DVTLuong1.ToString().Trim();
                        dr["TriGiaTinhThueS"] = tkmd.HangCollection[i].TriGiaTinhThueS.ToString().Replace(".", ",");
                        //dr["MaDongTienCuaTGTT"] = tkmd.HangCollection[i].matien.ToString().Trim();
                        dr["TriGiaTinhThueM"] = tkmd.HangCollection[i].TriGiaTinhThue.ToString().Replace(".", ",");
                        dr["SoLuongTinhThue"] = tkmd.HangCollection[i].SoLuongTinhThue.ToString().Replace(".", ",");
                        dr["MaDonViTinhChuanDanhThue"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaDVTDanhThue) ? String.Empty : tkmd.HangCollection[i].MaDVTDanhThue.ToString().Trim();
                        dr["DonGiaTinhThue"] = tkmd.HangCollection[i].DonGiaTinhThue.ToString().Replace(".", ",");
                        dr["DonViSoLuongTrongDonGiaTinhThue"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DVTLuong1) ? "" : tkmd.HangCollection[i].DVTLuong1.ToString().Trim();
                        dr["MaPhanLoaiThueSuat"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaPhanLoaiThueSuatThue) ? String.Empty : tkmd.HangCollection[i].MaPhanLoaiThueSuatThue.ToString().Trim();
                        dr["ThueSuatThueNhapKhau"] = String.IsNullOrEmpty(tkmd.HangCollection[i].ThueSuatThue) ? String.Empty : tkmd.HangCollection[i].ThueSuatThue.ToString().Trim();
                        dr["PhanLoaiNhapThueSuat"] = String.IsNullOrEmpty(tkmd.HangCollection[i].PhanLoaiThueSuatThue) ? String.Empty : tkmd.HangCollection[i].PhanLoaiThueSuatThue.ToString().Trim();
                        dr["MaXacDinhMucThueNhapKhau"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaHanNgach) ? "" : tkmd.HangCollection[i].MaHanNgach.ToString().Trim(); //
                        dr["SoTienThueNK"] = tkmd.HangCollection[i].SoTienThue.ToString().Replace(".", ",");
                        dr["MaNuocXuatXu"] = String.IsNullOrEmpty(tkmd.HangCollection[i].NuocXuatXu) ? "" : tkmd.HangCollection[i].NuocXuatXu.ToString().Trim();
                        dr["TenNoiXuatXu"] = String.IsNullOrEmpty(tkmd.HangCollection[i].TenNoiXuatXu) ? String.Empty : tkmd.HangCollection[i].TenNoiXuatXu.ToString().Trim();
                        dr["MaBieuThueNK"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaBieuThueNK) ? "" : tkmd.HangCollection[i].MaBieuThueNK.ToString().Trim();
                        dr["SoTienGiamThueNK"] = tkmd.HangCollection[i].SoTienGiamThue.ToString().Replace(".", ",");
                        dr["MaNgoaiHanNgach"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaHanNgach) ? "" : tkmd.HangCollection[i].MaHanNgach.ToString().Trim();
                        dr["SoThuTuDongHangToKhai"] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoDong) ? "" : tkmd.HangCollection[i].SoDong.ToString().Trim();
                        dr["SoDKDanhMucMienThue"] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoDMMienThue) ? "" : tkmd.HangCollection[i].SoDMMienThue.ToString().Trim();
                        dr["SoDongTuongUngDMMienThue"] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoDongDMMienThue) ? "" : tkmd.HangCollection[i].SoDongDMMienThue.ToString().Trim();
                        dr["MaMienGiamThueNK"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaMienGiamThue) ? "" : tkmd.HangCollection[i].MaMienGiamThue.ToString().Trim();
                        dr["DieuKhoanMienGiam"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DieuKhoanMienGiam) ? String.Empty : tkmd.HangCollection[i].DieuKhoanMienGiam.ToString().Trim();
                        //for (int j = 0; j < 5; j++)
                        //{
                        try
                        {
                            for (int j = 0; j < 5; j++)
                            {
                                dr["TenKhoanMucThueVaThuKhac" + (j + 1)] = String.IsNullOrEmpty(tkmd.HangCollection[i].ThueThuKhacCollection[j].TenKhoanMucThueVaThuKhac) ? String.Empty : tkmd.HangCollection[i].ThueThuKhacCollection[j].TenKhoanMucThueVaThuKhac.ToString().Trim();
                                dr["MaApDungThueSuatThueVaThuKhac" + (j + 1)] = String.IsNullOrEmpty(tkmd.HangCollection[i].ThueThuKhacCollection[j].MaTSThueThuKhac) ? String.Empty : tkmd.HangCollection[i].ThueThuKhacCollection[j].MaTSThueThuKhac.ToString().Trim();
                                dr["TriGiaTinhThueVaThuKhac" + (j + 1)] = tkmd.HangCollection[i].ThueThuKhacCollection[j].TriGiaTinhThueVaThuKhac.ToString().Replace(".", ",");
                                dr["SoLuongTinhThueVaThuKhac" + (j + 1)] = tkmd.HangCollection[i].ThueThuKhacCollection[j].SoLuongTinhThueVaThuKhac.ToString().Replace(".", ",");
                                dr["MaDonViTinhChuanDanhThueVaThuKhac" + (j + 1)] = String.IsNullOrEmpty(tkmd.HangCollection[i].ThueThuKhacCollection[j].MaDVTDanhThueVaThuKhac) ? String.Empty : tkmd.HangCollection[i].ThueThuKhacCollection[j].MaDVTDanhThueVaThuKhac.ToString().Trim();
                                dr["ThueSuatThueVaThuKhac" + (j + 1)] = String.IsNullOrEmpty(tkmd.HangCollection[i].ThueThuKhacCollection[j].ThueSuatThueVaThuKhac) ? String.Empty : tkmd.HangCollection[i].ThueThuKhacCollection[j].ThueSuatThueVaThuKhac.ToString().Trim();
                                dr["SoTienThueVaThuKhac" + (j + 1)] = tkmd.HangCollection[i].ThueThuKhacCollection[j].SoTienThueVaThuKhac.ToString().Replace(".", ",");
                                dr["MaMienGiamThueVaThuKhac" + (j + 1)] = String.IsNullOrEmpty(tkmd.HangCollection[i].ThueThuKhacCollection[j].MaMGThueThuKhac) ? String.Empty : tkmd.HangCollection[i].ThueThuKhacCollection[j].MaMGThueThuKhac.ToString().Trim();
                                dr["DieuKhoanMienGiamThueVaThuKhac" + (j + 1)] = String.IsNullOrEmpty(tkmd.HangCollection[i].ThueThuKhacCollection[j].DieuKhoanMienGiamThueVaThuKhac) ? String.Empty : tkmd.HangCollection[i].ThueThuKhacCollection[j].DieuKhoanMienGiamThueVaThuKhac.ToString().Trim();
                                dr["SoTienGiamThueVaThuKhac" + (j + 1)] = tkmd.HangCollection[i].ThueThuKhacCollection[j].SoTienGiamThueThuKhac.ToString().Replace(".", ",");
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                        //}
                        dt.Rows.Add(dr);
                    }
                    BindingReportHang(dt);

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            

        }

        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSoDong.DataBindings.Add("Text", DetailReport.DataSource, "SoDong");
            lblMaSoHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHH");
            lblMaQuanLyRieng.DataBindings.Add("Text", DetailReport.DataSource, "MaQuanLyRieng");
            lblMaPhanLoaiTaiXacNhanGia.DataBindings.Add("Text", DetailReport.DataSource, "MaPhanLoaiTaiXN");
            lblMoTaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MoTaHH");
            lblSoLuong1.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong1", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblMaDonViTinh1.DataBindings.Add("Text", DetailReport.DataSource, "MaDVT1");
            lblSoMucKhaiKhoangDieuChinh1.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC1");
            lblSoMucKhaiKhoangDieuChinh2.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC2");
            lblSoMucKhaiKhoangDieuChinh3.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC3");
            lblSoMucKhaiKhoangDieuChinh4.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC4");
            lblSoMucKhaiKhoangDieuChinh5.DataBindings.Add("Text", DetailReport.DataSource, "SoKhoanMucDC5");
            lblSoLuong2.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong2", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblMaDonViTinh2.DataBindings.Add("Text", DetailReport.DataSource, "MaDVT2");
            lblTriGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaHD", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblDonGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaHD", Company.KDT.SHARE.Components.Globals.FormatNumber(6, false));
            lblMaDongTienCuaDonGia.DataBindings.Add("Text", DetailReport.DataSource, "MaDongTienCuaDonGia");
            lblDonViCuaDonGiaVaSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "DonViCuaDonGiaVaSoLuong");
            lblTriGiaTinhThueS.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueS", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaDongTienCuaGiaTinhThue.DataBindings.Add("Text", DetailReport.DataSource, "MaDongTienCuaTGTT");
            lblTriGiaTinhThueM.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueM", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTinhThue.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThue");
            lblMaDonViTinhChuanDanhThue.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThue");
            lblDonGiaTinhThue.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaTinhThue", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblDonViSoLuongTrongDonGiaTinhThue.DataBindings.Add("Text", DetailReport.DataSource, "DonViSoLuongTrongDonGiaTinhThue");
            lblMaPhanLoaiThueSuat.DataBindings.Add("Text", DetailReport.DataSource, "MaPhanLoaiThueSuat");
            lblThueSuatThueNhapKhau.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueNhapKhau");
            lblPhanLoaiNhapThueSuat.DataBindings.Add("Text", DetailReport.DataSource, "PhanLoaiNhapThueSuat");
            lblMaXacDinhMucThueNhapKhau.DataBindings.Add("Text", DetailReport.DataSource, "MaXacDinhMucThueNhapKhau");
            lblSoTienThueNK.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueNK");
            lblMaNuocXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "MaNuocXuatXu");
            lblTenNoiXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "TenNoiXuatXu");
            lblMaBieuThueNK.DataBindings.Add("Text", DetailReport.DataSource, "MaBieuThueNK");
            lblSoTienGiamThueNK.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueNK");
            lblMaNgoaiHanNgach.DataBindings.Add("Text", DetailReport.DataSource, "MaNgoaiHanNgach");
            lblSoThuTuDongHangToKhai.DataBindings.Add("Text", DetailReport.DataSource, "SoThuTuDongHangToKhai");
            lblSoDKDanhMucMienThue.DataBindings.Add("Text", DetailReport.DataSource, "SoDKDanhMucMienThue");
            lblSoDongTuongUngDMMienThue.DataBindings.Add("Text", DetailReport.DataSource, "SoDongTuongUngDMMienThue");
            lblMaMienGiamThueNK.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueNK");
            lblDieuKhoanMienGiam.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiam");
            lblTenKhoanMucThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac1");
            lblMaApDungThueSuatThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac1");
            lblTriGiaTinhThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTinhThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac1");
            lblMaDonViTinhChuanDanhThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac1");
            lblThueSuatThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac1");
            lblSoTienThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaMienGiamThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac1");
            lblDieuKhoanMienGiamThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac1");
            lblSoTienGiamThueVaThuKhac1.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac1", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblTenKhoanMucThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac2");
            lblMaApDungThueSuatThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac2");
            lblTriGiaTinhThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTinhThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac2");
            lblMaDonViTinhChuanDanhThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac2");
            lblThueSuatThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac2");
            lblSoTienThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaMienGiamThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac2");
            lblDieuKhoanMienGiamThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac2");
            lblSoTienGiamThueVaThuKhac2.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac2", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblTenKhoanMucThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac3");
            lblMaApDungThueSuatThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac3");
            lblTriGiaTinhThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTinhThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac3");
            lblMaDonViTinhChuanDanhThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac3");
            lblThueSuatThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac3");
            lblSoTienThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaMienGiamThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac3");
            lblDieuKhoanMienGiamThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac3");
            lblSoTienGiamThueVaThuKhac3.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac3", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblTenKhoanMucThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac4");
            lblMaApDungThueSuatThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac4");
            lblTriGiaTinhThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTinhThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac4");
            lblMaDonViTinhChuanDanhThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac4");
            lblThueSuatThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac4");
            lblSoTienThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaMienGiamThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac4");
            lblDieuKhoanMienGiamThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac4");
            lblSoTienGiamThueVaThuKhac4.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac4", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblTenKhoanMucThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucThueVaThuKhac5");
            lblMaApDungThueSuatThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "MaApDungThueSuatThueVaThuKhac5");
            lblTriGiaTinhThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTinhThueVaThuKhac5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoLuongTinhThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTinhThueVaThuKhac5");
            lblMaDonViTinhChuanDanhThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "MaDonViTinhChuanDanhThueVaThuKhac5");
            lblThueSuatThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "ThueSuatThueVaThuKhac5");
            lblSoTienThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "SoTienThueVaThuKhac5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMaMienGiamThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "MaMienGiamThueVaThuKhac5");
            lblDieuKhoanMienGiamThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "DieuKhoanMienGiamThueVaThuKhac5");
            lblSoTienGiamThueVaThuKhac5.DataBindings.Add("Text", DetailReport.DataSource, "SoTienGiamThueVaThuKhac5", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));


        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
            //else
            //{
            //    lbl.Text = String.Format("{0:#.###.###,##}", lbl.Text);
            //}
        }

        decimal SoTTHang = 0;



        private void lblSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 3).ToString();
        }

        private void lblTongSoTrang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            int totalPages = count + 2;
            lblTongSoTrang.Text = totalPages.ToString();
        }

        private void lblSoTrang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            int totalPages = PrintingSystem.Document.PageCount + 3;
            lblSoTrang.Text = totalPages.ToString();
        }
    }
}
