﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface
{
    public partial class SelectDayForm : BaseForm
    {
        public HopDong HD;
        public SelectDayForm()
        {
            InitializeComponent();
        }

        private void btnNXoa_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            containerValidator1.Validate();
            if (!containerValidator1.IsValid )return;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                this.HD.DongBoToKhaiNhapSXXK(ccFromDate.Value, ccToDate.Value);
                showMsg("MSG_240246");
                //ShowMessage("Đồng bộ dữ liệu tờ khai nhập SXXK thành công.", false);
                this.Close();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi: " + ex.Message, false);
            }
            this.Cursor = Cursors.Default;
        }

    }
}