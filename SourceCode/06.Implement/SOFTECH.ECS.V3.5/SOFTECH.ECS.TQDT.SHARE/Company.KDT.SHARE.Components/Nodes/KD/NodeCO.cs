﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.KD
{
    public class NodeCO
    {        
        /// <summary>
        /// Khai bao.
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Loại chứng từ (Form D=861, A=865, AK=007, E=008) || Tổ chức cấp C/O || Ngày hóa đơn
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Số tham chiếu chứng từ || Số TK || Số C/O || Số hóa đơn
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày khai chứng từ || Ngày TK || Ngày cấp C/O
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Chức năng của chứng từ (=8)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo || Nước cấp C/O
        /// </summary>
        public const string issueLocation = "issueLocation";
        /// <summary>
        /// Trạng thái của chứng từ
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Số đăng ký chứng từ 
        /// </summary>
        public const string customsReference = "customsReference";
        /// <summary>
        /// Ngày đăng ký chứng từ 
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Hải quan tiếp nhận chứng từ || Mã hải quan 
        /// </summary>
        public const string declarationOffice = "declarationOffice";	
        /// <summary>
        /// Người khai hải quan
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Tên người khai hải quan || Người nhập khẩu || Tên nước xuất khẩu || Tên nước Nhập khẩu || Tên, địa chỉ người nhập khẩu || Tên cảng xếp hàng || Tên cảng dỡ hàng
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã người khai hải quan 
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Người nhập khẩu
        /// </summary>
        public const string importer = "importer";
        /// <summary>
        /// Tên, địa chỉ người nhập khẩu || Mã người xuất khẩu || Mã người nhập khẩu
        /// </summary>
        public const string Identity = "Identity";
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        public const string DeclarationDocument = "DeclarationDocument";
        /// <summary>
        /// Mã LH
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";
        /// <summary>
        /// Thông tin về C/O
        /// </summary>
        public const string CertificateOfOrigins = "CertificateOfOrigins";
        /// <summary>
        /// CO
        /// </summary>
        public const string CertificateOfOrigin = "CertificateOfOrigin";
        /// <summary>
        /// representative							3	
        /// </summary>
        public const string representative = "representative";
        /// <summary>
        /// Xuất khẩu
        /// </summary>
        public const string exporter = "exporter";
        /// <summary>
        /// Nước xuất khẩu
        /// </summary>
        public const string exportationCountry = "exportationCountry";
        /// <summary>
        /// Mã nước xuất khẩu trên CO || Mã nước nhập khẩu trên C/O || Mã cảng xếp hàng || Mã cảng dỡ hàng
        /// </summary>
        public const string code = "code";
        /// <summary>
        /// Nước nhập khẩu
        /// </summary>
        public const string importationCountry = "importationCountry";
        /// <summary>
        /// Cảng xếp hàng
        /// </summary>
        public const string LoadingLocation = "LoadingLocation";
        /// <summary>
        /// Ngày khởi hành
        /// </summary>
        public const string loading = "loading";
        /// <summary>
        /// Cảng dỡ hàng
        /// </summary>
        public const string UnLoadingLocation = "UnLoadingLocation";
        /// <summary>
        /// Các thông tin khác
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Các thông tin khác
        /// </summary>
        public const string content = "content";
        /// <summary>
        /// Hàng
        /// </summary>
        public const string GoodsItem = "GoodsItem";
        /// <summary>
        /// Số thứ tự hàng
        /// </summary>
        public const string sequence = "sequence";
        /// <summary>
        /// Trị giá tính thuế
        /// </summary>
        public const string statisticalValue = "statisticalValue";
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";
        /// <summary>
        /// Chi tiết hàng
        /// </summary>
        public const string Commodity = "Commodity";
        /// <summary>
        /// Mô tả hàng hóa
        /// </summary>
        public const string description = "description";
        /// <summary>
        /// Mã hàng do doanh nghiệp khai
        /// </summary>
        public const string identification = "identification";
        /// <summary>
        /// Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";
        /// <summary>
        /// 
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";
        /// <summary>
        /// Số kiện
        /// </summary>
        public const string quantity = "quantity";
        /// <summary>
        /// Mã đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";
        /// <summary>
        /// 
        /// </summary>
        public const string Origin = "Origin";
        /// <summary>
        /// Mã nước xuất xứ của hàng hóa
        /// </summary>
        public const string originCountry = "originCountry";
        /// <summary>
        /// Hóa đơn thương mại
        /// </summary>
        public const string Invoice = "Invoice";
        /// <summary>
        /// Loại C/O (Form A, D, E, AK) || Loại kiện
        /// </summary>
        public const string type = "type";
        /// <summary>
        /// Mã nguyên tệ
        /// </summary>
        public const string currencyType = "currencyType";
        /// <summary>
        /// Thông tin kiện
        /// </summary>
        public const string ConsignmentItemPackaging = "ConsignmentItemPackaging";
        /// <summary>
        /// Trọng lượng
        /// </summary>
        public const string grossMass = "grossMass";
        /// <summary>
        /// Nhãn mác
        /// </summary>
        public const string markNumber = "markNumber";

    }
}
