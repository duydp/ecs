﻿using System;
using Company.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuDangKyDetailForm : BaseForm
    {
        public NguyenPhuLieuDangKy NPLDangKy = new NguyenPhuLieuDangKy();

        //-----------------------------------------------------------------------------------------
        public NguyenPhuLieuDangKyDetailForm()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        private void NguyenPhuLieuDangKyDetailForm_Load(object sender, EventArgs e)
        {
            this.NPLDangKy.LoadNPLCollection();

            dgList.DataSource = this.NPLDangKy.NPLCollection;

            switch (this.NPLDangKy.TrangThaiXuLy)
            {
                case -1:
                    // lblTrangThai.Text = "Chưa gửi thông tin";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chưa gửi thông tin";
                    }
                    else
                    {
                        lblTrangThai.Text = "Information has not yet sent";
                    }
                    break;
                case 0:
                    // lblTrangThai.Text = "Chờ duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to approval";
                    }
                    break;
                case 1:
                    //lblTrangThai.Text = "Đã duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã duyệt chính thức";
                    }
                    else
                    {
                        lblTrangThai.Text = "Approved";
                    }
                    break;
                case 2:
                    //lblTrangThai.Text = "Không phê duyệt";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                    }
                    else
                    {
                        lblTrangThai.Text = "Not Approved";
                    }
                    break;
                case 11:
                    //lblTrangThai.Text = "Chờ Hủy";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ Hủy";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to Canceled";
                    }
                    break;
                case 10:
                    //lblTrangThai.Text = "Đã Hủy";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Đã hủy";
                    }
                    else
                    {
                        lblTrangThai.Text = "Canceled";
                    }
                    break;

            }

            txtSoTiepNhan.Text = this.NPLDangKy.SoTiepNhan.ToString();
            if (this.NPLDangKy.SoTiepNhan == 0)
                //txtSoTiepNhan.Text = (NPLDangKy.SoTiepNhanMax() + 1).ToString();
            txtMaHaiQuan.Text = this.NPLDangKy.MaHaiQuan;
            txtTenHaiQuan.Text = DonViHaiQuan.GetName(this.NPLDangKy.MaHaiQuan);
            if (this.OpenType == OpenFormType.View)
            {
                TopRebar1.Visible = false;
                dgList.AllowDelete = InheritableBoolean.False;
            }
            else
            {
                TopRebar1.Visible = true;
                dgList.AllowDelete = InheritableBoolean.True;
            }
            //MsgSend msg = new MsgSend();
            //msg.master_id = this.NPLDangKy.ID;
            //msg.LoaiHS = "NPL";
            //if (msg.Load())
            //{
            //    //lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
            //    if (GlobalSettings.NGON_NGU == "0")
            //    {
            //        lblTrangThai.Text = "Chưa xác nhận thông tin tới hải quan";
            //    }
            //    else
            //    {
            //        lblTrangThai.Text = "Not Confirm information to Customs";
            //    }
            //    TopRebar1.Visible = false;
            //    dgList.AllowDelete = InheritableBoolean.False;
            //}
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.KhaiDienTu)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    TopRebar1.Visible = false;
                }
            }
            setCommandStatus();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới nguyên phụ liệu.
        /// </summary>
        private void add()
        {
            NguyenPhuLieuEditForm f = new NguyenPhuLieuEditForm();
            f.OpenType = OpenFormType.Insert;
            f.MaHaiQuan = txtMaHaiQuan.Text;
            f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            f.NPLCollection = this.NPLDangKy.NPLCollection;
            f.nplDangKy = NPLDangKy;
            f.ShowDialog(this);

            if (f.NPLDetail != null)
            {
                NguyenPhuLieu npl = f.NPLDetail;
                npl.Master_ID = this.NPLDangKy.ID;
                this.NPLDangKy.NPLCollection.Add(npl);
                npl.Insert();

                //TODO: Update NPLChinh trong KDT
                List<Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung> nplBSListKDT = (List<Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung>)Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung.SelectCollectionBy_NPL_ID(npl.ID);
                if (nplBSListKDT != null && nplBSListKDT.Count > 0)
                {
                    Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung nplBs = nplBSListKDT[0];

                    nplBs.NPLChinh = npl.NPLChinh;
                    nplBs.Update();
                }
                else
                {
                    Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung nplBs = new Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung();
                    nplBs.NPL_ID = npl.ID;
                    nplBs.Ma = npl.Ma;
                    nplBs.NPLChinh = npl.NPLChinh;
                    nplBs.Insert();
                }

                BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                nplSXXK.DVT_ID = npl.DVT_ID;
                nplSXXK.Ma = npl.Ma;
                nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                nplSXXK.MaHS = npl.MaHS;
                nplSXXK.Ten = npl.Ten;
                nplSXXK.InsertUpdate();

                //TODO: Update NPLChinh trong SXXK
                Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung nplBSObjectSXXK = Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung.Load(this.MaHaiQuan.Trim(), this.MaDoanhNghiep, npl.Ma);
                if (nplBSObjectSXXK != null)
                {
                    nplBSObjectSXXK.NPLChinh = npl.NPLChinh;
                    nplBSObjectSXXK.Update();
                }
                else
                {
                    Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung nplBSObjectSXXKNew = new Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung();
                    nplBSObjectSXXKNew.MaHaiQuan = this.MaHaiQuan.Trim();
                    nplBSObjectSXXKNew.MaDoanhNghiep = this.MaDoanhNghiep;
                    nplBSObjectSXXKNew.Ma = npl.Ma;
                    nplBSObjectSXXKNew.NPLChinh = npl.NPLChinh;
                    nplBSObjectSXXKNew.Insert();
                }

                dgList.Refetch();
            }
        }

        //-----------------------------------------------------------------------------------------

        //private void upload()
        //{
        //    try
        //    {
        //        this.Cursor = Cursors.WaitCursor;
        //        string[] danhsachDaDangKy = new string[0];

        //        WSForm wsForm = new WSForm();
        //        wsForm.ShowDialog(this);
        //        if (!wsForm.IsReady) return;

        //        int ret = this.NPLDangKy.WSUpload(ref danhsachDaDangKy);

        //        // Thực hiện kiểm tra.
        //        switch(ret)
        //        {
        //            case 0:
        //                ShowMessage("Cập nhật không thành công!\nCó lỗi hệ thống.", false);
        //                this.setNeedUpload(true);
        //                break;
        //            case 1:
        //                ShowMessage("Cập nhật thành công!", false);
        //                this.setNeedUpload(false);
        //                break;
        //            case 2:
        //                if (ShowMessage("Cập nhật không thành công!\nDo chứng từ này đã được duyệt rồi.\nVậy bạn có muốn cập nhật dữ liệu từ Hải quan không?", true) == "Yes")
        //                {
        //                    this.NPLDangKy.WSDownload();
        //                }
        //                this.setNeedUpload(false);
        //                break;
        //            case 3:
        //                ShowMessage("Cập nhật không thành công!\nCó một số nguyên phụ liệu đã được đăng ký rồi (các dòng màu đỏ).\nVui lòng kiểm tra lại!", false);
        //                this.updateRowsOnGrid(danhsachDaDangKy);
        //                this.setNeedUpload(true);
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        //-----------------------------------------------------------------------------------------


        ///// <summary>
        ///// Gửi thông tin đăng ký đến Hải quan.
        ///// </summary>
        //private void send()
        //{
        //    try
        //    {
        //        this.Cursor = Cursors.WaitCursor;
        //        this.NPLDangKy.LoadNPLCollection();
        //        if (this.NPLDangKy.NPLCollection.Count == 0)
        //        {
        //            ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", false);
        //            this.Cursor = Cursors.Default;
        //            return;
        //        }

        //        string[] danhsachDaDangKy = new string[0];

        //        WSForm wsForm = new WSForm();
        //        wsForm.ShowDialog(this);
        //        if (!wsForm.IsReady) return;

        //        this.NPLDangKy.WSSend(ref danhsachDaDangKy);

        //        // Thực hiện kiểm tra.
        //        if (this.NPLDangKy.SoTiepNhan == 0)
        //        {
        //            this.updateRowsOnGrid(danhsachDaDangKy);
        //            ShowMessage("Đăng ký không thành công!\nDo có các nguyên phụ liệu đã được đăng ký rồi.\nVui lòng kiểm tra lại các dòng màu đỏ!", false);
        //        }
        //        else
        //        {
        //            ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + this.NPLDangKy.SoTiepNhan, false);
        //            cmdAdd.Enabled = cmdSend.Enabled = InheritableBoolean.False;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        ///// <summary>
        ///// Hủy thông tin đăng ký đến Hải quan.
        ///// </summary>
        //private void cancel()
        //{
        //    try
        //    {
        //        this.Cursor = Cursors.WaitCursor;
        //        WSForm wsForm = new WSForm();
        //        wsForm.ShowDialog(this);
        //        if (!wsForm.IsReady) return;

        //        bool result = this.NPLDangKy.WSCancel();
        //        if (result)
        //        {
        //            ShowMessage("Hủy thông tin đăng ký thành công!", false);
        //        }
        //        else
        //        {
        //            ShowMessage("Hủy thông tin đăng ký không thành công!", false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        //private void setNeedUpload(bool status)
        //{
        //    this.needUpload = status;
        //    if (status)
        //        cmdUpload1.Enabled = InheritableBoolean.True;
        //    else
        //        cmdUpload1.Enabled = InheritableBoolean.False;
        //}

        //-----------------------------------------------------------------------------------------


        #endregion

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (this.OpenType != OpenFormType.View)
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        NguyenPhuLieuEditForm f = new NguyenPhuLieuEditForm();
                        f.MaHaiQuan = txtMaHaiQuan.Text;
                        f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        f.NPLDetail = (NguyenPhuLieu)e.Row.DataRow;
                        f.NPLCollection = this.NPLDangKy.NPLCollection;
                        f.NPLCollection.RemoveAt(i.Position);
                        if (NPLDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            f.OpenType = OpenFormType.View;
                        }
                        else
                        {
                            f.OpenType = OpenFormType.Edit;
                        }
                        f.nplDangKy = NPLDangKy;
                        f.ShowDialog(this);
                        if (f.NPLDetail != null)
                        {
                            this.NPLDangKy.NPLCollection.Insert(i.Position, f.NPLDetail);
                        }
                        
                    }
                    dgList.Refetch();
                    break;
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdSuaNPL":
                    this.SuaNPLDaDuyet();
                    break;
            }
        }
        private void setCommandStatus()
        {
            if (NPLDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaNPL.Enabled = cmdSuaNPL1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
        }
        private void Save()
        {
            try
            {
                if (this.NPLDangKy.NPLCollection.Count == 0)
                {
                    //ShowMessage("Danh sách nguyên phụ liệu rỗng.\nKhông thể cập nhật dữ liệu.", false);
                    MLMessages("Danh sách nguyên phụ liệu rỗng.\nKhông thể cập nhật dữ liệu.", "MSG_SAV11", "", false);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                // Master.
                this.NPLDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (this.NPLDangKy.ID == 0)
                {
                    this.NPLDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    this.NPLDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.NPLDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.NPLDangKy.NgayTiepNhan = DateTime.Now;
                    //add lannt
                    this.NPLDangKy.GUIDSTR = Guid.NewGuid().ToString();
                }
                // Detail.
                NPLDangKy.NgayTiepNhan = DateTime.Now;
                NPLDangKy.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text);
                int sttHang = 1;
                foreach (NguyenPhuLieu nplD in this.NPLDangKy.NPLCollection)
                {
                    nplD.STTHang = sttHang++;
                }

                if (this.NPLDangKy.InsertUpdateFull())
                {
                    this.Cursor = Cursors.Default;


                    //if (GlobalSettings.NGON_NGU == "0")
                    //{
                    //    ShowMessage("Cập nhật thành công!", false);
                    //}
                    //else
                    //{
                    //    Message("MSG_SAV02", "", false);
                    //}

                    #region Lưu log thao tác
                    string where = "1 = 1";
                    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", NPLDangKy.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.NguyenPhuLieu);
                    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                    if (listLog.Count > 0)
                    {
                        long idLog = listLog[0].IDLog;
                        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                        long idDK = listLog[0].ID_DK;
                        string guidstr = listLog[0].GUIDSTR_DK;
                        string userKhaiBao = listLog[0].UserNameKhaiBao;
                        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                        string userSuaDoi = GlobalSettings.UserLog;
                        DateTime ngaySuaDoi = DateTime.Now;
                        string ghiChu = listLog[0].GhiChu;
                        bool isDelete = listLog[0].IsDelete;
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                    }
                    else
                    {
                        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                        log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.NguyenPhuLieu;
                        log.ID_DK = NPLDangKy.ID;
                        log.GUIDSTR_DK = "";
                        log.UserNameKhaiBao = GlobalSettings.UserLog;
                        log.NgayKhaiBao = DateTime.Now;
                        log.UserNameSuaDoi = GlobalSettings.UserLog;
                        log.NgaySuaDoi = DateTime.Now;
                        log.GhiChu = "";
                        log.IsDelete = false;
                        log.Insert();
                    }
                    #endregion

                    MLMessages("Lưu thành công!", "MSG_SAV02", "", false);
                    NPLDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    NPLDangKy.TransgferDataToSXXK();
                    setCommandStatus();
                    //cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else
                {
                    this.Cursor = Cursors.Default;
                    MLMessages("Lưu không thành công!", "MSG_SAV01", "", false);

                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("" + ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void SuaNPLDaDuyet()
        {
            string msg = "";
            msg += "===============================";
            msg += "\nBạn có muốn chuyển trạng thái Nguyên phụ liệu sang Chờ duyệt không?";
            msg += "\n\nNguyên phụ liêu có số tiếp nhận : " + NPLDangKy.SoTiepNhan.ToString();
            msg += "===============================";
            if (ShowMessage(msg, true) == "Yes")
            {
                NPLDangKy.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                NPLDangKy.InsertUpdate();
                setCommandStatus();
            }
        }
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                string msgLog = "";
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieu npl = (NguyenPhuLieu)i.GetRow().DataRow;

                        if (npl.ID > 0)
                        {
                            //TODO: Kiem tra NPL co su dung trong to khai chua?
                            List<Company.BLL.KDT.ToKhaiMauDich> suDungTK = Company.BLL.KDT.ToKhaiMauDich.CheckUseNPLInToKhai(npl.Ma, this.MaDoanhNghiep, this.MaHaiQuan.Trim());

                            if (suDungTK.Count > 0)
                            {
                                msgLog += string.Format(" Tờ khai [ID/ Số tờ khai/ Mã loại hình/ Năm đăng ký] = {0}/{1}/{2}/{3}\r\n", suDungTK[0].ID, suDungTK[0].SoToKhai, suDungTK[0].MaLoaiHinh, suDungTK[0].NgayDangKy.Year);
                            }

                            if (msgLog.Length == 0)
                            {
                                npl.Delete(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);

                                Company.KDT.SHARE.Components.Common.SXXK.KDT_NguyenPhuLieuBoSung.DeleteBy_NPL_ID(npl.ID);

                                msgLog += npl.Ma + ", ";

                                //TODO: Ghi log Xoa NPL
                                Company.KDT.SHARE.Components.Globals.SaveMessage("", npl.ID, Guid.Empty.ToString(), MessageTypes.DanhMucNguyenPhuLieu, MessageFunctions.ChuyenTrangThaiTay, MessageTitle.XoaNguyenPhuLieu, string.Format("Xóa các Nguyên phụ liệu trong Theo dõi khai báo: {0}\r\nNgày xóa: {1}\r\nNgười xóa: {2}", msgLog, DateTime.Now, ((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME));
                            }
                            else
                            {
                                string msgWarning = string.Format("[Không thể xóa] do mã NPL này đang sử dụng trong các tờ khai sau:\r\n" + msgLog);
                                Globals.ShowMessage(msgWarning, false);
                                e.Cancel = true;
                                return;
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}