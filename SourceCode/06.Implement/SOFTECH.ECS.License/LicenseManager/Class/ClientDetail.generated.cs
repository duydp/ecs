using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Licenses_v2.Class
{
	public partial class ClientDetail
	{
		#region Properties.
		
		public int ID { set; get; }
		public string ProductsID { set; get; }
		public string MaHaiQuan { set; get; }
		public string ServerDBName { set; get; }
		public string UserNameDB { set; get; }
		public string PasswordDB { set; get; }
		public string DBName { set; get; }
		public DateTime NgayOnline { set; get; }
		public DateTime NgayDangKy { set; get; }
		public DateTime NgayKichHoat { set; get; }
		public DateTime NgayHetHang { set; get; }
		public string DiaChiHaiQuan { set; get; }
		public string TenDichVu { set; get; }
		public string HostProxy { set; get; }
		public string PortProxy { set; get; }
		public string SerialCode { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ClientDetail Load(int id)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
			ClientDetail entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new ClientDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProductsID"))) entity.ProductsID = reader.GetString(reader.GetOrdinal("ProductsID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServerDBName"))) entity.ServerDBName = reader.GetString(reader.GetOrdinal("ServerDBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameDB"))) entity.UserNameDB = reader.GetString(reader.GetOrdinal("UserNameDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("PasswordDB"))) entity.PasswordDB = reader.GetString(reader.GetOrdinal("PasswordDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DBName"))) entity.DBName = reader.GetString(reader.GetOrdinal("DBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayOnline"))) entity.NgayOnline = reader.GetDateTime(reader.GetOrdinal("NgayOnline"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKichHoat"))) entity.NgayKichHoat = reader.GetDateTime(reader.GetOrdinal("NgayKichHoat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHang"))) entity.NgayHetHang = reader.GetDateTime(reader.GetOrdinal("NgayHetHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiHaiQuan"))) entity.DiaChiHaiQuan = reader.GetString(reader.GetOrdinal("DiaChiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDichVu"))) entity.TenDichVu = reader.GetString(reader.GetOrdinal("TenDichVu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HostProxy"))) entity.HostProxy = reader.GetString(reader.GetOrdinal("HostProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("PortProxy"))) entity.PortProxy = reader.GetString(reader.GetOrdinal("PortProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SerialCode"))) entity.SerialCode = reader.GetString(reader.GetOrdinal("SerialCode"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<ClientDetail> SelectCollectionAll()
		{
			List<ClientDetail> collection = new List<ClientDetail>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				ClientDetail entity = new ClientDetail();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProductsID"))) entity.ProductsID = reader.GetString(reader.GetOrdinal("ProductsID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServerDBName"))) entity.ServerDBName = reader.GetString(reader.GetOrdinal("ServerDBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameDB"))) entity.UserNameDB = reader.GetString(reader.GetOrdinal("UserNameDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("PasswordDB"))) entity.PasswordDB = reader.GetString(reader.GetOrdinal("PasswordDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DBName"))) entity.DBName = reader.GetString(reader.GetOrdinal("DBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayOnline"))) entity.NgayOnline = reader.GetDateTime(reader.GetOrdinal("NgayOnline"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKichHoat"))) entity.NgayKichHoat = reader.GetDateTime(reader.GetOrdinal("NgayKichHoat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHang"))) entity.NgayHetHang = reader.GetDateTime(reader.GetOrdinal("NgayHetHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiHaiQuan"))) entity.DiaChiHaiQuan = reader.GetString(reader.GetOrdinal("DiaChiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDichVu"))) entity.TenDichVu = reader.GetString(reader.GetOrdinal("TenDichVu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HostProxy"))) entity.HostProxy = reader.GetString(reader.GetOrdinal("HostProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("PortProxy"))) entity.PortProxy = reader.GetString(reader.GetOrdinal("PortProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SerialCode"))) entity.SerialCode = reader.GetString(reader.GetOrdinal("SerialCode"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<ClientDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<ClientDetail> collection = new List<ClientDetail>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ClientDetail entity = new ClientDetail();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProductsID"))) entity.ProductsID = reader.GetString(reader.GetOrdinal("ProductsID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServerDBName"))) entity.ServerDBName = reader.GetString(reader.GetOrdinal("ServerDBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameDB"))) entity.UserNameDB = reader.GetString(reader.GetOrdinal("UserNameDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("PasswordDB"))) entity.PasswordDB = reader.GetString(reader.GetOrdinal("PasswordDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DBName"))) entity.DBName = reader.GetString(reader.GetOrdinal("DBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayOnline"))) entity.NgayOnline = reader.GetDateTime(reader.GetOrdinal("NgayOnline"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKichHoat"))) entity.NgayKichHoat = reader.GetDateTime(reader.GetOrdinal("NgayKichHoat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHang"))) entity.NgayHetHang = reader.GetDateTime(reader.GetOrdinal("NgayHetHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiHaiQuan"))) entity.DiaChiHaiQuan = reader.GetString(reader.GetOrdinal("DiaChiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDichVu"))) entity.TenDichVu = reader.GetString(reader.GetOrdinal("TenDichVu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HostProxy"))) entity.HostProxy = reader.GetString(reader.GetOrdinal("HostProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("PortProxy"))) entity.PortProxy = reader.GetString(reader.GetOrdinal("PortProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SerialCode"))) entity.SerialCode = reader.GetString(reader.GetOrdinal("SerialCode"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<ClientDetail> SelectCollectionBy_SerialCode(string serialCode)
		{
			List<ClientDetail> collection = new List<ClientDetail>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_SerialCode(serialCode);
			while (reader.Read())
			{
				ClientDetail entity = new ClientDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProductsID"))) entity.ProductsID = reader.GetString(reader.GetOrdinal("ProductsID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServerDBName"))) entity.ServerDBName = reader.GetString(reader.GetOrdinal("ServerDBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameDB"))) entity.UserNameDB = reader.GetString(reader.GetOrdinal("UserNameDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("PasswordDB"))) entity.PasswordDB = reader.GetString(reader.GetOrdinal("PasswordDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DBName"))) entity.DBName = reader.GetString(reader.GetOrdinal("DBName"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayOnline"))) entity.NgayOnline = reader.GetDateTime(reader.GetOrdinal("NgayOnline"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKichHoat"))) entity.NgayKichHoat = reader.GetDateTime(reader.GetOrdinal("NgayKichHoat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHang"))) entity.NgayHetHang = reader.GetDateTime(reader.GetOrdinal("NgayHetHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiHaiQuan"))) entity.DiaChiHaiQuan = reader.GetString(reader.GetOrdinal("DiaChiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDichVu"))) entity.TenDichVu = reader.GetString(reader.GetOrdinal("TenDichVu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HostProxy"))) entity.HostProxy = reader.GetString(reader.GetOrdinal("HostProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("PortProxy"))) entity.PortProxy = reader.GetString(reader.GetOrdinal("PortProxy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SerialCode"))) entity.SerialCode = reader.GetString(reader.GetOrdinal("SerialCode"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_SerialCode(string serialCode)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectBy_SerialCode]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_ClientDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_SerialCode(string serialCode)
		{
			const string spName = "p_LICENSE_ClientDetail_SelectBy_SerialCode";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertClientDetail(string productsID, string maHaiQuan, string serverDBName, string userNameDB, string passwordDB, string dBName, DateTime ngayOnline, DateTime ngayDangKy, DateTime ngayKichHoat, DateTime ngayHetHang, string diaChiHaiQuan, string tenDichVu, string hostProxy, string portProxy, string serialCode)
		{
			ClientDetail entity = new ClientDetail();	
			entity.ProductsID = productsID;
			entity.MaHaiQuan = maHaiQuan;
			entity.ServerDBName = serverDBName;
			entity.UserNameDB = userNameDB;
			entity.PasswordDB = passwordDB;
			entity.DBName = dBName;
			entity.NgayOnline = ngayOnline;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayKichHoat = ngayKichHoat;
			entity.NgayHetHang = ngayHetHang;
			entity.DiaChiHaiQuan = diaChiHaiQuan;
			entity.TenDichVu = tenDichVu;
			entity.HostProxy = hostProxy;
			entity.PortProxy = portProxy;
			entity.SerialCode = serialCode;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LICENSE_ClientDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@ProductsID", SqlDbType.VarChar, ProductsID);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@ServerDBName", SqlDbType.NVarChar, ServerDBName);
			db.AddInParameter(dbCommand, "@UserNameDB", SqlDbType.NVarChar, UserNameDB);
			db.AddInParameter(dbCommand, "@PasswordDB", SqlDbType.NVarChar, PasswordDB);
			db.AddInParameter(dbCommand, "@DBName", SqlDbType.NVarChar, DBName);
			db.AddInParameter(dbCommand, "@NgayOnline", SqlDbType.DateTime, NgayOnline.Year <= 1753 ? DBNull.Value : (object) NgayOnline);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayKichHoat", SqlDbType.DateTime, NgayKichHoat.Year <= 1753 ? DBNull.Value : (object) NgayKichHoat);
			db.AddInParameter(dbCommand, "@NgayHetHang", SqlDbType.DateTime, NgayHetHang.Year <= 1753 ? DBNull.Value : (object) NgayHetHang);
			db.AddInParameter(dbCommand, "@DiaChiHaiQuan", SqlDbType.NVarChar, DiaChiHaiQuan);
			db.AddInParameter(dbCommand, "@TenDichVu", SqlDbType.NVarChar, TenDichVu);
			db.AddInParameter(dbCommand, "@HostProxy", SqlDbType.NVarChar, HostProxy);
			db.AddInParameter(dbCommand, "@PortProxy", SqlDbType.NVarChar, PortProxy);
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ClientDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ClientDetail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateClientDetail(int id, string productsID, string maHaiQuan, string serverDBName, string userNameDB, string passwordDB, string dBName, DateTime ngayOnline, DateTime ngayDangKy, DateTime ngayKichHoat, DateTime ngayHetHang, string diaChiHaiQuan, string tenDichVu, string hostProxy, string portProxy, string serialCode)
		{
			ClientDetail entity = new ClientDetail();			
			entity.ID = id;
			entity.ProductsID = productsID;
			entity.MaHaiQuan = maHaiQuan;
			entity.ServerDBName = serverDBName;
			entity.UserNameDB = userNameDB;
			entity.PasswordDB = passwordDB;
			entity.DBName = dBName;
			entity.NgayOnline = ngayOnline;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayKichHoat = ngayKichHoat;
			entity.NgayHetHang = ngayHetHang;
			entity.DiaChiHaiQuan = diaChiHaiQuan;
			entity.TenDichVu = tenDichVu;
			entity.HostProxy = hostProxy;
			entity.PortProxy = portProxy;
			entity.SerialCode = serialCode;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LICENSE_ClientDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@ProductsID", SqlDbType.VarChar, ProductsID);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@ServerDBName", SqlDbType.NVarChar, ServerDBName);
			db.AddInParameter(dbCommand, "@UserNameDB", SqlDbType.NVarChar, UserNameDB);
			db.AddInParameter(dbCommand, "@PasswordDB", SqlDbType.NVarChar, PasswordDB);
			db.AddInParameter(dbCommand, "@DBName", SqlDbType.NVarChar, DBName);
			db.AddInParameter(dbCommand, "@NgayOnline", SqlDbType.DateTime, NgayOnline.Year == 1753 ? DBNull.Value : (object) NgayOnline);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year == 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayKichHoat", SqlDbType.DateTime, NgayKichHoat.Year == 1753 ? DBNull.Value : (object) NgayKichHoat);
			db.AddInParameter(dbCommand, "@NgayHetHang", SqlDbType.DateTime, NgayHetHang.Year == 1753 ? DBNull.Value : (object) NgayHetHang);
			db.AddInParameter(dbCommand, "@DiaChiHaiQuan", SqlDbType.NVarChar, DiaChiHaiQuan);
			db.AddInParameter(dbCommand, "@TenDichVu", SqlDbType.NVarChar, TenDichVu);
			db.AddInParameter(dbCommand, "@HostProxy", SqlDbType.NVarChar, HostProxy);
			db.AddInParameter(dbCommand, "@PortProxy", SqlDbType.NVarChar, PortProxy);
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ClientDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ClientDetail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateClientDetail(int id, string productsID, string maHaiQuan, string serverDBName, string userNameDB, string passwordDB, string dBName, DateTime ngayOnline, DateTime ngayDangKy, DateTime ngayKichHoat, DateTime ngayHetHang, string diaChiHaiQuan, string tenDichVu, string hostProxy, string portProxy, string serialCode)
		{
			ClientDetail entity = new ClientDetail();			
			entity.ID = id;
			entity.ProductsID = productsID;
			entity.MaHaiQuan = maHaiQuan;
			entity.ServerDBName = serverDBName;
			entity.UserNameDB = userNameDB;
			entity.PasswordDB = passwordDB;
			entity.DBName = dBName;
			entity.NgayOnline = ngayOnline;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayKichHoat = ngayKichHoat;
			entity.NgayHetHang = ngayHetHang;
			entity.DiaChiHaiQuan = diaChiHaiQuan;
			entity.TenDichVu = tenDichVu;
			entity.HostProxy = hostProxy;
			entity.PortProxy = portProxy;
			entity.SerialCode = serialCode;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@ProductsID", SqlDbType.VarChar, ProductsID);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@ServerDBName", SqlDbType.NVarChar, ServerDBName);
			db.AddInParameter(dbCommand, "@UserNameDB", SqlDbType.NVarChar, UserNameDB);
			db.AddInParameter(dbCommand, "@PasswordDB", SqlDbType.NVarChar, PasswordDB);
			db.AddInParameter(dbCommand, "@DBName", SqlDbType.NVarChar, DBName);
			db.AddInParameter(dbCommand, "@NgayOnline", SqlDbType.DateTime, NgayOnline.Year == 1753 ? DBNull.Value : (object) NgayOnline);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year == 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayKichHoat", SqlDbType.DateTime, NgayKichHoat.Year == 1753 ? DBNull.Value : (object) NgayKichHoat);
			db.AddInParameter(dbCommand, "@NgayHetHang", SqlDbType.DateTime, NgayHetHang.Year == 1753 ? DBNull.Value : (object) NgayHetHang);
			db.AddInParameter(dbCommand, "@DiaChiHaiQuan", SqlDbType.NVarChar, DiaChiHaiQuan);
			db.AddInParameter(dbCommand, "@TenDichVu", SqlDbType.NVarChar, TenDichVu);
			db.AddInParameter(dbCommand, "@HostProxy", SqlDbType.NVarChar, HostProxy);
			db.AddInParameter(dbCommand, "@PortProxy", SqlDbType.NVarChar, PortProxy);
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ClientDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ClientDetail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteClientDetail(int id)
		{
			ClientDetail entity = new ClientDetail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_SerialCode(string serialCode)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_DeleteBy_SerialCode]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LICENSE_ClientDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ClientDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ClientDetail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}