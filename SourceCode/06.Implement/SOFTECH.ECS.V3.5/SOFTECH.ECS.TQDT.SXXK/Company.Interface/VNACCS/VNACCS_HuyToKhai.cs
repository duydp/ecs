﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Logger;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.VNACCS
{
    public partial class VNACCS_HuyToKhai : BaseForm
    {
        public VNACCS_HuyToKhai()
        {
            InitializeComponent();
        }
        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
            {
                switch (e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString())
                {
                    case "1":
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng xanh";
                        break;
                    case "2":
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng vàng";
                        break;
                    case "3":
                        e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng đỏ";
                        break;
                }
            }
        }
        private void VNACCS_ChuyenThanhKhoan_Load(object sender, EventArgs e)
        {
            SetError(string.Empty);
            loadData();

        }
        private void loadData()
        {
            try
            {
                //DateTime FromDate;
                //DateTime ToDate;
                //List<KDT_VNACC_ToKhaiMauDich> listTK = KDT_VNACC_ToKhaiMauDich.SelectCollectionDaDangKy(FromDate, ToDate);
                //grList.DataSource = listTK;
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThucHien_Click(object sender, EventArgs e)
        {
            btnThucHien.Enabled = false;
            btnDong.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void DoWork(object obj)
        {

        }
    }
}
