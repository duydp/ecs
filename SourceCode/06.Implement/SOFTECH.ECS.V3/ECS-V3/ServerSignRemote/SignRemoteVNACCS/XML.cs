﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;

namespace SignRemoteVNACCS
{
  public  class XML
    {
        public static string ReadNodeXmlAppSettings(string key)
        {
            return ReadNodeXmlAppSettings(key, "");
        }
        public static string ReadNodeXmlAppSettings(string key, string defaultValue)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config.FilePath);
            string groupSettingName = "appSettings";

            return ReadNodeXml(config, doc, groupSettingName, key, defaultValue);
        }
        public static string ReadNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, string default_Value)
        {
            string result = "";

            try
            {
                XmlNode Node = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']");
                if (Node != null)
                {

                    result = Node.SelectSingleNode("@value").Value;
                    if (string.IsNullOrEmpty(result))
                    {
                        result = default_Value;
                    }
                }
                else
                {
                    AddNodeConfig(key, default_Value);
                    result = default_Value;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
            if (string.IsNullOrEmpty(result))
                result = default_Value;
            return result;
        }
        public static void AddNodeConfig(string key, string ValueDefault)
        {
            //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Add(key, ValueDefault);
            config.Save();
        }
        public static void SaveNodeXmlAppSettings(string key, object value, object setting)
        {
            SaveNodeXmlAppSettings(key, value);
            try
            {
                setting = value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }
        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXmlAppSettings(string key, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string groupSettingName = "appSettings";

                SaveNodeXml(config, doc, groupSettingName, key, value);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }
        public static void SaveNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
                xmlDocument.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }
        }
        public static string ReadNodeXmlConnectionStrings2()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;

                return connectionString;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }
        public static void SaveNodeXmlConnectionStrings(string connectionString)
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
