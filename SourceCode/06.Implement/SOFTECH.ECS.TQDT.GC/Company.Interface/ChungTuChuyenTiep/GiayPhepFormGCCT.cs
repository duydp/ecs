﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL.DuLieuChuan;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class GiayPhepFormGCCT : BaseForm
    {
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT;
        public GiayPhep giayPhep = new GiayPhep();
        public bool isKhaiBoSung = false;
        public GiayPhepFormGCCT()
        {
            InitializeComponent();

            SetEvent_TextBox_DoiTac();
        }

        private void BindData()
        {
            //Company.GC.BLL.KDT.HangMauDich hmd = new Company.GC.BLL.KDT.HangMauDich();
            //hmd.ID_TK = TKCT.ID;

            //dgList.DataSource = giayPhep.ConvertListToDataSet(hmd.SelectBy_TKMD_ID().Tables[0]);

            dgList.DataSource = giayPhep.ListHMDofGiayPhep;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void GiayPhepForm_Load(object sender, EventArgs e)
        {
            dgList.RootTable.Columns["MaPhu"].Selectable = false;

            if (this._NguyenTe == null)
                _NguyenTe = Company.GC.BLL.DuLieuChuan.NguyenTe.SelectAll();
            foreach (DataRow rowNuoc in this._NguyenTe.Rows)
                dgList.RootTable.Columns["MaNguyenTe"].ValueList.Add(rowNuoc["ID"].ToString(), rowNuoc["Ten"].ToString());
            if (giayPhep.ID > 0)
            {
                txtMaCQCap.Text = giayPhep.MaCoQuanCap;
                txtMaDVdcCap.Text = giayPhep.MaDonViDuocCap;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtNguoiCap.Text = giayPhep.NguoiCap;
                txtNoiCap.Text = giayPhep.NoiCap;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                txtTenDVdcCap.Text = giayPhep.TenDonViDuocCap;
                txtTenCQCap.Text = giayPhep.TenQuanCap;
                txtThongTinKhac.Text = giayPhep.ThongTinKhac;
                BindData();
            }
            //if (TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO && !isKhaiBoSung)
            //{
            if (TKCT.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (giayPhep.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                txtSoTiepNhan.Text = giayPhep.SoTiepNhan + "";
                ccNgayTiepNhan.Text = giayPhep.NgayTiepNhan.ToShortDateString();
            }
            //}

            //Set ValueList
            Globals.FillHSValueList(this.dgList.RootTable.Columns["MaHS"]);
            Globals.FillDonViTinhValueList(this.dgList.RootTable.Columns["DVT_ID"]);
            Globals.FillNguyenTeValueList(this.dgList.RootTable.Columns["MaNguyenTe"]);

            // Thiết lập trạng thái các nút trên form.
            // HUNGTQ, Update 07/06/2010.
            SetButtonStateGIAYPHEP(TKCT, isKhaiBoSung, giayPhep);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<GiayPhepChiTiet> HangGiayPhepDetailCollection = new List<GiayPhepChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayPhepChiTiet hgpDetail = new GiayPhepChiTiet();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hgpDetail.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hgpDetail.ID = Convert.ToInt64(row["ID"]);
                        HangGiayPhepDetailCollection.Add(hgpDetail);
                    }
                }
                foreach (GiayPhepChiTiet hgpDetailTMP in HangGiayPhepDetailCollection)
                {
                    try
                    {
                        if (hgpDetailTMP.ID > 0)
                        {
                            hgpDetailTMP.Delete();
                        }
                        foreach (GiayPhepChiTiet hgpDetail in giayPhep.ListHMDofGiayPhep)
                        {
                            if (hgpDetail.HMD_ID == hgpDetailTMP.HMD_ID)
                            {
                                giayPhep.ListHMDofGiayPhep.Remove(hgpDetail);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private bool checkSoGiayPhep(string soGiayPhep)
        {
            foreach (GiayPhep gpTMP in TKCT.CTKGiayPhepCollection)
            {
                if (gpTMP.SoGiayPhep.Trim().ToUpper() == soGiayPhep.Trim().ToUpper())
                    return true;
            }
            return false;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            TKCT.CTKGiayPhepCollection.Remove(giayPhep);

            if (checkSoGiayPhep(txtSoGiayPhep.Text))
            {
                if (giayPhep.ID > 0)
                    TKCT.CTKGiayPhepCollection.Add(giayPhep);
                ShowMessage("Số giấy phép này đã tồn tại.", false);
                return;
            }


            if (!ValidateGiayPhep())
                return;

            giayPhep.MaCoQuanCap = txtMaCQCap.Text.Trim();
            giayPhep.MaDonViDuocCap = txtMaDVdcCap.Text.Trim();
            giayPhep.NgayGiayPhep = ccNgayGiayPhep.Value;
            giayPhep.NgayHetHan = ccNgayHHGiayPhep.Value;
            giayPhep.NguoiCap = txtNguoiCap.Text.Trim();
            giayPhep.NoiCap = txtNoiCap.Text.Trim();
            giayPhep.SoGiayPhep = txtSoGiayPhep.Text.Trim();
            giayPhep.TenDonViDuocCap = txtTenDVdcCap.Text.Trim();
            giayPhep.TenQuanCap = txtTenCQCap.Text.Trim();
            giayPhep.ThongTinKhac = txtThongTinKhac.Text;
            giayPhep.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            giayPhep.ID_TK = TKCT.ID;
            giayPhep.LoaiTK = "TKCT";

            if (isKhaiBoSung)
                giayPhep.LoaiKB = 1;
            else
                giayPhep.LoaiKB = 0;
            GridEXRow[] rowCollection = dgList.GetRows();
            /*
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (GiayPhepChiTiet item in giayPhep.ListHMDofGiayPhep)
                {
                    if (item.HMD_ID.ToString().Trim() == rowview["HMD_ID"].ToString().Trim())
                    {
                        //Cap nhat lai GiayPhepChiTiet copy tu HMD cua giay phep
                        item.GhiChu = rowview["GhiChu"].ToString();
                        item.MaChuyenNganh = rowview["MaChuyenNganh"].ToString();
                        if (rowview["MaNguyenTe"].ToString() != null && rowview["MaNguyenTe"].ToString() != "")
                            item.MaNguyenTe = rowview["MaNguyenTe"].ToString();
                        else
                        {
                            ShowMessage("Chưa chọn nguyên tệ cho hàng : " + (row.RowIndex + 1), false);
                            return;
                        }

                        item.MaHS = rowview["MaHS"].ToString();
                        item.MaPhu = rowview["MaPhu"].ToString();
                        item.TenHang = rowview["TenHang"].ToString();
                        item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
                        item.DVT_ID = rowview["DVT_ID"].ToString();
                        item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
                        item.DonGiaKB = Convert.ToDouble(rowview["DonGiaKB"]);
                        item.TriGiaKB = Convert.ToDouble(rowview["TriGiaKB"]);

                        break;
                    }
                }
            }
            */

            try
            {
                giayPhep.InsertUpdateFull();
                TKCT.CTKGiayPhepCollection.Add(giayPhep);
                BindData();
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
                return;
            }

        }

        public HangChuyenTiepCollection ConvertListToHangMauDichCollection(List<GiayPhepChiTiet> listHangGiayPhep, HangChuyenTiepCollection hangCollection)
        {

            HangChuyenTiepCollection tmp = new HangChuyenTiepCollection();

            foreach (GiayPhepChiTiet item in listHangGiayPhep)
            {
                foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HCTTMP in hangCollection)
                {
                    if (HCTTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HCTTMP);
                        break;
                    }
                }
            }
            return tmp;

        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            SelectHangMauDichFormGCCT f = new SelectHangMauDichFormGCCT();
            f.TKCT = TKCT;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất HMD
            //if (giayPhep.ListHMDofGiayPhep.Count > 0)
            //{
            //    f.TKCT.HMDCollection = ConvertListToHangMauDichCollection(giayPhep.ListHMDofGiayPhep,TKCT.HMDCollection);
            //}
            f.ShowDialog();
            if (f.HCTCollection.Count > 0)
            {
                foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HMD in f.HCTCollection)
                {
                    bool ok = false;
                    foreach (GiayPhepChiTiet hangGiayPhepDetail in giayPhep.ListHMDofGiayPhep)
                    {
                        if (hangGiayPhepDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        GiayPhepChiTiet hgpDetail = new GiayPhepChiTiet();
                        hgpDetail.HMD_ID = HMD.ID;
                        hgpDetail.GiayPhep_ID = giayPhep.ID;
                        hgpDetail.MaPhu = HMD.MaHang;
                        hgpDetail.MaHS = HMD.MaHS;
                        hgpDetail.TenHang = HMD.TenHang;
                        hgpDetail.DVT_ID = HMD.ID_DVT;
                        hgpDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hgpDetail.SoLuong = HMD.SoLuong;
                        hgpDetail.NuocXX_ID = HMD.ID_NuocXX;
                        hgpDetail.MaNguyenTe = TKCT.NguyenTe_ID;
                        hgpDetail.DonGiaKB = Convert.ToDouble(HMD.DonGia);
                        hgpDetail.TriGiaKB = Convert.ToDouble(HMD.TriGia);

                        giayPhep.ListHMDofGiayPhep.Add(hgpDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void btnChonGP_Click(object sender, EventArgs e)
        {
            ManageGiayPhepFormGCCT f = new ManageGiayPhepFormGCCT();
            f.isBrower = true;
            f.ID_TK = TKCT.ID;
            f.ShowDialog();
            if (f.giayPhep != null)
            {
                giayPhep = f.giayPhep;
                txtMaCQCap.Text = giayPhep.MaCoQuanCap;
                txtMaDVdcCap.Text = giayPhep.MaDonViDuocCap;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtNguoiCap.Text = giayPhep.NguoiCap;
                txtNoiCap.Text = giayPhep.NoiCap;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                txtTenDVdcCap.Text = giayPhep.TenDonViDuocCap;
                txtTenCQCap.Text = giayPhep.TenQuanCap;
                txtThongTinKhac.Text = giayPhep.ThongTinKhac;
                giayPhep.ID = 0;
                giayPhep.GuidStr = "";
                giayPhep.SoTiepNhan = 0;
                giayPhep.NamTiepNhan = 0;
                giayPhep.NgayTiepNhan = new DateTime(1900, 1, 1);
                BindData();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (giayPhep.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                bool thanhcong = giayPhep.WSKhaiBaoGP(password, TKCT.MaHaiQuanTiepNhan, (long)TKCT.SoToKhai, TKCT.MaLoaiHinh, TKCT.NamDK, TKCT.ID, TKCT.MaDoanhNghiep, null, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao, TKCT.ConvertHMDKDToHangMauDich());
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                if (this.giayPhep.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.giayPhep.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = giayPhep.WSLaySoTiepNhan(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungGiayPhepThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.giayPhep.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.giayPhep.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.giayPhep.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = giayPhep.WSLayPhanHoi(password);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungGiayPhepDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                    {
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    }
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        #region Begin Doi tac TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Mã đơn vị mua, bán.
        /// </summary>
        /// Hungtq, Update 30052010
        private void SetEvent_TextBox_DoiTac()
        {
            txtMaDVdcCap.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;

            txtMaDVdcCap.ButtonClick += new EventHandler(txtMaDVdcCap_ButtonClick);

            txtMaDVdcCap.Leave += new EventHandler(txtMaDVdcCap_Leave);
        }

        private void txtMaDVdcCap_ButtonClick(object sender, EventArgs e)
        {
            Company.GC.BLL.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaDVdcCap.Text = objDoiTac.MaCongTy;
                txtTenDVdcCap.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVdcCap_Leave(object sender, EventArgs e)
        {
            if (txtMaDVdcCap.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVdcCap.Text.Trim()) != "")
                txtTenDVdcCap.Text = DoiTac.GetName(txtMaDVdcCap.Text.Trim());
        }

        #endregion End Doi tac TextBox

        #region Begin VALIDATE GIAP PHEP

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateGiayPhep()
        {
            bool isValid = true;

            //SoGP	varchar(50)
            isValid &= Globals.ValidateNull(txtSoGiayPhep, err, "Số giấy phép");
            if (isValid)
                isValid &= Globals.ValidateLength(txtSoGiayPhep, 50, err, "Số giấy phép");

            //Ngay giay phep
            isValid &= Globals.ValidateNull(ccNgayGiayPhep, err, "Ngày cấp giấy phép");

            //Noi cap
            isValid &= Globals.ValidateNull(txtNoiCap, err, "Nơi cấp giấy phép");

            //NGUOI_CAP	nvarchar(50)
            isValid &= Globals.ValidateLength(txtNguoiCap, 50, err, "Người cấp");

            //MA_NGUOI_DUOC_CAP	varchar(17)
            isValid &= Globals.ValidateLength(txtMaDVdcCap, 17, err, "Mã đơn vị được cấp");

            //MA_CQC	varchar(7)
            isValid &= Globals.ValidateLength(txtMaCQCap, 7, err, "Mã cơ quan cấp");

            //HINH_THUC_TL	varchar(50)

            return isValid;
        }

        #endregion End VALIDATE GIAP PHEP

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form GIAY PHEP.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateGIAYPHEP(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep giayphep)
        {
            if (giayphep == null)
                return false;
            if (TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {
                bool status = false;

                //Khai bao moi
                if (isKhaiBoSung == false)
                {
                    //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                    status = (tkct.SoTiepNhan == 0);

                    btnXoa.Enabled = status;
                    btnChonGP.Enabled = status;
                    btnAddNew.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = false;
                    btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                }
                //Khai bao bo sung
                else
                {
                    //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                    if (tkct.SoToKhai == 0)
                    {
                        //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                        //Globals.ShowMessageTQDT(msg, false);

                        //return false;
                    }
                    else
                    {
                        status = true;

                        btnXoa.Enabled = status;
                        btnChonGP.Enabled = status;
                        btnAddNew.Enabled = status;
                        btnGhi.Enabled = status;
                        btnKetQuaXuLy.Enabled = true;
                        //Neu hop dong chua co so tiep nhan -> phai khai bao
                        if (giayphep.SoTiepNhan == 0)
                        {
                            btnKhaiBao.Enabled = true;
                            btnLayPhanHoi.Enabled = false;
                        }
                        //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                        else
                        {
                            btnKhaiBao.Enabled = false;
                            btnLayPhanHoi.Enabled = true;
                        }
                    }
                }
            }
            else
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }

            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (giayPhep.GuidStr != null && giayPhep.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(giayPhep.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

    }
}

