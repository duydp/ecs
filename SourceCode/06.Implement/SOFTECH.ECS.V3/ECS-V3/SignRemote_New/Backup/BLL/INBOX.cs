using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.IO;


namespace SignRemote
{
    public partial class INBOX
    {
        public static void InsertSignContent(string cnnString, string guidID, string content, int Status , string MSG_FROM)
        {
            try
            {
                SqlConnection cnn = new SqlConnection(cnnString);
                cnn.Open();
                string MSG_TO = System.Environment.MachineName.ToString();
                string cmdText = string.Format("Insert into t_MSG_INBOX(GUIDSTR,MSG_ORIGIN, STATUS,MSG_FROM,MSG_TO) values ('{0}','{1}',{2},'{3}','{4}')", guidID, content, Status, MSG_FROM, MSG_TO);
                SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);

                sqlCmd.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception ex)
            {
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("SqlConnectionString :" + cnnString);
                write.WriteLine("GUIDSTR :" + guidID);
                write.WriteLine("MSG_ORIGIN :" + content);
                write.WriteLine("STATUS :" + Status);
                write.WriteLine("MSG_FROM :" + MSG_FROM);
                write.WriteLine("MSG_TO :" + MSG_FROM);
                write.WriteLine("--------------------------------");
                write.Flush();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static void InsertSignContent(string cnnString, string guidID, string content)
        {
            try
            {
                SqlConnection cnn = new SqlConnection(cnnString);
                cnn.Open();
                string cmdText = string.Format("Insert into t_MSG_INBOX(GUIDSTR,MSG_ORIGIN) values ('{0}','{1}')", guidID, content);
                SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);

                sqlCmd.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static  INBOX GetTopDataContent()
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();//(Company.KDT.SHARE.Components.Globals.DataSignLan);
                string cmd = string.Format("Select top(1) * from t_MSG_INBOX order by CREATE_TIME desc");
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
                IDataReader reader = db.ExecuteReader(dbCommand);
                INBOX inbox = null;
                List<INBOX> InboxCollection = new List<INBOX>();
                if (reader.Read())
                {
                    inbox = new INBOX();
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) inbox.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
                    if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) inbox.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                    if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) inbox.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
                    InboxCollection.Add(inbox);
                }
                reader.Close();
                if (inbox != null)
                {
                    cmd = string.Format("Delete t_MSG_INBOX where GUIDSTR='{0}'", inbox.GUIDSTR);
                    dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
                    db.ExecuteNonQuery(dbCommand);
                }
                return inbox;
            }
            catch (Exception ex)
            {                
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        //public static INBOX GetTopDataContentErrorConnection()
        //{
        //    try
        //    {
        //        #region New
        //        Company.KDT.SHARE.Components.Globals.SERVER_NAME = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ServerName").ToString();
        //        Company.KDT.SHARE.Components.Globals.USER = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("user").ToString();
        //        Company.KDT.SHARE.Components.Globals.PASS = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("pass").ToString();
        //        Company.KDT.SHARE.Components.Globals.DataSignLan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DataSignLan").ToString();
        //        string cnnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { Company.KDT.SHARE.Components.Globals.SERVER_NAME, Company.KDT.SHARE.Components.Globals.DataSignLan, Company.KDT.SHARE.Components.Globals.USER, Company.KDT.SHARE.Components.Globals.PASS });
        //        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        SqlConnection cnn = new SqlConnection(cnnString);
        //        cnn.Open();
        //        string cmd = string.Format("Select top(1) * from t_MSG_INBOX order by CREATE_TIME desc");
        //        SqlCommand dbCommand = new SqlCommand(cmd, cnn);
        //        IDataReader reader = db.ExecuteReader(dbCommand);
        //        cnn.Close();
        //        INBOX inbox = null;
        //        List<INBOX> InboxCollection = new List<INBOX>();
        //        #endregion
        //        if (reader.Read())
        //        {
        //            inbox = new INBOX();
        //            if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) inbox.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
        //            if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) inbox.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
        //            if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) inbox.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
        //            InboxCollection.Add(inbox);
        //        }
        //        reader.Close();
        //        //if (inbox != null)
        //        //{
        //        //    cmd = string.Format("Delete t_MSG_INBOX where GUIDSTR='{0}'", inbox.GUIDSTR);
        //        //    dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
        //        //    db.ExecuteNonQuery(dbCommand);
        //        //}
        //        return inbox;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        return null;
        //    }
        //}

        public static List<INBOX> GetDataContentWailt()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();//(Company.KDT.SHARE.Components.Globals.DataSignLan);
            string cmd = string.Format("Select  * from t_MSG_INBOX WHERE STATUS IN ('-1','50') order by CREATE_TIME desc");
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
            IDataReader reader = db.ExecuteReader(dbCommand);
            INBOX inbox = null;
            List<INBOX> inboxCollection = new List<INBOX>();
            if (reader.Read())
            {
                inbox = new INBOX();
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) inbox.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) inbox.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) inbox.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
                inboxCollection.Add(inbox);
            }
            reader.Close();
            if (inboxCollection.Count > 0)
            {
                foreach (INBOX item in inboxCollection)
                {
                        cmd = string.Format("UPDATE dbo.t_MSG_INBOX SET STATUS = 1 where GUIDSTR='{0}'", item.GUIDSTR);
                        dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
                        db.ExecuteNonQuery(dbCommand);
                }
            }
            return inboxCollection;
        }
        public static INBOX SelectTop1(string whereCondition, string orderByExpression)
        {

            string spName = "[dbo].[p_MSG_Top1]";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@From", SqlDbType.NVarChar, "t_MSG_INBOX");
            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            INBOX entity = null;
            if (reader.Read())
            {
                entity = new INBOX();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) entity.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_DATA"))) entity.MSG_SIGN_DATA = reader.GetString(reader.GetOrdinal("MSG_SIGN_DATA"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_CERT"))) entity.MSG_SIGN_CERT = reader.GetString(reader.GetOrdinal("MSG_SIGN_CERT"));
                if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
                if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) entity.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));

            }

            reader.Close();
            return entity;
        }
    }

}