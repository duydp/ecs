/*
Run this script on:

        192.168.72.151.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.151.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 10/17/2011 1:51:49 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[t_KDT_AnDinhThue]'
GO
CREATE TABLE [dbo].[t_KDT_AnDinhThue]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[TKMD_Ref] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoQuyetDinh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayQuyetDinh] [datetime] NOT NULL,
[NgayHetHan] [datetime] NOT NULL,
[TaiKhoanKhoBac] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenKhoBac] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GhiChu] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_AnHanThue] on [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ADD CONSTRAINT [PK_t_KDT_AnHanThue] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_AnDinhThue_ChiTiet]'
GO
CREATE TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[IDAnDinhThue] [int] NOT NULL,
[SacThue] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TienThue] [numeric] (18, 4) NOT NULL,
[Chuong] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Loai] [int] NOT NULL,
[Khoan] [int] NOT NULL,
[Muc] [int] NOT NULL,
[TieuMuc] [int] NOT NULL,
[GhiChu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_AnDinhThue_ChiTiet] on [dbo].[t_KDT_AnDinhThue_ChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ADD CONSTRAINT [PK_t_KDT_AnDinhThue_ChiTiet] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SXXK_NPLXuatTon]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SXXK_NPLXuatTon]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_DanhSachNPLXuatTonOver5ByNgayHoanThanhXuat]'
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMucDangKy_SelectAll]
-- Database: Haiquan
-- Author: Ngo Thanh Tung`
-- Time created: Monday, March 31, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DanhSachNPLXuatTonOver5ByNgayHoanThanhXuat]
	@BangKeHoSoThanhLy_ID bigint,
	@SoThapPhanNPL int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	   [SoToKhai]
      ,[MaLoaiHinh]
      ,[NamDangKy]
      ,[MaHaiQuan]
      ,[MaSP]
      ,[TenSP]
      ,[TenDVT_SP]
      ,[LuongSP]
      ,[MaNPL]
      ,[DinhMuc]
      ,round(LuongNPL,@SoThapPhanNPL) as LuongNPL
      ,round(TonNPL,@SoThapPhanNPL) as TonNPL
      ,[BangKeHoSoThanhLy_ID]
      ,[NgayThucXuat]
      ,[NgayHoanThanhXuat]
      ,[NgayDangKy]
FROM
	dbo.v_KDT_SXXK_NPLXuatTon
WHERE
	BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID

ORDER BY 
--HungTQ Update 18/10/2010. Bo sung them sap xep theo tieu chi 'NgayHoanThanhXuat' truoc. Ly do vi chay thanh khoan nhung TK nao co ngay hoan thanh som nhat truoc.
	NgayHoanThanhXuat, 
	NgayDangKy, NgayThucXuat,SoToKhai,MaSP


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_DinhMucOfSanPham]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_DinhMucOfSanPham] 
	-- Add the parameters for the stored procedure here
	 @Master_ID BIGINT,
	 @MaSanPham VARCHAR(30),	 
	 @MaDoanhNghiep VARCHAR(14),	 
	 @MaHaiQuan CHAR(6) 	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT     dm.MaNguyenPhuLieu, dm.STTHang, dvt.Ten AS DVT_ID, dm.DinhMucSuDung, dm.TyLeHaoHut, 
							  dm.DinhMucSuDung + dm.DinhMucSuDung * dm.TyLeHaoHut / 100 AS DinhMucChung, npl.Ten AS TenNPL,
                     			(CASE  WHEN (dm.IsFromVietNam=0) THEN  N'Nhập khẩu' ELSE N'Mua tại VN' END) AS FromVietNam
		FROM         t_KDT_SXXK_DinhMuc AS dm INNER JOIN
							  t_SXXK_NguyenPhuLieu AS npl ON dm.MaNguyenPhuLieu = npl.Ma INNER JOIN
							  t_HaiQuan_DonViTinh AS dvt ON dvt.ID = npl.DVT_ID
		                      
		WHERE     (dm.Master_ID = @Master_ID) AND (dm.MaSanPham = @MaSanPham) AND (npl.MaDoanhNghiep = @MaDoanhNghiep) AND (npl.MaHaiQuan = @MaHaiQuan)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_DinhMucOfSanPhamDK]'
GO
-- =============================================
-- Author:		LanNT
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[p_SXXK_DinhMucOfSanPhamDK] 
	-- Add the parameters for the stored procedure here
	 @MaSanPham VARCHAR(30),	 
	 @MaDoanhNghiep VARCHAR(14),	 
	 @MaHaiQuan CHAR(6) 	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT     dm.MaNguyenPhuLieu, dvt.Ten AS DVT_ID, dm.DinhMucSuDung, dm.TyLeHaoHut, dm.DinhMucChung, npl.Ten AS TenNPL,

		(CASE  WHEN (dm.IsFromVietNam=0) THEN  N'Nhập khẩu' ELSE N'Mua tại VN' END) AS FromVietNam
		
		FROM         t_SXXK_DinhMuc AS dm INNER JOIN
							  t_SXXK_NguyenPhuLieu AS npl ON dm.MaNguyenPhuLieu = npl.Ma AND dm.MaHaiQuan = npl.MaHaiQuan AND 
							  dm.MaDoanhNghiep = npl.MaDoanhNghiep INNER JOIN
							  t_HaiQuan_DonViTinh AS dvt ON dvt.ID = npl.DVT_ID
		WHERE     (dm.MaSanPham = @MaSanPham) AND (npl.MaDoanhNghiep = @MaDoanhNghiep) AND (npl.MaHaiQuan = @MaHaiQuan)

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_AnDinhThue_ChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ADD
CONSTRAINT [FK_t_KDT_AnDinhThue_ChiTiet_t_KDT_AnDinhThue] FOREIGN KEY ([IDAnDinhThue]) REFERENCES [dbo].[t_KDT_AnDinhThue] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ADD
CONSTRAINT [FK_t_KDT_AnDinhThue_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors

GO
UPDATE    dbo.t_HaiQuan_Version SET [Version] =1.21, Date = getdate()