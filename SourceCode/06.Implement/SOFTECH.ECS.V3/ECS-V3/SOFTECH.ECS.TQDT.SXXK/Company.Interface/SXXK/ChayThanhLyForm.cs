using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using System.Threading;
using Company.Interface.Report.SXXK;

namespace Company.Interface.SXXK
{
    public partial class ChayThanhLyForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public int ProccesValue;
        public bool temp = false;
        private bool temp2 = false;
        private int limit = 99;
        private bool onRunning = false;
        public ChayThanhLyForm()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (onRunning) e.Cancel = false;
            else
                base.OnClosing(e);
        }
        private void ChayThanhLyForm_Load_1(object sender, EventArgs e)
        {
            this.ProccesValue = 1;
            backgroundWorker1.RunWorkerAsync(this.HSTL);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (temp)
                {
                    timer1.Enabled = false;
                    Thread.Sleep(2000);
                    this.Close();
                }
                DataRow dr = dtProcess.NewRow();
                if (GlobalSettings.NGON_NGU == "0")
                {
                    if (this.ProccesValue == 1)
                    {
                        //DataRow dr = dtProcess.NewRow();
                        // txtMessage.Text += "Đang khởi tạo dữ liệu thanh khoản...";
                        dr["TenProcess"] = "Đang khởi tạo dữ liệu thanh khoản...";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                    }
                    if (this.ProccesValue == 19)
                    {
                        dtProcess.Select("TenProcess='" + "Đang khởi tạo dữ liệu thanh khoản..." + "'")[0]["TrangThai"] = "Hoàn thành";
                        // txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 21)
                    {
                        dr["TenProcess"] = "Đang kiểm tra số liệu thanh khoản...";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang kiểm tra số liệu thanh khoản...";
                    }
                    if (this.ProccesValue == 39)
                    {
                        dtProcess.Select("TenProcess='" + "Đang kiểm tra số liệu thanh khoản..." + "'")[0]["TrangThai"] = "Hoàn thành";
                        //txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 41)
                    {
                        dr["TenProcess"] = "Đang xử lý số liệu thanh khoản....";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang xử lý số liệu thanh khoản...";
                    }
                    if (this.ProccesValue == 59)
                    {
                        dtProcess.Select("TenProcess='" + "Đang xử lý số liệu thanh khoản...." + "'")[0]["TrangThai"] = "Hoàn thành";
                    }
                    if (this.ProccesValue == 61)
                    {
                        dr["TenProcess"] = "Đang chạy thanh khoản...";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang chạy thanh khoản...";
                    }
                    if (this.ProccesValue == 62)
                    {
                        //try
                        //{
                        //    this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                        //}
                        //catch (Exception ex)
                        //{
                        //    ShowMessage("Lỗi: " + ex.Message, false);
                        //    this.Close();
                        //}
                    }
                    if (this.ProccesValue == 79)
                    {
                        dtProcess.Select("TenProcess='" + "Đang chạy thanh khoản..." + "'")[0]["TrangThai"] = "Hoàn thành";

                        //txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 81)
                    {
                        dr["TenProcess"] = "Đang tạo các báo cáo thanh khoản...";
                        dr["TrangThai"] = "Đang thực hiện";
                        dtProcess.Rows.Add(dr);
                        // txtMessage.Text += "Đang tạo các báo cáo thanh khoản...";
                    }
                    if (this.ProccesValue == 99 && !temp2)
                    {
                        dtProcess.Select("TenProcess='" + "Đang tạo các báo cáo thanh khoản..." + "'")[0]["TrangThai"] = "Hoàn thành";

                        //txtMessage.Text += "Hoàn thành!\n";
                        temp2 = true;
                    }
                }
                else
                {
                    if (this.ProccesValue == 1)
                    {
                        //DataRow dr = dtProcess.NewRow();
                        // txtMessage.Text += "Đang khởi tạo dữ liệu thanh khoản...";
                        dr["TenProcess"] = "Creating the Liquidation\'s Data...";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                    }
                    if (this.ProccesValue == 19)
                    {
                        dtProcess.Select("TenProcess='" + "Creating the data of liquidation..." + "'")[0]["TrangThai"] = "Hoàn thành";
                        // txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 21)
                    {
                        dr["TenProcess"] = "Checking the Figures of Liquidation ...";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang kiểm tra số liệu thanh khoản...";
                    }
                    if (this.ProccesValue == 39)
                    {
                        dtProcess.Select("TenProcess='" + "Checking the Figures of Liquidation ..." + "'")[0]["TrangThai"] = "Hoàn thành";
                        //txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 41)
                    {
                        dr["TenProcess"] = "Handling the Figures of Liquidation....";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang xử lý số liệu thanh khoản...";
                    }
                    if (this.ProccesValue == 59)
                    {
                        dtProcess.Select("TenProcess='" + "Handling the Figures of Liquidation...." + "'")[0]["TrangThai"] = "Hoàn thành";
                    }
                    if (this.ProccesValue == 61)
                    {
                        dr["TenProcess"] = "Running Liquidation...";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Đang chạy thanh khoản...";
                    }
                    if (this.ProccesValue == 62)
                    {
                        //try
                        //{
                        //    this.HSTL.ChayThanhLy(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK);
                        //}
                        //catch (Exception ex)
                        //{
                        //    ShowMessage("Lỗi: " + ex.Message, false);
                        //    this.Close();
                        //}
                    }
                    if (this.ProccesValue == 79)
                    {
                        dtProcess.Select("TenProcess='" + "Running Liquidation..." + "'")[0]["TrangThai"] = "Finish";

                        //txtMessage.Text += "Hoàn thành!\n";
                    }
                    if (this.ProccesValue == 81)
                    {
                        dr["TenProcess"] = "Creating the Reports of Liquidation...";
                        dr["TrangThai"] = "Excuting...";
                        dtProcess.Rows.Add(dr);
                        // txtMessage.Text += "Đang tạo các báo cáo thanh khoản...";
                    }
                    if (this.ProccesValue == 99 && !temp2)
                    {
                        dtProcess.Select("TenProcess='" + "Creating the Reports of Liquidation..." + "'")[0]["TrangThai"] = "Hoàn thành";

                        //txtMessage.Text += "Hoàn thành!\n";
                        temp2 = true;
                    }

                }

                if (this.ProccesValue == 100)
                {
                    if (pbProgress.Value == 99)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            dr["TenProcess"] = "Chạy thanh khoản thành công.";
                            dr["TrangThai"] = "Xin chờ...";
                        }
                        else
                        {
                            dr["TenProcess"] = " Run liquidation successfully.";
                            dr["TrangThai"] = "Wait...";
                        }
                        dtProcess.Rows.Add(dr);
                        //txtMessage.Text += "Chạy thanh khoản thành công.";
                        temp = true;
                    }
                    else
                    {
                        this.ProccesValue = pbProgress.Value;
                        limit = 100;
                    }

                }
                pbProgress.Value = this.ProccesValue;
                if (this.ProccesValue < limit) this.ProccesValue++;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void ChayThanhLyForm_Shown(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            onRunning = true;
            try
            {
                int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
                //if (this.HSTL.SoTiepNhan == 0)
                    //Hungtq Update 14/12/2010
                    this.HSTL.ChayThanhLyNgayHoanThanhXuat(GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.NPLKoTK, GlobalSettings.ChayToKhaiNKD, GlobalSettings.AmTKTiep, chenhLech, GlobalSettings.ToKhaiKoTK);
//                 else
//                     this.HSTL.ChayThanhKhoanDungTKXGC(GlobalSettings.SoThapPhan.LuongNPL);
            }
            catch (Exception ex)
            {
                timer1.Enabled = false;
                if (GlobalSettings.NGON_NGU == "0")
                    ShowMessage(" " + ex.Message, false);
                else
                    ShowMessage("There isn't declaration join to liquidate. Check norm of declaration. ", false);
                // MLMessages(ex.Message, "MSG_TK07", "", false);


            }
            onRunning = false;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.ProccesValue = 100;
        }

    }
}