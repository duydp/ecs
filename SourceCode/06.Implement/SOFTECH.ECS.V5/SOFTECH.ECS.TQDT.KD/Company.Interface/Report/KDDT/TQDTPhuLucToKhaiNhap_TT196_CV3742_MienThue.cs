using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.Utils;
#endif
using System.Collections.Generic;
using System.Linq;
namespace Company.Interface.Report
{
    public partial class TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue
        : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public List<Company.KDT.SHARE.QuanLyChungTu.Container> ContCollection = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        //public Company.Interface.Report.ReportViewTKNTQDTFormTT196 report;
        public int SoToKhai;
        public DateTime NgayDangKy;

        public bool BanLuuHaiQuan = false;
        public bool MienThueNK = false;
        public bool MienThueGTGT = false;
        public bool InMaHang = false;
        public int SoDongHang;
        public int Phulucso;
        public bool isCuaKhau = true;
        public bool inTriGiaTT = false;
        public bool inThueBVMT = false;
        public bool ToKhaiTaiCho = false;
        public bool IsToKhaiTaiCho = false;
        #if KD_V3 || KD_V4
        int formatSoLuong = GlobalSettings.SoThapPhan.SoLuongHMD;
        int formatDonGiaNT = GlobalSettings.SoThapPhan.DonGiaNT;
        int formatTriGiaNT = GlobalSettings.SoThapPhan.TriGiaNT;
        #elif GC_V3 || GC_V4
        int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
        int formatDonGiaNT = Company.KDT.SHARE.Components.Globals.DonGiaNT;
        int formatTriGiaNT = Company.KDT.SHARE.Components.Globals.TriGiaNT;
        #elif SXXK_V3 || SXXK_V4
        int formatSoLuong = GlobalSettings.SoThapPhan.LuongSP;
        int formatDonGiaNT = Company.KDT.SHARE.Components.Globals.DonGiaNT;
        int formatTriGiaNT = Company.KDT.SHARE.Components.Globals.TriGiaNT;
        #endif
        public TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue()
        {
            InitializeComponent();
           
        }
        public string GetChiCucHQCK()
        {
            if (isCuaKhau)
            {
                string chicuc = NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                return chicuc;
            }
            else
                return string.Empty;
        }
        public void BindReport(string pls)
        {
            try
            {
                if (IsToKhaiTaiCho)
                {
                    lblLoaiToKhai.Text = "Nhập khẩu tại chỗ";
                }
                ResetEmpty();
                float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));

                this.PrintingSystem.ShowMarginsWarning = false;
               // int pluc = report.index;

                //Set ngay mac dinh
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Thong tin chinh
                //Chi cục HD đăng ký
                lblChiCucHQDangKy.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                lblChiCucHQDangKy.Font = new Font("Times New Roman", sizeFont);

                //Chi cục Hải quan cửa khẩu nhập
                /*lblChiCucHQCuaKhau.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);*/
                //DonViHaiQuan.GetName(this.TKMD.CuaKhau_ID);
                if (!ToKhaiTaiCho)
                {
                    lblChiCucHQCuaKhau.Text = GetChiCucHQCK();
                    lblChiCucHQCuaKhau.Font = new Font("Times New Roman", sizeFont);
                }
                else
                {
                    lblTieuDeGocPhai.Text = "HQ/2012 - PLTKDTNK-TC";
                    lblChiCuc.Text = "";
                    lblChiCucHQCuaKhau.Text = "";
                }

                //So luong phu luc to khai
                lblSoPhuLucToKhai.Text = string.Format("{00}", Phulucso);

                //Số tờ khai
                if (this.TKMD.SoToKhai > 0)
                    lblSoToKhai.Text = this.TKMD.SoToKhai.ToString("N0");

                //Ngày giờ đăng ký
                if (this.TKMD.NgayDangKy > minDate)
                    lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgayDangKy.Text = "";
                // Ma Loai Hinh
                string stlh = "";
                stlh = LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
                lblLoaiHinh.Font = new Font("Times New Roman", sizeFont);

                #endregion


                #region Thong tin hang
                if (this.HMDCollection != null && this.HMDCollection.Count > 0)
                {
//                     if (true)
//                     {
                        List<HangMauDich> datasource = new List<HangMauDich>();
                        foreach (HangMauDich hmd in HMDCollection)
                        {
                            HangMauDich hangMD = new HangMauDich();
                            hangMD.MaPhu = hmd.MaPhu;
                            hangMD.SoThuTuHang = hmd.SoThuTuHang;
                            hangMD.TenHang = InMaHang ? hmd.TenHang + "/" + hmd.MaPhu : hmd.TenHang;
                            hangMD.MaHS = hmd.MaHS;
                            hangMD.NuocXX_ID = hmd.NuocXX_ID;
                            hangMD.SoLuong = hmd.SoLuong;
                            hangMD.DVT_ID = hmd.DVT_ID;
                            hangMD.DonGiaKB = hmd.DonGiaKB;
                            hangMD.TriGiaKB = hmd.TriGiaKB;
                            hangMD.TriGiaTT = hmd.TriGiaTT;
                            hangMD.ThueSuatXNK = hmd.ThueSuatXNK;
                            hangMD.ThueXNK = hmd.ThueXNK;
                            hangMD.TriGiaThuKhac = hmd.TriGiaThuKhac;
                            hangMD.TyLeThuKhac = hmd.TyLeThuKhac;
#if !GC_V4
                            hangMD.CheDoUuDai = !string.IsNullOrEmpty(hmd.CheDoUuDai) ? LoaiCO.GetName(hmd.CheDoUuDai) : "";
#endif
                            datasource.Add(hangMD);
                        }
                        //while (datasource.Count > 0 && datasource.Count < 7)
                        //{
                        //    datasource.Add(new HangMauDich { SoThuTuHang = HMDCollection[HMDCollection.Count - 1].SoThuTuHang + 1 });
                        //}
                        //  DetailHangHoa.DataSource = HMDCollection;

                        //                     if (datasource.Count < 7)
                        //                     {
                        while (datasource.Count >= 0 && datasource.Count < 20)
                        {
                            datasource.Add(new HangMauDich { SoThuTuHang = datasource[datasource.Count - 1].SoThuTuHang + 1 });
                        }
                        //                    }
                        DetailHangHoa.DataSource = datasource;
//                     }
//                     else
//                     {
//                         int soTTHang = 0;
// 
//                         while (HMDCollection.Count >= 0 && HMDCollection.Count < 7)
//                         {
//                             if (HMDCollection.Count == 0)
//                             {
//                                 HMDCollection.Add(new HangMauDich { SoThuTuHang = soTTHang + 1 });
//                             }
//                             else
//                                 HMDCollection.Add(new HangMauDich { SoThuTuHang = HMDCollection[HMDCollection.Count - 1].SoThuTuHang + 1 });
//                         }
//                         DetailHangHoa.DataSource = HMDCollection;
                  /*  }*/
                    lblSTTHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoThuTuHang");
                    lblMoTaHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "TenHang");
                    lblMaHSHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "MaHS");
                    lblXuatXuHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "NuocXX_ID");
                    lblLuongHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "SoLuong", Company.KDT.SHARE.Components.Globals.FormatNumber(formatSoLuong, true));
                    lblDVTHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "DVT_ID");
                    lblDonGiaHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "DonGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(formatDonGiaNT, true));
                    lblTGNTHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "TriGiaKB", Company.KDT.SHARE.Components.Globals.FormatNumber(formatTriGiaNT, true));
                    lblCheDoUuDaiHang.DataBindings.Add("Text", DetailHangHoa.DataSource, "CheDoUuDai");
                }

                #endregion

                #region Container
//                if (!GlobalSettings.IsKhongDungBangKeCont)
//                {
//                    if (TKMD.VanTaiDon != null && TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count <= 4)
//                    {
//                        for (int i = 0; i < this.ContCollection.Count; i++)
//                        {
//                            fillContainer(xrTable4, i);
//                        }
//#if KD_V4 || SXXK_V4 || GC_V4
//                        //Tong trong luong container
//                        double sumTrongLuong = TKMD.VanTaiDon.ContainerCollection.Sum(x => x.TrongLuong);
//                        lblTongTrongLuong.Text = string.Format("{0:N3}", sumTrongLuong);
//#endif
//                    }
//                }
//                else
//                {
//                    if (ContCollection != null & ContCollection.Count > 0)
//                        for (int i = 0; i < this.ContCollection.Count; i++)
//                        {
//                            fillContainer(xrTable4, i);
//#if KD_V4 || SXXK_V4 || GC_V4
//                            double sumTrongLuong = ContCollection.Sum(x => x.TrongLuong);
//                            lblTongTrongLuong.Text = string.Format("{0:N3}", sumTrongLuong);
//#endif
//                        }
//                }

                #endregion

                //Ngay thang nam in to khai
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                    lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayThangNam.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }


        }
         #region dien thong tin container
        private void fillContainer(XRTable tableCont, int contItem)
        {
            //int phulucso = report.index;
            XRControl control = new XRControl();
            Company.KDT.SHARE.QuanLyChungTu.Container cont = this.ContCollection[contItem];

            control = tableCont.Rows[contItem].Controls["lblSTTCont" + (contItem + 1)];
            if (!GlobalSettings.IsKhongDungBangKeCont)
                control.Text = (contItem + 1 + ((Phulucso - 1) * 4)).ToString();
            else
                control.Text = (contItem + 1 + (Phulucso * 4)).ToString();
            control = tableCont.Rows[contItem].Controls["lblSoHieuCont" + (contItem + 1)];
            control.Text = cont.SoHieu + " / " + cont.Seal_No;//TKMD.VanTaiDon.ContainerCollection[contItem].SoHieu;
#if KD_V4 || SXXK_V4 || GC_V4
            int countSoLuongKien = TKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == cont.SoHieu).GroupBy(x => x.SoHieuKien).Count();
            control = tableCont.Rows[contItem].Controls["lblSoLuongKienCont" + (contItem + 1)];
            control.Text = string.Format("{0:00}", cont.SoKien);
            double countTrongLuong = TKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == cont.SoHieu).Sum(x => x.TrongLuong);
            control = tableCont.Rows[contItem].Controls["lblTrongLuongCont" + (contItem + 1)];
            control.Text = string.Format("{0:0}", cont.TrongLuong);
#endif

        }
// 
        #endregion

     
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        private void ResetEmpty()
        {
            try
            {

                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan cua khau
                lblChiCucHQCuaKhau.Text = "";

                //Số tờ khai
                lblSoToKhai.Text = "";

                //Ngày giờ đăng ký
                lblNgayDangKy.Text = "";

                //So luong phu luc to khai
                lblSoPhuLucToKhai.Text = "";

                // Ma Loai Hinh
                lblLoaiHinh.Text = "";


            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void lblXuatXuHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell nuocXX = (XRTableCell)sender;

            if (!string.IsNullOrEmpty(nuocXX.Text))
                nuocXX.Text = Nuoc.GetName(nuocXX.Text.Trim());
            else
                nuocXX.Text = string.Empty;
        }

        private void lblDVTHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell dvt = (XRTableCell)sender;

            if (!string.IsNullOrEmpty(dvt.Text))
                dvt.Text = DonViTinh.GetName(dvt.Text.Trim());
            else
                dvt.Text = string.Empty;
        }

        private void lblCheDoUuDaiHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
//             XRTableCell CheDo = (XRTableCell)sender;
// 
//             if (!string.IsNullOrEmpty(CheDo.Text))
//                 CheDo.Text = LoaiCO.GetName(CheDo.Text.Trim());
//             else
//                 CheDo.Text = string.Empty;
        }
        private void xtcHang_LuongHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            double temp = 1;
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text == "0" || (double.TryParse(cell.Text, out temp) && temp == 0))
            {
                cell.Text = string.Empty;

            }
        }
        private void lblTGNTHang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            double temp = 1;
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text == "0" || (double.TryParse(cell.Text, out temp) && temp == 0))
            {
                cell.Text = string.Empty;

            }
        }
    }
}
