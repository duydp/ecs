﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseCalculateSampleDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseCalculateSampleDataForm));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.rdTinhLuongNPL = new Janus.Windows.EditControls.UIRadioButton();
            this.rdTinhLuongSPXuat = new Janus.Windows.EditControls.UIRadioButton();
            this.rdTinhDinhMuc = new Janus.Windows.EditControls.UIRadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtLuongNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtLuongSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDinhMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDinhMucChung = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTyLeHH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 437), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 437);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 413);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 413);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(849, 437);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.rdTinhLuongNPL);
            this.uiGroupBox3.Controls.Add(this.rdTinhLuongSPXuat);
            this.uiGroupBox3.Controls.Add(this.rdTinhDinhMuc);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.txtLuongNPL);
            this.uiGroupBox3.Controls.Add(this.txtLuongSP);
            this.uiGroupBox3.Controls.Add(this.txtDinhMuc);
            this.uiGroupBox3.Controls.Add(this.txtDinhMucChung);
            this.uiGroupBox3.Controls.Add(this.txtTyLeHH);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 116);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(849, 275);
            this.uiGroupBox3.TabIndex = 5;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // rdTinhLuongNPL
            // 
            this.rdTinhLuongNPL.AutoSize = true;
            this.rdTinhLuongNPL.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTinhLuongNPL.Location = new System.Drawing.Point(28, 104);
            this.rdTinhLuongNPL.Name = "rdTinhLuongNPL";
            this.rdTinhLuongNPL.Size = new System.Drawing.Size(128, 24);
            this.rdTinhLuongNPL.TabIndex = 17;
            this.rdTinhLuongNPL.Text = "Tính lượng NPL";
            this.rdTinhLuongNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.rdTinhLuongNPL.CheckedChanged += new System.EventHandler(this.rdTinhLuongNPL_CheckedChanged);
            // 
            // rdTinhLuongSPXuat
            // 
            this.rdTinhLuongSPXuat.AutoSize = true;
            this.rdTinhLuongSPXuat.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTinhLuongSPXuat.Location = new System.Drawing.Point(28, 66);
            this.rdTinhLuongSPXuat.Name = "rdTinhLuongSPXuat";
            this.rdTinhLuongSPXuat.Size = new System.Drawing.Size(153, 24);
            this.rdTinhLuongSPXuat.TabIndex = 17;
            this.rdTinhLuongSPXuat.Text = "Tính lượng SP xuất";
            this.rdTinhLuongSPXuat.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.rdTinhLuongSPXuat.CheckedChanged += new System.EventHandler(this.rdTinhLuongSPXuat_CheckedChanged);
            // 
            // rdTinhDinhMuc
            // 
            this.rdTinhDinhMuc.AutoSize = true;
            this.rdTinhDinhMuc.Checked = true;
            this.rdTinhDinhMuc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTinhDinhMuc.Location = new System.Drawing.Point(28, 30);
            this.rdTinhDinhMuc.Name = "rdTinhDinhMuc";
            this.rdTinhDinhMuc.Size = new System.Drawing.Size(121, 24);
            this.rdTinhDinhMuc.TabIndex = 17;
            this.rdTinhDinhMuc.TabStop = true;
            this.rdTinhDinhMuc.Text = "Tính định mức";
            this.rdTinhDinhMuc.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.rdTinhDinhMuc.CheckedChanged += new System.EventHandler(this.rdTinhDinhMuc_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(252, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "Lượng NPL nhập khẩu :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(252, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 19);
            this.label2.TabIndex = 14;
            this.label2.Text = "Lượng SP xuất khẩu";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(252, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 19);
            this.label9.TabIndex = 14;
            this.label9.Text = "Định mức";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(252, 181);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 19);
            this.label1.TabIndex = 13;
            this.label1.Text = "Định mức chung";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(252, 145);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 19);
            this.label10.TabIndex = 13;
            this.label10.Text = "Tỷ lệ HH (%)";
            // 
            // txtLuongNPL
            // 
            this.txtLuongNPL.BackColor = System.Drawing.Color.White;
            this.txtLuongNPL.DecimalDigits = 8;
            this.txtLuongNPL.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongNPL.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtLuongNPL.Location = new System.Drawing.Point(435, 29);
            this.txtLuongNPL.Name = "txtLuongNPL";
            this.txtLuongNPL.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtLuongNPL.Size = new System.Drawing.Size(212, 27);
            this.txtLuongNPL.TabIndex = 15;
            this.txtLuongNPL.Text = "0";
            this.txtLuongNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtLuongNPL.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLuongNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLuongNPL.TextChanged += new System.EventHandler(this.txtLuongNPL_TextChanged);
            // 
            // txtLuongSP
            // 
            this.txtLuongSP.BackColor = System.Drawing.Color.White;
            this.txtLuongSP.DecimalDigits = 8;
            this.txtLuongSP.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongSP.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtLuongSP.Location = new System.Drawing.Point(435, 65);
            this.txtLuongSP.Name = "txtLuongSP";
            this.txtLuongSP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtLuongSP.Size = new System.Drawing.Size(212, 27);
            this.txtLuongSP.TabIndex = 15;
            this.txtLuongSP.Text = "0";
            this.txtLuongSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtLuongSP.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLuongSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLuongSP.TextChanged += new System.EventHandler(this.txtLuongSP_TextChanged);
            // 
            // txtDinhMuc
            // 
            this.txtDinhMuc.BackColor = System.Drawing.Color.White;
            this.txtDinhMuc.DecimalDigits = 8;
            this.txtDinhMuc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDinhMuc.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDinhMuc.Location = new System.Drawing.Point(435, 103);
            this.txtDinhMuc.Name = "txtDinhMuc";
            this.txtDinhMuc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDinhMuc.Size = new System.Drawing.Size(212, 27);
            this.txtDinhMuc.TabIndex = 15;
            this.txtDinhMuc.Text = "0";
            this.txtDinhMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtDinhMuc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDinhMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDinhMuc.TextChanged += new System.EventHandler(this.txtDinhMuc_TextChanged);
            // 
            // txtDinhMucChung
            // 
            this.txtDinhMucChung.BackColor = System.Drawing.Color.White;
            this.txtDinhMucChung.DecimalDigits = 8;
            this.txtDinhMucChung.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDinhMucChung.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDinhMucChung.Location = new System.Drawing.Point(435, 177);
            this.txtDinhMucChung.Name = "txtDinhMucChung";
            this.txtDinhMucChung.ReadOnly = true;
            this.txtDinhMucChung.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDinhMucChung.Size = new System.Drawing.Size(212, 27);
            this.txtDinhMucChung.TabIndex = 16;
            this.txtDinhMucChung.Text = "0";
            this.txtDinhMucChung.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtDinhMucChung.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDinhMucChung.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTyLeHH
            // 
            this.txtTyLeHH.BackColor = System.Drawing.Color.White;
            this.txtTyLeHH.DecimalDigits = 8;
            this.txtTyLeHH.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyLeHH.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTyLeHH.Location = new System.Drawing.Point(435, 141);
            this.txtTyLeHH.Name = "txtTyLeHH";
            this.txtTyLeHH.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTyLeHH.Size = new System.Drawing.Size(212, 27);
            this.txtTyLeHH.TabIndex = 16;
            this.txtTyLeHH.Text = "0";
            this.txtTyLeHH.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTyLeHH.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyLeHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyLeHH.TextChanged += new System.EventHandler(this.txtTyLeHH_TextChanged);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 391);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(849, 46);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(750, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(87, 25);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(849, 116);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(10, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(839, 72);
            this.label4.TabIndex = 14;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // WareHouseCalculateSampleDataForm
            // 
            this.ClientSize = new System.Drawing.Size(1055, 443);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseCalculateSampleDataForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tính toán dữ liệu Định mức sản phẩm , Nguyên phụ liệu";
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongNPL;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongSP;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDinhMuc;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDinhMucChung;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTyLeHH;
        private Janus.Windows.EditControls.UIRadioButton rdTinhLuongNPL;
        private Janus.Windows.EditControls.UIRadioButton rdTinhLuongSPXuat;
        private Janus.Windows.EditControls.UIRadioButton rdTinhDinhMuc;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label4;
    }
}
