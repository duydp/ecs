﻿namespace Convert64
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btConvert = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.rtbSource = new System.Windows.Forms.RichTextBox();
            this.rtbDestination = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSaveFile = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLayNoiDungXml = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConvertFull = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboDate = new System.Windows.Forms.ComboBox();
            this.cboIDMsg = new System.Windows.Forms.ComboBox();
            this.chTrapDb = new System.Windows.Forms.CheckBox();
            this.btnLayHetXml = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btConvert
            // 
            this.btConvert.Location = new System.Drawing.Point(6, 19);
            this.btConvert.Name = "btConvert";
            this.btConvert.Size = new System.Drawing.Size(77, 23);
            this.btConvert.TabIndex = 1;
            this.btConvert.Text = "Convert";
            this.btConvert.UseVisualStyleBackColor = true;
            this.btConvert.Click += new System.EventHandler(this.btConvert_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 88);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(77, 17);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Sang 64bit";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // rtbSource
            // 
            this.rtbSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbSource.Location = new System.Drawing.Point(0, 0);
            this.rtbSource.Name = "rtbSource";
            this.rtbSource.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.rtbSource.Size = new System.Drawing.Size(321, 393);
            this.rtbSource.TabIndex = 4;
            this.rtbSource.Text = "";
            // 
            // rtbDestination
            // 
            this.rtbDestination.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbDestination.Location = new System.Drawing.Point(116, 3);
            this.rtbDestination.Name = "rtbDestination";
            this.rtbDestination.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbDestination.Size = new System.Drawing.Size(307, 390);
            this.rtbDestination.TabIndex = 5;
            this.rtbDestination.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Xml  files (*.xml)|*.xml|All files (*.*)|*.*";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(73, 7);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(273, 20);
            this.txtFileName.TabIndex = 6;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Location = new System.Drawing.Point(352, 5);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFile.TabIndex = 7;
            this.btnSelectFile.Text = "Chọn tệp tin";
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Tên tệp tin:";
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.Location = new System.Drawing.Point(453, 5);
            this.btnSaveFile.Name = "btnSaveFile";
            this.btnSaveFile.Size = new System.Drawing.Size(77, 23);
            this.btnSaveFile.TabIndex = 9;
            this.btnSaveFile.Text = "Lưu file đích";
            this.btnSaveFile.UseVisualStyleBackColor = true;
            this.btnSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Xml  files (*.xml)|*.xml|All files (*.*)|*.*";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "ID MSG:";
            // 
            // btnLayNoiDungXml
            // 
            this.btnLayNoiDungXml.Location = new System.Drawing.Point(5, 184);
            this.btnLayNoiDungXml.Name = "btnLayNoiDungXml";
            this.btnLayNoiDungXml.Size = new System.Drawing.Size(102, 23);
            this.btnLayNoiDungXml.TabIndex = 12;
            this.btnLayNoiDungXml.Text = "Lấy noi dung Xml";
            this.btnLayNoiDungXml.UseVisualStyleBackColor = true;
            this.btnLayNoiDungXml.Click += new System.EventHandler(this.btnLayNoiDungXml_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnConvertFull);
            this.groupBox1.Controls.Add(this.btConvert);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Location = new System.Drawing.Point(3, 255);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(114, 115);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chuyển đổi";
            // 
            // btnConvertFull
            // 
            this.btnConvertFull.Location = new System.Drawing.Point(7, 48);
            this.btnConvertFull.Name = "btnConvertFull";
            this.btnConvertFull.Size = new System.Drawing.Size(77, 34);
            this.btnConvertFull.TabIndex = 4;
            this.btnConvertFull.Text = "Convert Nguyên bản";
            this.btnConvertFull.UseVisualStyleBackColor = true;
            this.btnConvertFull.Click += new System.EventHandler(this.btnConvertFull_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboDate);
            this.groupBox2.Controls.Add(this.cboIDMsg);
            this.groupBox2.Controls.Add(this.chTrapDb);
            this.groupBox2.Controls.Add(this.btnLayHetXml);
            this.groupBox2.Controls.Add(this.btnLayNoiDungXml);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(113, 246);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Truy Message";
            // 
            // cboDate
            // 
            this.cboDate.FormattingEnabled = true;
            this.cboDate.Location = new System.Drawing.Point(5, 51);
            this.cboDate.Name = "cboDate";
            this.cboDate.Size = new System.Drawing.Size(81, 21);
            this.cboDate.TabIndex = 5;
            this.cboDate.SelectedIndexChanged += new System.EventHandler(this.cboDate_SelectedIndexChanged);
            // 
            // cboIDMsg
            // 
            this.cboIDMsg.FormattingEnabled = true;
            this.cboIDMsg.Location = new System.Drawing.Point(5, 104);
            this.cboIDMsg.Name = "cboIDMsg";
            this.cboIDMsg.Size = new System.Drawing.Size(81, 21);
            this.cboIDMsg.TabIndex = 5;
            // 
            // chTrapDb
            // 
            this.chTrapDb.Checked = true;
            this.chTrapDb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chTrapDb.Location = new System.Drawing.Point(8, 131);
            this.chTrapDb.Name = "chTrapDb";
            this.chTrapDb.Size = new System.Drawing.Size(80, 36);
            this.chTrapDb.TabIndex = 16;
            this.chTrapDb.Text = "Lay du lieu tu TrapDB";
            this.chTrapDb.UseVisualStyleBackColor = true;
            // 
            // btnLayHetXml
            // 
            this.btnLayHetXml.Location = new System.Drawing.Point(5, 213);
            this.btnLayHetXml.Name = "btnLayHetXml";
            this.btnLayHetXml.Size = new System.Drawing.Size(102, 23);
            this.btnLayHetXml.TabIndex = 12;
            this.btnLayHetXml.Text = "Lấy het Xml";
            this.btnLayHetXml.UseVisualStyleBackColor = true;
            this.btnLayHetXml.Click += new System.EventHandler(this.btnLayHetXml_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Date:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(1, 34);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rtbSource);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel2.Controls.Add(this.rtbDestination);
            this.splitContainer1.Size = new System.Drawing.Size(751, 393);
            this.splitContainer1.SplitterDistance = 321;
            this.splitContainer1.TabIndex = 15;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 428);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSelectFile);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.btnSaveFile);
            this.Name = "frmMain";
            this.Text = "Convert base 64 bit";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btConvert;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.RichTextBox rtbSource;
        private System.Windows.Forms.RichTextBox rtbDestination;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveFile;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnLayNoiDungXml;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chTrapDb;
        private System.Windows.Forms.Button btnConvertFull;
        private System.Windows.Forms.ComboBox cboIDMsg;
        private System.Windows.Forms.ComboBox cboDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLayHetXml;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}

