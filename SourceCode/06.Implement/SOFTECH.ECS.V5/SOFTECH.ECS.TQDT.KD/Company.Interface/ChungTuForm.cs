﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
namespace Company.Interface
{
    public partial class ChungTuForm : BaseForm
    {
        public ChungTuCollection collection;
        public Company.KD.BLL.KDT.ChungTu ctDetail;
        private int stt=0;
        private Image img;
        public string MaLoaiHinh= "";
        public ChungTuForm()
        {
            InitializeComponent();
            
        }
        private byte[] GetArrayFromImagen(Image imagen)
        {
            MemoryStream ms = new MemoryStream();
            imagen.Save(ms, ImageFormat.Jpeg);
            byte[] matriz = ms.ToArray();

            return matriz;
        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            if (ctDetail == null)
                ctDetail = new Company.KD.BLL.KDT.ChungTu();
            ctDetail.TenChungTu = uiComboBox1.Text;
            ctDetail.SoBanChinh = Convert.ToInt16(txtSoBanChinh.Text);
            ctDetail.SoBanSao = Convert.ToInt16(txtSoBanPhu.Text);
            ctDetail.FileUpLoad = txtFileUpload.Text;
            if (ctDetail.FileUpLoad.Trim().Length > 0)
            {
                FileStream file =new FileStream(ctDetail.FileUpLoad,FileMode.Open,FileAccess.Read);
                ctDetail.NoiDung=new byte[file.Length];
                file.Read(ctDetail.NoiDung, 0, (int)file.Length);
            }
            collection.Add(ctDetail);
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.Edit)
            {
                if(ctDetail!=null)
                    collection.Add(ctDetail);
            }
            this.Close();
        }

        private void ChungTuForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.Edit)
            {
                uiComboBox1.Text = ctDetail.TenChungTu;
                txtFileUpload.Text = ctDetail.FileUpLoad;
                txtSoBanChinh.Text = ctDetail.SoBanChinh.ToString();
                txtSoBanPhu.Text = ctDetail.SoBanSao.ToString();
            }
            else
                if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
                {
                    btnAddNew.Visible = false;
                }            
        }

        private void txtFileUpload_ButtonClick(object sender, EventArgs e)
        {
            DialogResult kq = openFileDialog1.ShowDialog(this);
            if (kq == DialogResult.OK)
            {
                txtFileUpload.Text = openFileDialog1.FileName;
            }
        }

     
    }
}
