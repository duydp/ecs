-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]
	@Master_ID bigint,
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TEANguoiXNK]
(
	[Master_ID],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_ID,
	@MaNguoiXNK,
	@TenNguoiXNK,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TEANguoiXNK]
SET
	[Master_ID] = @Master_ID,
	[MaNguoiXNK] = @MaNguoiXNK,
	[TenNguoiXNK] = @TenNguoiXNK,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TEANguoiXNK] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TEANguoiXNK] 
		SET
			[Master_ID] = @Master_ID,
			[MaNguoiXNK] = @MaNguoiXNK,
			[TenNguoiXNK] = @TenNguoiXNK,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TEANguoiXNK]
		(
			[Master_ID],
			[MaNguoiXNK],
			[TenNguoiXNK],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_ID,
			@MaNguoiXNK,
			@TenNguoiXNK,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TEANguoiXNK]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TEANguoiXNK] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEANguoiXNK]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TEANguoiXNK] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEANguoiXNK]	

GO

