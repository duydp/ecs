﻿using System;
using System.Drawing;
using Company.BLL.DuLieuChuan;

using Company.BLL.SXXK.ToKhai;

using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL;

namespace Company.Interface.SXXK
{
    public partial class HangMauDichRegisterForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private decimal clg;
        private decimal dgnt;
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public HangMauDich HMD = null;
        public string LoaiHangHoa;
        private decimal luong;

        public string MaNguyenTe;
        public string MaNuocXX;
        public string NhomLoaiHinh = string.Empty;
        private decimal st_clg;

        //-----------------------------------------------------------------------------------------				
        private decimal tgnt;
        private decimal tgtt_gtgt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tl_clg;
        private decimal tongTGKB;
        private decimal ts_gtgt;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal tt_gtgt;

        private decimal tt_nk;
        private decimal tt_ttdb;
        public decimal TyGiaTT;
        public ToKhaiMauDich TKMD;
        public bool create = false;
        //-----------------------------------------------------------------------------------------

        public HangMauDichRegisterForm()
        {
            InitializeComponent();
        }

        private void khoitao_GiaoDien()
        {
            if (this.NhomLoaiHinh.Substring(1, 2) == "GC" || this.NhomLoaiHinh.Substring(1, 2) == "SX")
            {
                txtMaHang.ButtonStyle = EditButtonStyle.Ellipsis;
                txtTenHang.ReadOnly = cbDonViTinh.ReadOnly = true;
                txtMaHang.BackColor = txtTenHang.BackColor = cbDonViTinh.BackColor = Color.FromArgb(255, 255, 192);
            }
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
        }

        private decimal tinhthue()
        {
            this.tgnt = this.dgnt * this.luong;
            this.tgtt_nk = this.tgnt * this.TyGiaTT;
            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }

        private decimal tinhthue2()
        {
            this.tgnt = this.dgnt * this.luong;
            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Ceiling(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg);

            return Math.Ceiling(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg);
        }

        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in HMDCollection)
            {
                if (hmd.MaPhu.Trim() == maHang) return true;
            }
            return false;
        }
        private bool KiemTraToKhaiDaPhanBo(string MaSP)
        {
            Company.BLL.SXXK.PhanBoToKhaiXuat pbTKX = new Company.BLL.SXXK.PhanBoToKhaiXuat();
            pbTKX.SoToKhaiXuat = TKMD.SoToKhai;
            pbTKX.MaHaiQuanXuat = TKMD.MaHaiQuan;
            pbTKX.MaLoaiHinhXuat = TKMD.MaLoaiHinh;
            pbTKX.NamDangKyXuat = TKMD.NamDangKy;
            pbTKX.MaSP = MaSP;

            if (Company.BLL.SXXK.PhanBoToKhaiXuat.SelectBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat(TKMD.MaHaiQuan, TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDangKy).Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            bool ok = true;
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
            npl.SoToKhai = HMD.SoToKhai;
            npl.MaNPL = HMD.MaPhu;
            npl.MaHaiQuan = HMD.MaHaiQuan;
            npl.MaLoaiHinh = HMD.MaLoaiHinh;
            npl.NamDangKy = HMD.NamDangKy;
            if (npl.CheckMaNPLDaThanhKhoan())
            {
                ShowMessage("Nguyên phụ liệu '" + npl.MaNPL + "' của tờ khai " + npl.SoToKhai + "/" + npl.NamDangKy + " đã thanh khoản máy, không thể sửa.", false);
                return;
            }
            HangMauDich HMDCu = new HangMauDich();
            HMDCu.SoToKhai = HMD.SoToKhai;
            HMDCu.SoThuTuHang = HMD.SoThuTuHang;
            HMDCu.MaHaiQuan = HMD.MaHaiQuan;
            HMDCu.MaLoaiHinh = HMD.MaLoaiHinh;
            HMDCu.NamDangKy = HMD.NamDangKy;
            HMDCu.Load();

            cvError.Validate();
            if (!cvError.IsValid) return;

            if (!txtMaHS_Leave()) return;

            if (TKMD.TrangThai == 0 && TKMD.MaLoaiHinh.StartsWith("X"))
            {
                //string st = ShowMessage("Tờ khai xuất này đã được phân bổ. Không thể chỉnh sửa dữ liệu được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", false);
                string st = MLMessages("Tờ khai xuất này đã được phân bổ. Không thể chỉnh sửa dữ liệu được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", "MSG_TK03", "", false);
                return;
            }
            else if (TKMD.MaLoaiHinh.StartsWith("N"))
            {
                Company.BLL.SXXK.PhanBoToKhaiXuat pbTKX = Company.BLL.SXXK.PhanBoToKhaiNhap.SelectToKhaiXuatDuocPhanBoDauTienChoToKhaiNhap(GlobalSettings.MA_HAI_QUAN, TKMD.MaLoaiHinh, TKMD.SoToKhai, TKMD.NamDangKy);
                if (pbTKX != null)
                {
                    //ShowMessage("Tờ khai nhập này đã được phân bổ. Không thể chỉnh sửa dữ liệu được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", false);
                    MLMessages("Tờ khai nhập này đã được phân bổ. Không thể chỉnh sửa dữ liệu được. Hãy xóa phân bổ trước khi chỉnh sửa dữ liệu.", "MSG_TK03", "", false);
                    {
                        return;
                    }
                }
            }
            this.HMDCollection.Remove(HMD);
            if (checkMaHangExit(txtMaHang.Text.Trim()))
            {
                //ShowMessage("Mặt hàng này đã được chọn.", false);
                MLMessages("Mặt hàng này đã được chọn.", "MSG_THK51", "", false);
                if (HMD.MaPhu.Trim() != "")
                    this.HMDCollection.Add(HMD);
                return;
            }

            HMD.MaHS = txtMaHS.Text;
            HMD.MaPhu = txtMaHang.Text.Trim();
            HMD.TenHang = txtTenHang.Text.Trim();
            HMD.NuocXX_ID = ctrNuocXX.Ma;
            HMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            HMD.SoToKhai = TKMD.SoToKhai;
            HMD.MaLoaiHinh = TKMD.MaLoaiHinh;
            HMD.NamDangKy = TKMD.NamDangKy;
            HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            HMD.SoLuong = Convert.ToDecimal(txtLuong.Value);
            HMD.DonGiaKB = Convert.ToDecimal(txtDGNT.Value);
            HMD.TriGiaKB = Convert.ToDecimal(txtTGNT.Value);
            HMD.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
            HMD.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
            HMD.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
            HMD.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
            HMD.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
            HMD.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), MidpointRounding.AwayFromZero);
            HMD.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value), MidpointRounding.AwayFromZero);
            HMD.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value), MidpointRounding.AwayFromZero);
            HMD.DonGiaTT = HMD.TriGiaTT / HMD.SoLuong;
            if (HMDCu.SoLuong + HMD.ThueXNK != HMD.ThueXNK + HMD.SoLuong) HMD.PhuThu = 1;

            if (chkMienThue.Checked)
                HMD.MienThue = 1;
            else
                HMD.MienThue = 0;
            this.HMDCollection.Add(HMD);
            // Tính thuế.
            //hmd.TinhThue(this.TyGiaTT);

            //this.HMDCollection.Add(hmd);

            this.tongTGKB += Convert.ToDecimal(txtTGNT.Value);
            //lblTongTGKB.Text = "Tổng trị giá khai báo: " + this.tongTGKB.ToString("N");
            if (GlobalSettings.NGON_NGU == "0")
            {
                lblTongTGKB.Text = "Tổng trị giá khai báo: " + this.tongTGKB.ToString("N");
            }
            else
            {

                lblTongTGKB.Text = "Total of value declarate: " + this.tongTGKB.ToString("N");
            }

            if (create)
            {
                int k = 0;
                foreach (HangMauDich hang in this.HMDCollection)
                {
                    if (k < hang.SoThuTuHang)
                        k = hang.SoThuTuHang;
                }
                HMD.SoThuTuHang = (short)(k + 1);
            }


            //dgList.DataSource = this.HMDCollection;
            try
            {
                this.TKMD.InsertUpdateFullHang(HMD, create);
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtLuong.Value = 0;
            txtDGNT.Value = 0;
            HMD = new HangMauDich();
            create = true;
        }

        //-----------------------------------------------------------------------------------------
        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (LoaiHangHoa == "N")
            {
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.Ma = txtMaHang.Text.Trim();
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma.Trim();
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten.Trim();
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    // epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                    }
                    else
                    {
                        epError.SetError(txtMaHang, "This material haven't exist.");
                    }
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                }
            }
            else
            {
                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = txtMaHang.Text.Trim();
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ; ;
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma.Trim();
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten.Trim();
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    //epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                    }
                    else
                    {
                        epError.SetError(txtMaHang, "This product haven't exist.");
                    }
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                }
            }
        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();

            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            this.tongTGKB = 0;
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                // Tính lại thuế.
                //hmd.TinhThue(this.TyGiaTT);
                // Tổng trị giá khai báo.
                this.tongTGKB += hmd.TriGiaKB;
            }
            dgList.DataSource = this.HMDCollection;
            //lblTongTGKB.Text = string.Format("Tổng trị giá khai báo: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
            //lblTyGiaTT.Text = "Tỷ giá VNĐ: " + this.TyGiaTT.ToString("N");
            if (GlobalSettings.NGON_NGU == "0")
            {
                lblTongTGKB.Text = string.Format("Tổng trị giá khai báo: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");
            }
            else
            {
                lblTongTGKB.Text = string.Format("Total of value declaration: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                lblTyGiaTT.Text = "Tax rate: " + this.TyGiaTT.ToString("N");
            }

            if (HMD != null && HMD.SoToKhai > 0)
            {
                txtMaHang.Text = this.HMD.MaPhu.Trim();
                txtTenHang.Text = this.HMD.TenHang.Trim();
                txtMaHS.Text = this.HMD.MaHS;
                cbDonViTinh.SelectedValue = this.HMD.DVT_ID;
                ctrNuocXX.Ma = this.HMD.NuocXX_ID;
                txtDGNT.Value = this.dgnt = this.HMD.DonGiaKB;
                txtLuong.Value = this.luong = this.HMD.SoLuong;
                txtTGNT.Value = this.HMD.TriGiaKB;
                txtTS_NK.Value = this.ts_nk = this.HMD.ThueSuatXNK;
                txtTS_TTDB.Value = this.ts_ttdb = this.HMD.ThueSuatTTDB;
                txtTS_GTGT.Value = this.ts_gtgt = this.HMD.ThueSuatGTGT;
                txtTL_CLG.Value = this.tl_clg = this.HMD.TyLeThuKhac;
                txtTGTT_NK.Value = HMD.TriGiaTT;
                this.ts_nk = this.HMD.ThueSuatXNK / 100;
                this.ts_ttdb = this.HMD.ThueSuatTTDB / 100;
                this.ts_gtgt = this.HMD.ThueSuatGTGT / 100;
                this.tl_clg = this.HMD.TyLeThuKhac / 100;
                this.tinhthue2();
            }
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            {
                uiButton1.Visible = false;
            }
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            this.luong = Convert.ToDecimal(txtLuong.Value);
            this.tinhthue();
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            this.tinhthue();
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.tinhthue2();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.tinhthue2();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tinhthue2();
        }

        private void txtTS_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            this.tinhthue2();
        }

        //-----------------------------------------------------------------------------------------------


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }

        private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        {
            grbThue.Enabled = !chkMienThue.Checked;
            txtTS_NK.Value = 0;
            txtTS_TTDB.Value = 0;
            txtTS_GTGT.Value = 0;
            txtTL_CLG.Value = 0;
            this.tinhthue();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh)
            {
                case "NSX":
                    NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
                    f.CalledForm = "HangMauDichForm";
                    f.MaHaiQuan = this.MaHaiQuan.Trim();
                    f.ShowDialog();
                    if (f.NguyenPhuLieuSelected.Ma.Trim() != "")
                    {
                        txtMaHang.Text = f.NguyenPhuLieuSelected.Ma.Trim();
                        txtTenHang.Text = f.NguyenPhuLieuSelected.Ten.Trim();
                        txtMaHS.Text = f.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = f.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "XSX":
                    if (this.LoaiHangHoa == "N")
                    {
                        NguyenPhuLieuRegistedForm fNPLX = new NguyenPhuLieuRegistedForm();
                        fNPLX.CalledForm = "HangMauDichForm";
                        fNPLX.MaHaiQuan = this.MaHaiQuan.Trim();
                        fNPLX.ShowDialog();
                        if (fNPLX.NguyenPhuLieuSelected.Ma.Trim() != "")
                        {
                            txtMaHang.Text = fNPLX.NguyenPhuLieuSelected.Ma.Trim();
                            txtTenHang.Text = fNPLX.NguyenPhuLieuSelected.Ten.Trim();
                            txtMaHS.Text = fNPLX.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = fNPLX.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                        }
                    }
                    else
                    {
                        SanPhamRegistedForm fSPX = new SanPhamRegistedForm();
                        fSPX.CalledForm = "HangMauDichForm";
                        fSPX.MaHaiQuan = this.MaHaiQuan.Trim();
                        fSPX.ShowDialog();
                        if (fSPX.SanPhamSelected.Ma.Trim() != "")
                        {
                            txtMaHang.Text = fSPX.SanPhamSelected.Ma.Trim();
                            txtTenHang.Text = fSPX.SanPhamSelected.Ten.Trim();
                            txtMaHS.Text = fSPX.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = fSPX.SanPhamSelected.DVT_ID.PadRight(3);
                        }
                    }
                    break;

            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    this.HMD = (HangMauDich)e.Row.DataRow;
                    if (HMD != null && HMD.SoToKhai > 0)
                    {
                        txtMaHang.Text = this.HMD.MaPhu.Trim();
                        txtTenHang.Text = this.HMD.TenHang.Trim();
                        txtMaHS.Text = this.HMD.MaHS;
                        cbDonViTinh.SelectedValue = this.HMD.DVT_ID;


                        txtDGNT.Value = this.dgnt = this.HMD.DonGiaKB;
                        txtLuong.Value = this.luong = this.HMD.SoLuong;
                        txtTGNT.Value = this.HMD.TriGiaKB;
                        txtTS_NK.Value = this.ts_nk = this.HMD.ThueSuatXNK;
                        txtTS_TTDB.Value = this.ts_ttdb = this.HMD.ThueSuatTTDB;
                        txtTS_GTGT.Value = this.ts_gtgt = this.HMD.ThueSuatGTGT;
                        txtTL_CLG.Value = this.tl_clg = this.HMD.TyLeThuKhac;
                        txtTGTT_NK.Value = this.HMD.TriGiaTT;
                        this.ts_nk = this.HMD.ThueSuatXNK / 100;
                        this.ts_ttdb = this.HMD.ThueSuatTTDB / 100;
                        this.ts_gtgt = this.HMD.ThueSuatGTGT / 100;
                        this.tl_clg = this.HMD.TyLeThuKhac / 100;
                        create = false;
                        this.tinhthue2();
                    }

                }
                break;
            }
        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue2();
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            txtMaHS_Leave();
        }

        private bool txtMaHS_Leave()
        {
            epError.SetError(txtMaHS, string.Empty);

            if (!Company.BLL.Utils.MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);

                if (GlobalSettings.NGON_NGU == "0")
                {
                    epError.SetError(txtMaHS, "Mã HS không hợp lệ.");
                }
                else
                {
                    epError.SetError(txtMaHS, "HS code is invalid.");
                }

                return false;
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }

            string MoTa = Company.BLL.Utils.MaHS.CheckExist(txtMaHS.Text);
            if (MoTa == "")
            {
                epError.SetIconPadding(txtMaHS, -8);

                if (GlobalSettings.NGON_NGU == "0")
                {
                    epError.SetError(txtMaHS, "Mã HS không có trong danh mục mã HS.");
                }
                else
                {
                    epError.SetError(txtMaHS, "HS code haven't exist in HS list.");
                }

                return false;
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }

            return true;
        }
    }
}
