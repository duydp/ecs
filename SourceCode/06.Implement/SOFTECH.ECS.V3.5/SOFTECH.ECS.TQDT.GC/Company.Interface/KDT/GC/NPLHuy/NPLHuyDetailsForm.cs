﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
namespace Company.Interface.KDT.GC
{
    public partial class NPLHuyDetailsForm : BaseForm
    {
        public NPLHuyDangKy NPLHuyDK = new NPLHuyDangKy();
        public NPLHuy HuyNPL = new NPLHuy();
        public long HopDong_ID;
       // bool isEdit = false;
        public NPLHuyDetailsForm()
        {
            InitializeComponent();
        }
        private void CungUngForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            txtMaNPL.Text = HuyNPL.MaNPL;
            txtTenNPL.Text = HuyNPL.TenNPL;
            txtSoLuong.ReadOnly = false;
            txtSoLuong.Text = HuyNPL.LuongNPLHuy.ToString();
            ucCategoryAllowEmpty1.Code = HuyNPL.DVT_ID;
            // txtSoLuong.Text = CungUngNPL.LuongCungUng.ToString();
            //dgList.DataSource = NPLHuy.SelectDynamic(" Master_ID = "+NPLHuyDK.ID,"").Tables[0];
            //dgList.Refetch();
            //decimal tongluongNPL = 0;

            //foreach ( GridEXRow item in dgList.GetDataRows())
            //{
            //    if(item.RowType== RowType.Record)
            //    tongluongNPL = tongluongNPL + Convert.ToDecimal(item.Cells["SoLuongNPL"].Text);
            //}
            //txtSoLuong.Value = tongluongNPL;
        }
        private void SetCommand()
        {
            int TrangThai = NPLHuyDK.TrangThaiXuLy;
            //if (CungUngDK.TrangThaiXuLy == -1 || CungUngDK.TrangThaiXuLy == 2)
            //{
            //    cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    btnXoa.Enabled = true;
            //}
            //else
            //{
            //    cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    btnXoa.Enabled = false;
            //}

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    List<KDT_GC_CungUng_Detail> ItemColl = new List<KDT_GC_CungUng_Detail>();
            //    if (dgList.GetRows().Length < 0) return;
            //    if (items.Count <= 0) return;
            //    if (ShowMessage("Bạn có muốn xóa Sản phẩm này không?", true) == "Yes")
            //    {
            //        foreach (GridEXSelectedItem i in items)
            //        {
            //            if (i.RowType == RowType.Record)
            //            {
            //                ItemColl.Add((KDT_GC_CungUng_Detail)i.GetRow().DataRow);
            //            }

            //        }
            //        foreach (KDT_GC_CungUng_Detail item in ItemColl)
            //        {
            //            if (item.ID > 0)
            //            {
            //                KDT_GC_CungUng_ChungTu.DeleteDynamic("Master_ID = " + item.ID);
            //                item.Delete();
            //            }
            //            HuyNPL.CungUngDetail_List.Remove(item);
            //        }
            //        dgList.Refetch();
            //    }

            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}

        }



        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //if (e.Row.RowType == RowType.Record)
            //{
            //    KDT_GC_CungUng_Detail cungUngDetail = new KDT_GC_CungUng_Detail();
            //    cungUngDetail = (KDT_GC_CungUng_Detail)dgList.CurrentRow.DataRow;
            //    CungUngDetailForm f = new CungUngDetailForm();
            //    f.CU_Detail = cungUngDetail;
            //    f.MaNPL = HuyNPL.MaNPL;
            //    f.HopDong_ID = NPLHuyDK.HopDong_ID;
            //    f.tragThai = NPLHuyDK.TrangThaiXuLy;
            //    f.ShowDialog();
            //    LoadData();
            //}
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            //CungUngSelectTKForm f = new CungUngSelectTKForm();
            //f.CungUng = HuyNPL;
            //f.IDHopDong = NPLHuyDK.HopDong_ID;
            //f.ShowDialog();
            //LoadData();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (NPLHuyDK.ID == 0)
                {
                    NPLHuyDK.TrangThaiXuLy = -1;
                    NPLHuyDK.Insert();
                }
                else
                    NPLHuyDK.Update();
                HuyNPL.Master_ID = NPLHuyDK.ID;
                HuyNPL.LuongNPLHuy = Convert.ToDecimal(txtSoLuong.Value);
                //if (HuyNPL.ID == 0)
                //    NPLHuyDK.NPLCungUng_List.Add(HuyNPL);
                HuyNPL.InsertUpdate();
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lưu không thành công :" + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //if (e.Row.RowType == RowType.Record)
            //{
            //    KDT_GC_CungUng_Detail cu = new KDT_GC_CungUng_Detail();
            //    cu = (KDT_GC_CungUng_Detail)e.Row.DataRow;
            //    decimal luong = 0;
            //    foreach (KDT_GC_CungUng_ChungTu item in cu.CungUngCT_List)
            //    {
            //        luong = luong + Convert.ToDecimal( item.SoLuong);

            //    }
            //    e.Row.Cells["SoLuongNPL"].Text = Convert.ToString(luong);


        }


    }


}

