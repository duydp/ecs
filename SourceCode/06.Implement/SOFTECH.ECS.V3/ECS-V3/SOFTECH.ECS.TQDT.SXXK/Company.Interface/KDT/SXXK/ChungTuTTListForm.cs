﻿using System;
using Company.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using Company.KDT.SHARE.Components;
using System.Windows.Forms;
using Company.Interface;
using System.Collections.Generic;

namespace Company.Interface.KDT.SXXK
{
    public partial class ChungTuTTListForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();

        public ChungTuTTListForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            ChungTuTTForm cttt = new ChungTuTTForm();
            cttt.ChungTuTT.Master_ID = HSTL.ID;
            cttt.ShowDialog(this);
            if (cttt.ChungTuTT.ID > 0)
            {
                HSTL.ChungTuTTs.Add(cttt.ChungTuTT);
            }
            FillData();
        }

        private void ChungTuTTListForm_Load(object sender, EventArgs e)
        {            
            FillData();            
        }
        private void FillData()
        {
            try
            {
                HSTL.LoadChungTuTTs();
                dgList.DataSource = HSTL.ChungTuTTs;
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuTT chungtuTT = (ChungTuTT)e.Row.DataRow;
            chungtuTT.LoadChungTuChiTiets();
            ChungTuTTForm cttt = new ChungTuTTForm();
            cttt.ChungTuTT.Master_ID = HSTL.ID;
            cttt.ChungTuTT = chungtuTT;
            cttt.ShowDialog(this);
            FillData();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            if (ShowMessage("Bạn có thật sự muốn xóa chứng từ này không?", true) != "Yes") return;

            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    ChungTuTT cttt = (ChungTuTT)i.GetRow().DataRow;
                    cttt.Delete();
                }
            }
            FillData();
        }

    }
}
