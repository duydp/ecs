﻿using System.ComponentModel;
using System.Windows.Forms;
using Company.Interface.Controls;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface
{
    partial class HangGCCTForm
    {
        private UIGroupBox uiGroupBox1;
        private Label label16;
        private Label label17;
        private Label label19;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label27;
        private UIGroupBox uiGroupBox2;
        private UIGroupBox grbThue;
        private Label label1;
        private Label label5;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private UIGroupBox uiGroupBox4;
        private UIButton btnGhi;
        private NumericEditBox txtDGNT;
        private NumericEditBox txtTGNT;
        private EditBox txtMaHang;
        private EditBox txtTenHang;
        private EditBox txtMaHS;
        private NumericEditBox txtTienThue_GTGT;
        private NumericEditBox txtTienThue_NK;
        private NumericEditBox txtTGTT_TTDB;
        private NumericEditBox txtTGTT_GTGT;
        private NumericEditBox txtTGTT_NK;
        private NumericEditBox txtTS_GTGT;
        private NumericEditBox txtTS_NK;
        private NumericEditBox txtTienThue_TTDB;
        private NumericEditBox txtTS_TTDB;
        private Label label18;
        private ToolTip toolTip1;
        private Label lblTyGiaTT;
        private Label lblNguyenTe_DGNT;
        private Label lblNguyenTe_TGNT;
        private ErrorProvider epError;
        private NumericEditBox txtCLG;
        private Label label9;
        private NumericEditBox txtTL_CLG;
        private NumericEditBox txtTien_CLG;
        private NuocHControl ctrNuocXX;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HangGCCTForm));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SaoChepCha = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblTyGiaTT = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDonGiaTuyetDoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTriGiaKB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrNuocXX = new Company.Interface.Controls.NuocHControl();
            this.lblNguyenTe_TGNT = new System.Windows.Forms.Label();
            this.lblNguyenTe_DGNT = new System.Windows.Forms.Label();
            this.txtDGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbLoaiCo = new Janus.Windows.EditControls.UIComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtNhanHieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtQuyCach = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtThanhPhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtModel = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtTenHangSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSMoRong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtMaHangSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkThueTuyetDoi = new Janus.Windows.EditControls.UICheckBox();
            this.cbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.chNoCo = new Janus.Windows.EditControls.UICheckBox();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grbThue = new Janus.Windows.EditControls.UIGroupBox();
            this.txtPhuThu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtTyLeThuKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.chkMienThue = new Janus.Windows.EditControls.UICheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTSVatGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTSTTDBGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTSXNKGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTongSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTienThue_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTien_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtCLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTL_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.revMaHS = new Company.Controls.CustomValidation.RegularExpressionValidator();
            this.vrTGTT = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeValidator1 = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeValidator2 = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeValidator3 = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeValidator4 = new Company.Controls.CustomValidation.RangeValidator();
            this.rvLuong = new Company.Controls.CustomValidation.RangeValidator();
            this.rvDGNT = new Company.Controls.CustomValidation.RangeValidator();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThue)).BeginInit();
            this.grbThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vrTGTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvDGNT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(718, 681);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.BorderColor = System.Drawing.Color.White;
            this.uiGroupBox1.Controls.Add(this.btnAddNew);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.lblTyGiaTT);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.grbThue);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(718, 681);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(379, 461);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 11;
            this.btnAddNew.Text = "Tạo mới";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(548, 461);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 2;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(631, 461);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(12, 490);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(694, 166);
            this.dgList.TabIndex = 4;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaoChepCha});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(156, 26);
            // 
            // SaoChepCha
            // 
            this.SaoChepCha.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepCha.Image")));
            this.SaoChepCha.Name = "SaoChepCha";
            this.SaoChepCha.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.SaoChepCha.Size = new System.Drawing.Size(155, 22);
            this.SaoChepCha.Text = "Sao chép";
            this.SaoChepCha.Click += new System.EventHandler(this.SaoChepCha_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.AutoSize = true;
            this.lblTyGiaTT.BackColor = System.Drawing.Color.Transparent;
            this.lblTyGiaTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTyGiaTT.Location = new System.Drawing.Point(15, 463);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Size = new System.Drawing.Size(98, 13);
            this.lblTyGiaTT.TabIndex = 8;
            this.lblTyGiaTT.Text = "Tỷ giá tính thuế:";
            this.lblTyGiaTT.Click += new System.EventHandler(this.lblTyGiaTT_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.txtDonGiaTuyetDoi);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaKB);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.txtLuong);
            this.uiGroupBox4.Controls.Add(this.ctrNuocXX);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_TGNT);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_DGNT);
            this.uiGroupBox4.Controls.Add(this.txtDGNT);
            this.uiGroupBox4.Controls.Add(this.txtTGNT);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 177);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(694, 104);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Phần số lượng";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(224, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "(USD)";
            // 
            // txtDonGiaTuyetDoi
            // 
            this.txtDonGiaTuyetDoi.DecimalDigits = 20;
            this.txtDonGiaTuyetDoi.Enabled = false;
            this.txtDonGiaTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTuyetDoi.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaTuyetDoi.Location = new System.Drawing.Point(99, 46);
            this.txtDonGiaTuyetDoi.Name = "txtDonGiaTuyetDoi";
            this.txtDonGiaTuyetDoi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaTuyetDoi.Size = new System.Drawing.Size(119, 21);
            this.txtDonGiaTuyetDoi.TabIndex = 2;
            this.txtDonGiaTuyetDoi.Text = "0";
            this.txtDonGiaTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtDonGiaTuyetDoi, "Đơn giá khai báo");
            this.txtDonGiaTuyetDoi.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaTuyetDoi.VisualStyleManager = this.vsmMain;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 50);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(90, 13);
            this.label28.TabIndex = 4;
            this.label28.Text = "Đơn giá tuyệt đối";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(224, 80);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 13);
            this.label23.TabIndex = 12;
            this.label23.Text = "(VNĐ)";
            // 
            // txtTriGiaKB
            // 
            this.txtTriGiaKB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTriGiaKB.DecimalDigits = 0;
            this.txtTriGiaKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaKB.Location = new System.Drawing.Point(99, 75);
            this.txtTriGiaKB.Name = "txtTriGiaKB";
            this.txtTriGiaKB.ReadOnly = true;
            this.txtTriGiaKB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaKB.Size = new System.Drawing.Size(119, 21);
            this.txtTriGiaKB.TabIndex = 4;
            this.txtTriGiaKB.TabStop = false;
            this.txtTriGiaKB.Text = "0";
            this.txtTriGiaKB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTriGiaKB, "Trị giá khai báo");
            this.txtTriGiaKB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaKB.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(6, 80);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Trị giá khai báo";
            // 
            // txtLuong
            // 
            this.txtLuong.DecimalDigits = 20;
            this.txtLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtLuong.Location = new System.Drawing.Point(100, 15);
            this.txtLuong.MaxLength = 15;
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLuong.Size = new System.Drawing.Size(119, 21);
            this.txtLuong.TabIndex = 0;
            this.txtLuong.Text = "0";
            this.txtLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtLuong.VisualStyleManager = this.vsmMain;
            this.txtLuong.Click += new System.EventHandler(this.txtLuong_Click);
            this.txtLuong.Leave += new System.EventHandler(this.txtDGNT_Leave);
            // 
            // ctrNuocXX
            // 
            this.ctrNuocXX.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXX.ErrorMessage = "\"Nước xuất xứ\" không được bỏ trống.";
            this.ctrNuocXX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXX.Location = new System.Drawing.Point(401, 75);
            this.ctrNuocXX.Ma = "";
            this.ctrNuocXX.Name = "ctrNuocXX";
            this.ctrNuocXX.ReadOnly = false;
            this.ctrNuocXX.Size = new System.Drawing.Size(198, 22);
            this.ctrNuocXX.TabIndex = 5;
            this.ctrNuocXX.VisualStyleManager = this.vsmMain;
            // 
            // lblNguyenTe_TGNT
            // 
            this.lblNguyenTe_TGNT.AutoSize = true;
            this.lblNguyenTe_TGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_TGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_TGNT.Location = new System.Drawing.Point(527, 48);
            this.lblNguyenTe_TGNT.Name = "lblNguyenTe_TGNT";
            this.lblNguyenTe_TGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_TGNT.TabIndex = 9;
            this.lblNguyenTe_TGNT.Text = "Mã nguyên tệ";
            // 
            // lblNguyenTe_DGNT
            // 
            this.lblNguyenTe_DGNT.AutoSize = true;
            this.lblNguyenTe_DGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_DGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_DGNT.Location = new System.Drawing.Point(526, 20);
            this.lblNguyenTe_DGNT.Name = "lblNguyenTe_DGNT";
            this.lblNguyenTe_DGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_DGNT.TabIndex = 4;
            this.lblNguyenTe_DGNT.Text = "Mã nguyên tệ";
            // 
            // txtDGNT
            // 
            this.txtDGNT.DecimalDigits = 20;
            this.txtDGNT.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDGNT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDGNT.Location = new System.Drawing.Point(401, 15);
            this.txtDGNT.MaxLength = 15;
            this.txtDGNT.Name = "txtDGNT";
            this.txtDGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDGNT.Size = new System.Drawing.Size(119, 21);
            this.txtDGNT.TabIndex = 1;
            this.txtDGNT.Text = "0";
            this.txtDGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtDGNT, "Đơn giá khai báo");
            this.txtDGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDGNT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDGNT.VisualStyleManager = this.vsmMain;
            // 
            // txtTGNT
            // 
            this.txtTGNT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGNT.DecimalDigits = 20;
            this.txtTGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGNT.FormatString = "G20";
            this.txtTGNT.Location = new System.Drawing.Point(401, 42);
            this.txtTGNT.Name = "txtTGNT";
            this.txtTGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGNT.Size = new System.Drawing.Size(119, 21);
            this.txtTGNT.TabIndex = 3;
            this.txtTGNT.Text = "0";
            this.txtTGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTGNT, "Trị giá khai báo");
            this.txtTGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGNT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGNT.VisualStyleManager = this.vsmMain;
            this.txtTGNT.Leave += new System.EventHandler(this.txtDGNT_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(296, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Đơn giá nguyên tệ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(296, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Trị giá nguyên tệ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số lượng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(296, 74);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Nước xuất xứ";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbLoaiCo);
            this.uiGroupBox2.Controls.Add(this.label36);
            this.uiGroupBox2.Controls.Add(this.label37);
            this.uiGroupBox2.Controls.Add(this.txtNhanHieu);
            this.uiGroupBox2.Controls.Add(this.txtQuyCach);
            this.uiGroupBox2.Controls.Add(this.label38);
            this.uiGroupBox2.Controls.Add(this.label34);
            this.uiGroupBox2.Controls.Add(this.label35);
            this.uiGroupBox2.Controls.Add(this.txtThanhPhan);
            this.uiGroupBox2.Controls.Add(this.txtModel);
            this.uiGroupBox2.Controls.Add(this.label33);
            this.uiGroupBox2.Controls.Add(this.label32);
            this.uiGroupBox2.Controls.Add(this.txtTenHangSX);
            this.uiGroupBox2.Controls.Add(this.txtMaHSMoRong);
            this.uiGroupBox2.Controls.Add(this.label31);
            this.uiGroupBox2.Controls.Add(this.txtMaHangSX);
            this.uiGroupBox2.Controls.Add(this.chkThueTuyetDoi);
            this.uiGroupBox2.Controls.Add(this.cbDonViTinh);
            this.uiGroupBox2.Controls.Add(this.chNoCo);
            this.uiGroupBox2.Controls.Add(this.txtMaHang);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.txtMaHS);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(694, 163);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Phần tên hàng";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // cbLoaiCo
            // 
            this.cbLoaiCo.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbLoaiCo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLoaiCo.Location = new System.Drawing.Point(588, 98);
            this.cbLoaiCo.Name = "cbLoaiCo";
            this.cbLoaiCo.Size = new System.Drawing.Size(99, 21);
            this.cbLoaiCo.TabIndex = 12;
            this.cbLoaiCo.Tag = "";
            this.cbLoaiCo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbLoaiCo.VisualStyleManager = this.vsmMain;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(231, 99);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(52, 13);
            this.label36.TabIndex = 40;
            this.label36.Text = "Quy cách";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(11, 100);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(55, 13);
            this.label37.TabIndex = 38;
            this.label37.Text = "Nhãn hiệu";
            // 
            // txtNhanHieu
            // 
            this.txtNhanHieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNhanHieu.Location = new System.Drawing.Point(80, 98);
            this.txtNhanHieu.MaxLength = 256;
            this.txtNhanHieu.Name = "txtNhanHieu";
            this.txtNhanHieu.Size = new System.Drawing.Size(139, 21);
            this.txtNhanHieu.TabIndex = 7;
            this.txtNhanHieu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNhanHieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNhanHieu.VisualStyleManager = this.vsmMain;
            // 
            // txtQuyCach
            // 
            this.txtQuyCach.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtQuyCach.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtQuyCach.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuyCach.Location = new System.Drawing.Point(299, 98);
            this.txtQuyCach.MaxLength = 1000;
            this.txtQuyCach.Name = "txtQuyCach";
            this.txtQuyCach.Size = new System.Drawing.Size(197, 21);
            this.txtQuyCach.TabIndex = 8;
            this.txtQuyCach.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtQuyCach.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtQuyCach.VisualStyleManager = this.vsmMain;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(526, 102);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(57, 13);
            this.label38.TabIndex = 36;
            this.label38.Text = "Loại Co nợ";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(231, 127);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(35, 13);
            this.label34.TabIndex = 36;
            this.label34.Text = "Model";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(11, 127);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(64, 13);
            this.label35.TabIndex = 34;
            this.label35.Text = "Thành phần";
            // 
            // txtThanhPhan
            // 
            this.txtThanhPhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPhan.Location = new System.Drawing.Point(80, 125);
            this.txtThanhPhan.MaxLength = 100;
            this.txtThanhPhan.Name = "txtThanhPhan";
            this.txtThanhPhan.Size = new System.Drawing.Size(139, 21);
            this.txtThanhPhan.TabIndex = 9;
            this.txtThanhPhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThanhPhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtThanhPhan.VisualStyleManager = this.vsmMain;
            // 
            // txtModel
            // 
            this.txtModel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtModel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtModel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtModel.Location = new System.Drawing.Point(299, 125);
            this.txtModel.MaxLength = 35;
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(197, 21);
            this.txtModel.TabIndex = 10;
            this.txtModel.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtModel.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtModel.VisualStyleManager = this.vsmMain;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(449, 45);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(79, 13);
            this.label33.TabIndex = 32;
            this.label33.Text = "Mã HS mở rộng";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(231, 71);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 13);
            this.label32.TabIndex = 30;
            this.label32.Text = "Tên hãng SX";
            // 
            // txtTenHangSX
            // 
            this.txtTenHangSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangSX.Location = new System.Drawing.Point(299, 69);
            this.txtTenHangSX.MaxLength = 256;
            this.txtTenHangSX.Name = "txtTenHangSX";
            this.txtTenHangSX.Size = new System.Drawing.Size(197, 21);
            this.txtTenHangSX.TabIndex = 6;
            this.txtTenHangSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenHangSX.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHSMoRong
            // 
            this.txtMaHSMoRong.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHSMoRong.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHSMoRong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSMoRong.Location = new System.Drawing.Point(529, 42);
            this.txtMaHSMoRong.MaxLength = 12;
            this.txtMaHSMoRong.Name = "txtMaHSMoRong";
            this.txtMaHSMoRong.Size = new System.Drawing.Size(158, 21);
            this.txtMaHSMoRong.TabIndex = 4;
            this.txtMaHSMoRong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHSMoRong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHSMoRong.VisualStyleManager = this.vsmMain;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(12, 73);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(63, 13);
            this.label31.TabIndex = 28;
            this.label31.Text = "Mã hãng SX";
            // 
            // txtMaHangSX
            // 
            this.txtMaHangSX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangSX.Location = new System.Drawing.Point(80, 71);
            this.txtMaHangSX.MaxLength = 30;
            this.txtMaHangSX.Name = "txtMaHangSX";
            this.txtMaHangSX.Size = new System.Drawing.Size(139, 21);
            this.txtMaHangSX.TabIndex = 5;
            this.txtMaHangSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHangSX.VisualStyleManager = this.vsmMain;
            // 
            // chkThueTuyetDoi
            // 
            this.chkThueTuyetDoi.BackColor = System.Drawing.Color.Transparent;
            this.chkThueTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkThueTuyetDoi.ForeColor = System.Drawing.Color.Blue;
            this.chkThueTuyetDoi.Location = new System.Drawing.Point(529, 132);
            this.chkThueTuyetDoi.Name = "chkThueTuyetDoi";
            this.chkThueTuyetDoi.Size = new System.Drawing.Size(129, 14);
            this.chkThueTuyetDoi.TabIndex = 13;
            this.chkThueTuyetDoi.Text = "Thuế tuyệt đối";
            this.chkThueTuyetDoi.VisualStyleManager = this.vsmMain;
            this.chkThueTuyetDoi.CheckedChanged += new System.EventHandler(this.uiCheckBox1_CheckedChanged);
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDonViTinh.DisplayMember = "Ten";
            this.cbDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDonViTinh.Location = new System.Drawing.Point(299, 43);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.Size = new System.Drawing.Size(140, 21);
            this.cbDonViTinh.TabIndex = 3;
            this.cbDonViTinh.ValueMember = "ID";
            this.cbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // chNoCo
            // 
            this.chNoCo.BackColor = System.Drawing.Color.Transparent;
            this.chNoCo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chNoCo.ForeColor = System.Drawing.Color.Blue;
            this.chNoCo.Location = new System.Drawing.Point(528, 73);
            this.chNoCo.Name = "chNoCo";
            this.chNoCo.Size = new System.Drawing.Size(130, 14);
            this.chNoCo.TabIndex = 11;
            this.chNoCo.Text = "Nợ Co?";
            // 
            // txtMaHang
            // 
            this.txtMaHang.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHang.Location = new System.Drawing.Point(80, 15);
            this.txtMaHang.MaxLength = 30;
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(139, 21);
            this.txtMaHang.TabIndex = 0;
            this.txtMaHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHang.VisualStyleManager = this.vsmMain;
            this.txtMaHang.ButtonClick += new System.EventHandler(this.txtMaHang_ButtonClick);
            this.txtMaHang.Leave += new System.EventHandler(this.txtMaNPL_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã HS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(230, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên hàng";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(12, 17);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã hàng";
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(299, 13);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(388, 21);
            this.txtTenHang.TabIndex = 1;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHS
            // 
            this.txtMaHS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHS.Location = new System.Drawing.Point(80, 42);
            this.txtMaHS.MaxLength = 12;
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(139, 21);
            this.txtMaHS.TabIndex = 2;
            this.txtMaHS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHS.VisualStyleManager = this.vsmMain;
            this.txtMaHS.TextChanged += new System.EventHandler(this.txtMaHS_TextChanged);
            this.txtMaHS.Leave += new System.EventHandler(this.txtMaHS_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(230, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Đơn vị tính";
            // 
            // grbThue
            // 
            this.grbThue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbThue.BackColor = System.Drawing.Color.Transparent;
            this.grbThue.Controls.Add(this.txtPhuThu);
            this.grbThue.Controls.Add(this.label29);
            this.grbThue.Controls.Add(this.txtTyLeThuKhac);
            this.grbThue.Controls.Add(this.chkMienThue);
            this.grbThue.Controls.Add(this.label30);
            this.grbThue.Controls.Add(this.txtTSVatGiam);
            this.grbThue.Controls.Add(this.txtTSTTDBGiam);
            this.grbThue.Controls.Add(this.txtTSXNKGiam);
            this.grbThue.Controls.Add(this.label21);
            this.grbThue.Controls.Add(this.label24);
            this.grbThue.Controls.Add(this.label25);
            this.grbThue.Controls.Add(this.txtTongSoTienThue);
            this.grbThue.Controls.Add(this.label20);
            this.grbThue.Controls.Add(this.txtTienThue_TTDB);
            this.grbThue.Controls.Add(this.txtTienThue_GTGT);
            this.grbThue.Controls.Add(this.txtTien_CLG);
            this.grbThue.Controls.Add(this.txtTienThue_NK);
            this.grbThue.Controls.Add(this.txtTGTT_TTDB);
            this.grbThue.Controls.Add(this.txtTGTT_GTGT);
            this.grbThue.Controls.Add(this.txtCLG);
            this.grbThue.Controls.Add(this.txtTGTT_NK);
            this.grbThue.Controls.Add(this.txtTL_CLG);
            this.grbThue.Controls.Add(this.txtTS_TTDB);
            this.grbThue.Controls.Add(this.txtTS_GTGT);
            this.grbThue.Controls.Add(this.txtTS_NK);
            this.grbThue.Controls.Add(this.label16);
            this.grbThue.Controls.Add(this.label17);
            this.grbThue.Controls.Add(this.label19);
            this.grbThue.Controls.Add(this.label1);
            this.grbThue.Controls.Add(this.label5);
            this.grbThue.Controls.Add(this.label9);
            this.grbThue.Controls.Add(this.label10);
            this.grbThue.Controls.Add(this.label11);
            this.grbThue.Controls.Add(this.label12);
            this.grbThue.Controls.Add(this.label13);
            this.grbThue.Controls.Add(this.label14);
            this.grbThue.Controls.Add(this.label15);
            this.grbThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbThue.Location = new System.Drawing.Point(13, 287);
            this.grbThue.Name = "grbThue";
            this.grbThue.Size = new System.Drawing.Size(693, 168);
            this.grbThue.TabIndex = 2;
            this.grbThue.Text = "Phần thuế (VND)";
            this.grbThue.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbThue.VisualStyleManager = this.vsmMain;
            // 
            // txtPhuThu
            // 
            this.txtPhuThu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPhuThu.DecimalDigits = 3;
            this.txtPhuThu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuThu.Location = new System.Drawing.Point(183, 126);
            this.txtPhuThu.Name = "txtPhuThu";
            this.txtPhuThu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPhuThu.Size = new System.Drawing.Size(166, 21);
            this.txtPhuThu.TabIndex = 16;
            this.txtPhuThu.TabStop = false;
            this.txtPhuThu.Text = "0.000";
            this.txtPhuThu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhuThu.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtPhuThu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtPhuThu.VisualStyleManager = this.vsmMain;
            this.txtPhuThu.Leave += new System.EventHandler(this.txtPhuThu_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(183, 110);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 13);
            this.label29.TabIndex = 34;
            this.label29.Text = "Tiền thu khác";
            // 
            // txtTyLeThuKhac
            // 
            this.txtTyLeThuKhac.DecimalDigits = 8;
            this.txtTyLeThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyLeThuKhac.Location = new System.Drawing.Point(18, 126);
            this.txtTyLeThuKhac.Name = "txtTyLeThuKhac";
            this.txtTyLeThuKhac.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyLeThuKhac.Size = new System.Drawing.Size(158, 21);
            this.txtTyLeThuKhac.TabIndex = 15;
            this.txtTyLeThuKhac.TabStop = false;
            this.txtTyLeThuKhac.Text = "0.00000000";
            this.txtTyLeThuKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyLeThuKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            524288});
            this.txtTyLeThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyLeThuKhac.VisualStyleManager = this.vsmMain;
            this.txtTyLeThuKhac.Leave += new System.EventHandler(this.txtTyLeThuKhac_Leave);
            // 
            // chkMienThue
            // 
            this.chkMienThue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMienThue.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.chkMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMienThue.ForeColor = System.Drawing.Color.Red;
            this.chkMienThue.Location = new System.Drawing.Point(609, -4);
            this.chkMienThue.Name = "chkMienThue";
            this.chkMienThue.Size = new System.Drawing.Size(80, 23);
            this.chkMienThue.TabIndex = 2;
            this.chkMienThue.Text = "Miễn thuế";
            this.chkMienThue.VisualStyleManager = this.vsmMain;
            this.chkMienThue.CheckedChanged += new System.EventHandler(this.chkMienThue_CheckedChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(15, 110);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(74, 13);
            this.label30.TabIndex = 32;
            this.label30.Text = "Tỷ lệ thu khác";
            // 
            // txtTSVatGiam
            // 
            this.txtTSVatGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSVatGiam.Location = new System.Drawing.Point(183, 81);
            this.txtTSVatGiam.MaxLength = 80;
            this.txtTSVatGiam.Name = "txtTSVatGiam";
            this.txtTSVatGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSVatGiam.TabIndex = 10;
            this.txtTSVatGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSVatGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTSVatGiam.VisualStyleManager = this.vsmMain;
            // 
            // txtTSTTDBGiam
            // 
            this.txtTSTTDBGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSTTDBGiam.Location = new System.Drawing.Point(520, 38);
            this.txtTSTTDBGiam.MaxLength = 80;
            this.txtTSTTDBGiam.Name = "txtTSTTDBGiam";
            this.txtTSTTDBGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSTTDBGiam.TabIndex = 6;
            this.txtTSTTDBGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSTTDBGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTSTTDBGiam.VisualStyleManager = this.vsmMain;
            // 
            // txtTSXNKGiam
            // 
            this.txtTSXNKGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSXNKGiam.Location = new System.Drawing.Point(182, 38);
            this.txtTSXNKGiam.MaxLength = 80;
            this.txtTSXNKGiam.Name = "txtTSXNKGiam";
            this.txtTSXNKGiam.Size = new System.Drawing.Size(49, 21);
            this.txtTSXNKGiam.TabIndex = 2;
            this.txtTSXNKGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTSXNKGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTSXNKGiam.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(182, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "TS Giảm";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(519, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "TS Giảm";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(182, 65);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 13);
            this.label25.TabIndex = 20;
            this.label25.Text = "TS Giảm";
            // 
            // txtTongSoTienThue
            // 
            this.txtTongSoTienThue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTongSoTienThue.DecimalDigits = 0;
            this.txtTongSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTienThue.FormatString = "###,###,###.########";
            this.txtTongSoTienThue.Location = new System.Drawing.Point(475, 125);
            this.txtTongSoTienThue.Name = "txtTongSoTienThue";
            this.txtTongSoTienThue.ReadOnly = true;
            this.txtTongSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTienThue.Size = new System.Drawing.Size(211, 21);
            this.txtTongSoTienThue.TabIndex = 17;
            this.txtTongSoTienThue.TabStop = false;
            this.txtTongSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTongSoTienThue.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(372, 130);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "Tổng số tiền thuế";
            // 
            // txtTienThue_TTDB
            // 
            this.txtTienThue_TTDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_TTDB.DecimalDigits = 0;
            this.txtTienThue_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_TTDB.Location = new System.Drawing.Point(576, 38);
            this.txtTienThue_TTDB.Name = "txtTienThue_TTDB";
            this.txtTienThue_TTDB.ReadOnly = true;
            this.txtTienThue_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_TTDB.Size = new System.Drawing.Size(110, 21);
            this.txtTienThue_TTDB.TabIndex = 7;
            this.txtTienThue_TTDB.TabStop = false;
            this.txtTienThue_TTDB.Text = "0";
            this.txtTienThue_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTienThue_TTDB.VisualStyleManager = this.vsmMain;
            // 
            // txtTienThue_GTGT
            // 
            this.txtTienThue_GTGT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_GTGT.DecimalDigits = 0;
            this.txtTienThue_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_GTGT.Location = new System.Drawing.Point(239, 81);
            this.txtTienThue_GTGT.Name = "txtTienThue_GTGT";
            this.txtTienThue_GTGT.ReadOnly = true;
            this.txtTienThue_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_GTGT.Size = new System.Drawing.Size(110, 21);
            this.txtTienThue_GTGT.TabIndex = 11;
            this.txtTienThue_GTGT.TabStop = false;
            this.txtTienThue_GTGT.Text = "0";
            this.txtTienThue_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTienThue_GTGT.VisualStyleManager = this.vsmMain;
            // 
            // txtTien_CLG
            // 
            this.txtTien_CLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTien_CLG.DecimalDigits = 0;
            this.txtTien_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTien_CLG.Location = new System.Drawing.Point(576, 81);
            this.txtTien_CLG.Name = "txtTien_CLG";
            this.txtTien_CLG.ReadOnly = true;
            this.txtTien_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTien_CLG.Size = new System.Drawing.Size(110, 21);
            this.txtTien_CLG.TabIndex = 14;
            this.txtTien_CLG.TabStop = false;
            this.txtTien_CLG.Text = "0";
            this.txtTien_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTien_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTien_CLG.Visible = false;
            this.txtTien_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTien_CLG.VisualStyleManager = this.vsmMain;
            // 
            // txtTienThue_NK
            // 
            this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTienThue_NK.DecimalDigits = 0;
            this.txtTienThue_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_NK.Location = new System.Drawing.Point(239, 38);
            this.txtTienThue_NK.Name = "txtTienThue_NK";
            this.txtTienThue_NK.ReadOnly = true;
            this.txtTienThue_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_NK.Size = new System.Drawing.Size(107, 21);
            this.txtTienThue_NK.TabIndex = 3;
            this.txtTienThue_NK.TabStop = false;
            this.txtTienThue_NK.Text = "0";
            this.txtTienThue_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTienThue_NK.VisualStyleManager = this.vsmMain;
            this.txtTienThue_NK.Leave += new System.EventHandler(this.txtTienThue_NK_Leave);
            // 
            // txtTGTT_TTDB
            // 
            this.txtTGTT_TTDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGTT_TTDB.DecimalDigits = 0;
            this.txtTGTT_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_TTDB.Location = new System.Drawing.Point(355, 38);
            this.txtTGTT_TTDB.Name = "txtTGTT_TTDB";
            this.txtTGTT_TTDB.ReadOnly = true;
            this.txtTGTT_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_TTDB.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_TTDB.TabIndex = 4;
            this.txtTGTT_TTDB.TabStop = false;
            this.txtTGTT_TTDB.Text = "0";
            this.txtTGTT_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGTT_TTDB.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT_GTGT
            // 
            this.txtTGTT_GTGT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTGTT_GTGT.DecimalDigits = 0;
            this.txtTGTT_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_GTGT.Location = new System.Drawing.Point(18, 81);
            this.txtTGTT_GTGT.Name = "txtTGTT_GTGT";
            this.txtTGTT_GTGT.ReadOnly = true;
            this.txtTGTT_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_GTGT.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_GTGT.TabIndex = 8;
            this.txtTGTT_GTGT.TabStop = false;
            this.txtTGTT_GTGT.Text = "0";
            this.txtTGTT_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGTT_GTGT.VisualStyleManager = this.vsmMain;
            // 
            // txtCLG
            // 
            this.txtCLG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCLG.DecimalDigits = 0;
            this.txtCLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCLG.Location = new System.Drawing.Point(355, 81);
            this.txtCLG.Name = "txtCLG";
            this.txtCLG.ReadOnly = true;
            this.txtCLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCLG.Size = new System.Drawing.Size(108, 21);
            this.txtCLG.TabIndex = 12;
            this.txtCLG.TabStop = false;
            this.txtCLG.Text = "0";
            this.txtCLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtCLG.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT_NK
            // 
            this.txtTGTT_NK.BackColor = System.Drawing.Color.White;
            this.txtTGTT_NK.DecimalDigits = 0;
            this.txtTGTT_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_NK.Location = new System.Drawing.Point(18, 38);
            this.txtTGTT_NK.Name = "txtTGTT_NK";
            this.txtTGTT_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_NK.Size = new System.Drawing.Size(108, 21);
            this.txtTGTT_NK.TabIndex = 0;
            this.txtTGTT_NK.TabStop = false;
            this.txtTGTT_NK.Text = "0";
            this.txtTGTT_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGTT_NK.VisualStyleManager = this.vsmMain;
            this.txtTGTT_NK.TextChanged += new System.EventHandler(this.txtTGTT_NK_TextChanged);
            this.txtTGTT_NK.Leave += new System.EventHandler(this.txtTGTT_NK_Leave);
            // 
            // txtTL_CLG
            // 
            this.txtTL_CLG.DecimalDigits = 3;
            this.txtTL_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTL_CLG.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTL_CLG.Location = new System.Drawing.Point(472, 81);
            this.txtTL_CLG.MaxLength = 10;
            this.txtTL_CLG.Name = "txtTL_CLG";
            this.txtTL_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTL_CLG.Size = new System.Drawing.Size(41, 21);
            this.txtTL_CLG.TabIndex = 13;
            this.txtTL_CLG.Text = "0";
            this.txtTL_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTL_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTL_CLG.Visible = false;
            this.txtTL_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTL_CLG.VisualStyleManager = this.vsmMain;
            this.txtTL_CLG.Leave += new System.EventHandler(this.txtTL_CLG_Leave);
            // 
            // txtTS_TTDB
            // 
            this.txtTS_TTDB.DecimalDigits = 3;
            this.txtTS_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_TTDB.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_TTDB.Location = new System.Drawing.Point(472, 38);
            this.txtTS_TTDB.MaxLength = 10;
            this.txtTS_TTDB.Name = "txtTS_TTDB";
            this.txtTS_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_TTDB.Size = new System.Drawing.Size(41, 21);
            this.txtTS_TTDB.TabIndex = 5;
            this.txtTS_TTDB.Text = "0";
            this.txtTS_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTS_TTDB.VisualStyleManager = this.vsmMain;
            this.txtTS_TTDB.Leave += new System.EventHandler(this.txtTS_TTDB_Leave);
            // 
            // txtTS_GTGT
            // 
            this.txtTS_GTGT.DecimalDigits = 3;
            this.txtTS_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_GTGT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_GTGT.Location = new System.Drawing.Point(135, 81);
            this.txtTS_GTGT.MaxLength = 10;
            this.txtTS_GTGT.Name = "txtTS_GTGT";
            this.txtTS_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_GTGT.Size = new System.Drawing.Size(41, 21);
            this.txtTS_GTGT.TabIndex = 9;
            this.txtTS_GTGT.Text = "0";
            this.txtTS_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTS_GTGT.VisualStyleManager = this.vsmMain;
            this.txtTS_GTGT.Leave += new System.EventHandler(this.txtTS_GTGT_Leave);
            // 
            // txtTS_NK
            // 
            this.txtTS_NK.DecimalDigits = 3;
            this.txtTS_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_NK.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTS_NK.Location = new System.Drawing.Point(135, 38);
            this.txtTS_NK.MaxLength = 10;
            this.txtTS_NK.Name = "txtTS_NK";
            this.txtTS_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_NK.Size = new System.Drawing.Size(41, 21);
            this.txtTS_NK.TabIndex = 1;
            this.txtTS_NK.Text = "0";
            this.txtTS_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTS_NK.VisualStyleManager = this.vsmMain;
            this.txtTS_NK.Leave += new System.EventHandler(this.txtTS_NK_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Trị giá tính thuế NK";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(236, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Tiền thuế NK";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(135, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "TS (%)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(352, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Trị giá tính thuế TTĐB";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Trị giá tính thuế GTGT";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(352, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Chênh lệch giá";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(472, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "TS (%)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(135, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "TS (%)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(472, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Tỷ lệ (%)";
            this.label12.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(573, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Tiền thuế TTĐB";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(236, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Tiền thuế GTGT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(573, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Tiền chênh lệch giá";
            this.label15.Visible = false;
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.Location = new System.Drawing.Point(464, 461);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 1;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenHang;
            this.rfvTen.ErrorMessage = "\"Tên hàng\" không được để trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            this.rfvTen.Tag = "rfvTen";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHS;
            this.rfvMaHS.ErrorMessage = "\"Mã HS\" không được để trống.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // revMaHS
            // 
            this.revMaHS.ControlToValidate = this.txtMaHS;
            this.revMaHS.ErrorMessage = "\"Mã HS\" không hợp lệ.";
            this.revMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("revMaHS.Icon")));
            this.revMaHS.Tag = "revMaHS";
            this.revMaHS.ValidationExpression = "\\d{1,12}";
            // 
            // vrTGTT
            // 
            this.vrTGTT.ControlToValidate = this.txtTGTT_NK;
            this.vrTGTT.ErrorMessage = "Giá trị nhập chưa hợp lệ";
            this.vrTGTT.Icon = ((System.Drawing.Icon)(resources.GetObject("vrTGTT.Icon")));
            this.vrTGTT.MaximumValue = "1000000000000";
            this.vrTGTT.MinimumValue = "0";
            this.vrTGTT.Tag = "vrTGTT";
            this.vrTGTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeValidator1
            // 
            this.rangeValidator1.ControlToValidate = this.txtTS_GTGT;
            this.rangeValidator1.ErrorMessage = "Gía trị nhập chưa hợp lệ";
            this.rangeValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator1.Icon")));
            this.rangeValidator1.MaximumValue = "1000000000000";
            this.rangeValidator1.MinimumValue = "0";
            this.rangeValidator1.Tag = "rangeValidator1";
            this.rangeValidator1.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeValidator2
            // 
            this.rangeValidator2.ControlToValidate = this.txtTS_TTDB;
            this.rangeValidator2.ErrorMessage = "Giá trị nhập chưa hợp lệ";
            this.rangeValidator2.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator2.Icon")));
            this.rangeValidator2.MaximumValue = "1000000000000";
            this.rangeValidator2.MinimumValue = "0";
            this.rangeValidator2.Tag = "rangeValidator2";
            this.rangeValidator2.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeValidator3
            // 
            this.rangeValidator3.ControlToValidate = this.txtTL_CLG;
            this.rangeValidator3.ErrorMessage = "Giá trị nhập chưa hợp lệ";
            this.rangeValidator3.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator3.Icon")));
            this.rangeValidator3.MaximumValue = "1000000000000";
            this.rangeValidator3.MinimumValue = "0";
            this.rangeValidator3.Tag = "rangeValidator3";
            this.rangeValidator3.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeValidator4
            // 
            this.rangeValidator4.ControlToValidate = this.txtTS_NK;
            this.rangeValidator4.ErrorMessage = "Giá trị nhập chưa hợp lệ";
            this.rangeValidator4.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator4.Icon")));
            this.rangeValidator4.MaximumValue = "1000000000000";
            this.rangeValidator4.MinimumValue = "0";
            this.rangeValidator4.Tag = "rangeValidator4";
            this.rangeValidator4.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvLuong
            // 
            this.rvLuong.ControlToValidate = this.txtLuong;
            this.rvLuong.ErrorMessage = "Giá trị không hợp lệ";
            this.rvLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rvLuong.Icon")));
            this.rvLuong.MaximumValue = "1000000000000";
            this.rvLuong.MinimumValue = "0";
            this.rvLuong.Tag = "rvLuong";
            this.rvLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvDGNT
            // 
            this.rvDGNT.ControlToValidate = this.txtDGNT;
            this.rvDGNT.ErrorMessage = "Giá trị không hợp lệ";
            this.rvDGNT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvDGNT.Icon")));
            this.rvDGNT.MaximumValue = "1000000000000";
            this.rvDGNT.MinimumValue = "0";
            this.rvDGNT.Tag = "rvLuong";
            this.rvDGNT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToValidate = this.txtLuong;
            this.cvSoLuong.ErrorMessage = "Giá trị không hợp lệ";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Tag = "cvSoLuong";
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // HangGCCTForm
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(718, 681);
            this.Controls.Add(this.uiGroupBox1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HangGCCTForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin hàng hóa";
            this.Load += new System.EventHandler(this.HangMauDichForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThue)).EndInit();
            this.grbThue.ResumeLayout(false);
            this.grbThue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vrTGTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvDGNT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion        

        private UIComboBox cbDonViTinh;
        private GridEX dgList;
        private ImageList ImageList1;
        private NumericEditBox txtTongSoTienThue;
        private Label label20;
        private UIButton btnClose;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Company.Controls.CustomValidation.RegularExpressionValidator revMaHS;
        private NumericEditBox txtLuong;
        private Label label23;
        private NumericEditBox txtTriGiaKB;
        private Label label22;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem SaoChepCha;
        private UIButton btnXoa;
        private Company.Controls.CustomValidation.RangeValidator vrTGTT;
        private Company.Controls.CustomValidation.RangeValidator rangeValidator1;
        private Company.Controls.CustomValidation.RangeValidator rangeValidator2;
        private Company.Controls.CustomValidation.RangeValidator rangeValidator3;
        private Company.Controls.CustomValidation.RangeValidator rangeValidator4;
        private Company.Controls.CustomValidation.RangeValidator rvLuong;
        private Company.Controls.CustomValidation.RangeValidator rvDGNT;
        private Company.Controls.CustomValidation.CompareValidator cvSoLuong;
        private UICheckBox chkThueTuyetDoi;
        private EditBox txtTSVatGiam;
        private EditBox txtTSTTDBGiam;
        private EditBox txtTSXNKGiam;
        private Label label21;
        private Label label24;
        private Label label25;
        protected UICheckBox chkMienThue;
        private Label label26;
        private NumericEditBox txtDonGiaTuyetDoi;
        private Label label28;
        private NumericEditBox txtPhuThu;
        private Label label29;
        private NumericEditBox txtTyLeThuKhac;
        private Label label30;
        private Label label32;
        private EditBox txtTenHangSX;
        private Label label31;
        private EditBox txtMaHangSX;
        private Label label33;
        private EditBox txtMaHSMoRong;
        private Label label36;
        private Label label37;
        private EditBox txtNhanHieu;
        private EditBox txtQuyCach;
        private Label label34;
        private Label label35;
        private EditBox txtThanhPhan;
        private EditBox txtModel;
        private UIButton btnAddNew;
        private UICheckBox chNoCo;
        private UIComboBox cbLoaiCo;
        private Label label38;
    }
}
