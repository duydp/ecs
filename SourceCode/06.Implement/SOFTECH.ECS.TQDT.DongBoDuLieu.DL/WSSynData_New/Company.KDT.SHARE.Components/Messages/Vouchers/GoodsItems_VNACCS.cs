﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.GoodsItem;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
    [XmlRoot("Declaration")]
   public class GoodsItems_VNACCS
    {
        /// <summary>
        /// Loại chứng từ = 308
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số tham chiếu chứng từ
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày khai báo YYYY-MM-DD HH:mm:ss
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Chức năng của chứng từ = 8
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nơi khai báo
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Số tờ khai 
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

        /// <summary>
        /// Tình trạng của chứng từ
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

        /// <summary>
        /// Ngày đăng ký chứng từ
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        /// <summary>
        /// Hải quan tiếp nhận
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }


        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }

        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }

        /// <summary>
        /// Năm quyêt toán
        /// </summary>
        [XmlElement("finalYear")]
        public string FinalYear { get; set; }

        /// <summary>
        /// Ngày bắt đầu báo cáo
        /// </summary>
        [XmlElement("startDate")]
        public string StartDate { get; set; }


        /// <summary>
        /// Ngày kết thúc báo cáo
        /// </summary>
        [XmlElement("finishDate")]
        public string FinishDate { get; set; }

        /// <summary>
        /// Loại hình báo cáo
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Loại sửa
        /// </summary>
        [XmlElement("updateType")]
        public string UpdateType { get; set; }

        /// <summary>
        /// Đơn vị tính báo cáo
        /// </summary>
        [XmlElement("unit")]
        public string Unit { get; set; }

        /// <summary>
        /// AdditionalDocument
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public Company.KDT.SHARE.Components.AdditionalDocument AdditionalInformation { get; set; }

        /// <summary>
        /// Thông tin về giấy phép
        /// </summary>
        [XmlElement("GoodsItems")]
        public GoodsItems GoodsItems { get; set; }

        /// <summary>
        /// Thông tin về giấy phép
        /// </summary>
        [XmlElement("ContractReferences")]
        public ContractReferences ContractReferences { get; set; }

        /// <summary>
        /// AttachedFile
        /// </summary>
        [XmlElement("AttachedFile")]
        public Company.KDT.SHARE.Components.AttachedFile AttachedFile { get; set; }
    }
}
