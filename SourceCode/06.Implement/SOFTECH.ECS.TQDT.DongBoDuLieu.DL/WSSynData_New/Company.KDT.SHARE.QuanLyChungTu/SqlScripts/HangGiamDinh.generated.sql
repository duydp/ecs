/****** Object:  Table [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]    Script Date: 12/9/2012 11:02:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[HMD_ID] [bigint] NOT NULL,
	[ChungThu_ID] [bigint] NOT NULL,
	[MaPhu] [varchar](50) NULL,
	[TenHang] [nvarchar](255) NULL,
	[MaHS] [varchar](12) NULL,
	[NuocXX_ID] [varchar](5) NULL,
	[SoLuong] [decimal](18, 4) NULL,
	[DVT_ID] [varchar](5) NULL,
	[SoVanTaiDon] [varchar](50) NULL,
	[NgayVanDon] [datetime] NULL,
	[TinhTrangContainer] [bit] NULL,
	[SoHieuContainer] [varchar](50) NULL,
	[GhiChu] [nvarchar](2000) NULL,
 CONSTRAINT [PK_t_KDT_ChungThuGiamDinh_HangGiamDinh] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_ChungThuGiamDinh_HangGiamDinh_t_KDT_ChungThuGiamDinh] FOREIGN KEY([ChungThu_ID])
REFERENCES [dbo].[t_KDT_ChungThuGiamDinh] ([ID])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh] CHECK CONSTRAINT [FK_t_KDT_ChungThuGiamDinh_HangGiamDinh_t_KDT_ChungThuGiamDinh]
GO

ALTER TABLE [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_ChungThuGiamDinh_HangGiamDinh_t_KDT_HangMauDich] FOREIGN KEY([HMD_ID])
REFERENCES [dbo].[t_KDT_HangMauDich] ([ID])
GO

ALTER TABLE [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh] CHECK CONSTRAINT [FK_t_KDT_ChungThuGiamDinh_HangGiamDinh_t_KDT_HangMauDich]
GO



-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_ChungThu_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_ChungThu_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_HMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_HMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_ChungThu_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_ChungThu_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_HMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_HMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Insert]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Insert]
	@HMD_ID bigint,
	@ChungThu_ID bigint,
	@MaPhu varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@NuocXX_ID varchar(5),
	@SoLuong decimal(18, 4),
	@DVT_ID varchar(5),
	@SoVanTaiDon varchar(50),
	@NgayVanDon datetime,
	@TinhTrangContainer bit,
	@SoHieuContainer varchar(50),
	@GhiChu nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
(
	[HMD_ID],
	[ChungThu_ID],
	[MaPhu],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[SoVanTaiDon],
	[NgayVanDon],
	[TinhTrangContainer],
	[SoHieuContainer],
	[GhiChu]
)
VALUES 
(
	@HMD_ID,
	@ChungThu_ID,
	@MaPhu,
	@TenHang,
	@MaHS,
	@NuocXX_ID,
	@SoLuong,
	@DVT_ID,
	@SoVanTaiDon,
	@NgayVanDon,
	@TinhTrangContainer,
	@SoHieuContainer,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Update]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Update]
	@ID bigint,
	@HMD_ID bigint,
	@ChungThu_ID bigint,
	@MaPhu varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@NuocXX_ID varchar(5),
	@SoLuong decimal(18, 4),
	@DVT_ID varchar(5),
	@SoVanTaiDon varchar(50),
	@NgayVanDon datetime,
	@TinhTrangContainer bit,
	@SoHieuContainer varchar(50),
	@GhiChu nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
SET
	[HMD_ID] = @HMD_ID,
	[ChungThu_ID] = @ChungThu_ID,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[MaHS] = @MaHS,
	[NuocXX_ID] = @NuocXX_ID,
	[SoLuong] = @SoLuong,
	[DVT_ID] = @DVT_ID,
	[SoVanTaiDon] = @SoVanTaiDon,
	[NgayVanDon] = @NgayVanDon,
	[TinhTrangContainer] = @TinhTrangContainer,
	[SoHieuContainer] = @SoHieuContainer,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_InsertUpdate]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_InsertUpdate]
	@ID bigint,
	@HMD_ID bigint,
	@ChungThu_ID bigint,
	@MaPhu varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@NuocXX_ID varchar(5),
	@SoLuong decimal(18, 4),
	@DVT_ID varchar(5),
	@SoVanTaiDon varchar(50),
	@NgayVanDon datetime,
	@TinhTrangContainer bit,
	@SoHieuContainer varchar(50),
	@GhiChu nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh] 
		SET
			[HMD_ID] = @HMD_ID,
			[ChungThu_ID] = @ChungThu_ID,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[MaHS] = @MaHS,
			[NuocXX_ID] = @NuocXX_ID,
			[SoLuong] = @SoLuong,
			[DVT_ID] = @DVT_ID,
			[SoVanTaiDon] = @SoVanTaiDon,
			[NgayVanDon] = @NgayVanDon,
			[TinhTrangContainer] = @TinhTrangContainer,
			[SoHieuContainer] = @SoHieuContainer,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
		(
			[HMD_ID],
			[ChungThu_ID],
			[MaPhu],
			[TenHang],
			[MaHS],
			[NuocXX_ID],
			[SoLuong],
			[DVT_ID],
			[SoVanTaiDon],
			[NgayVanDon],
			[TinhTrangContainer],
			[SoHieuContainer],
			[GhiChu]
		)
		VALUES 
		(
			@HMD_ID,
			@ChungThu_ID,
			@MaPhu,
			@TenHang,
			@MaHS,
			@NuocXX_ID,
			@SoLuong,
			@DVT_ID,
			@SoVanTaiDon,
			@NgayVanDon,
			@TinhTrangContainer,
			@SoHieuContainer,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Delete]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_ChungThu_ID]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_ChungThu_ID]
	@ChungThu_ID bigint
AS

DELETE FROM [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
WHERE
	[ChungThu_ID] = @ChungThu_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_HMD_ID]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteBy_HMD_ID]
	@HMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
WHERE
	[HMD_ID] = @HMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Load]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[ChungThu_ID],
	[MaPhu],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[SoVanTaiDon],
	[NgayVanDon],
	[TinhTrangContainer],
	[SoHieuContainer],
	[GhiChu]
FROM
	[dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_ChungThu_ID]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_ChungThu_ID]
	@ChungThu_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[ChungThu_ID],
	[MaPhu],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[SoVanTaiDon],
	[NgayVanDon],
	[TinhTrangContainer],
	[SoHieuContainer],
	[GhiChu]
FROM
	[dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
WHERE
	[ChungThu_ID] = @ChungThu_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_HMD_ID]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectBy_HMD_ID]
	@HMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[ChungThu_ID],
	[MaPhu],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[SoVanTaiDon],
	[NgayVanDon],
	[TinhTrangContainer],
	[SoHieuContainer],
	[GhiChu]
FROM
	[dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]
WHERE
	[HMD_ID] = @HMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectDynamic]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HMD_ID],
	[ChungThu_ID],
	[MaPhu],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[SoVanTaiDon],
	[NgayVanDon],
	[TinhTrangContainer],
	[SoHieuContainer],
	[GhiChu]
FROM [dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectAll]
-- Database: ECS_TQDT_KD_V3.5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, December 9, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungThuGiamDinh_HangGiamDinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMD_ID],
	[ChungThu_ID],
	[MaPhu],
	[TenHang],
	[MaHS],
	[NuocXX_ID],
	[SoLuong],
	[DVT_ID],
	[SoVanTaiDon],
	[NgayVanDon],
	[TinhTrangContainer],
	[SoHieuContainer],
	[GhiChu]
FROM
	[dbo].[t_KDT_ChungThuGiamDinh_HangGiamDinh]	

GO

