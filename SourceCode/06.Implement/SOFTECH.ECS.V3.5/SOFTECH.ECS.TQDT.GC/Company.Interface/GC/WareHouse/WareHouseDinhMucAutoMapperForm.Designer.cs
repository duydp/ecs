﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseDinhMucAutoMapperForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grListSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grListSP_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseDinhMucAutoMapperForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListSP = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblError = new System.Windows.Forms.Label();
            this.uiProgressBar1 = new Janus.Windows.EditControls.UIProgressBar();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnProcess = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(921, 450);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(921, 450);
            this.uiGroupBox2.TabIndex = 12;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.grListSP);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(915, 393);
            this.uiGroupBox8.TabIndex = 6;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListSP
            // 
            this.grListSP.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListSP.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListSP.ColumnAutoResize = true;
            grListSP_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grListSP_DesignTimeLayout_Reference_0.Instance")));
            grListSP_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grListSP_DesignTimeLayout_Reference_0});
            grListSP_DesignTimeLayout.LayoutString = resources.GetString("grListSP_DesignTimeLayout.LayoutString");
            this.grListSP.DesignTimeLayout = grListSP_DesignTimeLayout;
            this.grListSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListSP.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListSP.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListSP.FrozenColumns = 5;
            this.grListSP.GroupByBoxVisible = false;
            this.grListSP.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSP.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListSP.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListSP.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListSP.Hierarchical = true;
            this.grListSP.Location = new System.Drawing.Point(3, 8);
            this.grListSP.Name = "grListSP";
            this.grListSP.RecordNavigator = true;
            this.grListSP.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListSP.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListSP.Size = new System.Drawing.Size(909, 382);
            this.grListSP.TabIndex = 85;
            this.grListSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListSP.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.lblError);
            this.uiGroupBox7.Controls.Add(this.uiProgressBar1);
            this.uiGroupBox7.Controls.Add(this.btnClose);
            this.uiGroupBox7.Controls.Add(this.btnProcess);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 401);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(915, 46);
            this.uiGroupBox7.TabIndex = 5;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.Color.Transparent;
            this.lblError.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(518, 22);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(48, 13);
            this.lblError.TabIndex = 24;
            this.lblError.Text = "lblError";
            // 
            // uiProgressBar1
            // 
            this.uiProgressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiProgressBar1.Location = new System.Drawing.Point(9, 20);
            this.uiProgressBar1.Name = "uiProgressBar1";
            this.uiProgressBar1.Size = new System.Drawing.Size(503, 17);
            this.uiProgressBar1.TabIndex = 23;
            this.uiProgressBar1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(816, 17);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 22;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnProcess.Image")));
            this.btnProcess.Location = new System.Drawing.Point(730, 17);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(80, 23);
            this.btnProcess.TabIndex = 21;
            this.btnProcess.Text = "Thực hiện";
            this.btnProcess.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // WareHouseDinhMucAutoMapperForm
            // 
            this.ClientSize = new System.Drawing.Size(921, 450);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseDinhMucAutoMapperForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tự động Map định mức sang Định mức kho kế toán";
            this.Load += new System.EventHandler(this.WareHouseDinhMucAutoMapperForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.GridEX grListSP;
        private Janus.Windows.EditControls.UIProgressBar uiProgressBar1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnProcess;
        private System.Windows.Forms.Label lblError;
    }
}
