using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.GC.BLL.KDT.GC
{
	public partial class PhuLucHopDong : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKCT_ID { set; get; }
		public string SoPhuLuc { set; get; }
		public DateTime NgayPhuLuc { set; get; }
		public DateTime NgayHetHan { set; get; }
		public string ThongTinKhac { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<PhuLucHopDong> ConvertToCollection(IDataReader reader)
		{
			IList<PhuLucHopDong> collection = new List<PhuLucHopDong>();
			while (reader.Read())
			{
				PhuLucHopDong entity = new PhuLucHopDong();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) entity.TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoPhuLuc"))) entity.SoPhuLuc = reader.GetString(reader.GetOrdinal("SoPhuLuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuLuc"))) entity.NgayPhuLuc = reader.GetDateTime(reader.GetOrdinal("NgayPhuLuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<PhuLucHopDong> collection, long id)
        {
            foreach (PhuLucHopDong item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_PhuLucHopDong VALUES(@TKCT_ID, @SoPhuLuc, @NgayPhuLuc, @NgayHetHan, @ThongTinKhac)";
            string update = "UPDATE t_KDT_GC_PhuLucHopDong SET TKCT_ID = @TKCT_ID, SoPhuLuc = @SoPhuLuc, NgayPhuLuc = @NgayPhuLuc, NgayHetHan = @NgayHetHan, ThongTinKhac = @ThongTinKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhuLucHopDong WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKCT_ID", SqlDbType.BigInt, "TKCT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhuLuc", SqlDbType.NVarChar, "SoPhuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhuLuc", SqlDbType.DateTime, "NgayPhuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKCT_ID", SqlDbType.BigInt, "TKCT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhuLuc", SqlDbType.NVarChar, "SoPhuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhuLuc", SqlDbType.DateTime, "NgayPhuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_PhuLucHopDong VALUES(@TKCT_ID, @SoPhuLuc, @NgayPhuLuc, @NgayHetHan, @ThongTinKhac)";
            string update = "UPDATE t_KDT_GC_PhuLucHopDong SET TKCT_ID = @TKCT_ID, SoPhuLuc = @SoPhuLuc, NgayPhuLuc = @NgayPhuLuc, NgayHetHan = @NgayHetHan, ThongTinKhac = @ThongTinKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhuLucHopDong WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKCT_ID", SqlDbType.BigInt, "TKCT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhuLuc", SqlDbType.NVarChar, "SoPhuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhuLuc", SqlDbType.DateTime, "NgayPhuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKCT_ID", SqlDbType.BigInt, "TKCT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhuLuc", SqlDbType.NVarChar, "SoPhuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhuLuc", SqlDbType.DateTime, "NgayPhuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static PhuLucHopDong Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<PhuLucHopDong> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<PhuLucHopDong> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<PhuLucHopDong> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<PhuLucHopDong> SelectCollectionBy_TKCT_ID(long tKCT_ID)
		{
            IDataReader reader = SelectReaderBy_TKCT_ID(tKCT_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKCT_ID(long tKCT_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_SelectBy_TKCT_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, tKCT_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKCT_ID(long tKCT_ID)
		{
			const string spName = "p_KDT_GC_PhuLucHopDong_SelectBy_TKCT_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, tKCT_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertPhuLucHopDong(long tKCT_ID, string soPhuLuc, DateTime ngayPhuLuc, DateTime ngayHetHan, string thongTinKhac)
		{
			PhuLucHopDong entity = new PhuLucHopDong();	
			entity.TKCT_ID = tKCT_ID;
			entity.SoPhuLuc = soPhuLuc;
			entity.NgayPhuLuc = ngayPhuLuc;
			entity.NgayHetHan = ngayHetHan;
			entity.ThongTinKhac = thongTinKhac;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, TKCT_ID);
			db.AddInParameter(dbCommand, "@SoPhuLuc", SqlDbType.NVarChar, SoPhuLuc);
			db.AddInParameter(dbCommand, "@NgayPhuLuc", SqlDbType.DateTime, NgayPhuLuc.Year <= 1753 ? DBNull.Value : (object) NgayPhuLuc);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<PhuLucHopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (PhuLucHopDong item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdatePhuLucHopDong(long id, long tKCT_ID, string soPhuLuc, DateTime ngayPhuLuc, DateTime ngayHetHan, string thongTinKhac)
		{
			PhuLucHopDong entity = new PhuLucHopDong();			
			entity.ID = id;
			entity.TKCT_ID = tKCT_ID;
			entity.SoPhuLuc = soPhuLuc;
			entity.NgayPhuLuc = ngayPhuLuc;
			entity.NgayHetHan = ngayHetHan;
			entity.ThongTinKhac = thongTinKhac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_PhuLucHopDong_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, TKCT_ID);
			db.AddInParameter(dbCommand, "@SoPhuLuc", SqlDbType.NVarChar, SoPhuLuc);
			db.AddInParameter(dbCommand, "@NgayPhuLuc", SqlDbType.DateTime, NgayPhuLuc.Year <= 1753 ? DBNull.Value : (object) NgayPhuLuc);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<PhuLucHopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (PhuLucHopDong item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdatePhuLucHopDong(long id, long tKCT_ID, string soPhuLuc, DateTime ngayPhuLuc, DateTime ngayHetHan, string thongTinKhac)
		{
			PhuLucHopDong entity = new PhuLucHopDong();			
			entity.ID = id;
			entity.TKCT_ID = tKCT_ID;
			entity.SoPhuLuc = soPhuLuc;
			entity.NgayPhuLuc = ngayPhuLuc;
			entity.NgayHetHan = ngayHetHan;
			entity.ThongTinKhac = thongTinKhac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, TKCT_ID);
			db.AddInParameter(dbCommand, "@SoPhuLuc", SqlDbType.NVarChar, SoPhuLuc);
			db.AddInParameter(dbCommand, "@NgayPhuLuc", SqlDbType.DateTime, NgayPhuLuc.Year <= 1753 ? DBNull.Value : (object) NgayPhuLuc);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<PhuLucHopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (PhuLucHopDong item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeletePhuLucHopDong(long id)
		{
			PhuLucHopDong entity = new PhuLucHopDong();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKCT_ID(long tKCT_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_DeleteBy_TKCT_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, tKCT_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuLucHopDong_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<PhuLucHopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (PhuLucHopDong item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}