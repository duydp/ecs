--USE [ECS_TQDT_SXXK_V4]
--GO

/****** Object:  Table [dbo].[t_KDT_ToKhaiMauDichBoSung]    Script Date: 02/06/2013 11:32:22 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_ToKhaiMauDichBoSung]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[t_KDT_ToKhaiMauDichBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[PhienBan] [int] NOT NULL,
	[ChuKySo] [bit] NOT NULL,
	[GhiChu] [nvarchar](250) NULL,
 CONSTRAINT [PK_t_KDT_ToKhaiMauDich_BoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END

GO

--USE [ECS_TQDT_SXXK_V4]
--GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]    Script Date: 02/06/2013 13:45:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]    Script Date: 02/06/2013 13:45:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]    Script Date: 02/06/2013 13:45:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]    Script Date: 02/06/2013 13:45:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]
GO

--USE [ECS_TQDT_SXXK_V4]
--GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[TKMD_ID] = @TKMD_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[TKMD_ID] = @TKMD_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDichBoSung] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[PhienBan] = @PhienBan,
			[ChuKySo] = @ChuKySo,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
		(
			[TKMD_ID],
			[PhienBan],
			[ChuKySo],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@PhienBan,
			@ChuKySo,
			@GhiChu
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_ToKhaiMauDichBoSung]
SET
	[TKMD_ID] = @TKMD_ID,
	[PhienBan] = @PhienBan,
	[ChuKySo] = @ChuKySo,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]    Script Date: 02/06/2013 13:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_Insert]
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
(
	[TKMD_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
)
VALUES 
(
	@TKMD_ID,
	@PhienBan,
	@ChuKySo,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

--Bo sung them
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDichBoSung_InsertUpdate_ByTKMD]
	@TKMD_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDichBoSung] WHERE [TKMD_ID] = @TKMD_ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDichBoSung] 
		SET
			[PhienBan] = @PhienBan,
			[ChuKySo] = @ChuKySo,
			[GhiChu] = @GhiChu
		WHERE
			[TKMD_ID] = @TKMD_ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDichBoSung]
		(
			[TKMD_ID],
			[PhienBan],
			[ChuKySo],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@PhienBan,
			@ChuKySo,
			@GhiChu
		)		
	END


GO

UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '6.8'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO	
