using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Data;

namespace DongBoDuLieu.BLL
{
    public class WebServiceDB
    {
        public static bool CreateAccount(string madoanhnghiep,string matkhau, string name)
        {
            WS.DongBoDuLieu.WSDongBoDuLieu ws = new WS.DongBoDuLieu.WSDongBoDuLieu();
            ws.Url = ReadNodeXmlAppSettings("DiaChiWSDongBo");
            ws.Timeout = 1000000000;
            DongBoDuLieu.BLL.WS.DongBoDuLieu.DoanhNghiep dn = new DongBoDuLieu.BLL.WS.DongBoDuLieu.DoanhNghiep();
            dn.MaDoanhNghiep =madoanhnghiep;
            dn.MatKhau=matkhau ;
            dn.TenDoanhNghiep = madoanhnghiep;
            return ws.InsertAcountDN(dn);
        }
        public static DataSet getDataBySQL(string sql, string MaHaiQuan, string database, string MaDoanhNghiep, string MatKhau)
        {
            WS.DongBoDuLieu.WSDongBoDuLieu ws = new WS.DongBoDuLieu.WSDongBoDuLieu();
            ws.Url = ReadNodeXmlAppSettings("DiaChiWSDongBo");
            ws.Timeout = 1000000000;

            string matKhauWS = "";

            //if(ws.Url.Contains("dngcustoms.gov.vn"))
            //    matKhauWS="dotnetnuke";
            //else
                matKhauWS="daibangtungcanh";

            return ws.GetData(sql, MaHaiQuan, database, matKhauWS, MaDoanhNghiep, MatKhau);
        }
        //lay truc tiep tu database
        public static DataSet getDataBySQLDatabase(string sql, string MaHaiQuan, string database)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(database);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand);
        }

        public static DataSet getDataBySQLDatabaseLocal(string sql, string MaHaiQuan)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand);
        }

        public static bool CheckConnectionToDatabase(string sql, string MaHaiQuan, string database)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(database);
            IDbConnection con = db.CreateConnection();
            try
            {
                con.Open();
            }
            catch { return false; }

            return con.State == ConnectionState.Open ? true : false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="http"> Nhap ip cua hai quan</param>
        public static void SetWSHaiQuan(string ip)
        {
            // Uri url = new Uri(Properties.Settings.Default.DongBoDuLieu_BLL_WS_DongBoDuLieu_WSDongBoDuLieu);
            //Properties.Settings.Default.DongBoDuLieu_BLL_WS_DongBoDuLieu_WSDongBoDuLieu = ip;
            //Properties.Settings.Default.Save();
        }

        public static string GetIPWSHaiQuan()
        {
            return Properties.Settings.Default.DongBoDuLieu_BLL_WS_DongBoDuLieu_WSDongBoDuLieu;
        }
        public static void ChangePass(string maDN, string oldPass, string newPass)
        {
            WS.DongBoDuLieu.WSDongBoDuLieu ws = new WS.DongBoDuLieu.WSDongBoDuLieu();
            ws.Timeout = 1000000000;
            ws.DoiMatKhauDongBo("daibangtungcanh", maDN, oldPass, newPass);

        }

        public static string ReadNodeXmlAppSettings(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config.FilePath);
            string groupSettingName = "appSettings";

            return ReadNodeXml(config, doc, groupSettingName, key);
        }

        public static string ReadNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key)
        {
            string result = "";

            try
            {
                result = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return result;
        }

    }
}
