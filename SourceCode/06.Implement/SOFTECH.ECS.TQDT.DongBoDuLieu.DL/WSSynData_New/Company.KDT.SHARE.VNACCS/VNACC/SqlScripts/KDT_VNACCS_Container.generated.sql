-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Container_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Container_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Insert]
	@ID_TKMD bigint,
	@SoVanDon nvarchar(35),
	@SoContainer nvarchar(15),
	@SoSeal nvarchar(15),
	@GhiChuKhac nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_Container]
(
	[ID_TKMD],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChuKhac]
)
VALUES 
(
	@ID_TKMD,
	@SoVanDon,
	@SoContainer,
	@SoSeal,
	@GhiChuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Update]
	@ID bigint,
	@ID_TKMD bigint,
	@SoVanDon nvarchar(35),
	@SoContainer nvarchar(15),
	@SoSeal nvarchar(15),
	@GhiChuKhac nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_Container]
SET
	[ID_TKMD] = @ID_TKMD,
	[SoVanDon] = @SoVanDon,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal,
	[GhiChuKhac] = @GhiChuKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_InsertUpdate]
	@ID bigint,
	@ID_TKMD bigint,
	@SoVanDon nvarchar(35),
	@SoContainer nvarchar(15),
	@SoSeal nvarchar(15),
	@GhiChuKhac nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_Container] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_Container] 
		SET
			[ID_TKMD] = @ID_TKMD,
			[SoVanDon] = @SoVanDon,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal,
			[GhiChuKhac] = @GhiChuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_Container]
		(
			[ID_TKMD],
			[SoVanDon],
			[SoContainer],
			[SoSeal],
			[GhiChuKhac]
		)
		VALUES 
		(
			@ID_TKMD,
			@SoVanDon,
			@SoContainer,
			@SoSeal,
			@GhiChuKhac
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_Container]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_Container] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ID_TKMD],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_Container]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ID_TKMD],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChuKhac]
FROM [dbo].[t_KDT_VNACCS_Container] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Container_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 11, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Container_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ID_TKMD],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_Container]	

GO

