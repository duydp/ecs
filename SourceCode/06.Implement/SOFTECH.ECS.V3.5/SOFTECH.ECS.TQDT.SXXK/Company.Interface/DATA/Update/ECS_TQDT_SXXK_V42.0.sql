SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[t_KDT_VNACCS_Careeries]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
UPDATE [dbo].[t_KDT_VNACCS_Careeries] SET ChuKySanXuat = N'' WHERE ChuKySanXuat IS NULL
GO
ALTER TABLE [dbo].[t_KDT_VNACCS_Careeries] ALTER COLUMN [ChuKySanXuat] [nvarchar] (2000) NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[t_SXXK_ThongTinDinhMuc]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[t_SXXK_ThongTinDinhMuc]', N'LenhSanXuat_ID') IS NULL
ALTER TABLE [dbo].[t_SXXK_ThongTinDinhMuc] ADD[LenhSanXuat_ID] [bigint] NOT NULL CONSTRAINT [DF__t_SXXK_Th__LenhS__7B4740D3] DEFAULT ((0))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_t_SXXK_ThongTinDinhMuc] on [dbo].[t_SXXK_ThongTinDinhMuc]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_t_SXXK_ThongTinDinhMuc' AND object_id = OBJECT_ID(N'[dbo].[t_SXXK_ThongTinDinhMuc]'))
ALTER TABLE [dbo].[t_SXXK_ThongTinDinhMuc] ADD CONSTRAINT [PK_t_SXXK_ThongTinDinhMuc] PRIMARY KEY CLUSTERED  ([MaSanPham], [MaDoanhNghiep], [MaHaiQuan], [LenhSanXuat_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[t_VNACC_Category_Cargo]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[t_VNACC_Category_Cargo] ALTER COLUMN [TableID] [varchar] (10) NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[t_VNACC_Category_Cargo] ALTER COLUMN [BondedAreaName] [nvarchar] (900) NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_t_KDT_VNACC_Cargo] on [dbo].[t_VNACC_Category_Cargo]'
GO
WITH cte
AS (SELECT ROW_NUMBER() OVER (PARTITION BY BondedAreaCode ORDER BY TableID) AS STT,
           *
    FROM dbo.t_VNACC_Category_Cargo c
    WHERE EXISTS
    (
        SELECT BondedAreaCode,
               BondedAreaName
        FROM dbo.t_VNACC_Category_Cargo
        WHERE BondedAreaCode = c.BondedAreaCode
        GROUP BY BondedAreaCode,
                 BondedAreaName
        HAVING COUNT(1) > 1
    ))
DELETE FROM cte
WHERE cte.STT = 2;
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_t_KDT_VNACC_Cargo' AND object_id = OBJECT_ID(N'[dbo].[t_VNACC_Category_Cargo]'))
ALTER TABLE [dbo].[t_VNACC_Category_Cargo] ADD CONSTRAINT [PK_t_KDT_VNACC_Cargo] PRIMARY KEY CLUSTERED  ([BondedAreaCode], [BondedAreaName])
GO
--IF @@ERROR <> 0 SET NOEXEC ON
--GO
--PRINT N'Adding foreign keys to [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]'
--GO
--IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__t_KDT_VNA__GoodI__32E18E80]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New]', 'U'))
--ALTER TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New] ADD CONSTRAINT [FK__t_KDT_VNA__GoodI__32E18E80] FOREIGN KEY ([GoodItem_ID]) REFERENCES [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details] ([ID])
--GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) BEGIN
	PRINT 'The database update succeeded'
	
	IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '42.0') = 0
	BEGIN
		INSERT INTO dbo.t_HaiQuan_Version VALUES('42.0',GETDATE(), N'CẬP NHẬT t_KDT_VNACCS_Careeries (ChuKySanXuat), t_SXXK_ThongTinDinhMuc (LenhSanXuat_ID), t_VNACC_Category_Cargo (PRIMARY KEY), t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New (FOREIGN KEY)')
	END	

END
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO