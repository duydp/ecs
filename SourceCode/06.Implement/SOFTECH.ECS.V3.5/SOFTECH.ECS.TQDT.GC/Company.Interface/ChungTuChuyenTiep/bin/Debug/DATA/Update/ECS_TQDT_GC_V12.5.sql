
-------------------------------------------- STORE


/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_SelectAll]    Script Date: 11/12/2013 09:11:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_SelectDynamic]    Script Date: 11/12/2013 09:11:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Load]    Script Date: 11/12/2013 09:11:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Delete]    Script Date: 11/12/2013 09:11:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_DeleteDynamic]    Script Date: 11/12/2013 09:11:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_InsertUpdate]    Script Date: 11/12/2013 09:11:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Update]    Script Date: 11/12/2013 09:11:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Insert]    Script Date: 11/12/2013 09:11:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_ApplicationProcedureType_Insert]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_HSCode_SelectAll]    Script Date: 11/12/2013 09:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_HSCode_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_HSCode_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_HSCode_SelectDynamic]    Script Date: 11/12/2013 09:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_HSCode_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_HSCode_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_HSCode_Load]    Script Date: 11/12/2013 09:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_HSCode_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_HSCode_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_HSCode_InsertUpdate]    Script Date: 11/12/2013 09:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_HSCode_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_HSCode_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_HSCode_Delete]    Script Date: 11/12/2013 09:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_HSCode_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_HSCode_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_HSCode_DeleteDynamic]    Script Date: 11/12/2013 09:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_HSCode_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_HSCode_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_HSCode_Update]    Script Date: 11/12/2013 09:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_HSCode_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_HSCode_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_Category_HSCode_Insert]    Script Date: 11/12/2013 09:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_Category_HSCode_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_Category_HSCode_Insert]
GO

/****** Object:  Table [dbo].[t_HaiQuan_TieuChi]    Script Date: 11/12/2013 09:20:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_HaiQuan_TieuChi]') AND type in (N'U'))
DROP TABLE [dbo].[t_HaiQuan_TieuChi]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]
	@LePhi_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
WHERE
	[LePhi_ID] = @LePhi_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]
	@LePhi_ID bigint,
	@TenSacPhi nvarchar(210),
	@SoPhiPhaiNop numeric(12, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
(
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
)
VALUES 
(
	@LePhi_ID,
	@TenSacPhi,
	@SoPhiPhaiNop
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]
	@ID bigint,
	@LePhi_ID bigint,
	@TenSacPhi nvarchar(210),
	@SoPhiPhaiNop numeric(12, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] 
		SET
			[LePhi_ID] = @LePhi_ID,
			[TenSacPhi] = @TenSacPhi,
			[SoPhiPhaiNop] = @SoPhiPhaiNop
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
		(
			[LePhi_ID],
			[TenSacPhi],
			[SoPhiPhaiNop]
		)
		VALUES 
		(
			@LePhi_ID,
			@TenSacPhi,
			@SoPhiPhaiNop
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]
	@LePhi_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
WHERE
	[LePhi_ID] = @LePhi_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]
	@ID bigint,
	@LePhi_ID bigint,
	@TenSacPhi nvarchar(210),
	@SoPhiPhaiNop numeric(12, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
SET
	[LePhi_ID] = @LePhi_ID,
	[TenSacPhi] = @TenSacPhi,
	[SoPhiPhaiNop] = @SoPhiPhaiNop
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_Load]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[PhanLoaiBaoCaoSuaDoi],
	[SoToKhaiDauTien],
	[SoNhanhToKhai],
	[TongSoTKChiaNho],
	[SoToKhaiTNTX],
	[MaLoaiHinh],
	[MaPhanLoaiHH],
	[MaPhuongThucVT],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[ThoiHanTaiNhapTaiXuat],
	[NgayDangKy],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaUyThac],
	[TenUyThac],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[NguoiUyThacXK],
	[MaDaiLyHQ],
	[SoLuong],
	[MaDVTSoLuong],
	[TrongLuong],
	[MaDVTTrongLuong],
	[MaDDLuuKho],
	[SoHieuKyHieu],
	[MaPTVC],
	[TenPTVC],
	[NgayHangDen],
	[MaDiaDiemDoHang],
	[TenDiaDiemDohang],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[SoLuongCont],
	[MaKetQuaKiemTra],
	[MaVanbanPhapQuy1],
	[MaVanbanPhapQuy2],
	[MaVanbanPhapQuy3],
	[MaVanbanPhapQuy4],
	[MaVanbanPhapQuy5],
	[PhanLoaiHD],
	[SoTiepNhanHD],
	[SoHoaDon],
	[NgayPhatHanhHD],
	[PhuongThucTT],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiTriGia],
	[SoTiepNhanTKTriGia],
	[MaTTHieuChinhTriGia],
	[GiaHieuChinhTriGia],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[SoDangKyBH],
	[ChiTietKhaiTriGia],
	[TriGiaTinhThue],
	[PhanLoaiKhongQDVND],
	[MaTTTriGiaTinhThue],
	[TongHeSoPhanBoTG],
	[MaLyDoDeNghiBP],
	[NguoiNopThue],
	[MaNHTraThueThay],
	[NamPhatHanhHM],
	[KyHieuCTHanMuc],
	[SoCTHanMuc],
	[MaXDThoiHanNopThue],
	[MaNHBaoLanh],
	[NamPhatHanhBL],
	[KyHieuCTBaoLanh],
	[SoCTBaoLanh],
	[NgayNhapKhoDau],
	[NgayKhoiHanhVC],
	[DiaDiemDichVC],
	[NgayDen],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra],
	[TongSoTrangCuaToKhai],
	[TongSoDongHangCuaToKhai],
	[NgayKhaiBaoNopThue],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]
	@Master_ID bigint,
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayDangKy datetime,
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(11, 0),
	@SoTienBaoLanh numeric(11, 0),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(11, 0),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(11, 0),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
(
	[Master_ID],
	[PhanLoaiBaoCaoSuaDoi],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayDangKy],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra]
)
VALUES 
(
	@Master_ID,
	@PhanLoaiBaoCaoSuaDoi,
	@MaPhanLoaiKiemTra,
	@MaSoThueDaiDien,
	@TenCoQuanHaiQuan,
	@NgayDangKy,
	@NgayThayDoiDangKy,
	@BieuThiTruongHopHetHan,
	@TenDaiLyHaiQuan,
	@MaNhanVienHaiQuan,
	@TenDDLuuKho,
	@MaPhanLoaiTongGiaCoBan,
	@PhanLoaiCongThucChuan,
	@MaPhanLoaiDieuChinhTriGia,
	@PhuongPhapDieuChinhTriGia,
	@TongTienThuePhaiNop,
	@SoTienBaoLanh,
	@TenTruongDonViHaiQuan,
	@NgayCapPhep,
	@PhanLoaiThamTraSauThongQuan,
	@NgayPheDuyetBP,
	@NgayHoanThanhKiemTraBP,
	@SoNgayDoiCapPhepNhapKhau,
	@TieuDe,
	@MaSacThueAnHan_VAT,
	@TenSacThueAnHan_VAT,
	@HanNopThueSauKhiAnHan_VAT,
	@PhanLoaiNopThue,
	@TongSoTienThueXuatKhau,
	@MaTTTongTienThueXuatKhau,
	@TongSoTienLePhi,
	@MaTTCuaSoTienBaoLanh,
	@SoQuanLyNguoiSuDung,
	@NgayHoanThanhKiemTra
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayDangKy datetime,
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(11, 0),
	@SoTienBaoLanh numeric(11, 0),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(11, 0),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(11, 0),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi] 
		SET
			[Master_ID] = @Master_ID,
			[PhanLoaiBaoCaoSuaDoi] = @PhanLoaiBaoCaoSuaDoi,
			[MaPhanLoaiKiemTra] = @MaPhanLoaiKiemTra,
			[MaSoThueDaiDien] = @MaSoThueDaiDien,
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[NgayDangKy] = @NgayDangKy,
			[NgayThayDoiDangKy] = @NgayThayDoiDangKy,
			[BieuThiTruongHopHetHan] = @BieuThiTruongHopHetHan,
			[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
			[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
			[TenDDLuuKho] = @TenDDLuuKho,
			[MaPhanLoaiTongGiaCoBan] = @MaPhanLoaiTongGiaCoBan,
			[PhanLoaiCongThucChuan] = @PhanLoaiCongThucChuan,
			[MaPhanLoaiDieuChinhTriGia] = @MaPhanLoaiDieuChinhTriGia,
			[PhuongPhapDieuChinhTriGia] = @PhuongPhapDieuChinhTriGia,
			[TongTienThuePhaiNop] = @TongTienThuePhaiNop,
			[SoTienBaoLanh] = @SoTienBaoLanh,
			[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
			[NgayCapPhep] = @NgayCapPhep,
			[PhanLoaiThamTraSauThongQuan] = @PhanLoaiThamTraSauThongQuan,
			[NgayPheDuyetBP] = @NgayPheDuyetBP,
			[NgayHoanThanhKiemTraBP] = @NgayHoanThanhKiemTraBP,
			[SoNgayDoiCapPhepNhapKhau] = @SoNgayDoiCapPhepNhapKhau,
			[TieuDe] = @TieuDe,
			[MaSacThueAnHan_VAT] = @MaSacThueAnHan_VAT,
			[TenSacThueAnHan_VAT] = @TenSacThueAnHan_VAT,
			[HanNopThueSauKhiAnHan_VAT] = @HanNopThueSauKhiAnHan_VAT,
			[PhanLoaiNopThue] = @PhanLoaiNopThue,
			[TongSoTienThueXuatKhau] = @TongSoTienThueXuatKhau,
			[MaTTTongTienThueXuatKhau] = @MaTTTongTienThueXuatKhau,
			[TongSoTienLePhi] = @TongSoTienLePhi,
			[MaTTCuaSoTienBaoLanh] = @MaTTCuaSoTienBaoLanh,
			[SoQuanLyNguoiSuDung] = @SoQuanLyNguoiSuDung,
			[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
		(
			[Master_ID],
			[PhanLoaiBaoCaoSuaDoi],
			[MaPhanLoaiKiemTra],
			[MaSoThueDaiDien],
			[TenCoQuanHaiQuan],
			[NgayDangKy],
			[NgayThayDoiDangKy],
			[BieuThiTruongHopHetHan],
			[TenDaiLyHaiQuan],
			[MaNhanVienHaiQuan],
			[TenDDLuuKho],
			[MaPhanLoaiTongGiaCoBan],
			[PhanLoaiCongThucChuan],
			[MaPhanLoaiDieuChinhTriGia],
			[PhuongPhapDieuChinhTriGia],
			[TongTienThuePhaiNop],
			[SoTienBaoLanh],
			[TenTruongDonViHaiQuan],
			[NgayCapPhep],
			[PhanLoaiThamTraSauThongQuan],
			[NgayPheDuyetBP],
			[NgayHoanThanhKiemTraBP],
			[SoNgayDoiCapPhepNhapKhau],
			[TieuDe],
			[MaSacThueAnHan_VAT],
			[TenSacThueAnHan_VAT],
			[HanNopThueSauKhiAnHan_VAT],
			[PhanLoaiNopThue],
			[TongSoTienThueXuatKhau],
			[MaTTTongTienThueXuatKhau],
			[TongSoTienLePhi],
			[MaTTCuaSoTienBaoLanh],
			[SoQuanLyNguoiSuDung],
			[NgayHoanThanhKiemTra]
		)
		VALUES 
		(
			@Master_ID,
			@PhanLoaiBaoCaoSuaDoi,
			@MaPhanLoaiKiemTra,
			@MaSoThueDaiDien,
			@TenCoQuanHaiQuan,
			@NgayDangKy,
			@NgayThayDoiDangKy,
			@BieuThiTruongHopHetHan,
			@TenDaiLyHaiQuan,
			@MaNhanVienHaiQuan,
			@TenDDLuuKho,
			@MaPhanLoaiTongGiaCoBan,
			@PhanLoaiCongThucChuan,
			@MaPhanLoaiDieuChinhTriGia,
			@PhuongPhapDieuChinhTriGia,
			@TongTienThuePhaiNop,
			@SoTienBaoLanh,
			@TenTruongDonViHaiQuan,
			@NgayCapPhep,
			@PhanLoaiThamTraSauThongQuan,
			@NgayPheDuyetBP,
			@NgayHoanThanhKiemTraBP,
			@SoNgayDoiCapPhepNhapKhau,
			@TieuDe,
			@MaSacThueAnHan_VAT,
			@TenSacThueAnHan_VAT,
			@HanNopThueSauKhiAnHan_VAT,
			@PhanLoaiNopThue,
			@TongSoTienThueXuatKhau,
			@MaTTTongTienThueXuatKhau,
			@TongSoTienLePhi,
			@MaTTCuaSoTienBaoLanh,
			@SoQuanLyNguoiSuDung,
			@NgayHoanThanhKiemTra
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[PhanLoaiBaoCaoSuaDoi],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayDangKy],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[PhanLoaiBaoCaoSuaDoi],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayDangKy],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[PhanLoaiBaoCaoSuaDoi],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayDangKy],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]
	@ID bigint,
	@Master_ID bigint,
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayDangKy datetime,
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(11, 0),
	@SoTienBaoLanh numeric(11, 0),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(11, 0),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(11, 0),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
SET
	[Master_ID] = @Master_ID,
	[PhanLoaiBaoCaoSuaDoi] = @PhanLoaiBaoCaoSuaDoi,
	[MaPhanLoaiKiemTra] = @MaPhanLoaiKiemTra,
	[MaSoThueDaiDien] = @MaSoThueDaiDien,
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[NgayDangKy] = @NgayDangKy,
	[NgayThayDoiDangKy] = @NgayThayDoiDangKy,
	[BieuThiTruongHopHetHan] = @BieuThiTruongHopHetHan,
	[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
	[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
	[TenDDLuuKho] = @TenDDLuuKho,
	[MaPhanLoaiTongGiaCoBan] = @MaPhanLoaiTongGiaCoBan,
	[PhanLoaiCongThucChuan] = @PhanLoaiCongThucChuan,
	[MaPhanLoaiDieuChinhTriGia] = @MaPhanLoaiDieuChinhTriGia,
	[PhuongPhapDieuChinhTriGia] = @PhuongPhapDieuChinhTriGia,
	[TongTienThuePhaiNop] = @TongTienThuePhaiNop,
	[SoTienBaoLanh] = @SoTienBaoLanh,
	[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
	[NgayCapPhep] = @NgayCapPhep,
	[PhanLoaiThamTraSauThongQuan] = @PhanLoaiThamTraSauThongQuan,
	[NgayPheDuyetBP] = @NgayPheDuyetBP,
	[NgayHoanThanhKiemTraBP] = @NgayHoanThanhKiemTraBP,
	[SoNgayDoiCapPhepNhapKhau] = @SoNgayDoiCapPhepNhapKhau,
	[TieuDe] = @TieuDe,
	[MaSacThueAnHan_VAT] = @MaSacThueAnHan_VAT,
	[TenSacThueAnHan_VAT] = @TenSacThueAnHan_VAT,
	[HanNopThueSauKhiAnHan_VAT] = @HanNopThueSauKhiAnHan_VAT,
	[PhanLoaiNopThue] = @PhanLoaiNopThue,
	[TongSoTienThueXuatKhau] = @TongSoTienThueXuatKhau,
	[MaTTTongTienThueXuatKhau] = @MaTTTongTienThueXuatKhau,
	[TongSoTienLePhi] = @TongSoTienLePhi,
	[MaTTCuaSoTienBaoLanh] = @MaTTCuaSoTienBaoLanh,
	[SoQuanLyNguoiSuDung] = @SoQuanLyNguoiSuDung,
	[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectAll]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[PhanLoaiBaoCaoSuaDoi],
	[SoToKhaiDauTien],
	[SoNhanhToKhai],
	[TongSoTKChiaNho],
	[SoToKhaiTNTX],
	[MaLoaiHinh],
	[MaPhanLoaiHH],
	[MaPhuongThucVT],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[ThoiHanTaiNhapTaiXuat],
	[NgayDangKy],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaUyThac],
	[TenUyThac],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[NguoiUyThacXK],
	[MaDaiLyHQ],
	[SoLuong],
	[MaDVTSoLuong],
	[TrongLuong],
	[MaDVTTrongLuong],
	[MaDDLuuKho],
	[SoHieuKyHieu],
	[MaPTVC],
	[TenPTVC],
	[NgayHangDen],
	[MaDiaDiemDoHang],
	[TenDiaDiemDohang],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[SoLuongCont],
	[MaKetQuaKiemTra],
	[MaVanbanPhapQuy1],
	[MaVanbanPhapQuy2],
	[MaVanbanPhapQuy3],
	[MaVanbanPhapQuy4],
	[MaVanbanPhapQuy5],
	[PhanLoaiHD],
	[SoTiepNhanHD],
	[SoHoaDon],
	[NgayPhatHanhHD],
	[PhuongThucTT],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiTriGia],
	[SoTiepNhanTKTriGia],
	[MaTTHieuChinhTriGia],
	[GiaHieuChinhTriGia],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[SoDangKyBH],
	[ChiTietKhaiTriGia],
	[TriGiaTinhThue],
	[PhanLoaiKhongQDVND],
	[MaTTTriGiaTinhThue],
	[TongHeSoPhanBoTG],
	[MaLyDoDeNghiBP],
	[NguoiNopThue],
	[MaNHTraThueThay],
	[NamPhatHanhHM],
	[KyHieuCTHanMuc],
	[SoCTHanMuc],
	[MaXDThoiHanNopThue],
	[MaNHBaoLanh],
	[NamPhatHanhBL],
	[KyHieuCTBaoLanh],
	[SoCTBaoLanh],
	[NgayNhapKhoDau],
	[NgayKhoiHanhVC],
	[DiaDiemDichVC],
	[NgayDen],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra],
	[TongSoTrangCuaToKhai],
	[TongSoDongHangCuaToKhai],
	[NgayKhaiBaoNopThue],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoToKhai],
	[PhanLoaiBaoCaoSuaDoi],
	[SoToKhaiDauTien],
	[SoNhanhToKhai],
	[TongSoTKChiaNho],
	[SoToKhaiTNTX],
	[MaLoaiHinh],
	[MaPhanLoaiHH],
	[MaPhuongThucVT],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[ThoiHanTaiNhapTaiXuat],
	[NgayDangKy],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaUyThac],
	[TenUyThac],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[NguoiUyThacXK],
	[MaDaiLyHQ],
	[SoLuong],
	[MaDVTSoLuong],
	[TrongLuong],
	[MaDVTTrongLuong],
	[MaDDLuuKho],
	[SoHieuKyHieu],
	[MaPTVC],
	[TenPTVC],
	[NgayHangDen],
	[MaDiaDiemDoHang],
	[TenDiaDiemDohang],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[SoLuongCont],
	[MaKetQuaKiemTra],
	[MaVanbanPhapQuy1],
	[MaVanbanPhapQuy2],
	[MaVanbanPhapQuy3],
	[MaVanbanPhapQuy4],
	[MaVanbanPhapQuy5],
	[PhanLoaiHD],
	[SoTiepNhanHD],
	[SoHoaDon],
	[NgayPhatHanhHD],
	[PhuongThucTT],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiTriGia],
	[SoTiepNhanTKTriGia],
	[MaTTHieuChinhTriGia],
	[GiaHieuChinhTriGia],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[SoDangKyBH],
	[ChiTietKhaiTriGia],
	[TriGiaTinhThue],
	[PhanLoaiKhongQDVND],
	[MaTTTriGiaTinhThue],
	[TongHeSoPhanBoTG],
	[MaLyDoDeNghiBP],
	[NguoiNopThue],
	[MaNHTraThueThay],
	[NamPhatHanhHM],
	[KyHieuCTHanMuc],
	[SoCTHanMuc],
	[MaXDThoiHanNopThue],
	[MaNHBaoLanh],
	[NamPhatHanhBL],
	[KyHieuCTBaoLanh],
	[SoCTBaoLanh],
	[NgayNhapKhoDau],
	[NgayKhoiHanhVC],
	[DiaDiemDichVC],
	[NgayDen],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra],
	[TongSoTrangCuaToKhai],
	[TongSoDongHangCuaToKhai],
	[NgayKhaiBaoNopThue],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]    Script Date: 11/11/2013 17:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Insert]
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoTienThue numeric(12, 0),
	@TongSoTienMien numeric(11, 0),
	@TongSoTienGiam numeric(11, 0),
	@TongSoThuePhaiNop numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
(
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoTienThue],
	[TongSoTienMien],
	[TongSoTienGiam],
	[TongSoThuePhaiNop],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
)
VALUES 
(
	@TenCoQuanHaiQuan,
	@TenChiCucHaiQuanNoiMoToKhai,
	@SoChungTu,
	@TenDonViXuatNhapKhau,
	@MaDonViXuatNhapKhau,
	@MaBuuChinh,
	@DiaChiNguoiXuatNhapKhau,
	@SoDienThoaiNguoiXuatNhapKhau,
	@SoToKhai,
	@NgayDangKyToKhai,
	@MaPhanLoaiToKhai,
	@TenNganHangBaoLanh,
	@MaNganHangBaoLanh,
	@KiHieuChungTuBaoLanh,
	@SoChungTuBaoLanh,
	@LoaiBaoLanh,
	@TenNganHangTraThay,
	@MaNganHangTraThay,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoHieuPhatHanhHanMuc,
	@TongSoTienThue,
	@TongSoTienMien,
	@TongSoTienGiam,
	@TongSoThuePhaiNop,
	@MaTienTe,
	@TyGiaQuyDoi,
	@SoNgayDuocAnHan,
	@NgayHetHieuLucTamNhapTaiXuat,
	@SoTaiKhoanKhoBac,
	@TenKhoBac,
	@LaiSuatPhatChamNop,
	@NgayPhatHanhChungTu
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_InsertUpdate]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoTienThue numeric(12, 0),
	@TongSoTienMien numeric(11, 0),
	@TongSoTienGiam numeric(11, 0),
	@TongSoThuePhaiNop numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] 
		SET
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[TenChiCucHaiQuanNoiMoToKhai] = @TenChiCucHaiQuanNoiMoToKhai,
			[SoChungTu] = @SoChungTu,
			[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
			[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
			[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
			[SoToKhai] = @SoToKhai,
			[NgayDangKyToKhai] = @NgayDangKyToKhai,
			[MaPhanLoaiToKhai] = @MaPhanLoaiToKhai,
			[TenNganHangBaoLanh] = @TenNganHangBaoLanh,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[KiHieuChungTuBaoLanh] = @KiHieuChungTuBaoLanh,
			[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
			[LoaiBaoLanh] = @LoaiBaoLanh,
			[TenNganHangTraThay] = @TenNganHangTraThay,
			[MaNganHangTraThay] = @MaNganHangTraThay,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
			[TongSoTienThue] = @TongSoTienThue,
			[TongSoTienMien] = @TongSoTienMien,
			[TongSoTienGiam] = @TongSoTienGiam,
			[TongSoThuePhaiNop] = @TongSoThuePhaiNop,
			[MaTienTe] = @MaTienTe,
			[TyGiaQuyDoi] = @TyGiaQuyDoi,
			[SoNgayDuocAnHan] = @SoNgayDuocAnHan,
			[NgayHetHieuLucTamNhapTaiXuat] = @NgayHetHieuLucTamNhapTaiXuat,
			[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
			[TenKhoBac] = @TenKhoBac,
			[LaiSuatPhatChamNop] = @LaiSuatPhatChamNop,
			[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
		(
			[TenCoQuanHaiQuan],
			[TenChiCucHaiQuanNoiMoToKhai],
			[SoChungTu],
			[TenDonViXuatNhapKhau],
			[MaDonViXuatNhapKhau],
			[MaBuuChinh],
			[DiaChiNguoiXuatNhapKhau],
			[SoDienThoaiNguoiXuatNhapKhau],
			[SoToKhai],
			[NgayDangKyToKhai],
			[MaPhanLoaiToKhai],
			[TenNganHangBaoLanh],
			[MaNganHangBaoLanh],
			[KiHieuChungTuBaoLanh],
			[SoChungTuBaoLanh],
			[LoaiBaoLanh],
			[TenNganHangTraThay],
			[MaNganHangTraThay],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoHieuPhatHanhHanMuc],
			[TongSoTienThue],
			[TongSoTienMien],
			[TongSoTienGiam],
			[TongSoThuePhaiNop],
			[MaTienTe],
			[TyGiaQuyDoi],
			[SoNgayDuocAnHan],
			[NgayHetHieuLucTamNhapTaiXuat],
			[SoTaiKhoanKhoBac],
			[TenKhoBac],
			[LaiSuatPhatChamNop],
			[NgayPhatHanhChungTu]
		)
		VALUES 
		(
			@TenCoQuanHaiQuan,
			@TenChiCucHaiQuanNoiMoToKhai,
			@SoChungTu,
			@TenDonViXuatNhapKhau,
			@MaDonViXuatNhapKhau,
			@MaBuuChinh,
			@DiaChiNguoiXuatNhapKhau,
			@SoDienThoaiNguoiXuatNhapKhau,
			@SoToKhai,
			@NgayDangKyToKhai,
			@MaPhanLoaiToKhai,
			@TenNganHangBaoLanh,
			@MaNganHangBaoLanh,
			@KiHieuChungTuBaoLanh,
			@SoChungTuBaoLanh,
			@LoaiBaoLanh,
			@TenNganHangTraThay,
			@MaNganHangTraThay,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoHieuPhatHanhHanMuc,
			@TongSoTienThue,
			@TongSoTienMien,
			@TongSoTienGiam,
			@TongSoThuePhaiNop,
			@MaTienTe,
			@TyGiaQuyDoi,
			@SoNgayDuocAnHan,
			@NgayHetHieuLucTamNhapTaiXuat,
			@SoTaiKhoanKhoBac,
			@TenKhoBac,
			@LaiSuatPhatChamNop,
			@NgayPhatHanhChungTu
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoTienThue],
	[TongSoTienMien],
	[TongSoTienGiam],
	[TongSoThuePhaiNop],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoTienThue],
	[TongSoTienMien],
	[TongSoTienGiam],
	[TongSoThuePhaiNop],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoTienThue],
	[TongSoTienMien],
	[TongSoTienGiam],
	[TongSoThuePhaiNop],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu_Update]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoTienThue numeric(12, 0),
	@TongSoTienMien numeric(11, 0),
	@TongSoTienGiam numeric(11, 0),
	@TongSoThuePhaiNop numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
SET
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[TenChiCucHaiQuanNoiMoToKhai] = @TenChiCucHaiQuanNoiMoToKhai,
	[SoChungTu] = @SoChungTu,
	[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
	[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
	[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
	[SoToKhai] = @SoToKhai,
	[NgayDangKyToKhai] = @NgayDangKyToKhai,
	[MaPhanLoaiToKhai] = @MaPhanLoaiToKhai,
	[TenNganHangBaoLanh] = @TenNganHangBaoLanh,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[KiHieuChungTuBaoLanh] = @KiHieuChungTuBaoLanh,
	[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
	[LoaiBaoLanh] = @LoaiBaoLanh,
	[TenNganHangTraThay] = @TenNganHangTraThay,
	[MaNganHangTraThay] = @MaNganHangTraThay,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
	[TongSoTienThue] = @TongSoTienThue,
	[TongSoTienMien] = @TongSoTienMien,
	[TongSoTienGiam] = @TongSoTienGiam,
	[TongSoThuePhaiNop] = @TongSoThuePhaiNop,
	[MaTienTe] = @MaTienTe,
	[TyGiaQuyDoi] = @TyGiaQuyDoi,
	[SoNgayDuocAnHan] = @SoNgayDuocAnHan,
	[NgayHetHieuLucTamNhapTaiXuat] = @NgayHetHieuLucTamNhapTaiXuat,
	[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
	[TenKhoBac] = @TenKhoBac,
	[LaiSuatPhatChamNop] = @LaiSuatPhatChamNop,
	[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteBy_ThuePhaiThu_ID]
	@ThuePhaiThu_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
WHERE
	[ThuePhaiThu_ID] = @ThuePhaiThu_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Insert]
	@ThuePhaiThu_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@TienThue numeric(12, 0),
	@SoTienMienThue numeric(11, 0),
	@SoTienGiamThue numeric(11, 0),
	@SoThuePhaiNop numeric(12, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
(
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
)
VALUES 
(
	@ThuePhaiThu_ID,
	@TenSacThue,
	@TieuMuc,
	@TienThue,
	@SoTienMienThue,
	@SoTienGiamThue,
	@SoThuePhaiNop
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_InsertUpdate]
	@ID bigint,
	@ThuePhaiThu_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@TienThue numeric(12, 0),
	@SoTienMienThue numeric(11, 0),
	@SoTienGiamThue numeric(11, 0),
	@SoThuePhaiNop numeric(12, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] 
		SET
			[ThuePhaiThu_ID] = @ThuePhaiThu_ID,
			[TenSacThue] = @TenSacThue,
			[TieuMuc] = @TieuMuc,
			[TienThue] = @TienThue,
			[SoTienMienThue] = @SoTienMienThue,
			[SoTienGiamThue] = @SoTienGiamThue,
			[SoThuePhaiNop] = @SoThuePhaiNop
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
		(
			[ThuePhaiThu_ID],
			[TenSacThue],
			[TieuMuc],
			[TienThue],
			[SoTienMienThue],
			[SoTienGiamThue],
			[SoThuePhaiNop]
		)
		VALUES 
		(
			@ThuePhaiThu_ID,
			@TenSacThue,
			@TieuMuc,
			@TienThue,
			@SoTienMienThue,
			@SoTienGiamThue,
			@SoThuePhaiNop
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectBy_ThuePhaiThu_ID]
	@ThuePhaiThu_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
WHERE
	[ThuePhaiThu_ID] = @ThuePhaiThu_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ThuePhaiThu_ID],
	[TenSacThue],
	[TieuMuc],
	[TienThue],
	[SoTienMienThue],
	[SoTienGiamThue],
	[SoThuePhaiNop]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_Update]
	@ID bigint,
	@ThuePhaiThu_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@TienThue numeric(12, 0),
	@SoTienMienThue numeric(11, 0),
	@SoTienGiamThue numeric(11, 0),
	@SoThuePhaiNop numeric(12, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]
SET
	[ThuePhaiThu_ID] = @ThuePhaiThu_ID,
	[TenSacThue] = @TenSacThue,
	[TieuMuc] = @TieuMuc,
	[TienThue] = @TienThue,
	[SoTienMienThue] = @SoTienMienThue,
	[SoTienGiamThue] = @SoTienGiamThue,
	[SoThuePhaiNop] = @SoThuePhaiNop
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_Update]    Script Date: 11/11/2013 17:34:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Update]
	@ID bigint,
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@SoToKhaiDauTien numeric(12, 0),
	@SoNhanhToKhai numeric(2, 0),
	@TongSoTKChiaNho numeric(2, 0),
	@SoToKhaiTNTX numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@MaPhanLoaiHH varchar(1),
	@MaPhuongThucVT varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@ThoiHanTaiNhapTaiXuat datetime,
	@NgayDangKy datetime,
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaUyThac varchar(13),
	@TenUyThac nvarchar(100),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@NguoiUyThacXK nvarchar(70),
	@MaDaiLyHQ varchar(5),
	@SoLuong numeric(8, 0),
	@MaDVTSoLuong varchar(3),
	@TrongLuong numeric(14, 4),
	@MaDVTTrongLuong varchar(3),
	@MaDDLuuKho varchar(7),
	@SoHieuKyHieu varchar(140),
	@MaPTVC varchar(9),
	@TenPTVC nvarchar(35),
	@NgayHangDen datetime,
	@MaDiaDiemDoHang varchar(6),
	@TenDiaDiemDohang nvarchar(35),
	@MaDiaDiemXepHang varchar(6),
	@TenDiaDiemXepHang nvarchar(35),
	@SoLuongCont numeric(3, 0),
	@MaKetQuaKiemTra varchar(1),
	@MaVanbanPhapQuy1 varchar(2),
	@MaVanbanPhapQuy2 varchar(2),
	@MaVanbanPhapQuy3 varchar(2),
	@MaVanbanPhapQuy4 varchar(2),
	@MaVanbanPhapQuy5 varchar(2),
	@PhanLoaiHD varchar(1),
	@SoTiepNhanHD numeric(12, 0),
	@SoHoaDon varchar(35),
	@NgayPhatHanhHD datetime,
	@PhuongThucTT varchar(7),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiTriGia varchar(1),
	@SoTiepNhanTKTriGia numeric(9, 0),
	@MaTTHieuChinhTriGia varchar(3),
	@GiaHieuChinhTriGia numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@SoDangKyBH varchar(6),
	@ChiTietKhaiTriGia nvarchar(280),
	@TriGiaTinhThue numeric(24, 4),
	@PhanLoaiKhongQDVND varchar(1),
	@MaTTTriGiaTinhThue varchar(3),
	@TongHeSoPhanBoTG numeric(24, 4),
	@MaLyDoDeNghiBP varchar(1),
	@NguoiNopThue varchar(1),
	@MaNHTraThueThay varchar(11),
	@NamPhatHanhHM numeric(4, 0),
	@KyHieuCTHanMuc varchar(10),
	@SoCTHanMuc varchar(10),
	@MaXDThoiHanNopThue varchar(1),
	@MaNHBaoLanh varchar(11),
	@NamPhatHanhBL numeric(4, 0),
	@KyHieuCTBaoLanh varchar(10),
	@SoCTBaoLanh varchar(10),
	@NgayNhapKhoDau datetime,
	@NgayKhoiHanhVC datetime,
	@DiaDiemDichVC varchar(7),
	@NgayDen datetime,
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(15, 4),
	@SoTienBaoLanh numeric(15, 4),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(15, 4),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(15, 4),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime,
	@TongSoTrangCuaToKhai numeric(2, 0),
	@TongSoDongHangCuaToKhai numeric(2, 0),
	@NgayKhaiBaoNopThue datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich]
SET
	[SoToKhai] = @SoToKhai,
	[PhanLoaiBaoCaoSuaDoi] = @PhanLoaiBaoCaoSuaDoi,
	[SoToKhaiDauTien] = @SoToKhaiDauTien,
	[SoNhanhToKhai] = @SoNhanhToKhai,
	[TongSoTKChiaNho] = @TongSoTKChiaNho,
	[SoToKhaiTNTX] = @SoToKhaiTNTX,
	[MaLoaiHinh] = @MaLoaiHinh,
	[MaPhanLoaiHH] = @MaPhanLoaiHH,
	[MaPhuongThucVT] = @MaPhuongThucVT,
	[PhanLoaiToChuc] = @PhanLoaiToChuc,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHS] = @NhomXuLyHS,
	[ThoiHanTaiNhapTaiXuat] = @ThoiHanTaiNhapTaiXuat,
	[NgayDangKy] = @NgayDangKy,
	[MaDonVi] = @MaDonVi,
	[TenDonVi] = @TenDonVi,
	[MaBuuChinhDonVi] = @MaBuuChinhDonVi,
	[DiaChiDonVi] = @DiaChiDonVi,
	[SoDienThoaiDonVi] = @SoDienThoaiDonVi,
	[MaUyThac] = @MaUyThac,
	[TenUyThac] = @TenUyThac,
	[MaDoiTac] = @MaDoiTac,
	[TenDoiTac] = @TenDoiTac,
	[MaBuuChinhDoiTac] = @MaBuuChinhDoiTac,
	[DiaChiDoiTac1] = @DiaChiDoiTac1,
	[DiaChiDoiTac2] = @DiaChiDoiTac2,
	[DiaChiDoiTac3] = @DiaChiDoiTac3,
	[DiaChiDoiTac4] = @DiaChiDoiTac4,
	[MaNuocDoiTac] = @MaNuocDoiTac,
	[NguoiUyThacXK] = @NguoiUyThacXK,
	[MaDaiLyHQ] = @MaDaiLyHQ,
	[SoLuong] = @SoLuong,
	[MaDVTSoLuong] = @MaDVTSoLuong,
	[TrongLuong] = @TrongLuong,
	[MaDVTTrongLuong] = @MaDVTTrongLuong,
	[MaDDLuuKho] = @MaDDLuuKho,
	[SoHieuKyHieu] = @SoHieuKyHieu,
	[MaPTVC] = @MaPTVC,
	[TenPTVC] = @TenPTVC,
	[NgayHangDen] = @NgayHangDen,
	[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
	[TenDiaDiemDohang] = @TenDiaDiemDohang,
	[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
	[TenDiaDiemXepHang] = @TenDiaDiemXepHang,
	[SoLuongCont] = @SoLuongCont,
	[MaKetQuaKiemTra] = @MaKetQuaKiemTra,
	[MaVanbanPhapQuy1] = @MaVanbanPhapQuy1,
	[MaVanbanPhapQuy2] = @MaVanbanPhapQuy2,
	[MaVanbanPhapQuy3] = @MaVanbanPhapQuy3,
	[MaVanbanPhapQuy4] = @MaVanbanPhapQuy4,
	[MaVanbanPhapQuy5] = @MaVanbanPhapQuy5,
	[PhanLoaiHD] = @PhanLoaiHD,
	[SoTiepNhanHD] = @SoTiepNhanHD,
	[SoHoaDon] = @SoHoaDon,
	[NgayPhatHanhHD] = @NgayPhatHanhHD,
	[PhuongThucTT] = @PhuongThucTT,
	[PhanLoaiGiaHD] = @PhanLoaiGiaHD,
	[MaDieuKienGiaHD] = @MaDieuKienGiaHD,
	[MaTTHoaDon] = @MaTTHoaDon,
	[TongTriGiaHD] = @TongTriGiaHD,
	[MaPhanLoaiTriGia] = @MaPhanLoaiTriGia,
	[SoTiepNhanTKTriGia] = @SoTiepNhanTKTriGia,
	[MaTTHieuChinhTriGia] = @MaTTHieuChinhTriGia,
	[GiaHieuChinhTriGia] = @GiaHieuChinhTriGia,
	[MaPhanLoaiPhiVC] = @MaPhanLoaiPhiVC,
	[MaTTPhiVC] = @MaTTPhiVC,
	[PhiVanChuyen] = @PhiVanChuyen,
	[MaPhanLoaiPhiBH] = @MaPhanLoaiPhiBH,
	[MaTTPhiBH] = @MaTTPhiBH,
	[PhiBaoHiem] = @PhiBaoHiem,
	[SoDangKyBH] = @SoDangKyBH,
	[ChiTietKhaiTriGia] = @ChiTietKhaiTriGia,
	[TriGiaTinhThue] = @TriGiaTinhThue,
	[PhanLoaiKhongQDVND] = @PhanLoaiKhongQDVND,
	[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
	[TongHeSoPhanBoTG] = @TongHeSoPhanBoTG,
	[MaLyDoDeNghiBP] = @MaLyDoDeNghiBP,
	[NguoiNopThue] = @NguoiNopThue,
	[MaNHTraThueThay] = @MaNHTraThueThay,
	[NamPhatHanhHM] = @NamPhatHanhHM,
	[KyHieuCTHanMuc] = @KyHieuCTHanMuc,
	[SoCTHanMuc] = @SoCTHanMuc,
	[MaXDThoiHanNopThue] = @MaXDThoiHanNopThue,
	[MaNHBaoLanh] = @MaNHBaoLanh,
	[NamPhatHanhBL] = @NamPhatHanhBL,
	[KyHieuCTBaoLanh] = @KyHieuCTBaoLanh,
	[SoCTBaoLanh] = @SoCTBaoLanh,
	[NgayNhapKhoDau] = @NgayNhapKhoDau,
	[NgayKhoiHanhVC] = @NgayKhoiHanhVC,
	[DiaDiemDichVC] = @DiaDiemDichVC,
	[NgayDen] = @NgayDen,
	[GhiChu] = @GhiChu,
	[SoQuanLyNoiBoDN] = @SoQuanLyNoiBoDN,
	[MaPhanLoaiKiemTra] = @MaPhanLoaiKiemTra,
	[MaSoThueDaiDien] = @MaSoThueDaiDien,
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[NgayThayDoiDangKy] = @NgayThayDoiDangKy,
	[BieuThiTruongHopHetHan] = @BieuThiTruongHopHetHan,
	[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
	[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
	[TenDDLuuKho] = @TenDDLuuKho,
	[MaPhanLoaiTongGiaCoBan] = @MaPhanLoaiTongGiaCoBan,
	[PhanLoaiCongThucChuan] = @PhanLoaiCongThucChuan,
	[MaPhanLoaiDieuChinhTriGia] = @MaPhanLoaiDieuChinhTriGia,
	[PhuongPhapDieuChinhTriGia] = @PhuongPhapDieuChinhTriGia,
	[TongTienThuePhaiNop] = @TongTienThuePhaiNop,
	[SoTienBaoLanh] = @SoTienBaoLanh,
	[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
	[NgayCapPhep] = @NgayCapPhep,
	[PhanLoaiThamTraSauThongQuan] = @PhanLoaiThamTraSauThongQuan,
	[NgayPheDuyetBP] = @NgayPheDuyetBP,
	[NgayHoanThanhKiemTraBP] = @NgayHoanThanhKiemTraBP,
	[SoNgayDoiCapPhepNhapKhau] = @SoNgayDoiCapPhepNhapKhau,
	[TieuDe] = @TieuDe,
	[MaSacThueAnHan_VAT] = @MaSacThueAnHan_VAT,
	[TenSacThueAnHan_VAT] = @TenSacThueAnHan_VAT,
	[HanNopThueSauKhiAnHan_VAT] = @HanNopThueSauKhiAnHan_VAT,
	[PhanLoaiNopThue] = @PhanLoaiNopThue,
	[TongSoTienThueXuatKhau] = @TongSoTienThueXuatKhau,
	[MaTTTongTienThueXuatKhau] = @MaTTTongTienThueXuatKhau,
	[TongSoTienLePhi] = @TongSoTienLePhi,
	[MaTTCuaSoTienBaoLanh] = @MaTTCuaSoTienBaoLanh,
	[SoQuanLyNguoiSuDung] = @SoQuanLyNguoiSuDung,
	[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra,
	[TongSoTrangCuaToKhai] = @TongSoTrangCuaToKhai,
	[TongSoDongHangCuaToKhai] = @TongSoDongHangCuaToKhai,
	[NgayKhaiBaoNopThue] = @NgayKhaiBaoNopThue,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO



/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgLog_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MsgPhanBo_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEADieuChinh_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_HangHoa_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_HangHoa_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_HangHoa_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_HangHoa_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_HangHoa_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_HangHoa_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]    Script Date: 11/11/2013 17:40:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_Delete]    Script Date: 11/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_Delete]
-- Database: TESTVNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, October 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_MsgLog]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]    Script Date: 11/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]
-- Database: TESTVNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, October 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_MsgLog] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_Insert]    Script Date: 11/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_Insert]
-- Database: TESTVNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, October 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Insert]
	@MaNghiepVu varchar(10),
	@TieuDeThongBao nvarchar(255),
	@NoiDungThongBao nvarchar(max),
	@Log_Messages nvarchar(max),
	@Item_id bigint,
	@CreatedTime datetime,
	@MessagesTag varchar(26),
	@InputMessagesID varchar(10),
	@IndexTag nchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_MsgLog]
(
	[MaNghiepVu],
	[TieuDeThongBao],
	[NoiDungThongBao],
	[Log_Messages],
	[Item_id],
	[CreatedTime],
	[MessagesTag],
	[InputMessagesID],
	[IndexTag]
)
VALUES 
(
	@MaNghiepVu,
	@TieuDeThongBao,
	@NoiDungThongBao,
	@Log_Messages,
	@Item_id,
	@CreatedTime,
	@MessagesTag,
	@InputMessagesID,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]    Script Date: 11/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]
-- Database: TESTVNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, October 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_InsertUpdate]
	@ID bigint,
	@MaNghiepVu varchar(10),
	@TieuDeThongBao nvarchar(255),
	@NoiDungThongBao nvarchar(max),
	@Log_Messages nvarchar(max),
	@Item_id bigint,
	@CreatedTime datetime,
	@MessagesTag varchar(26),
	@InputMessagesID varchar(10),
	@IndexTag nchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_MsgLog] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_MsgLog] 
		SET
			[MaNghiepVu] = @MaNghiepVu,
			[TieuDeThongBao] = @TieuDeThongBao,
			[NoiDungThongBao] = @NoiDungThongBao,
			[Log_Messages] = @Log_Messages,
			[Item_id] = @Item_id,
			[CreatedTime] = @CreatedTime,
			[MessagesTag] = @MessagesTag,
			[InputMessagesID] = @InputMessagesID,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_MsgLog]
		(
			[MaNghiepVu],
			[TieuDeThongBao],
			[NoiDungThongBao],
			[Log_Messages],
			[Item_id],
			[CreatedTime],
			[MessagesTag],
			[InputMessagesID],
			[IndexTag]
		)
		VALUES 
		(
			@MaNghiepVu,
			@TieuDeThongBao,
			@NoiDungThongBao,
			@Log_Messages,
			@Item_id,
			@CreatedTime,
			@MessagesTag,
			@InputMessagesID,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_Load]    Script Date: 11/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_Load]
-- Database: TESTVNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, October 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNghiepVu],
	[TieuDeThongBao],
	[NoiDungThongBao],
	[Log_Messages],
	[Item_id],
	[CreatedTime],
	[MessagesTag],
	[InputMessagesID],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_MsgLog]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_SelectAll]    Script Date: 11/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_SelectAll]
-- Database: TESTVNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, October 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNghiepVu],
	[TieuDeThongBao],
	[NoiDungThongBao],
	[Log_Messages],
	[Item_id],
	[CreatedTime],
	[MessagesTag],
	[InputMessagesID],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_MsgLog]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]    Script Date: 11/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]
-- Database: TESTVNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, October 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNghiepVu],
	[TieuDeThongBao],
	[NoiDungThongBao],
	[Log_Messages],
	[Item_id],
	[CreatedTime],
	[MessagesTag],
	[InputMessagesID],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_MsgLog] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgLog_Update]    Script Date: 11/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgLog_Update]
-- Database: TESTVNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, October 27, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgLog_Update]
	@ID bigint,
	@MaNghiepVu varchar(10),
	@TieuDeThongBao nvarchar(255),
	@NoiDungThongBao nvarchar(max),
	@Log_Messages nvarchar(max),
	@Item_id bigint,
	@CreatedTime datetime,
	@MessagesTag varchar(26),
	@InputMessagesID varchar(10),
	@IndexTag nchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_MsgLog]
SET
	[MaNghiepVu] = @MaNghiepVu,
	[TieuDeThongBao] = @TieuDeThongBao,
	[NoiDungThongBao] = @NoiDungThongBao,
	[Log_Messages] = @Log_Messages,
	[Item_id] = @Item_id,
	[CreatedTime] = @CreatedTime,
	[MessagesTag] = @MessagesTag,
	[InputMessagesID] = @InputMessagesID,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_MsgPhanBo]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_MsgPhanBo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Insert]
	@Master_ID bigint,
	@TrangThai varchar(10),
	@RTPTag varchar(64),
	@messagesTag varchar(50),
	@MessagesInputID varchar(50),
	@IndexTag varchar(50),
	@MaNghiepVu varchar(10),
	@GhiChu nvarchar(255),
	@TerminalID varchar(6),
	@CreatedTime datetime,
	@SoTiepNhan varchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_MsgPhanBo]
(
	[Master_ID],
	[TrangThai],
	[RTPTag],
	[messagesTag],
	[MessagesInputID],
	[IndexTag],
	[MaNghiepVu],
	[GhiChu],
	[TerminalID],
	[CreatedTime],
	[SoTiepNhan]
)
VALUES 
(
	@Master_ID,
	@TrangThai,
	@RTPTag,
	@messagesTag,
	@MessagesInputID,
	@IndexTag,
	@MaNghiepVu,
	@GhiChu,
	@TerminalID,
	@CreatedTime,
	@SoTiepNhan
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@TrangThai varchar(10),
	@RTPTag varchar(64),
	@messagesTag varchar(50),
	@MessagesInputID varchar(50),
	@IndexTag varchar(50),
	@MaNghiepVu varchar(10),
	@GhiChu nvarchar(255),
	@TerminalID varchar(6),
	@CreatedTime datetime,
	@SoTiepNhan varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_MsgPhanBo] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_MsgPhanBo] 
		SET
			[Master_ID] = @Master_ID,
			[TrangThai] = @TrangThai,
			[RTPTag] = @RTPTag,
			[messagesTag] = @messagesTag,
			[MessagesInputID] = @MessagesInputID,
			[IndexTag] = @IndexTag,
			[MaNghiepVu] = @MaNghiepVu,
			[GhiChu] = @GhiChu,
			[TerminalID] = @TerminalID,
			[CreatedTime] = @CreatedTime,
			[SoTiepNhan] = @SoTiepNhan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_MsgPhanBo]
		(
			[Master_ID],
			[TrangThai],
			[RTPTag],
			[messagesTag],
			[MessagesInputID],
			[IndexTag],
			[MaNghiepVu],
			[GhiChu],
			[TerminalID],
			[CreatedTime],
			[SoTiepNhan]
		)
		VALUES 
		(
			@Master_ID,
			@TrangThai,
			@RTPTag,
			@messagesTag,
			@MessagesInputID,
			@IndexTag,
			@MaNghiepVu,
			@GhiChu,
			@TerminalID,
			@CreatedTime,
			@SoTiepNhan
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_Load]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[TrangThai],
	[RTPTag],
	[messagesTag],
	[MessagesInputID],
	[IndexTag],
	[MaNghiepVu],
	[GhiChu],
	[TerminalID],
	[CreatedTime],
	[SoTiepNhan]
FROM
	[dbo].[t_KDT_VNACCS_MsgPhanBo]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[TrangThai],
	[RTPTag],
	[messagesTag],
	[MessagesInputID],
	[IndexTag],
	[MaNghiepVu],
	[GhiChu],
	[TerminalID],
	[CreatedTime],
	[SoTiepNhan]
FROM
	[dbo].[t_KDT_VNACCS_MsgPhanBo]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[TrangThai],
	[RTPTag],
	[messagesTag],
	[MessagesInputID],
	[IndexTag],
	[MaNghiepVu],
	[GhiChu],
	[TerminalID],
	[CreatedTime],
	[SoTiepNhan]
FROM [dbo].[t_KDT_VNACCS_MsgPhanBo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_MsgPhanBo_Update]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MsgPhanBo_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Sunday, September 29, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MsgPhanBo_Update]
	@ID bigint,
	@Master_ID bigint,
	@TrangThai varchar(10),
	@RTPTag varchar(64),
	@messagesTag varchar(50),
	@MessagesInputID varchar(50),
	@IndexTag varchar(50),
	@MaNghiepVu varchar(10),
	@GhiChu nvarchar(255),
	@TerminalID varchar(6),
	@CreatedTime datetime,
	@SoTiepNhan varchar(50)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_MsgPhanBo]
SET
	[Master_ID] = @Master_ID,
	[TrangThai] = @TrangThai,
	[RTPTag] = @RTPTag,
	[messagesTag] = @messagesTag,
	[MessagesInputID] = @MessagesInputID,
	[IndexTag] = @IndexTag,
	[MaNghiepVu] = @MaNghiepVu,
	[GhiChu] = @GhiChu,
	[TerminalID] = @TerminalID,
	[CreatedTime] = @CreatedTime,
	[SoTiepNhan] = @SoTiepNhan
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_Delete]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TEA]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TEA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TEA_HangHoa]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TEA_HangHoa] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Insert]
	@Master_ID bigint,
	@MoTaHangHoa nvarchar(200),
	@SoLuongDangKyMT numeric(19, 4),
	@DVTSoLuongDangKyMT varchar(4),
	@SoLuongDaSuDung numeric(19, 4),
	@DVTSoLuongDaSuDung varchar(4),
	@TriGia numeric(24, 4),
	@TriGiaDuKien numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TEA_HangHoa]
(
	[Master_ID],
	[MoTaHangHoa],
	[SoLuongDangKyMT],
	[DVTSoLuongDangKyMT],
	[SoLuongDaSuDung],
	[DVTSoLuongDaSuDung],
	[TriGia],
	[TriGiaDuKien],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_ID,
	@MoTaHangHoa,
	@SoLuongDangKyMT,
	@DVTSoLuongDangKyMT,
	@SoLuongDaSuDung,
	@DVTSoLuongDaSuDung,
	@TriGia,
	@TriGiaDuKien,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MoTaHangHoa nvarchar(200),
	@SoLuongDangKyMT numeric(19, 4),
	@DVTSoLuongDangKyMT varchar(4),
	@SoLuongDaSuDung numeric(19, 4),
	@DVTSoLuongDaSuDung varchar(4),
	@TriGia numeric(24, 4),
	@TriGiaDuKien numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TEA_HangHoa] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TEA_HangHoa] 
		SET
			[Master_ID] = @Master_ID,
			[MoTaHangHoa] = @MoTaHangHoa,
			[SoLuongDangKyMT] = @SoLuongDangKyMT,
			[DVTSoLuongDangKyMT] = @DVTSoLuongDangKyMT,
			[SoLuongDaSuDung] = @SoLuongDaSuDung,
			[DVTSoLuongDaSuDung] = @DVTSoLuongDaSuDung,
			[TriGia] = @TriGia,
			[TriGiaDuKien] = @TriGiaDuKien,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TEA_HangHoa]
		(
			[Master_ID],
			[MoTaHangHoa],
			[SoLuongDangKyMT],
			[DVTSoLuongDangKyMT],
			[SoLuongDaSuDung],
			[DVTSoLuongDaSuDung],
			[TriGia],
			[TriGiaDuKien],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_ID,
			@MoTaHangHoa,
			@SoLuongDangKyMT,
			@DVTSoLuongDangKyMT,
			@SoLuongDaSuDung,
			@DVTSoLuongDaSuDung,
			@TriGia,
			@TriGiaDuKien,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MoTaHangHoa],
	[SoLuongDangKyMT],
	[DVTSoLuongDangKyMT],
	[SoLuongDaSuDung],
	[DVTSoLuongDaSuDung],
	[TriGia],
	[TriGiaDuKien],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEA_HangHoa]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MoTaHangHoa],
	[SoLuongDangKyMT],
	[DVTSoLuongDangKyMT],
	[SoLuongDaSuDung],
	[DVTSoLuongDaSuDung],
	[TriGia],
	[TriGiaDuKien],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEA_HangHoa]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MoTaHangHoa],
	[SoLuongDangKyMT],
	[DVTSoLuongDangKyMT],
	[SoLuongDaSuDung],
	[DVTSoLuongDaSuDung],
	[TriGia],
	[TriGiaDuKien],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TEA_HangHoa] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_HangHoa_Update]
	@ID bigint,
	@Master_ID bigint,
	@MoTaHangHoa nvarchar(200),
	@SoLuongDangKyMT numeric(19, 4),
	@DVTSoLuongDangKyMT varchar(4),
	@SoLuongDaSuDung numeric(19, 4),
	@DVTSoLuongDaSuDung varchar(4),
	@TriGia numeric(24, 4),
	@TriGiaDuKien numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TEA_HangHoa]
SET
	[Master_ID] = @Master_ID,
	[MoTaHangHoa] = @MoTaHangHoa,
	[SoLuongDangKyMT] = @SoLuongDangKyMT,
	[DVTSoLuongDangKyMT] = @DVTSoLuongDangKyMT,
	[SoLuongDaSuDung] = @SoLuongDaSuDung,
	[DVTSoLuongDaSuDung] = @DVTSoLuongDaSuDung,
	[TriGia] = @TriGia,
	[TriGiaDuKien] = @TriGiaDuKien,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_Insert]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Insert]
	@SoDanhMucMienThue numeric(12, 0),
	@PhanLoaiXuatNhapKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@DiaChiCuaNguoiKhai nvarchar(100),
	@SDTCuaNguoiKhai varchar(20),
	@ThoiHanMienThue datetime,
	@TenDuAnDauTu nvarchar(70),
	@DiaDiemXayDungDuAn nvarchar(100),
	@MucTieuDuAn nvarchar(100),
	@MaMienGiam varchar(5),
	@PhamViDangKyDMMT nvarchar(100),
	@NgayDuKienXNK datetime,
	@GP_GCNDauTuSo varchar(20),
	@NgayChungNhan datetime,
	@CapBoi nvarchar(100),
	@GhiChu nvarchar(255),
	@CamKetSuDung nvarchar(140),
	@MaNguoiKhai varchar(13),
	@KhaiBaoTuNgay datetime,
	@KhaiBaoDenNgay datetime,
	@MaSoQuanLyDSMT varchar(14),
	@PhanLoaiCapPhep varchar(1),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TEA]
(
	[SoDanhMucMienThue],
	[PhanLoaiXuatNhapKhau],
	[CoQuanHaiQuan],
	[DiaChiCuaNguoiKhai],
	[SDTCuaNguoiKhai],
	[ThoiHanMienThue],
	[TenDuAnDauTu],
	[DiaDiemXayDungDuAn],
	[MucTieuDuAn],
	[MaMienGiam],
	[PhamViDangKyDMMT],
	[NgayDuKienXNK],
	[GP_GCNDauTuSo],
	[NgayChungNhan],
	[CapBoi],
	[GhiChu],
	[CamKetSuDung],
	[MaNguoiKhai],
	[KhaiBaoTuNgay],
	[KhaiBaoDenNgay],
	[MaSoQuanLyDSMT],
	[PhanLoaiCapPhep],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@SoDanhMucMienThue,
	@PhanLoaiXuatNhapKhau,
	@CoQuanHaiQuan,
	@DiaChiCuaNguoiKhai,
	@SDTCuaNguoiKhai,
	@ThoiHanMienThue,
	@TenDuAnDauTu,
	@DiaDiemXayDungDuAn,
	@MucTieuDuAn,
	@MaMienGiam,
	@PhamViDangKyDMMT,
	@NgayDuKienXNK,
	@GP_GCNDauTuSo,
	@NgayChungNhan,
	@CapBoi,
	@GhiChu,
	@CamKetSuDung,
	@MaNguoiKhai,
	@KhaiBaoTuNgay,
	@KhaiBaoDenNgay,
	@MaSoQuanLyDSMT,
	@PhanLoaiCapPhep,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_InsertUpdate]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_InsertUpdate]
	@ID bigint,
	@SoDanhMucMienThue numeric(12, 0),
	@PhanLoaiXuatNhapKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@DiaChiCuaNguoiKhai nvarchar(100),
	@SDTCuaNguoiKhai varchar(20),
	@ThoiHanMienThue datetime,
	@TenDuAnDauTu nvarchar(70),
	@DiaDiemXayDungDuAn nvarchar(100),
	@MucTieuDuAn nvarchar(100),
	@MaMienGiam varchar(5),
	@PhamViDangKyDMMT nvarchar(100),
	@NgayDuKienXNK datetime,
	@GP_GCNDauTuSo varchar(20),
	@NgayChungNhan datetime,
	@CapBoi nvarchar(100),
	@GhiChu nvarchar(255),
	@CamKetSuDung nvarchar(140),
	@MaNguoiKhai varchar(13),
	@KhaiBaoTuNgay datetime,
	@KhaiBaoDenNgay datetime,
	@MaSoQuanLyDSMT varchar(14),
	@PhanLoaiCapPhep varchar(1),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TEA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TEA] 
		SET
			[SoDanhMucMienThue] = @SoDanhMucMienThue,
			[PhanLoaiXuatNhapKhau] = @PhanLoaiXuatNhapKhau,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[DiaChiCuaNguoiKhai] = @DiaChiCuaNguoiKhai,
			[SDTCuaNguoiKhai] = @SDTCuaNguoiKhai,
			[ThoiHanMienThue] = @ThoiHanMienThue,
			[TenDuAnDauTu] = @TenDuAnDauTu,
			[DiaDiemXayDungDuAn] = @DiaDiemXayDungDuAn,
			[MucTieuDuAn] = @MucTieuDuAn,
			[MaMienGiam] = @MaMienGiam,
			[PhamViDangKyDMMT] = @PhamViDangKyDMMT,
			[NgayDuKienXNK] = @NgayDuKienXNK,
			[GP_GCNDauTuSo] = @GP_GCNDauTuSo,
			[NgayChungNhan] = @NgayChungNhan,
			[CapBoi] = @CapBoi,
			[GhiChu] = @GhiChu,
			[CamKetSuDung] = @CamKetSuDung,
			[MaNguoiKhai] = @MaNguoiKhai,
			[KhaiBaoTuNgay] = @KhaiBaoTuNgay,
			[KhaiBaoDenNgay] = @KhaiBaoDenNgay,
			[MaSoQuanLyDSMT] = @MaSoQuanLyDSMT,
			[PhanLoaiCapPhep] = @PhanLoaiCapPhep,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TEA]
		(
			[SoDanhMucMienThue],
			[PhanLoaiXuatNhapKhau],
			[CoQuanHaiQuan],
			[DiaChiCuaNguoiKhai],
			[SDTCuaNguoiKhai],
			[ThoiHanMienThue],
			[TenDuAnDauTu],
			[DiaDiemXayDungDuAn],
			[MucTieuDuAn],
			[MaMienGiam],
			[PhamViDangKyDMMT],
			[NgayDuKienXNK],
			[GP_GCNDauTuSo],
			[NgayChungNhan],
			[CapBoi],
			[GhiChu],
			[CamKetSuDung],
			[MaNguoiKhai],
			[KhaiBaoTuNgay],
			[KhaiBaoDenNgay],
			[MaSoQuanLyDSMT],
			[PhanLoaiCapPhep],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@SoDanhMucMienThue,
			@PhanLoaiXuatNhapKhau,
			@CoQuanHaiQuan,
			@DiaChiCuaNguoiKhai,
			@SDTCuaNguoiKhai,
			@ThoiHanMienThue,
			@TenDuAnDauTu,
			@DiaDiemXayDungDuAn,
			@MucTieuDuAn,
			@MaMienGiam,
			@PhamViDangKyDMMT,
			@NgayDuKienXNK,
			@GP_GCNDauTuSo,
			@NgayChungNhan,
			@CapBoi,
			@GhiChu,
			@CamKetSuDung,
			@MaNguoiKhai,
			@KhaiBaoTuNgay,
			@KhaiBaoDenNgay,
			@MaSoQuanLyDSMT,
			@PhanLoaiCapPhep,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_Load]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoDanhMucMienThue],
	[PhanLoaiXuatNhapKhau],
	[CoQuanHaiQuan],
	[DiaChiCuaNguoiKhai],
	[SDTCuaNguoiKhai],
	[ThoiHanMienThue],
	[TenDuAnDauTu],
	[DiaDiemXayDungDuAn],
	[MucTieuDuAn],
	[MaMienGiam],
	[PhamViDangKyDMMT],
	[NgayDuKienXNK],
	[GP_GCNDauTuSo],
	[NgayChungNhan],
	[CapBoi],
	[GhiChu],
	[CamKetSuDung],
	[MaNguoiKhai],
	[KhaiBaoTuNgay],
	[KhaiBaoDenNgay],
	[MaSoQuanLyDSMT],
	[PhanLoaiCapPhep],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEA]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_SelectAll]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoDanhMucMienThue],
	[PhanLoaiXuatNhapKhau],
	[CoQuanHaiQuan],
	[DiaChiCuaNguoiKhai],
	[SDTCuaNguoiKhai],
	[ThoiHanMienThue],
	[TenDuAnDauTu],
	[DiaDiemXayDungDuAn],
	[MucTieuDuAn],
	[MaMienGiam],
	[PhamViDangKyDMMT],
	[NgayDuKienXNK],
	[GP_GCNDauTuSo],
	[NgayChungNhan],
	[CapBoi],
	[GhiChu],
	[CamKetSuDung],
	[MaNguoiKhai],
	[KhaiBaoTuNgay],
	[KhaiBaoDenNgay],
	[MaSoQuanLyDSMT],
	[PhanLoaiCapPhep],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEA]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_SelectDynamic]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoDanhMucMienThue],
	[PhanLoaiXuatNhapKhau],
	[CoQuanHaiQuan],
	[DiaChiCuaNguoiKhai],
	[SDTCuaNguoiKhai],
	[ThoiHanMienThue],
	[TenDuAnDauTu],
	[DiaDiemXayDungDuAn],
	[MucTieuDuAn],
	[MaMienGiam],
	[PhamViDangKyDMMT],
	[NgayDuKienXNK],
	[GP_GCNDauTuSo],
	[NgayChungNhan],
	[CapBoi],
	[GhiChu],
	[CamKetSuDung],
	[MaNguoiKhai],
	[KhaiBaoTuNgay],
	[KhaiBaoDenNgay],
	[MaSoQuanLyDSMT],
	[PhanLoaiCapPhep],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TEA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEA_Update]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Update]
	@ID bigint,
	@SoDanhMucMienThue numeric(12, 0),
	@PhanLoaiXuatNhapKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@DiaChiCuaNguoiKhai nvarchar(100),
	@SDTCuaNguoiKhai varchar(20),
	@ThoiHanMienThue datetime,
	@TenDuAnDauTu nvarchar(70),
	@DiaDiemXayDungDuAn nvarchar(100),
	@MucTieuDuAn nvarchar(100),
	@MaMienGiam varchar(5),
	@PhamViDangKyDMMT nvarchar(100),
	@NgayDuKienXNK datetime,
	@GP_GCNDauTuSo varchar(20),
	@NgayChungNhan datetime,
	@CapBoi nvarchar(100),
	@GhiChu nvarchar(255),
	@CamKetSuDung nvarchar(140),
	@MaNguoiKhai varchar(13),
	@KhaiBaoTuNgay datetime,
	@KhaiBaoDenNgay datetime,
	@MaSoQuanLyDSMT varchar(14),
	@PhanLoaiCapPhep varchar(1),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TEA]
SET
	[SoDanhMucMienThue] = @SoDanhMucMienThue,
	[PhanLoaiXuatNhapKhau] = @PhanLoaiXuatNhapKhau,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[DiaChiCuaNguoiKhai] = @DiaChiCuaNguoiKhai,
	[SDTCuaNguoiKhai] = @SDTCuaNguoiKhai,
	[ThoiHanMienThue] = @ThoiHanMienThue,
	[TenDuAnDauTu] = @TenDuAnDauTu,
	[DiaDiemXayDungDuAn] = @DiaDiemXayDungDuAn,
	[MucTieuDuAn] = @MucTieuDuAn,
	[MaMienGiam] = @MaMienGiam,
	[PhamViDangKyDMMT] = @PhamViDangKyDMMT,
	[NgayDuKienXNK] = @NgayDuKienXNK,
	[GP_GCNDauTuSo] = @GP_GCNDauTuSo,
	[NgayChungNhan] = @NgayChungNhan,
	[CapBoi] = @CapBoi,
	[GhiChu] = @GhiChu,
	[CamKetSuDung] = @CamKetSuDung,
	[MaNguoiKhai] = @MaNguoiKhai,
	[KhaiBaoTuNgay] = @KhaiBaoTuNgay,
	[KhaiBaoDenNgay] = @KhaiBaoDenNgay,
	[MaSoQuanLyDSMT] = @MaSoQuanLyDSMT,
	[PhanLoaiCapPhep] = @PhanLoaiCapPhep,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TEADieuChinh]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TEADieuChinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]    Script Date: 11/11/2013 17:40:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Insert]
	@Master_ID bigint,
	@LanDieuChinhGP_GCN int,
	@ChungNhanDieuChinhSo varchar(20),
	@NgayChungNhanDieuChinh datetime,
	@DieuChinhBoi nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TEADieuChinh]
(
	[Master_ID],
	[LanDieuChinhGP_GCN],
	[ChungNhanDieuChinhSo],
	[NgayChungNhanDieuChinh],
	[DieuChinhBoi],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_ID,
	@LanDieuChinhGP_GCN,
	@ChungNhanDieuChinhSo,
	@NgayChungNhanDieuChinh,
	@DieuChinhBoi,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@LanDieuChinhGP_GCN int,
	@ChungNhanDieuChinhSo varchar(20),
	@NgayChungNhanDieuChinh datetime,
	@DieuChinhBoi nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TEADieuChinh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TEADieuChinh] 
		SET
			[Master_ID] = @Master_ID,
			[LanDieuChinhGP_GCN] = @LanDieuChinhGP_GCN,
			[ChungNhanDieuChinhSo] = @ChungNhanDieuChinhSo,
			[NgayChungNhanDieuChinh] = @NgayChungNhanDieuChinh,
			[DieuChinhBoi] = @DieuChinhBoi,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TEADieuChinh]
		(
			[Master_ID],
			[LanDieuChinhGP_GCN],
			[ChungNhanDieuChinhSo],
			[NgayChungNhanDieuChinh],
			[DieuChinhBoi],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_ID,
			@LanDieuChinhGP_GCN,
			@ChungNhanDieuChinhSo,
			@NgayChungNhanDieuChinh,
			@DieuChinhBoi,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_Load]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LanDieuChinhGP_GCN],
	[ChungNhanDieuChinhSo],
	[NgayChungNhanDieuChinh],
	[DieuChinhBoi],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEADieuChinh]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[LanDieuChinhGP_GCN],
	[ChungNhanDieuChinhSo],
	[NgayChungNhanDieuChinh],
	[DieuChinhBoi],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEADieuChinh]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[LanDieuChinhGP_GCN],
	[ChungNhanDieuChinhSo],
	[NgayChungNhanDieuChinh],
	[DieuChinhBoi],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TEADieuChinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEADieuChinh_Update]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEADieuChinh_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEADieuChinh_Update]
	@ID bigint,
	@Master_ID bigint,
	@LanDieuChinhGP_GCN int,
	@ChungNhanDieuChinhSo varchar(20),
	@NgayChungNhanDieuChinh datetime,
	@DieuChinhBoi nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TEADieuChinh]
SET
	[Master_ID] = @Master_ID,
	[LanDieuChinhGP_GCN] = @LanDieuChinhGP_GCN,
	[ChungNhanDieuChinhSo] = @ChungNhanDieuChinhSo,
	[NgayChungNhanDieuChinh] = @NgayChungNhanDieuChinh,
	[DieuChinhBoi] = @DieuChinhBoi,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TEANguoiXNK]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TEANguoiXNK] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Insert]
	@Master_ID bigint,
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TEANguoiXNK]
(
	[Master_ID],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_ID,
	@MaNguoiXNK,
	@TenNguoiXNK,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TEANguoiXNK] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TEANguoiXNK] 
		SET
			[Master_ID] = @Master_ID,
			[MaNguoiXNK] = @MaNguoiXNK,
			[TenNguoiXNK] = @TenNguoiXNK,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TEANguoiXNK]
		(
			[Master_ID],
			[MaNguoiXNK],
			[TenNguoiXNK],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_ID,
			@MaNguoiXNK,
			@TenNguoiXNK,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEANguoiXNK]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEANguoiXNK]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TEANguoiXNK] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEANguoiXNK_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TEANguoiXNK]
SET
	[Master_ID] = @Master_ID,
	[MaNguoiXNK] = @MaNguoiXNK,
	[TenNguoiXNK] = @TenNguoiXNK,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_Delete]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TIA]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TIA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_Delete]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_HangHoa_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TIA_HangHoa]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_DeleteDynamic]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_HangHoa_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TIA_HangHoa] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_Insert]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_HangHoa_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_Insert]
	@Master_ID bigint,
	@MaSoHangHoa varchar(12),
	@SoLuongBanDau numeric(19, 4),
	@DVTSoLuongBanDau varchar(4),
	@SoLuongDaTaiNhapTaiXuat numeric(19, 4),
	@DVTSoLuongDaTaiNhapTaiXuat varchar(4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TIA_HangHoa]
(
	[Master_ID],
	[MaSoHangHoa],
	[SoLuongBanDau],
	[DVTSoLuongBanDau],
	[SoLuongDaTaiNhapTaiXuat],
	[DVTSoLuongDaTaiNhapTaiXuat],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_ID,
	@MaSoHangHoa,
	@SoLuongBanDau,
	@DVTSoLuongBanDau,
	@SoLuongDaTaiNhapTaiXuat,
	@DVTSoLuongDaTaiNhapTaiXuat,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_InsertUpdate]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_HangHoa_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaSoHangHoa varchar(12),
	@SoLuongBanDau numeric(19, 4),
	@DVTSoLuongBanDau varchar(4),
	@SoLuongDaTaiNhapTaiXuat numeric(19, 4),
	@DVTSoLuongDaTaiNhapTaiXuat varchar(4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TIA_HangHoa] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TIA_HangHoa] 
		SET
			[Master_ID] = @Master_ID,
			[MaSoHangHoa] = @MaSoHangHoa,
			[SoLuongBanDau] = @SoLuongBanDau,
			[DVTSoLuongBanDau] = @DVTSoLuongBanDau,
			[SoLuongDaTaiNhapTaiXuat] = @SoLuongDaTaiNhapTaiXuat,
			[DVTSoLuongDaTaiNhapTaiXuat] = @DVTSoLuongDaTaiNhapTaiXuat,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TIA_HangHoa]
		(
			[Master_ID],
			[MaSoHangHoa],
			[SoLuongBanDau],
			[DVTSoLuongBanDau],
			[SoLuongDaTaiNhapTaiXuat],
			[DVTSoLuongDaTaiNhapTaiXuat],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_ID,
			@MaSoHangHoa,
			@SoLuongBanDau,
			@DVTSoLuongBanDau,
			@SoLuongDaTaiNhapTaiXuat,
			@DVTSoLuongDaTaiNhapTaiXuat,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_Load]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_HangHoa_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSoHangHoa],
	[SoLuongBanDau],
	[DVTSoLuongBanDau],
	[SoLuongDaTaiNhapTaiXuat],
	[DVTSoLuongDaTaiNhapTaiXuat],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TIA_HangHoa]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectAll]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSoHangHoa],
	[SoLuongBanDau],
	[DVTSoLuongBanDau],
	[SoLuongDaTaiNhapTaiXuat],
	[DVTSoLuongDaTaiNhapTaiXuat],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TIA_HangHoa]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectDynamic]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaSoHangHoa],
	[SoLuongBanDau],
	[DVTSoLuongBanDau],
	[SoLuongDaTaiNhapTaiXuat],
	[DVTSoLuongDaTaiNhapTaiXuat],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TIA_HangHoa] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_HangHoa_Update]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_HangHoa_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_HangHoa_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaSoHangHoa varchar(12),
	@SoLuongBanDau numeric(19, 4),
	@DVTSoLuongBanDau varchar(4),
	@SoLuongDaTaiNhapTaiXuat numeric(19, 4),
	@DVTSoLuongDaTaiNhapTaiXuat varchar(4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TIA_HangHoa]
SET
	[Master_ID] = @Master_ID,
	[MaSoHangHoa] = @MaSoHangHoa,
	[SoLuongBanDau] = @SoLuongBanDau,
	[DVTSoLuongBanDau] = @DVTSoLuongBanDau,
	[SoLuongDaTaiNhapTaiXuat] = @SoLuongDaTaiNhapTaiXuat,
	[DVTSoLuongDaTaiNhapTaiXuat] = @DVTSoLuongDaTaiNhapTaiXuat,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_Insert]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Insert]
	@SoToKhai numeric(18, 0),
	@CoBaoNhapKhauXuatKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@MaNguoiKhaiDauTien varchar(5),
	@TenNguoiKhaiDauTien nvarchar(50),
	@MaNguoiXuatNhapKhau varchar(13),
	@TenNguoiXuatNhapKhau nvarchar(100),
	@ThoiHanTaiXuatNhap datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TIA]
(
	[SoToKhai],
	[CoBaoNhapKhauXuatKhau],
	[CoQuanHaiQuan],
	[MaNguoiKhaiDauTien],
	[TenNguoiKhaiDauTien],
	[MaNguoiXuatNhapKhau],
	[TenNguoiXuatNhapKhau],
	[ThoiHanTaiXuatNhap],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@SoToKhai,
	@CoBaoNhapKhauXuatKhau,
	@CoQuanHaiQuan,
	@MaNguoiKhaiDauTien,
	@TenNguoiKhaiDauTien,
	@MaNguoiXuatNhapKhau,
	@TenNguoiXuatNhapKhau,
	@ThoiHanTaiXuatNhap,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_InsertUpdate]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_InsertUpdate]
	@ID bigint,
	@SoToKhai numeric(18, 0),
	@CoBaoNhapKhauXuatKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@MaNguoiKhaiDauTien varchar(5),
	@TenNguoiKhaiDauTien nvarchar(50),
	@MaNguoiXuatNhapKhau varchar(13),
	@TenNguoiXuatNhapKhau nvarchar(100),
	@ThoiHanTaiXuatNhap datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TIA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TIA] 
		SET
			[SoToKhai] = @SoToKhai,
			[CoBaoNhapKhauXuatKhau] = @CoBaoNhapKhauXuatKhau,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[MaNguoiKhaiDauTien] = @MaNguoiKhaiDauTien,
			[TenNguoiKhaiDauTien] = @TenNguoiKhaiDauTien,
			[MaNguoiXuatNhapKhau] = @MaNguoiXuatNhapKhau,
			[TenNguoiXuatNhapKhau] = @TenNguoiXuatNhapKhau,
			[ThoiHanTaiXuatNhap] = @ThoiHanTaiXuatNhap,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TIA]
		(
			[SoToKhai],
			[CoBaoNhapKhauXuatKhau],
			[CoQuanHaiQuan],
			[MaNguoiKhaiDauTien],
			[TenNguoiKhaiDauTien],
			[MaNguoiXuatNhapKhau],
			[TenNguoiXuatNhapKhau],
			[ThoiHanTaiXuatNhap],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@SoToKhai,
			@CoBaoNhapKhauXuatKhau,
			@CoQuanHaiQuan,
			@MaNguoiKhaiDauTien,
			@TenNguoiKhaiDauTien,
			@MaNguoiXuatNhapKhau,
			@TenNguoiXuatNhapKhau,
			@ThoiHanTaiXuatNhap,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_Load]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[CoBaoNhapKhauXuatKhau],
	[CoQuanHaiQuan],
	[MaNguoiKhaiDauTien],
	[TenNguoiKhaiDauTien],
	[MaNguoiXuatNhapKhau],
	[TenNguoiXuatNhapKhau],
	[ThoiHanTaiXuatNhap],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TIA]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_SelectAll]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[CoBaoNhapKhauXuatKhau],
	[CoQuanHaiQuan],
	[MaNguoiKhaiDauTien],
	[TenNguoiKhaiDauTien],
	[MaNguoiXuatNhapKhau],
	[TenNguoiXuatNhapKhau],
	[ThoiHanTaiXuatNhap],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TIA]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_SelectDynamic]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoToKhai],
	[CoBaoNhapKhauXuatKhau],
	[CoQuanHaiQuan],
	[MaNguoiKhaiDauTien],
	[TenNguoiKhaiDauTien],
	[MaNguoiXuatNhapKhau],
	[TenNguoiXuatNhapKhau],
	[ThoiHanTaiXuatNhap],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TIA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TIA_Update]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Update]
	@ID bigint,
	@SoToKhai numeric(18, 0),
	@CoBaoNhapKhauXuatKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@MaNguoiKhaiDauTien varchar(5),
	@TenNguoiKhaiDauTien nvarchar(50),
	@MaNguoiXuatNhapKhau varchar(13),
	@TenNguoiXuatNhapKhau nvarchar(100),
	@ThoiHanTaiXuatNhap datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TIA]
SET
	[SoToKhai] = @SoToKhai,
	[CoBaoNhapKhauXuatKhau] = @CoBaoNhapKhauXuatKhau,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[MaNguoiKhaiDauTien] = @MaNguoiKhaiDauTien,
	[TenNguoiKhaiDauTien] = @TenNguoiKhaiDauTien,
	[MaNguoiXuatNhapKhau] = @MaNguoiXuatNhapKhau,
	[TenNguoiXuatNhapKhau] = @TenNguoiXuatNhapKhau,
	[ThoiHanTaiXuatNhap] = @ThoiHanTaiXuatNhap,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TKTriGiaThap]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TKTriGiaThap] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]    Script Date: 11/11/2013 17:40:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]
	@MaLoaiHinh varchar(1),
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSua varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@SoHouseAWB varchar(20),
	@SoMasterAWB varchar(20),
	@SoLuong numeric(12, 4),
	@TrongLuong numeric(14, 4),
	@MaDDLuuKho varchar(7),
	@TenMayBayChoHang nvarchar(12),
	@NgayHangDen datetime,
	@MaCangDoHang varchar(3),
	@MaDDNhanHangCuoiCung varchar(5),
	@MaDiaDiemXepHang varchar(6),
	@TongTriGiaTinhThue numeric(24, 4),
	@MaTTTongTriGiaTinhThue varchar(3),
	@GiaKhaiBao numeric(12, 4),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MoTaHangHoa nvarchar(200),
	@MaNuocXuatXu varchar(2),
	@TriGiaTinhThue numeric(24, 4),
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@TrangThaiXuLy nchar(10),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TKTriGiaThap]
(
	[MaLoaiHinh],
	[SoToKhai],
	[PhanLoaiBaoCaoSua],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[SoHouseAWB],
	[SoMasterAWB],
	[SoLuong],
	[TrongLuong],
	[MaDDLuuKho],
	[TenMayBayChoHang],
	[NgayHangDen],
	[MaCangDoHang],
	[MaDDNhanHangCuoiCung],
	[MaDiaDiemXepHang],
	[TongTriGiaTinhThue],
	[MaTTTongTriGiaTinhThue],
	[GiaKhaiBao],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[MoTaHangHoa],
	[MaNuocXuatXu],
	[TriGiaTinhThue],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@MaLoaiHinh,
	@SoToKhai,
	@PhanLoaiBaoCaoSua,
	@PhanLoaiToChuc,
	@CoQuanHaiQuan,
	@NhomXuLyHS,
	@MaDonVi,
	@TenDonVi,
	@MaBuuChinhDonVi,
	@DiaChiDonVi,
	@SoDienThoaiDonVi,
	@MaDoiTac,
	@TenDoiTac,
	@MaBuuChinhDoiTac,
	@DiaChiDoiTac1,
	@DiaChiDoiTac2,
	@DiaChiDoiTac3,
	@DiaChiDoiTac4,
	@MaNuocDoiTac,
	@SoHouseAWB,
	@SoMasterAWB,
	@SoLuong,
	@TrongLuong,
	@MaDDLuuKho,
	@TenMayBayChoHang,
	@NgayHangDen,
	@MaCangDoHang,
	@MaDDNhanHangCuoiCung,
	@MaDiaDiemXepHang,
	@TongTriGiaTinhThue,
	@MaTTTongTriGiaTinhThue,
	@GiaKhaiBao,
	@PhanLoaiGiaHD,
	@MaDieuKienGiaHD,
	@MaTTHoaDon,
	@TongTriGiaHD,
	@MaPhanLoaiPhiVC,
	@MaTTPhiVC,
	@PhiVanChuyen,
	@MaPhanLoaiPhiBH,
	@MaTTPhiBH,
	@PhiBaoHiem,
	@MoTaHangHoa,
	@MaNuocXuatXu,
	@TriGiaTinhThue,
	@GhiChu,
	@SoQuanLyNoiBoDN,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]    Script Date: 11/11/2013 17:40:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_InsertUpdate]
	@ID bigint,
	@MaLoaiHinh varchar(1),
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSua varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@SoHouseAWB varchar(20),
	@SoMasterAWB varchar(20),
	@SoLuong numeric(12, 4),
	@TrongLuong numeric(14, 4),
	@MaDDLuuKho varchar(7),
	@TenMayBayChoHang nvarchar(12),
	@NgayHangDen datetime,
	@MaCangDoHang varchar(3),
	@MaDDNhanHangCuoiCung varchar(5),
	@MaDiaDiemXepHang varchar(6),
	@TongTriGiaTinhThue numeric(24, 4),
	@MaTTTongTriGiaTinhThue varchar(3),
	@GiaKhaiBao numeric(12, 4),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MoTaHangHoa nvarchar(200),
	@MaNuocXuatXu varchar(2),
	@TriGiaTinhThue numeric(24, 4),
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@TrangThaiXuLy nchar(10),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TKTriGiaThap] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TKTriGiaThap] 
		SET
			[MaLoaiHinh] = @MaLoaiHinh,
			[SoToKhai] = @SoToKhai,
			[PhanLoaiBaoCaoSua] = @PhanLoaiBaoCaoSua,
			[PhanLoaiToChuc] = @PhanLoaiToChuc,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHS] = @NhomXuLyHS,
			[MaDonVi] = @MaDonVi,
			[TenDonVi] = @TenDonVi,
			[MaBuuChinhDonVi] = @MaBuuChinhDonVi,
			[DiaChiDonVi] = @DiaChiDonVi,
			[SoDienThoaiDonVi] = @SoDienThoaiDonVi,
			[MaDoiTac] = @MaDoiTac,
			[TenDoiTac] = @TenDoiTac,
			[MaBuuChinhDoiTac] = @MaBuuChinhDoiTac,
			[DiaChiDoiTac1] = @DiaChiDoiTac1,
			[DiaChiDoiTac2] = @DiaChiDoiTac2,
			[DiaChiDoiTac3] = @DiaChiDoiTac3,
			[DiaChiDoiTac4] = @DiaChiDoiTac4,
			[MaNuocDoiTac] = @MaNuocDoiTac,
			[SoHouseAWB] = @SoHouseAWB,
			[SoMasterAWB] = @SoMasterAWB,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[MaDDLuuKho] = @MaDDLuuKho,
			[TenMayBayChoHang] = @TenMayBayChoHang,
			[NgayHangDen] = @NgayHangDen,
			[MaCangDoHang] = @MaCangDoHang,
			[MaDDNhanHangCuoiCung] = @MaDDNhanHangCuoiCung,
			[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[MaTTTongTriGiaTinhThue] = @MaTTTongTriGiaTinhThue,
			[GiaKhaiBao] = @GiaKhaiBao,
			[PhanLoaiGiaHD] = @PhanLoaiGiaHD,
			[MaDieuKienGiaHD] = @MaDieuKienGiaHD,
			[MaTTHoaDon] = @MaTTHoaDon,
			[TongTriGiaHD] = @TongTriGiaHD,
			[MaPhanLoaiPhiVC] = @MaPhanLoaiPhiVC,
			[MaTTPhiVC] = @MaTTPhiVC,
			[PhiVanChuyen] = @PhiVanChuyen,
			[MaPhanLoaiPhiBH] = @MaPhanLoaiPhiBH,
			[MaTTPhiBH] = @MaTTPhiBH,
			[PhiBaoHiem] = @PhiBaoHiem,
			[MoTaHangHoa] = @MoTaHangHoa,
			[MaNuocXuatXu] = @MaNuocXuatXu,
			[TriGiaTinhThue] = @TriGiaTinhThue,
			[GhiChu] = @GhiChu,
			[SoQuanLyNoiBoDN] = @SoQuanLyNoiBoDN,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TKTriGiaThap]
		(
			[MaLoaiHinh],
			[SoToKhai],
			[PhanLoaiBaoCaoSua],
			[PhanLoaiToChuc],
			[CoQuanHaiQuan],
			[NhomXuLyHS],
			[MaDonVi],
			[TenDonVi],
			[MaBuuChinhDonVi],
			[DiaChiDonVi],
			[SoDienThoaiDonVi],
			[MaDoiTac],
			[TenDoiTac],
			[MaBuuChinhDoiTac],
			[DiaChiDoiTac1],
			[DiaChiDoiTac2],
			[DiaChiDoiTac3],
			[DiaChiDoiTac4],
			[MaNuocDoiTac],
			[SoHouseAWB],
			[SoMasterAWB],
			[SoLuong],
			[TrongLuong],
			[MaDDLuuKho],
			[TenMayBayChoHang],
			[NgayHangDen],
			[MaCangDoHang],
			[MaDDNhanHangCuoiCung],
			[MaDiaDiemXepHang],
			[TongTriGiaTinhThue],
			[MaTTTongTriGiaTinhThue],
			[GiaKhaiBao],
			[PhanLoaiGiaHD],
			[MaDieuKienGiaHD],
			[MaTTHoaDon],
			[TongTriGiaHD],
			[MaPhanLoaiPhiVC],
			[MaTTPhiVC],
			[PhiVanChuyen],
			[MaPhanLoaiPhiBH],
			[MaTTPhiBH],
			[PhiBaoHiem],
			[MoTaHangHoa],
			[MaNuocXuatXu],
			[TriGiaTinhThue],
			[GhiChu],
			[SoQuanLyNoiBoDN],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@MaLoaiHinh,
			@SoToKhai,
			@PhanLoaiBaoCaoSua,
			@PhanLoaiToChuc,
			@CoQuanHaiQuan,
			@NhomXuLyHS,
			@MaDonVi,
			@TenDonVi,
			@MaBuuChinhDonVi,
			@DiaChiDonVi,
			@SoDienThoaiDonVi,
			@MaDoiTac,
			@TenDoiTac,
			@MaBuuChinhDoiTac,
			@DiaChiDoiTac1,
			@DiaChiDoiTac2,
			@DiaChiDoiTac3,
			@DiaChiDoiTac4,
			@MaNuocDoiTac,
			@SoHouseAWB,
			@SoMasterAWB,
			@SoLuong,
			@TrongLuong,
			@MaDDLuuKho,
			@TenMayBayChoHang,
			@NgayHangDen,
			@MaCangDoHang,
			@MaDDNhanHangCuoiCung,
			@MaDiaDiemXepHang,
			@TongTriGiaTinhThue,
			@MaTTTongTriGiaTinhThue,
			@GiaKhaiBao,
			@PhanLoaiGiaHD,
			@MaDieuKienGiaHD,
			@MaTTHoaDon,
			@TongTriGiaHD,
			@MaPhanLoaiPhiVC,
			@MaTTPhiVC,
			@PhiVanChuyen,
			@MaPhanLoaiPhiBH,
			@MaTTPhiBH,
			@PhiBaoHiem,
			@MoTaHangHoa,
			@MaNuocXuatXu,
			@TriGiaTinhThue,
			@GhiChu,
			@SoQuanLyNoiBoDN,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]    Script Date: 11/11/2013 17:40:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaLoaiHinh],
	[SoToKhai],
	[PhanLoaiBaoCaoSua],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[SoHouseAWB],
	[SoMasterAWB],
	[SoLuong],
	[TrongLuong],
	[MaDDLuuKho],
	[TenMayBayChoHang],
	[NgayHangDen],
	[MaCangDoHang],
	[MaDDNhanHangCuoiCung],
	[MaDiaDiemXepHang],
	[TongTriGiaTinhThue],
	[MaTTTongTriGiaTinhThue],
	[GiaKhaiBao],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[MoTaHangHoa],
	[MaNuocXuatXu],
	[TriGiaTinhThue],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TKTriGiaThap]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]    Script Date: 11/11/2013 17:40:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaLoaiHinh],
	[SoToKhai],
	[PhanLoaiBaoCaoSua],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[SoHouseAWB],
	[SoMasterAWB],
	[SoLuong],
	[TrongLuong],
	[MaDDLuuKho],
	[TenMayBayChoHang],
	[NgayHangDen],
	[MaCangDoHang],
	[MaDDNhanHangCuoiCung],
	[MaDiaDiemXepHang],
	[TongTriGiaTinhThue],
	[MaTTTongTriGiaTinhThue],
	[GiaKhaiBao],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[MoTaHangHoa],
	[MaNuocXuatXu],
	[TriGiaTinhThue],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TKTriGiaThap]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]    Script Date: 11/11/2013 17:40:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaLoaiHinh],
	[SoToKhai],
	[PhanLoaiBaoCaoSua],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[SoHouseAWB],
	[SoMasterAWB],
	[SoLuong],
	[TrongLuong],
	[MaDDLuuKho],
	[TenMayBayChoHang],
	[NgayHangDen],
	[MaCangDoHang],
	[MaDDNhanHangCuoiCung],
	[MaDiaDiemXepHang],
	[TongTriGiaTinhThue],
	[MaTTTongTriGiaTinhThue],
	[GiaKhaiBao],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[MoTaHangHoa],
	[MaNuocXuatXu],
	[TriGiaTinhThue],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TKTriGiaThap] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]    Script Date: 11/11/2013 17:40:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]
	@ID bigint,
	@MaLoaiHinh varchar(1),
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSua varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@SoHouseAWB varchar(20),
	@SoMasterAWB varchar(20),
	@SoLuong numeric(12, 4),
	@TrongLuong numeric(14, 4),
	@MaDDLuuKho varchar(7),
	@TenMayBayChoHang nvarchar(12),
	@NgayHangDen datetime,
	@MaCangDoHang varchar(3),
	@MaDDNhanHangCuoiCung varchar(5),
	@MaDiaDiemXepHang varchar(6),
	@TongTriGiaTinhThue numeric(24, 4),
	@MaTTTongTriGiaTinhThue varchar(3),
	@GiaKhaiBao numeric(12, 4),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MoTaHangHoa nvarchar(200),
	@MaNuocXuatXu varchar(2),
	@TriGiaTinhThue numeric(24, 4),
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@TrangThaiXuLy nchar(10),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TKTriGiaThap]
SET
	[MaLoaiHinh] = @MaLoaiHinh,
	[SoToKhai] = @SoToKhai,
	[PhanLoaiBaoCaoSua] = @PhanLoaiBaoCaoSua,
	[PhanLoaiToChuc] = @PhanLoaiToChuc,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHS] = @NhomXuLyHS,
	[MaDonVi] = @MaDonVi,
	[TenDonVi] = @TenDonVi,
	[MaBuuChinhDonVi] = @MaBuuChinhDonVi,
	[DiaChiDonVi] = @DiaChiDonVi,
	[SoDienThoaiDonVi] = @SoDienThoaiDonVi,
	[MaDoiTac] = @MaDoiTac,
	[TenDoiTac] = @TenDoiTac,
	[MaBuuChinhDoiTac] = @MaBuuChinhDoiTac,
	[DiaChiDoiTac1] = @DiaChiDoiTac1,
	[DiaChiDoiTac2] = @DiaChiDoiTac2,
	[DiaChiDoiTac3] = @DiaChiDoiTac3,
	[DiaChiDoiTac4] = @DiaChiDoiTac4,
	[MaNuocDoiTac] = @MaNuocDoiTac,
	[SoHouseAWB] = @SoHouseAWB,
	[SoMasterAWB] = @SoMasterAWB,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[MaDDLuuKho] = @MaDDLuuKho,
	[TenMayBayChoHang] = @TenMayBayChoHang,
	[NgayHangDen] = @NgayHangDen,
	[MaCangDoHang] = @MaCangDoHang,
	[MaDDNhanHangCuoiCung] = @MaDDNhanHangCuoiCung,
	[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[MaTTTongTriGiaTinhThue] = @MaTTTongTriGiaTinhThue,
	[GiaKhaiBao] = @GiaKhaiBao,
	[PhanLoaiGiaHD] = @PhanLoaiGiaHD,
	[MaDieuKienGiaHD] = @MaDieuKienGiaHD,
	[MaTTHoaDon] = @MaTTHoaDon,
	[TongTriGiaHD] = @TongTriGiaHD,
	[MaPhanLoaiPhiVC] = @MaPhanLoaiPhiVC,
	[MaTTPhiVC] = @MaTTPhiVC,
	[PhiVanChuyen] = @PhiVanChuyen,
	[MaPhanLoaiPhiBH] = @MaPhanLoaiPhiBH,
	[MaTTPhiBH] = @MaTTPhiBH,
	[PhiBaoHiem] = @PhiBaoHiem,
	[MoTaHangHoa] = @MoTaHangHoa,
	[MaNuocXuatXu] = @MaNuocXuatXu,
	[TriGiaTinhThue] = @TriGiaTinhThue,
	[GhiChu] = @GhiChu,
	[SoQuanLyNoiBoDN] = @SoQuanLyNoiBoDN,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '12.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('12.5', GETDATE(), N'Cap nhat store (VNACCS)')
END	

