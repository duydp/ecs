﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_OutsourcingManufactureFactory_Product : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long OutsourcingManufactureFactory_ManufactureFactory_ID { set; get; }
		public string MaSP { set; get; }
		public string MaHS { set; get; }
		public decimal ChuKySXTG { set; get; }
		public int ChuKySXDVT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_OutsourcingManufactureFactory_Product> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_OutsourcingManufactureFactory_Product> collection = new List<KDT_VNACCS_OutsourcingManufactureFactory_Product>();
			while (reader.Read())
			{
				KDT_VNACCS_OutsourcingManufactureFactory_Product entity = new KDT_VNACCS_OutsourcingManufactureFactory_Product();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("OutsourcingManufactureFactory_ManufactureFactory_ID"))) entity.OutsourcingManufactureFactory_ManufactureFactory_ID = reader.GetInt64(reader.GetOrdinal("OutsourcingManufactureFactory_ManufactureFactory_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChuKySXTG"))) entity.ChuKySXTG = reader.GetDecimal(reader.GetOrdinal("ChuKySXTG"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChuKySXDVT"))) entity.ChuKySXDVT = reader.GetInt32(reader.GetOrdinal("ChuKySXDVT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_OutsourcingManufactureFactory_Product> collection, long id)
        {
            foreach (KDT_VNACCS_OutsourcingManufactureFactory_Product item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_OutsourcingManufactureFactory_Product VALUES(@OutsourcingManufactureFactory_ManufactureFactory_ID, @MaSP, @MaHS, @ChuKySXTG, @ChuKySXDVT)";
            string update = "UPDATE t_KDT_VNACCS_OutsourcingManufactureFactory_Product SET OutsourcingManufactureFactory_ManufactureFactory_ID = @OutsourcingManufactureFactory_ManufactureFactory_ID, MaSP = @MaSP, MaHS = @MaHS, ChuKySXTG = @ChuKySXTG, ChuKySXDVT = @ChuKySXDVT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_OutsourcingManufactureFactory_Product WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, "OutsourcingManufactureFactory_ManufactureFactory_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.NVarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.NVarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuKySXTG", SqlDbType.Decimal, "ChuKySXTG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuKySXDVT", SqlDbType.Int, "ChuKySXDVT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, "OutsourcingManufactureFactory_ManufactureFactory_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.NVarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.NVarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuKySXTG", SqlDbType.Decimal, "ChuKySXTG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuKySXDVT", SqlDbType.Int, "ChuKySXDVT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_OutsourcingManufactureFactory_Product VALUES(@OutsourcingManufactureFactory_ManufactureFactory_ID, @MaSP, @MaHS, @ChuKySXTG, @ChuKySXDVT)";
            string update = "UPDATE t_KDT_VNACCS_OutsourcingManufactureFactory_Product SET OutsourcingManufactureFactory_ManufactureFactory_ID = @OutsourcingManufactureFactory_ManufactureFactory_ID, MaSP = @MaSP, MaHS = @MaHS, ChuKySXTG = @ChuKySXTG, ChuKySXDVT = @ChuKySXDVT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_OutsourcingManufactureFactory_Product WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, "OutsourcingManufactureFactory_ManufactureFactory_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.NVarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.NVarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuKySXTG", SqlDbType.Decimal, "ChuKySXTG", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChuKySXDVT", SqlDbType.Int, "ChuKySXDVT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, "OutsourcingManufactureFactory_ManufactureFactory_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.NVarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.NVarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuKySXTG", SqlDbType.Decimal, "ChuKySXTG", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChuKySXDVT", SqlDbType.Int, "ChuKySXDVT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_OutsourcingManufactureFactory_Product Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_OutsourcingManufactureFactory_Product> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_OutsourcingManufactureFactory_Product> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_OutsourcingManufactureFactory_Product> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_OutsourcingManufactureFactory_Product> SelectCollectionBy_OutsourcingManufactureFactory_ManufactureFactory_ID(long outsourcingManufactureFactory_ManufactureFactory_ID)
		{
            IDataReader reader = SelectReaderBy_OutsourcingManufactureFactory_ManufactureFactory_ID(outsourcingManufactureFactory_ManufactureFactory_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID(long outsourcingManufactureFactory_ManufactureFactory_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, outsourcingManufactureFactory_ManufactureFactory_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_OutsourcingManufactureFactory_ManufactureFactory_ID(long outsourcingManufactureFactory_ManufactureFactory_ID)
		{
			const string spName = "p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, outsourcingManufactureFactory_ManufactureFactory_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_OutsourcingManufactureFactory_Product(long outsourcingManufactureFactory_ManufactureFactory_ID, string maSP, string maHS, decimal chuKySXTG, int chuKySXDVT)
		{
			KDT_VNACCS_OutsourcingManufactureFactory_Product entity = new KDT_VNACCS_OutsourcingManufactureFactory_Product();	
			entity.OutsourcingManufactureFactory_ManufactureFactory_ID = outsourcingManufactureFactory_ManufactureFactory_ID;
			entity.MaSP = maSP;
			entity.MaHS = maHS;
			entity.ChuKySXTG = chuKySXTG;
			entity.ChuKySXDVT = chuKySXDVT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, OutsourcingManufactureFactory_ManufactureFactory_ID);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.NVarChar, MaSP);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
			db.AddInParameter(dbCommand, "@ChuKySXTG", SqlDbType.Decimal, ChuKySXTG);
			db.AddInParameter(dbCommand, "@ChuKySXDVT", SqlDbType.Int, ChuKySXDVT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_OutsourcingManufactureFactory_Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_OutsourcingManufactureFactory_Product item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_OutsourcingManufactureFactory_Product(long id, long outsourcingManufactureFactory_ManufactureFactory_ID, string maSP, string maHS, decimal chuKySXTG, int chuKySXDVT)
		{
			KDT_VNACCS_OutsourcingManufactureFactory_Product entity = new KDT_VNACCS_OutsourcingManufactureFactory_Product();			
			entity.ID = id;
			entity.OutsourcingManufactureFactory_ManufactureFactory_ID = outsourcingManufactureFactory_ManufactureFactory_ID;
			entity.MaSP = maSP;
			entity.MaHS = maHS;
			entity.ChuKySXTG = chuKySXTG;
			entity.ChuKySXDVT = chuKySXDVT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_OutsourcingManufactureFactory_Product_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, OutsourcingManufactureFactory_ManufactureFactory_ID);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.NVarChar, MaSP);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
			db.AddInParameter(dbCommand, "@ChuKySXTG", SqlDbType.Decimal, ChuKySXTG);
			db.AddInParameter(dbCommand, "@ChuKySXDVT", SqlDbType.Int, ChuKySXDVT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_OutsourcingManufactureFactory_Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_OutsourcingManufactureFactory_Product item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_OutsourcingManufactureFactory_Product(long id, long outsourcingManufactureFactory_ManufactureFactory_ID, string maSP, string maHS, decimal chuKySXTG, int chuKySXDVT)
		{
			KDT_VNACCS_OutsourcingManufactureFactory_Product entity = new KDT_VNACCS_OutsourcingManufactureFactory_Product();			
			entity.ID = id;
			entity.OutsourcingManufactureFactory_ManufactureFactory_ID = outsourcingManufactureFactory_ManufactureFactory_ID;
			entity.MaSP = maSP;
			entity.MaHS = maHS;
			entity.ChuKySXTG = chuKySXTG;
			entity.ChuKySXDVT = chuKySXDVT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, OutsourcingManufactureFactory_ManufactureFactory_ID);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.NVarChar, MaSP);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
			db.AddInParameter(dbCommand, "@ChuKySXTG", SqlDbType.Decimal, ChuKySXTG);
			db.AddInParameter(dbCommand, "@ChuKySXDVT", SqlDbType.Int, ChuKySXDVT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_OutsourcingManufactureFactory_Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_OutsourcingManufactureFactory_Product item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int DeleteKDT_VNACCS_OutsourcingManufactureFactory_Product(long id)
		{
			KDT_VNACCS_OutsourcingManufactureFactory_Product entity = new KDT_VNACCS_OutsourcingManufactureFactory_Product();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID(long outsourcingManufactureFactory_ManufactureFactory_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@OutsourcingManufactureFactory_ManufactureFactory_ID", SqlDbType.BigInt, outsourcingManufactureFactory_ManufactureFactory_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_OutsourcingManufactureFactory_Product> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_OutsourcingManufactureFactory_Product item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}