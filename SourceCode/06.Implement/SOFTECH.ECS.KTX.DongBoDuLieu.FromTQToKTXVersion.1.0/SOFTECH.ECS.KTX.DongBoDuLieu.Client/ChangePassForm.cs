﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;


namespace DongBo
{
    public partial class ChangePassForm : Form
    {
        public ChangePassForm()
        {
            InitializeComponent();
        }
        public string maHQ;
        public string maDN;
        public string matKhau;
        private string matKhauCu;
        private  string matKhauMoi;

        private void txtOldPass_TextChanged(object sender, EventArgs e)
        {

        }
        private void ChangePass( string maDN, string oldPass, string newPass)
        {
            //DongBoDuLieu.BLL.WebServiceDB.g WS = new DongBoDuLieu.BLL.WebServiceDB();
        
            DongBoDuLieu.BLL.WebServiceDB.ChangePass( maDN, oldPass, newPass);
        }
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }
        private void btnChangePass_Click(object sender, EventArgs e)
        {
            if (txtOldPass.Text.Length < 0)
            {
                MessageBox.Show("Mật khẩu cũ không được để trống");
                return;
            }
            //if (!txtOldPass.Text.Equals(matKhau))
            //{
            //    MessageBox.Show("Mật khẩu cũ không đúng");
            //    return;
            //}
            if (!txtNewPass.Text.Equals(txtRePass.Text ))
            {
                MessageBox.Show("Mật khẩu cũ  và mật khẩu mới không trùng nhau");
                return;
            }
            matKhauCu = GetMD5Value(Properties.Settings.Default.MaDoanhNghiep.Trim() + "+" + txtOldPass.Text.Trim());
            matKhauMoi = GetMD5Value(Properties.Settings.Default.MaDoanhNghiep.Trim() + "+" + txtNewPass.Text.Trim());

            try
            {
                this.ChangePass( maDN, matKhauCu, matKhauMoi);
                MessageBox.Show(" Đổi mật khẩu thành công ", "Thông báo"); 

            }
            catch (Exception ex) { MessageBox.Show(" Đổi mật khẩu không thành công,lỗi : " + ex.Message.ToString(), "Thông báo"); }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}