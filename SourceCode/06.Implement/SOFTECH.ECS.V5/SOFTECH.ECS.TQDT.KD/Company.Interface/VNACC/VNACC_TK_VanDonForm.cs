﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_TK_VanDonForm : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        DataTable dt = new DataTable();
        public VNACC_TK_VanDonForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                int sott = 1;
                TKMD.VanDonCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow item in dt.Rows)
                {
                    KDT_VNACC_TK_SoVanDon vd = new KDT_VNACC_TK_SoVanDon();
                    if (item["ID"].ToString().Length != 0)
                    {
                        vd.ID = Convert.ToInt32(item["ID"].ToString());
                        vd.TKMD_ID = Convert.ToInt32(item["TKMD_ID"].ToString());
                    }
                    vd.SoTT = sott;
                    vd.SoVanDon = item["SoVanDon"].ToString();
                    TKMD.VanDonCollection.Add(vd);
                    sott++;
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void VNACC_VanDonForm_Load(object sender, EventArgs e)
        {
            dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.VanDonCollection);
            grList.DataSource = dt;
        }


        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa thông tin vận đơn này không?", true) == "Yes")
                    {
                        KDT_VNACC_TK_SoVanDon.DeleteDynamic("ID=" + ID);
                        grList.CurrentRow.Delete();
                    }
                }
                else
                    grList.CurrentRow.Delete();
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
    }
}
