﻿namespace Company.Interface
{
    partial class VNACC_ChuyenCuaKhau_ManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ChuyenCuaKhau_ManagerForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnTimKiem = new Janus.Windows.EditControls.UIButton();
            this.txtNamDangKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbbTrangThaiXuLy = new Janus.Windows.EditControls.UIComboBox();
            this.cbbPhanLoaiXuatNhap = new Janus.Windows.EditControls.UIComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyTờKhaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1070, 626);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrMaHaiQuan);
            this.uiGroupBox1.Controls.Add(this.btnExportExcel);
            this.uiGroupBox1.Controls.Add(this.btnTimKiem);
            this.uiGroupBox1.Controls.Add(this.txtNamDangKy);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.cbbTrangThaiXuLy);
            this.uiGroupBox1.Controls.Add(this.cbbPhanLoaiXuatNhap);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1070, 83);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Tìm kiếm";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaHaiQuan
            // 
            this.ctrMaHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrMaHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrMaHaiQuan.Code = "";
            this.ctrMaHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHaiQuan.IsOnlyWarning = false;
            this.ctrMaHaiQuan.IsValidate = true;
            this.ctrMaHaiQuan.Location = new System.Drawing.Point(96, 17);
            this.ctrMaHaiQuan.Name = "ctrMaHaiQuan";
            this.ctrMaHaiQuan.Name_VN = "";
            this.ctrMaHaiQuan.SetOnlyWarning = false;
            this.ctrMaHaiQuan.SetValidate = false;
            this.ctrMaHaiQuan.ShowColumnCode = true;
            this.ctrMaHaiQuan.ShowColumnName = true;
            this.ctrMaHaiQuan.Size = new System.Drawing.Size(273, 21);
            this.ctrMaHaiQuan.TabIndex = 21;
            this.ctrMaHaiQuan.TagCode = "";
            this.ctrMaHaiQuan.TagName = "";
            this.ctrMaHaiQuan.Where = null;
            this.ctrMaHaiQuan.WhereCondition = "";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(740, 52);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(110, 23);
            this.btnExportExcel.TabIndex = 20;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Image = ((System.Drawing.Image)(resources.GetObject("btnTimKiem.Image")));
            this.btnTimKiem.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTimKiem.Location = new System.Drawing.Point(740, 21);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(110, 23);
            this.btnTimKiem.TabIndex = 20;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtNamDangKy
            // 
            this.txtNamDangKy.Location = new System.Drawing.Point(622, 21);
            this.txtNamDangKy.Name = "txtNamDangKy";
            this.txtNamDangKy.Size = new System.Drawing.Size(112, 21);
            this.txtNamDangKy.TabIndex = 14;
            this.txtNamDangKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNamDangKy.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Location = new System.Drawing.Point(435, 21);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(106, 21);
            this.txtSoToKhai.TabIndex = 14;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoToKhai.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(373, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Trạng thái";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Hải quan";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Loại hình XNK";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(375, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Số tờ khai";
            // 
            // cbbTrangThaiXuLy
            // 
            this.cbbTrangThaiXuLy.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Chưa khai báo";
            uiComboBoxItem1.Value = "0";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Khai báo tạm";
            uiComboBoxItem2.Value = "1";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Khai báo chính thức";
            uiComboBoxItem3.Value = "2";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Đã duyệt";
            uiComboBoxItem4.Value = "3";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Sửa tờ khai";
            uiComboBoxItem5.Value = "4";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Tờ khai đã Hủy";
            uiComboBoxItem6.Value = "5";
            this.cbbTrangThaiXuLy.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbbTrangThaiXuLy.Location = new System.Drawing.Point(435, 52);
            this.cbbTrangThaiXuLy.Name = "cbbTrangThaiXuLy";
            this.cbbTrangThaiXuLy.Size = new System.Drawing.Size(299, 21);
            this.cbbTrangThaiXuLy.TabIndex = 12;
            this.cbbTrangThaiXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbTrangThaiXuLy.SelectedValueChanged += new System.EventHandler(this.cbbTrangThaiXuLy_SelectedValueChanged);
            // 
            // cbbPhanLoaiXuatNhap
            // 
            this.cbbPhanLoaiXuatNhap.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "-- Tất cả--";
            uiComboBoxItem7.Value = "0";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Xuất khẩu";
            uiComboBoxItem8.Value = "E";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Nhập khẩu";
            uiComboBoxItem9.Value = "I";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Loại khác";
            uiComboBoxItem10.Value = "C";
            this.cbbPhanLoaiXuatNhap.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10});
            this.cbbPhanLoaiXuatNhap.Location = new System.Drawing.Point(96, 47);
            this.cbbPhanLoaiXuatNhap.Name = "cbbPhanLoaiXuatNhap";
            this.cbbPhanLoaiXuatNhap.Size = new System.Drawing.Size(251, 21);
            this.cbbPhanLoaiXuatNhap.TabIndex = 13;
            this.cbbPhanLoaiXuatNhap.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbPhanLoaiXuatNhap.SelectedIndexChanged += new System.EventHandler(this.cbbPhanLoaiXuatNhap_SelectedIndexChanged);
            this.cbbPhanLoaiXuatNhap.SelectedValueChanged += new System.EventHandler(this.cbbPhanLoaiXuatNhap_SelectedValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(547, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Năm đăng ký";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.grList);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 83);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1070, 543);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Danh sách tờ khai";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.ColumnAutoResize = true;
            this.grList.ContextMenuStrip = this.contextMenuStrip2;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grList.FrozenColumns = 6;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.LineAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(3, 17);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(1064, 492);
            this.grList.TabIndex = 0;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grList_DeletingRecord);
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            this.grList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_LoadingRow);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyTờKhaiToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(142, 26);
            // 
            // copyTờKhaiToolStripMenuItem
            // 
            this.copyTờKhaiToolStripMenuItem.Name = "copyTờKhaiToolStripMenuItem";
            this.copyTờKhaiToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.copyTờKhaiToolStripMenuItem.Text = "Copy tờ khai";
            this.copyTờKhaiToolStripMenuItem.Click += new System.EventHandler(this.tsmiCopy_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.btnXoa);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 509);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1064, 31);
            this.uiGroupBox3.TabIndex = 1;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(965, 6);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(90, 23);
            this.btnXoa.TabIndex = 20;
            this.btnXoa.Text = "Xóa TK ";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // VNACC_ChuyenCuaKhau_ManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 626);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ChuyenCuaKhau_ManagerForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Theo dõi tờ khai vận chuyển";
            this.Load += new System.EventHandler(this.VNACC_ChuyenCuaKhau_ManagerForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIButton btnTimKiem;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIComboBox cbbTrangThaiXuLy;
        private Janus.Windows.EditControls.UIComboBox cbbPhanLoaiXuatNhap;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDangKy;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaHaiQuan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiCopy;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem copyTờKhaiToolStripMenuItem;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
    }
}