﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TK_GiayPhepForm : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        DataTable dt = new DataTable();
        public VNACC_TK_GiayPhepForm()
        {
            InitializeComponent();
        }

        private void VNACC_TK_GiayPhepForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.GiayPhepCollection);
            grList.DataSource = dt;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int sott = 1;
                TKMD.GiayPhepCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow i in dt.Rows)
                {
                    KDT_VNACC_TK_GiayPhep gp = new KDT_VNACC_TK_GiayPhep();
                    if (i["ID"].ToString().Length != 0)
                    {
                        gp.ID = Convert.ToInt32(i["ID"].ToString());
                        gp.TKMD_ID = Convert.ToInt32(i["TKMD_ID"].ToString());
                    }
                    gp.SoTT = sott;
                    gp.PhanLoai = i["PhanLoai"].ToString();
                    gp.SoGiayPhep = i["SoGiayPhep"].ToString();
                    TKMD.GiayPhepCollection.Add(gp);
                    sott++;
                }
                ShowMessage("Lưu thành công", false);


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa giấy phép này không?", true) == "Yes")
                    {
                        KDT_VNACC_TK_GiayPhep.DeleteDynamic("ID=" + ID);
                        grList.CurrentRow.Delete();
                    }
                }
                else
                    grList.CurrentRow.Delete();
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

    }
}
