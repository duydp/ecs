﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
   public class PNK_AdditionalDocuments
    {
        /// <summary>
        /// Thông tin về chứng từ khác
        /// </summary>
        [XmlElement("AdditionalDocument")]
        public List<PNK_AdditionalDocument> AdditionalDocument { get; set; }

    }
}
