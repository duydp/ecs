-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_ThietBi_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_ThietBi_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Insert]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@NuocXX_ID char(3),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(50),
	@GhiChu nvarchar(256)
AS
INSERT INTO [dbo].[t_KDT_GC_ThietBi]
(
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@NuocXX_ID,
	@DonGia,
	@TriGia,
	@NguyenTe_ID,
	@STTHang,
	@TinhTrang,
	@GhiChu
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@NuocXX_ID char(3),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(50),
	@GhiChu nvarchar(256)
AS

UPDATE
	[dbo].[t_KDT_GC_ThietBi]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[NuocXX_ID] = @NuocXX_ID,
	[DonGia] = @DonGia,
	[TriGia] = @TriGia,
	[NguyenTe_ID] = @NguyenTe_ID,
	[STTHang] = @STTHang,
	[TinhTrang] = @TinhTrang,
	[GhiChu] = @GhiChu
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_InsertUpdate]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@NuocXX_ID char(3),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(50),
	@GhiChu nvarchar(256)
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_KDT_GC_ThietBi] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_ThietBi] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[NuocXX_ID] = @NuocXX_ID,
			[DonGia] = @DonGia,
			[TriGia] = @TriGia,
			[NguyenTe_ID] = @NguyenTe_ID,
			[STTHang] = @STTHang,
			[TinhTrang] = @TinhTrang,
			[GhiChu] = @GhiChu
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_ThietBi]
	(
			[HopDong_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[NuocXX_ID],
			[DonGia],
			[TriGia],
			[NguyenTe_ID],
			[STTHang],
			[TinhTrang],
			[GhiChu]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@NuocXX_ID,
			@DonGia,
			@TriGia,
			@NguyenTe_ID,
			@STTHang,
			@TinhTrang,
			@GhiChu
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Delete]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_ThietBi]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_ThietBi]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_ThietBi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_Load]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_ThietBi]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_ThietBi]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu]
FROM [dbo].[t_KDT_GC_ThietBi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_ThietBi_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_ThietBi_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NuocXX_ID],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_ThietBi]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_SanPham_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_SanPham_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Insert]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@DonGia numeric(18, 5)
AS
INSERT INTO [dbo].[t_KDT_GC_SanPham]
(
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@NhomSanPham_ID,
	@STTHang,
	@DonGia
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@DonGia numeric(18, 5)
AS

UPDATE
	[dbo].[t_KDT_GC_SanPham]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[NhomSanPham_ID] = @NhomSanPham_ID,
	[STTHang] = @STTHang,
	[DonGia] = @DonGia
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_InsertUpdate]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@DonGia numeric(18, 5)
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_KDT_GC_SanPham] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_SanPham] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[NhomSanPham_ID] = @NhomSanPham_ID,
			[STTHang] = @STTHang,
			[DonGia] = @DonGia
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_SanPham]
	(
			[HopDong_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[NhomSanPham_ID],
			[STTHang],
			[DonGia]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@NhomSanPham_ID,
			@STTHang,
			@DonGia
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Delete]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_SanPham] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_Load]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia]
FROM
	[dbo].[t_KDT_GC_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia]
FROM
	[dbo].[t_KDT_GC_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia]
FROM [dbo].[t_KDT_GC_SanPham] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_SanPham_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_SanPham_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[NhomSanPham_ID],
	[STTHang],
	[DonGia]
FROM
	[dbo].[t_KDT_GC_SanPham]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NhomSanPham_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Insert]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong money,
	@STTHang int,
	@TenSanPham nvarchar(250),
	@TriGia float
AS
INSERT INTO [dbo].[t_KDT_GC_NhomSanPham]
(
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia]
)
VALUES
(
	@HopDong_ID,
	@MaSanPham,
	@SoLuong,
	@GiaGiaCong,
	@STTHang,
	@TenSanPham,
	@TriGia
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Update]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong money,
	@STTHang int,
	@TenSanPham nvarchar(250),
	@TriGia float
AS

UPDATE
	[dbo].[t_KDT_GC_NhomSanPham]
SET
	[SoLuong] = @SoLuong,
	[GiaGiaCong] = @GiaGiaCong,
	[STTHang] = @STTHang,
	[TenSanPham] = @TenSanPham,
	[TriGia] = @TriGia
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_InsertUpdate]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong money,
	@STTHang int,
	@TenSanPham nvarchar(250),
	@TriGia float
AS
IF EXISTS(SELECT [HopDong_ID], [MaSanPham] FROM [dbo].[t_KDT_GC_NhomSanPham] WHERE [HopDong_ID] = @HopDong_ID AND [MaSanPham] = @MaSanPham)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NhomSanPham] 
		SET
			[SoLuong] = @SoLuong,
			[GiaGiaCong] = @GiaGiaCong,
			[STTHang] = @STTHang,
			[TenSanPham] = @TenSanPham,
			[TriGia] = @TriGia
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [MaSanPham] = @MaSanPham
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_NhomSanPham]
	(
			[HopDong_ID],
			[MaSanPham],
			[SoLuong],
			[GiaGiaCong],
			[STTHang],
			[TenSanPham],
			[TriGia]
	)
	VALUES
	(
			@HopDong_ID,
			@MaSanPham,
			@SoLuong,
			@GiaGiaCong,
			@STTHang,
			@TenSanPham,
			@TriGia
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Delete]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NhomSanPham]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_NhomSanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NhomSanPham] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_Load]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia]
FROM
	[dbo].[t_KDT_GC_NhomSanPham]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia]
FROM
	[dbo].[t_KDT_GC_NhomSanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia]
FROM [dbo].[t_KDT_GC_NhomSanPham] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NhomSanPham_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NhomSanPham_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TriGia]
FROM
	[dbo].[t_KDT_GC_NhomSanPham]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_NguyenPhuLieu_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Insert]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@STTHang int,
	@DonGia numeric(18, 5),
	@GhiChu nvarchar(256)
AS
INSERT INTO [dbo].[t_KDT_GC_NguyenPhuLieu]
(
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@STTHang,
	@DonGia,
	@GhiChu
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@STTHang int,
	@DonGia numeric(18, 5),
	@GhiChu nvarchar(256)
AS

UPDATE
	[dbo].[t_KDT_GC_NguyenPhuLieu]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[STTHang] = @STTHang,
	[DonGia] = @DonGia,
	[GhiChu] = @GhiChu
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_InsertUpdate]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@STTHang int,
	@DonGia numeric(18, 5),
	@GhiChu nvarchar(256)
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_KDT_GC_NguyenPhuLieu] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NguyenPhuLieu] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[STTHang] = @STTHang,
			[DonGia] = @DonGia,
			[GhiChu] = @GhiChu
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_NguyenPhuLieu]
	(
			[HopDong_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[STTHang],
			[DonGia],
			[GhiChu]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@STTHang,
			@DonGia,
			@GhiChu
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Delete]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NguyenPhuLieu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_Load]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu]
FROM [dbo].[t_KDT_GC_NguyenPhuLieu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_NguyenPhuLieu_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_NguyenPhuLieu_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[DonGia],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_NguyenPhuLieu]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangMau_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangMau_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Insert]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@STTHang int,
	@GhiChu nvarchar(1000)
AS
INSERT INTO [dbo].[t_KDT_GC_HangMau]
(
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[GhiChu]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@STTHang,
	@GhiChu
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@STTHang int,
	@GhiChu nvarchar(1000)
AS

UPDATE
	[dbo].[t_KDT_GC_HangMau]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[STTHang] = @STTHang,
	[GhiChu] = @GhiChu
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_InsertUpdate]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@STTHang int,
	@GhiChu nvarchar(1000)
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_KDT_GC_HangMau] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HangMau] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[STTHang] = @STTHang,
			[GhiChu] = @GhiChu
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_GC_HangMau]
	(
			[HopDong_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[STTHang],
			[GhiChu]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@STTHang,
			@GhiChu
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Delete]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HangMau]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HangMau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_Load]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_HangMau]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[GhiChu]
FROM [dbo].[t_KDT_GC_HangMau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangMau_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangMau_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[STTHang],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_HangMau]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_Update]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_Load]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_GC_ThietBi_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_ThietBi_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_Insert]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaNhap numeric(18, 5),
	@NuocXX_ID char(3),
	@TinhTrang nvarchar(255),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TrangThai int
AS
INSERT INTO [dbo].[t_GC_ThietBi]
(
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[NuocXX_ID],
	[TinhTrang],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TrangThai]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@SoLuongDaNhap,
	@NuocXX_ID,
	@TinhTrang,
	@DonGia,
	@TriGia,
	@NguyenTe_ID,
	@STTHang,
	@TrangThai
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaNhap numeric(18, 5),
	@NuocXX_ID char(3),
	@TinhTrang nvarchar(255),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TrangThai int
AS

UPDATE
	[dbo].[t_GC_ThietBi]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[SoLuongDaNhap] = @SoLuongDaNhap,
	[NuocXX_ID] = @NuocXX_ID,
	[TinhTrang] = @TinhTrang,
	[DonGia] = @DonGia,
	[TriGia] = @TriGia,
	[NguyenTe_ID] = @NguyenTe_ID,
	[STTHang] = @STTHang,
	[TrangThai] = @TrangThai
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_InsertUpdate]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaNhap numeric(18, 5),
	@NuocXX_ID char(3),
	@TinhTrang nvarchar(255),
	@DonGia float,
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TrangThai int
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_GC_ThietBi] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_GC_ThietBi] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[SoLuongDaNhap] = @SoLuongDaNhap,
			[NuocXX_ID] = @NuocXX_ID,
			[TinhTrang] = @TinhTrang,
			[DonGia] = @DonGia,
			[TriGia] = @TriGia,
			[NguyenTe_ID] = @NguyenTe_ID,
			[STTHang] = @STTHang,
			[TrangThai] = @TrangThai
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_ThietBi]
	(
			[HopDong_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[SoLuongDaNhap],
			[NuocXX_ID],
			[TinhTrang],
			[DonGia],
			[TriGia],
			[NguyenTe_ID],
			[STTHang],
			[TrangThai]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@SoLuongDaNhap,
			@NuocXX_ID,
			@TinhTrang,
			@DonGia,
			@TriGia,
			@NguyenTe_ID,
			@STTHang,
			@TrangThai
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_Delete]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_GC_ThietBi]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_GC_ThietBi]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_ThietBi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_Load]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[NuocXX_ID],
	[TinhTrang],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TrangThai]
FROM
	[dbo].[t_GC_ThietBi]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[NuocXX_ID],
	[TinhTrang],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TrangThai]
FROM
	[dbo].[t_GC_ThietBi]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[NuocXX_ID],
	[TinhTrang],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TrangThai]
FROM [dbo].[t_GC_ThietBi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThietBi_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_ThietBi_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[NuocXX_ID],
	[TinhTrang],
	[DonGia],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TrangThai]
FROM
	[dbo].[t_GC_ThietBi]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_Update]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_Load]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_GC_SanPham_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_SanPham_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_Insert]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaXuat numeric(18, 5),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS
INSERT INTO [dbo].[t_GC_SanPham]
(
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaXuat],
	[NhomSanPham_ID],
	[STTHang],
	[TrangThai],
	[DonGia]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@SoLuongDaXuat,
	@NhomSanPham_ID,
	@STTHang,
	@TrangThai,
	@DonGia
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaXuat numeric(18, 5),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS

UPDATE
	[dbo].[t_GC_SanPham]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[SoLuongDaXuat] = @SoLuongDaXuat,
	[NhomSanPham_ID] = @NhomSanPham_ID,
	[STTHang] = @STTHang,
	[TrangThai] = @TrangThai,
	[DonGia] = @DonGia
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_InsertUpdate]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 5),
	@SoLuongDaXuat numeric(18, 5),
	@NhomSanPham_ID varchar(12),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_GC_SanPham] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_GC_SanPham] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[SoLuongDaXuat] = @SoLuongDaXuat,
			[NhomSanPham_ID] = @NhomSanPham_ID,
			[STTHang] = @STTHang,
			[TrangThai] = @TrangThai,
			[DonGia] = @DonGia
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_SanPham]
	(
			[HopDong_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[SoLuongDaXuat],
			[NhomSanPham_ID],
			[STTHang],
			[TrangThai],
			[DonGia]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@SoLuongDaXuat,
			@NhomSanPham_ID,
			@STTHang,
			@TrangThai,
			@DonGia
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_Delete]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_GC_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_GC_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_SanPham] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_Load]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaXuat],
	[NhomSanPham_ID],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM
	[dbo].[t_GC_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaXuat],
	[NhomSanPham_ID],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM
	[dbo].[t_GC_SanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_SanPham_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaXuat],
	[NhomSanPham_ID],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM [dbo].[t_GC_SanPham] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_SanPham_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
------------- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_Update]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_Load]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_GC_NhomSanPham_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NhomSanPham_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_Insert]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong money,
	@STTHang int,
	@TenSanPham nvarchar(250),
	@TrangThai int
AS
INSERT INTO [dbo].[t_GC_NhomSanPham]
(
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TrangThai]
)
VALUES
(
	@HopDong_ID,
	@MaSanPham,
	@SoLuong,
	@GiaGiaCong,
	@STTHang,
	@TenSanPham,
	@TrangThai
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_Update]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong money,
	@STTHang int,
	@TenSanPham nvarchar(250),
	@TrangThai int
AS

UPDATE
	[dbo].[t_GC_NhomSanPham]
SET
	[SoLuong] = @SoLuong,
	[GiaGiaCong] = @GiaGiaCong,
	[STTHang] = @STTHang,
	[TenSanPham] = @TenSanPham,
	[TrangThai] = @TrangThai
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_InsertUpdate]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50),
	@SoLuong numeric(18, 5),
	@GiaGiaCong money,
	@STTHang int,
	@TenSanPham nvarchar(250),
	@TrangThai int
AS
IF EXISTS(SELECT [HopDong_ID], [MaSanPham] FROM [dbo].[t_GC_NhomSanPham] WHERE [HopDong_ID] = @HopDong_ID AND [MaSanPham] = @MaSanPham)
	BEGIN
		UPDATE
			[dbo].[t_GC_NhomSanPham] 
		SET
			[SoLuong] = @SoLuong,
			[GiaGiaCong] = @GiaGiaCong,
			[STTHang] = @STTHang,
			[TenSanPham] = @TenSanPham,
			[TrangThai] = @TrangThai
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [MaSanPham] = @MaSanPham
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_NhomSanPham]
	(
			[HopDong_ID],
			[MaSanPham],
			[SoLuong],
			[GiaGiaCong],
			[STTHang],
			[TenSanPham],
			[TrangThai]
	)
	VALUES
	(
			@HopDong_ID,
			@MaSanPham,
			@SoLuong,
			@GiaGiaCong,
			@STTHang,
			@TenSanPham,
			@TrangThai
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_Delete]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_GC_NhomSanPham]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_GC_NhomSanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_NhomSanPham] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_Load]
	@HopDong_ID bigint,
	@MaSanPham nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TrangThai]
FROM
	[dbo].[t_GC_NhomSanPham]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TrangThai]
FROM
	[dbo].[t_GC_NhomSanPham]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TrangThai]
FROM [dbo].[t_GC_NhomSanPham] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NhomSanPham_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NhomSanPham_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[SoLuong],
	[GiaGiaCong],
	[STTHang],
	[TenSanPham],
	[TrangThai]
FROM
	[dbo].[t_GC_NhomSanPham]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Update]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Load]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_GC_NguyenPhuLieu_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_NguyenPhuLieu_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Insert]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@SoLuongDaNhap numeric(18, 6),
	@SoLuongDaDung numeric(18, 6),
	@SoLuongCungUng numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS
INSERT INTO [dbo].[t_GC_NguyenPhuLieu]
(
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@SoLuongDaNhap,
	@SoLuongDaDung,
	@SoLuongCungUng,
	@TongNhuCau,
	@STTHang,
	@TrangThai,
	@DonGia
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@SoLuongDaNhap numeric(18, 6),
	@SoLuongDaDung numeric(18, 6),
	@SoLuongCungUng numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS

UPDATE
	[dbo].[t_GC_NguyenPhuLieu]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[SoLuongDaNhap] = @SoLuongDaNhap,
	[SoLuongDaDung] = @SoLuongDaDung,
	[SoLuongCungUng] = @SoLuongCungUng,
	[TongNhuCau] = @TongNhuCau,
	[STTHang] = @STTHang,
	[TrangThai] = @TrangThai,
	[DonGia] = @DonGia
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_InsertUpdate]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@SoLuongDaNhap numeric(18, 6),
	@SoLuongDaDung numeric(18, 6),
	@SoLuongCungUng numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_GC_NguyenPhuLieu] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_GC_NguyenPhuLieu] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[SoLuongDaNhap] = @SoLuongDaNhap,
			[SoLuongDaDung] = @SoLuongDaDung,
			[SoLuongCungUng] = @SoLuongCungUng,
			[TongNhuCau] = @TongNhuCau,
			[STTHang] = @STTHang,
			[TrangThai] = @TrangThai,
			[DonGia] = @DonGia
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_NguyenPhuLieu]
	(
			[HopDong_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[SoLuongDaNhap],
			[SoLuongDaDung],
			[SoLuongCungUng],
			[TongNhuCau],
			[STTHang],
			[TrangThai],
			[DonGia]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@SoLuongDaNhap,
			@SoLuongDaDung,
			@SoLuongCungUng,
			@TongNhuCau,
			@STTHang,
			@TrangThai,
			@DonGia
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Delete]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_GC_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_GC_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_NguyenPhuLieu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_Load]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM
	[dbo].[t_GC_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM
	[dbo].[t_GC_NguyenPhuLieu]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM [dbo].[t_GC_NguyenPhuLieu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_NguyenPhuLieu_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_NguyenPhuLieu_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM
	[dbo].[t_GC_NguyenPhuLieu]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_HangMau_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_HangMau_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_Update]

IF OBJECT_ID(N'[dbo].[p_GC_HangMau_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_HangMau_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_HangMau_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_HangMau_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_Load]

IF OBJECT_ID(N'[dbo].[p_GC_HangMau_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_HangMau_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_HangMau_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_HangMau_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_HangMau_Insert]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@SoLuongDaNhap numeric(18, 6),
	@SoLuongDaDung numeric(18, 6),
	@SoLuongCungUng numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS
INSERT INTO [dbo].[t_GC_HangMau]
(
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
)
VALUES
(
	@HopDong_ID,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@SoLuongDangKy,
	@SoLuongDaNhap,
	@SoLuongDaDung,
	@SoLuongCungUng,
	@TongNhuCau,
	@STTHang,
	@TrangThai,
	@DonGia
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_HangMau_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_HangMau_Update]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@SoLuongDaNhap numeric(18, 6),
	@SoLuongDaDung numeric(18, 6),
	@SoLuongCungUng numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS

UPDATE
	[dbo].[t_GC_HangMau]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[SoLuongDangKy] = @SoLuongDangKy,
	[SoLuongDaNhap] = @SoLuongDaNhap,
	[SoLuongDaDung] = @SoLuongDaDung,
	[SoLuongCungUng] = @SoLuongCungUng,
	[TongNhuCau] = @TongNhuCau,
	[STTHang] = @STTHang,
	[TrangThai] = @TrangThai,
	[DonGia] = @DonGia
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_HangMau_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_HangMau_InsertUpdate]
	@HopDong_ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@SoLuongDangKy numeric(18, 6),
	@SoLuongDaNhap numeric(18, 6),
	@SoLuongDaDung numeric(18, 6),
	@SoLuongCungUng numeric(18, 6),
	@TongNhuCau numeric(18, 6),
	@STTHang int,
	@TrangThai int,
	@DonGia numeric(18, 5)
AS
IF EXISTS(SELECT [HopDong_ID], [Ma] FROM [dbo].[t_GC_HangMau] WHERE [HopDong_ID] = @HopDong_ID AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_GC_HangMau] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[SoLuongDangKy] = @SoLuongDangKy,
			[SoLuongDaNhap] = @SoLuongDaNhap,
			[SoLuongDaDung] = @SoLuongDaDung,
			[SoLuongCungUng] = @SoLuongCungUng,
			[TongNhuCau] = @TongNhuCau,
			[STTHang] = @STTHang,
			[TrangThai] = @TrangThai,
			[DonGia] = @DonGia
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_HangMau]
	(
			[HopDong_ID],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[SoLuongDangKy],
			[SoLuongDaNhap],
			[SoLuongDaDung],
			[SoLuongCungUng],
			[TongNhuCau],
			[STTHang],
			[TrangThai],
			[DonGia]
	)
	VALUES
	(
			@HopDong_ID,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@SoLuongDangKy,
			@SoLuongDaNhap,
			@SoLuongDaDung,
			@SoLuongCungUng,
			@TongNhuCau,
			@STTHang,
			@TrangThai,
			@DonGia
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_HangMau_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_HangMau_Delete]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_GC_HangMau]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_HangMau_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_HangMau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_HangMau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_HangMau_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_HangMau_Load]
	@HopDong_ID bigint,
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM
	[dbo].[t_GC_HangMau]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_HangMau_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_HangMau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM [dbo].[t_GC_HangMau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_HangMau_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_HangMau_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaNhap],
	[SoLuongDaDung],
	[SoLuongCungUng],
	[TongNhuCau],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM
	[dbo].[t_GC_HangMau]	

GO


CREATE PROCEDURE [dbo].[p_GC_SanPham_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[SoLuongDangKy],
	[SoLuongDaXuat],
	[NhomSanPham_ID],
	[STTHang],
	[TrangThai],
	[DonGia]
FROM
	[dbo].[t_GC_SanPham]	

GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_SelectBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_SelectBy_Master_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_HangPhuKien_DeleteBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_DeleteBy_Master_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Insert]
	@MaHang varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@ThongTinCu nvarchar(255),
	@SoLuong numeric(18, 5),
	@DonGia float,
	@DVT_ID char(3),
	@NuocXX_ID char(3),
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(255),
	@Master_ID bigint,
	@NhomSP nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_HangPhuKien]
(
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP]
)
VALUES 
(
	@MaHang,
	@TenHang,
	@MaHS,
	@ThongTinCu,
	@SoLuong,
	@DonGia,
	@DVT_ID,
	@NuocXX_ID,
	@TriGia,
	@NguyenTe_ID,
	@STTHang,
	@TinhTrang,
	@Master_ID,
	@NhomSP
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Update]
	@ID bigint,
	@MaHang varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@ThongTinCu nvarchar(255),
	@SoLuong numeric(18, 5),
	@DonGia float,
	@DVT_ID char(3),
	@NuocXX_ID char(3),
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(255),
	@Master_ID bigint,
	@NhomSP nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_HangPhuKien]
SET
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[MaHS] = @MaHS,
	[ThongTinCu] = @ThongTinCu,
	[SoLuong] = @SoLuong,
	[DonGia] = @DonGia,
	[DVT_ID] = @DVT_ID,
	[NuocXX_ID] = @NuocXX_ID,
	[TriGia] = @TriGia,
	[NguyenTe_ID] = @NguyenTe_ID,
	[STTHang] = @STTHang,
	[TinhTrang] = @TinhTrang,
	[Master_ID] = @Master_ID,
	[NhomSP] = @NhomSP
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_InsertUpdate]
	@ID bigint,
	@MaHang varchar(50),
	@TenHang nvarchar(255),
	@MaHS varchar(12),
	@ThongTinCu nvarchar(255),
	@SoLuong numeric(18, 5),
	@DonGia float,
	@DVT_ID char(3),
	@NuocXX_ID char(3),
	@TriGia float,
	@NguyenTe_ID char(3),
	@STTHang int,
	@TinhTrang nvarchar(255),
	@Master_ID bigint,
	@NhomSP nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_HangPhuKien] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_HangPhuKien] 
		SET
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[MaHS] = @MaHS,
			[ThongTinCu] = @ThongTinCu,
			[SoLuong] = @SoLuong,
			[DonGia] = @DonGia,
			[DVT_ID] = @DVT_ID,
			[NuocXX_ID] = @NuocXX_ID,
			[TriGia] = @TriGia,
			[NguyenTe_ID] = @NguyenTe_ID,
			[STTHang] = @STTHang,
			[TinhTrang] = @TinhTrang,
			[Master_ID] = @Master_ID,
			[NhomSP] = @NhomSP
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_HangPhuKien]
		(
			[MaHang],
			[TenHang],
			[MaHS],
			[ThongTinCu],
			[SoLuong],
			[DonGia],
			[DVT_ID],
			[NuocXX_ID],
			[TriGia],
			[NguyenTe_ID],
			[STTHang],
			[TinhTrang],
			[Master_ID],
			[NhomSP]
		)
		VALUES 
		(
			@MaHang,
			@TenHang,
			@MaHS,
			@ThongTinCu,
			@SoLuong,
			@DonGia,
			@DVT_ID,
			@NuocXX_ID,
			@TriGia,
			@NguyenTe_ID,
			@STTHang,
			@TinhTrang,
			@Master_ID,
			@NhomSP
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_HangPhuKien]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_DeleteBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_HangPhuKien]
WHERE
	[Master_ID] = @Master_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_HangPhuKien] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP]
FROM
	[dbo].[t_KDT_GC_HangPhuKien]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_SelectBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP]
FROM
	[dbo].[t_KDT_GC_HangPhuKien]
WHERE
	[Master_ID] = @Master_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP]
FROM [dbo].[t_KDT_GC_HangPhuKien] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_HangPhuKien_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_HangPhuKien_SelectAll]















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHang],
	[TenHang],
	[MaHS],
	[ThongTinCu],
	[SoLuong],
	[DonGia],
	[DVT_ID],
	[NuocXX_ID],
	[TriGia],
	[NguyenTe_ID],
	[STTHang],
	[TinhTrang],
	[Master_ID],
	[NhomSP]
FROM
	[dbo].[t_KDT_GC_HangPhuKien]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Insert]
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_DinhMuc]
(
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
)
VALUES 
(
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DVT_ID,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@GhiChu,
	@STTHang,
	@Master_ID,
	@NPL_TuCungUng,
	@TenSanPham,
	@TenNPL,
	@MaDDSX
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Update]
	@ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_GC_DinhMuc]
SET
	[MaSanPham] = @MaSanPham,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DVT_ID] = @DVT_ID,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[GhiChu] = @GhiChu,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[TenSanPham] = @TenSanPham,
	[TenNPL] = @TenNPL,
	[MaDDSX] = @MaDDSX
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_InsertUpdate]
	@ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@GhiChu nvarchar(250),
	@STTHang int,
	@Master_ID bigint,
	@NPL_TuCungUng numeric(18, 8),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@MaDDSX nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DinhMuc] 
		SET
			[MaSanPham] = @MaSanPham,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DVT_ID] = @DVT_ID,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[GhiChu] = @GhiChu,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[TenSanPham] = @TenSanPham,
			[TenNPL] = @TenNPL,
			[MaDDSX] = @MaDDSX
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_DinhMuc]
		(
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DVT_ID],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[GhiChu],
			[STTHang],
			[Master_ID],
			[NPL_TuCungUng],
			[TenSanPham],
			[TenNPL],
			[MaDDSX]
		)
		VALUES 
		(
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DVT_ID,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@GhiChu,
			@STTHang,
			@Master_ID,
			@NPL_TuCungUng,
			@TenSanPham,
			@TenNPL,
			@MaDDSX
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_DinhMuc]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_DinhMuc]
WHERE
	[Master_ID] = @Master_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
FROM
	[dbo].[t_KDT_GC_DinhMuc]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
FROM
	[dbo].[t_KDT_GC_DinhMuc]
WHERE
	[Master_ID] = @Master_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
FROM [dbo].[t_KDT_GC_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMuc_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMuc_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[MaDDSX]
FROM
	[dbo].[t_KDT_GC_DinhMuc]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Insert]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255)
AS
INSERT INTO [dbo].[t_GC_DinhMuc]
(
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL]
)
VALUES
(
	@HopDong_ID,
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@STTHang,
	@NPL_TuCungUng,
	@TenSanPham,
	@TenNPL
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Update]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255)
AS

UPDATE
	[dbo].[t_GC_DinhMuc]
SET
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[STTHang] = @STTHang,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[TenSanPham] = @TenSanPham,
	[TenNPL] = @TenNPL
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_InsertUpdate]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255)
AS
IF EXISTS(SELECT [HopDong_ID], [MaSanPham], [MaNguyenPhuLieu] FROM [dbo].[t_GC_DinhMuc] WHERE [HopDong_ID] = @HopDong_ID AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu)
	BEGIN
		UPDATE
			[dbo].[t_GC_DinhMuc] 
		SET
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[STTHang] = @STTHang,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[TenSanPham] = @TenSanPham,
			[TenNPL] = @TenNPL
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [MaSanPham] = @MaSanPham
			AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_DinhMuc]
	(
			[HopDong_ID],
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[STTHang],
			[NPL_TuCungUng],
			[TenSanPham],
			[TenNPL]
	)
	VALUES
	(
			@HopDong_ID,
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@STTHang,
			@NPL_TuCungUng,
			@TenSanPham,
			@TenNPL
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Delete]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Load]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL]
FROM
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL]
FROM
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL]
FROM [dbo].[t_GC_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL]
FROM
	[dbo].[t_GC_DinhMuc]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '28.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('28.5',GETDATE(), N'CẬP NHẬT THAY ĐỔI KIỂU DỮ LIỆU MỘT SỐ TRƯỜNG CỦA NGUYÊN PHỤ LIỆU - THIẾT BỊ- HÀNG MẪU - SẢN PHẨM - NHÓM SẢN PHẨM -ĐỊNH MỨC -HÀNG PHỤ KIỆN')
END