using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.KDT;
using Company.BLL;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class CapSoToKhaiGiay_Form : BaseForm
    {
        ToKhaiMauDich tkmd;
        public CapSoToKhaiGiay_Form(ToKhaiMauDich tk)
        {
            InitializeComponent();

            this.tkmd = tk;
            this.Text = this.Text + " " + this.tkmd.ID.ToString();
            this.txtSoTKGiay.Text = tkmd.ChiTietDonViDoiTac;
            this.lblCaption.Text = this.lblCaption.Text + " " + this.tkmd.ID.ToString();
            this.lblMaToKhai.Text = this.tkmd.ID.ToString();
            this.txtMaLoaiHinh.Text = this.tkmd.MaLoaiHinh;
            this.cbPL.SelectedValue = this.tkmd.PhanLuong;
            this.txtHuongDan.Text = this.tkmd.HUONGDAN;

            //Hungtq, cap nhat 30/07/2010. Neu to khai chuyen trang thai da co So to khai, ngay dang ky thi tu dong hien thi So to khai va ngay dang ky do tren form.
            if (tkmd.SoToKhai > 0)
            {
                txtSoToKhai.Text = tkmd.SoToKhai.ToString();
                ccNgayDayKy.Text = tkmd.NgayDangKy.ToString();
            }
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.validData())
                {
                    if (tkmd.MaLoaiHinh.Contains("V") || tkmd.MaLoaiHinh.Contains("N"))
                    {
                        decimal SoTkVNACC = System.Convert.ToDecimal(txtSoToKhai.Text.ToString().Substring(0, 11));
                        CapSoToKhai capsos = CapSoToKhai.GetFromTKMD_TKGiay(txtSoTKGiay.Text);
                        if (capsos == null)
                        {
                            capsos = new CapSoToKhai();
                            capsos.SoTK = CapSoToKhai.SoTKMax() + 1;
                        }
                        capsos.MaLoaiHinh = tkmd.MaLoaiHinh.Substring(2, 3);
                        capsos.SoNhanhTK = 0;
                        capsos.SoTKDauTien = 0;
                        capsos.SoTKTNTX = 0;
                        capsos.SoTKVNACCS = Convert.ToDecimal(txtSoToKhai.Text);
                        capsos.NamDangKy = ccNgayDayKy.Value.Year;
                        capsos.SoTKVNACCSFull = capsos.SoTK;
                        capsos.SoTKGiay = txtSoTKGiay.Text;
                        capsos.InsertUpdate();
                        tkmd.TrangThaiVNACCS = capsos;//Company.KDT.SHARE.VNACCS.CapSoToKhai.CapSoToKhaiV5(SoTkVNACC, tkmdVNACC);
                        tkmd.SoToKhai = tkmd.TrangThaiVNACCS.SoTK;
                        tkmd.LoaiVanDon = txtSoToKhai.Text;
                        tkmd.MaLoaiHinh = txtMaLoaiHinh.Text;
                        tkmd.ChiTietDonViDoiTac = txtSoTKGiay.Text;
                        tkmd.NgayDangKy = ccNgayDayKy.Value;
                        tkmd.NgayTiepNhan = ccNgayDayKy.Value;
                        tkmd.PhanLuong = cbPL.SelectedValue.ToString();
                    }
                    else
                        this.getData();

                    this.tkmd.LoadHMDCollection();

                    Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkDaDangKy = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDaDangKy.MaHaiQuan = tkmd.MaHaiQuan;
                    tkDaDangKy.NamDangKy = (short)tkmd.NgayDangKy.Year;
                    tkDaDangKy.MaLoaiHinh = tkmd.MaLoaiHinh;
                    tkDaDangKy.SoToKhai = tkmd.SoToKhai;
                    tkDaDangKy.PhanLuong = tkmd.PhanLuong;
                    //Hungtq, Bo sung ngay dang ky to khai. 30/07/2010.
                    tkDaDangKy.NgayDangKy = tkmd.NgayDangKy;

                    if (!tkDaDangKy.Load())
                    {
                        this.tkmd.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        this.tkmd.TransgferDataToSXXK();

                        #region  Ghi vào Kết quả xử lý

                        KetQuaXuLy kqxl = new KetQuaXuLy();

                        kqxl.ItemID = this.tkmd.ID;
                        //Hungtq update
                        if (this.tkmd.GUIDSTR.Length > 0)
                            kqxl.ReferenceID = new Guid(this.tkmd.GUIDSTR);
                        kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiChuyenTT;

                        string tenluong = "Xanh";
                        if (this.tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                            tenluong = "Vàng";
                        else if (this.tkmd.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                            tenluong = "Đỏ";

                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}", this.tkmd.SoToKhai, this.tkmd.NgayDangKy.ToShortDateString(), this.tkmd.MaLoaiHinh.Trim(), this.tkmd.MaHaiQuan.Trim(), tenluong);
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();
                        Company.KDT.SHARE.Components.MessageTypes msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap;
                        if (tkmd.MaLoaiHinh.Substring(0, 1) == "X")
                            msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;
                        #endregion
                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty, tkmd.ID, tkmd.GUIDSTR,
                            msgType,
                            Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                            Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay,
                            kqxl.NoiDung);
                        if (MLMessages("Chuyển trạng thái thành công", "MSG_THK105", "", false) == "Cancel")
                        {
                            this.Close();
                        }

                    }
                    else
                    {
                        // ShowMessage("Thông tin tờ khai này trùng trong danh sách đăng ký! \nVui lòng kiểm tra lại số tờ khai và ngày đăng ký", false);
                        MLMessages("Thông tin tờ khai này trùng trong danh sách đăng ký! \nVui lòng kiểm tra lại số tờ khai và ngày đăng ký", "MSG_THK106", "", false);
                    }
                }
                else
                {
                    // ShowMessage("Dữ liệu nhập vào chưa hợp lệ! \nVui lòng nhập lại.", false);
                    MLMessages("Dữ liệu nhập vào chưa hợp lệ! \nVui lòng nhập lại.", "MSG_THK107", "", false);
                }
            }
            catch (Exception ex)
            {
                MLMessages(ex.ToString(), "", "", false);
            }
        }

        private bool validData()
        {
            bool value = true;
            if (txtMaLoaiHinh.Text.Contains("V") || txtMaLoaiHinh.Text.Contains("N"))
            {
                if (txtSoToKhai.Text.Length != 18) value = false;
            }
            else
                if (txtSoToKhai.Text == "" || int.Parse(txtSoToKhai.Text) < 0) value = false;

            return value;
        }

        private void getData()
        {
            this.tkmd.SoToKhai = int.Parse(txtSoToKhai.Text);
            this.tkmd.NgayDangKy = ccNgayDayKy.Value;
            this.tkmd.PhanLuong = cbPL.SelectedValue.ToString();
            this.tkmd.HUONGDAN = txtHuongDan.Text;
            if (this.tkmd.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                this.tkmd.NgayTiepNhan = this.tkmd.NgayDangKy;
            }
        }

        private void btnHuybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChuyenTrangThaiTK_Load(object sender, EventArgs e)
        {
            //getData();
            //this.tkmd.LoadHMDCollection();
        }
    }
}