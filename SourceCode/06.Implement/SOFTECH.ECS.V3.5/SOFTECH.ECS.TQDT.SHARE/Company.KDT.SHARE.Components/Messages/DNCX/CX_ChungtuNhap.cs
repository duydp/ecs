﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class CX_ChungtuNhap:DeclarationBase
    {
         /// <summary>
       /// 
       /// </summary>
       [XmlElement("GoodsShipment")]
       public GoodsShipment GoodsShipment { get; set; }
    }
}
