﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.Interface.Report.GC;
using Company.Interface.Report;

namespace Company.Interface.GC
{
    public partial class BK10Form : BaseForm
    {

        public BK10Form()
        {
            InitializeComponent();            
        }
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        //private BKDinhMucCollection BKDMColl = new BKDinhMucCollection();
        private BKDinhMuc dinhmuc = new BKDinhMuc();
        public DataSet dsBK = new DataSet();
        private void LoadData()
        {

            //BKDMColl = new BKDinhMuc().SelectCollectionDynamic("1=1","MaNPL");
            //dgList.DataSource = BKDMColl;

            dsBK = new BKDinhMuc().SelectDynamic("OldHD_ID =" + this.HD.ID, "STT");
            dgList.DataSource = dsBK.Tables[0];

            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
 
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void lblViewRP_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            ReportViewBC10Form BK10 = new ReportViewBC10Form();
            BK10.HD = this.HD;
            BK10.dsBK = this.dsBK;
            BK10.Show();
            this.Cursor = Cursors.Default ;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (dinhmuc.UpdateDataSetFromGrid(dsBK))
               {
                   showMsg("MSG_2702001");
                   //Message("MSG_SAV02", "", false);
                   //ShowMessage("Cập nhật thành công", false);
               }
            else
               {
                   showMsg("MSG_2702002");
                   //Message("MSG_SAV01", "", false);
                   //ShowMessage("Cập nhật không thành công", false);
               }

             LoadData();

        }

        private void KB06Form_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["DinhMucSD"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            LoadData();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        

      
        
       
     

         
 
    }
}