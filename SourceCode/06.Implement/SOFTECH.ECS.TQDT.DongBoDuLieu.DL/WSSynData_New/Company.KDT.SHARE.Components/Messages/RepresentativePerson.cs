﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Người đại diện doanh nghiệp
    /// </summary>
    public class RepresentativePerson
    {
        ///<summary>
        ///Chức vụ a..35
        ///</summary>
        [XmlElement("contactFunction")]
        public string ContactFunction { set; get; }
        
        ///<summary>
        ///Tên người gửi a..35
        ///</summary>
        [XmlElement("name")]
        public string Name { set; get; }

    }
}
