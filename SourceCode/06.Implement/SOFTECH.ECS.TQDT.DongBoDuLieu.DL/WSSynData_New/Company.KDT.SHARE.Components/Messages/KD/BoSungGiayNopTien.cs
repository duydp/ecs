﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.KD
{
    [XmlRoot("Declaration")]
    class BoSungGiayNopTien : DeclarationBase
    {
        // Thông tin tham chiếu đến tờ khai
        [XmlElement("DeclarationDocument")]
        public DeclarationDocument DeclarationDocument { get; set; }

        //Thông tin giấy nộp tiền
        [XmlElement("Receipt")]
        public string Receipt { get; set; }

    }
}
