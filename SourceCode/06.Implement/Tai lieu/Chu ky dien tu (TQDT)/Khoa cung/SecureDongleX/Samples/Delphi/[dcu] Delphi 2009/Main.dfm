object frmMain: TfrmMain
  Left = 554
  Top = 454
  BorderStyle = bsDialog
  Caption = 'SecureDongle X Delphi .dcu Sample'
  ClientHeight = 257
  ClientWidth = 577
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 468
    Height = 19
    Caption = 'RSA Encryption and Decryption is not available using .dcu'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lstOutput: TListBox
    Left = 13
    Top = 50
    Width = 444
    Height = 191
    ExtendedSelect = False
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
  end
  object btnWrite: TButton
    Left = 472
    Top = 48
    Width = 91
    Height = 33
    Caption = 'Write'
    TabOrder = 1
    OnClick = btnWriteClick
  end
  object btnRead: TButton
    Left = 472
    Top = 87
    Width = 91
    Height = 33
    Caption = 'Read'
    TabOrder = 2
    OnClick = btnReadClick
  end
  object btnTransform: TButton
    Left = 472
    Top = 128
    Width = 89
    Height = 33
    Caption = 'Transform'
    TabOrder = 3
    OnClick = btnTransformClick
  end
  object btnWriteMap: TButton
    Left = 472
    Top = 168
    Width = 89
    Height = 33
    Caption = 'Write with Map'
    TabOrder = 4
    OnClick = btnWriteMapClick
  end
  object btnReadMap: TButton
    Left = 472
    Top = 208
    Width = 89
    Height = 33
    Caption = 'Read with Map'
    TabOrder = 5
    OnClick = btnReadMapClick
  end
end
