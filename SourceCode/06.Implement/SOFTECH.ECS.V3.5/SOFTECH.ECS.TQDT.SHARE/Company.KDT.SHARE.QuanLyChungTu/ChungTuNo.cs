﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class ChungTuNo
    {
        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_ChungTuNo_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
            db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year <= 1753 ? DBNull.Value : (object)NGAY_CT);
            db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
            db.AddInParameter(dbCommand, "@NOI_CAP", SqlDbType.NVarChar, NOI_CAP);
            db.AddInParameter(dbCommand, "@TO_CHUC_CAP", SqlDbType.NVarChar, TO_CHUC_CAP);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
            db.AddInParameter(dbCommand, "@IsNoChungTu", SqlDbType.Bit, IsNoChungTu);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
            db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year <= 1753 ? DBNull.Value : (object)NGAYTN);
            db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
            db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object)NgayHetHan);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public static bool KiemTraChungTuCoBoSung(List<ChungTuNo> collections)
        {
            foreach (ChungTuNo item in collections)
            {
                if (item.LoaiKB == "1")
                {
                    return true;
                }
            }

            return false;
        }
    }
}
