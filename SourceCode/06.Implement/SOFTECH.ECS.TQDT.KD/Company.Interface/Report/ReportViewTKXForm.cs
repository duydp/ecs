﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Company.Interface.Report
{
    
    public partial class ReportViewTKXForm : BaseForm
    {
        public ToKhaiXuat ToKhaiChinhReport = new ToKhaiXuat();
        public PhuLucToKhaiXuat PhuLucReport = new PhuLucToKhaiXuat();
        public PhuLucHangXuatCoThue PhuLucHangXuatReport = new PhuLucHangXuatCoThue();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public XRTableCell Cell =new XRTableCell();
        private System.Drawing.Printing.Margins MarginTKX = new System.Drawing.Printing.Margins();
        private System.Drawing.Printing.Margins MarginPhuLucTKX = new System.Drawing.Printing.Margins();


        public ReportViewTKXForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            this.MarginTKX = GlobalSettings.MarginTKX;
            this.MarginPhuLucTKX = GlobalSettings.MarginPhuLucTKX;
            if (this.TKMD.HMDCollection.Count <= 9) btnConfig.Visible = false;
            else
            {
                int count = (this.TKMD.HMDCollection.Count - 1) / 15 + 1;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            if (IsHaveTax()) cboToKhai.Items.Add("Phụ lục hàng xuất có thuế" , cboToKhai.Items.Count);

            cboToKhai.SelectedIndex = 0;
            this.ToKhaiChinhReport.TKMD = this.TKMD;
            this.ToKhaiChinhReport.Margins = this.MarginTKX;
            this.ToKhaiChinhReport.report = this;
            this.ToKhaiChinhReport.BindReport();            
            printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
            this.ToKhaiChinhReport.CreateDocument();

        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.Margins = this.MarginTKX;
                this.ToKhaiChinhReport.TKMD = this.TKMD;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                if (!IsHaveTax())
                {
                    HangMauDichCollection HMDReportCollection = new HangMauDichCollection();
                    int begin = (cboToKhai.SelectedIndex - 1) * 15;
                    int end = cboToKhai.SelectedIndex * 15;
                    if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                    for (int i = begin; i < end; i++)
                        HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                    this.PhuLucReport = new PhuLucToKhaiXuat();
                    this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                    if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                        this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                    this.PhuLucReport.HMDCollection = HMDReportCollection;
                    this.PhuLucReport.TKMD = this.TKMD;
                    this.PhuLucReport.BindReport();
                    printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                    this.PhuLucReport.CreateDocument();
                }
                else
                {
                    if ((cboToKhai.SelectedIndex + 1) == cboToKhai.Items.Count)
                    {
                        this.PhuLucHangXuatReport.HMDCollection = this.TKMD.HMDCollection;
                        this.PhuLucHangXuatReport.Margins = this.MarginPhuLucTKX;
                        this.PhuLucHangXuatReport.TKMD = this.TKMD;
                        this.PhuLucHangXuatReport.BindReport();
                        printControl1.PrintingSystem = PhuLucHangXuatReport.PrintingSystem;
                        this.PhuLucHangXuatReport.CreateDocument();
                    }
                    else
                    {
                        HangMauDichCollection HMDReportCollection = new HangMauDichCollection();
                        int begin = (cboToKhai.SelectedIndex - 1) * 15;
                        int end = cboToKhai.SelectedIndex * 15;
                        if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                        for (int i = begin; i < end; i++)
                            HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                        this.PhuLucReport = new PhuLucToKhaiXuat();
                        this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                        if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                            this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                        this.PhuLucReport.HMDCollection = HMDReportCollection;
                        this.PhuLucReport.TKMD = this.TKMD;
                        this.PhuLucReport.BindReport();
                        printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                        this.PhuLucReport.CreateDocument();
                    }
                }

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.MarginTKX = printControl1.PrintingSystem.PageMargins;
                this.ToKhaiChinhReport.Margins = this.MarginTKX;
                Properties.Settings.Default.MarginTKX = this.MarginTKX;
                Properties.Settings.Default.Save();
                GlobalSettings.RefreshKey();
                this.ToKhaiChinhReport.setVisibleImage(false);
                this.ToKhaiChinhReport.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                if (IsHaveTax())
                {
                    if ((cboToKhai.SelectedIndex + 1) == cboToKhai.Items.Count)
                    {
                        this.MarginPhuLucTKX = printControl1.PrintingSystem.PageMargins;
                        this.PhuLucReport.Margins = this.MarginPhuLucTKX;
                        Properties.Settings.Default.MarginPhuLucTKX = this.MarginPhuLucTKX;
                        Properties.Settings.Default.Save();
                        GlobalSettings.RefreshKey();
                        this.PhuLucHangXuatReport.setVisibleImage(false);
                        this.PhuLucHangXuatReport.CreateDocument();
                        printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                        this.PhuLucHangXuatReport.setVisibleImage(true);
                        this.PhuLucHangXuatReport.CreateDocument();
                    }
                    else
                        printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                else
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });

                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            
        }

        private void btnQuickPrint_Click(object sender, EventArgs e)
        {
            
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            CauHinhNhomHangForm f = new CauHinhNhomHangForm();
            f.HMDCollection = this.TKMD.HMDCollection;
            f.ShowDialog();
            this.TKMD.HMDCollection = f.HMDCollection;
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.Margins = this.MarginTKX;
                this.ToKhaiChinhReport.TKMD = this.TKMD;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                HangMauDichCollection HMDReportCollection = new HangMauDichCollection();
                int begin = (cboToKhai.SelectedIndex - 1) * 15;
                int end = cboToKhai.SelectedIndex * 15;
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                this.PhuLucReport = new PhuLucToKhaiXuat();
                this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.BindReport();
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }
        public void setVisibleButtonConfig(bool k)
        {
            btnConfig.Visible = k;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setVisibleImage(false);
                this.ToKhaiChinhReport.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                if (!IsHaveTax())
                {
                    if ((cboToKhai.SelectedIndex + 1) == cboToKhai.Items.Count)
                    {

                        this.PhuLucHangXuatReport.setVisibleImage(false);
                        this.PhuLucHangXuatReport.CreateDocument();
                        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                        this.PhuLucHangXuatReport.setVisibleImage(true);
                        this.PhuLucHangXuatReport.CreateDocument();
                    }
                    else
                        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                else
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });

                }
            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.ToKhaiChinhReport.CreateDocument();
            }
            
        }
    }
}