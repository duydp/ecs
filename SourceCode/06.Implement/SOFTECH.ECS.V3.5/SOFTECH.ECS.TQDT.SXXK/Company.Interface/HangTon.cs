﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL.SXXK.ToKhai;
using Company.BLL;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;
using Company.QuanTri;
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;
using Company.KDT.SHARE.Components;
namespace Company.Interface
{
    public partial class HangTon : BaseForm
    {
        private DataSet ds = new DataSet();
        private Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection tkmdCollection = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTon clsnhapton = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
        private DataSet dsTKKhongDuyet = new DataSet();
        private DataRow[] results = null;

        public HangTon()
        {
            InitializeComponent();
        }

        private void HangTon_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgList.ColumnAutoResize = false;

                cbTrangThai.SelectedIndex = 0;
                dgList.Tables[0].Columns["MaNPL"].Width = 80;
                dgList.Tables[0].Columns["TenNPL"].Width = 150;
                dgList.Tables[0].Columns["Luong"].Width = 80;
                dgList.Tables[0].Columns["Ton"].Width = 70;
                dgList.Tables[0].Columns["ThueXNK"].Width = 80;
                dgList.Tables[0].Columns["Ton"].Width = 70;
                dgList.Tables[0].Columns["SolanThanhLy"].Width = 60;
                dgList.Tables[0].Columns["LanThanhLy"].Width = 80;
                dgList.Tables[0].Columns["TrangThaiThanhKhoan"].Width = 90;
                dgList.Tables[0].Columns["saisoluong"].Width = 80;
                dgList.Tables[0].Columns["SoLuongDangKy"].Width = 80;
                dgList.Tables[0].Columns["TienTrinhChayThanhLy"].Width = 90;

                dgList.Tables[0].Columns["Luong"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Ton"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Luong"].TotalFormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Ton"].TotalFormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = new Company.BLL.SXXK.NguyenPhuLieu().SelectDynamic("MaDoanhNghiep = '" + MaDoanhNghiep + "'", "").Tables[0];
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["Ma"].ToString());
                txtMaNPL.AutoCompleteCustomSource = col;
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
                {
                    uiButton2.Visible = false;
                }
                if (!((SiteIdentity)MainForm.EcsQuanTri.Identity).user.isAdmin)
                {
                    uiButton4.Visible = false;
                }
                txtNamTiepNhan.Text = DateTime.Today.Year.ToString();

                //Updated by Hungtq, 08/08/2012.
                //Lay danh dach to khai co trang thai khong phai da duyet
              //  string whereCondition = string.Format("MaDoanhNghiep = '{0}' AND MaHaiQuan = '{1}'", GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
               // whereCondition += string.Format("AND NamDK = {0} AND MaLoaiHinh LIKE 'N%' AND TrangThaiXuLy != 1 AND SoToKhai != 0", txtNamTiepNhan.Text);

               // dsTKKhongDuyet = new Company.BLL.KDT.ToKhaiMauDich().SelectDynamic(whereCondition, "");

                searchNew();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            ToKhaiMauDich tk = new ToKhaiMauDich();
            if (dgList.GetRow().RowType == RowType.Record)
            {
                GridEXRow dr = dgList.GetRow();
                Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)dr.DataRow;
                tk.MaHaiQuan = npl.MaHaiQuan;
                tk.SoToKhai = (int)npl.SoToKhai;
                tk.MaLoaiHinh = npl.MaLoaiHinh;
                tk.NamDangKy = npl.NamDangKy;

            }
            else if (dgList.GetRow().RowType == RowType.GroupHeader)
            {
                object[] obj = (object[])dgList.GetRow().GroupValue;
                string a = obj[0].ToString();
                tk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                tk.SoToKhai = (int)obj[0];
                tk.MaLoaiHinh = obj[1].ToString();
                tk.NamDangKy = (short)Convert.ToDateTime(obj[2].ToString()).Year;
            }

            tk.Load();

            ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
            f.TKMD = tk;
            f.NhomLoaiHinh = tk.MaLoaiHinh.Substring(0, 3);
            f.MdiParent = this.ParentForm;
            f.Name = "Tờ khai nhập khẩu số " + tk.SoToKhai + "/" + tk.NamDangKy;
            f.Show();
        }

        private void search()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoToKhai = " + txtSoTiepNhan.Value;
                }

                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND NamDangKy = " + txtNamTiepNhan.Value;
                }
                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }
                string temp = "";
                if (cbTrangThai.SelectedIndex > 0)
                {
                    temp += " AND ThanhLy = '" + cbTrangThai.SelectedValue.ToString() + "'";
                }

                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                    {
                        temp += " AND TenChuHang LIKE '%" + txtTenChuHang.Text.Trim() + "%'";
                    }
                }
                else
                {
                    temp += " AND TenChuHang LIKE ''";

                }


                where += " AND Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan IN " +
                "(SELECT Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan FROM dbo.t_SXXK_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'N%'" + temp + ")";

                // Thực hiện tìm kiếm.            
                tkmdCollection = clsnhapton.SelectCollectionDynamic(where, "t_SXXK_ThanhLy_NPLNhapTon.SoToKhai");

                dgList.DataSource = tkmdCollection;
                dgList.Refetch();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //Update by HUNGTQ, 17/08/2011.
        private void searchNew()
        {
            try
            {
                string whereCondition = string.Format("MaDoanhNghiep = '{0}' AND MaHaiQuan = '{1}'", GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                whereCondition += string.Format("AND NamDK like '%{0}%' AND MaLoaiHinh LIKE 'N%' AND TrangThaiXuLy != 1 AND SoToKhai != 0", txtNamTiepNhan.Text);

                dsTKKhongDuyet = new Company.BLL.KDT.ToKhaiMauDich().SelectDynamic(whereCondition, "");

                Cursor = Cursors.WaitCursor;

                // Xây dựng điều kiện tìm kiếm.
                string where = "t.MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND t.MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoToKhaiVNACCS  like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND t.NamDangKy = " + txtNamTiepNhan.Value;
                }
                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }

                if (cbTrangThai.SelectedIndex > 0)
                {
                    where += " AND t.ThanhLy = '" + cbTrangThai.SelectedValue.ToString() + "'";
                }

                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                    {
                        where += " AND t.TenChuHang LIKE N'%" + txtTenChuHang.Text.Trim() + "%'";
                    }
                }
                else
                {
                    where += " AND t.TenChuHang LIKE N'%%'";

                }


                //where += " AND Cast(t.SoToKhai as varchar(10))+ t.MaLoaiHinh +  Cast(t.NamDangKy as varchar(4))+ t.MaHaiQuan IN " +
                //"(SELECT Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan FROM dbo.t_SXXK_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'N%'" + temp + ")";

                if (chkHienThiLech.Checked)
                {
                    //where += "AND ( SELECT COUNT(*)  FROM[dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu  AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan WHERE nt.SoToKhai = t.SoToKhai AND nt.MaLoaiHinh = t.MaLoaiHinh AND nt.NamDangKy = t.NamDangKy AND nt.MaNPL = t.MaNPL AND nt.MaHaiQuan = t.MaHaiQuan AND nt.MaDoanhNghiep = t.MaDoanhNghiep AND nt.Luong <> h.SoLuong ) != 0 ";
                    where += " AND t.SaiSoLuong = 1";

                    where += " OR (t.SaiSoLuong = 0 AND t.SoLanThanhLy = 0 AND t.Luong <> t.Ton AND t.NamDangKy = " + txtNamTiepNhan.Value;

                    if (txtSoTiepNhan.TextLength > 0)
                    {
                        where += " AND SoToKhaiVNACCS = " + txtSoTiepNhan.Value;
                    }

                    if (txtMaNPL.Text.Length > 0)
                    {
                        if (chkTimChinhXac.Checked)
                        {
                            where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                        }
                        else
                            where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                    }

                    where += ")";

                    //Bo sung 22/09/2011 by HUNGTQ
                    where += " OR (t.TienTrinhChayThanhLy LIKE '%Sai%' AND t.SoLanThanhLy != 0 AND t.NamDangKy = " + txtNamTiepNhan.Value;

                    if (txtSoTiepNhan.TextLength > 0)
                    {
                        where += " AND t.SoToKhai = " + txtSoTiepNhan.Value;
                    }

                    if (txtMaNPL.Text.Length > 0)
                    {
                        if (chkTimChinhXac.Checked)
                        {
                            where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                        }
                        else
                            where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                    }

                    where += ")";

                    where += " OR (t.TenNPL IS NULL AND t.NamDangKy = " + txtNamTiepNhan.Value + ")";
                }

                if (chkHienThiTKKhongDuyet.Checked)
                {
                    //Bo sung 09/08/2012 by HUNGTQ. Chi hien thi thong tin NPL cua TK khong duoc duyet.
                    where += string.Format(" AND t.SoToKhai IN (SELECT SoToKhai FROM dbo.t_KDT_ToKhaiMauDich WHERE TrangThaiXuLy != 1 AND SoToKhai != 0 AND MaDoanhNghiep = '{0}' AND MaHaiQuan = '{1}' AND NamDK = {2} AND MaLoaiHinh LIKE 'N%')", GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, txtNamTiepNhan.Text);
                }

                // Thực hiện tìm kiếm.            
                tkmdCollection = clsnhapton.SelectCollection_ByYear(where, "t.SoToKhai");
                //ds = clsnhapton.SelectAll_ByYear(where, "t.SoToKhai");

                dgList.DataSource = tkmdCollection;
                //dgList.DataSource = ds.Tables[0];
                dgList.Refetch();
                //GridEXGroup group = new GridEXGroup();
                //GridEXCustomGroup customGr = new GridEXCustomGroup();
                //customGr.CustomGroupType = CustomGroupType.CompositeColumns;
                //customGr.CompositeColumns = new Janus.Windows.GridEX.GridEXColumn[] { dgList.RootTable.Columns["SoToKhai"], dgList.RootTable.Columns["MaLoaiHinh"] };
                //group.GroupInterval = GroupInterval.Alphabetical;
                //dgList.RootTable.Columns["SoToKhai"].DefaultGroupInterval = GroupInterval.Text;
                //group.CustomGroup = customGr;
                

                //dgList.RootTable.Groups.Add(group);

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.searchNew();
        }

        private void Thongke()
        {
            //Company.BLL.SXXK.ThanhKhoan.NPLNhapTon clsNhapton = new NPLNhapTon();
            //NPLNhapTonCollection NhaptonCollection = new NPLNhapTonCollection();
            //foreach (NPLNhapTon nplnhapton in NhaptonCollection)
            //{

            //}
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSoTiepNhan.Text.Trim() == "")
                {
                    ShowMessage("Bạn hãy nhập số tờ khai trước khi lưu.", false);
                    return;
                }
                foreach (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl in tkmdCollection)
                {
                    if (npl.Ton < 0)
                    {
                        ShowMessage("Dữ liệu không hợp lệ. Hãy kiểm tra lại", false);
                        return;
                    }
                    if (npl.ThueTon < 0)
                    {
                        ShowMessage("Dữ liệu không hợp lệ. Hãy kiểm tra lại", false);
                        return;
                    }
                    Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npltemp = npl.Copy();
                    if ((npl.Ton + (decimal)npl.ThueXNKTon) != (npltemp.Ton + (decimal)npltemp.ThueXNKTon) && npl.Luong != npl.Ton)
                        npl.PhuThu = 1;
                    else
                        npl.PhuThu = 0;

                }
                clsnhapton.InsertUpdate(tkmdCollection, chkTuDongTinhThue.Checked);

                //this.search();
                searchNew();

                //ShowMessage("Cập nhật thông tin thành công", false);
                MLMessages("Cập nhật thông tin thành công", "MSG_THK64", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Dữ liệu không hợp lệ. Không lưu được.Hãy kiểm tra lại." + ex.Message, false);
            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            gridEXPrintDocument1.Print();
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            try
            {
                new Company.BLL.KDT.SXXK.HoSoThanhLyDangKy().FixOldData(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                //ShowMessage("Điều chỉnh thành công.", false);
                MLMessages("Điều chỉnh thành công.", "MSG_THK99", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
        }

        private void cbTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.search();
            searchNew();
        }

        private void mnuGrid_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "toolStripThanhkhoan":
                    this.doThanhKhoan();
                    break;
            }
        }

        private void doThanhKhoan()
        {

            if (dgList.GetRow().RowType == RowType.Record)
            {
                GridEXRow dr = dgList.GetRow();
                Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)dr.DataRow;
                TheoDoiThanhKhoanNPLToKhaiForm f = new TheoDoiThanhKhoanNPLToKhaiForm();
                f.NPL = npl;
                f.ShowDialog(this);

            }
            else if (dgList.GetRow().RowType == RowType.GroupHeader)
            {
                ToKhaiMauDich tk = new ToKhaiMauDich();
                object[] obj = (object[])dgList.GetRow().GroupValue;
                string a = obj[0].ToString();
                tk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                tk.SoToKhai = (int)obj[0];
                tk.MaLoaiHinh = obj[1].ToString();
                tk.NamDangKy = (short)Convert.ToDateTime(obj[2]).Year;
                tk.Load();
                TheoDoiThanhKhoanToKhaiForm f = new TheoDoiThanhKhoanToKhaiForm();
                f.TKMD = tk;
                f.ShowDialog(this);
            }




        }

        private DateTime getMaxDateBKTKX(HoSoThanhLyDangKy HSTL)
        {
            int i = HSTL.getBKToKhaiXuat();
            HSTL.BKCollection[i].LoadChiTietBangKe();
            BKToKhaiXuatCollection bkTKXCollection = HSTL.BKCollection[i].bkTKXColletion;
            DateTime dt = bkTKXCollection[0].NgayDangKy;
            for (int j = 1; j < bkTKXCollection.Count; j++)
            {
                if (dt < bkTKXCollection[j].NgayDangKy) dt = bkTKXCollection[j].NgayDangKy;
            }
            return dt;
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TenNPL"].Text == "")
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["SaiSoLuong"].Value.ToString() != "0")
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["SaiSoLuong"].Value.ToString() == "0"
                    && e.Row.Cells["SoLanThanhLy"].Value.ToString() == "0"
                    && (Convert.ToDecimal(e.Row.Cells["Luong"].Value) != Convert.ToDecimal(e.Row.Cells["Ton"].Value)))
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["TienTrinhChayThanhLy"].Value.ToString().Contains("Sai"))
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                    e.Row.Cells["TienTrinhChayThanhLy"].ToolTipText = e.Row.Cells["TienTrinhChayThanhLy"].Text;
                }

                //Updated by Hungtq, 08/08/2012.
                //Thiet lap mau to khai co trang thai khong phai da duyet
                results = dsTKKhongDuyet.Tables[0].Select(string.Format("SoToKhai = {0} And NamDK = {1} And MaLoaiHinh = '{2}'", e.Row.Cells["SoToKhai"].Text, e.Row.Cells["NamDangKY"].Text,e.Row.Cells["MaLoaiHinh"].Text.Trim()) );

                if (results.Length != 0)
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.BackColor = Color.Yellow;
                    e.Row.RowStyle.ForeColor = Color.Black;
                }
            }
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {
            try
            {
                GridEXRow row = dgList.GetRow();
                Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)row.DataRow;
                if (npl.CheckMaNPLDaThanhKhoan())
                {
                    ShowMessage("Nguyên phụ liệu '" + npl.MaNPL + "' của tờ khai " + npl.SoToKhai + "/" + npl.NamDangKy + " đã thanh khoản máy, không thể sửa.", false);
                    e.Cancel = true;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private Company.BLL.SXXK.ThanhKhoan.NPLNhapTon ConvertDataRowViewToObject(GridEXRow row)
        {
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplNT = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();

            nplNT.SoToKhai = Convert.ToInt32(row.Cells["SoToKhai"].Value);

            nplNT.SoToKhai = Convert.ToInt32(row.Cells["SoToKhai"].Value);
            nplNT.MaLoaiHinh = Convert.ToString(row.Cells["MaLoaiHinh"].Value);
            nplNT.NamDangKy = Convert.ToInt16(row.Cells["NamDangKy"].Value);
            nplNT.MaHaiQuan = Convert.ToString(row.Cells["MaHaiQuan"].Value);
            nplNT.MaNPL = Convert.ToString(row.Cells["MaNPL"].Value);
            nplNT.MaDoanhNghiep = Convert.ToString(row.Cells["MaDoanhNghiep"].Value);
            nplNT.Luong = Convert.ToDecimal(row.Cells["Luong"].Value);
            nplNT.Ton = Convert.ToDecimal(row.Cells["Ton"].Value);
            nplNT.ThueXNK = Convert.ToDouble(row.Cells["ThueXNK"].Value);
            nplNT.ThueTTDB = Convert.ToDouble(row.Cells["ThueTTDB"].Value);
            nplNT.ThueVAT = Convert.ToDouble(row.Cells["ThueVAT"].Value);
            nplNT.PhuThu = Convert.ToDouble(row.Cells["PhuThu"].Value);
            nplNT.ThueCLGia = Convert.ToDouble(row.Cells["ThueCLGia"].Value);
            nplNT.ThueXNKTon = Convert.ToDouble(row.Cells["ThueXNKTon"].Value);

            nplNT.TenNPL = Convert.ToString(row.Cells["TenNPL"].Value);
            nplNT.TenDVT = Convert.ToString(row.Cells["TenDVT"].Value);
            nplNT.SoLanThanhLy = Convert.ToInt32(row.Cells["SoLanThanhLy"].Value);
            nplNT.LanThanhLy = Convert.ToInt32(row.Cells["LanThanhLy"].Value);
            nplNT.TrangThaiThanhKhoan = Convert.ToInt32(row.Cells["TrangThaiThanhKhoan"].Value);
            nplNT.SaiSoLuong = Convert.ToInt32(row.Cells["SaiSoLuong"].Value);
            nplNT.SoLuongDangKy = Convert.ToDecimal(row.Cells["SoLuongDangKy"].Value);
            nplNT.TienTrinhChayThanhLy = Convert.ToString(row.Cells["TienTrinhChayThanhLy"].Value);

            return nplNT;
        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {

            dgList.Tables[0].Columns["SoToKhaiVNACCS"].Visible = true;
            //dgList.Tables[0].Columns["SoToKhai"].Visible = true;
            dgList.Tables[0].Columns["NgayDangKy"].Visible = true;
            dgList.Tables[0].Columns["MaLoaiHinh"].Visible = true;
            GridEXGroup customG = dgList.Tables[0].Groups[0].Clone();
            dgList.Tables[0].Groups.Clear();
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog(this);
            if (sfNPL.FileName != "")
            {
                gridEXExporter1.GridEX = dgList;
                Stream str = sfNPL.OpenFile();
                gridEXExporter1.Export(str);
                str.Close();
                dgList.Tables[0].Groups.Add(customG);
                dgList.Tables[0].Columns["SoToKhaiVNACCS"].Visible = false;
                dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
                dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;

                if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
            else
            {
                dgList.Tables[0].Columns["SoToKhaiVNACCS"].Visible = false;
                dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
                dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;
                dgList.Tables[0].Groups.Add(customG);
            }


        }

        private void uiButton5_Click(object sender, EventArgs e)
        {
            dgList.ShowFieldChooser();
            dgList.SaveSettings = true;
        }

        private void btnTimKiemNPL_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = Company.BLL.SXXK.ThanhKhoan.NPLNhapTon.SelectTongNPLbyTenChuHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, txtTenChuHang2.Text);
                dgListNPL.DataSource = ds.Tables[0];
                dgListNPL.Refetch();
            }
            catch { dgListNPL.Refresh(); }
        }

        private void uiTab1_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void uiTab1_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            if (uiTab1.SelectedIndex > 0)
            {
                this.AcceptButton = btnTimKiemNPL;
                btnTimKiemNPL_Click(null, null);

            }
            else
                this.AcceptButton = btnSearch;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (int.Parse(e.Row.Cells["TrangThaiThanhKhoan"].Value.ToString()) == 400)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã chạy thanh khoản";
                    e.Row.Cells["TrangThaiThanhKhoan"].ToolTipText = "Đã chạy thanh khoản";
                }

                else if (int.Parse(e.Row.Cells["TrangThaiThanhKhoan"].Value.ToString()) == 401)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đóng hồ sơ";
                    e.Row.Cells["TrangThaiThanhKhoan"].ToolTipText = "Đóng hồ sơ";
                }

                else if (int.Parse(e.Row.Cells["TrangThaiThanhKhoan"].Value.ToString()) == 0)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Chưa thanh khoản/ Đang nhập liệu";
                    e.Row.Cells["TrangThaiThanhKhoan"].ToolTipText = "Chưa thanh khoản/ Đang nhập liệu";
                }
                //if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("V"))
                //{
                //    e.Row.Cells["MaLoaiHinh"].Text = e.Row.Cells["MaLoaiHinh"].Value.ToString().Substring(2);
                //    e.Row.Cells["SoToKhai"].Text = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(System.Convert.ToInt16(e.Row.Cells["SoToKhai"].Value.ToString())).ToString();
                //}
//                 else
//                     e.Row.Cells["SoTKVNACCS"].Text = e.Row.Cells["SoToKhai"].Value.ToString();
            }
        }

        private void btnDieuChinhLuongTon_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection nplSelect = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();

                // Xây dựng điều kiện tìm kiếm.
                string where = "t.MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND t.MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND t.SoToKhai  like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND t.NamDangKy = " + txtNamTiepNhan.Value;
                }
                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }

                if (cbTrangThai.SelectedIndex > 0)
                {
                    where += " AND t.ThanhLy = '" + cbTrangThai.SelectedValue.ToString() + "'";
                }

                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                    {
                        where += " AND t.TenChuHang LIKE N'%" + txtTenChuHang.Text.Trim() + "%'";
                    }
                }
                else
                {
                    where += " AND t.TenChuHang LIKE N'%%'";

                }


                //where += "AND ( SELECT COUNT(*)  FROM[dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu  AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan WHERE nt.SoToKhai = t.SoToKhai AND nt.MaLoaiHinh = t.MaLoaiHinh AND nt.NamDangKy = t.NamDangKy AND nt.MaNPL = t.MaNPL AND nt.MaHaiQuan = t.MaHaiQuan AND nt.MaDoanhNghiep = t.MaDoanhNghiep AND nt.Luong <> h.SoLuong ) != 0 ";
                where += " AND t.SaiSoLuong = 1";

                where += " OR (t.SaiSoLuong = 0 AND t.SoLanThanhLy = 0 AND t.Luong <> t.Ton AND t.NamDangKy = " + txtNamTiepNhan.Value;

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND t.SoToKhai = " + txtSoTiepNhan.Value;
                }

                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }

                where += ")";

                //Bo sung 22/09/2011 by HUNGTQ
                where += " OR (t.TienTrinhChayThanhLy LIKE '%Sai%' AND t.SoLanThanhLy != 0 AND t.NamDangKy = " + txtNamTiepNhan.Value;

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND t.SoToKhai = " + txtSoTiepNhan.Value;
                }

                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }

                where += ")";

                // Thực hiện tìm kiếm.            
                nplSelect = clsnhapton.SelectCollection_ByYear(where, "t.SoToKhai");

                if (nplSelect.Count == 0)
                {
                    Globals.ShowMessage("Không có thông tin nguyên phụ liệu bị lệch lượng nhập và tồn đầu của Năm " + txtNamTiepNhan.Text + ".", false);
                    return;
                }
                else
                {
                    HangTonDieuChinh f = new HangTonDieuChinh();
                    f.NplTonSelect = nplSelect;
                    f.ShowDialog(this);

                    searchNew();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Điều chỉnh lượng tồn", ex);

                Globals.ShowMessage("Quá trình thực hiện có lỗi: " + ex.Message, false);
            }
            finally { this.Cursor = Cursors.Default; }
        }


    }
}