/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 08/24/2012 4:20:32 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] DROP
CONSTRAINT [FK_t_HangMauDich_t_ToKhaiMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] DROP CONSTRAINT [IX_t_KDT_HangMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ALTER COLUMN [SoLuong] [numeric] (18, 5) NULL
ALTER TABLE [dbo].[t_KDT_HangMauDich] ALTER COLUMN [TrongLuong] [numeric] (18, 5) NULL
ALTER TABLE [dbo].[t_KDT_HangMauDich] ALTER COLUMN [SoLuong_HTS] [numeric] (18, 5) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Delete]
	@ID char(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_NguyenTe]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViTinh_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViTinh_Update]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_DonViTinh]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViTinh_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViTinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViTinh]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViTinh_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViTinh_Load]
	@ID char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViTinh]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViTinh_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViTinh_InsertUpdate]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DonViTinh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DonViTinh] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DonViTinh]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdateByKTX]'
GO
    
    
    
    
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]    
-- Database: Haiquan    
-- Author: Ngo Thanh Tung    
-- Time created: Monday, August 18, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdateByKTX]    
 @ID bigint,    
 @TKMD_ID bigint,    
 @SoThuTuHang int,    
 @MaHS varchar(12),    
 @MaPhu varchar(30),    
 @TenHang nvarchar(80),    
 @NuocXX_ID char(3),    
 @DVT_ID char(3),    
 @SoLuong numeric(18, 5),    
 @TrongLuong numeric(18, 5),    
 @DonGiaKB numeric(38, 15),    
 @DonGiaTT numeric(38, 15),    
 @TriGiaKB numeric(38, 15),    
 @TriGiaTT numeric(38, 15),    
 @TriGiaKB_VND numeric(38, 15),    
 @ThueSuatXNK numeric(5, 2),    
 @ThueSuatTTDB numeric(5, 2),    
 @ThueSuatGTGT numeric(5, 2),    
 @ThueXNK money,    
 @ThueTTDB money,    
 @ThueGTGT money,    
 @PhuThu money,    
 @TyLeThuKhac numeric(5, 2),    
 @TriGiaThuKhac money,    
 @MienThue tinyint,    
 @Ma_HTS varchar(50),    
 @DVT_HTS char(3),    
 @SoLuong_HTS numeric(18, 5),    
 @ThueSuatXNKGiam  VARCHAR(50),    
 @ThueSuatTTDBGiam  VARCHAR(50),    
 @ThueSuatVATGiam  VARCHAR(50)    
AS    
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich]   
  WHERE    
   TKMD_ID = @TKMD_ID AND  
   MaHS = @MaHS AND  
   MaPhu = @MaPhu AND  
   TenHang = @TenHang AND  
   NuocXX_ID = @NuocXX_ID AND  
   DVT_ID = @DVT_ID AND  
   SoLuong = @SoLuong)    
 BEGIN    
  UPDATE    
   [dbo].[t_KDT_HangMauDich]     
  SET    
   [TKMD_ID] = @TKMD_ID,    
   [SoThuTuHang] = @SoThuTuHang,    
   [MaHS] = @MaHS,    
   [MaPhu] = @MaPhu,    
   [TenHang] = @TenHang,    
   [NuocXX_ID] = @NuocXX_ID,    
   [DVT_ID] = @DVT_ID,    
   [SoLuong] = @SoLuong,    
   [TrongLuong] = @TrongLuong,    
   [DonGiaKB] = @DonGiaKB,    
   [DonGiaTT] = @DonGiaTT,    
   [TriGiaKB] = @TriGiaKB,    
   [TriGiaTT] = @TriGiaTT,    
   [TriGiaKB_VND] = @TriGiaKB_VND,    
   [ThueSuatXNK] = @ThueSuatXNK,    
   [ThueSuatTTDB] = @ThueSuatTTDB,    
   [ThueSuatGTGT] = @ThueSuatGTGT,    
   [ThueXNK] = @ThueXNK,    
   [ThueTTDB] = @ThueTTDB,    
   [ThueGTGT] = @ThueGTGT,    
   [PhuThu] = @PhuThu,    
   [TyLeThuKhac] = @TyLeThuKhac,    
   [TriGiaThuKhac] = @TriGiaThuKhac,    
   [MienThue] = @MienThue,    
   [Ma_HTS] = @Ma_HTS,    
   [DVT_HTS] = @DVT_HTS,    
   [SoLuong_HTS] = @SoLuong_HTS,    
    ThueSuatXNKGiam=@ThueSuatXNKGiam,    
   ThueSuatTTDBGiam=@ThueSuatTTDBGiam,    
   ThueSuatVATGiam=@ThueSuatVATGiam    
  WHERE    
   [ID] = @ID    
 END    
ELSE    
 BEGIN    
      
  INSERT INTO [dbo].[t_KDT_HangMauDich]    
  (    
   [TKMD_ID],    
   [SoThuTuHang],    
   [MaHS],    
   [MaPhu],    
   [TenHang],    
   [NuocXX_ID],    
   [DVT_ID],    
   [SoLuong],    
   [TrongLuong],    
   [DonGiaKB],    
   [DonGiaTT],    
   [TriGiaKB],    
   [TriGiaTT],    
   [TriGiaKB_VND],    
   [ThueSuatXNK],    
   [ThueSuatTTDB],    
   [ThueSuatGTGT],    
   [ThueXNK],    
   [ThueTTDB],    
   [ThueGTGT],    
   [PhuThu],    
   [TyLeThuKhac],    
   [TriGiaThuKhac],    
   [MienThue],    
   [Ma_HTS],    
   [DVT_HTS],    
   [SoLuong_HTS],    
   ThueSuatXNKGiam,    
   ThueSuatTTDBGiam,    
   ThueSuatVATGiam    
  )    
  VALUES     
  (    
   @TKMD_ID,    
   @SoThuTuHang,    
   @MaHS,    
   @MaPhu,    
   @TenHang,    
   @NuocXX_ID,    
   @DVT_ID,    
   @SoLuong,    
   @TrongLuong,    
   @DonGiaKB,    
   @DonGiaTT,    
   @TriGiaKB,    
   @TriGiaTT,    
   @TriGiaKB_VND,    
   @ThueSuatXNK,    
   @ThueSuatTTDB,    
   @ThueSuatGTGT,    
   @ThueXNK,    
   @ThueTTDB,    
   @ThueGTGT,    
   @PhuThu,    
   @TyLeThuKhac,    
   @TriGiaThuKhac,    
   @MienThue,    
   @Ma_HTS,    
   @DVT_HTS,    
   @SoLuong_HTS,    
   @ThueSuatXNKGiam,    
   @ThueSuatTTDBGiam,    
   @ThueSuatVATGiam    
  )      
 END    
    
    
    
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViTinh_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViTinh_Insert]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_DonViTinh]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViTinh_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViTinh_Delete]
	@ID char(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DonViTinh]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Update]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_DonViHaiQuan]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Load]
	@ID char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DonViHaiQuan] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
	@ID char(6)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_Update]
	@ID varchar(7),
	@MoTa nvarchar(30),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_DieuKienGiaoHang]
SET
	[MoTa] = @MoTa,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DieuKienGiaoHang]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_Load]
	@ID varchar(7)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DieuKienGiaoHang]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_InsertUpdate]
	@ID varchar(7),
	@MoTa nvarchar(30),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DieuKienGiaoHang] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DieuKienGiaoHang] 
		SET
			[MoTa] = @MoTa,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DieuKienGiaoHang]
	(
			[ID],
			[MoTa],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@MoTa,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_Insert]
	@ID varchar(7),
	@MoTa nvarchar(30),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_DieuKienGiaoHang]
(
	[ID],
	[MoTa],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@MoTa,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_Delete]
	@ID varchar(7)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DieuKienGiaoHang]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Update]
	@ID varchar(6),
	@Ten nvarchar(50),
	@MaCuc char(2),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_CuaKhau]
SET
	[Ten] = @Ten,
	[MaCuc] = @MaCuc,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[MaCuc],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_CuaKhau]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Load]
	@ID varchar(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[MaCuc],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_CuaKhau]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]
	@ID varchar(6),
	@Ten nvarchar(50),
	@MaCuc char(2),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_CuaKhau] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_CuaKhau] 
		SET
			[Ten] = @Ten,
			[MaCuc] = @MaCuc,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_CuaKhau]
	(
			[ID],
			[Ten],
			[MaCuc],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@MaCuc,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Insert]
	@ID varchar(6),
	@Ten nvarchar(50),
	@MaCuc char(2),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_CuaKhau]
(
	[ID],
	[Ten],
	[MaCuc],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@MaCuc,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Delete]
	@ID varchar(6)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_CuaKhau]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_MaHS_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Update]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_MaHS_Update]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_MaHS]
SET
	[Nhom] = @Nhom,
	[PN1] = @PN1,
	[PN2] = @PN2,
	[Pn3] = @Pn3,
	[Mo_ta] = @Mo_ta,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[HS10SO] = @HS10SO


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_MaHS_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_SelectAll]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_MaHS]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Load]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Load]
	@HS10SO nvarchar(255)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_MaHS]
WHERE
	[HS10SO] = @HS10SO

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_InsertUpdate]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_InsertUpdate]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [HS10SO] FROM [dbo].[t_HaiQuan_MaHS] WHERE [HS10SO] = @HS10SO)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_MaHS] 
		SET
			[Nhom] = @Nhom,
			[PN1] = @PN1,
			[PN2] = @PN2,
			[Pn3] = @Pn3,
			[Mo_ta] = @Mo_ta,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[HS10SO] = @HS10SO
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_MaHS]
	(
			[Nhom],
			[PN1],
			[PN2],
			[Pn3],
			[Mo_ta],
			[HS10SO],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@Nhom,
			@PN1,
			@PN2,
			@Pn3,
			@Mo_ta,
			@HS10SO,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Insert]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Insert]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_MaHS]
(
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@Nhom,
	@PN1,
	@PN2,
	@Pn3,
	@Mo_ta,
	@HS10SO,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Delete]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Delete]
	@HS10SO nvarchar(255)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_MaHS]
WHERE
	[HS10SO] = @HS10SO


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]'
GO
CREATE TABLE [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
(
[ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_LoaiPhiChungTuThanhToan] on [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] ADD CONSTRAINT [PK_t_HaiQuan_LoaiPhiChungTuThanhToan] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Delete]
-- Database: ECS_TQDT_gc_v3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Delete]
	@ID varchar(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Insert]
-- Database: ECS_TQDT_gc_v3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Insert]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_InsertUpdate]
-- Database: ECS_TQDT_gc_v3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_InsertUpdate]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Load]
-- Database: ECS_TQDT_gc_v3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Load]
	@ID varchar(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectAll]
-- Database: ECS_TQDT_gc_v3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Update]
-- Database: ECS_TQDT_gc_v3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Update]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 06, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@ThueSuatXNKGiam varchar(50),
	@ThueSuatTTDBGiam varchar(50),
	@ThueSuatVATGiam varchar(50),
	@MaHSMoRong nvarchar(12),
	@FOC bit,
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@TenHangSX nvarchar(256),
	@MaHangSX nvarchar(30),
	@ThueTuyetDoi bit,
	@DonGiaTuyetDoi float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangMauDich]
(
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[MaHSMoRong],
	[FOC],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[TenHangSX],
	[MaHangSX],
	[ThueTuyetDoi],
	[DonGiaTuyetDoi]
)
VALUES 
(
	@TKMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@TrongLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueSuatXNK,
	@ThueSuatTTDB,
	@ThueSuatGTGT,
	@ThueXNK,
	@ThueTTDB,
	@ThueGTGT,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue,
	@Ma_HTS,
	@DVT_HTS,
	@SoLuong_HTS,
	@ThueSuatXNKGiam,
	@ThueSuatTTDBGiam,
	@ThueSuatVATGiam,
	@MaHSMoRong,
	@FOC,
	@NhanHieu,
	@QuyCachPhamChat,
	@ThanhPhan,
	@Model,
	@TenHangSX,
	@MaHangSX,
	@ThueTuyetDoi,
	@DonGiaTuyetDoi
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 06, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@ThueSuatXNKGiam varchar(50),
	@ThueSuatTTDBGiam varchar(50),
	@ThueSuatVATGiam varchar(50),
	@MaHSMoRong nvarchar(12),
	@FOC bit,
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@TenHangSX nvarchar(256),
	@MaHangSX nvarchar(30),
	@ThueTuyetDoi bit,
	@DonGiaTuyetDoi float
AS

UPDATE
	[dbo].[t_KDT_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueXNK] = @ThueXNK,
	[ThueTTDB] = @ThueTTDB,
	[ThueGTGT] = @ThueGTGT,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue,
	[Ma_HTS] = @Ma_HTS,
	[DVT_HTS] = @DVT_HTS,
	[SoLuong_HTS] = @SoLuong_HTS,
	[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
	[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
	[ThueSuatVATGiam] = @ThueSuatVATGiam,
	[MaHSMoRong] = @MaHSMoRong,
	[FOC] = @FOC,
	[NhanHieu] = @NhanHieu,
	[QuyCachPhamChat] = @QuyCachPhamChat,
	[ThanhPhan] = @ThanhPhan,
	[Model] = @Model,
	[TenHangSX] = @TenHangSX,
	[MaHangSX] = @MaHangSX,
	[ThueTuyetDoi] = @ThueTuyetDoi,
	[DonGiaTuyetDoi] = @DonGiaTuyetDoi
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 06, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@ThueSuatXNKGiam varchar(50),
	@ThueSuatTTDBGiam varchar(50),
	@ThueSuatVATGiam varchar(50),
	@MaHSMoRong nvarchar(12),
	@FOC bit,
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@TenHangSX nvarchar(256),
	@MaHangSX nvarchar(30),
	@ThueTuyetDoi bit,
	@DonGiaTuyetDoi float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
			[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
			[ThueSuatVATGiam] = @ThueSuatVATGiam,
			[MaHSMoRong] = @MaHSMoRong,
			[FOC] = @FOC,
			[NhanHieu] = @NhanHieu,
			[QuyCachPhamChat] = @QuyCachPhamChat,
			[ThanhPhan] = @ThanhPhan,
			[Model] = @Model,
			[TenHangSX] = @TenHangSX,
			[MaHangSX] = @MaHangSX,
			[ThueTuyetDoi] = @ThueTuyetDoi,
			[DonGiaTuyetDoi] = @DonGiaTuyetDoi
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			[ThueSuatXNKGiam],
			[ThueSuatTTDBGiam],
			[ThueSuatVATGiam],
			[MaHSMoRong],
			[FOC],
			[NhanHieu],
			[QuyCachPhamChat],
			[ThanhPhan],
			[Model],
			[TenHangSX],
			[MaHangSX],
			[ThueTuyetDoi],
			[DonGiaTuyetDoi]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@ThueSuatXNKGiam,
			@ThueSuatTTDBGiam,
			@ThueSuatVATGiam,
			@MaHSMoRong,
			@FOC,
			@NhanHieu,
			@QuyCachPhamChat,
			@ThanhPhan,
			@Model,
			@TenHangSX,
			@MaHangSX,
			@ThueTuyetDoi,
			@DonGiaTuyetDoi
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_Update]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_PhuongThucVanTai]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_PhuongThucVanTai]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_Load]
	@ID varchar(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_PhuongThucVanTai]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_InsertUpdate]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_PhuongThucVanTai] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_PhuongThucVanTai] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_PhuongThucVanTai]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_Insert]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_PhuongThucVanTai]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_Delete]
	@ID varchar(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_PhuongThucVanTai]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_PhuongThucThanhToan_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_Update]
	@ID varchar(10),
	@GhiChu nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_PhuongThucThanhToan]
SET
	[GhiChu] = @GhiChu,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GhiChu],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_PhuongThucThanhToan]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_PhuongThucThanhToan_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_Load]
	@ID varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GhiChu],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_PhuongThucThanhToan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_PhuongThucThanhToan_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_InsertUpdate]
	@ID varchar(10),
	@GhiChu nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_PhuongThucThanhToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_PhuongThucThanhToan] 
		SET
			[GhiChu] = @GhiChu,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_PhuongThucThanhToan]
	(
			[ID],
			[GhiChu],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@GhiChu,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_PhuongThucThanhToan_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_Insert]
	@ID varchar(10),
	@GhiChu nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_PhuongThucThanhToan]
(
	[ID],
	[GhiChu],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@GhiChu,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_PhuongThucThanhToan_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_Delete]
	@ID varchar(10)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_PhuongThucThanhToan]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_Update]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_Nuoc]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_Nuoc]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_Load]
	@ID char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_Nuoc]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_InsertUpdate]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_Nuoc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_Nuoc] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_Nuoc]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_InsertUpdateBy]'
GO

------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_InsertUpdate]      
-- Database: ECS_TQDT_GC      
-- Author: Ngo Thanh Tung      
-- Time created: Tuesday, February 22, 2011      
------------------------------------------------------------------------------------------------------------------------      
      
ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_InsertUpdateBy]      
 @ID bigint,      
 @IDHopDong bigint,      
 @SoTiepNhan bigint,      
 @NgayTiepNhan datetime,      
 @TrangThaiXuLy int,      
 @MaHaiQuanTiepNhan char(6),      
 @SoToKhai bigint,      
 @CanBoDangKy nvarchar(50),      
 @NgayDangKy datetime,      
 @MaDoanhNghiep varchar(20),      
 @SoHopDongDV varchar(50),      
 @NgayHDDV datetime,      
 @NgayHetHanHDDV datetime,      
 @NguoiChiDinhDV nvarchar(250),      
 @MaKhachHang varchar(20),      
 @TenKH nvarchar(250),      
 @SoHDKH varchar(50),      
 @NgayHDKH datetime,      
 @NgayHetHanHDKH datetime,      
 @NguoiChiDinhKH nvarchar(250),      
 @MaDaiLy varchar(20),      
 @MaLoaiHinh nchar(10),      
 @DiaDiemXepHang nvarchar(250),      
 @TyGiaVND money,      
 @TyGiaUSD money,      
 @LePhiHQ money,      
 @SoBienLai nchar(10),      
 @NgayBienLai datetime,      
 @ChungTu nvarchar(250),      
 @NguyenTe_ID char(3),      
 @MaHaiQuanKH char(6),      
 @ID_Relation bigint,      
 @GUIDSTR nvarchar(500),      
 @DeXuatKhac nvarchar(500),      
 @LyDoSua nvarchar(500),      
 @ActionStatus int,      
 @GuidReference nvarchar(500),      
 @NamDK smallint,      
 @HUONGDAN nvarchar(500),      
 @PhanLuong varchar(50),      
 @Huongdan_PL nvarchar(500),      
 @PTTT_ID varchar(10),      
 @DKGH_ID varchar(7),      
 @ThoiGianGiaoHang datetime  ,
 @SoKien numeric(15, 0),  
 @TrongLuong numeric(18, 2),  
 @TrongLuongTinh float    
AS      
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_ToKhaiChuyenTiep] WHERE SoToKhai = @SoToKhai AND NgayDangKy = @NgayDangKy AND MaLoaiHinh = @MaLoaiHinh AND MaHaiQuanTiepNhan = @MaHaiQuanTiepNhan AND MaDoanhNghiep = @MaDoanhNghiep AND IDHopDong = @IDHopDong)    
  
 BEGIN      
  UPDATE      
   [dbo].[t_KDT_GC_ToKhaiChuyenTiep]       
  SET      
   [IDHopDong] = @IDHopDong,      
   [SoTiepNhan] = @SoTiepNhan,      
   [NgayTiepNhan] = @NgayTiepNhan,      
   [TrangThaiXuLy] = @TrangThaiXuLy,      
   [MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,      
   [SoToKhai] = @SoToKhai,      
   [CanBoDangKy] = @CanBoDangKy,      
   [NgayDangKy] = @NgayDangKy,      
   [MaDoanhNghiep] = @MaDoanhNghiep,      
   [SoHopDongDV] = @SoHopDongDV,      
   [NgayHDDV] = @NgayHDDV,      
   [NgayHetHanHDDV] = @NgayHetHanHDDV,      
   [NguoiChiDinhDV] = @NguoiChiDinhDV,      
   [MaKhachHang] = @MaKhachHang,      
   [TenKH] = @TenKH,      
   [SoHDKH] = @SoHDKH,      
   [NgayHDKH] = @NgayHDKH,      
   [NgayHetHanHDKH] = @NgayHetHanHDKH,      
   [NguoiChiDinhKH] = @NguoiChiDinhKH,      
   [MaDaiLy] = @MaDaiLy,      
   [MaLoaiHinh] = @MaLoaiHinh,      
   [DiaDiemXepHang] = @DiaDiemXepHang,      
   [TyGiaVND] = @TyGiaVND,      
   [TyGiaUSD] = @TyGiaUSD,      
   [LePhiHQ] = @LePhiHQ,      
   [SoBienLai] = @SoBienLai,      
   [NgayBienLai] = @NgayBienLai,      
   [ChungTu] = @ChungTu,      
   [NguyenTe_ID] = @NguyenTe_ID,      
   [MaHaiQuanKH] = @MaHaiQuanKH,      
   [ID_Relation] = @ID_Relation,      
   [GUIDSTR] = @GUIDSTR,      
   [DeXuatKhac] = @DeXuatKhac,      
   [LyDoSua] = @LyDoSua,      
   [ActionStatus] = @ActionStatus,      
   [GuidReference] = @GuidReference,      
   [NamDK] = @NamDK,      
   [HUONGDAN] = @HUONGDAN,      
   [PhanLuong] = @PhanLuong,      
   [Huongdan_PL] = @Huongdan_PL,      
   [PTTT_ID] = @PTTT_ID,      
   [DKGH_ID] = @DKGH_ID,      
   [ThoiGianGiaoHang] = @ThoiGianGiaoHang,
   [SoKien] = @SoKien,  
   [TrongLuong] = @TrongLuong,  
   [TrongLuongNet] = @TrongLuongTinh
  WHERE      
   [ID] = @ID      
 END      
ELSE      
 BEGIN      
        
  INSERT INTO [dbo].[t_KDT_GC_ToKhaiChuyenTiep]      
  (      
   [IDHopDong],      
   [SoTiepNhan],      
   [NgayTiepNhan],      
   [TrangThaiXuLy],      
   [MaHaiQuanTiepNhan],      
   [SoToKhai],      
   [CanBoDangKy],         [NgayDangKy],      
   [MaDoanhNghiep],      
   [SoHopDongDV],      
   [NgayHDDV],      
   [NgayHetHanHDDV],      
   [NguoiChiDinhDV],      
   [MaKhachHang],      
   [TenKH],      
   [SoHDKH],      
   [NgayHDKH],      
   [NgayHetHanHDKH],      
   [NguoiChiDinhKH],      
   [MaDaiLy],       [MaLoaiHinh],      
   [DiaDiemXepHang],      
   [TyGiaVND],      
   [TyGiaUSD],      
   [LePhiHQ],      
   [SoBienLai],      
   [NgayBienLai],      
   [ChungTu],      
   [NguyenTe_ID],      
   [MaHaiQuanKH],      
   [ID_Relation],      
   [GUIDSTR],      
   [DeXuatKhac],      
   [LyDoSua],      
   [ActionStatus],      
   [GuidReference],      
   [NamDK],      
   [HUONGDAN],      
   [PhanLuong],      
   [Huongdan_PL],      
   [PTTT_ID],      
   [DKGH_ID],      
   [ThoiGianGiaoHang]   ,
   [SoKien],  
   [TrongLuong],  
   [TrongLuongNet]    
  )      
  VALUES       
  (      
   @IDHopDong,      
   @SoTiepNhan,      
   @NgayTiepNhan,      
   @TrangThaiXuLy,      
   @MaHaiQuanTiepNhan,      
   @SoToKhai,      
   @CanBoDangKy,      
   @NgayDangKy,      
   @MaDoanhNghiep,      
   @SoHopDongDV,      
   @NgayHDDV,      
   @NgayHetHanHDDV,      
   @NguoiChiDinhDV,      
   @MaKhachHang,      
   @TenKH,      
   @SoHDKH,      
   @NgayHDKH,      
   @NgayHetHanHDKH,      
   @NguoiChiDinhKH,      
   @MaDaiLy,      
   @MaLoaiHinh,      
   @DiaDiemXepHang,      
   @TyGiaVND,      
   @TyGiaUSD,      
   @LePhiHQ,      
   @SoBienLai,      
   @NgayBienLai,      
   @ChungTu,      
   @NguyenTe_ID,      
   @MaHaiQuanKH,      
   @ID_Relation,      
   @GUIDSTR,      
   @DeXuatKhac,      
   @LyDoSua,      
   @ActionStatus,      
   @GuidReference,      
   @NamDK,      
   @HUONGDAN,      
   @PhanLuong,      
   @Huongdan_PL,      
   @PTTT_ID,      
   @DKGH_ID,      
   @ThoiGianGiaoHang      ,
   @SoKien,  
   @TrongLuong,  
   @TrongLuongTinh
  )        
 END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_Insert]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_Nuoc]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_Delete]
	@ID char(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_Nuoc]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Update]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money,
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_NguyenTe]
SET
	[Ten] = @Ten,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_LoadBy]'
GO
     
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Load]    
-- Database: ECS_TQDT_GC    
-- Author: Ngo Thanh Tung    
-- Time created: Tuesday, February 22, 2011    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_LoadBy]    
 @IDHopDong bigint,      
 @SoToKhai bigint,      
 @MaLoaiHinh nchar(10),      
 @NgayDangKy DATETIME,      
 @MaDoanhNghiep varchar(20),      
 @MaHaiQuanTiepNhan char(6)     
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
SELECT    
 [ID],    
 [IDHopDong],    
 [SoTiepNhan],    
 [NgayTiepNhan],    
 [TrangThaiXuLy],    
 [MaHaiQuanTiepNhan],    
 [SoToKhai],    
 [CanBoDangKy],    
 [NgayDangKy],    
 [MaDoanhNghiep],    
 [SoHopDongDV],    
 [NgayHDDV],    
 [NgayHetHanHDDV],    
 [NguoiChiDinhDV],    
 [MaKhachHang],    
 [TenKH],    
 [SoHDKH],    
 [NgayHDKH],    
 [NgayHetHanHDKH],    
 [NguoiChiDinhKH],    
 [MaDaiLy],    
 [MaLoaiHinh],    
 [DiaDiemXepHang],    
 [TyGiaVND],    
 [TyGiaUSD],    
 [LePhiHQ],    
 [SoBienLai],    
 [NgayBienLai],    
 [ChungTu],    
 [NguyenTe_ID],    
 [MaHaiQuanKH],    
 [ID_Relation],    
 [GUIDSTR],    
 [DeXuatKhac],    
 [LyDoSua],    
 [ActionStatus],    
 [GuidReference],    
 [NamDK],    
 [HUONGDAN],    
 [PhanLuong],    
 [Huongdan_PL],    
 [PTTT_ID],    
 [DKGH_ID],    
 [ThoiGianGiaoHang],
 [SoKien],  
 [TrongLuong],  
 [TrongLuongNet] AS TrongLuongTinh
FROM    
 [dbo].[t_KDT_GC_ToKhaiChuyenTiep]    
WHERE    
 SoToKhai = @SoToKhai AND NgayDangKy = @NgayDangKy AND MaLoaiHinh = @MaLoaiHinh AND MaHaiQuanTiepNhan = @MaHaiQuanTiepNhan AND MaDoanhNghiep = @MaDoanhNghiep AND IDHopDong = @IDHopDong
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[TyGiaTinhThue],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_NguyenTe]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Load]
	@ID char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[TyGiaTinhThue],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_NguyenTe]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money,
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_NguyenTe] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_NguyenTe] 
		SET
			[Ten] = @Ten,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_NguyenTe]
	(
			[ID],
			[Ten],
			[TyGiaTinhThue],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@TyGiaTinhThue,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Insert]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money,
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_NguyenTe]
(
	[ID],
	[Ten],
	[TyGiaTinhThue],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@TyGiaTinhThue,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdateBy]'
GO
    
    
    
    
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]    
-- Database: Haiquan    
-- Author: Ngo Thanh Tung    
-- Time created: Monday, August 18, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdateBy]    
 @ID bigint,    
 @TKMD_ID bigint,    
 @SoThuTuHang int,    
 @MaHS varchar(12),    
 @MaPhu varchar(30),    
 @TenHang nvarchar(80),    
 @NuocXX_ID char(3),    
 @DVT_ID char(3),    
 @SoLuong numeric(18, 5),    
 @TrongLuong numeric(18, 5),    
 @DonGiaKB numeric(38, 15),    
 @DonGiaTT numeric(38, 15),    
 @TriGiaKB numeric(38, 15),    
 @TriGiaTT numeric(38, 15),    
 @TriGiaKB_VND numeric(38, 15),    
 @ThueSuatXNK numeric(5, 2),    
 @ThueSuatTTDB numeric(5, 2),    
 @ThueSuatGTGT numeric(5, 2),    
 @ThueXNK money,    
 @ThueTTDB money,    
 @ThueGTGT money,    
 @PhuThu money,    
 @TyLeThuKhac numeric(5, 2),    
 @TriGiaThuKhac money,    
 @MienThue tinyint,    
 @Ma_HTS varchar(50),    
 @DVT_HTS char(3),    
 @SoLuong_HTS numeric(18, 5),    
 @ThueSuatXNKGiam  VARCHAR(50),    
 @ThueSuatTTDBGiam  VARCHAR(50),    
 @ThueSuatVATGiam  VARCHAR(50)    
AS    
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich]   
  WHERE    
   TKMD_ID = @TKMD_ID AND  
   MaHS = @MaHS AND  
   MaPhu = @MaPhu AND  
   TenHang = @TenHang AND  
   NuocXX_ID = @NuocXX_ID AND  
   DVT_ID = @DVT_ID AND  
   SoLuong = @SoLuong)    
 BEGIN    
  UPDATE    
   [dbo].[t_KDT_HangMauDich]     
  SET    
   [TKMD_ID] = @TKMD_ID,    
   [SoThuTuHang] = @SoThuTuHang,    
   [MaHS] = @MaHS,    
   [MaPhu] = @MaPhu,    
   [TenHang] = @TenHang,    
   [NuocXX_ID] = @NuocXX_ID,    
   [DVT_ID] = @DVT_ID,    
   [SoLuong] = @SoLuong,    
   [TrongLuong] = @TrongLuong,    
   [DonGiaKB] = @DonGiaKB,    
   [DonGiaTT] = @DonGiaTT,    
   [TriGiaKB] = @TriGiaKB,    
   [TriGiaTT] = @TriGiaTT,    
   [TriGiaKB_VND] = @TriGiaKB_VND,    
   [ThueSuatXNK] = @ThueSuatXNK,    
   [ThueSuatTTDB] = @ThueSuatTTDB,    
   [ThueSuatGTGT] = @ThueSuatGTGT,    
   [ThueXNK] = @ThueXNK,    
   [ThueTTDB] = @ThueTTDB,    
   [ThueGTGT] = @ThueGTGT,    
   [PhuThu] = @PhuThu,    
   [TyLeThuKhac] = @TyLeThuKhac,    
   [TriGiaThuKhac] = @TriGiaThuKhac,    
   [MienThue] = @MienThue,    
   [Ma_HTS] = @Ma_HTS,    
   [DVT_HTS] = @DVT_HTS,    
   [SoLuong_HTS] = @SoLuong_HTS,    
    ThueSuatXNKGiam=@ThueSuatXNKGiam,    
   ThueSuatTTDBGiam=@ThueSuatTTDBGiam,    
   ThueSuatVATGiam=@ThueSuatVATGiam    
  WHERE    
   TKMD_ID = @TKMD_ID AND  
   MaHS = @MaHS AND  
   MaPhu = @MaPhu AND  
   TenHang = @TenHang AND  
   NuocXX_ID = @NuocXX_ID AND  
   DVT_ID = @DVT_ID AND  
   SoLuong = @SoLuong    
 END    
ELSE    
 BEGIN    
      
  INSERT INTO [dbo].[t_KDT_HangMauDich]    
  (    
   [TKMD_ID],    
   [SoThuTuHang],    
   [MaHS],    
   [MaPhu],    
   [TenHang],    
   [NuocXX_ID],    
   [DVT_ID],    
   [SoLuong],    
   [TrongLuong],    
   [DonGiaKB],    
   [DonGiaTT],    
   [TriGiaKB],    
   [TriGiaTT],    
   [TriGiaKB_VND],    
   [ThueSuatXNK],    
   [ThueSuatTTDB],    
   [ThueSuatGTGT],    
   [ThueXNK],    
   [ThueTTDB],    
   [ThueGTGT],    
   [PhuThu],    
   [TyLeThuKhac],    
   [TriGiaThuKhac],    
   [MienThue],    
   [Ma_HTS],    
   [DVT_HTS],    
   [SoLuong_HTS],    
   ThueSuatXNKGiam,    
   ThueSuatTTDBGiam,    
   ThueSuatVATGiam    
  )    
  VALUES     
  (    
   @TKMD_ID,    
   @SoThuTuHang,    
   @MaHS,    
   @MaPhu,    
   @TenHang,    
   @NuocXX_ID,    
   @DVT_ID,    
   @SoLuong,    
   @TrongLuong,    
   @DonGiaKB,    
   @DonGiaTT,    
   @TriGiaKB,    
   @TriGiaTT,    
   @TriGiaKB_VND,    
   @ThueSuatXNK,    
   @ThueSuatTTDB,    
   @ThueSuatGTGT,    
   @ThueXNK,    
   @ThueTTDB,    
   @ThueGTGT,    
   @PhuThu,    
   @TyLeThuKhac,    
   @TriGiaThuKhac,    
   @MienThue,    
   @Ma_HTS,    
   @DVT_HTS,    
   @SoLuong_HTS,    
   @ThueSuatXNKGiam,    
   @ThueSuatTTDBGiam,    
   @ThueSuatVATGiam    
  )      
 END    
    
    
    
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_InsertUpdateBy]'
GO
      
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]      
-- Database: ECS_GCTQ      
-- Author: Ngo Thanh Tung      
-- Time created: Wednesday, January 06, 2010      
------------------------------------------------------------------------------------------------------------------------      
      
ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_InsertUpdateBy]      
 @ID bigint,      
 @SoTiepNhan bigint,      
 @NgayTiepNhan datetime,      
 @MaHaiQuan char(6),      
 @SoToKhai int,      
 @MaLoaiHinh char(5),      
 @NgayDangKy datetime,      
 @MaDoanhNghiep varchar(14),      
 @TenDoanhNghiep nvarchar(255),      
 @MaDaiLyTTHQ varchar(14),      
 @TenDaiLyTTHQ nvarchar(255),      
 @TenDonViDoiTac nvarchar(255),      
 @ChiTietDonViDoiTac nvarchar(255),      
 @SoGiayPhep nvarchar(50),      
 @NgayGiayPhep datetime,      
 @NgayHetHanGiayPhep datetime,      
 @SoHopDong nvarchar(50),      
 @NgayHopDong datetime,      
 @NgayHetHanHopDong datetime,      
 @SoHoaDonThuongMai nvarchar(50),      
 @NgayHoaDonThuongMai datetime,      
 @PTVT_ID varchar(3),      
 @SoHieuPTVT nvarchar(50),      
 @NgayDenPTVT datetime,      
 @QuocTichPTVT_ID char(3),      
 @LoaiVanDon nvarchar(50),      
 @SoVanDon nvarchar(50),      
 @NgayVanDon datetime,      
 @NuocXK_ID char(3),      
 @NuocNK_ID char(3),      
 @DiaDiemXepHang nvarchar(255),      
 @CuaKhau_ID char(4),      
 @DKGH_ID varchar(7),      
 @NguyenTe_ID char(3),      
 @TyGiaTinhThue money,      
 @TyGiaUSD money,      
 @PTTT_ID varchar(10),      
 @SoHang smallint,      
 @SoLuongPLTK smallint,      
 @TenChuHang nvarchar(30),    
 @ChucVu nvarchar(20),    
 @SoContainer20 numeric(15, 0),      
 @SoContainer40 numeric(15, 0),      
 @SoKien numeric(15, 0),      
 @TrongLuong numeric(18, 2),      
 @TongTriGiaKhaiBao money,      
 @TongTriGiaTinhThue money,      
 @LoaiToKhaiGiaCong varchar(3),      
 @LePhiHaiQuan money,      
 @PhiBaoHiem money,      
 @PhiVanChuyen money,      
 @PhiXepDoHang money,      
 @PhiKhac money,      
 @CanBoDangKy varchar(100),      
 @QuanLyMay bit,      
 @TrangThaiXuLy int,      
 @LoaiHangHoa char(1),      
 @GiayTo nvarchar(40),      
 @PhanLuong varchar(2),      
 @MaDonViUT varchar(14),      
 @TenDonViUT nvarchar(50),      
 @TrongLuongNet float,      
 @SoTienKhoan money,      
 @IDHopDong bigint,      
 @MaMid varchar(100),      
 @Ngay_THN_THX datetime,      
 @TrangThaiPhanBo int,      
 @GUIDSTR nvarchar(500),      
 @DeXuatKhac nvarchar(500),      
 @LyDoSua nvarchar(500),      
 @ActionStatus smallint,      
 @GuidReference nvarchar(500),      
 @NamDK int,      
 @HUONGDAN nvarchar(max)      
AS      
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE SoToKhai = @SoToKhai AND NgayDangKy = @NgayDangKy AND MaLoaiHinh = @MaLoaiHinh AND MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep AND IDHopDong = @IDHopDong AND SoHopDong = @SoHopDong
)      
 BEGIN      
  UPDATE      
   [dbo].[t_KDT_ToKhaiMauDich]       
  SET      
   [SoTiepNhan] = @SoTiepNhan,      
   [NgayTiepNhan] = @NgayTiepNhan,      
   [MaHaiQuan] = @MaHaiQuan,      
   [SoToKhai] = @SoToKhai,      
   [MaLoaiHinh] = @MaLoaiHinh,      
   [NgayDangKy] = @NgayDangKy,      
   [MaDoanhNghiep] = @MaDoanhNghiep,      
   [TenDoanhNghiep] = @TenDoanhNghiep,      
   [MaDaiLyTTHQ] = @MaDaiLyTTHQ,      
   [TenDaiLyTTHQ] = @TenDaiLyTTHQ,      
   [TenDonViDoiTac] = @TenDonViDoiTac,      
   [ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,      
   [SoGiayPhep] = @SoGiayPhep,      
   [NgayGiayPhep] = @NgayGiayPhep,      
   [NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,      
   [SoHopDong] = @SoHopDong,      
   [NgayHopDong] = @NgayHopDong,      
   [NgayHetHanHopDong] = @NgayHetHanHopDong,      
   [SoHoaDonThuongMai] = @SoHoaDonThuongMai,      
   [NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,      
   [PTVT_ID] = @PTVT_ID,      
   [SoHieuPTVT] = @SoHieuPTVT,      
   [NgayDenPTVT] = @NgayDenPTVT,      
   [QuocTichPTVT_ID] = @QuocTichPTVT_ID,      
   [LoaiVanDon] = @LoaiVanDon,      
   [SoVanDon] = @SoVanDon,      
   [NgayVanDon] = @NgayVanDon,      
   [NuocXK_ID] = @NuocXK_ID,      
   [NuocNK_ID] = @NuocNK_ID,      
   [DiaDiemXepHang] = @DiaDiemXepHang,      
   [CuaKhau_ID] = @CuaKhau_ID,      
   [DKGH_ID] = @DKGH_ID,      
   [NguyenTe_ID] = @NguyenTe_ID,      
   [TyGiaTinhThue] = @TyGiaTinhThue,      
   [TyGiaUSD] = @TyGiaUSD,      
   [PTTT_ID] = @PTTT_ID,      
   [SoHang] = @SoHang,      
   [SoLuongPLTK] = @SoLuongPLTK,      
   [TenChuHang] = @TenChuHang,   
   [ChucVu] = @ChucVu,     
   [SoContainer20] = @SoContainer20,      
   [SoContainer40] = @SoContainer40,      
   [SoKien] = @SoKien,      
   [TrongLuong] = @TrongLuong,      
   [TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,      
   [TongTriGiaTinhThue] = @TongTriGiaTinhThue,      
   [LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,      
   [LePhiHaiQuan] = @LePhiHaiQuan,      
   [PhiBaoHiem] = @PhiBaoHiem,      
   [PhiVanChuyen] = @PhiVanChuyen,      
   [PhiXepDoHang] = @PhiXepDoHang,      
   [PhiKhac] = @PhiKhac,      
   [CanBoDangKy] = @CanBoDangKy,      
   [QuanLyMay] = @QuanLyMay,      
   [TrangThaiXuLy] = @TrangThaiXuLy,      
   [LoaiHangHoa] = @LoaiHangHoa,      
   [GiayTo] = @GiayTo,      
   [PhanLuong] = @PhanLuong,      
   [MaDonViUT] = @MaDonViUT,      
   [TenDonViUT] = @TenDonViUT,      
   [TrongLuongNet] = @TrongLuongNet,      
   [SoTienKhoan] = @SoTienKhoan,      
   [IDHopDong] = @IDHopDong,      
   [MaMid] = @MaMid,      
   [Ngay_THN_THX] = @Ngay_THN_THX,      
   [TrangThaiPhanBo] = @TrangThaiPhanBo,      
   [GUIDSTR] = @GUIDSTR,      
   [DeXuatKhac] = @DeXuatKhac,      
   [LyDoSua] = @LyDoSua,      
   [ActionStatus] = @ActionStatus,      
   [GuidReference] = @GuidReference,      
   [NamDK] = @NamDK,      
   [HUONGDAN] = @HUONGDAN      
  WHERE      
   [ID] = @ID      
 END      
ELSE      
 BEGIN      
        
  INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]      
  (      
   [SoTiepNhan],      
   [NgayTiepNhan],      
   [MaHaiQuan],      
   [SoToKhai],      
   [MaLoaiHinh],      
   [NgayDangKy],      
   [MaDoanhNghiep],      
   [TenDoanhNghiep],      
   [MaDaiLyTTHQ],      
   [TenDaiLyTTHQ],      
   [TenDonViDoiTac],      
   [ChiTietDonViDoiTac],      
   [SoGiayPhep],      
   [NgayGiayPhep],      
   [NgayHetHanGiayPhep],      
   [SoHopDong],      
   [NgayHopDong],      
   [NgayHetHanHopDong],      
   [SoHoaDonThuongMai],      
   [NgayHoaDonThuongMai],      
   [PTVT_ID],      
   [SoHieuPTVT],      
   [NgayDenPTVT],      
   [QuocTichPTVT_ID],      
   [LoaiVanDon],      
   [SoVanDon],      
   [NgayVanDon],      
   [NuocXK_ID],      
   [NuocNK_ID],      
   [DiaDiemXepHang],      
   [CuaKhau_ID],      
   [DKGH_ID],      
   [NguyenTe_ID],      
   [TyGiaTinhThue],      
   [TyGiaUSD],      
   [PTTT_ID],      
   [SoHang],      
   [SoLuongPLTK],      
   [TenChuHang], 
   [ChucVu],     
   [SoContainer20],      
   [SoContainer40],      
   [SoKien],      
   [TrongLuong],      
   [TongTriGiaKhaiBao],      
   [TongTriGiaTinhThue],      
   [LoaiToKhaiGiaCong],      
   [LePhiHaiQuan],      
   [PhiBaoHiem],      
   [PhiVanChuyen],      
   [PhiXepDoHang],      
   [PhiKhac],      
   [CanBoDangKy],      
   [QuanLyMay],      
   [TrangThaiXuLy],      
   [LoaiHangHoa],      
   [GiayTo],      
   [PhanLuong],      
   [MaDonViUT],      
   [TenDonViUT],      
   [TrongLuongNet],      
   [SoTienKhoan],      
   [IDHopDong],      
   [MaMid],      
   [Ngay_THN_THX],      
   [TrangThaiPhanBo],      
   [GUIDSTR],      
   [DeXuatKhac],      
   [LyDoSua],      
   [ActionStatus],      
   [GuidReference],      
   [NamDK],      
   [HUONGDAN]   
   
  )      
  VALUES       
  (      
   @SoTiepNhan,      
   @NgayTiepNhan,      
   @MaHaiQuan,      
   @SoToKhai,      
   @MaLoaiHinh,      
   @NgayDangKy,      
   @MaDoanhNghiep,      
   @TenDoanhNghiep,      
   @MaDaiLyTTHQ,      
   @TenDaiLyTTHQ,      
   @TenDonViDoiTac,      
   @ChiTietDonViDoiTac,      
   @SoGiayPhep,      
   @NgayGiayPhep,      
   @NgayHetHanGiayPhep,      
   @SoHopDong,      
   @NgayHopDong,      
   @NgayHetHanHopDong,      
   @SoHoaDonThuongMai,      
   @NgayHoaDonThuongMai,      
   @PTVT_ID,      
   @SoHieuPTVT,      
   @NgayDenPTVT,      
   @QuocTichPTVT_ID,      
   @LoaiVanDon,      
   @SoVanDon,      
   @NgayVanDon,      
   @NuocXK_ID,      
   @NuocNK_ID,      
   @DiaDiemXepHang,      
   @CuaKhau_ID,      
   @DKGH_ID,      
   @NguyenTe_ID,      
   @TyGiaTinhThue,      
   @TyGiaUSD,      
   @PTTT_ID,      
   @SoHang,      
   @SoLuongPLTK,      
   @TenChuHang,  
   @ChucVu,    
   @SoContainer20,      
   @SoContainer40,      
   @SoKien,      
   @TrongLuong,      
   @TongTriGiaKhaiBao,      
   @TongTriGiaTinhThue,      
   @LoaiToKhaiGiaCong,      
   @LePhiHaiQuan,      
   @PhiBaoHiem,      
   @PhiVanChuyen,      
   @PhiXepDoHang,      
   @PhiKhac,      
   @CanBoDangKy,      
   @QuanLyMay,      
   @TrangThaiXuLy,      
   @LoaiHangHoa,      
   @GiayTo,      
   @PhanLuong,      
   @MaDonViUT,      
   @TenDonViUT,      
   @TrongLuongNet,      
   @SoTienKhoan,      
   @IDHopDong,      
   @MaMid,      
   @Ngay_THN_THX,      
   @TrangThaiPhanBo,      
   @GUIDSTR,      
   @DeXuatKhac,      
   @LyDoSua,      
   @ActionStatus,      
   @GuidReference,      
   @NamDK,      
   @HUONGDAN   
  )        
 END   
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_Update]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_Cuc]
SET
	[Ten] = @Ten,
	[IPService] = @IPService,
	[ServicePathV1] = @ServicePathV1,
	[ServicePathV2] = @ServicePathV2,
	[ServicePathV3] = @ServicePathV3,
	[IPServiceCKS] = @IPServiceCKS,
	[ServicePathCKS] = @ServicePathCKS,
	[IsDefault] = @IsDefault,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_Cuc]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_Load]
	@ID nvarchar(2)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_Cuc]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_InsertUpdate]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_Cuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_Cuc] 
		SET
			[Ten] = @Ten,
			[IPService] = @IPService,
			[ServicePathV1] = @ServicePathV1,
			[ServicePathV2] = @ServicePathV2,
			[ServicePathV3] = @ServicePathV3,
			[IPServiceCKS] = @IPServiceCKS,
			[ServicePathCKS] = @ServicePathCKS,
			[IsDefault] = @IsDefault,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_Cuc]
	(
			[ID],
			[Ten],
			[IPService],
			[ServicePathV1],
			[ServicePathV2],
			[ServicePathV3],
			[IPServiceCKS],
			[ServicePathCKS],
			[IsDefault],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@IPService,
			@ServicePathV1,
			@ServicePathV2,
			@ServicePathV3,
			@IPServiceCKS,
			@ServicePathCKS,
			@IsDefault,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_Insert]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_Cuc]
(
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@IPService,
	@ServicePathV1,
	@ServicePathV2,
	@ServicePathV3,
	@IPServiceCKS,
	@ServicePathCKS,
	@IsDefault,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_Delete]
	@ID nvarchar(2)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_Cuc]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_LoadBySoTiepNhan]'
GO
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Load]      
-- Database: ECS_GCTQ      
-- Author: Ngo Thanh Tung      
-- Time created: Wednesday, January 06, 2010      
------------------------------------------------------------------------------------------------------------------------      
      
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_LoadBySoTiepNhan]      
 @MaHaiQuan char(6),        
 @SoTiepNhan int,        
 @MaLoaiHinh char(5),        
 @NgayTiepNhan datetime,        
 @MaDoanhNghiep varchar(14),  
 @IDHopDong bigint,  
 @SoHopDong nvarchar(50)    
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
SELECT      *      
FROM      
 [dbo].[t_KDT_ToKhaiMauDich]      
WHERE      
 SoTiepNhan = @SoTiepNhan AND NgayTiepNhan = @NgayTiepNhan AND MaLoaiHinh = @MaLoaiHinh AND MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep  AND IDHopDong = @IDHopDong AND SoHopDong = @SoHopDong
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
-- Database: ECS_GCTQ
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 06, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(255),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(50),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@ChucVu nvarchar(20), -- Chức vụ người đại diện doanh nghiệp
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@IDHopDong bigint,
	@MaMid varchar(100),
	@Ngay_THN_THX datetime,
	@TrangThaiPhanBo int,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDich] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
			[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHanHopDong] = @NgayHetHanHopDong,
			[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
			[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
			[PTVT_ID] = @PTVT_ID,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
			[LoaiVanDon] = @LoaiVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[NuocXK_ID] = @NuocXK_ID,
			[NuocNK_ID] = @NuocNK_ID,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[CuaKhau_ID] = @CuaKhau_ID,
			[DKGH_ID] = @DKGH_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[TyGiaUSD] = @TyGiaUSD,
			[PTTT_ID] = @PTTT_ID,
			[SoHang] = @SoHang,
			[SoLuongPLTK] = @SoLuongPLTK,
			[TenChuHang] = @TenChuHang,
			[ChucVu] = @ChucVu,
			[SoContainer20] = @SoContainer20,
			[SoContainer40] = @SoContainer40,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
			[LePhiHaiQuan] = @LePhiHaiQuan,
			[PhiBaoHiem] = @PhiBaoHiem,
			[PhiVanChuyen] = @PhiVanChuyen,
			[PhiXepDoHang] = @PhiXepDoHang,
			[PhiKhac] = @PhiKhac,
			[CanBoDangKy] = @CanBoDangKy,
			[QuanLyMay] = @QuanLyMay,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[LoaiHangHoa] = @LoaiHangHoa,
			[GiayTo] = @GiayTo,
			[PhanLuong] = @PhanLuong,
			[MaDonViUT] = @MaDonViUT,
			[TenDonViUT] = @TenDonViUT,
			[TrongLuongNet] = @TrongLuongNet,
			[SoTienKhoan] = @SoTienKhoan,
			[IDHopDong] = @IDHopDong,
			[MaMid] = @MaMid,
			[Ngay_THN_THX] = @Ngay_THN_THX,
			[TrangThaiPhanBo] = @TrangThaiPhanBo,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamDK] = @NamDK,
			[HUONGDAN] = @HUONGDAN
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaDaiLyTTHQ],
			[TenDaiLyTTHQ],
			[TenDonViDoiTac],
			[ChiTietDonViDoiTac],
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHanGiayPhep],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHanHopDong],
			[SoHoaDonThuongMai],
			[NgayHoaDonThuongMai],
			[PTVT_ID],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[QuocTichPTVT_ID],
			[LoaiVanDon],
			[SoVanDon],
			[NgayVanDon],
			[NuocXK_ID],
			[NuocNK_ID],
			[DiaDiemXepHang],
			[CuaKhau_ID],
			[DKGH_ID],
			[NguyenTe_ID],
			[TyGiaTinhThue],
			[TyGiaUSD],
			[PTTT_ID],
			[SoHang],
			[SoLuongPLTK],
			[TenChuHang],
			ChucVu,
			[SoContainer20],
			[SoContainer40],
			[SoKien],
			[TrongLuong],
			[TongTriGiaKhaiBao],
			[TongTriGiaTinhThue],
			[LoaiToKhaiGiaCong],
			[LePhiHaiQuan],
			[PhiBaoHiem],
			[PhiVanChuyen],
			[PhiXepDoHang],
			[PhiKhac],
			[CanBoDangKy],
			[QuanLyMay],
			[TrangThaiXuLy],
			[LoaiHangHoa],
			[GiayTo],
			[PhanLuong],
			[MaDonViUT],
			[TenDonViUT],
			[TrongLuongNet],
			[SoTienKhoan],
			[IDHopDong],
			[MaMid],
			[Ngay_THN_THX],
			[TrangThaiPhanBo],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamDK],
			[HUONGDAN]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaDaiLyTTHQ,
			@TenDaiLyTTHQ,
			@TenDonViDoiTac,
			@ChiTietDonViDoiTac,
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHanGiayPhep,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHanHopDong,
			@SoHoaDonThuongMai,
			@NgayHoaDonThuongMai,
			@PTVT_ID,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@QuocTichPTVT_ID,
			@LoaiVanDon,
			@SoVanDon,
			@NgayVanDon,
			@NuocXK_ID,
			@NuocNK_ID,
			@DiaDiemXepHang,
			@CuaKhau_ID,
			@DKGH_ID,
			@NguyenTe_ID,
			@TyGiaTinhThue,
			@TyGiaUSD,
			@PTTT_ID,
			@SoHang,
			@SoLuongPLTK,
			@TenChuHang,
			@ChucVu,
			@SoContainer20,
			@SoContainer40,
			@SoKien,
			@TrongLuong,
			@TongTriGiaKhaiBao,
			@TongTriGiaTinhThue,
			@LoaiToKhaiGiaCong,
			@LePhiHaiQuan,
			@PhiBaoHiem,
			@PhiVanChuyen,
			@PhiXepDoHang,
			@PhiKhac,
			@CanBoDangKy,
			@QuanLyMay,
			@TrangThaiXuLy,
			@LoaiHangHoa,
			@GiayTo,
			@PhanLuong,
			@MaDonViUT,
			@TenDonViUT,
			@TrongLuongNet,
			@SoTienKhoan,
			@IDHopDong,
			@MaMid,
			@Ngay_THN_THX,
			@TrangThaiPhanBo,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamDK,
			@HUONGDAN
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViTinh_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViTinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_DonViTinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViTinh_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViTinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DonViTinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_DonViHaiQuan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_Cuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_Cuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[MaCuc],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_CuaKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_CuaKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GhiChu],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_PhuongThucThanhToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_PhuongThucThanhToan_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_PhuongThucThanhToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_Nuoc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_Nuoc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[TyGiaTinhThue],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_NguyenTe] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_NguyenTe] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_DieuKienGiaoHang] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DieuKienGiaoHang] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_SelectDynamic]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_MaHS] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_DeleteDynamic]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_MaHS] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_DeleteDynamic]
-- Database: ECS_TQDT_gc_v3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectDynamic]
-- Database: ECS_TQDT_gc_v3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_PhuongThucVanTai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_PhuongThucVanTai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ADD CONSTRAINT [IX_t_KDT_HangMauDich] UNIQUE NONCLUSTERED  ([TKMD_ID], [MaPhu], [DVT_ID], [MaHS], [TenHang], [SoLuong])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ADD
CONSTRAINT [FK_t_HangMauDich_t_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version WHERE [Version] ='4.1') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.1', getdate(), null)
	end 
