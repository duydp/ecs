﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.Report.WareHouse;

namespace Company.Interface.GC
{
    public partial class PhieuXuatKho_ThongKeTon : BaseForm
    {
        public long IDPhieuXuat = 0;
        public string MaPO = "";
        public long IDHopDong = 0;
        public HopDong hd = new HopDong();
        public PhieuXuatKho_ThongKeTon()
        {
            InitializeComponent();
            this.Load += new EventHandler(PhieuXuatKho_ThongKeTon_Load);
            cbHopDong.ValueChanged += new EventHandler(cbHopDong_ValueChanged);
        }

        void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            BindXuatNhapTon();
        }

        void PhieuXuatKho_ThongKeTon_Load(object sender, EventArgs e)
        {
            BindHopDong();
            cbbLoaiHangHoa.SelectedIndex = 0;
            //if (IDHopDong > 0)
            //    //cbHopDong.Value = HopDong.Load(IDHopDong);
            //    cbHopDong.SelectedItem = HopDong.Load(IDHopDong);
        }
        private void BindHopDong()
        {
            if (string.IsNullOrEmpty(hd.SoHopDong))
            {
                hd.SoHopDong = "";
                hd.ID = 0;
                List<HopDong> collection;
                {
                    string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                }
                collection.Add(hd);
                cbHopDong.DataSource = collection;
                cbHopDong.DisplayMember = "SoHopDong";
                cbHopDong.ValueMember = "ID";
                if (IDHopDong == 0)
                    cbHopDong.SelectedIndex = collection.Count - 1;
                else
                {
                    cbHopDong.SelectedIndex = collection.FindIndex(HD => HD.ID == IDHopDong);
                }
            }
            else
            {
                //hd.SoHopDong = "";
                //hd.ID = 0;
                List<HopDong> collection;
                {
                    string where = string.Format(" MaDoanhNghiep='{0}' AND SoHopDong = '{1}'", GlobalSettings.MA_DON_VI,hd.SoHopDong);
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                }
                collection.Add(hd);
                cbHopDong.DataSource = collection;
                cbHopDong.DisplayMember = "SoHopDong";
                cbHopDong.ValueMember = "ID";
                if (IDHopDong == 0)
                    cbHopDong.SelectedIndex = collection.Count - 1;
                else
                {
                    cbHopDong.SelectedIndex = collection.FindIndex(HD => HD.ID == IDHopDong);
                }
            }
            long idHD = System.Convert.ToInt64(cbHopDong.Value);
            dgList.DataSource = KDT_GC_PhieuXuatKho.LoadXuatNhapTon(idHD, IDPhieuXuat, MaPO);
        }
        private void BindXuatNhapTon()
        {

            try
            {
                long idHD = System.Convert.ToInt64(cbHopDong.Value);
                dgList.DataSource = KDT_GC_PhieuXuatKho.LoadXuatNhapTon(idHD, IDPhieuXuat,MaPO);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

        private void btnPrintReport_Click(object sender, EventArgs e)
        {
            try
            {
                long HopDong_ID = System.Convert.ToInt64(cbHopDong.Value);
                string SoHopDong = HopDong.GetNameHDRent(HopDong_ID);
                DateTime DateFrom = new DateTime(clcTuNgay.Value.Year,clcTuNgay.Value.Month,clcTuNgay.Value.Day,00,00,00);
                DateTime DateTo = new DateTime(clcDenNgay.Value.Year,clcDenNgay.Value.Month,clcDenNgay.Value.Day,23,59,59);
                if (cbbLoaiHangHoa.SelectedValue == "1")
                {
                    ReportViewBCXNT_Kho report = new ReportViewBCXNT_Kho();
                    report.BindReport(SoHopDong,DateFrom,DateTo);
                    report.ShowPreview();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
    }
}
