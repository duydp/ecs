﻿/*
Run this script on:

        192.168.72.100.ECS_TQDT_KD_V3    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_DANHMUC

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 09/26/2012 3:15:19 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_HaiQuan_LoaiHinhMauDich]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiHinhMauDich] ADD
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION

if( (select count(*) from dbo.t_HaiQuan_Version where [Version] ='4.4') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.4', getdate(), N'Cập nhật mới cấu trúc bảng mã loại hình')
	end 
	
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
