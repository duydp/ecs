
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_NPLNhapTonThucTe_SelectDynamic]  
-- Database: ECS_GIACONG  
-- Author: Ngo Thanh Tung  
-- Time created: Wednesday, January 14, 2009  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_GC_NPLNhapTonThucTe_SelectDynamic]  
 @WhereCondition nvarchar(500),  
 @OrderByExpression nvarchar(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(3250)  
  
SET @SQL = 'SELECT  a.*, b.TenHang as TenNPL,b.MaHS,b.DVT_ID, b.TriGiaKB, a.Ton * b.TriGiaKB / a.Luong  as TonTriGiaKB, b.NuocXX_ID as XuatXu  
 FROM (SELECT * FROM [dbo].[t_GC_NPLNhapTonThucTe] WHERE MaLoaiHinh LIKE ''N%'')a  
 INNER JOIN v_GC_HangToKhai b ON a.SoToKhai = b.SoToKhai AND a.NgayDangKy = b.NgayDangKY AND a.MaLoaiHinh = b.MaLoaiHinh AND a.MaNPL = b.MaPhu  
  
 WHERE ' + @WhereCondition +   
 'UNION   
 SELECT  a.*, b.TenHang as TenNPL,b.MaHS,b.ID_DVT as DVT_ID, b.TriGia, a.Ton * b.TriGia / a.Luong  as TonTriGiaKB, b.ID_NuocXX as XuatXu  
 FROM (SELECT * FROM [dbo].[t_GC_NPLNhapTonThucTe] WHERE MaLoaiHinh LIKE ''PH%'' OR MaLoaiHinh LIKE ''N%'')a  
 INNER JOIN v_GC_HangChuyenTiep b ON a.SoToKhai = b.SoToKhai AND a.NgayDangKy = b.NgayDangKY AND a.MaLoaiHinh = b.MaLoaiHinh AND a.MaNPL = b.MaHang  
 WHERE ' + @WhereCondition   
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO

UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '7.0'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO	
