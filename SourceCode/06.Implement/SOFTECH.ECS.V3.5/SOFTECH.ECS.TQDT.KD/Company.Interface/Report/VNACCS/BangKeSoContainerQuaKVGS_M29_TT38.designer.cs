namespace Company.Interface.Report.VNACCS
{
    partial class BangKeSoContainerQuaKVGS_M29_TT38
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuKienCont = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoSeal = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoSealHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXacNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVach = new DevExpress.XtraReports.UI.XRTableCell();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.txtBarCode = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayThangNam = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNguoiKhaiHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCucHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHQGiamSat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrangThaiTK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiKhaiHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 123.4167F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(12.45846F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(717F, 70F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.lblSoHieuKienCont,
            this.lblSoSeal,
            this.lblSoSealHQ,
            this.lblXacNhan,
            this.lblMaVach});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 5.1692293585605666;
            // 
            // lblSTT
            // 
            this.lblSTT.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 5, 0, 100F);
            this.lblSTT.StylePriority.UseFont = false;
            this.lblSTT.StylePriority.UsePadding = false;
            this.lblSTT.StylePriority.UseTextAlignment = false;
            this.lblSTT.Text = "1";
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSTT.Weight = 0.19428556217522786;
            this.lblSTT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblSTT_BeforePrint);
            // 
            // lblSoHieuKienCont
            // 
            this.lblSoHieuKienCont.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHieuKienCont.Name = "lblSoHieuKienCont";
            this.lblSoHieuKienCont.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 0, 100F);
            this.lblSoHieuKienCont.StylePriority.UseFont = false;
            this.lblSoHieuKienCont.StylePriority.UsePadding = false;
            this.lblSoHieuKienCont.StylePriority.UseTextAlignment = false;
            this.lblSoHieuKienCont.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoHieuKienCont.Weight = 0.89485771908393308;
            // 
            // lblSoSeal
            // 
            this.lblSoSeal.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoSeal.Name = "lblSoSeal";
            this.lblSoSeal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 0, 100F);
            this.lblSoSeal.StylePriority.UseFont = false;
            this.lblSoSeal.StylePriority.UsePadding = false;
            this.lblSoSeal.StylePriority.UseTextAlignment = false;
            this.lblSoSeal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoSeal.Weight = 0.6465709426080708;
            // 
            // lblSoSealHQ
            // 
            this.lblSoSealHQ.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoSealHQ.Name = "lblSoSealHQ";
            this.lblSoSealHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 0, 100F);
            this.lblSoSealHQ.StylePriority.UseFont = false;
            this.lblSoSealHQ.StylePriority.UsePadding = false;
            this.lblSoSealHQ.StylePriority.UseTextAlignment = false;
            this.lblSoSealHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoSealHQ.Weight = 0.57285807722966386;
            // 
            // lblXacNhan
            // 
            this.lblXacNhan.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXacNhan.Name = "lblXacNhan";
            this.lblXacNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 0, 100F);
            this.lblXacNhan.StylePriority.UseFont = false;
            this.lblXacNhan.StylePriority.UsePadding = false;
            this.lblXacNhan.Weight = 0.75805651041146027;
            // 
            // lblMaVach
            // 
            this.lblMaVach.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.winControlContainer1});
            this.lblMaVach.Multiline = true;
            this.lblMaVach.Name = "lblMaVach";
            this.lblMaVach.StylePriority.UseTextAlignment = false;
            this.lblMaVach.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaVach.Weight = 0.86662847302766077;
            this.lblMaVach.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblMaVach_BeforePrint);
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(4.020691F, 10.00001F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.winControlContainer1.WinControl = this.pictureBox1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 21);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 13F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 105F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15,
            this.xrLabel12,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel2});
            this.GroupFooter1.HeightF = 278.125F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(12.45847F, 184.1667F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(644.3769F, 48.12502F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "- Cột số (2): Đối với hàng nhập khẩu: lấy từ Danh sách container do người khai hả" +
                "i quan gửi đến hệ thống.";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(29.54361F, 132.7085F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(627.2918F, 48.12502F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Trường hợp có sự thay đổi số container đã khai báo, căn cứ chứng từ do người khai" +
                " hải quan nộp, xuất trình, công chức hải quan cập nhật số container vào Hệ thống" +
                " để in lại danh sách container";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(30.00002F, 106.4584F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(627.2919F, 26.25008F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "+ Đối với hàng xuất khẩu: lấy từ tiêu chí “Số container” trên tờ khai xuất.";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(30F, 58.33333F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(627.2918F, 48.12502F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "+ Đối với hàng nhập khẩu: lấy từ Danh sách container do người khai hải quan gửi đ" +
                "ến hệ thống.";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(12.45847F, 34.16664F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(98.6042F, 24.16669F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "- Cột số (1):";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 9.999974F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(114.7899F, 24.16666F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "GHI CHÚ:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6,
            this.lblLoaiHinh,
            this.txtBarCode,
            this.xrLabel10,
            this.lblNgayThangNam,
            this.xrLabel11,
            this.lblTenNguoiKhaiHQ,
            this.xrLabel3,
            this.lblCucHQ,
            this.lblChiCucHQ,
            this.lblChiCucHQGiamSat,
            this.xrLabel1,
            this.lblLuong,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel16,
            this.xrTable1,
            this.lblTrangThaiTK,
            this.lblNgayKhai,
            this.lblSoToKhai,
            this.lblMaNguoiKhaiHQ,
            this.xrLabel14,
            this.xrLabel13});
            this.GroupHeader1.HeightF = 483.625F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(369.0379F, 290F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(104.6673F, 21F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "7. Loại hình:";
            // 
            // lblLoaiHinh
            // 
            this.lblLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaiHinh.LocationFloat = new DevExpress.Utils.PointFloat(473.7052F, 290F);
            this.lblLoaiHinh.Name = "lblLoaiHinh";
            this.lblLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLoaiHinh.SizeF = new System.Drawing.SizeF(161.9619F, 21F);
            this.lblLoaiHinh.StylePriority.UseFont = false;
            // 
            // txtBarCode
            // 
            this.txtBarCode.AutoModule = true;
            this.txtBarCode.LocationFloat = new DevExpress.Utils.PointFloat(571.4793F, 32F);
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
            this.txtBarCode.ShowText = false;
            this.txtBarCode.SizeF = new System.Drawing.SizeF(144.3326F, 26.45834F);
            code128Generator1.CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetB;
            this.txtBarCode.Symbology = code128Generator1;
            this.txtBarCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txtBarCode.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.txtBarCode_BeforePrint);
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(87.5F, 130F);
            this.xrLabel10.Multiline = true;
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(569.7918F, 45.83333F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "DANH SÁCH CONTAINER \r\nĐỦ ĐIỀU KIỆN QUA KHU VỰC GIÁM SÁT HẢI QUAN\r\n";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayThangNam
            // 
            this.lblNgayThangNam.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayThangNam.LocationFloat = new DevExpress.Utils.PointFloat(391.8119F, 64.25002F);
            this.lblNgayThangNam.Name = "lblNgayThangNam";
            this.lblNgayThangNam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayThangNam.SizeF = new System.Drawing.SizeF(324F, 24.25002F);
            this.lblNgayThangNam.StylePriority.UseFont = false;
            this.lblNgayThangNam.StylePriority.UseTextAlignment = false;
            this.lblNgayThangNam.Text = "………, ngày……tháng……. năm 20…";
            this.lblNgayThangNam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(30F, 230F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(141.4967F, 21F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "2. Đơn vị XNK:";
            // 
            // lblTenNguoiKhaiHQ
            // 
            this.lblTenNguoiKhaiHQ.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.lblTenNguoiKhaiHQ.LocationFloat = new DevExpress.Utils.PointFloat(154.3317F, 230F);
            this.lblTenNguoiKhaiHQ.Name = "lblTenNguoiKhaiHQ";
            this.lblTenNguoiKhaiHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNguoiKhaiHQ.SizeF = new System.Drawing.SizeF(450.4169F, 21F);
            this.lblTenNguoiKhaiHQ.StylePriority.UseFont = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(512.5F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(207.1171F, 24.25002F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Mẫu số 29/DSCT/GSQL";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblCucHQ
            // 
            this.lblCucHQ.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCucHQ.LocationFloat = new DevExpress.Utils.PointFloat(9.99999F, 39.99999F);
            this.lblCucHQ.Name = "lblCucHQ";
            this.lblCucHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHQ.SizeF = new System.Drawing.SizeF(339.3319F, 20F);
            this.lblCucHQ.StylePriority.UseFont = false;
            this.lblCucHQ.StylePriority.UseTextAlignment = false;
            this.lblCucHQ.Text = "CỤC HẢI QUAN TỈNH/ TP";
            this.lblCucHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblChiCucHQ
            // 
            this.lblChiCucHQ.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChiCucHQ.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 64.25002F);
            this.lblChiCucHQ.Name = "lblChiCucHQ";
            this.lblChiCucHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQ.SizeF = new System.Drawing.SizeF(339.3319F, 20F);
            this.lblChiCucHQ.StylePriority.UseFont = false;
            this.lblChiCucHQ.StylePriority.UseTextAlignment = false;
            this.lblChiCucHQ.Text = "CHI CỤC HẢI QUAN";
            this.lblChiCucHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblChiCucHQGiamSat
            // 
            this.lblChiCucHQGiamSat.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.lblChiCucHQGiamSat.LocationFloat = new DevExpress.Utils.PointFloat(263.3332F, 200F);
            this.lblChiCucHQGiamSat.Name = "lblChiCucHQGiamSat";
            this.lblChiCucHQGiamSat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQGiamSat.SizeF = new System.Drawing.SizeF(417.0836F, 21F);
            this.lblChiCucHQGiamSat.StylePriority.UseFont = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(29.54361F, 200F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(233.7896F, 21F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "1. Chi cục hải quan giám sát:";
            // 
            // lblLuong
            // 
            this.lblLuong.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLuong.LocationFloat = new DevExpress.Utils.PointFloat(452.8718F, 320F);
            this.lblLuong.Name = "lblLuong";
            this.lblLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuong.SizeF = new System.Drawing.SizeF(169.0418F, 21F);
            this.lblLuong.StylePriority.UseFont = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(369.0379F, 320F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(83.83392F, 21F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "8. Luồng:";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(29.54361F, 260F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(124.7881F, 21F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "3. Mã số thuế:";
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(369.038F, 260F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(133.0454F, 21F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "6. Ngày tờ khai:";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(12.45846F, 388.4167F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(717F, 95.20834F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell6,
            this.xrTableCell9,
            this.xrTableCell2,
            this.xrTableCell10,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1.7805203940783334;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "STT";
            this.xrTableCell1.Weight = 0.19451361743868381;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.Text = "SỐ HIỆU CONTAINER (1)";
            this.xrTableCell6.Weight = 0.89485691320580141;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "SỐ SEAL \r\nCONTAINER\r\n(nếu có)\r\n(2)";
            this.xrTableCell9.Weight = 0.646571615388451;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.Text = "SỐ SEAL HẢI \r\nQUAN\r\n(nếu có)\r\n(3)";
            this.xrTableCell2.Weight = 0.572858119082385;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "XÁC NHẬN CỦA BỘ PHẬN GIÁM SÁT HẢI QUAN\r\n(4)\r\n";
            this.xrTableCell10.Weight = 0.75805617704926809;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "MÃ VẠCH\r\n(5)\r\n";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.86640088423137951;
            // 
            // lblTrangThaiTK
            // 
            this.lblTrangThaiTK.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiTK.LocationFloat = new DevExpress.Utils.PointFloat(211.0002F, 320F);
            this.lblTrangThaiTK.Name = "lblTrangThaiTK";
            this.lblTrangThaiTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrangThaiTK.SizeF = new System.Drawing.SizeF(140.3647F, 21F);
            this.lblTrangThaiTK.StylePriority.UseFont = false;
            // 
            // lblNgayKhai
            // 
            this.lblNgayKhai.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayKhai.LocationFloat = new DevExpress.Utils.PointFloat(502.0834F, 260F);
            this.lblNgayKhai.Name = "lblNgayKhai";
            this.lblNgayKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayKhai.SizeF = new System.Drawing.SizeF(161.9619F, 21F);
            this.lblNgayKhai.StylePriority.UseFont = false;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhai.LocationFloat = new DevExpress.Utils.PointFloat(143.08F, 290F);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.SizeF = new System.Drawing.SizeF(154.4652F, 21F);
            this.lblSoToKhai.StylePriority.UseFont = false;
            // 
            // lblMaNguoiKhaiHQ
            // 
            this.lblMaNguoiKhaiHQ.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiKhaiHQ.LocationFloat = new DevExpress.Utils.PointFloat(154.3317F, 260F);
            this.lblMaNguoiKhaiHQ.Name = "lblMaNguoiKhaiHQ";
            this.lblMaNguoiKhaiHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNguoiKhaiHQ.SizeF = new System.Drawing.SizeF(137.9169F, 21F);
            this.lblMaNguoiKhaiHQ.StylePriority.UseFont = false;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(29.54361F, 320F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(181.4566F, 21F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "5. Trạng thái tờ khai: ";
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(30F, 290F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(113.08F, 21F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "4. Số tờ khai:";
            // 
            // BangKeSoContainerQuaKVGS_M29_TT38
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.GroupFooter1,
            this.GroupHeader1});
            this.Margins = new System.Drawing.Printing.Margins(54, 49, 13, 105);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell lblSoSealHQ;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVach;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuKienCont;
        private DevExpress.XtraReports.UI.XRTableCell lblSoSeal;
        private DevExpress.XtraReports.UI.XRTableCell lblXacNhan;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblNgayThangNam;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lblTenNguoiKhaiHQ;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblCucHQ;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQ;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQGiamSat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblLuong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel lblTrangThaiTK;
        private DevExpress.XtraReports.UI.XRLabel lblNgayKhai;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiKhaiHQ;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRBarCode txtBarCode;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lblLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
    }
}
