﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Chức năng khai báo
    /// </summary>
    [Serializable]
    [XmlRoot("Declaration")]
    public class SubjectBase : IssueBase
    {
        /// <summary>
        /// Loại chứng từ
        /// 929	Tờ khai nhập khẩu thương mại ( KD )
        /// 930	Tờ khai xuất khẩu thương mại ( KD ),         
        /// 100 Nguyên phụ liệu SXXK
        /// 101 Sản phẩm SXXK
        /// 103 Định mức SXXK
        /// 931	Tờ khai nhập khẩu SXXK
        /// 932	Tờ khai xuất khẩu SXXK, 
        /// 601	Hợp đồng Gia công
        /// 602	Phụ kiện hợp đồng Gia công
        /// 603	Định mức sản phẩm Gia công
        /// 935	Tờ khai nhập khẩu Gia công
        /// 936	Tờ khai xuất khẩu Gia công
        /// 939	Tờ khai điện tử đơn giản
        /// 940	Tờ khai điện tử tháng
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        

        /// <summary>
        /// 29: Số TN
        /// 30: SỐ TK
        /// 31: Phần Luồng
        /// </summary>
        [XmlElement("declarationStatus")]
        public string DeclarationStatus { get; set; }

        /// <summary>
        /// Tình trạng chứng từ
        /// 1: 
        /// 2: 
        /// 3: 
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }
        
        /// <summary>
        /// Số tờ khai
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference{ get; set; }

        /// <summary>
        /// Ngày Tờ Khai
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance{ get; set; }
        /// <summary>
        /// Mã Loại Hình
        /// </summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }
    }
}
