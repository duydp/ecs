using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;
using GemBox.Spreadsheet;
namespace Company.Interface.KDT.SXXK
{
    public partial class ImportKDTDMForm : BaseForm
    {
        public DinhMucDangKy dmDangKy;// = new DinhMucDangKy(); 
        int min = 221;//max = 346;                                    
        public ImportKDTDMForm()
        {
            InitializeComponent();
            this.Height = min;

        }

        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        //private int checkDMExit1(string maSP, string maNPL)
        //{
        //    for (int i = 0; i < this.DMCollection.Count; i++)
        //    {
        //        if (this.DMCollection[i].MaSanPHam.ToUpper() == maSP.ToUpper() && this.DMCollection[i].MaNguyenPhuLieu.ToUpper() == maNPL.ToUpper()) return i;
        //    }
        //    return -1;

        //}

        private void ImportDMForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            txtMaSPColumn.Text = GlobalSettings.DinhMuc.MaSP;
            txtMaNPLColumn.Text = GlobalSettings.DinhMuc.MaNPL;
            txtDMSDColumn.Text = GlobalSettings.DinhMuc.DinhMucSuDung;
            txtTLHHColumn.Text = GlobalSettings.DinhMuc.TyLeHH;
            //openFileDialog1.InitialDirectory = Application.StartupPath;
            // this.dgList.DataSource = this.DMReadCollection;

            //HungTQ
            //chkTinhGop.Visible = true;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid) return;
                int beginRow = Convert.ToInt32(txtRow.Value) - 1;
                if (beginRow < 0)
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                    error.SetIconPadding(txtRow, 8);
                    return;

                }
                this.Cursor = Cursors.WaitCursor;
                uiButton1.Enabled = false;
                Workbook wb = new Workbook();
                Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text, true);
                }
                catch
                {
                    // ShowMessage("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", false);
                    MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);

                    this.uiButton1.Enabled = true;
                    this.Cursor = Cursors.Default;
                    return;
                }
                try
                {
                    ws = wb.Worksheets[txtSheet.Text];
                }
                catch
                {
                    //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                    MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                    this.uiButton1.Enabled = true;
                    this.Cursor = Cursors.Default;
                    return;
                }

                WorksheetRowCollection wsrc = ws.Rows;
                char maSPColumn = Convert.ToChar(txtMaSPColumn.Text);
                int maSPCol = ConvertCharToInt(maSPColumn);

                char maNPLColumn = Convert.ToChar(txtMaNPLColumn.Text);
                int maNPLCol = ConvertCharToInt(maNPLColumn);

                char DMSDColumn = Convert.ToChar(txtDMSDColumn.Text);
                int DMSDCol = ConvertCharToInt(DMSDColumn);

                char TLHHColumn = Convert.ToChar(txtTLHHColumn.Text);
                int TLHHCol = ConvertCharToInt(TLHHColumn);

                Company.BLL.KDT.SXXK.DinhMucCollection dmDuplicate = new Company.BLL.KDT.SXXK.DinhMucCollection();
                StringBuilder sbError = new StringBuilder();
                StringBuilder sbErrorDuplicate = new StringBuilder();
                bool hienThiThongBao = true;

                DinhMucCollection dmCollectionTMP = new DinhMucCollection();
                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            DinhMuc dinhMuc = new DinhMuc();
                            dinhMuc.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                            if (dinhMuc.MaSanPham.Trim() == "")
                            {

                                continue;
                            }

                            dinhMuc.MaNguyenPhuLieu = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                            if (dinhMuc.MaNguyenPhuLieu.Trim() == "")
                            {
                                continue;
                            }
                            dinhMuc.DinhMucSuDung = Math.Round(Convert.ToDecimal(wsr.Cells[DMSDCol].Value), GlobalSettings.SoThapPhan.DinhMuc);
                            if (dinhMuc.DinhMucSuDung <= 0)
                            {
                                if (hienThiThongBao)
                                    MLMessages("Định mức sử dụng tại dòng : " + (wsr.Index + 1) + "<=0.Hãy kiểm tra lại", "MSG_EXC06", "" + (wsr.Index + 1), false);
                                this.Cursor = Cursors.Default;

                                sbError.AppendLine("Định mức sử dụng tại dòng : " + (wsr.Index + 1) + "<=0.Hãy kiểm tra lại");

                                return;
                                //continue;
                            }

                            dinhMuc.TyLeHaoHut = Math.Round(Convert.ToDecimal(wsr.Cells[TLHHCol].Value), GlobalSettings.SoThapPhan.TLHH);
                            if (dinhMuc.TyLeHaoHut <= 0 && dinhMuc.TyLeHaoHut > 100)
                            {
                                if (hienThiThongBao)
                                    MLMessages("Tỷ lệ hao hụt tại dòng : " + (wsr.Index + 1) + "không hợp lệ.Hãy kiểm tra lại", "MSG_EXC06", "" + (wsr.Index + 1), false);
                                this.Cursor = Cursors.Default;

                                sbError.AppendLine("Tỷ lệ hao hụt tại dòng : " + (wsr.Index + 1) + "không hợp lệ.Hãy kiểm tra lại");

                                return;
                                //continue;
                            }
                            //dinhMuc.DVT_ID =DonViTinh_GetID(Convert.ToString(wsr.Cells[DVTCol].Value).Trim());

                            dinhMuc.IsFromVietNam = chIsFromVietnam.Checked;
                            Company.BLL.SXXK.NguyenPhuLieu NPLDuyet = new Company.BLL.SXXK.NguyenPhuLieu();
                            NPLDuyet.Ma = dinhMuc.MaNguyenPhuLieu;
                            NPLDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            NPLDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;

                            if (!NPLDuyet.Load())
                            {
                                // ShowMessage("Nguyên phụ liệu có mã  : " + dinhMuc.MaNguyenPhuLieu + " không có trong hệ thống.", false);

                                if (hienThiThongBao)
                                    MLMessages("Nguyên phụ liệu có mã  : " + dinhMuc.MaNguyenPhuLieu + " không có trong hệ thống.", "MSG_PUB05", "", false);
                                this.Cursor = Cursors.Default;

                                sbError.AppendLine("Nguyên phụ liệu có mã  : " + dinhMuc.MaNguyenPhuLieu + " không có trong hệ thống.");

                                return;
                                //continue;
                            }
                            Company.BLL.SXXK.SanPham SPDuyet = new Company.BLL.SXXK.SanPham();
                            SPDuyet.Ma = dinhMuc.MaSanPham;
                            SPDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            SPDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            if (!SPDuyet.Load())
                            {
                                //ShowMessage("Sản phẩm có mã  : " + dinhMuc.MaSanPham + " không có trong hệ thống.", false);

                                if (hienThiThongBao)
                                    MLMessages("Sản phẩm có mã   : " + dinhMuc.MaSanPham + " không có trong hệ thống.", "MSG_PUB05", "", false);
                                this.Cursor = Cursors.Default;

                                sbError.AppendLine("Sản phẩm có mã   : " + dinhMuc.MaSanPham + " không có trong hệ thống.");

                                return;
                                //continue;
                            }
                            long id = DinhMuc.GetIDDinhMucExit(dinhMuc.MaSanPham, dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                            if (id > 0)
                            {
                                // ShowMessage("Sản phẩm : "+dinhMuc.MaSanPham+" đã được khai báo trong danh sách định mức có ID= "+id, false);

                                MLMessages("Sản phẩm : " + dinhMuc.MaSanPham + " đã được khai báo trong danh sách định mức có ID= " + id, "MSG_DMC04", "", false);
                                //dmCollection.Clear();
                                this.Cursor = Cursors.Default;
                                break;
                            }
                            Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
                            ttdm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            ttdm.MaSanPham = dinhMuc.MaSanPham;
                            if (ttdm.Load())
                            {
                                //ShowMessage("Sản phẩm : " + dinhMuc.MaSanPham + " đã được khai báo định mức.", false);

                                MLMessages("Sản phẩm : " + dinhMuc.MaSanPham + " đã được khai báo định mức. ", "MSG_DMC04", "", false);
                                //dmCollection.Clear();
                                break;
                            }
                            dinhMuc.TenNPL = NPLDuyet.Ten;
                            bool exits = false;
                            if (chkOverwrite.Checked)
                            {
                                foreach (DinhMuc dm in dmDangKy.DMCollection)
                                {
                                    if (dinhMuc.MaSanPham.Trim().ToUpper() == dm.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == dm.MaNguyenPhuLieu.Trim().ToUpper())
                                    {
                                        dmDangKy.DMCollection.Remove(dm);
                                        //dm.Delete();
                                        dmDangKy.DMCollection.Add(dinhMuc);
                                        exits = true;

                                        break;
                                    }
                                }
                                if (!exits)
                                    dmDangKy.DMCollection.Add(dinhMuc);
                            }
                            else
                            {
                                foreach (DinhMuc dm in dmDangKy.DMCollection)
                                {
                                    if (dinhMuc.MaSanPham.Trim().ToUpper() == dm.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == dm.MaNguyenPhuLieu.Trim().ToUpper())
                                    {
                                        //ShowMessage("Định mức của sản phẩm : " + dinhMuc.MaSanPham + " và nguyên phụ liệu : " + dinhMuc.MaNguyenPhuLieu + " đã có trên lưới nên sẽ được bỏ qua.", false);

                                        if (hienThiThongBao)
                                            MLMessages("Định mức của sản phẩm : " + dinhMuc.MaSanPham + " và nguyên phụ liệu : " + dinhMuc.MaNguyenPhuLieu + " đã có trên lưới nên sẽ được bỏ qua.", "MSG_EXC05", "", false);
                                        exits = true;

                                        sbErrorDuplicate.AppendLine("Định mức của sản phẩm : " + dinhMuc.MaSanPham + " và nguyên phụ liệu : " + dinhMuc.MaNguyenPhuLieu + " đã có trên lưới nên sẽ được bỏ qua.");

                                        dmDuplicate.Add(dinhMuc);

                                        break;
                                    }
                                }
                                if (!exits)
                                    dmDangKy.DMCollection.Add(dinhMuc);
                            }

                        }
                        catch
                        {
                            // if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                            if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", Convert.ToString(wsr.Index + 1), true) != "Yes")
                            {

                                this.uiButton1.Enabled = true;
                                this.Cursor = Cursors.Default;
                                break;
                            }
                        }
                    }
                }

                if (chkTinhGop.Checked)
                {
                    foreach (DinhMuc dmDK in dmDangKy.DMCollection)
                    {
                        dmDK.DinhMucSuDung += TinhTongDinhMuc(SearchDM(dmDuplicate, dmDK.MaSanPham, dmDK.MaNguyenPhuLieu));
                    }
                }

                if (dmDangKy.DMCollection.Count > 0)
                {
                    // ShowMessage("Đã đọc xong dữ liệu", false); 
                    if (chkOverwrite.Checked == false)
                        MLMessages("Đã đọc xong dữ liệu." + "\n\n" + sbErrorDuplicate.ToString() + "\n" + sbError.ToString(), "MSG_EXC04", "", false);
                    else
                        MLMessages("Đã đọc xong dữ liệu.", "MSG_EXC04", "", false);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally {
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }

        private decimal TinhTongDinhMuc(DinhMucCollection dmCollection)
        {
            decimal tongDM = 0;

            foreach (DinhMuc item in dmCollection)
            {
                tongDM += item.DinhMucSuDung;
            }

            return tongDM;
        }

        private DinhMucCollection SearchDM(DinhMucCollection dmCollection, string maSP, string maNPL)
        {
            DinhMucCollection collection = new DinhMucCollection();

            foreach (DinhMuc dmObj in dmCollection)
            {
                if (dmObj.MaSanPham == maSP && dmObj.MaNguyenPhuLieu == maNPL)
                {
                    collection.Add(Clone(dmObj));
                }
            }

            return collection;
        }

        private DinhMuc Clone(DinhMuc dm)
        {
            DinhMuc dmClone = new DinhMuc();
            dmClone.DinhMucSuDung = dm.DinhMucSuDung;
            dmClone.DVT_ID = dm.DVT_ID;
            dmClone.GhiChu = dm.GhiChu;
            dmClone.ID = dm.ID;
            dmClone.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
            dmClone.MaSanPham = dm.MaSanPham;
            dmClone.Master_ID = dm.Master_ID;
            dmClone.STTHang = dm.STTHang;
            dmClone.TenNPL = dm.TenNPL;
            dmClone.TyLeHaoHut = dm.TyLeHaoHut;

            return dmClone;
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }

        private void btnMorong_Click(object sender, EventArgs e)
        {
            if (Height == 221)
            {
                Height = 346;
                btnMorong.ImageIndex = 4;
                btnMorong.Text = "Thu nhỏ";
            }
            else
            {
                Height = 221;
                btnMorong.ImageIndex = 5;
                btnMorong.Text = "Mở rộng";
            }
        }

        //private void chkOverwrite_CheckedChanged(object sender, EventArgs e)
        //{
        //    CheckBoxOverwriteChanged();
        //}

        //private void CheckBoxOverwriteChanged()
        //{
        //    chkOverwrite.CheckedChanged -= new EventHandler(chkOverwrite_CheckedChanged);
        //    chkTinhGop.CheckedChanged -= new EventHandler(chkTinhGop_CheckedChanged);

        //    if (chkTinhGop.Checked)
        //        chkTinhGop.Checked = !chkOverwrite.Checked;

        //    chkOverwrite.CheckedChanged += new EventHandler(chkOverwrite_CheckedChanged);
        //    chkTinhGop.CheckedChanged += new EventHandler(chkTinhGop_CheckedChanged);

        //}

        //private void CheckBoxTinhGopChanged()
        //{
        //    chkOverwrite.CheckedChanged -= new EventHandler(chkOverwrite_CheckedChanged);
        //    chkTinhGop.CheckedChanged -= new EventHandler(chkTinhGop_CheckedChanged);

        //    if (chkOverwrite.Checked)
        //        chkOverwrite.Checked = !chkTinhGop.Checked;

        //    chkOverwrite.CheckedChanged += new EventHandler(chkOverwrite_CheckedChanged);
        //    chkTinhGop.CheckedChanged += new EventHandler(chkTinhGop_CheckedChanged);

        //}

        //private void chkTinhGop_CheckedChanged(object sender, EventArgs e)
        //{
        //    CheckBoxTinhGopChanged();
        //}
    }
}