
----------------------------------------------BẢNG MÃ BIỂU THUẾ NHẬP KHẨU SỬ DỤNG VNACCS ---------------------------------------------------
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B01')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B01',N'Biểu thuế nhập khẩu ưu đãi')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi' WHERE TableID='A404' AND TaxCode=N'B01'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B02')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B02',N'Chương 98 (1) - Biểu thuế nhập khẩu ưu đãi ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Chương 98 (1) - Biểu thuế nhập khẩu ưu đãi ' WHERE TableID='A404' AND TaxCode=N'B02'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B03')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B03',N'Biểu thuế nhập khẩu thông thường (bằng 150% thuế suất MFN áp dụng cho các nước không có quan hệ tối huệ quốc đối với Việt Nam)')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu thông thường (bằng 150% thuế suất MFN áp dụng cho các nước không có quan hệ tối huệ quốc đối với Việt Nam)' WHERE TableID='A404' AND TaxCode=N'B03'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B04')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B04',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại hàng hóa ASEAN (ATIGA)')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại hàng hóa ASEAN (ATIGA)' WHERE TableID='A404' AND TaxCode=N'B04'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B05')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B05',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Mậu dịch Tự do ASEAN - Trung Quốc')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Mậu dịch Tự do ASEAN - Trung Quốc' WHERE TableID='A404' AND TaxCode=N'B05'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B06')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B06',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Mậu dịch Tự do ASEAN - Hàn Quốc')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Khu vực Mậu dịch Tự do ASEAN - Hàn Quốc' WHERE TableID='A404' AND TaxCode=N'B06'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B07')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B07',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Khu vực Thương mại tự do ASEAN - Úc - Niu Di lân')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Khu vực Thương mại tự do ASEAN - Úc - Niu Di lân' WHERE TableID='A404' AND TaxCode=N'B07'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B08')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B08',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại Hàng hoá ASEAN - Ấn Độ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại Hàng hoá ASEAN - Ấn Độ' WHERE TableID='A404' AND TaxCode=N'B08'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B09')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B09',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Đối tác kinh tế toàn diện ASEAN - Nhật Bản')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Đối tác kinh tế toàn diện ASEAN - Nhật Bản' WHERE TableID='A404' AND TaxCode=N'B09'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B10')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B10',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định đối tác kinh tế Việt Nam - Nhật Bản')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định đối tác kinh tế Việt Nam - Nhật Bản' WHERE TableID='A404' AND TaxCode=N'B10'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B11')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B11',N'Biểu thuế thuế nhập khẩu đối với các mặt hàng được áp dụng ưu đãi thuế suất thuế nhập khẩu Việt - Lào')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế thuế nhập khẩu đối với các mặt hàng được áp dụng ưu đãi thuế suất thuế nhập khẩu Việt - Lào' WHERE TableID='A404' AND TaxCode=N'B11'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B12')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B12',N'Biểu thuế thuế nhập khẩu đối với hàng hoá có xuất xứ Campuchia')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế thuế nhập khẩu đối với hàng hoá có xuất xứ Campuchia' WHERE TableID='A404' AND TaxCode=N'B12'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B13')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B13',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại tự do Việt Nam - Chi Lê')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại tự do Việt Nam - Chi Lê' WHERE TableID='A404' AND TaxCode=N'B13'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B14')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B14',N'Biểu thuế NK ngoài hạn ngạch ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế NK ngoài hạn ngạch ' WHERE TableID='A404' AND TaxCode=N'B14'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B15')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B15',N'Biểu thuế nhập khẩu tuyệt đối')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu tuyệt đối' WHERE TableID='A404' AND TaxCode=N'B15'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B16')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B16',N'Biểu thuế nhập khẩu hỗn hợp')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu hỗn hợp' WHERE TableID='A404' AND TaxCode=N'B16'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B17')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B17',N'Chương 98 (2) - Biểu thuế nhập khẩu ưu đãi ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Chương 98 (2) - Biểu thuế nhập khẩu ưu đãi ' WHERE TableID='A404' AND TaxCode=N'B17'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B30')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B30',N'Mã biểu thuế áp dụng cho đối tượng không chịu thuế nhập khẩu')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Mã biểu thuế áp dụng cho đối tượng không chịu thuế nhập khẩu' WHERE TableID='A404' AND TaxCode=N'B30'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_TaxClassificationCode WHERE TableID='A404' AND TaxCode=N'B18')
BEGIN
    INSERT INTO t_VNACC_Category_TaxClassificationCode(TableID,TaxCode,TaxName) VALUES('A404',N'B18',N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại tự do Việt Nam - Hàn Quốc')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_TaxClassificationCode SET TaxName=N'Biểu thuế nhập khẩu ưu đãi đặc biệt của Việt Nam để thực hiện Hiệp định Thương mại tự do Việt Nam - Hàn Quốc' WHERE TableID='A404' AND TaxCode=N'B18'
END
END

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '33.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('33.2',GETDATE(), N'BẢNG MÃ BIỂU THUẾ NHẬP KHẨU SỬ DỤNG VNACCS')
END