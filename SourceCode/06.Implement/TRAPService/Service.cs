﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KDTService
{
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Services.WebServiceBinding(ConformsTo = (System.Web.Services.WsiProfiles)1)]
    [System.Web.Services.WebService(Namespace = "http://cis.customs.gov.vn/")]
    public class SService
    {
        static SService()
        {
        }
        [System.Diagnostics.DebuggerNonUserCode]
        public SService()
        {
        }
        [System.Web.Services.WebMethod(Description = "Ti\u1EBFp nh\u1EADn Message c\u1EE7a doanh nghi\u1EC7p.")]
        public string Send(string MessageXML)
        {
            try
            {
                Helpers.SaveMessage(MessageXML, "Gửi-S");
                HQService.Service cis = new HQService.Service();
                cis.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                string ret = cis.Send(MessageXML);
                Helpers.SaveMessage(ret, "Phản hồi-S");
                return ret;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return string.Empty;

            }

        }
        [System.Web.Services.WebMethod(Description = "Ki\u1EC3m tra tr\u1EA1ng th\u00E1i Webservice ti\u1EBFp nh\u1EADn.")]
        public string TestWebservie()
        {
            return "SUCCESSFULL";
        }
    }
}
