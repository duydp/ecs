﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;

namespace DongBo
{
    public class GlobalSettings
    {
        public enum PhanHe
        {
            GiaCong = 0,
            SanXuatKhau = 1
        }
        public static bool UpdateToKhaiMauDich;

        public static string MaHaiQuan;
        public static string MaDoanhNghiep;
        public static string TenTruyCap;
        public static string MatKhau = "";
        public static string TenHaiQuan;
        public static string PhienBan = "KTX";
        public static bool IsSynFromHQ = true;
        public static bool LayThongTinDaDuyet = true;
        public static bool SoSanhDuLieu = true;
        public static bool LayThongTinToKhaiSua = false;

        public static string DiaChiWSDongBo = "";
        //Source
        public static string DiaChiSQLSource = @".\sqlexpress";
        public static string UserSQLSource = "sa";
        public static string PassSQLSource = "";
        public static string DBSource = "ECS_TQ_SXXK_Source";
        //Target
        public static string DiaChiSQLTarget = @".\sqlexpress";
        public static string UserSQLTarget = "sa";
        public static string PassSQLTarget = "";
        public static string DBTarget = "ECS_TQ_SXXK_Target";

        public static string ConnectionStringSource = "";
        public static string ConnectionStringTarget = "";

        public static List<string> ListTableNameSource = new List<string>();
        public static List<string> ListTableNameTarget = new List<string>();
        public static int IndexTableNameSource = 0;
        public static int IndexTableNameTarget = 0;

        public static int Phanhe = 0;
        public static DateTime NgayBatDau = new DateTime(2007, 1, 1);
        public static DateTime NgayKetThuc = DateTime.Now;

        public static bool GhiDe = false;
        public static DataSet[] dsDNNew = new DataSet[] { null, null, null, null, null };
        public static DataSet[] dsHQ = new DataSet[] { null, null, null, null, null };
        public static DataSet dsTKMDHQ = new DataSet();
        /**
         * 0. NPL
         * 1. SP
         * 2. DM
         * 3. TK
         * 4. HMD
         * */
        public static bool IsGetNPL = true;
        public static bool IsGetSP = true;
        public static bool IsGetDM = true;
        public static bool IsGetTK = true;
        public static bool radSynFromHQ = true;
        public static bool radSynLocal = false;

        public static bool IsChiLayDuLieuDaDuyet = true;
        public static bool IsSoSanhDuLieu = true;
        public static bool IsChiLayToKhaiSuaBoSung = false;

        #region SAVE NODE XML

        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXmlAppSettings(string key, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string groupSettingName = "appSettings";

                SaveNodeXml(config, doc, groupSettingName, key, value);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXmlAppSettings(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config.FilePath);
            string groupSettingName = "appSettings";

            return ReadNodeXml(config, doc, groupSettingName, key);
        }

        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
                xmlDocument.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key)
        {
            string result = "";

            try
            {
                result = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key + ", gia tri = " + result, ex); }

            return result;
        }

        public static void SaveNodeXmlSetting(string name, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string s = "//setting[@name='" + name + "']";
                doc.SelectSingleNode(s).SelectSingleNode("value").ChildNodes[0].Value = value.ToString();
                doc.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static void SaveNodeXmlConnectionStringSource(string connectionString)
        {
            try
            {
                //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                //doc.Load(config.FilePath);
                //doc.SelectSingleNode("//connectionStrings").ChildNodes[0].Attributes[1].Value = connectionString;
                //doc.Save(config.FilePath);

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static void SaveNodeXmlConnectionStringTarget(string key, string connectionString)
        {
            try
            {
                //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                //doc.Load(config.FilePath);
                //doc.SelectSingleNode("//connectionStrings").ChildNodes[1].Attributes[1].Value = connectionString;
                //doc.Save(config.FilePath);

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings[key].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static sConnectionString ReadNodeXmlConnectionStrings()
        {
            sConnectionString con = new sConnectionString();

            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;

                ConnectionStringSource = connectionString;

                string[] arr = connectionString.Split(new char[] { ';' });

                con.Server = arr[0].Split(new char[] { '=' })[1];
                con.Database = arr[1].Split(new char[] { '=' })[1];
                con.User = arr[2].Split(new char[] { '=' })[1];
                con.Pass = arr[3].Split(new char[] { '=' })[1];
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return con;
        }

        public static sConnectionString ReadNodeXmlConnectionStrings(string key)
        {
            sConnectionString con = new sConnectionString();

            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings[key].ConnectionString;

                ConnectionStringTarget = connectionString;

                string[] arr = connectionString.Split(new char[] { ';' });

                con.Server = arr[0].Split(new char[] { '=' })[1];
                con.Database = arr[1].Split(new char[] { '=' })[1];
                con.User = arr[2].Split(new char[] { '=' })[1];
                con.Pass = arr[3].Split(new char[] { '=' })[1];

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return con;
        }

        public struct sConnectionString
        {
            public string Server;
            public string Database;
            public string User;
            public string Pass;
        }

        public static System.Drawing.Printing.Margins GetMargin(string margins)
        {
            string[] arr = margins.Split(new char[] { ',' });

            return new System.Drawing.Printing.Margins(
                int.Parse(arr[0].Trim()),
                int.Parse(arr[1].Trim()),
                int.Parse(arr[2].Trim()),
                int.Parse(arr[3].Trim()));
        }
        #endregion

        public static string RefreshKey()
        {
            try
            {
                LayThongTinToKhaiSua = Convert.ToBoolean(ReadNodeXmlAppSettings("LayThongTinToKhaiSua"));
                SoSanhDuLieu = Convert.ToBoolean(ReadNodeXmlAppSettings("SoSanhDuLieu"));
                LayThongTinDaDuyet = Convert.ToBoolean(ReadNodeXmlAppSettings("LayThongTinDaDuyet"));
                IsSynFromHQ = Convert.ToBoolean(ReadNodeXmlAppSettings("IsSynFromHQ"));
                DiaChiWSDongBo = ReadNodeXmlAppSettings("DiaChiWSDongBo");
                MaHaiQuan = ReadNodeXmlAppSettings("MaHaiQuan");
                MaDoanhNghiep = ReadNodeXmlAppSettings("MaDoanhNghiep");
                TenTruyCap = ReadNodeXmlAppSettings("TenTruyCap");
                MatKhau = ReadNodeXmlAppSettings("MatKhau");
                PhienBan = ReadNodeXmlAppSettings("PhienBan");
                Phanhe = ReadNodeXmlAppSettings("PhanHe") == "GC" ? 0 : 1;

                try
                {
                    NgayBatDau = Convert.ToDateTime(ReadNodeXmlAppSettings("NgayBatDau"), new System.Globalization.CultureInfo("en-US").DateTimeFormat);
                }
                catch { NgayBatDau = DateTime.Now.Date; }

                try
                {
                    NgayKetThuc = ReadNodeXmlAppSettings("NgayKetThuc") != "" ? Convert.ToDateTime(ReadNodeXmlAppSettings("NgayKetThuc"), new System.Globalization.CultureInfo("en-US").DateTimeFormat) : DateTime.Today;
                }
                catch { NgayBatDau = DateTime.Now.Date; }

                sConnectionString con = ReadNodeXmlConnectionStrings();
                DiaChiSQLSource = con.Server;
                DBSource = con.Database;
                UserSQLSource = con.User;
                PassSQLSource = con.Pass;

                sConnectionString con2 = ReadNodeXmlConnectionStrings("MSSQLTarget");
                DiaChiSQLTarget = con2.Server;
                DBTarget = con2.Database;
                UserSQLTarget = con2.User;
                PassSQLTarget = con2.Pass;

                return "";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return ex.Message; }
        }

        public static void saveConfigHQ(string DiaChiWSDongBo, string MaHaiQuan, string MaDoanhNghiep, string TenTruyCap, string MatKhau, string phienBan, bool isSynFromHQ)
        {
            SaveNodeXmlAppSettings("DiaChiWSDongBo", DiaChiWSDongBo);
            SaveNodeXmlAppSettings("MaHaiQuan", MaHaiQuan);
            SaveNodeXmlAppSettings("MaDoanhNghiep", MaDoanhNghiep);
            SaveNodeXmlAppSettings("TenTruyCap", TenTruyCap);
            SaveNodeXmlAppSettings("MatKhau", MatKhau);
            SaveNodeXmlAppSettings("PhienBan", phienBan);
            SaveNodeXmlAppSettings("IsSynFromHQ", isSynFromHQ);

            RefreshKey();
        }

        public static void saveConfigDN(string DiaChiSQLSource, string DBSource, string UserSQLSource, string PassSQLSource,
            string DiaChiSQLTarget, string DBTarget, string UserSQLTarget, string PassSQLTarget, int phanHe, DateTime ngayBatDau, DateTime ngayKetThuc,
            bool layTKDaDuyet, bool soSanhDuLieu, bool layTKSua)
        {
            try
            {
                string sql = "Server=" + DiaChiSQLSource;
                sql += ";Database=" + DBSource;
                sql += ";Uid=" + UserSQLSource;
                sql += ";Pwd=" + PassSQLSource;
                SaveNodeXmlConnectionStringSource(sql);

                string sqlTemp = "Server=" + DiaChiSQLTarget;
                sqlTemp += ";Database=" + DBTarget;
                sqlTemp += ";Uid=" + UserSQLTarget;
                sqlTemp += ";Pwd=" + PassSQLTarget;
                SaveNodeXmlConnectionStringTarget("MSSQLTarget", sqlTemp);

                SaveNodeXmlAppSettings("DiaChiSQLSource", DiaChiSQLSource);
                SaveNodeXmlAppSettings("DBSource", DBSource);
                SaveNodeXmlAppSettings("UserSQLSource", UserSQLSource);
                SaveNodeXmlAppSettings("PassSQLSource", PassSQLSource);

                SaveNodeXmlAppSettings("DiaChiSQLTarget", DiaChiSQLTarget);
                SaveNodeXmlAppSettings("DBTarget", DBTarget);
                SaveNodeXmlAppSettings("UserSQLTarget", UserSQLTarget);
                SaveNodeXmlAppSettings("PassSQLTarget", PassSQLTarget);

                SaveNodeXmlAppSettings("PhanHe", phanHe == 0 ? "GC" : "SXXK");
                SaveNodeXmlAppSettings("NgayBatDau", ngayBatDau.ToString("MM/dd/yyyy"));
                SaveNodeXmlAppSettings("NgayKetThuc", ngayKetThuc.ToString("MM/dd/yyyy"));

                SaveNodeXmlAppSettings("LayThongTinDaDuyet", layTKDaDuyet);
                SaveNodeXmlAppSettings("SoSanhDuLieu", soSanhDuLieu);
                SaveNodeXmlAppSettings("LayThongTinToKhaiSua", layTKSua);

                if (RefreshKey() != "")
                {
                    GlobalSettings.NgayBatDau = ngayBatDau;
                    GlobalSettings.NgayKetThuc = NgayKetThuc;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}
