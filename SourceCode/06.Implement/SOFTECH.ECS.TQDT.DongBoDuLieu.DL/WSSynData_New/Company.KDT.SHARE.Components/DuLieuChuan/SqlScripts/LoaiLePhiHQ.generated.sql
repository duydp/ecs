-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiLePhiHQ_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_Insert]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiLePhiHQ_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_Update]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiLePhiHQ_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiLePhiHQ_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_Delete]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiLePhiHQ_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiLePhiHQ_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_Load]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiLePhiHQ_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiLePhiHQ_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiLePhiHQ_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_Insert]
	@MaLePhi varchar(50),
	@TenLePhi nvarchar(2048),
	@MoTa nvarchar(2048),
	@TrangThai varchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_LoaiLePhiHQ]
(
	[MaLePhi],
	[TenLePhi],
	[MoTa],
	[TrangThai],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@MaLePhi,
	@TenLePhi,
	@MoTa,
	@TrangThai,
	@DateCreated,
	@DateModified
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiLePhiHQ_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_Update]
	@MaLePhi varchar(50),
	@TenLePhi nvarchar(2048),
	@MoTa nvarchar(2048),
	@TrangThai varchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_LoaiLePhiHQ]
SET
	[TenLePhi] = @TenLePhi,
	[MoTa] = @MoTa,
	[TrangThai] = @TrangThai,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[MaLePhi] = @MaLePhi

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiLePhiHQ_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_InsertUpdate]
	@MaLePhi varchar(50),
	@TenLePhi nvarchar(2048),
	@MoTa nvarchar(2048),
	@TrangThai varchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [MaLePhi] FROM [dbo].[t_HaiQuan_LoaiLePhiHQ] WHERE [MaLePhi] = @MaLePhi)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_LoaiLePhiHQ] 
		SET
			[TenLePhi] = @TenLePhi,
			[MoTa] = @MoTa,
			[TrangThai] = @TrangThai,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[MaLePhi] = @MaLePhi
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_LoaiLePhiHQ]
	(
			[MaLePhi],
			[TenLePhi],
			[MoTa],
			[TrangThai],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@MaLePhi,
			@TenLePhi,
			@MoTa,
			@TrangThai,
			@DateCreated,
			@DateModified
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiLePhiHQ_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_Delete]
	@MaLePhi varchar(50)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_LoaiLePhiHQ]
WHERE
	[MaLePhi] = @MaLePhi

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiLePhiHQ_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_LoaiLePhiHQ] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiLePhiHQ_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_Load]
	@MaLePhi varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaLePhi],
	[TenLePhi],
	[MoTa],
	[TrangThai],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_LoaiLePhiHQ]
WHERE
	[MaLePhi] = @MaLePhi
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiLePhiHQ_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaLePhi],
	[TenLePhi],
	[MoTa],
	[TrangThai],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_LoaiLePhiHQ] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiLePhiHQ_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiLePhiHQ_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaLePhi],
	[TenLePhi],
	[MoTa],
	[TrangThai],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_LoaiLePhiHQ]	

GO

