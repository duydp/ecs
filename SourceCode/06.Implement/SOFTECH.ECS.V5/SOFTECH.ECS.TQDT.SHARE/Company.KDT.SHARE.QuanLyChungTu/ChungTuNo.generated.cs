using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class ChungTuNo
	{
		#region Properties.
		
		public long ID { set; get; }
		public string SO_CT { set; get; }
		public DateTime NGAY_CT { set; get; }
		public string MA_LOAI_CT { set; get; }
		public string NOI_CAP { set; get; }
		public string TO_CHUC_CAP { set; get; }
		public string LoaiKB { set; get; }
		public bool IsNoChungTu { set; get; }
		public DateTime ThoiHanNop { set; get; }
		public string DIENGIAI { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string GUIDSTR { set; get; }
		public decimal SOTN { set; get; }
		public DateTime NGAYTN { set; get; }
		public string Phanluong { set; get; }
		public string HuongDan { set; get; }
		public long TKMDID { set; get; }
		public DateTime NgayHetHan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ChungTuNo Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ChungTuNo_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			ChungTuNo entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new ChungTuNo();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SO_CT"))) entity.SO_CT = reader.GetString(reader.GetOrdinal("SO_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_CT"))) entity.NGAY_CT = reader.GetDateTime(reader.GetOrdinal("NGAY_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MA_LOAI_CT"))) entity.MA_LOAI_CT = reader.GetString(reader.GetOrdinal("MA_LOAI_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NOI_CAP"))) entity.NOI_CAP = reader.GetString(reader.GetOrdinal("NOI_CAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TO_CHUC_CAP"))) entity.TO_CHUC_CAP = reader.GetString(reader.GetOrdinal("TO_CHUC_CAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetString(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsNoChungTu"))) entity.IsNoChungTu = reader.GetBoolean(reader.GetOrdinal("IsNoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("DIENGIAI"))) entity.DIENGIAI = reader.GetString(reader.GetOrdinal("DIENGIAI"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOTN"))) entity.SOTN = reader.GetDecimal(reader.GetOrdinal("SOTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYTN"))) entity.NGAYTN = reader.GetDateTime(reader.GetOrdinal("NGAYTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanluong"))) entity.Phanluong = reader.GetString(reader.GetOrdinal("Phanluong"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuongDan"))) entity.HuongDan = reader.GetString(reader.GetOrdinal("HuongDan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMDID"))) entity.TKMDID = reader.GetInt64(reader.GetOrdinal("TKMDID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<ChungTuNo> SelectCollectionAll()
		{
			List<ChungTuNo> collection = new List<ChungTuNo>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				ChungTuNo entity = new ChungTuNo();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SO_CT"))) entity.SO_CT = reader.GetString(reader.GetOrdinal("SO_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_CT"))) entity.NGAY_CT = reader.GetDateTime(reader.GetOrdinal("NGAY_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MA_LOAI_CT"))) entity.MA_LOAI_CT = reader.GetString(reader.GetOrdinal("MA_LOAI_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NOI_CAP"))) entity.NOI_CAP = reader.GetString(reader.GetOrdinal("NOI_CAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TO_CHUC_CAP"))) entity.TO_CHUC_CAP = reader.GetString(reader.GetOrdinal("TO_CHUC_CAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetString(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsNoChungTu"))) entity.IsNoChungTu = reader.GetBoolean(reader.GetOrdinal("IsNoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("DIENGIAI"))) entity.DIENGIAI = reader.GetString(reader.GetOrdinal("DIENGIAI"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOTN"))) entity.SOTN = reader.GetDecimal(reader.GetOrdinal("SOTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYTN"))) entity.NGAYTN = reader.GetDateTime(reader.GetOrdinal("NGAYTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanluong"))) entity.Phanluong = reader.GetString(reader.GetOrdinal("Phanluong"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuongDan"))) entity.HuongDan = reader.GetString(reader.GetOrdinal("HuongDan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMDID"))) entity.TKMDID = reader.GetInt64(reader.GetOrdinal("TKMDID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<ChungTuNo> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<ChungTuNo> collection = new List<ChungTuNo>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ChungTuNo entity = new ChungTuNo();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SO_CT"))) entity.SO_CT = reader.GetString(reader.GetOrdinal("SO_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_CT"))) entity.NGAY_CT = reader.GetDateTime(reader.GetOrdinal("NGAY_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MA_LOAI_CT"))) entity.MA_LOAI_CT = reader.GetString(reader.GetOrdinal("MA_LOAI_CT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NOI_CAP"))) entity.NOI_CAP = reader.GetString(reader.GetOrdinal("NOI_CAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TO_CHUC_CAP"))) entity.TO_CHUC_CAP = reader.GetString(reader.GetOrdinal("TO_CHUC_CAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetString(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsNoChungTu"))) entity.IsNoChungTu = reader.GetBoolean(reader.GetOrdinal("IsNoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("DIENGIAI"))) entity.DIENGIAI = reader.GetString(reader.GetOrdinal("DIENGIAI"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("SOTN"))) entity.SOTN = reader.GetDecimal(reader.GetOrdinal("SOTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAYTN"))) entity.NGAYTN = reader.GetDateTime(reader.GetOrdinal("NGAYTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanluong"))) entity.Phanluong = reader.GetString(reader.GetOrdinal("Phanluong"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuongDan"))) entity.HuongDan = reader.GetString(reader.GetOrdinal("HuongDan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMDID"))) entity.TKMDID = reader.GetInt64(reader.GetOrdinal("TKMDID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuNo_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungTuNo_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuNo_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungTuNo_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertChungTuNo(string sO_CT, DateTime nGAY_CT, string mA_LOAI_CT, string nOI_CAP, string tO_CHUC_CAP, string loaiKB, bool isNoChungTu, DateTime thoiHanNop, string dIENGIAI, int trangThaiXuLy, string gUIDSTR, decimal sOTN, DateTime nGAYTN, string phanluong, string huongDan, long tKMDID, DateTime ngayHetHan)
		{
			ChungTuNo entity = new ChungTuNo();	
			entity.SO_CT = sO_CT;
			entity.NGAY_CT = nGAY_CT;
			entity.MA_LOAI_CT = mA_LOAI_CT;
			entity.NOI_CAP = nOI_CAP;
			entity.TO_CHUC_CAP = tO_CHUC_CAP;
			entity.LoaiKB = loaiKB;
			entity.IsNoChungTu = isNoChungTu;
			entity.ThoiHanNop = thoiHanNop;
			entity.DIENGIAI = dIENGIAI;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.SOTN = sOTN;
			entity.NGAYTN = nGAYTN;
			entity.Phanluong = phanluong;
			entity.HuongDan = huongDan;
			entity.TKMDID = tKMDID;
			entity.NgayHetHan = ngayHetHan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ChungTuNo_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
			db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year <= 1753 ? DBNull.Value : (object) NGAY_CT);
			db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
			db.AddInParameter(dbCommand, "@NOI_CAP", SqlDbType.NVarChar, NOI_CAP);
			db.AddInParameter(dbCommand, "@TO_CHUC_CAP", SqlDbType.NVarChar, TO_CHUC_CAP);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
			db.AddInParameter(dbCommand, "@IsNoChungTu", SqlDbType.Bit, IsNoChungTu);
			db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object) ThoiHanNop);
			db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
			db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year <= 1753 ? DBNull.Value : (object) NGAYTN);
			db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
			db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ChungTuNo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuNo item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateChungTuNo(long id, string sO_CT, DateTime nGAY_CT, string mA_LOAI_CT, string nOI_CAP, string tO_CHUC_CAP, string loaiKB, bool isNoChungTu, DateTime thoiHanNop, string dIENGIAI, int trangThaiXuLy, string gUIDSTR, decimal sOTN, DateTime nGAYTN, string phanluong, string huongDan, long tKMDID, DateTime ngayHetHan)
		{
			ChungTuNo entity = new ChungTuNo();			
			entity.ID = id;
			entity.SO_CT = sO_CT;
			entity.NGAY_CT = nGAY_CT;
			entity.MA_LOAI_CT = mA_LOAI_CT;
			entity.NOI_CAP = nOI_CAP;
			entity.TO_CHUC_CAP = tO_CHUC_CAP;
			entity.LoaiKB = loaiKB;
			entity.IsNoChungTu = isNoChungTu;
			entity.ThoiHanNop = thoiHanNop;
			entity.DIENGIAI = dIENGIAI;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.SOTN = sOTN;
			entity.NGAYTN = nGAYTN;
			entity.Phanluong = phanluong;
			entity.HuongDan = huongDan;
			entity.TKMDID = tKMDID;
			entity.NgayHetHan = ngayHetHan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ChungTuNo_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
			db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year == 1753 ? DBNull.Value : (object) NGAY_CT);
			db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
			db.AddInParameter(dbCommand, "@NOI_CAP", SqlDbType.NVarChar, NOI_CAP);
			db.AddInParameter(dbCommand, "@TO_CHUC_CAP", SqlDbType.NVarChar, TO_CHUC_CAP);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
			db.AddInParameter(dbCommand, "@IsNoChungTu", SqlDbType.Bit, IsNoChungTu);
			db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year == 1753 ? DBNull.Value : (object) ThoiHanNop);
			db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
			db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year == 1753 ? DBNull.Value : (object) NGAYTN);
			db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
			db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ChungTuNo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuNo item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateChungTuNo(long id, string sO_CT, DateTime nGAY_CT, string mA_LOAI_CT, string nOI_CAP, string tO_CHUC_CAP, string loaiKB, bool isNoChungTu, DateTime thoiHanNop, string dIENGIAI, int trangThaiXuLy, string gUIDSTR, decimal sOTN, DateTime nGAYTN, string phanluong, string huongDan, long tKMDID, DateTime ngayHetHan)
		{
			ChungTuNo entity = new ChungTuNo();			
			entity.ID = id;
			entity.SO_CT = sO_CT;
			entity.NGAY_CT = nGAY_CT;
			entity.MA_LOAI_CT = mA_LOAI_CT;
			entity.NOI_CAP = nOI_CAP;
			entity.TO_CHUC_CAP = tO_CHUC_CAP;
			entity.LoaiKB = loaiKB;
			entity.IsNoChungTu = isNoChungTu;
			entity.ThoiHanNop = thoiHanNop;
			entity.DIENGIAI = dIENGIAI;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.GUIDSTR = gUIDSTR;
			entity.SOTN = sOTN;
			entity.NGAYTN = nGAYTN;
			entity.Phanluong = phanluong;
			entity.HuongDan = huongDan;
			entity.TKMDID = tKMDID;
			entity.NgayHetHan = ngayHetHan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungTuNo_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
			db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year == 1753 ? DBNull.Value : (object) NGAY_CT);
			db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
			db.AddInParameter(dbCommand, "@NOI_CAP", SqlDbType.NVarChar, NOI_CAP);
			db.AddInParameter(dbCommand, "@TO_CHUC_CAP", SqlDbType.NVarChar, TO_CHUC_CAP);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
			db.AddInParameter(dbCommand, "@IsNoChungTu", SqlDbType.Bit, IsNoChungTu);
			db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year == 1753 ? DBNull.Value : (object) ThoiHanNop);
			db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
			db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year == 1753 ? DBNull.Value : (object) NGAYTN);
			db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
			db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ChungTuNo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuNo item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteChungTuNo(long id)
		{
			ChungTuNo entity = new ChungTuNo();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungTuNo_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ChungTuNo_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ChungTuNo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuNo item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}