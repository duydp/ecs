using System;

namespace HaiQuan.HS
{
	public class Global
	{
		public static string ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["HSConnectionString"];
	}
	
	public enum HSSearchWhat
	{
		AllWord = 0,
		AnyWord = 1,
		Exact = 2
	}
}
