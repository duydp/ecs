using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class BangKeHoSoThanhLy 
	{
		#region Private members.
		
		protected long _ID;
		protected string _MaBangKe = String.Empty;
		protected string _TenBangKe = String.Empty;
		protected int _STTHang;
        protected long _SoHSTL;
		protected long _MaterID;
        protected int _TrangThaiXL;
		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public string MaBangKe
		{
			set {this._MaBangKe = value;}
			get {return this._MaBangKe;}
		}
		public string TenBangKe
		{
			set {this._TenBangKe = value;}
			get {return this._TenBangKe;}
		}
		public int STTHang
		{
			set {this._STTHang = value;}
			get {return this._STTHang;}
		}
        public long SoHSTL
		{
			set {this._SoHSTL = value;}
			get {return this._SoHSTL;}
		}
		public long MaterID
		{
			set {this._MaterID = value;}
			get {return this._MaterID;}
		}
        public int TrangThaiXL
        {
            set { this._TrangThaiXL = value; }
            get { return this._TrangThaiXL; }
        }
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_Load";
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBangKe"))) this._MaBangKe = reader.GetString(reader.GetOrdinal("MaBangKe"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenBangKe"))) this._TenBangKe = reader.GetString(reader.GetOrdinal("TenBangKe"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHSTL"))) this._SoHSTL = reader.GetInt64(reader.GetOrdinal("SoHSTL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaterID"))) this._MaterID = reader.GetInt64(reader.GetOrdinal("MaterID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXL"))) this.TrangThaiXL = reader.GetInt32(reader.GetOrdinal("TrangThaiXL"));

                return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static BangKeHoSoThanhLyCollection SelectCollectionBy_MaterID(long masterID)
		{
			string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_SelectBy_MaterID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaterID", SqlDbType.BigInt, masterID);
			
			BangKeHoSoThanhLyCollection collection = new BangKeHoSoThanhLyCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				BangKeHoSoThanhLy entity = new BangKeHoSoThanhLy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBangKe"))) entity.MaBangKe = reader.GetString(reader.GetOrdinal("MaBangKe"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenBangKe"))) entity.TenBangKe = reader.GetString(reader.GetOrdinal("TenBangKe"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHSTL"))) entity.SoHSTL = reader.GetInt64(reader.GetOrdinal("SoHSTL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaterID"))) entity.MaterID = reader.GetInt64(reader.GetOrdinal("MaterID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXL"))) entity.TrangThaiXL = reader.GetInt32(reader.GetOrdinal("TrangThaiXL"));
              
                collection.Add(entity);
			}
            reader.Close();
			return collection;
		}
        public static BangKeHoSoThanhLyCollection SelectCollectionBy_MaterIDTransaction(long masterID,SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_SelectBy_MaterID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaterID", SqlDbType.BigInt, masterID);

            BangKeHoSoThanhLyCollection collection = new BangKeHoSoThanhLyCollection();
            IDataReader reader = db.ExecuteReader(dbCommand,transaction);
            while (reader.Read())
            {
                BangKeHoSoThanhLy entity = new BangKeHoSoThanhLy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaBangKe"))) entity.MaBangKe = reader.GetString(reader.GetOrdinal("MaBangKe"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenBangKe"))) entity.TenBangKe = reader.GetString(reader.GetOrdinal("TenBangKe"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHSTL"))) entity.SoHSTL = reader.GetInt64(reader.GetOrdinal("SoHSTL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaterID"))) entity.MaterID = reader.GetInt64(reader.GetOrdinal("MaterID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXL"))) entity.TrangThaiXL = reader.GetInt32(reader.GetOrdinal("TrangThaiXL"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_MaterID()
		{
			string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_SelectBy_MaterID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaterID", SqlDbType.BigInt, this._MaterID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public BangKeHoSoThanhLyCollection SelectCollectionAll()
		{
			BangKeHoSoThanhLyCollection collection = new BangKeHoSoThanhLyCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				BangKeHoSoThanhLy entity = new BangKeHoSoThanhLy();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBangKe"))) entity.MaBangKe = reader.GetString(reader.GetOrdinal("MaBangKe"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenBangKe"))) entity.TenBangKe = reader.GetString(reader.GetOrdinal("TenBangKe"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHSTL"))) entity.SoHSTL = reader.GetInt64(reader.GetOrdinal("SoHSTL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaterID"))) entity.MaterID = reader.GetInt64(reader.GetOrdinal("MaterID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXL"))) entity.TrangThaiXL = reader.GetInt32(reader.GetOrdinal("TrangThaiXL"));
                collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public BangKeHoSoThanhLyCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			BangKeHoSoThanhLyCollection collection = new BangKeHoSoThanhLyCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				BangKeHoSoThanhLy entity = new BangKeHoSoThanhLy();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBangKe"))) entity.MaBangKe = reader.GetString(reader.GetOrdinal("MaBangKe"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenBangKe"))) entity.TenBangKe = reader.GetString(reader.GetOrdinal("TenBangKe"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHSTL"))) entity.SoHSTL = reader.GetInt64(reader.GetOrdinal("SoHSTL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaterID"))) entity.MaterID = reader.GetInt64(reader.GetOrdinal("MaterID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXL"))) entity.TrangThaiXL = reader.GetInt32(reader.GetOrdinal("TrangThaiXL"));
                collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_Insert";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaBangKe", SqlDbType.VarChar, this._MaBangKe);
			db.AddInParameter(dbCommand, "@TenBangKe", SqlDbType.NVarChar, this._TenBangKe);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			db.AddInParameter(dbCommand, "@SoHSTL", SqlDbType.BigInt, this._SoHSTL);
			db.AddInParameter(dbCommand, "@MaterID", SqlDbType.BigInt, this._MaterID);
            db.AddInParameter(dbCommand, "@TrangThaiXL", SqlDbType.Int, this.TrangThaiXL);
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				this._ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(BangKeHoSoThanhLyCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BangKeHoSoThanhLy item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, BangKeHoSoThanhLyCollection collection)
        {
            foreach (BangKeHoSoThanhLy item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_InsertUpdate";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@MaBangKe", SqlDbType.VarChar, this._MaBangKe);
			db.AddInParameter(dbCommand, "@TenBangKe", SqlDbType.NVarChar, this._TenBangKe);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            db.AddInParameter(dbCommand, "@SoHSTL", SqlDbType.BigInt, this._SoHSTL);
			db.AddInParameter(dbCommand, "@MaterID", SqlDbType.BigInt, this._MaterID);
            db.AddInParameter(dbCommand, "@TrangThaiXL", SqlDbType.Int, this.TrangThaiXL);
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(BangKeHoSoThanhLyCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BangKeHoSoThanhLy item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_Update";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			db.AddInParameter(dbCommand, "@MaBangKe", SqlDbType.VarChar, this._MaBangKe);
			db.AddInParameter(dbCommand, "@TenBangKe", SqlDbType.NVarChar, this._TenBangKe);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            db.AddInParameter(dbCommand, "@SoHSTL", SqlDbType.BigInt, this._SoHSTL);
			db.AddInParameter(dbCommand, "@MaterID", SqlDbType.BigInt, this._MaterID);
            db.AddInParameter(dbCommand, "@TrangThaiXL", SqlDbType.Int, this.TrangThaiXL);
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(BangKeHoSoThanhLyCollection collection, SqlTransaction transaction)
        {
            foreach (BangKeHoSoThanhLy item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_BangKeHoSoThanhLy_Delete";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(BangKeHoSoThanhLyCollection collection, SqlTransaction transaction)
        {
            foreach (BangKeHoSoThanhLy item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(BangKeHoSoThanhLyCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (BangKeHoSoThanhLy item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion
	}	
}