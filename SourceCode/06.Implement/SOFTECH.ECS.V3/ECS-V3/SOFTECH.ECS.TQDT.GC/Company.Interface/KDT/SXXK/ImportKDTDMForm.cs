using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
namespace Company.Interface.KDT.SXXK
{
    public partial class ImportKDTDMForm : BaseForm
    {
        public  DinhMucDangKy dmDangKy ;// = new DinhMucDangKy();                                                   
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public ImportKDTDMForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }

        //private int checkDMExit1(string maSP, string maNPL)
        //{
        //    for (int i = 0; i < this.DMCollection.Count; i++)
        //    {
        //        if (this.DMCollection[i].MaSanPHam.ToUpper() == maSP.ToUpper() && this.DMCollection[i].MaNguyenPhuLieu.ToUpper() == maNPL.ToUpper()) return i;
        //    }
        //    return -1;

        //}
       
        private void ImportDMForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            txtMaSPColumn.Text = GlobalSettings.DinhMuc.MaSP;
            txtMaNPLColumn.Text = GlobalSettings.DinhMuc.MaNPL;
            txtDMSDColumn.Text = GlobalSettings.DinhMuc.DinhMucSuDung;
            txtTLHHColumn.Text = GlobalSettings.DinhMuc.TyLeHH;            
            openFileDialog1.InitialDirectory = Application.StartupPath;
           // this.dgList.DataSource = this.DMReadCollection;
        }
        private bool EmptyCheck(string st)
        {
            if (st == "") return true;
            return false;
        }
        private bool CheckNumber(decimal so)
        {
            if (so < 0 || so > 1000000000000) return true;
            return false;
        }
        
        private bool CheckTLHH(decimal so)
        {
            if (  so< 0 ||so > 100) return true;
            return false;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
           
            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                showMsg("MSG_0203008");
                //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            
            WorksheetRowCollection wsrc = ws.Rows;
            char maSPColumn = Convert.ToChar(txtMaSPColumn.Text);
            int maSPCol = ConvertCharToInt(maSPColumn);
           
            char maNPLColumn = Convert.ToChar(txtMaNPLColumn.Text);
            int maNPLCol = ConvertCharToInt(maNPLColumn);
            
            char DMSDColumn = Convert.ToChar(txtDMSDColumn.Text);
            int DMSDCol = ConvertCharToInt(DMSDColumn);
            
            char TLHHColumn = Convert.ToChar(txtTLHHColumn.Text);
            int TLHHCol = ConvertCharToInt(TLHHColumn);


            char NPLCungUngColumn = Convert.ToChar(txtNPLCungUng.Text);
            int NPLCungUngCol = ConvertCharToInt(NPLCungUngColumn);
            int j = 0;
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {                   
                    try
                    {

                        DinhMuc dinhMuc = new DinhMuc();
                        if (wsr.Cells[maNPLCol].Value == null || Convert.ToString(wsr.Cells[maNPLCol].Value) == "") continue;
                        //if (EmptyCheck(Convert.ToString(wsr.Cells[maNPLCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maSPCol].Value)) || CheckNumber(Convert.ToDecimal(wsr.Cells[DMSDCol].Value))  || CheckNumber(Convert.ToDecimal(wsr.Cells[NPLCungUngCol].Value)) || CheckTLHH(Convert.ToDecimal(wsr.Cells[TLHHCol].Value)))
                        //{
                        //    ShowMessage(" Dữ liệu tại dòng " + (wsr.Index + 1) + " sai, được bỏ qua", false);
                        //    continue;
                        //}                                                
                        dinhMuc.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.Ma = dinhMuc.MaSanPham;
                        sp.HopDong_ID = HD.ID;
                        j = wsr.Index + 1;
                        if (!sp.Load())
                        {
                            showMsg("MSG_0203015", dinhMuc.MaSanPham);
                            //MLMessages("Mã sản phẩm : "+dinhMuc.MaSanPham+" không có trong hợp đồng này.","MSG_WRN10",dinhMuc.MaSanPham, false);
                            this.Cursor = Cursors.Default;
                            uiButton1.Enabled = true;
                            return;
                        }
                        dinhMuc.TenSanPham = sp.Ten;
                        dinhMuc.MaNguyenPhuLieu = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.Ma = dinhMuc.MaNguyenPhuLieu;
                        npl.HopDong_ID = HD.ID;
                        if (!npl.Load())
                        {
                            showMsg("MSG_0203016", dinhMuc.MaNguyenPhuLieu);
                            //MLMessages("Mã nguyên phụ liệu : " + dinhMuc.MaNguyenPhuLieu + " không có trong hợp đồng này.", "MSG_WRN10", dinhMuc.MaNguyenPhuLieu, false);
                            this.Cursor = Cursors.Default;
                            uiButton1.Enabled = true;
                            return;
                        }
                        dinhMuc.TenNPL = npl.Ten;
                        dinhMuc.DVT_ID = npl.DVT_ID;
                        try
                        {
                            dinhMuc.DinhMucSuDung =  Math.Round(Convert.ToDecimal(wsr.Cells[DMSDCol].Value),GlobalSettings.SoThapPhan.DinhMuc,MidpointRounding.AwayFromZero);
                        }catch
                        {
                            showMsg("MSG_0203017", wsr.Index + 1);
                            //MLMessages("Định mức sử dụng tại dòng " + (wsr.Index + 1) + "  quá lớn", "MSG_EXC09", j.ToString(), false);
                        }
                        try
                        {
                            dinhMuc.TyLeHaoHut = Convert.ToDecimal(wsr.Cells[TLHHCol].Value);
                        }
                        catch
                        {
                            showMsg("MSG_0203018", wsr.Index + 1);
                            //MLMessages("Tỷ lệ hao hụt tại dòng " + (wsr.Index + 1) + "  quá lớn", "MSG_EXC09",j.ToString(), false);
                        }
                        try
                        {
                            dinhMuc.NPL_TuCungUng = Convert.ToDecimal(wsr.Cells[NPLCungUngCol].Value);                       
                        }
                        catch
                        {
                            showMsg("MSG_0203019", wsr.Index + 1);
                            //MLMessages("NPL tự cung ứng tại dòng " + (wsr.Index + 1) + "  quá lớn","MSG_EXC09",j.ToString(), false);
                        }
                        if (dinhMuc.NPL_TuCungUng > dinhMuc.DinhMucSuDung)
                        {
                            showMsg("MSG_0203020", wsr.Index + 1);
                            //MLMessages(" Tại dòng " + (wsr.Index + 1) + " NPL tự cung ứng lớn hơn ĐMSD,được bỏ qua","MSG_EXC12",j.ToString(), false);
                            continue;
                        }
                        dinhMuc.DVT_ID = npl.DVT_ID;
                        long id = dmDangKy.GetIDDinhMucExit(dinhMuc.MaSanPham, dmDangKy.ID, this.HD.ID);
                        if (id > 0)
                        {
                            showMsg("MSG_2702034", new String[] { dinhMuc.MaSanPham, id.ToString() });
                            //MLMessages("Sản phẩm : "+dinhMuc.MaSanPham+" đã được khai báo trong danh sách định mức có ID= "+id,"MSG_WRN06",dinhMuc.MaSanPham, false);
                            //dmCollection.Clear();
                            break;
                        }
                        else if (id < 0)
                        {
                            showMsg("MSG_240230", dinhMuc.MaSanPham);
                            //MLMessages("Sản phẩm : " + dinhMuc.MaSanPham + " đã được khai báo định mức.","MSG_EXC10",dinhMuc.MaSanPham, false);
                            //dmCollection.Clear();
                            break;
                        }
                        Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                        dmDuyet.HopDong_ID = HD.ID;
                        dmDuyet.MaSanPham = dinhMuc.MaSanPham;
                        if (dmDuyet.CheckExitsDinhMucSanPham())
                        {
                            showMsg("MSG_240230", dinhMuc.MaSanPham);
                            //MLMessages("Sản phẩm : " + dinhMuc.MaSanPham + " đã được khai báo định mức.", "MSG_EXC10", dinhMuc.MaSanPham, false);
                            //dmCollection.Clear();
                            break;
                        }                      
                        if (chkOverwrite.Checked)
                        {
                            bool isExit = false;
                            foreach (DinhMuc dm2 in dmDangKy.DMCollection)
                            {
                                if (dinhMuc.MaSanPham.Trim().ToUpper()== dm2.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == dm2.MaNguyenPhuLieu.Trim().ToUpper())
                                {
                                    dmDangKy.DMCollection.Remove(dm2);
                                    dm2.Delete(HD.ID);
                                    dmDangKy.DMCollection.Add(dinhMuc);
                                    isExit = true;
                                    break;
                                }
                            }
                            if (!isExit)
                                dmDangKy.DMCollection.Add(dinhMuc);
                           
                        }
                        else
                        {
                            bool isExit = false;
                            foreach (DinhMuc dm2 in dmDangKy.DMCollection)
                            {
                                if (dinhMuc.MaSanPham.Trim().ToUpper() == dm2.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == dm2.MaNguyenPhuLieu.Trim().ToUpper())
                                {
                                    //ShowMessage(" ", false);
                                    isExit = true;
                                    break;
                                }
                            }
                            if(!isExit)
                                dmDangKy.DMCollection.Add(dinhMuc);
                        }

                        

                      
                       
                    }
                    catch
                    {
                        if (showMsg("MSG_0203014", wsr.Index + 1, true) != "Yes")
                        {
                            this.uiButton1.Enabled = true;
                            this.Cursor = Cursors.Default;                            
                            break;
                        }
                    }
                }
            }
            if (dmDangKy.DMCollection.Count > 0)
                //ShowMessage("Đã đọc xong dữ liệu", false);
                showMsg("MSG_EXC04");
                //Message("MSG_EXC04", "", false);
            this.uiButton1.Enabled = true;
            this.Cursor = Cursors.Default;
            this.Close();
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }

        private void lblLinkExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GC("DM");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

     


    }
}