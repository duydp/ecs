using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Runtime.InteropServices;

namespace SDXSample
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		internal System.Windows.Forms.Button cmdReadMap;
		internal System.Windows.Forms.Button cmdWriteMap;
		internal System.Windows.Forms.Button cmdDecrypt;
		internal System.Windows.Forms.Button cmdEncrypt;
		internal System.Windows.Forms.Button cmdRead;
		internal System.Windows.Forms.Button cmdTransform;
		internal System.Windows.Forms.Button cmdWrite;
		internal System.Windows.Forms.Label Label1;
		internal System.Windows.Forms.ListBox lstOutput;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdReadMap = new System.Windows.Forms.Button();
			this.cmdWriteMap = new System.Windows.Forms.Button();
			this.cmdDecrypt = new System.Windows.Forms.Button();
			this.cmdEncrypt = new System.Windows.Forms.Button();
			this.cmdRead = new System.Windows.Forms.Button();
			this.cmdTransform = new System.Windows.Forms.Button();
			this.cmdWrite = new System.Windows.Forms.Button();
			this.Label1 = new System.Windows.Forms.Label();
			this.lstOutput = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// cmdReadMap
			// 
			this.cmdReadMap.Location = new System.Drawing.Point(400, 296);
			this.cmdReadMap.Name = "cmdReadMap";
			this.cmdReadMap.Size = new System.Drawing.Size(112, 32);
			this.cmdReadMap.TabIndex = 19;
			this.cmdReadMap.Text = "Read with Map";
			this.cmdReadMap.Click += new System.EventHandler(this.cmdReadMap_Click);
			// 
			// cmdWriteMap
			// 
			this.cmdWriteMap.Location = new System.Drawing.Point(400, 256);
			this.cmdWriteMap.Name = "cmdWriteMap";
			this.cmdWriteMap.Size = new System.Drawing.Size(112, 32);
			this.cmdWriteMap.TabIndex = 18;
			this.cmdWriteMap.Text = "Write with Map";
			this.cmdWriteMap.Click += new System.EventHandler(this.cmdWriteMap_Click);
			// 
			// cmdDecrypt
			// 
			this.cmdDecrypt.Location = new System.Drawing.Point(400, 216);
			this.cmdDecrypt.Name = "cmdDecrypt";
			this.cmdDecrypt.Size = new System.Drawing.Size(112, 32);
			this.cmdDecrypt.TabIndex = 17;
			this.cmdDecrypt.Text = "Decrypt Data";
			this.cmdDecrypt.Click += new System.EventHandler(this.cmdDecrypt_Click);
			// 
			// cmdEncrypt
			// 
			this.cmdEncrypt.Location = new System.Drawing.Point(400, 176);
			this.cmdEncrypt.Name = "cmdEncrypt";
			this.cmdEncrypt.Size = new System.Drawing.Size(112, 32);
			this.cmdEncrypt.TabIndex = 16;
			this.cmdEncrypt.Text = "Encrypt Data";
			this.cmdEncrypt.Click += new System.EventHandler(this.cmdEncrypt_Click);
			// 
			// cmdRead
			// 
			this.cmdRead.Location = new System.Drawing.Point(400, 136);
			this.cmdRead.Name = "cmdRead";
			this.cmdRead.Size = new System.Drawing.Size(112, 32);
			this.cmdRead.TabIndex = 15;
			this.cmdRead.Text = "Read Data";
			this.cmdRead.Click += new System.EventHandler(this.cmdRead_Click);
			// 
			// cmdTransform
			// 
			this.cmdTransform.Location = new System.Drawing.Point(400, 96);
			this.cmdTransform.Name = "cmdTransform";
			this.cmdTransform.Size = new System.Drawing.Size(112, 32);
			this.cmdTransform.TabIndex = 14;
			this.cmdTransform.Text = "Transform Data";
			this.cmdTransform.Click += new System.EventHandler(this.cmdTransform_Click);
			// 
			// cmdWrite
			// 
			this.cmdWrite.Location = new System.Drawing.Point(400, 56);
			this.cmdWrite.Name = "cmdWrite";
			this.cmdWrite.Size = new System.Drawing.Size(112, 32);
			this.cmdWrite.TabIndex = 13;
			this.cmdWrite.Text = "Write Data";
			this.cmdWrite.Click += new System.EventHandler(this.cmdWrite_Click);
			// 
			// Label1
			// 
			this.Label1.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Label1.Location = new System.Drawing.Point(8, 16);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(512, 32);
			this.Label1.TabIndex = 11;
			this.Label1.Text = "SecureDongle X C# 2003 Sample";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lstOutput
			// 
			this.lstOutput.Location = new System.Drawing.Point(16, 56);
			this.lstOutput.Name = "lstOutput";
			this.lstOutput.Size = new System.Drawing.Size(368, 264);
			this.lstOutput.TabIndex = 10;
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(528, 342);
			this.Controls.Add(this.cmdReadMap);
			this.Controls.Add(this.cmdWriteMap);
			this.Controls.Add(this.cmdDecrypt);
			this.Controls.Add(this.cmdEncrypt);
			this.Controls.Add(this.cmdRead);
			this.Controls.Add(this.cmdTransform);
			this.Controls.Add(this.cmdWrite);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.lstOutput);
			this.Name = "frmMain";
			this.Text = "SecureDongle X C# 2003 Sample";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain());
		}

		//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-
		[DllImport("SDX.dll")]
		static extern int SDX_Find();
		[DllImport("SDX.dll")]
		static extern int SDX_Open(int mode, Int32 uid, ref Int32 hid);
		[DllImport("SDX.dll")]
		static extern void SDX_Close(int handle);
		[DllImport("SDX.dll")]
		static extern int SDX_Read(int handle, int block_index, byte[] buffer512);
		[DllImport("SDX.dll")]
		static extern int SDX_Write(int handle, int block_index, String buffer512);
		[DllImport("SDX.dll")]
		static extern int SDX_GetVersion(int handle);
		[DllImport("SDX.dll")]      
		static extern int SDX_Transform(int handle, int len, byte[] bufferData);
		[DllImport("SDX.dll")]
		static extern int SDX_RSAEncrypt(int handle, int startIndex, String bufferData, ref int len, byte[] key512);
		[DllImport("SDX.dll")]
		static extern int SDX_RSADecrypt(int handle, int startIndex, byte[] bufferData, ref int len, byte[] key512);
		
		const int HID_MODE = -1;
		//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
		int ret = 0;
		Int32 handle = 0;
		Int32 hid = 0;
		Int32 uid = 0;
		String seed, Writebuff, error;
		byte[] key512 = new byte[512];
		//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=

		private void cmdWrite_Click(object sender, System.EventArgs e)
		{
			// Write to SecureDongle X with specified UID
			ret = SDX_Find();
			if (ret < 0)
			{
				error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}

			if (ret == 0)
			{
				lstOutput.Items.Add("No SecureDongle X plugged");
				return;
			}

			lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
			ret = SDX_Open(1, uid, ref hid);
			if (ret < 0)
			{
				error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			handle = ret;
			error = String.Format("Open SecureDongle X: {0:X}", handle);
			lstOutput.Items.Add(error);

			// Write 'SDX - Sample Data' to block 0
			Writebuff = "SDX - Sample Data";
			ret = SDX_Write(handle, 0, Writebuff);
			if (ret < 0)
			{
				error = String.Format("Error Writing data: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			lstOutput.Items.Add("Success Writing 'SDX - Sample Data' to Block 0");

			SDX_Close(handle);
		}

		private void cmdTransform_Click(object sender, System.EventArgs e)
		{
			// Transform data using specified SecureDongle X
			ret = SDX_Find();
			if (ret < 0)
			{
				error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}

			if (ret == 0)
			{
				lstOutput.Items.Add("No SecureDongle X plugged");
				return;
			}

			lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
			ret = SDX_Open(1, uid, ref hid);
			if (ret < 0)
			{
				error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			handle = ret;
			error = String.Format("Open SecureDongle X: {0:X}", handle);
			lstOutput.Items.Add(error);

			// Transform data
			byte[] retbuff = new byte[512];
			retbuff = ASCIIEncoding.UTF8.GetBytes("SDXTransform Test");
                  
			ret = SDX_Transform(handle, retbuff.Length, retbuff);
			if (ret < 0)
			{
				error = String.Format("Error Transforming: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			string sReturn = System.Text.ASCIIEncoding.ASCII.GetString(retbuff);
			lstOutput.Items.Add("Transform result: " + sReturn);
        
			SDX_Close(handle);
		}

		private void cmdRead_Click(object sender, System.EventArgs e)
		{
			// Read SecureDongle X with specified UID
			ret = SDX_Find();
			if (ret < 0)
			{
				error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}

			if (ret == 0)
			{
				lstOutput.Items.Add("No SecureDongle X plugged");
				return;
			}

			lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
			ret = SDX_Open(1, uid, ref hid);
			if (ret < 0)
			{
				error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			handle = ret;
			error = String.Format("Open SecureDongle X: {0:X}", handle);
			lstOutput.Items.Add(error);

			// Read from Block 0      
			byte[] retbuff = new byte[512];
			ret = SDX_Read(handle, 0, retbuff);
			if (ret < 0)
			{
				error = String.Format("Error Reading data: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			string sReturn = System.Text.ASCIIEncoding.ASCII.GetString(retbuff);
			lstOutput.Items.Add("Success Read. Data: " + sReturn);

			SDX_Close(handle);
		}

		private void cmdEncrypt_Click(object sender, System.EventArgs e)
		{
			// Encrypt using RSA and store to SecureDongle X with specified UID
			int startIndex, bufferLength;
			string text;

			ret = SDX_Find();
			if (ret < 0)
			{
				error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}

			if (ret == 0)
			{
				lstOutput.Items.Add("No SecureDongle X plugged");
				return;
			}

			lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
			ret = SDX_Open(1, uid, ref hid);
			if (ret < 0)
			{
				error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			handle = ret;
			error = String.Format("Open SecureDongle X: {0:X}", handle);
			lstOutput.Items.Add(error);

			// Encrypt and write to SDX index 100
			text = "SDX Test RSA Encrypt";
			bufferLength = text.Length;
			startIndex = 100;

			ret = SDX_RSAEncrypt(handle, startIndex, text, ref bufferLength, key512);
			if (ret < 0)
			{
				error = String.Format("Error Encrypting data: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			lstOutput.Items.Add("Write success, size: " + bufferLength.ToString() + ". Key to decrypt is stored inside key512 variable.");
			lstOutput.Items.Add("You can try reading the contents of the SDX");

			SDX_Close(handle);
		}

		private void cmdDecrypt_Click(object sender, System.EventArgs e)
		{
			// Read from SecureDongle X with specified UID and decrypt using RSA
			int startIndex, bufferLength;
			byte[] plainText = new byte[512];

			ret = SDX_Find();
			if (ret < 0)
			{
				error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}

			if (ret == 0)
			{
				lstOutput.Items.Add("No SecureDongle X plugged");
				return;
			}

			lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
			ret = SDX_Open(1, uid, ref hid);
			if (ret < 0)
			{
				error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			handle = ret;
			error = String.Format("Open SecureDongle X: {0:X}", handle);
			lstOutput.Items.Add(error);

			// Decrypt with size 256, starting from index 100
			bufferLength = 256;
			startIndex = 100;

			ret = SDX_RSADecrypt(handle, startIndex, plainText, ref bufferLength, key512);
			if (ret < 0)
			{
				error = String.Format("Error Decrypting data: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			string sReturn = System.Text.ASCIIEncoding.ASCII.GetString(plainText);
			lstOutput.Items.Add("Decrypted Data: " + sReturn);

			SDX_Close(handle);
		}

		private void cmdWriteMap_Click(object sender, System.EventArgs e)
		{
			// Write to SecureDongle X with specified UID
			// This is a sample of simple Data Mapping to put plain text data not in order
			// For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
			ret = SDX_Find();
			if (ret < 0)
			{
				error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}

			if (ret == 0)
			{
				lstOutput.Items.Add("No SecureDongle X plugged");
				return;
			}

			lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
			ret = SDX_Open(1,uid, ref hid);
			if (ret < 0)
			{
				error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			handle = ret;
			error = String.Format("Open SecureDongle X: {0:X}", handle);
			lstOutput.Items.Add(error);

			// Write encrypted 'SDX - Sample Map Encryption' to block 0
			Writebuff = "SDX - Sample Map Encryption";
			lstOutput.Items.Add("Original data: " + Writebuff);
			lstOutput.Items.Add("Length = " + Writebuff.Length.ToString());

			//****************** ENCRYPTION SAMPLE *******************
			byte[] temp = new byte[512];
			int i, sze, indx, tmp;
			Random rand = new Random();

			if (Writebuff.Length > 512) 
			{
				lstOutput.Items.Add("Error: Data size max is 512");            
				return;
			}
        
			// Create a 512-byte Full Map with randomized content
			for (i=0; i<512; i++) 
			{
				if ((rand.Next(2) % 2) == 0)
				{
					tmp = (rand.Next(26) % 26) + 65;
					temp[i] = (byte)tmp;
				} 
				else 
				{
					tmp = (rand.Next(26) % 26) + 97;
					temp[i] = (byte)tmp;
				}
			}
        
			// Calculate where to put the data, and put it there
			for (i=0; i<Writebuff.Length;i++)
			{
				sze = i * (Writebuff.Length - 1);
				indx = sze % 512;
				temp[indx] = (byte)Writebuff[i];
			}
			String stri = Encoding.GetEncoding(1252).GetString(temp);
         
			//********************************************************
        
			ret = SDX_Write(handle, 0, stri);
			if (ret < 0)
			{
				error = String.Format("Error Writing data: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			lstOutput.Items.Add("Success Writing 'SDX - Sample Data' to Block 0");

			SDX_Close(handle);
		}

		private void cmdReadMap_Click(object sender, System.EventArgs e)
		{
			// Read from SecureDongle X with specified UID
			// This is the sample to read the Mapped data
			// Don't forget to set the Length of data to be read first
			ret = SDX_Find();
			if (ret < 0)
			{
				error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}

			if (ret == 0)
			{
				lstOutput.Items.Add("No SecureDongle X plugged");
				return;
			}

			lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
			ret = SDX_Open(1, uid, ref hid);
			if (ret < 0)
			{
				error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
			handle = ret;
			error = String.Format("Open SecureDongle X: {0:X}", handle);
			lstOutput.Items.Add(error);
			short datasize = 27;

			// Read from Block 0      
			byte[] retbuff = new byte[512];
			ret = SDX_Read(handle, 0, retbuff);
			if (ret < 0)
			{
				error = String.Format("Error Reading data: {0:X}", ret);
				lstOutput.Items.Add(error);
				return;
			}
               
			//******************* DECRYPTION **********************
			string buff = "";
			int i, sze, indx;
			i = 0;
			for (i=0; i<datasize; i++) 
			{
				sze = i * (datasize - 1);
				indx = sze % 512;
				buff = buff + (char)retbuff[indx];         
			}
			//*****************************************************
        
			lstOutput.Items.Add("Success Read. Data: " + buff);

			SDX_Close(handle);
		}
	}
}
