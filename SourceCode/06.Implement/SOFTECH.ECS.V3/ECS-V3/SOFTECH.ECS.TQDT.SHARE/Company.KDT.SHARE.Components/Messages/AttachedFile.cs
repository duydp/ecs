﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace Company.KDT.SHARE.Components
{
    public class AttachedFile
    {
        /// <summary>
        /// Đường dẫn file
        /// </summary>
        [XmlElement("fileName")]
        public string FileName { get; set; }
        /// <summary>
        /// Nội dung
        /// </summary>
        [XmlElement("content")]
        public Content Content { get; set; }
        
    }
}
