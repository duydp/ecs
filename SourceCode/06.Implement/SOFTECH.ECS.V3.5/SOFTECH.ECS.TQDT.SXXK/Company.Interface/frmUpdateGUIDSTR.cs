﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface
{
    public partial class frmUpdateGUIDSTR : BaseForm
    {
        private string table = string.Empty;
        private string guidstr = string.Empty;
        private int id = 0;
        public frmUpdateGUIDSTR()
        {
            InitializeComponent();
        }
        public frmUpdateGUIDSTR(string table, string guidstr, int id) 
        {
            InitializeComponent();
            this.table = table;
            this.guidstr = guidstr;
            this.id = id;
        }
        private void btnDo_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtGUIDSTR.Text == "")
                {
                    ShowMessage("Bạn chưa nhập chuỗi phản hồi.",false);
                }
                else
                {
                    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                    {
                        connection.Open();
                        SqlTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand("p_KDT_UpdateGUIDSTR");
                            db.AddInParameter(dbCommand, "@table", SqlDbType.VarChar, table);
                            db.AddInParameter(dbCommand, "@guidstr", SqlDbType.NVarChar, "'" + txtGUIDSTR.Text + "'");
                            db.AddInParameter(dbCommand, "@where", SqlDbType.VarChar, "ID = " + id);
                            db.ExecuteNonQuery(dbCommand);

                            transaction.Commit();
                            ShowMessage("Cập nhập thành công",false);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                        }
                        finally
                        {
                            connection.Close();

                            Cursor = Cursors.Default;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }            
        }
    }
}
