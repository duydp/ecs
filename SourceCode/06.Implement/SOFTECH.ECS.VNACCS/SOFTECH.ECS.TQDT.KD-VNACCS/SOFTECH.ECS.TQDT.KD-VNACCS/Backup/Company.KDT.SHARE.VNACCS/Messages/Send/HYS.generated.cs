using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class HYS : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute TSB { get; set; }
public PropertiesAttribute STL { get; set; }
public PropertiesAttribute REF { get; set; }
public PropertiesAttribute KIJ { get; set; }

public HYS()
        {
CH = new PropertiesAttribute(6, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
TSB = new PropertiesAttribute(3, typeof(string));
STL = new PropertiesAttribute(20, typeof(string));
REF = new PropertiesAttribute(20, typeof(string));
KIJ = new PropertiesAttribute(996, typeof(string));
TongSoByte = 1459;
}

}public partial class EnumGroupID
    {
}

}
