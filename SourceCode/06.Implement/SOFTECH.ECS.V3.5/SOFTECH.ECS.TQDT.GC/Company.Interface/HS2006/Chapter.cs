using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;

namespace HaiQuan.HS
{
	public class Chapter
	{
		public OleDbDataReader SelectBySection(string section_ID)
		{
			string query = string.Format("SELECT * FROM t_Chapters WHERE Section_ID = '{0}' ORDER BY ID", section_ID);
			return OleDbHelper.ExecuteReader(Global.ConnectionString, CommandType.Text, query);//, parameters);
		}
	}
}
