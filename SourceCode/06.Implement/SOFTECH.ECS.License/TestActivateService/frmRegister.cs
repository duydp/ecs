﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;

namespace TestActivateService
{
    public partial class frmRegister : Form
    {
        private string ProductID;

        public frmRegister()
        {
            InitializeComponent();
        }

        private void frmRegister_Load(object sender, EventArgs e)
        {
            try
            {
                txtCodeActivate.Text = Program.getProcessorID();
                ProductID = this.getProductID();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra, vui lòng liên hệ với nhà cung cấp.");
                this.Close();
            }
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            if (this.validForm())
            {
                if (this.doActivate())
                {
                    if (MessageBox.Show("Kích hoạt thành công!\nHãy khởi động lại chương trình để hoàn thành việc kích hoạt", "MSG_0203085", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                    {
                        Application.Restart();
                    }
                    else
                    {
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Kích hoạt không thành công : \nThông tin kích hoạt không chính xác.");
                }
            }
            else
            {
                MessageBox.Show("Dữ liệu chưa hợp lệ, hãy kiểm tra lại");
            }
        }

        private bool doActivate()
        {
            bool val = false;

            if (radioCDKey.Checked)
            {
                try
                {
                    string[] InfoActivation = Program.lic.Activate(txtCDKey.Text, this.ProductID, txtCodeActivate.Text);
                    if (Boolean.Parse(InfoActivation[0]))
                    {
                        if (this.saveConfig(InfoActivation))
                        {
                            val = true;
                        }
                        else
                        {
                            MessageBox.Show("Có lỗi dưới Client!\n\nVui lòng kích hoạt lại.");
                        }
                    }
                    else
                    {
                        MessageBox.Show(InfoActivation[1]);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Có lỗi dưới khi kích hoạt! \n\nVui lòng kiểm tra kết nối internet");
                }
            }
            else
            {
                try
                {
                    List<License> listLic = Program.lic.LoadListFromFile(txtFile.Text);
                    foreach (License lic in listLic)
                    {
                        if (lic.codeActivate == Program.getCodeToActivate())
                        {
                            Program.lic = lic;
                            Program.lic.Save();
                            val = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Tệp tin không đúng phiên bản.");
                }
            }

            return val;
        }

        private bool validForm()
        {
            bool value = false;
            if (radioCDKey.Checked)
            {
                if (txtCDKey.Text.Trim().Length == 25)
                {
                    value = true;
                }
            }
            else
            {
                if (txtFile.Text.Length > 0 && File.Exists(txtFile.Text))
                {
                    value = true;
                }
            }
            return value;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool saveConfig(string[] licenseInfo)
        {
            bool value = false;
            try
            {
                Program.lic.codeActivate = Program.getCodeToActivate();
                Program.lic.licenseName = licenseInfo[1];
                Program.lic.numberClient = licenseInfo[2];
                Program.lic.dayExpires = licenseInfo[3];
                //Program.lic.dateTrial = (System.Convert.ToDateTime(licenseInfo[3]) - System.DateTime.Today).Days.ToString();
                Program.lic.Save();

                value = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return value;
        }

        private string getProductID()
        {
            string val;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                val = config.AppSettings.Settings["ProductID"].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return val;
        }

        private void txtFile_ButtonClick(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "ECS Licenses | *.lic";
            if (od.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = od.FileName;
            }
            od.Dispose();
        }

        private void radioCDKey_CheckedChanged(object sender, EventArgs e)
        {
            txtCDKey.Enabled = radioCDKey.Checked;
            txtFile.Enabled = !radioCDKey.Checked;
        }
    }
}
