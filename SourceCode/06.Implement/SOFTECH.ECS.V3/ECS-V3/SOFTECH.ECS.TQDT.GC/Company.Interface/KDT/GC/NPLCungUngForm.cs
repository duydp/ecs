﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
namespace Company.Interface.KDT.GC
{
    public partial class NPLCungUngForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        private NPLCungUng nplCungUng = new NPLCungUng();
        bool isEdit = false;
        public NPLCungUngForm()
        {
            InitializeComponent();
        }
        private void KhoiTaoDuLieuChuan()
        {
            //this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            //cbDonViTinh.DataSource = this._DonViTinh;
            //cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            //txtMaHS.AutoCompleteCustomSource = col;
            SetCommandStatus();
        }
        
        private void NPLCungUngForm_Load(object sender, EventArgs e)
        {
            KhoiTaoDuLieuChuan();
            dgList.DataSource = TKMD.NPLCungUngs;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["DVT_ID"].Value != null)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);

        }

        private void SetCommandStatus()
        {
            bool isEnable = TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET
               || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY;
            btnChiTiet.Enabled = btnXoa.Enabled = isEnable;
            dgList.AllowDelete = isEnable ? InheritableBoolean.True : InheritableBoolean.False;
        }
     
        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                GridEXRow r = dgList.GetRow();
                if (r.DataRow == null) return;
                else nplCungUng = (NPLCungUng)r.DataRow;
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không?", true) != "Yes") return;
                if (nplCungUng.ID > 0)
                {
                    nplCungUng.Delete();
                    NPLCungUngDetail.DeleteCollection(nplCungUng.NPLCungUngDetails);
                }
                TKMD.NPLCungUngs.Remove(nplCungUng);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }


        private void btnChiTiet_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() == null)
            {
                ShowMessage("Vui lòng chọn hàng", false);
                return;
            }
            NPLCungUng nplcu = (NPLCungUng)dgList.GetRow().DataRow;
            NPLCungUngDetailForm f = new NPLCungUngDetailForm();
            f.TKMD = this.TKMD;
            f.nplCungUng = nplcu;
            f.ShowDialog();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                NPLCungUng nplcu = (NPLCungUng)dgList.GetRow().DataRow;
                NPLCungUngDetailForm f = new NPLCungUngDetailForm();
                f.TKMD = this.TKMD;
                f.nplCungUng = nplcu;
                f.ShowDialog();
            }
            catch (System.Exception ex)
            {

            }

        }

        private void btnCaculate_Click(object sender, EventArgs e)
        {
            NPLCungUngTheoTKForm f = new NPLCungUngTheoTKForm();
            f.TKMD = TKMD;
            f.ShowDialog(this);
            TKMD.LoadChungTuHaiQuan();
            dgList.DataSource = TKMD.NPLCungUngs;
            
        }

        
    }
}
