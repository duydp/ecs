
/****** Object:  Table [dbo].[t_KDT_GC_NguyenPhuLieuBoSung]    Script Date: 03/27/2013 15:28:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_GC_NguyenPhuLieuBoSung]') AND type in (N'U'))
DROP TABLE [dbo].[t_KDT_GC_NguyenPhuLieuBoSung]
GO

/****** Object:  Table [dbo].[t_KDT_GC_NguyenPhuLieuBoSung]    Script Date: 03/27/2013 15:28:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_GC_NguyenPhuLieuBoSung](
	[HopDong_ID] [bigint] NOT NULL,
	[Ma] [varchar](30) NOT NULL,
	[NguonCungCap] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](256) NULL,
	[TuCungUng] [bit] NOT NULL,
 CONSTRAINT [PK_t_KDT_GC_NguyenPhuLieuBoSung_1] PRIMARY KEY CLUSTERED 
(
	[HopDong_ID] ASC,
	[Ma] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_KDT_GC_NguyenPhuLieuBoSung] ADD  CONSTRAINT [DF_t_KDT_GC_NguyenPhuLieuBoSung_TuCungUng]  DEFAULT ((0)) FOR [TuCungUng]
GO
UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '8.5'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO
