Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SecureDongle X")> 
<Assembly: AssemblyDescription("vB.net 2003 Sample")> 
<Assembly: AssemblyCompany("SecureMetric")> 
<Assembly: AssemblyProduct("SecureDongle X")> 
<Assembly: AssemblyCopyright("Copyright (c)2008-2009 SecureMetric")> 
<Assembly: AssemblyTrademark("SecureDongle X")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("6B11415B-C03A-4D58-901C-3C1E8A997572")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 
