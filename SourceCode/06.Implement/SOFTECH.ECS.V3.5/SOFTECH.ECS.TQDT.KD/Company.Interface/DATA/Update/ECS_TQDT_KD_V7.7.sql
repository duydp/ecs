
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_t_HaiQuan_MaHS_DateCreated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[t_HaiQuan_MaHS] DROP CONSTRAINT [DF_t_HaiQuan_MaHS_DateCreated]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_t_HaiQuan_MaHS_DateModified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[t_HaiQuan_MaHS] DROP CONSTRAINT [DF_t_HaiQuan_MaHS_DateModified]
END

GO


/****** Object:  Table [dbo].[t_HaiQuan_MaHS]    Script Date: 04/04/2013 17:03:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_HaiQuan_MaHS]') AND type in (N'U'))
DROP TABLE [dbo].[t_HaiQuan_MaHS]
GO


/****** Object:  Table [dbo].[t_HaiQuan_MaHS]    Script Date: 04/04/2013 17:03:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_HaiQuan_MaHS](
[MaHang] [varchar](10) NOT NULL,
[MoTa_VN] [nvarchar](1000) NOT NULL,
[DVT] [nvarchar](50) NULL,
[MoTa_EN] [nvarchar](1000) NULL,
[MaTuongUng] [nvarchar](10) NULL,
[ThueNKUD] [float] NULL,
[ThueNKUDDB] [float] NULL,
[ThueVAT] [float] NULL,
[ThueTTDB] [float] NULL,
[ThueXK] [float] NULL,
[ThueMT] [float] NULL,
[GhiChu] [nvarchar](255) NULL,
[ChinhSachMatHang] [nvarchar](255) NULL,
[QuanLyGia] [nvarchar](255) NULL,
[ThuocNhom] [nvarchar](255) NULL,
[GiayPhepNKKTD] [nvarchar](255) NULL,
[GiayPhepNK] [nvarchar](255) NULL,
[KiemDich] [nvarchar](255) NULL,
[PhanTichNguyCoGayHai] [nvarchar](255) NULL,
[VSATTP] [nvarchar](255) NULL,
[HangHoaNhom2KTCL] [nvarchar](255) NULL,
[CamNKQSD] [nvarchar](255) NULL,
[CamNK] [nvarchar](255) NULL,
[CamXK] [nvarchar](255) NULL,
[HanNgachThueQuan] [nvarchar](255) NULL,
[QuanLyHoaChat] [nvarchar](255) NULL,
[HangTieuDungTT07] [nvarchar](255) NULL,
[ChinhSachKhac] [nvarchar](255) NULL,
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL,
[Nhom] [nvarchar](4) NULL,
[PN1] [nvarchar](2) NULL,
[PN2] [nvarchar](2) NULL,
[Pn3] [nvarchar](2) NULL,
[Mo_ta] [nvarchar](1000) NULL,
[HS10SO] [nvarchar](10) NULL,
CONSTRAINT [PK_t_HaiQuan_MaHS] PRIMARY KEY CLUSTERED 
(
[MaHang] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_HaiQuan_MaHS] ADD  CONSTRAINT [DF_t_HaiQuan_MaHS_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[t_HaiQuan_MaHS] ADD  CONSTRAINT [DF_t_HaiQuan_MaHS_DateModified]  DEFAULT (getdate()) FOR [DateModified]
GO



-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_HaiQuan_MaHS_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_MaHS_Insert]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_MaHS_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_MaHS_Update]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_MaHS_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_MaHS_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_MaHS_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_MaHS_Delete]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_MaHS_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_MaHS_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_MaHS_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_MaHS_Load]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_MaHS_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_MaHS_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Insert]
	@MaHang varchar(10),
	@MoTa_VN nvarchar(1000),
	@DVT nvarchar(50),
	@MoTa_EN nvarchar(1000),
	@MaTuongUng nvarchar(10),
	@ThueNKUD float,
	@ThueNKUDDB float,
	@ThueVAT float,
	@ThueTTDB float,
	@ThueXK float,
	@ThueMT float,
	@GhiChu nvarchar(255),
	@ChinhSachMatHang nvarchar(255),
	@QuanLyGia nvarchar(255),
	@ThuocNhom nvarchar(255),
	@GiayPhepNKKTD nvarchar(255),
	@GiayPhepNK nvarchar(255),
	@KiemDich nvarchar(255),
	@PhanTichNguyCoGayHai nvarchar(255),
	@VSATTP nvarchar(255),
	@HangHoaNhom2KTCL nvarchar(255),
	@CamNKQSD nvarchar(255),
	@CamNK nvarchar(255),
	@CamXK nvarchar(255),
	@HanNgachThueQuan nvarchar(255),
	@QuanLyHoaChat nvarchar(255),
	@HangTieuDungTT07 nvarchar(255),
	@ChinhSachKhac nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime,
	@Nhom nvarchar(4),
	@PN1 nvarchar(2),
	@PN2 nvarchar(2),
	@Pn3 nvarchar(2),
	@Mo_ta nvarchar(1000),
	@HS10SO nvarchar(10)
AS
INSERT INTO [dbo].[t_HaiQuan_MaHS]
(
	[MaHang],
	[MoTa_VN],
	[DVT],
	[MoTa_EN],
	[MaTuongUng],
	[ThueNKUD],
	[ThueNKUDDB],
	[ThueVAT],
	[ThueTTDB],
	[ThueXK],
	[ThueMT],
	[GhiChu],
	[ChinhSachMatHang],
	[QuanLyGia],
	[ThuocNhom],
	[GiayPhepNKKTD],
	[GiayPhepNK],
	[KiemDich],
	[PhanTichNguyCoGayHai],
	[VSATTP],
	[HangHoaNhom2KTCL],
	[CamNKQSD],
	[CamNK],
	[CamXK],
	[HanNgachThueQuan],
	[QuanLyHoaChat],
	[HangTieuDungTT07],
	[ChinhSachKhac],
	[DateCreated],
	[DateModified],
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
)
VALUES
(
	@MaHang,
	@MoTa_VN,
	@DVT,
	@MoTa_EN,
	@MaTuongUng,
	@ThueNKUD,
	@ThueNKUDDB,
	@ThueVAT,
	@ThueTTDB,
	@ThueXK,
	@ThueMT,
	@GhiChu,
	@ChinhSachMatHang,
	@QuanLyGia,
	@ThuocNhom,
	@GiayPhepNKKTD,
	@GiayPhepNK,
	@KiemDich,
	@PhanTichNguyCoGayHai,
	@VSATTP,
	@HangHoaNhom2KTCL,
	@CamNKQSD,
	@CamNK,
	@CamXK,
	@HanNgachThueQuan,
	@QuanLyHoaChat,
	@HangTieuDungTT07,
	@ChinhSachKhac,
	@DateCreated,
	@DateModified,
	@Nhom,
	@PN1,
	@PN2,
	@Pn3,
	@Mo_ta,
	@HS10SO
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Update]
	@MaHang varchar(10),
	@MoTa_VN nvarchar(1000),
	@DVT nvarchar(50),
	@MoTa_EN nvarchar(1000),
	@MaTuongUng nvarchar(10),
	@ThueNKUD float,
	@ThueNKUDDB float,
	@ThueVAT float,
	@ThueTTDB float,
	@ThueXK float,
	@ThueMT float,
	@GhiChu nvarchar(255),
	@ChinhSachMatHang nvarchar(255),
	@QuanLyGia nvarchar(255),
	@ThuocNhom nvarchar(255),
	@GiayPhepNKKTD nvarchar(255),
	@GiayPhepNK nvarchar(255),
	@KiemDich nvarchar(255),
	@PhanTichNguyCoGayHai nvarchar(255),
	@VSATTP nvarchar(255),
	@HangHoaNhom2KTCL nvarchar(255),
	@CamNKQSD nvarchar(255),
	@CamNK nvarchar(255),
	@CamXK nvarchar(255),
	@HanNgachThueQuan nvarchar(255),
	@QuanLyHoaChat nvarchar(255),
	@HangTieuDungTT07 nvarchar(255),
	@ChinhSachKhac nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime,
	@Nhom nvarchar(4),
	@PN1 nvarchar(2),
	@PN2 nvarchar(2),
	@Pn3 nvarchar(2),
	@Mo_ta nvarchar(1000),
	@HS10SO nvarchar(10)
AS

UPDATE
	[dbo].[t_HaiQuan_MaHS]
SET
	[MoTa_VN] = @MoTa_VN,
	[DVT] = @DVT,
	[MoTa_EN] = @MoTa_EN,
	[MaTuongUng] = @MaTuongUng,
	[ThueNKUD] = @ThueNKUD,
	[ThueNKUDDB] = @ThueNKUDDB,
	[ThueVAT] = @ThueVAT,
	[ThueTTDB] = @ThueTTDB,
	[ThueXK] = @ThueXK,
	[ThueMT] = @ThueMT,
	[GhiChu] = @GhiChu,
	[ChinhSachMatHang] = @ChinhSachMatHang,
	[QuanLyGia] = @QuanLyGia,
	[ThuocNhom] = @ThuocNhom,
	[GiayPhepNKKTD] = @GiayPhepNKKTD,
	[GiayPhepNK] = @GiayPhepNK,
	[KiemDich] = @KiemDich,
	[PhanTichNguyCoGayHai] = @PhanTichNguyCoGayHai,
	[VSATTP] = @VSATTP,
	[HangHoaNhom2KTCL] = @HangHoaNhom2KTCL,
	[CamNKQSD] = @CamNKQSD,
	[CamNK] = @CamNK,
	[CamXK] = @CamXK,
	[HanNgachThueQuan] = @HanNgachThueQuan,
	[QuanLyHoaChat] = @QuanLyHoaChat,
	[HangTieuDungTT07] = @HangTieuDungTT07,
	[ChinhSachKhac] = @ChinhSachKhac,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified,
	[Nhom] = @Nhom,
	[PN1] = @PN1,
	[PN2] = @PN2,
	[Pn3] = @Pn3,
	[Mo_ta] = @Mo_ta,
	[HS10SO] = @HS10SO
WHERE
	[MaHang] = @MaHang

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_InsertUpdate]
	@MaHang varchar(10),
	@MoTa_VN nvarchar(1000),
	@DVT nvarchar(50),
	@MoTa_EN nvarchar(1000),
	@MaTuongUng nvarchar(10),
	@ThueNKUD float,
	@ThueNKUDDB float,
	@ThueVAT float,
	@ThueTTDB float,
	@ThueXK float,
	@ThueMT float,
	@GhiChu nvarchar(255),
	@ChinhSachMatHang nvarchar(255),
	@QuanLyGia nvarchar(255),
	@ThuocNhom nvarchar(255),
	@GiayPhepNKKTD nvarchar(255),
	@GiayPhepNK nvarchar(255),
	@KiemDich nvarchar(255),
	@PhanTichNguyCoGayHai nvarchar(255),
	@VSATTP nvarchar(255),
	@HangHoaNhom2KTCL nvarchar(255),
	@CamNKQSD nvarchar(255),
	@CamNK nvarchar(255),
	@CamXK nvarchar(255),
	@HanNgachThueQuan nvarchar(255),
	@QuanLyHoaChat nvarchar(255),
	@HangTieuDungTT07 nvarchar(255),
	@ChinhSachKhac nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime,
	@Nhom nvarchar(4),
	@PN1 nvarchar(2),
	@PN2 nvarchar(2),
	@Pn3 nvarchar(2),
	@Mo_ta nvarchar(1000),
	@HS10SO nvarchar(10)
AS
IF EXISTS(SELECT [MaHang] FROM [dbo].[t_HaiQuan_MaHS] WHERE [MaHang] = @MaHang)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_MaHS] 
		SET
			[MoTa_VN] = @MoTa_VN,
			[DVT] = @DVT,
			[MoTa_EN] = @MoTa_EN,
			[MaTuongUng] = @MaTuongUng,
			[ThueNKUD] = @ThueNKUD,
			[ThueNKUDDB] = @ThueNKUDDB,
			[ThueVAT] = @ThueVAT,
			[ThueTTDB] = @ThueTTDB,
			[ThueXK] = @ThueXK,
			[ThueMT] = @ThueMT,
			[GhiChu] = @GhiChu,
			[ChinhSachMatHang] = @ChinhSachMatHang,
			[QuanLyGia] = @QuanLyGia,
			[ThuocNhom] = @ThuocNhom,
			[GiayPhepNKKTD] = @GiayPhepNKKTD,
			[GiayPhepNK] = @GiayPhepNK,
			[KiemDich] = @KiemDich,
			[PhanTichNguyCoGayHai] = @PhanTichNguyCoGayHai,
			[VSATTP] = @VSATTP,
			[HangHoaNhom2KTCL] = @HangHoaNhom2KTCL,
			[CamNKQSD] = @CamNKQSD,
			[CamNK] = @CamNK,
			[CamXK] = @CamXK,
			[HanNgachThueQuan] = @HanNgachThueQuan,
			[QuanLyHoaChat] = @QuanLyHoaChat,
			[HangTieuDungTT07] = @HangTieuDungTT07,
			[ChinhSachKhac] = @ChinhSachKhac,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified,
			[Nhom] = @Nhom,
			[PN1] = @PN1,
			[PN2] = @PN2,
			[Pn3] = @Pn3,
			[Mo_ta] = @Mo_ta,
			[HS10SO] = @HS10SO
		WHERE
			[MaHang] = @MaHang
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_MaHS]
	(
			[MaHang],
			[MoTa_VN],
			[DVT],
			[MoTa_EN],
			[MaTuongUng],
			[ThueNKUD],
			[ThueNKUDDB],
			[ThueVAT],
			[ThueTTDB],
			[ThueXK],
			[ThueMT],
			[GhiChu],
			[ChinhSachMatHang],
			[QuanLyGia],
			[ThuocNhom],
			[GiayPhepNKKTD],
			[GiayPhepNK],
			[KiemDich],
			[PhanTichNguyCoGayHai],
			[VSATTP],
			[HangHoaNhom2KTCL],
			[CamNKQSD],
			[CamNK],
			[CamXK],
			[HanNgachThueQuan],
			[QuanLyHoaChat],
			[HangTieuDungTT07],
			[ChinhSachKhac],
			[DateCreated],
			[DateModified],
			[Nhom],
			[PN1],
			[PN2],
			[Pn3],
			[Mo_ta],
			[HS10SO]
	)
	VALUES
	(
			@MaHang,
			@MoTa_VN,
			@DVT,
			@MoTa_EN,
			@MaTuongUng,
			@ThueNKUD,
			@ThueNKUDDB,
			@ThueVAT,
			@ThueTTDB,
			@ThueXK,
			@ThueMT,
			@GhiChu,
			@ChinhSachMatHang,
			@QuanLyGia,
			@ThuocNhom,
			@GiayPhepNKKTD,
			@GiayPhepNK,
			@KiemDich,
			@PhanTichNguyCoGayHai,
			@VSATTP,
			@HangHoaNhom2KTCL,
			@CamNKQSD,
			@CamNK,
			@CamXK,
			@HanNgachThueQuan,
			@QuanLyHoaChat,
			@HangTieuDungTT07,
			@ChinhSachKhac,
			@DateCreated,
			@DateModified,
			@Nhom,
			@PN1,
			@PN2,
			@Pn3,
			@Mo_ta,
			@HS10SO
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Delete]
	@MaHang varchar(10)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_MaHS]
WHERE
	[MaHang] = @MaHang

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_MaHS] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Load]
	@MaHang varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHang],
	[MoTa_VN],
	[DVT],
	[MoTa_EN],
	[MaTuongUng],
	[ThueNKUD],
	[ThueNKUDDB],
	[ThueVAT],
	[ThueTTDB],
	[ThueXK],
	[ThueMT],
	[GhiChu],
	[ChinhSachMatHang],
	[QuanLyGia],
	[ThuocNhom],
	[GiayPhepNKKTD],
	[GiayPhepNK],
	[KiemDich],
	[PhanTichNguyCoGayHai],
	[VSATTP],
	[HangHoaNhom2KTCL],
	[CamNKQSD],
	[CamNK],
	[CamXK],
	[HanNgachThueQuan],
	[QuanLyHoaChat],
	[HangTieuDungTT07],
	[ChinhSachKhac],
	[DateCreated],
	[DateModified],
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
FROM
	[dbo].[t_HaiQuan_MaHS]
WHERE
	[MaHang] = @MaHang
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaHang],
	[MoTa_VN],
	[DVT],
	[MoTa_EN],
	[MaTuongUng],
	[ThueNKUD],
	[ThueNKUDDB],
	[ThueVAT],
	[ThueTTDB],
	[ThueXK],
	[ThueMT],
	[GhiChu],
	[ChinhSachMatHang],
	[QuanLyGia],
	[ThuocNhom],
	[GiayPhepNKKTD],
	[GiayPhepNK],
	[KiemDich],
	[PhanTichNguyCoGayHai],
	[VSATTP],
	[HangHoaNhom2KTCL],
	[CamNKQSD],
	[CamNK],
	[CamXK],
	[HanNgachThueQuan],
	[QuanLyHoaChat],
	[HangTieuDungTT07],
	[ChinhSachKhac],
	[DateCreated],
	[DateModified],
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
FROM [dbo].[t_HaiQuan_MaHS] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, April 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHang],
	[MoTa_VN],
	[DVT],
	[MoTa_EN],
	[MaTuongUng],
	[ThueNKUD],
	[ThueNKUDDB],
	[ThueVAT],
	[ThueTTDB],
	[ThueXK],
	[ThueMT],
	[GhiChu],
	[ChinhSachMatHang],
	[QuanLyGia],
	[ThuocNhom],
	[GiayPhepNKKTD],
	[GiayPhepNK],
	[KiemDich],
	[PhanTichNguyCoGayHai],
	[VSATTP],
	[HangHoaNhom2KTCL],
	[CamNKQSD],
	[CamNK],
	[CamXK],
	[HanNgachThueQuan],
	[QuanLyHoaChat],
	[HangTieuDungTT07],
	[ChinhSachKhac],
	[DateCreated],
	[DateModified],
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO]
FROM
	[dbo].[t_HaiQuan_MaHS]	

GO

update t_haiquan_version  set version='7.7', notes=N'Cập nhật ma HS'