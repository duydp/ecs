﻿namespace Company.Interface.VNACCS.TollManagement
{
    partial class HP_ToKhaiNopPhiManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListVDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HP_ToKhaiNopPhiManagementForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListVDN = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.grbHangRoi = new Janus.Windows.EditControls.UIGroupBox();
            this.cbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ckbTimKiem = new Janus.Windows.EditControls.UICheckBox();
            this.grbtimkiem = new Janus.Windows.EditControls.UIGroupBox();
            this.clcTuNgay = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ucCalendar1 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbbDDNP = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.cbbLoaiHangHoa = new Janus.Windows.EditControls.UIComboBox();
            this.cbbNhomLoaiHinh = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoCT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListVDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHangRoi)).BeginInit();
            this.grbHangRoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).BeginInit();
            this.grbtimkiem.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.grbHangRoi);
            this.grbMain.Size = new System.Drawing.Size(1217, 594);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.dgListVDN);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 162);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1217, 386);
            this.uiGroupBox1.TabIndex = 298;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgListVDN
            // 
            this.dgListVDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListVDN.AlternatingColors = true;
            this.dgListVDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListVDN.ColumnAutoResize = true;
            dgListVDN_DesignTimeLayout.LayoutString = resources.GetString("dgListVDN_DesignTimeLayout.LayoutString");
            this.dgListVDN.DesignTimeLayout = dgListVDN_DesignTimeLayout;
            this.dgListVDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListVDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListVDN.FrozenColumns = 3;
            this.dgListVDN.GroupByBoxVisible = false;
            this.dgListVDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListVDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListVDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListVDN.Location = new System.Drawing.Point(3, 8);
            this.dgListVDN.Margin = new System.Windows.Forms.Padding(0);
            this.dgListVDN.Name = "dgListVDN";
            this.dgListVDN.RecordNavigator = true;
            this.dgListVDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListVDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListVDN.Size = new System.Drawing.Size(1211, 375);
            this.dgListVDN.TabIndex = 16;
            this.dgListVDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListVDN.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListVDN_RowDoubleClick);
            this.dgListVDN.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListVDN_LoadingRow);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 548);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1217, 46);
            this.uiGroupBox2.TabIndex = 11;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1125, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // grbHangRoi
            // 
            this.grbHangRoi.BackColor = System.Drawing.Color.Transparent;
            this.grbHangRoi.Controls.Add(this.cbStatus);
            this.grbHangRoi.Controls.Add(this.label3);
            this.grbHangRoi.Controls.Add(this.ckbTimKiem);
            this.grbHangRoi.Controls.Add(this.grbtimkiem);
            this.grbHangRoi.Controls.Add(this.cbbDDNP);
            this.grbHangRoi.Controls.Add(this.cbbLoaiHangHoa);
            this.grbHangRoi.Controls.Add(this.cbbNhomLoaiHinh);
            this.grbHangRoi.Controls.Add(this.txtSoToKhai);
            this.grbHangRoi.Controls.Add(this.label2);
            this.grbHangRoi.Controls.Add(this.label1);
            this.grbHangRoi.Controls.Add(this.label13);
            this.grbHangRoi.Controls.Add(this.txtSoCT);
            this.grbHangRoi.Controls.Add(this.label19);
            this.grbHangRoi.Controls.Add(this.label7);
            this.grbHangRoi.Controls.Add(this.btnSearch);
            this.grbHangRoi.Controls.Add(this.label8);
            this.grbHangRoi.Controls.Add(this.txtMa);
            this.grbHangRoi.Dock = System.Windows.Forms.DockStyle.Top;
            this.grbHangRoi.Location = new System.Drawing.Point(0, 0);
            this.grbHangRoi.Name = "grbHangRoi";
            this.grbHangRoi.Size = new System.Drawing.Size(1217, 162);
            this.grbHangRoi.TabIndex = 0;
            this.grbHangRoi.Text = "Thông tin tìm kiếm";
            this.grbHangRoi.VisualStyleManager = this.vsmMain;
            // 
            // cbStatus
            // 
            this.cbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Chưa khai báo";
            uiComboBoxItem1.Value = "-1";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Đã có số tiếp nhận";
            uiComboBoxItem2.Value = 0;
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Đã có thông báo nộp phí";
            uiComboBoxItem3.Value = "0";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Được chấp nhận";
            uiComboBoxItem4.Value = "2";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Đã thực hiện nộp phí";
            uiComboBoxItem5.Value = "3";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Đã khai báo";
            uiComboBoxItem6.Value = "-3";
            this.cbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbStatus.Location = new System.Drawing.Point(449, 91);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(206, 21);
            this.cbStatus.TabIndex = 334;
            this.cbStatus.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(345, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Trạng thái";
            // 
            // ckbTimKiem
            // 
            this.ckbTimKiem.Location = new System.Drawing.Point(666, 20);
            this.ckbTimKiem.Margin = new System.Windows.Forms.Padding(0);
            this.ckbTimKiem.Name = "ckbTimKiem";
            this.ckbTimKiem.Size = new System.Drawing.Size(15, 15);
            this.ckbTimKiem.TabIndex = 333;
            this.ckbTimKiem.VisualStyleManager = this.vsmMain;
            this.ckbTimKiem.CheckedChanged += new System.EventHandler(this.ckbTimKiem_CheckedChanged);
            // 
            // grbtimkiem
            // 
            this.grbtimkiem.Controls.Add(this.clcTuNgay);
            this.grbtimkiem.Controls.Add(this.ucCalendar1);
            this.grbtimkiem.Controls.Add(this.label9);
            this.grbtimkiem.Controls.Add(this.label5);
            this.grbtimkiem.Enabled = false;
            this.grbtimkiem.Location = new System.Drawing.Point(671, 21);
            this.grbtimkiem.Name = "grbtimkiem";
            this.grbtimkiem.Size = new System.Drawing.Size(200, 84);
            this.grbtimkiem.TabIndex = 332;
            this.grbtimkiem.VisualStyleManager = this.vsmMain;
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.Location = new System.Drawing.Point(87, 20);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.ReadOnly = false;
            this.clcTuNgay.Size = new System.Drawing.Size(90, 21);
            this.clcTuNgay.TabIndex = 8;
            this.clcTuNgay.TagName = "";
            this.clcTuNgay.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcTuNgay.WhereCondition = "";
            // 
            // ucCalendar1
            // 
            this.ucCalendar1.Location = new System.Drawing.Point(87, 50);
            this.ucCalendar1.Name = "ucCalendar1";
            this.ucCalendar1.ReadOnly = false;
            this.ucCalendar1.Size = new System.Drawing.Size(90, 21);
            this.ucCalendar1.TabIndex = 9;
            this.ucCalendar1.TagName = "";
            this.ucCalendar1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ucCalendar1.WhereCondition = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Từ ngày";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Đến ngày";
            // 
            // cbbDDNP
            // 
            this.cbbDDNP.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cbbDDNP.Appearance.Options.UseBackColor = true;
            this.cbbDDNP.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A205;
            this.cbbDDNP.Code = "";
            this.cbbDDNP.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbDDNP.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.cbbDDNP.IsOnlyWarning = false;
            this.cbbDDNP.IsValidate = true;
            this.cbbDDNP.Location = new System.Drawing.Point(115, 91);
            this.cbbDDNP.Name = "cbbDDNP";
            this.cbbDDNP.Name_VN = "";
            this.cbbDDNP.SetOnlyWarning = false;
            this.cbbDDNP.SetValidate = false;
            this.cbbDDNP.ShowColumnCode = true;
            this.cbbDDNP.ShowColumnName = true;
            this.cbbDDNP.Size = new System.Drawing.Size(206, 21);
            this.cbbDDNP.TabIndex = 5;
            this.cbbDDNP.TagCode = "";
            this.cbbDDNP.TagName = "";
            this.cbbDDNP.Where = null;
            this.cbbDDNP.WhereCondition = "";
            // 
            // cbbLoaiHangHoa
            // 
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Hàng container";
            uiComboBoxItem7.Value = "100";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Hàng lỏng, hàng rời";
            uiComboBoxItem8.Value = "101";
            this.cbbLoaiHangHoa.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbLoaiHangHoa.Location = new System.Drawing.Point(449, 22);
            this.cbbLoaiHangHoa.Name = "cbbLoaiHangHoa";
            this.cbbLoaiHangHoa.Size = new System.Drawing.Size(206, 21);
            this.cbbLoaiHangHoa.TabIndex = 2;
            this.cbbLoaiHangHoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiHangHoa.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbbNhomLoaiHinh
            // 
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Hàng tạm nhập, tái xuất, hàng chuyển khẩu, hàng gửi kho ngoại quan";
            uiComboBoxItem9.Value = "TF001";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Đối với hàng quá cảnh";
            uiComboBoxItem10.Value = "TF002";
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Đối với hàng hóa nhập khẩu, hàng hóa xuất khẩu";
            uiComboBoxItem11.Value = "TF003";
            this.cbbNhomLoaiHinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem9,
            uiComboBoxItem10,
            uiComboBoxItem11});
            this.cbbNhomLoaiHinh.Location = new System.Drawing.Point(115, 127);
            this.cbbNhomLoaiHinh.Name = "cbbNhomLoaiHinh";
            this.cbbNhomLoaiHinh.Size = new System.Drawing.Size(540, 21);
            this.cbbNhomLoaiHinh.TabIndex = 7;
            this.cbbNhomLoaiHinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbNhomLoaiHinh.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.BackColor = System.Drawing.Color.White;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(115, 55);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(206, 22);
            this.txtSoToKhai.TabIndex = 3;
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoToKhai.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(345, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Loại hàng hóa : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Điểm nộp phí :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 130);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "Nhóm loại hình :";
            // 
            // txtSoCT
            // 
            this.txtSoCT.BackColor = System.Drawing.Color.White;
            this.txtSoCT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCT.Location = new System.Drawing.Point(449, 56);
            this.txtSoCT.Name = "txtSoCT";
            this.txtSoCT.Size = new System.Drawing.Size(206, 22);
            this.txtSoCT.TabIndex = 4;
            this.txtSoCT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoCT.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(345, 60);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 14);
            this.label19.TabIndex = 0;
            this.label19.Text = "Số chứng từ :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Số tờ khai  :";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(671, 125);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(112, 23);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Mã doanh nghiệp :";
            // 
            // txtMa
            // 
            this.txtMa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Location = new System.Drawing.Point(115, 22);
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(206, 21);
            this.txtMa.TabIndex = 1;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMa.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // HP_ToKhaiNopPhiManagementForm
            // 
            this.ClientSize = new System.Drawing.Size(1217, 594);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HP_ToKhaiNopPhiManagementForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách tờ khai nộp phí";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListVDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbHangRoi)).EndInit();
            this.grbHangRoi.ResumeLayout(false);
            this.grbHangRoi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).EndInit();
            this.grbtimkiem.ResumeLayout(false);
            this.grbtimkiem.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX dgListVDN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox grbHangRoi;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private Janus.Windows.EditControls.UIComboBox cbbNhomLoaiHinh;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label7;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty cbbDDNP;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHangHoa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UICheckBox ckbTimKiem;
        private Janus.Windows.EditControls.UIGroupBox grbtimkiem;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcTuNgay;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar ucCalendar1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIComboBox cbStatus;
    }
}
