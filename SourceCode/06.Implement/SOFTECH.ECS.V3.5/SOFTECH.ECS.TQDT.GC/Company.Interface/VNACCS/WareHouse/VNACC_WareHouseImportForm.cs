﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
#endif
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.VNACCS;
//using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho;
using Company.Interface.GC.WareHouse;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
using Company.Interface.GC.WareHouse;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
#endif

namespace Company.Interface.VNACCS.WareHouse
{
    public partial class VNACC_WareHouseImportForm : BaseFormHaveGuidPanel
    {
        public T_KDT_VNACCS_WarehouseImport wareHouseImport = new T_KDT_VNACCS_WarehouseImport();
        public T_KDT_VNACCS_WarehouseImport_Detail wareHouseDetail;
        public T_KDT_VNACCS_WarehouseImport_GoodsDetail wareHouseGoodDetial;
        public List<T_KDT_VNACCS_WarehouseImport_Detail> wareHouseDetailCollection = new List<T_KDT_VNACCS_WarehouseImport_Detail>();
        public List<T_KDT_VNACCS_WarehouseImport_GoodsDetail> wareHouseGoodDetaiCollection = new List<T_KDT_VNACCS_WarehouseImport_GoodsDetail>();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        KDT_VNACC_ToKhaiMauDich TKMD;
#if GC_V4
        public HopDong HD;
#endif
        public VNACC_WareHouseImportForm()
        {
            InitializeComponent();
        }

        private void VNACC_WareHouseImportForm_Load(object sender, EventArgs e)
        {
            try
            {
#if GC_V4
                BindHopDong();
#elif SXXK_V4
                grbHopDong.Visible = false;
#endif
                cbbLoaiCT.SelectedIndex = 0;
                if (wareHouseImport.ID == 0)
                {
                    wareHouseImport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                    txtMaDN.Text = GlobalSettings.MA_DON_VI;
                }
                else
                {
                    wareHouseImport = T_KDT_VNACCS_WarehouseImport.Load(wareHouseImport.ID);
                    wareHouseImport.WarehouseImportDetailsCollection = T_KDT_VNACCS_WarehouseImport_Detail.SelectCollectionBy_WarehouseImport_ID(wareHouseImport.ID);
                    SetWareHouseImport();
                }
                BindData();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindHopDong()
        {
#if GC_V4
            HopDong hd = new HopDong();
            hd.SoHopDong = "";
            hd.ID = 0;
            List<HopDong> collection;
            {
                string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                try
                {
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                    collection.Add(hd);
                    cbbSoHD.DataSource = collection;
                    cbbSoHD.DisplayMember = "SoHopDong";
                    cbbSoHD.ValueMember = "ID";
                    cbbSoHD.SelectedIndex = collection.Count - 1;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
#endif

        }
        private void SetCommandStatus()
        {
            if (wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseImport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseImport.NgayTN;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                dtpNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseImport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseImport.NgayTN;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseImport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseImport.NgayTN;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseImport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseImport.NgayTN;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseImport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseImport.NgayTN;
                lblTrangThai.Text = setText("Chờ duyệt", "Wait approved");
                this.OpenType = OpenFormType.View;
            }

        }
        private void cmMainPNK_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdSelect":
                    this.SelectWareHouseImport();
                    break;
                case "cmdAddExcel":
                    this.ImportExcel();
                    break;
                case "cmdSend":
                    this.SendV5("Send");
                    break;
                case "cmdCancel":
                    if (ShowMessage("Doanh nghiệp có chắc chắn muốn khai báo hủy phiếu nhập kho này đến HQ không ? ", true) == "Yes")
                    {
                        wareHouseImport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        wareHouseImport.Update();
                        SetCommandStatus();
                        this.SendV5("Cancel");
                    }
                    break;
                case "cmdSendEdit":
                    this.SendV5("Edit");
                    break;
                case "cmdFeedback":
                    this.FeedbackV5();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdEdit":
                    this.Edit();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGuidString();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
                default:
                    break;
            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = wareHouseImport.ID;
                f.loaiKhaiBao = LoaiKhaiBao.WareHouseImport;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", wareHouseImport.ID, LoaiKhaiBao.WareHouseImport), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageWarehouseImport(wareHouseImport);
                    wareHouseImport.InsertUpdateFull();
                    ShowMessage("Cập nhật thông tin thành công .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SelectWareHouseImport()
        {
            try
            {
                string whereCondition = "";
                string LoaiChungTu = "N";
                WareHouseManager f = new WareHouseManager();
                f.IsShowChonPXNK = true;
                f.whereCondition = whereCondition;
                f.LoaiChungTu = LoaiChungTu;
#if GC_V4
                if (HD != null)
                {
                    f.hd = HD;
                }
#endif
                f.ShowDialog(this);
                if (f.PhieuXNKhoCollectionSelected.Count >= 1)
                {
                    foreach (T_KHOKETOAN_PHIEUXNKHO PhieuXNK in f.PhieuXNKhoCollectionSelected)
                    {
                        if (PhieuXNK.LOAIHANGHOA == "N")
                        {
                            // Phiếu nhập kho NPL
                            T_KDT_VNACCS_WarehouseImport_Detail wareHouseDetail = new T_KDT_VNACCS_WarehouseImport_Detail();
                            wareHouseDetail.SoPhieuNhap = PhieuXNK.SOCT;
                            wareHouseDetail.NgayPhieuNhap = PhieuXNK.NGAYCT;
                            wareHouseDetail.MaNguoiGiaoHang = PhieuXNK.MADN;
                            wareHouseDetail.TenNguoiGiaoHang = PhieuXNK.TENDN;
                            foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in PhieuXNK.HangCollection)
                            {
                                T_KDT_VNACCS_WarehouseImport_GoodsDetail wareHouseGoodDetail = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
                                wareHouseGoodDetail.MaHangHoa = item.MAHANGMAP;
                                wareHouseGoodDetail.TenHangHoa = item.TENHANGMAP;
                                wareHouseGoodDetail.DVT = item.DVTMAP.ToString();
                                //wareHouseGoodDetail.MaDinhDanhSX = item.MaDinhDanhSX;
                                wareHouseGoodDetail.LoaiHangHoa = 1;
                                wareHouseGoodDetail.NguonNhap = 3;
                                wareHouseGoodDetail.SoLuongDuKienNhap = Convert.ToDecimal(item.SOLUONG);
                                wareHouseGoodDetail.SoLuongThucNhap = Convert.ToDecimal(item.SOLUONG);
                                wareHouseDetail.Collection.Add(wareHouseGoodDetail);
                            }
                            wareHouseImport.WarehouseImportDetailsCollection.Add(wareHouseDetail);

                        }
                        else if (PhieuXNK.LOAIHANGHOA == "S")
                        {
                            // Phiếu nhập kho SP
                            T_KDT_VNACCS_WarehouseImport_Detail wareHouseDetail = new T_KDT_VNACCS_WarehouseImport_Detail();
                            wareHouseDetail.SoPhieuNhap = PhieuXNK.SOCT;
                            wareHouseDetail.NgayPhieuNhap = PhieuXNK.NGAYCT;
                            wareHouseDetail.MaNguoiGiaoHang = PhieuXNK.MADN;
                            wareHouseDetail.TenNguoiGiaoHang = PhieuXNK.TENDN;
                            foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in PhieuXNK.HangCollection)
                            {
                                T_KDT_VNACCS_WarehouseImport_GoodsDetail wareHouseGoodDetail = new T_KDT_VNACCS_WarehouseImport_GoodsDetail();
                                wareHouseGoodDetail.MaHangHoa = item.MAHANGMAP;
                                wareHouseGoodDetail.TenHangHoa = item.TENHANGMAP;
                                wareHouseGoodDetail.DVT = item.DVTMAP.ToString();
                                //wareHouseGoodDetail.MaDinhDanhSX = item.MaDinhDanhSX;
                                wareHouseGoodDetail.LoaiHangHoa = 1;
                                wareHouseGoodDetail.NguonNhap = 3;
                                wareHouseGoodDetail.SoLuongDuKienNhap = Convert.ToDecimal(item.SOLUONG);
                                wareHouseGoodDetail.SoLuongThucNhap = Convert.ToDecimal(item.SOLUONG);
                                wareHouseDetail.Collection.Add(wareHouseGoodDetail);
                            }
                            wareHouseImport.WarehouseImportDetailsCollection.Add(wareHouseDetail);
                        }
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void UpdateGuidString()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_WarehouseExport", "", Convert.ToInt32(wareHouseImport.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Edit()
        {
            try
            {
                if (ShowMessage("Doanh nghiệp có chắc chắn muốn chuyển phiếu nhập kho này sang sửa không ? ", true) == "Yes")
                {
                    wareHouseImport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    wareHouseImport.Update();
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMaDN, errorProvider, "MÃ DOANH NGHIỆP ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDN, errorProvider, "TÊN DOANH NGHIỆP ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaKho, errorProvider, "MÃ KHO", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenKho, errorProvider, "TÊN KHO", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiCT, errorProvider, "LOẠI CHỨNG TỪ XNK / TỜ KHAI ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoTKCT, errorProvider, "SỐ CHỨNG TỪ XNK / TỜ KHAI", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (wareHouseImport.WarehouseImportDetailsCollection.Count == 0)
                {
                    ShowMessage("Doanh nghiệp chưa thêm danh sách phiếu nhập kho ", false);
                    return;
                }
                GetWareHouseImport();
                wareHouseImport.InsertUpdateFull();
                ShowMessage("Lưu thành công .", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Thông báo lỗi\r\n" + ex, false);
            }
        }
        private void GetWareHouseImport()
        {
            try
            {
                wareHouseImport.SoTN = Convert.ToInt32(txtSoTN.Text);
                wareHouseImport.NgayTN = dtpNgayTN.Value;
                wareHouseImport.MaHQ = GlobalSettings.MA_HAI_QUAN;
                wareHouseImport.MaDoanhNghiep = txtMaDN.Text;
                wareHouseImport.TenDoanhNghiep = txtTenDN.Text;
                wareHouseImport.NgayBatDauBC = dtpNgayBatDau.Value;
                wareHouseImport.NgayKetthucBC = dtpNgayKetThuc.Value;
                wareHouseImport.MaKho = txtMaKho.Text;
                wareHouseImport.TenKho = txtTenKho.Text;
                wareHouseImport.Loai = Convert.ToDecimal(cbbLoaiCT.SelectedValue.ToString());
                wareHouseImport.SoTKChungTu = txtSoTKCT.Text;
                wareHouseImport.SoHopDong = cbbSoHD.Text.ToString();
                wareHouseImport.NgayHopDong = clcNgayHD.Value;
                wareHouseImport.NgayHetHanHD = clcNgayHH.Value;
                wareHouseImport.MaHQTiepNhanHD = ctrCoQuanHQ.Ma;
                wareHouseImport.GhiChuKhac = txtGhiChu.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetWareHouseImport()
        {
            try
            {
                txtSoTN.Text = wareHouseImport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseImport.NgayTN;
                txtMaDN.Text = wareHouseImport.MaDoanhNghiep;
                txtTenDN.Text = wareHouseImport.TenDoanhNghiep;
                dtpNgayBatDau.Value = wareHouseImport.NgayBatDauBC;
                dtpNgayKetThuc.Value = wareHouseImport.NgayKetthucBC;
                txtMaKho.Text = wareHouseImport.MaKho;
                txtTenKho.Text = wareHouseImport.TenKho;
                cbbLoaiCT.SelectedValue = wareHouseImport.Loai.ToString();
                txtSoTKCT.Text = wareHouseImport.SoTKChungTu;
                cbbSoHD.Text = wareHouseImport.SoHopDong;
                clcNgayHD.Value = wareHouseImport.NgayHopDong;
                clcNgayHH.Value = wareHouseImport.NgayHetHanHD;
                ctrCoQuanHQ.Ma = wareHouseImport.MaHQTiepNhanHD;
                txtGhiChu.Text = wareHouseImport.GhiChuKhac;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Add()
        {
            try
            {
                VNACC_WareHouseImportGoodsForm f = new VNACC_WareHouseImportGoodsForm();
                f.TKMD = TKMD;
#if GC_V4
                f.HD = HD;
#endif
                f.wareHouseImport = wareHouseImport;
                f.wareHouseDetailCollection = wareHouseDetailCollection;
                f.OpenType = this.OpenType;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = wareHouseImport.WarehouseImportDetailsCollection;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ImportExcel()
        {
#if GC_V4
            try
            {
                HD = HopDong.Load(Convert.ToInt32(cbbSoHD.SelectedValue.ToString()));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
#endif
 
        }
        private void SendV5(string Status)
        {
            if (ShowMessage("Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (wareHouseImport.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    wareHouseImport = T_KDT_VNACCS_WarehouseImport.Load(wareHouseImport.ID);
                    wareHouseImport.WarehouseImportDetailsCollection = T_KDT_VNACCS_WarehouseImport_Detail.SelectCollectionBy_WarehouseImport_ID(wareHouseImport.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.WareHouseImport;
                sendXML.master_id = wareHouseImport.ID;

                if (sendXML.Load())
                {
                    if (Status=="Send")
                    {
                        if (wareHouseImport.TrangThaiXuLy==TrangThaiXuLy.CHO_DUYET)
                        {
                            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;   
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    wareHouseImport.GuidStr = Guid.NewGuid().ToString();
                    GC_ImportWareHouse importWareHouse = new GC_ImportWareHouse();
                    importWareHouse = Mapper_V4.ToDataTransferImportWareHouseNew(wareHouseImport, wareHouseDetail, wareHouseGoodDetial, GlobalSettings.TEN_DON_VI,Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = wareHouseImport.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(wareHouseImport.MaHQ)),
                              Identity = wareHouseImport.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = importWareHouse.Issuer,
                              //Function = DeclarationFunction.SUA,
                              Reference = wareHouseImport.GuidStr,
                          },
                          importWareHouse
                        );
                    if (Status=="Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                    }
                    else if (Status=="Edit")
                    {
                         msgSend.Subject.Function = DeclarationFunction.SUA;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                    }
                    //wareHouseImport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(wareHouseImport.ID, MessageTitle.RegisterWareHouseImport);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.WareHouseImport;
                        sendXML.master_id = wareHouseImport.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            wareHouseImport.Update();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackV5();
                            SetCommandStatus();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(wareHouseImport.ID, MessageTitle.RegisterWareHouseImport);
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        private void FeedbackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = wareHouseImport.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.ImportWareHouse,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.ImportWareHouse,
                };
                subjectBase.Type = DeclarationIssuer.ImportWareHouse;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = wareHouseImport.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(wareHouseImport.MaHQ.Trim())),
                                                  Identity = wareHouseImport.MaHQ
                                              }, subjectBase, null);
                if (wareHouseImport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(wareHouseImport.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        wareHouseImport.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        wareHouseImport.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = wareHouseImport.ID;
                form.DeclarationIssuer = DeclarationIssuer.ImportWareHouse;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void InPhieuTN()
        {
            try
            {
                if (this.wareHouseImport.SoTN == 0) return;
#if GC_V4
                Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
#elif SXXK_V4
                Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
#endif
                phieuTN.phieu = "PHIẾU NHẬP KHO";
                phieuTN.soTN = wareHouseImport.SoTN.ToString();
                phieuTN.ngayTN = wareHouseImport.NgayTN.ToString("dd/MM/yyyy");
                phieuTN.maHaiQuan = wareHouseImport.MaHQ;
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgList.GetRows().Length < 0) return;

                T_KDT_VNACCS_WarehouseImport_Detail wareHouseDetail = new T_KDT_VNACCS_WarehouseImport_Detail();
                wareHouseDetail = (T_KDT_VNACCS_WarehouseImport_Detail)dgList.CurrentRow.DataRow;
                VNACC_WareHouseImportGoodsForm f = new VNACC_WareHouseImportGoodsForm();
                f.wareHouseImport = wareHouseImport;
                f.wareHouseDetail = wareHouseDetail;
                f.wareHouseDetailCollection = wareHouseDetailCollection;
                f.ShowDialog(this);
                BindData();
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ImportWareHouseSendHandler(wareHouseImport, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void txtMaKho_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                WareHouseRegisterManagementForm f = new WareHouseRegisterManagementForm();
                f.isBrowser = true;
                f.ShowDialog(this);
                if (f.kho != null)
                {
                    txtMaKho.Text = f.kho.MAKHO;
                    txtTenKho.Text = f.kho.TENKHO;   
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaDN_TextChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoTKCT_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (cbbLoaiCT.SelectedValue.ToString()=="1")
                {
                    VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
                    f.IsShowChonToKhai = true;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.OK)
                    {
                        TKMD = f.TKMDDuocChon;
                        txtSoTKCT.Text = TKMD.SoToKhai.ToString();
#if GC_V4
                        HopDong HD = HopDong.Load(TKMD.HopDong_ID);
                        ctrCoQuanHQ.Ma = HD.MaHaiQuan;
                        cbbSoHD.Text = HD.SoHopDong;
                        clcNgayHD.Value = HD.NgayDangKy;
                        clcNgayHH.Value = HD.NgayHetHan;
#endif
                    }
                }
                else
                {
                    ShowMessageTQDT("Doanh nghiệp chưa chọn Loại chứng từ là Tờ khai XNK.", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void cbbSoHD_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
#if GC_V4
                HD = HopDong.Load(Convert.ToInt64(cbbSoHD.SelectedValue.ToString()));
                clcNgayHD.Value = HD.NgayDangKy;
                clcNgayHH.Value = HD.NgayHetHan;
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
