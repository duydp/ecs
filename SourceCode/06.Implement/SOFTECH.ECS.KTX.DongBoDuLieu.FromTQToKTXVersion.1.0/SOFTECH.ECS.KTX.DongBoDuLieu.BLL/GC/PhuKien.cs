using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.KDT.SHARE.Components.Utils;

namespace DongBoDuLieu.BLL.GC
{
    public class PK
    {
        #region Lay Du Lieu Tu Hai Quan
        public static DataSet GetDataSetPhuKien(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {

            string sql = "select distinct So_HD,Ma_HQHD,DVGC,Ngay_Ky,So_PK,Ngay_PK,So_TN,Ngay_TN,Nguoi_Duyet,VBCP  from DPKHDGC where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' ORDER BY Ngay_PK";
            DataSet dsPhuKienDK = WebServiceDB.getDataBySQL(sql.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsPhuKienDK;

            //TODO: PhuKien
            /*
            StringBuilder queryPK = new StringBuilder();
            queryPK.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryPK.Append(" SELECT DISTINCT");
            queryPK.Append("  So_HD ,");
            queryPK.Append("  Ma_HQHD ,");
            queryPK.Append("  DVGC ,");
            queryPK.Append("  Ngay_Ky ,");
            queryPK.Append("  So_PK ,");
            queryPK.Append("  Ngay_PK ,");
            queryPK.Append("  So_TN ,");
            queryPK.Append("  Ngay_TN ,");
            queryPK.Append("  Nguoi_Duyet ,");
            queryPK.Append("  VBCP");
            queryPK.Append(" FROM    DPKHDGC ");
            queryPK.Append(" WHERE Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            queryPK.Append(" and So_HD=''" + HD.SoHopDong + "''");
            queryPK.Append(" ORDER BY Ngay_PK");
            queryPK.Append(" ') AS a;");

            DataSet dsPhuKienDK = WebServiceDB.getDataBySQL(queryPK.ToString().ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsPhuKienDK;
            */
        }

        public static DataSet GetDataSetPhuKienByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string matKhau)
        {
            string sql = "select distinct So_HD,Ma_HQHD,DVGC,Ngay_Ky,So_PK,Ngay_PK,So_TN,Ngay_TN,Nguoi_Duyet,VBCP  from DPKHDGC where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' " +
                         " and Ngay_PK>='" + NgayBatDau.ToString("MM/dd/yyyy") + "' and Ngay_PK<='" + NgayKetThuc.ToString("MM/dd/yyyy") + "'  ORDER BY Ngay_PK ";
            DataSet dsPhuKienDK = WebServiceDB.getDataBySQL(sql.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsPhuKienDK;

        }

        public static DataSet GetDataSetALLLoaiPhuKien(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {

            string sql = "select * from DPKHDGC  where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "'";
            DataSet ds = WebServiceDB.getDataBySQL(sql.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return ds;

            //TODO: PhuKien
            /*
            StringBuilder queryPK = new StringBuilder();
            queryPK.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryPK.Append(" SELECT * ");
            queryPK.Append(" FROM    DPKHDGC ");
            queryPK.Append(" WHERE Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            queryPK.Append(" and So_HD=''" + HD.SoHopDong + "''");
            queryPK.Append(" ORDER BY Ngay_PK");
            queryPK.Append(" ') AS a;");

            DataSet ds = WebServiceDB.getDataBySQL(queryPK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return ds;
            */
        }

        public static DataSet GetDataSetLoaiPhuKienByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string matKhau)
        {

            string sql = "select * from DPKHDGC  where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "'" +
                        " and Ngay_PK>='" + NgayBatDau.ToString("MM/dd/yyyy") + "' and Ngay_PK<='" + NgayKetThuc.ToString("MM/dd/yyyy") + "'";
            DataSet ds = WebServiceDB.getDataBySQL(sql.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return ds;

            //TODO: PhuKien
            /*
            StringBuilder queryPK = new StringBuilder();
            queryPK.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryPK.Append(" SELECT * ");
            queryPK.Append(" FROM    DPKHDGC ");
            queryPK.Append(" WHERE Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            queryPK.Append(" and So_HD=''" + HD.SoHopDong + "''");
            queryPK.Append(" and Ngay_PK >= ''" + NgayBatDau.ToString("MM/dd/yyyy") + "'' and Ngay_PK <= ''" + NgayKetThuc.ToString("MM/dd/yyyy") + "''");
            queryPK.Append(" ORDER BY Ngay_PK");
            queryPK.Append(" ') AS a;");

            DataSet ds = WebServiceDB.getDataBySQL(queryPK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return ds;
            */
        }

        public static DataSet GetDataSetHangPhuKien(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {

            string sql = "select * from DHangPKHD  where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "'";
            DataSet dsHangPK = WebServiceDB.getDataBySQL(sql.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsHangPK;

            //TODO: PhuKien
            /*
            StringBuilder query = new StringBuilder();
            query.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            query.Append(" SELECT dp.* ");
            query.Append(" FROM DHangPKHD dp ");
            query.Append(" INNER JOIN DPKHDGC d ON d.So_HD = dp.So_HD AND d.Ma_HQHD = dp.Ma_HQHD ");
            query.Append(" AND d.DVGC = dp.DVGC AND d.Ngay_Ky = dp.Ngay_Ky AND d.So_PK = dp.So_PK AND d.Ma_PK = dp.Ma_PK ");
            query.Append(" WHERE dp.Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND dp.DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            query.Append(" and dp.So_HD=''" + HD.SoHopDong + "''");
            query.Append(" ORDER BY Ngay_PK");
            query.Append(" ') AS a;");

            DataSet dsHangPK = WebServiceDB.getDataBySQL(query.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsHangPK;
            */
        }

        public static DataSet GetDataSetHangPhuKienByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string matKhau)
        {
            string sql = "SELECT dp.* " +
                        "FROM DHangPKHD dp  " +
                        "INNER JOIN DPKHDGC d ON d.So_HD = dp.So_HD AND d.Ma_HQHD = dp.Ma_HQHD " +
                        "AND d.DVGC = dp.DVGC AND d.Ngay_Ky = dp.Ngay_Ky AND d.So_PK = dp.So_PK AND d.Ma_PK = dp.Ma_PK " +
                        "AND dp.So_HD='" + HD.SoHopDong + "' AND dp.Ma_HQHD='" + HD.MaHaiQuan + "' AND dp.DVGC='" + HD.MaDoanhNghiep + "' AND d.Ngay_PK>='" + NgayBatDau.ToString("MM/dd/yyyy") + "' and d.Ngay_PK<='" + NgayKetThuc.ToString("MM/dd/yyyy") + "'";
            DataSet dsHangPK = WebServiceDB.getDataBySQL(sql.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsHangPK;
        }
        #endregion Lay Du Lieu Tu Hai Quan

        #region Cap nhat du lieu
        public static void InsertGhiDeALLPhuKien(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsPhuKienDK, DataSet dsLoaiPK, DataSet dsHangPK)
        {
            try
            {
                Company.GC.BLL.KDT.GC.PhuKienDangKyCollection pkdkCollection = new Company.GC.BLL.KDT.GC.PhuKienDangKyCollection();
                if (dsPhuKienDK != null && dsPhuKienDK.Tables[0].Rows.Count > 0)
                {
                    System.Globalization.NumberFormatInfo infoNumber = new System.Globalization.CultureInfo("vi-VN").NumberFormat;
                    foreach (DataRow row in dsPhuKienDK.Tables[0].Rows)
                    {
                        string sohopdong = row["So_HD"].ToString().Trim();
                        Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
                        pkdk.MaDoanhNghiep = HD.MaDoanhNghiep;
                        pkdk.MaHaiQuan = HD.MaHaiQuan;
                        pkdk.NgayPhuKien = Convert.ToDateTime(row["Ngay_PK"]);
                        pkdk.NgayTiepNhan = Convert.ToDateTime(row["Ngay_TN"]);
                        pkdk.NguoiDuyet = FontConverter.TCVN2Unicode(row["Nguoi_Duyet"].ToString());
                        pkdk.SoPhuKien = row["So_PK"].ToString().Trim();
                        pkdk.SoTiepNhan = Convert.ToInt64(row["So_TN"].ToString());
                        pkdk.TrangThaiXuLy = 1;
                        pkdk.VanBanChoPhep = FontConverter.TCVN2Unicode(row["VBCP"].ToString().Trim());
                        pkdk.HopDong_ID = HD.ID;
                        pkdk.TrangThaiXuLy = 1;

                        string st = "DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and So_PK='" + pkdk.SoPhuKien + "'";
                        DataRow[] rowLoaiPK = dsLoaiPK.Tables[0].Select(st);
                        foreach (DataRow rowLoai in rowLoaiPK)
                        {
                            Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                            LoaiPK.MaPhuKien = rowLoai["Ma_PK"].ToString();
                            LoaiPK.NoiDung = FontConverter.TCVN2Unicode(rowLoai["Noi_Dung"].ToString());
                            LoaiPK.ThongTinCu = rowLoai["Old_Info"].ToString();
                            if (LoaiPK.MaPhuKien != "H06")
                                LoaiPK.ThongTinMoi = rowLoai["New_Info"].ToString();
                            else
                            {
                                System.Globalization.DateTimeFormatInfo infoDate = new CultureInfo("en-US").DateTimeFormat;
                                LoaiPK.ThongTinMoi = rowLoai["New_Info"].ToString() != "" ? Convert.ToDateTime(rowLoai["New_Info"].ToString(), infoDate).ToString("dd/MM/yyyy") : "";
                            }

                            LoaiPK.HPKCollection = new Company.GC.BLL.KDT.GC.HangPhuKienCollection();
                            DataRow[] rowHangLoaiPK = dsHangPK.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and So_PK='" + pkdk.SoPhuKien + "' and Ma_PK='" + LoaiPK.MaPhuKien + "'");
                            foreach (DataRow rowHang in rowHangLoaiPK)
                            {
                                Company.GC.BLL.KDT.GC.HangPhuKien hangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                                if (rowHang["DonGia"].ToString() != "")
                                    hangPK.DonGia = Convert.ToDouble(rowHang["DonGia"]);
                                hangPK.DVT_ID = rowHang["Ma_DVT"].ToString();
                                if (LoaiPK.MaPhuKien != "S15")
                                    hangPK.MaHang = rowHang["P_Code"].ToString().Substring(1).Trim();
                                else
                                    hangPK.MaHang = rowHang["P_Code"].ToString().Trim();
                                hangPK.MaHS = rowHang["HS_Code"].ToString().Trim();
                                hangPK.NguyenTe_ID = rowHang["NGTe"].ToString();
                                hangPK.NhomSP = rowHang["Ma_SP"].ToString();
                                hangPK.NuocXX_ID = rowHang["Xuat_xu"].ToString();
                                try
                                {
                                    if (rowHang["So_Luong"].ToString() != "")
                                    {
                                        if (LoaiPK.MaPhuKien.Trim() == "N05")
                                        {
                                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                            npl.SetDabaseMoi("MSSQL");
                                            npl.Ma = hangPK.MaHang;
                                            npl.HopDong_ID = HD.ID;
                                            npl.Load();
                                            hangPK.ThongTinCu = npl.SoLuongDangKy + "";
                                            hangPK.SoLuong = npl.SoLuongDangKy + Convert.ToDecimal(rowHang["So_Luong"]);
                                        }
                                        else if (LoaiPK.MaPhuKien.Trim() == "N11")
                                            hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                        else if (LoaiPK.MaPhuKien.Trim() == "T01")
                                            hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                        else
                                            hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    }
                                }
                                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }
                                hangPK.TenHang = FontConverter.TCVN2Unicode(rowHang["Ten_SP"].ToString().Trim());
                                hangPK.TinhTrang = FontConverter.TCVN2Unicode(rowHang["TinhTrang"].ToString().Trim());
                                if (rowHang["TriGia"].ToString() != "")
                                    hangPK.TriGia = Convert.ToDouble(rowHang["TriGia"]);
                                LoaiPK.HPKCollection.Add(hangPK);
                            }
                            pkdk.PKCollection.Add(LoaiPK);
                        }
                        pkdkCollection.Add(pkdk);
                    }
                }

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy item in pkdkCollection)
                        {
                            item.HopDong_ID = HD.ID;
                            item.InsertTransactionKTX(transaction);
                            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in item.PKCollection)
                            {
                                int stt = 1;
                                pk.Master_ID = item.ID;
                                pk.InsertTransaction(transaction);

                                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hang in pk.HPKCollection)
                                {
                                    hang.STTHang = stt++;
                                    hang.Master_ID = pk.ID;
                                    hang.InsertTransaction(transaction);
                                }
                            }
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }


        public static void InsertMoiDuLieuPhuKien(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsPhuKienDK, DataSet dsLoaiPK, DataSet dsHangPK)
        {
            Company.GC.BLL.KDT.GC.PhuKienDangKyCollection pkdkCollection = new Company.GC.BLL.KDT.GC.PhuKienDangKyCollection();
            if (dsPhuKienDK.Tables[0].Rows.Count > 0)
            {
                System.Globalization.NumberFormatInfo infoNumber = new System.Globalization.CultureInfo("vi-VN").NumberFormat;
                foreach (DataRow row in dsPhuKienDK.Tables[0].Rows)
                {
                    string sohopdong = row["So_HD"].ToString().Trim();
                    Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
                    pkdk.MaDoanhNghiep = HD.MaDoanhNghiep;
                    pkdk.MaHaiQuan = HD.MaHaiQuan;
                    pkdk.SoPhuKien = row["So_PK"].ToString().Trim();
                    pkdk.HopDong_ID = HD.ID;
                    pkdk.SetDabaseMoi("MSSQL");
                    if (pkdk.LoadBySoPhuKien())
                        continue;
                    pkdk.NgayPhuKien = Convert.ToDateTime(row["Ngay_PK"]);
                    pkdk.NgayTiepNhan = Convert.ToDateTime(row["Ngay_TN"]);
                    pkdk.NguoiDuyet = FontConverter.TCVN2Unicode(row["Nguoi_Duyet"].ToString());
                    pkdk.SoTiepNhan = Convert.ToInt64(row["So_TN"].ToString());
                    pkdk.TrangThaiXuLy = 1;
                    pkdk.VanBanChoPhep = FontConverter.TCVN2Unicode(row["VBCP"].ToString().Trim());

                    pkdk.TrangThaiXuLy = 1;

                    string st = "DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and So_PK='" + pkdk.SoPhuKien + "'";
                    DataRow[] rowLoaiPK = dsLoaiPK.Tables[0].Select(st);
                    foreach (DataRow rowLoai in rowLoaiPK)
                    {
                        Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                        LoaiPK.MaPhuKien = rowLoai["Ma_PK"].ToString();
                        LoaiPK.NoiDung = FontConverter.TCVN2Unicode(rowLoai["Noi_Dung"].ToString());
                        LoaiPK.ThongTinCu = rowLoai["Old_Info"].ToString();
                        if (LoaiPK.MaPhuKien != "H06")
                            LoaiPK.ThongTinMoi = rowLoai["New_Info"].ToString();
                        else
                        {
                            System.Globalization.DateTimeFormatInfo infoDate = new CultureInfo("en-US").DateTimeFormat;
                            LoaiPK.ThongTinMoi = Convert.ToDateTime(rowLoai["New_Info"].ToString(), infoDate).ToString("dd/MM/yyyy");
                        }

                        LoaiPK.HPKCollection = new Company.GC.BLL.KDT.GC.HangPhuKienCollection();
                        DataRow[] rowHangLoaiPK = dsHangPK.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and So_PK='" + pkdk.SoPhuKien + "' and Ma_PK='" + LoaiPK.MaPhuKien + "'");
                        foreach (DataRow rowHang in rowHangLoaiPK)
                        {
                            Company.GC.BLL.KDT.GC.HangPhuKien hangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
                            if (rowHang["DonGia"].ToString() != "")
                                hangPK.DonGia = Convert.ToDouble(rowHang["DonGia"]);
                            hangPK.DVT_ID = rowHang["Ma_DVT"].ToString();
                            if (LoaiPK.MaPhuKien != "S15")
                                hangPK.MaHang = rowHang["P_Code"].ToString().Substring(1).Trim();
                            else
                                hangPK.MaHang = rowHang["P_Code"].ToString().Trim();
                            hangPK.MaHS = rowHang["HS_Code"].ToString().Trim();
                            hangPK.NguyenTe_ID = rowHang["NGTe"].ToString();
                            hangPK.NhomSP = rowHang["Ma_SP"].ToString();
                            hangPK.NuocXX_ID = rowHang["Xuat_xu"].ToString();
                            try
                            {
                                if (rowHang["So_Luong"].ToString() != "")
                                {
                                    if (LoaiPK.MaPhuKien.Trim() == "N05")
                                    {
                                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                        npl.Ma = hangPK.MaHang;
                                        npl.HopDong_ID = HD.ID;
                                        npl.Load();
                                        hangPK.ThongTinCu = npl.SoLuongDangKy + "";
                                        hangPK.SoLuong = npl.SoLuongDangKy + Convert.ToDecimal(rowHang["So_Luong"]);
                                    }
                                    else if (LoaiPK.MaPhuKien.Trim() == "N11")
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    else if (LoaiPK.MaPhuKien.Trim() == "T01")
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                    else
                                        hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                                }
                            }
                            catch { }
                            hangPK.TenHang = FontConverter.TCVN2Unicode(rowHang["Ten_SP"].ToString().Trim());
                            hangPK.TinhTrang = FontConverter.TCVN2Unicode(rowHang["TinhTrang"].ToString().Trim());
                            if (rowHang["TriGia"].ToString() != "")
                                hangPK.TriGia = Convert.ToDouble(rowHang["TriGia"]);
                            LoaiPK.HPKCollection.Add(hangPK);
                        }
                        pkdk.PKCollection.Add(LoaiPK);
                    }
                    pkdkCollection.Add(pkdk);
                }
            }


            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy item in pkdkCollection)
                    {
                        item.HopDong_ID = HD.ID;
                        item.InsertTransaction(transaction);
                        foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = item.ID;
                            pk.InsertTransaction(transaction);

                            foreach (Company.GC.BLL.KDT.GC.HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pk.ID;
                                hang.InsertTransaction(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        #endregion Cap nhat du lieu


    }
}
