using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_TIA : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public decimal SoToKhai { set; get; }
		public string CoBaoNhapKhauXuatKhau { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public string MaNguoiKhaiDauTien { set; get; }
		public string TenNguoiKhaiDauTien { set; get; }
		public string MaNguoiXuatNhapKhau { set; get; }
		public string TenNguoiXuatNhapKhau { set; get; }
		public DateTime ThoiHanTaiXuatNhap { set; get; }
		public string TrangThaiXuLy { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TIA> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TIA> collection = new List<KDT_VNACCS_TIA>();
			while (reader.Read())
			{
				KDT_VNACCS_TIA entity = new KDT_VNACCS_TIA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoBaoNhapKhauXuatKhau"))) entity.CoBaoNhapKhauXuatKhau = reader.GetString(reader.GetOrdinal("CoBaoNhapKhauXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhaiDauTien"))) entity.MaNguoiKhaiDauTien = reader.GetString(reader.GetOrdinal("MaNguoiKhaiDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhaiDauTien"))) entity.TenNguoiKhaiDauTien = reader.GetString(reader.GetOrdinal("TenNguoiKhaiDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiXuatNhapKhau"))) entity.MaNguoiXuatNhapKhau = reader.GetString(reader.GetOrdinal("MaNguoiXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiXuatNhapKhau"))) entity.TenNguoiXuatNhapKhau = reader.GetString(reader.GetOrdinal("TenNguoiXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanTaiXuatNhap"))) entity.ThoiHanTaiXuatNhap = reader.GetDateTime(reader.GetOrdinal("ThoiHanTaiXuatNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TIA> collection, long id)
        {
            foreach (KDT_VNACCS_TIA item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TIA VALUES(@SoToKhai, @CoBaoNhapKhauXuatKhau, @CoQuanHaiQuan, @MaNguoiKhaiDauTien, @TenNguoiKhaiDauTien, @MaNguoiXuatNhapKhau, @TenNguoiXuatNhapKhau, @ThoiHanTaiXuatNhap, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TIA SET SoToKhai = @SoToKhai, CoBaoNhapKhauXuatKhau = @CoBaoNhapKhauXuatKhau, CoQuanHaiQuan = @CoQuanHaiQuan, MaNguoiKhaiDauTien = @MaNguoiKhaiDauTien, TenNguoiKhaiDauTien = @TenNguoiKhaiDauTien, MaNguoiXuatNhapKhau = @MaNguoiXuatNhapKhau, TenNguoiXuatNhapKhau = @TenNguoiXuatNhapKhau, ThoiHanTaiXuatNhap = @ThoiHanTaiXuatNhap, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TIA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoNhapKhauXuatKhau", SqlDbType.VarChar, "CoBaoNhapKhauXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhaiDauTien", SqlDbType.VarChar, "MaNguoiKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiDauTien", SqlDbType.NVarChar, "TenNguoiKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiXuatNhapKhau", SqlDbType.VarChar, "MaNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiXuatNhapKhau", SqlDbType.NVarChar, "TenNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanTaiXuatNhap", SqlDbType.DateTime, "ThoiHanTaiXuatNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoNhapKhauXuatKhau", SqlDbType.VarChar, "CoBaoNhapKhauXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhaiDauTien", SqlDbType.VarChar, "MaNguoiKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiDauTien", SqlDbType.NVarChar, "TenNguoiKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiXuatNhapKhau", SqlDbType.VarChar, "MaNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiXuatNhapKhau", SqlDbType.NVarChar, "TenNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanTaiXuatNhap", SqlDbType.DateTime, "ThoiHanTaiXuatNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TIA VALUES(@SoToKhai, @CoBaoNhapKhauXuatKhau, @CoQuanHaiQuan, @MaNguoiKhaiDauTien, @TenNguoiKhaiDauTien, @MaNguoiXuatNhapKhau, @TenNguoiXuatNhapKhau, @ThoiHanTaiXuatNhap, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TIA SET SoToKhai = @SoToKhai, CoBaoNhapKhauXuatKhau = @CoBaoNhapKhauXuatKhau, CoQuanHaiQuan = @CoQuanHaiQuan, MaNguoiKhaiDauTien = @MaNguoiKhaiDauTien, TenNguoiKhaiDauTien = @TenNguoiKhaiDauTien, MaNguoiXuatNhapKhau = @MaNguoiXuatNhapKhau, TenNguoiXuatNhapKhau = @TenNguoiXuatNhapKhau, ThoiHanTaiXuatNhap = @ThoiHanTaiXuatNhap, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TIA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoNhapKhauXuatKhau", SqlDbType.VarChar, "CoBaoNhapKhauXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhaiDauTien", SqlDbType.VarChar, "MaNguoiKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiDauTien", SqlDbType.NVarChar, "TenNguoiKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiXuatNhapKhau", SqlDbType.VarChar, "MaNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiXuatNhapKhau", SqlDbType.NVarChar, "TenNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanTaiXuatNhap", SqlDbType.DateTime, "ThoiHanTaiXuatNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoNhapKhauXuatKhau", SqlDbType.VarChar, "CoBaoNhapKhauXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhaiDauTien", SqlDbType.VarChar, "MaNguoiKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiDauTien", SqlDbType.NVarChar, "TenNguoiKhaiDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiXuatNhapKhau", SqlDbType.VarChar, "MaNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiXuatNhapKhau", SqlDbType.NVarChar, "TenNguoiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanTaiXuatNhap", SqlDbType.DateTime, "ThoiHanTaiXuatNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TIA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TIA> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TIA> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TIA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TIA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TIA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TIA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TIA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TIA(decimal soToKhai, string coBaoNhapKhauXuatKhau, string coQuanHaiQuan, string maNguoiKhaiDauTien, string tenNguoiKhaiDauTien, string maNguoiXuatNhapKhau, string tenNguoiXuatNhapKhau, DateTime thoiHanTaiXuatNhap, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TIA entity = new KDT_VNACCS_TIA();	
			entity.SoToKhai = soToKhai;
			entity.CoBaoNhapKhauXuatKhau = coBaoNhapKhauXuatKhau;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.MaNguoiKhaiDauTien = maNguoiKhaiDauTien;
			entity.TenNguoiKhaiDauTien = tenNguoiKhaiDauTien;
			entity.MaNguoiXuatNhapKhau = maNguoiXuatNhapKhau;
			entity.TenNguoiXuatNhapKhau = tenNguoiXuatNhapKhau;
			entity.ThoiHanTaiXuatNhap = thoiHanTaiXuatNhap;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@CoBaoNhapKhauXuatKhau", SqlDbType.VarChar, CoBaoNhapKhauXuatKhau);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiDauTien", SqlDbType.VarChar, MaNguoiKhaiDauTien);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiDauTien", SqlDbType.NVarChar, TenNguoiKhaiDauTien);
			db.AddInParameter(dbCommand, "@MaNguoiXuatNhapKhau", SqlDbType.VarChar, MaNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TenNguoiXuatNhapKhau", SqlDbType.NVarChar, TenNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@ThoiHanTaiXuatNhap", SqlDbType.DateTime, ThoiHanTaiXuatNhap.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiXuatNhap);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TIA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TIA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TIA(long id, decimal soToKhai, string coBaoNhapKhauXuatKhau, string coQuanHaiQuan, string maNguoiKhaiDauTien, string tenNguoiKhaiDauTien, string maNguoiXuatNhapKhau, string tenNguoiXuatNhapKhau, DateTime thoiHanTaiXuatNhap, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TIA entity = new KDT_VNACCS_TIA();			
			entity.ID = id;
			entity.SoToKhai = soToKhai;
			entity.CoBaoNhapKhauXuatKhau = coBaoNhapKhauXuatKhau;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.MaNguoiKhaiDauTien = maNguoiKhaiDauTien;
			entity.TenNguoiKhaiDauTien = tenNguoiKhaiDauTien;
			entity.MaNguoiXuatNhapKhau = maNguoiXuatNhapKhau;
			entity.TenNguoiXuatNhapKhau = tenNguoiXuatNhapKhau;
			entity.ThoiHanTaiXuatNhap = thoiHanTaiXuatNhap;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TIA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@CoBaoNhapKhauXuatKhau", SqlDbType.VarChar, CoBaoNhapKhauXuatKhau);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiDauTien", SqlDbType.VarChar, MaNguoiKhaiDauTien);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiDauTien", SqlDbType.NVarChar, TenNguoiKhaiDauTien);
			db.AddInParameter(dbCommand, "@MaNguoiXuatNhapKhau", SqlDbType.VarChar, MaNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TenNguoiXuatNhapKhau", SqlDbType.NVarChar, TenNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@ThoiHanTaiXuatNhap", SqlDbType.DateTime, ThoiHanTaiXuatNhap.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiXuatNhap);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TIA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TIA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TIA(long id, decimal soToKhai, string coBaoNhapKhauXuatKhau, string coQuanHaiQuan, string maNguoiKhaiDauTien, string tenNguoiKhaiDauTien, string maNguoiXuatNhapKhau, string tenNguoiXuatNhapKhau, DateTime thoiHanTaiXuatNhap, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TIA entity = new KDT_VNACCS_TIA();			
			entity.ID = id;
			entity.SoToKhai = soToKhai;
			entity.CoBaoNhapKhauXuatKhau = coBaoNhapKhauXuatKhau;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.MaNguoiKhaiDauTien = maNguoiKhaiDauTien;
			entity.TenNguoiKhaiDauTien = tenNguoiKhaiDauTien;
			entity.MaNguoiXuatNhapKhau = maNguoiXuatNhapKhau;
			entity.TenNguoiXuatNhapKhau = tenNguoiXuatNhapKhau;
			entity.ThoiHanTaiXuatNhap = thoiHanTaiXuatNhap;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@CoBaoNhapKhauXuatKhau", SqlDbType.VarChar, CoBaoNhapKhauXuatKhau);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiDauTien", SqlDbType.VarChar, MaNguoiKhaiDauTien);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiDauTien", SqlDbType.NVarChar, TenNguoiKhaiDauTien);
			db.AddInParameter(dbCommand, "@MaNguoiXuatNhapKhau", SqlDbType.VarChar, MaNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TenNguoiXuatNhapKhau", SqlDbType.NVarChar, TenNguoiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@ThoiHanTaiXuatNhap", SqlDbType.DateTime, ThoiHanTaiXuatNhap.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiXuatNhap);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TIA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TIA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TIA(long id)
		{
			KDT_VNACCS_TIA entity = new KDT_VNACCS_TIA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TIA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TIA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}