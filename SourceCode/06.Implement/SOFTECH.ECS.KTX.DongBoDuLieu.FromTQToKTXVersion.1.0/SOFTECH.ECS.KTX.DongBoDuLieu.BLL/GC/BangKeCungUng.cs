using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.KDT.SHARE.Components.Utils;

namespace DongBoDuLieu.BLL.GC
{
    public class BKCU
    {
        public static DataSet GetDataSetBangKeCungUng(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {
            
            string sqlBangKe = "select * from DBKNPLTCU_SPX where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "'";
            DataSet dsBangKe = WebServiceDB.getDataBySQL(sqlBangKe, HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsBangKe;

            //TODO: BKCungUng
            /*
            StringBuilder query = new StringBuilder();
            query.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            query.Append(" SELECT * ");
            query.Append(" from DBKNPLTCU_SPX ");
            query.Append("WHERE Ma_HQ = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' and So_HD=''" + HD.SoHopDong + "''");
            query.Append(" ') AS a;");

            DataSet dsBangKe = WebServiceDB.getDataBySQL(query.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsBangKe;
            */
        }

        public static DataSet GetDataSetBangKeCungUngByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string matKhau)
        {
            string sqlBangKe = "select * from DBKNPLTCU_SPX where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "'" +
                               " and Ngay_TN>='" + NgayBatDau.ToString("MM/dd/yyyy") + "' and Ngay_TN<='" + NgayKetThuc.ToString("MM/dd/yyyy") + "'";
            DataSet dsBangKe = WebServiceDB.getDataBySQL(sqlBangKe, HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsBangKe;
        }

        public static DataSet GetDataSetSanPhamCungUng(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {
            
            string sqlSanPhamBangKe = "select * from DHANGBKNPLTCU_SPX where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "'";
            DataSet dsSanPhamBangKe = WebServiceDB.getDataBySQL(sqlSanPhamBangKe, HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsSanPhamBangKe;

            //TODO: BKCungUng
            /*
            StringBuilder query = new StringBuilder();
            query.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            query.Append(" SELECT * ");
            query.Append(" from DHANGBKNPLTCU_SPX ");
            query.Append("WHERE Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' and So_HD=''" + HD.SoHopDong + "''");
            query.Append(" ') AS a;");

            DataSet dsSanPhamBangKe = WebServiceDB.getDataBySQL(query.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsSanPhamBangKe;
            */
        }

        public static DataSet GetDataSetSanPhamCungUngBy(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string matKhau)
        {
            string sqlSanPhamBangKe = "SELECT ds2.* FROM DBKNPLTCU_SPX ds " +
                                     "INNER JOIN DHANGBKNPLTCU_SPX ds2 ON ds2.So_TN = ds.So_TN AND ds2.Ma_HQTN = ds.Ma_HQTN AND ds2.NamTN = ds.NamTN where ds2.DVGC='" + HD.MaDoanhNghiep + "'  and ds2.Ma_HQHD='" + HD.MaHaiQuan + "' and ds2.So_HD='" + HD.SoHopDong + "'" +
                                     " and ds.Ngay_TN>='" + NgayBatDau.ToString("MM/dd/yyyy") + "' and ds.Ngay_TN<='" + NgayKetThuc.ToString("MM/dd/yyyy") + "'";
            DataSet dsSanPhamBangKe = WebServiceDB.getDataBySQL(sqlSanPhamBangKe, HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsSanPhamBangKe;
        }


        public static DataSet GetDataSetNPLCungUng(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {
            
            string sqlNPLBangKe = "select * from DDMUCCU_SPX where DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "'";
            DataSet dsNPLBangKe = WebServiceDB.getDataBySQL(sqlNPLBangKe, HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsNPLBangKe;
            
            //TODO: BKCungUng
            /*
            StringBuilder query = new StringBuilder();
            query.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            query.Append(" SELECT * ");
            query.Append(" from DDMUCCU_SPX ");
            query.Append("WHERE Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' and So_HD=''" + HD.SoHopDong + "''");
            query.Append(" ') AS a;");

            DataSet dsNPLBangKe = WebServiceDB.getDataBySQL(query.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsNPLBangKe;
            */
        }


        public static DataSet GetDataSetNPLCungUngByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string matKhau)
        {
            string sqlNPLBangKe = "SELECT dbdt.* FROM DDMUCCU_SPX dbdt " +
                                "INNER JOIN DBKNPLTCU_SPX ds ON ds.So_TN = dbdt.So_TN AND ds.Ma_HQTN = dbdt.Ma_HQTN " +
                                "AND ds.NamTN = dbdt.NamTN AND ds.So_HD = dbdt.So_HD AND ds.Ma_HQHD = dbdt.Ma_HQHD " +
                                "AND ds.DVGC = dbdt.DVGC AND ds.Ngay_ky = dbdt.Ngay_ky " +
                                        " and ds.Ngay_TN>='" + NgayBatDau.ToString("MM/dd/yyyy") + "' and ds.Ngay_TN<='" + NgayKetThuc.ToString("MM/dd/yyyy") + "'";
            DataSet dsNPLBangKe = WebServiceDB.getDataBySQL(sqlNPLBangKe, HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsNPLBangKe;
        }

        public static void InsertGhiDeALLBangKeCungUng(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsBangKe, DataSet dsSanPhamBangKe, DataSet dsNPLBangKe)
        {
            Company.GC.BLL.KDT.GC.BKCungUngDangKyCollection BKCollection = new Company.GC.BLL.KDT.GC.BKCungUngDangKyCollection();
            foreach (DataRow rowBangKe in dsBangKe.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.BKCungUngDangKy BKCungUng = new Company.GC.BLL.KDT.GC.BKCungUngDangKy();
                BKCungUng.MaDoanhNghiep = HD.MaDoanhNghiep;
                BKCungUng.MaHaiQuan = HD.MaHaiQuan;
                BKCungUng.NgayTiepNhan = Convert.ToDateTime(rowBangKe["Ngay_TN"]);
                BKCungUng.SoBangKe = Convert.ToInt64(rowBangKe["So_TN"]);
                BKCungUng.SoTiepNhan = 0;
                BKCungUng.TrangThaiXuLy = 1;
                string MaLoaiHinh = rowBangKe["Ma_LH"].ToString();
                int NamDK = Convert.ToInt32(rowBangKe["NamDK"].ToString());
                long SoToKhai = Convert.ToInt64(rowBangKe["SoTK"].ToString());
                if (MaLoaiHinh.StartsWith("XGC"))
                {
                    BKCungUng.TKMD_ID = Company.GC.BLL.KDT.ToKhaiMauDich.GetIDToKhaiMauDichBySoToKhaiAndNamDangKy(HD.MaHaiQuan, MaLoaiHinh, SoToKhai, NamDK, "MSSQL");
                }
                else
                {
                    BKCungUng.TKCT_ID = Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.GetIDToKhaiChuyenTiepBySoToKhaiAndNamDangKy(HD.MaHaiQuan, MaLoaiHinh, SoToKhai, NamDK, "MSSQL");
                }
                if (BKCungUng.TKMD_ID > 0 || BKCungUng.TKCT_ID > 0)
                {
                    BKCungUng.SanPhamCungUngCollection = new Company.GC.BLL.KDT.GC.SanPhanCungUngCollection();
                    DataRow[] rowSPCungUngCollection = dsSanPhamBangKe.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and So_TN=" + BKCungUng.SoBangKe + " and NamTN=" + NamDK);
                    foreach (DataRow SPCungUng in rowSPCungUngCollection)
                    {
                        Company.GC.BLL.KDT.GC.SanPhanCungUng spCungUngEntity = new Company.GC.BLL.KDT.GC.SanPhanCungUng();
                        string maSP = SPCungUng["SPP_Code"].ToString();
                        spCungUngEntity.LuongCUSanPham = Convert.ToDecimal(SPCungUng["SoLuongSP"]);
                        spCungUngEntity.MaSanPham = maSP.Substring(1).Trim();
                        spCungUngEntity.NPLCungUngCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUngCollection();
                        DataRow[] rowNPLOfSanPhamCollection = dsNPLBangKe.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and So_TN=" + BKCungUng.SoBangKe + " and NamTN=" + NamDK + " and SPP_Code='" + maSP + "'");
                        foreach (DataRow rowNPL in rowNPLOfSanPhamCollection)
                        {
                            Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng nplCungUng = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng();
                            nplCungUng.DinhMucCungUng = Convert.ToDouble(rowNPL["DMGC"]);
                            nplCungUng.DonGia = Convert.ToDouble(rowNPL["DonGia"]);
                            nplCungUng.HinhThuCungUng = FontConverter.TCVN2Unicode(rowNPL["HinhThucCU"].ToString());
                            nplCungUng.LuongCung = Convert.ToDecimal(rowNPL["LuongCU"]);
                            nplCungUng.MaNguyenPhuLieu = rowNPL["NPLP_Code"].ToString().Substring(1).Trim();
                            nplCungUng.TriGia = Convert.ToDouble(rowNPL["TriGia"]);
                            nplCungUng.TyLeHH = Convert.ToDouble(rowNPL["TLHH"]);
                            spCungUngEntity.NPLCungUngCollection.Add(nplCungUng);
                        }
                        BKCungUng.SanPhamCungUngCollection.Add(spCungUngEntity);
                    }
                }
                BKCollection.Add(BKCungUng);
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (Company.GC.BLL.KDT.GC.BKCungUngDangKy BKCU in BKCollection)
                    {
                        BKCU.InsertTransactionKTX(transaction);

                        if (BKCU.SanPhamCungUngCollection == null)
                            continue;

                        foreach (Company.GC.BLL.KDT.GC.SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                        {
                            spCU.Master_ID = BKCU.ID;
                            spCU.InsertTransaction(transaction);
                            foreach (Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng nplCU in spCU.NPLCungUngCollection)
                            {
                                nplCU.Master_ID = spCU.ID;
                                nplCU.InsertTransaction(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static void InsertGhiDuLieuMoiBangKeCungUng(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsBangKe, DataSet dsSanPhamBangKe, DataSet dsNPLBangKe)
        {
            Company.GC.BLL.KDT.GC.BKCungUngDangKyCollection BKCollection = new Company.GC.BLL.KDT.GC.BKCungUngDangKyCollection();
            foreach (DataRow rowBangKe in dsBangKe.Tables[0].Rows)
            {
                Company.GC.BLL.KDT.GC.BKCungUngDangKy BKCungUng = new Company.GC.BLL.KDT.GC.BKCungUngDangKy();
                BKCungUng.MaDoanhNghiep = HD.MaDoanhNghiep;
                BKCungUng.MaHaiQuan = HD.MaHaiQuan;
                BKCungUng.NgayTiepNhan = Convert.ToDateTime(rowBangKe["Ngay_TN"]);
                BKCungUng.SoBangKe = Convert.ToInt64(rowBangKe["So_TN"]);
                BKCungUng.SoTiepNhan = 0;
                BKCungUng.TrangThaiXuLy = 1;
                BKCungUng.SetDabaseMoi("MSSQL");
                if (BKCungUng.LoadBySoBangKe())
                    continue;
                string MaLoaiHinh = rowBangKe["Ma_LH"].ToString();
                int NamDK = Convert.ToInt32(rowBangKe["NamDK"].ToString());
                long SoToKhai = Convert.ToInt64(rowBangKe["SoTK"].ToString());
                if (MaLoaiHinh.StartsWith("XGC"))
                {
                    BKCungUng.TKMD_ID = Company.GC.BLL.KDT.ToKhaiMauDich.GetIDToKhaiMauDichBySoToKhaiAndNamDangKy(HD.MaHaiQuan, MaLoaiHinh, SoToKhai, NamDK, "MSSQL");
                }
                else
                {
                    BKCungUng.TKCT_ID = Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.GetIDToKhaiChuyenTiepBySoToKhaiAndNamDangKy(HD.MaHaiQuan, MaLoaiHinh, SoToKhai, NamDK, "MSSQL");
                }
                if (BKCungUng.TKMD_ID > 0 || BKCungUng.TKCT_ID > 0)
                {
                    BKCungUng.SanPhamCungUngCollection = new Company.GC.BLL.KDT.GC.SanPhanCungUngCollection();
                    DataRow[] rowSPCungUngCollection = dsSanPhamBangKe.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and So_TN=" + BKCungUng.SoBangKe + " and NamTN=" + NamDK);
                    foreach (DataRow SPCungUng in rowSPCungUngCollection)
                    {
                        Company.GC.BLL.KDT.GC.SanPhanCungUng spCungUngEntity = new Company.GC.BLL.KDT.GC.SanPhanCungUng();
                        string maSP = SPCungUng["SPP_Code"].ToString();
                        spCungUngEntity.LuongCUSanPham = Convert.ToDecimal(SPCungUng["SoLuongSP"]);
                        spCungUngEntity.MaSanPham = maSP.Substring(1).Trim();
                        spCungUngEntity.NPLCungUngCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUngCollection();
                        DataRow[] rowNPLOfSanPhamCollection = dsNPLBangKe.Tables[0].Select("DVGC='" + HD.MaDoanhNghiep + "'  and Ma_HQHD='" + HD.MaHaiQuan + "' and So_HD='" + HD.SoHopDong + "' and So_TN=" + BKCungUng.SoBangKe + " and NamTN=" + NamDK + " and SPP_Code='" + maSP + "'");
                        foreach (DataRow rowNPL in rowNPLOfSanPhamCollection)
                        {
                            Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng nplCungUng = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng();
                            nplCungUng.DinhMucCungUng = Convert.ToDouble(rowNPL["DMGC"]);
                            nplCungUng.DonGia = Convert.ToDouble(rowNPL["DonGia"]);
                            nplCungUng.HinhThuCungUng = FontConverter.TCVN2Unicode(rowNPL["HinhThucCU"].ToString());
                            nplCungUng.LuongCung = Convert.ToDecimal(rowNPL["LuongCU"]);
                            nplCungUng.MaNguyenPhuLieu = rowNPL["NPLP_Code"].ToString().Substring(1).Trim();
                            nplCungUng.TriGia = Convert.ToDouble(rowNPL["TriGia"]);
                            nplCungUng.TyLeHH = Convert.ToDouble(rowNPL["TLHH"]);
                            spCungUngEntity.NPLCungUngCollection.Add(nplCungUng);
                        }
                        BKCungUng.SanPhamCungUngCollection.Add(spCungUngEntity);
                    }
                }
                BKCollection.Add(BKCungUng);
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (Company.GC.BLL.KDT.GC.BKCungUngDangKy BKCU in BKCollection)
                    {
                        BKCU.InsertTransaction(transaction);
                        foreach (Company.GC.BLL.KDT.GC.SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                        {
                            spCU.Master_ID = BKCU.ID;
                            spCU.InsertTransaction(transaction);
                            foreach (Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng nplCU in spCU.NPLCungUngCollection)
                            {
                                nplCU.Master_ID = spCU.ID;
                                nplCU.InsertTransaction(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

    }
}
