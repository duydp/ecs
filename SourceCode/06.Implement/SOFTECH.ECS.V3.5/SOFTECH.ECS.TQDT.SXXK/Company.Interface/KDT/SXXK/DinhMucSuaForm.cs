﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Xml.Serialization;
using System.Xml;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucSuaForm : BaseForm
    {
        SelectSanPhamDMSUAForm fsp = new SelectSanPhamDMSUAForm();
        SelectNPL_DMSUAForm fnpl = new SelectNPL_DMSUAForm();

        public DinhMucDangKy dmDangKy = new DinhMucDangKy();
        public DinhMucSUA dmSUA = new DinhMucSUA();
        public DinhMucDangKySUA dmdkSUA = new DinhMucDangKySUA();
        public bool isTaoMoi;

        public DataTable dmSUATable = new DataTable();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;

        public long IDDMDK;

        public DinhMucSuaForm()
        {
            InitializeComponent();
        }

        private void DinhMucSuaForm_Load(object sender, EventArgs e)
        {
            btnChonSP.Enabled = false;
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["DinhMucChung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            if (dmdkSUA != null && dmdkSUA.ID != 0)
            {
                DataSet ds = DinhMucDangKySUA.getDinhMucSUADaDK_OfSanPham(dmdkSUA.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                dmSUATable = ds.Tables[0];
            }
            else
            {
                dmSUATable = new DataTable();
                dmSUATable.Columns.Add("ID");
                dmSUATable.Columns.Add("MaSanPham");
                dmSUATable.Columns.Add("MaNguyenPhuLieu");
                dmSUATable.Columns.Add("DVT_ID");
                dmSUATable.Columns.Add("TenNPL");
                dmSUATable.Columns.Add("DinhMucSuDung");
                dmSUATable.Columns.Add("TyLeHaoHut");
                dmSUATable.Columns.Add("DinhMucChung");
                DataSet ds = DinhMucDangKy.getDinhMucDaDK_OfSanPham(IDDMDK, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                dmSUATable = ds.Tables[0];
            }
            dgList.DataSource = dmSUATable;

            if (dmdkSUA.SoTiepNhan > 0)
            {
                txtSoTiepNhan.Text = this.dmdkSUA.SoTiepNhan.ToString("N0");
                ccNgayTiepNhan.Value = this.dmdkSUA.NgayTiepNhan;
                ccNgayTiepNhan.Text = this.dmdkSUA.NgayTiepNhan.ToShortDateString();
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                btnGhi.Enabled = false;
                btnChonSP.Enabled = false;
                btnChonNPL.Enabled = false;
            }
            this.setCommandStatus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (this.dmSUATable.Rows.Count == 0)
            {
                ShowMessage("Chưa chọn định mức", false);
                return;
            }
            save();

            //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
            try
            {
                string where = "1 = 1";
                where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", dmdkSUA.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.DinhMuc);
                List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                if (listLog.Count > 0)
                {
                    long idLog = listLog[0].IDLog;
                    string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                    long idDK = listLog[0].ID_DK;
                    string guidstr = dmdkSUA.GUIDSTR;
                    string userKhaiBao = listLog[0].UserNameKhaiBao;
                    DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                    string userSuaDoi = GlobalSettings.UserLog;
                    DateTime ngaySuaDoi = DateTime.Now;
                    string ghiChu = listLog[0].GhiChu;
                    bool isDelete = listLog[0].IsDelete;
                    Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }
        private void save()
        {
            dmdkSUA.SoTiepNhan = 0;
            dmdkSUA.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            dmdkSUA.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            dmdkSUA.TrangThaiXuLy = -1;
            dmdkSUA.ActionStatus = -1;
            dmdkSUA.NgayTiepNhan = DateTime.Parse("01/01/1900");
            dmdkSUA.NgayDangKy= DateTime.Parse("01/01/1900");
            dmdkSUA.NgayApDung = DateTime.Parse("01/01/1900");
            dmdkSUA.IDDMDK = this.IDDMDK;
            try
            {
                long id = 0;
                if (dmdkSUA.ID == 0)
                    id = dmdkSUA.Insert();
                else
                    dmdkSUA.InsertUpdate();
                id = id != 0 ? id : dmdkSUA.ID;
                foreach (DataRow dr in this.dmSUATable.Rows)
                {
                    dmSUA.ID = dr["ID"].ToString() == "" ? 0 : long.Parse(dr["ID"].ToString());
                    dmSUA.MaSanPham = dr["MaSanPham"].ToString();
                    dmSUA.MaNguyenPhuLieu = dr["MaNguyenPhuLieu"].ToString();
                    dmSUA.DinhMucSuDung = decimal.Parse(dr["DinhMucSuDung"].ToString());
                    dmSUA.TyLeHaoHut = decimal.Parse(dr["TyLeHaoHut"].ToString());
                    dmSUA.Master_IDSUA = id;
                    dmSUA.DVT_ID = dr["DVT_ID"].ToString();
                    if (isTaoMoi) dmSUA.ID = 0;
                    if (dmSUA.ID == 0)
                        dmSUA.Insert();
                    else
                        dmSUA.Update();
                }
                DataSet ds = DinhMucDangKySUA.getDinhMucSUADaDK_OfSanPham(dmdkSUA.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                dmSUATable = ds.Tables[0];
                ShowMessage("Lưu thành công.", false);

            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
                return;
            }

        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (dmdkSUA.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                this.SendV3();
            else
                send();
        }

        private void send()
        {
            MsgSend sendXML = new MsgSend();
            string password = "";
            sendXML.LoaiHS = "DM_SUA";
            sendXML.master_id = dmdkSUA.ID;
            if (sendXML.Load())
            {
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (DinhMucSUA.SelectCollectionBy_Master_IDSUA(dmdkSUA.ID).Count == 0)
                {
                    MLMessages("Danh sách định mức rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                xmlCurrent = dmdkSUA.WSSendXMLSuaDM(password);
                this.Cursor = Cursors.Default;

                sendXML = new MsgSend();
                sendXML.LoaiHS = "DM_SUA";
                sendXML.master_id = dmdkSUA.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                //LayPhanHoi(password);
                LaySTN(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true);
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }
                #region Ghi lỗi
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo sửa định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                #endregion

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "DM_SUA";
                sendXML.master_id = dmdkSUA.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(Company.KDT.SHARE.Components.MessgaseType.DinhMuc, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, dmdkSUA.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI.Trim();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI.Trim();

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN.Trim();
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));
                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = dmdkSUA.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                xmlCurrent = dmdkSUA.LayPhanHoi(pass, xml.InnerXml);
                //xmlCurrent = dmdkSUA.WSRequestDaDuyet(pass);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        LayPhanHoi(pass);
                    }
                    return;
                }

                if (sendXML.func == 1)
                {
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmdkSUA.SoTiepNhan, "MSG_SEN05", "" + dmdkSUA.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.dmdkSUA.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.dmdkSUA.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.dmdkSUA.NgayTiepNhan.ToShortDateString();
                    btnKhaiBao.Enabled = false;
                }
                else if (sendXML.func == 2)
                {
                    if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN18", "", false);
                        lblTrangThai.Text = "Đã duyệt";
                        //xoa thông tin msg nay trong database
                        sendXML.Delete();
                    }
                    else if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        lblTrangThai.Text = "Chờ duyệt";
                        if (dmdkSUA.PhanLuong != "")
                        {
                            string tenluong = "Xanh";
                            if (dmdkSUA.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                tenluong = "Vàng";
                            else if (dmdkSUA.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                tenluong = "Đỏ";
                            MLMessages("Định mức đã được phân luồng: " + tenluong + "\n" + dmdkSUA.HUONGDAN, "MSG_SEN08", "", false);
                        }
                        else
                            MLMessages("Hải quan chưa xử lý!", "MSG_SEN08", "", false);
                    }
                    else if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                        sendXML.Delete();
                    }
                }

                //xoa thông tin msg nay trong database
                //sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\n", "MSG_SEN19", "", true);

                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }
                #region Ghi lỗi
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo sửa SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                #endregion
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LaySTN(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmdkSUA.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //sendXML.msg = sendXML.msg.Replace("<DU_LIEU>", "<DU_LIEU REFERENSE=\"480b97df-1515-40c2-a174-6ee31a9861bc\">");
                xmlCurrent = dmdkSUA.LayPhanHoi(pass, sendXML.msg);

                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        LayPhanHoi(pass);
                    }
                    return;
                }

                if (sendXML.func == 1)
                {
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmdkSUA.SoTiepNhan, "MSG_SEN05", "" + dmdkSUA.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.dmdkSUA.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.dmdkSUA.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.dmdkSUA.NgayTiepNhan.ToShortDateString();
                    btnKhaiBao.Enabled = false;
                }
                else if (sendXML.func == 2)
                {
                    if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN18", "", false);
                        lblTrangThai.Text = "Đã duyệt";
                        //xoa thông tin msg nay trong database
                        sendXML.Delete();
                    }
                    else if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        lblTrangThai.Text = "Chờ duyệt";
                        if (dmdkSUA.PhanLuong != "")
                        {
                            string tenluong = "Xanh";
                            if (dmdkSUA.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                tenluong = "Vàng";
                            else if (dmdkSUA.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                tenluong = "Đỏ";
                            MLMessages("Định mức đã được phân luồng: " + tenluong + "\n" + dmdkSUA.HUONGDAN, "MSG_SEN08", "", false);
                        }
                        else
                            MLMessages("Hải quan chưa xử lý!", "MSG_SEN08", "", false);
                    }
                    else if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                        sendXML.Delete();
                    }
                }

                //xoa thông tin msg nay trong database
                //sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\n", "MSG_SEN19", "", true);

                        }
                        else
                        {
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                        }
                    }
                    else
                    {
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                    }
                    #endregion FPTService
                }
                #region Ghi lỗi
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo sửa SP. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                #endregion
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                this.LayPhanHoiV3();
            else
            {
                string password = "";
                WSForm wsForm = new WSForm();
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    this.Cursor = Cursors.WaitCursor;
                    password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                    if (this.dmdkSUA.SoTiepNhan > 0)
                    {
                        string xmlCurrent = "";
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = "DM_SUA";
                        sendXML.master_id = dmdkSUA.ID;
                        if (!sendXML.Load())
                        {
                            xmlCurrent = dmdkSUA.WSDownLoad(password);
                            sendXML.msg = xmlCurrent;
                            xmlCurrent = "";
                            sendXML.LoaiHS = "DM_SUA";
                            sendXML.master_id = dmdkSUA.ID;
                        }
                        sendXML.func = 2;
                        sendXML.InsertUpdate();

                        LayPhanHoi(password);
                        this.Cursor = Cursors.Default;
                    }
                    else
                        LaySTN(password);
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                // if (ShowMessage("Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", "MSG_SEN19", "", true);
                            }
                            else
                            {
                                // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                                MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                                if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                                {
                                    GlobalSettings.PassWordDT = "";
                                }
                            }
                        }
                        else
                        {
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                            GlobalSettings.PassWordDT = "";
                        }
                        #endregion FPTService
                    }
                    #region Ghi lỗi
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi lấy phản hồi sửa định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                    #endregion

                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        private void btnChonSP_Click(object sender, EventArgs e)
        {
            try
            {
                if (fsp == null)
                {
                    fsp = new SelectSanPhamDMSUAForm();
                }
                fsp.ShowDialog(this);

                if (fsp.SanPhamSelected.Ma.Length > 0)
                {
                    DataSet ds = DinhMucDangKy.getDinhMucDaDK_OfSanPham(fsp.SanPhamSelected.Ma, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                    bool ok = true;
                    if (ds != null)
                    {
                        foreach (DataRow dr in dmSUATable.Rows)
                        {
                            if (dr["MaSanPham"].ToString().Equals(fsp.SanPhamSelected.Ma))
                            {
                                ok = false;
                                break;
                            }
                        }
                        if (ok)
                        {

                            foreach (DataRow dsRow in ds.Tables[0].Rows)
                            {
                                DataRow newRow = dmSUATable.NewRow();
                                //newRow["ID"] = ds.Tables[0].Rows[0]["ID"];
                                newRow["MaSanPham"] = dsRow["MaSanPham"];
                                newRow["MaNguyenPhuLieu"] = dsRow["MaNguyenPhuLieu"];
                                newRow["TenNPL"] = dsRow["TenNPL"];
                                newRow["DVT_ID"] = dsRow["DVT_ID"];
                                newRow["DinhMucSuDung"] = dsRow["DinhMucSuDung"];
                                newRow["TyLeHaoHut"] = dsRow["TyLeHaoHut"];
                                newRow["DinhMucChung"] = dsRow["DinhMucChung"];

                                dmSUATable.Rows.Add(newRow);
                            }
                        }
                    }
                    dgList.DataSource = dmSUATable;
                    //dgList.Refetch();
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());

                #region Ghi lỗi
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi click nút chọn sản phẩm. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                #endregion

            }
        }

        private void btnChonNPL_Click(object sender, EventArgs e)
        {
            if (dgList.CurrentRow == null)
            {
                return;
            }

            if (fnpl == null)
            {
                fnpl = new SelectNPL_DMSUAForm();
            }
            fnpl.ShowDialog(this);
            try
            {
                if (fnpl.NguyenPhuLieuSelected.Ma.Length > 0)
                {

                    GridEXRow row = dgList.CurrentRow;
                    DataRowView dr;
                    if (row.RowType == RowType.GroupHeader)
                        dr = (DataRowView)row.GetChildRows()[0].DataRow;
                    else
                        dr = (DataRowView)row.DataRow;
                    DataRow newRow = dmSUATable.NewRow();
                    newRow["ID"] = 0;
                    newRow["MaSanPham"] = dr.Row.ItemArray[1].ToString();
                    newRow["MaNguyenPhuLieu"] = fnpl.NguyenPhuLieuSelected.Ma;
                    newRow["TenNPL"] = fnpl.NguyenPhuLieuSelected.Ten;
                    newRow["DVT_ID"] = fnpl.NguyenPhuLieuSelected.DVT_ID;
                    newRow["DinhMucSuDung"] = 0;
                    newRow["TyLeHaoHut"] = 0;
                    newRow["DinhMucChung"] = 0;

                    dmSUATable.Rows.Add(newRow);
                    dgList.DataSource = dmSUATable;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnKetQuaXyLy_Click(object sender, EventArgs e)
        {
            //if (dmdkSUA.ID != 0)
            //    Globals.KetQuaXuLyChung(dmdkSUA.ID);
            //else
            //    Globals.ShowMessageTQDT("Không có thông tin", false);
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = dmdkSUA.ID;
            form.DeclarationIssuer = DeclarationIssuer.SXXK_DINH_MUC;
            form.ShowDialog(this);

        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //decimal dmChung = Convert.ToDecimal(e.Row.Cells["DinhMuc"].Text) * (Convert.ToDecimal(e.Row.Cells["TyLeHaoHut"].Text) / 100 + 1);
            //decimal dmsd = Convert.ToDecimal(e.Row.Cells["DinhMucSuDung"].Text);
            //decimal tlhh = Convert.ToDecimal(e.Row.Cells["TyLeHaoHut"].Text);
            //decimal dmChung = Globals.tinhDinhMucChung(dmsd, tlhh);
            //e.Row.Cells["DinhMucChung"].Value = dmChung;
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                //if (MLMessages("Bạn có muốn xóa các sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
                //{

                //    foreach (GridEXSelectedItem i in items)
                //    {
                //        if (i.RowType == RowType.Record)
                //        {
                //            dmSUATable.Rows.RemoveAt(i.Position-1);
                //        }
                //    }
                //    dgList.DataSource = dmSUATable;
                //}
            }

        }

        private void dgList_CellEdited(object sender, ColumnActionEventArgs e)
        {
            if (e.Column.Key.Equals("DinhMucSuDung") || e.Column.Key.Equals("TyLeHaoHut"))
            {
                decimal dmsd = Convert.ToDecimal(dgList.CurrentRow.Cells["DinhMucSuDung"].Value);
                decimal tlhh = Convert.ToDecimal(dgList.CurrentRow.Cells["TyLeHaoHut"].Value);
                dgList.CurrentRow.Cells["DinhMucChung"].Value = Globals.tinhDinhMucChung(dmsd, tlhh);
            }
        }

        private void dgList_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dgList_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;

                if (items.Count > 0)
                {
                    if (MLMessages("Bạn có muốn xóa các định mức này không?", "MSG_DEL01", "", true) == "Yes")
                    {
                        try
                        {
                            List<string> maSPList = new List<string>();
                            List<string> maNPLList = new List<string>();
                            List<long> dmIDList = new List<long>();
                            List<DinhMuc> dmTmpList = new List<DinhMuc>();
                            foreach (GridEXSelectedItem i in items)
                            {
                                if (i.RowType == RowType.GroupHeader)
                                {
                                    GridEXRow[] dRow = i.GetRow().GetChildRows();
                                    if (dRow.Length > 0)
                                    {
                                        if (!maSPList.Contains(dRow[0].Cells["MaSanPham"].Value.ToString()))
                                            maSPList.Add(dRow[0].Cells["MaSanPham"].Value.ToString());
                                    }
                                }
                                else if (i.RowType == RowType.Record)
                                {
                                    GridEXRow row = i.GetRow();
                                    DataRowView dr = (DataRowView)row.DataRow;
                                    //dmIDList.Add(Convert.ToInt64(dr.Row.ItemArray[0].ToString()));
                                    DinhMuc newDMSUA = new DinhMuc();
                                    newDMSUA.ID = Convert.ToInt64(dr.Row.ItemArray[0].ToString() == "" ? 0 : dr.Row.ItemArray[0]);
                                    newDMSUA.MaSanPham = dr.Row.ItemArray[1].ToString();
                                    newDMSUA.MaNguyenPhuLieu = dr.Row.ItemArray[2].ToString();

                                    dmTmpList.Add(newDMSUA);
                                }
                            }
                            //Xóa danh sách định mức của 1 sản phẩm
                            foreach (string maSP in maSPList)
                            {
                                string strExpr = "MaSanPham = '" + maSP + "'";
                                DataRow[] drList = dmSUATable.Select(strExpr);
                                foreach (DataRow dr in drList)
                                {
                                    DinhMucSUA dmSua = DinhMucSUA.Load(Convert.ToInt64(dr["ID"]));
                                    if (dmSua != null)
                                        dmSua.Delete();
                                    dmSUATable.Rows.Remove(dr);

                                }
                            }
                            //Xóa danh sách ĐM (theo NPL) được chọn
                            foreach (DinhMuc id in dmTmpList)
                            {
                                DinhMucSUA dmSua = DinhMucSUA.Load(id.ID);
                                if (dmSua != null)
                                    dmSua.Delete();
                                string strExpr = "MaSanPham = '" + id.MaSanPham + "' and MaNguyenPhuLieu = '" + id.MaNguyenPhuLieu + "'";
                                DataRow[] dr = dmSUATable.Select(strExpr);
                                if (dr.Length > 0)
                                    dmSUATable.Rows.Remove(dr[0]);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
            }

        }

        private void btnXoaSP_Click(object sender, EventArgs e)
        {

        }

        private void btnXoaNPL_Click(object sender, EventArgs e)
        {

        }
        //----------------------------------------------------------------------------------------
        #region Send V3 Create by LANNT
        private void SendV3()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmdkSUA.ID;
            if (sendXML.Load())
            {
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN03", "", false);
                return;
            }
            try
            {
                if (DinhMucSUA.SelectCollectionBy_Master_IDSUA(dmdkSUA.ID).Count == 0)
                {
                    MLMessages("Danh sách sản phẩm rỗng.\nKhông thể thực hiện khai báo thông tin đến Hải quan.", "MSG_SEN17", "", false);
                    this.Cursor = Cursors.Default;
                    return;
                }
                else
                {
                    List<DinhMucSUA> listDMSua = (List<DinhMucSUA>)DinhMucSUA.SelectCollectionBy_Master_IDSUA(dmdkSUA.ID);
                    dmdkSUA.GUIDSTR = Guid.NewGuid().ToString();
                    Company.KDT.SHARE.Components.SXXK_DinhMucSP sanpham = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc_SUA(dmdkSUA, listDMSua,GlobalSettings.TEN_DON_VI);
                    ObjectSend msgSend = new ObjectSend(
                                   new Company.KDT.SHARE.Components.NameBase()
                                   {
                                       Name = GlobalSettings.TEN_DON_VI,
                                       Identity = dmdkSUA.MaDoanhNghiep,
                                   }
                                     , new Company.KDT.SHARE.Components.NameBase()
                                     {
                                         Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmdkSUA.MaHaiQuan),
                                         Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdkSUA.MaHaiQuan.Trim()):dmdkSUA.MaHaiQuan,
                                     }
                                  ,
                                    new Company.KDT.SHARE.Components.SubjectBase()
                                    {
                                        Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DINH_MUC,
                                        Function = Company.KDT.SHARE.Components.DeclarationFunction.SUA,
                                        Reference = dmdkSUA.GUIDSTR,
                                    }
                                    ,
                                    sanpham);
                    
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                   // isSend = true;
                    if (isSend & feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        dmdkSUA.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                        sendForm.Message.XmlSaveMessage(dmdkSUA.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoDinhMucSua);
                        dmdkSUA.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                        sendXML.func = 1;
                        // sendXML.msg = msgSend;
                        sendXML.InsertUpdate();
                        LayPhanHoiV3();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
            {
                dmdkSUA.Update();
                setCommandStatus();
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            try
            {
                //feedbackContent = SingleMessage.DinhMucSendHandler(dmDangKy, ref msgInfor, e);
                feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                string noidung = feedbackContent.AdditionalInformations[0].Content.Text;

                if (e.Error == null)
                {
                    bool isDeleteMsg = false;
                    
                    feedbackContent = Company.KDT.SHARE.Components.Helpers.GetFeedBackContent(e.FeedBackMessage);
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case Company.KDT.SHARE.Components.DeclarationFunction.KHONG_CHAP_NHAN:
                            noidung = SingleMessage.GetErrorContent(feedbackContent);
                            dmdkSUA.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                            e.FeedBackMessage.XmlSaveMessage(dmdkSUA.ID, MessageTitle.TuChoiTiepNhan, noidung);
                            isDeleteMsg = true;
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.CHUA_XU_LY:
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.CAP_SO_TIEP_NHAN:
                            string[] ketqua = feedbackContent.AdditionalInformations[0].Content.Text.Split('/');

                            if (ketqua.Length > 1)
                            {
                                noidung = "\r\nSố Tiếp Nhận: " + ketqua[0] + "\r\nNăm Ðăng Ký: " + ketqua[1] + "\r\nNgày tiếp nhận: " + feedbackContent.Issue;
                                dmdkSUA.SoTiepNhan = long.Parse(ketqua[0].Trim());
                                dmdkSUA.NamDK = short.Parse(ketqua[1].Trim());
                            }
                            else
                            {
                                dmdkSUA.SoTiepNhan = System.Convert.ToInt32(feedbackContent.CustomsReference);
                                dmdkSUA.NamDK = System.Convert.ToInt16( DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null).Year);
                                noidung = "\r\nSố Tiếp Nhận: " + dmdkSUA.SoTiepNhan + "\r\nNăm Ðăng Ký: " + dmdkSUA.NamDK + "\r\nNgày tiếp nhận: " + feedbackContent.Issue;
                            }
                            noidung = "Cấp số tiếp nhận khai báo sửa\r\n" + noidung;

                            dmdkSUA.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                            dmdkSUA.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);
                            e.FeedBackMessage.XmlSaveMessage(dmdkSUA.ID, "Cấp số tiếp nhận sửa định mức", noidung);
                            break;
                        case Company.KDT.SHARE.Components.DeclarationFunction.THONG_QUAN:
                            isDeleteMsg = true;
                            e.FeedBackMessage.XmlSaveMessage(dmdkSUA.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQDuyet, noidung);
                            #region
                            //cập nhật lại Định mức gốc
                            List<DinhMucSUA> dmSUAList = (List<DinhMucSUA>)DinhMucSUA.SelectCollectionBy_Master_IDSUA(dmdkSUA.ID);
                            DinhMuc dmGoc = new DinhMuc();
                            DinhMuc dmTemp = new DinhMuc();
                            long masterID = 0;
                            foreach (DinhMucSUA dmSUA in dmSUAList)
                            {
                                //Cập nhật vào bảng t_KDT_SXXK_DinhMuc
                                dmGoc = DinhMuc.GetDinhMucByMaSP_NPL(dmSUA.MaSanPham, dmSUA.MaNguyenPhuLieu, dmdkSUA.MaHaiQuan.Trim(), dmdkSUA.MaDoanhNghiep);
                                dmTemp = DinhMuc.GetDinhMucByMaSP(dmSUA.MaSanPham, dmdkSUA.MaHaiQuan.Trim(), dmdkSUA.MaDoanhNghiep);
                                masterID = dmTemp.Master_ID;
                                if (dmGoc == null)
                                {
                                    dmGoc = new DinhMuc();
                                    dmGoc.ID = 0;
                                    dmGoc.Master_ID = dmTemp.Master_ID;
                                }
                                dmGoc.MaSanPham = dmSUA.MaSanPham;
                                dmGoc.MaNguyenPhuLieu = dmSUA.MaNguyenPhuLieu;
                                dmGoc.DinhMucSuDung = dmSUA.DinhMucSuDung;
                                dmGoc.TyLeHaoHut = dmSUA.TyLeHaoHut;
                                dmGoc.DVT_ID = dmSUA.DVT_ID;
                                if (dmGoc.ID == 0)
                                    dmGoc.Insert();
                                else
                                    dmGoc.Update();
                                // Cập nhật vào bảng t_SXXK_DinhMuc
                                BLL.SXXK.DinhMuc dmSXXK = BLL.SXXK.DinhMuc.getDinhMuc(dmdkSUA.MaHaiQuan.Trim(), dmdkSUA.MaDoanhNghiep, dmGoc.MaSanPham, dmGoc.MaNguyenPhuLieu);
                                if (dmSXXK == null)
                                {
                                    dmSXXK = new BLL.SXXK.DinhMuc();
                                    dmSXXK.MaDoanhNghiep = dmdkSUA.MaDoanhNghiep;
                                    dmSXXK.MaHaiQuan = dmdkSUA.MaHaiQuan.Trim();
                                    dmSXXK.MaSanPHam = dmGoc.MaSanPham;
                                    dmSXXK.MaNguyenPhuLieu = dmGoc.MaNguyenPhuLieu;
                                    dmSXXK.DinhMucSuDung = dmSUA.DinhMucSuDung;
                                    dmSXXK.TyLeHaoHut = dmSUA.TyLeHaoHut;
                                    dmSXXK.DinhMucChung = Globals.tinhDinhMucChung(dmSUA.DinhMucSuDung, dmSUA.TyLeHaoHut);
                                    dmSXXK.Insert();
                                }
                                else
                                {
                                    dmSXXK.DinhMucSuDung = dmSUA.DinhMucSuDung;
                                    dmSXXK.TyLeHaoHut = dmSUA.TyLeHaoHut;
                                    dmSXXK.DinhMucChung = Globals.tinhDinhMucChung(dmSUA.DinhMucSuDung, dmSUA.TyLeHaoHut);
                                    dmSXXK.Update();
                                }
                                // Cập nhật vào bảng t_SXXK_ThongTinDinhMuc
                            }
                            //Xóa Định mức không tồn tại trong danh sách khai báo sửa
                            DinhMucCollection dmList = DinhMuc.SelectCollectionBy_Master_ID(masterID);
                            foreach (DinhMuc dm in dmList)
                            {
                                //Nếu không có trong danh sách khai báo sửa thì xóa trong danh sách ĐM cũ
                                DinhMucSUA obj = DinhMucSUA.SearchBy(dmSUAList, dm.MaSanPham, dm.MaNguyenPhuLieu);
                                if (obj == null)
                                {
                                    BLL.SXXK.DinhMuc.getDinhMuc(dmdkSUA.MaHaiQuan.Trim(), dmdkSUA.MaDoanhNghiep, dm.MaSanPham, dm.MaNguyenPhuLieu).Delete();
                                    dm.Delete();
                                }
                            }
                            #endregion
                            dmdkSUA.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            this.ShowMessage("Định mức được chấp nhận sửa", false);
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(dmdkSUA.ID, MessageTitle.Error, noidung);
                                SingleMessage.SendMail("Không hiểu nội dung trả về từ hải quan", dmdkSUA.MaHaiQuan, e);
                                break;
                            }
                    }
                    if (isDeleteMsg)
                        SingleMessage.DeleteMsgSend(LoaiKhaiBao.DinhMuc, dmdkSUA.ID);
                    if (feedbackContent.Function.Trim() != DeclarationFunction.CHUA_XU_LY)
                        dmdkSUA.Update();
                    msgInfor = noidung;
                }
                else
                {
                    SingleMessage.DeleteMsgSend(LoaiKhaiBao.DinhMuc, dmdkSUA.ID);
                    SingleMessage.SendMail("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", dmdkSUA.MaHaiQuan, e);
                }
            }
            catch (Exception ex)
            {
                string msgTitle = string.Empty;
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                    msgTitle = "Không hiểu thông tin trả về từ hải quan";
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                    msgTitle = "Hệ thống không thể xử lý thông tin";
                }
                SingleMessage.SendMail(msgTitle, dmdkSUA.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
        }
        private void LayPhanHoiV3()
        {
            string reference = dmdkSUA.GUIDSTR;
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                Company.KDT.SHARE.Components.SubjectBase subjectBase = new Company.KDT.SHARE.Components.SubjectBase()
                {
                    Issuer = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DINH_MUC,
                    Reference = reference,
                    Function = Company.KDT.SHARE.Components.DeclarationFunction.HOI_TRANG_THAI,
                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DINH_MUC,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new Company.KDT.SHARE.Components.NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = dmdkSUA.MaDoanhNghiep
                                            },
                                              new Company.KDT.SHARE.Components.NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmdkSUA.MaHaiQuan.Trim()),
                                                  Identity =Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmdkSUA.MaHaiQuan.Trim()):dmdkSUA.MaHaiQuan.Trim()
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) setCommandStatus();
                }
            }
        }
        private void setCommandStatus()
        {
            if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                btnChonNPL.Enabled = btnChonSP.Enabled = btnGhi.Enabled = btnKhaiBao.Enabled = true;
                if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    btnLayPhanHoi.Enabled = true;
                else
                    btnLayPhanHoi.Enabled = false;
            }
            else if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
            {
                btnChonNPL.Enabled = btnChonSP.Enabled = btnGhi.Enabled = btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
            }
            else if (dmdkSUA.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnChonNPL.Enabled = btnChonSP.Enabled = btnGhi.Enabled = btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                txtSoTiepNhan.Text = dmdkSUA.SoTiepNhan.ToString();
                ccNgayTiepNhan.Value = dmdkSUA.NgayTiepNhan;
            }
            else
            {
                btnChonNPL.Enabled = btnChonSP.Enabled = btnGhi.Enabled = btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            this.SetLbStatus();

        }

        #endregion
        #region Label Trạng thái
        private void SetLbStatus()
        {
            switch (dmdkSUA.TrangThaiXuLy)
            {
                case 0:
                    lblTrangThai.Text = "Chờ duyệt";
                    break;
                case 1:
                    lblTrangThai.Text = "Đã duyệt";
                    break;
                case -1:
                    lblTrangThai.Text = "Chưa khai báo";
                    break;
                case 2:
                    lblTrangThai.Text = "Không phê duyệt";
                    break;
                case -3:
                    lblTrangThai.Text = "Đã khai báo nhưng chưa có phản hồi";
                    break;
            }
        }

        #endregion
    }
}
