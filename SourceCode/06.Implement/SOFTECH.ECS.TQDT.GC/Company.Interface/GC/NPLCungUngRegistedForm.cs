﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean=Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.GC.BLL.KDT.SXXK;
using System.Xml.Serialization;
using Company.Interface.KDT.GC;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.GC
{
	public partial class NPLCungUngRegistedForm : BaseForm
	{
        private BKCungUngDangKyCollection BKCungUngCollection = new BKCungUngDangKyCollection();
        private BKCungUngDangKy BKCU = new BKCungUngDangKy();
        public long IDHopDong = 0;
        public NPLCungUngRegistedForm()
		{
			InitializeComponent();
		}

		//-----------------------------------------------------------------------------------------

        
		private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
		{
            txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
            this.Cursor = Cursors.WaitCursor;
            this.search();
			this.Cursor = Cursors.Default;

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            BKCungUngDangKy BKCU = (BKCungUngDangKy)e.Row.DataRow;
            NPLCungUngTheoTKSendForm f = new NPLCungUngTheoTKSendForm();
            BKCU.Load();
            if (BKCU.TKMD_ID > 0)
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = BKCU.TKMD_ID;
                TKMD.Load();
                f.TKMD = TKMD;
            }
            else
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = BKCU.TKCT_ID;
                TKCT.Load();
                f.TKCT = TKCT;
            }
            f.BKCU = BKCU;
            f.ShowDialog();
            search();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
          
            string where = string.Format("MaDoanhNghiep='{0}'",GlobalSettings.MA_DON_VI);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
            }


            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
            }
            where += " AND TrangThaiXuLy = 1";
            if (txtSoBangKe.TextLength > 0)
            {
                where += " AND SoBangKe = " + txtSoBangKe.Value;
            }
           
            // Thực hiện tìm kiếm.            
            this.BKCungUngCollection = new  BKCungUngDangKy().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.BKCungUngCollection;
            SetCommandStatus();
           
            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();            
        }
        private void SetCommandStatus()
        {
            
        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }
            }
        }

      
        
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            //HopDongManageForm f = new HopDongManageForm();
            //f.IsBrowseForm = true;
            //f.IsDaDuyet = true;
            //f.ShowDialog();
            //if (f.HopDongSelected.ID > 0)
            //{
            //    txtSoHopDong.Text = f.HopDongSelected.SoHopDong + "(" + f.HopDongSelected.NgayKy.ToString("dd/MM/yyyy") + ")";
            //    this.IDHopDong = f.HopDongSelected.ID;
            //}
        }



        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa không ?", true) == "Yes")
            {                
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        BKCungUngDangKy bkCungUng = (BKCungUngDangKy)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "DMCU";
                        sendXML.master_id = bkCungUng.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_2702012", i.Position + 1);
                            //ShowMessage("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                        }
                        else
                        {
                            if (bkCungUng.ID > 0)
                            {
                                bkCungUng.Delete();
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa không ?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        BKCungUngDangKy bkCungUng = (BKCungUngDangKy)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "DMCU";
                        sendXML.master_id = bkCungUng.ID;
                        if (sendXML.Load())
                        {
                            showMsg("MSG_2702012", i.Position + 1);
                            //ShowMessage("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", false);
                        }
                        else
                        {
                            if (bkCungUng.ID > 0)
                            {
                                bkCungUng.Delete();
                            }
                        }
                    }
                }
                this.search();
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
	}
}