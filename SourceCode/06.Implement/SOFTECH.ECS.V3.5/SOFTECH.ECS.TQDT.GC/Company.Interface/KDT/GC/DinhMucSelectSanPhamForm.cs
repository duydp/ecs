﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Company.Interface.KDT.GC
{
    public partial class DinhMucSelectSanPhamForm : BaseForm
    {
        public Company.GC.BLL.GC.SanPham SPDetail;
        public List<Company.GC.BLL.GC.SanPham> SPSelectCollection = new List<Company.GC.BLL.GC.SanPham>();
        public HopDong HD = new HopDong();     
        public bool isBrower = false;
        public DinhMucSelectSanPhamForm()
        {
            InitializeComponent();
            SPDetail = new  Company.GC.BLL.GC.SanPham();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }



        private void SanPhamGCEditForm_Load(object sender, EventArgs e)
        {            
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            Company.GC.BLL.GC.SanPham sp = new  Company.GC.BLL.GC.SanPham();
            sp.HopDong_ID = HD.ID;
            //dgList.DataSource = sp.SelectCollectionBy_HopDong_ID_ChuaCoDM();
            //KhanhHn 23/06/2012
            dgList.DataSource = sp.SelectCollectionBy_HopDong_ID();
        }

  
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            SPDetail = (Company.GC.BLL.GC.SanPham)e.Row.DataRow;
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    SPDetail = (Company.GC.BLL.GC.SanPham)item.DataRow;
                    SPSelectCollection.Add(SPDetail);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        
    }
}