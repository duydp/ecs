﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class HistoryDetailPhuKienForm : Company.Interface.BaseForm
    {
        public KDT_GC_HangPhuKien_Log HPKCollection = new KDT_GC_HangPhuKien_Log();

        public KDT_GC_LoaiPhuKien_Log LoaiPK = new KDT_GC_LoaiPhuKien_Log();
        public List<KDT_GC_LoaiPhuKien_Log> LoaiPKCollection = new List<KDT_GC_LoaiPhuKien_Log>();
        public HistoryDetailPhuKienForm()
        {
            InitializeComponent();
        }        
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }
        private void BindDataPK()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = LoaiPKCollection;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        LoaiPK = (KDT_GC_LoaiPhuKien_Log)i.GetRow().DataRow;
                        HPKCollection = new KDT_GC_HangPhuKien_Log();
                        LoaiPK.HPKCollection = KDT_GC_HangPhuKien_Log.SelectCollectionBy_Master_ID(LoaiPK.ID);
                        BindData();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }

        private void gridEX1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    switch (LoaiPK.MaPhuKien.Trim())
                    {
                        case "101": { } break;//Hủy hợp đồng
                        case "201": { } break;//Gia hạn hợp đồng
                        #region Sản phẩm
                        case "802":
                            {
                                e.Row.Cells["NhomSP"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSP"].Value);
                                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                            } break;//Thêm mới
                        case "502":
                            {
                                e.Row.Cells["NhomSP"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSP"].Value);
                                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                            } break;//Sửa
                        case "102":
                            {
                                e.Row.Cells["NhomSP"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSP"].Value);
                                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                            } break;//Hủy
                        #endregion Sản phẩm

                        #region Nguyên phụ liệu
                        case "803": { e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim(); } break;//Thêm mới
                        case "503": { e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim(); } break;//Sửa
                        case "103": { e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim(); } break;//Hủy
                        #endregion Nguyên phụ liệu
                        #region Thiết bị
                        case "804":
                            {
                                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                                e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value).Trim();
                                string tinhTrang = e.Row.Cells["TinhTrang"].Value.ToString() == "0" ? "Còn mới" : "Cũ";
                                e.Row.Cells["TinhTrang"].Text = tinhTrang;
                                try
                                {
                                    e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value).Trim();
                                }
                                catch { }
                            } break;//Thêm mới
                        case "504":
                            {
                                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                                e.Row.Cells["TenXuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value).Trim();
                                string tinhTrang = e.Row.Cells["TinhTrang"].Value.ToString() == "0" ? "Còn mới" : "Cũ";
                                e.Row.Cells["TinhTrang"].Text = tinhTrang;
                                try
                                {
                                    e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value).Trim();
                                }
                                catch { }
                            } break;//Sửa
                        case "104":
                            {
                                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                                e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value).Trim();
                                string tinhTrang = e.Row.Cells["TinhTrang"].Value.ToString() == "0" ? "Còn mới" : "Cũ";
                                e.Row.Cells["TinhTrang"].Text = tinhTrang;
                                try
                                {
                                    e.Row.Cells["NguyenTe_ID"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value).Trim();
                                }
                                catch { }
                            } break;//Hủy
                        #endregion Thiết bị
                        #region Hàng mẫu
                        case "805": { e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim(); } break;//Thêm mới
                        case "505": { e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim(); } break;//Sửa
                        case "105": { e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim(); } break;//Hủy
                        #endregion Hàng mẫu
                        #region Sửa thông tin chung HD
                        case "501":{ } break;//sưa thong tin chung Hợp đồng

                        #endregion

                    }                    
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                gridEX1.Refetch();
                gridEX1.DataSource = LoaiPK.HPKCollection;
                gridEX1.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void HistoryDetailPhuKienForm_Load(object sender, EventArgs e)
        {
            BindDataPK();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH LỊCH SỬ HÀNG HOÁ PHỤ KIỆN.xls";
                sfNPL.Filter = "Excel files| *.xls";
                Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter = new Janus.Windows.GridEX.Export.GridEXExporter();
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    gridEXExporter.GridEX = gridEX1;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
