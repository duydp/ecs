﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC01TT117Form : BaseForm
    {
        public DataSet dsNguyenPhuLieu;
        public DataTable dtBangNguyenPhuLieu;
        private BangKe01_HSTK_GC_TT117 BC01;
        private BangKe01_HSTK_GC_TT117 BC01All = new BangKe01_HSTK_GC_TT117();
        public HopDong HD = new HopDong();
        public DataSet dsNPL;
        public XRLabel Label;
        public ToKhaiMauDich TKMD;
        public Company.GC.BLL.GC.NguyenPhuLieu NPL;
        int sotable;
        int soLuongNPL;
        //Linhhtn thêm
        public DataSet dsTK;
        int soLuongTK;
        int soBang;
        DataTable dtToKhai;
        ToKhaiMauDichCollection TKMDCol;
        ToKhaiChuyenTiepCollection TKCTCol;

        public ReportViewBC01TT117Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            //dsNguyenPhuLieu = new DataSet();
            //dtBangNguyenPhuLieu = new DataTable();
            //BC01 = new BangKe01NewTT74();
            //Label = new XRLabel();
            //TKMD = new ToKhaiMauDich();
            //NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
            //dsNPL = TKMD.GetHangMauDichNPLNhapAll(this.HD.ID);
            //soLuongNPL = dsNPL.Tables[0].Rows.Count;
            //sotable = (soLuongNPL - 1) / 6 + 1;
            //this.BC01.report = this;
            //for (int i = 0; i < sotable; i++)
            //{
            //    cbPage.Items.Add("Trang " + (i + 1), i);
            //}
            //if(cbPage.Items.Count > 0)
            //    cbPage.SelectedIndex = 0;
            //btnNext.Enabled = !this.BC01.Last;
            //btnPrevious.Enabled = !this.BC01.First;
            loadReport();
        }
        private void loadReport()
        {
            BC01 = new BangKe01_HSTK_GC_TT117();
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            DataSet dataSource = npl.BaoCaoBC01HSTK_GC_TT117(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            for (int i = 0; i < dataSource.Tables.Count; i++)
            {
                cbPage.Items.Add(dataSource.Tables[i].TableName, dataSource.Tables[i]);
            }
            if (cbPage.Items.Count > 0) cbPage.SelectedIndex = 0;
        }
        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.BC01.HD = this.HD;
            DataTable dt = (DataTable)cbPage.SelectedItem.Value;
            if (dt.Rows.Count == 0) return;
            this.BC01.BindReport(dt);
            printControl1.PrintingSystem = this.BC01.PrintingSystem;
            this.BC01.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }


        public DataTable CreatSchemaDataSetNewRP01(int BangSo)
        {
            DataTable dttemp = new DataTable("dtBangNguyenPhuLieu" + BangSo.ToString());
            DataColumn[] dcCol = new DataColumn[8];

            dcCol[0] = new DataColumn("TKNK", typeof(string));
            dcCol[0].Caption = "TKNK";

            int t = 1;
            for (int k = BangSo * 6; k < (BangSo + 1) * 6; k++)
            {

                if (k < soLuongNPL)
                {
                    string Ma = dsNPL.Tables[0].Rows[k].ItemArray[0].ToString();
                    string DVT_ID = NPL.GetDVTByMa(Ma, this.HD.ID);

                    /* DataSet tạm để lấy tên nguyên phụ liệu */
                    string Ten = "";

                    DataSet dsNPLTemp = NPL.GetNPL(Ma, this.HD.ID);

                    if (dsNPLTemp.Tables[0].Rows.Count > 0)
                        Ten = dsNPLTemp.Tables[0].Rows[0].ItemArray[0].ToString();

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "Mã " + Ma;
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = " Tên : " + Ten + "\r\n Mã : " + Ma + "\r\n ĐV tính : " + DonViTinh_GetName(DVT_ID);
                    t++;
                }
                else
                {
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "";
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = "";
                    t++;
                }
            }

            dcCol[t] = new DataColumn();
            dcCol[t].ColumnName = "GhiChu";
            dcCol[t].DataType = typeof(string);
            dcCol[t].Caption = "GhiChu";
            dttemp.Columns.AddRange(dcCol);


            ToKhaiMauDichCollection TKMDCol = this.HD.GetTKNKBCNew();

            foreach (ToKhaiMauDich TKMD in TKMDCol)
            {
                TKMD.LoadHMDCollection();
                DataRow drData = dttemp.NewRow();
                foreach (HangMauDich HMD in TKMD.HMDCollection)
                {
                    drData[0] = TKMD.SoToKhai.ToString().Trim() + "/" + LoaiHinhMauDich_GetTenVT(TKMD.MaLoaiHinh).Trim() + "/" + GlobalSettings.MA_HAI_QUAN + "\r\n" + TKMD.NgayDangKy.ToShortDateString();
                    for (int n = 1; n <= 6; n++)
                    {
                        string ColName = "Mã " + HMD.MaPhu;
                        if (dttemp.Columns[n].ColumnName == ColName)
                            drData[n] = HMD.SoLuong;
                    }
                }
                for (int n = 1; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }

            }

            ToKhaiChuyenTiepCollection TKCTCol = this.HD.GetTKCTNhapNPL();

            foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
            {
                TKCT.LoadHCTCollection();
                DataRow drData = dttemp.NewRow();
                foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                {
                    drData[0] = TKCT.SoToKhai.ToString().Trim() + "-" + TKCT.MaLoaiHinh.Trim() + "-" + DateTime.Parse(TKCT.NgayDangKy.ToString().Trim()).Year;
                    for (int n = 1; n <= 6; n++)
                    {
                        string ColName = "Mã " + HCT.MaHang;
                        if (dttemp.Columns[n].ColumnName == ColName)
                            drData[n] = HCT.SoLuong;
                    }
                }
                for (int n = 1; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }

            }


            return dttemp;
        }

        public DataTable CreatSchemaDataSetNewRP01TT74(int BangSo)
        {
            DataTable dttemp = new DataTable("dtBangNguyenPhuLieu" + BangSo.ToString());
            DataColumn[] dcCol = new DataColumn[10];

            dcCol[0] = new DataColumn("NPL", typeof(string));
            dcCol[0].Caption = "NPL";

            dcCol[1] = new DataColumn();
            dcCol[1].ColumnName = "DVTinh";
            dcCol[1].DataType = typeof(string);
            dcCol[1].Caption = "Đv tính";


            int t = 2;
            #region Create Column in datatable
            for (int k = BangSo * 5; k < (BangSo + 1) * 5; k++)
            {

                if (k < soLuongTK)
                {
                    string soTK = dsTK.Tables[0].Rows[k].ItemArray[0].ToString();
                    string ngayTK = Convert.ToDateTime(dsTK.Tables[0].Rows[k].ItemArray[2].ToString()).ToShortDateString();
                    string loaiHinh = dsTK.Tables[0].Rows[k].ItemArray[1].ToString();
                    string namDK = Convert.ToDateTime(dsTK.Tables[0].Rows[k].ItemArray[2].ToString()).Year.ToString();
                    string maHQ = dsTK.Tables[0].Rows[k].ItemArray[4].ToString();

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "Số TK " + soTK + loaiHinh.Trim() + namDK;
                    dcCol[t].DataType = typeof(decimal);
                    if (loaiHinh.StartsWith("NGC"))
                        dcCol[t].Caption = "Tờ khai \r\n Số: " + soTK + "/" + LoaiHinhMauDich_GetTenVT(loaiHinh).Trim() + "/" + maHQ + "  \r\n Ngày: " + ngayTK + "\r\n ";
                    else
                        dcCol[t].Caption = "Tờ khai \r\n Số: " + soTK + "-" + loaiHinh.Trim() + "-" + namDK + "  \r\n Ngày: " + ngayTK + "\r\n ";
                    t++;
                }
                else// không có dữ liệu thì tạo cột trống
                {
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "";
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = "";
                    t++;
                }
            }

            dcCol[t] = new DataColumn();
            dcCol[t].ColumnName = "TongCong";
            dcCol[t].DataType = typeof(decimal);
            dcCol[t].Caption = "Tổng cộng";


            dcCol[t + 1] = new DataColumn();
            dcCol[t + 1].ColumnName = "GhiChu";
            dcCol[t + 1].DataType = typeof(string);
            dcCol[t + 1].Caption = "GhiChu";

            dcCol[t + 2] = new DataColumn();
            dcCol[t + 2].ColumnName = "MaNPL";
            dcCol[t + 2].DataType = typeof(string);
            dcCol[t + 2].Caption = "MaNPL";
            dttemp.Columns.AddRange(dcCol);
            #endregion  Create Column in datatable

            //Company.GC.BLL.GC.NguyenPhuLieuCollection nplCol = this.HD.GetNPL();
            decimal Tong = 0;
            //foreach (Company.GC.BLL.GC.NguyenPhuLieu npl in nplCol)
            foreach (DataRow drNPL in dsNPL.Tables[0].Rows)
            {
                string maNPL = drNPL[0].ToString();
                dsNguyenPhuLieu = NPL.GetNPL(maNPL, this.HD.ID);
                DataRow drData = dttemp.NewRow();
                drData[0] = dsNguyenPhuLieu.Tables[0].Rows[0].ItemArray[0].ToString() + " / " + maNPL;
                drData[1] = DonViTinh_GetName(dsNguyenPhuLieu.Tables[0].Rows[0].ItemArray[1].ToString());
                drData[9] = maNPL;
                for (int i = 2; i < 7; i++)
                {

                    foreach (ToKhaiMauDich TKMD in TKMDCol)
                    {
                        string ColName = "Số TK " + TKMD.SoToKhai + TKMD.MaLoaiHinh.Trim() + TKMD.NgayDangKy.Year;
                        if (dttemp.Columns[i].ColumnName == ColName)
                        {
                            TKMD.LoadHMDCollection();
                            foreach (HangMauDich HMD in TKMD.HMDCollection)
                            {
                                if (HMD.MaPhu == maNPL)
                                {
                                    drData[i] = HMD.SoLuong;
                                    Tong += Decimal.Parse(HMD.SoLuong.ToString());
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
                    {
                        string ColName = "Số TK " + TKCT.SoToKhai + TKCT.MaLoaiHinh.Trim() + TKCT.NgayDangKy.Year;
                        if (dttemp.Columns[i].ColumnName == ColName)
                        {
                            TKCT.LoadHCTCollection();
                            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                            {
                                if (HCT.MaHang == maNPL)
                                {
                                    drData[i] = HCT.SoLuong;
                                    Tong += Decimal.Parse(HCT.SoLuong.ToString());
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                dttemp.Rows.Add(drData);// addRow tất cả 
                /*
                 * chỉ addRow những dòng có dữ liệu
                for (int i = 2; i <= 7; i++)
                {
                    if (drData[i].ToString() != "")
                    {
                        drData[7] = Tong;
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }*/

            }
            if (this.BC01.Last)
            {
                foreach (DataRow dr in dttemp.Rows)
                {
                    Decimal tongCong = 0;
                    foreach (ToKhaiMauDich TKMD in TKMDCol)
                    {
                        TKMD.LoadHMDCollection();
                        foreach (HangMauDich HMD in TKMD.HMDCollection)
                        {
                            if (HMD.MaPhu.Trim() == dr[9].ToString().Trim())
                            {
                                tongCong += Decimal.Parse(HMD.SoLuong.ToString());
                                dr[7] = tongCong;
                                break;
                            }
                        }
                    }
                    foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
                    {
                        TKCT.LoadHCTCollection();
                        foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                        {
                            if (HCT.MaHang.Trim() == dr[9].ToString().Trim())
                            {
                                tongCong += Decimal.Parse(HCT.SoLuong.ToString());
                                dr[7] = tongCong;
                                break;
                            }
                        }
                    }
                }
            }
            return dttemp;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex--;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex++;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BC01.setText(this.Label, txtName.Text);
            this.BC01.CreateDocument();
        }

        private void btnPrintAll_Click(object sender, EventArgs e)
        {

            int to = Convert.ToInt32(txtTo.Value);
            int from = Convert.ToInt32(txtFrom.Value);
            this.BC01All.First = true;
            this.BC01All.Last = false;
            this.BC01All.HD = this.HD;
            if (dsNguyenPhuLieu.Tables["dtBangNguyenPhuLieu" + 0] != null)
                this.BC01All.BindReport(this.dsNguyenPhuLieu.Tables["dtBangNguyenPhuLieu" + 0]);
            else
            {
                dtBangNguyenPhuLieu = new DataTable("dtBangNguyenPhuLieu" + 0);
                dtBangNguyenPhuLieu = CreatSchemaDataSetNewRP01(0);
                this.BC01All.BindReport(dtBangNguyenPhuLieu);
                dsNguyenPhuLieu.Tables.Add(dtBangNguyenPhuLieu);

            }
            this.BC01All.CreateDocument();

            for (int i = from; i < to; i++)
            {

                BangKe01New BC01Temp = new BangKe01New();
                BC01Temp.First = false;
                BC01Temp.Last = (from == cbPage.Items.Count);
                BC01Temp.HD = this.HD;
                if (dsNguyenPhuLieu.Tables["dtBangNguyenPhuLieu" + i] != null)
                    BC01Temp.BindReport(this.dsNguyenPhuLieu.Tables["dtBangNguyenPhuLieu" + i]);
                else
                {
                    dtBangNguyenPhuLieu = new DataTable("dtBangNguyenPhuLieu" + i);
                    dtBangNguyenPhuLieu = CreatSchemaDataSetNewRP01(i);
                    BC01Temp.BindReport(dtBangNguyenPhuLieu);
                    dsNguyenPhuLieu.Tables.Add(dtBangNguyenPhuLieu);

                }
                BC01Temp.CreateDocument();
                this.BC01All.Pages.AddRange(BC01Temp.Pages);
            }
            this.BC01All.PrintingSystem.ShowMarginsWarning = false;
            //printControl1.PrintingSystem = this.BC01All.PrintingSystem;
            this.BC01All.PrintDialog();
        }

        private void txtFrom_TextChanged(object sender, EventArgs e)
        {

            if (Convert.ToInt32(txtFrom.Value) < 1) txtFrom.Value = 1;
        }

        private void txtTo_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtTo.Value) > cbPage.Items.Count) txtTo.Value = cbPage.Items.Count;
            if (Convert.ToInt32(txtTo.Value) < 1) txtTo.Value = 1;
        }

    }
}