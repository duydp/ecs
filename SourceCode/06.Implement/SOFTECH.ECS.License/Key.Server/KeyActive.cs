﻿using System;
using System.Collections.Generic;
using System.Text;
using KeySecurity;
using System.IO;

namespace KeyServer
{
    public class KeyActive
    {
        const string fileMesssage = "StatusMessages.xml";
        StatusMessages Status = null;
        public KeyActive()
        {
            try
            {
                if (!File.Exists(fileMesssage))
                {
                    throw new Exception("File not exist");
                }
                string content = KeyCode.ReadFile(fileMesssage);
                Status = KeyCode.Deserialize<StatusMessages>(content);
            }
            catch
            {
                Status = new StatusMessages()
                {
                    Expired = "Hết hạn sử dụng",
                    Invalid = "Số Serial không có giá trị sử dụng \r\nHay đã kích hoạt trước đây .",
                    InternalError = "Kích hoạt không thành công",
                    NotFound = "Số Serial không tồn tại",
                    Licensed = "Kích hoạt phần mềm thành công",
                    TrialVersion = "Kích hoạt phần mềm thành công",
                    MachineHashMismatch = "Bạn đã sao chép bản phần mềm từ máy khác",
                    Lock = "Số Serial đã bị khoá",
                };
                try
                {
                    string content = KeyCode.Serializer(Status, true, true);
                    KeyCode.WriteToFile(content, fileMesssage);
                }
                catch
                {
                }
            }

        }

        public string Active(string keyInfo)
        {
            string ret = string.Empty;
            License lic = null;
            Key key_old = null;
            try
            {
                string keyContent;
                keyContent = KeyCode.Decrypt(keyInfo);
                keyContent = KeyCode.ConvertFromBase64(keyContent);
                lic = KeyCode.Deserialize<License>(keyContent);
                string where = "ActivedKey='" + lic.KeyInfo.Key + "'";
                List<Key> keys = Key.SelectCollectionDynamic(where, "");
                Key key = null;
                if (keys.Count > 0)
                {
                    key = keys[0];
                    if (key.Status != TypeKey.TrialAll && key.ProductId.Trim().ToUpper() != lic.KeyInfo.ProductId.Trim().ToUpper())
                    {
                        lic = new License()
                        {
                            Status = LicenseStatus.Invalid,
                            Message = Status.Invalid
                        };
                    }
                    else if (key.ActivedDate.Year > 1900)
                    {
                        lic = new License()
                        {
                            Status = LicenseStatus.Invalid,
                            Message = Status.Invalid
                        };
                    }
                    else if (key.Status == TypeKey.Lock)
                    {
                        lic = new License()
                        {
                            Status = LicenseStatus.Invalid,
                            Message = Status.Invalid
                        };
                    }
                    else if (key.Status == TypeKey.TrialAll)
                    {
                        #region Active key Trial public

                        string condition = string.Format("ProductID = '{0}' AND MachineCode = '{1}'", lic.KeyInfo.ProductId, lic.KeyInfo.MachineCode);
                        List<TrialManager> listMachineCode = (List<TrialManager>)TrialManager.SelectCollectionDynamic(condition, null);
                        if (listMachineCode == null || listMachineCode.Count == 0)
                        {
                            TrialManager Trial = new TrialManager()
                            {
                                ProductID = lic.KeyInfo.ProductId,
                                MachineCode = lic.KeyInfo.MachineCode,
                                ActivedDate = DateTime.Now,
                                ExpiredDate = DateTime.Now.AddDays(key.Days),
                                Status = "1"
                            };
                            Trial.Insert();
                            lic = new License()
                            {
                                Status = LicenseStatus.TrialVersion,
                                Message = Status.TrialVersion,
                                KeyInfo = new KeyInfo()
                                {
                                    Key = key.ActivedKey,
                                    ExpiredDate = Trial.ExpiredDate,
                                    TrialDays = key.Days,
                                    MachineCode = Trial.MachineCode,
                                    ProductId = Trial.ProductID,
                                    IsShow = false,
                                    Command = key.Command,
                                    Notice = string.Empty
                                }
                            };
                        }
                        else
                        {
                            lic = new License()
                            {
                                Status = LicenseStatus.Invalid,
                                Message = Status.Invalid
                            };
                        }  
                        #endregion
                    }
                    else
                    {
                        #region Kiểm tra key cũ

                        double oldDays = 0; // Số ngày còn lại của key cũ
                        string sql = "Year(ActivedDate)>1900 And MachineCode='" + lic.KeyInfo.MachineCode + "' And ProductID='" + lic.KeyInfo.ProductId + "'" + " AND ExpiredDate > '" + DateTime.Now.ToString("yyyy-MM-dd") + "'"; ;
                        List<Key> listkeys = Key.SelectCollectionDynamic(sql, "ActivedDate desc");
                        oldDays = 0; // Số ngày còn lại của key cũ
                        if (listkeys != null && listkeys.Count > 0)
                        {
                            key_old = listkeys[0];
                            if (key_old.Status == TypeKey.Trial || key_old.Status == TypeKey.License || key_old.Status == TypeKey.TrialAll)
                            {

                                if (key_old.Status == TypeKey.License & key_old.ExpiredDate > DateTime.Now)
                                {
                                    TimeSpan t1 = new TimeSpan(DateTime.Now.Ticks);
                                    TimeSpan t2 = new TimeSpan(key_old.ExpiredDate.Ticks);
                                    oldDays = (t2 - t1).Days > 0 ? (t2 - t1).Days : 0;
                                }
                                string notes = key_old.Notes;
                                notes += string.Format(", ({0}) LOCK vì Kích hoạt key mới", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                                key_old.Notes = notes;
                                key_old.Notice = string.Empty;
                                key_old.Status = TypeKey.Lock;
                                key_old.LastCheckStatus = DateTime.Now;
                                key_old.Update();
                            }
                        }

                        #endregion

                        key.ActivedDate = DateTime.Now;
                        key.ExpiredDate = DateTime.Now.AddDays(key.Days + oldDays);
                        if (key.RequestedDate.Year < 1900)
                            key.RequestedDate = new DateTime(1900, 1, 1);
                        key.MachineCode = lic.KeyInfo.MachineCode;
                        key.LastCheckStatus = DateTime.Now;
                        key.Update();
                        lic = new License()
                        {
                            Status = key.Status == TypeKey.License ? LicenseStatus.Licensed : LicenseStatus.TrialVersion,
                            Message = key.Status == TypeKey.License ? Status.Licensed : Status.TrialVersion,
                            KeyInfo = new KeyInfo()
                            {
                                Key = key.ActivedKey,
                                ExpiredDate = key.ExpiredDate,
                                TrialDays = key.Days,
                                MachineCode = lic.KeyInfo.MachineCode,
                                ProductId = key.ProductId,
                                IsShow = false,
                                Command = key.Command,
                                Notice = string.Empty
                            }
                        };
                    }
                }
                else
                {
                    lic = new License()
                    {
                        Status = LicenseStatus.NotFound,
                        Message = Status.NotFound
                    };
                }
            }
            catch
            {
                lic = new License()
               {
                   KeyInfo = null,
                   Status = LicenseStatus.InternalError,
                   Message = Status.InternalError
               };
            }
            string base64 = KeyCode.ConvertToBase64(KeyCode.Serializer(lic, true, true));
            ret = KeyCode.Encrypt(base64);
            return ret;
        }
        public string CheckInfo(string keyInfo)
        {
            string ret = string.Empty;
            License lic = null;
            try
            {
                string keyContent;
                keyContent = KeyCode.Decrypt(keyInfo);
                keyContent = KeyCode.ConvertFromBase64(keyContent);
                lic = KeyCode.Deserialize<License>(keyContent);
                string where = "Year(ActivedDate)>1900 And MachineCode='" + lic.KeyInfo.MachineCode + "' And ProductID='" + lic.KeyInfo.ProductId + "'" + " AND ExpiredDate > '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                List<Key> keys = Key.SelectCollectionDynamic(where, "ActivedDate desc");
                //Key key = null;
                if (keys.Count > 0)
                {
                    Key key = keys[0];

                    bool isShow = false;
                    isShow = !string.IsNullOrEmpty(key.CustomerField10);
                    key.CustomerField10 = string.Empty;
                    if (isShow)
                        key.CustomerField10 = bool.TrueString;

                    if (key.MachineCode == "BCCD9543209EBFBC7FE9DCE45")
                    {
                        if (key.CustomerCode !="4000395355")
                        {
                            lic = new License()
                            {
                                KeyInfo = null,
                                Status = LicenseStatus.NotFound,
                                Message = Status.NotFound
                            };
                        }
                        else
                        {
                            if (key.Status == TypeKey.Lock)
                            {
                                //lic = new License()
                                //{
                                //    Status = LicenseStatus.Lock,
                                //    Message = string.IsNullOrEmpty(key.Notice) ? Status.Lock : Status.Lock + ": " + key.Notice,
                                //    KeyInfo = null,
                                //};
                                lic = new License()
                                {
                                    KeyInfo = null,
                                    Status = LicenseStatus.NotFound,
                                    Message = Status.NotFound
                                };
                            }
                            else if (key.Status == TypeKey.Trial || key.Status == TypeKey.License)
                            {
                                TimeSpan t1 = new TimeSpan(DateTime.Now.Ticks);
                                TimeSpan t2 = new TimeSpan(key.ExpiredDate.Ticks);
                                key.LastCheckStatus = DateTime.Now;
                                key.Update();
                                lic = new License()
                                {
                                    Status = key.Status == TypeKey.License ? LicenseStatus.Licensed : LicenseStatus.TrialVersion,
                                    Message = key.Status == TypeKey.License ? Status.Licensed : Status.TrialVersion,
                                    KeyInfo = new KeyInfo()
                                    {
                                        Key = key.ActivedKey,
                                        ExpiredDate = key.ExpiredDate,
                                        TrialDays = (t2 - t1).Days,
                                        MachineCode = lic.KeyInfo.MachineCode,
                                        ProductId = key.ProductId,
                                        IsShow = isShow,
                                        Command = key.Command,
                                        Notice = isShow ? key.Notice : string.Empty
                                    }
                                };
                            }
                        }
                    }
                    else
                    {
                        if (key.Status == TypeKey.Lock)
                        {
                            //lic = new License()
                            //{
                            //    Status = LicenseStatus.Lock,
                            //    Message = string.IsNullOrEmpty(key.Notice) ? Status.Lock : Status.Lock + ": " + key.Notice,
                            //    KeyInfo = null,
                            //};
                            lic = new License()
                            {
                                KeyInfo = null,
                                Status = LicenseStatus.NotFound,
                                Message = Status.NotFound
                            };
                        }
                        else if (key.Status == TypeKey.Trial || key.Status == TypeKey.License)
                        {
                            TimeSpan t1 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan t2 = new TimeSpan(key.ExpiredDate.Ticks);
                            key.LastCheckStatus = DateTime.Now;
                            key.Update();
                            lic = new License()
                            {
                                Status = key.Status == TypeKey.License ? LicenseStatus.Licensed : LicenseStatus.TrialVersion,
                                Message = key.Status == TypeKey.License ? Status.Licensed : Status.TrialVersion,
                                KeyInfo = new KeyInfo()
                                {
                                    Key = key.ActivedKey,
                                    ExpiredDate = key.ExpiredDate,
                                    TrialDays = (t2 - t1).Days,
                                    MachineCode = lic.KeyInfo.MachineCode,
                                    ProductId = key.ProductId,
                                    IsShow = isShow,
                                    Command = key.Command,
                                    Notice = isShow ? key.Notice : string.Empty
                                }
                            };
                        }
                    }
                }
                else
                {
                    #region Check key trial public
                    List<TrialManager> listTrial = (List<TrialManager>)TrialManager.SelectCollectionDynamic("MachineCode='" + lic.KeyInfo.MachineCode + "' And ProductID='" + lic.KeyInfo.ProductId + "'" + " AND ExpiredDate > '" + DateTime.Now.ToString("yyyy-MM-dd"), null);
                    if (listTrial != null && listTrial.Count > 0)
                    {
                        TrialManager trial = listTrial[0];
                        if (trial.Status == TypeKey.Lock)
                        {
                            //lic = new License()
                            //{
                            //    Status = LicenseStatus.Lock,
                            //    Message = string.IsNullOrEmpty(key.Notice) ? Status.Lock : Status.Lock + ": " + key.Notice,
                            //    KeyInfo = null,
                            //};
                            lic = new License()
                            {
                                KeyInfo = null,
                                Status = LicenseStatus.NotFound,
                                Message = Status.NotFound
                            };
                        }
                        else
                        {
                            TimeSpan t1 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan t2 = new TimeSpan(trial.ExpiredDate.Ticks);
                            trial.LastCheckStatus = DateTime.Now;
                            trial.Update();
                            lic = new License()
                            {
                                Status = trial.Status == TypeKey.License ? LicenseStatus.Licensed : LicenseStatus.TrialVersion,
                                Message = trial.Status == TypeKey.License ? Status.Licensed : Status.TrialVersion,
                                KeyInfo = new KeyInfo()
                                {
                                    // Key = trial.ActivedKey,
                                    Key = "Trial version",
                                    ExpiredDate = trial.ExpiredDate,
                                    TrialDays = (t2 - t1).Days,
                                    MachineCode = lic.KeyInfo.MachineCode,
                                    ProductId = trial.ProductID,
                                    // IsShow = isShow,
                                    // Command = key.Command,
                                    Notice = trial.IsNotice ? trial.Notice : string.Empty
                                }
                            };
                        }
                    }
                    else
                    {
                        lic = new License()
                      {
                          KeyInfo = null,
                          Status = LicenseStatus.NotFound,
                          Message = Status.NotFound
                      };
                    }
                    #endregion
                }
            }
            catch
            {
                lic = new License()
                {
                    KeyInfo = null,
                    Status = LicenseStatus.MachineHashMismatch,
                    Message = Status.MachineHashMismatch
                };
            }
            string base64 = KeyCode.ConvertToBase64(KeyCode.Serializer(lic, true, true));
            ret = KeyCode.Encrypt(base64);
            return ret;
        }
    }
}
