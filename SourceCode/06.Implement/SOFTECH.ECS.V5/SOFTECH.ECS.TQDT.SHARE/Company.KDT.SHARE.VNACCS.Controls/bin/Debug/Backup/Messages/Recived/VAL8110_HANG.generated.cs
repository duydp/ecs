﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS.Messages.Recived
{
   public partial class VAL8110_HANG :BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute D01 { get; set; }
        public PropertiesAttribute D02 { get; set; }
        public PropertiesAttribute D03 { get; set; }
        public PropertiesAttribute D04 { get; set; }
        public PropertiesAttribute D05 { get; set; }
        public PropertiesAttribute D06 { get; set; }
        public PropertiesAttribute D07 { get; set; }

        public VAL8110_HANG()
        {
            D01 = new PropertiesAttribute(12, typeof(string));
            D02 = new PropertiesAttribute(15, typeof(int));
            D03 = new PropertiesAttribute(4, typeof(string));
            D04 = new PropertiesAttribute(15, typeof(int));
            D05 = new PropertiesAttribute(4, typeof(string));
            D06 = new PropertiesAttribute(15, typeof(int));
            D07 = new PropertiesAttribute(4, typeof(int));
        }
    }

}
