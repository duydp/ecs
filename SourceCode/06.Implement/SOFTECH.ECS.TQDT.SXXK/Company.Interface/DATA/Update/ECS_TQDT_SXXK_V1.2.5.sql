/*
Run this script on:

        ecsteam\sqlexpress.ECS_TQDT_SXXK    -  This database will be modified

to synchronize it with:

        ECSTEAM.ECS_TQDT_SXXK_VERSION

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 11/03/2011 6:57:30 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[USER_GROUP]'
GO
ALTER TABLE [dbo].[USER_GROUP] DROP
CONSTRAINT [FK_USER_GROUP_SXXK_GROUPS1]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[USER_GROUP]'
GO
ALTER TABLE [dbo].[USER_GROUP] WITH NOCHECK ADD
CONSTRAINT [FK_USER_GROUP_SXXK_GROUPS1] FOREIGN KEY ([MA_NHOM]) REFERENCES [dbo].[GROUPS] ([MA_NHOM]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.2.5', Date = getdate()