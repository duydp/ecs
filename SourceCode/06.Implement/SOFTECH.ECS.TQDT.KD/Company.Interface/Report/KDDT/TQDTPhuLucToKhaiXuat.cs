﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;

namespace Company.Interface.Report.KDTD
{
    public partial class TQDTPhuLucToKhaiXuat : DevExpress.XtraReports.UI.XtraReport
    {
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public int SoToKhai;
        public DateTime NgayDangKy;
        public ToKhaiMauDich TKMD;
        public Company.Interface.Report.ReportViewTKXTQDTForm report;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public int soDongHang;
        public bool isCuaKhau;

        public TQDTPhuLucToKhaiXuat()
        {
            InitializeComponent();
        }
        public void BindReport(string pls)
        {
            try
            {
                xrLabel3.Text = this.TKMD.MaLoaiHinh;//.Substring(1, 2);
                this.PrintingSystem.ShowMarginsWarning = false;
                decimal tongTriGiaNT = 0;
                decimal tongThueXNK = 0;
                decimal tongThueGTGT = 0;
                decimal tongTriGiaThuKhac = 0;
                //lblMaHaiQuan.Text = GlobalSettings.MA_HAI_QUAN;
                //lblChiCucHQ.Text = GlobalSettings.MA_CUC_HAI_QUAN;
                //lblChiCucHQCK.Text = GlobalSettings.MA_HAI_QUAN;
                if (isCuaKhau)
                {
                    string chicuc = Company.BLL.KDT.SXXK.NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                    if (chicuc == string.Empty)
                        chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                    lblChiCucHQCK.Text = chicuc;
                    lblChiCucHQ.Text = Company.KD.BLL.DuLieuChuan.DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                }
                else
                    lblChiCucHQCK.Text = string.Empty;
                

                if (TKMD.NgayDangKy > new DateTime(1900, 1, 1))
                    lblNgayDangKy.Text = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                else
                    lblNgayDangKy.Text = "";

                if (TKMD.SoToKhai != 0)
                    lblSoToKhai.Text = TKMD.SoToKhai + "";
                else
                    lblSoToKhai.Text = "";


                lblThongBaoMienThue.Text = GlobalSettings.TieuDeInDinhMuc;
                lblThongBaoMienThueGTGT.Text = GlobalSettings.MienThueGTGT;

                lblThongBaoMienThue.Visible = MienThue1;
                lblThongBaoMienThueGTGT.Visible = MienThue2;

                xrLabel1.Text = pls;
                for (int i = 0; i < this.HMDCollection.Count; i++)
                {
                    XRControl control = new XRControl();
                    HangMauDich hmd = this.HMDCollection[i];

                    //STT Hang. Hungtq Update 18/12/2010
                    control = this.xrTable1.Rows[i + 3].Controls["STT" + (i + 1)];
                    //control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * 9)).ToString(); //Comment by Hungtq
                    control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * soDongHang)).ToString();

                    control = this.xrTable1.Rows[i + 3].Controls["TenHang" + (i + 1)];//2
                    //if(control !=null )
                    if (!inMaHang)
                        control.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaPhu.Trim().Length > 0)
                            control.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            control.Text = hmd.TenHang;
                    }
                    control.WordWrap = true;
                    control.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));

                    control = this.xrTable1.Rows[i + 3].Controls["MaHS" + (i + 1)];
                    control.Text = hmd.MaHS;

                    control = this.xrTable1.Rows[i + 3].Controls["XuatXu" + (i + 1)];
                    control.Text = hmd.NuocXX_ID;

                    control = this.xrTable1.Rows[i + 3].Controls["Luong" + (i + 1)];
                    control.Text = hmd.SoLuong.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.SoLuongHMD));

                    control = this.xrTable1.Rows[i + 3].Controls["DVT" + (i + 1)];
                    control.Text = DonViTinh.GetName((object)hmd.DVT_ID);

                    //if (hmd.FOC)
                    //{
                    //    control = this.xrTable1.Rows[i + 3].Controls["DonGiaNT" + (i + 1)];
                    //    control.Text = hmd.DonGiaKB.ToString("G10");

                    //    control = this.xrTable1.Rows[i + 3].Controls["TriGiaNT" + (i + 1)];
                    //    control.Text = "F.O.C";

                    //    control = this.xrTable2.Rows[i + 3].Controls["TriGiaTT" + (i + 1)];
                    //    control.Text = "";


                    //    control = this.xrTable2.Rows[i + 3].Controls["ThueSuatXNK" + (i + 1)];
                    //    control.Text = "";


                    //    control = this.xrTable2.Rows[i + 3].Controls["TienThueXNK" + (i + 1)];
                    //    control.Text = "";


                    //    control = this.xrTable2.Rows[i + 3].Controls["TriGiaTTGTGT" + (i + 1)];
                    //    double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB;
                    //    control.Text = "";


                    //    control = this.xrTable2.Rows[i + 3].Controls["ThueSuatGTGT" + (i + 1)];
                    //    control.Text = "";


                    //    control = this.xrTable2.Rows[i + 3].Controls["TienThueGTGT" + (i + 1)];
                    //    control.Text = "";



                    //    control = this.xrTable2.Rows[i + 3].Controls["TyLeThuKhac" + (i + 1)];

                    //    control.Text = "";

                    //    control = this.xrTable2.Rows[i + 3].Controls["TriGiaThuKhac" + (i + 1)];

                    //    control.Text = "";
                    //}
                    //else
                    //{
                    control = this.xrTable1.Rows[i + 3].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.SoThapPhan.DonGiaNT));

                    control = this.xrTable1.Rows[i + 3].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hmd.TriGiaKB.ToString("N2");

                    //STT Hang. Hungtq Update 18/12/2010
                    control = this.xrTable2.Rows[i + 2].Controls["STT2" + (i + 1)];
                    //control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * 9)).ToString(); //Comment by Hungtq
                    control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * soDongHang)).ToString();

                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                    control.Text = hmd.TriGiaTT.ToString("N0");

                    if (MienThue1) control.Text = "";
                    //if (hmd.MienThue == 1) control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatXNK" + (i + 1)];
                    //if (hmd.ThueTuyetDoi)
                    //{
                    //    control.Text = "";
                    //}
                    //else
                    //{
                    //    if (hmd.ThueSuatXNKGiam.Length == 0)
                    control.Text = hmd.ThueSuatXNK.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(5));
                    //    else
                    //        control.Text = hmd.ThueSuatXNKGiam;
                    //}
                    //if (MienThue1) control.Text = "";
                    //if (hmd.MienThue == 1) control.Text = "";
                    control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                    control.Text = hmd.ThueXNK.ToString("N0");
                    if (MienThue1) control.Text = "";
                    //if (hmd.MienThue == 1) control.Text = "";
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        //control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        //decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                        //control.Text = TriGiaTTGTGT.ToString("N0");
                        //if (MienThue2) control.Text = "";

                        //control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        //if (hmd.ThueSuatVATGiam.Length == 0)
                        //control.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    control.Text = hmd.ThueSuatVATGiam;
                        //if (MienThue2) control.Text = "";
                        //control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        //control.Text = hmd.ThueGTGT.ToString("N0");
                        //if (MienThue2) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];
                        if (Convert.ToDouble(hmd.TyLeThuKhac) > 0)
                            control.Text = hmd.TyLeThuKhac.ToString("N0");
                        else
                            control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];
                        if (Convert.ToDouble(hmd.TriGiaThuKhac) > 0)
                            control.Text = hmd.TriGiaThuKhac.ToString("N0");
                        else
                            control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                    }
                    else if (hmd.ThueGTGT == 0 && hmd.ThueTTDB > 0)
                    {
                        //control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        //decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                        //control.Text = TriGiaTTTTDB.ToString("N0");
                        //if (MienThue2) control.Text = "";

                        //control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        //if (hmd.ThueSuatTTDBGiam.Length == 0)
                        //control.Text = hmd.ThueSuatTTDB.ToString("N0");
                        // else
                        //    control.Text = hmd.ThueSuatTTDBGiam;
                        //if (MienThue2) control.Text = "";
                        //control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        //control.Text = hmd.ThueTTDB.ToString("N0");
                        //if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];

                        control.Text = hmd.TyLeThuKhac.ToString("N0");
                        //if (hmd.MienThue == 1) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.TriGiaThuKhac.ToString("N0");
                        //if (hmd.MienThue == 1) control.Text = "";
                    }
                    else if (hmd.ThueGTGT > 0 && hmd.ThueTTDB > 0)
                    {
                        //control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        //decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT + hmd.ThueXNK);
                        //control.Text = TriGiaTTTTDB.ToString("N0");
                        //if (MienThue2) control.Text = "";

                        //control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        // if (hmd.ThueSuatTTDBGiam.Length == 0)
                        //control.Text = hmd.ThueSuatTTDB.ToString("N0");
                        //else
                        //   control.Text = hmd.ThueSuatTTDBGiam;
                        //if (MienThue2) control.Text = "";
                        //control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        //control.Text = hmd.ThueTTDB.ToString("N0");
                        //if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];

                        //if (hmd.ThueSuatVATGiam.Length == 0)
                        control.Text = hmd.ThueSuatGTGT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(5));
                        //else
                        //    control.Text = hmd.ThueSuatVATGiam;
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.ThueGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                    }
                    // }

                    //if (!hmd.FOC)
                    //{
                    tongTriGiaNT += Convert.ToDecimal(Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero));
                    tongThueXNK += Convert.ToDecimal(hmd.ThueXNK);
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                        tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {

                        tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                        tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                    {
                        tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                        tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                    }
                    //}
                }
                lblTongbangso.Text = this.TinhTongThueHMD().ToString();
                string s = Company.KD.BLL.Utils.VNCurrency.ToString((decimal)this.TinhTongThueHMD()).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);
                lblTongThueXNKChu.Text = s.Replace("  ", " ");

                lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
                lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
                //lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                if (tongTriGiaThuKhac > 0)
                    lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
                else
                    lblTongTriGiaThuKhac.Text = "";
                if (MienThue1) lblTongTienThueXNK.Text = "";
                //if (MienThue2) lblTongTienThueGTGT.Text = "";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Co 1 loi ko xac dinh duoc la do hang ko nhap gia tri FOC ma de trong, bat buoc phai nhap = 0: Khi import excel
            }
        }
        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {                //if (!hmd.FOC)
                tong += Convert.ToDecimal(hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac);
            }
            return tong;
        }
        public void setVisibleImage(bool t)
        {
            //xrPictureBox1.Visible = t;
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            // lblThongBaoMienThue.Visible = t;
            xrTable2.Visible = !t;
            lblTongTienThueXNK.Visible = lblTongTriGiaThuKhac.Visible = !t;
        }
        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        public void setThongTin(XRControl cell, string thongTin)
        {
            if (cell.Name.Equals("lblDeXuatKhac"))
                cell.Text = "28. Đề xuất khác: " + thongTin;
            else
                cell.Text = thongTin;
        }

        private void TenHang1_PreviewClick_1(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang4_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang5_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang6_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang7_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang8_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang9_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
    }
}
