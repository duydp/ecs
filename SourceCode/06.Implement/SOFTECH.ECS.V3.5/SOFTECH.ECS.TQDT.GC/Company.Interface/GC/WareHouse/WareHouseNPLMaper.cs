﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseNPLMaper : BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_NGUYENPHULIEU NPL = new T_KHOKETOAN_NGUYENPHULIEU();
        public T_KHOKETOAN_NGUYENPHULIEU_MAP NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
        public bool isAdd = true;
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        public HopDong HD;
#endif
        public WareHouseNPLMaper()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhKT.DataSource = this._DonViTinh;
            txtDonViTinhKT.DisplayMember = "Ten";
            txtDonViTinhKT.ValueMember = "Ten";
            txtDonViTinhKT.ReadOnly = false;

            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "Ten";
            txtDonViTinhNPL.ReadOnly = false;

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";

            cbbTK.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTK.DisplayMember = "MATK";
            cbbTK.ValueMember = "MATK";
        }

        private void WareHouseNPLMaper_Load(object sender, EventArgs e)
        {
            SetNPL();
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = NPL.NPLMapCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);               
            }
        }
        private void GetNPLMap()
        {
            try
            {

                NPLMap.MANPL = txtMaNPL.Text.ToString();
                NPLMap.TENNPL = txtTenNPL.Text.ToString();
                NPLMap.DVT = txtDonViTinhNPL.Text.ToString();
                NPLMap.MANPLMAP = txtMaNPLKT.Text.ToString();
                NPLMap.TENNPLMAP = txtTenNPLKT.Text.ToString();
                NPLMap.DVT = txtDonViTinhKT.Text.ToString();
                NPLMap.TYLEQD = Convert.ToDecimal(txtTLQD.Text);
                NPLMap.HOPDONG_ID = HD.ID;
                if (isAdd)
                    NPL.NPLMapCollection.Add(NPLMap);
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }
        private void GetNPL()
        {
            NPL.MANPL = txtMaNPLKT.Text.ToString();
            NPL.TENNPL = txtTenNPLKT.Text.ToString();
            NPL.MAHS = ctrMaSoHang.Code;
            NPL.DVT = txtDonViTinhKT.Text.ToString();
            NPL.TENTIENGANH = txtTenTiengAnh.Text.ToString();
            NPL.GHICHU = txtGhiChu.Text.ToString();
            NPL.MAKHO = cbbMaKho.Text.ToString();
            NPL.TAIKHOAN = cbbTK.Text.ToString();
            NPL.DONHANG = txtDonHang.Text.ToString();
            NPL.NGUOIBAN = txtNguoiBan.Text.ToString();
            NPL.KICHTHUOC = txtKichThuoc.Text.ToString();
            NPL.MAUSAC = txtMauSac.Text.ToString();
            NPL.HOPDONG_ID = HD.ID;

            GetNPLMap();
        }
        private void SetNPL()
        {
            txtMaNPLKT.Text = NPL.MANPL;
            txtTenNPLKT.Text = NPL.TENNPL;
            ctrMaSoHang.Code = NPL.MAHS;
            txtDonViTinhKT.Text = NPL.DVT;
            txtTenTiengAnh.Text = NPL.TENTIENGANH;
            txtGhiChu.Text = NPL.GHICHU;
            cbbMaKho.Value = NPL.MAKHO;
            cbbTK.Value = NPL.TAIKHOAN;
            txtDonHang.Text = NPL.DONHANG;
            txtNguoiBan.Text = NPL.NGUOIBAN;
            txtMauSac.Text = NPL.MAUSAC;
            txtKichThuoc.Text = NPL.KICHTHUOC;

            BindData();
        }
        private void SetNPLMap()
        {
            txtMaNPL.Text = NPLMap.MANPL;
            txtTenNPL.Text = NPLMap.TENNPL;
            txtDonViTinhNPL.Text = NPLMap.DVT;
            txtTLQD.Text = NPLMap.TYLEQD.ToString();
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                ctrMaSoHang.SetValidate = !isOnlyWarning;
                ctrMaSoHang.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaSoHang.IsValidate;

                isValid &= ValidateControl.ValidateNull(txtMaNPLKT, errorProvider, " Mã Nguyên phụ liệu ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPLKT, errorProvider, " Tên Nguyên phụ liệu ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinhKT, errorProvider, " Đơn vị tính ", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtMaNPL, errorProvider, " Mã Nguyên phụ liệu ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, " Tên Nguyên phụ liệu ");
                isValid &= ValidateControl.ValidateNull(txtMaHSNPL, errorProvider, " Mã HS ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinhNPL, errorProvider, " Đơn vị tính ", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtTLQD, errorProvider, " Tỷ lệ quy đổi ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetNPL();
                CheckExitNPL();
                NPL.InsertUpdateFull();
                isAdd = true;
                BindData();
                NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                txtMaNPL.Text = String.Empty;
                txtTenNPL.Text = String.Empty;
                txtMaHSNPL.Text = String.Empty;
                txtDonViTinhNPL.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void CheckExitNPL()
        {
            //Kiểm tra NPL Map đã được khai báo .
            //List<T_KHOKETOAN_NGUYENPHULIEU> NPLCollection = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("HOPDONG_ID = " + HD.ID + " AND MANPL = '" + txtMaNPLKT.Text.ToString().Trim() + "'", "");
            //if (NPLCollection.Count >= 1)
            //{
            //    errorProvider.SetError(txtMaNPLKT, "MÃ NGUYÊN PHỤ LIỆU : ''" + txtMaNPLKT.Text.ToString() + "'' NÀY ĐÃ MAP TRƯỚC ĐÓ .");
            //    return;
            //}
            //if (NPL.ID == 0)
            //{
            //}
            //else
            //{
            //    foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPL.NPLMapCollection)
            //    {
            //        if (item.MANPLMAP != txtMaNPLKT.Text.ToString().Trim())
            //        {
            //            errorProvider.SetError(txtMaNPLKT, "Mã NPL : ''" + txtMaNPLKT.Text.ToString() + "'' này khác với mã NPL đã có ở danh sách phía dưới .");
            //            return;
            //        }
            //    }
            //}
            if (isAdd)
            {
                //Kiểm tra NPL VNACCS đã có trên lưới .
                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPL.NPLMapCollection)
                {
                    if (item.MANPLMAP == txtMaNPL.Text.ToString().Trim())
                    {
                        errorProvider.SetError(txtMaNPL, "MÃ NGUYÊN PHỤ LIỆU : ''" + txtMaNPL.Text.ToString() + "'' NÀY ĐÃ CÓ TRONG DANH SÁCH PHÍA DƯỚI .");
                        return;
                    }
                }
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<T_KHOKETOAN_NGUYENPHULIEU_MAP> ItemColl = new List<T_KHOKETOAN_NGUYENPHULIEU_MAP>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_NGUYENPHULIEU_MAP)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        NPL.NPLMapCollection.Remove(item);
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType==Janus.Windows.GridEX.RowType.Record)
                {
                    NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                    NPLMap = (T_KHOKETOAN_NGUYENPHULIEU_MAP)e.Row.DataRow;
                    isAdd = false;
                    SetNPLMap();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;
#if SXXK_V4
            if (this.loaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    ctrMaSoHang.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                   
                }
            txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);

            }
            else if (this.loaiHangHoa == "N")
            {
                if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm.isBrowser = true;
                this.NPLRegistedForm.ShowDialog(this);
                if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                }
                txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
            }
            else 
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                    ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                }
                txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
            }
#elif GC_V4
            this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
            this.NPLRegistedForm.isBrower = true;
            this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
            this.NPLRegistedForm.ShowDialog();
            if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
            {
                txtMaNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                txtTenNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                txtMaHSNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                txtDonViTinhNPL.Text = DonViTinh_GetName(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID);
            }
#endif
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
#if GC_V4
            bool isNotFound = false;
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            npl.HopDong_ID = HD.ID;
            npl.Ma = txtMaNPL.Text.Trim();
            if (npl.Load())
            {
                txtMaNPL.Text = npl.Ma;
                ctrMaSoHang.Code = npl.MaHS;
                txtTenNPL.Text = npl.Ten;
                txtDonViTinhNPL.Text = DonViTinh_GetName(npl.DVT_ID);

            }
            else
            {
                isNotFound = true;
            }
            if (isNotFound)
            {

                txtMaNPL.Text = txtDonViTinhNPL.Text = string.Empty;
                return;
            }
#elif SXXK_V4
            if (loaiHangHoa == "N")
            {
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.Ma = txtMaHangHoa.Text;
                if (npl.Load())
                {
                    txtMaHangHoa.Text = npl.Ma;
                    //if (ctrMaSoHang.Code.Trim().Length == 0)
                        ctrMaSoHang.Code = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));

                }


            }
            else if (loaiHangHoa == "S")
            {
                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = txtMaHangHoa.Text;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (sp.Load())
                {
                    txtMaHangHoa.Text = sp.Ma;
                    //if (ctrMaSoHang.Code.Trim().Length == 0)
                        ctrMaSoHang.Code = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));

                }
            }
#endif
        }

        private void btnAddExcel_Click(object sender, EventArgs e)
        {
            try
            {
                WareHouseNPLSPMapperReadExcel f = new WareHouseNPLSPMapperReadExcel();
                f.HD = HD;
                f.NPL = NPL;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
