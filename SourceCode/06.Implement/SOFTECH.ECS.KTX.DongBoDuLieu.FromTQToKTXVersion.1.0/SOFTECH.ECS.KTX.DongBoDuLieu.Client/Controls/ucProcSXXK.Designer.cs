﻿namespace DongBo.Controls
{
    partial class ucProcSXXK
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pBar = new System.Windows.Forms.ProgressBar();
            this.lblContent = new System.Windows.Forms.Label();
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.btnXulylai = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnHistory = new System.Windows.Forms.Button();
            this.pnlTop.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.btnHistory);
            this.pnlContent.Controls.Add(this.btnXulylai);
            this.pnlContent.Controls.Add(this.label1);
            this.pnlContent.Controls.Add(this.lblContent);
            this.pnlContent.Controls.Add(this.pBar);
            // 
            // lblCaption
            // 
            this.lblCaption.Size = new System.Drawing.Size(163, 22);
            this.lblCaption.Text = "Đang xử lý dữ liệu";
            // 
            // pBar
            // 
            this.pBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pBar.Location = new System.Drawing.Point(107, 133);
            this.pBar.Name = "pBar";
            this.pBar.Size = new System.Drawing.Size(271, 18);
            this.pBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pBar.TabIndex = 0;
            // 
            // lblContent
            // 
            this.lblContent.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblContent.AutoSize = true;
            this.lblContent.Location = new System.Drawing.Point(177, 117);
            this.lblContent.Name = "lblContent";
            this.lblContent.Size = new System.Drawing.Size(66, 13);
            this.lblContent.TabIndex = 1;
            this.lblContent.Text = "Xử lý dữ liệu";
            // 
            // bgWorker
            // 
            this.bgWorker.WorkerSupportsCancellation = true;
            this.bgWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_DoWork);
            this.bgWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_RunWorkerCompleted);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(104, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tình trạng:";
            // 
            // btnXulylai
            // 
            this.btnXulylai.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXulylai.Location = new System.Drawing.Point(405, 244);
            this.btnXulylai.Name = "btnXulylai";
            this.btnXulylai.Size = new System.Drawing.Size(75, 23);
            this.btnXulylai.TabIndex = 2;
            this.btnXulylai.Text = "Xử lý lại";
            this.btnXulylai.UseVisualStyleBackColor = true;
            this.btnXulylai.Click += new System.EventHandler(this.btnXulylai_Click);
            // 
            // btnHistory
            // 
            this.btnHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHistory.Location = new System.Drawing.Point(8, 246);
            this.btnHistory.Name = "btnHistory";
            this.btnHistory.Size = new System.Drawing.Size(75, 23);
            this.btnHistory.TabIndex = 2;
            this.btnHistory.Text = "Xem nhật ký";
            this.btnHistory.UseVisualStyleBackColor = true;
            this.btnHistory.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // ucProcSXXK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ucProcSXXK";
            this.Load += new System.EventHandler(this.ucProcSXXK_Load);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar pBar;
        private System.Windows.Forms.Label lblContent;
        private System.ComponentModel.BackgroundWorker bgWorker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnXulylai;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnHistory;
    }
}
