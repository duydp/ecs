namespace Licenses_v2.Module
{
    partial class FrmInvoiceInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvoiceInfo));
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblCaption = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtInvID = new DevExpress.XtraEditors.TextEdit();
            this.btnCustNew = new DevExpress.XtraEditors.SimpleButton();
            this.btnCustEdit = new DevExpress.XtraEditors.SimpleButton();
            this.cboxCustID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnInvSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnInvCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnInvClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnInvEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnInvDetailNew = new DevExpress.XtraEditors.SimpleButton();
            this.btnInvDetailDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnInvDetailEdit = new DevExpress.XtraEditors.SimpleButton();
            this.gridInvDetail = new DevExpress.XtraGrid.GridControl();
            this.gridData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.txtInvEmployeeID = new DevExpress.XtraEditors.TextEdit();
            this.txtInvCreateDate = new DevExpress.XtraEditors.TextEdit();
            this.txtInvNotes = new DevExpress.XtraEditors.MemoEdit();
            this.txtCustEmail = new DevExpress.XtraEditors.TextEdit();
            this.txtCustCity = new DevExpress.XtraEditors.TextEdit();
            this.txtCustAddress = new DevExpress.XtraEditors.MemoEdit();
            this.txtCustPhone = new DevExpress.XtraEditors.TextEdit();
            this.txtCustFirstName = new DevExpress.XtraEditors.TextEdit();
            this.txtCustName = new DevExpress.XtraEditors.TextEdit();
            this.txtCustLastName = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.groupProdInfo = new DevExpress.XtraEditors.GroupControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboxCustID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridInvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvEmployeeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvCreateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupProdInfo)).BeginInit();
            this.groupProdInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelTop.Controls.Add(this.pnlTop);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(600, 67);
            this.panelTop.TabIndex = 1;
            // 
            // pnlTop
            // 
            this.pnlTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTop.BackgroundImage")));
            this.pnlTop.Controls.Add(this.lblCaption);
            this.pnlTop.Controls.Add(this.pictureBox1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(600, 67);
            this.pnlTop.TabIndex = 7;
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Appearance.Options.UseFont = true;
            this.lblCaption.Appearance.Options.UseForeColor = true;
            this.lblCaption.Location = new System.Drawing.Point(72, 8);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(216, 31);
            this.lblCaption.TabIndex = 2;
            this.lblCaption.Text = "Thông tin hóa đơn";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(69, 67);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // txtInvID
            // 
            this.txtInvID.Location = new System.Drawing.Point(117, 80);
            this.txtInvID.Name = "txtInvID";
            this.txtInvID.Properties.ReadOnly = true;
            this.txtInvID.Size = new System.Drawing.Size(113, 20);
            this.txtInvID.TabIndex = 26;
            // 
            // btnCustNew
            // 
            this.btnCustNew.Image = ((System.Drawing.Image)(resources.GetObject("btnCustNew.Image")));
            this.btnCustNew.Location = new System.Drawing.Point(452, 104);
            this.btnCustNew.Name = "btnCustNew";
            this.btnCustNew.Size = new System.Drawing.Size(76, 22);
            this.btnCustNew.TabIndex = 13;
            this.btnCustNew.Text = "Thêm mới";
            this.btnCustNew.Click += new System.EventHandler(this.btnCustNew_Click);
            // 
            // btnCustEdit
            // 
            this.btnCustEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnCustEdit.Image")));
            this.btnCustEdit.Location = new System.Drawing.Point(533, 104);
            this.btnCustEdit.Name = "btnCustEdit";
            this.btnCustEdit.Size = new System.Drawing.Size(54, 22);
            this.btnCustEdit.TabIndex = 12;
            this.btnCustEdit.Text = "Sửa";
            this.btnCustEdit.Click += new System.EventHandler(this.btnCustEdit_Click);
            // 
            // cboxCustID
            // 
            this.cboxCustID.Location = new System.Drawing.Point(117, 106);
            this.cboxCustID.Name = "cboxCustID";
            this.cboxCustID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboxCustID.Properties.DisplayMember = "Name";
            this.cboxCustID.Properties.NullText = "(Chưa chọn khách hàng)";
            this.cboxCustID.Properties.PopupFormMinSize = new System.Drawing.Size(500, 0);
            this.cboxCustID.Properties.ValueMember = "ID";
            this.cboxCustID.Properties.View = this.gridLookUpEdit1View;
            this.cboxCustID.Size = new System.Drawing.Size(332, 20);
            this.cboxCustID.TabIndex = 11;
            this.cboxCustID.EditValueChanged += new System.EventHandler(this.cboxCustID_EditValueChanged);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn6});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowPreview = true;
            this.gridLookUpEdit1View.PreviewFieldName = "BillingAddress";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã khách hàng";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 70;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tên khách hàng";
            this.gridColumn2.FieldName = "Name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 141;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Họ";
            this.gridColumn3.FieldName = "ContactLastName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 70;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Tên";
            this.gridColumn4.FieldName = "ContactFirstName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 81;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Số điện thoại";
            this.gridColumn6.FieldName = "PhoneNumber";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            this.gridColumn6.Width = 100;
            // 
            // btnInvSave
            // 
            this.btnInvSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInvSave.Image = ((System.Drawing.Image)(resources.GetObject("btnInvSave.Image")));
            this.btnInvSave.Location = new System.Drawing.Point(382, 516);
            this.btnInvSave.Name = "btnInvSave";
            this.btnInvSave.Size = new System.Drawing.Size(56, 22);
            this.btnInvSave.TabIndex = 25;
            this.btnInvSave.Text = "Lưu";
            this.btnInvSave.Click += new System.EventHandler(this.btnInvSave_Click);
            // 
            // btnInvCancel
            // 
            this.btnInvCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInvCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnInvCancel.Image")));
            this.btnInvCancel.Location = new System.Drawing.Point(444, 516);
            this.btnInvCancel.Name = "btnInvCancel";
            this.btnInvCancel.Size = new System.Drawing.Size(73, 22);
            this.btnInvCancel.TabIndex = 24;
            this.btnInvCancel.Text = "Hủy bỏ";
            this.btnInvCancel.Click += new System.EventHandler(this.btnInvCancel_Click);
            // 
            // btnInvClose
            // 
            this.btnInvClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInvClose.Image = ((System.Drawing.Image)(resources.GetObject("btnInvClose.Image")));
            this.btnInvClose.Location = new System.Drawing.Point(523, 516);
            this.btnInvClose.Name = "btnInvClose";
            this.btnInvClose.Size = new System.Drawing.Size(66, 22);
            this.btnInvClose.TabIndex = 23;
            this.btnInvClose.Text = "Đóng";
            this.btnInvClose.Click += new System.EventHandler(this.btnInvClose_Click);
            // 
            // btnInvEdit
            // 
            this.btnInvEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInvEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnInvEdit.Image")));
            this.btnInvEdit.Location = new System.Drawing.Point(320, 516);
            this.btnInvEdit.Name = "btnInvEdit";
            this.btnInvEdit.Size = new System.Drawing.Size(56, 22);
            this.btnInvEdit.TabIndex = 22;
            this.btnInvEdit.Text = "Sửa";
            this.btnInvEdit.Click += new System.EventHandler(this.btnInvEdit_Click);
            // 
            // btnInvDetailNew
            // 
            this.btnInvDetailNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInvDetailNew.Image = ((System.Drawing.Image)(resources.GetObject("btnInvDetailNew.Image")));
            this.btnInvDetailNew.Location = new System.Drawing.Point(399, 23);
            this.btnInvDetailNew.Name = "btnInvDetailNew";
            this.btnInvDetailNew.Size = new System.Drawing.Size(53, 22);
            this.btnInvDetailNew.TabIndex = 21;
            this.btnInvDetailNew.Text = "Thêm";
            this.btnInvDetailNew.Click += new System.EventHandler(this.btnInvDetailNew_Click);
            // 
            // btnInvDetailDelete
            // 
            this.btnInvDetailDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInvDetailDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnInvDetailDelete.Image")));
            this.btnInvDetailDelete.Location = new System.Drawing.Point(518, 23);
            this.btnInvDetailDelete.Name = "btnInvDetailDelete";
            this.btnInvDetailDelete.Size = new System.Drawing.Size(54, 22);
            this.btnInvDetailDelete.TabIndex = 20;
            this.btnInvDetailDelete.Text = "Xóa";
            this.btnInvDetailDelete.Click += new System.EventHandler(this.btnInvDetailDelete_Click);
            // 
            // btnInvDetailEdit
            // 
            this.btnInvDetailEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInvDetailEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnInvDetailEdit.Image")));
            this.btnInvDetailEdit.Location = new System.Drawing.Point(458, 23);
            this.btnInvDetailEdit.Name = "btnInvDetailEdit";
            this.btnInvDetailEdit.Size = new System.Drawing.Size(54, 22);
            this.btnInvDetailEdit.TabIndex = 18;
            this.btnInvDetailEdit.Text = "Sửa";
            this.btnInvDetailEdit.Click += new System.EventHandler(this.btnInvDetailEdit_Click);
            // 
            // gridInvDetail
            // 
            this.gridInvDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridInvDetail.Location = new System.Drawing.Point(5, 49);
            this.gridInvDetail.MainView = this.gridData;
            this.gridInvDetail.Name = "gridInvDetail";
            this.gridInvDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
            this.gridInvDetail.Size = new System.Drawing.Size(567, 83);
            this.gridInvDetail.TabIndex = 17;
            this.gridInvDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridData});
            this.gridInvDetail.DoubleClick += new System.EventHandler(this.gridInvDetail_DoubleClick);
            // 
            // gridData
            // 
            this.gridData.GridControl = this.gridInvDetail;
            this.gridData.Name = "gridData";
            this.gridData.OptionsBehavior.Editable = false;
            this.gridData.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // txtInvEmployeeID
            // 
            this.txtInvEmployeeID.Location = new System.Drawing.Point(117, 342);
            this.txtInvEmployeeID.Name = "txtInvEmployeeID";
            this.txtInvEmployeeID.Properties.ReadOnly = true;
            this.txtInvEmployeeID.Size = new System.Drawing.Size(126, 20);
            this.txtInvEmployeeID.TabIndex = 16;
            // 
            // txtInvCreateDate
            // 
            this.txtInvCreateDate.Location = new System.Drawing.Point(117, 316);
            this.txtInvCreateDate.Name = "txtInvCreateDate";
            this.txtInvCreateDate.Properties.ReadOnly = true;
            this.txtInvCreateDate.Size = new System.Drawing.Size(126, 20);
            this.txtInvCreateDate.TabIndex = 15;
            // 
            // txtInvNotes
            // 
            this.txtInvNotes.Location = new System.Drawing.Point(350, 316);
            this.txtInvNotes.Name = "txtInvNotes";
            this.txtInvNotes.Size = new System.Drawing.Size(220, 51);
            this.txtInvNotes.TabIndex = 14;
            // 
            // txtCustEmail
            // 
            this.txtCustEmail.Location = new System.Drawing.Point(338, 147);
            this.txtCustEmail.Name = "txtCustEmail";
            this.txtCustEmail.Properties.ReadOnly = true;
            this.txtCustEmail.Size = new System.Drawing.Size(153, 20);
            this.txtCustEmail.TabIndex = 10;
            // 
            // txtCustCity
            // 
            this.txtCustCity.Location = new System.Drawing.Point(338, 121);
            this.txtCustCity.Name = "txtCustCity";
            this.txtCustCity.Properties.ReadOnly = true;
            this.txtCustCity.Size = new System.Drawing.Size(136, 20);
            this.txtCustCity.TabIndex = 9;
            // 
            // txtCustAddress
            // 
            this.txtCustAddress.Location = new System.Drawing.Point(338, 57);
            this.txtCustAddress.Name = "txtCustAddress";
            this.txtCustAddress.Properties.ReadOnly = true;
            this.txtCustAddress.Size = new System.Drawing.Size(220, 51);
            this.txtCustAddress.TabIndex = 8;
            // 
            // txtCustPhone
            // 
            this.txtCustPhone.Location = new System.Drawing.Point(105, 121);
            this.txtCustPhone.Name = "txtCustPhone";
            this.txtCustPhone.Properties.ReadOnly = true;
            this.txtCustPhone.Size = new System.Drawing.Size(126, 20);
            this.txtCustPhone.TabIndex = 7;
            // 
            // txtCustFirstName
            // 
            this.txtCustFirstName.Location = new System.Drawing.Point(105, 92);
            this.txtCustFirstName.Name = "txtCustFirstName";
            this.txtCustFirstName.Properties.ReadOnly = true;
            this.txtCustFirstName.Size = new System.Drawing.Size(126, 20);
            this.txtCustFirstName.TabIndex = 6;
            // 
            // txtCustName
            // 
            this.txtCustName.Location = new System.Drawing.Point(105, 29);
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Properties.ReadOnly = true;
            this.txtCustName.Size = new System.Drawing.Size(289, 20);
            this.txtCustName.TabIndex = 5;
            // 
            // txtCustLastName
            // 
            this.txtCustLastName.Location = new System.Drawing.Point(105, 61);
            this.txtCustLastName.Name = "txtCustLastName";
            this.txtCustLastName.Properties.ReadOnly = true;
            this.txtCustLastName.Size = new System.Drawing.Size(126, 20);
            this.txtCustLastName.TabIndex = 4;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnInvDetailDelete);
            this.groupControl1.Controls.Add(this.btnInvDetailEdit);
            this.groupControl1.Controls.Add(this.btnInvDetailNew);
            this.groupControl1.Controls.Add(this.gridInvDetail);
            this.groupControl1.Location = new System.Drawing.Point(12, 373);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(577, 137);
            this.groupControl1.TabIndex = 26;
            this.groupControl1.Text = "Thông tin hóa đơn";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(19, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(80, 13);
            this.labelControl1.TabIndex = 27;
            this.labelControl1.Text = "Tên khách hàng:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(82, 64);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(17, 13);
            this.labelControl2.TabIndex = 27;
            this.labelControl2.Text = "Họ:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(77, 95);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(22, 13);
            this.labelControl3.TabIndex = 27;
            this.labelControl3.Text = "Tên:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(33, 124);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(66, 13);
            this.labelControl4.TabIndex = 27;
            this.labelControl4.Text = "Số điện thoại:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(270, 318);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(74, 13);
            this.labelControl5.TabIndex = 27;
            this.labelControl5.Text = "Thông tin khác:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(272, 150);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 13);
            this.labelControl6.TabIndex = 27;
            this.labelControl6.Text = "Thư điện tử:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(296, 64);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 13);
            this.labelControl7.TabIndex = 27;
            this.labelControl7.Text = "Địa chỉ:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(277, 124);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(55, 13);
            this.labelControl8.TabIndex = 27;
            this.labelControl8.Text = "Thành phố:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(63, 319);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(48, 13);
            this.labelControl9.TabIndex = 27;
            this.labelControl9.Text = "Ngày tạo:";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(45, 345);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(68, 13);
            this.labelControl10.TabIndex = 27;
            this.labelControl10.Text = "Mã nhân viên:";
            // 
            // groupProdInfo
            // 
            this.groupProdInfo.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupProdInfo.AppearanceCaption.Options.UseFont = true;
            this.groupProdInfo.Controls.Add(this.labelControl1);
            this.groupProdInfo.Controls.Add(this.labelControl2);
            this.groupProdInfo.Controls.Add(this.labelControl8);
            this.groupProdInfo.Controls.Add(this.labelControl6);
            this.groupProdInfo.Controls.Add(this.labelControl3);
            this.groupProdInfo.Controls.Add(this.labelControl7);
            this.groupProdInfo.Controls.Add(this.labelControl4);
            this.groupProdInfo.Controls.Add(this.txtCustName);
            this.groupProdInfo.Controls.Add(this.txtCustLastName);
            this.groupProdInfo.Controls.Add(this.txtCustFirstName);
            this.groupProdInfo.Controls.Add(this.txtCustAddress);
            this.groupProdInfo.Controls.Add(this.txtCustEmail);
            this.groupProdInfo.Controls.Add(this.txtCustCity);
            this.groupProdInfo.Controls.Add(this.txtCustPhone);
            this.groupProdInfo.Location = new System.Drawing.Point(12, 132);
            this.groupProdInfo.Name = "groupProdInfo";
            this.groupProdInfo.Size = new System.Drawing.Size(575, 178);
            this.groupProdInfo.TabIndex = 28;
            this.groupProdInfo.Text = "Thông tin khách hàng";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(67, 83);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(44, 13);
            this.labelControl11.TabIndex = 27;
            this.labelControl11.Text = "Hóa đơn:";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(51, 109);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(60, 13);
            this.labelControl12.TabIndex = 27;
            this.labelControl12.Text = "Khách hàng:";
            // 
            // FrmInvoiceInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 545);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.groupProdInfo);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.btnCustEdit);
            this.Controls.Add(this.btnCustNew);
            this.Controls.Add(this.txtInvID);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.cboxCustID);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.txtInvEmployeeID);
            this.Controls.Add(this.txtInvCreateDate);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.txtInvNotes);
            this.Controls.Add(this.btnInvEdit);
            this.Controls.Add(this.btnInvSave);
            this.Controls.Add(this.btnInvCancel);
            this.Controls.Add(this.btnInvClose);
            this.Name = "FrmInvoiceInfo";
            this.Text = "FrmInvoiceInfo";
            this.Load += new System.EventHandler(this.FrmInvoiceInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboxCustID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridInvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvEmployeeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvCreateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupProdInfo)).EndInit();
            this.groupProdInfo.ResumeLayout(false);
            this.groupProdInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.TextEdit txtCustEmail;
        private DevExpress.XtraEditors.TextEdit txtCustCity;
        private DevExpress.XtraEditors.MemoEdit txtCustAddress;
        private DevExpress.XtraEditors.TextEdit txtCustPhone;
        private DevExpress.XtraEditors.TextEdit txtCustFirstName;
        private DevExpress.XtraEditors.TextEdit txtCustName;
        private DevExpress.XtraEditors.TextEdit txtCustLastName;
        private DevExpress.XtraEditors.SimpleButton btnCustNew;
        private DevExpress.XtraEditors.SimpleButton btnCustEdit;
        private DevExpress.XtraEditors.GridLookUpEdit cboxCustID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.TextEdit txtInvCreateDate;
        private DevExpress.XtraEditors.MemoEdit txtInvNotes;
        private DevExpress.XtraEditors.TextEdit txtInvEmployeeID;
        private DevExpress.XtraGrid.GridControl gridInvDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridData;
        private DevExpress.XtraEditors.SimpleButton btnInvDetailDelete;
        private DevExpress.XtraEditors.SimpleButton btnInvDetailEdit;
        private DevExpress.XtraEditors.SimpleButton btnInvSave;
        private DevExpress.XtraEditors.SimpleButton btnInvCancel;
        private DevExpress.XtraEditors.SimpleButton btnInvClose;
        private DevExpress.XtraEditors.SimpleButton btnInvEdit;
        private DevExpress.XtraEditors.SimpleButton btnInvDetailNew;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.TextEdit txtInvID;
        private System.Windows.Forms.Panel pnlTop;
        private DevExpress.XtraEditors.LabelControl lblCaption;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.GroupControl groupProdInfo;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
    }
}