﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_TransportEquipment : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int TrangThaiXuLy { set; get; }
		public long SoTN { set; get; }
		public DateTime NgayTN { set; get; }
		public string MaHQ { set; get; }
		public string MaDoanhNghiep { set; get; }
		public long TKMD_ID { set; get; }
		public string TenNhaVanChuyen { set; get; }
		public string MaNhaVanChuyen { set; get; }
		public string DiaChiNhaVanChuyen { set; get; }
		public string CoBaoXNK { set; get; }
		public string MaLoaiHinhVanChuyen { set; get; }
		public string MaHQGiamSat { set; get; }
		public string MaPTVC { set; get; }
		public string MaMucDichVC { set; get; }
		public DateTime NgayBatDauVC { set; get; }
		public DateTime NgayKetThucVC { set; get; }
		public DateTime NgayHopDongVC { set; get; }
		public string SoHopDongVC { set; get; }
		public DateTime NgayHetHanHDVC { set; get; }
		public string MaDiaDiemXepHang { set; get; }
		public string TenDiaDiemXepHang { set; get; }
		public string MaViTriXepHang { set; get; }
		public string MaCangCuaKhauGaXepHang { set; get; }
		public string MaDiaDiemDoHang { set; get; }
		public string TenDiaDiemDoHang { set; get; }
		public string MaViTriDoHang { set; get; }
		public string MaCangCuaKhauGaDoHang { set; get; }
		public string GhiChuKhac { set; get; }
		public string TuyenDuong { set; get; }
		public string SoDienThoaiHQ { set; get; }
		public string SoFaxHQ { set; get; }
		public string TenDaiDienDN { set; get; }
		public string ThoiGianVC { set; get; }
		public decimal SoKMVC { set; get; }
		public string GuidStr { set; get; }
        public List<KDT_VNACCS_TransportEquipment_Detail> TransportEquipmentCollection = new List<KDT_VNACCS_TransportEquipment_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TransportEquipment> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TransportEquipment> collection = new List<KDT_VNACCS_TransportEquipment>();
			while (reader.Read())
			{
				KDT_VNACCS_TransportEquipment entity = new KDT_VNACCS_TransportEquipment();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTN"))) entity.SoTN = reader.GetInt64(reader.GetOrdinal("SoTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTN"))) entity.NgayTN = reader.GetDateTime(reader.GetOrdinal("NgayTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNhaVanChuyen"))) entity.TenNhaVanChuyen = reader.GetString(reader.GetOrdinal("TenNhaVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNhaVanChuyen"))) entity.MaNhaVanChuyen = reader.GetString(reader.GetOrdinal("MaNhaVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNhaVanChuyen"))) entity.DiaChiNhaVanChuyen = reader.GetString(reader.GetOrdinal("DiaChiNhaVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoBaoXNK"))) entity.CoBaoXNK = reader.GetString(reader.GetOrdinal("CoBaoXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhVanChuyen"))) entity.MaLoaiHinhVanChuyen = reader.GetString(reader.GetOrdinal("MaLoaiHinhVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQGiamSat"))) entity.MaHQGiamSat = reader.GetString(reader.GetOrdinal("MaHQGiamSat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPTVC"))) entity.MaPTVC = reader.GetString(reader.GetOrdinal("MaPTVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaMucDichVC"))) entity.MaMucDichVC = reader.GetString(reader.GetOrdinal("MaMucDichVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDauVC"))) entity.NgayBatDauVC = reader.GetDateTime(reader.GetOrdinal("NgayBatDauVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThucVC"))) entity.NgayKetThucVC = reader.GetDateTime(reader.GetOrdinal("NgayKetThucVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDongVC"))) entity.NgayHopDongVC = reader.GetDateTime(reader.GetOrdinal("NgayHopDongVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongVC"))) entity.SoHopDongVC = reader.GetString(reader.GetOrdinal("SoHopDongVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDVC"))) entity.NgayHetHanHDVC = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemXepHang"))) entity.MaDiaDiemXepHang = reader.GetString(reader.GetOrdinal("MaDiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemXepHang"))) entity.TenDiaDiemXepHang = reader.GetString(reader.GetOrdinal("TenDiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaViTriXepHang"))) entity.MaViTriXepHang = reader.GetString(reader.GetOrdinal("MaViTriXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangCuaKhauGaXepHang"))) entity.MaCangCuaKhauGaXepHang = reader.GetString(reader.GetOrdinal("MaCangCuaKhauGaXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemDoHang"))) entity.MaDiaDiemDoHang = reader.GetString(reader.GetOrdinal("MaDiaDiemDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemDoHang"))) entity.TenDiaDiemDoHang = reader.GetString(reader.GetOrdinal("TenDiaDiemDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaViTriDoHang"))) entity.MaViTriDoHang = reader.GetString(reader.GetOrdinal("MaViTriDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangCuaKhauGaDoHang"))) entity.MaCangCuaKhauGaDoHang = reader.GetString(reader.GetOrdinal("MaCangCuaKhauGaDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TuyenDuong"))) entity.TuyenDuong = reader.GetString(reader.GetOrdinal("TuyenDuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiHQ"))) entity.SoDienThoaiHQ = reader.GetString(reader.GetOrdinal("SoDienThoaiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFaxHQ"))) entity.SoFaxHQ = reader.GetString(reader.GetOrdinal("SoFaxHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiDienDN"))) entity.TenDaiDienDN = reader.GetString(reader.GetOrdinal("TenDaiDienDN"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianVC"))) entity.ThoiGianVC = reader.GetString(reader.GetOrdinal("ThoiGianVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKMVC"))) entity.SoKMVC = reader.GetDecimal(reader.GetOrdinal("SoKMVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TransportEquipment> collection, long id)
        {
            foreach (KDT_VNACCS_TransportEquipment item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TransportEquipment VALUES(@TrangThaiXuLy, @SoTN, @NgayTN, @MaHQ, @MaDoanhNghiep, @TKMD_ID, @TenNhaVanChuyen, @MaNhaVanChuyen, @DiaChiNhaVanChuyen, @CoBaoXNK, @MaLoaiHinhVanChuyen, @MaHQGiamSat, @MaPTVC, @MaMucDichVC, @NgayBatDauVC, @NgayKetThucVC, @NgayHopDongVC, @SoHopDongVC, @NgayHetHanHDVC, @MaDiaDiemXepHang, @TenDiaDiemXepHang, @MaViTriXepHang, @MaCangCuaKhauGaXepHang, @MaDiaDiemDoHang, @TenDiaDiemDoHang, @MaViTriDoHang, @MaCangCuaKhauGaDoHang, @GhiChuKhac, @TuyenDuong, @SoDienThoaiHQ, @SoFaxHQ, @TenDaiDienDN, @ThoiGianVC, @SoKMVC, @GuidStr)";
            string update = "UPDATE t_KDT_VNACCS_TransportEquipment SET TrangThaiXuLy = @TrangThaiXuLy, SoTN = @SoTN, NgayTN = @NgayTN, MaHQ = @MaHQ, MaDoanhNghiep = @MaDoanhNghiep, TKMD_ID = @TKMD_ID, TenNhaVanChuyen = @TenNhaVanChuyen, MaNhaVanChuyen = @MaNhaVanChuyen, DiaChiNhaVanChuyen = @DiaChiNhaVanChuyen, CoBaoXNK = @CoBaoXNK, MaLoaiHinhVanChuyen = @MaLoaiHinhVanChuyen, MaHQGiamSat = @MaHQGiamSat, MaPTVC = @MaPTVC, MaMucDichVC = @MaMucDichVC, NgayBatDauVC = @NgayBatDauVC, NgayKetThucVC = @NgayKetThucVC, NgayHopDongVC = @NgayHopDongVC, SoHopDongVC = @SoHopDongVC, NgayHetHanHDVC = @NgayHetHanHDVC, MaDiaDiemXepHang = @MaDiaDiemXepHang, TenDiaDiemXepHang = @TenDiaDiemXepHang, MaViTriXepHang = @MaViTriXepHang, MaCangCuaKhauGaXepHang = @MaCangCuaKhauGaXepHang, MaDiaDiemDoHang = @MaDiaDiemDoHang, TenDiaDiemDoHang = @TenDiaDiemDoHang, MaViTriDoHang = @MaViTriDoHang, MaCangCuaKhauGaDoHang = @MaCangCuaKhauGaDoHang, GhiChuKhac = @GhiChuKhac, TuyenDuong = @TuyenDuong, SoDienThoaiHQ = @SoDienThoaiHQ, SoFaxHQ = @SoFaxHQ, TenDaiDienDN = @TenDaiDienDN, ThoiGianVC = @ThoiGianVC, SoKMVC = @SoKMVC, GuidStr = @GuidStr WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TransportEquipment WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNhaVanChuyen", SqlDbType.NVarChar, "TenNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhaVanChuyen", SqlDbType.NVarChar, "MaNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNhaVanChuyen", SqlDbType.NVarChar, "DiaChiNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoXNK", SqlDbType.NVarChar, "CoBaoXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinhVanChuyen", SqlDbType.NVarChar, "MaLoaiHinhVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQGiamSat", SqlDbType.NVarChar, "MaHQGiamSat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPTVC", SqlDbType.NVarChar, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMucDichVC", SqlDbType.NVarChar, "MaMucDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDauVC", SqlDbType.DateTime, "NgayBatDauVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThucVC", SqlDbType.DateTime, "NgayKetThucVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongVC", SqlDbType.DateTime, "NgayHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongVC", SqlDbType.NVarChar, "SoHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanHDVC", SqlDbType.DateTime, "NgayHetHanHDVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.NVarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaViTriXepHang", SqlDbType.NVarChar, "MaViTriXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.NVarChar, "MaCangCuaKhauGaXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDoHang", SqlDbType.NVarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, "TenDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaViTriDoHang", SqlDbType.NVarChar, "MaViTriDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.NVarChar, "MaCangCuaKhauGaDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuyenDuong", SqlDbType.NVarChar, "TuyenDuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiHQ", SqlDbType.NVarChar, "SoDienThoaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxHQ", SqlDbType.NVarChar, "SoFaxHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiDienDN", SqlDbType.NVarChar, "TenDaiDienDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianVC", SqlDbType.NVarChar, "ThoiGianVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoKMVC", SqlDbType.Decimal, "SoKMVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNhaVanChuyen", SqlDbType.NVarChar, "TenNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhaVanChuyen", SqlDbType.NVarChar, "MaNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNhaVanChuyen", SqlDbType.NVarChar, "DiaChiNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoXNK", SqlDbType.NVarChar, "CoBaoXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinhVanChuyen", SqlDbType.NVarChar, "MaLoaiHinhVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQGiamSat", SqlDbType.NVarChar, "MaHQGiamSat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPTVC", SqlDbType.NVarChar, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMucDichVC", SqlDbType.NVarChar, "MaMucDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDauVC", SqlDbType.DateTime, "NgayBatDauVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThucVC", SqlDbType.DateTime, "NgayKetThucVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongVC", SqlDbType.DateTime, "NgayHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongVC", SqlDbType.NVarChar, "SoHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanHDVC", SqlDbType.DateTime, "NgayHetHanHDVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.NVarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaViTriXepHang", SqlDbType.NVarChar, "MaViTriXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.NVarChar, "MaCangCuaKhauGaXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDoHang", SqlDbType.NVarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, "TenDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaViTriDoHang", SqlDbType.NVarChar, "MaViTriDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.NVarChar, "MaCangCuaKhauGaDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuyenDuong", SqlDbType.NVarChar, "TuyenDuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiHQ", SqlDbType.NVarChar, "SoDienThoaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxHQ", SqlDbType.NVarChar, "SoFaxHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiDienDN", SqlDbType.NVarChar, "TenDaiDienDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianVC", SqlDbType.NVarChar, "ThoiGianVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoKMVC", SqlDbType.Decimal, "SoKMVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TransportEquipment VALUES(@TrangThaiXuLy, @SoTN, @NgayTN, @MaHQ, @MaDoanhNghiep, @TKMD_ID, @TenNhaVanChuyen, @MaNhaVanChuyen, @DiaChiNhaVanChuyen, @CoBaoXNK, @MaLoaiHinhVanChuyen, @MaHQGiamSat, @MaPTVC, @MaMucDichVC, @NgayBatDauVC, @NgayKetThucVC, @NgayHopDongVC, @SoHopDongVC, @NgayHetHanHDVC, @MaDiaDiemXepHang, @TenDiaDiemXepHang, @MaViTriXepHang, @MaCangCuaKhauGaXepHang, @MaDiaDiemDoHang, @TenDiaDiemDoHang, @MaViTriDoHang, @MaCangCuaKhauGaDoHang, @GhiChuKhac, @TuyenDuong, @SoDienThoaiHQ, @SoFaxHQ, @TenDaiDienDN, @ThoiGianVC, @SoKMVC, @GuidStr)";
            string update = "UPDATE t_KDT_VNACCS_TransportEquipment SET TrangThaiXuLy = @TrangThaiXuLy, SoTN = @SoTN, NgayTN = @NgayTN, MaHQ = @MaHQ, MaDoanhNghiep = @MaDoanhNghiep, TKMD_ID = @TKMD_ID, TenNhaVanChuyen = @TenNhaVanChuyen, MaNhaVanChuyen = @MaNhaVanChuyen, DiaChiNhaVanChuyen = @DiaChiNhaVanChuyen, CoBaoXNK = @CoBaoXNK, MaLoaiHinhVanChuyen = @MaLoaiHinhVanChuyen, MaHQGiamSat = @MaHQGiamSat, MaPTVC = @MaPTVC, MaMucDichVC = @MaMucDichVC, NgayBatDauVC = @NgayBatDauVC, NgayKetThucVC = @NgayKetThucVC, NgayHopDongVC = @NgayHopDongVC, SoHopDongVC = @SoHopDongVC, NgayHetHanHDVC = @NgayHetHanHDVC, MaDiaDiemXepHang = @MaDiaDiemXepHang, TenDiaDiemXepHang = @TenDiaDiemXepHang, MaViTriXepHang = @MaViTriXepHang, MaCangCuaKhauGaXepHang = @MaCangCuaKhauGaXepHang, MaDiaDiemDoHang = @MaDiaDiemDoHang, TenDiaDiemDoHang = @TenDiaDiemDoHang, MaViTriDoHang = @MaViTriDoHang, MaCangCuaKhauGaDoHang = @MaCangCuaKhauGaDoHang, GhiChuKhac = @GhiChuKhac, TuyenDuong = @TuyenDuong, SoDienThoaiHQ = @SoDienThoaiHQ, SoFaxHQ = @SoFaxHQ, TenDaiDienDN = @TenDaiDienDN, ThoiGianVC = @ThoiGianVC, SoKMVC = @SoKMVC, GuidStr = @GuidStr WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TransportEquipment WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNhaVanChuyen", SqlDbType.NVarChar, "TenNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhaVanChuyen", SqlDbType.NVarChar, "MaNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNhaVanChuyen", SqlDbType.NVarChar, "DiaChiNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoXNK", SqlDbType.NVarChar, "CoBaoXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinhVanChuyen", SqlDbType.NVarChar, "MaLoaiHinhVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQGiamSat", SqlDbType.NVarChar, "MaHQGiamSat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPTVC", SqlDbType.NVarChar, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaMucDichVC", SqlDbType.NVarChar, "MaMucDichVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDauVC", SqlDbType.DateTime, "NgayBatDauVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetThucVC", SqlDbType.DateTime, "NgayKetThucVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDongVC", SqlDbType.DateTime, "NgayHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDongVC", SqlDbType.NVarChar, "SoHopDongVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanHDVC", SqlDbType.DateTime, "NgayHetHanHDVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.NVarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaViTriXepHang", SqlDbType.NVarChar, "MaViTriXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.NVarChar, "MaCangCuaKhauGaXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDoHang", SqlDbType.NVarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, "TenDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaViTriDoHang", SqlDbType.NVarChar, "MaViTriDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.NVarChar, "MaCangCuaKhauGaDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuyenDuong", SqlDbType.NVarChar, "TuyenDuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiHQ", SqlDbType.NVarChar, "SoDienThoaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxHQ", SqlDbType.NVarChar, "SoFaxHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiDienDN", SqlDbType.NVarChar, "TenDaiDienDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianVC", SqlDbType.NVarChar, "ThoiGianVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoKMVC", SqlDbType.Decimal, "SoKMVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNhaVanChuyen", SqlDbType.NVarChar, "TenNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhaVanChuyen", SqlDbType.NVarChar, "MaNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNhaVanChuyen", SqlDbType.NVarChar, "DiaChiNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoXNK", SqlDbType.NVarChar, "CoBaoXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinhVanChuyen", SqlDbType.NVarChar, "MaLoaiHinhVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQGiamSat", SqlDbType.NVarChar, "MaHQGiamSat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPTVC", SqlDbType.NVarChar, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaMucDichVC", SqlDbType.NVarChar, "MaMucDichVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDauVC", SqlDbType.DateTime, "NgayBatDauVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetThucVC", SqlDbType.DateTime, "NgayKetThucVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDongVC", SqlDbType.DateTime, "NgayHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDongVC", SqlDbType.NVarChar, "SoHopDongVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanHDVC", SqlDbType.DateTime, "NgayHetHanHDVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.NVarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaViTriXepHang", SqlDbType.NVarChar, "MaViTriXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.NVarChar, "MaCangCuaKhauGaXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDoHang", SqlDbType.NVarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, "TenDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaViTriDoHang", SqlDbType.NVarChar, "MaViTriDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.NVarChar, "MaCangCuaKhauGaDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuyenDuong", SqlDbType.NVarChar, "TuyenDuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiHQ", SqlDbType.NVarChar, "SoDienThoaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxHQ", SqlDbType.NVarChar, "SoFaxHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiDienDN", SqlDbType.NVarChar, "TenDaiDienDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianVC", SqlDbType.NVarChar, "ThoiGianVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoKMVC", SqlDbType.Decimal, "SoKMVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TransportEquipment Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TransportEquipment> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TransportEquipment> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TransportEquipment> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TransportEquipment(int trangThaiXuLy, long soTN, DateTime ngayTN, string maHQ, string maDoanhNghiep, long tKMD_ID, string tenNhaVanChuyen, string maNhaVanChuyen, string diaChiNhaVanChuyen, string coBaoXNK, string maLoaiHinhVanChuyen, string maHQGiamSat, string maPTVC, string maMucDichVC, DateTime ngayBatDauVC, DateTime ngayKetThucVC, DateTime ngayHopDongVC, string soHopDongVC, DateTime ngayHetHanHDVC, string maDiaDiemXepHang, string tenDiaDiemXepHang, string maViTriXepHang, string maCangCuaKhauGaXepHang, string maDiaDiemDoHang, string tenDiaDiemDoHang, string maViTriDoHang, string maCangCuaKhauGaDoHang, string ghiChuKhac, string tuyenDuong, string soDienThoaiHQ, string soFaxHQ, string tenDaiDienDN, string thoiGianVC, decimal soKMVC, string guidStr)
		{
			KDT_VNACCS_TransportEquipment entity = new KDT_VNACCS_TransportEquipment();	
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TKMD_ID = tKMD_ID;
			entity.TenNhaVanChuyen = tenNhaVanChuyen;
			entity.MaNhaVanChuyen = maNhaVanChuyen;
			entity.DiaChiNhaVanChuyen = diaChiNhaVanChuyen;
			entity.CoBaoXNK = coBaoXNK;
			entity.MaLoaiHinhVanChuyen = maLoaiHinhVanChuyen;
			entity.MaHQGiamSat = maHQGiamSat;
			entity.MaPTVC = maPTVC;
			entity.MaMucDichVC = maMucDichVC;
			entity.NgayBatDauVC = ngayBatDauVC;
			entity.NgayKetThucVC = ngayKetThucVC;
			entity.NgayHopDongVC = ngayHopDongVC;
			entity.SoHopDongVC = soHopDongVC;
			entity.NgayHetHanHDVC = ngayHetHanHDVC;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.MaViTriXepHang = maViTriXepHang;
			entity.MaCangCuaKhauGaXepHang = maCangCuaKhauGaXepHang;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDoHang = tenDiaDiemDoHang;
			entity.MaViTriDoHang = maViTriDoHang;
			entity.MaCangCuaKhauGaDoHang = maCangCuaKhauGaDoHang;
			entity.GhiChuKhac = ghiChuKhac;
			entity.TuyenDuong = tuyenDuong;
			entity.SoDienThoaiHQ = soDienThoaiHQ;
			entity.SoFaxHQ = soFaxHQ;
			entity.TenDaiDienDN = tenDaiDienDN;
			entity.ThoiGianVC = thoiGianVC;
			entity.SoKMVC = soKMVC;
			entity.GuidStr = guidStr;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TenNhaVanChuyen", SqlDbType.NVarChar, TenNhaVanChuyen);
			db.AddInParameter(dbCommand, "@MaNhaVanChuyen", SqlDbType.NVarChar, MaNhaVanChuyen);
			db.AddInParameter(dbCommand, "@DiaChiNhaVanChuyen", SqlDbType.NVarChar, DiaChiNhaVanChuyen);
			db.AddInParameter(dbCommand, "@CoBaoXNK", SqlDbType.NVarChar, CoBaoXNK);
			db.AddInParameter(dbCommand, "@MaLoaiHinhVanChuyen", SqlDbType.NVarChar, MaLoaiHinhVanChuyen);
			db.AddInParameter(dbCommand, "@MaHQGiamSat", SqlDbType.NVarChar, MaHQGiamSat);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.NVarChar, MaPTVC);
			db.AddInParameter(dbCommand, "@MaMucDichVC", SqlDbType.NVarChar, MaMucDichVC);
			db.AddInParameter(dbCommand, "@NgayBatDauVC", SqlDbType.DateTime, NgayBatDauVC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauVC);
			db.AddInParameter(dbCommand, "@NgayKetThucVC", SqlDbType.DateTime, NgayKetThucVC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucVC);
			db.AddInParameter(dbCommand, "@NgayHopDongVC", SqlDbType.DateTime, NgayHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHopDongVC);
			db.AddInParameter(dbCommand, "@SoHopDongVC", SqlDbType.NVarChar, SoHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHetHanHDVC", SqlDbType.DateTime, NgayHetHanHDVC.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHDVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.NVarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@MaViTriXepHang", SqlDbType.NVarChar, MaViTriXepHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.NVarChar, MaCangCuaKhauGaXepHang);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.NVarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, TenDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaViTriDoHang", SqlDbType.NVarChar, MaViTriDoHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.NVarChar, MaCangCuaKhauGaDoHang);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@TuyenDuong", SqlDbType.NVarChar, TuyenDuong);
			db.AddInParameter(dbCommand, "@SoDienThoaiHQ", SqlDbType.NVarChar, SoDienThoaiHQ);
			db.AddInParameter(dbCommand, "@SoFaxHQ", SqlDbType.NVarChar, SoFaxHQ);
			db.AddInParameter(dbCommand, "@TenDaiDienDN", SqlDbType.NVarChar, TenDaiDienDN);
			db.AddInParameter(dbCommand, "@ThoiGianVC", SqlDbType.NVarChar, ThoiGianVC);
			db.AddInParameter(dbCommand, "@SoKMVC", SqlDbType.Decimal, SoKMVC);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TransportEquipment> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TransportEquipment item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TransportEquipment(long id, int trangThaiXuLy, long soTN, DateTime ngayTN, string maHQ, string maDoanhNghiep, long tKMD_ID, string tenNhaVanChuyen, string maNhaVanChuyen, string diaChiNhaVanChuyen, string coBaoXNK, string maLoaiHinhVanChuyen, string maHQGiamSat, string maPTVC, string maMucDichVC, DateTime ngayBatDauVC, DateTime ngayKetThucVC, DateTime ngayHopDongVC, string soHopDongVC, DateTime ngayHetHanHDVC, string maDiaDiemXepHang, string tenDiaDiemXepHang, string maViTriXepHang, string maCangCuaKhauGaXepHang, string maDiaDiemDoHang, string tenDiaDiemDoHang, string maViTriDoHang, string maCangCuaKhauGaDoHang, string ghiChuKhac, string tuyenDuong, string soDienThoaiHQ, string soFaxHQ, string tenDaiDienDN, string thoiGianVC, decimal soKMVC, string guidStr)
		{
			KDT_VNACCS_TransportEquipment entity = new KDT_VNACCS_TransportEquipment();			
			entity.ID = id;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TKMD_ID = tKMD_ID;
			entity.TenNhaVanChuyen = tenNhaVanChuyen;
			entity.MaNhaVanChuyen = maNhaVanChuyen;
			entity.DiaChiNhaVanChuyen = diaChiNhaVanChuyen;
			entity.CoBaoXNK = coBaoXNK;
			entity.MaLoaiHinhVanChuyen = maLoaiHinhVanChuyen;
			entity.MaHQGiamSat = maHQGiamSat;
			entity.MaPTVC = maPTVC;
			entity.MaMucDichVC = maMucDichVC;
			entity.NgayBatDauVC = ngayBatDauVC;
			entity.NgayKetThucVC = ngayKetThucVC;
			entity.NgayHopDongVC = ngayHopDongVC;
			entity.SoHopDongVC = soHopDongVC;
			entity.NgayHetHanHDVC = ngayHetHanHDVC;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.MaViTriXepHang = maViTriXepHang;
			entity.MaCangCuaKhauGaXepHang = maCangCuaKhauGaXepHang;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDoHang = tenDiaDiemDoHang;
			entity.MaViTriDoHang = maViTriDoHang;
			entity.MaCangCuaKhauGaDoHang = maCangCuaKhauGaDoHang;
			entity.GhiChuKhac = ghiChuKhac;
			entity.TuyenDuong = tuyenDuong;
			entity.SoDienThoaiHQ = soDienThoaiHQ;
			entity.SoFaxHQ = soFaxHQ;
			entity.TenDaiDienDN = tenDaiDienDN;
			entity.ThoiGianVC = thoiGianVC;
			entity.SoKMVC = soKMVC;
			entity.GuidStr = guidStr;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TransportEquipment_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TenNhaVanChuyen", SqlDbType.NVarChar, TenNhaVanChuyen);
			db.AddInParameter(dbCommand, "@MaNhaVanChuyen", SqlDbType.NVarChar, MaNhaVanChuyen);
			db.AddInParameter(dbCommand, "@DiaChiNhaVanChuyen", SqlDbType.NVarChar, DiaChiNhaVanChuyen);
			db.AddInParameter(dbCommand, "@CoBaoXNK", SqlDbType.NVarChar, CoBaoXNK);
			db.AddInParameter(dbCommand, "@MaLoaiHinhVanChuyen", SqlDbType.NVarChar, MaLoaiHinhVanChuyen);
			db.AddInParameter(dbCommand, "@MaHQGiamSat", SqlDbType.NVarChar, MaHQGiamSat);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.NVarChar, MaPTVC);
			db.AddInParameter(dbCommand, "@MaMucDichVC", SqlDbType.NVarChar, MaMucDichVC);
			db.AddInParameter(dbCommand, "@NgayBatDauVC", SqlDbType.DateTime, NgayBatDauVC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauVC);
			db.AddInParameter(dbCommand, "@NgayKetThucVC", SqlDbType.DateTime, NgayKetThucVC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucVC);
			db.AddInParameter(dbCommand, "@NgayHopDongVC", SqlDbType.DateTime, NgayHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHopDongVC);
			db.AddInParameter(dbCommand, "@SoHopDongVC", SqlDbType.NVarChar, SoHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHetHanHDVC", SqlDbType.DateTime, NgayHetHanHDVC.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHDVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.NVarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@MaViTriXepHang", SqlDbType.NVarChar, MaViTriXepHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.NVarChar, MaCangCuaKhauGaXepHang);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.NVarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, TenDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaViTriDoHang", SqlDbType.NVarChar, MaViTriDoHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.NVarChar, MaCangCuaKhauGaDoHang);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@TuyenDuong", SqlDbType.NVarChar, TuyenDuong);
			db.AddInParameter(dbCommand, "@SoDienThoaiHQ", SqlDbType.NVarChar, SoDienThoaiHQ);
			db.AddInParameter(dbCommand, "@SoFaxHQ", SqlDbType.NVarChar, SoFaxHQ);
			db.AddInParameter(dbCommand, "@TenDaiDienDN", SqlDbType.NVarChar, TenDaiDienDN);
			db.AddInParameter(dbCommand, "@ThoiGianVC", SqlDbType.NVarChar, ThoiGianVC);
			db.AddInParameter(dbCommand, "@SoKMVC", SqlDbType.Decimal, SoKMVC);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TransportEquipment> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TransportEquipment item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TransportEquipment(long id, int trangThaiXuLy, long soTN, DateTime ngayTN, string maHQ, string maDoanhNghiep, long tKMD_ID, string tenNhaVanChuyen, string maNhaVanChuyen, string diaChiNhaVanChuyen, string coBaoXNK, string maLoaiHinhVanChuyen, string maHQGiamSat, string maPTVC, string maMucDichVC, DateTime ngayBatDauVC, DateTime ngayKetThucVC, DateTime ngayHopDongVC, string soHopDongVC, DateTime ngayHetHanHDVC, string maDiaDiemXepHang, string tenDiaDiemXepHang, string maViTriXepHang, string maCangCuaKhauGaXepHang, string maDiaDiemDoHang, string tenDiaDiemDoHang, string maViTriDoHang, string maCangCuaKhauGaDoHang, string ghiChuKhac, string tuyenDuong, string soDienThoaiHQ, string soFaxHQ, string tenDaiDienDN, string thoiGianVC, decimal soKMVC, string guidStr)
		{
			KDT_VNACCS_TransportEquipment entity = new KDT_VNACCS_TransportEquipment();			
			entity.ID = id;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TKMD_ID = tKMD_ID;
			entity.TenNhaVanChuyen = tenNhaVanChuyen;
			entity.MaNhaVanChuyen = maNhaVanChuyen;
			entity.DiaChiNhaVanChuyen = diaChiNhaVanChuyen;
			entity.CoBaoXNK = coBaoXNK;
			entity.MaLoaiHinhVanChuyen = maLoaiHinhVanChuyen;
			entity.MaHQGiamSat = maHQGiamSat;
			entity.MaPTVC = maPTVC;
			entity.MaMucDichVC = maMucDichVC;
			entity.NgayBatDauVC = ngayBatDauVC;
			entity.NgayKetThucVC = ngayKetThucVC;
			entity.NgayHopDongVC = ngayHopDongVC;
			entity.SoHopDongVC = soHopDongVC;
			entity.NgayHetHanHDVC = ngayHetHanHDVC;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.MaViTriXepHang = maViTriXepHang;
			entity.MaCangCuaKhauGaXepHang = maCangCuaKhauGaXepHang;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDoHang = tenDiaDiemDoHang;
			entity.MaViTriDoHang = maViTriDoHang;
			entity.MaCangCuaKhauGaDoHang = maCangCuaKhauGaDoHang;
			entity.GhiChuKhac = ghiChuKhac;
			entity.TuyenDuong = tuyenDuong;
			entity.SoDienThoaiHQ = soDienThoaiHQ;
			entity.SoFaxHQ = soFaxHQ;
			entity.TenDaiDienDN = tenDaiDienDN;
			entity.ThoiGianVC = thoiGianVC;
			entity.SoKMVC = soKMVC;
			entity.GuidStr = guidStr;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TenNhaVanChuyen", SqlDbType.NVarChar, TenNhaVanChuyen);
			db.AddInParameter(dbCommand, "@MaNhaVanChuyen", SqlDbType.NVarChar, MaNhaVanChuyen);
			db.AddInParameter(dbCommand, "@DiaChiNhaVanChuyen", SqlDbType.NVarChar, DiaChiNhaVanChuyen);
			db.AddInParameter(dbCommand, "@CoBaoXNK", SqlDbType.NVarChar, CoBaoXNK);
			db.AddInParameter(dbCommand, "@MaLoaiHinhVanChuyen", SqlDbType.NVarChar, MaLoaiHinhVanChuyen);
			db.AddInParameter(dbCommand, "@MaHQGiamSat", SqlDbType.NVarChar, MaHQGiamSat);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.NVarChar, MaPTVC);
			db.AddInParameter(dbCommand, "@MaMucDichVC", SqlDbType.NVarChar, MaMucDichVC);
			db.AddInParameter(dbCommand, "@NgayBatDauVC", SqlDbType.DateTime, NgayBatDauVC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauVC);
			db.AddInParameter(dbCommand, "@NgayKetThucVC", SqlDbType.DateTime, NgayKetThucVC.Year <= 1753 ? DBNull.Value : (object) NgayKetThucVC);
			db.AddInParameter(dbCommand, "@NgayHopDongVC", SqlDbType.DateTime, NgayHopDongVC.Year <= 1753 ? DBNull.Value : (object) NgayHopDongVC);
			db.AddInParameter(dbCommand, "@SoHopDongVC", SqlDbType.NVarChar, SoHopDongVC);
			db.AddInParameter(dbCommand, "@NgayHetHanHDVC", SqlDbType.DateTime, NgayHetHanHDVC.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHDVC);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.NVarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@MaViTriXepHang", SqlDbType.NVarChar, MaViTriXepHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaXepHang", SqlDbType.NVarChar, MaCangCuaKhauGaXepHang);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.NVarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, TenDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaViTriDoHang", SqlDbType.NVarChar, MaViTriDoHang);
			db.AddInParameter(dbCommand, "@MaCangCuaKhauGaDoHang", SqlDbType.NVarChar, MaCangCuaKhauGaDoHang);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@TuyenDuong", SqlDbType.NVarChar, TuyenDuong);
			db.AddInParameter(dbCommand, "@SoDienThoaiHQ", SqlDbType.NVarChar, SoDienThoaiHQ);
			db.AddInParameter(dbCommand, "@SoFaxHQ", SqlDbType.NVarChar, SoFaxHQ);
			db.AddInParameter(dbCommand, "@TenDaiDienDN", SqlDbType.NVarChar, TenDaiDienDN);
			db.AddInParameter(dbCommand, "@ThoiGianVC", SqlDbType.NVarChar, ThoiGianVC);
			db.AddInParameter(dbCommand, "@SoKMVC", SqlDbType.Decimal, SoKMVC);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TransportEquipment> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TransportEquipment item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TransportEquipment(long id)
		{
			KDT_VNACCS_TransportEquipment entity = new KDT_VNACCS_TransportEquipment();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TransportEquipment_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TransportEquipment> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TransportEquipment item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (KDT_VNACCS_TransportEquipment_Detail item in TransportEquipmentCollection)
                    {
                        if (item.TransportEquipment_ID == 0)
                        {
                            item.TransportEquipment_ID = this.ID;
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
	}	
}