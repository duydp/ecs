﻿using System;
using System.Collections.Generic;
using System.Text;

[Serializable]
public class ActiveInfo
{
    //Hai quan khai bao
    private string _maChiCuc;

    public string MaChiCuc
    {
        get { return _maChiCuc; }
        set { _maChiCuc = value; }
    }
    private string _tenChiCuc;

    public string TenChiCuc
    {
        get { return _tenChiCuc; }
        set { _tenChiCuc = value; }
    }
    private string _proxyHost;

    public string ProxyHost
    {
        get { return _proxyHost; }
        set { _proxyHost = value; }
    }
    private string _proxyPort;

    public string ProxyPort
    {
        get { return _proxyPort; }
        set { _proxyPort = value; }
    }
    private string _proxyUser;

    public string ProxyUser
    {
        get { return _proxyUser; }
        set { _proxyUser = value; }
    }
    private string _proxyPassword;

    public string ProxyPassword
    {
        get { return _proxyPassword; }
        set { _proxyPassword = value; }
    }

    //Doanh nghiep
    private string _maMay;

    public string MaMay
    {
        get { return _maMay; }
        set { _maMay = value; }
    }
    private string _tenMay;

    public string TenMay
    {
        get { return _tenMay; }
        set { _tenMay = value; }
    }
    private string _maSoThue;

    public string MaSoThue
    {
        get { return _maSoThue; }
        set { _maSoThue = value; }
    }
    private string _maDoanhNghiep;

    public string MaDoanhNghiep
    {
        get { return _maDoanhNghiep; }
        set { _maDoanhNghiep = value; }
    }
    private string _tenDoanhNghiep;

    public string TenDoanhNghiep
    {
        get { return _tenDoanhNghiep; }
        set { _tenDoanhNghiep = value; }
    }
    private string _diaChi;

    public string DiaChi
    {
        get { return _diaChi; }
        set { _diaChi = value; }
    }
    private string _dienThoai;

    public string DienThoai
    {
        get { return _dienThoai; }
        set { _dienThoai = value; }
    }
    private string _fax;

    public string Fax
    {
        get { return _fax; }
        set { _fax = value; }
    }
    private string _email;

    public string Email
    {
        get { return _email; }
        set { _email = value; }
    }
    private string _tenNguoiLienHe;

    public string TenNguoiLienHe
    {
        get { return _tenNguoiLienHe; }
        set { _tenNguoiLienHe = value; }
    }

    //CSDL Doanh nghiep
    private string _serverName;

    public string ServerName
    {
        get { return _serverName; }
        set { _serverName = value; }
    }
    private string _userName;

    public string UserName
    {
        get { return _userName; }
        set { _userName = value; }
    }
    private string _passWord;

    public string PassWord
    {
        get { return _passWord; }
        set { _passWord = value; }
    }
    private string _databaseName;

    public string DatabaseName
    {
        get { return _databaseName; }
        set { _databaseName = value; }
    }

}