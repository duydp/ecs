﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TKTriGiaManager : BaseForm
    {
        public List<KDT_VNACCS_TKTriGiaThap> ListTKTG = new List<KDT_VNACCS_TKTriGiaThap>();
        private DateTime dayMin = new DateTime(1900, 1, 1);
        public VNACC_TKTriGiaManager()
        {
            InitializeComponent();

            this.Text = "Theo dõi tờ khai trị giá thấp";
        }

        private void VNACC_ToKhaiManager_Load(object sender, EventArgs e)
        {
            try
            {
                cbbPhanLoaiXuatNhap.SelectedValue = "0";
                cbbTrangThaiXuLy.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        //private void LoadData()
        //{

        //   // ListTKTG = KDT_VNACCS_TKTriGiaThap.SelectCollectionAll();
        //   // grList.DataSource = ListTKTG;
        //}
        private KDT_VNACCS_TKTriGiaThap getTKTGID(long id)
        {
            try
            {
                ListTKTG = KDT_VNACCS_TKTriGiaThap.SelectCollectionAll();
                foreach (KDT_VNACCS_TKTriGiaThap TKTG in ListTKTG)
                {
                    if (TKTG.ID == id)
                    {
                        return TKTG;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                KDT_VNACCS_TKTriGiaThap TKTG = getTKTGID(id);
                //string loaiHinh = "";


                if (TKTG.MaLoaiHinh == "E")
                {
                    VNACC_TKXuatTriGiaForm f = new VNACC_TKXuatTriGiaForm();
                    f.TKTriGiaThap = TKTG;
                    f.ShowDialog();
                }
                if (TKTG.MaLoaiHinh == "I")
                {
                    VNACC_TKNhapTriGiaForm f = new VNACC_TKNhapTriGiaForm();
                    f.TKTriGiaThap = TKTG;
                    f.ShowDialog();
                }
                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soToKhai = txtSoToKhai.Text.Trim();
                string loaiHinh = (cbbPhanLoaiXuatNhap.SelectedValue.ToString() == "0") ? "" : cbbPhanLoaiXuatNhap.SelectedValue.ToString();
                string trangThai = (cbbTrangThaiXuLy.SelectedValue == null) ? "0" : cbbTrangThaiXuLy.SelectedValue.ToString();
                string where = String.Empty;
                if (soToKhai != "")
                    where = "SoToKhai='" + soToKhai + "' and ";
                if (clcNgayDangKy.Text != "" && clcNgayDangKy.Value > dayMin)
                    where = where + "NgayDangKy='" + String.Format("{0:yyyy/MM/dd}", clcNgayDangKy.Value) + "' and ";
                if (loaiHinh != "")
                    where = where + "MaLoaiHinh='" + loaiHinh + "' and ";
                if (trangThai != "")
                    where = where + "TrangThaiXuLy='" + trangThai + "'";
                ListTKTG.Clear();
                ListTKTG = KDT_VNACCS_TKTriGiaThap.SelectCollectionDynamic(where, "ID");

                grList.DataSource = ListTKTG;
                grList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;

            }
        }

        private void cbbTrangThaiXuLy_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null, null);
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (e.Row.RowType == RowType.Record)
                {
                    string maLH = e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim();
                    if (maLH == "I")
                        e.Row.Cells["MaLoaiHinh"].Text = maLH + " - Nhập khẩu";
                    else
                        e.Row.Cells["MaLoaiHinh"].Text = maLH + " - Xuất khẩu";

                    if (e.Row.Cells["NgayDangKy"].Text != "")
                    {
                        DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                        if (dt.Year <= 1900)
                            e.Row.Cells["NgayDangKy"].Text = "";
                        else
                            e.Row.Cells["NgayDangKy"].Text = dt.ToShortDateString();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                int id = System.Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                int TrangThai = System.Convert.ToInt32(grList.CurrentRow.Cells["TrangThaiXuLy"].Value.ToString());
                if (TrangThai == 0)
                {
                    if (ShowMessage("Bạn có muốn xóa tờ khai này không?", true) == "Yes")
                    {
                        KDT_VNACCS_TKTriGiaThap.DeleteDynamic("ID=" + id);
                        ShowMessage("Xóa tờ khai thành công.", false);
                    }
                }
                else
                    ShowMessage("Không thể xóa tờ khai đã được xác nhận với Hải quan. ", false);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
            btnTimKiem_Click(null, null);
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách _" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel(grList);
        }

        private void cbbPhanLoaiXuatNhap_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(null,null);
        }
    }
}
