/*
 * Author: HUNGTQ
 */
using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Reflection;
using System.Resources;

namespace Logger
{
    /// <summary>
    /// Summary description for LocalLogger.
    /// </summary>
    public class LocalLogger
    {
        private StreamWriter sw = null;
        private static LocalLogger instance;
        private string fileName;

        // Create a TraceSwitch.
        private static TraceSwitch generalSwitch = new TraceSwitch("General", "Entire Application");

        public LocalLogger()
        {
            //
            // TODO: Add constructor logic here
            //
            string baseDir = AppDomain.CurrentDomain.BaseDirectory + "\\";
            fileName = baseDir + "Error.log";

            //Kiem tra gioi han dung luong file .log 2MB
            if (System.IO.File.Exists(fileName))
            {
                System.IO.FileInfo fi = new FileInfo(fileName);
                if (fi.Length > (1024 * 2))
                    System.IO.File.Delete(fileName);
            }
        }
        public static LocalLogger Instance()
        {
            if (instance == null)
                instance = new LocalLogger();
            return instance;
        }

        private void stop()
        {
            //sw.Close();
        }
        ~LocalLogger()
        {
            if (sw != null) sw.Close();
            sw = null;
        }

        public void WriteMessage(Exception ex)
        {
            WriteMessage(ex.GetHashCode(), ex.Message, ex, true);
        }

        public void WriteMessage(string title, Exception ex)
        {
            WriteMessage(ex.GetHashCode(), title + " - " + ex.Message, ex, true);
        }

        private void WriteMessage(int nCode, string sMessage, System.Exception oInnerException, bool bLog)
        {
            // need to add logic to check what log destination we
            // should logging to e.g. file, eventlog, database, remote
            // debugger
            if (bLog)
            {
                // log it
                sw = new StreamWriter(fileName, true, System.Text.Encoding.Unicode);
                lock (sw)
                {
                    sw.WriteLine(Format(nCode, sMessage, oInnerException));
                    sw.Close();
                }
            }
        }

        private string Format(int nCode, string sMessage, System.Exception oInnerException)
        {
            StringBuilder sNewMessage = new StringBuilder();
            string sErrorStack = null;

            // get the error stack, if InnerException is null,
            // sErrorStack will be "exception was not chained" and
            // should never be null
            sErrorStack = BuildErrorStack(oInnerException);

            // we want immediate gradification
            Trace.AutoFlush = true;

            sNewMessage.AppendLine("===============================\n")
                       .Append("No.: \n")
                       .Append(nCode)
                       .Append(" - ")
                 .Append(DateTime.Now.ToShortDateString())
                       .Append(":")
                 .AppendLine(DateTime.Now.ToShortTimeString())
                       .AppendLine("===============================\n")
                       .AppendLine(sMessage)
                       .Append("\n")
                       .Append(sErrorStack)
                       .AppendLine("\n");

            return sNewMessage.ToString();
        }

        /// <summary>
        /// Takes a first nested exception object and builds a error
        /// stack from its chained contents
        /// </summary>
        /// <param name="oChainedException"></param>
        /// <returns></returns>
        private string BuildErrorStack(System.Exception oChainedException)
        {
            string sErrorStack = null;
            StringBuilder sbErrorStack = new StringBuilder();
            int nErrStackNum = 1;
            System.Exception oInnerException = null;

            if (oChainedException != null)
            {
                sbErrorStack.AppendLine("\n")
                            .AppendLine("Error Stack: \n")
                            .AppendLine("-----\n");

                oInnerException = oChainedException;
                while (oInnerException != null)
                {
                    sbErrorStack.Append(nErrStackNum)
                                .Append(") ")
                     .Append(oInnerException.Message)
                                .AppendLine("\n");

                    oInnerException =
                          oInnerException.InnerException;

                    nErrStackNum++;
                }

                sbErrorStack.Append("Call Stack:\n")
                  .Append(oChainedException.StackTrace);

                sErrorStack = sbErrorStack.ToString();
            }
            else
            {
                sErrorStack = "exception was not chained";
            }

            return sErrorStack;
        }

    }

}
