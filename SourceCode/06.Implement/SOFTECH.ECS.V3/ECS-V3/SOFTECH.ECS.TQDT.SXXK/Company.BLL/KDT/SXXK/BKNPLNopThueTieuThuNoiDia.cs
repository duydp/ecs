using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.BLL.KDT.SXXK
{
    public partial class BKNPLNopThueTieuThuNoiDia
    {
        public override bool Equals(object obj)
        {
            BKNPLNopThueTieuThuNoiDia item = (BKNPLNopThueTieuThuNoiDia)obj;
            bool isEqual = item.SoToKhai == SoToKhai && item.MaLoaiHinh == MaLoaiHinh
                && item.MaHaiQuan == MaHaiQuan && item.NamDangKy == NamDangKy;
            return isEqual;
        }
    }
}