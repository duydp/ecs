﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;


namespace Company.Interface
{
    public partial class VNACC_TK_TKTriGiaForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        DataTable dt = new DataTable();
        DataSet dsMaTen = new DataSet();
        DataSet dsMaTienTe = new DataSet();
        DataSet dsMaPhanLoai = new DataSet();
        public bool isAdd = true;
        KDT_VNACC_TK_KhoanDieuChinh khoanDieuChinh = new KDT_VNACC_TK_KhoanDieuChinh();
        public String Caption;
        public bool IsChange;
        public VNACC_TK_TKTriGiaForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.IDA.ToString());
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void VNACC_TK_TKTriGiaForm_Load(object sender, EventArgs e)
        {
            try
            {
                SetIDControl();
                LoadData();
                Caption = this.Text;
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                {
                    btnGhi.Enabled = false;
                    btnXoa.Enabled = false;
                    btnPhanBoPhi.Enabled = false;
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void LoadData()
        {
            try
            {
                dsMaTen = VNACC_Category_Common.SelectDynamic("ReferenceDB='A401'", "");
                dsMaTienTe = VNACC_Category_CurrencyExchange.SelectAll();
                dsMaPhanLoai = VNACC_Category_Common.SelectDynamic("ReferenceDB='E014'", "");
                dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.KhoanDCCollection);
                grList.DropDowns["drdMaTen"].DataSource = dsMaTen.Tables[0];
                grList.DropDowns["drdMaTT"].DataSource = dsMaTienTe.Tables[0];
                grList.DropDowns["drdMaPhanLoai"].DataSource = dsMaPhanLoai.Tables[0];
                grList.DropDowns["drdMaTen"].AutoSizeColumns();
                grList.DropDowns["drdMaTT"].AutoSizeColumns();
                grList.DropDowns["drdMaPhanLoai"].AutoSizeColumns();
                grList.DataSource = dt;

                SetKhaiTriGia();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void SetKhaiTriGia()
        {
            try
            {

                ctrMaPhanLoaiTriGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaPhanLoaiTriGia.Code = TKMD.MaPhanLoaiTriGia;
                ctrMaPhanLoaiTriGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtSoTiepNhanTKTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoTiepNhanTKTriGia.Text = Convert.ToString(TKMD.SoTiepNhanTKTriGia);
                txtSoTiepNhanTKTriGia.TextChanged += new EventHandler(txt_TextChanged);


                ctrMaTTHieuChinhTriGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTHieuChinhTriGia.Code = TKMD.MaTTHieuChinhTriGia;
                ctrMaTTHieuChinhTriGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtGiaHieuChinhTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtGiaHieuChinhTriGia.Text = Convert.ToString(TKMD.GiaHieuChinhTriGia);
                txtGiaHieuChinhTriGia.TextChanged += new EventHandler(txt_TextChanged);

                ctrMaPhanLoaiPhiVC.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaPhanLoaiPhiVC.Code = TKMD.MaPhanLoaiPhiVC;
                ctrMaPhanLoaiPhiVC.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtPhiVanChuyen.TextChanged -= new EventHandler(txt_TextChanged);
                txtPhiVanChuyen.Text = Convert.ToString(TKMD.PhiVanChuyen);
                txtPhiVanChuyen.TextChanged += new EventHandler(txt_TextChanged);

                ctrMaTTPhiVC.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTPhiVC.Code = TKMD.MaTTPhiVC;
                ctrMaTTPhiVC.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                ctrMaPhanLoaiPhiBH.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaPhanLoaiPhiBH.Code = TKMD.MaPhanLoaiPhiBH;
                ctrMaPhanLoaiPhiBH.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtPhiBaoHiem.TextChanged -= new EventHandler(txt_TextChanged);
                txtPhiBaoHiem.Text = Convert.ToString(TKMD.PhiBaoHiem);
                txtPhiBaoHiem.TextChanged += new EventHandler(txt_TextChanged);

                ctrMaTTPhiBH.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTPhiBH.Code = TKMD.MaTTPhiBH;
                ctrMaTTPhiBH.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtChiTietKhaiTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtChiTietKhaiTriGia.Text = TKMD.ChiTietKhaiTriGia;
                txtChiTietKhaiTriGia.TextChanged += new EventHandler(txt_TextChanged);

                txtTongHeSoPhanBoTG.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongHeSoPhanBoTG.Text = TKMD.TongHeSoPhanBoTG.ToString();
                txtTongHeSoPhanBoTG.TextChanged += new EventHandler(txt_TextChanged);

                txtSoDangKyBH.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDangKyBH.Text = TKMD.SoDangKyBH;
                txtSoDangKyBH.TextChanged += new EventHandler(txt_TextChanged);

                txtMaPhanLoaiTongGiaCoBan.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaPhanLoaiTongGiaCoBan.Text = TKMD.MaPhanLoaiTongGiaCoBan;
                txtMaPhanLoaiTongGiaCoBan.TextChanged += new EventHandler(txt_TextChanged);

                //ctrMaPhanLoaiTriGia.Code = TKMD.MaPhanLoaiTriGia;
                //txtSoTiepNhanTKTriGia.Text = Convert.ToString(TKMD.SoTiepNhanTKTriGia);
                //ctrMaTTHieuChinhTriGia.Code = TKMD.MaTTHieuChinhTriGia;
                //txtGiaHieuChinhTriGia.Text = Convert.ToString(TKMD.GiaHieuChinhTriGia);
                //ctrMaPhanLoaiPhiVC.Code = TKMD.MaPhanLoaiPhiVC;
                //txtPhiVanChuyen.Text = Convert.ToString(TKMD.PhiVanChuyen);
                //ctrMaTTPhiVC.Code = TKMD.MaTTPhiVC;
                //ctrMaPhanLoaiPhiBH.Code = TKMD.MaPhanLoaiPhiBH;
                //txtPhiBaoHiem.Text = Convert.ToString(TKMD.PhiBaoHiem);
                //ctrMaTTPhiBH.Code = TKMD.MaTTPhiBH;
                //txtChiTietKhaiTriGia.Text = TKMD.ChiTietKhaiTriGia;
                //txtTongHeSoPhanBoTG.Text = TKMD.TongHeSoPhanBoTG.ToString();
                //txtSoDangKyBH.Text = TKMD.SoDangKyBH;
                //txtMaPhanLoaiTongGiaCoBan.Text = TKMD.MaPhanLoaiTongGiaCoBan;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void GetKhaiTriGia()
        {
            try
            {
                TKMD.MaPhanLoaiTriGia = ctrMaPhanLoaiTriGia.Code;
                TKMD.SoTiepNhanTKTriGia = Convert.ToDecimal(txtSoTiepNhanTKTriGia.Value);
                TKMD.MaTTHieuChinhTriGia = ctrMaTTHieuChinhTriGia.Code;
                TKMD.GiaHieuChinhTriGia = Convert.ToDecimal(txtGiaHieuChinhTriGia.Value);
                TKMD.MaPhanLoaiPhiVC = ctrMaPhanLoaiPhiVC.Code;
                TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
                TKMD.MaTTPhiVC = ctrMaTTPhiVC.Code;
                TKMD.MaPhanLoaiPhiBH = ctrMaPhanLoaiPhiBH.Code;
                TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
                TKMD.MaTTPhiBH = ctrMaTTPhiBH.Code;
                TKMD.ChiTietKhaiTriGia = txtChiTietKhaiTriGia.Text;
                TKMD.SoDangKyBH = txtSoDangKyBH.Text;
                TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(txtTongHeSoPhanBoTG.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(ctrTen, errorProvider, "Mã tên khoản điều chỉnh", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaPhanLoai, errorProvider, "Mã phân loại khoản điều chỉnh", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaTT, errorProvider, "Mã tiền tệ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTriGiaKhoanDC, errorProvider, "Trị giá khoản điều chỉnh", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void GetKhoanDieuChinh()
        {
            try
            {
                khoanDieuChinh.TKMD_ID = TKMD.ID;
                khoanDieuChinh.MaTenDieuChinh = ctrTen.Code;
                khoanDieuChinh.MaPhanLoaiDieuChinh = ctrMaPhanLoai.Code;
                khoanDieuChinh.MaTTDieuChinhTriGia = ctrMaTT.Code;
                khoanDieuChinh.TriGiaKhoanDieuChinh = Convert.ToDecimal(txtTriGiaKhoanDC.Text.ToString());
                khoanDieuChinh.TongHeSoPhanBo = Convert.ToDecimal(txtTongHSPB.Text.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = TKMD.KhoanDCCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetKhoanDieuChinh()
        {
            try
            {
                ctrTen.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrTen.Code = khoanDieuChinh.MaTenDieuChinh;
                ctrTen.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                ctrMaPhanLoai.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaPhanLoai.Code = khoanDieuChinh.MaPhanLoaiDieuChinh;
                ctrMaPhanLoai.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                ctrMaTT.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTT.Code = khoanDieuChinh.MaTTDieuChinhTriGia;
                ctrMaTT.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtTriGiaKhoanDC.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGiaKhoanDC.Text = khoanDieuChinh.TriGiaKhoanDieuChinh.ToString();
                txtTriGiaKhoanDC.TextChanged += new EventHandler(txt_TextChanged);

                txtTongHSPB.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongHSPB.Text = khoanDieuChinh.TongHeSoPhanBo.ToString();
                txtTongHSPB.TextChanged += new EventHandler(txt_TextChanged);

                //ctrTen.Code = khoanDieuChinh.MaTenDieuChinh;
                //ctrMaPhanLoai.Code = khoanDieuChinh.MaPhanLoaiDieuChinh;
                //ctrMaTT.Code = khoanDieuChinh.MaTTDieuChinhTriGia;
                //txtTriGiaKhoanDC.Text = khoanDieuChinh.TriGiaKhoanDieuChinh.ToString();
                //txtTongHSPB.Text = khoanDieuChinh.TongHeSoPhanBo.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Save()
        {
            try
            {
                GetKhaiTriGia();
                //int sott = 1;
                //TKMD.KhoanDCCollection.Clear();
                //foreach (DataRow i in dt.Rows)
                //{
                //    KDT_VNACC_TK_KhoanDieuChinh dc = new KDT_VNACC_TK_KhoanDieuChinh();
                //    if (i["ID"].ToString().Length != 0)
                //    {
                //        dc.ID = Convert.ToInt32(i["ID"].ToString());
                //        dc.TKMD_ID = Convert.ToInt32(i["TKMD_ID"].ToString());
                //    }
                //    dc.SoTT = sott;
                //    dc.MaTenDieuChinh = i["MaTenDieuChinh"].ToString();
                //    dc.MaPhanLoaiDieuChinh = i["MaPhanLoaiDieuChinh"].ToString();
                //    dc.MaTTDieuChinhTriGia = i["MaTTDieuChinhTriGia"].ToString();
                //    dc.TriGiaKhoanDieuChinh = string.IsNullOrEmpty(i["TriGiaKhoanDieuChinh"].ToString()) ? 0 : Convert.ToDecimal(i["TriGiaKhoanDieuChinh"].ToString());
                //    dc.TongHeSoPhanBo = string.IsNullOrEmpty(i["TongHeSoPhanBo"].ToString()) ? 0 : Convert.ToDecimal(i["TongHeSoPhanBo"].ToString());

                //    TKMD.KhoanDCCollection.Add(dc);
                //    sott++;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                this.Cursor = Cursors.Default;
            }

        }



        private void grList_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }
        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                ctrMaPhanLoaiTriGia.TagName = "VD1"; //Mã phân loại khai trị giá
                txtSoTiepNhanTKTriGia.Tag = "VD2"; //Số tiếp nhận tờ khai trị giá tổng hợp
                ctrMaTTHieuChinhTriGia.TagName = "VCC"; //Mã tiền tệ của giá cơ sở hiệu chỉnh trị giá
                txtGiaHieuChinhTriGia.Tag = "VPC"; //Giá cơ sở để hiệu chỉnh trị giá
                ctrMaPhanLoaiPhiVC.TagName = "FR1"; //Mã phân loại phí vận chuyển
                ctrMaTTPhiVC.TagName = "FR2"; //Mã tiền tệ phí vận chuyển
                txtPhiVanChuyen.Tag = "FR3"; //Phí vận chuyển
                ctrMaPhanLoaiPhiBH.TagName = "IN1"; //Mã phân loại bảo hiểm
                ctrMaTTPhiBH.TagName = "IN2"; //Mã tiền tệ của tiền bảo hiểm
                txtPhiBaoHiem.Tag = "IN3"; //Phí bảo hiểm
                txtSoDangKyBH.Tag = "IN4"; //Số đăng ký bảo hiểm tổng hợp
                //txtMaTenDieuChinh.Tag = "VR_"; //Mã tên khoản điều chỉnh
                //txtMaPhanLoaiDieuChinh.Tag = "VI_"; //Mã phân loại điều chỉnh trị giá
                //txtMaTTDieuChinhTriGia.Tag = "VC_"; //Mã đồng tiền của khoản điều chỉnh trị giá
                //txtTriGiaKhoanDieuChinh.Tag = "VP_"; //Trị giá khoản điều chỉnh
                //txtTongHeSoPhanBo.Tag = "VT_"; //Tổng hệ số phân bổ trị giá khoản điều chỉnh
                txtChiTietKhaiTriGia.Tag = "VLD"; //Chi tiết khai trị giá
                txtTongHeSoPhanBoTG.Tag = "TP"; //Tổng hệ số phân bổ trị giá


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_TK_KhoanDieuChinh> ItemColl = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp có muốn xóa khoản điều chỉnh này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_TK_KhoanDieuChinh)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_TK_KhoanDieuChinh item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.KhoanDCCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (KDT_VNACC_TK_KhoanDieuChinh item in TKMD.KhoanDCCollection)
                    {
                        item.SoTT = k;
                        k++;
                    }
                    this.SetChange(true);
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            //try
            //{
            //    int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
            //    if (ID > 0)
            //    {
            //        if (ShowMessage("Bạn có muốn xóa khoản điều chỉnh này không?", true) == "Yes")
            //        {
            //            KDT_VNACC_TK_KhoanDieuChinh.DeleteDynamic("ID=" + ID);
            //        }
            //    }
            //    grList.CurrentRow.Delete();
            //    BindData();

            //    this.Cursor = Cursors.Default;
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    this.Cursor = Cursors.Default;

            //}
        }

        private void btnPhanBoPhi_Click(object sender, EventArgs e)
        {
            try
            {
                VNACC_TK_PhanBoPhi f = new VNACC_TK_PhanBoPhi(dsMaTen, dsMaPhanLoai);
                f.TKMD = this.TKMD;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=WnhWxIKqr3k");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grList_Validating(object sender, CancelEventArgs e)
        {
          
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetKhoanDieuChinh();
                if (isAdd)
                    TKMD.KhoanDCCollection.Add(khoanDieuChinh);
                int i = 1;
                foreach (KDT_VNACC_TK_KhoanDieuChinh item in TKMD.KhoanDCCollection)
                {
                    item.SoTT = i;
                    i++;
                }
                khoanDieuChinh = new KDT_VNACC_TK_KhoanDieuChinh();
                ctrTen.Code = String.Empty;
                ctrMaPhanLoai.Code = String.Empty;
                ctrMaTT.Code = String.Empty;
                txtTriGiaKhoanDC.Text = String.Empty;
                txtTongHSPB.Text = String.Empty;
                isAdd = true;
                BindData();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    khoanDieuChinh = new KDT_VNACC_TK_KhoanDieuChinh();
                    khoanDieuChinh = (KDT_VNACC_TK_KhoanDieuChinh)e.Row.DataRow;
                    SetKhoanDieuChinh();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
    }
}
