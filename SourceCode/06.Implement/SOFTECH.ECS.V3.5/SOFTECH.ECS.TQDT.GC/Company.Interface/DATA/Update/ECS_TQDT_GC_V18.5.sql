 alter table  t_GC_ThanhKhoanHDGC
 alter column Ten nvarchar(255) not null
 GO
 
/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_Insert]    Script Date: 07/14/2014 15:38:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_Insert]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_Insert]
	@Ten nvarchar(255),
	@DVT nvarchar(15),
	@TongLuongNK numeric(18, 5),
	@TongLuongCU numeric(18, 5),
	@TongLuongXK numeric(18, 5),
	@ChenhLech numeric(18, 5),
	@KetLuanXLCL nvarchar(max),
	@OldHD_ID bigint,
	@STT int,
	@HopDong_ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_GC_ThanhKhoanHDGC]
(
	[Ten],
	[DVT],
	[TongLuongNK],
	[TongLuongCU],
	[TongLuongXK],
	[ChenhLech],
	[KetLuanXLCL],
	[OldHD_ID],
	[STT]
)
VALUES 
(
	@Ten,
	@DVT,
	@TongLuongNK,
	@TongLuongCU,
	@TongLuongXK,
	@ChenhLech,
	@KetLuanXLCL,
	@OldHD_ID,
	@STT
)

SET @HopDong_ID = SCOPE_IDENTITY()

GO

/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_InsertUpdate]    Script Date: 07/14/2014 15:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_InsertUpdate]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_InsertUpdate]
	@HopDong_ID bigint,
	@Ten nvarchar(255),
	@DVT nvarchar(15),
	@TongLuongNK numeric(18, 5),
	@TongLuongCU numeric(18, 5),
	@TongLuongXK numeric(18, 5),
	@ChenhLech numeric(18, 5),
	@KetLuanXLCL nvarchar(max),
	@OldHD_ID bigint,
	@STT int
AS
IF EXISTS(SELECT [HopDong_ID] FROM [dbo].[t_GC_ThanhKhoanHDGC] WHERE [HopDong_ID] = @HopDong_ID)
	BEGIN
		UPDATE
			[dbo].[t_GC_ThanhKhoanHDGC] 
		SET
			[Ten] = @Ten,
			[DVT] = @DVT,
			[TongLuongNK] = @TongLuongNK,
			[TongLuongCU] = @TongLuongCU,
			[TongLuongXK] = @TongLuongXK,
			[ChenhLech] = @ChenhLech,
			[KetLuanXLCL] = @KetLuanXLCL,
			[OldHD_ID] = @OldHD_ID,
			[STT] = @STT
		WHERE
			[HopDong_ID] = @HopDong_ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_GC_ThanhKhoanHDGC]
		(
			[Ten],
			[DVT],
			[TongLuongNK],
			[TongLuongCU],
			[TongLuongXK],
			[ChenhLech],
			[KetLuanXLCL],
			[OldHD_ID],
			[STT]
		)
		VALUES 
		(
			@Ten,
			@DVT,
			@TongLuongNK,
			@TongLuongCU,
			@TongLuongXK,
			@ChenhLech,
			@KetLuanXLCL,
			@OldHD_ID,
			@STT
		)		
	END
GO

/****** Object:  StoredProcedure [dbo].[p_GC_ThanhKhoanHDGC_Update]    Script Date: 07/14/2014 15:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GC_ThanhKhoanHDGC_Update]
-- Database: ECS_GIACONG
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 07, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_GC_ThanhKhoanHDGC_Update]
	@HopDong_ID bigint,
	@Ten nvarchar(255),
	@DVT nvarchar(15),
	@TongLuongNK numeric(18, 5),
	@TongLuongCU numeric(18, 5),
	@TongLuongXK numeric(18, 5),
	@ChenhLech numeric(18, 5),
	@KetLuanXLCL nvarchar(max),
	@OldHD_ID bigint,
	@STT int
AS
UPDATE
	[dbo].[t_GC_ThanhKhoanHDGC]
SET
	[Ten] = @Ten,
	[DVT] = @DVT,
	[TongLuongNK] = @TongLuongNK,
	[TongLuongCU] = @TongLuongCU,
	[TongLuongXK] = @TongLuongXK,
	[ChenhLech] = @ChenhLech,
	[KetLuanXLCL] = @KetLuanXLCL,
	[OldHD_ID] = @OldHD_ID,
	[STT] = @STT
WHERE
	[HopDong_ID] = @HopDong_ID

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.5',GETDATE(), N' Cập nhật t_GC_thanhKhoanHDGC')
END