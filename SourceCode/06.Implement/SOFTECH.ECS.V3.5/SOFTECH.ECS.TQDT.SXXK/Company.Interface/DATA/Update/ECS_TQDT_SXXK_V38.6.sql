
GO
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]      
-- Database: ECS_GC      
-- Author: Dang Phuoc Duy    
-- Time created: Tuesday, October 20, 2015    
------------------------------------------------------------------------------------------------------------------------      
      
ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]      
 @WhereCondition NVARCHAR(500),      
 @OrderByExpression NVARCHAR(250) = NULL      
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(3250)      
      
SET @SQL = 'SELECT      
  tkmd.ID,      
 [SoTiepNhan],      
 [NgayTiepNhan],      
 [MaHaiQuan],      
 [SoToKhai],      
 [MaLoaiHinh],      
 [NgayDangKy],      
 [MaDoanhNghiep],      
 [TenDoanhNghiep],      
 [MaDaiLyTTHQ],      
 [TenDaiLyTTHQ],      
 [TenDonViDoiTac],      
 [ChiTietDonViDoiTac],      
 [SoGiayPhep],      
 [NgayGiayPhep],      
 [NgayHetHanGiayPhep],      
 [SoHopDong],      
 [NgayHopDong],      
 [NgayHetHanHopDong],      
 [SoHoaDonThuongMai],      
 [NgayHoaDonThuongMai],      
 [PTVT_ID],      
 [SoHieuPTVT],      
 [NgayDenPTVT],      
 [QuocTichPTVT_ID],      
 [LoaiVanDon],      
 [SoVanDon],      
 [NgayVanDon],      
 [NuocXK_ID],      
 [NuocNK_ID],      
 [DiaDiemXepHang],      
 [CuaKhau_ID],      
 [DKGH_ID],      
 [NguyenTe_ID],      
 [TyGiaTinhThue],      
 [TyGiaUSD],      
 [PTTT_ID],      
 [SoHang],      
 [SoLuongPLTK],      
 [TenChuHang],      
 [ChucVu],      
 [SoContainer20],      
 [SoContainer40],      
 [SoKien],      
 [TongTriGiaKhaiBao],      
 [TongTriGiaTinhThue],      
 [LoaiToKhaiGiaCong],      
 [LePhiHaiQuan],      
 [PhiBaoHiem],      
 [PhiVanChuyen],      
 [PhiXepDoHang],      
 [PhiKhac],      
 [CanBoDangKy],      
 [QuanLyMay],      
 [TrangThaiXuLy],      
 [LoaiHangHoa],      
 [GiayTo],      
 [PhanLuong],      
 [MaDonViUT],      
 [TenDonViUT],      
 [TrongLuongNet],      
 [SoTienKhoan],          
 [GUIDSTR],      
 [DeXuatKhac],      
 [LyDoSua],      
 [ActionStatus],      
 [GuidReference],      
 [NamDK],      
 [HUONGDAN] ,
 HeSoNhan,    
 hmd.SoThuTuHang,    
 hmd.MaPhu,  
 hmd.MaHS,    
 hmd.TenHang,    
 hmd.NuocXX_ID,    
 hmd.SoLuong,    
 hmd.DonGiaKB,    
 hmd.TriGiaKB,    
 hmd.DonGiaTT,    
 hmd.TriGiaTT,    
 CASE WHEN tkmd.ID <>0 THEN (SELECT dvt.Ten FROM dbo.t_HaiQuan_DonViTinh dvt WHERE dvt.ID=hmd.DVT_ID) ELSE NULL END AS DVT    
 FROM [dbo].[t_KDT_ToKhaiMauDich] tkmd INNER JOIN dbo.t_KDT_HangMauDich hmd ON hmd.TKMD_ID = tkmd.ID  WHERE ' + @WhereCondition      
      
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0      
BEGIN      
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression      
END      
      
EXEC sp_executesql @SQL      


 GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_VNACCS_CapSoToKhai_Load]  
-- Database: ECS_TQDT_SXXK_V4_UPGRADE  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, May 08, 2014  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_VNACCS_CapSoToKhai_Load]  
 @SoTK int,  
 @MaLoaiHinh varchar(50)  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT  
 [SoTK],  
 [MaLoaiHinh],  
 [NamDangKy],  
 [SoTKVNACCS],  
 [SoTKVNACCSFull],  
 [SoTKDauTien],  
 [SoNhanhTK],  
 [TongSoTKChiaNho],  
 [SoTKTNTX],  
 [Temp1],  
 [SoTKGiay]  
FROM  
 [dbo].[t_VNACCS_CapSoToKhai]  
WHERE  
 [SoTK] = @SoTK  
 AND [MaLoaiHinh] = @MaLoaiHinh  

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.6',GETDATE(), N'CẬP NHẬT PROCDURE ')
END