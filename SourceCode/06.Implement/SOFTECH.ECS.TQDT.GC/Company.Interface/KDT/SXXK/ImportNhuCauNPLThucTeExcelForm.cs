using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
namespace Company.Interface.KDT.SXXK
{
    public partial class ImportNhuCauNPLThucTeExcelForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public DataTable tableNPL;
        private DataTable tableTMP;
        public ImportNhuCauNPLThucTeExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if(!cvError.IsValid)return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;           
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                //Message("MSG_EXC03", "", false);
                showMsg("MSG_0203008");
                //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                return;
            }
            try
            {
               ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //Message("MSG_EXC01", "", false);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn ;
            int maHangCol = 0;          
            char SoLuongColumn ;
            int SoLuongCol = 0;
            try{
                  maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
                  maHangCol = ConvertCharToInt(maHangColumn);
                
                  SoLuongColumn = Convert.ToChar(txtSoLuong.Text);
                  SoLuongCol = ConvertCharToInt(SoLuongColumn);
            }
            catch { return; }
            bool ok = true;
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        string MaNPL = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        NguyenPhuLieu NPL = new NguyenPhuLieu();
                        NPL.HopDong_ID = HD.ID;
                        NPL.Ma = MaNPL;
                        if (!NPL.Load())
                        {
                            string st = showMsg("MSG_0203021", MaNPL, true);
                            if (st != "Yes")
                            {
                                ok = false;
                                break;
                            }
                            else
                                continue;
                        }
                        decimal SoLuongDangKy = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value);
                        DataRow[] rowNPLCollection = tableTMP.Select("MaNPL='" + MaNPL + "'");
                        if (rowNPLCollection.Length == 0)
                        {
                            DataRow rowNPL = tableTMP.NewRow();
                            rowNPL["MaNPL"] = MaNPL;
                            rowNPL["TenNPL"] = NPL.Ten;
                            rowNPL["NhuCauDM"] = 0;
                            rowNPL["NhuCauThucTe"] = SoLuongDangKy;
                            tableTMP.Rows.Add(rowNPL);
                        }
                        else
                        {
                            DataRow row = rowNPLCollection[0];
                            row["NhuCauThucTe"] = SoLuongDangKy;
                        }
                    }
                    catch(Exception ex)
                    {
                        showMsg("MSG_2702004", ex.Message);
                        //ShowMessage("Có lỗi khi đọc dữ liệu : "+ex.Message, false);
                        ok = false;
                        break;
                    }
                }
            }
            if (ok)
            {
                tableNPL = tableTMP;
            }
            this.Close();
        }

        private void NguyenPhuLieuReadExcelForm_Load(object sender, EventArgs e)
        {
            tableTMP = new DataTable();
            foreach (DataColumn col in tableNPL.Columns)
                tableTMP.Columns.Add(col.ColumnName, col.DataType);
            openFileDialog1.InitialDirectory = Application.StartupPath;
            foreach (DataRow row in tableNPL.Rows)
                tableTMP.ImportRow(row);
        }
        
    }
}