﻿namespace Company.Interface.KDT.SXXK
{
    partial class LenhSanXuatSPManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LenhSanXuatSPManagerForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.cbbTinhTrang = new Janus.Windows.EditControls.UIComboBox();
            this.txtPO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMaDDSX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grList);
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1038, 616);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnSearch);
            this.uiGroupBox1.Controls.Add(this.cbbTinhTrang);
            this.uiGroupBox1.Controls.Add(this.txtPO);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.txtMaDDSX);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1038, 91);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(310, 55);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(105, 23);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbbTinhTrang
            // 
            this.cbbTinhTrang.BackColor = System.Drawing.Color.White;
            this.cbbTinhTrang.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbTinhTrang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Tạo mới";
            uiComboBoxItem4.Value = "0";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Đang sản xuất";
            uiComboBoxItem5.Value = "1";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Đã hoàn thành";
            uiComboBoxItem6.Value = "2";
            this.cbbTinhTrang.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbbTinhTrang.Location = new System.Drawing.Point(310, 23);
            this.cbbTinhTrang.Name = "cbbTinhTrang";
            this.cbbTinhTrang.Size = new System.Drawing.Size(273, 22);
            this.cbbTinhTrang.TabIndex = 2;
            this.cbbTinhTrang.ValueMember = "ID";
            this.cbbTinhTrang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbTinhTrang.SelectedIndexChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtPO
            // 
            this.txtPO.BackColor = System.Drawing.Color.White;
            this.txtPO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPO.Location = new System.Drawing.Point(106, 56);
            this.txtPO.Name = "txtPO";
            this.txtPO.Size = new System.Drawing.Size(131, 22);
            this.txtPO.TabIndex = 3;
            this.txtPO.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPO.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(245, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 14);
            this.label19.TabIndex = 0;
            this.label19.Text = "Tình trạng";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(12, 60);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "Số đơn hàng";
            // 
            // txtMaDDSX
            // 
            this.txtMaDDSX.BackColor = System.Drawing.Color.White;
            this.txtMaDDSX.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDDSX.Location = new System.Drawing.Point(106, 23);
            this.txtMaDDSX.Name = "txtMaDDSX";
            this.txtMaDDSX.Size = new System.Drawing.Size(131, 22);
            this.txtMaDDSX.TabIndex = 1;
            this.txtMaDDSX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDDSX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDDSX.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 14);
            this.label15.TabIndex = 0;
            this.label15.Text = "Lệnh sản xuất";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnExportExcel);
            this.uiGroupBox7.Controls.Add(this.btnClose);
            this.uiGroupBox7.Controls.Add(this.btnDelete);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 568);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1038, 48);
            this.uiGroupBox7.TabIndex = 6;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(957, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDelete.Location = new System.Drawing.Point(868, 16);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(83, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grList_DesignTimeLayout_Reference_0.Instance")));
            grList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grList_DesignTimeLayout_Reference_0});
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Hierarchical = true;
            this.grList.Location = new System.Drawing.Point(0, 91);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(1038, 477);
            this.grList.TabIndex = 348;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            this.grList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_LoadingRow);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(6, 16);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(90, 23);
            this.btnExportExcel.TabIndex = 335;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // LenhSanXuatSPManagerForm
            // 
            this.ClientSize = new System.Drawing.Size(1038, 616);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "LenhSanXuatSPManagerForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Theo dõi lệnh sản xuất của sản phẩm";
            this.Load += new System.EventHandler(this.LenhSanXuatSPManagerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.EditControls.UIComboBox cbbTinhTrang;
        private Janus.Windows.GridEX.EditControls.EditBox txtPO;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDDSX;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
    }
}
