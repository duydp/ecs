﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class CustomsGoodsItems
    {
        /// <summary>
        ///  Danh sách Khoản mục phí.
        /// </summary>
       [XmlElement("CustomsGoodsItem")]
       public List<CustomsGoodsItem> CustomsGoodsItem { get; set; }
    }
}
