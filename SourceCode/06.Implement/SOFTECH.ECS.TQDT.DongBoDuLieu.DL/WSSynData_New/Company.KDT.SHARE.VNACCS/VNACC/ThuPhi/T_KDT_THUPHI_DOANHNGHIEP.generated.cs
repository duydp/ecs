﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class T_KDT_THUPHI_DOANHNGHIEP : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaHQ { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string DiaChi { set; get; }
		public string NguoiLienHe { set; get; }
		public string Email { set; get; }
		public string SoDienThoai { set; get; }
		public string Serial { set; get; }
		public string CertString { set; get; }
		public string Subject { set; get; }
		public string Issuer { set; get; }
		public DateTime ValidFrom { set; get; }
		public DateTime ValidTo { set; get; }
		public string GuidStr { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_THUPHI_DOANHNGHIEP> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_THUPHI_DOANHNGHIEP> collection = new List<T_KDT_THUPHI_DOANHNGHIEP>();
			while (reader.Read())
			{
				T_KDT_THUPHI_DOANHNGHIEP entity = new T_KDT_THUPHI_DOANHNGHIEP();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiLienHe"))) entity.NguoiLienHe = reader.GetString(reader.GetOrdinal("NguoiLienHe"));
				if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoai"))) entity.SoDienThoai = reader.GetString(reader.GetOrdinal("SoDienThoai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Serial"))) entity.Serial = reader.GetString(reader.GetOrdinal("Serial"));
				if (!reader.IsDBNull(reader.GetOrdinal("CertString"))) entity.CertString = reader.GetString(reader.GetOrdinal("CertString"));
				if (!reader.IsDBNull(reader.GetOrdinal("Subject"))) entity.Subject = reader.GetString(reader.GetOrdinal("Subject"));
				if (!reader.IsDBNull(reader.GetOrdinal("Issuer"))) entity.Issuer = reader.GetString(reader.GetOrdinal("Issuer"));
				if (!reader.IsDBNull(reader.GetOrdinal("ValidFrom"))) entity.ValidFrom = reader.GetDateTime(reader.GetOrdinal("ValidFrom"));
				if (!reader.IsDBNull(reader.GetOrdinal("ValidTo"))) entity.ValidTo = reader.GetDateTime(reader.GetOrdinal("ValidTo"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KDT_THUPHI_DOANHNGHIEP> collection, long id)
        {
            foreach (T_KDT_THUPHI_DOANHNGHIEP item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_THUPHI_DOANHNGHIEP VALUES(@MaHQ, @NgayTiepNhan, @TrangThaiXuLy, @MaDoanhNghiep, @TenDoanhNghiep, @DiaChi, @NguoiLienHe, @Email, @SoDienThoai, @Serial, @CertString, @Subject, @Issuer, @ValidFrom, @ValidTo, @GuidStr)";
            string update = "UPDATE T_KDT_THUPHI_DOANHNGHIEP SET MaHQ = @MaHQ, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, DiaChi = @DiaChi, NguoiLienHe = @NguoiLienHe, Email = @Email, SoDienThoai = @SoDienThoai, Serial = @Serial, CertString = @CertString, Subject = @Subject, Issuer = @Issuer, ValidFrom = @ValidFrom, ValidTo = @ValidTo, GuidStr = @GuidStr WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_DOANHNGHIEP WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiLienHe", SqlDbType.NVarChar, "NguoiLienHe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Email", SqlDbType.NVarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoai", SqlDbType.NVarChar, "SoDienThoai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Serial", SqlDbType.NVarChar, "Serial", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CertString", SqlDbType.NVarChar, "CertString", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Subject", SqlDbType.NVarChar, "Subject", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Issuer", SqlDbType.NVarChar, "Issuer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ValidFrom", SqlDbType.DateTime, "ValidFrom", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ValidTo", SqlDbType.DateTime, "ValidTo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiLienHe", SqlDbType.NVarChar, "NguoiLienHe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Email", SqlDbType.NVarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoai", SqlDbType.NVarChar, "SoDienThoai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Serial", SqlDbType.NVarChar, "Serial", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CertString", SqlDbType.NVarChar, "CertString", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Subject", SqlDbType.NVarChar, "Subject", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Issuer", SqlDbType.NVarChar, "Issuer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ValidFrom", SqlDbType.DateTime, "ValidFrom", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ValidTo", SqlDbType.DateTime, "ValidTo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_THUPHI_DOANHNGHIEP VALUES(@MaHQ, @NgayTiepNhan, @TrangThaiXuLy, @MaDoanhNghiep, @TenDoanhNghiep, @DiaChi, @NguoiLienHe, @Email, @SoDienThoai, @Serial, @CertString, @Subject, @Issuer, @ValidFrom, @ValidTo, @GuidStr)";
            string update = "UPDATE T_KDT_THUPHI_DOANHNGHIEP SET MaHQ = @MaHQ, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, DiaChi = @DiaChi, NguoiLienHe = @NguoiLienHe, Email = @Email, SoDienThoai = @SoDienThoai, Serial = @Serial, CertString = @CertString, Subject = @Subject, Issuer = @Issuer, ValidFrom = @ValidFrom, ValidTo = @ValidTo, GuidStr = @GuidStr WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_DOANHNGHIEP WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiLienHe", SqlDbType.NVarChar, "NguoiLienHe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Email", SqlDbType.NVarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoai", SqlDbType.NVarChar, "SoDienThoai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Serial", SqlDbType.NVarChar, "Serial", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CertString", SqlDbType.NVarChar, "CertString", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Subject", SqlDbType.NVarChar, "Subject", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Issuer", SqlDbType.NVarChar, "Issuer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ValidFrom", SqlDbType.DateTime, "ValidFrom", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ValidTo", SqlDbType.DateTime, "ValidTo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi", SqlDbType.NVarChar, "DiaChi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiLienHe", SqlDbType.NVarChar, "NguoiLienHe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Email", SqlDbType.NVarChar, "Email", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoai", SqlDbType.NVarChar, "SoDienThoai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Serial", SqlDbType.NVarChar, "Serial", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CertString", SqlDbType.NVarChar, "CertString", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Subject", SqlDbType.NVarChar, "Subject", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Issuer", SqlDbType.NVarChar, "Issuer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ValidFrom", SqlDbType.DateTime, "ValidFrom", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ValidTo", SqlDbType.DateTime, "ValidTo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_THUPHI_DOANHNGHIEP Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_THUPHI_DOANHNGHIEP> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_THUPHI_DOANHNGHIEP> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_THUPHI_DOANHNGHIEP> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_THUPHI_DOANHNGHIEP(string maHQ, DateTime ngayTiepNhan, int trangThaiXuLy, string maDoanhNghiep, string tenDoanhNghiep, string diaChi, string nguoiLienHe, string email, string soDienThoai, string serial, string certString, string subject, string issuer, DateTime validFrom, DateTime validTo, string guidStr)
		{
			T_KDT_THUPHI_DOANHNGHIEP entity = new T_KDT_THUPHI_DOANHNGHIEP();	
			entity.MaHQ = maHQ;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.DiaChi = diaChi;
			entity.NguoiLienHe = nguoiLienHe;
			entity.Email = email;
			entity.SoDienThoai = soDienThoai;
			entity.Serial = serial;
			entity.CertString = certString;
			entity.Subject = subject;
			entity.Issuer = issuer;
			entity.ValidFrom = validFrom;
			entity.ValidTo = validTo;
			entity.GuidStr = guidStr;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@NguoiLienHe", SqlDbType.NVarChar, NguoiLienHe);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);
			db.AddInParameter(dbCommand, "@SoDienThoai", SqlDbType.NVarChar, SoDienThoai);
			db.AddInParameter(dbCommand, "@Serial", SqlDbType.NVarChar, Serial);
			db.AddInParameter(dbCommand, "@CertString", SqlDbType.NVarChar, CertString);
			db.AddInParameter(dbCommand, "@Subject", SqlDbType.NVarChar, Subject);
			db.AddInParameter(dbCommand, "@Issuer", SqlDbType.NVarChar, Issuer);
			db.AddInParameter(dbCommand, "@ValidFrom", SqlDbType.DateTime, ValidFrom.Year <= 1753 ? DBNull.Value : (object) ValidFrom);
			db.AddInParameter(dbCommand, "@ValidTo", SqlDbType.DateTime, ValidTo.Year <= 1753 ? DBNull.Value : (object) ValidTo);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_THUPHI_DOANHNGHIEP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_DOANHNGHIEP item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_THUPHI_DOANHNGHIEP(long id, string maHQ, DateTime ngayTiepNhan, int trangThaiXuLy, string maDoanhNghiep, string tenDoanhNghiep, string diaChi, string nguoiLienHe, string email, string soDienThoai, string serial, string certString, string subject, string issuer, DateTime validFrom, DateTime validTo, string guidStr)
		{
			T_KDT_THUPHI_DOANHNGHIEP entity = new T_KDT_THUPHI_DOANHNGHIEP();			
			entity.ID = id;
			entity.MaHQ = maHQ;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.DiaChi = diaChi;
			entity.NguoiLienHe = nguoiLienHe;
			entity.Email = email;
			entity.SoDienThoai = soDienThoai;
			entity.Serial = serial;
			entity.CertString = certString;
			entity.Subject = subject;
			entity.Issuer = issuer;
			entity.ValidFrom = validFrom;
			entity.ValidTo = validTo;
			entity.GuidStr = guidStr;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_THUPHI_DOANHNGHIEP_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@NguoiLienHe", SqlDbType.NVarChar, NguoiLienHe);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);
			db.AddInParameter(dbCommand, "@SoDienThoai", SqlDbType.NVarChar, SoDienThoai);
			db.AddInParameter(dbCommand, "@Serial", SqlDbType.NVarChar, Serial);
			db.AddInParameter(dbCommand, "@CertString", SqlDbType.NVarChar, CertString);
			db.AddInParameter(dbCommand, "@Subject", SqlDbType.NVarChar, Subject);
			db.AddInParameter(dbCommand, "@Issuer", SqlDbType.NVarChar, Issuer);
			db.AddInParameter(dbCommand, "@ValidFrom", SqlDbType.DateTime, ValidFrom.Year <= 1753 ? DBNull.Value : (object) ValidFrom);
			db.AddInParameter(dbCommand, "@ValidTo", SqlDbType.DateTime, ValidTo.Year <= 1753 ? DBNull.Value : (object) ValidTo);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_THUPHI_DOANHNGHIEP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_DOANHNGHIEP item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_THUPHI_DOANHNGHIEP(long id, string maHQ, DateTime ngayTiepNhan, int trangThaiXuLy, string maDoanhNghiep, string tenDoanhNghiep, string diaChi, string nguoiLienHe, string email, string soDienThoai, string serial, string certString, string subject, string issuer, DateTime validFrom, DateTime validTo, string guidStr)
		{
			T_KDT_THUPHI_DOANHNGHIEP entity = new T_KDT_THUPHI_DOANHNGHIEP();			
			entity.ID = id;
			entity.MaHQ = maHQ;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.DiaChi = diaChi;
			entity.NguoiLienHe = nguoiLienHe;
			entity.Email = email;
			entity.SoDienThoai = soDienThoai;
			entity.Serial = serial;
			entity.CertString = certString;
			entity.Subject = subject;
			entity.Issuer = issuer;
			entity.ValidFrom = validFrom;
			entity.ValidTo = validTo;
			entity.GuidStr = guidStr;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, DiaChi);
			db.AddInParameter(dbCommand, "@NguoiLienHe", SqlDbType.NVarChar, NguoiLienHe);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);
			db.AddInParameter(dbCommand, "@SoDienThoai", SqlDbType.NVarChar, SoDienThoai);
			db.AddInParameter(dbCommand, "@Serial", SqlDbType.NVarChar, Serial);
			db.AddInParameter(dbCommand, "@CertString", SqlDbType.NVarChar, CertString);
			db.AddInParameter(dbCommand, "@Subject", SqlDbType.NVarChar, Subject);
			db.AddInParameter(dbCommand, "@Issuer", SqlDbType.NVarChar, Issuer);
			db.AddInParameter(dbCommand, "@ValidFrom", SqlDbType.DateTime, ValidFrom.Year <= 1753 ? DBNull.Value : (object) ValidFrom);
			db.AddInParameter(dbCommand, "@ValidTo", SqlDbType.DateTime, ValidTo.Year <= 1753 ? DBNull.Value : (object) ValidTo);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_THUPHI_DOANHNGHIEP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_DOANHNGHIEP item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_THUPHI_DOANHNGHIEP(long id)
		{
			T_KDT_THUPHI_DOANHNGHIEP entity = new T_KDT_THUPHI_DOANHNGHIEP();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_THUPHI_DOANHNGHIEP> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_DOANHNGHIEP item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}