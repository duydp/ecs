﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.Interface;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Company.Interface.GC.PhanBo;

namespace SOFTECH.ECS.TQDT.GC.GC.PhanBo
{
    public partial class CapNhatPhanBoToKhaiForm : BaseForm
    {
        public CapNhatPhanBoToKhaiForm()
        {
            InitializeComponent();
        }
        DataTable ToKhaiChuaPB = new DataTable();
        private void LoadPhanBo(int NamDangKy)
        {
            try
            {
                DataTable dtPhanBo = PhanBoToKhaiNhap.ToKhaiChuaPhanBo(NamDangKy);
                dgList.Refetch();
                dgList.DataSource = dtPhanBo;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                dgList.Refresh();
            }
        }
        private void btnCapNhatPB_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXRow[] listCheck = dgList.GetCheckedRows();
                foreach (GridEXRow item in listCheck)
                {
                    DataRowView dr = (DataRowView)item.DataRow;
                    //dr.Row.Table.Columns.Add("NgayHangVeKho");
                    DateTime NgayHangVeKho = (DateTime)dgList.GetRow(item.Position).Cells["NgayHangVeKho"].Value;
                    long ID = Convert.ToInt64(dr["ID"]);
                    if (ID == 0)
                    {
                        ToKhaiChuyenTiep TKCT = ToKhaiChuyenTiep.Load(Convert.ToInt64(dr["IDTKCT"]));
                        TKCT.LoadHCTCollection();
                        TKCT.TransferToNPLTon(NgayHangVeKho);
                    }
                    else
                    {
                        ToKhaiMauDich TKMD = new ToKhaiMauDich();
                        TKMD.ID = ID;
                        TKMD.Load();
                        TKMD.LoadHMDCollection();
                        TKMD.TransgferDataToNPLTonThucTe(NgayHangVeKho);
                    }
                }
                ShowMessage("Cập nhật thành công ", false);
            }
            catch (System.Exception ex)
            {
                
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            uiButton1_Click(null, null);
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadPhanBo(System.Convert.ToInt16(txtNamDangKy.Value));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void CapNhatPhanBoToKhaiForm_Load(object sender, EventArgs e)
        {
            try
            {
                txtNamDangKy.Value = DateTime.Now.Year;
                clcDenNgay.Value = DateTime.Now;
                clcTuNgay.Value = DateTime.Today.AddDays(-30);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Bảng kê tờ khai_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Yes || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgList_CellValueChanged(object sender, ColumnActionEventArgs e)
        {

        }

        private void dgList_CellEdited(object sender, ColumnActionEventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CapNhatNgayHangVeKhoForm f = new CapNhatNgayHangVeKhoForm();
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnSearchTK_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["SoToKhaiVNACCS"], ConditionOperator.Contains, txtSoTK.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchDate_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["NgayDangKy"], ConditionOperator.Between, clcTuNgay.Value.ToString("dd/MM/yyyy"), clcDenNgay.Value.ToString("dd/MM/yyyy"));
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }


    }
}
