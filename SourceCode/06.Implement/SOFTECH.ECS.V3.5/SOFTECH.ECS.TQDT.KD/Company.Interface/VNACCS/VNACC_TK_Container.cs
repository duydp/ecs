﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
#elif GC_V4
using Company.GC.BLL.KDT.SXXK;
#elif KD_V4
using Company.KD.BLL.KDT.SXXK;
#endif
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class VNACC_TK_Container : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = null;
        public KDT_VNACC_TK_SoContainer SoCont = new KDT_VNACC_TK_SoContainer();
        DataTable dt = new DataTable();
        public KDT_VNACC_TK_Container Cont = null;

        public KDT_ContainerDangKy ContDK = new KDT_ContainerDangKy();
        private FeedBackContent feedbackContent = null;
        private Container_VNACCS feedbackContKVGS = null;
        private string msgInfor = string.Empty;
        bool isAdd = true;
        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        public String Caption;
        public bool IsChange;
        public VNACC_TK_Container()
        {
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void LoadData()
        {
            
            try
            {
                Cont = TKMD.ContainerTKMD;
                SetContainer();
                BindData();
                //dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(Cont.SoContainerCollection);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                //throw;
            }
            

        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = Cont.SoContainerCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetContainer()
        {
            try
            {
                Cont.MaDiaDiem1 = ctrMaDiaDiem1.Code;
                Cont.MaDiaDiem2 = ctrMaDiaDiem2.Code;
                Cont.MaDiaDiem3 = ctrMaDiaDiem3.Code;
                Cont.MaDiaDiem4 = ctrMaDiaDiem4.Code;
                Cont.MaDiaDiem5 = ctrMaDiaDiem5.Code;
                Cont.TenDiaDiem = txtTenDiaDiem.Text;
                Cont.DiaChiDiaDiem = txtDiaChiDiaDiem.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void SetContainer()
        {
            try
            {
                ctrMaDiaDiem1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaDiaDiem1.Code = Cont.MaDiaDiem1;
                ctrMaDiaDiem1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                ctrMaDiaDiem2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaDiaDiem2.Code = Cont.MaDiaDiem2;
                ctrMaDiaDiem2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                ctrMaDiaDiem3.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaDiaDiem3.Code = Cont.MaDiaDiem3;
                ctrMaDiaDiem3.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                ctrMaDiaDiem4.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaDiaDiem4.Code = Cont.MaDiaDiem4;
                ctrMaDiaDiem4.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                ctrMaDiaDiem5.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaDiaDiem5.Code = Cont.MaDiaDiem5;
                ctrMaDiaDiem5.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtTenDiaDiem.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenDiaDiem.Text = Cont.TenDiaDiem;
                txtTenDiaDiem.TextChanged += new EventHandler(txt_TextChanged);

                txtDiaChiDiaDiem.TextChanged -= new EventHandler(txt_TextChanged);
                txtDiaChiDiaDiem.Text = Cont.DiaChiDiaDiem;
                txtDiaChiDiaDiem.TextChanged += new EventHandler(txt_TextChanged);

                //ctrMaDiaDiem1.Code = Cont.MaDiaDiem1;
                //ctrMaDiaDiem2.Code = Cont.MaDiaDiem2;
                //ctrMaDiaDiem3.Code = Cont.MaDiaDiem3;
                //ctrMaDiaDiem4.Code = Cont.MaDiaDiem4;
                //ctrMaDiaDiem5.Code = Cont.MaDiaDiem5;
                //txtTenDiaDiem.Text = Cont.TenDiaDiem;
                //txtDiaChiDiaDiem.Text = Cont.DiaChiDiaDiem;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_TK_Container_Load(object sender, EventArgs e)
        {
            try
            {
                LoadData();
                Caption = this.Text;
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                {
                    btnGhi.Enabled = false;
                    btnXoa.Enabled = false;
                }
                List<KDT_ContainerDangKy> ContainerCollection = KDT_ContainerDangKy.SelectCollectionDynamic("TKMD_ID=" + TKMD.ID, "");
                if (ContainerCollection.Count>=1)
                {
                    ContDK = ContainerCollection[0];
                    ContDK.ListCont = KDT_ContainerBS.SelectCollectionDynamic("Master_id = " + ContDK.ID, "");
                }
                if (ContDK.ID == 0)
                {
                    ContDK = new KDT_ContainerDangKy();
                    ContDK.MaHQ = TKMD.CoQuanHaiQuan;
                    ContDK.TKMD_ID = TKMD.ID;
                    ContDK.MaDoanhNghiep = TKMD.MaDonVi;
                    ContDK.GuidStr = Guid.NewGuid().ToString();
                    ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_CO_PHAN_HOI_QUA_KVGS;
                    foreach (KDT_VNACC_TK_SoContainer item in TKMD.ContainerTKMD.SoContainerCollection)
                    {
                        KDT_ContainerBS cont = new KDT_ContainerBS();
                        cont.SoContainer = item.SoContainer;
                        ContDK.ListCont.Add(cont);
                    }
                    ContDK.InsertUpdateFull();
                }
                else
                {
                    //ContDK = KDT_ContainerDangKy.LoadFull(ContDK.ID);
                }
                SetCommandStatus();
                if (TKMD.MaPhuongThucVT != "2")
                {
                    this.Text = "Lấy phản hồi danh sách hàng hóa đủ điều kiện qua KVGS";
                    //ctrMaDiaDiem1.Enabled = ctrMaDiaDiem2.Enabled = ctrMaDiaDiem3.Enabled = ctrMaDiaDiem4.Enabled = ctrMaDiaDiem5.Enabled = false;
                    //txtTenDiaDiem.Enabled = txtDiaChiDiaDiem.Enabled = txtSoContainer.Enabled = false;
                    //btnGhi.Enabled = btnXoa.Enabled = false;
                    cmdSend.Text = cmdSend1.Text = "Khai báo";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        bool isAddNew = true;
        private bool ValidateFormContainer(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(ctrMaDiaDiem1, errorProvider, " Mã địa điểm xếp hàng lên xe chở hàng ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiDiaDiem, errorProvider, " Địa chỉ địa điểm xếp hàng ", isOnlyWarning);
                if (TKMD.MaPhuongThucVT == "2")
                {
                    isValid &= ValidateControl.ValidateNull(txtSoContainer, errorProvider, " Số Container ", isOnlyWarning);   
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {

            try
            {
                if (!ValidateFormContainer(false))
                    return;
                if (Cont == null)
                {
                    Cont = new KDT_VNACC_TK_Container();
                    isAddNew = true;
                }
                GetContainer();
                GetSoContainer();
                TKMD.ContainerTKMD = Cont;
                BindData();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private void GetSoContainer()
        {
            try
            {
                int sott = 1;
                bool isExits = false;
                //SoCont.SoTT = sott;
                //SoCont.SoContainer = txtSoContainer.Text.ToString().Trim();
                foreach (KDT_VNACC_TK_SoContainer item in Cont.SoContainerCollection)
                {
                    if (item.SoContainer == txtSoContainer.Text.ToString().Trim())
                    {
                        errorProvider.SetError(txtSoContainer, "Số Container : ''" + txtSoContainer.Text.ToString() + "'' này đã có trong danh sách phía dưới.");
                        isExits = true;
                        return;     
                    }                  
                }
                if(!isExits)
                    SoCont.SoContainer = txtSoContainer.Text.ToString().Trim();
                if (isAddNew)
                    Cont.SoContainerCollection.Add(SoCont);
                foreach (KDT_VNACC_TK_SoContainer item in  Cont.SoContainerCollection)
                {
                    item.SoTT = sott;
                    sott++;
                }
                isAddNew = true;
                SoCont = new KDT_VNACC_TK_SoContainer();

                txtSoContainer.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoContainer.Text = String.Empty;
                txtSoContainer.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoContainer.Text = String.Empty;
                //Cont.SoContainerCollection.Clear();
                //if (dt.Rows.Count == 0) return;
                //foreach (DataRow i in dt.Rows)
                //{
                //    KDT_VNACC_TK_SoContainer SoCont = new KDT_VNACC_TK_SoContainer();
                //    if (i["ID"].ToString().Length != 0)
                //    {
                //        SoCont.ID = Convert.ToInt32(i["ID"].ToString());
                //        SoCont.Master_id = Convert.ToInt32(i["Master_ID"].ToString());
                //    }
                //    SoCont.SoTT = sott;
                //    SoCont.SoContainer = i["SoContainer"].ToString();
                //    Cont.SoContainerCollection.Add(SoCont);
                //    sott++;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_TK_SoContainer> ItemColl = new List<KDT_VNACC_TK_SoContainer>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa Container này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_TK_SoContainer)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_TK_SoContainer item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        Cont.SoContainerCollection.Remove(item);
                    }

                }
                int sott = 1;
                foreach (KDT_VNACC_TK_SoContainer item in Cont.SoContainerCollection)
                {
                    item.SoTT = sott;
                    sott++;
                }
                this.SetChange(true);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
            //try
            //{
            //    int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
            //    if (ID > 0)
            //    {
            //        if (ShowMessage("Bạn có muốn xóa số Container này không?", true) == "Yes")
            //        {
            //            KDT_VNACC_TK_SoContainer.DeleteDynamic("ID=" + ID);
            //            grList.CurrentRow.Delete();
            //        }
            //    }
            //    else
            //        grList.CurrentRow.Delete();
            //    grList.Refetch();

            //    this.Cursor = Cursors.Default;
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    
            //    this.Cursor = Cursors.Default;
            //}
        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void ctrMaDiaDiem1_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ctrMaDiaDiem1.Code))
                {
                    VNACC_Category_Cargo cargo = VNACC_Category_Cargo.Load(ctrMaDiaDiem1.Code);

                    txtTenDiaDiem.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenDiaDiem.Text = cargo.BondedAreaName;
                    txtTenDiaDiem.TextChanged += new EventHandler(txt_TextChanged);

                    txtDiaChiDiaDiem.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDiaChiDiaDiem.Text = cargo.Notes.ToString();
                    txtDiaChiDiaDiem.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTenDiaDiem.Text = cargo.BondedAreaName;
                    //txtDiaChiDiaDiem.Text = cargo.Notes.ToString();
                }
                else
                {
                    txtTenDiaDiem.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenDiaDiem.Text = String.Empty;
                    txtTenDiaDiem.TextChanged += new EventHandler(txt_TextChanged);

                    txtDiaChiDiaDiem.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDiaChiDiaDiem.Text = String.Empty;
                    txtDiaChiDiaDiem.TextChanged += new EventHandler(txt_TextChanged);
                    
                    //txtTenDiaDiem.Text = String.Empty;
                    //txtDiaChiDiaDiem.Text = String.Empty;
                    GetContainer();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    SoCont = new KDT_VNACC_TK_SoContainer();
                    SoCont = (KDT_VNACC_TK_SoContainer)e.Row.DataRow;

                    txtSoContainer.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoContainer.Text = SoCont.SoContainer;
                    txtSoContainer.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoContainer.Text = SoCont.SoContainer;
                    isAddNew = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdPrint":
                    Print();
                    break;
            }
        }

        private void Print()
        {
            try
            {
                if (TKMD.MaPhuongThucVT == "2")
                {
                    Company.Interface.Report.VNACCS.BangKeSoContainerQuaKVGS bangkeCont = new Company.Interface.Report.VNACCS.BangKeSoContainerQuaKVGS();
                    bangkeCont.TKMD = TKMD;
                    bangkeCont.BindReport(ContDK);
                    bangkeCont.ShowRibbonPreview();
                }
                else
                {
                    Company.Interface.Report.VNACCS.BangKeHangRoiQuaKVGS bangkeCont = new Company.Interface.Report.VNACCS.BangKeHangRoiQuaKVGS();
                    bangkeCont.TKMD = TKMD;
                    bangkeCont.BindReport(ContDK);
                    bangkeCont.ShowRibbonPreview();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = this.ContDK.ID;
                form.DeclarationIssuer = DeclarationIssuer.Container_KVGS;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void SetCommandStatus()
        {
            if (ContDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_CO_PHAN_HOI_QUA_KVGS || ContDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                this.OpenType = OpenFormType.Edit;
            }
            else if (ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_CO_PHAN_HOI_QUA_KVGS)
            {
                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (ContDK.TrangThaiXuLy == TrangThaiXuLy.DA_CO_DANH_SACH_HANG_QUA_KVGS)
            {

                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                this.OpenType = OpenFormType.View;
            }

        }
        private void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
            sendXML.master_id = ContDK.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = ContDK.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.Container_KVGS,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.Container_KVGS,
                };
                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = ContDK.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ.Trim())),
                                                  Identity = ContDK.MaHQ
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessageKVGS;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContKVGS.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContKVGS.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                    else if (feedbackContKVGS.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        void SendKVGSHandler(object sender, SendEventArgs e)
        {
            feedbackContKVGS = SingleMessage.ContainerVnaccsSendHandler(ContDK, ref msgInfor, e);
        }
        void SendMessageKVGS(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendKVGSHandler),
                sender, e);
        }
        private void Send()
        {
            #region Hỏi phải hồi KVGS
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
                sendXML.master_id = ContDK.ID;
                if (sendXML.Load())
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {


                        string returnMessage = string.Empty;
                        string reference = ContDK.GuidStr;

                        Container_VNACCS contVNACCS = new Container_VNACCS();
                        contVNACCS = ToDataTransferContainerKVGS(ContDK, TKMD, GlobalSettings.TEN_DON_VI);

                        SubjectBase subjectBase = new SubjectBase()
                        {
                            Issuer = DeclarationIssuer.Container_KVGS,
                            Reference = reference,
                            Function = DeclarationFunction.KHAI_BAO,
                            Type = DeclarationIssuer.Container_KVGS,
                        };
                        ObjectSend msgSend = new ObjectSend(
                                                    new NameBase()
                                                    {
                                                        Name = GlobalSettings.TEN_DON_VI,
                                                        Identity = ContDK.MaDoanhNghiep,
                                                        Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                                                    },
                                                      new NameBase()
                                                      {
                                                          Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(ContDK.MaHQ.Trim())),
                                                          Identity = ContDK.MaHQ
                                                      }, subjectBase, contVNACCS);
                        ContDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_CO_PHAN_HOI_QUA_KVGS;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessageKVGS;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContKVGS.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(ContDK.ID, MessageTitle.KhaiBaoBSContainer);
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            sendXML.LoaiHS = LoaiKhaiBao.ContainerKVGS;
                            sendXML.master_id = ContDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();
                            ContDK.Update();
                            if (feedbackContKVGS.Function == DeclarationFunction.CHUA_XU_LY)
                            {
                                Feedback();
                                SetCommandStatus();
                            }
                            else if (feedbackContKVGS.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                            {
                                ContDK.Update();
                                ShowMessageTQDT(msgInfor, false);
                                SetCommandStatus();
                            }
                            else if (feedbackContKVGS.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                            {
                                ShowMessageTQDT(msgInfor, false);
                            }
                            else
                            {
                                isSend = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                            }
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            #endregion
        }
        public static Container_VNACCS ToDataTransferContainerKVGS(KDT_ContainerDangKy ContDK, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep)
        {
            string reference = string.Empty;
            if (string.IsNullOrEmpty(ContDK.GuidStr))
            {
                reference = Guid.NewGuid().ToString();
            }
            else
            {
                reference = ContDK.GuidStr;
            }
            Container_VNACCS Cont = new Container_VNACCS()
            {
                Issuer = DeclarationIssuer.Container_KVGS,
                Reference = reference,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = "",
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = TKMD.SoToKhai.ToString(),
                Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime),
                DeclarationOffice = TKMD.CoQuanHaiQuan,
                NatureOfTransaction = TKMD.MaLoaiHinh,
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Identity = ContDK.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                }

            };
            Cont.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = ContDK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            return Cont;
        }

        private void VNACC_TK_Container_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Container có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.btnGhi_Click(null, null);
                }
            }
        }

    }
}
