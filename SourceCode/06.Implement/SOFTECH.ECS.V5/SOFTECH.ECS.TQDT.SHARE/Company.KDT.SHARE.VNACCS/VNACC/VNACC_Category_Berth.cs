using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_Berth 
    {
        protected static List<VNACC_Category_Berth> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_Berth> collection = new List<VNACC_Category_Berth>();
            while (reader.Read())
            {
                VNACC_Category_Berth entity = new VNACC_Category_Berth();
                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceDB"))) entity.ReferenceDB = reader.GetString(reader.GetOrdinal("ReferenceDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("Code"))) entity.Code = reader.GetString(reader.GetOrdinal("Code"));
                if (!reader.IsDBNull(reader.GetOrdinal("MoTa_HeThong"))) entity.MoTa_HeThong = reader.GetString(reader.GetOrdinal("MoTa_HeThong"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_Berth> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [ReferenceDB], [Code], [MoTa_HeThong] FROM [dbo].[t_VNACC_Category_Berth]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_Berth> SelectCollectionBy(List<VNACC_Category_Berth> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_Berth o)
            {
                return o.ReferenceDB.Contains(tableID);
            });
        }

        public static List<VNACC_Category_Berth> SelectCollectionByCountryCode(List<VNACC_Category_Berth> collections, string countryCode)
        {
            return collections.FindAll(delegate(VNACC_Category_Berth o)
            {
                return o.Code.Contains(countryCode);
            });
        }
    }
}