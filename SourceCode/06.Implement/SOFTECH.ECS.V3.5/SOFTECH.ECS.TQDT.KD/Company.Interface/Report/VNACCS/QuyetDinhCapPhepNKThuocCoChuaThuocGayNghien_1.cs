using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhCapPhepNKThuocCoChuaThuocGayNghien_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhCapPhepNKThuocCoChuaThuocGayNghien_1()
        {
            InitializeComponent();
        }
        public void BindingReport(VAGP130 vagp)
        {
            lblMaNguoiKhai.Text = vagp.SBC.GetValue().ToString().ToUpper();
            lblTenNguoiKhai.Text = vagp.SBN.GetValue().ToString().ToUpper();
            lblSoDonXinCapPhep.Text = vagp.ONO.GetValue().ToString().ToUpper();
            lblChucNangChungTu.Text = vagp.FNC.GetValue().ToString().ToUpper();
            lblLoaiGP.Text = vagp.PRT.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vagp.DOS.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vagp.DOS.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            lblMaDonViCapPhep.Text = vagp.OAC.GetValue().ToString().ToUpper();
            lblTenDonViCapPhep.Text = vagp.OAN.GetValue().ToString().ToUpper();
            lblKetQuaXuLy.Text = vagp.PCT.GetValue().ToString().ToUpper();
            lblSoGiayPhep.Text = vagp.PNO.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vagp.PDA.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayGiayPhep.Text = Convert.ToDateTime(vagp.PDA.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayGiayPhep.Text = "";
            }
            if (Convert.ToDateTime(vagp.EFD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHieuLucGP.Text = Convert.ToDateTime(vagp.EFD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHieuLucGP.Text = "";
            }
            if (Convert.ToDateTime(vagp.EXP.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHetHieuLucGP.Text = Convert.ToDateTime(vagp.EXP.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHetHieuLucGP.Text = "";
            }
            lblMaDoanhNghiepNK.Text = vagp.IMC.GetValue().ToString().ToUpper();
            lblTenDoanhNghiepNK.Text = vagp.IMN.GetValue().ToString().ToUpper();
            lblMaBuuChinhDNNK.Text = vagp.IPC.GetValue().ToString().ToUpper();
            lblSoNhaDNNK.Text = vagp.IAD.GetValue().ToString().ToUpper();
            lblMaQuocGiaDNNK.Text = vagp.ICC.GetValue().ToString().ToUpper();
            lblSoDienThoaiDNNK.Text = vagp.IPN.GetValue().ToString().ToUpper();
            lblSoFaxDNNK.Text = vagp.IFX.GetValue().ToString().ToUpper();
            lblEmailDNNK.Text = vagp.IEM.GetValue().ToString().ToUpper();
            lblMaCuaKhauNhapDuKien.Text = vagp.EPC.GetValue().ToString().ToUpper();
            lblTenCuaKhauNhapDuKien.Text = vagp.EPN.GetValue().ToString().ToUpper();
            lblGhiChu.Text = vagp.RMK.GetValue().ToString().ToUpper();
            lblDaiDienDVCapPhep.Text = vagp.AOP.GetValue().ToString().ToUpper();

            int HangHoa = vagp.HangHoa.Count;
            if (HangHoa <= 5)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 5, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            

        }



    }
}
