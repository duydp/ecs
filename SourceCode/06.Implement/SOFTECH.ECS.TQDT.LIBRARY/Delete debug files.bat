del App.config
del *.pdb
del *.manifest
del *.lic
del *.vshost.*
del *.application
del *.log
del error*.txt
del *.old
del *.locked
del TempFolder\*.*
rmdir TempFolder
del Errors\*.*
rmdir Errors

del Temp\*.*
rmdir Temp

del Data\Update\LogUpdate\*.*
rmdir Data\Update\LogUpdate

del MiniSQL\Templates\*.*
rmdir MiniSQL\Templates
del MiniSQL\*.*
rmdir MiniSQL

del DevExpress.Data.v9.2.xml
del DevExpress.Utils.v9.2.xml
del DevExpress.XtraBars.v9.2.xml
del DevExpress.XtraCharts.v9.2.xml
del DevExpress.XtraCharts.v9.2.UI.xml
del DevExpress.XtraEditors.v9.2.xml
del DevExpress.XtraGrid.v9.2.xml
del DevExpress.XtraLayout.v9.2.xml
del DevExpress.XtraPrinting.v9.2.xml
del DevExpress.XtraReports.v9.2.xml
del DevExpress.XtraTreeList.v9.2.xml

del DevExpress.Data.v10.2.xml
del DevExpress.RichEdit.v10.2.Core.xml
del DevExpress.Utils.v10.2.xml
del DevExpress.XtraBars.v10.2.xml
del DevExpress.XtraEditors.v10.2.xml
del DevExpress.XtraGrid.v10.2.xml
del DevExpress.XtraLayout.v10.2.xml
del DevExpress.XtraPrinting.v10.2.xml
del DevExpress.XtraRichEdit.v10.2.xml
del DevExpress.XtraTreeList.v10.2.xml
del DevExpress.XtraVerticalGrid.v10.2.xml
del DevExpress.XtraNavBar.v10.2.xml
del DevExpress.XtraCharts.v10.2.xml
del DevExpress.XtraCharts.v10.2.UI.xml
del DevExpress.XtraPivotGrid.v10.2.xml
del DevExpress.PivotGrid.v10.2.Core.xml
del DevExpress.Xpo.v10.2.xml

del SQLFolderBrowser.xml
del TyGia.xml
del MiniSqlQuery.Core.xml
del ApplicationSettings.xsd