﻿//Lypt created
//Date 07/01/2010
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.GC
{
    public partial class ToKhaiGCCTA4Nhap : DevExpress.XtraReports.UI.XtraReport
    {
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //public string PTVT_Name = "";
        //public string PTTT_Name = "";
        //public string DKGH = "";
        //public string DongTienTT = "";
        //public string NuocXK = "";

        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewGCCTTQForm report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public ToKhaiGCCTA4Nhap()
        {
            InitializeComponent();
        }

        //public string GetChiCucHQCK()
        //{
        //    string chiCucHQCK = "";
        //    if (this.TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
        //    {
        //        chiCucHQCK = this.TKCT. + "-" + CuaKhau.GetName(this.TKCT.CuaKhau_ID);
        //    }
        //    else
        //    {
        //        chiCucHQCK = this.TKCT.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKCT.CuaKhau_ID);
        //    }
        //    return chiCucHQCK;
        //}

        public void BindReport()
        {

            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;

            string MaLH = TKCT.MaLoaiHinh.Substring(0,1);
            string MaLH2 = TKCT.MaLoaiHinh.Substring(4, 1);

            if (MaLH == "N" || MaLH2 == "N")
            {
                lblLoaiHinhKhai.Text = "Nhập khẩu";
                //1. Nguoi nhap khau
                try
                {
                    // lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontNguoiXuatKhau")));
                    lblNguoiXK.Text = TKCT.TenKH;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
                    return;
                }

                //2. Nguoi xuat khau
                try
                {
                    // lblNguoiXK.Text = (this.TKCT.MaDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI).ToUpper();
                    lblNguoiNK.Text = (GlobalSettings.TEN_DON_VI).ToUpper();
                    lblMaDoanhNghiep.Text = this.TKCT.MaDoanhNghiep;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
                    return;
                }
               
            }
            else if (MaLH == "X" || MaLH2 =="X")
            {

                try
                {
                    // lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontNguoiXuatKhau")));
                    lblNguoiNK.Text = TKCT.TenKH;
                    //lblMaDoanhNghiep.Text = this.TKCT.MaKhachHang;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
                    return;
                }

                //2. Nguoi xuat khau
                try
                {
                    // lblNguoiXK.Text = (this.TKCT.MaDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI).ToUpper();
                    lblNguoiXK.Text = (GlobalSettings.TEN_DON_VI).ToUpper();
                    lblMaNguoiXuat.Text = this.TKCT.MaDoanhNghiep;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
                    return;
                }
                lblLoaiHinhKhai.Text = "Xuất khẩu";
                lblHD.Text = "Hợp đồng nhận";
                lblNgayHD.Text = "Ngày hợp đồng nhận";
                lblNgayHHHD.Text = "Ngày hết hạn hợp đồng nhận";

            }
            else
                lblLoaiHinhKhai.Text = "";
            lblMienThueNK.Visible = MienThue1;
            lblMienThueGTGT.Visible = MienThue2;

           
            //lblMienThueNK.Text = GlobalSettings.TieuDeInDinhMuc;
            //lblMienThueGTGT.Text = GlobalSettings.MienThueGTGT;

            DateTime minDate = new DateTime(1900, 1, 1);
            //if (this.TKMD.SoTiepNhan != 0)
            //    this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKMD.SoTiepNhan;


           lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;
          //  lblChiCucHQCK.Text = GetChiCucHQCK();

            //So tham chieu
            if (this.TKCT.ID > 0)
                lblThamChieu.Text = this.TKCT.ID + "";
            else
                lblThamChieu.Text = "";

            //Ngay gui
            if (this.TKCT.NgayTiepNhan > minDate)
                lblNgayGui.Text = this.TKCT.NgayTiepNhan.ToString("dd/MM/yyyy");
            else
                lblNgayGui.Text = "";

            //So to khai
            if (this.TKCT.SoToKhai > 0)
                lblToKhai.Text = this.TKCT.SoToKhai + "";
            else
                lblToKhai.Text = "";

            //Ngay dang ký
            if (this.TKCT.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKCT.NgayDangKy.ToString("dd/MM/yyyy");
            else
                lblNgayDangKy.Text = "";

            //1. Nguoi xuat khau
            //try
            //{
            //   // lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontNguoiXuatKhau")));
            //    lblNguoiXK.Text = TKCT.TenKH;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Có lỗi ở file config. Chi tiết lỗi: " + ex.Message, "Thông báo");
            //    return;
            //}

            ////2. Nguoi nhap khau
            //if (GlobalSettings.WordWrap)
            //    lblNguoiNK.Text = (this.TKCT.NguoiChiDinhDV + "\r\n" + GlobalSettings.DIA_CHI).ToUpper();
            //else
            //    lblNguoiNK.Text = (this.TKCT.NguoiChiDinhDV + ". " + GlobalSettings.DIA_CHI).ToUpper(); //lblMaDoanhNghiep1

            //lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKCT.NguoiChiDinhDV);// +"\r\n" + GlobalSettings.DIA_CHI.ToUpper();


        //    //3. Nguoi uy thac
        //    lblNguoiUyThac.Text = TKMD.TenDonViUT;

        //    //4. Dai ly lam thu tuc
        //    //lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
        //    //lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;

            //5 Loai hinh
            string stlh = Company.GC.BLL.DuLieuChuan.LoaiHinhMauDich.GetName(TKCT.MaLoaiHinh);
            lblLoaiHinh.Text = TKCT.MaLoaiHinh +"-"+ stlh;
            //lblLoaiHinhKhai.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

        //    //6. Hoa don  thuong mai
        //    lblHoaDonThuongMai.Text = "" + this.TKMD.SoHoaDonThuongMai;

        //    if (this.TKMD.NgayHoaDonThuongMai > minDate)
        //        lblNgayHoaDon.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
        //    else
        //        lblNgayHoaDon.Text = "";

        //    //7. Giay phep
        //    if (TKMD.SoGiayPhep != "")
        //        lblGiayPhep.Text = "" + this.TKMD.SoGiayPhep;
        //    else
        //        lblGiayPhep.Text = "";

        //    if (this.TKMD.NgayGiayPhep > minDate)
        //        lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
        //    else
        //        lblNgayGiayPhep.Text = "";

        //    if (this.TKMD.NgayHetHanGiayPhep > minDate)
        //        lblNgayHetHanGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
        //    else
        //        lblNgayHetHanGiayPhep.Text = "";

            //8. Hop dong
            if (this.TKCT.SoHopDongDV.Length > 36)
                lblHopDong.Font = new Font("Times New Roman", 6.5f);

            lblHopDong.Text = "" + this.TKCT.SoHopDongDV;

            if (this.TKCT.NgayHDDV > minDate)
                lblNgayHopDong.Text = this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
            else
                lblNgayHopDong.Text = " ";

            if (this.TKCT.NgayHetHanHDDV > minDate)
                lblNgayHetHanHopDong.Text = "" + this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
            else
                lblNgayHetHanHopDong.Text = " ";

        //    //9. Van tai don
        //    lblVanTaiDon.Text = this.TKMD.SoVanDon;
        //    if (this.TKMD.NgayVanDon > minDate)
        //        lblNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
        //    else
        //        lblNgayVanTaiDon.Text = "";

        //    //10. Cang xep hang
        //    lblCangXepHang.Text = this.TKMD.DiaDiemXepHang;

        //    //11. Cang do hang
        //    lblMaCangdoHang.Text = this.TKMD.CuaKhau_ID;
        //    lblCangDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);

        //    //12. Phuong tien van tai
        //    lblTenPTVT.Text = PhuongThucVanTai.getName(TKMD.PTVT_ID);
        //    lblPhuongTienVanTai.Text = " " + this.TKMD.SoHieuPTVT;
        //    //if (this.TKMD.NgayDenPTVT > minDate)
        //    //     lblNgayDenPTVT.Text = "Ngày đến: " + this.TKMD.NgayDenPTVT.ToString("dd/MM/yyyy");
        //    //else
        //    //    lblNgayDenPTVT.Text = "Ngày đến: ";

        //    //13. Nuox xuat khau
        //    lblNuocXK.Text = this.TKMD.NuocXK_ID;
        //    lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocXK_ID);

            //14. Dieu kien giao hang
            lblDieuKienGiaoHang.Text = this.TKCT.DKGH_ID;

            //15. Phuong thuc thanh toan
            lblPhuongThucThanhToan.Text = this.TKCT.PTTT_ID;

            //16. Dong tin thanh toan
            lblDongTienThanhToan.Text = this.TKCT.NguyenTe_ID;

            //17. Ty gia tinh thue
            lblTyGiaTinhThue.Text = this.TKCT.TyGiaVND.ToString("G10");

            //18. Phan luong & huong dan cua hai quan
            lblHuongDan.Text = TKCT.HUONGDAN;
            //if (TKCT.PhanLuong == "1")
            //    lblLyDoTK.Text = "Tờ khai được thông quan";
            //else if (TKCT.PhanLuong == "2")
            //    lblLyDoTK.Text = "Tờ khai phải xuất trình giấy tờ";
            //else if (TKCT.PhanLuong == "3")
            //    lblLyDoTK.Text = "Tờ khai phải kiểm hóa";
            //else
            //    lblLyDoTK.Text = "";

            

            //19. Chung tu hai quan truoc
            lblChungTu.Text = this.TKCT.SoHDKH;
            lblNgayChungtu.Text = this.TKCT.NgayHDKH.ToString("dd/MM/yyyy");
            lblNgayHetHanCT.Text = this.TKCT.NgayHetHanHDKH.ToString("dd/MM/yyyy");


        //    //truoc 26. Phi bao hiem
        //    string st = "";
        //    if (this.TKMD.PhiBaoHiem > 0)
        //        st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
        //    if (this.TKMD.PhiVanChuyen > 0)
        //        st += " F = " + this.TKMD.PhiVanChuyen.ToString("N2");
        //    if (this.TKMD.PhiKhac > 0)
        //        st += " Phí khác = " + this.TKMD.PhiKhac.ToString("N2");
        //    if (GlobalSettings.ChiPhiKhac == 1)
        //        lblPhiBaoHiem.Text = st;


            //31. Tong trong luong
            if (TKCT.TrongLuong > 0)
                lbltongtrongluong.Text = TKCT.TrongLuong + " kg ";
            else
                lbltongtrongluong.Text = "";
            // Tông số kiện

            lblTongKien.Text =" Tổng số kiện: " + TKCT.SoKien.ToString("N0");
            //if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
            //    lblChitietCon.Text = "danh sách container theo bảng kê đính kèm";
            //else
            //{
            //    string soHieuKien = "";
            //    Company.KDT.SHARE.QuanLyChungTu.Container objContainer = null;

            //    if (TKMD.VanTaiDon != null)
            //    {
            //        for (int cnt = 0; cnt < TKMD.VanTaiDon.ContainerCollection.Count; cnt++)
            //        {
            //            objContainer = TKMD.VanTaiDon.ContainerCollection[cnt];

            //            soHieuKien += objContainer.SoHieu + "/" + objContainer.Seal_No;

            //            if (cnt < TKMD.VanTaiDon.ContainerCollection.Count - 1)
            //            {
            //                soHieuKien += "; ";
            //            }
            //        }
            //    }
            //    lblChitietCon.Text = soHieuKien;
            //}
            //if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
            //    lblChitietCon.Text = "chi tiết phụ lục đính kèm";
            //else
            //{
            //    //DATLMQ bo sung So hieu kien, cont 26/10/2010
            //    if (TKMD.VanTaiDon != null)
            //    {
            //        if (TKMD.VanTaiDon.ContainerCollection.Count != 0)
            //        {
            //            for (int i = 0; i < TKMD.VanTaiDon.ContainerCollection.Count; i++)
            //            {
            //                if (i == TKMD.VanTaiDon.ContainerCollection.Count - 1)
            //                    lblChitietCon.Text += TKMD.VanTaiDon.ContainerCollection[i].SoHieu + "/" + TKMD.VanTaiDon.ContainerCollection[i].Seal_No;
            //                else
            //                    lblChitietCon.Text += TKMD.VanTaiDon.ContainerCollection[i].SoHieu + "/" + TKMD.VanTaiDon.ContainerCollection[i].Seal_No + ";";
            //            }
            //        }
            //        else
            //        {
            //            lblChitietCon.Text = "";
            //        }
            //    }
            //    else
            //    {
            //        lblChitietCon.Text = "";
            //    }
            //}
            //lblChitietCon.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            #region CONTAINER
            //Tong so CONTAINER
            //string tsContainer = "";
            //string cont20 = "", cont40 = "", soKien = "";
            //if (TKCT.SoContainer20 > 0)
            //{
            //    cont20 = "Cont20: " + Convert.ToInt32(TKMD.SoContainer20);

            //    tsContainer += cont20 + "; ";
            //}
            //else
            //    cont20 = "";

            //if (TKMD.SoContainer40 > 0)
            //{
            //    cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

            //    tsContainer += cont40 + "; ";
            //}
            //else
            //    cont40 = "";

            //if (TKMD.SoKien > 0)
            //{
            //    soKien = "Tổng số kiện: " + TKMD.SoKien;

            //    tsContainer += soKien;
            //}
            //else
            //    soKien = "";

            //TongSoContainer.Text = tsContainer.Length > 0 ? "Tổng số container: " + tsContainer : "Tổng số container:";
            #endregion
            
            if (this.TKCT.HCTCollection.Count <= 3)
            {
                #region Dong hang 1
                if (this.TKCT.HCTCollection.Count >= 1)
                {
                    
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[0];
                    if (!inMaHang)
                        TenHang1.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaHang.Trim().Length > 0)
                            TenHang1.Text = hmd.TenHang + "/" + hmd.MaHang;
                        else
                            TenHang1.Text = hmd.TenHang;
                    }
                    TenHang1.WordWrap = true;
                    try
                    {
                        TenHang1.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }
                    MaHS1.Text = hmd.MaHS;
                    XuatXu1.Text = hmd.ID_NuocXX;
                    Luong1.Text = hmd.SoLuong.ToString("G15");
                    DVT1.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT1.Text = hmd.DonGia.ToString("G10");
                    TriGiaNT1.Text = hmd.TriGia.ToString("N2");
                    tongTienThueXNK += hmd.ThueXNK;
                    //thue XNK  
                    if (hmd.ThueSuatXNK.Equals(0))
                        TienThueXNK1.Text = "";
                    else
                    {
                        TriGiaTT1.Text = hmd.TriGiaTT.ToString("N0");
                        TienThueXNK1.Text = hmd.ThueXNK.ToString("N0");
                        ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString("N0");
                    }
                    //Thue thu khac
                    if (hmd.TyLeThuKhac.Equals(0))
                    {
                        TyLeThuKhac1.Text = "";
                    }
                    else
                    {
                        TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                    }
                    //Thue GTGT or Thue TTDB
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                       
                        TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        TriGiaTTGTGT1.Text = TriGiaTTGTGT.ToString("N0");
                        ThueSuatGTGT1.Text = hmd.ThueSuatGTGT.ToString("N0");
                        TienThueGTGT1.Text = hmd.ThueGTGT.ToString("N0");
                       
                    }
                    else if ((hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)||(hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0))
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                        TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                        ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                    }
                    if (MienThue1)
                    {
                        TriGiaTT1.Text = "";
                        ThueSuatXNK1.Text = "";
                        TienThueXNK1.Text = "";
                    }
                    if (MienThue2)
                    {

                    }
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;
                }

                #endregion
                #region Dong hang 2
                if (this.TKCT.HCTCollection.Count >= 2)
                {
                    
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[1];
                    if (!inMaHang)
                        TenHang2.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaHang.Trim().Length > 0)
                            TenHang2.Text = hmd.TenHang + "/" + hmd.MaHang;
                        else
                            TenHang2.Text = hmd.TenHang;
                    }
                    TenHang2.WordWrap = true;
                    try
                    {
                        TenHang2.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }
                    MaHS2.Text = hmd.MaHS;
                    XuatXu2.Text = hmd.ID_NuocXX;
                    Luong2.Text = hmd.SoLuong.ToString("G15");
                    DVT2.Text = DonViTinh.GetName(hmd.ID_DVT);

                    DonGiaNT2.Text = hmd.DonGia.ToString("G10");
                    TriGiaNT2.Text = hmd.TriGia.ToString("N2");

                    tongTienThueXNK += hmd.ThueXNK;
                    #region hien thi thue hang 2
                    //thue XNK
                    if (hmd.ThueSuatXNK.Equals(0))
                        TienThueXNK2.Text = "";
                    else
                    {
                        TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");
                        TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                        ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");
                    }
                    //Thue Thu khac
                    if (hmd.TyLeThuKhac.Equals(0))
                    {
                        TyLeThuKhac2.Text = "";
                    }
                    else
                    {
                        TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                    }
                    //Thue GTGT or Thue TTDB
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                        
                        TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        TriGiaTTGTGT2.Text = TriGiaTTGTGT.ToString("N0");
                        ThueSuatGTGT2.Text = hmd.ThueSuatGTGT.ToString("N0");
                        TienThueGTGT2.Text = hmd.ThueGTGT.ToString("N0");

                    }
                    else if ((hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0) || (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0))
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                       
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                        TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                        ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                    }
                    #endregion
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;
                }
                #endregion
                #region Dong hang 3
                if (this.TKCT.HCTCollection.Count == 3)
                {
                    
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[2];
                    if (!inMaHang)
                        TenHang3.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaHang.Trim().Length > 0)
                            TenHang3.Text = hmd.TenHang + "/" + hmd.MaHang;
                        else
                            TenHang3.Text = hmd.TenHang;
                    }


                    TenHang3.WordWrap = true;
                    try
                    {
                        TenHang3.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }

                    MaHS3.Text = hmd.MaHS;
                    XuatXu3.Text = hmd.ID_NuocXX;
                    Luong3.Text = hmd.SoLuong.ToString("G15");
                    DVT3.Text = DonViTinh.GetName(hmd.ID_DVT);

                    DonGiaNT3.Text = hmd.DonGia.ToString("G10");
                    TriGiaNT3.Text = hmd.TriGia.ToString("N2");
                    tongTienThueXNK += hmd.ThueXNK;
                    #region hien thi thue hang 3
                    //Thue XNK
                    if (hmd.ThueSuatXNK.Equals(0))
                        TienThueXNK3.Text = "";
                    else
                    {
                        TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");
                        TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                        ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");
                    }
                    //Thue thu khac
                    if (hmd.TyLeThuKhac.Equals(0))
                    {
                        TyLeThuKhac3.Text = "";
                    }
                    else
                    {
                        TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                    }
                    //thue GTGT or Thue TTDB
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                        
                        TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        TriGiaTTGTGT3.Text = TriGiaTTGTGT.ToString("N0");
                        ThueSuatGTGT3.Text = hmd.ThueSuatGTGT.ToString("N0");
                        TienThueGTGT3.Text = hmd.ThueGTGT.ToString("N0");

                    }
                    else if ((hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0) || (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0))
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                        TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                        ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                    }
                    #endregion
                    
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa += hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT;
                }
                #endregion
            }
            else
            {
                #region PHU LUC

                TenHang1.Text = "HÀNG HÓA NHẬP";
                TenHang2.Text = "(Chi tiết theo phụ lục đính kèm)";
                foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
                {
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);
                    tongTienThueXNK += hmd.ThueXNK;
                    tongTriGiaTT += hmd.TriGia;
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                    }
                }

                //XuatXu1.Text = Nuoc.GetName(this.TKMD.HMDCollection[0].NuocXX_ID);

                /* 
                 //Khi co phu luc thi khong hien thi :
                TriGiaNT1.Text = tongTriGiaNT.ToString("N2");

                TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
                TriGiaTTGTGT1.Text = tongTriGiaTTGTGT.ToString("N0");
                TienThueGTGT1.Text = tongThueGTGT.ToString("N0");
                TriGiaThuKhac1.Text = tongTriGiaThuKhac.ToString("N0");
                 */
                #endregion
            }

            //tongTriGiaNT += Convert.ToDecimal(this.TKCT.PhiBaoHiem + TKCT.PhiKhac + TKCT.PhiVanChuyen);

            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");

            //lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");

            //if (tongThueGTGT > 0)
            //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
            //else
            //    lblTongTienThueGTGT.Text = "";

            //if (tongTriGiaThuKhac > 0)
            //    lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            //else
            //    lblTongTriGiaThuKhac.Text = "";

            //30. Tong so tien thue & Thu khac
            lblTongThueXNKSo.Text = tongTienThueTatCa.ToString("N0");

            string s = Company.GC.BLL.Utils.VNCurrency.ToString((decimal)tongTienThueTatCa).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);

            lblTongThueXNKChu.Text = s.Replace("  ", " ");

            //if (MienThue1)
            //{
            //    TriGiaTT1.Text = "";
            //    ThueSuatXNK1.Text = "";
            //    TienThueXNK1.Text = "";
            //    TriGiaTT2.Text = "";
            //    ThueSuatXNK2.Text = "";
            //    TienThueXNK2.Text = "";
            //    TriGiaTT3.Text = "";
            //    ThueSuatXNK3.Text = "";
            //    TienThueXNK3.Text = "";
            //    lblTongThueXNK.Text = "";
            //}
            //if (MienThue2)
            //{
            //    TriGiaTTGTGT1.Text = "";
            //    ThueSuatGTGT1.Text = "";
            //    TienThueGTGT1.Text = "";
            //    TriGiaTTGTGT2.Text = "";
            //    ThueSuatGTGT2.Text = "";
            //    TienThueGTGT2.Text = "";
            //    TriGiaTTGTGT3.Text = "";
            //    ThueSuatGTGT3.Text = "";
            //    TienThueGTGT3.Text = "";
            //    lblTongTienThueGTGT.Text = "";
            //}

            //if (MienThue1 && MienThue2)
            //{
            //    lblTongThueXNKSo.Text = lblTongThueXNKChu.Text = "";
            //}

            //Neu co phu luc dinh kem -> khong hien thi gia tri thue
            if (this.TKCT.HCTCollection.Count > 3)
            {
                lblTongThueXNK.Text = lblTongTienThueGTGT.Text = lblTongTriGiaThuKhac.Text = "";
            }

            //Ngay thang nam in to khai
            if (TKCT.NgayDangKy == new DateTime(1900, 1, 1))
                lblNgayIn.Text = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
            else
                lblNgayIn.Text = "Ngày " + TKCT.NgayTiepNhan.Day + " tháng " + TKCT.NgayTiepNhan.Month + " năm " + TKCT.NgayTiepNhan.Year;

            //32. Ghi chep khac
            lblGhChepKhac.Text = "32. Ghi chép khác: " + TKCT.DeXuatKhac;
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        //public decimal TinhTongThueHMD()
        //{
        //    decimal tong = 0;
        //    foreach (HangMauDich hmd in this.TKMD.HMDCollection)
        //    {
        //        tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
        //    }
        //    return tong;
        //}
        public void setNhomHang(string tenNhomHang)
        {
            TenHang1.Text = tenNhomHang;
        }
        //private bool IsHaveTax()
        //{
        //    foreach (HangMauDich hmd in this.TKMD.HMDCollection)
        //        if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
        //    return false;
        //}
        public void ShowMienThue(bool t)
        {
            //lblMienThueGTGT.Visible = t;
            //xrTable2.Visible = !t;
            //lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = lblTongThueXNKChu.Visible = !t;//xrTablesaaa.Visible = lblTongThueXNKChu.Visible = !t;

        }
        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        public void setThongTin(XRControl cell, string thongTin)
        {
            if (cell.Name.Equals("lblGhChepKhac"))
                cell.Text = "32. Ghi chép khác: " + thongTin;
            else
                cell.Text = thongTin;
        }

        public void setDeXuatKhac(string deXuatKhac)
        {
            lblGhChepKhac.Text = "32. Ghi chép khác: " + deXuatKhac;
        }

        private void TriGiaTT1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {

        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblGhChepKhac_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
           // report.txtDeXuatKhac.Text = cell.Text;
            //report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lbltongtrongluong_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblNgayIn_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        //public void BindData()
        //{
        //    DateTime minDate = new DateTime(1900, 1, 1);            
        //    //BindData of Detail
        //    lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
        //    lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
        //    lblMaSoHH.DataBindings.Add("Text", this.DataSource, "MaHS");
        //    lblXuatXu.DataBindings.Add("Text", this.DataSource, "XuatXu");
        //    lblSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuong");
        //    lblDonViTinh.DataBindings.Add("Text", this.DataSource, "DonViTinh");
        //    lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGiaTT", "{0:N2}");
        //    lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaTT", "{0:N2}");

        //    //BindData header
        //    lblCangDoHang.Text = TKMD.TenDaiLyTTHQ.ToString();
        //    lblCangXepHang.Text = TKMD.DiaDiemXepHang.ToString();
        //    //lblChiCucHQ.Text
        //    //lblChiCucHQCK.Text
        //    lblDieuKienGiaoHang.Text = DKGH;
        //    lblDongTienThanhToan.Text = DongTienTT;
        //    lblGiayPhep.Text = TKMD.SoGiayPhep;
        //    lblHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
        //    lblHopDong.Tag = TKMD.SoHopDong;
        //    lblLoaiHinh.Text =  TKMD.LoaiVanDon;
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayDangKy.Text = TKMD.NgayDangKy.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGiayPhep.Text = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGui.Text = TKMD.NgayTiepNhan.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanGiayPhep.Text = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanHopDong.Text = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHoaDon.Text = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHopDong.Text = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayVanTaiDon.Text = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
        //    lblNguoiNK.Text = TKMD.TenChuHang;
        //    lblNguoiUyThac.Text = TKMD.TenDonViUT;
        //    lblNguoiXK.Text = TKMD.TenDonViDoiTac;
        //    lblNuocXK.Text = NuocXK;
        //    lblPhuongThucThanhToan.Text = PTTT_Name;
        //    lblPhuongTienVanTai.Text = PTVT_Name;
        //    //lblThamChieu.Text
        //    lblToKhai.Text = TKMD.SoToKhai.ToString();
        //    lblVanTaiDon.Text = TKMD.LoaiVanDon;
        //    lblTyGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
        //    lblHuongDan.Text = TKMD.HUONGDAN;
        //    string ctu = "";
        //    foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
        //    {
        //        ctu += ct.TenChungTu + ", ";
        //    }
        //    lblChungTu.Text = ctu;

        //}
    }
}
