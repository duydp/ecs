﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using System.IO;
using Company.GC.BLL.KDT.GC;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Janus.Windows.GridEX;
using ExportToExcel;
using Company.Interface.Report.GC.ThongTu38;
namespace Company.Interface.GC.PhanBo
{
    public partial class NhapXuatTonForm : BaseForm
    {
        public DateTime dateFrom;
        public DateTime dateTo;
        public string NPL;
        public string NPLTemp;
        public decimal LuongNhap = 0;
        public decimal LuongXuat = 0;
        public decimal LuongTon = 0;
        public decimal TriGiaNhap = 0;
        public decimal TriGiaXuat = 0;
        public decimal TriGiaTon = 0;
        public int ID_HopDong;
        public DataTable dtNPLNhapDetail = new DataTable();
        DataTable dtNPLXuatDetail = new DataTable();
        DataTable dtTotal = new DataTable();
        public NhapXuatTonForm()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            try
            {
                //Lấy danh sách tất cả các NPL của các tờ khai nhập trong khoảng thời gian
                ID_HopDong = Convert.ToInt32(cbHopDong.Value.ToString());
                dateFrom = new DateTime(dtpDateFrom.Value.Year, dtpDateFrom.Value.Month, dtpDateFrom.Value.Day, 00, 00, 00);
                dateTo = new DateTime(dtpDateTo.Value.Year, dtpDateTo.Value.Month, dtpDateTo.Value.Day, 23, 59, 59);
                DataTable dtNPLNhap = NPLNhapTonThucTe.SelectBy_Date(ID_HopDong,dateFrom,dateTo).Tables[0];                
                DataTable dtNPLXuat = PhanBoToKhaiNhap.SelectBy_Date(ID_HopDong,dateFrom,dateTo).Tables[0];                
                DataTable dtNPL = Company.GC.BLL.GC.NguyenPhuLieu.SelectCollectionBy_HD_ID(ID_HopDong.ToString()).Tables[0];
                dtTotal = new DataTable();

                DataColumn dtColMANPL = new DataColumn("MANPL");
                dtColMANPL.DataType = typeof(System.String);
                DataColumn dtColTENNPL = new DataColumn("TENNPL");
                dtColTENNPL.DataType = typeof(System.String);
                DataColumn dtColDVT = new DataColumn("DVT");
                dtColDVT.DataType = typeof(System.String);
                DataColumn dtColLUONGNHAP = new DataColumn("LUONGNHAP");
                dtColLUONGNHAP.DataType = typeof(System.Decimal);
                DataColumn dtColTRIGIANHAP = new DataColumn("TRIGIANHAP");
                dtColTRIGIANHAP.DataType = typeof(System.Decimal);
                DataColumn dtColLUONGXUAT = new DataColumn("LUONGXUAT");
                dtColLUONGXUAT.DataType = typeof(System.Decimal);
                DataColumn dtColTRIGIAXUAT = new DataColumn("TRIGIAXUAT");
                dtColTRIGIAXUAT.DataType = typeof(System.Decimal);
                DataColumn dtColLUONGTON = new DataColumn("LUONGTON");
                dtColLUONGTON.DataType = typeof(System.Decimal);
                DataColumn dtColTRIGIATON = new DataColumn("TRIGIATON");
                dtColTRIGIATON.DataType = typeof(System.Decimal);

                dtTotal.Columns.Add(dtColMANPL);
                dtTotal.Columns.Add(dtColTENNPL);
                dtTotal.Columns.Add(dtColDVT);
                dtTotal.Columns.Add(dtColLUONGNHAP);
                dtTotal.Columns.Add(dtColTRIGIANHAP);
                dtTotal.Columns.Add(dtColLUONGXUAT);
                dtTotal.Columns.Add(dtColTRIGIAXUAT);
                dtTotal.Columns.Add(dtColLUONGTON);
                dtTotal.Columns.Add(dtColTRIGIATON);
                //dtTotal = dtNPL.Copy();
                foreach (DataRow dr in dtNPLNhap.Rows)
                {
                    try
                    {
                        NPL = dr["MANPL"].ToString();
                        foreach (DataRow drNPL in dtNPL.Rows)
                        {
                            NPLTemp = drNPL["Ma"].ToString();
                            if (NPL == NPLTemp)
                            {
                                DataRow drTotal = dtTotal.NewRow();
                                drTotal["MANPL"] = NPL;
                                drTotal["TENNPL"] = drNPL["Ten"].ToString();
                                drTotal["DVT"] = drNPL["TenDVT"].ToString();
                                drTotal["LUONGNHAP"] = dr["LUONGNHAP"].ToString();
                                drTotal["TRIGIANHAP"] = dr["TRIGIANHAP"].ToString();
                                drTotal["LUONGXUAT"] = 0;
                                drTotal["TRIGIAXUAT"] = 0;
                                drTotal["LUONGTON"] = 0;
                                drTotal["TRIGIATON"] = 0;
                                dtTotal.Rows.Add(drTotal);
                            }
                        }
                   }
                    catch (Exception ex)
                    {
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("Lỗi xử lý lượng nhập tại MÃ NPL : " + NPL.ToString());
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
                foreach (DataRow dr in dtNPLXuat.Rows)
                {
                    try
                    {
                        NPL = dr["MANPL"].ToString();
                        foreach (DataRow drTotal in dtTotal.Rows)
                        {
                            NPLTemp = drTotal["MANPL"].ToString();
                            if (NPL == NPLTemp)
                            {
                                LuongNhap = Convert.ToDecimal(drTotal["LUONGNHAP"].ToString());
                                LuongXuat = Convert.ToDecimal(dr["LUONGXUAT"].ToString());
                                LuongTon = LuongNhap - LuongXuat;
                                TriGiaNhap = Convert.ToDecimal(drTotal["TRIGIANHAP"].ToString());
                                TriGiaXuat = Convert.ToDecimal(dr["TRIGIAXUAT"].ToString());
                                TriGiaTon = TriGiaNhap - TriGiaXuat;
                                //Gán giá trị vào  lại Table
                                drTotal["LUONGXUAT"] = LuongXuat.ToString();
                                drTotal["TRIGIAXUAT"] = TriGiaXuat.ToString();
                                drTotal["LUONGTON"] = LuongTon.ToString();
                                drTotal["TRIGIATON"] = TriGiaTon.ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("Lỗi xử lý lượng tồn tại MÃ NPL : " + NPL.ToString());
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
                //Cập nhật lại Table khi NPL xuất bị NULL
                foreach (DataRow drTotal in dtTotal.Rows)
                {
                    LuongNhap = Convert.ToDecimal(drTotal["LUONGNHAP"].ToString());
                    LuongXuat = Convert.ToDecimal(drTotal["LUONGXUAT"].ToString());
                    LuongTon = LuongNhap - LuongXuat;
                    TriGiaNhap = Convert.ToDecimal(drTotal["TRIGIANHAP"].ToString());
                    TriGiaXuat = Convert.ToDecimal(drTotal["TRIGIAXUAT"].ToString());
                    TriGiaTon = TriGiaNhap - TriGiaXuat;
                    //Gán giá trị vào  lại Table
                    drTotal["LUONGXUAT"] = LuongXuat.ToString();
                    drTotal["TRIGIAXUAT"] = TriGiaXuat.ToString();
                    drTotal["LUONGTON"] = LuongTon.ToString();
                    drTotal["TRIGIATON"] = TriGiaTon.ToString();                    
                }
                dgList.DataSource = dtTotal;
                dgList.Refetch();
                //BindDataNPLNhapDetail();
                //BindDataNPLXuatDetail();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindHopDong()
        {
            HopDong hd = new HopDong();
            hd.SoHopDong = "";
            hd.ID = 0;
            List<HopDong> collection;
            {
                string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
            }
            collection.Add(hd);
            cbHopDong.DataSource = collection;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = collection.Count - 1;
        }
        public void BindDataNPLNhapDetail()
        {
            try
            {
                ID_HopDong = Convert.ToInt32(cbHopDong.Value.ToString());
                dateFrom = new DateTime(dtpDateFrom.Value.Year, dtpDateFrom.Value.Month, dtpDateFrom.Value.Day, 00, 00, 00);
                dateTo = new DateTime(dtpDateTo.Value.Year, dtpDateTo.Value.Month, dtpDateTo.Value.Day, 23, 59, 59);
                dtNPLNhapDetail = NPLNhapTonThucTe.SelectNPLNhapBy_Date(ID_HopDong, dateFrom, dateTo).Tables[0];
                dgListNPLNhap.DataSource = dtNPLNhapDetail;
                dgListNPLNhap.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void BindDataNPLXuatDetail()
        {
            try
            {
                ID_HopDong = Convert.ToInt32(cbHopDong.Value.ToString());
                dateFrom = new DateTime(dtpDateFrom.Value.Year, dtpDateFrom.Value.Month, dtpDateFrom.Value.Day, 00, 00, 00);
                dateTo = new DateTime(dtpDateTo.Value.Year, dtpDateTo.Value.Month, dtpDateTo.Value.Day, 23, 59, 59);
                dtNPLXuatDetail = PhanBoToKhaiNhap.SelectNPLXuatBy_Date(ID_HopDong, dateFrom, dateTo).Tables[0];
                dgListNPLXuat.DataSource = dtNPLXuatDetail;
                dgListNPLXuat.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void NhapXuatTonForm_Load(object sender, EventArgs e)
        {
            BindHopDong();
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(cbHopDong.Value.ToString()))
            {
                try
                {
                    HopDong HD = new HopDong();
                    HD.ID = Convert.ToInt64(cbHopDong.Value.ToString());
                    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                    {
                        connection.Open();
                        SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                        try
                        {
                            HD.Load(transaction);
                            transaction.Commit();
                            dtpDateFrom.Value = HD.NgayKy;
                            dtpDateTo.Value = HD.NgayHetHan;
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        finally
                        {
                            connection.Close();
                        }

                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                
            }
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = tabControl1.SelectedTab.Text + "_" + cbHopDong.SelectedText + "_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close(); 
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void mnuExport_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedTab.Name)
            {
                case "tpNPLTon":
                    ExportExcel(dgList);
                    break;
                case "tpNPLNhap":
                    ExportExcel(dgListNPLNhap);
                    break;
                case "tpSPXuat":
                    ViewNPLXuat report = new ViewNPLXuat();
                    report.BindReport(dtNPLXuatDetail);
                    report.ShowPreview();
                    break;
                default:
                    break;
            }
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            switch (tabControl1.SelectedTab.Name)
            {
                case "tpNPLNhap":
                    BindDataNPLNhapDetail();
                    break;
                case "tpSPXuat":
                    BindDataNPLXuatDetail();
                    break;
                default:
                    break;
            }
        }
    }
}
