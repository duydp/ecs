﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class KDT_GC_PhuKienDangKy : ICloneable
	{
		#region Properties.
		
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public DateTime NgayPhuKien { set; get; }
		public string NguoiDuyet { set; get; }
		public string VanBanChoPhep { set; get; }
		public string GhiChu { set; get; }
		public string SoPhuKien { set; get; }
		public long HopDong_ID { set; get; }
		public long ID { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string GUIDSTR { set; get; }
		public string DeXuatKhac { set; get; }
		public string LyDoSua { set; get; }
		public int ActionStatus { set; get; }
		public string GuidReference { set; get; }
		public short NamDK { set; get; }
		public string HUONGDAN { set; get; }
		public string PhanLuong { set; get; }
		public string HuongdanPL { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_PhuKienDangKy> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_PhuKienDangKy> collection = new List<KDT_GC_PhuKienDangKy>();
			while (reader.Read())
			{
				KDT_GC_PhuKienDangKy entity = new KDT_GC_PhuKienDangKy();
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) entity.NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) entity.NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
				if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) entity.VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) entity.SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) entity.HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_PhuKienDangKy> collection, long id)
        {
            foreach (KDT_GC_PhuKienDangKy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_PhuKienDangKy VALUES(@SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @NgayPhuKien, @NguoiDuyet, @VanBanChoPhep, @GhiChu, @SoPhuKien, @HopDong_ID, @MaHaiQuan, @MaDoanhNghiep, @GUIDSTR, @DeXuatKhac, @LyDoSua, @ActionStatus, @GuidReference, @NamDK, @HUONGDAN, @PhanLuong, @HuongdanPL)";
            string update = "UPDATE t_KDT_GC_PhuKienDangKy SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, NgayPhuKien = @NgayPhuKien, NguoiDuyet = @NguoiDuyet, VanBanChoPhep = @VanBanChoPhep, GhiChu = @GhiChu, SoPhuKien = @SoPhuKien, HopDong_ID = @HopDong_ID, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, GUIDSTR = @GUIDSTR, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ActionStatus = @ActionStatus, GuidReference = @GuidReference, NamDK = @NamDK, HUONGDAN = @HUONGDAN, PhanLuong = @PhanLuong, HuongdanPL = @HuongdanPL WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhuKienDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhuKien", SqlDbType.DateTime, "NgayPhuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiDuyet", SqlDbType.NVarChar, "NguoiDuyet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@VanBanChoPhep", SqlDbType.NVarChar, "VanBanChoPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhuKien", SqlDbType.VarChar, "SoPhuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamDK", SqlDbType.SmallInt, "NamDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongdanPL", SqlDbType.NVarChar, "HuongdanPL", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhuKien", SqlDbType.DateTime, "NgayPhuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiDuyet", SqlDbType.NVarChar, "NguoiDuyet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@VanBanChoPhep", SqlDbType.NVarChar, "VanBanChoPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhuKien", SqlDbType.VarChar, "SoPhuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamDK", SqlDbType.SmallInt, "NamDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongdanPL", SqlDbType.NVarChar, "HuongdanPL", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_PhuKienDangKy VALUES(@SoTiepNhan, @NgayTiepNhan, @TrangThaiXuLy, @NgayPhuKien, @NguoiDuyet, @VanBanChoPhep, @GhiChu, @SoPhuKien, @HopDong_ID, @MaHaiQuan, @MaDoanhNghiep, @GUIDSTR, @DeXuatKhac, @LyDoSua, @ActionStatus, @GuidReference, @NamDK, @HUONGDAN, @PhanLuong, @HuongdanPL)";
            string update = "UPDATE t_KDT_GC_PhuKienDangKy SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, NgayPhuKien = @NgayPhuKien, NguoiDuyet = @NguoiDuyet, VanBanChoPhep = @VanBanChoPhep, GhiChu = @GhiChu, SoPhuKien = @SoPhuKien, HopDong_ID = @HopDong_ID, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, GUIDSTR = @GUIDSTR, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ActionStatus = @ActionStatus, GuidReference = @GuidReference, NamDK = @NamDK, HUONGDAN = @HUONGDAN, PhanLuong = @PhanLuong, HuongdanPL = @HuongdanPL WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhuKienDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhuKien", SqlDbType.DateTime, "NgayPhuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiDuyet", SqlDbType.NVarChar, "NguoiDuyet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@VanBanChoPhep", SqlDbType.NVarChar, "VanBanChoPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhuKien", SqlDbType.VarChar, "SoPhuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamDK", SqlDbType.SmallInt, "NamDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongdanPL", SqlDbType.NVarChar, "HuongdanPL", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhuKien", SqlDbType.DateTime, "NgayPhuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiDuyet", SqlDbType.NVarChar, "NguoiDuyet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@VanBanChoPhep", SqlDbType.NVarChar, "VanBanChoPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhuKien", SqlDbType.VarChar, "SoPhuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActionStatus", SqlDbType.Int, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamDK", SqlDbType.SmallInt, "NamDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongdanPL", SqlDbType.NVarChar, "HuongdanPL", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_PhuKienDangKy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_PhuKienDangKy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_PhuKienDangKy> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_PhuKienDangKy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_GC_PhuKienDangKy> SelectCollectionBy_HopDong_ID(long hopDong_ID)
		{
            IDataReader reader = SelectReaderBy_HopDong_ID(hopDong_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_SelectBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "p_KDT_GC_PhuKienDangKy_SelectBy_HopDong_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_PhuKienDangKy(long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, DateTime ngayPhuKien, string nguoiDuyet, string vanBanChoPhep, string ghiChu, string soPhuKien, long hopDong_ID, string maHaiQuan, string maDoanhNghiep, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdanPL)
		{
			KDT_GC_PhuKienDangKy entity = new KDT_GC_PhuKienDangKy();	
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayPhuKien = ngayPhuKien;
			entity.NguoiDuyet = nguoiDuyet;
			entity.VanBanChoPhep = vanBanChoPhep;
			entity.GhiChu = ghiChu;
			entity.SoPhuKien = soPhuKien;
			entity.HopDong_ID = hopDong_ID;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.HuongdanPL = huongdanPL;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, NgayPhuKien.Year <= 1753 ? DBNull.Value : (object) NgayPhuKien);
			db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, NguoiDuyet);
			db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, VanBanChoPhep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, SoPhuKien);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@SoTiepNhan");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@SoTiepNhan");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_PhuKienDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhuKienDangKy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_PhuKienDangKy(long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, DateTime ngayPhuKien, string nguoiDuyet, string vanBanChoPhep, string ghiChu, string soPhuKien, long hopDong_ID, long id, string maHaiQuan, string maDoanhNghiep, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdanPL)
		{
			KDT_GC_PhuKienDangKy entity = new KDT_GC_PhuKienDangKy();			
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayPhuKien = ngayPhuKien;
			entity.NguoiDuyet = nguoiDuyet;
			entity.VanBanChoPhep = vanBanChoPhep;
			entity.GhiChu = ghiChu;
			entity.SoPhuKien = soPhuKien;
			entity.HopDong_ID = hopDong_ID;
			entity.ID = id;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.HuongdanPL = huongdanPL;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_PhuKienDangKy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, NgayPhuKien.Year <= 1753 ? DBNull.Value : (object) NgayPhuKien);
			db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, NguoiDuyet);
			db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, VanBanChoPhep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, SoPhuKien);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_PhuKienDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhuKienDangKy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_PhuKienDangKy(long soTiepNhan, DateTime ngayTiepNhan, int trangThaiXuLy, DateTime ngayPhuKien, string nguoiDuyet, string vanBanChoPhep, string ghiChu, string soPhuKien, long hopDong_ID, long id, string maHaiQuan, string maDoanhNghiep, string gUIDSTR, string deXuatKhac, string lyDoSua, int actionStatus, string guidReference, short namDK, string hUONGDAN, string phanLuong, string huongdanPL)
		{
			KDT_GC_PhuKienDangKy entity = new KDT_GC_PhuKienDangKy();			
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayPhuKien = ngayPhuKien;
			entity.NguoiDuyet = nguoiDuyet;
			entity.VanBanChoPhep = vanBanChoPhep;
			entity.GhiChu = ghiChu;
			entity.SoPhuKien = soPhuKien;
			entity.HopDong_ID = hopDong_ID;
			entity.ID = id;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamDK = namDK;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.HuongdanPL = huongdanPL;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, NgayPhuKien.Year <= 1753 ? DBNull.Value : (object) NgayPhuKien);
			db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, NguoiDuyet);
			db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, VanBanChoPhep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, SoPhuKien);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_PhuKienDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhuKienDangKy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_PhuKienDangKy(long id)
		{
			KDT_GC_PhuKienDangKy entity = new KDT_GC_PhuKienDangKy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_DeleteBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_PhuKienDangKy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_PhuKienDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhuKienDangKy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}