-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_DeleteBy_StorageAreasProduction_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Insert]
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiCSSX nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_AffiliatedMemberCompany]
(
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@TenDoanhNghiep,
	@MaDoanhNghiep,
	@DiaChiCSSX
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiCSSX nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_AffiliatedMemberCompany]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[DiaChiCSSX] = @DiaChiCSSX
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiCSSX nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_AffiliatedMemberCompany] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_AffiliatedMemberCompany] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[DiaChiCSSX] = @DiaChiCSSX
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_AffiliatedMemberCompany]
		(
			[StorageAreasProduction_ID],
			[TenDoanhNghiep],
			[MaDoanhNghiep],
			[DiaChiCSSX]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@TenDoanhNghiep,
			@MaDoanhNghiep,
			@DiaChiCSSX
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_AffiliatedMemberCompany]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_AffiliatedMemberCompany]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_AffiliatedMemberCompany] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
FROM
	[dbo].[t_KDT_VNACCS_AffiliatedMemberCompany]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
FROM
	[dbo].[t_KDT_VNACCS_AffiliatedMemberCompany]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
FROM [dbo].[t_KDT_VNACCS_AffiliatedMemberCompany] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AffiliatedMemberCompany_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
FROM
	[dbo].[t_KDT_VNACCS_AffiliatedMemberCompany]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Career_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Career_DeleteBy_StorageAreasProduction_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_Insert]
	@StorageAreasProduction_ID bigint,
	@LoaiNganhNghe numeric(1, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_Career]
(
	[StorageAreasProduction_ID],
	[LoaiNganhNghe]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@LoaiNganhNghe
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@LoaiNganhNghe numeric(1, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_Career]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[LoaiNganhNghe] = @LoaiNganhNghe
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@LoaiNganhNghe numeric(1, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_Career] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_Career] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[LoaiNganhNghe] = @LoaiNganhNghe
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_Career]
		(
			[StorageAreasProduction_ID],
			[LoaiNganhNghe]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@LoaiNganhNghe
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_Career]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_Career]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_Career] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiNganhNghe]
FROM
	[dbo].[t_KDT_VNACCS_Career]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiNganhNghe]
FROM
	[dbo].[t_KDT_VNACCS_Career]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[LoaiNganhNghe]
FROM [dbo].[t_KDT_VNACCS_Career] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Career_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Career_SelectAll]



AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiNganhNghe]
FROM
	[dbo].[t_KDT_VNACCS_Career]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_DeleteBy_StorageAreasProduction_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Insert]
	@StorageAreasProduction_ID bigint,
	@LoaiNganhNghe numeric(1, 0),
	@ChuKySanXuat nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_Careeries]
(
	[StorageAreasProduction_ID],
	[LoaiNganhNghe],
	[ChuKySanXuat]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@LoaiNganhNghe,
	@ChuKySanXuat
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@LoaiNganhNghe numeric(1, 0),
	@ChuKySanXuat nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_Careeries]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[LoaiNganhNghe] = @LoaiNganhNghe,
	[ChuKySanXuat] = @ChuKySanXuat
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@LoaiNganhNghe numeric(1, 0),
	@ChuKySanXuat nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_Careeries] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_Careeries] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[LoaiNganhNghe] = @LoaiNganhNghe,
			[ChuKySanXuat] = @ChuKySanXuat
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_Careeries]
		(
			[StorageAreasProduction_ID],
			[LoaiNganhNghe],
			[ChuKySanXuat]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@LoaiNganhNghe,
			@ChuKySanXuat
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_Careeries]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_Careeries]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_Careeries] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiNganhNghe],
	[ChuKySanXuat]
FROM
	[dbo].[t_KDT_VNACCS_Careeries]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiNganhNghe],
	[ChuKySanXuat]
FROM
	[dbo].[t_KDT_VNACCS_Careeries]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[LoaiNganhNghe],
	[ChuKySanXuat]
FROM [dbo].[t_KDT_VNACCS_Careeries] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectAll]




AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiNganhNghe],
	[ChuKySanXuat]
FROM
	[dbo].[t_KDT_VNACCS_Careeries]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContentInspection_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_DeleteBy_StorageAreasProduction_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_Insert]
	@StorageAreasProduction_ID bigint,
	@SoBienBanKiemTra nvarchar(50),
	@SoKetLuanKiemTra nvarchar(50),
	@NgayKiemTra datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_ContentInspection]
(
	[StorageAreasProduction_ID],
	[SoBienBanKiemTra],
	[SoKetLuanKiemTra],
	[NgayKiemTra]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@SoBienBanKiemTra,
	@SoKetLuanKiemTra,
	@NgayKiemTra
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@SoBienBanKiemTra nvarchar(50),
	@SoKetLuanKiemTra nvarchar(50),
	@NgayKiemTra datetime
AS

UPDATE
	[dbo].[t_KDT_VNACCS_ContentInspection]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[SoBienBanKiemTra] = @SoBienBanKiemTra,
	[SoKetLuanKiemTra] = @SoKetLuanKiemTra,
	[NgayKiemTra] = @NgayKiemTra
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@SoBienBanKiemTra nvarchar(50),
	@SoKetLuanKiemTra nvarchar(50),
	@NgayKiemTra datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_ContentInspection] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_ContentInspection] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[SoBienBanKiemTra] = @SoBienBanKiemTra,
			[SoKetLuanKiemTra] = @SoKetLuanKiemTra,
			[NgayKiemTra] = @NgayKiemTra
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_ContentInspection]
		(
			[StorageAreasProduction_ID],
			[SoBienBanKiemTra],
			[SoKetLuanKiemTra],
			[NgayKiemTra]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@SoBienBanKiemTra,
			@SoKetLuanKiemTra,
			@NgayKiemTra
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_ContentInspection]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_ContentInspection]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_ContentInspection] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[SoBienBanKiemTra],
	[SoKetLuanKiemTra],
	[NgayKiemTra]
FROM
	[dbo].[t_KDT_VNACCS_ContentInspection]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[SoBienBanKiemTra],
	[SoKetLuanKiemTra],
	[NgayKiemTra]
FROM
	[dbo].[t_KDT_VNACCS_ContentInspection]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[SoBienBanKiemTra],
	[SoKetLuanKiemTra],
	[NgayKiemTra]
FROM [dbo].[t_KDT_VNACCS_ContentInspection] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContentInspection_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContentInspection_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[SoBienBanKiemTra],
	[SoKetLuanKiemTra],
	[NgayKiemTra]
FROM
	[dbo].[t_KDT_VNACCS_ContentInspection]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_HoldingCompany_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_DeleteBy_StorageAreasProduction_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_Insert]
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiCSSX nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_HoldingCompany]
(
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@TenDoanhNghiep,
	@MaDoanhNghiep,
	@DiaChiCSSX
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiCSSX nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_HoldingCompany]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[DiaChiCSSX] = @DiaChiCSSX
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiCSSX nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_HoldingCompany] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_HoldingCompany] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[DiaChiCSSX] = @DiaChiCSSX
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_HoldingCompany]
		(
			[StorageAreasProduction_ID],
			[TenDoanhNghiep],
			[MaDoanhNghiep],
			[DiaChiCSSX]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@TenDoanhNghiep,
			@MaDoanhNghiep,
			@DiaChiCSSX
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_HoldingCompany]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_HoldingCompany]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_HoldingCompany] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
FROM
	[dbo].[t_KDT_VNACCS_HoldingCompany]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
FROM
	[dbo].[t_KDT_VNACCS_HoldingCompany]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
FROM [dbo].[t_KDT_VNACCS_HoldingCompany] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_HoldingCompany_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_HoldingCompany_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiCSSX]
FROM
	[dbo].[t_KDT_VNACCS_HoldingCompany]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]
	@StorageAreasProduction_ID bigint,
	@LoaiCSSX numeric(1, 0),
	@DiaChiCSSX nvarchar(255),
	@LoaiDiaChiCSSX numeric(1, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSanXuat nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_ManufactureFactory]
(
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@LoaiCSSX,
	@DiaChiCSSX,
	@LoaiDiaChiCSSX,
	@SoLuongSoHuu,
	@SoLuongDiThue,
	@SoLuongKhac,
	@TongSoLuong,
	@NangLucSanXuat
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@LoaiCSSX numeric(1, 0),
	@DiaChiCSSX nvarchar(255),
	@LoaiDiaChiCSSX numeric(1, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSanXuat nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_ManufactureFactory]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[LoaiCSSX] = @LoaiCSSX,
	[DiaChiCSSX] = @DiaChiCSSX,
	[LoaiDiaChiCSSX] = @LoaiDiaChiCSSX,
	[SoLuongSoHuu] = @SoLuongSoHuu,
	[SoLuongDiThue] = @SoLuongDiThue,
	[SoLuongKhac] = @SoLuongKhac,
	[TongSoLuong] = @TongSoLuong,
	[NangLucSanXuat] = @NangLucSanXuat
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@LoaiCSSX numeric(1, 0),
	@DiaChiCSSX nvarchar(255),
	@LoaiDiaChiCSSX numeric(1, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSanXuat nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_ManufactureFactory] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_ManufactureFactory] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[LoaiCSSX] = @LoaiCSSX,
			[DiaChiCSSX] = @DiaChiCSSX,
			[LoaiDiaChiCSSX] = @LoaiDiaChiCSSX,
			[SoLuongSoHuu] = @SoLuongSoHuu,
			[SoLuongDiThue] = @SoLuongDiThue,
			[SoLuongKhac] = @SoLuongKhac,
			[TongSoLuong] = @TongSoLuong,
			[NangLucSanXuat] = @NangLucSanXuat
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_ManufactureFactory]
		(
			[StorageAreasProduction_ID],
			[LoaiCSSX],
			[DiaChiCSSX],
			[LoaiDiaChiCSSX],
			[SoLuongSoHuu],
			[SoLuongDiThue],
			[SoLuongKhac],
			[TongSoLuong],
			[NangLucSanXuat]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@LoaiCSSX,
			@DiaChiCSSX,
			@LoaiDiaChiCSSX,
			@SoLuongSoHuu,
			@SoLuongDiThue,
			@SoLuongKhac,
			@TongSoLuong,
			@NangLucSanXuat
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_ManufactureFactory]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_ManufactureFactory]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_ManufactureFactory] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat]
FROM
	[dbo].[t_KDT_VNACCS_ManufactureFactory]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat]
FROM
	[dbo].[t_KDT_VNACCS_ManufactureFactory]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat]
FROM [dbo].[t_KDT_VNACCS_ManufactureFactory] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat]
FROM
	[dbo].[t_KDT_VNACCS_ManufactureFactory]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_MemberCompany_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_DeleteBy_StorageAreasProduction_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_Insert]
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiChiNhanh nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_MemberCompany]
(
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiChiNhanh]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@TenDoanhNghiep,
	@MaDoanhNghiep,
	@DiaChiChiNhanh
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiChiNhanh nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_MemberCompany]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[DiaChiChiNhanh] = @DiaChiChiNhanh
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiChiNhanh nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_MemberCompany] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_MemberCompany] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[DiaChiChiNhanh] = @DiaChiChiNhanh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_MemberCompany]
		(
			[StorageAreasProduction_ID],
			[TenDoanhNghiep],
			[MaDoanhNghiep],
			[DiaChiChiNhanh]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@TenDoanhNghiep,
			@MaDoanhNghiep,
			@DiaChiChiNhanh
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_MemberCompany]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_MemberCompany]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_MemberCompany] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiChiNhanh]
FROM
	[dbo].[t_KDT_VNACCS_MemberCompany]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiChiNhanh]
FROM
	[dbo].[t_KDT_VNACCS_MemberCompany]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiChiNhanh]
FROM [dbo].[t_KDT_VNACCS_MemberCompany] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_MemberCompany_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_MemberCompany_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiChiNhanh]
FROM
	[dbo].[t_KDT_VNACCS_MemberCompany]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiTruSoChinh nvarchar(255),
	@LoaiDiaChiTruSoChinh numeric(1, 0),
	@NuocDauTu nvarchar(255),
	@NganhNgheSanXuat nvarchar(255),
	@TenDoanhNghiepTKTD nvarchar(255),
	@MaDoanhNghiepTKTD nvarchar(17),
	@LyDoChuyenDoi nvarchar(255),
	@SoCMNDCT nvarchar(17),
	@NgayCapGiayPhepCT datetime,
	@NoiCapGiayPhepCT nvarchar(255),
	@NoiDangKyHKTTCT nvarchar(255),
	@SoDienThoaiCT nvarchar(17),
	@SoCMNDGD nvarchar(17),
	@NgayCapGiayPhepGD datetime,
	@NoiCapGiayPhepGD nvarchar(255),
	@NoiDangKyHKTTGD nvarchar(255),
	@SoDienThoaiGD nvarchar(17),
	@DaDuocCQHQKT numeric(1, 0),
	@BiXuPhatVeBuonLau numeric(1, 0),
	@BiXuPhatVeTronThue numeric(1, 0),
	@BiXuPhatVeKeToan numeric(1, 0),
	@ThoiGianSanXuat numeric(1, 0),
	@SoLuongSanPham numeric(10, 0),
	@BoPhanQuanLy numeric(10, 0),
	@SoLuongCongNhan numeric(10, 0),
	@TenCongTyMe nvarchar(255),
	@MaCongTyMe nvarchar(17),
	@SoLuongThanhVien numeric(10, 0),
	@SoLuongChiNhanh numeric(10, 0),
	@SoLuongThanhVienCTM numeric(10, 0),
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_StorageAreasProduction]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiTruSoChinh],
	[LoaiDiaChiTruSoChinh],
	[NuocDauTu],
	[NganhNgheSanXuat],
	[TenDoanhNghiepTKTD],
	[MaDoanhNghiepTKTD],
	[LyDoChuyenDoi],
	[SoCMNDCT],
	[NgayCapGiayPhepCT],
	[NoiCapGiayPhepCT],
	[NoiDangKyHKTTCT],
	[SoDienThoaiCT],
	[SoCMNDGD],
	[NgayCapGiayPhepGD],
	[NoiCapGiayPhepGD],
	[NoiDangKyHKTTGD],
	[SoDienThoaiGD],
	[DaDuocCQHQKT],
	[BiXuPhatVeBuonLau],
	[BiXuPhatVeTronThue],
	[BiXuPhatVeKeToan],
	[ThoiGianSanXuat],
	[SoLuongSanPham],
	[BoPhanQuanLy],
	[SoLuongCongNhan],
	[TenCongTyMe],
	[MaCongTyMe],
	[SoLuongThanhVien],
	[SoLuongChiNhanh],
	[SoLuongThanhVienCTM],
	[GhiChuKhac],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@TenDoanhNghiep,
	@MaDoanhNghiep,
	@DiaChiTruSoChinh,
	@LoaiDiaChiTruSoChinh,
	@NuocDauTu,
	@NganhNgheSanXuat,
	@TenDoanhNghiepTKTD,
	@MaDoanhNghiepTKTD,
	@LyDoChuyenDoi,
	@SoCMNDCT,
	@NgayCapGiayPhepCT,
	@NoiCapGiayPhepCT,
	@NoiDangKyHKTTCT,
	@SoDienThoaiCT,
	@SoCMNDGD,
	@NgayCapGiayPhepGD,
	@NoiCapGiayPhepGD,
	@NoiDangKyHKTTGD,
	@SoDienThoaiGD,
	@DaDuocCQHQKT,
	@BiXuPhatVeBuonLau,
	@BiXuPhatVeTronThue,
	@BiXuPhatVeKeToan,
	@ThoiGianSanXuat,
	@SoLuongSanPham,
	@BoPhanQuanLy,
	@SoLuongCongNhan,
	@TenCongTyMe,
	@MaCongTyMe,
	@SoLuongThanhVien,
	@SoLuongChiNhanh,
	@SoLuongThanhVienCTM,
	@GhiChuKhac,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiTruSoChinh nvarchar(255),
	@LoaiDiaChiTruSoChinh numeric(1, 0),
	@NuocDauTu nvarchar(255),
	@NganhNgheSanXuat nvarchar(255),
	@TenDoanhNghiepTKTD nvarchar(255),
	@MaDoanhNghiepTKTD nvarchar(17),
	@LyDoChuyenDoi nvarchar(255),
	@SoCMNDCT nvarchar(17),
	@NgayCapGiayPhepCT datetime,
	@NoiCapGiayPhepCT nvarchar(255),
	@NoiDangKyHKTTCT nvarchar(255),
	@SoDienThoaiCT nvarchar(17),
	@SoCMNDGD nvarchar(17),
	@NgayCapGiayPhepGD datetime,
	@NoiCapGiayPhepGD nvarchar(255),
	@NoiDangKyHKTTGD nvarchar(255),
	@SoDienThoaiGD nvarchar(17),
	@DaDuocCQHQKT numeric(1, 0),
	@BiXuPhatVeBuonLau numeric(1, 0),
	@BiXuPhatVeTronThue numeric(1, 0),
	@BiXuPhatVeKeToan numeric(1, 0),
	@ThoiGianSanXuat numeric(1, 0),
	@SoLuongSanPham numeric(10, 0),
	@BoPhanQuanLy numeric(10, 0),
	@SoLuongCongNhan numeric(10, 0),
	@TenCongTyMe nvarchar(255),
	@MaCongTyMe nvarchar(17),
	@SoLuongThanhVien numeric(10, 0),
	@SoLuongChiNhanh numeric(10, 0),
	@SoLuongThanhVienCTM numeric(10, 0),
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_StorageAreasProduction]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[DiaChiTruSoChinh] = @DiaChiTruSoChinh,
	[LoaiDiaChiTruSoChinh] = @LoaiDiaChiTruSoChinh,
	[NuocDauTu] = @NuocDauTu,
	[NganhNgheSanXuat] = @NganhNgheSanXuat,
	[TenDoanhNghiepTKTD] = @TenDoanhNghiepTKTD,
	[MaDoanhNghiepTKTD] = @MaDoanhNghiepTKTD,
	[LyDoChuyenDoi] = @LyDoChuyenDoi,
	[SoCMNDCT] = @SoCMNDCT,
	[NgayCapGiayPhepCT] = @NgayCapGiayPhepCT,
	[NoiCapGiayPhepCT] = @NoiCapGiayPhepCT,
	[NoiDangKyHKTTCT] = @NoiDangKyHKTTCT,
	[SoDienThoaiCT] = @SoDienThoaiCT,
	[SoCMNDGD] = @SoCMNDGD,
	[NgayCapGiayPhepGD] = @NgayCapGiayPhepGD,
	[NoiCapGiayPhepGD] = @NoiCapGiayPhepGD,
	[NoiDangKyHKTTGD] = @NoiDangKyHKTTGD,
	[SoDienThoaiGD] = @SoDienThoaiGD,
	[DaDuocCQHQKT] = @DaDuocCQHQKT,
	[BiXuPhatVeBuonLau] = @BiXuPhatVeBuonLau,
	[BiXuPhatVeTronThue] = @BiXuPhatVeTronThue,
	[BiXuPhatVeKeToan] = @BiXuPhatVeKeToan,
	[ThoiGianSanXuat] = @ThoiGianSanXuat,
	[SoLuongSanPham] = @SoLuongSanPham,
	[BoPhanQuanLy] = @BoPhanQuanLy,
	[SoLuongCongNhan] = @SoLuongCongNhan,
	[TenCongTyMe] = @TenCongTyMe,
	[MaCongTyMe] = @MaCongTyMe,
	[SoLuongThanhVien] = @SoLuongThanhVien,
	[SoLuongChiNhanh] = @SoLuongChiNhanh,
	[SoLuongThanhVienCTM] = @SoLuongThanhVienCTM,
	[GhiChuKhac] = @GhiChuKhac,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_InsertUpdate]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiTruSoChinh nvarchar(255),
	@LoaiDiaChiTruSoChinh numeric(1, 0),
	@NuocDauTu nvarchar(255),
	@NganhNgheSanXuat nvarchar(255),
	@TenDoanhNghiepTKTD nvarchar(255),
	@MaDoanhNghiepTKTD nvarchar(17),
	@LyDoChuyenDoi nvarchar(255),
	@SoCMNDCT nvarchar(17),
	@NgayCapGiayPhepCT datetime,
	@NoiCapGiayPhepCT nvarchar(255),
	@NoiDangKyHKTTCT nvarchar(255),
	@SoDienThoaiCT nvarchar(17),
	@SoCMNDGD nvarchar(17),
	@NgayCapGiayPhepGD datetime,
	@NoiCapGiayPhepGD nvarchar(255),
	@NoiDangKyHKTTGD nvarchar(255),
	@SoDienThoaiGD nvarchar(17),
	@DaDuocCQHQKT numeric(1, 0),
	@BiXuPhatVeBuonLau numeric(1, 0),
	@BiXuPhatVeTronThue numeric(1, 0),
	@BiXuPhatVeKeToan numeric(1, 0),
	@ThoiGianSanXuat numeric(1, 0),
	@SoLuongSanPham numeric(10, 0),
	@BoPhanQuanLy numeric(10, 0),
	@SoLuongCongNhan numeric(10, 0),
	@TenCongTyMe nvarchar(255),
	@MaCongTyMe nvarchar(17),
	@SoLuongThanhVien numeric(10, 0),
	@SoLuongChiNhanh numeric(10, 0),
	@SoLuongThanhVienCTM numeric(10, 0),
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_StorageAreasProduction] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[DiaChiTruSoChinh] = @DiaChiTruSoChinh,
			[LoaiDiaChiTruSoChinh] = @LoaiDiaChiTruSoChinh,
			[NuocDauTu] = @NuocDauTu,
			[NganhNgheSanXuat] = @NganhNgheSanXuat,
			[TenDoanhNghiepTKTD] = @TenDoanhNghiepTKTD,
			[MaDoanhNghiepTKTD] = @MaDoanhNghiepTKTD,
			[LyDoChuyenDoi] = @LyDoChuyenDoi,
			[SoCMNDCT] = @SoCMNDCT,
			[NgayCapGiayPhepCT] = @NgayCapGiayPhepCT,
			[NoiCapGiayPhepCT] = @NoiCapGiayPhepCT,
			[NoiDangKyHKTTCT] = @NoiDangKyHKTTCT,
			[SoDienThoaiCT] = @SoDienThoaiCT,
			[SoCMNDGD] = @SoCMNDGD,
			[NgayCapGiayPhepGD] = @NgayCapGiayPhepGD,
			[NoiCapGiayPhepGD] = @NoiCapGiayPhepGD,
			[NoiDangKyHKTTGD] = @NoiDangKyHKTTGD,
			[SoDienThoaiGD] = @SoDienThoaiGD,
			[DaDuocCQHQKT] = @DaDuocCQHQKT,
			[BiXuPhatVeBuonLau] = @BiXuPhatVeBuonLau,
			[BiXuPhatVeTronThue] = @BiXuPhatVeTronThue,
			[BiXuPhatVeKeToan] = @BiXuPhatVeKeToan,
			[ThoiGianSanXuat] = @ThoiGianSanXuat,
			[SoLuongSanPham] = @SoLuongSanPham,
			[BoPhanQuanLy] = @BoPhanQuanLy,
			[SoLuongCongNhan] = @SoLuongCongNhan,
			[TenCongTyMe] = @TenCongTyMe,
			[MaCongTyMe] = @MaCongTyMe,
			[SoLuongThanhVien] = @SoLuongThanhVien,
			[SoLuongChiNhanh] = @SoLuongChiNhanh,
			[SoLuongThanhVienCTM] = @SoLuongThanhVienCTM,
			[GhiChuKhac] = @GhiChuKhac,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_StorageAreasProduction]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[TenDoanhNghiep],
			[MaDoanhNghiep],
			[DiaChiTruSoChinh],
			[LoaiDiaChiTruSoChinh],
			[NuocDauTu],
			[NganhNgheSanXuat],
			[TenDoanhNghiepTKTD],
			[MaDoanhNghiepTKTD],
			[LyDoChuyenDoi],
			[SoCMNDCT],
			[NgayCapGiayPhepCT],
			[NoiCapGiayPhepCT],
			[NoiDangKyHKTTCT],
			[SoDienThoaiCT],
			[SoCMNDGD],
			[NgayCapGiayPhepGD],
			[NoiCapGiayPhepGD],
			[NoiDangKyHKTTGD],
			[SoDienThoaiGD],
			[DaDuocCQHQKT],
			[BiXuPhatVeBuonLau],
			[BiXuPhatVeTronThue],
			[BiXuPhatVeKeToan],
			[ThoiGianSanXuat],
			[SoLuongSanPham],
			[BoPhanQuanLy],
			[SoLuongCongNhan],
			[TenCongTyMe],
			[MaCongTyMe],
			[SoLuongThanhVien],
			[SoLuongChiNhanh],
			[SoLuongThanhVienCTM],
			[GhiChuKhac],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@TenDoanhNghiep,
			@MaDoanhNghiep,
			@DiaChiTruSoChinh,
			@LoaiDiaChiTruSoChinh,
			@NuocDauTu,
			@NganhNgheSanXuat,
			@TenDoanhNghiepTKTD,
			@MaDoanhNghiepTKTD,
			@LyDoChuyenDoi,
			@SoCMNDCT,
			@NgayCapGiayPhepCT,
			@NoiCapGiayPhepCT,
			@NoiDangKyHKTTCT,
			@SoDienThoaiCT,
			@SoCMNDGD,
			@NgayCapGiayPhepGD,
			@NoiCapGiayPhepGD,
			@NoiDangKyHKTTGD,
			@SoDienThoaiGD,
			@DaDuocCQHQKT,
			@BiXuPhatVeBuonLau,
			@BiXuPhatVeTronThue,
			@BiXuPhatVeKeToan,
			@ThoiGianSanXuat,
			@SoLuongSanPham,
			@BoPhanQuanLy,
			@SoLuongCongNhan,
			@TenCongTyMe,
			@MaCongTyMe,
			@SoLuongThanhVien,
			@SoLuongChiNhanh,
			@SoLuongThanhVienCTM,
			@GhiChuKhac,
			@GuidStr
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_StorageAreasProduction]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiTruSoChinh],
	[LoaiDiaChiTruSoChinh],
	[NuocDauTu],
	[NganhNgheSanXuat],
	[TenDoanhNghiepTKTD],
	[MaDoanhNghiepTKTD],
	[LyDoChuyenDoi],
	[SoCMNDCT],
	[NgayCapGiayPhepCT],
	[NoiCapGiayPhepCT],
	[NoiDangKyHKTTCT],
	[SoDienThoaiCT],
	[SoCMNDGD],
	[NgayCapGiayPhepGD],
	[NoiCapGiayPhepGD],
	[NoiDangKyHKTTGD],
	[SoDienThoaiGD],
	[DaDuocCQHQKT],
	[BiXuPhatVeBuonLau],
	[BiXuPhatVeTronThue],
	[BiXuPhatVeKeToan],
	[ThoiGianSanXuat],
	[SoLuongSanPham],
	[BoPhanQuanLy],
	[SoLuongCongNhan],
	[TenCongTyMe],
	[MaCongTyMe],
	[SoLuongThanhVien],
	[SoLuongChiNhanh],
	[SoLuongThanhVienCTM],
	[GhiChuKhac],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiTruSoChinh],
	[LoaiDiaChiTruSoChinh],
	[NuocDauTu],
	[NganhNgheSanXuat],
	[TenDoanhNghiepTKTD],
	[MaDoanhNghiepTKTD],
	[LyDoChuyenDoi],
	[SoCMNDCT],
	[NgayCapGiayPhepCT],
	[NoiCapGiayPhepCT],
	[NoiDangKyHKTTCT],
	[SoDienThoaiCT],
	[SoCMNDGD],
	[NgayCapGiayPhepGD],
	[NoiCapGiayPhepGD],
	[NoiDangKyHKTTGD],
	[SoDienThoaiGD],
	[DaDuocCQHQKT],
	[BiXuPhatVeBuonLau],
	[BiXuPhatVeTronThue],
	[BiXuPhatVeKeToan],
	[ThoiGianSanXuat],
	[SoLuongSanPham],
	[BoPhanQuanLy],
	[SoLuongCongNhan],
	[TenCongTyMe],
	[MaCongTyMe],
	[SoLuongThanhVien],
	[SoLuongChiNhanh],
	[SoLuongThanhVienCTM],
	[GhiChuKhac],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]
-- Database: ECS_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 18, 2017
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]







































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiTruSoChinh],
	[LoaiDiaChiTruSoChinh],
	[NuocDauTu],
	[NganhNgheSanXuat],
	[TenDoanhNghiepTKTD],
	[MaDoanhNghiepTKTD],
	[LyDoChuyenDoi],
	[SoCMNDCT],
	[NgayCapGiayPhepCT],
	[NoiCapGiayPhepCT],
	[NoiDangKyHKTTCT],
	[SoDienThoaiCT],
	[SoCMNDGD],
	[NgayCapGiayPhepGD],
	[NoiCapGiayPhepGD],
	[NoiDangKyHKTTGD],
	[SoDienThoaiGD],
	[DaDuocCQHQKT],
	[BiXuPhatVeBuonLau],
	[BiXuPhatVeTronThue],
	[BiXuPhatVeKeToan],
	[ThoiGianSanXuat],
	[SoLuongSanPham],
	[BoPhanQuanLy],
	[SoLuongCongNhan],
	[TenCongTyMe],
	[MaCongTyMe],
	[SoLuongThanhVien],
	[SoLuongChiNhanh],
	[SoLuongThanhVienCTM],
	[GhiChuKhac],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '34.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('34.4',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO CÁO QUYẾT TOÁN VÀ THÔNG BÁO CƠ SỞ SẢN XUẤT')
END