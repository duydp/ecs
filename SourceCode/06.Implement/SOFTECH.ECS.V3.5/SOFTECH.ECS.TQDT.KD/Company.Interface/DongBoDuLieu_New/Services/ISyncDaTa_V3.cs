﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Net;
using Company.KDT.SHARE.Components;
#if SXXK_V3 || SXXK_V4
using ToKhaiMauDich = Company.BLL.KDT.ToKhaiMauDich;
using Company.KDT.SHARE.Components;
#endif
namespace Company.Interface.DongBoDuLieu_New.Services
{
    [ServiceContractAttribute(Namespace = "http://tempuri.org/", ConfigurationName = "ISyncData")]
    public interface ISyncData_SXXK : ISyncData
    {
        [OperationContractAttribute(Action = "http://tempuri.org/SeachTKMD_SXXK", ReplyAction = "*")]
        string SeachTKMD_SXXK(string userNameLogin, string passWordLogin, string listDaiLy, string MaDoanhNghiep, DateTime FromNgayDongBo, DateTime ToNgayDongBo);

        [OperationContractAttribute(Action = "http://tempuri.org/Send_SXXK", ReplyAction = "*")]
        string Send_SXXK(string userNameLogin, string passWordLogin, string strTKMD);

        [OperationContractAttribute(Action = "http://tempuri.org/LoadConfigure", ReplyAction = "*")]
        string LoadConfigure(string key);

        [OperationContractAttribute(Action = "http://tempuri.org/Receive_SXXK", ReplyAction = "*")]
        string Receive_SXXK(string userNameLogin, string passWordLogin, int soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep);

        //[OperationContractAttribute(Action = "http://tempuri.org/LoginDaiLy", ReplyAction = "*")]
        //string LoginDaiLy(string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/SendVNACCS", ReplyAction = "*")]
        string SendVNACCS(string userNameLogin, string passWordLogin, string datastr);

        [OperationContractAttribute(Action = "http://tempuri.org/ReceiveVNACCS", ReplyAction = "*")]
        string ReceiveVNACCS(string sotokhai, DateTime ngayDangKy, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/ReceiveVNACCSCTQ", ReplyAction = "*")]
        string ReceiveVNACCSCTQ(string sotokhai, DateTime ngayDangKy, string userNameLogin, string passWordLogin, string trangThaiXuLy);

        [OperationContractAttribute(Action = "http://tempuri.org/SelectTrack_VNACCS", ReplyAction = "*")]
        List<Company.BLL.VNACCS.DongBoDuLieu_TrackVNACCS> SelectTrack_VNACCS(string userNameLogin, string passWordLogin, string tuNgay, string denNgay, string madoanhnghiep, string userDaiLy);

        [OperationContractAttribute(Action = "http://tempuri.org/SelectSoToKhai", ReplyAction = "*")]
        List<string> SelectSoToKhai(string userNameLogin, string passWordLogin, string tuNgay, string denNgay, string madoanhnghiep, string userDaiLy);
    }
    public class IsyncDaTa_V3
    {

        public static ISyncData_SXXK SyncService()
        {
            ISyncData_SXXK service = null;
            string url;
            try
            {
                url = LoadConfigure("WS_SyncData");
                service = CreateService<ISyncData_SXXK>(url);

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw (ex);
            }
            return service;
        }
        public static string LoadConfigure(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

            return config.AppSettings.Settings[key].Value.ToString();
        }
        private static T CreateService<T>(string url)
        {
            T service;
            try
            {

                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    url = string.Format("http://{0}", url);
                BasicHttpBinding binding = new BasicHttpBinding();
                EndpointAddress address = new EndpointAddress(url);
                string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
                string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
                if (!string.IsNullOrEmpty(host + port))
                {
                    WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                    proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    binding.ProxyAddress = proxy.Address;
                }
                binding.UseDefaultWebProxy = true;
                binding.SendTimeout = new TimeSpan(0, Company.KDT.SHARE.Components.Globals.TimeConnect, 0);
                binding.MaxReceivedMessageSize = int.MaxValue;
                binding.MaxBufferSize = int.MaxValue;
                binding.TextEncoding = System.Text.Encoding.UTF8;
                binding.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                binding.OpenTimeout = new TimeSpan(0, Company.KDT.SHARE.Components.Globals.TimeConnect, 0);
                //binding.Security.Message.ClientCredentialType.
                if (address.Uri.Scheme == "https")
                {
                    //Dùng https
                    binding.Security.Mode = BasicHttpSecurityMode.Transport;
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
                }

                /*
                 * Mở theo cách add service(Source gen). LanNT
                 * cis = new Company.KDT.SHARE.Components.CisWS.CISServiceSoapClient(binding, address);
                 */
                /*
                 * Mở theo Channel
                 */
                ChannelFactory<T> factory = new ChannelFactory<T>(binding, address);

                //client.ClientCredentials.UserName.UserName = "shoaib";
                //client.ClientCredentials.UserName.Password = "shaikh";
                service = factory.CreateChannel();


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Khởi tạo service kết nối", ex);
                throw new Exception("Lỗi kết nối đến server.\r\n" + ex.Message + "\r\nURL = " + url);
            }
            return service;
        }
    }
}
