-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]
	@LoaiChungTu varchar(3),
	@LoaiHinhBaoLanh varchar(1),
	@MaNganHangCungCapBaoLanh varchar(11),
	@NamPhatHanh numeric(4, 0),
	@KiHieuChungTuPhatHanhBaoLanh varchar(10),
	@SoChungTuPhatHanhBaoLanh varchar(10),
	@MaTienTe varchar(3),
	@MaNganHangTraThay varchar(11),
	@TenNganHangTraThay nvarchar(300),
	@MaDonViSuDungHanMuc varchar(13),
	@TenDonViSuDungHanMuc nvarchar(300),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@MaKetQuaXuLy varchar(75),
	@TenNganHangCungCapBaoLanh nvarchar(210),
	@SoToKhai numeric(12, 0),
	@NgayDuDinhKhaiBao datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210),
	@SoVanDon_1 varchar(35),
	@SoVanDon_2 varchar(35),
	@SoVanDon_3 varchar(35),
	@SoVanDon_4 varchar(35),
	@SoVanDon_5 varchar(35),
	@SoHoaDon varchar(35),
	@MaDonViSuDungBaoLanh varchar(13),
	@TenDonViSuDungBaoLanh nvarchar(300),
	@MaDonViDaiDienSuDungBaoLanh varchar(13),
	@TenDonViDaiDienSuDungBaoLanh nvarchar(300),
	@NgayBatDauHieuLuc datetime,
	@NgayHetHieuLuc datetime,
	@SoTienDangKyBaoLanh numeric(13, 0),
	@CoBaoVoHieu varchar(1),
	@SoDuBaoLanh numeric(13, 0),
	@SoTienHanMucDangKi numeric(13, 0),
	@SoDuHanMuc numeric(13, 0),
	@SoThuTuHanMuc varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@NgayKhaiBao datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuThanhToan]
(
	[LoaiChungTu],
	[LoaiHinhBaoLanh],
	[MaNganHangCungCapBaoLanh],
	[NamPhatHanh],
	[KiHieuChungTuPhatHanhBaoLanh],
	[SoChungTuPhatHanhBaoLanh],
	[MaTienTe],
	[MaNganHangTraThay],
	[TenNganHangTraThay],
	[MaDonViSuDungHanMuc],
	[TenDonViSuDungHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[MaKetQuaXuLy],
	[TenNganHangCungCapBaoLanh],
	[SoToKhai],
	[NgayDuDinhKhaiBao],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan],
	[SoVanDon_1],
	[SoVanDon_2],
	[SoVanDon_3],
	[SoVanDon_4],
	[SoVanDon_5],
	[SoHoaDon],
	[MaDonViSuDungBaoLanh],
	[TenDonViSuDungBaoLanh],
	[MaDonViDaiDienSuDungBaoLanh],
	[TenDonViDaiDienSuDungBaoLanh],
	[NgayBatDauHieuLuc],
	[NgayHetHieuLuc],
	[SoTienDangKyBaoLanh],
	[CoBaoVoHieu],
	[SoDuBaoLanh],
	[SoTienHanMucDangKi],
	[SoDuHanMuc],
	[SoThuTuHanMuc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[NgayKhaiBao]
)
VALUES 
(
	@LoaiChungTu,
	@LoaiHinhBaoLanh,
	@MaNganHangCungCapBaoLanh,
	@NamPhatHanh,
	@KiHieuChungTuPhatHanhBaoLanh,
	@SoChungTuPhatHanhBaoLanh,
	@MaTienTe,
	@MaNganHangTraThay,
	@TenNganHangTraThay,
	@MaDonViSuDungHanMuc,
	@TenDonViSuDungHanMuc,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoHieuPhatHanhHanMuc,
	@MaKetQuaXuLy,
	@TenNganHangCungCapBaoLanh,
	@SoToKhai,
	@NgayDuDinhKhaiBao,
	@MaLoaiHinh,
	@CoQuanHaiQuan,
	@TenCucHaiQuan,
	@SoVanDon_1,
	@SoVanDon_2,
	@SoVanDon_3,
	@SoVanDon_4,
	@SoVanDon_5,
	@SoHoaDon,
	@MaDonViSuDungBaoLanh,
	@TenDonViSuDungBaoLanh,
	@MaDonViDaiDienSuDungBaoLanh,
	@TenDonViDaiDienSuDungBaoLanh,
	@NgayBatDauHieuLuc,
	@NgayHetHieuLuc,
	@SoTienDangKyBaoLanh,
	@CoBaoVoHieu,
	@SoDuBaoLanh,
	@SoTienHanMucDangKi,
	@SoDuHanMuc,
	@SoThuTuHanMuc,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@TrangThaiXuLy,
	@NgayKhaiBao
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@LoaiHinhBaoLanh varchar(1),
	@MaNganHangCungCapBaoLanh varchar(11),
	@NamPhatHanh numeric(4, 0),
	@KiHieuChungTuPhatHanhBaoLanh varchar(10),
	@SoChungTuPhatHanhBaoLanh varchar(10),
	@MaTienTe varchar(3),
	@MaNganHangTraThay varchar(11),
	@TenNganHangTraThay nvarchar(300),
	@MaDonViSuDungHanMuc varchar(13),
	@TenDonViSuDungHanMuc nvarchar(300),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@MaKetQuaXuLy varchar(75),
	@TenNganHangCungCapBaoLanh nvarchar(210),
	@SoToKhai numeric(12, 0),
	@NgayDuDinhKhaiBao datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210),
	@SoVanDon_1 varchar(35),
	@SoVanDon_2 varchar(35),
	@SoVanDon_3 varchar(35),
	@SoVanDon_4 varchar(35),
	@SoVanDon_5 varchar(35),
	@SoHoaDon varchar(35),
	@MaDonViSuDungBaoLanh varchar(13),
	@TenDonViSuDungBaoLanh nvarchar(300),
	@MaDonViDaiDienSuDungBaoLanh varchar(13),
	@TenDonViDaiDienSuDungBaoLanh nvarchar(300),
	@NgayBatDauHieuLuc datetime,
	@NgayHetHieuLuc datetime,
	@SoTienDangKyBaoLanh numeric(13, 0),
	@CoBaoVoHieu varchar(1),
	@SoDuBaoLanh numeric(13, 0),
	@SoTienHanMucDangKi numeric(13, 0),
	@SoDuHanMuc numeric(13, 0),
	@SoThuTuHanMuc varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@NgayKhaiBao datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuThanhToan]
SET
	[LoaiChungTu] = @LoaiChungTu,
	[LoaiHinhBaoLanh] = @LoaiHinhBaoLanh,
	[MaNganHangCungCapBaoLanh] = @MaNganHangCungCapBaoLanh,
	[NamPhatHanh] = @NamPhatHanh,
	[KiHieuChungTuPhatHanhBaoLanh] = @KiHieuChungTuPhatHanhBaoLanh,
	[SoChungTuPhatHanhBaoLanh] = @SoChungTuPhatHanhBaoLanh,
	[MaTienTe] = @MaTienTe,
	[MaNganHangTraThay] = @MaNganHangTraThay,
	[TenNganHangTraThay] = @TenNganHangTraThay,
	[MaDonViSuDungHanMuc] = @MaDonViSuDungHanMuc,
	[TenDonViSuDungHanMuc] = @TenDonViSuDungHanMuc,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[TenNganHangCungCapBaoLanh] = @TenNganHangCungCapBaoLanh,
	[SoToKhai] = @SoToKhai,
	[NgayDuDinhKhaiBao] = @NgayDuDinhKhaiBao,
	[MaLoaiHinh] = @MaLoaiHinh,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[TenCucHaiQuan] = @TenCucHaiQuan,
	[SoVanDon_1] = @SoVanDon_1,
	[SoVanDon_2] = @SoVanDon_2,
	[SoVanDon_3] = @SoVanDon_3,
	[SoVanDon_4] = @SoVanDon_4,
	[SoVanDon_5] = @SoVanDon_5,
	[SoHoaDon] = @SoHoaDon,
	[MaDonViSuDungBaoLanh] = @MaDonViSuDungBaoLanh,
	[TenDonViSuDungBaoLanh] = @TenDonViSuDungBaoLanh,
	[MaDonViDaiDienSuDungBaoLanh] = @MaDonViDaiDienSuDungBaoLanh,
	[TenDonViDaiDienSuDungBaoLanh] = @TenDonViDaiDienSuDungBaoLanh,
	[NgayBatDauHieuLuc] = @NgayBatDauHieuLuc,
	[NgayHetHieuLuc] = @NgayHetHieuLuc,
	[SoTienDangKyBaoLanh] = @SoTienDangKyBaoLanh,
	[CoBaoVoHieu] = @CoBaoVoHieu,
	[SoDuBaoLanh] = @SoDuBaoLanh,
	[SoTienHanMucDangKi] = @SoTienHanMucDangKi,
	[SoDuHanMuc] = @SoDuHanMuc,
	[SoThuTuHanMuc] = @SoThuTuHanMuc,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[NgayKhaiBao] = @NgayKhaiBao
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_InsertUpdate]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@LoaiHinhBaoLanh varchar(1),
	@MaNganHangCungCapBaoLanh varchar(11),
	@NamPhatHanh numeric(4, 0),
	@KiHieuChungTuPhatHanhBaoLanh varchar(10),
	@SoChungTuPhatHanhBaoLanh varchar(10),
	@MaTienTe varchar(3),
	@MaNganHangTraThay varchar(11),
	@TenNganHangTraThay nvarchar(300),
	@MaDonViSuDungHanMuc varchar(13),
	@TenDonViSuDungHanMuc nvarchar(300),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@MaKetQuaXuLy varchar(75),
	@TenNganHangCungCapBaoLanh nvarchar(210),
	@SoToKhai numeric(12, 0),
	@NgayDuDinhKhaiBao datetime,
	@MaLoaiHinh varchar(3),
	@CoQuanHaiQuan varchar(6),
	@TenCucHaiQuan nvarchar(210),
	@SoVanDon_1 varchar(35),
	@SoVanDon_2 varchar(35),
	@SoVanDon_3 varchar(35),
	@SoVanDon_4 varchar(35),
	@SoVanDon_5 varchar(35),
	@SoHoaDon varchar(35),
	@MaDonViSuDungBaoLanh varchar(13),
	@TenDonViSuDungBaoLanh nvarchar(300),
	@MaDonViDaiDienSuDungBaoLanh varchar(13),
	@TenDonViDaiDienSuDungBaoLanh nvarchar(300),
	@NgayBatDauHieuLuc datetime,
	@NgayHetHieuLuc datetime,
	@SoTienDangKyBaoLanh numeric(13, 0),
	@CoBaoVoHieu varchar(1),
	@SoDuBaoLanh numeric(13, 0),
	@SoTienHanMucDangKi numeric(13, 0),
	@SoDuHanMuc numeric(13, 0),
	@SoThuTuHanMuc varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@NgayKhaiBao datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuThanhToan] 
		SET
			[LoaiChungTu] = @LoaiChungTu,
			[LoaiHinhBaoLanh] = @LoaiHinhBaoLanh,
			[MaNganHangCungCapBaoLanh] = @MaNganHangCungCapBaoLanh,
			[NamPhatHanh] = @NamPhatHanh,
			[KiHieuChungTuPhatHanhBaoLanh] = @KiHieuChungTuPhatHanhBaoLanh,
			[SoChungTuPhatHanhBaoLanh] = @SoChungTuPhatHanhBaoLanh,
			[MaTienTe] = @MaTienTe,
			[MaNganHangTraThay] = @MaNganHangTraThay,
			[TenNganHangTraThay] = @TenNganHangTraThay,
			[MaDonViSuDungHanMuc] = @MaDonViSuDungHanMuc,
			[TenDonViSuDungHanMuc] = @TenDonViSuDungHanMuc,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[TenNganHangCungCapBaoLanh] = @TenNganHangCungCapBaoLanh,
			[SoToKhai] = @SoToKhai,
			[NgayDuDinhKhaiBao] = @NgayDuDinhKhaiBao,
			[MaLoaiHinh] = @MaLoaiHinh,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[TenCucHaiQuan] = @TenCucHaiQuan,
			[SoVanDon_1] = @SoVanDon_1,
			[SoVanDon_2] = @SoVanDon_2,
			[SoVanDon_3] = @SoVanDon_3,
			[SoVanDon_4] = @SoVanDon_4,
			[SoVanDon_5] = @SoVanDon_5,
			[SoHoaDon] = @SoHoaDon,
			[MaDonViSuDungBaoLanh] = @MaDonViSuDungBaoLanh,
			[TenDonViSuDungBaoLanh] = @TenDonViSuDungBaoLanh,
			[MaDonViDaiDienSuDungBaoLanh] = @MaDonViDaiDienSuDungBaoLanh,
			[TenDonViDaiDienSuDungBaoLanh] = @TenDonViDaiDienSuDungBaoLanh,
			[NgayBatDauHieuLuc] = @NgayBatDauHieuLuc,
			[NgayHetHieuLuc] = @NgayHetHieuLuc,
			[SoTienDangKyBaoLanh] = @SoTienDangKyBaoLanh,
			[CoBaoVoHieu] = @CoBaoVoHieu,
			[SoDuBaoLanh] = @SoDuBaoLanh,
			[SoTienHanMucDangKi] = @SoTienHanMucDangKi,
			[SoDuHanMuc] = @SoDuHanMuc,
			[SoThuTuHanMuc] = @SoThuTuHanMuc,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[NgayKhaiBao] = @NgayKhaiBao
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuThanhToan]
		(
			[LoaiChungTu],
			[LoaiHinhBaoLanh],
			[MaNganHangCungCapBaoLanh],
			[NamPhatHanh],
			[KiHieuChungTuPhatHanhBaoLanh],
			[SoChungTuPhatHanhBaoLanh],
			[MaTienTe],
			[MaNganHangTraThay],
			[TenNganHangTraThay],
			[MaDonViSuDungHanMuc],
			[TenDonViSuDungHanMuc],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoHieuPhatHanhHanMuc],
			[MaKetQuaXuLy],
			[TenNganHangCungCapBaoLanh],
			[SoToKhai],
			[NgayDuDinhKhaiBao],
			[MaLoaiHinh],
			[CoQuanHaiQuan],
			[TenCucHaiQuan],
			[SoVanDon_1],
			[SoVanDon_2],
			[SoVanDon_3],
			[SoVanDon_4],
			[SoVanDon_5],
			[SoHoaDon],
			[MaDonViSuDungBaoLanh],
			[TenDonViSuDungBaoLanh],
			[MaDonViDaiDienSuDungBaoLanh],
			[TenDonViDaiDienSuDungBaoLanh],
			[NgayBatDauHieuLuc],
			[NgayHetHieuLuc],
			[SoTienDangKyBaoLanh],
			[CoBaoVoHieu],
			[SoDuBaoLanh],
			[SoTienHanMucDangKi],
			[SoDuHanMuc],
			[SoThuTuHanMuc],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[TrangThaiXuLy],
			[NgayKhaiBao]
		)
		VALUES 
		(
			@LoaiChungTu,
			@LoaiHinhBaoLanh,
			@MaNganHangCungCapBaoLanh,
			@NamPhatHanh,
			@KiHieuChungTuPhatHanhBaoLanh,
			@SoChungTuPhatHanhBaoLanh,
			@MaTienTe,
			@MaNganHangTraThay,
			@TenNganHangTraThay,
			@MaDonViSuDungHanMuc,
			@TenDonViSuDungHanMuc,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoHieuPhatHanhHanMuc,
			@MaKetQuaXuLy,
			@TenNganHangCungCapBaoLanh,
			@SoToKhai,
			@NgayDuDinhKhaiBao,
			@MaLoaiHinh,
			@CoQuanHaiQuan,
			@TenCucHaiQuan,
			@SoVanDon_1,
			@SoVanDon_2,
			@SoVanDon_3,
			@SoVanDon_4,
			@SoVanDon_5,
			@SoHoaDon,
			@MaDonViSuDungBaoLanh,
			@TenDonViSuDungBaoLanh,
			@MaDonViDaiDienSuDungBaoLanh,
			@TenDonViDaiDienSuDungBaoLanh,
			@NgayBatDauHieuLuc,
			@NgayHetHieuLuc,
			@SoTienDangKyBaoLanh,
			@CoBaoVoHieu,
			@SoDuBaoLanh,
			@SoTienHanMucDangKi,
			@SoDuHanMuc,
			@SoThuTuHanMuc,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@TrangThaiXuLy,
			@NgayKhaiBao
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuThanhToan]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[LoaiHinhBaoLanh],
	[MaNganHangCungCapBaoLanh],
	[NamPhatHanh],
	[KiHieuChungTuPhatHanhBaoLanh],
	[SoChungTuPhatHanhBaoLanh],
	[MaTienTe],
	[MaNganHangTraThay],
	[TenNganHangTraThay],
	[MaDonViSuDungHanMuc],
	[TenDonViSuDungHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[MaKetQuaXuLy],
	[TenNganHangCungCapBaoLanh],
	[SoToKhai],
	[NgayDuDinhKhaiBao],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan],
	[SoVanDon_1],
	[SoVanDon_2],
	[SoVanDon_3],
	[SoVanDon_4],
	[SoVanDon_5],
	[SoHoaDon],
	[MaDonViSuDungBaoLanh],
	[TenDonViSuDungBaoLanh],
	[MaDonViDaiDienSuDungBaoLanh],
	[TenDonViDaiDienSuDungBaoLanh],
	[NgayBatDauHieuLuc],
	[NgayHetHieuLuc],
	[SoTienDangKyBaoLanh],
	[CoBaoVoHieu],
	[SoDuBaoLanh],
	[SoTienHanMucDangKi],
	[SoDuHanMuc],
	[SoThuTuHanMuc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiChungTu],
	[LoaiHinhBaoLanh],
	[MaNganHangCungCapBaoLanh],
	[NamPhatHanh],
	[KiHieuChungTuPhatHanhBaoLanh],
	[SoChungTuPhatHanhBaoLanh],
	[MaTienTe],
	[MaNganHangTraThay],
	[TenNganHangTraThay],
	[MaDonViSuDungHanMuc],
	[TenDonViSuDungHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[MaKetQuaXuLy],
	[TenNganHangCungCapBaoLanh],
	[SoToKhai],
	[NgayDuDinhKhaiBao],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan],
	[SoVanDon_1],
	[SoVanDon_2],
	[SoVanDon_3],
	[SoVanDon_4],
	[SoVanDon_5],
	[SoHoaDon],
	[MaDonViSuDungBaoLanh],
	[TenDonViSuDungBaoLanh],
	[MaDonViDaiDienSuDungBaoLanh],
	[TenDonViDaiDienSuDungBaoLanh],
	[NgayBatDauHieuLuc],
	[NgayHetHieuLuc],
	[SoTienDangKyBaoLanh],
	[CoBaoVoHieu],
	[SoDuBaoLanh],
	[SoTienHanMucDangKi],
	[SoDuHanMuc],
	[SoThuTuHanMuc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[NgayKhaiBao]
FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 13, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[LoaiHinhBaoLanh],
	[MaNganHangCungCapBaoLanh],
	[NamPhatHanh],
	[KiHieuChungTuPhatHanhBaoLanh],
	[SoChungTuPhatHanhBaoLanh],
	[MaTienTe],
	[MaNganHangTraThay],
	[TenNganHangTraThay],
	[MaDonViSuDungHanMuc],
	[TenDonViSuDungHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[MaKetQuaXuLy],
	[TenNganHangCungCapBaoLanh],
	[SoToKhai],
	[NgayDuDinhKhaiBao],
	[MaLoaiHinh],
	[CoQuanHaiQuan],
	[TenCucHaiQuan],
	[SoVanDon_1],
	[SoVanDon_2],
	[SoVanDon_3],
	[SoVanDon_4],
	[SoVanDon_5],
	[SoHoaDon],
	[MaDonViSuDungBaoLanh],
	[TenDonViSuDungBaoLanh],
	[MaDonViDaiDienSuDungBaoLanh],
	[TenDonViDaiDienSuDungBaoLanh],
	[NgayBatDauHieuLuc],
	[NgayHetHieuLuc],
	[SoTienDangKyBaoLanh],
	[CoBaoVoHieu],
	[SoDuBaoLanh],
	[SoTienHanMucDangKi],
	[SoDuHanMuc],
	[SoThuTuHanMuc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_VNACC_ChungTuThanhToan]	

GO

