﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.KDT.GC;
using System.Data;
using Company.KDT.SHARE.Components;

namespace Company.GC.BLL.VNACC
{
    public class ConvertFromVNACCS
    {

        public static Company.GC.BLL.KDT.ToKhaiMauDich ConvertToKhaiFromVNACCS(KDT_VNACC_ToKhaiMauDich tkmdVNACC, Company.GC.BLL.KDT.ToKhaiMauDich TKMD)
        {
            if (tkmdVNACC != null)
            {
                try
                {
                    //tkmdVNACC.LoadToKhai(tkmdVNACC.ID);
                    tkmdVNACC.LoadFull();
                    if (TKMD == null)
                        TKMD = new Company.GC.BLL.KDT.ToKhaiMauDich();
                    //TKMD.SoToKhai = //long.MaxValue tkmdVNACC.SoToKhai;
                    decimal SoTkVNACC = System.Convert.ToDecimal(tkmdVNACC.SoToKhai.ToString().Substring(0, 11));
                    TKMD.TrangThaiVNACCS = CapSoToKhai.CapSoToKhaiV5(SoTkVNACC, tkmdVNACC);
                    TKMD.SoToKhai = TKMD.TrangThaiVNACCS.SoTK;
                    TKMD.MaLoaiHinh = CapSoToKhai.ConvertMaLoaiHinh(tkmdVNACC.MaLoaiHinh);
                    TKMD.MaHaiQuan = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4MaHaiQuan(tkmdVNACC.CoQuanHaiQuan);
                    TKMD.NgayDangKy = tkmdVNACC.NgayDangKy;
                    TKMD.NgayTiepNhan = tkmdVNACC.NgayDangKy;
                    TKMD.MaDoanhNghiep = tkmdVNACC.MaDonVi;
                    TKMD.TenDoanhNghiep = tkmdVNACC.TenDonVi;
                    TKMD.MaDonViUT = tkmdVNACC.MaUyThac;
                    TKMD.TenDonViUT = tkmdVNACC.TenUyThac;
                    TKMD.TenDonViDoiTac = tkmdVNACC.TenDoiTac;
                    TKMD.MaDaiLyTTHQ = tkmdVNACC.MaDaiLyHQ;
                    TKMD.SoKien = tkmdVNACC.SoLuong;
                    TKMD.TrongLuong = tkmdVNACC.TrongLuong;
                    TKMD.DiaDiemXepHang = tkmdVNACC.TenDiaDiemXepHang;
                    TKMD.PTTT_ID = tkmdVNACC.PhuongThucTT;
                    TKMD.DKGH_ID = tkmdVNACC.MaDieuKienGiaHD;
                    TKMD.NguyenTe_ID = tkmdVNACC.MaTTHoaDon;
                    TKMD.PhiVanChuyen = tkmdVNACC.PhiVanChuyen;
                    TKMD.PhiBaoHiem = tkmdVNACC.PhiBaoHiem;
                    TKMD.DeXuatKhac = tkmdVNACC.GhiChu;
                    TKMD.PhanLuong = string.IsNullOrEmpty(tkmdVNACC.MaPhanLoaiKiemTra) ? "" : tkmdVNACC.MaPhanLoaiKiemTra.Substring(0, 1);
                    TKMD.TrangThaiXuLy = 1;
                    TKMD.IDHopDong = tkmdVNACC.HopDong_ID;
                    TKMD.LoaiHangHoa = tkmdVNACC.LoaiHang;
                    TKMD.NamDK = tkmdVNACC.NgayDangKy.Year;
                    if (string.IsNullOrEmpty(TKMD.LoaiHangHoa))
                    {
                        if (tkmdVNACC.HangCollection != null && tkmdVNACC.HangCollection.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(tkmdVNACC.HangCollection[0].MaQuanLy))
                            {
                                string loaiHang = tkmdVNACC.HangCollection[0].MaQuanLy;
                                 loaiHang = loaiHang.Trim();
                                if (loaiHang.StartsWith("1"))
                                    TKMD.LoaiHangHoa = "N";
                                else if (loaiHang.StartsWith("2"))
                                    TKMD.LoaiHangHoa = "S";
                                else if (loaiHang.StartsWith("3"))
                                    TKMD.LoaiHangHoa = "T";
                                else if (loaiHang.StartsWith("4"))
                                    TKMD.LoaiHangHoa = "H";
                            }
                        }
                    }
                    TKMD.TyGiaTinhThue = (tkmdVNACC.TyGiaCollection != null && tkmdVNACC.TyGiaCollection.Count > 0) ? tkmdVNACC.TyGiaCollection[0].TyGiaTinhThue : 1;
                    TKMD.SoVanDon = (tkmdVNACC.VanDonCollection != null && tkmdVNACC.VanDonCollection.Count > 0) ? tkmdVNACC.VanDonCollection[0].SoVanDon : "";
                    TKMD.NgayVanDon = tkmdVNACC.NgayHangDen;
                    int index = 0;
                    TKMD.ActionStatus = 5000;
                    if (TKMD.HMDCollection != null && TKMD.HMDCollection.Count > 0)
                        TKMD.DeleteHangCollection(null);
                    TKMD.HMDCollection = new List<Company.GC.BLL.KDT.HangMauDich>();
                    TKMD.LoaiVanDon = tkmdVNACC.SoToKhai.ToString();//SoTkVNACC.ToString();
                    foreach (KDT_VNACC_HangMauDich hmdvnacc in tkmdVNACC.HangCollection)
                    {
                        index++;
                        Company.GC.BLL.KDT.HangMauDich hmd = new Company.GC.BLL.KDT.HangMauDich();
                        //hmd.SoThuTuHang = index;
                        hmd.MaHS = hmdvnacc.MaSoHang;
                        hmd.TenHang = hmdvnacc.TenHang;
                        hmd.ThueSuatXNK = hmdvnacc.ThueSuat;
                        hmd.ThueSuatXNK = hmdvnacc.ThueSuatTuyetDoi;
                        hmd.NuocXX_ID = string.IsNullOrEmpty(hmdvnacc.NuocXuatXu) ? "VN" : hmdvnacc.NuocXuatXu;
                        hmd.SoLuong = hmdvnacc.SoLuong1;
                        hmd.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(hmdvnacc.DVTLuong1);
                        hmd.TriGiaKB = hmdvnacc.TriGiaHoaDon;
                        hmd.DonGiaKB = hmdvnacc.DonGiaHoaDon;
                        hmd.BieuThueXNK = hmdvnacc.MaBieuThueNK;
                        hmd.ThueSuatXNKGiam = hmdvnacc.SoTienGiamThue;
                        hmd.TriGiaTT = hmdvnacc.TriGiaTinhThue;
                        int stt = 0;
                        if (int.TryParse(hmdvnacc.SoDong, out stt))
                            hmd.SoThuTuHang = stt;
                        else
                            hmd.SoThuTuHang = index;

                        hmd.DonGiaTT = hmdvnacc.DonGiaTinhThue;
                        hmd.TriGiaTT = hmdvnacc.TriGiaTinhThueS == 0 ? hmdvnacc.TriGiaTinhThue : hmdvnacc.TriGiaTinhThueS;
                        hmd.ThueXNK = hmdvnacc.SoTienThue;
                        hmd.MaPhu = hmdvnacc.MaHangHoa;
                        TKMD.HMDCollection.Add(hmd);
                    }
                    return TKMD;
                }
                catch (System.Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }
            }
            return null;

        }
        public static ToKhaiChuyenTiep ConvertToKhaiFromVNACCS(KDT_VNACC_ToKhaiMauDich TKCTVNACC, ToKhaiChuyenTiep TKCT)
        {
            if (TKCTVNACC != null)
            {
                try
                {
                    //TKCTVNACC.LoadToKhai(TKCTVNACC.ID);
                    
                    TKCTVNACC.LoadFull();
                    if (TKCT == null)
                        TKCT = new ToKhaiChuyenTiep();
                    //TKCT.SoToKhai = //long.MaxValue TKCTVNACC.SoToKhai;
                    decimal SoTkVNACC = System.Convert.ToDecimal(TKCTVNACC.SoToKhai.ToString().Substring(0, 11));
                    TKCT.TrangThaiVNACCS = CapSoToKhai.CapSoToKhaiV5(SoTkVNACC, TKCTVNACC);
                    TKCT.SoToKhai = TKCT.TrangThaiVNACCS.SoTK;
                    TKCT.MaLoaiHinh = CapSoToKhai.ConvertMaLoaiHinh(TKCTVNACC.MaLoaiHinh);
                    TKCT.MaHaiQuanTiepNhan = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4MaHaiQuan(TKCTVNACC.CoQuanHaiQuan);
                    TKCT.NgayDangKy = TKCTVNACC.NgayDangKy;
                    TKCT.NgayTiepNhan = TKCTVNACC.NgayDangKy;
                    TKCT.MaDoanhNghiep = TKCTVNACC.MaDonVi;
                    //TKCT.Ten = TKCTVNACC.TenDonVi;
                    TKCT.MaDonViUT = TKCTVNACC.MaUyThac;
                    TKCT.TenDonViUT = TKCTVNACC.TenUyThac;
                    TKCT.TenDonViDoiTac = TKCTVNACC.TenDoiTac;
                    TKCT.MaDaiLyTTHQ = TKCTVNACC.MaDaiLyHQ;
                    TKCT.SoKien = TKCTVNACC.SoLuong;
                    TKCT.TrongLuong = TKCTVNACC.TrongLuong;
                    TKCT.DiaDiemXepHang = TKCTVNACC.TenDiaDiemXepHang;
                    TKCT.PTTT_ID = TKCTVNACC.PhuongThucTT;
                    TKCT.DKGH_ID = TKCTVNACC.MaDieuKienGiaHD;
                    TKCT.NguyenTe_ID = TKCTVNACC.MaTTHoaDon;
                    //TKCT.PhiVanChuyen = TKCTVNACC.PhiVanChuyen;
                    //TKCT.PhiBaoHiem = TKCTVNACC.PhiBaoHiem;
                    TKCT.DeXuatKhac = TKCTVNACC.GhiChu;
                    TKCT.PhanLuong = string.IsNullOrEmpty(TKCTVNACC.MaPhanLoaiKiemTra) ? "" : TKCTVNACC.MaPhanLoaiKiemTra.Substring(0, 1);
                    TKCT.TrangThaiXuLy = 1;
                    TKCT.IDHopDong = TKCTVNACC.HopDong_ID;
                    TKCT.LoaiHangHoa = TKCTVNACC.LoaiHang;
                    if (string.IsNullOrEmpty(TKCT.LoaiHangHoa))
                    {
                        if (TKCTVNACC.HangCollection != null && TKCTVNACC.HangCollection.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(TKCTVNACC.HangCollection[0].MaQuanLy))
                            {
                                string loaiHang = TKCTVNACC.HangCollection[0].MaQuanLy;
                                loaiHang = loaiHang.Trim();
                                if (loaiHang.StartsWith("1"))
                                    TKCT.LoaiHangHoa = "N";
                                else if (loaiHang.StartsWith("2"))
                                    TKCT.LoaiHangHoa = "S";
                                else if (loaiHang.StartsWith("3"))
                                    TKCT.LoaiHangHoa = "T";
                                else if (loaiHang.StartsWith("4"))
                                    TKCT.LoaiHangHoa = "H";
                            }
                        }
                    }
                    //TKCT.TyGiaTinhThue = (TKCTVNACC.TyGiaCollection != null && TKCTVNACC.TyGiaCollection.Count > 0) ? TKCTVNACC.TyGiaCollection[0].TyGiaTinhThue : 1;
                    //TKCT.SoVanDon = (TKCTVNACC.VanDonCollection != null && TKCTVNACC.VanDonCollection.Count > 0) ? TKCTVNACC.VanDonCollection[0].SoVanDon : "";
                    //TKCT.NgayVanDon = TKCTVNACC.NgayHangDen;
                    int index = 0;
                    TKCT.ActionStatus = 5000;
                    TKCT.Huongdan_PL = TKCTVNACC.SoToKhai.ToString();//SoTkVNACC.ToString();
                    if (TKCT.HCTCollection != null && TKCT.HCTCollection.Count > 0)
                    {
                        //Company.GC.BLL.GC.NguyenPhuLieuCollection ListNPLChange = new Company.GC.BLL.GC.NguyenPhuLieuCollection();
                        //BLL.GC.SanPhamCollection ListSPChange = new BLL.GC.SanPhamCollection();
                        //    if (TKCT.LoaiHangHoa.Contains("S"))
                        //    {
                        //        Company.GC.BLL.GC.NguyenPhuLieuCollection NPLCollectionDataTemp = new Company.GC.BLL.GC.NguyenPhuLieu().SelectCollectionBy_HopDong_ID();
                        //        BLL.GC.SanPhamCollection SPCollectionData = new Company.GC.BLL.GC.SanPham().SelectCollectionBy_HopDong_ID();

                        //        foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                        //        {
                                    #region Sản phẩm
                        //            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        //            foreach (Company.GC.BLL.GC.SanPham item in SPCollectionData)
                        //            {
                        //                if (item.Ma.Trim().ToUpper() == HCT.MaHang.Trim().ToUpper())
                        //                {
                        //                    sp = item;
                        //                    break;
                        //                }
                        //            }

                        //            if (!string.IsNullOrEmpty(sp.Ma))
                        //            {
                        //                if (TKCT.MaLoaiHinh.Contains("XV"))
                        //                {
                        //                    sp.SoLuongDaXuat = sp.SoLuongDaXuat - HCT.SoLuong; // Nếu là tờ khai xuất thì trừa lai số lượng xuất
                        //                }
                        //                else if (TKCT.MaLoaiHinh.Contains("NV"))
                        //                {
                        //                    sp.SoLuongDaXuat = sp.SoLuongDaXuat + HCT.SoLuong; // Nếu là tờ khai tái nhập thì cộng lại số lượng đã xuất
                        //                }
                        //                ListSPChange.Add(sp);

                        //            }
                                    #endregion
                                    #region Nguyên Phụ Liệu
                        //            DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCT.MaHang.Trim(), Math.Round(HCT.SoLuong, 8), TKCT.IDHopDong , 8); // Load NPL theo định mức

                        //            foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                        //            {
                        //                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //                string maNPL = row["MaNguyenPhuLieu"].ToString();
                        //                foreach (Company.GC.BLL.GC.NguyenPhuLieu item in NPLCollectionDataTemp)
                        //                {
                        //                    if (item.Ma.Trim().ToUpper() == maNPL.Trim().ToUpper())
                        //                    {
                        //                        npl = item;
                        //                        break;
                        //                    }

                        //                }
                        //                npl.SoLuongDaDung = npl.SoLuongDaDung - Math.Round(Convert.ToDecimal(row["LuongCanDung"]), 8);
                                        
                        //            }



                                    #endregion


                        //        }
                        //    }
                        TKCT.DeleteHangHoa();
                    }
                    TKCT.HCTCollection = new List<HangChuyenTiep>();
                    //TKCT.LoaiVanDon = SoTkVNACC.ToString();
                    foreach (KDT_VNACC_HangMauDich hmdvnacc in TKCTVNACC.HangCollection)
                    {
                        index++;
                        HangChuyenTiep hmd = new HangChuyenTiep();
                        //hmd.SoThuTuHang = index;
                        hmd.MaHS = hmdvnacc.MaSoHang;
                        hmd.TenHang = hmdvnacc.TenHang;
                        hmd.ThueSuatXNK = hmdvnacc.ThueSuat;
                        hmd.ThueSuatXNK = hmdvnacc.ThueSuatTuyetDoi;
                        hmd.ID_NuocXX = string.IsNullOrEmpty(hmdvnacc.NuocXuatXu) ? "VN" : hmdvnacc.NuocXuatXu;
                        hmd.SoLuong = hmdvnacc.SoLuong1;
                        hmd.ID_DVT = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(hmdvnacc.DVTLuong1);
                        hmd.TriGia = hmdvnacc.TriGiaHoaDon;
                        hmd.DonGia = hmdvnacc.DonGiaHoaDon;
                        //hmd. = hmdvnacc.MaBieuThueNK;
                        hmd.ThueSuatXNKGiam = hmdvnacc.SoTienGiamThue.ToString();
                        hmd.TriGiaTT = hmdvnacc.TriGiaTinhThue;
                        int stt = 0;
                        if (int.TryParse(hmdvnacc.SoDong, out stt))
                            hmd.SoThuTuHang = stt;
                        else
                            hmd.SoThuTuHang = index;

                        hmd.DonGiaTT = hmdvnacc.DonGiaTinhThue;
                        hmd.TriGiaTT = hmdvnacc.TriGiaTinhThueS == 0 ? hmdvnacc.TriGiaTinhThue : hmdvnacc.TriGiaTinhThueS;
                        hmd.ThueXNK = hmdvnacc.SoTienThue;
                        hmd.MaHang = hmdvnacc.MaHangHoa;
                        TKCT.HCTCollection.Add(hmd);
                    }
                    return TKCT;
                }
                catch (System.Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }
            }
            return null;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="soToKhaiVNACCS"> Số tờ khai 12 số</param>
        /// <param name="MaLoaiHinh"></param>
        /// <returns></returns>
        public static string HuyToKhai(decimal soToKhaiVNACCS, string MaLoaiHinhVNACCS)
        {
            decimal sotk = System.Convert.ToDecimal(soToKhaiVNACCS.ToString().Substring(0, 11));
            //bool isCapNhat = false;
            try
            {
          
             if (MaLoaiHinhVNACCS.Contains("E23") || MaLoaiHinhVNACCS.Contains("E54"))
            {
                ToKhaiChuyenTiep tkct = ToKhaiChuyenTiep.LoadToKhaiDaChuyenDoiVNACC(sotk);
                tkct.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                tkct.Update();
                 if(tkct != null && tkct.ID > 0)
                 {
                     if (MaLoaiHinhVNACCS.Contains("E54"))
                     {
                         if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                             return "Tờ khai chuyển tiếp có số = " + soToKhaiVNACCS + " này đã được phân bổ nên không thể xóa được.";
                     }
                     else
                     {
                         if(Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)tkct.SoToKhai, tkct.MaLoaiHinh, tkct.MaHaiQuanTiepNhan, (short)tkct.NgayDangKy.Year, tkct.IDHopDong))
                             return "Tờ khai chuyển tiếp có id= " + soToKhaiVNACCS + " đã được phân bổ nên không thể xóa được.";
                     }
                     PhuLucHopDong.DeleteBy_TKCT_ID(tkct.ID);
                     tkct.DeleteHangHoa();
                     tkct.Delete();
                 }
            }
             else
             {
                 Company.GC.BLL.KDT.ToKhaiMauDich tkmd = Company.GC.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
                 tkmd.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                 tkmd.Update();
                 if (tkmd != null && tkmd.ID > 0)
                 {
                     string MaLoaiHinhV4 = CapSoToKhai.ConvertMaLoaiHinh(MaLoaiHinhVNACCS);

                     if (MaLoaiHinhV4.Contains("XV"))
                     {
                         if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(tkmd.ID))
                             return "Tờ khai có số = " + soToKhaiVNACCS + " này đã được phân bổ nên không thể xóa được.";

                     }
                     else
                     {
                         if (Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, (short)tkmd.NgayDangKy.Year, tkmd.IDHopDong))
                             return "Tờ khai có số = " + soToKhaiVNACCS + " này đã được phân bổ nên không thể xóa được.";
                         Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase db = (Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase)Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
                         using (System.Data.SqlClient.SqlConnection connection = (System.Data.SqlClient.SqlConnection)db.CreateConnection())
                         {
                             connection.Open();
                             System.Data.SqlClient.SqlTransaction transaction = connection.BeginTransaction();
                             try
                             {
                                 Company.GC.BLL.GC.NPLNhapTonThucTe.DeleteNPLTonByToKhai(
                                               tkmd.SoToKhai,
                                               (short)tkmd.NgayDangKy.Year,
                                               tkmd.MaLoaiHinh,
                                               tkmd.MaHaiQuan, null);
                                 transaction.Commit();
                             }
                             catch (Exception ex)
                             {
                                 transaction.Rollback();
                                 Logger.LocalLogger.Instance().WriteMessage(ex);
                             }
                             finally
                             {
                                 connection.Close();
                             }
                         }
                     }
                     Company.KDT.SHARE.QuanLyChungTu.AnHanThue.DeleteBy_TKMD_ID(tkmd.ID);
                     Company.KDT.SHARE.QuanLyChungTu.SoContainer.DeleteBy_TKMD_ID(tkmd.ID);
                     Company.KDT.SHARE.QuanLyChungTu.DamBaoNghiaVuNT.DeleteBy_TKMD_ID(tkmd.ID);
                     tkmd.Delete();
                 }
             }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return ex.Message;
            }
             return string.Empty;
        }


        public static bool InsertUpdateTKMDFromVNACCS(KDT_VNACC_ToKhaiMauDich tkmdVNACC, bool ChuyenPhanBo)
        {
            decimal sotk = System.Convert.ToDecimal(tkmdVNACC.SoToKhai.ToString().Substring(0, 11));
            bool isCapNhat = false;
            if (tkmdVNACC.MaLoaiHinh.Contains("E23") || tkmdVNACC.MaLoaiHinh.Contains("E54"))
            {
                ToKhaiChuyenTiep tkct = ToKhaiChuyenTiep.LoadToKhaiDaChuyenDoiVNACC(sotk);
                if (tkct != null)
                {
                    tkct.LoadHCTCollection();
                    isCapNhat = true;
                }
                else
                    tkct = new ToKhaiChuyenTiep();
                try
                {
                    tkct = ConvertToKhaiFromVNACCS(tkmdVNACC, tkct);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw;
                }
                
                if (tkct != null)
                {
                    tkct.InsertUpdateFull();
                    if (isCapNhat)
                        tkct.CapNhatThongTinHangToKhaiSua(false);
                    else
                    {
                        if (ChuyenPhanBo)
                            tkct.TransferToNPLTon(tkct.NgayDangKy);
                    }
                    return true;
                }
                else
                    return false;
            }
            else
            {
                //Company.GC.BLL.KDT.ToKhaiMauDich tkmd = new Company.GC.BLL.KDT.ToKhaiMauDich();
               
                //Company.BLL.KDT.ToKhaiMauDichCollection listTKMD = Company.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
                Company.GC.BLL.KDT.ToKhaiMauDich tkmd = Company.GC.BLL.KDT.ToKhaiMauDich.LoadToKhaiDaChuyenDoiVNACC(sotk);
                
                if (tkmd != null)
                {
                    tkmd.LoadHMDCollection();
                    isCapNhat = true;
                }
                else
                    tkmd = new Company.GC.BLL.KDT.ToKhaiMauDich();

                tkmd = ConvertToKhaiFromVNACCS(tkmdVNACC, tkmd);

                if (tkmd != null)
                {
                    tkmd.InsertUpdateFull();

                    if (isCapNhat)
                        tkmd.CapNhatThongTinHangToKhaiSua(false);
                    else
                    {
                        if (ChuyenPhanBo)
                            tkmd.TransgferDataToNPLTonThucTe(tkmd.NgayDangKy);
                    }
                    //else
                    //    tkmd.TransgferDataToSXXK();

                    return true;
                }
                else
                    return false;

            }

        }

      

    }
}
