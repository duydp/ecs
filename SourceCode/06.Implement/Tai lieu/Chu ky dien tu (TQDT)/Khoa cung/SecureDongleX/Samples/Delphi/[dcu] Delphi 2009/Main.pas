unit Main;

interface

uses
    Windows, Messages, SysUtils,
    Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, SDX;

type
    TfrmMain = class(TForm)
    lstOutput: TListBox;
    btnWrite: TButton;
    btnRead: TButton;
    btnTransform: TButton;
    btnWriteMap: TButton;
    btnReadMap: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnWriteClick(Sender: TObject);
    procedure btnReadClick(Sender: TObject);
    procedure btnTransformClick(Sender: TObject);
    procedure btnWriteMapClick(Sender: TObject);
    procedure btnReadMapClick(Sender: TObject);
    private
        { Private declarations }
    public
        { Public declarations }
    end;

var
    frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.FormCreate(Sender: TObject);
begin
    lstOutput.Items.Clear();
end;

// Write to SecureDongle X with specified UID
procedure TfrmMain.btnWriteClick(Sender: TObject);
var
  retcode, handle, block_index : integer;
  uid, hid                     : Longword;
  stri                         : string;
begin
  retcode := SDX_Find();
  if (retcode < 0) then begin
    FmtStr(stri, 'Error Finding SecureDongle X: %x', [retcode]);
    lstOutput.Items.Add(stri);
    Exit;
  end;
  if (retcode = 0) then begin
    lstOutput.Items.Add('No SecureDongle X plugged');
    Exit;
  end;
  FmtStr(stri, 'Found SecureDongle X: %x', [retcode]);
  lstOutput.Items.Add(stri);

  stri := Inputbox('Input', 'Please input UID (i.e. 715400947)', '715400947');
  uid := StrToInt(stri);
	retcode := SDX_Open(1, uid, @hid);
	if retcode < 0 then	begin
    FmtStr(stri, 'Error: %x', [retcode]);
		lstOutput.Items.Add(stri);
    Exit;
  end else begin
    FmtStr(stri, 'Succeeded Opening SecureDongle X, UID: %d', [uid]);
		lstOutput.Items.Add(stri);
	end;

	handle := retcode;
  stri := Inputbox('Input', 'Please input write block index (0-4)', '0');
  block_index := StrToInt(stri);
  stri := Inputbox('Input', 'Please input data to be written (i.e. helloworld)', 'SDX - Sample Data');
	retcode := SDX_Write(handle, block_index, PAnsiChar(AnsiString(Stri)));
	if retcode < SDXERR_SUCCESS then begin
    FmtStr(stri, 'Error: %x', [retcode]);
    lstOutput.Items.Add(stri);
    Exit;
	end;
	lstOutput.Items.Add('Write: ' + stri);

	SDX_Close(handle);
end;

// Read SecureDongle X with specified UID
procedure TfrmMain.btnReadClick(Sender: TObject);
var
  retcode, handle, block_index : integer;
  uid, hid                     : cardinal;
  buffer                       : array [0..512] of ansichar;
  stri                         : string;
begin
  retcode := SDX_Find();
  if (retcode < SDXERR_SUCCESS) then begin
    FmtStr(stri, 'Error Finding SecureDongle X: %x', [retcode]);
    lstOutput.Items.Add(stri);
    Exit;
  end;
  if (retcode = 0) then begin
    lstOutput.Items.Add('No SecureDongle X plugged');
    Exit;
  end;
  FmtStr(stri, 'Found SecureDongle X: %x', [retcode]);
  lstOutput.Items.Add(stri);

  stri := Inputbox('Input', 'Please input UID (i.e. 715400947)', '715400947');
  uid := StrToInt(stri);
	retcode := SDX_Open(1, uid, @hid);
	if retcode < SDXERR_SUCCESS then	begin
    FmtStr(stri, 'Error: %x', [retcode]);
		lstOutput.Items.Add(stri);
    Exit;
  end else begin
    FmtStr(stri, 'Succeeded Opening SecureDongle X, UID: %d', [uid]);
		lstOutput.Items.Add(stri);
	end;

	handle := retcode;
  stri := Inputbox('Input', 'Please input read block index (0-4)', '0');
  block_index := StrToInt(stri);
  retcode := SDX_Read(handle, block_index, buffer);
  if retcode < SDXERR_SUCCESS then begin
    FmtStr(stri, 'Error: %x', [retcode]);
   	lstOutput.Items.Add(stri);
   	Exit;
  end;
  stri := 'Read: ' + buffer;
  lstOutput.Items.Add(stri);

  SDX_Close(handle);
end;

// Transform data using specified SecureDongle X
procedure TfrmMain.btnTransformClick(Sender: TObject);
var
  retcode, handle : integer;
  uid, hid        : cardinal;
  stri            : string;
  Pstri           : PAnsiChar;
begin
  retcode := SDX_Find();
  if (retcode < SDXERR_SUCCESS) then begin
    FmtStr(stri, 'Error Finding SecureDongle X: %x', [retcode]);
    lstOutput.Items.Add(stri);
    Exit;
  end;
  if (retcode = 0) then begin
    lstOutput.Items.Add('No SecureDongle X plugged');
    Exit;
  end;
  FmtStr(stri, 'Found SecureDongle X: %x', [retcode]);
  lstOutput.Items.Add(stri);

  stri := Inputbox('Input', 'Please input UID (i.e. 715400947)', '715400947');
  uid := StrToInt(stri);
	retcode := SDX_Open(1, uid, @hid);
	if retcode < SDXERR_SUCCESS then	begin
    FmtStr(stri, 'Error: %x', [retcode]);
		lstOutput.Items.Add(stri);
    Exit;
  end else begin
    FmtStr(stri, 'Succeeded Opening SecureDongle X, UID: %d', [uid]);
		lstOutput.Items.Add(stri);
	end;

	handle := retcode;
  stri := Inputbox('Input', 'Please input data (i.e. helloworld)', 'SDXTransform Test');
  PStri := PAnsiChar(AnsiString(stri));
  retcode := SDX_Transform(handle, Length(stri), PStri);
  if retcode < SDXERR_SUCCESS then begin
    FmtStr(stri, 'Error: %x', [retcode]);
   	lstOutput.Items.Add(stri);
   	Exit;
  end;
  lstOutput.Items.Add('Transform result: ' + PStri);

  SDX_Close(handle);
end;

// Write to SecureDongle X with specified UID
// This is a sample of simple Data Mapping to put plain text data not in order
// For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
procedure TfrmMain.btnWriteMapClick(Sender: TObject);
var
  retcode, handle, block_index : integer;
  uid, hid                     : cardinal;
  stri, len                    : String;
  temp                         : array [0..512] of byte;
  i, sze, indx                 : integer;
begin
  retcode := SDX_Find();
  if (retcode < SDXERR_SUCCESS) then begin
    FmtStr(stri, 'Error Finding SecureDongle X: %x', [retcode]);
    lstOutput.Items.Add(stri);
    Exit;
  end;
  if (retcode = 0) then begin
    lstOutput.Items.Add('No SecureDongle X plugged');
    Exit;
  end;
  FmtStr(stri, 'Found SecureDongle X: %x', [retcode]);
  lstOutput.Items.Add(stri);

  stri := Inputbox('Input', 'Please input UID (i.e. 715400947)', '715400947');
  uid := StrToInt(stri);
	retcode := SDX_Open(1, uid, @hid);
	if retcode < SDXERR_SUCCESS then	begin
    FmtStr(stri, 'Error: %x', [retcode]);
		lstOutput.Items.Add(stri);
    Exit;
  end else begin
    FmtStr(stri, 'Succeeded Opening SecureDongle X, UID: %d', [uid]);
		lstOutput.Items.Add(stri);
	end;

	handle := retcode;
  stri := Inputbox('Input', 'Please input write block index (0-4)', '0');
  block_index := StrToInt(stri);
  stri := Inputbox('Input', 'Please input data to be written (i.e. helloworld)', 'SDX - Sample Map Encryption');
  lstOutput.Items.Add('Original data: ' + stri);
  FmtStr(len, 'Length: %d', [Length(stri)]);
  lstOutput.Items.Add(len);

  //******************* ENCRYPTION SAMPLE ********************
  if Length(stri)*SizeOf(stri) > 512 then begin
    lstOutput.Items.Add('Error: Data size max is 512');
    Exit;
  end;

  // Create a 512-byte Full Map with randomized content
  for i := 0 to 512 do begin
    if ((Random(2) mod 2) = 0) then begin
      temp[i] := (Random(26) mod 26) + 65;
    end else begin
      temp[i] := (Random(26) mod 26) + 97;
    end;
  end;

  // Calculate where to put the data, and put it there
  for i := 0 to Length(stri) - 1 do begin
    sze := i * (Length(stri) - 1);
    indx := sze mod 512;
    temp[indx] := Ord(stri[i+1]);
  end;
  Stri := '';
  for i := 0 to 512 do begin
    Stri := Stri + Chr(temp[i]);
  end;
  //**********************************************************
	retcode := SDX_Write(handle, block_index, PAnsiChar(AnsiString(stri)));
	if retcode < SDXERR_SUCCESS then begin
    FmtStr(stri, 'Error: %x', [retcode]);
    lstOutput.Items.Add(stri);
    Exit;
	end;
	lstOutput.Items.Add('Write: ' + stri);

	SDX_Close(handle);
end;

procedure TfrmMain.btnReadMapClick(Sender: TObject);
var
  retcode, handle, block_index : integer;
  uid, hid                     : cardinal;
  buffer                       : array [0..512] of ansichar;
  stri, buff                   : string;
  pchars                       : PChar;
  i, sze, indx, datasize       : integer;
begin
  retcode := SDX_Find();
  if (retcode < SDXERR_SUCCESS) then begin
    FmtStr(stri, 'Error Finding SecureDongle X: %x', [retcode]);
    lstOutput.Items.Add(stri);
    Exit;
  end;
  if (retcode = 0) then begin
    lstOutput.Items.Add('No SecureDongle X plugged');
    Exit;
  end;
  FmtStr(stri, 'Found SecureDongle X: %x', [retcode]);
  lstOutput.Items.Add(stri);

  stri := Inputbox('Input', 'Please input UID (i.e. 715400947)', '715400947');
  uid := StrToInt(stri);
	retcode := SDX_Open(1, uid, @hid);
	if retcode < SDXERR_SUCCESS then	begin
    FmtStr(stri, 'Error: %x', [retcode]);
		lstOutput.Items.Add(stri);
    Exit;
  end else begin
    FmtStr(stri, 'Succeeded Opening SecureDongle X, UID: %d', [uid]);
		lstOutput.Items.Add(stri);
	end;

	handle := retcode;
  stri := Inputbox('Input', 'Please input write block index (0-4)', '0');
  block_index := StrToInt(stri);
  stri := InputBox('Input', 'Please enter size of the data to be read (1-512)', '');
  datasize := StrToInt(stri);
  if (datasize < 1) or (datasize > 512) then begin
    lstOutput.Items.Add('Data size wrong');
    Exit;
  end;

  retcode := SDX_Read(handle, block_index, buffer);
  if retcode < SDXERR_SUCCESS then begin
    FmtStr(stri, 'Error: %x', [retcode]);
   	lstOutput.Items.Add(stri);
   	Exit;
  end;

  // ******************* DECRYPTION **********************
  buff := '';
  for i := 0 to datasize - 1 do begin
    sze := i * (datasize - 1);
    indx := sze mod 512;
    buff := buff + Char(buffer[indx]);
  end;
  // *****************************************************

  pchars := PChar(buff);
  stri := 'Read: ' + pchars;
  lstOutput.Items.Add(stri);

  SDX_Close(handle);
end;

end.
