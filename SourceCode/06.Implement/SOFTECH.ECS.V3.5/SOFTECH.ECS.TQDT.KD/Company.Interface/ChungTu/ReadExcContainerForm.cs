﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Company.KD.BLL.KDT;
using Infragistics.Excel;
//using Company.KD.BLL.SXXK;
namespace Company.Interface
{
    public partial class ReadExcContainerForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        private string ConnectionStringExcel = "Driver={Microsoft Excel Driver(*.xls)};DriverId=790;Dbq=";
        public ReadExcContainerForm()
        {
            InitializeComponent();
        }

        private void editBox8_TextChanged(object sender, EventArgs e)
        {

        }
        //Hien thi Dialog box 
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        
        //private int checkNPLExit(string maNPL)
        //{
        //    for (int i = 0; i < this.TKMD.HMDCollection.Count; i++)
        //    {
        //        if (this.TKMD.HMDCollection[i].MaPhu.ToUpper() == maNPL.ToUpper()) return i;
        //    }
        //    return -1;
        //}
      
        private void txtTriGiaColumn_TextChanged(object sender, EventArgs e)
        {

        }



        private bool checkSoHieu(string soHieu)
        {
            foreach (Company.KDT.SHARE.QuanLyChungTu.Container c in TKMD.VanTaiDon.ContainerCollection)
                if (c.SoHieu.Trim().ToUpper() == soHieu.ToUpper().Trim())
                {
                    return true;
                }
            return false;
        }
      
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                error.SetIconPadding(txtRow, 8);
                return;

            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
               
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text.Trim()];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"MSG_EXC01","",txtSheet.Text, false);
                return;
            }
            //editing :
            WorksheetRowCollection wsrc = ws.Rows;
            char maSoHieuColunm = Convert.ToChar(txtSoHieu.Text.Trim());
            int SoHieuCol = ConvertCharToInt(maSoHieuColunm);

            char SoSealColunm = Convert.ToChar(txtSoSeal.Text.Trim());
            int SoSealCol = ConvertCharToInt(SoSealColunm);

            char LoaiConColumn = Convert.ToChar(txtLoaiCon.Text.Trim());
            int LoaiConCol = ConvertCharToInt(LoaiConColumn);

            char TrangThaiColumn = Convert.ToChar(txtTrangThai.Text.Trim());
            int TrangThaiCol = ConvertCharToInt(TrangThaiColumn);
            //int count = -1;
            int count = 0;
            foreach (WorksheetRow wsr in wsrc)
            {
                count++;
                if (wsr.Index >= beginRow)
                {
                    Company.KDT.SHARE.QuanLyChungTu.Container cont = new Company.KDT.SHARE.QuanLyChungTu.Container();

                    try
                    {
                        if (checkSoHieu(Convert.ToString(wsr.Cells[SoHieuCol].Value).Trim()))
                        {
                            ShowMessage("Số hiệu Container: \'" + wsr.Cells[SoHieuCol].Value + "\' container đã tồn tại trên lưới sẽ được bỏ qua", false);
                            continue;
                        }
                        cont.SoHieu = Convert.ToString(wsr.Cells[SoHieuCol].Value).Trim();
                        cont.Seal_No = Convert.ToString(wsr.Cells[SoSealCol].Value).Trim();
                        cont.LoaiContainer = Convert.ToString(wsr.Cells[LoaiConCol].Value).Trim();
                        cont.Trang_thai = Convert.ToInt32(wsr.Cells[TrangThaiCol].Value.ToString());
                        TKMD.VanTaiDon.ContainerCollection.Add(cont);
                        if (cont.LoaiContainer == "2")
                            TKMD.SoContainer20++;
                        else if(cont.LoaiContainer == "4")
                            TKMD.SoContainer40++;
                        this.SaveDefaultCon();
                    }
                    catch
                    {
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?","MSG_EXC06",Convert.ToString(wsr.Index + 1), true) != "Yes")
                        {
                            return;
                        }
                        this.SaveDefaultCon();
                    }
                }
            }
            if (TKMD.VanTaiDon.ContainerCollection.Count > 0)
            {
                MLMessages("Hoàn thành,nhập thành công " + TKMD.VanTaiDon.ContainerCollection.Count + " dòng Container trong tổng số " + count + " dòng trên file", "MSG_WRN34", "", false);
            }
            else
                MLMessages("Hoàn thành,Không có dòng Container được nhập","111","",false);
            this.SaveDefaultCon();
            this.Close();

        }
        private void SaveDefaultCon()
        {
            DocExcel docExcel = new DocExcel();
            docExcel.insertThongSo(txtSheet.Text.Trim(),
                                                                  txtRow.Text.Trim(),
                                                                  txtSoHieu.Text.Trim(),
                                                                  txtSoSeal.Text.Trim(),
                                                                  txtLoaiCon.Text.Trim(),
                                                                  txtTrangThai.Text.Trim()
                                                                 

                                                                  );

        }
        private void ReadDefault()
        {
            DocExcel docExcel = new DocExcel();
            docExcel.ReadDefaultCon(txtSheet,
                                                                  txtRow,
                                                                  txtSoHieu,
                                                                  txtSoSeal,
                                                                  txtLoaiCon,
                                                                  txtTrangThai 
                                                                 
                                                                  );
        }

        private void ReadExcelForm_Load(object sender, EventArgs e)
        {
            //openFileDialog1.InitialDirectory = Application.StartupPath;
            ReadDefault();
        }

        private void txtXuatXuColumn_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMaHSColumn_TextChanged(object sender, EventArgs e)
        {

        }
    }
}