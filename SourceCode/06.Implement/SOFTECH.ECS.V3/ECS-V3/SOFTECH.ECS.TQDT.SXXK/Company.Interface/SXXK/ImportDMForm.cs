using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using GemBox.Spreadsheet;
namespace Company.Interface.SXXK
{
    public partial class ImportDMForm : BaseForm
    {
        public DinhMucCollection DMCollection = new DinhMucCollection();
        public DinhMucCollection DMReadCollection = new DinhMucCollection();
        public DinhMucCollection DMChuaCoCollection = new DinhMucCollection();
        public ThongTinDinhMucCollection TTDMReadCollection = new ThongTinDinhMucCollection();
        public ThongTinDinhMucCollection TTDMChuaCoCollection = new ThongTinDinhMucCollection();
        public ThongTinDinhMucCollection TTDMCollection = new ThongTinDinhMucCollection();
        public ImportDMForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }

        private int checkDMExit1(string maSP, string maNPL)
        {
            for (int i = 0; i < this.DMCollection.Count; i++)
            {
                if (this.DMCollection[i].MaSanPHam.ToUpper() == maSP.ToUpper() && this.DMCollection[i].MaNguyenPhuLieu.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnSave.Enabled = false;
                if (chkSaveSetting.Checked == true)
                {
                    GlobalSettings.Luu_DM(txtMaSPColumn.Text, txtMaNPLColumn.Text, txtDMSDColumn.Text, txtTLHHColumn.Text, txtDMCColumn.Text);
                }
                new ThongTinDinhMuc().InsertUpdate(this.TTDMReadCollection);
                if (chkOverwrite.Checked == true)
                {
                    
                    new DinhMuc().InsertUpdate(this.DMReadCollection);
                    
                }
                else
                {
                    new DinhMuc().InsertUpdate(this.DMChuaCoCollection);
                   
                }
                this.DMCollection.AddRange(this.DMChuaCoCollection);
               // ShowMessage("Import thành công.", false);
                MLMessages("Nhập thành công.", "MSG_PUB02", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            finally
            {
                btnSave.Enabled = true;
                this.Cursor = Cursors.Default;
                this.Close();
            }

        }

        private void ImportDMForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            txtMaSPColumn.Text = GlobalSettings.DinhMuc.MaSP;
            txtMaNPLColumn.Text = GlobalSettings.DinhMuc.MaNPL;
            txtDMSDColumn.Text = GlobalSettings.DinhMuc.DinhMucSuDung;
            txtTLHHColumn.Text = GlobalSettings.DinhMuc.TyLeHH;
            txtDMCColumn.Text = GlobalSettings.DinhMuc.DinhMucChung;
            this.dgList.DataSource = this.DMReadCollection;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }
        private int checkTTDMExit1(string maSP)
        {
            for (int i = 0; i < this.TTDMCollection.Count; i++)
            {
                if (this.TTDMCollection[i].MaSanPham.ToUpper() == maSP.ToUpper()) return i;
            }
            return -1;
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {                    
                     error.SetError(txtRow, "Begin row must be greater than zero ");
                }
                error.SetIconPadding(txtRow, 8);
                return;

            }
            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text,true );
            }
            catch
            {
                //ShowMessage("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn và đóng file lại trước khi đọc.", false);
                MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
              //  ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            this.DMReadCollection.Clear();
            WorksheetRowCollection wsrc = ws.Rows;
            char maSPColumn = Convert.ToChar(txtMaSPColumn.Text);
            int maSPCol = ConvertCharToInt(maSPColumn);
            char maNPLColumn = Convert.ToChar(txtMaNPLColumn.Text);
            int maNPLCol = ConvertCharToInt(maNPLColumn);
            char DMSDColumn = Convert.ToChar(txtDMSDColumn.Text);
            int DMSDCol = ConvertCharToInt(DMSDColumn);
            char TLHHColumn = Convert.ToChar(txtTLHHColumn.Text);
            int TLHHCol = ConvertCharToInt(TLHHColumn);
            char DMCColumn = Convert.ToChar(txtDMCColumn.Text);
            int DMCCol = ConvertCharToInt(DMCColumn);
            //char MaHQColumn = Convert.ToChar(txtMaHQColumn.Text);
            //int MaHQCol = ConvertCharToInt(MaHQColumn);
            //char MaDVColumn = Convert.ToChar(txtMaDVColumn.Text);
            //int MaDVCol= ConvertCharToInt(MaDVColumn);
            this.DMReadCollection.Clear();
            string maSP = "";
            ThongTinDinhMuc TTDM = new ThongTinDinhMuc();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        if (maSP != Convert.ToString(wsr.Cells[maSPCol].Value).Trim())
                        {
                            TTDM = new ThongTinDinhMuc();
                            TTDM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            TTDM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            TTDM.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                            maSP = TTDM.MaSanPham;
                            if (checkTTDMExit1(TTDM.MaSanPham) >= 0)
                            {
                                TTDM.IsExist = 1;
                            }
                            else
                            {
                                this.TTDMChuaCoCollection.Add(TTDM);

                            }
                            this.TTDMReadCollection.Add(TTDM);
                        }
                        DinhMuc DM = new DinhMuc();
                        DM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        DM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        DM.MaSanPHam = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                        DM.MaNguyenPhuLieu = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                        DM.DinhMucSuDung = Convert.ToDecimal(wsr.Cells[DMSDCol].Value);
                        DM.TyLeHaoHut = Convert.ToDecimal(wsr.Cells[TLHHCol].Value);
                        DM.DinhMucChung = Convert.ToDecimal(wsr.Cells[DMCCol].Value);
                        if (checkDMExit1(DM.MaSanPHam, DM.MaNguyenPhuLieu) >= 0)
                        {
                            DM.IsExist = 1;                           
                        }
                        else
                        { 
                            this.DMChuaCoCollection.Add(DM);

                        }
                        this.DMReadCollection.Add(DM);
                    }
                    catch
                    {
                       // if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                        {
                            this.DMReadCollection.Clear();
                            this.uiButton1.Enabled = true;
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }
                }
            }
            dgList.DataSource = this.DMReadCollection;
            dgList.Refetch();
            this.uiButton1.Enabled = true;
            this.Cursor = Cursors.Default;
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }


    }
}