-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Insert]
	@Master_ID bigint,
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayDangKy datetime,
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(11, 0),
	@SoTienBaoLanh numeric(11, 0),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(11, 0),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(11, 0),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
(
	[Master_ID],
	[PhanLoaiBaoCaoSuaDoi],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayDangKy],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra]
)
VALUES 
(
	@Master_ID,
	@PhanLoaiBaoCaoSuaDoi,
	@MaPhanLoaiKiemTra,
	@MaSoThueDaiDien,
	@TenCoQuanHaiQuan,
	@NgayDangKy,
	@NgayThayDoiDangKy,
	@BieuThiTruongHopHetHan,
	@TenDaiLyHaiQuan,
	@MaNhanVienHaiQuan,
	@TenDDLuuKho,
	@MaPhanLoaiTongGiaCoBan,
	@PhanLoaiCongThucChuan,
	@MaPhanLoaiDieuChinhTriGia,
	@PhuongPhapDieuChinhTriGia,
	@TongTienThuePhaiNop,
	@SoTienBaoLanh,
	@TenTruongDonViHaiQuan,
	@NgayCapPhep,
	@PhanLoaiThamTraSauThongQuan,
	@NgayPheDuyetBP,
	@NgayHoanThanhKiemTraBP,
	@SoNgayDoiCapPhepNhapKhau,
	@TieuDe,
	@MaSacThueAnHan_VAT,
	@TenSacThueAnHan_VAT,
	@HanNopThueSauKhiAnHan_VAT,
	@PhanLoaiNopThue,
	@TongSoTienThueXuatKhau,
	@MaTTTongTienThueXuatKhau,
	@TongSoTienLePhi,
	@MaTTCuaSoTienBaoLanh,
	@SoQuanLyNguoiSuDung,
	@NgayHoanThanhKiemTra
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Update]
	@ID bigint,
	@Master_ID bigint,
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayDangKy datetime,
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(11, 0),
	@SoTienBaoLanh numeric(11, 0),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(11, 0),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(11, 0),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
SET
	[Master_ID] = @Master_ID,
	[PhanLoaiBaoCaoSuaDoi] = @PhanLoaiBaoCaoSuaDoi,
	[MaPhanLoaiKiemTra] = @MaPhanLoaiKiemTra,
	[MaSoThueDaiDien] = @MaSoThueDaiDien,
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[NgayDangKy] = @NgayDangKy,
	[NgayThayDoiDangKy] = @NgayThayDoiDangKy,
	[BieuThiTruongHopHetHan] = @BieuThiTruongHopHetHan,
	[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
	[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
	[TenDDLuuKho] = @TenDDLuuKho,
	[MaPhanLoaiTongGiaCoBan] = @MaPhanLoaiTongGiaCoBan,
	[PhanLoaiCongThucChuan] = @PhanLoaiCongThucChuan,
	[MaPhanLoaiDieuChinhTriGia] = @MaPhanLoaiDieuChinhTriGia,
	[PhuongPhapDieuChinhTriGia] = @PhuongPhapDieuChinhTriGia,
	[TongTienThuePhaiNop] = @TongTienThuePhaiNop,
	[SoTienBaoLanh] = @SoTienBaoLanh,
	[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
	[NgayCapPhep] = @NgayCapPhep,
	[PhanLoaiThamTraSauThongQuan] = @PhanLoaiThamTraSauThongQuan,
	[NgayPheDuyetBP] = @NgayPheDuyetBP,
	[NgayHoanThanhKiemTraBP] = @NgayHoanThanhKiemTraBP,
	[SoNgayDoiCapPhepNhapKhau] = @SoNgayDoiCapPhepNhapKhau,
	[TieuDe] = @TieuDe,
	[MaSacThueAnHan_VAT] = @MaSacThueAnHan_VAT,
	[TenSacThueAnHan_VAT] = @TenSacThueAnHan_VAT,
	[HanNopThueSauKhiAnHan_VAT] = @HanNopThueSauKhiAnHan_VAT,
	[PhanLoaiNopThue] = @PhanLoaiNopThue,
	[TongSoTienThueXuatKhau] = @TongSoTienThueXuatKhau,
	[MaTTTongTienThueXuatKhau] = @MaTTTongTienThueXuatKhau,
	[TongSoTienLePhi] = @TongSoTienLePhi,
	[MaTTCuaSoTienBaoLanh] = @MaTTCuaSoTienBaoLanh,
	[SoQuanLyNguoiSuDung] = @SoQuanLyNguoiSuDung,
	[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayDangKy datetime,
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(11, 0),
	@SoTienBaoLanh numeric(11, 0),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(11, 0),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(11, 0),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi] 
		SET
			[Master_ID] = @Master_ID,
			[PhanLoaiBaoCaoSuaDoi] = @PhanLoaiBaoCaoSuaDoi,
			[MaPhanLoaiKiemTra] = @MaPhanLoaiKiemTra,
			[MaSoThueDaiDien] = @MaSoThueDaiDien,
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[NgayDangKy] = @NgayDangKy,
			[NgayThayDoiDangKy] = @NgayThayDoiDangKy,
			[BieuThiTruongHopHetHan] = @BieuThiTruongHopHetHan,
			[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
			[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
			[TenDDLuuKho] = @TenDDLuuKho,
			[MaPhanLoaiTongGiaCoBan] = @MaPhanLoaiTongGiaCoBan,
			[PhanLoaiCongThucChuan] = @PhanLoaiCongThucChuan,
			[MaPhanLoaiDieuChinhTriGia] = @MaPhanLoaiDieuChinhTriGia,
			[PhuongPhapDieuChinhTriGia] = @PhuongPhapDieuChinhTriGia,
			[TongTienThuePhaiNop] = @TongTienThuePhaiNop,
			[SoTienBaoLanh] = @SoTienBaoLanh,
			[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
			[NgayCapPhep] = @NgayCapPhep,
			[PhanLoaiThamTraSauThongQuan] = @PhanLoaiThamTraSauThongQuan,
			[NgayPheDuyetBP] = @NgayPheDuyetBP,
			[NgayHoanThanhKiemTraBP] = @NgayHoanThanhKiemTraBP,
			[SoNgayDoiCapPhepNhapKhau] = @SoNgayDoiCapPhepNhapKhau,
			[TieuDe] = @TieuDe,
			[MaSacThueAnHan_VAT] = @MaSacThueAnHan_VAT,
			[TenSacThueAnHan_VAT] = @TenSacThueAnHan_VAT,
			[HanNopThueSauKhiAnHan_VAT] = @HanNopThueSauKhiAnHan_VAT,
			[PhanLoaiNopThue] = @PhanLoaiNopThue,
			[TongSoTienThueXuatKhau] = @TongSoTienThueXuatKhau,
			[MaTTTongTienThueXuatKhau] = @MaTTTongTienThueXuatKhau,
			[TongSoTienLePhi] = @TongSoTienLePhi,
			[MaTTCuaSoTienBaoLanh] = @MaTTCuaSoTienBaoLanh,
			[SoQuanLyNguoiSuDung] = @SoQuanLyNguoiSuDung,
			[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
		(
			[Master_ID],
			[PhanLoaiBaoCaoSuaDoi],
			[MaPhanLoaiKiemTra],
			[MaSoThueDaiDien],
			[TenCoQuanHaiQuan],
			[NgayDangKy],
			[NgayThayDoiDangKy],
			[BieuThiTruongHopHetHan],
			[TenDaiLyHaiQuan],
			[MaNhanVienHaiQuan],
			[TenDDLuuKho],
			[MaPhanLoaiTongGiaCoBan],
			[PhanLoaiCongThucChuan],
			[MaPhanLoaiDieuChinhTriGia],
			[PhuongPhapDieuChinhTriGia],
			[TongTienThuePhaiNop],
			[SoTienBaoLanh],
			[TenTruongDonViHaiQuan],
			[NgayCapPhep],
			[PhanLoaiThamTraSauThongQuan],
			[NgayPheDuyetBP],
			[NgayHoanThanhKiemTraBP],
			[SoNgayDoiCapPhepNhapKhau],
			[TieuDe],
			[MaSacThueAnHan_VAT],
			[TenSacThueAnHan_VAT],
			[HanNopThueSauKhiAnHan_VAT],
			[PhanLoaiNopThue],
			[TongSoTienThueXuatKhau],
			[MaTTTongTienThueXuatKhau],
			[TongSoTienLePhi],
			[MaTTCuaSoTienBaoLanh],
			[SoQuanLyNguoiSuDung],
			[NgayHoanThanhKiemTra]
		)
		VALUES 
		(
			@Master_ID,
			@PhanLoaiBaoCaoSuaDoi,
			@MaPhanLoaiKiemTra,
			@MaSoThueDaiDien,
			@TenCoQuanHaiQuan,
			@NgayDangKy,
			@NgayThayDoiDangKy,
			@BieuThiTruongHopHetHan,
			@TenDaiLyHaiQuan,
			@MaNhanVienHaiQuan,
			@TenDDLuuKho,
			@MaPhanLoaiTongGiaCoBan,
			@PhanLoaiCongThucChuan,
			@MaPhanLoaiDieuChinhTriGia,
			@PhuongPhapDieuChinhTriGia,
			@TongTienThuePhaiNop,
			@SoTienBaoLanh,
			@TenTruongDonViHaiQuan,
			@NgayCapPhep,
			@PhanLoaiThamTraSauThongQuan,
			@NgayPheDuyetBP,
			@NgayHoanThanhKiemTraBP,
			@SoNgayDoiCapPhepNhapKhau,
			@TieuDe,
			@MaSacThueAnHan_VAT,
			@TenSacThueAnHan_VAT,
			@HanNopThueSauKhiAnHan_VAT,
			@PhanLoaiNopThue,
			@TongSoTienThueXuatKhau,
			@MaTTTongTienThueXuatKhau,
			@TongSoTienLePhi,
			@MaTTCuaSoTienBaoLanh,
			@SoQuanLyNguoiSuDung,
			@NgayHoanThanhKiemTra
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[PhanLoaiBaoCaoSuaDoi],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayDangKy],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[PhanLoaiBaoCaoSuaDoi],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayDangKy],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_PhanHoi_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[PhanLoaiBaoCaoSuaDoi],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayDangKy],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]	

GO

