﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public class HangMauDich
    {
        #region Private members.

        protected long _ID;
        protected long _TKMD_ID;
        protected int _SoThuTuHang;
        protected string _MaHS = String.Empty;
        protected string _MaPhu = String.Empty;
        protected string _TenHang = String.Empty;
        protected string _NuocXX_ID = String.Empty;
        protected string _DVT_ID = String.Empty;
        protected decimal _SoLuong;
        protected decimal _TrongLuong;
        protected double _DonGiaKB;
        protected double _DonGiaTT;
        protected double _TriGiaKB;
        protected double _TriGiaTT;
        protected double _TriGiaKB_VND;
        protected double _ThueSuatXNK;
        protected double _ThueSuatTTDB;
        protected double _ThueSuatGTGT;
        protected double _ThueXNK;
        protected double _ThueTTDB;
        protected double _ThueGTGT;
        protected double _PhuThu;
        protected double _TyLeThuKhac;
        protected double _TriGiaThuKhac;
        protected byte _MienThue;
        protected string _Ma_HTS = String.Empty;
        protected string _DVT_HTS = String.Empty;
        protected decimal _SoLuong_HTS;
        protected string _NhomHang = string.Empty;
        protected string _ThueSuatXNKGiam = string.Empty;
        protected string _ThueSuatTTDBGiam = string.Empty;
        protected string _ThueSuatVATGiam = string.Empty;
        protected double _DonGiaTuyetDoi;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.
        public string NhomHang
        {
            set { this._NhomHang = value; }
            get { return this._NhomHang; }
        }
        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long TKMD_ID
        {
            set { this._TKMD_ID = value; }
            get { return this._TKMD_ID; }
        }
        public int SoThuTuHang
        {
            set { this._SoThuTuHang = value; }
            get { return this._SoThuTuHang; }
        }
        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }
        public string MaPhu
        {
            set { this._MaPhu = value; }
            get { return this._MaPhu; }
        }
        public string TenHang
        {
            set { this._TenHang = value; }
            get { return this._TenHang; }
        }
        public string NuocXX_ID
        {
            set { this._NuocXX_ID = value; }
            get { return this._NuocXX_ID; }
        }
        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }
        public decimal SoLuong
        {
            set { this._SoLuong = value; }
            get { return this._SoLuong; }
        }
        public decimal TrongLuong
        {
            set { this._TrongLuong = value; }
            get { return this._TrongLuong; }
        }
        public double DonGiaKB
        {
            set { this._DonGiaKB = value; }
            get { return this._DonGiaKB; }
        }
        public double DonGiaTT
        {
            set { this._DonGiaTT = value; }
            get { return this._DonGiaTT; }
        }
        public double TriGiaKB
        {
            set { this._TriGiaKB = value; }
            get { return this._TriGiaKB; }
        }
        public double TriGiaTT
        {
            set { this._TriGiaTT = value; }
            get { return this._TriGiaTT; }
        }
        public double TriGiaKB_VND
        {
            set { this._TriGiaKB_VND = value; }
            get { return this._TriGiaKB_VND; }
        }
        public double ThueSuatXNK
        {
            set { this._ThueSuatXNK = value; }
            get { return this._ThueSuatXNK; }
        }
        public double ThueSuatTTDB
        {
            set { this._ThueSuatTTDB = value; }
            get { return this._ThueSuatTTDB; }
        }
        public double ThueSuatGTGT
        {
            set { this._ThueSuatGTGT = value; }
            get { return this._ThueSuatGTGT; }
        }
        public double ThueXNK
        {
            set { this._ThueXNK = value; }
            get { return this._ThueXNK; }
        }
        public double ThueTTDB
        {
            set { this._ThueTTDB = value; }
            get { return this._ThueTTDB; }
        }
        public double ThueGTGT
        {
            set { this._ThueGTGT = value; }
            get { return this._ThueGTGT; }
        }
        public double PhuThu
        {
            set { this._PhuThu = value; }
            get { return this._PhuThu; }
        }
        public double TyLeThuKhac
        {
            set { this._TyLeThuKhac = value; }
            get { return this._TyLeThuKhac; }
        }
        public double TriGiaThuKhac
        {
            set { this._TriGiaThuKhac = value; }
            get { return this._TriGiaThuKhac; }
        }
        public byte MienThue
        {
            set { this._MienThue = value; }
            get { return this._MienThue; }
        }
        public string Ma_HTS
        {
            set { this._Ma_HTS = value; }
            get { return this._Ma_HTS; }
        }
        public string DVT_HTS
        {
            set { this._DVT_HTS = value; }
            get { return this._DVT_HTS; }
        }
        public decimal SoLuong_HTS
        {
            set { this._SoLuong_HTS = value; }
            get { return this._SoLuong_HTS; }
        }
        public string ThueSuatXNKGiam
        {
            set { this._ThueSuatXNKGiam = value; }
            get { return this._ThueSuatXNKGiam; }
        }
        public string ThueSuatTTDBGiam
        {
            set { this._ThueSuatTTDBGiam = value; }
            get { return this._ThueSuatTTDBGiam; }
        }
        public string ThueSuatVATGiam
        {
            set { this._ThueSuatVATGiam = value; }
            get { return this._ThueSuatVATGiam; }
        }

        public double DonGiaTuyetDoi
        {
            set { this._DonGiaTuyetDoi = value; }
            get { return this._DonGiaTuyetDoi; }
        }
        //---------------------------------------------------------------------------------------------


        #endregion
    }
}
