﻿namespace Company.Interface
{
    partial class GiayPhepForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiayPhepForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtThongTinKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTenCQCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaCQCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDVdcCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaDVdcCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNoiCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNguoiCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ccNgayHHGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayGiayPhep = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoGiayPhep = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvNgayHH = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNoiCap = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDVdcCap = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenCQCap = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.btnChonGP = new Janus.Windows.EditControls.UIButton();
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnKhaiBao = new Janus.Windows.EditControls.UIButton();
            this.btnLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            this.btnKetQuaXuLy = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayGiayPhep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoGiayPhep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNoiCap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVdcCap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCQCap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnKhaiBao);
            this.grbMain.Controls.Add(this.btnLayPhanHoi);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Controls.Add(this.btnChonGP);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.btnAddNew);
            this.grbMain.Size = new System.Drawing.Size(693, 513);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtThongTinKhac);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtTenCQCap);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.txtMaCQCap);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtTenDVdcCap);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtMaDVdcCap);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.txtNoiCap);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtNguoiCap);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.ccNgayHHGiayPhep);
            this.uiGroupBox2.Controls.Add(this.txtSoGiayPhep);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayGiayPhep);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(9, 84);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(671, 202);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtThongTinKhac
            // 
            this.txtThongTinKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThongTinKhac.Location = new System.Drawing.Point(114, 127);
            this.txtThongTinKhac.MaxLength = 255;
            this.txtThongTinKhac.Multiline = true;
            this.txtThongTinKhac.Name = "txtThongTinKhac";
            this.txtThongTinKhac.Size = new System.Drawing.Size(539, 69);
            this.txtThongTinKhac.TabIndex = 10;
            this.txtThongTinKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThongTinKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtThongTinKhac.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Thông tin khác";
            // 
            // txtTenCQCap
            // 
            this.txtTenCQCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenCQCap.Location = new System.Drawing.Point(367, 101);
            this.txtTenCQCap.MaxLength = 255;
            this.txtTenCQCap.Name = "txtTenCQCap";
            this.txtTenCQCap.Size = new System.Drawing.Size(286, 21);
            this.txtTenCQCap.TabIndex = 9;
            this.txtTenCQCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenCQCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenCQCap.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(259, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Tên cơ quan cấp";
            // 
            // txtMaCQCap
            // 
            this.txtMaCQCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaCQCap.Location = new System.Drawing.Point(114, 101);
            this.txtMaCQCap.MaxLength = 255;
            this.txtMaCQCap.Name = "txtMaCQCap";
            this.txtMaCQCap.Size = new System.Drawing.Size(136, 21);
            this.txtMaCQCap.TabIndex = 8;
            this.txtMaCQCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaCQCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaCQCap.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Mã cơ quan cấp";
            // 
            // txtTenDVdcCap
            // 
            this.txtTenDVdcCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDVdcCap.Location = new System.Drawing.Point(367, 74);
            this.txtTenDVdcCap.MaxLength = 255;
            this.txtTenDVdcCap.Name = "txtTenDVdcCap";
            this.txtTenDVdcCap.Size = new System.Drawing.Size(286, 21);
            this.txtTenDVdcCap.TabIndex = 7;
            this.txtTenDVdcCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDVdcCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDVdcCap.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(259, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Tên đơn vị được cấp";
            // 
            // txtMaDVdcCap
            // 
            this.txtMaDVdcCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDVdcCap.Location = new System.Drawing.Point(114, 74);
            this.txtMaDVdcCap.MaxLength = 255;
            this.txtMaDVdcCap.Name = "txtMaDVdcCap";
            this.txtMaDVdcCap.Size = new System.Drawing.Size(136, 21);
            this.txtMaDVdcCap.TabIndex = 6;
            this.txtMaDVdcCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDVdcCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDVdcCap.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Mã đơn vị được cấp";
            // 
            // txtNoiCap
            // 
            this.txtNoiCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoiCap.Location = new System.Drawing.Point(367, 47);
            this.txtNoiCap.MaxLength = 255;
            this.txtNoiCap.Name = "txtNoiCap";
            this.txtNoiCap.Size = new System.Drawing.Size(286, 21);
            this.txtNoiCap.TabIndex = 5;
            this.txtNoiCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNoiCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNoiCap.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(259, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nơi cấp";
            // 
            // txtNguoiCap
            // 
            this.txtNguoiCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiCap.Location = new System.Drawing.Point(114, 47);
            this.txtNguoiCap.MaxLength = 255;
            this.txtNguoiCap.Name = "txtNguoiCap";
            this.txtNguoiCap.Size = new System.Drawing.Size(136, 21);
            this.txtNguoiCap.TabIndex = 4;
            this.txtNguoiCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNguoiCap.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(485, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ngày hết hạn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Người cấp";
            // 
            // ccNgayHHGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayHHGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayHHGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHGiayPhep.IsNullDate = true;
            this.ccNgayHHGiayPhep.Location = new System.Drawing.Point(563, 19);
            this.ccNgayHHGiayPhep.Name = "ccNgayHHGiayPhep";
            this.ccNgayHHGiayPhep.Nullable = true;
            this.ccNgayHHGiayPhep.NullButtonText = "Xóa";
            this.ccNgayHHGiayPhep.ShowNullButton = true;
            this.ccNgayHHGiayPhep.Size = new System.Drawing.Size(90, 21);
            this.ccNgayHHGiayPhep.TabIndex = 3;
            this.ccNgayHHGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayHHGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGiayPhep.Location = new System.Drawing.Point(114, 20);
            this.txtSoGiayPhep.MaxLength = 255;
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(136, 21);
            this.txtSoGiayPhep.TabIndex = 1;
            this.txtSoGiayPhep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(259, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày giấy phép";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(8, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(69, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số giấy phép";
            // 
            // ccNgayGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiayPhep.IsNullDate = true;
            this.ccNgayGiayPhep.Location = new System.Drawing.Point(367, 21);
            this.ccNgayGiayPhep.Name = "ccNgayGiayPhep";
            this.ccNgayGiayPhep.Nullable = true;
            this.ccNgayGiayPhep.NullButtonText = "Xóa";
            this.ccNgayGiayPhep.ShowNullButton = true;
            this.ccNgayGiayPhep.Size = new System.Drawing.Size(90, 21);
            this.ccNgayGiayPhep.TabIndex = 2;
            this.ccNgayGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.AutoEdit = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(9, 292);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(672, 135);
            this.dgList.TabIndex = 5;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(10, 433);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(88, 23);
            this.btnXoa.TabIndex = 4;
            this.btnXoa.Text = "Xoá hàng";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(606, 433);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(428, 433);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(97, 23);
            this.btnAddNew.TabIndex = 1;
            this.btnAddNew.Text = "Chọn hàng";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(531, 433);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(69, 23);
            this.btnGhi.TabIndex = 2;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvNgayGiayPhep
            // 
            this.rfvNgayGiayPhep.ControlToValidate = this.ccNgayGiayPhep;
            this.rfvNgayGiayPhep.ErrorMessage = "\"Ngày giấy phép\" không được bỏ trống.";
            this.rfvNgayGiayPhep.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayGiayPhep.Icon")));
            this.rfvNgayGiayPhep.Tag = "rfvNgayGiayPhep";
            // 
            // rfvSoGiayPhep
            // 
            this.rfvSoGiayPhep.ControlToValidate = this.txtSoGiayPhep;
            this.rfvSoGiayPhep.ErrorMessage = "\"Số giấy phép\" không được để trống.";
            this.rfvSoGiayPhep.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoGiayPhep.Icon")));
            this.rfvSoGiayPhep.Tag = "rfvSoGiayPhep";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvNgayHH
            // 
            this.rfvNgayHH.ControlToValidate = this.ccNgayHHGiayPhep;
            this.rfvNgayHH.ErrorMessage = "\"Ngày hết hạn giấy phép\" không được bỏ trống.";
            this.rfvNgayHH.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayHH.Icon")));
            this.rfvNgayHH.Tag = "rfvNgayHH";
            // 
            // rfvNoiCap
            // 
            this.rfvNoiCap.ControlToValidate = this.txtNoiCap;
            this.rfvNoiCap.ErrorMessage = "\"Nơi cấp\" không được bỏ trống.";
            this.rfvNoiCap.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNoiCap.Icon")));
            this.rfvNoiCap.Tag = "rfvNoiCap";
            // 
            // rfvTenDVdcCap
            // 
            this.rfvTenDVdcCap.ControlToValidate = this.txtTenDVdcCap;
            this.rfvTenDVdcCap.ErrorMessage = "\"Tên đơn vị được cấp\" không được bỏ trống.";
            this.rfvTenDVdcCap.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDVdcCap.Icon")));
            this.rfvTenDVdcCap.Tag = "rfvTenDVdcCap";
            // 
            // rfvTenCQCap
            // 
            this.rfvTenCQCap.ControlToValidate = this.txtTenCQCap;
            this.rfvTenCQCap.ErrorMessage = "\"Tên cơ quan cấp\" không được bỏ trống.";
            this.rfvTenCQCap.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenCQCap.Icon")));
            this.rfvTenCQCap.Tag = "rfvTenCQCap";
            // 
            // btnChonGP
            // 
            this.btnChonGP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChonGP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonGP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChonGP.Icon")));
            this.btnChonGP.Location = new System.Drawing.Point(104, 433);
            this.btnChonGP.Name = "btnChonGP";
            this.btnChonGP.Size = new System.Drawing.Size(149, 23);
            this.btnChonGP.TabIndex = 4;
            this.btnChonGP.Text = "Chọn giấy phép đã có";
            this.btnChonGP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnChonGP.VisualStyleManager = this.vsmMain;
            this.btnChonGP.Click += new System.EventHandler(this.btnChonGP_Click);
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXuLy);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.label9);
            this.grpTiepNhan.Controls.Add(this.label10);
            this.grpTiepNhan.Controls.Add(this.label11);
            this.grpTiepNhan.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(0, 0);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(693, 68);
            this.grpTiepNhan.TabIndex = 24;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(407, 15);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(141, 21);
            this.ccNgayTiepNhan.TabIndex = 2;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(121, 15);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(142, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(21, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Trạng thái";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(305, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Ngày tiếp nhận";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số tiếp nhận";
            // 
            // btnKhaiBao
            // 
            this.btnKhaiBao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKhaiBao.Icon")));
            this.btnKhaiBao.Location = new System.Drawing.Point(129, 462);
            this.btnKhaiBao.Name = "btnKhaiBao";
            this.btnKhaiBao.Size = new System.Drawing.Size(88, 23);
            this.btnKhaiBao.TabIndex = 6;
            this.btnKhaiBao.Text = "Khai  báo";
            this.btnKhaiBao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKhaiBao.Click += new System.EventHandler(this.btnKhaiBao_Click);
            // 
            // btnLayPhanHoi
            // 
            this.btnLayPhanHoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayPhanHoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayPhanHoi.Icon")));
            this.btnLayPhanHoi.Location = new System.Drawing.Point(8, 462);
            this.btnLayPhanHoi.Name = "btnLayPhanHoi";
            this.btnLayPhanHoi.Size = new System.Drawing.Size(109, 23);
            this.btnLayPhanHoi.TabIndex = 7;
            this.btnLayPhanHoi.Text = "Lấy phản hồi";
            this.btnLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLayPhanHoi.Click += new System.EventHandler(this.btnLayPhanHoi_Click);
            // 
            // btnKetQuaXuLy
            // 
            this.btnKetQuaXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKetQuaXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXuLy.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKetQuaXuLy.Icon")));
            this.btnKetQuaXuLy.Location = new System.Drawing.Point(439, 39);
            this.btnKetQuaXuLy.Name = "btnKetQuaXuLy";
            this.btnKetQuaXuLy.Size = new System.Drawing.Size(109, 23);
            this.btnKetQuaXuLy.TabIndex = 11;
            this.btnKetQuaXuLy.Text = "Kết quả xử lý";
            this.btnKetQuaXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXuLy.Click += new System.EventHandler(this.btnKetQuaXuLy_Click);
            // 
            // GiayPhepForm
            // 
            this.AcceptButton = this.btnGhi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(693, 513);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "GiayPhepForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin giấy phép";
            this.Load += new System.EventHandler(this.GiayPhepForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayGiayPhep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoGiayPhep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNoiCap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVdcCap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCQCap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayPhep;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiCap;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiCap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayHHGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDVdcCap;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDVdcCap;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCQCap;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCQCap;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtThongTinKhac;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnAddNew;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayGiayPhep;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoGiayPhep;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayHH;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNoiCap;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDVdcCap;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenCQCap;
        private Janus.Windows.EditControls.UIButton btnChonGP;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIButton btnKhaiBao;
        private Janus.Windows.EditControls.UIButton btnLayPhanHoi;
        private Janus.Windows.EditControls.UIButton btnKetQuaXuLy;
    }
}
