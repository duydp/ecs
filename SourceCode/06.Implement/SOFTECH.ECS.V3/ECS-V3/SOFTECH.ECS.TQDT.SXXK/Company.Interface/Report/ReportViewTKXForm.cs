﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Company.Interface.Report
{
    
    public partial class ReportViewTKXForm : BaseForm
    {
        public ToKhaiXuat ToKhaiChinhReport = new ToKhaiXuat();
        public PhuLucToKhaiXuat PhuLucReport = new PhuLucToKhaiXuat();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        System.Drawing.Printing.Margins Margin = new System.Drawing.Printing.Margins();
        public XRControl Cell = new XRControl();
        public ReportViewTKXForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            this.Margin = GlobalSettings.MarginTKX;
            if (this.TKMD.HMDCollection.Count <= 9) ;
            else
            {
                int count = (this.TKMD.HMDCollection.Count - 1) / 15 + 1;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            cboToKhai.SelectedIndex = 0;

        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.Margins = this.Margin;
                this.ToKhaiChinhReport.TKMD = this.TKMD;
                this.ToKhaiChinhReport.report = this;
                this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                int begin = (cboToKhai.SelectedIndex - 1) * 15;
                int end = cboToKhai.SelectedIndex * 15;
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                this.PhuLucReport = new PhuLucToKhaiXuat();
                this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.Margin = printControl1.PrintingSystem.PageMargins;
                this.ToKhaiChinhReport.setVisibleImage(chkHinhNen.Checked);
                this.ToKhaiChinhReport.Margins = this.Margin;
                GlobalSettings.MarginTKX = this.Margin;
                GlobalSettings.RefreshKey();
                this.ToKhaiChinhReport.CreateDocument();
                printControl1.PrintingSystem.PrintDlg();
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        if (cboToKhai.SelectedIndex == 0)
            //        {
            //            this.ToKhaiChinhReport.ptbImage.Visible = false;
            //            this.ToKhaiChinhReport.CreateDocument();
            //        }
            //        else
            //        {
            //            this.PhuLucReport.xrPictureBox1.Visible = false;
            //            this.PhuLucReport.CreateDocument();
            //        }
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { false });
            //        if (cboToKhai.SelectedIndex == 0)
            //        {
            //            this.ToKhaiChinhReport.ptbImage.Visible = true;
            //            this.ToKhaiChinhReport.CreateDocument();
            //        }
            //        else
            //        {
            //            this.PhuLucReport.xrPictureBox1.Visible = true;
            //            this.PhuLucReport.CreateDocument();
            //        }
            //        break;
            //    case "excel":
            //        if (cboToKhai.SelectedIndex == 0)
            //        {
            //            this.ToKhaiChinhReport.ptbImage.Visible = false;
            //            this.ToKhaiChinhReport.CreateDocument();
            //        }
            //        else
            //        {
            //            this.PhuLucReport.xrPictureBox1.Visible = false;
            //            this.PhuLucReport.CreateDocument();
            //        }
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { false });
            //        if (cboToKhai.SelectedIndex == 0)
            //        {
            //            this.ToKhaiChinhReport.ptbImage.Visible = true;
            //            this.ToKhaiChinhReport.CreateDocument();
            //        }
            //        else
            //        {
            //            this.PhuLucReport.xrPictureBox1.Visible = true;
            //            this.PhuLucReport.CreateDocument();
            //        }
            //        break;
            //}
        }

        private void btnQuickPrint_Click(object sender, EventArgs e)
        {
            //if (cboToKhai.SelectedIndex == 0)
            //{
            //    this.ToKhaiChinhReport.setVisibleImage(false);
            //    this.ToKhaiChinhReport.CreateDocument();
            //    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
            //    this.ToKhaiChinhReport.setVisibleImage(true);
            //    this.ToKhaiChinhReport.CreateDocument();
            //}
            //else
            //{
            //    this.PhuLucReport.setVisibleImage(false);
            //    this.PhuLucReport.CreateDocument();
            //    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
            //    this.PhuLucReport.setVisibleImage(true);
            //    this.PhuLucReport.CreateDocument();
            //}
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            CauHinhNhomHangForm f = new CauHinhNhomHangForm();
            f.HMDCollection = this.TKMD.HMDCollection;
            f.ShowDialog(this);
            this.TKMD.HMDCollection = f.HMDCollection;
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.TKMD = this.TKMD;
                this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                int begin = (cboToKhai.SelectedIndex - 1) * 15;
                int end = cboToKhai.SelectedIndex * 15;
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                this.PhuLucReport = new PhuLucToKhaiXuat();
                this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }
        public void setVisibleButtonConfig(bool k)
        {
            //btnConfig.Visible = k;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setVisibleImage(chkHinhNen.Checked);
                this.ToKhaiChinhReport.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnApDung_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setNhomHang(this.Cell, txtTenHang.Text);
                this.ToKhaiChinhReport.CreateDocument();
            }
        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }

        }
    }
}