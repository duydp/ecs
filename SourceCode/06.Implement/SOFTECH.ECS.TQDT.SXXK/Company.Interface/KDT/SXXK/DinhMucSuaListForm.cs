﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucSuaListForm : BaseForm
    {
        public DinhMucSuaListForm()
        {
            InitializeComponent();
            SetComboTrangthai(cbStatus);
        }

        private void SetComboTrangthai(Janus.Windows.EditControls.UIComboBox cboTrangThai)
        {
            cboTrangThai.Items.Add("Chưa khai báo", TrangThaiXuLy.CHUA_KHAI_BAO);
            cboTrangThai.Items.Add("Chờ duyệt", TrangThaiXuLy.CHO_DUYET);
            cboTrangThai.Items.Add("Đã duyệt", TrangThaiXuLy.DA_DUYET);
            cboTrangThai.Items.Add("Không phê duyệt", TrangThaiXuLy.KHONG_PHE_DUYET);
        }

        private void DinhMucSuaListForm_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Value = DateTime.Today.Year;
            cbStatus.SelectedIndex = 0;
            this.search();
        }
        public void BindData()
        {
            //dgList.DataSource = DinhMucDangKySUA.SelectCollectionAll();

        }
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.

            string where = "1 = 1";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                }
            }
            if (cbStatus.SelectedValue != null)
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;
            // Thực hiện tìm kiếm.            
            dgList.DataSource = DinhMucDangKySUA.SelectCollectionDynamic(where,"");

            //this.setCommandStatus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            DinhMucSuaForm f = new DinhMucSuaForm();
            f.dmdkSUA = new DinhMucDangKySUA();
            f.ShowDialog(this);
            this.search();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            deleteDM();
        }
        private void deleteDM()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                if (MLMessages("Bạn có muốn xóa các lần khai báo này không?", "MSG_DEL01", "", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "DM_SUA";
                            sendXML.master_id = Convert.ToInt64(i.GetRow().Cells["ID"].Value);
                            if (!sendXML.Load() && i.GetRow().Cells["TrangThaiXuLy"].Value.ToString() == "-1")
                            {
                                DinhMucDangKySUA dmdkSUA = (DinhMucDangKySUA)i.GetRow().DataRow;
                                List<DinhMucSUA> dmList = (List<DinhMucSUA>)DinhMucSUA.SelectCollectionBy_Master_IDSUA(dmdkSUA.ID);
                                if (dmList.Count > 0)
                                {
                                    foreach (DinhMucSUA sp in dmList)
                                        sp.Delete();
                                }
                                dmdkSUA.Delete();
                            }
                            else
                            {
                                int pos = i.Position + 1;
                                MLMessages("Danh sách thứ " + pos + "  đã gửi thông tin tới hải quan.", "MSG_NPL03", "" + pos + "", false);
                            }
                        }
                    }
                    this.search();
                }
            }
        }
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            DinhMucSuaForm suaForm = new DinhMucSuaForm();
            suaForm.dmdkSUA = (DinhMucDangKySUA)dgList.GetRow().DataRow;
            //suaForm.masterIdSUA = suaForm.spdkSUA.ID;

            int tt = suaForm.dmdkSUA.TrangThaiXuLy;

            suaForm.ShowDialog();
            if (tt != suaForm.dmdkSUA.TrangThaiXuLy)
                this.search();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
            {
                case -1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    break;
                case 0:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    break;
                case 1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                    break;
                case 2:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    break;
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            deleteDM();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            this.search();
        }
    }
}
