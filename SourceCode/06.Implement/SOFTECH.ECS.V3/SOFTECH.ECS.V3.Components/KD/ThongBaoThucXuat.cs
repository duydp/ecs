﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{

    //NAMNTH
    public class ThongBaoThucXuat
    {
        /// <summary>
        /// ConfigThongBaoThucXuat
        /// </summary>        
        /// <returns>string</returns>
        public static string ConfigThongBaoThucXuat(ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/KD/ThongBaoThucXuat.xml");
                if (tkmd != null && docXML != null)
                {
                    XElement Root = docXML.Root;                    
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                        
                        XElement AdditionalInformation = Root.Element(NodeThongBaoThucXuat.AdditionalInformation);
                        foreach (XElement element in AdditionalInformation.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodeThongBaoThucXuat.content.ToUpper()))
                                element.SetValue("");//Thông tin thực xuất
                            if (element.Name.ToString().ToUpper().Equals(NodeThongBaoThucXuat.statement.ToUpper()))
                                element.SetValue("");//Mã nội dung phản hồi
                        }

                        //docXML.Save(@"D:\NewThongBaoThucXuat.xml");
                        //docXML.Save(@"../../../SOFTECH.ECS.V3.Components/XML/LoadKD/NewThongBaoThucXuat.xml");
                        return docXML.ToString();
                    }

                }
                return "";
            }
            catch
            {
                return "";
            }

        }
    }
}

