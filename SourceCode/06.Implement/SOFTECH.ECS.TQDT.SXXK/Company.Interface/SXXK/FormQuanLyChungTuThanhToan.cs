﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.KDT.SXXK.CTTT;

namespace Company.Interface.SXXK
{
    public partial class FormQuanLyChungTuThanhToan : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();

        public FormQuanLyChungTuThanhToan()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            FrmChungTuThanhToanChiTiet frm = new FrmChungTuThanhToanChiTiet();
            frm.HSTL = HSTL;
            frm.ChungTuThanhToan = new ChungTu();
            frm.ShowDialog();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Janus.Windows.GridEX.GridEXSelectedItemCollection items = dgList.SelectedItems;

                if (items.Count > 0)
                {
                    if (MLMessages("Bạn có muốn xóa các chứng từ thanh toán này không?", "MSG_DEL01", "", true) == "Yes")
                    {
                        foreach (Janus.Windows.GridEX.GridEXSelectedItem i in items)
                        {
                            if (i.RowType == Janus.Windows.GridEX.RowType.Record)
                            {
                                //Get chung tu thanh toan
                                ChungTu cttt = (ChungTu)i.GetRow().DataRow;

                                //Delete chung tu thanh toan chi tiet
                                if (cttt.ChungTuChiTietCollections == null)
                                    cttt.LoadChungTuChiTiet();

                                foreach (ChungTuChiTiet item in cttt.ChungTuChiTietCollections)
                                {
                                    item.Delete();
                                }

                                //Delete chung tu thanh toan
                                cttt.Delete();
                            }
                        }
                    }
                }

                //Reload data
                dgList.DataSource = ChungTu.SelectCollectionAll();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void FormQuanLyChungTuThanhToan_Load(object sender, EventArgs e)
        {
            try
            {
                dgList.DataSource = ChungTu.SelectCollectionAll();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                ChungTu cttt = (ChungTu)e.Row.DataRow;

                FrmChungTuThanhToanChiTiet frm = new FrmChungTuThanhToanChiTiet();
                frm.HSTL = HSTL;
                cttt.LoadChiPhiKhac();
                cttt.LoadChungTuChiTiet();
                frm.ChungTuThanhToan = cttt;
                frm.ShowDialog();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
