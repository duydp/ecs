﻿using System;
using System.Windows.Forms;
using Company.BLL;
//using Company.BLL.GC;

using Company.BLL.SXXK;
using Company.BLL.SXXK.ThanhKhoan ;
using Company.BLL.SXXK.ToKhai;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Drawing;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
using Company.KDT.SHARE.Components;
//using Company.BLL.KDT.SXXK;
//using Company.Interface.Reports;

namespace Company.Interface
{    
    public partial class ToKhaiThuocLoaiHinhKhacForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();      
            
        public string NhomLoaiHinh = string.Empty;
        private string xmlCurrent = "";
        public ToKhaiThuocLoaiHinhKhacForm()
        {
            InitializeComponent();            
        }

        private void loadTKMDData()
        {
           // if (TKMD.SoToKhai == 0)
            //    return;           
           
            ctrDonViHaiQuan.Ma = this.TKMD.MaHaiQuan;
            txtSoLuongPLTK.Value = this.TKMD.SoLuongPLTK;
           
            // Doanh nghiệp.
            txtMaDonVi.Text = this.TKMD.MaDoanhNghiep;
            txtTenDonVi.Text = this.TKMD.TenDoanhNghiep;
          
            // Đơn vị đối tác.
            txtTenDonViDoiTac.Text = this.TKMD.TenDonViDoiTac;
           
            // Đại lý TTHQ.
            txtMaDaiLy.Text = this.TKMD.MaDaiLyTTHQ;
            txtTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;
            
            //Don vi uy thac
            txtMaDonViUyThac.Text = this.TKMD.MaDonViUT;
            txtTenDonViUyThac.Text = this.TKMD.MaDonViUT;

            // Loại hình mậu dịch.
            ctrLoaiHinhKhac.Ma = this.TKMD.MaLoaiHinh;
            
            // Phương tiện vận tải.
            cbPTVT.SelectedValue = this.TKMD.PTVT_ID;
            txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
            ccNgayDen.Value = this.TKMD.NgayDenPTVT;
          
            // Nước.edit
             if (this.TKMD.MaLoaiHinh.Substring(0, 1) == "N")
               ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
            else
              ctrNuocXuatKhau.Ma = this.TKMD.NuocNK_ID;
           
            // ĐKGH.
            cbDKGH.SelectedValue = this.TKMD.DKGH_ID;
           
            // PTTT.
            cbPTTT.SelectedValue = this.TKMD.PTTT_ID;
           
            // Giấy phép.
            txtSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep.Year > 1900) ccNgayGiayPhep.Value = this.TKMD.NgayGiayPhep;
            if (this.TKMD.NgayHetHanGiayPhep.Year > 1900) ccNgayHHGiayPhep.Value = this.TKMD.NgayHetHanGiayPhep;
            if (this.TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;
           
            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.Text = this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai.Year > 1900) ccNgayHDTM.Value = this.TKMD.NgayHoaDonThuongMai;
            if (this.TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;
           
            // Địa điểm dỡ hàng.
            ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;
            
            // Nguyên tệ.
            ctrNguyenTe.Ma = this.TKMD.NguyenTe_ID;
            txtTyGiaTinhThue.Value = this.TKMD.TyGiaTinhThue;
            txtTyGiaUSD.Value = this.TKMD.TyGiaUSD;
            
            // Hợp đồng.
            txtSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong.Year > 1900) ccNgayHopDong.Value = this.TKMD.NgayHopDong;
            if (this.TKMD.NgayHetHanHopDong.Year > 1900) ccNgayHHHopDong.Value = this.TKMD.NgayHetHanHopDong;
            if (this.TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;
            
            // Vận tải đơn.
            txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Value = this.TKMD.NgayVanDon;
            if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;
            
            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;
            
            // Tên chủ hàng.
            txtTenChuHang.Text = this.TKMD.TenChuHang;
            
            // Container 20.
            txtSoContainer20.Value = this.TKMD.SoContainer20;
            
            // Container 40.
            txtSoContainer40.Value = this.TKMD.SoContainer40;
            
            // Số kiện hàng.
            txtSoKien.Value = this.TKMD.SoKien;
            
            // Trọng lượng.
            txtTrongLuong.Value = this.TKMD.TrongLuong;
            
            // Lệ phí HQ.
            txtLePhiHQ.Value = this.TKMD.LePhiHaiQuan;
            
            // Phí BH.
            txtPhiBaoHiem.Value = this.TKMD.PhiBaoHiem;
            
            // Phí VC.
            txtPhiVanChuyen.Value = this.TKMD.PhiVanChuyen;
            if (this.TKMD.Xuat_NPL_SP == "S")
            {
                radSP.Checked = true;
                radNPL.Checked = false;
                radTB.Checked = false;
            }
            else if (this.TKMD.Xuat_NPL_SP == "N")
            {
                radSP.Checked = false;
                radNPL.Checked = true;
                radTB.Checked = false;
            }
            else if (this.TKMD.Xuat_NPL_SP == "T")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = true;
            }
            if(this.TKMD.HMDCollection==null || this.TKMD.HMDCollection.Count==0)
                this.TKMD.LoadHMDCollection();         
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();
            //chung tu kem
            txtChungTu.Text = this.TKMD.ChungTu;
            //txtSoCT.Text=this.TKMD.so
            // ngay thuc nhap xuat
            cbNgayDangky.Value = this.TKMD.NgayDangKy;
            cbNgayThucNX.Value  = this.TKMD.NGAY_THN_THX;
            //so to khai 
            if (this.TKMD.SoToKhai > 0)
                txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();
         
           if (TKMD.TrangThaiThanhKhoan == "")
               if (GlobalSettings.NGON_NGU == "0")
               {
                   lblTrangThai.Text = "Chưa thanh khoản";
               }
               else
               {
                   lblTrangThai.Text = "Not yet liquidated";
               }
               else if (TKMD.TrangThaiThanhKhoan == "L")
                   lblTrangThai.Text = "Thanh khoản một phần";
               else if (TKMD.TrangThaiThanhKhoan == "H")
               {
                   lblTrangThai.Text = "Thanh khoản hết";

               }
            
        }
        
        private void khoitao_DuLieuChuan()
        {
            // Loại hình mậu dịch.

             ctrLoaiHinhKhac.Nhom = this.NhomLoaiHinh; ;

           
            
            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
            
            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            
            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;
        }
        #region Khoitaogiaodien
         private void khoitao_GiaoDienToKhai()
         {
             //string Loaihinh = ctrLoaiHinhKhac.Ma;
             //this.NhomLoaiHinh = Loaihinh;
             //string loaiToKhai = Loaihinh.Substring(0, 1); 

             this.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 1);
             string loaiToKhai = this.NhomLoaiHinh.Substring(0, 1);  
            
            if (loaiToKhai == "N")
            {
                // Tiêu đề.
                
                if (GlobalSettings.NGON_NGU == "0")
                {
                    this.Text = "Tờ khai nhập khẩu (" + this.NhomLoaiHinh + ")";
                }
                else
                {
                    this.Text = "Import declaration (" + this.NhomLoaiHinh + ")";
                }
                ctrLoaiHinhKhac.Ma = "NSX01";
                lblSoTiepNhan.Text = setText("Ngày thực nhập","Actual date of import");
            }
            else
            {
                // Tiêu đề.
                //this.Text = "Tờ khai xuất khẩu (" + this.NhomLoaiHinh + ")";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    this.Text = "Tờ khai xuất khẩu (" + this.NhomLoaiHinh + ")";
                }
                else
                {
                    this.Text = "Export Declaration (" + this.NhomLoaiHinh + ")";
                }
                lblSoTiepNhan.Text = setText("Ngày thực xuất", "Actual date of export");
                ctrLoaiHinhKhac.Ma = "XSX01";
                this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                // Người XK / Người NK.
                //grbNguoiNK.Text = "Người xuất khẩu";
                //grbNguoiXK.Text = "Người nhập khẩu";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    grbNguoiNK.Text = "Người xuất khẩu";
                    grbNguoiXK.Text = "Người nhập khẩu";
                }
                else
                {
                    grbNguoiNK.Text = "Importer";
                    grbNguoiXK.Text = "Exporter";
                }

                // Phương tiện vận tải.
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;

                chkHoaDonThuongMai.Enabled = grbHoaDonThuongMai.Enabled = false;
                // Vận tải đơn.
                chkVanTaiDon.Enabled = grbVanTaiDon.Enabled = false;

                // Nước XK.
                if (GlobalSettings.NGON_NGU == "0")
                {
                    grbNuocXK.Text = "Nước nhập khẩu";
                    grbDiaDiemDoHang.Text = "Địa điểm xếp hàng";
                }
                else
                {
                    grbNuocXK.Text = "Import country";
                    grbDiaDiemDoHang.Text = "Export store" ;

                }
               

                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                
                chkDiaDiemXepHang.Enabled = grbDiaDiemXepHang.Enabled = false;
                grbDiaDiemXepHang.Visible = chkDiaDiemXepHang.Visible = false;
            }

            // Loại hình gia công.
            if (this.NhomLoaiHinh=="NGC"  || this.NhomLoaiHinh=="XGC")                  
            {
               // chkHopDong.Checked = true;
               // chkHopDong.Visible = false;
               // txtSoHopDong.ReadOnly = ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = true;
               // txtSoHopDong.BackColor = ccNgayHopDong.BackColor = ccNgayHHHopDong.BackColor = Color.FromArgb(255, 255, 192);
               // txtSoHopDong.ButtonStyle = EditButtonStyle.Ellipsis;
                if (this.NhomLoaiHinh.Trim() == "N")
                {

                    radSP.Visible = false;
                    radSP.Checked = false;
                    radNPL.Checked = true;
                    radTB.Checked = false;

                }
              
            }
            else if (this.NhomLoaiHinh == "N" || this.NhomLoaiHinh == "X")
            {
                if (this.NhomLoaiHinh == "N")
                {
                    grpLoaiHangHoa.Visible = false;
                    radNPL.Checked = true;
                    radSP.Checked = false;
                    radTB.Checked = false;

                }       
                else
                {
                    radTB.Visible = false;
                }
                
            }
            else
                grpLoaiHangHoa.Visible = false;

         }
        #endregion 
        //-----------------------------------------------------------------------------------------
        
        private void ToKhaiNhapKhauSXXK_Form_Load(object sender, EventArgs e)
        {
            // Người nhập khẩu / Người xuất khẩu.
            txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
            txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            txtTyGiaUSD.Value = Convert.ToDecimal(GlobalSettings.TY_GIA_USD);
            ccNgayDen.Value = DateTime.Today;
            
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            {
                cmbToolBar.Visible = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }  
            this.khoitao_DuLieuChuan();   
            // this.loadTKMDData();
             switch (this.OpenType)
             {
                 case OpenFormType.View:
                     this.loadTKMDData();
                     this.ViewTKMD();
                     //this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
                     break;
                 case OpenFormType.Edit:
                     this.loadTKMDData();
                     this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);
                     break;
             }
            
         this.khoitao_GiaoDienToKhai();
            setCommandStatus();
        }

        private void ViewTKMD()
        {
            //cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
           
            ctrDonViHaiQuan.ReadOnly = false ; //edit
            txtSoLuongPLTK.ReadOnly = false;
            
            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = false;
            txtTenDonVi.ReadOnly = false;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.ReadOnly = false;

            // Đại lý TTHQ.
            txtMaDaiLy.ReadOnly = false;
            txtTenDaiLy.ReadOnly = false;

            // Loại hình mậu dịch.
            ctrLoaiHinhKhac.ReadOnly = false;

            // Phương tiện vận tải.
            cbPTVT.ReadOnly = false;
            txtSoHieuPTVT.ReadOnly = false;
            ccNgayDen.ReadOnly = false;

            // Nước.
            ctrNuocXuatKhau.ReadOnly = false;

            // ĐKGH.
            cbDKGH.ReadOnly = false;

            // PTTT.
            cbPTTT.ReadOnly = false;

            // Giấy phép.
            txtSoGiayPhep.ReadOnly = false;
            ccNgayGiayPhep.ReadOnly = false;
            ccNgayHHGiayPhep.ReadOnly = false;


            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.ReadOnly = false;
            ccNgayHDTM.ReadOnly = false;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.ReadOnly = false;

            // Nguyên tệ.
            ctrNguyenTe.ReadOnly = false;
            txtTyGiaTinhThue.ReadOnly = false;
            txtTyGiaUSD.ReadOnly = false;

            // Hợp đồng.
            txtSoHopDong.ReadOnly = false;
            ccNgayHopDong.ReadOnly = false;
            ccNgayHHHopDong.ReadOnly = false;

            // Vận tải đơn.
            txtSoVanTaiDon.ReadOnly = false;
            ccNgayVanTaiDon.ReadOnly = false;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.ReadOnly = false;

            // Tên chủ hàng.
            txtTenChuHang.ReadOnly = false;

            // Container 20.
            txtSoContainer20.ReadOnly = false;

            // Container 40.
            txtSoContainer40.ReadOnly = false;

            // Số kiện hàng.
            txtSoKien.ReadOnly = false;

            // Trọng lượng.
            txtSoKien.ReadOnly = false;
            txtTrongLuong.ReadOnly = false;

            // Lệ phí HQ.
            txtLePhiHQ.ReadOnly = false;

            // Phí BH.
            txtPhiBaoHiem.ReadOnly = false;

            // Phí VC.
            txtPhiVanChuyen.ReadOnly = false;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHoaDonThuongMai_CheckedChanged(object sender, EventArgs e)
        {
            grbHoaDonThuongMai.Enabled = chkHoaDonThuongMai.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkGiayPhep_CheckedChanged(object sender, EventArgs e)
        {
            gbGiayPhep.Enabled = chkGiayPhep.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHopDong_CheckedChanged(object sender, EventArgs e)
        {
            grbHopDong.Enabled = chkHopDong.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkVanTaiDon_CheckedChanged(object sender, EventArgs e)
        {
            grbVanTaiDon.Enabled = chkVanTaiDon.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkDiaDiemXepHang_CheckedChanged(object sender, EventArgs e)
        {
            grbDiaDiemXepHang.Enabled = chkDiaDiemXepHang.Checked;
        }
        //-----------------------------------------------------------------------------------------
       

        private void themHang()
        {
            if(chkHopDong.Checked )
            {
                if (txtSoHopDong.Text == "")
                {
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        epError.SetError(txtSoHopDong, "Số hợp đồng không được rỗng.");
                    }
                    else
                    {
                        epError.SetError(txtSoHopDong, "Contact No. don't allow empty.");
                    }
                    epError.SetIconPadding(txtSoHopDong, -8);
                    return;
                }
                else epError.Clear();
            }
            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;

            LoaiHinhKhacForm f = new LoaiHinhKhacForm();
            this.TKMD.MaLoaiHinh = ctrLoaiHinhKhac.Ma;
            f.TKMD = this.TKMD;
            f.NhomLoaiHinh = this.NhomLoaiHinh;
            if (radNPL.Checked) f.LoaiHangHoa = "N";
            if (radSP.Checked) f.LoaiHangHoa = "S";
            if (radTB.Checked) f.LoaiHangHoa = "T";
            f.MaHaiQuan = ctrDonViHaiQuan.Ma;
            f.HMDCollection = this.TKMD.HMDCollection;
            f.MaNguyenTe = ctrNguyenTe.Ma;
            f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            
            f.ShowDialog(this);
            this.TKMD.HMDCollection = f.HMDCollection;           
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            setCommandStatus();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin tờ khai.
        /// </summary>
        public void Save()
        {
            bool valid = false;
            if (ctrLoaiHinhKhac.Ma.Trim() == "")
            {
               // ShowMessage("Bạn phải nhập mã loại hình.", false);
                MLMessages("Bạn phải nhập mã loại hình.", "MSG_PUB42", "", false);
                return;
            }
            //if (TKMD.MaLoaiHinh.Substring(0,1) == "N") 
            //{
            //    cvError.ContainerToValidate = this;
            //    cvError.Validate();
            //    valid = cvError.IsValid;
            //}
            //else
            //{
            //    cvError.ContainerToValidate = this.txtTenDonViDoiTac;
            //    cvError.Validate();
            //    bool valid1 = cvError.IsValid;
            //    cvError.ContainerToValidate = this.grbNguyenTe;
            //    cvError.Validate();
            //    bool valid2 = cvError.IsValid;
            //    valid = valid1 && valid2;
            //}
            cvError.Validate();
            if (!cvError.IsValid)return;
            
                if (this.TKMD.HMDCollection.Count == 0)
                {
                   // ShowMessage("Chưa nhập thông tin hàng của tờ khai", false);
                    MLMessages("Chưa nhập thông tin hàng của tờ khai", "MSG_SAV01", "", false);
                    return;
                }
                
                if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
                {
                    string msg = setText("Tỷ giá tính thuế không hợp lệ.", "Invalid input value ");
                    epError.SetError(txtTyGiaTinhThue, msg);
                    epError.SetIconPadding(txtTyGiaTinhThue, -8);
                    return;
                    
                }
                if (txtSoToKhai.Text == "")
                {
                    string msg = setText("Số tờ khai không được rỗng"," 'Declaration number' must be filled");
                    epError.SetError(txtSoToKhai, msg);
                    epError.SetIconPadding(txtSoToKhai, -8);
                    return;
                }
                this.TKMD.NGAY_THN_THX  = this.TKMD.NGAY_THN_THX ; //edit
                this.TKMD.MaHaiQuan = ctrDonViHaiQuan.Ma;
               
                this.TKMD.SoHang = (short)this.TKMD.HMDCollection.Count;
                this.TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);
                
                // Doanh nghiệp.
                this.TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                // Đơn vị đối tác.
                this.TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;

                

                this.TKMD.MaDonViUT = txtMaDonViUyThac.Text;                

                // Đại lý TTHQ.
                this.TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                this.TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                //Ngay thuc nhap xuat 
                this.TKMD.NGAY_THN_THX = cbNgayThucNX.Value; 
                // Loại hình mậu dịch.
                this.TKMD.MaLoaiHinh = ctrLoaiHinhKhac.Ma;

                // Giấy phép.
                if (chkGiayPhep.Checked)
                {
                    this.TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                    this.TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
                    this.TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;
                }
                else
                {
                    this.TKMD.SoGiayPhep = "";
                    this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }

                // 7. Hợp đồng.
                if (chkHopDong.Checked)
                {
                    this.TKMD.SoHopDong = txtSoHopDong.Text;
                    // Ngày HD.
                    if (ccNgayHopDong.IsNullDate)
                        this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHopDong = ccNgayHopDong.Value;
                    // Ngày hết hạn HD.
                    if (ccNgayHHHopDong.IsNullDate)
                        this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;
                }
                else
                {
                    this.TKMD.SoHopDong = "";
                    this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                }

                // Hóa đơn thương mại.
                if (chkHoaDonThuongMai.Checked)
                {
                    this.TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                    if (ccNgayHDTM.IsNullDate)
                        this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
                }
                else
                {
                    this.TKMD.SoHoaDonThuongMai = "";
                    this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                }

                // Phương tiện vận tải.
                this.TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                this.TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                this.TKMD.NgayDenPTVT = ccNgayDen.Value;

                // Vận tải đơn.
                if (chkVanTaiDon.Checked)
                {
                    this.TKMD.SoVanDon = txtSoVanTaiDon.Text;
                    if (ccNgayVanTaiDon.IsNullDate)
                        this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
                }
                else
                {
                    this.TKMD.SoVanDon = "";
                    this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                }

                // Nước.
                if (TKMD.MaLoaiHinh == "N")
                {
                    this.TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                    this.TKMD.NuocNK_ID = "VN".PadRight(3);
                }
                else
                {
                    this.TKMD.NuocXK_ID = "VN".PadRight(3);
                    this.TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma; 
                }

                // Địa điểm xếp hàng.
                if (chkDiaDiemXepHang.Checked)
                    this.TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                else
                    this.TKMD.DiaDiemXepHang = "";

                // Địa điểm dỡ hàng.
                this.TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                // Điều kiện giao hàng.
                this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                // Đồng tiền thanh toán.
                this.TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                // Phương thức thanh toán.
                this.TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                //
                this.TKMD.TenChuHang = txtTenChuHang.Text;
                this.TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                this.TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                this.TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                this.TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                // Tổng trị giá khai báo (Chưa tính).
                this.TKMD.TongTriGiaKhaiBao = 0;
                //
                this.TKMD.TrangThaiThanhKhoan = "";
                this.TKMD.LoaiToKhaiGiaCong = "NPL";
                //
                this.TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                //this.tkmd.NgayGui = DateTime.Parse("01/01/1900");
                if (radNPL.Checked) this.TKMD.Xuat_NPL_SP = "N";
                if (radSP.Checked) this.TKMD.Xuat_NPL_SP = "S";
                if (radTB.Checked) this.TKMD.Xuat_NPL_SP = "T";
                //this.tinhLaiThue();
                this.TKMD.ChungTu = txtChungTu.Text;
                try{
                    this.TKMD.SoToKhai = Convert.ToInt32(txtSoToKhai.Text);
                }catch(Exception ex )
                {
                    string msg = setText(" Dữ liệu không hợp lệ \n ", " Invalid input value \n");
                    ShowMessage(msg +  ex.Message, false);
                    return;
                }
                this.TKMD.NgayDangKy = cbNgayDangky.Value;
                TKMD.TrangThai = 1;
                long ret;        
        
                //NPL ton 
                if (GlobalSettings.TuDongTinhThue == "1")
                    this.tinhLaiThue();
                else
                    this.tinhTongTriGiaKhaiBao();
                try
                {
                    //this.TKMD.InsertUpdate(); 
                    bool ok = true;
                    
                    this.TKMD.InsertUpdateFull();

                    //  ShowMessage("Lưu tờ khai thành công.", false);
                    MLMessages("Lưu thành công!", "MSG_SAV02", "", false);

                    setCommandStatus();

                }
                catch (Exception ex)
                {
                    ShowMessage("Dữ liệu không hợp lệ. Lỗi /n " + ex.Message, false);
                }
            
        }
       
        //-----------------------------------------------------------------------------------------
        private void tinhLaiThue()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            decimal Phi = TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen;
            decimal TongTriGiaHang = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
                this.TKMD.TongTriGiaTinhThue += hmd.TriGiaTT;
            }
            decimal TriGiaTTMotDong = 0;
            TriGiaTTMotDong = (1 + Phi / TongTriGiaHang) * TKMD.TyGiaTinhThue;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                hmd.DonGiaTT = TriGiaTTMotDong * hmd.DonGiaKB;
                hmd.TriGiaTT = hmd.DonGiaTT * hmd.SoLuong;
                hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
            }
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();
        }
        private void tinhTongTriGiaKhaiBao()
        {
            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
                this.TKMD.TongTriGiaTinhThue += hmd.TriGiaTT;
            }
        }
        //private void inToKhai()
        //{
        //    if (this.TKMD.SoToKhai == 0)
        //    {
        //        ShowMessage("Bạn hãy lưu tờ khai trước khi in.", false);
        //        return;
        //    }
        //    switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
        //    {
        //        case "N":
        //            ReportViewLHKForm f = new ReportViewLHKForm();
        //            f.TKMD = this.TKMD;
        //            f.ShowDialog(this);
        //            break;

        //        case "X":
        //            ReportViewTKXLHKForm f1 = new ReportViewTKXLHKForm();
        //            f1.TKMD = this.TKMD;
        //            f1.ShowDialog(this);
        //            break;
        //    }
        //}

        private void setCommandStatus()
        {
            if (TKMD.TrangThaiThanhKhoan  == "")
            {
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Chưa thanh khoản";
                }
                else
                {
                    lblTrangThai.Text = "Not Liquidated yet";
                }
            }
            else if (TKMD.TrangThaiThanhKhoan == "L")
            {
                
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //lblTrangThai.Text = "Thanh khoản một phần";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Thanh khoản một phần";
                }
                else
                {
                    lblTrangThai.Text = "Partial liquidation";
                }
            }
            else if (TKMD.TrangThaiThanhKhoan == "H")
            {
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
               // lblTrangThai.Text = "Thanh khoản hết";
                if (GlobalSettings.NGON_NGU == "0")
                {
                    lblTrangThai.Text = "Thanh khoản hết";
                }
                else
                {
                    lblTrangThai.Text = "Liquidatation of all";
                }
            }
          
        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch(e.Command.Key)
            {
                             
                case "ThemHang":
                    this.themHang();
                    break;                
                case "cmdSave":
                    this.Save();
                    break;                              
                //case "cmdPrint": 
                //    this.inToKhai();
                //   break;               
            }
        }
            
       
       //-----------------------------------------------------------------------------------------
        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
                string tem= "" ;
        }

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
           
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    LoaiHinhKhacModifyForm f = new LoaiHinhKhacModifyForm();
                     f.HMD = this.TKMD.HMDCollection[i.Position];
                   // if (this.HDGC.SoHopDong == "")
                   // {
                   //    this.HDGC.SoHopDong = TKMD.SoHopDong;
                   //    this.HDGC.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                   //    this.HDGC.MaHaiQuan = TKMD.MaHaiQuan;
                   //    this.HDGC.NgayKy = TKMD.NgayHopDong;
                   //}
                  // f.HD = this.HDGC;
                   f.collection = this.TKMD.HMDCollection;
                   f.MaLoaiHinh = this.TKMD.MaLoaiHinh;
                   if (radNPL.Checked) this.TKMD.Xuat_NPL_SP = "N";
                   if (radSP.Checked) this.TKMD.Xuat_NPL_SP = "S";
                   if (radTB.Checked) this.TKMD.Xuat_NPL_SP = "T";
                   f.LoaiHangHoa = this.TKMD.Xuat_NPL_SP;
                   f.NhomLoaiHinh = this.NhomLoaiHinh;
                   f.MaHaiQuan = this.TKMD.MaHaiQuan;
                   f.MaNguyenTe = ctrNguyenTe.Ma;
                   f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);

                   f.ShowDialog(this);
                   if (f.IsEdited)
                   {
                       if (f.HMD.SoThuTuHang  > 0)
                           f.HMD.Update();
                       else
                           this.TKMD.HMDCollection[i.Position] = f.HMD;
                   }
                   else if (f.IsDeleted)
                   {
                       if (f.HMD.SoThuTuHang > 0)
                           f.HMD.Delete();
                       this.TKMD.HMDCollection.RemoveAt(i.Position);
                   }
                    dgList.DataSource = this.TKMD.HMDCollection;
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.DataSource = this.TKMD.HMDCollection; }
                    setCommandStatus();
                }
                break;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text.ToString());
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (this.TKMD.TrangThaiThanhKhoan  == "")
                {
                    ShowMessage("Tờ khai đã được duyệt không được sửa", false);
                    e.Cancel = true;
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    HangMauDich hmd = (HangMauDich)e.Row.DataRow;
                    //if (hmd.id > 0)
                    //{
                    //    hmd.Delete();
                    //}
                    if (TKMD.TrangThaiThanhKhoan != "")
                    {                      
                        this.TKMD.Load();
                        this.TKMD.TrangThaiThanhKhoan  ="Chưa khai báo";
                        this.TKMD.Update();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void gridEX1_RowDoubleClick(object sender, RowActionEventArgs e)
        {
           
        }

        private void gridEX1_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (this.TKMD.TrangThaiThanhKhoan == "temp")
            {
                ShowMessage("Tờ khai đã được duyệt không được sửa",false);
                e.Cancel = true;
                return;
            }
            
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                        try
                        {
                            hmd.DeleteHangTransaction();
                        }
                        catch (Exception ex)
                        {
                            e.Cancel = true;
                            ShowMessage("Có lỗi khi xóa hàng :" + ex.Message, false);
                        }
                        //try
                        //{
                        //    hmd.TinhToanLuongTonSauKhiXoa(hmd, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep);
                        //}
                        //catch (Exception exLuongTon)
                        //{
                        //    e.Cancel = true;
                        //    ShowMessage("Có lỗi khi cập nhật lượng tồn :" + exLuongTon.Message, false);
                        //}
                    }
                }
            }
            else
                e.Cancel = true;
        }

        private void cmbToolBar_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            //test
            //ReadExcelForm reform = new ReadExcelForm();
            //reform.ShowDialog(this);
        }

        private void editBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void ctrLoaiHinhKhac_Load(object sender, EventArgs e)
        {

        }
    }
}
