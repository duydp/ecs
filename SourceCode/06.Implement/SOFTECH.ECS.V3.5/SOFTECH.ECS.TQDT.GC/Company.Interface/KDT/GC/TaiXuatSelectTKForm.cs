﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
namespace Company.Interface.KDT.GC
{
    public partial class TaiXuatSelectTKForm : BaseForm
    {
        public KDT_GC_TaiXuatDangKy TaiXuatDangky = new KDT_GC_TaiXuatDangKy();
       // public KDT_GC_CungUng CungUng = new KDT_GC_CungUng();
        public long IDHopDong;

        public HopDong HD = new HopDong();
        public TaiXuatSelectTKForm()
        {
            InitializeComponent();
        }

      
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void CungUngManagerForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            DataSet ds = KDT_GC_TaiXuatDangKy.SelectTKXTheoHD(TaiXuatDangky.HopDong_ID);
            dgList.DataSource = ds.Tables[0];
            dgList.Refetch();
        }
       

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            // if (e.Row.RowType == RowType.Record)
            //{
            //    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
            //     switch(trangthai)
            //     {
            //         case"-1":
            //             e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
            //             break;
            //         case "0":
            //             e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
            //             break;
            //         case "1":
            //             e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
            //             break;
            //         case "21":
            //             e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
            //             break;
            //     }
            // }
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            KDT_GC_TaiXuat TaiXuat = new KDT_GC_TaiXuat();
             Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
             for (int i = 0; i < listChecked.Length; i++)
             {
                 TaiXuat = new KDT_GC_TaiXuat();
                 Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                 bool trungTK = false;
                 foreach (KDT_GC_TaiXuat TX in TaiXuatDangky.TaiXuat_List)
                 {

                     if (TX.SoToKhaiVNACCS.Trim() == item.Cells["SoToKhaiVNACCS"].Text.Trim() && TX.MaLoaiHinh.Trim() == item.Cells["MaLoaiHinh"].Text.Trim())
                         {
                             if (TX.MaNPL.Trim() == item.Cells["MaNPL"].Text.Trim())
                             {
                                 trungTK = true;
                             }
                         }
                    
                     
                 }
                 if (trungTK)
                     continue;
                 TaiXuat.SoToKhai = item.Cells["SoToKhai"].Text;
                 TaiXuat.SoToKhaiVNACCS = item.Cells["SoToKhaiVNACCS"].Text;
                 TaiXuat.MaLoaiHinh = item.Cells["MaLoaiHinh"].Text;
                 TaiXuat.MaHaiQuan = item.Cells["MaHaiQuan"].Text;
                TaiXuat.NgayDangKy = Convert.ToDateTime(item.Cells["NgayDangKy"].Text);
                 TaiXuat.MaNPL = item.Cells["MaNPL"].Text;
                 TaiXuat.TenNPL = item.Cells["TenNPL"].Text;
                 TaiXuat.DVT_ID = item.Cells["DVT_ID"].Text;
                 TaiXuat.LuongTaiXuat = Convert.ToDecimal(item.Cells["SoLuong"].Text);
                 TaiXuat.LoaiHang = 1;
                 TaiXuat.GhiChu = "";
                 TaiXuatDangky.TaiXuat_List.Add(TaiXuat);
             }
             this.Close();
        }

     
        
    }
}
