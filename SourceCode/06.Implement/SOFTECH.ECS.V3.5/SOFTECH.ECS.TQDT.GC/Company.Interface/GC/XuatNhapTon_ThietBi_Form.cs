﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;

using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.SXXK;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.GC;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.Messages.GoodsItem;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Send;
namespace Company.Interface
{
    public partial class XuatNhapTon_ThietBi_Form : BaseForm
    {
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public bool IsActive = false;
        public bool IsReadExcel = false;
        KDT_VNACCS_BaoCaoQuyetToan_MMTB goodItem = new KDT_VNACCS_BaoCaoQuyetToan_MMTB();
        KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
        public HopDong HD = new HopDong();
        private DataTable tb = new DataTable();
        private DataTable tb_M15 = new DataTable();
        public string ListHD = "";
        int totalRows = 0;
        int index = 0;
        public XuatNhapTon_ThietBi_Form()
        {
            InitializeComponent();

        }

        public DataSet GetXNT()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();
            #region Commnent SQL
            //Lấy danh sách SP chưa có định mức
//            try
//            {
//                //Check HopDong
//                string test = "if exists(select * from t_KDT_ToKhaiMauDich where SoHopDong ='' or SoHopDong is null) "+
//    "update t_kdt_tokhaimaudich  set SoHopDong = (select top 1 sohopdong from t_KDT_GC_HopDong h where h.id = t_kdt_tokhaimaudich.IDHopDong) where t_kdt_tokhaimaudich.soHopDong='' "+
//" if exists(select * from t_kdt_gc_tokhaichuyentiep where SoHopDongDV ='' or SoHopDongDV is null)		"+
//"	update t_kdt_gc_tokhaichuyentiep  set SoHopDongDV = (select top 1 sohopdong from t_KDT_GC_HopDong h where h.id = t_kdt_gc_tokhaichuyentiep.IDHopDong) where t_kdt_gc_tokhaichuyentiep.SoHopDongDV is null";
//                DbCommand cmd = db.GetSqlStringCommand(test);
//                db.ExecuteNonQuery(cmd);
//                //Check HopDong

//                string proc = "p_GC_XuatNhapTon";
//                if (ListHD != "")
//                {
//                    //proc = "p_GC_XuatNhapTon_SelectByIDHopDong";
//                    string listHD1 = "SoHopDong in(" + ListHD + ") ";
//                    string listHD2 = "SoHopDongDV in(" + ListHD + ") ";
//                    string sql1 = " 					SELECT  0 AS STT , "+
//"         [HopDong_ID] ,                                                                                              "+
//"         hd.SoHopDong ,                                                                                              "+
//"         t.SoToKhai ,                                                                                                "+
//"         t.SoToKhaiVnacc ,                                                                                           "+
//"         t.NgayDangKy ,                                                                                              "+
//"         t.MaLoaiHinh ,                                                                                              "+
//"         [Ma] ,                                                                                                      "+
//"         [Ten] ,                                                                                                     "+
//"         n.[MaHS] ,                                                                                                  "+
//"         CASE WHEN DVT_ID <> '' THEN ( SELECT    Ten                                                                 "+
//"                                       FROM      t_HaiQuan_DonViTinh                                                 "+
//"                                       WHERE     ID = n.DVT_ID                                                       "+
//"                                     )                                                                               "+
//"              ELSE ''                                                                                                "+
//"         END AS DVT_ID ,                                                                                             "+
//"         CONVERT(DECIMAL(6, 0),[SoLuongDangKy]) AS SoLuongDangKy ,                                                   "+
//"         CONVERT(DECIMAL(6, 0),[SoLuongDaNhap]) AS SoLuongDaNhap,                                                    "+
//"         CONVERT(DECIMAL(6, 0),t.SoLuong ) AS SoLuong,                                                               "+
//"         CONVERT(DECIMAL(6, 0),t.SoLuong) AS LuongSD_TK ,                                                            "+
//" 		 CONVERT(DECIMAL(6, 0),t.SoLuongTamNhap) as SoLuongTamNhap,                                                   "+
//" 		 CONVERT(DECIMAL(6, 0),t.SoLuongTaiXuat) as SoLuongTaiXuat,                                                   "+
//" 		 CONVERT(DECIMAL(6, 0),t.SoLuongChuyenHD) as SoLuongChuyenHD,                                                 "+
//" 		 CONVERT(DECIMAL(6, 0),t.SoLuongTon) as SoLuongTon,                                                           "+
//"         t.GhiChu                                                                                                    "+
//" FROM    [dbo].[t_GC_ThietBi] n                                                                                      "+
//"         JOIN ( SELECT   tkmd.IDHopDong ,                                                                            "+
//"                         tkmd.SoToKhai ,                                                                             "+
//"                         tkmd.Huongdan_PL AS SoToKhaiVnacc ,                                                         "+
//"                         tkmd.NgayDangKy ,                                                                           "+
//"                         tkmd.MaLoaiHinh ,                                                                           "+
//"                         hmd.MaHang AS MaPhu ,                                                                       "+
//"                         hmd.TenHang ,                                                                               "+
//"                         hmd.SoLuong,                                                                                "+
//" 						CASE WHEN tkmd.MaLoaiHinh LIKE '%N%' THEN hmd.SoLuong ELSE 0 END AS SoLuongTamNhap,           "+
//"                         CASE WHEN tkmd.MaLoaiHinh IN ('XVG21','XVG22','XVG24','XGC20') THEN hmd.SoLuong ELSE 0 END AS SoLuongTaiXuat,         " +
//"                         CASE WHEN tkmd.MaLoaiHinh IN ('XVG23') THEN hmd.SoLuong ELSE 0 END AS SoLuongChuyenHD,        " +
//"                         0 AS SoLuongTon,                                                                            "+
//"                         tkmd.DeXuatKhac AS GhiChu                                                                   "+
//"                FROM     t_KDT_GC_HangChuyenTiep hmd                                                                 "+
//"                         INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID                        "+
//"                WHERE    IDHopDong IN ( " + ListHD + " )                                                               " +
//"                         AND maloaihinh LIKE '%V%'                                                                   "+
//"                UNION                                                                                                "+
//"                SELECT   tkmd.IDHopDong ,                                                                            "+
//"                         tkmd.SoToKhai ,                                                                             "+
//"                         tkmd.LoaiVanDon AS SoToKhaiVnacc ,                                                          "+
//"                         tkmd.NgayDangKy ,                                                                           "+
//"                         tkmd.MaLoaiHinh ,                                                                           "+
//"                         hmd.MaPhu ,                                                                                 "+
//"                         hmd.TenHang ,                                                                               "+
//"                         hmd.SoLuong ,                                                                               "+
//" 						CASE WHEN tkmd.MaLoaiHinh LIKE '%N%' THEN hmd.SoLuong ELSE 0 END AS SoLuongTamNhap,           "+
//"                         CASE WHEN tkmd.MaLoaiHinh IN ('XVG21','XVG22','XVG24','XGC20') THEN hmd.SoLuong ELSE 0 END AS SoLuongTaiXuat,         " +
//"                         CASE WHEN tkmd.MaLoaiHinh IN ('XVG23') THEN hmd.SoLuong ELSE 0 END AS SoLuongChuyenHD,        "+
//"                         0 AS SoLuongTon,                                                                            "+
//"                         tkmd.DeXuatKhac AS GhiChu                                                                   "+
//"                FROM     t_KDT_HangMauDich hmd                                                                       "+
//"                         INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID                                "+
//"                WHERE    IDHopDong IN ( " + ListHD + " )                                                                 " +
//"                         AND maloaihinh LIKE '%V%'                                                                   "+
//"                UNION                                                                                                "+
//"                SELECT   tkmd.IDHopDong ,                                                                            "+
//"                         tkmd.SoToKhai ,                                                                             "+
//"                         tkmd.LoaiVanDon AS SoToKhaiVnacc ,                                                          "+
//"                         tkmd.NgayDangKy ,                                                                           "+
//"                         tkmd.MaLoaiHinh ,                                                                           "+
//"                         hmd.MaPhu ,                                                                                 "+
//"                         hmd.TenHang ,                                                                               "+
//"                         hmd.SoLuong,                                                                                "+
//" 						CASE WHEN tkmd.MaLoaiHinh LIKE '%N%' THEN hmd.SoLuong ELSE 0 END AS SoLuongTamNhap,           "+
//"                         CASE WHEN tkmd.MaLoaiHinh IN ('XVG21','XVG22','XVG24','XGC20') THEN hmd.SoLuong ELSE 0 END AS SoLuongTaiXuat,         " +
//"                         CASE WHEN tkmd.MaLoaiHinh IN ('XVG23') THEN hmd.SoLuong ELSE 0 END AS SoLuongChuyenHD,        " +
//"                         0 AS SoLuongTon,                                                                            "+
//"                         tkmd.DeXuatKhac AS GhiChu                                                                   "+
//"                FROM     t_KDT_HangMauDich hmd                                                                       "+
//"                         INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID                                "+
//"                WHERE    IDHopDong IN ( " + ListHD + " )                                                                 " +
//"                         AND maloaihinh LIKE '%V%'                                                                   "+
//" 			UNION                                                                                                     "+
//"                SELECT   tkmd.IDHopDong ,                                                                            "+
//"                         tkmd.SoToKhai ,                                                                             "+
//"                         Convert(varchar(18),tkmd.SoToKhai)  AS SoToKhaiVnacc ,                                      "+
//"                         tkmd.NgayDangKy ,                                                                           "+
//"                         tkmd.MaLoaiHinh ,                                                                           "+
//"                         hmd.MaPhu ,                                                                                 "+
//"                         hmd.TenHang ,                                                                               "+
//"                         hmd.SoLuong ,                                                                               "+
//" 						CASE WHEN tkmd.MaLoaiHinh LIKE '%N%' THEN hmd.SoLuong ELSE 0 END AS SoLuongTamNhap,           "+
//"                         CASE WHEN tkmd.MaLoaiHinh IN ('XVG21','XVG22','XVG24','XGC20') THEN hmd.SoLuong ELSE 0 END AS SoLuongTaiXuat,         " +
//"                         CASE WHEN tkmd.MaLoaiHinh IN ('XVG23') THEN hmd.SoLuong ELSE 0 END AS SoLuongChuyenHD,        " +
//"                         0 AS SoLuongTon,                                                                            "+
//" 						tkmd.DeXuatKhac AS GhiChu                                                                     "+
//"                FROM     t_KDT_HangMauDich hmd                                                                       "+
//"                         INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID                                "+
//"                WHERE    IDHopDong IN ( " + ListHD + " )                                                                 " +
//"                         AND maloaihinh  NOT LIKE '%V%'                                                              "+
//"              ) t ON n.HopDong_ID = t.IDHopDong                                                                      "+
//"                     AND n.Ma = t.MaPhu                                                                              "+
//"                                                                                                                     "+
//"         JOIN t_kdt_gc_hopdong hd ON hd.ID = n.HopDong_ID                                                            "+
//" WHERE   ID IN ( "+ListHD+" )                                                                                 "+
//" ORDER BY HopDong_ID ,                                                                                               "+
//"         NgayDangKy,                                                                                                 "+
//" 		Ma";
                    
//                    //db.AddInParameter(cmd, "@HopDong_ID", DbType.String, listHD1);
//                    //db.AddInParameter(cmd, "@HopDong_ID_DV", DbType.String, listHD2);
//                    cmd = db.GetSqlStringCommand(sql1);
//                }
//                else
//                {
//                    //cmd = db.GetStoredProcCommand(proc);
//                    //db.AddInParameter(cmd, "@HopDong_ID", DbType.Int64, this.HD.ID);
//                }

//                //db.LoadDataSet(cmd, ds, "t_NPLNhapTon");
//                cmd.CommandTimeout = 60000;
//                ds = db.ExecuteDataSet(cmd);
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
//                //throw (ex);
//            }
#endregion
            try
            {
                DbCommand cmd = db.GetStoredProcCommand("p_GC_XuatNhapTon_TB_TT38");
                db.AddInParameter(cmd, "@HopDong_ID", DbType.String, ListHD);
                cmd.CommandTimeout = 60000;
                ds = db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return ds;

        }
        private void XuatNhapTonForm_Load(object sender, EventArgs e)
        {
            if(ListHD!="")
            {
                dgList.RootTable.Columns["SoHopDong"].Visible = true;
                //btnInBaoCao.Enabled = false;
                btnXuatMau15.Enabled = false;
            }
            else
            {
                dgList.RootTable.Columns["SoHopDong"].Visible = true;
                //btnInBaoCao.Enabled = true;
                btnXuatMau15.Enabled = true;
                btnXuLyXNT_Mau15.Enabled = false;
            }
            dgList.HideColumnsWhenGrouped = InheritableBoolean.True;
            tb = GetXNT().Tables[0];
            dgList.DataSource = tb;// GetXNT().Tables["t_NPLNhapTon"];
            
        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "XuatNhapTon_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
            ////-----------------------
            //dgList.Tables[0].Columns["SoToKhaiVNACC"].Visible = true;
            ////dgList.Tables[0].Columns["SoToKhai"].Visible = true;
            //dgList.Tables[0].Columns["NgayDangKy"].Visible = true;
            //dgList.Tables[0].Columns["MaLoaiHinh"].Visible = true;
            //GridEXGroup customG = dgList.Tables[0].Groups[0].Clone();
            //dgList.Tables[0].Groups.Clear();
            //SaveFileDialog sfNPL = new SaveFileDialog();
            //sfNPL.Filter = "Excel files| *.xls";
            //sfNPL.ShowDialog(this);
            //if (sfNPL.FileName != "")
            //{
            //    gridEXExporter1.GridEX = dgList;
            //    Stream str = sfNPL.OpenFile();
            //    gridEXExporter1.Export(str);
            //    str.Close();
            //    dgList.Tables[0].Groups.Add(customG);
            //    dgList.Tables[0].Columns["SoToKhaiVNACC"].Visible = false;
            //    dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
            //    dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;

            //    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
            //    {
            //        Process.Start(sfNPL.FileName);
            //    }
            //}
            //else
            //{
            //    dgList.Tables[0].Columns["SoToKhaiVNACC"].Visible = false;
            //    dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
            //    dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;
            //    dgList.Tables[0].Groups.Add(customG);
            //}
        }

        private void btnInBaoCao_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    NhapXuatTon_TT38 xnt = new NhapXuatTon_TT38();
                    xnt.HD = this.HD;
                    xnt.tb = tb;
                    xnt.BindReport("");
                    xnt.ShowPreview();
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXuatMau15_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Mau16_TT38_ChiTiet xnt = new Mau16_TT38_ChiTiet();
                //DataTable temp = tb.DefaultView.ToTable(true, "SoHopDong", "Ma", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");
                //backgroundWorker1.RunWorkerAsync();
                xnt.tb_Mau15 = tb;
                xnt.BindReport("");
                xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnXuLyXNT_Mau15_Click(object sender, EventArgs e)
        {

            lblXuLy.Text = "Đang xử lý...";
            btnXuLyXNT_Mau15.Enabled = false;
            backgroundWorker1.RunWorkerAsync();
            dgList.DataSource = tb;
            ProcessData();
        }
        private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        {
            //Return its own if the column names are empty
            if (columnNamesInDt == string.Empty || groupByColumnNames == string.Empty)
            {
                return dt;
            }

            //Once the columns are added find the distinct rows and group it bu the numbet
            DataTable _dt = dt.DefaultView.ToTable(true, groupByColumnNames);

            //The column names in data table
            string[] _columnNamesInDt = columnNamesInDt.Split(',');

            for (int i = 0; i < _columnNamesInDt.Length; i = i + 1)
            {
                if (_columnNamesInDt[i] != groupByColumnNames)
                {
                    _dt.Columns.Add(_columnNamesInDt[i]);
                }
            }

            //Gets the collection and send it back
            for (int i = 0; i < _dt.Rows.Count; i = i + 1)
            {
                for (int j = 0; j < _columnNamesInDt.Length; j = j + 1)
                {
                    if (_columnNamesInDt[j] != groupByColumnNames)
                    {
                        _dt.Rows[i][j] = dt.Compute(typeOfCalculation + "(" + _columnNamesInDt[j] + ")", groupByColumnNames + " = '" + _dt.Rows[i][groupByColumnNames].ToString() + "'");
                    }
                }
            }

            return _dt;
        }
        private void ProcessData()
        {
            DataTable dtbTB = Company.GC.BLL.GC.ThietBi.SelectAllNew().Tables[0];
            DataTable dtTable = tb.Clone();
            dtTable = GetGroupedBy(tb, "Ma,SoLuongTamNhap,LuongSD_TK,SoLuongChuyenHD,SoLuongTon", "Ma", "Sum");
            for (int i = -1; i <= dtTable.Rows.Count;++i)
            {
                try
                {
                    goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
                    goodItemDetail.STT = i+1;
                    goodItemDetail.ID_HopDong = HD.ID;//Convert.ToInt32(dtTable.Rows[i]["HopDong_ID"].ToString());
                    goodItemDetail.MaHangHoa = dtTable.Rows[i]["Ma"].ToString();
                    DataRow[] drTB = dtbTB.Select("Ma='" + goodItemDetail.MaHangHoa + "' AND HopDong_ID = " + HD.ID);
                    goodItemDetail.TenHangHoa = drTB[0]["Ten"].ToString(); ;//dtTable.Rows[i]["Ten"].ToString();
                    goodItemDetail.DVT = DVT_VNACC(drTB[0]["DVT_ID"].ToString());//DVT_VNACC(DonViTinh_GetID(dtTable.Rows[i]["DVT_ID"].ToString()));
                    goodItemDetail.MaHS = Convert.ToDecimal(drTB[0]["MaHS"].ToString());//Convert.ToDecimal(dtTable.Rows[i]["MaHS"].ToString());
                    HopDong hd = HopDong.Load(goodItemDetail.ID_HopDong);
                    goodItemDetail.SoHopDong = hd.SoHopDong.ToString();
                    goodItemDetail.NgayHopDong = hd.NgayDangKy;
                    goodItemDetail.NgayHetHan = hd.NgayHetHan;
                    goodItemDetail.MaHQ = hd.MaHaiQuan;
                    goodItemDetail.LuongTamNhap = Convert.ToDecimal(dtTable.Rows[i]["SoLuongTamNhap"].ToString());
                    goodItemDetail.LuongTaiXuat = Convert.ToDecimal(dtTable.Rows[i]["SoLuongChuyenHD"].ToString());
                    goodItemDetail.LuongChuyenTiep = 0;//Convert.ToDecimal(dtTable.Rows[i]["SoLuongTon"].ToString());
                    goodItemDetail.LuongConLai = goodItemDetail.LuongTamNhap - goodItemDetail.LuongTaiXuat;//Convert.ToDecimal(dtTable.Rows[i]["SoLuongTon"].ToString());
                    //goodItemDetail.GhiChu = dtTable.Rows[i]["GhiChu"].ToString();
                    goodItem.GoodItemDetailCollection.Add(goodItemDetail);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

            }
            KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail goodItemDetails = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
            goodItemDetails.STT = 1;
            goodItemDetails.SoHopDong = HD.SoHopDong;
            goodItemDetails.NgayHetHan = HD.NgayHetHan;
            goodItemDetails.NgayHopDong = HD.NgayDangKy;
            goodItemDetails.MaHQ = HD.MaHaiQuan;
            goodItemDetails.GhiChuKhac = HD.GhiChu;
            goodItemDetails.ID_HopDong = HD.ID;
            goodItem.GoodItemCollection.Add(goodItemDetails);
            BindData();

        }
        private void BindData()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItem.GoodItemDetailCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (lblXuLy.Text == "Đã xử lý xuất nhập tồn")
            {
                //foreach (DataRow dr in tb.Rows)
                //{
                //    if(dr[""])
                //}
            }
            else 
            {

                #region Tính Lương tồn
                DataTable temp = tb.DefaultView.ToTable(true,  "SoHopDong", "Ma", "SoLuongDaNhap");

                totalRows = temp.Rows.Count;
                progressBar1.Minimum = 0;
                try
                {
                    progressBar1.Maximum = 100;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

                backgroundWorker1.WorkerReportsProgress = true;
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    decimal tongNhap = 0;
                    //decimal tongSuDung = 0;
                    //decimal tongTon = 0;
                    foreach (DataRow dr in temp.Select())
                    {
                        //string ma1 = dr["Ma"].ToString();
                        tongNhap = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                        //tongSuDung = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                        //tongTon = Convert.ToDecimal(dr["LuongTon"].ToString());

                        decimal luongTK = 0;
                        //decimal luongSD = 0;
                        //decimal luongTon = 0;
                        

                        foreach (DataRow dr1 in tb.Select("Ma='" + dr["Ma"].ToString() + "' and SoHopDong = '" + dr["SoHopDong"].ToString() + "'", "NgayDangKy"))
                        {
                            luongTK = Convert.ToDecimal(dr1["SoLuong"].ToString());
                            
                                #region Tính toán thiết bị
                                try
                                {
                                    #region Tính lượng 

                                    if (tongNhap > luongTK)
                                    {
                                        dr1["LuongSD_TK"] = luongTK;
                                        dr1["SoLuongTon"] = 0;
                                        tongNhap = tongNhap - luongTK;
                                    }
                                    else if (tongNhap > 0 && tongNhap < luongTK)
                                    {
                                        dr1["LuongSD_TK"] = tongNhap;
                                        dr1["SoLuongTon"] = luongTK - tongNhap;
                                        tongNhap = 0;
                                    }
                                    else
                                    {
                                        dr1["LuongSD_TK"] = 0;
                                    }

                                    #endregion

                                }
                                catch (Exception ex)
                                {

                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                                #endregion Tính toán thiết bị



                        }

                        index++;
                        try
                        {
                            int i = (index * 100) / totalRows;
                            backgroundWorker1.ReportProgress(i);
                        }
                        catch (Exception)
                        {

                        }

                    }
                }

                #endregion

            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                
                progressBar1.Value = e.ProgressPercentage;
                lblPercen.Text = e.ProgressPercentage.ToString() + " %";
                lblXuLy.Text = "Đang xử lý...("+index+"/"+totalRows+")";
            }
            catch (Exception )
            {

            //    throw;
            }
            
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblXuLy.Text = "Đã xử lý xuất nhập tồn";
            btnXuLyXNT_Mau15.Enabled = false;
            btnXuatMau15.Enabled = true;
            //btnInBaoCao.Enabled = true;
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send("Send");
                    break;
                case "cmdSendEdit":
                    Send("Edit");
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                default:
                    break;
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_BaoCaoQuyetToan_MMTB", "", Convert.ToInt32(goodItem.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void BindDataHDGoodItem()
        {
            //try
            //{
            //    dgListHD.Refresh();
            //    dgListHD.DataSource = goodItem.GoodItemCollection;
            //    dgListHD.Refetch();
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    
            //}
        }
        public void BindDataGoodItem()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItem.GoodItemDetailCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void Save()
        {
            try
            {
                GetGoodItem();
                if (goodItem.GoodItemDetailCollection.Count ==0)
                {
                    return;
                }
                //if (goodItem != null)
                //{
                //    goodItemHDDetail.DeleteBy_GoodItems(goodItem.ID);
                //    goodItemDetail.DeleteBy_Contract_ID(goodItemHDDetail.ID);
                //}
                goodItem.InsertUpdateFull();
                //BindDataHDGoodItem();
                BindDataGoodItem();
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void GetGoodItem()
        {
            try
            {
                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                goodItem.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                goodItem.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void SetGoodItem()
        {
            try
            {
                string TrangThai = goodItem.TrangThaiXuLy.ToString();
                switch (TrangThai)
                {
                    case "0":
                        lblTrangThai.Text = "Chờ duyệt";
                        break;
                    case "-1":
                        lblTrangThai.Text = "Chưa khai báo";
                        break;
                    case "1":
                        lblTrangThai.Text = "Đã duyệt";
                        break;
                    case "2":
                        lblTrangThai.Text = "Không phê duyệt";
                        break;

                }
                txtSoTiepNhan.Text = goodItem.SoTN.ToString();
                dateNgayTN.Value = goodItem.NgayTN;
                goodItem.GoodItemCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.SelectCollectionDynamic("GoodItems = " + goodItem.ID, " ");
                goodItem.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionDynamic("Contract_ID = " + HD.ID, " ");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void Send(string Status)
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (goodItem.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    goodItem = KDT_VNACCS_BaoCaoQuyetToan_MMTB.Load(goodItem.ID);
                    goodItem.GoodItemCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.SelectCollectionDynamic("GoodItems = " + goodItem.ID, " ");
                    string List_ID = "";
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                    {
                        List_ID += item.ID.ToString() + ",";
                    }
                    List_ID = List_ID.Substring(0, List_ID.Length - 1);
                    goodItem.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionDynamic("Contract_ID IN (" + List_ID + ")", " ");
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ContractReference;
                sendXML.master_id = goodItem.ID;

                if (sendXML.Load())
                {
                    if (Status == "Send")
                    {
                        if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    goodItem.GuidStr = Guid.NewGuid().ToString();
                    ContractReference_VNACCS contractReference_VNACCS = new ContractReference_VNACCS();
                    contractReference_VNACCS = Mapper_V4.ToDataTransferContractReferences(goodItem, goodItemDetail, GlobalSettings.DIA_CHI, GlobalSettings.TEN_DON_VI, Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = goodItem.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ)),
                              Identity = goodItem.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = contractReference_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = goodItem.GuidStr,
                          },
                          contractReference_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterContractRefrence);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.ContractReference;
                        sendXML.master_id = goodItem.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        goodItem.Update();
                        Feedback();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = goodItem.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.ContractReference,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.ContractReference,
                };
                subjectBase.Type = DeclarationIssuer.ContractReference;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = goodItem.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ.Trim())),
                                                  Identity = goodItem.MaHQ
                                              }, subjectBase, null);
                if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = goodItem.ID;
                form.DeclarationIssuer = DeclarationIssuer.ContractReference;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName ="Tổng hợp số liệu báo cáo_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void ReadExcelFile_Click(object sender, EventArgs e)
        {
            try
            {
                goodItem.GoodItemDetailCollection.Clear();
                goodItem.GoodItemCollection.Clear();
                IsReadExcel = true;
                ReadExcelFormMMTB f = new ReadExcelFormMMTB();
                f.goodItem = goodItem;
                f.ShowDialog(this);
                BindDataGoodItem();
                BindDataHDGoodItem();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ContractRefrenceSendHandler(goodItem, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

    }
}