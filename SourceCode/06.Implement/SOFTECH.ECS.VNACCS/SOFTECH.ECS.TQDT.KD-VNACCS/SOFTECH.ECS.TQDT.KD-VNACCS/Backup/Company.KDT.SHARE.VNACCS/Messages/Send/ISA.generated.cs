using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class ISA : BasicVNACC
    {
public PropertiesAttribute APP { get; set; }
public PropertiesAttribute STS { get; set; }
public PropertiesAttribute APN { get; set; }
public PropertiesAttribute KBN { get; set; }

public ISA()
        {
APP = new PropertiesAttribute(6, typeof(string));
STS = new PropertiesAttribute(2, typeof(string));
APN = new PropertiesAttribute(20, typeof(string));
KBN = new PropertiesAttribute(1, typeof(string));
}

}public partial class EnumGroupID
    {
}

}
