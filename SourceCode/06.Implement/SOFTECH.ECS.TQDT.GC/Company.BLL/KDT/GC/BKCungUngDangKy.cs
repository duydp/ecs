﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
namespace Company.GC.BLL.KDT.GC
{
    public partial class BKCungUngDangKy
    {
        private SanPhanCungUngCollection _spCUCollection;
        public SanPhanCungUngCollection SanPhamCungUngCollection
        {
            set { _spCUCollection = value; }
            get { return _spCUCollection; }
        }
        public void LoadSanPhamCungUngCollection()
        {
            SanPhanCungUng sp = new SanPhanCungUng();
            sp.Master_ID = this.ID;
            this._spCUCollection = sp.SelectCollectionBy_Master_ID();
        }
        public void LoadSanPhamCungUngCollection(string database)
        {
            SanPhanCungUng sp = new SanPhanCungUng();
            sp.SetDabaseMoi(database);
            sp.Master_ID = this.ID;
            this._spCUCollection = sp.SelectCollectionBy_Master_ID();
        }
        public void LoadSanPhamCungUngCollection(SqlTransaction transaction)
        {
            SanPhanCungUng sp = new SanPhanCungUng();
            sp.Master_ID = this.ID;
            this._spCUCollection = sp.SelectCollectionBy_Master_ID(transaction);
        }
        public void LoadSanPhamCungUngCollection(SqlTransaction transaction, string database)
        {
            SanPhanCungUng sp = new SanPhanCungUng();
            sp.Master_ID = this.ID;
            this._spCUCollection = sp.SelectCollectionBy_Master_ID(transaction, database);
        }
        public bool InsertUpdateFull()
        {
            HopDong HD = new HopDong();
            if (this.TKCT_ID > 0)
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = TKCT_ID;
                TKCT.Load();
                HD.ID = TKCT.IDHopDong;
            }
            else
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = this.TKMD_ID;
                TKMD.Load();
                HD.ID = TKMD.IDHopDong;
            }

            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    foreach (SanPhanCungUng sp in this.SanPhamCungUngCollection)
                    {

                        long id_SP = sp.ID;
                        if (sp.ID == 0)
                        {
                            sp.Master_ID = this.ID;
                            sp.ID = sp.InsertTransaction(transaction);
                        }
                        else
                        {
                            sp.UpdateTransaction(transaction);
                        }
                        if (sp.NPLCungUngCollection == null) continue;
                        if (id_SP > 0)
                        {
                            NguyenPhuLieuCungUngCollection nplCollection = new NguyenPhuLieuCungUngCollection();
                            NguyenPhuLieuCungUng nplCungUng = (new NguyenPhuLieuCungUng());
                            nplCungUng.Master_ID = id_SP;
                            nplCollection = nplCungUng.SelectCollectionBy_Master_ID(transaction);
                            foreach (NguyenPhuLieuCungUng nplDelete in nplCollection)
                            {
                                nplDelete.Load(transaction);
                                nplDelete.Delete(HD, transaction, sp);
                            }
                        }
                        foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                        {
                            npl.Master_ID = sp.ID;
                            npl.ID = npl.InsertTransaction(transaction);
                            Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                            NPLDuyet.HopDong_ID = HD.ID;
                            NPLDuyet.Ma = npl.MaNguyenPhuLieu;
                            NPLDuyet.Load(transaction);
                            //cap nhat luong moi
                            //NPLDuyet.SoLuongDaDung += npl.LuongCung;
                            NPLDuyet.SoLuongCungUng += npl.LuongCung;
                            //tru di luong cu da cong vao truoc do
                            //decimal DinhMucChung = (new Company.GC.BLL.GC.DinhMuc()).GetDinhMucChungOfMaSPAndMaNPL(sp.MaSanPham, npl.MaNguyenPhuLieu, HD.ID,transaction);
                            //decimal LuongSuDung = DinhMucChung * sp.LuongCUSanPham;
                            //NPLDuyet.SoLuongDaDung -= LuongSuDung;
                            NPLDuyet.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.ID = this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);
            foreach (SanPhanCungUng sp in this.SanPhamCungUngCollection)
            {
                if (sp.ID == 0)
                {
                    sp.Master_ID = this.ID;
                    sp.ID = sp.InsertTransaction(transaction);
                }
                else
                {
                    sp.UpdateTransaction(transaction);
                }
                foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                {
                    if (npl.ID == 0)
                    {
                        npl.Master_ID = sp.ID;
                        npl.ID = npl.InsertTransaction(transaction);
                    }
                    else
                    {
                        npl.UpdateTransaction(transaction);
                    }

                }
            }
        }
        public void InsertUpdateDongBoDuLieu()
        {
            HopDong HD = new HopDong();
            if (this.TKCT_ID > 0)
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = TKCT_ID;
                TKCT.Load();
                HD.ID = TKCT.IDHopDong;
            }
            else
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = this.TKMD_ID;
                TKMD.Load();
                HD.ID = TKMD.IDHopDong;
            }

            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    foreach (SanPhanCungUng sp in this.SanPhamCungUngCollection)
                    {
                        if (id == 0)
                            sp.ID = 0;
                        long id_SP = sp.ID;
                        if (sp.ID == 0)
                        {
                            sp.Master_ID = this.ID;
                            sp.ID = sp.InsertTransaction(transaction);
                        }
                        else
                        {
                            sp.UpdateTransaction(transaction);
                        }
                        foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                        {
                            npl.Master_ID = sp.ID;
                            npl.ID = npl.InsertTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDongBoDuLieu(string name)
        {
            HopDong HD = new HopDong();
            if (this.TKCT_ID > 0)
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = TKCT_ID;
                TKCT.SetDabaseMoi(name);
                TKCT.Load();
                HD.ID = TKCT.IDHopDong;
            }
            else
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = this.TKMD_ID;
                TKMD.SetDabaseMoi(name);
                TKMD.Load();
                HD.ID = TKMD.IDHopDong;
            }

            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(name);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    foreach (SanPhanCungUng sp in this.SanPhamCungUngCollection)
                    {
                        if (id == 0)
                            sp.ID = 0;
                        long id_SP = sp.ID;
                        if (sp.ID == 0)
                        {
                            sp.Master_ID = this.ID;
                            sp.ID = sp.InsertTransaction(transaction);
                        }
                        else
                        {
                            sp.UpdateTransaction(transaction);
                        }
                        foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                        {
                            npl.Master_ID = sp.ID;
                            npl.ID = npl.InsertTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void DeleteBangKeCungUngOfHopDong(SqlTransaction tran, HopDong HD)
        {
            string sqlDeleteTKMD = "delete from t_KDT_GC_BKCungUngDangKy where TKMD_ID in (select ID from t_KDT_ToKhaiMauDich where IDHopDong=" + HD.ID + ")";
            SqlCommand dbCommandDeleteTKMD = (SqlCommand)db.GetSqlStringCommand(sqlDeleteTKMD);
            db.ExecuteNonQuery(dbCommandDeleteTKMD, tran);

            string sqlDeleteTKCT = "delete from t_KDT_GC_BKCungUngDangKy where TKCT_ID in (select ID from t_KDT_GC_ToKhaiChuyenTiep where IDHopDong=" + HD.ID + ")";
            SqlCommand dbCommandDeleteTKCT = (SqlCommand)db.GetSqlStringCommand(sqlDeleteTKCT);
            db.ExecuteNonQuery(dbCommandDeleteTKCT, tran);
        }
        public void DeleteBangKeCungUngOfHopDong(SqlTransaction tran, HopDong HD, string dbName)
        {
            SetDabaseMoi(dbName);

            string sqlDeleteTKMD = "delete from t_KDT_GC_BKCungUngDangKy where TKMD_ID in (select ID from t_KDT_ToKhaiMauDich where IDHopDong=" + HD.ID + ")";
            SqlCommand dbCommandDeleteTKMD = (SqlCommand)db.GetSqlStringCommand(sqlDeleteTKMD);
            db.ExecuteNonQuery(dbCommandDeleteTKMD, tran);

            string sqlDeleteTKCT = "delete from t_KDT_GC_BKCungUngDangKy where TKCT_ID in (select ID from t_KDT_GC_ToKhaiChuyenTiep where IDHopDong=" + HD.ID + ")";
            SqlCommand dbCommandDeleteTKCT = (SqlCommand)db.GetSqlStringCommand(sqlDeleteTKCT);
            db.ExecuteNonQuery(dbCommandDeleteTKCT, tran);
        }

        public void InsertUpdateDongBoDuLieuMOi(BKCungUngDangKyCollection Collection, HopDong HD)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                //BKCungUngDangKy BKCungUng = new BKCungUngDangKy();
                //BKCungUng.DeleteBangKeCungUngOfHopDong(transaction, HD);              
                try
                {
                    foreach (BKCungUngDangKy item in Collection)
                    {
                        if (item.ID == 0)
                            item.ID = item.InsertTransaction(transaction);
                        else
                            item.UpdateTransaction(transaction);
                        foreach (SanPhanCungUng sp in item.SanPhamCungUngCollection)
                        {
                            if (sp.ID == 0)
                            {
                                sp.Master_ID = item.ID;
                                sp.ID = sp.InsertTransaction(transaction);
                            }
                            else
                            {
                                sp.UpdateTransaction(transaction);
                            }
                            foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                            {
                                npl.Master_ID = sp.ID;
                                npl.ID = npl.InsertTransaction(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateDongBoDuLieuMOiKTX(BKCungUngDangKyCollection Collection, HopDong HD)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                //BKCungUngDangKy BKCungUng = new BKCungUngDangKy();
                //BKCungUng.DeleteBangKeCungUngOfHopDong(transaction, HD);              
                try
                {
                    foreach (BKCungUngDangKy item in Collection)
                    {
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionKTX(transaction);
                        else
                            item.UpdateTransactionKTX(transaction);

                        foreach (SanPhanCungUng sp in item.SanPhamCungUngCollection)
                        {
                            if (sp.ID == 0)
                            {
                                sp.Master_ID = item.ID;
                                sp.ID = sp.InsertTransaction(transaction);
                            }
                            else
                            {
                                sp.UpdateTransaction(transaction);
                            }
                            foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                            {
                                npl.Master_ID = sp.ID;
                                npl.ID = npl.InsertTransaction(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateDongBoDuLieuMOiKTX(BKCungUngDangKyCollection Collection, HopDong HD, string databaseName)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                //BKCungUngDangKy BKCungUng = new BKCungUngDangKy();
                //BKCungUng.DeleteBangKeCungUngOfHopDong(transaction, HD);              
                try
                {
                    foreach (BKCungUngDangKy item in Collection)
                    {
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionKTX(transaction, databaseName);
                        else
                            item.UpdateTransactionKTX(transaction, databaseName);

                        foreach (SanPhanCungUng sp in item.SanPhamCungUngCollection)
                        {
                            if (sp.ID == 0)
                            {
                                sp.Master_ID = item.ID;
                                sp.ID = sp.InsertTransaction(transaction, databaseName);
                            }
                            else
                            {
                                sp.UpdateTransaction(transaction, databaseName);
                            }
                            foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                            {
                                npl.Master_ID = sp.ID;
                                npl.ID = npl.InsertTransaction(transaction, databaseName);
                            }
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpdateDongBoDuLieu(BKCungUngDangKyCollection Collection, HopDong HD)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                BKCungUngDangKy BKCungUng = new BKCungUngDangKy();
                BKCungUng.DeleteBangKeCungUngOfHopDong(transaction, HD);
                try
                {
                    foreach (BKCungUngDangKy item in Collection)
                    {
                        if (item.ID == 0)
                            item.ID = item.InsertTransaction(transaction);
                        else
                            item.UpdateTransaction(transaction);
                        foreach (SanPhanCungUng sp in item.SanPhamCungUngCollection)
                        {
                            if (sp.ID == 0)
                            {
                                sp.Master_ID = item.ID;
                                sp.ID = sp.InsertTransaction(transaction);
                            }
                            else
                            {
                                sp.UpdateTransaction(transaction);
                            }
                            foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                            {
                                npl.Master_ID = sp.ID;
                                npl.ID = npl.InsertTransaction(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateDongBoDuLieu(BKCungUngDangKyCollection Collection, HopDong HD, string dbName)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(dbName);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                //BKCungUngDangKy BKCungUng = new BKCungUngDangKy();
                //BKCungUng.DeleteBangKeCungUngOfHopDong(transaction, HD, dbName);

                try
                {
                    foreach (BKCungUngDangKy item in Collection)
                    {
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionTQDT(transaction, dbName);
                        else
                            item.UpdateTransactionTQDT(transaction, dbName);

                        foreach (SanPhanCungUng sp in item.SanPhamCungUngCollection)
                        {
                            if (sp.ID == 0)
                            {
                                sp.Master_ID = item.ID;
                                sp.ID = sp.InsertTransaction(transaction, dbName);
                            }
                            else
                            {
                                sp.UpdateTransaction(transaction, dbName);
                            }

                            foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                            {
                                npl.Master_ID = sp.ID;
                                npl.ID = npl.InsertTransaction(transaction, dbName);
                            }
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateDongBoDuLieuKTX(BKCungUngDangKyCollection Collection, HopDong HD)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                BKCungUngDangKy BKCungUng = new BKCungUngDangKy();
                BKCungUng.DeleteBangKeCungUngOfHopDong(transaction, HD);
                try
                {
                    foreach (BKCungUngDangKy item in Collection)
                    {
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionKTX(transaction);
                        else
                            item.UpdateTransactionKTX(transaction);
                        foreach (SanPhanCungUng sp in item.SanPhamCungUngCollection)
                        {
                            if (sp.ID == 0)
                            {
                                sp.Master_ID = item.ID;
                                sp.ID = sp.InsertTransaction(transaction);
                            }
                            else
                            {
                                sp.UpdateTransaction(transaction);
                            }
                            foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                            {
                                npl.Master_ID = sp.ID;
                                npl.ID = npl.InsertTransaction(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateDongBoDuLieuKTX(BKCungUngDangKyCollection Collection, HopDong HD, string dbName)
        {
         
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(dbName);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                //BKCungUngDangKy BKCungUng = new BKCungUngDangKy();
                //BKCungUng.DeleteBangKeCungUngOfHopDong(transaction, HD, dbName);

                try
                {
                    ToKhaiMauDich tkmdTemp = new ToKhaiMauDich();
                    ToKhaiChuyenTiep tkctTemp = new ToKhaiChuyenTiep();
                    bool isTKMD, isTKCT;
                    foreach (BKCungUngDangKy item in Collection)
                    {
                        
                        tkmdTemp.SoToKhai = item.SoToKhai;
                        tkmdTemp.MaLoaiHinh = item.MaLoaiHinh;
                        tkmdTemp.NamDK = item.NgayTiepNhan.Year;
                        tkmdTemp.MaHaiQuan = item.MaHaiQuan;
                        tkmdTemp.MaDoanhNghiep = item.MaDoanhNghiep;

                        
                        isTKMD = tkmdTemp.LoadBySoToKhai(transaction, item.NgayTiepNhan.Year, dbName);
                        
                        tkctTemp.SoToKhai = item.SoToKhai;
                        tkctTemp.MaLoaiHinh = item.MaLoaiHinh;
                        tkctTemp.NamDK = short.Parse(item.NgayTiepNhan.Year.ToString());
                        tkctTemp.MaHaiQuanTiepNhan = item.MaHaiQuan;
                        tkctTemp.MaDoanhNghiep = item.MaDoanhNghiep;

                        isTKCT = tkctTemp.LoadBySoToKhai(transaction, item.NgayTiepNhan.Year, dbName);

                        item.ID = 0;
                        item.TKMD_ID = isTKMD ? tkmdTemp.ID : 0;
                        item.TKCT_ID = isTKCT ? tkctTemp.ID : 0;
                        item.InsertUpdateTransactionKTX(transaction, dbName);
                        
                        BKCungUngDangKy bkcuTemp = new BKCungUngDangKy();
                        bkcuTemp.SoTiepNhan = item.SoTiepNhan;
                        bkcuTemp.NgayTiepNhan = item.NgayTiepNhan;
                        bkcuTemp.TKMD_ID = item.TKMD_ID;
                        bkcuTemp.TKCT_ID = item.TKCT_ID;
                        bkcuTemp.MaHaiQuan = item.MaHaiQuan;
                        bkcuTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                        bkcuTemp.SoBangKe = item.SoBangKe;
                        bkcuTemp.LoadKTX(transaction, dbName);
                        item.LoadSanPhamCungUngCollection(dbName);
                        foreach (SanPhanCungUng sp in item.SanPhamCungUngCollection)
                        {
                            sp.Master_ID = bkcuTemp.ID;
                            sp.ID = sp.InsertUpdateTransaction(transaction, dbName);

                            SanPhanCungUng spcuTemp = new SanPhanCungUng();
                            spcuTemp.Master_ID = bkcuTemp.ID;
                            spcuTemp.MaSanPham = sp.MaSanPham;
                            spcuTemp.Load(transaction, dbName);

                            foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                            {
                                npl.Master_ID = spcuTemp.ID;
                                npl.ID = npl.InsertUpdateTransaction(transaction, dbName);
                            }
                        }
                    }

                    transaction.Commit();
  
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransactionTQDT(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_BKCungUngDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_BKCungUngDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_BKCungUngDangKy_Insert_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransactionTQDT(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_BKCungUngDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_BKCungUngDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_BKCungUngDangKy_Update_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_BKCungUngDangKy_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, this._Huongdan_PL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransactionKTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_BKCungUngDangKy_InsertUpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public bool Load(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_BKCungUngDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) this._TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) this._SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) this._NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) this._Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool LoadKTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_BKCungUngDangKy_LoadByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, this._TKMD_ID);
            this.db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, this._TKCT_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this._SoBangKe);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) this._TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) this._SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public void Delete()
        {
            HopDong HD = new HopDong();
            if (this.TKCT_ID > 0)
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = TKCT_ID;
                TKCT.Load();
                HD.ID = TKCT.IDHopDong;
            }
            else
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = this.TKMD_ID;
                TKMD.Load();
                HD.ID = TKMD.IDHopDong;
            }
            if (this.SanPhamCungUngCollection == null || this.SanPhamCungUngCollection.Count == 0)
            {
                this.LoadSanPhamCungUngCollection();
            }
            foreach (SanPhanCungUng spCungUng in this.SanPhamCungUngCollection)
                spCungUng.LoadNPLCungUngCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (SanPhanCungUng spCungUng in this.SanPhamCungUngCollection)
                        spCungUng.Delete(HD, transaction);
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpdateBKCungUngXuLyDuLieu(SqlTransaction transaction)
        {
            HopDong HD = new HopDong();
            if (this.TKCT_ID > 0)
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT.ID = TKCT_ID;
                TKCT.Load(transaction);
                HD.ID = TKCT.IDHopDong;
            }
            else
            {
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.ID = this.TKMD_ID;
                TKMD.Load(transaction);
                HD.ID = TKMD.IDHopDong;
            }

            foreach (SanPhanCungUng sp in this.SanPhamCungUngCollection)
            {
                foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                {
                    Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                    NPLDuyet.HopDong_ID = HD.ID;
                    NPLDuyet.Ma = npl.MaNguyenPhuLieu;
                    NPLDuyet.Load(transaction);
                    //cap nhat luong moi
                    //NPLDuyet.SoLuongDaDung += npl.LuongCung;
                    NPLDuyet.SoLuongCungUng += npl.LuongCung;
                    //tru di luong cu da cong vao truoc do
                    //decimal DinhMucChung = (new Company.GC.BLL.GC.DinhMuc()).GetDinhMucChungOfMaSPAndMaNPL(sp.MaSanPham, npl.MaNguyenPhuLieu, HD.ID,transaction);
                    //decimal LuongSuDung = DinhMucChung * sp.LuongCUSanPham;
                    //NPLDuyet.SoLuongDaDung -= LuongSuDung;
                    NPLDuyet.UpdateTransaction(transaction);
                }
            }
        }
        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_BKCungUngDangKy_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand, transaction);
        }
        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_BKCungUngDangKy_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            if (transaction != null)
                return this.db.ExecuteReader(dbCommand, transaction);
            else
                return this.db.ExecuteReader(dbCommand);
        }
        public BKCungUngDangKyCollection SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction)
        {
            BKCungUngDangKyCollection collection = new BKCungUngDangKyCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression, transaction);
            while (reader.Read())
            {
                BKCungUngDangKy entity = new BKCungUngDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) entity.TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) entity.SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public BKCungUngDangKyCollection SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction, string dbName)
        {
            BKCungUngDangKyCollection collection = new BKCungUngDangKyCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression, transaction, dbName);
            while (reader.Read())
            {
                BKCungUngDangKy entity = new BKCungUngDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) entity.TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) entity.SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static long GetIDBangKeBySoBangKeAndNamDangKy(string MaHQ, long SoBangKe, int NamDK)
        {
            string sql = "select ID from t_KDT_GC_BKCungUngDangKy where SoBangKe=" + SoBangKe + " and year(NgayTiepNhan)=" + NamDK + " and MaHaiQuan='" + MaHQ + "' ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object id = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(id);
        }

        public static long GetIDBangKeBySoBangKeAndNamDangKy(string MaHQ, long SoBangKe, int NamDK, string name)
        {
            string sql = "select ID from t_KDT_GC_BKCungUngDangKy where SoBangKe=" + SoBangKe + " and year(NgayTiepNhan)=" + NamDK + " and MaHaiQuan='" + MaHQ + "' ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(name);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object id = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(id);
        }
        public BKCungUngDangKyCollection XuatBKCungUngDangKy(string sqlThamSo)
        {
            string sql = "select * from t_KDT_GC_BKCungUngDangKy where " + sqlThamSo;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            BKCungUngDangKyCollection collection = new BKCungUngDangKyCollection();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                BKCungUngDangKy entity = new BKCungUngDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) entity.TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) entity.SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = FontConverter.Unicode2TCVN(Company.GC.BLL.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            //DATLMQ bổ sung reference cho Định mức 07/01/2011
            //this.GUIDSTR = (System.Guid.NewGuid().ToString().ToUpper());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep.Trim();
            this.Update();
            return doc.InnerXml;
        }
        private string ConvertCollectionToXML()
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";

            HopDong HD = new HopDong();
            ToKhaiChuyenTiep tkct = null;
            ToKhaiMauDich tkmd = null;
            if (this.TKMD_ID > 0)
            {
                tkmd = new ToKhaiMauDich();
                tkmd.ID = this.TKMD_ID;
                tkmd.Load();
                HD.ID = tkmd.IDHopDong;
            }
            else
            {
                tkct = new ToKhaiChuyenTiep();
                tkct.ID = this.TKCT_ID;
                tkct.Load();
                HD.ID = tkct.IDHopDong;
            }
            HD.Load();
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\B03GiaCong\\KhaiBaoNPLTuCungUng.XML");
            docNPL.SelectSingleNode("Root/BKNPLCU").Attributes["So_HD"].Value = HD.SoHopDong;
            docNPL.SelectSingleNode("Root/BKNPLCU").Attributes["Ma_HQHD"].Value = HD.MaHaiQuan;
            docNPL.SelectSingleNode("Root/BKNPLCU").Attributes["DVGC"].Value = HD.MaDoanhNghiep;
            docNPL.SelectSingleNode("Root/BKNPLCU").Attributes["Ngay_Ky"].Value = HD.NgayKy.ToString("MM/dd/yyyy");
            if (tkct != null)
                docNPL.SelectSingleNode("Root/BKNPLCU").Attributes["MA_LH"].Value = tkct.MaLoaiHinh;
            else
                docNPL.SelectSingleNode("Root/BKNPLCU").Attributes["MA_LH"].Value = tkmd.MaLoaiHinh;
            if (tkct == null)
                docNPL.SelectSingleNode("Root/BKNPLCU").Attributes["SoTKDN"].Value = tkmd.SoTiepNhan.ToString();
            else
                docNPL.SelectSingleNode("Root/BKNPLCU").Attributes["SoTKDN"].Value = tkct.SoTiepNhan.ToString();

            XmlNode nodeSPs = docNPL.SelectSingleNode("Root/SanPhams");
            if (SanPhamCungUngCollection.Count == 0)
                LoadSanPhamCungUngCollection();
            foreach (SanPhanCungUng spCungUng in this.SanPhamCungUngCollection)
            {
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = spCungUng.MaSanPham;
                sp.Load();

                XmlElement nodeSanPham = docNPL.CreateElement("SanPham");

                XmlAttribute attMaSP = docNPL.CreateAttribute("MaSP");
                attMaSP.Value = sp.Ma;
                nodeSanPham.Attributes.Append(attMaSP);

                XmlAttribute attTenSP = docNPL.CreateAttribute("TenSP");
                attTenSP.Value = FontConverter.Unicode2TCVN(sp.Ten);
                nodeSanPham.Attributes.Append(attTenSP);

                XmlAttribute attSoLuongSP = docNPL.CreateAttribute("SoLuongSP");
                attSoLuongSP.Value = spCungUng.LuongCUSanPham.ToString(f);
                nodeSanPham.Attributes.Append(attSoLuongSP);

                XmlAttribute attMaDVT = docNPL.CreateAttribute("MaDVT");
                attMaDVT.Value = sp.DVT_ID;
                nodeSanPham.Attributes.Append(attMaDVT);
                if (spCungUng.NPLCungUngCollection.Count == 0)
                    spCungUng.LoadNPLCungUngCollection();
                XmlElement nodeNPLs = docNPL.CreateElement("NPLs");
                nodeSanPham.AppendChild(nodeNPLs);
                nodeSPs.AppendChild(nodeSanPham);
                if (spCungUng.NPLCungUngCollection.Count == 0)
                    spCungUng.LoadNPLCungUngCollection();
                foreach (NguyenPhuLieuCungUng nplCungUng in spCungUng.NPLCungUngCollection)
                {
                    XmlElement nodeNPL = docNPL.CreateElement("NPL");

                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = nplCungUng.MaNguyenPhuLieu;
                    npl.Load();
                    XmlAttribute attMaNPL = docNPL.CreateAttribute("MaNPL");
                    attMaNPL.Value = npl.Ma;
                    nodeNPL.Attributes.Append(attMaNPL);

                    XmlAttribute attTenNPL = docNPL.CreateAttribute("TenNPL");
                    attTenNPL.Value = FontConverter.Unicode2TCVN(npl.Ten);
                    nodeNPL.Attributes.Append(attTenNPL);

                    XmlAttribute attMaDVTNPL = docNPL.CreateAttribute("MaDVT");
                    attMaDVTNPL.Value = npl.DVT_ID;
                    nodeNPL.Attributes.Append(attMaDVTNPL);

                    XmlAttribute attLuongCU = docNPL.CreateAttribute("LuongCU");
                    attLuongCU.Value = nplCungUng.LuongCung.ToString(f);
                    nodeNPL.Attributes.Append(attLuongCU);

                    XmlAttribute attDonGia = docNPL.CreateAttribute("DonGia");
                    attDonGia.Value = nplCungUng.DonGia.ToString(f);
                    nodeNPL.Attributes.Append(attDonGia);

                    XmlAttribute attTriGia = docNPL.CreateAttribute("TriGia");
                    attTriGia.Value = nplCungUng.TriGia.ToString(f);
                    nodeNPL.Attributes.Append(attTriGia);

                    XmlAttribute attDMGC = docNPL.CreateAttribute("DMGC");
                    attDMGC.Value = nplCungUng.DinhMucCungUng.ToString("N8", f);
                    nodeNPL.Attributes.Append(attDMGC);

                    XmlAttribute attTLHH = docNPL.CreateAttribute("TLHH");
                    attTLHH.Value = nplCungUng.TyLeHH.ToString("N2", f);
                    nodeNPL.Attributes.Append(attTLHH);

                    XmlAttribute attHinhThucCU = docNPL.CreateAttribute("HinhThucCU");
                    attHinhThucCU.Value = FontConverter.Unicode2TCVN(nplCungUng.HinhThuCungUng);
                    nodeNPL.Attributes.Append(attHinhThucCU);
                    nodeNPLs.AppendChild(nodeNPL);
                }
                nodeSPs.AppendChild(nodeSanPham);
            }
            return docNPL.InnerXml;
        }
        public string WSSend(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DinhMucCungUng, MessgaseFunction.KhaiBao));

            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXML());
            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoNPLTuCungUng);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        //XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                        //XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                        //nodeRoot.RemoveChild(node);
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError =FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            return "";
        }

        public string WSRequest(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DinhMucCungUng, MessgaseFunction.HoiTrangThai));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\B03GiaCong\\LayPhanHoiDinhMucCungUng.xml");
            docNPL.SelectSingleNode("Root/So_TNDKDT").InnerText = this.SoTiepNhan.ToString();
            docNPL.SelectSingleNode("Root/Nam_TN").InnerText = this.NgayTiepNhan.Year.ToString();
            docNPL.SelectSingleNode("Root/Ma_HQTN").InnerText = this.MaHaiQuan.ToString();
            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);                
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if(msgError!=string.Empty)
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }

            return "";
        }

        public string WSCancel(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DinhMucCungUng, MessgaseFunction.HuyKhaiBao));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\B03GiaCong\\HuyKhaiBaoDinhMucCungUng.xml");
            docNPL.SelectSingleNode("Root/So_TNDKDT").InnerText = this.SoTiepNhan.ToString();
            docNPL.SelectSingleNode("Root/Nam_TN").InnerText = this.NgayTiepNhan.Year.ToString();
            docNPL.SelectSingleNode("Root/Ma_HQTN").InnerText = this.MaHaiQuan.ToString();
            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            //int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            //doc.GetElementsByTagName("function")[0].InnerText = "5";
            doc.GetElementsByTagName("function")[0].InnerText = MessgaseFunction.LayPhanHoi.ToString();
            doc.GetElementsByTagName("type")[0].InnerText = MessgaseType.ThongTin.ToString();

            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                string msgError = string.Empty;
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty)
                    {
                        kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                        throw ex;
                    }
                    else Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            bool ok = false;
            try
            {
                /*
                if (function == MessgaseFunction.KhaiBao)
                {
                    this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                    this.NgayTiepNhan = DateTime.Today;
                    this.TrangThaiXuLy = 0;
                    ok = true;
                }
                else if (function == MessgaseFunction.HuyKhaiBao)
                {
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.TrangThaiXuLy = -1;
                    ok = true;
                }
                else if (function == MessgaseFunction.HoiTrangThai)
                {
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/@TrangThaiXL").InnerText == "1")
                    {
                        this.TrangThaiXuLy = 1;
                        this.SoBangKe = Convert.ToInt64(docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SoTN"].Value);
                        //this.NgayDangKy = Convert.ToDateTime(docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").Attributes["NgayDK"].Value);
                        //TransgferDataToSXXK();
                        ok = true;
                        try
                        {
                            XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                            if (nodeMess != null)
                            {
                                return nodeMess.OuterXml;
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        try
                        {
                            XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                            if (nodeMess != null)
                            {
                                return nodeMess.OuterXml;
                            }
                        }
                        catch { }
                    }
                }
                 * 
                //DATLMQ update Phân tích message trả về 09/01/2011
                else*/

                if (function == MessgaseFunction.LayPhanHoi)
                {
                    XmlNode nodeSoTN = docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SoTN"];
                    if (nodeSoTN != null)
                    {
                        this.SoTiepNhan = Convert.ToInt64(nodeSoTN.InnerText);
                        this.NgayTiepNhan = Convert.ToDateTime(docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["NgayTN"].InnerText);
                        this.ActionStatus = 0;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                        this.Update();
                        kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoNPLTuCungUngLayPhanHoi,
                            string.Format("Số tiếp nhận {0},Ngày tiếp nhận {1} ", SoTiepNhan,NgayTiepNhan));
                    }
                    else if (docNPL.SelectSingleNode("Envelope/Header/Subject/function").InnerText=="2")//hủy
                    {
                        
                        this.SoTiepNhan = 0;
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.Update();
                        xml.XmlSaveMessage(ID, MessageTitle.KhaiBaoHuyNPLTuCungUng);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            if (ok)
                this.Update();
            return "";

        }


        public bool LoadBySoBangKe()
        {
            string spName = "select * from t_KDT_GC_BKCungUngDangKy where SoBangKe=@SoBangKe and MaHaiQuan=@MaHaiQuan and year(NgayTiepNhan)=@NamDK";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoBangKe", SqlDbType.BigInt, this.SoBangKe);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.Int, this.NgayTiepNhan.Year);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this._TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) this._TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) this._SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public static void GetSanPhamCungUngByPhanBo(long ID_TKMD, BKCungUngDangKy BKCU, HopDong HD, int SoThapPhanSP)
        {
            string sql = "SELECT tgpbtkx.MaSP,tgpbtkx.SoLuongXuat,tgpbtkn.SoToKhaiNhap,tgpbtkn.MaLoaiHinhNhap,tgpbtkn.MaHaiQuanNhap,tgpbtkn.NamDangKyNhap " +
                        ",tgpbtkn.MaNPL,tgpbtkn.LuongPhanBo,tgpbtkn.LuongCungUng,tgpbtkx.ID_TKMD ,tgpbtkn.DinhMucChung " +
                        "FROM t_GC_PhanBoToKhaiXuat tgpbtkx " +
                        "INNER JOIN t_GC_PhanBoToKhaiNhap tgpbtkn ON tgpbtkn.TKXuat_ID = tgpbtkx.ID " +
                        "WHERE (tgpbtkn.MaLoaiHinhNhap LIKE '%SX%' OR tgpbtkn.LuongCungUng>0) AND tgpbtkx.ID_TKMD=@ID_TKMD order by masp";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            string masp = "";
            SanPhanCungUng spCungUng = null;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (masp.Trim() != row["MaSP"].ToString().Trim())
                {
                    masp = row["MaSP"].ToString().Trim();
                    spCungUng = new SanPhanCungUng();
                    spCungUng.MaSanPham = masp;
                    spCungUng.LuongCUSanPham = Convert.ToDecimal(row["SoLuongXuat"]);
                    spCungUng.NPLCungUngCollection = new NguyenPhuLieuCungUngCollection();
                    BKCU.SanPhamCungUngCollection.Add(spCungUng);
                }
                string maloaihinh = row["MaLoaiHinhNhap"].ToString();
                NguyenPhuLieuCungUng nplCungUng = new NguyenPhuLieuCungUng();
                nplCungUng.MaNguyenPhuLieu = row["MaNPL"].ToString();
                bool isAdd = true;
                foreach (NguyenPhuLieuCungUng itemTest in spCungUng.NPLCungUngCollection)
                {
                    if (itemTest.MaNguyenPhuLieu.Trim().ToUpper() == nplCungUng.MaNguyenPhuLieu.Trim().ToUpper())
                    {
                        isAdd = false;
                        nplCungUng = itemTest;
                        break;
                    }
                }
                if (maloaihinh.Contains("SX"))
                {
                    //if (nplCungUng.MaNguyenPhuLieu == "640699900017" && masp == "SR7700-05B")
                    //{
                    //}

                    if (nplCungUng.LuongCung == 0)
                        nplCungUng.HinhThuCungUng = "Cung ung to khai : " + row["SoToKhaiNhap"].ToString() + row["MaLoaiHinhNhap"].ToString() + row["NamDangKyNhap"].ToString();
                    else
                        nplCungUng.HinhThuCungUng += ";" + row["SoToKhaiNhap"].ToString() + row["MaLoaiHinhNhap"].ToString() + row["NamDangKyNhap"].ToString();
                    nplCungUng.LuongCung += Convert.ToDecimal(row["LuongPhanBo"]) + Convert.ToDecimal(row["LuongCungUng"]);
                    if (Convert.ToDecimal(row["LuongCungUng"]) > 0)
                    {
                        nplCungUng.HinhThuCungUng += ";Mua VN";
                    }
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = Convert.ToInt32(row["SoToKhaiNhap"].ToString());
                    TKMD.MaLoaiHinh = (row["MaLoaiHinhNhap"].ToString());
                    TKMD.MaHaiQuan = (row["MaHaiQuanNhap"].ToString());
                    TKMD.LoadBySoToKhai(Convert.ToInt32(row["NamDangKyNhap"].ToString()));

                    nplCungUng.DonGia = Convert.ToDouble((new HangMauDich()).GetDonGiaHangTKMD(TKMD.ID, nplCungUng.MaNguyenPhuLieu));
                    nplCungUng.TriGia = nplCungUng.DonGia * Convert.ToDouble(nplCungUng.LuongCung);
                }
                else
                {
                    nplCungUng.LuongCung = Convert.ToDecimal(row["LuongCungUng"]);
                    nplCungUng.HinhThuCungUng = "Mua VN";
                }


                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.MaNguyenPhuLieu = nplCungUng.MaNguyenPhuLieu;
                dm.MaSanPham = spCungUng.MaSanPham;
                dm.HopDong_ID = HD.ID;
                if (!dm.Load())
                {
                    throw new Exception("Mã sản phẩm : " + spCungUng.MaSanPham + " không có định mức với nguyên phụ liệu : " + nplCungUng.MaNguyenPhuLieu);
                }
                else
                {
                    nplCungUng.DinhMucCungUng = Convert.ToDouble(dm.DinhMucSuDung);
                    nplCungUng.TyLeHH = Convert.ToDouble(dm.TyLeHaoHut);
                }

                if (isAdd)
                    spCungUng.NPLCungUngCollection.Add(nplCungUng);

            }


        }

        #region DungChoDongBoDuLieu

        protected int _SoToKhai;
        protected string _MaLoaiHinh;
        protected int _NamDangKy;
        protected int _isTKMD;

        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }

        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public int NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public int isTKMD
        {
            set { this._isTKMD = value; }
            get { return this._isTKMD; }
        }

        public BKCungUngDangKyCollection SelectBangKeCUByHopDong_KTX(long IDHopDong, string dbName)
        {
            SetDabaseMoi(dbName);

            string sql = "SELECT tkgbudk.*,0 as isTKMD,tktkmd.SoToKhai,tktkmd.MaLoaiHinh,YEAR(tktkmd.NgayDangKy) AS NamDK " +
            "FROM t_KDT_GC_BKCungUngDangKy tkgbudk " +
            "INNER JOIN t_KDT_ToKhaiMauDich tktkmd ON tkgbudk.TKMD_ID= tktkmd.ID " +
            "where tktkmd.IDHopDong =" + IDHopDong + " " +

            "UNION " +

            "SELECT tkgbudk.*,1 as isTKMD,tktkmd.SoToKhai,tktkmd.MaLoaiHinh,YEAR(tktkmd.NgayDangKy)AS NamDK " +
            "FROM t_KDT_GC_BKCungUngDangKy tkgbudk " +
            "INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tktkmd ON tkgbudk.TKCT_ID= tktkmd.ID " +
            "where tktkmd.IDHopDong =" + IDHopDong;

            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            IDataReader reader = db.ExecuteReader(command);
            BKCungUngDangKyCollection collection = new BKCungUngDangKyCollection();
            while (reader.Read())
            {
                BKCungUngDangKy entity = new BKCungUngDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) entity.TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) entity.SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));


                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = (int)reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDangKy = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("isTKMD"))) entity.isTKMD = reader.GetInt32(reader.GetOrdinal("isTKMD"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public BKCungUngDangKyCollection SelectBangKeCUByHopDong(long IDHopDong)
        {
            string sql = "SELECT tkgbudk.[ID] ,tkgbudk.[SoTiepNhan] ,tkgbudk.[NgayTiepNhan] ,tkgbudk.[TKMD_ID] ,tkgbudk.[TKCT_ID] ,tkgbudk.[TrangThaiXuLy] ,tkgbudk.[MaHaiQuan] ,tkgbudk.[MaDoanhNghiep] ,tkgbudk.[SoBangKe] ,tkgbudk.[GUIDSTR] ,tkgbudk.[DeXuatKhac] ,tkgbudk.[LyDoSua] ,tkgbudk.[ActionStatus] ,tkgbudk.[GuidReference] ,tkgbudk.[HUONGDAN] ,tkgbudk.[PhanLuong] ,tkgbudk.[Huongdan_PL] ,0 AS isTKMD , tktkmd.SoToKhai , tktkmd.MaLoaiHinh , YEAR(tktkmd.NgayDangKy) AS NamDK FROM t_KDT_GC_BKCungUngDangKy tkgbudk INNER JOIN t_KDT_ToKhaiMauDich tktkmd ON tkgbudk.TKMD_ID = tktkmd.ID WHERE tktkmd.IDHopDong = " + IDHopDong + " " +

                        "UNION " +

                        "SELECT tkgbudk.[ID] ,tkgbudk.[SoTiepNhan] ,tkgbudk.[NgayTiepNhan] ,tkgbudk.[TKMD_ID] ,tkgbudk.[TKCT_ID] ,tkgbudk.[TrangThaiXuLy] ,tkgbudk.[MaHaiQuan] ,tkgbudk.[MaDoanhNghiep] ,tkgbudk.[SoBangKe] ,tkgbudk.[GUIDSTR] ,tkgbudk.[DeXuatKhac] ,tkgbudk.[LyDoSua] ,tkgbudk.[ActionStatus] ,tkgbudk.[GuidReference] ,tkgbudk.[HUONGDAN] ,tkgbudk.[PhanLuong] ,tkgbudk.[Huongdan_PL], 1 AS isTKMD , tktkmd.SoToKhai , tktkmd.MaLoaiHinh , YEAR(tktkmd.NgayDangKy) AS NamDK FROM t_KDT_GC_BKCungUngDangKy tkgbudk INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tktkmd ON tkgbudk.TKCT_ID = tktkmd.ID WHERE tktkmd.IDHopDong = " + IDHopDong;
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            IDataReader reader = db.ExecuteReader(command);
            BKCungUngDangKyCollection collection = new BKCungUngDangKyCollection();
            while (reader.Read())
            {
                BKCungUngDangKy entity = new BKCungUngDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) entity.TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) entity.SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));


                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = (int)reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDangKy = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("isTKMD"))) entity.isTKMD = reader.GetInt32(reader.GetOrdinal("isTKMD"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public BKCungUngDangKyCollection SelectBangKeCUByHopDong(long IDHopDong, string dbName)
        {
            SetDabaseMoi(dbName);

            return SelectBangKeCUByHopDong(IDHopDong);
        }

        public BKCungUngDangKyCollection SelectBangKeCUByHopDong_KhongLayChuaKhaiBao(long IDHopDong, string dbName)
        {
            SetDabaseMoi(dbName);

            string sql = "SELECT tkgbudk.*,0 as isTKMD,tktkmd.SoToKhai,tktkmd.MaLoaiHinh,YEAR(tktkmd.NgayDangKy) AS NamDK " +
                        "FROM t_KDT_GC_BKCungUngDangKy tkgbudk " +
                        "INNER JOIN t_KDT_ToKhaiMauDich tktkmd ON tkgbudk.TKMD_ID= tktkmd.ID " +
                        "where tktkmd.IDHopDong =" + IDHopDong + " AND tkgbudk.TrangThaiXuLy != -1 AND tktkmd.TrangThaiXuLy != -1" +

                        "UNION " +

                        "SELECT tkgbudk.*,1 as isTKMD,tktkmd.SoToKhai,tktkmd.MaLoaiHinh,YEAR(tktkmd.NgayDangKy)AS NamDK " +
                        "FROM t_KDT_GC_BKCungUngDangKy tkgbudk " +
                        "INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tktkmd ON tkgbudk.TKCT_ID= tktkmd.ID " +
                        "where tktkmd.IDHopDong =" + IDHopDong + " AND tkgbudk.TrangThaiXuLy != -1 AND tktkmd.TrangThaiXuLy != -1";
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            IDataReader reader = db.ExecuteReader(command);
            BKCungUngDangKyCollection collection = new BKCungUngDangKyCollection();
            while (reader.Read())
            {
                BKCungUngDangKy entity = new BKCungUngDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKCT_ID"))) entity.TKCT_ID = reader.GetInt64(reader.GetOrdinal("TKCT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBangKe"))) entity.SoBangKe = reader.GetInt64(reader.GetOrdinal("SoBangKe"));


                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = (int)reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDangKy = reader.GetInt32(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("isTKMD"))) entity.isTKMD = reader.GetInt32(reader.GetOrdinal("isTKMD"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }


        #endregion DungChoDongBoDuLieu

        #region DongBoDuLieuPhongKhai
        public static void DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_GC_BKCungUngDangKy where MaDoanhNghiep=@DVGC and TrangThaiXuLy!=-1 and MaHaiQuan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.ExecuteNonQuery(dbCommandDelete);

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select HQQLHGCTEMPT.dbo.DBKNPLTCU_SPX.* from DBKNPLTCU_SPX where DVGC=@DVGC and TrangThaiXl<>2 and Ma_HQHD=@Ma_HQHD";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                BKCungUngDangKy BKCungUng = new BKCungUngDangKy();
                BKCungUng.MaDoanhNghiep = MaDoanhNghiep;
                BKCungUng.MaHaiQuan = MaHaiQuan;
                BKCungUng.NgayTiepNhan = DateTime.Today;
                BKCungUng.SoTiepNhan = Convert.ToInt64(row["So_TNDT"]);
                BKCungUng.TrangThaiXuLy = 0;
                short NamTN = Convert.ToInt16(row["Nam_TNDT"]);
                string MaLoaiHinhTKMD = row["Ma_LH"].ToString();
                ToKhaiMauDich TKMD = null;
                ToKhaiChuyenTiep TKCT = null;
                if (BKCungUng.SoTiepNhan == 50)
                {

                }
                if (MaLoaiHinhTKMD.StartsWith("XGC"))
                {
                    TKMD = ToKhaiMauDich.DongBoDuLieuKhaiDT(MaDoanhNghiep, MaHaiQuan, nameConnectKDT, Convert.ToInt64(row["SoTKDN"].ToString()), MaLoaiHinhTKMD, NamTN);
                }
                else
                {
                    TKCT = ToKhaiChuyenTiep.DongBoDuLieuKhaiDT(MaDoanhNghiep, MaHaiQuan, nameConnectKDT, Convert.ToInt64(row["SoTKDN"].ToString()), MaLoaiHinhTKMD, NamTN);
                }
                if (TKMD != null)
                {

                    BKCungUng.SanPhamCungUngCollection = new SanPhanCungUngCollection();
                    sql = "select HQQLHGCTEMPT.dbo.DHANGBKNPLTCU_SPX.* from DHANGBKNPLTCU_SPX where So_TNDT=@So_TNDT  and Ma_HQTNDT=@Ma_HQTNDT and Nam_TNDT=@Nam_TNDT";
                    SqlCommand dbCommandSP = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(dbCommandSP, "@So_TNDT", SqlDbType.Decimal, BKCungUng.SoTiepNhan);
                    db.AddInParameter(dbCommandSP, "@Ma_HQTNDT", SqlDbType.VarChar, MaHaiQuan);
                    db.AddInParameter(dbCommandSP, "@Nam_TNDT", SqlDbType.SmallInt, NamTN);

                    DataSet dsSP = db.ExecuteDataSet(dbCommandSP);
                    foreach (DataRow rowSP in dsSP.Tables[0].Rows)
                    {
                        SanPhanCungUng spCungUng = new SanPhanCungUng();
                        spCungUng.LuongCUSanPham = Convert.ToDecimal(rowSP["SoLuongSP"]);
                        spCungUng.MaSanPham = rowSP["SPP_Code"].ToString().Substring(1);
                        spCungUng.NPLCungUngCollection = new NguyenPhuLieuCungUngCollection();


                        sql = "select HQQLHGCTEMPT.dbo.DDMUCCU_SPX.* from DDMUCCU_SPX where So_TNDT=@So_TNDT  and Ma_HQTNDT=@Ma_HQTNDT and Nam_TNDT=@Nam_TNDT and SPP_Code=@SPP_Code";
                        SqlCommand dbCommandNPL = (SqlCommand)db.GetSqlStringCommand(sql);
                        db.AddInParameter(dbCommandNPL, "@So_TNDT", SqlDbType.Decimal, BKCungUng.SoTiepNhan);
                        db.AddInParameter(dbCommandNPL, "@Ma_HQTNDT", SqlDbType.VarChar, MaHaiQuan);
                        db.AddInParameter(dbCommandNPL, "@Nam_TNDT", SqlDbType.SmallInt, NamTN);
                        db.AddInParameter(dbCommandNPL, "@SPP_Code", SqlDbType.VarChar, "S" + spCungUng.MaSanPham);
                        DataSet dsNPL = db.ExecuteDataSet(dbCommandNPL);
                        foreach (DataRow rowNPL in dsNPL.Tables[0].Rows)
                        {
                            NguyenPhuLieuCungUng npl = new NguyenPhuLieuCungUng();
                            npl.DinhMucCungUng = Convert.ToDouble(rowNPL["DMGC"]);
                            npl.DonGia = Convert.ToDouble(rowNPL["DonGia"]);
                            npl.HinhThuCungUng = FontConverter.TCVN2Unicode(rowNPL["HinhThucCU"].ToString());
                            npl.LuongCung = Convert.ToDecimal(rowNPL["LuongCU"]);
                            npl.MaNguyenPhuLieu = rowNPL["NPLP_Code"].ToString().Substring(1);
                            npl.TriGia = Convert.ToDouble(rowNPL["TriGia"]);
                            npl.TyLeHH = Convert.ToDouble(rowNPL["TLHH"]);
                            spCungUng.NPLCungUngCollection.Add(npl);
                        }
                        BKCungUng.SanPhamCungUngCollection.Add(spCungUng);
                    }
                    DongBoDuLieuKhaiDT(TKMD, BKCungUng);
                }
                else if (TKCT != null)
                {

                    BKCungUng.SanPhamCungUngCollection = new SanPhanCungUngCollection();
                    sql = "select HQQLHGCTEMPT.dbo.DHANGBKNPLTCU_SPX.* from DHANGBKNPLTCU_SPX where So_TNDT=@So_TNDT  and Ma_HQTNDT=@Ma_HQTNDT and Nam_TNDT=@Nam_TNDT";
                    SqlCommand dbCommandSP = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(dbCommandSP, "@So_TNDT", SqlDbType.Decimal, BKCungUng.SoTiepNhan);
                    db.AddInParameter(dbCommandSP, "@Ma_HQTNDT", SqlDbType.VarChar, MaHaiQuan);
                    db.AddInParameter(dbCommandSP, "@Nam_TNDT", SqlDbType.SmallInt, NamTN);

                    DataSet dsSP = db.ExecuteDataSet(dbCommandSP);
                    foreach (DataRow rowSP in dsSP.Tables[0].Rows)
                    {
                        SanPhanCungUng spCungUng = new SanPhanCungUng();
                        spCungUng.LuongCUSanPham = Convert.ToDecimal(rowSP["SoLuongSP"]);
                        spCungUng.MaSanPham = rowSP["SPP_Code"].ToString().Substring(1);
                        spCungUng.NPLCungUngCollection = new NguyenPhuLieuCungUngCollection();


                        sql = "select HQQLHGCTEMPT.dbo.DDMUCCU_SPX.* from DDMUCCU_SPX where So_TNDT=@So_TNDT  and Ma_HQTNDT=@Ma_HQTNDT and Nam_TNDT=@Nam_TNDT and SPP_Code=@SPP_Code";
                        SqlCommand dbCommandNPL = (SqlCommand)db.GetSqlStringCommand(sql);
                        db.AddInParameter(dbCommandNPL, "@So_TNDT", SqlDbType.Decimal, BKCungUng.SoTiepNhan);
                        db.AddInParameter(dbCommandNPL, "@Ma_HQTNDT", SqlDbType.VarChar, MaHaiQuan);
                        db.AddInParameter(dbCommandNPL, "@Nam_TNDT", SqlDbType.SmallInt, NamTN);
                        db.AddInParameter(dbCommandNPL, "@SPP_Code", SqlDbType.VarChar, "S" + spCungUng.MaSanPham);
                        DataSet dsNPL = db.ExecuteDataSet(dbCommandNPL);
                        foreach (DataRow rowNPL in dsNPL.Tables[0].Rows)
                        {
                            NguyenPhuLieuCungUng npl = new NguyenPhuLieuCungUng();
                            npl.DinhMucCungUng = Convert.ToDouble(rowNPL["DMGC"]);
                            npl.DonGia = Convert.ToDouble(rowNPL["DonGia"]);
                            npl.HinhThuCungUng = FontConverter.TCVN2Unicode(rowNPL["HinhThucCU"].ToString());
                            npl.LuongCung = Convert.ToDecimal(rowNPL["LuongCU"]);
                            npl.MaNguyenPhuLieu = rowNPL["NPLP_Code"].ToString().Substring(1);
                            npl.TriGia = Convert.ToDouble(rowNPL["TriGia"]);
                            npl.TyLeHH = Convert.ToDouble(rowNPL["TLHH"]);
                            spCungUng.NPLCungUngCollection.Add(npl);
                        }
                        BKCungUng.SanPhamCungUngCollection.Add(spCungUng);
                    }
                    DongBoDuLieuKhaiDT(TKCT, BKCungUng);
                }

            }

        }
        public static void DongBoDuLieuKhaiDT(ToKhaiMauDich TKMD, BKCungUngDangKy BKCungUng)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection con = (SqlConnection)db.CreateConnection())
            {
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                try
                {
                    TKMD.InsertUpdateFull(tran);
                    BKCungUng.TKMD_ID = TKMD.ID;
                    BKCungUng.InsertUpdateFull(tran);
                    tran.Commit();
                }
                catch { tran.Rollback(); }
                finally { con.Close(); }
            }


        }

        public static void DongBoDuLieuKhaiDT(ToKhaiChuyenTiep TKCT, BKCungUngDangKy BKCungUng)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection con = (SqlConnection)db.CreateConnection())
            {
                con.Open();
                SqlTransaction tran = con.BeginTransaction();
                try
                {
                    TKCT.InsertUpdateFull(tran);
                    BKCungUng.TKCT_ID = TKCT.ID;
                    BKCungUng.InsertUpdateFull(tran);
                    tran.Commit();
                }
                catch { tran.Rollback(); }
                finally { con.Close(); }
            }


        }

        #endregion DongBoDuLieuPhongKhai

        #region TQDT :
        public string TQDTWSLayPhanHoi(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi(MessgaseType.DinhMucCungUng, MessgaseFunction.HoiTrangThai));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProram();
            docNPL.Load(path + "\\B03GiaCong\\LayPhanHoiDaDuyet.xml");
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            //docNPL.SelectSingleNode("Root/So_TNDKDT").InnerText = this.SoTiepNhan.ToString();
            //docNPL.SelectSingleNode("Root/Nam_TN").InnerText = this.NgayTiepNhan.Year.ToString();
            //docNPL.SelectSingleNode("Root/Ma_HQTN").InnerText = this.MaHaiQuan.ToString();

            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();
            //luu vao string
            //XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }
        public string TQDTLayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            doc.GetElementsByTagName("function")[0].InnerText = "5";
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            bool ok = false;
            if (function == MessgaseFunction.KhaiBao)
            {
                this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                this.NgayTiepNhan = DateTime.Today;
                this.TrangThaiXuLy = 0;
                ok = true;
            }
            else if (function == MessgaseFunction.HuyKhaiBao)
            {
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.TrangThaiXuLy = -1;
                ok = true;
            }
            else if (function == MessgaseFunction.HoiTrangThai)
            {
                if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/@TrangThaiXL").InnerText == "1")
                {
                    this.TrangThaiXuLy = 1;
                    this.SoBangKe = Convert.ToInt64(docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["SoTN"].Value);
                    //this.NgayDangKy = Convert.ToDateTime(docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").Attributes["NgayDK"].Value);
                    //TransgferDataToSXXK();
                    ok = true;
                    try
                    {
                        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        if (nodeMess != null)
                        {
                            return nodeMess.OuterXml;
                        }
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        if (nodeMess != null)
                        {
                            return nodeMess.OuterXml;
                        }
                    }
                    catch { }
                }
            }
            if (ok)
                this.Update();
            return "";

        }
        #endregion
    }
}
