using System.Data;
using System.Data.Common;

namespace Company.GC.BLL.DuLieuChuan
{
    public class LoaiHinhMauDich : BaseClass
    {
        public static DataTable SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_LoaiHinhMauDich ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static DataTable SelectBy_Nhom(string nhom)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiHinhMauDich WHERE LEFT([ID], 3) = '{0}' ORDER BY Ten", nhom);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public static DataTable SelectBy_NhomOne(string one)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiHinhMauDich WHERE LEFT([ID], 1) = '{0}' ORDER BY Ten", one);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public static string SelectTenVTByMa(string ma)
        {
            string query = string.Format("SELECT Ten_VT FROM t_HaiQuan_LoaiHinhMauDich WHERE ID = '{0}' ORDER BY Ten", ma);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteScalar(dbCommand).ToString();
        }

        //HungTQ, Update 30052010
        public static string GetName(string maLoaiHinh)
        {
            string query = "SELECT Ten FROM t_HaiQuan_LoaiHinhMauDich WHERE ID = '" + maLoaiHinh + "'";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteScalar(dbCommand).ToString();
        }
        //Hungtq, 14/10/2011.
        public static DataTable SelectByMa(string ma)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiHinhMauDich WHERE ID = '{0}' ORDER BY Ten", ma);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
    }
}