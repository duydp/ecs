using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class SMB : BasicVNACC
    {
        public PropertiesAttribute APN { get; set; }

        public SMB()
        {
            APN = new PropertiesAttribute(12, typeof(string));
        }
    }

    public partial class EnumGroupID { }

}
