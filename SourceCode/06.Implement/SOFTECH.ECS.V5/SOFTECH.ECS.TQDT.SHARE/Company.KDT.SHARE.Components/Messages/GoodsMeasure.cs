﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class GoodsMeasure
    {
        /// <summary>
        ///  tỉ lệ chuyển đổi
        /// </summary>
        [XmlElement("conversionRate")]
        public string ConversionRate { get; set; }
        /// <summary>
        /// Số lượng(double) n..14,3
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }
        /// <summary>
        /// Khối lượng- trọng lượng n..14,3
        /// </summary>
        [XmlElement("grossMass")]
        public string GrossMass { get; set; }
        /// <summary>
        /// Mã đơn vị tính an..3
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }
        [XmlElement("tariff")]
        public string Tariff { get; set; }

        /// <summary>
        /// Tổng trị giá của hàng trong vận đơn hàng không
        /// </summary>
        [XmlElement("customsValue")]
        public string CustomsValue { get; set; }

        /// <summary>
        /// Trọng lượng tịnh 
        /// </summary>
        [XmlElement("netMass")]
        public string NetMass { get; set; }


        /// <summary>
        /// Trọng lượng tịnh 
        /// </summary>
        [XmlElement("unitPrice")]
        public string UnitPrice { get; set; }

        #region Dành cho thanh khoản GC
        /// <summary>
        /// Luong nhap
        /// </summary>
        [XmlElement("quantityImport")]
        public string quantityImport { get; set; }

        /// <summary>
        /// Luong Xuat 
        /// </summary>
        [XmlElement("quantityExport")]
        public string quantityExport { get; set; }


        /// <summary>
        /// Luong Cung ung
        /// </summary>
        [XmlElement("quantitySupply")]
        public string quantitySupply { get; set; }

        /// <summary>
        /// Luong Du
        /// </summary>
        [XmlElement("quantityExcess")]
        public string quantityExcess { get; set; }
        #endregion



    }
}
