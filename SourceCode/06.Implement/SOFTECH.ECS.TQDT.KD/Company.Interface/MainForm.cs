﻿using System;
using System.Windows.Forms;
using Company.KD.BLL;
using Company.Interface.SXXK;
using Janus.Windows.ExplorerBar;
using Company.KD.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.LookAndFeel;
using Company.KD.BLL.KDT.SXXK;
using Company.Interface.Report;
using Company.Interface.DanhMucChuan;
using System.IO;
using System.Xml.Serialization;
using Company.QuanTri;
using Company.Interface.QuanTri;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using Company.Interface.PhongKhai;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Configuration;

namespace Company.Interface
{
    public partial class MainForm : BaseForm
    {
        public static int flag = 0;
        public static bool isLoginSuccess = false;
        WebBrowser wbManin = new WebBrowser();
        System.Windows.Forms.HtmlDocument docHTML;
        private HtmlAgilityPack.HtmlDocument htmlAgilityPackDoc = new HtmlAgilityPack.HtmlDocument();
        public static int versionHD = 0;
        public static int typeLogin = 0;
        int soLanLayTyGia = 0;
        private FrmQuerySQL frmQuery;

        //0 la ban doanh nghiep // 1 ban dai ly // 2 la ban phong khai
        #region SXXK
        private static QueueForm queueForm = new QueueForm();
        // Nguyên phụ liệu.       
        private NguyenPhuLieuRegistedForm nplRegistedForm;
        // Sản phẩm.
        //private SanPhamForm spForm;        
        private SanPhamRegistedForm spRegistedForm;

        //bao cao-thong ke
        private BaocaoThongkeTK thongkeTKForm;
        private BaocaoTriGiaXK bcTriGiaXKForm;
        private BaoCaoThongKeNuoc_Hang thongkeKNForm;

        ToKhaiMauDichManageForm tkmdManageForm;
        private ToKhaiMauDichRegisterForm tkmdRegisterForm;
        ToKhaiMauDichForm tkmdForm;

        #endregion

        //-----------------------------------------------------------------------------------------------

        #region Du lieu Chuan
        private MaHSForm maHSForm;
        private PTTTForm ptttForm;
        private PTVTForm ptvtForm;
        private DonViTinhForm dvtForm;
        private DonViHaiQuanForm dvhqForm;
        private CuaKhauForm ckForm;
        private NguyenTeForm ntForm;
        private DKGHForm dkghForm;
        private NuocForm nuocForm;
        #endregion

        #region Quản trị
        public static ECSPrincipal EcsQuanTri;
        QuanLyNguoiDung NguoiDung;
        QuanLyNhomNguoiDung NhomNguoiDung;
        #endregion Quản trị

        QuanLyMessage quanlyMess;
        private void khoitao_GiaTriMacDinh()
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            //this.Text += "  version " + Application.ProductVersion;
            if (versionHD == 0)
            {
                string statusStr1 = setText("Người dùng : " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI), "User : " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Company : {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI));
                string statusStr2 = setText(string.Format("Đơn vị hải quan : {0} - {1}. Service khai báo: {2}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN, GlobalSettings.DiaChiWS), string.Format("Customs : {0} - {1}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN));
                statusBar.Panels["DoanhNghiep"].ToolTipText = statusBar.Panels["DoanhNghiep"].Text = statusStr1;
                statusBar.Panels["HaiQuan"].ToolTipText = statusBar.Panels["HaiQuan"].Text = statusStr2;
            }
            else
            {
                //this.Text = "Hệ thống hỗ trợ khai báo từ xa loại hình Kinh Doanh - Đầu Tư version " + Application.ProductVersion;
                statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                statusBar.Panels["HaiQuan"].Text = statusBar.Panels["HaiQuan"].ToolTipText = string.Format("Đơn vị Hải quan: {0} - {1}. Service khai báo: {2}", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN, GlobalSettings.DiaChiWS);
            }
        }
        public static void LoadECSPrincipal(long id)
        {
            EcsQuanTri = new ECSPrincipal(id);
        }
        public static void ShowQueueForm()
        {
            queueForm.Show();
        }
        public static void AddToQueueForm(HangDoi hd)
        {
            queueForm.HDCollection.Add(hd);
            queueForm.RefreshQueue();
        }
        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            wbManin.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbManin_DocumentCompleted);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        void wbManin_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                docHTML = wbManin.Document;
                HtmlElement itemTyGia = null;
                foreach (HtmlElement item in docHTML.GetElementsByTagName("div"))
                {
                    if (item.GetAttribute("id") == "wp_ds26")
                    {
                        itemTyGia = item;
                        break;
                    }
                }
                if (itemTyGia == null)
                {
                    MainForm.flag = 0;
                    return;
                }
                int i = 1;
                string stUSD = "";
                try
                {
                    foreach (HtmlElement itemTD in docHTML.GetElementsByTagName("TD"))
                    {
                        if (itemTD.InnerText != null)
                        {

                        }
                        if (itemTD.InnerText != null && itemTD.InnerText.IndexOf("1 USD=") > -1)
                        {
                            stUSD = itemTD.InnerText.Trim();
                        }
                    }
                    if (stUSD != "")
                    {
                        stUSD = stUSD.Substring(6).Trim();
                        stUSD = stUSD.Substring(0, stUSD.Length - 3).Trim();
                        Company.KD.BLL.DuLieuChuan.NguyenTe.Update(null, "USD", Convert.ToDecimal(stUSD));

                    }
                }
                catch { }
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("Root");
                doc.AppendChild(root);
                foreach (HtmlElement itemTD in itemTyGia.GetElementsByTagName("TR"))
                {
                    if (i == 1)
                    {
                        i++;
                        continue;
                    }
                    HtmlElementCollection collection = itemTD.GetElementsByTagName("TD");
                    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                    itemMaNT.InnerText = collection[1].InnerText;

                    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                    itemTenNT.InnerText = collection[2].InnerText;

                    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                    itemTyNT.InnerText = collection[3].InnerText;

                    itemNT.AppendChild(itemMaNT);
                    itemNT.AppendChild(itemTenNT);
                    itemNT.AppendChild(itemTyNT);
                    root.AppendChild(itemNT);
                }
                Company.KD.BLL.DuLieuChuan.NguyenTe.UpdateTyGia(root);
                doc.Save("TyGia.xml");
                timer1.Enabled = false;
            }
            catch { MainForm.flag = 0; }
            //timer1.Enabled = false;
        }
        private void ShowLoginForm()
        {
            // this.Hide();
        }
        private void LoadDongBoDuLieuDN()
        {
            LoadDongBoDuLieuForm dongboDNForm = new LoadDongBoDuLieuForm();
            dongboDNForm.ShowDialog();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Hungtq complemented 14/12/2010
                timer1.Enabled = false; //Disable timer get Exchange Rate
                string strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                this.Text += " - Build " + strVersion;

                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Application.StartupPath + "\\Config.xml");
                    XmlNode node = doc.SelectSingleNode("Root/Type");
                    MainForm.versionHD = Convert.ToInt32(node.InnerText);
                    // Đại lý.
                    if (MainForm.versionHD == 2)
                    {
                        cmdXuatDuLieuChoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                    {
                        cmdXuatDuLieuChoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        cmdDongBoPhongKhai.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        cmdCauHinh.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        cmdRestore.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        cmdBackUp.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    if (ConfigurationManager.AppSettings["NgonNgu"] == "0")
                    {
                        this.BackgroundImage = System.Drawing.Image.FromFile("ecs-kinhdoanh.png");
                    }
                }
                catch { }
                //try
                //{
                //if (Properties.Settings.Default.NgonNgu == "1")
                //{
                //    //this.BackgroundImage = System.Drawing.Image.FromFile("BG-KDDT.jpg");
                //}
                //else
                //{
                //    //this.BackgroundImage = System.Drawing.Image.FromFile("BG-KDDT.jpg");
                //}

                this.BackgroundImageLayout = ImageLayout.Stretch;
                //}
                //catch { }
                this.Hide();
                if (versionHD == 0)
                {
                    Login login = new Login();
                    login.ShowDialog();
                }
                else if (versionHD == 1)
                {
                    Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                    login.ShowDialog();
                }
                else if (versionHD == 2)
                {
                    Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                    login.ShowDialog();
                }
                if (isLoginSuccess)
                {
                    this.Show();
                    GlobalSettings.IsDaiLy = MainForm.versionHD == 1 ? true : false;
                    //Hungtq updated 20/12/2011. Cau hinh Ecs Express
                    Express();

                    if (versionHD == 0)
                    {
                        if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                        {
                            QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                            ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                        if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
                        {
                            cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                    }
                    else if (versionHD == 2)
                    {
                        switch (typeLogin)
                        {
                            case 1: // User Da Cau Hinh
                                this.Show();
                                this.khoitao_GiaTriMacDinh();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 2: // User Chua Cau Hinh
                                this.Show();
                                DangKyForm dangKyForm = new DangKyForm();
                                dangKyForm.ShowDialog();
                                this.khoitao_GiaTriMacDinh();
                                this.LoadDongBoDuLieuDN();
                                break;
                            case 3:// Admin
                                this.Hide();
                                CreatAccountForm fAdminForm = new CreatAccountForm();
                                fAdminForm.ShowDialog();
                                this.LoginUserKhac();
                                break;
                        }
                    }

                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    vsmMain.DefaultColorScheme = GlobalSettings.GiaoDien.Id;
                    if (GlobalSettings.GiaoDien.Id == "Office2007")
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                    {
                        cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                        cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    this.khoitao_GiaTriMacDinh();



                    if (GlobalSettings.NGAYSAOLUU != null && GlobalSettings.NGAYSAOLUU != "")
                    {
                        string st = GlobalSettings.NGAYSAOLUU;
                        DateTime time = string.IsNullOrEmpty(st) == false ? Convert.ToDateTime(st) : DateTime.Now;
                        int NHAC_NHO_SAO_LUU = Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU"));
                        TimeSpan time1 = DateTime.Today.Subtract(time);
                        int ngay = NHAC_NHO_SAO_LUU - time1.Days;
                        if (ngay <= 0)
                            ShowBackupAndRestore(true);
                    }
                    #region active mới
                    try
                    {
                        this.requestActivate_new();
                        int dayInt = 7;
                        int dateTrial = KeySecurity.Active.Install.License.KeyInfo.TrialDays;
                        if (!string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Key))
                        {
                            cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            if (dayInt > dateTrial)
                                ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty Cổ phẩn SOFTECH để tiếp tục sử dụng", false);
                            this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                        }
                        else
                        {
                            this.Text += setText(" - Bản dùng thử.", " - Trial.");
                        }

                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        ShowMessageTQDT("Lỗi kiểm tra bản quyền", "Thông báo", false);
                        Application.ExitThread();
                        Application.Exit();
                    }
                    


                    #endregion

                    #region Active key cũ
                    //if (MainForm.versionHD != 2)
                    //{
                    //    //linhhtn - thêm thông báo sắp hết hạn sử dụng - 20110309
                    //    int dayInt = 7;
                    //    int dateTrial = int.Parse(Program.lic.dateTrial);
                    //    try
                    //    {
                    //        string day = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ExpiredDate"));
                    //        dayInt = int.Parse(day);
                    //    }
                    //    catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
                    //    if (Program.isActivated)
                    //    {
                    //        cmdActivate.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //        CultureInfo en = new CultureInfo("en-US");
                    //        for (int k = 0; k < dayInt; k++)
                    //        {
                    //            //Kiem tra Culture, neu la vi-VN -> chuyen doi ve cung 1 chuan en-US.
                    //            string dateString = DateTime.Now.Date.ToString("MM/dd/yyyy");

                    //            if (DateTime.Parse(dateString, en).AddDays(k) == DateTime.Parse(Program.lic.dayExpires, en).Date)
                    //            {
                    //                int ngayConLai = k + 1;
                    //                ShowMessage("Phần mềm còn " + ngayConLai + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                    //                break;
                    //            }
                    //        }
                    //        this.Text += setText(" - Đã đăng ký bản quyền.", " - License register.");
                    //    }
                    //    else
                    //    {
                    //        this.Text += setText(" - Bản dùng thử.", " - Trial.");

                    //        if (dateTrial <= 0)
                    //            this.requestActivate();
                    //        else if (dateTrial <= dayInt)
                    //            ShowMessage("Phần mềm còn " + dateTrial + " ngày nữa là hết hạn sử dụng.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng để tiếp tục sử dụng", false);
                    //    }
                    //}
                    #endregion


                    if (GlobalSettings.MA_DON_VI == "" || GlobalSettings.MA_DON_VI == "?")
                    {
                        ShowThietLapThongTinDNAndHQ();
                    }
                    AutoUpdate(string.Empty);
                    //Hungtq complemented 14/12/2010
                    timer1.Enabled = true; //Enable timer get Exchange Rate

                    backgroundWorker1.RunWorkerAsync();

                    if (!(GlobalSettings.LastBackup.Equals("")) && !(GlobalSettings.NHAC_NHO_SAO_LUU.Equals("")))
                        if (Convert.ToDateTime(GlobalSettings.LastBackup).Add(TimeSpan.Parse(GlobalSettings.NHAC_NHO_SAO_LUU)) == DateTime.Today)
                        {
                            ShowBackupAndRestore(true);
                        }
                }
                else
                    Application.Exit();
                //}
                //else
                //{
                //    ShowMessage("Chương trình đã hết hạn sử dụng. \nLiên hệ với Softech để được cung cấp phiên bản chính thức. \nSố điện thoại: (0511)3.810535 hoặc FAX: (0511)3.810278.\nXin cám ơn quí khách hàng đã sử dụng chương trình của chúng tôi.", false);
                //    Application.Exit();
                //}
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void requestActivate()
        {
            if (ShowMessage("Đã hết hạn dùng thử.\nVui lòng liên hệ với Công ty cổ phẩn Công Nghệ Phầm Mềm Đà Nẵng để tiếp tục sử dụng", true) == "Yes")
            {
                ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                obj.ShowDialog();
                requestActivate();
            }
            else
            {
                Application.Exit();
            }
        }

        private void expSXXK_ItemClick(object sender, ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "HangHoaNhap":
                    this.show_NguyenPhuLieuRegistedForm();
                    break;

                // SP đã đăng ký.
                case "HangHoaXuat":
                    this.show_SanPhamRegistedForm();
                    break;

                // Tờ khai mậu dịch kinh doanh.
                case "tkNhapKhau":
                    this.show_ToKhaiMauDichForm("NKD");
                    break;
                case "tkXuatKhau":
                    this.show_ToKhaiMauDichForm("XKD");
                    break;

                case "TheoDoiTKSXXK":
                    show_ToKhaiSXXKManage("KD");
                    break;
                case "ToKhaiSXXKDangKy":
                    show_ToKhaiSXXKDangKy("KD");
                    break;

                // Tờ khai mậu dịch đầu tư
                case "tkNhapDT":
                    this.show_ToKhaiMauDichForm("NDT");
                    break;
                case "tkXuatDT":
                    this.show_ToKhaiMauDichForm("XDT");
                    break;

                case "tkTheoDoiDT":
                    show_ToKhaiSXXKManage("DT");
                    break;
                case "tkDTDangKy":
                    show_ToKhaiSXXKDangKy("DT");
                    break;
                case "ThongKeTK":
                    show_ThongKeTK();
                    break;
                case "TriGiaXK":
                    show_TriGiaXK();
                    break;
                case "ThongKeKimNgachNuoc_MatHang":
                    show_ThongKeKimNgachNuoc_MatHang();
                    break;
            }
        }

        private void show_ThongKeKimNgachNuoc_MatHang()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongKeKimNgachNuoc_MatHang"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            thongkeKNForm = new Company.Interface.BaoCaoThongKeNuoc_Hang();
            thongkeKNForm.MdiParent = this;
            thongkeKNForm.Show();
        }
        //---------------------------------------------------------------------------------------

        private void show_TriGiaXK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TriGiaXK"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            bcTriGiaXKForm = new Company.Interface.BaocaoTriGiaXK();
            bcTriGiaXKForm.MdiParent = this;
            bcTriGiaXKForm.Show();
        }
        //---------------------------------------------------------------------------------------

        private void show_ThongKeTK()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ThongKeTK"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            thongkeTKForm = new Company.Interface.BaocaoThongkeTK();
            thongkeTKForm.MdiParent = this;
            thongkeTKForm.Show();
        }
        //---------------------------------------------------------------------------------------
        private void show_ToKhaiSXXKDangKy(string nhomLH)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new ToKhaiMauDichRegisterForm();
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.NhomLoaiHinh = nhomLH;
            //   tkmdRegisterForm.Text = "Tờ khai "+nhomLH+" đã đăng ký";
            tkmdRegisterForm.Show();
        }

        private void show_ToKhaiSXXKManage(string NhomLH)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm" + NhomLH))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            //     tkmdManageForm.Name+=NhomLH;
            tkmdManageForm.nhomLoaiHinh = NhomLH;
            tkmdManageForm.MdiParent = this;
            //  tkmdManageForm.Text = "Theo dõi tờ khai "+NhomLH;
            tkmdManageForm.Show();
        }

        //-----------------------------------------------------------------------------------------------
        private void show_NguyenPhuLieuRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenPhuLieuRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            nplRegistedForm = new NguyenPhuLieuRegistedForm();
            nplRegistedForm.MdiParent = this;
            nplRegistedForm.Show();
        }

        ////-----------------------------------------------------------------------------------------------     

        //-----------------------------------------------------------------------------------------------
        private void show_SanPhamRegistedForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SanPhamRegistedForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            spRegistedForm = new SanPhamRegistedForm();
            spRegistedForm.MdiParent = this;
            spRegistedForm.Show();
        }

        //-----------------------------------------------------------------------------------------
        private void show_ToKhaiMauDichForm(string nhomLoaiHinh)
        {
            if (MainForm.versionHD == 0)
            {
                if (!EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
                {
                    ShowMessage("Bạn không có quyền thực hiện chức năng này.", false);
                    return;
                }
            }
            tkmdForm = new ToKhaiMauDichForm();
            tkmdForm.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            //   tkmdForm.Name = nhomLoaiHinh;
            tkmdForm.NhomLoaiHinh = nhomLoaiHinh;
            tkmdForm.MdiParent = this;
            tkmdForm.Show();
        }

        //-----------------------------------------------------------------------------------------

        private void show_ToKhaiMauDichManageForm(string nhomloaihinh)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdManageForm = new ToKhaiMauDichManageForm();
            tkmdManageForm.nhomLoaiHinh = nhomloaihinh;
            tkmdManageForm.MdiParent = this;
            tkmdManageForm.Show();
        }

        private void show_ToKhaiMauDichRegisterForm(string nhomLH)
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichRegisterForm" + nhomLH))
                {
                    forms[i].Activate();
                    return;
                }
            }
            tkmdRegisterForm = new ToKhaiMauDichRegisterForm();
            tkmdRegisterForm.Name = "ToKhaiMauDichRegisterForm" + nhomLH;
            tkmdRegisterForm.NhomLoaiHinh = nhomLH;
            tkmdRegisterForm.MdiParent = this;
            tkmdRegisterForm.Show();
        }

        private void pnlMain_SelectedPanelChanged(object sender, Janus.Windows.UI.Dock.PanelActionEventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (queueForm.HDCollection.Count > 0)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(HangDoiCollection));
                FileStream fs = new FileStream("HangDoi.xml", FileMode.Create);
                serializer.Serialize(fs, queueForm.HDCollection);
            }
            queueForm.Dispose();
            Application.Exit();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdnhomcuakhau":
                    {
                        new company.Interface.DanhMucChuan.NhomCuaKhauForm().ShowDialog();
                    }
                    break;
                case "cmdThoat":
                    {
                        this.Close();
                    }
                    break;
                case "DongBoDuLieu":
                    {
                        this.DongBoDuLieuDN();
                    }
                    break;
                case "cmd2007":
                    if (cmd20071.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    GlobalSettings.Luu_GiaoDien("Office2007");
                    UpdateStyleForAllForm("Office2007");
                    //this.UpdateStyles();

                    break;
                case "cmd2003":
                    if (cmd20031.Checked == Janus.Windows.UI.InheritableBoolean.True) break;
                    GlobalSettings.KhoiTao_GiaTriMacDinh();
                    cmd20071.Checked = Janus.Windows.UI.InheritableBoolean.False;
                    cmd20031.Checked = Janus.Windows.UI.InheritableBoolean.True;
                    GlobalSettings.Luu_GiaoDien("Office2003");
                    UpdateStyleForAllForm("Office2003");
                    //this.UpdateStyles();
                    break;
                case "cmdVN":
                    if (ConfigurationManager.AppSettings["NgonNgu"] == "1")
                    {
                        if (MLMessages("Chương trình sẽ khởi động lại và chuyển giao diện sang tiếng Việt,hãy lưu lại các công việc đang làm.\nBạn muốn chuyển giao diện không?", "MSG_0203092", "", true) == "Yes")
                        {
                            ConfigurationManager.AppSettings["NgonNgu"] = "0";
                            System.Xml.XmlDocument newCfg = new System.Xml.XmlDocument();
                            newCfg.Load("SOFTECH.ECS.TQDT.KD.exe.config");
                            XmlNode newNode = newCfg.SelectSingleNode("configuration/userSettings/Company.Interface.Properties.Settings/setting[@name='NgonNgu']");
                            if (newNode != null) newNode.InnerText = ConfigurationManager.AppSettings["NgonNgu"];
                            newCfg.Save("SOFTECH.ECS.TQDT.KD.exe.config");
                            //Properties.Settings.Default.NgonNgu = "0";
                            //Properties.Settings.Default.Save();
                            CultureInfo culture = new CultureInfo("vi-VN");
                            //Thread.CurrentThread.CurrentCulture = culture;
                            //Thread.CurrentThread.CurrentUICulture = culture;
                            Application.Restart();
                        }

                    }
                    break;
                case "cmdEng":
                    if (ConfigurationManager.AppSettings["NgonNgu"] == "0")
                    {
                        ConfigurationManager.AppSettings["NgonNgu"] = "1";
                        System.Xml.XmlDocument newCfg = new System.Xml.XmlDocument();
                        newCfg.Load("SOFTECH.ECS.TQDT.KD.exe.config");
                        XmlNode newNode = newCfg.SelectSingleNode("configuration/userSettings/Company.Interface.Properties.Settings/setting[@name='NgonNgu']");
                        if (newNode != null) newNode.InnerText = ConfigurationManager.AppSettings["NgonNgu"];
                        newCfg.Save("SOFTECH.ECS.TQDT.KD.exe.config");
                        //Properties.Settings.Default.Save();
                        CultureInfo culture = new CultureInfo("en-US");
                        this.InitCulture("en-US", "Company.Interface.LanguageResource.Language");
                        Form[] forms = this.MdiChildren;
                        for (int i = 0; i < forms.Length; i++)
                        {
                            ((BaseForm)forms[i]).InitCulture("en-US", "Company.Interface.LanguageResource.Language");
                        }

                        if (ConfigurationManager.AppSettings["NgonNgu"] == "0")
                        {
                            this.BackgroundImage = System.Drawing.Image.FromFile("ecs-kinhdoanh.png");
                        }
                        this.BackgroundImageLayout = ImageLayout.Stretch;

                    }
                    break;
                case "cmdHelp":
                    try
                    {
                        System.Diagnostics.Process.Start(Application.StartupPath + "\\Helps\\Huong dan su dung phan mem ECS_TQDT_KD.pdf");
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(setText("Hiện tại chưa có file help!", "There is not a help file!"), false);
                    }
                    break;
                case "cmdMaHS":
                    ShowMaHSForm();
                    break;
                case "cmdHaiQuan":
                    ShowDVHQForm();
                    break;
                case "cmdNuoc":
                    ShowNuocForm();
                    break;
                case "cmdNguyenTe":
                    ShowNguyenTeForm();
                    break;
                case "cmdPTTT":
                    ShowPTTTForm();
                    break;
                case "cmdPTVT":
                    ShowPTVTForm();
                    break;
                case "cmdDKGH":
                    ShowDKGHForm();
                    break;
                case "cmdDVT":
                    ShowDVTForm();
                    break;
                case "cmdCuaKhau":
                    ShowCuaKhauForm();
                    break;
                case "cmdRestore":
                    ShowRestoreForm();
                    break;
                case "cmdBackUp":
                    ShowBackupAndRestore(true);
                    break;
                case "ThongSoKetNoi":
                    ShowThietLapThongSoKetNoi();
                    break;
                case "TLThongTinDNHQ":
                    ShowThietLapThongTinDNAndHQ();
                    break;
                case "cmdThietLapIn":
                    ShowThietLapInBaoCao();
                    break;
                case "cmdCauHinhToKhai":
                    ShowCauHinhToKhai();
                    break;
                case "QuanLyNguoiDung":
                    ShowQuanLyNguoiDung();
                    break;
                case "QuanLyNhom":
                    ShowQuanLyNhomNguoiDung();
                    break;
                case "LoginUser":
                    LoginUserKhac();
                    break;
                case "cmdChangePass":
                    ChangePassForm();
                    break;
                case "MaHS":
                    MaHSForm();
                    break;
                case "DonViDoiTac":
                    DonViDoiTacForm();
                    break;
                case "cmdAutoUpdate":
                    AutoUpdate("MSG");
                    break;
                case "cmdXuatToKhaiDauTu":
                    XuatToKhaiChoPhongKhai();
                    break;
                case "cmdXuatToKhaiKinhDoanh":
                    XuatToKhaiChoPhongKhai();
                    break;
                case "cmdAbout":
                    this.dispInfo();
                    break;
                case "cmdActivate":
                    this.dispActivate();
                    break;
                case "cmdCloseMe":
                    this.doCloseMe();
                    break;
                case "cmdCloseAllButMe":
                    this.doCloseAllButMe();
                    break;
                case "cmdCloseAll":
                    this.doCloseAll();
                    break;
                case "cmdNhapToKhaiDauTu":
                    NhapToKhaiTuDoanhNghiep();
                    break;

                case "cmdNhapToKhaiKinhDoanh":
                    NhapToKhaiTuDoanhNghiep();
                    break;
                case "QuanLyMess":
                    WSForm2 wsform2 = new WSForm2();
                    wsform2.ShowDialog();
                    if (WSForm2.IsSuccess == true)
                    {
                        ShowQuanLyMess();
                    }
                    break;
                case "cmdThietLapCHDN":
                    ShowThietLapform();
                    break;
                case "cmdQuery":
                    WSForm2 login = new WSForm2();
                    login.ShowDialog();
                    if (WSForm2.IsSuccess == true)
                    {
                        //string fileName = Application.StartupPath + "\\MiniSQL\\MiniSQLQuery.exe";
                        //if (System.IO.File.Exists(fileName))
                        //{
                        //    MiniSqlQuery.Core.DbConnectionDefinition conn = new MiniSqlQuery.Core.DbConnectionDefinition();
                        //    conn.ConnectionString = "Data Source=" + GlobalSettings.SERVER_NAME + ";Initial Catalog=" + GlobalSettings.DATABASE_NAME +
                        //                            ";User ID=" + GlobalSettings.USER + ";Password=" + GlobalSettings.PASS;
                        //    conn.Name = GlobalSettings.DATABASE_NAME;
                        //    System.Diagnostics.Process.Start(fileName);
                        //}

                        QuerySQL();
                    }
                    break;
                case "cmdLog":
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\Error.log");
                    break;
            }
        }

        private void QuerySQL()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("FrmQuerySQL"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            frmQuery = new FrmQuerySQL();
            frmQuery.MdiParent = this;
            frmQuery.Show();
        }

        private void ShowThietLapform()
        {
            //Classes.clsBien.boolShow = true;
            DangKyForm dangkyForm = new DangKyForm();
            dangkyForm.ShowDialog();

        }
        private void ShowQuanLyMess()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyMessage"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            quanlyMess = new QuanLyMessage();
            quanlyMess.MdiParent = this;
            quanlyMess.Show();
        }
        private void NhapToKhaiTuDoanhNghiep()
        {
            ToKhaiMauDichCollection TKMDCollection = new ToKhaiMauDichCollection();

            XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    TKMDCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
                    fs.Close();

                    if (TKMDCollection.Count != 0)
                    {
                        int i = 0;
                        foreach (ToKhaiMauDich TKMD in TKMDCollection)
                        {
                            try
                            {
                                TKMD.InsertFull();
                                i++;
                            }
                            catch { }
                        }
                        if (i > 0)
                            ShowMessage("Nhập thành công " + i + " tờ khai.", false);
                    }
                    else
                        ShowMessage("Không có dữ liệu để nhập.", false);

                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
        }
        private void dispActivate()
        {
            ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
            obj.Show();
        }

        private void dispInfo()
        {
            ProdInfo.frmInfo obj = new Company.Interface.ProdInfo.frmInfo();
            obj.ShowDialog();
        }

        private void XuatToKhaiChoPhongKhai()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ToKhaiMauDichManageForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ToKhaiMauDichManageForm f = new ToKhaiMauDichManageForm();

            f.MdiParent = this;
            f.Show();
        }
        //private void AutoUpdate()
        //{
        //    if (ShowMessage(setText("Bạn có muốn cập nhật chương trình mới nhất không?", "Do you want to update new licence"), true) == "Yes")
        //    {
        //        System.Diagnostics.Process.Start(Application.StartupPath + "\\AutoUpdate\\ECS_AutoUpdate.exe");
        //        Application.Exit();
        //    }

        //}
        private void DonViDoiTacForm()
        {
            DonViDoiTacForm f = new DonViDoiTacForm();
            f.ShowDialog();
        }
        private void MaHSForm()
        {
            HaiQuan.HS.HSMainForm f = new HaiQuan.HS.HSMainForm();
            f.Show();
        }
        private void ChangePassForm()
        {
            ChangePassForm f = new ChangePassForm();
            f.ShowDialog();
        }
        private void LoginUserKhac()
        {
            this.Hide();
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
            MainForm.EcsQuanTri = null;
            isLoginSuccess = false;
            if (versionHD == 0)
            {
                Login login = new Login();
                login.ShowDialog();
            }
            else if (versionHD == 1)
            {
                Company.Interface.DaiLy.Login login = new Company.Interface.DaiLy.Login();
                login.ShowDialog();
            }
            else if (versionHD == 2)
            {
                Company.Interface.PhongKhai.LoginForm login = new Company.Interface.PhongKhai.LoginForm();
                login.ShowDialog();
            }
            if (isLoginSuccess)
            {
                //statusBar.Panels["DoanhNghiep"].Text = statusBar.Panels["DoanhNghiep"].ToolTipText = "Người dùng: " + ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME + " - " + string.Format("Doanh nghiệp: {0} - {1}", GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);
                this.Show();
                if (versionHD == 0)
                {
                    if (MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.Management)))
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.True;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                    else
                    {
                        QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                        ThongSoKetNoi.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        TLThongTinDNHQ.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
                    {
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    }
                    else
                        cmdCauHinhToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                else if (versionHD == 1)
                {
                    QuanTri.Visible = Janus.Windows.UI.InheritableBoolean.False;
                }
                else if (versionHD == 2)
                {
                    switch (typeLogin)
                    {
                        case 1: // User Da Cau Hinh
                            this.Show();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 2: // User Chua Cau Hinh
                            this.Show();
                            DangKyForm dangKyForm = new DangKyForm();
                            dangKyForm.ShowDialog();
                            this.khoitao_GiaTriMacDinh();
                            this.LoadDongBoDuLieuDN();
                            break;
                        case 3:// Admin
                            this.Hide();
                            CreatAccountForm fAdminForm = new CreatAccountForm();
                            fAdminForm.ShowDialog();
                            this.ShowLoginForm();
                            break;
                    }
                }
                khoitao_GiaTriMacDinh();
            }
            else
                Application.Exit();
        }
        private void ShowQuanLyNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            NguoiDung = new QuanLyNguoiDung();
            NguoiDung.MdiParent = this;
            NguoiDung.Show();
        }
        private void ShowQuanLyNhomNguoiDung()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("QuanLyNhomNguoiDung"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            NhomNguoiDung = new QuanLyNhomNguoiDung();
            NhomNguoiDung.MdiParent = this;
            NhomNguoiDung.Show();
        }
        private void ShowCauHinhToKhai()
        {
            CauHinhToKhaiForm f = new CauHinhToKhaiForm();
            f.ShowDialog();
        }

        private void ShowThietLapInBaoCao()
        {
            CauHinhInForm f = new CauHinhInForm();
            f.ShowDialog();
        }
        private void ShowRestoreForm()
        {
            //RestoreForm f = new RestoreForm();
            //f.ShowDialog();
        }
        private void ShowThietLapThongTinDNAndHQ()
        {

            CultureInfo cultureCurrent = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            CultureInfo cultureVN = new CultureInfo("vi-VN");
            ThongTinDNAndHQForm f = new ThongTinDNAndHQForm();
            f.ShowDialog();
            khoitao_GiaTriMacDinh();
            if (!cultureCurrent.Equals(Thread.CurrentThread.CurrentCulture) && Thread.CurrentThread.CurrentCulture.Equals(cultureVN))
            {
                Application.Restart();
            }
            else if (!cultureCurrent.Equals(Thread.CurrentThread.CurrentCulture))
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    ((BaseForm)forms[i]).InitCulture(Thread.CurrentThread.CurrentCulture.Name, "Company.Interface.LanguageResource.ResourceEN");
                }
                this.InitCulture(Thread.CurrentThread.CurrentCulture.Name, "Company.Interface.LanguageResource.ResourceEN");
            }
        }
        private void ShowThietLapThongSoKetNoi()
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();

        }
        private void ShowBackupAndRestore(bool isBackUp)
        {
            BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            f.isBackUp = isBackUp;
            f.ShowDialog();
        }
        private void ShowCuaKhauForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CuaKhauForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ckForm = new CuaKhauForm();
            ckForm.MdiParent = this;
            ckForm.Show();
        }

        private void ShowDVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DonViTinhForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dvtForm = new DonViTinhForm();
            dvtForm.MdiParent = this;
            dvtForm.Show();
        }

        private void ShowDKGHForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DKGHForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dkghForm = new DKGHForm();
            dkghForm.MdiParent = this;
            dkghForm.Show();
        }
        private void ShowPTVTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTVTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptvtForm = new PTVTForm();
            ptvtForm.MdiParent = this;
            ptvtForm.Show();
        }
        private void ShowPTTTForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PTTTForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ptttForm = new PTTTForm();
            ptttForm.MdiParent = this;
            ptttForm.Show();
        }

        private void ShowNguyenTeForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NguyenTeForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            ntForm = new NguyenTeForm();
            ntForm.MdiParent = this;
            ntForm.Show();
        }

        private void ShowNuocForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("NuocForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            nuocForm = new NuocForm();
            nuocForm.MdiParent = this;
            nuocForm.Show();
        }

        private void ShowDVHQForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("DonViHaiQuanForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            dvhqForm = new DonViHaiQuanForm();
            dvhqForm.MdiParent = this;
            dvhqForm.Show();
        }

        private void ShowMaHSForm()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("MaHSForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            maHSForm = new MaHSForm();
            maHSForm.MdiParent = this;
            maHSForm.Show();
        }

        private void DongBoDuLieuDN()
        {
            //BackUpAndReStoreForm f = new BackUpAndReStoreForm();
            //f.ShowDialog();
        }
        private void UpdateStyleForAllForm(string style)
        {
            this.vsmMain.DefaultColorScheme = style;
            queueForm.vsmMain.DefaultColorScheme = style;

            if (tkmdForm != null)
                tkmdForm.vsmMain.DefaultColorScheme = style;
            if (tkmdManageForm != null)
                tkmdManageForm.vsmMain.DefaultColorScheme = style;
            if (tkmdRegisterForm != null)
                tkmdRegisterForm.vsmMain.DefaultColorScheme = style;
            if (maHSForm != null)
                maHSForm.vsmMain.DefaultColorScheme = style;
            if (ptttForm != null)
                ptttForm.vsmMain.DefaultColorScheme = style;
            if (ptvtForm != null)
                ptvtForm.vsmMain.DefaultColorScheme = style;
            if (dvtForm != null)
                dvtForm.vsmMain.DefaultColorScheme = style;
            if (dvhqForm != null)
                dvhqForm.vsmMain.DefaultColorScheme = style;
            if (ckForm != null)
                ckForm.vsmMain.DefaultColorScheme = style;
            if (ntForm != null)
                ntForm.vsmMain.DefaultColorScheme = style;
            if (dkghForm != null)
                dkghForm.vsmMain.DefaultColorScheme = style;
            if (nuocForm != null)
                nuocForm.vsmMain.DefaultColorScheme = style;

        }
        private void cmbMenu_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void pmMain_MdiTabMouseDown(object sender, Janus.Windows.UI.Dock.MdiTabMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                e.Tab.Form.Activate();
                if (this.MdiChildren.Length == 1)
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdCloseAllButMe.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                mnuRightClick.Show(this, e.X + pmMain.MdiTabGroups[0].Location.X, e.Y + 6);
            }
        }

        private void doCloseMe()
        {
            Form form = pmMain.MdiTabGroups[0].SelectedTab.Form;
            form.Close();
        }

        private void doCloseAllButMe()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i] != pmMain.MdiTabGroups[0].SelectedTab.Form)
                {
                    forms[i].Close();
                }
            }
        }

        private void doCloseAll()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
        }

        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (MainForm.flag == 0 && soLanLayTyGia <= 3)
            {
                soLanLayTyGia++;

                if (!backgroundWorker1.IsBusy)
                    backgroundWorker1.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Lay ty gia ngoai te Online
        /// </summary>
        private bool GetTyGia()
        {
            try
            {
                //Update by Hungtq
                string path = "http://www.sbv.gov.vn/wps/portal/!ut/p/c5/pVLLboMwEPwkFtsYc3RsMGCCgyGP5hIhtYoSNQ9VUdTm6-skVaXSBg71yqfZmZ0drbf0XO3b82bdnjaHffvqLbwlXYEkUZgEBBivMWRG6AiURiRG3txbAFnVW3Ycf5wWxUWcm-2lgrJp30s5d79C47KYPM_sdMT5di3xm9N8oqsEcYqSFIHJkYSMcMsRNbgm4Y-JJhKBm8iEAJtgFQV3tlA8JWEBoPJKQKbympGRDwDBgN8b-wFuZvgfbGKCwayW144-7zeFB4_DF_4gGz_o6JtURpCxXBeNP8EJwne8L_k-f4x2-KxhseOPwkqzsQ-TXzgBlwCJo8qoWW5D2u8vQ539VIJCt5-Nq5pLhKZBRx-AXP1TkFwnSFkysL_fi-cK-ufHA_551_8f19V3Ie5-yvSwe_GOu7PWuqA2_i72CXkZa0g!/dl3/d3/L0lDU0lKSmdwcGlRb0tVUWtnQSEhL29Pb2dBRUlRaGpFQ1VJZ0FBQUl5RkFNaHdVaFM0TFVFQVVvIS80QzFiOVdfTnIwZ0NVZ3hFbVJDVXdpSkg0QSEhLzdfRjJBNjJGSDIwME5SMjBJNDNOM1BTNzAwVTcvNFd0ME40NTcwMDIzMC8xNTM0OTU1NzkyMjUvYmZfYWN0aW9uL21haW4!";
                htmlAgilityPackDoc = new HtmlAgilityPack.HtmlWeb().Load(path);

                List<Company.KDT.SHARE.Components.NguyenTe> listNgoaiTe = Company.KDT.SHARE.Components.Globals.GetUSDExchangeRateNew(htmlAgilityPackDoc);

                if (listNgoaiTe.Count == 0)
                {
                    return false;
                }

                //Luu file XML
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("Root");
                doc.AppendChild(root);
                foreach (Company.KDT.SHARE.Components.NguyenTe item in listNgoaiTe)
                {
                    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                    itemMaNT.InnerText = item.Ten;

                    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                    itemTenNT.InnerText = item.TenDayDu;

                    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                    itemTyNT.InnerText = item.TyGiaTinhThue.ToString();

                    itemNT.AppendChild(itemMaNT);
                    itemNT.AppendChild(itemTenNT);
                    itemNT.AppendChild(itemTyNT);
                    root.AppendChild(itemNT);
                }
                doc.Save("TyGia.xml");

                //Update Data
                Company.KDT.SHARE.Components.NguyenTe.InsertUpdateCollection(listNgoaiTe);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

            return true;
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                this.cmdDataVersion1.Text = string.Format("Dữ liệu phiên bản: {0}", (dataVersion != "" ? dataVersion : "?"));

                long sotk = ToKhaiMauDich.SelectCountSoTK(GlobalSettings.MA_DON_VI);
                if ((sotk % 2 == 0 && sotk > 0) || (sotk % 10 == 0 && sotk >= 10))
                {
                    string loaihinh = "ECSKD";
                    if (versionHD == 1)
                        loaihinh = "ECSDLKD";
                    if (versionHD != 2)
                        WebServiceConnection.sendThongTinDN(GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, "SoTK=" + sotk, loaihinh);
                }
                string error = KeySecurity.Active.Install.ECS_updateInfo(UpdateInfo(sotk.ToString()));
                if (!string.IsNullOrEmpty(error))
                    Logger.LocalLogger.Instance().WriteMessage("Lỗi cập nhật thông tin doanh nghiệp: " + error, null);
                if (!e.Cancel)
                {
                    //Hungtq complemented 14/12/2010
                    if (Company.KDT.SHARE.Components.Globals.GetTyGia() == true)
                    {
                        timer1.Enabled = false;
                        MainForm.flag = 1;
                    }
                    else
                    {
                        MainForm.flag = 0;
                        soLanLayTyGia = 0;
                    }
                }

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #region AutoUpdate ONLINE

        private void AutoUpdate(string args)
        {
            try
            {
                Company.KDT.SHARE.Components.DownloadUpdate dl = new DownloadUpdate(args);
                dl.DoDownload();
            }
            catch { }

        }

        #region TẠO COMMAND TOOLBAR

        private delegate void CreateCommandCallback(string lastestVersion);
        private void CreateCommand(string lastestVersion)
        {
            try
            {
                this.cmbMenu.CommandManager = this.cmMain;
                this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                    this.cmdNewVersion
                });

                if (!this.cmMain.Commands.Contains("cmdNewVersion"))
                    this.cmMain.Commands.Add(this.cmdNewVersion);

                // 
                // cmdNewVersion
                // 
                this.cmdNewVersion.Key = "cmdNewVersion";
                this.cmdNewVersion.Name = "cmdNewVersion";
                this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;
                this.cmdNewVersion.TextAlignment = Janus.Windows.UI.CommandBars.ContentAlignment.MiddleCenter;
                this.cmdNewVersion.TextImageRelation = Janus.Windows.UI.CommandBars.TextImageRelation.ImageBeforeText;
                this.cmdNewVersion.ImageReplaceableColor = System.Drawing.Color.Pink;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdNewVersion;

        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung(string lastestVersion)
        {
            #region Initialize
            //Tao nut command tren toolbar.
            this.cmdNewVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdNewVersion");

            //this.cmbMenu.CommandManager = this.cmMain;
            //this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            //    this.cmdNewVersion
            //});

            //if (!this.cmMain.Commands.Contains("cmdNewVersion"))
            //    this.cmMain.Commands.Add(this.cmdNewVersion);

            //// 
            //// cmdNewVersion
            //// 
            //this.cmdNewVersion.Key = "cmdNewVersion";
            //this.cmdNewVersion.Name = "cmdNewVersion";
            //this.cmdNewVersion.Text = "*** Phiên bản mới: " + lastestVersion;

            // Invoke the method that updates the form's label
            this.Invoke(new CreateCommandCallback(this.CreateCommand), new object[] { lastestVersion });

            #endregion

            cmdNewVersion.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdNewVersion_Click);

        }

        private void cmdNewVersion_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

            switch (e.Command.Key)
            {
                case "cmdNewVersion":
                    AutoUpdate("MSG");
                    break;
            }

        }

        #endregion
        #endregion

        #region Begin ECS Express

        private void Express()
        {
            try
            {
                string val = new Company.KDT.SHARE.Components.Utils.License().DecryptString(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Ep"));

                if (!string.IsNullOrEmpty(val) && bool.Parse(val) == true)
                {
                    SetExpress(false);
                }
                else
                    SetExpress(true);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void SetExpress(bool visible)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Janus.Windows.UI.InheritableBoolean status = visible == true ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

                /*Menu He thong*/
                cmdThietLapCHDN.Visible = status;           //Thiet lap thong tin doanh nghiep
                cmdXuatDuLieuChoPhongKhai.Visible = status; //Xuat du lieu cho phong khai
                cmdDongBoPhongKhai.Visible = status;                 //Nhap du lieu doanh nghiep
                cmdRestore.Visible = status;                //Phuc hoi du lieu

                /*Menu Giao dien*/
                cmdEng.Visible = status;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        #endregion End ECS Express


        private void requestActivate_new()
        {

            string sfmtMsg = "{0}\nVui lòng liên hệ với Công ty cổ phẩn SOFTECH để tiếp tục sử dụng";
            bool isShowActive = false;

            if (KeySecurity.Active.Install.License.KeyInfo == null)
            {
                sfmtMsg = string.Format(sfmtMsg, "Bạn chưa đăng ký sử dụng phần mềm.");
                isShowActive = true;
            }
            else if (KeySecurity.Active.Install.License.KeyInfo.TrialDays == 0)
            {
                sfmtMsg = string.Format(sfmtMsg, "Đã hết hạn dùng phần mềm");
                isShowActive = true;
            }

            if (isShowActive)
            {
                if (ShowMessage(sfmtMsg, true) == "Yes")
                {
                    ProdInfo.frmRegister obj = new Company.Interface.ProdInfo.frmRegister();
                    if (obj.ShowDialog(this) == DialogResult.OK)
                    {
                        ShowMessage("Kích hoạt thành công. Chương trình sẽ tự khởi động lại", false);
                        Application.ExitThread();
                        Application.Restart();
                    }
                    else
                    {
                        ShowMessage("Kích hoạt không thành công, chương trình sẽ tự đóng", false);
                        Application.ExitThread();
                        Application.Exit();
                    }

                }
                else
                {
                    Application.ExitThread();
                    Application.Exit();
                }
            }
            if (KeySecurity.Active.Install.License.KeyInfo.IsShow
                && !string.IsNullOrEmpty(KeySecurity.Active.Install.License.KeyInfo.Notice))
            {
                InfoOnline inf = new InfoOnline();
                inf.SetRTF(KeySecurity.Active.Install.License.KeyInfo.Notice);
                inf.ShowDialog();
            }
        }
        #region Cập nhật thông tin

        private string UpdateInfo(string soTK)
        {
            string ListDoanhNghiep = GlobalSettings.NguoiLienHe;
            Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer info = new Company.KDT.SHARE.Components.Licenses.ECS_InforCustomer();
            if (GlobalSettings.IsDaiLy)
                ListDoanhNghiep = Company.KD.BLL.KDT.HeThongPhongKhai.GetDoanhNghiep();
            #region Tạo thông tin update
            info.Id = GlobalSettings.MA_DON_VI;
            info.Name = GlobalSettings.TEN_DON_VI;
            info.Address = GlobalSettings.DIA_CHI;
            info.Phone = GlobalSettings.SoDienThoaiDN;
            info.Contact_Person = GlobalSettings.NguoiLienHe;
            info.Email = GlobalSettings.MailDoanhNghiep;
            info.Id_Customs = GlobalSettings.MA_HAI_QUAN;
            info.IP_Customs = GlobalSettings.DiaChiWS;
            info.ServerName = GlobalSettings.SERVER_NAME;
            info.Data_Name = GlobalSettings.DATABASE_NAME;
            info.Data_User = GlobalSettings.USER;
            info.Data_Pass = GlobalSettings.PASS;
            info.Data_Version = Company.KDT.SHARE.Components.Version.GetVersion();
            info.App_Version = Application.ProductVersion;
            info.LastCheck = DateTime.Now;
            info.temp1 = ListDoanhNghiep;    
            info.MachineCode = KeySecurity.KeyCode.ProcessInfo();
            info.Product_ID = "ECS_TQDT_KD_V2";
            info.RecordCount = soTK;
            #endregion
            return Helpers.Serializer(info);
        }
        #endregion
    }
}
