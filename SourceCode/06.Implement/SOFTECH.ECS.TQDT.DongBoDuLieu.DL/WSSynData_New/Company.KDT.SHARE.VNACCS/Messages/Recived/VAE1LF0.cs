﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAE1LF0 : BasicVNACC
    {
        public List<VAE1LF0_HANG> HangMD { get; set; }
        public void LoadVAE1LF0(string strResult)
        {
            try
            {
                this.GetObject<VAE1LF0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAE1LF0, false, VAE1LF0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAE1LF0.TongSoByte);
                while (true)
                {
                    VAE1LF0_HANG vad1agHang = new VAE1LF0_HANG();
                    vad1agHang.GetObject<VAE1LF0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAE1LF0_HANG", false, VAE1LF0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAE1LF0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAE1LF0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAE1LF0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAE1LF0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}
