using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_ChungTuThanhToan
    {
        List<KDT_VNACC_ChungTuThanhToan_ChiTiet> _listChungTuChiTiet = new List<KDT_VNACC_ChungTuThanhToan_ChiTiet>();

        public List<KDT_VNACC_ChungTuThanhToan_ChiTiet> ChungTuChiTietCollection
        {
            set { this._listChungTuChiTiet = value; }
            get { return this._listChungTuChiTiet; }
        }

        public void LoadChungTuThanhToan_ChiTiet()
        {
            _listChungTuChiTiet = KDT_VNACC_ChungTuThanhToan_ChiTiet.SelectCollectionBy_ChungTuThanhToan_ID(this.ID);
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert();
                    else
                        this.Update();


                    foreach (KDT_VNACC_ChungTuThanhToan_ChiTiet item in this.ChungTuChiTietCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.ChungTuThanhToan_ID = this.ID;
                            item.ID = item.Insert();
                        }
                        else
                        {
                            item.Update();
                        }
                    }
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public static DataSet SelectAllManagement(List<String> List )
        {
            string spName = @"SELECT [ID]
                                  ,[LoaiChungTu]
                                  ,[MaNganHangCungCapBaoLanh] as MaNganHang
                                  ,[TenNganHangCungCapBaoLanh] AS TenNganHang
                                  ,[NamPhatHanh]
                                  ,[KiHieuChungTuPhatHanhBaoLanh] AS KiHieuChungTu
                                  ,[SoChungTuPhatHanhBaoLanh] AS SoChungTu
                                  ,[MaTienTe]
	                              ,NgayKhaiBao
                                  ,[TrangThaiXuLy]
                              FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan]
                              WHERE " + List[0].ToString() + ""+

                          " UNION "+
                              
                           " SELECT [ID] "+
                            "      ,[LoaiChungTu]"+       
                             "     ,[MaNganHangTraThay] AS MaNganHang"+
                              "    ,[TenNganHangTraThay] AS TenNganHang"+
                               "   ,[NamPhatHanh]"+
                                "  ,[KiHieuChungTuPhatHanhHanMuc] AS KiHieuChungTu"+
                                 " ,[SoHieuPhatHanhHanMuc] AS SoChungTu"+
                                  ",[MaTienTe]"+
	                               ",NgayKhaiBao"+
                                    ",[TrangThaiXuLy]"+
                              "FROM [dbo].[t_KDT_VNACC_ChungTuThanhToan]  "+
                              "WHERE " + List[1].ToString() + "";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            return db.ExecuteDataSet(dbCommand);
        }
    }
}