using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAE4000_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CMD { get; set; }
public PropertiesAttribute COC { get; set; }
public PropertiesAttribute TXA { get; set; }
public PropertiesAttribute TXB { get; set; }
public PropertiesAttribute TXC { get; set; }
public PropertiesAttribute TXD { get; set; }
public PropertiesAttribute CMN { get; set; }
public PropertiesAttribute RE { get; set; }
public PropertiesAttribute REG { get; set; }
public PropertiesAttribute QN1 { get; set; }
public PropertiesAttribute QT1 { get; set; }
public PropertiesAttribute QN2 { get; set; }
public PropertiesAttribute QT2 { get; set; }
public PropertiesAttribute BPR { get; set; }
public PropertiesAttribute BPC { get; set; }
public PropertiesAttribute DPR { get; set; }
public PropertiesAttribute UPR { get; set; }
public PropertiesAttribute UPC { get; set; }
public PropertiesAttribute TSC { get; set; }
public PropertiesAttribute TDL { get; set; }
public PropertiesAttribute TXN { get; set; }
public PropertiesAttribute TXR { get; set; }
public GroupAttribute OL_ { get; set; }

public VAE4000_HANG()
        {
CMD = new PropertiesAttribute(12, typeof(string));
COC = new PropertiesAttribute(7, typeof(string));
TXA = new PropertiesAttribute(10, typeof(int));
TXB = new PropertiesAttribute(10, typeof(int));
TXC = new PropertiesAttribute(4, typeof(string));
TXD = new PropertiesAttribute(3, typeof(string));
CMN = new PropertiesAttribute(600, typeof(string));
RE = new PropertiesAttribute(5, typeof(string));
REG = new PropertiesAttribute(16, typeof(int));
QN1 = new PropertiesAttribute(12, typeof(int));
QT1 = new PropertiesAttribute(4, typeof(string));
QN2 = new PropertiesAttribute(12, typeof(int));
QT2 = new PropertiesAttribute(4, typeof(string));
BPR = new PropertiesAttribute(20, typeof(int));
BPC = new PropertiesAttribute(3, typeof(string));
DPR = new PropertiesAttribute(20, typeof(int));
UPR = new PropertiesAttribute(9, typeof(int));
UPC = new PropertiesAttribute(3, typeof(string));
TSC = new PropertiesAttribute(4, typeof(string));
TDL = new PropertiesAttribute(2, typeof(int));
TXN = new PropertiesAttribute(12, typeof(int));
TXR = new PropertiesAttribute(3, typeof(int));
#region OL_
List<PropertiesAttribute> listOL_ = new List<PropertiesAttribute>();
listOL_.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAE4000_HANG_OL_, typeof(string)));
OL_ = new GroupAttribute("OL_", 5, listOL_);
#endregion OL_
TongSoByte = 839;
}

}public partial class EnumGroupID
    {
public static readonly string VAE4000_HANG_OL_ = "VAE4000_HANG_OL_";
}

}
