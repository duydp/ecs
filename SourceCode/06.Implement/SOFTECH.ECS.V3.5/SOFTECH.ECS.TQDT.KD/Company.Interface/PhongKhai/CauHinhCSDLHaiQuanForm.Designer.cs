﻿namespace Company.Interface.PhongKhai
{
    partial class CauHinhCSDLHaiQuanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHinhCSDLHaiQuanForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grpKhaiDT = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton5 = new Janus.Windows.EditControls.UIButton();
            this.label18 = new System.Windows.Forms.Label();
            this.txtServerKhaiDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPassKhaiDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtUserKhaiDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDatabaseKhaiDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.grpSXXK = new Janus.Windows.EditControls.UIGroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.uiButton4 = new Janus.Windows.EditControls.UIButton();
            this.txtServerSXXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPassSXXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtUserSXXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDatabaseSXXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.grpECS_SXXK = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtServerECS_SXXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPassECS_SXXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtUserECS_SXXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDatabaseECS_SXXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.rfvServerECS_SXXK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDatabaseECS_SXXK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvUserECS_SXXK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.containerValidator1 = new Company.Controls.CustomValidation.ContainerValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvServerSXXK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDatabaseSXXK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvUserSXXk = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvServerKhaiDT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDatabaseKhaiDT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvUserKhaiDT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpKhaiDT)).BeginInit();
            this.grpKhaiDT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpSXXK)).BeginInit();
            this.grpSXXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpECS_SXXK)).BeginInit();
            this.grpECS_SXXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvServerECS_SXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDatabaseECS_SXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvUserECS_SXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvServerSXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDatabaseSXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvUserSXXk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvServerKhaiDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDatabaseKhaiDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvUserKhaiDT)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(648, 309);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.grpKhaiDT);
            this.uiGroupBox1.Controls.Add(this.grpSXXK);
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Controls.Add(this.grpECS_SXXK);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(648, 309);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // grpKhaiDT
            // 
            this.grpKhaiDT.BackColor = System.Drawing.Color.Transparent;
            this.grpKhaiDT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.grpKhaiDT.Controls.Add(this.uiButton5);
            this.grpKhaiDT.Controls.Add(this.label18);
            this.grpKhaiDT.Controls.Add(this.txtServerKhaiDT);
            this.grpKhaiDT.Controls.Add(this.txtPassKhaiDT);
            this.grpKhaiDT.Controls.Add(this.txtUserKhaiDT);
            this.grpKhaiDT.Controls.Add(this.txtDatabaseKhaiDT);
            this.grpKhaiDT.Controls.Add(this.label6);
            this.grpKhaiDT.Controls.Add(this.label11);
            this.grpKhaiDT.Controls.Add(this.label12);
            this.grpKhaiDT.Controls.Add(this.label13);
            this.grpKhaiDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpKhaiDT.Location = new System.Drawing.Point(12, 188);
            this.grpKhaiDT.Name = "grpKhaiDT";
            this.grpKhaiDT.Size = new System.Drawing.Size(615, 82);
            this.grpKhaiDT.TabIndex = 2;
            this.grpKhaiDT.Text = "Database KhaiDT";
            this.grpKhaiDT.VisualStyleManager = this.vsmMain;
            // 
            // uiButton5
            // 
            this.uiButton5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton5.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton5.Icon")));
            this.uiButton5.Location = new System.Drawing.Point(482, 49);
            this.uiButton5.Name = "uiButton5";
            this.uiButton5.Size = new System.Drawing.Size(122, 23);
            this.uiButton5.TabIndex = 9;
            this.uiButton5.Text = "Kiểm tra kết nối";
            this.uiButton5.VisualStyleManager = this.vsmMain;
            this.uiButton5.Click += new System.EventHandler(this.uiButton5_Click);
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.SystemColors.Info;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(17, 49);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(271, 22);
            this.label18.TabIndex = 8;
            this.label18.Text = "User: chỉ nên cấp quyền đọc trên database KhaiDT";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtServerKhaiDT
            // 
            this.txtServerKhaiDT.Location = new System.Drawing.Point(56, 20);
            this.txtServerKhaiDT.Name = "txtServerKhaiDT";
            this.txtServerKhaiDT.Size = new System.Drawing.Size(110, 21);
            this.txtServerKhaiDT.TabIndex = 1;
            this.txtServerKhaiDT.VisualStyleManager = this.vsmMain;
            // 
            // txtPassKhaiDT
            // 
            this.txtPassKhaiDT.Location = new System.Drawing.Point(522, 20);
            this.txtPassKhaiDT.Name = "txtPassKhaiDT";
            this.txtPassKhaiDT.PasswordChar = '*';
            this.txtPassKhaiDT.Size = new System.Drawing.Size(82, 21);
            this.txtPassKhaiDT.TabIndex = 7;
            this.txtPassKhaiDT.VisualStyleManager = this.vsmMain;
            // 
            // txtUserKhaiDT
            // 
            this.txtUserKhaiDT.Location = new System.Drawing.Point(378, 20);
            this.txtUserKhaiDT.Name = "txtUserKhaiDT";
            this.txtUserKhaiDT.Size = new System.Drawing.Size(75, 21);
            this.txtUserKhaiDT.TabIndex = 5;
            this.txtUserKhaiDT.Text = "ecs";
            this.txtUserKhaiDT.VisualStyleManager = this.vsmMain;
            // 
            // txtDatabaseKhaiDT
            // 
            this.txtDatabaseKhaiDT.Location = new System.Drawing.Point(224, 20);
            this.txtDatabaseKhaiDT.Name = "txtDatabaseKhaiDT";
            this.txtDatabaseKhaiDT.Size = new System.Drawing.Size(109, 21);
            this.txtDatabaseKhaiDT.TabIndex = 3;
            this.txtDatabaseKhaiDT.Text = "HQQLHGCTEMPT";
            this.txtDatabaseKhaiDT.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(459, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Password:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(339, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "User:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(166, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Database:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Server:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpSXXK
            // 
            this.grpSXXK.BackColor = System.Drawing.Color.Transparent;
            this.grpSXXK.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.grpSXXK.Controls.Add(this.label14);
            this.grpSXXK.Controls.Add(this.uiButton4);
            this.grpSXXK.Controls.Add(this.txtServerSXXK);
            this.grpSXXK.Controls.Add(this.txtPassSXXK);
            this.grpSXXK.Controls.Add(this.txtUserSXXK);
            this.grpSXXK.Controls.Add(this.txtDatabaseSXXK);
            this.grpSXXK.Controls.Add(this.label7);
            this.grpSXXK.Controls.Add(this.label8);
            this.grpSXXK.Controls.Add(this.label9);
            this.grpSXXK.Controls.Add(this.label10);
            this.grpSXXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSXXK.Location = new System.Drawing.Point(12, 12);
            this.grpSXXK.Name = "grpSXXK";
            this.grpSXXK.Size = new System.Drawing.Size(615, 81);
            this.grpSXXK.TabIndex = 0;
            this.grpSXXK.Text = "Database ECS.KD";
            this.grpSXXK.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.SystemColors.Info;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(17, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(271, 22);
            this.label14.TabIndex = 10;
            this.label14.Text = "User: phải có quyền đọc, ghi trên database ECS.KD";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiButton4
            // 
            this.uiButton4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton4.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton4.Icon")));
            this.uiButton4.Location = new System.Drawing.Point(482, 49);
            this.uiButton4.Name = "uiButton4";
            this.uiButton4.Size = new System.Drawing.Size(122, 23);
            this.uiButton4.TabIndex = 9;
            this.uiButton4.Text = "Kiểm tra kết nối";
            this.uiButton4.VisualStyleManager = this.vsmMain;
            this.uiButton4.Click += new System.EventHandler(this.uiButton4_Click);
            // 
            // txtServerSXXK
            // 
            this.txtServerSXXK.Location = new System.Drawing.Point(56, 20);
            this.txtServerSXXK.Name = "txtServerSXXK";
            this.txtServerSXXK.Size = new System.Drawing.Size(110, 21);
            this.txtServerSXXK.TabIndex = 1;
            this.txtServerSXXK.VisualStyleManager = this.vsmMain;
            // 
            // txtPassSXXK
            // 
            this.txtPassSXXK.Location = new System.Drawing.Point(522, 20);
            this.txtPassSXXK.Name = "txtPassSXXK";
            this.txtPassSXXK.PasswordChar = '*';
            this.txtPassSXXK.Size = new System.Drawing.Size(82, 21);
            this.txtPassSXXK.TabIndex = 7;
            this.txtPassSXXK.VisualStyleManager = this.vsmMain;
            // 
            // txtUserSXXK
            // 
            this.txtUserSXXK.Location = new System.Drawing.Point(378, 20);
            this.txtUserSXXK.Name = "txtUserSXXK";
            this.txtUserSXXK.Size = new System.Drawing.Size(75, 21);
            this.txtUserSXXK.TabIndex = 5;
            this.txtUserSXXK.Text = "ecs";
            this.txtUserSXXK.VisualStyleManager = this.vsmMain;
            // 
            // txtDatabaseSXXK
            // 
            this.txtDatabaseSXXK.Location = new System.Drawing.Point(224, 20);
            this.txtDatabaseSXXK.Name = "txtDatabaseSXXK";
            this.txtDatabaseSXXK.Size = new System.Drawing.Size(109, 21);
            this.txtDatabaseSXXK.TabIndex = 3;
            this.txtDatabaseSXXK.Text = "ECS.KD";
            this.txtDatabaseSXXK.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(459, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Password:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(339, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "User:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(166, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Database:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Server:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(246, 276);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 23);
            this.uiButton1.TabIndex = 3;
            this.uiButton1.Text = "&Lưu";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click_1);
            // 
            // uiButton2
            // 
            this.uiButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(327, 276);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 4;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            // 
            // grpECS_SXXK
            // 
            this.grpECS_SXXK.BackColor = System.Drawing.Color.Transparent;
            this.grpECS_SXXK.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.grpECS_SXXK.Controls.Add(this.uiButton3);
            this.grpECS_SXXK.Controls.Add(this.label1);
            this.grpECS_SXXK.Controls.Add(this.txtServerECS_SXXK);
            this.grpECS_SXXK.Controls.Add(this.txtPassECS_SXXK);
            this.grpECS_SXXK.Controls.Add(this.txtUserECS_SXXK);
            this.grpECS_SXXK.Controls.Add(this.txtDatabaseECS_SXXK);
            this.grpECS_SXXK.Controls.Add(this.label5);
            this.grpECS_SXXK.Controls.Add(this.label4);
            this.grpECS_SXXK.Controls.Add(this.label3);
            this.grpECS_SXXK.Controls.Add(this.label2);
            this.grpECS_SXXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpECS_SXXK.Location = new System.Drawing.Point(12, 99);
            this.grpECS_SXXK.Name = "grpECS_SXXK";
            this.grpECS_SXXK.Size = new System.Drawing.Size(615, 85);
            this.grpECS_SXXK.TabIndex = 1;
            this.grpECS_SXXK.Text = "Database ECS.SXXK";
            this.grpECS_SXXK.VisualStyleManager = this.vsmMain;
            // 
            // uiButton3
            // 
            this.uiButton3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton3.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton3.Icon")));
            this.uiButton3.Location = new System.Drawing.Point(482, 49);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(122, 23);
            this.uiButton3.TabIndex = 9;
            this.uiButton3.Text = "Kiểm tra kết nối";
            this.uiButton3.VisualStyleManager = this.vsmMain;
            this.uiButton3.Click += new System.EventHandler(this.uiButton3_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Info;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(271, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "User: phải có quyền đọc, ghi trên database ECS.SXXK";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtServerECS_SXXK
            // 
            this.txtServerECS_SXXK.Location = new System.Drawing.Point(56, 20);
            this.txtServerECS_SXXK.Name = "txtServerECS_SXXK";
            this.txtServerECS_SXXK.Size = new System.Drawing.Size(110, 21);
            this.txtServerECS_SXXK.TabIndex = 1;
            this.txtServerECS_SXXK.VisualStyleManager = this.vsmMain;
            // 
            // txtPassECS_SXXK
            // 
            this.txtPassECS_SXXK.Location = new System.Drawing.Point(522, 20);
            this.txtPassECS_SXXK.Name = "txtPassECS_SXXK";
            this.txtPassECS_SXXK.PasswordChar = '*';
            this.txtPassECS_SXXK.Size = new System.Drawing.Size(82, 21);
            this.txtPassECS_SXXK.TabIndex = 7;
            this.txtPassECS_SXXK.VisualStyleManager = this.vsmMain;
            // 
            // txtUserECS_SXXK
            // 
            this.txtUserECS_SXXK.Location = new System.Drawing.Point(378, 20);
            this.txtUserECS_SXXK.Name = "txtUserECS_SXXK";
            this.txtUserECS_SXXK.Size = new System.Drawing.Size(75, 21);
            this.txtUserECS_SXXK.TabIndex = 5;
            this.txtUserECS_SXXK.Text = "ecs";
            this.txtUserECS_SXXK.VisualStyleManager = this.vsmMain;
            // 
            // txtDatabaseECS_SXXK
            // 
            this.txtDatabaseECS_SXXK.Location = new System.Drawing.Point(224, 20);
            this.txtDatabaseECS_SXXK.Name = "txtDatabaseECS_SXXK";
            this.txtDatabaseECS_SXXK.Size = new System.Drawing.Size(109, 21);
            this.txtDatabaseECS_SXXK.TabIndex = 3;
            this.txtDatabaseECS_SXXK.Text = "ECS.SXXK";
            this.txtDatabaseECS_SXXK.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(459, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Password:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(339, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "User:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(166, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Database:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Server:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(200, 100);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "uiGroupBox2";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // rfvServerECS_SXXK
            // 
            this.rfvServerECS_SXXK.ControlToValidate = this.txtServerECS_SXXK;
            this.rfvServerECS_SXXK.ErrorMessage = "\"Tên máy chủ chứa cơ sơ dữ liệu\" không được trống";
            this.rfvServerECS_SXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvServerECS_SXXK.Icon")));
            // 
            // rfvDatabaseECS_SXXK
            // 
            this.rfvDatabaseECS_SXXK.ControlToValidate = this.txtDatabaseECS_SXXK;
            this.rfvDatabaseECS_SXXK.ErrorMessage = "\"Cơ sơ dữ liệu \" không được trống";
            this.rfvDatabaseECS_SXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDatabaseECS_SXXK.Icon")));
            // 
            // rfvUserECS_SXXK
            // 
            this.rfvUserECS_SXXK.ControlToValidate = this.txtUserECS_SXXK;
            this.rfvUserECS_SXXK.ErrorMessage = "Chưa nhập tên truy cập";
            this.rfvUserECS_SXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvUserECS_SXXK.Icon")));
            // 
            // containerValidator1
            // 
            this.containerValidator1.ContainerToValidate = this;
            this.containerValidator1.HostingForm = this;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // rfvServerSXXK
            // 
            this.rfvServerSXXK.ControlToValidate = this.txtServerSXXK;
            this.rfvServerSXXK.ErrorMessage = "\"Tên máy chủ chứa cơ sơ dữ liệu\" không được trống";
            this.rfvServerSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvServerSXXK.Icon")));
            // 
            // rfvDatabaseSXXK
            // 
            this.rfvDatabaseSXXK.ControlToValidate = this.txtDatabaseSXXK;
            this.rfvDatabaseSXXK.ErrorMessage = "\"Cơ sơ dữ liệu \" không được trống";
            this.rfvDatabaseSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDatabaseSXXK.Icon")));
            // 
            // rfvUserSXXk
            // 
            this.rfvUserSXXk.ControlToValidate = this.txtUserSXXK;
            this.rfvUserSXXk.ErrorMessage = "Chưa nhập tên truy cập";
            this.rfvUserSXXk.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvUserSXXk.Icon")));
            // 
            // rfvServerKhaiDT
            // 
            this.rfvServerKhaiDT.ControlToValidate = this.txtServerKhaiDT;
            this.rfvServerKhaiDT.ErrorMessage = "\"Tên máy chủ chứa cơ sơ dữ liệu\" không được trống";
            this.rfvServerKhaiDT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvServerKhaiDT.Icon")));
            // 
            // rfvDatabaseKhaiDT
            // 
            this.rfvDatabaseKhaiDT.ControlToValidate = this.txtDatabaseKhaiDT;
            this.rfvDatabaseKhaiDT.ErrorMessage = "\"Cơ sơ dữ liệu \" không được trống";
            this.rfvDatabaseKhaiDT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDatabaseKhaiDT.Icon")));
            // 
            // rfvUserKhaiDT
            // 
            this.rfvUserKhaiDT.ControlToValidate = this.txtUserKhaiDT;
            this.rfvUserKhaiDT.ErrorMessage = "Chưa nhập tên truy cập";
            this.rfvUserKhaiDT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvUserKhaiDT.Icon")));
            // 
            // CauHinhCSDLHaiQuanForm
            // 
            this.AcceptButton = this.uiButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 309);
            this.Controls.Add(this.uiGroupBox2);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CauHinhCSDLHaiQuanForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thiết lập thông số kết nối cơ sở dữ liệu";
            this.Load += new System.EventHandler(this.SendForm_Load);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpKhaiDT)).EndInit();
            this.grpKhaiDT.ResumeLayout(false);
            this.grpKhaiDT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpSXXK)).EndInit();
            this.grpSXXK.ResumeLayout(false);
            this.grpSXXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpECS_SXXK)).EndInit();
            this.grpECS_SXXK.ResumeLayout(false);
            this.grpECS_SXXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvServerECS_SXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDatabaseECS_SXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvUserECS_SXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvServerSXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDatabaseSXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvUserSXXk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvServerKhaiDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDatabaseKhaiDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvUserKhaiDT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox grpECS_SXXK;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtPassECS_SXXK;
        private Janus.Windows.GridEX.EditControls.EditBox txtUserECS_SXXK;
        private Janus.Windows.GridEX.EditControls.EditBox txtDatabaseECS_SXXK;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvServerECS_SXXK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDatabaseECS_SXXK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvUserECS_SXXK;
        private Company.Controls.CustomValidation.ContainerValidator containerValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.GridEX.EditControls.EditBox txtServerECS_SXXK;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.EditControls.UIGroupBox grpKhaiDT;
        private Janus.Windows.GridEX.EditControls.EditBox txtServerKhaiDT;
        private Janus.Windows.GridEX.EditControls.EditBox txtPassKhaiDT;
        private Janus.Windows.GridEX.EditControls.EditBox txtUserKhaiDT;
        private Janus.Windows.GridEX.EditControls.EditBox txtDatabaseKhaiDT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIGroupBox grpSXXK;
        private Janus.Windows.GridEX.EditControls.EditBox txtServerSXXK;
        private Janus.Windows.GridEX.EditControls.EditBox txtPassSXXK;
        private Janus.Windows.GridEX.EditControls.EditBox txtUserSXXK;
        private Janus.Windows.GridEX.EditControls.EditBox txtDatabaseSXXK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvServerSXXK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDatabaseSXXK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvUserSXXk;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvServerKhaiDT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDatabaseKhaiDT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvUserKhaiDT;
        private Janus.Windows.EditControls.UIButton uiButton5;
        private Janus.Windows.EditControls.UIButton uiButton4;
        private Janus.Windows.EditControls.UIButton uiButton3;
        private System.Windows.Forms.Label label14;

    }
}