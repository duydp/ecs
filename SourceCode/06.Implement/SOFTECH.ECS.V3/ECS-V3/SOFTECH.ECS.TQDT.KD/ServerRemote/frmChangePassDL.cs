﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;

namespace ServerRemote
{
    public partial class frmChangePassDL : Company.Interface.BaseForm
    {
        public frmChangePassDL()
        {
            InitializeComponent();
        }

        public enum OpenType
        {
            DaiLy,
            DoanhNghiep
        }
        public OpenType openType;
        public string User 
        { 
            get { return lblAccountDL.Text; } 
            set { lblAccountDL.Text = value; } 
        }
        public string Ten
        {
            get { return lblTenDaiLy.Text; }
            set { lblTenDaiLy.Text = value; }
        }
        private void frmChangePass_Load(object sender, EventArgs e)
        {
            Display();
        }
        private void Display()
        {
            if (this.openType == OpenType.DoanhNghiep)
            {
                lbluser.Text = "Tài khoản :";
                lblTen.Text = "Tên doanh nghiệp :";
                txtPassOld.Enabled = true;
                lblpassOld.Enabled = true;
            }
            else
            {
                txtPassOld.Enabled = false;
                lblpassOld.Enabled = false;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMatKhau.Text.Trim() == txtNhapLaiMK.Text.Trim())
                {
                    if (this.openType == OpenType.DoanhNghiep)
                    {
                        string passold = Helpers.GetMD5Value(txtPassOld.Text.Trim());
                        if (passold != Globals.PasswordLogin)
                        {
                            MessageBoxControl.ShowMessage("Mật khẩu cũ không đúng");
                            //MessageBox.Show("Mật khẩu cũ không đúng");
                            return;
                        }
                    }
                    string PassChange = Helpers.GetMD5Value(txtMatKhau.Text.Trim());
                    ISignMessage service = WebService.SignMessage();
                    string result = service.ChangePass(Globals.UserNameLogin, Globals.PasswordLogin, User, PassChange, openType == OpenType.DoanhNghiep);
                    if (string.IsNullOrEmpty(result))
                    {
                        if (this.openType == OpenType.DoanhNghiep)
                        {
                            Globals.PasswordLogin = PassChange;
                        }
                        MessageBoxControl.ShowMessage("Đổi mật khẩu thành công");
                        //MessageBox.Show("Đổi mật khẩu thành công");
                        this.Close();
                    }
                    else
                    {
                        MessageBoxControl.ShowMessage(result);
                        //MessageBox.Show(result);
                    }
                }
                else
                {
                    MessageBoxControl.ShowMessage("Mật khẩu mới không giống nhau");
                    //MessageBox.Show("Mật khẩu mới không giống nhau");
                }
            }
            catch (System.Exception ex)
            {
                MessageBoxControl.ShowMessage("Lỗi kết nối:" + ex.Message);
                //MessageBox.Show("Lỗi kết nối: " + ex.Message);
            }
           
            
        }
    }
}
