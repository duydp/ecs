﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu.CTTT
{
    public partial class CTTT_HopDongXuatKhau
    {
        List<CTTT_HopDongXuatKhauChiTiet> _HopDongXuatKhauChiTietList = new List<CTTT_HopDongXuatKhauChiTiet>();
        public List<CTTT_HopDongXuatKhauChiTiet> HopDongXuatKhauChiTietList
        {
            set { this._HopDongXuatKhauChiTietList = value; }
            get { return this._HopDongXuatKhauChiTietList; }
        }

        public bool InsertUpdateFull()
        {
            bool ret = false;
              SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
              using (SqlConnection connection = (SqlConnection)db.CreateConnection())
              {
                  connection.Open();
                  SqlTransaction transaction = connection.BeginTransaction();
                  try
                  {
                      if (this.Id == 0)
                          this.Id = this.Insert();
                      else
                          this.Update();

                      //Sô container
                      foreach (CTTT_HopDongXuatKhauChiTiet item in this.HopDongXuatKhauChiTietList)
                      {
                          if (item.ID == 0)
                          {
                              item.Master_id = this.Id;
                              item.ID = item.Insert();
                          }
                          else
                          {
                              item.InsertUpdate();
                          }
                      }
                      ret = true;
                      transaction.Commit();
                  }
                  catch (Exception ex)
                  {
                      transaction.Rollback();
                      transaction.Commit();
                      this.Id = 0;
                      connection.Close();
                      throw new Exception(ex.Message);
                  }
                  finally
                  {
                      connection.Close();
                  }

              }
            return ret;
        }

    }
   
}
