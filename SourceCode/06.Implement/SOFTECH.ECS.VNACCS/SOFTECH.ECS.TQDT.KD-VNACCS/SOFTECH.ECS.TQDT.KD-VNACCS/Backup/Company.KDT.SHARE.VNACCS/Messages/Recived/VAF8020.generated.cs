using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAF8020 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CSM { get; set; }
public PropertiesAttribute CBN { get; set; }
public PropertiesAttribute SPN { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute DDC { get; set; }
public PropertiesAttribute DKC { get; set; }
public PropertiesAttribute IEN { get; set; }
public PropertiesAttribute IEC { get; set; }
public PropertiesAttribute PCD { get; set; }
public PropertiesAttribute ADB { get; set; }
public PropertiesAttribute TEL { get; set; }
public PropertiesAttribute SBN { get; set; }
public PropertiesAttribute SBK { get; set; }
public PropertiesAttribute SBS { get; set; }
public PropertiesAttribute SRN { get; set; }
public PropertiesAttribute TSN { get; set; }
public PropertiesAttribute BNM { get; set; }
public PropertiesAttribute BCD { get; set; }
public PropertiesAttribute BPS { get; set; }
public PropertiesAttribute BPN { get; set; }
public GroupAttribute TN1 { get; set; }
public PropertiesAttribute TDA { get; set; }
public PropertiesAttribute TA4 { get; set; }
public PropertiesAttribute TDQ { get; set; }
public PropertiesAttribute CCC { get; set; }
public PropertiesAttribute EXR { get; set; }
public PropertiesAttribute LOP { get; set; }
public PropertiesAttribute EFD { get; set; }
public PropertiesAttribute TAN { get; set; }
public PropertiesAttribute TNM { get; set; }
public PropertiesAttribute ARR { get; set; }
public PropertiesAttribute DOP { get; set; }

public VAF8020()
        {
CSM = new PropertiesAttribute(210, typeof(string));
CBN = new PropertiesAttribute(210, typeof(string));
SPN = new PropertiesAttribute(12, typeof(int));
ICN = new PropertiesAttribute(12, typeof(int));
DDC = new PropertiesAttribute(8, typeof(DateTime));
DKC = new PropertiesAttribute(3, typeof(string));
IEN = new PropertiesAttribute(300, typeof(string));
IEC = new PropertiesAttribute(13, typeof(string));
PCD = new PropertiesAttribute(7, typeof(string));
ADB = new PropertiesAttribute(300, typeof(string));
TEL = new PropertiesAttribute(20, typeof(string));
SBN = new PropertiesAttribute(210, typeof(string));
SBK = new PropertiesAttribute(11, typeof(string));
SBS = new PropertiesAttribute(10, typeof(string));
SRN = new PropertiesAttribute(10, typeof(string));
TSN = new PropertiesAttribute(60, typeof(string));
BNM = new PropertiesAttribute(210, typeof(string));
BCD = new PropertiesAttribute(11, typeof(string));
BPS = new PropertiesAttribute(10, typeof(string));
BPN = new PropertiesAttribute(10, typeof(string));
TDA = new PropertiesAttribute(11, typeof(int));
TA4 = new PropertiesAttribute(11, typeof(int));
TDQ = new PropertiesAttribute(12, typeof(int));
CCC = new PropertiesAttribute(3, typeof(string));
EXR = new PropertiesAttribute(9, typeof(int));
LOP = new PropertiesAttribute(3, typeof(int));
EFD = new PropertiesAttribute(8, typeof(DateTime));
TAN = new PropertiesAttribute(15, typeof(string));
TNM = new PropertiesAttribute(210, typeof(string));
ARR = new PropertiesAttribute(765, typeof(string));
DOP = new PropertiesAttribute(8, typeof(DateTime));
#region TN1
List<PropertiesAttribute> listTN1 = new List<PropertiesAttribute>();
listTN1.Add(new PropertiesAttribute(27, 6, EnumGroupID.VAF8020_TN1, typeof(string)));
listTN1.Add(new PropertiesAttribute(4, 6, EnumGroupID.VAF8020_IS1, typeof(int)));
listTN1.Add(new PropertiesAttribute(11, 6, EnumGroupID.VAF8020_IA1, typeof(int)));
listTN1.Add(new PropertiesAttribute(11, 6, EnumGroupID.VAF8020_IM1, typeof(int)));
listTN1.Add(new PropertiesAttribute(12, 6, EnumGroupID.VAF8020_IQ1, typeof(int)));
TN1 = new GroupAttribute("TN1", 6, listTN1);
#endregion TN1
TongSoByte = 3204;
}

}public partial class EnumGroupID
    {
public static readonly string VAF8020_TN1 = "VAF8020_TN1";
public static readonly string VAF8020_IS1 = "VAF8020_IS1";
public static readonly string VAF8020_IA1 = "VAF8020_IA1";
public static readonly string VAF8020_IM1 = "VAF8020_IM1";
public static readonly string VAF8020_IQ1 = "VAF8020_IQ1";
}

}
