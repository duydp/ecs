using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace Company.Interface.ProdInfo
{
    public partial class frmRegister_old : BaseForm
    {
        private string ProductID;

        public frmRegister_old()
        {
            InitializeComponent();
        }

        private void frmRegister_Load(object sender, EventArgs e)
        {
            try
            {
                txtCodeActivate.Text = Program.getProcessorID();
                ProductID = this.getProductID();
            }
            catch (Exception ex)
            {
                MLMessages("Có lỗi xảy ra, vui lòng liên hệ với nhà cung cấp.","MSG_0203084","", false);
                this.Close();
            }
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            if (this.validForm())
            {
                if (this.doActivate())
                {
                    if (MLMessages("Kích hoạt thành công!\nHãy khởi động lại chương trình để hoàn thành việc kích hoạt","MSG_0203085","", false) == "Cancel")
                    {
                        Application.Restart();
                    }
                    else
                    {
                        this.Close();
                    }
                }
                else
                {
                    MLMessages("Kích hoạt không thành công : \nThông tin kích hoạt không chính xác.","MSG_0203086","", false);
                }
            }
            else
            {
                ShowMessage(setText("Dữ liệu chưa hợp lệ, hãy kiểm tra lại", "Invalid input value.Please check again"), false);
            }
        }

        private bool doActivate()
        {
            bool val = false;

            if (radioCDKey.Checked)
            {
                try
                {
                    string[] InfoActivation = Program.lic.Activate(txtCDKey.Text, this.ProductID, txtCodeActivate.Text);
                    if (Boolean.Parse(InfoActivation[0]))
                    {
                        if (this.saveConfig(InfoActivation))
                        {
                            val = true;
                        }
                        else
                        {
                            MLMessages("Có lỗi dưới Client!\n\nVui lòng kích hoạt lại.","MSG_0203087","", false);
                        }
                    }
                    else
                    {
                        ShowMessage(InfoActivation[1], false);
                    }
                }
                catch (Exception e)
                {
                    MLMessages("Có lỗi dưới khi kích hoạt! \n\nVui lòng kiểm tra kết nối internet","MSG_0203088","", false);
                }
            }
            else
            {
                try
                {
                    
                    List<Company.KDT.SHARE.Components.Utils.License> listLic = Program.lic.LoadListFromFile(txtFile.Text);
                    foreach (Company.KDT.SHARE.Components.Utils.License lic in listLic)
                    {
                        if (lic.codeActivate == Program.getCodeToActivate())
                        {
                            Program.lic = lic;
                            Program.lic.Save();
                            val = true;
                        }
                    } 
                }
                catch (Exception e)
                {
                    MLMessages("Tệp tin không đúng phiên bản.","MSG_0203089","", false);
                }
            }

            return val;
        }

        private bool validForm()
        { 
            bool value = false;
            if (radioCDKey.Checked)
            {
                if (txtCDKey.Text.Trim().Length == 25)
                {
                    value = true;
                }
            }
            else
            {
                if (txtFile.Text.Length > 0 && File.Exists(txtFile.Text))
                {
                    value = true;
                }
            }
            return value;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool saveConfig(string[] licenseInfo)
        {
            bool value = false;
            try
            {
                Program.lic.codeActivate = Program.getCodeToActivate();
                Program.lic.licenseName = licenseInfo[1];
                Program.lic.numberClient = licenseInfo[2];
                Program.lic.dayExpires = licenseInfo[3];
                Program.lic.Save();

                value = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return value;
        }

        private string getProductID()
        {
            string val;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                val = config.AppSettings.Settings["ProductID"].Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return val;
        }

        private void txtFile_ButtonClick(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "ECS Licenses | *.Ecslic";
            if (od.ShowDialog(this) == DialogResult.OK)
            {
                txtFile.Text = od.FileName;
            }
            od.Dispose();
        }

        private void radioCDKey_CheckedChanged(object sender, EventArgs e)
        {
            txtCDKey.Enabled = radioCDKey.Checked;
            txtFile.Enabled = !radioCDKey.Checked;
        }
    }
}