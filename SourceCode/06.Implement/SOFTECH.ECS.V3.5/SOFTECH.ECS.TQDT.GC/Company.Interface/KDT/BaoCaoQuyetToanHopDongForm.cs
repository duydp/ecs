﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.SXXK;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.IO;
using System.Data.SqlClient;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
using Janus.Windows.GridEX;
using Company.Interface.Report.GC.TT39;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.KDT
{
    public partial class BaoCaoQuyetToanHopDongForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public HoSoQuyetToan_DSHopDong DSHD = new HoSoQuyetToan_DSHopDong();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
        public string LIST_HOPDONG_ID;
        public int NAMQUYETTOAN;
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        public DataTable dtNPLTotal = new DataTable();
        public DataTable dtSPTotal = new DataTable();
        public bool IsActive = false;
        public bool IsReadExcel = false;

        public string LoaiHangHoa = "NPL";
        public KDT_VNACCS_BaoCaoQuyetToan_TT39 BCQT = new KDT_VNACCS_BaoCaoQuyetToan_TT39();
        public List<HopDong> HDCollection = new List<HopDong>();

        public KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HopDong = new KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail();
        public List<KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail> HopDongCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail>();
        public KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail HangHoa = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
        public List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> HangHoaCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();

        public String Caption;
        public bool IsChange;

        public BaoCaoQuyetToanHopDongForm()
        {
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (BCQT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || BCQT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || BCQT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void BaoCaoQuyetToanHopDongForm_Load(object sender, EventArgs e)
        {
            try
            {
                cbbLoaiHinhBaoCao.TextChanged -= new EventHandler(txt_TextChanged);
                cbbLoaiHinhBaoCao.SelectedIndex = 0;
                cbbLoaiHinhBaoCao.TextChanged += new EventHandler(txt_TextChanged);

                cbbType.TextChanged -= new EventHandler(txt_TextChanged);
                cbbType.SelectedIndex = 0;
                cbbType.TextChanged += new EventHandler(txt_TextChanged);

                Caption = this.Text;
                if (BCQT.ID > 0)
                {
                    SetBCQTTT39();
                    if (BCQT.HopDongCollection.Count > 0)
                    {
                        ProcessDataTT39();
                    }
                }
                else
                {
                    BCQT.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
                NAMQUYETTOAN = clcNgayBatDauBC.Value.Year;
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ProcessDataTT39()
        {
            GetListHopDong();
            BindDataTotal();
            BindDataNPLQuyetToan();
            BindDataSPQuyetToan();
        }
        private void BindDataTotal()
        {
            try
            {
                // Xử lý dữ liệu
                List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> ToTalHangHoaCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();
                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HD in BCQT.HopDongCollection)
                {
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in HD.HangHoaCollection)
                    {
                        ToTalHangHoaCollection.Add(item);
                    }
                }
                dgListTotal.Refresh();
                dgListTotal.DataSource = ToTalHangHoaCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send("Send");
                    break;
                case "cmdEdit":
                    Send("Edit");
                    break;
                case "cmdCancel":
                    if (ShowMessage("Doanh nghiệp có chắc chắn muốn Khai báo Hủy báo cáo quyết toán này đến HQ không ? ", true) == "Yes")
                    {
                        BCQT.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        BCQT.Update();
                        setCommandStatus();
                        Send("Cancel");
                    }
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    UpdateGUIDSTR();
                    break;
                case "cmdAddExcel":
                    btnReadExcel_Click(null, null);
                    break;
                case "cmdAdd":
                    Add();
                    break;
                case "cmdChuyenTT":
                    ChuyenTT();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
                default:
                    break;
            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = BCQT.ID;
                f.loaiKhaiBao = LoaiKhaiBao.GoodItem;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", BCQT.ID, LoaiKhaiBao.GoodItem), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageBCQT(BCQT);
                    BCQT.InsertUpdateFull();
                    ShowMessage("Cập nhật thông tin thành công .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ChuyenTT()
        {
            try
            {
                if (ShowMessage("Doanh nghiệp có chắc chắn muốn Chuyển báo cáo quyết toán này sang Khai báo sửa không ? ", true) == "Yes")
                {
                    BCQT.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    BCQT.Update();
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void setCommandStatus()
        {
            if (BCQT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnProcess.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdChuyenTT.Enabled = cmdChuyenTT.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = BCQT.SoTN.ToString();
                clcNgayTN.Value = BCQT.NgayTN;
                lblTrangThaiXuLy.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (BCQT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || BCQT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnProcess.Enabled = true;

                clcNgayTN.Value = DateTime.Now;
                lblTrangThaiXuLy.Text = BCQT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (BCQT.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnProcess.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTN.Text = BCQT.SoTN.ToString();
                clcNgayTN.Value = BCQT.NgayTN;
                lblTrangThaiXuLy.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (BCQT.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || BCQT.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || BCQT.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnProcess.Enabled = false;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThaiXuLy.Text = BCQT.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (BCQT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnProcess.Enabled = true;

                txtSoTN.Text = BCQT.SoTN.ToString();
                clcNgayTN.Value = BCQT.NgayTN;
                lblTrangThaiXuLy.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (BCQT.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnProcess.Enabled = true;

                txtSoTN.Text = BCQT.SoTN.ToString();
                clcNgayTN.Value = BCQT.NgayTN;
                lblTrangThaiXuLy.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (BCQT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || BCQT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || BCQT.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChuyenTT.Enabled = cmdChuyenTT1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnProcess.Enabled = false;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = BCQT.SoTN.ToString();
                clcNgayTN.Value = BCQT.NgayTN;
                lblTrangThaiXuLy.Text = BCQT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Not declared") : setText("Chờ duyệt Sửa / Hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }

        }
        private void GetListHopDong()
        {
            LIST_HOPDONG_ID = String.Empty;
            foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
            {
                LIST_HOPDONG_ID += item.ID_HopDong + ",";
            }
            if (!String.IsNullOrEmpty(LIST_HOPDONG_ID))
            {
                LIST_HOPDONG_ID = LIST_HOPDONG_ID.Substring(0, LIST_HOPDONG_ID.Length - 1);
            }
        }
        private void Add()
        {
            BaoCaoQuyetToan_HangHoaForm f = new BaoCaoQuyetToan_HangHoaForm();
            f.BCQT = BCQT;
            f.LoaiHangHoa = LoaiHangHoa;
            f.OpenType = this.OpenType;
            f.ShowDialog(this);
            ProcessDataTT39();
        }
        private void btnReadExcel_Click(object sender, EventArgs e)
        {
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in goodItem.GoodItemsCollection)
            {
                if (item.ID > 0)
                    item.Delete();
            }
            goodItem.GoodItemsCollection.Clear();
            IsReadExcel = true;
            ReadExcelFormNPL f = new ReadExcelFormNPL();
            f.goodItemNew = goodItem;
            f.ShowDialog(this);
            this.SetChange(f.ImportSuccess);
            BindDataGoodItem();
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_BaoCaoQuyetToan_TT39", "", Convert.ToInt32(BCQT.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(cbbLoaiHinhBaoCao, errorProvider, "Loại hình báo cáo", isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(clcNgayBatDauBC, errorProvider, "Ngày bắt đầu báo cáo", isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(clcNgayKetThucBC, errorProvider, "Ngày kết thúc báo cáo", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (BCQT.HopDongCollection.Count == 0)
                {
                    ShowMessage("Doanh nghiệp chưa chọn hợp đồng và hàng hóa đưa vào Báo cáo quyết toán", false);
                    return;
                }
                GetBCQTTT39();
                BCQT.InsertUpdateFull();
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ShowMessage("Lưu thành công", false);
                SetBCQTTT39();
                BindDataTotal();
                this.SetChange(false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetBCQTTT39()
        {
            try
            {
                if (BCQT.ID == 0)
                {
                    BCQT.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
                BCQT.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                BCQT.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                BCQT.NgayBatDauBC = clcNgayBatDauBC.Value;
                BCQT.NgayKetThucBC = clcNgayKetThucBC.Value;
                if (!String.IsNullOrEmpty(cbbType.SelectedValue.ToString()))
                {
                    BCQT.LoaiSua = Convert.ToInt32(cbbType.SelectedValue.ToString());
                }
                else
                {
                    BCQT.LoaiSua = 1;
                }
                BCQT.LoaiBC = Convert.ToInt32(cbbLoaiHinhBaoCao.SelectedValue.ToString());
                BCQT.GhiChuKhac = txtGhiChuKhac.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetBCQTTT39()
        {
            try
            {
                string TrangThai = BCQT.TrangThaiXuLy.ToString();
                switch (TrangThai)
                {
                    case "0":
                        lblTrangThaiXuLy.Text = "Chờ duyệt";
                        break;
                    case "-1":
                        lblTrangThaiXuLy.Text = "Chưa khai báo";
                        break;
                    case "1":
                        lblTrangThaiXuLy.Text = "Đã duyệt";
                        break;
                    case "2":
                        lblTrangThaiXuLy.Text = "Không phê duyệt";
                        break;

                }
                txtSoTN.Text = BCQT.SoTN.ToString();
                clcNgayTN.Value = BCQT.NgayTN;

                cbbLoaiHinhBaoCao.TextChanged -= new EventHandler(txt_TextChanged);
                cbbLoaiHinhBaoCao.SelectedValue = BCQT.LoaiBC.ToString();
                cbbLoaiHinhBaoCao.TextChanged += new EventHandler(txt_TextChanged);

                cbbType.TextChanged -= new EventHandler(txt_TextChanged);
                cbbType.SelectedValue = BCQT.LoaiSua.ToString();
                cbbType.TextChanged += new EventHandler(txt_TextChanged);

                clcNgayBatDauBC.TextChanged -= new EventHandler(txt_TextChanged);
                clcNgayBatDauBC.Value = BCQT.NgayBatDauBC;
                clcNgayBatDauBC.TextChanged += new EventHandler(txt_TextChanged);

                clcNgayKetThucBC.TextChanged -= new EventHandler(txt_TextChanged);
                clcNgayKetThucBC.Value = BCQT.NgayKetThucBC;
                clcNgayKetThucBC.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChuKhac.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChuKhac.Text = BCQT.GhiChuKhac;
                txtGhiChuKhac.TextChanged += new EventHandler(txt_TextChanged);

                BCQT.HopDongCollection = KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail.SelectCollectionBy_GoodItem_ID(BCQT.ID);
                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
                {
                    item.HangHoaCollection = KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail.SelectCollectionBy_Contract_ID(item.ID);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void SaveGoodItem()
        {
            try
            {
                DSHD = new HoSoQuyetToan_DSHopDong();
                DSHD.Master_ID = Convert.ToInt32(goodItem.ID);
                DSHD.NgayKy = goodItem.NgayTN;
                DSHD.SoHopDong = LIST_HOPDONG_ID;
                DSHD.InsertUpdate();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void BindDataNPLQuyetToan()
        {
            try
            {
                dgListNPL.Refresh();
                if (!String.IsNullOrEmpty(LIST_HOPDONG_ID))
                {
                    if (GlobalSettings.MA_DON_VI == "4000395355")
                    {
                        dgListNPL.DataSource = T_GC_NPL_QUYETOAN.SelectDynamicNew("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MANPL").Tables[0];
                    }
                    else
                    {
                        dgListNPL.DataSource = T_GC_NPL_QUYETOAN.SelectDynamic("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MANPL").Tables[0];
                    }
                }
                else
                {
                    dgListNPL.DataSource = null;
                }
                dgListNPL.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void BindDataSPQuyetToan()
        {
            try
            {
                dgListDataSPQT.Refresh();
                if (!String.IsNullOrEmpty(LIST_HOPDONG_ID))
                {
                    if (GlobalSettings.MA_DON_VI == "4000395355")
                    {
                        dgListDataSPQT.DataSource = T_GC_SANPHAM_QUYETOAN.SelectDynamicNew("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MASP").Tables[0];
                    }
                    else
                    {
                        dgListDataSPQT.DataSource = T_GC_SANPHAM_QUYETOAN.SelectDynamic("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MASP").Tables[0];
                    }
                }
                else
                {
                    dgListDataSPQT.DataSource = null;
                }
                dgListDataSPQT.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public void BindDataGoodItem()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItem.GoodItemsCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void GetGoodItem()
        {
            try
            {
                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                goodItem.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                goodItem.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                goodItem.NgayBatDauBC = clcNgayBatDauBC.Value;
                goodItem.NgayKetThucBC = clcNgayKetThucBC.Value;
                if (!String.IsNullOrEmpty(cbbType.SelectedValue.ToString()))
                {
                    goodItem.LoaiSua = Convert.ToInt32(cbbType.SelectedValue.ToString());
                }
                else
                {
                    goodItem.LoaiSua = 0;
                }
                goodItem.LoaiBaoCao = Convert.ToDecimal(cbbLoaiHinhBaoCao.SelectedValue.ToString());
                goodItem.GhiChuKhac = txtGhiChuKhac.Text.ToString();
                //if (!IsReadExcel)
                //{
                //    GetDataGoodItem();
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void SetGoodItem()
        {
            try
            {
                string TrangThai = goodItem.TrangThaiXuLy.ToString();
                switch (TrangThai)
                {
                    case "0":
                        lblTrangThaiXuLy.Text = "Chờ duyệt";
                        break;
                    case "-1":
                        lblTrangThaiXuLy.Text = "Chưa khai báo";
                        break;
                    case "1":
                        lblTrangThaiXuLy.Text = "Đã duyệt";
                        break;
                    case "2":
                        lblTrangThaiXuLy.Text = "Không phê duyệt";
                        break;

                }
                txtSoTN.Text = goodItem.SoTN.ToString();
                clcNgayTN.Value = goodItem.NgayTN;
                cbbLoaiHinhBaoCao.SelectedValue = goodItem.LoaiBaoCao.ToString();
                cbbType.SelectedValue = goodItem.LoaiSua.ToString();
                clcNgayBatDauBC.Value = goodItem.NgayBatDauBC;
                clcNgayKetThucBC.Value = goodItem.NgayKetThucBC;
                txtGhiChuKhac.Text = goodItem.GhiChuKhac;
                goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetDataBCQTTT39()
        {
            // Xóa dữ liệu ban đầu
            foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HDBCQT in BCQT.HopDongCollection)
            {
                if (HDBCQT.ID > 0)
                {
                    KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail.DeleteBy_Contract_ID(HDBCQT.ID);
                }
            }
            // Xử lý dữ liệu báo cáo 
            if (cbbLoaiHinhBaoCao.SelectedValue.ToString() == "1")
            {
                int k = 1;
                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HD in BCQT.HopDongCollection)
                {
                    HD.STT = k++;
                    int i = 1;
                    List<T_GC_NPL_QUYETOAN> NPLCollection = new List<T_GC_NPL_QUYETOAN>();
                    NPLCollection = T_GC_NPL_QUYETOAN.SelectCollectionDynamic(" HOPDONG_ID IN (" + HD.ID_HopDong + ") AND NAMQUYETTOAN = " + NAMQUYETTOAN, "HOPDONG_ID");
                    HD.HangHoaCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();
                    foreach (T_GC_NPL_QUYETOAN item in NPLCollection)
                    {
                        if (item.LUONGTONDK == 0 && item.LUONGNHAPTK == 0 && item.LUONGXUATTK == 0 && item.LUONGTONCK == 0)
                        {
                        }
                        else
                        {
                            HangHoa = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
                            HangHoa.STT = i++;
                            HangHoa.TenHangHoa = item.TENNPL;
                            HangHoa.MaHangHoa = item.MANPL;
                            HangHoa.DVT = DVT_VNACC(DonViTinh_GetID(item.DVT));
                            HangHoa.LuongTonDauKy = item.LUONGTONDK;
                            HangHoa.LuongNhapTrongKy = item.LUONGNHAPTK;
                            HangHoa.LuongTaiXuat = 0;
                            HangHoa.LuongChuyenMucDichSD = 0;
                            HangHoa.LuongXuatKhau = item.LUONGXUATTK;
                            HangHoa.LuongXuatKhac = 0;
                            HangHoa.LuongTonCuoiKy = item.LUONGTONCK;
                            HangHoa.GhiChu = "";
                            HD.HangHoaCollection.Add(HangHoa);
                        }
                    }
                }
            }
            else
            {
                int k = 1;
                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HD in BCQT.HopDongCollection)
                {
                    HD.STT = k++;
                    int i = 1;
                    List<T_GC_SANPHAM_QUYETOAN> SPCollection = new List<T_GC_SANPHAM_QUYETOAN>();
                    SPCollection = T_GC_SANPHAM_QUYETOAN.SelectCollectionDynamic(" HOPDONG_ID IN (" + HD.ID_HopDong + ") AND NAMQUYETTOAN = " + NAMQUYETTOAN, "HOPDONG_ID");
                    HD.HangHoaCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();
                    foreach (T_GC_SANPHAM_QUYETOAN item in SPCollection)
                    {
                        if (item.LUONGTONDK == 0 && item.LUONGNHAPTK == 0 && item.LUONGXUATTK == 0 && item.LUONGTONCK == 0)
                        {
                        }
                        else
                        {
                            HangHoa = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
                            HangHoa.STT = i++;
                            HangHoa.TenHangHoa = item.TENSP;
                            HangHoa.MaHangHoa = item.MASP;
                            HangHoa.DVT = DVT_VNACC(DonViTinh_GetID(item.DVT));
                            HangHoa.LuongTonDauKy = item.LUONGTONDK;
                            HangHoa.LuongNhapTrongKy = item.LUONGNHAPTK;
                            HangHoa.LuongTaiXuat = 0;
                            HangHoa.LuongChuyenMucDichSD = 0;
                            HangHoa.LuongXuatKhau = item.LUONGXUATTK;
                            HangHoa.LuongXuatKhac = 0;
                            HangHoa.LuongTonCuoiKy = item.LUONGTONCK;
                            HangHoa.GhiChu = "";
                            HD.HangHoaCollection.Add(HangHoa);
                        }
                    }
                }
            }

        }
        private void GetDataGoodItemNew()
        {
            goodItem.GoodItemsCollection.Clear();
            if (cbbLoaiHinhBaoCao.SelectedValue.ToString() == "1")
            {
                List<T_GC_NPL_QUYETOAN> NPLCollection = new List<T_GC_NPL_QUYETOAN>();
                NPLCollection = T_GC_NPL_QUYETOAN.SelectCollectionDynamic(" HOPDONG_ID IN (" + LIST_HOPDONG_ID + ")", "HOPDONG_ID");
                List<HopDong> HDCollection = Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic("ID IN (" + LIST_HOPDONG_ID + ")", "ID");
                foreach (HopDong HD in HDCollection)
                {

                    int i = 0;
                    foreach (T_GC_NPL_QUYETOAN item in NPLCollection)
                    {
                        goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                        goodItemDetail.STT = i++;
                        goodItemDetail.TenHangHoa = item.TENNPL;
                        goodItemDetail.MaHangHoa = item.MANPL;
                        goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(item.DVT));
                        goodItemDetail.TonDauKy = item.LUONGTONDK;
                        goodItemDetail.NhapTrongKy = item.LUONGNHAPTK;
                        goodItemDetail.TaiXuat = 0;
                        goodItemDetail.ChuyenMDSD = 0;
                        goodItemDetail.XuatTrongKy = item.LUONGXUATTK;
                        goodItemDetail.XuatKhac = 0;
                        goodItemDetail.TonCuoiKy = item.LUONGTONCK;
                        goodItemDetail.GhiChu = "";
                        goodItem.GoodItemsCollection.Add(goodItemDetail);
                    }
                }
            }
            else
            {
                List<T_GC_SANPHAM_QUYETOAN> SPCollection = new List<T_GC_SANPHAM_QUYETOAN>();
                SPCollection = T_GC_SANPHAM_QUYETOAN.SelectCollectionDynamic(" HOPDONG_ID IN (" + LIST_HOPDONG_ID + ")", "HOPDONG_ID");
                List<HopDong> HDCollection = Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic("ID IN (" + LIST_HOPDONG_ID + ")", "ID");
                foreach (HopDong HD in HDCollection)
                {

                    int i = 0;
                    foreach (T_GC_SANPHAM_QUYETOAN item in SPCollection)
                    {
                        goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                        goodItemDetail.STT = i++;
                        goodItemDetail.TenHangHoa = item.MASP;
                        goodItemDetail.MaHangHoa = item.TENSP;
                        goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(item.DVT));
                        goodItemDetail.TonDauKy = item.LUONGTONDK;
                        goodItemDetail.NhapTrongKy = item.LUONGNHAPTK;
                        goodItemDetail.TaiXuat = 0;
                        goodItemDetail.ChuyenMDSD = 0;
                        goodItemDetail.XuatTrongKy = item.LUONGXUATTK;
                        goodItemDetail.XuatKhac = 0;
                        goodItemDetail.TonCuoiKy = item.LUONGTONCK;
                        goodItemDetail.GhiChu = "";
                        goodItem.GoodItemsCollection.Add(goodItemDetail);
                    }
                }
            }
        }
        public void GetDataGoodItem()
        {
            try
            {
                //Nguyên phụ liệu
                goodItem.GoodItemsCollection.Clear();
                DataRow[] drNPL = new DataRow[dtNPLTotal.Rows.Count];
                dtNPLTotal.Rows.CopyTo(drNPL, 0);
                int k;
                for (k = 0; k < dtNPLTotal.Rows.Count; k++)
                {
                    goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                    goodItemDetail.STT = k + 1;
                    goodItemDetail.TenHangHoa = drNPL[k]["TENNPL"].ToString();
                    goodItemDetail.MaHangHoa = drNPL[k]["MANPL"].ToString();
                    goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(drNPL[k]["DVT"].ToString()));
                    goodItemDetail.TonDauKy = Convert.ToDecimal(drNPL[k]["LUONGTONDK"].ToString());
                    goodItemDetail.NhapTrongKy = Convert.ToDecimal(drNPL[k]["LUONGNHAPTK"].ToString());
                    goodItemDetail.XuatTrongKy = Convert.ToDecimal(drNPL[k]["LUONGXUATTK"].ToString());
                    goodItemDetail.TonCuoiKy = Convert.ToDecimal(drNPL[k]["LUONGTONCK"].ToString());

                    goodItemDetail.GhiChu = "";//dr["GHICHU"].ToString();
                    goodItem.GoodItemsCollection.Add(goodItemDetail);
                }

                //Sản phẩm
                DataRow[] drSP = new DataRow[dtSPTotal.Rows.Count];
                dtSPTotal.Rows.CopyTo(drSP, 0);
                for (int i = 0; i < dtSPTotal.Rows.Count; i++)
                {
                    goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                    goodItemDetail.STT = k + 1; ;
                    goodItemDetail.TenHangHoa = drSP[i]["TENSP"].ToString();
                    if (GlobalSettings.MA_DON_VI != "4000395355")
                    {
                        goodItemDetail.MaHangHoa = drSP[i]["MASP"].ToString();
                    }
                    else
                    {
                        goodItemDetail.MaHangHoa = String.Empty;
                    }
                    goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(drSP[i]["DVT"].ToString()));

                    goodItemDetail.TonDauKy = Convert.ToDecimal(drSP[i]["LUONGTONDK"].ToString());
                    goodItemDetail.NhapTrongKy = Convert.ToDecimal(drSP[i]["LUONGNHAPTK"].ToString());
                    goodItemDetail.XuatTrongKy = Convert.ToDecimal(drSP[i]["LUONGXUATTK"].ToString());
                    goodItemDetail.TonCuoiKy = Convert.ToDecimal(drSP[i]["LUONGTONCK"].ToString());
                    goodItemDetail.GhiChu = "";//dr["GHICHU"].ToString();
                    goodItem.GoodItemsCollection.Add(goodItemDetail);
                    k++;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void Send(string Status)
        {
            if (ShowMessage("Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (BCQT.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    BCQT = KDT_VNACCS_BaoCaoQuyetToan_TT39.Load(BCQT.ID);
                    BCQT.HopDongCollection = KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail.SelectCollectionBy_GoodItem_ID(BCQT.ID);
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
                    {
                        item.HangHoaCollection = KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail.SelectCollectionBy_Contract_ID(item.ID);
                    }
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                sendXML.master_id = BCQT.ID;

                if (sendXML.Load())
                {
                    if (Status == "Send")
                    {
                        if (goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                        {
                            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    BCQT.GuidStr = Guid.NewGuid().ToString();
                    GoodsItems_VNACCS goodsItem_VNACCS = new GoodsItems_VNACCS();
                    goodsItem_VNACCS = Mapper_V4.ToDataTransferGoodsItem_New(BCQT, GlobalSettings.DIA_CHI, GlobalSettings.TEN_DON_VI, Status, LIST_HOPDONG_ID);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = BCQT.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BCQT.MaHQ)),
                              Identity = BCQT.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = goodsItem_VNACCS.Issuer,
                              Reference = BCQT.GuidStr,
                          },
                          goodsItem_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        BCQT.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(BCQT.ID, MessageTitle.RegisterGoodItem);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                        sendXML.master_id = BCQT.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(goodsItem_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            BCQT.Update();
                            setCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            setCommandStatus();
                            Feedback();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            BCQT.Update();
                            setCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            ShowMessageTQDT(msgInfor, false);
                            Feedback();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(BCQT.ID, MessageTitle.RegisterGoodItem);
                        ShowMessageTQDT(msgInfor, false);
                        setCommandStatus();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
            sendXML.master_id = BCQT.ID;
            if (!sendXML.Load())
            {
                if (ShowMessageTQDT("Thông báo từ hệ thống", "Thông tin chưa được gửi đến hải quan hoặc Hệ thống HQ bị quá tải không nhận được phản hồi . Nếu trường hợp hệ thống HQ bị quá tải Bạn chọn Yes để tạo Message nhận phản hồi . Chọn No để bỏ qua", true) == "Yes")
                {
                    if (BCQT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET | BCQT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                    {
                        sendXML.msg = String.Empty;
                        sendXML.func = Convert.ToInt32(DeclarationFunction.HOI_TRANG_THAI);
                        sendXML.InsertUpdate();
                    }
                    else
                    {
                        sendXML.msg = String.Empty;
                        sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                        sendXML.InsertUpdate();
                    }
                }
                else
                {
                    return;
                }
            }
            while (isFeedBack)
            {
                string reference = BCQT.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.GoodsItemNew,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.GoodsItemNew,
                };
                subjectBase.Type = DeclarationIssuer.GoodsItemNew;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = BCQT.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BCQT.MaHQ.Trim())),
                                                  Identity = BCQT.MaHQ
                                              }, subjectBase, null);
                if (BCQT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(BCQT.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                while (isFeedBack)
                {
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    if (isFeedBack)
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            BCQT.Update();
                            isFeedBack = true;
                            setCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            BCQT.Update();
                            isFeedBack = false;
                            setCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            BCQT.Update();
                            isFeedBack = false;
                            setCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.GoodItemSendHandlerNew(BCQT, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = BCQT.ID;
                form.DeclarationIssuer = DeclarationIssuer.GoodsItemNew;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        //private DataTable OptionProcessNPL()
        //{
        //    try
        //    {
        //        DataSet ds = new DataSet();
        //        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        DataTable dtNPL = new DataTable();
        //        bool MaNPL = (bool)cbbMa.SelectedValue;
        //        bool TenNPL = (bool)cbbTenNPL.SelectedValue;
        //        string SQLGroup = "";
        //        string SQLSelect = "";
        //        if (MaNPL)
        //        {
        //            SQLGroup += " MANPL,";
        //            if (TenNPL)
        //            {
        //                SQLGroup += " TENNPL,";
        //            }
        //        }
        //        else
        //        {
        //            if (TenNPL)
        //            {
        //                SQLGroup += " TENNPL,";
        //            }
        //        }
        //        if (SQLGroup.Length > 0)
        //        {
        //            SQLGroup = SQLGroup.Substring(0, SQLGroup.Length - 1);
        //            //try
        //            //{
        //            //    DbCommand cmd = db.GetStoredProcCommand("p_T_GC_NPL_QUYETOAN_SelectGroupByNPL");
        //            //    db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
        //            //    db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
        //            //    db.AddInParameter(cmd, "@VALUEGROUP", DbType.String, SQLGroup);
        //            //    cmd.CommandTimeout = 600;
        //            //    ds = db.ExecuteDataSet(cmd);
        //            //    dtNPL = ds.Tables[0];
        //            //}
        //            //catch (Exception ex)
        //            //{
        //            //    StreamWriter write = File.AppendText("Error.txt");
        //            //    write.WriteLine("--------------------------------");
        //            //    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
        //            //    write.WriteLine(ex.StackTrace);
        //            //    write.WriteLine("Lỗi là : ");
        //            //    write.WriteLine(ex.Message);
        //            //    write.WriteLine("Lỗi tại Procedure : p_T_GC_NPL_QUYETOAN_SelectGroupByNPL");
        //            //    write.WriteLine("--------------------------------");
        //            //    write.Flush();
        //            //    write.Close();
        //            //    Logger.LocalLogger.Instance().WriteMessage(ex);
        //            //}
        //            SQLSelect += "SELECT " + SQLGroup + ", SUM(LUONGTONDK) AS LUONGTONDK,SUM(TRIGIATONDK) AS TRIGIATONDK,SUM(LUONGNHAPTK) AS LUONGNHAPTK,SUM(TRIGIANHAPTK) AS TRIGIANHAPTK,SUM(LUONGXUATTK) AS LUONGXUATTK,SUM(TRIGIAXUATTK) AS TRIGIAXUATTK,SUM(LUONGTONCK) AS LUONGTONCK,SUM(TRIGIATONCK) AS TRIGIATONCK FROM dbo.T_GC_NPL_QUYETOAN";
        //            SQLSelect += " WHERE HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + " GROUP BY " + SQLGroup;
        //        }
        //        else
        //        {
        //            try
        //            {
        //                DbCommand cmd = db.GetStoredProcCommand("p_T_GC_NPL_QUYETOAN_SelectGroup");
        //                db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
        //                db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
        //                cmd.CommandTimeout = 60;
        //                ds = db.ExecuteDataSet(cmd);
        //                dtNPL = ds.Tables[0];
        //            }
        //            catch (Exception ex)
        //            {
        //                StreamWriter write = File.AppendText("Error.txt");
        //                write.WriteLine("--------------------------------");
        //                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
        //                write.WriteLine(ex.StackTrace);
        //                write.WriteLine("Lỗi là : ");
        //                write.WriteLine(ex.Message);
        //                write.WriteLine("Lỗi tại Procedure : p_T_GC_NPL_QUYETOAN_SelectGroup");
        //                write.WriteLine("--------------------------------");
        //                write.Flush();
        //                write.Close();
        //                Logger.LocalLogger.Instance().WriteMessage(ex);
        //            }
        //            //SQLSelect += "SELECT MANPL,TENNPL,DVT,HOPDONG_ID, SUM(LUONGTONDK) AS LUONGTONDK,SUM(TRIGIATONDK) AS TRIGIATONDK,SUM(LUONGNHAPTK) AS LUONGNHAPTK,SUM(TRIGIANHAPTK) AS TRIGIANHAPTK,SUM(LUONGXUATTK) AS LUONGXUATTK,SUM(TRIGIAXUATTK) AS TRIGIAXUATTK,SUM(LUONGTONCK) AS LUONGTONCK,SUM(TRIGIATONCK) AS TRIGIATONCK  FROM dbo.T_GC_NPL_QUYETOAN";
        //            //SQLSelect += " WHERE HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + " GROUP BY MANPL,TENNPL,DVT,HOPDONG_ID ";
        //        }
        //        try
        //        {
        //            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //            SqlCommand cmd = (SqlCommand)db.GetSqlStringCommand(SQLSelect);
        //            //DataSet ds = new DataSet();
        //            cmd.CommandTimeout = 60;
        //            ds = db.ExecuteDataSet(cmd);
        //            dtNPL = ds.Tables[0];
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //        }
        //        return dtNPL;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        
        //    }
        //}
        //private DataTable OptionProcessSP()
        //{
        //    DataSet ds = new DataSet();
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    DataTable dtSP = new DataTable();
        //    bool MaSP = (bool)cbbMaSP.SelectedValue;
        //    bool TenSP = (bool)cbbTenSP.SelectedValue;
        //    bool NhomSP = (bool)cbbNhomSP.SelectedValue;
        //    string SQLGroup = "";
        //    string SQLSelect = "";
        //    if (MaSP)
        //    {
        //        SQLGroup += " MASP,";
        //        if (TenSP)
        //        {
        //            SQLGroup += " TENSP,";
        //        }
        //    }
        //    else
        //    {
        //        if (TenSP)
        //        {
        //            SQLGroup += " TENSP,";
        //        }
        //    }
        //    if (SQLGroup.Length > 0)
        //    {
        //        SQLGroup = SQLGroup.Substring(0, SQLGroup.Length - 1);
        //        //try
        //        //{
        //        //    DbCommand cmd = db.GetStoredProcCommand("p_T_GC_SANPHAM_QUYETOAN_SelectGroupBySP");
        //        //    db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
        //        //    db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
        //        //    db.AddInParameter(cmd, "@VALUEGROUP", DbType.String, SQLGroup);
        //        //    cmd.CommandTimeout = 60;
        //        //    ds = db.ExecuteDataSet(cmd);
        //        //    dtSP = ds.Tables[0];
        //        //}
        //        //catch (Exception ex)
        //        //{
        //        //    StreamWriter write = File.AppendText("Error.txt");
        //        //    write.WriteLine("--------------------------------");
        //        //    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
        //        //    write.WriteLine(ex.StackTrace);
        //        //    write.WriteLine("Lỗi là : ");
        //        //    write.WriteLine(ex.Message);
        //        //    write.WriteLine("Lỗi tại Procedure : p_T_GC_SANPHAM_QUYETOAN_SelectGroupBySP");
        //        //    write.WriteLine("--------------------------------");
        //        //    write.Flush();
        //        //    write.Close();
        //        //    Logger.LocalLogger.Instance().WriteMessage(ex);
        //        //}
        //        SQLSelect += " SELECT " + SQLGroup + ", SUM(LUONGTONDK) AS LUONGTONDK,SUM(TRIGIATONDK) AS TRIGIATONDK,SUM(LUONGNHAPTK) AS LUONGNHAPTK,SUM(TRIGIANHAPTK) AS TRIGIANHAPTK,SUM(LUONGXUATTK) AS LUONGXUATTK,SUM(TRIGIAXUATTK) AS TRIGIAXUATTK,SUM(LUONGTONCK) AS LUONGTONCK,SUM(TRIGIATONCK) AS TRIGIATONCK FROM dbo.T_GC_SANPHAM_QUYETOAN";
        //        SQLSelect += " WHERE HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + " GROUP BY " + SQLGroup;
        //    }
        //    else
        //    {
        //        try
        //        {
        //            DbCommand cmd = db.GetStoredProcCommand("p_T_GC_SANPHAM_QUYETOAN_SelectGroup");
        //            db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
        //            db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
        //            cmd.CommandTimeout = 60;
        //            ds = db.ExecuteDataSet(cmd);
        //            dtSP = ds.Tables[0];
        //        }
        //        catch (Exception ex)
        //        {
        //            StreamWriter write = File.AppendText("Error.txt");
        //            write.WriteLine("--------------------------------");
        //            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
        //            write.WriteLine(ex.StackTrace);
        //            write.WriteLine("Lỗi là : ");
        //            write.WriteLine(ex.Message);
        //            write.WriteLine("Lỗi tại Procedure : p_T_GC_SANPHAM_QUYETOAN_SelectGroup");
        //            write.WriteLine("--------------------------------");
        //            write.Flush();
        //            write.Close();
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //            
        //        }
        //        //SQLSelect += " SELECT MASP,TENSP,DVT,HOPDONG_ID, SUM(LUONGTONDK) AS LUONGTONDK,SUM(TRIGIATONDK) AS TRIGIATONDK,SUM(LUONGNHAPTK) AS LUONGNHAPTK,SUM(TRIGIANHAPTK) AS TRIGIANHAPTK,SUM(LUONGXUATTK) AS LUONGXUATTK,SUM(TRIGIAXUATTK) AS TRIGIAXUATTK,SUM(LUONGTONCK) AS LUONGTONCK,SUM(TRIGIATONCK) AS TRIGIATONCK  FROM dbo.T_GC_SANPHAM_QUYETOAN  ";
        //        //SQLSelect += " WHERE HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + " GROUP BY MASP,TENSP,DVT,HOPDONG_ID ";
        //    }
        //    if (NhomSP)
        //    {
        //        try
        //        {
        //            DbCommand cmd = db.GetStoredProcCommand("p_T_GC_SANPHAM_QUYETOAN_SelectGroupByNhomSP");
        //            db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
        //            db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
        //            cmd.CommandTimeout = 60;
        //            ds = db.ExecuteDataSet(cmd);
        //            dtSP = ds.Tables[0];
        //        }
        //        catch (Exception ex)
        //        {
        //            StreamWriter write = File.AppendText("Error.txt");
        //            write.WriteLine("--------------------------------");
        //            write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
        //            write.WriteLine(ex.StackTrace);
        //            write.WriteLine("Lỗi là : ");
        //            write.WriteLine(ex.Message);
        //            write.WriteLine("Lỗi tại Procedure : p_T_GC_SANPHAM_QUYETOAN_SelectGroupByNhomSP");
        //            write.WriteLine("--------------------------------");
        //            write.Flush();
        //            write.Close();
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //            
        //        }
        //        //SQLSelect = "";
        //        //SQLSelect += "";
        //    }
        //    try
        //    {
        //        //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //        SqlCommand cmd = (SqlCommand)db.GetSqlStringCommand(SQLSelect);
        //        //DataSet ds = new DataSet();
        //        cmd.CommandTimeout = 60;
        //        ds = db.ExecuteDataSet(cmd);
        //        dtSP = ds.Tables[0];
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        
        //    }
        //    return dtSP;
        //}
        private void btnProcess_Click(object sender, EventArgs e)
        {
            //GetDataGoodItemNew();
            if (BCQT.HopDongCollection.Count == 0)
            {
                ShowMessage("Doanh nghiệp chưa chọn Hợp đồng để đưa vào Báo cáo quyết toán.", false);
                return;
            }
            GetDataBCQTTT39();
            BindDataNPLQuyetToan();
            BindDataSPQuyetToan();
            BindDataTotal();
            this.SetChange(true);
        }

        private void btnPrintReportNumber_Click(object sender, EventArgs e)
        {
            if (LoaiHangHoa == "NPL")
            {
                Report_15_BCQT_NVL_GSQL report = new Report_15_BCQT_NVL_GSQL();
                report.BindReport(BCQT);

                report.ShowPreview();
            }
            else
            {
                Report_15a_BCQT_SP_GSQL report = new Report_15a_BCQT_SP_GSQL();
                report.BindReport(BCQT);
                report.ShowPreview();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgListNPL_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                decimal HOPDONG_ID = Convert.ToDecimal(e.Row.Cells["HOPDONG_ID"].Value.ToString());
                e.Row.Cells["HOPDONG_ID"].Text = Company.GC.BLL.KDT.GC.HopDong.GetNameHDRent(HOPDONG_ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListDataSPQT_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                decimal HOPDONG_ID = Convert.ToDecimal(e.Row.Cells["HOPDONG_ID"].Value.ToString());
                e.Row.Cells["HOPDONG_ID"].Text = Company.GC.BLL.KDT.GC.HopDong.GetNameHDRent(HOPDONG_ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgListTotal_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                Int64 Contract_ID = Convert.ToInt64(e.Row.Cells["Contract_ID"].Value.ToString());
                if (Contract_ID > 0)
                {
                    KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HD = new KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail();
                    HD = KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail.Load(Contract_ID);
                    e.Row.Cells["Contract_ID"].Text = Company.GC.BLL.KDT.GC.HopDong.GetNameHDRent(HD.ID_HopDong);
                }
                else
                {
                    //foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
                    //{
                    //    e.Row.Cells["Contract_ID"].Text = Company.GC.BLL.KDT.GC.HopDong.GetNameHDRent(item.ID_HopDong);
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedTab.Name)
            {
                case "tpNPLQT":
                    ExportExcel(dgListNPL);
                    break;
                case "tpSPQT":
                    ExportExcel(dgListDataSPQT);
                    break;
                case "tpTotal":
                    ExportExcel(dgListTotal);
                    break;
                default:
                    break;
            }
        }

        private void ExportExcel(GridEX dgList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = tabControl1.SelectedTab.Text + " Từ ngày_" + clcNgayBatDauBC.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " Đến ngày _" + clcNgayKetThucBC.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLoaiHinhBaoCao_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbbLoaiHinhBaoCao.SelectedValue.ToString() == "1")
            {
                LoaiHangHoa = "NPL";
            }
            else
            {
                LoaiHangHoa = "SP";
            }
        }

        private void clcNgayBatDauBC_TextChanged(object sender, EventArgs e)
        {
            NAMQUYETTOAN = clcNgayBatDauBC.Value.Year;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(Application.StartupPath + "\\ExcelTemplate\\Mau 15, 15a, 15b, 15c - BCQT.xlsx");
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=gLKRbHxsRPk");
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListTotal_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            Add();
        }

        private void BaoCaoQuyetToanHopDongForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Báo cáo quyết toán  có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.Save();
                }
            }
        }
    }
}
