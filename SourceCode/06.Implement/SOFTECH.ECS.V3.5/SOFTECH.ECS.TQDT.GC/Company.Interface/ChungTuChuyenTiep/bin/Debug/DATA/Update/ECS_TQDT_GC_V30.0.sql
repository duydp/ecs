IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39]
GO

CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_TT39
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT NOT NULL,
SoTN BIGINT,
NgayTN DATETIME,
NgayBatDauBC DATETIME NOT NULL,
NgayKetThucBC DATETIME NOT NULL,
LoaiBC INT NOT NULL,
LoaiSua INT,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
GuidStr VARCHAR(MAX),
GhiChuKhac NVARCHAR(2000)
)
GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY,
GoodItem_ID BIGINT NOT NULL FOREIGN KEY REFERENCES t_KDT_VNACCS_BaoCaoQuyetToan_TT39(ID),
STT NUMERIC(5) NOT NULL ,
ID_HopDong BIGINT  NULL,
SoHopDong NVARCHAR(80) NOT NULL,
NgayHopDong DATETIME NOT NULL,
MaHQ NVARCHAR(6) NOT NULL ,
NgayHetHan DATETIME NOT NULL,
GhiChuKhac NVARCHAR(2000)
)
GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
Contract_ID BIGINT NOT NULL FOREIGN KEY REFERENCES t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details(ID),
STT NUMERIC(5) NOT NULL ,
TenHangHoa NVARCHAR(255) NOT NULL,
MaHangHoa NVARCHAR(50) NOT NULL,
DVT NVARCHAR(4) NOT NULL,
LuongTonDauKy NUMERIC(18,4) NOT NULL,
LuongNhapTrongKy NUMERIC(18,4) NOT NULL,
LuongTaiXuat NUMERIC(18,4) NOT NULL ,
LuongChuyenMucDichSD NUMERIC(18,4) NOT NULL ,
LuongXuatKhau NUMERIC(18,4) NOT NULL ,
LuongXuatKhac NUMERIC(18,4) NOT NULL ,
LuongTonCuoiKy NUMERIC(18,4) NOT NULL ,
GhiChu NVARCHAR(2000),
)

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetThucBC datetime,
	@LoaiBC int,
	@LoaiSua int,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@GuidStr varchar(max),
	@GhiChuKhac nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetThucBC],
	[LoaiBC],
	[LoaiSua],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[GhiChuKhac]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@NgayBatDauBC,
	@NgayKetThucBC,
	@LoaiBC,
	@LoaiSua,
	@MaHQ,
	@MaDoanhNghiep,
	@GuidStr,
	@GhiChuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetThucBC datetime,
	@LoaiBC int,
	@LoaiSua int,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@GuidStr varchar(max),
	@GhiChuKhac nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[NgayBatDauBC] = @NgayBatDauBC,
	[NgayKetThucBC] = @NgayKetThucBC,
	[LoaiBC] = @LoaiBC,
	[LoaiSua] = @LoaiSua,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GuidStr] = @GuidStr,
	[GhiChuKhac] = @GhiChuKhac
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetThucBC datetime,
	@LoaiBC int,
	@LoaiSua int,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@GuidStr varchar(max),
	@GhiChuKhac nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[NgayBatDauBC] = @NgayBatDauBC,
			[NgayKetThucBC] = @NgayKetThucBC,
			[LoaiBC] = @LoaiBC,
			[LoaiSua] = @LoaiSua,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GuidStr] = @GuidStr,
			[GhiChuKhac] = @GhiChuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[NgayBatDauBC],
			[NgayKetThucBC],
			[LoaiBC],
			[LoaiSua],
			[MaHQ],
			[MaDoanhNghiep],
			[GuidStr],
			[GhiChuKhac]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@NgayBatDauBC,
			@NgayKetThucBC,
			@LoaiBC,
			@LoaiSua,
			@MaHQ,
			@MaDoanhNghiep,
			@GuidStr,
			@GhiChuKhac
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetThucBC],
	[LoaiBC],
	[LoaiSua],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetThucBC],
	[LoaiBC],
	[LoaiSua],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[GhiChuKhac]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetThucBC],
	[LoaiBC],
	[LoaiSua],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectBy_Contract_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectBy_Contract_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteBy_Contract_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteBy_Contract_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Insert]
	@Contract_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@LuongTonDauKy numeric(18, 4),
	@LuongNhapTrongKy numeric(18, 4),
	@LuongTaiXuat numeric(18, 4),
	@LuongChuyenMucDichSD numeric(18, 4),
	@LuongXuatKhau numeric(18, 4),
	@LuongXuatKhac numeric(18, 4),
	@LuongTonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]
(
	[Contract_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongTonDauKy],
	[LuongNhapTrongKy],
	[LuongTaiXuat],
	[LuongChuyenMucDichSD],
	[LuongXuatKhau],
	[LuongXuatKhac],
	[LuongTonCuoiKy],
	[GhiChu]
)
VALUES 
(
	@Contract_ID,
	@STT,
	@TenHangHoa,
	@MaHangHoa,
	@DVT,
	@LuongTonDauKy,
	@LuongNhapTrongKy,
	@LuongTaiXuat,
	@LuongChuyenMucDichSD,
	@LuongXuatKhau,
	@LuongXuatKhac,
	@LuongTonCuoiKy,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Update]
	@ID bigint,
	@Contract_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@LuongTonDauKy numeric(18, 4),
	@LuongNhapTrongKy numeric(18, 4),
	@LuongTaiXuat numeric(18, 4),
	@LuongChuyenMucDichSD numeric(18, 4),
	@LuongXuatKhau numeric(18, 4),
	@LuongXuatKhac numeric(18, 4),
	@LuongTonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]
SET
	[Contract_ID] = @Contract_ID,
	[STT] = @STT,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[DVT] = @DVT,
	[LuongTonDauKy] = @LuongTonDauKy,
	[LuongNhapTrongKy] = @LuongNhapTrongKy,
	[LuongTaiXuat] = @LuongTaiXuat,
	[LuongChuyenMucDichSD] = @LuongChuyenMucDichSD,
	[LuongXuatKhau] = @LuongXuatKhau,
	[LuongXuatKhac] = @LuongXuatKhac,
	[LuongTonCuoiKy] = @LuongTonCuoiKy,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_InsertUpdate]
	@ID bigint,
	@Contract_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@DVT nvarchar(4),
	@LuongTonDauKy numeric(18, 4),
	@LuongNhapTrongKy numeric(18, 4),
	@LuongTaiXuat numeric(18, 4),
	@LuongChuyenMucDichSD numeric(18, 4),
	@LuongXuatKhau numeric(18, 4),
	@LuongXuatKhac numeric(18, 4),
	@LuongTonCuoiKy numeric(18, 4),
	@GhiChu nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details] 
		SET
			[Contract_ID] = @Contract_ID,
			[STT] = @STT,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[DVT] = @DVT,
			[LuongTonDauKy] = @LuongTonDauKy,
			[LuongNhapTrongKy] = @LuongNhapTrongKy,
			[LuongTaiXuat] = @LuongTaiXuat,
			[LuongChuyenMucDichSD] = @LuongChuyenMucDichSD,
			[LuongXuatKhau] = @LuongXuatKhau,
			[LuongXuatKhac] = @LuongXuatKhac,
			[LuongTonCuoiKy] = @LuongTonCuoiKy,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]
		(
			[Contract_ID],
			[STT],
			[TenHangHoa],
			[MaHangHoa],
			[DVT],
			[LuongTonDauKy],
			[LuongNhapTrongKy],
			[LuongTaiXuat],
			[LuongChuyenMucDichSD],
			[LuongXuatKhau],
			[LuongXuatKhac],
			[LuongTonCuoiKy],
			[GhiChu]
		)
		VALUES 
		(
			@Contract_ID,
			@STT,
			@TenHangHoa,
			@MaHangHoa,
			@DVT,
			@LuongTonDauKy,
			@LuongNhapTrongKy,
			@LuongTaiXuat,
			@LuongChuyenMucDichSD,
			@LuongXuatKhau,
			@LuongXuatKhac,
			@LuongTonCuoiKy,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteBy_Contract_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteBy_Contract_ID]
	@Contract_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]
WHERE
	[Contract_ID] = @Contract_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Contract_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongTonDauKy],
	[LuongNhapTrongKy],
	[LuongTaiXuat],
	[LuongChuyenMucDichSD],
	[LuongXuatKhau],
	[LuongXuatKhac],
	[LuongTonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectBy_Contract_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectBy_Contract_ID]
	@Contract_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Contract_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongTonDauKy],
	[LuongNhapTrongKy],
	[LuongTaiXuat],
	[LuongChuyenMucDichSD],
	[LuongXuatKhau],
	[LuongXuatKhac],
	[LuongTonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]
WHERE
	[Contract_ID] = @Contract_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Contract_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongTonDauKy],
	[LuongNhapTrongKy],
	[LuongTaiXuat],
	[LuongChuyenMucDichSD],
	[LuongXuatKhau],
	[LuongXuatKhac],
	[LuongTonCuoiKy],
	[GhiChu]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Contract_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[DVT],
	[LuongTonDauKy],
	[LuongNhapTrongKy],
	[LuongTaiXuat],
	[LuongChuyenMucDichSD],
	[LuongXuatKhau],
	[LuongXuatKhac],
	[LuongTonCuoiKy],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectBy_GoodItem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectBy_GoodItem_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_DeleteBy_GoodItem_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_DeleteBy_GoodItem_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Insert]
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@ID_HopDong bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@GhiChuKhac nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]
(
	[GoodItem_ID],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
)
VALUES 
(
	@GoodItem_ID,
	@STT,
	@ID_HopDong,
	@SoHopDong,
	@NgayHopDong,
	@MaHQ,
	@NgayHetHan,
	@GhiChuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Update]
	@ID bigint,
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@ID_HopDong bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@GhiChuKhac nvarchar(2000)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]
SET
	[GoodItem_ID] = @GoodItem_ID,
	[STT] = @STT,
	[ID_HopDong] = @ID_HopDong,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[MaHQ] = @MaHQ,
	[NgayHetHan] = @NgayHetHan,
	[GhiChuKhac] = @GhiChuKhac
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_InsertUpdate]
	@ID bigint,
	@GoodItem_ID bigint,
	@STT numeric(5, 0),
	@ID_HopDong bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQ nvarchar(6),
	@NgayHetHan datetime,
	@GhiChuKhac nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details] 
		SET
			[GoodItem_ID] = @GoodItem_ID,
			[STT] = @STT,
			[ID_HopDong] = @ID_HopDong,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[MaHQ] = @MaHQ,
			[NgayHetHan] = @NgayHetHan,
			[GhiChuKhac] = @GhiChuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]
		(
			[GoodItem_ID],
			[STT],
			[ID_HopDong],
			[SoHopDong],
			[NgayHopDong],
			[MaHQ],
			[NgayHetHan],
			[GhiChuKhac]
		)
		VALUES 
		(
			@GoodItem_ID,
			@STT,
			@ID_HopDong,
			@SoHopDong,
			@NgayHopDong,
			@MaHQ,
			@NgayHetHan,
			@GhiChuKhac
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_DeleteBy_GoodItem_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_DeleteBy_GoodItem_ID]
	@GoodItem_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]
WHERE
	[GoodItem_ID] = @GoodItem_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectBy_GoodItem_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectBy_GoodItem_ID]
	@GoodItem_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]
WHERE
	[GoodItem_ID] = @GoodItem_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GoodItem_ID],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
FROM [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GoodItem_ID],
	[STT],
	[ID_HopDong],
	[SoHopDong],
	[NgayHopDong],
	[MaHQ],
	[NgayHetHan],
	[GhiChuKhac]
FROM
	[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Details]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.0',GETDATE(), N'CẬP NHẬT BÁO CÁO QUYẾT TOÁN')
END