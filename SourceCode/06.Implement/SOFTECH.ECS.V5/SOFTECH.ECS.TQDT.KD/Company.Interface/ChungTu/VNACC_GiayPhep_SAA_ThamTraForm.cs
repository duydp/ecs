﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class VNACC_GiayPhep_SAA_ThamTraForm : BaseFormHaveGuidPanel
    {
        private KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra TinhTrangThamTra = null;
        public KDT_VNACC_GiayPhep_SAA GiayPhep_SAA;
        private EGiayPhep GiayPhepType = EGiayPhep.SAA;
        private bool isAddNew = true;

        private DataTable dtHS = new DataTable();

        public VNACC_GiayPhep_SAA_ThamTraForm()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SAA.ToString());
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (isAddNew)
                {
                    TinhTrangThamTra = new KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra();

                    GetHangGiayPhep(TinhTrangThamTra);

                    GiayPhep_SAA.TinhTrangThamTraCollection.Add(TinhTrangThamTra);
                }
                else
                {
                    GetHangGiayPhep(TinhTrangThamTra);
                }

                grdHang.DataSource = GiayPhep_SAA.HangCollection;
                grdHang.Refetch();

                TinhTrangThamTra = new KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra();
                SetHangGiayPhep(TinhTrangThamTra);
                isAddNew = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHangGiayPhep(KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra item)
        {
            item.TinhTrangThamTra = ucTinhTrangThamTra.Code;
            item.PhuongThucXuLyCuoiCung = Convert.ToInt32(ucPhuongThucXuLyCuoiCung.Code);
            item.SoDonXinCapPhep = Convert.ToInt32(txtSoDonXinCapPhep.Text);
            item.MaNhanVienKiemTra = txtMaNhanVienKiemTra.Text;
            item.NgayKhaiBao = dtNgayKhaiBao.Value;
            item.MaNguoiKhai = txtMaNguoiKhai.Text;
            item.TenNguoiKhai = txtTenNguoiKhai.Text;
            item.NgayGiaoViec = dtNgayGiaoViec.Value;
            item.GioGiaoViec = dtGioGiaoViec.Value;
            item.NguoiGiaoViec = txtNguoiGiaoVIec.Text;
            item.NoiDungGiaoViec = txtNoiDungGiaoViec.Text;
        }

        private void SetHangGiayPhep(KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra item)
        {
            ucTinhTrangThamTra.Code = item.TinhTrangThamTra; //Thông tin tình trạng thẩm tra/kiểm tra
            ucPhuongThucXuLyCuoiCung.Code = item.PhuongThucXuLyCuoiCung.ToString(); //Phương thức xử lý cuối cùng
            txtSoDonXinCapPhep.Text = item.SoDonXinCapPhep.ToString(); //Số đơn xin cấp phép
            txtMaNhanVienKiemTra.Text = item.MaNhanVienKiemTra; //Mã nhân viên kiểm tra
            dtNgayKhaiBao.Value = item.NgayKhaiBao; //Ngày khai báo
            txtMaNguoiKhai.Text = item.MaNguoiKhai; //Mã người khai
            txtTenNguoiKhai.Text = item.TenNguoiKhai; //Tên người khai
            dtNgayGiaoViec.Value = item.NgayGiaoViec; //Ngày giao việc
            dtGioGiaoViec.Value = item.GioGiaoViec; //Giờ giao việc
            txtNguoiGiaoVIec.Text = item.NguoiGiaoViec; //Người giao việc
            txtNoiDungGiaoViec.Text = item.NoiDungGiaoViec; //Nội dung giao việc

        }

        private void VNACC_GiayPhep_SAA_ThamTraForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SetIDControl();

                grdHang.DataSource = GiayPhep_SAA.TinhTrangThamTraCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> hangColl = new List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();
                        GiayPhep_SAA.TinhTrangThamTraCollection.Remove(hmd);
                    }

                    grdHang.DataSource = GiayPhep_SAA.HangCollection;
                    try
                    {
                        grdHang.Refetch();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                // List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra> hangColl = new List<KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                TinhTrangThamTra = (KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra)items[0].GetRow().DataRow;
                SetHangGiayPhep(TinhTrangThamTra);
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaDonViCapPhep.Tag = "APP"; //Mã đơn vị cấp phép
                txtTinhTrangThamTraTimKiem.Tag = "STS"; //Thông tin tình trạng thẩm tra/kiểm tra
                txtSoDonXinCapPhep.Tag = "APN"; //Số đơn xin cấp phép/ Số giấy phép
                ucPhanLoaiTraCuu.Tag = "KBN"; //Phân loại tra cứu
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
    }
}
