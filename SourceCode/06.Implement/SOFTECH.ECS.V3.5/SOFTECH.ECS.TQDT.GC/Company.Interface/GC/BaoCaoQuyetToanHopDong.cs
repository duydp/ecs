﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.GC.BLL.GC;
using Company.Interface.Report.GC.ThongTu38;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
#endif
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.GC.BLL.KDT.GC;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Data.Common;
#endif
namespace Company.Interface.GC
{
    public partial class BaoCaoQuyetToanHopDong : BaseForm
    {
        public HopDong HD;
        public String MaNPL;
        public String MaSP;
        public String TenSP;
        public decimal LUONGTONDK = 0;
        public decimal TRIGIATONDK = 0;
        public decimal LUONGNHAPTK = 0;
        public decimal TRIGIANHAPTK = 0;
        public decimal LUONGXUATTK = 0;
        public decimal TRIGIAXUATTK = 0;
        public decimal LUONGTONCK = 0;
        public decimal TRIGIATONCK = 0;
        public HoSoQuyetToan_DSHopDong DSHD = new HoSoQuyetToan_DSHopDong();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public string LIST_HOPDONG_ID;
        public int NAMQUYETTOAN;
        public System.IO.FileInfo fin;
        public System.IO.FileStream fs;
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public DataTable dtNPLTotal = new DataTable();
        public DataTable dtSPTotal = new DataTable();
        public bool IsActive = false;
        public bool IsReadExcel = false;
        public BaoCaoQuyetToanHopDong()
        {
            InitializeComponent();
        }
        private void BindDataNPLQuyetToan()
        {
            try
            {
                dgListNPL.Refresh();
                if (GlobalSettings.MA_DON_VI == "4000395355")
                {
                    dgListNPL.DataSource = T_GC_NPL_QUYETOAN.SelectDynamicNew("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MANPL").Tables[0];
                }
                else
                {
                    dgListNPL.DataSource = T_GC_NPL_QUYETOAN.SelectDynamic("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MANPL").Tables[0];
                }
                dgListNPL.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        private void BindDataSPQuyetToan()
        {
            try
            {
                dgListDataSPQT.Refresh();
                if (GlobalSettings.MA_DON_VI == "4000395355")
                {
                    dgListDataSPQT.DataSource = T_GC_SANPHAM_QUYETOAN.SelectDynamicNew("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MASP").Tables[0];
                }
                else
                {
                    dgListDataSPQT.DataSource = T_GC_SANPHAM_QUYETOAN.SelectDynamic("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MASP").Tables[0];
                }
                dgListDataSPQT.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        public void BindDataGoodItem()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItem.GoodItemsCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void btnPrintReportNumber_Click(object sender, EventArgs e)
        {
            try
            {
                BaoCaoQuyetToan_Mau15_TT38_Total report = new BaoCaoQuyetToan_Mau15_TT38_Total();
                report.LIST_HOPDONG_ID = LIST_HOPDONG_ID;
                report.HD = HD;
                report.goodItem = goodItem;
                report.dtNPLTotal = dtNPLTotal;
                report.dtSPTotal = dtSPTotal;
                report.NAMQUYETTOAN = NAMQUYETTOAN;
                report.isNumber = true;
                report.BindReport(true);
                report.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnPrintReportValue_Click(object sender, EventArgs e)
        {
            try
            {
                BaoCaoQuyetToan_Mau15_TT38_Total report = new BaoCaoQuyetToan_Mau15_TT38_Total();
                report.LIST_HOPDONG_ID = LIST_HOPDONG_ID;
                report.HD = HD;
                report.goodItem = goodItem;
                report.dtNPLTotal = dtNPLTotal;
                report.dtSPTotal = dtSPTotal;
                report.NAMQUYETTOAN = NAMQUYETTOAN;
                report.isNumber = false;
                report.BindReport(false);
                report.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BaoCaoQuyetToanHopDong_Load(object sender, EventArgs e)
        {
            try
            {
                if (GlobalSettings.MA_DON_VI == "4000395355")
                {
                    btnPrintReportValue.Visible = false;
                    uiGroupBox5.Visible = false;
                    uiGroupBox6.Visible = false;
                }
                else
                {
                    cbbMa.SelectedIndex = 1;
                    cbbTenNPL.SelectedIndex = 1;
                    cbbMaSP.SelectedIndex = 1;
                    cbbTenSP.SelectedIndex = 1;
                    cbbNhomSP.SelectedIndex = 1;
                }
                txtNamQT.Text = (DateTime.Now.Year - 1).ToString();
                cbbLoaiHinh.SelectedValue = 1;
                cbbDVT.SelectedValue = 2;
                if (IsActive == false)
                {
                    LIST_HOPDONG_ID = LIST_HOPDONG_ID.Substring(0, LIST_HOPDONG_ID.Length - 1);
                    BindDataNPLQuyetToan();
                    BindDataSPQuyetToan();
                }
                else
                {
                    SetGoodItem();
                    BindDataGoodItem();
                    DSHD = new HoSoQuyetToan_DSHopDong();
                    DSHD = HoSoQuyetToan_DSHopDong.Load(goodItem.ID);
                    LIST_HOPDONG_ID = DSHD.SoHopDong;
                    NAMQUYETTOAN = Convert.ToInt32(goodItem.NamQuyetToan);
                    BindDataNPLQuyetToan();
                    BindDataSPQuyetToan();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        public void SaveGoodItem()
        {
            try
            {
                DSHD = new HoSoQuyetToan_DSHopDong();
                DSHD.Master_ID = Convert.ToInt32(goodItem.ID);
                DSHD.NgayKy = goodItem.NgayTN;
                DSHD.SoHopDong = LIST_HOPDONG_ID;
                DSHD.InsertUpdate();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = tabControl1.SelectedTab.Text + "_" + LIST_HOPDONG_ID + "_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedTab.Name)
            {
                case "tpNPLQT":
                    ExportExcel(dgListNPL);
                    break;
                case "tpSPQT":
                    ExportExcel(dgListDataSPQT);
                    break;
                case "tpTotal":
                    ExportExcel(dgListTotal);
                    break;
                default:
                    break;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtNamQT, errorProvider, "Năm quyết toán", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHinh, errorProvider, "Loại hình báo cáo", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbDVT, errorProvider, "Đơn vị tính báo cáo", isOnlyWarning);    
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend" :
                    Send("Send");
                    break;
                case "cmdSendEdit":
                    Send("Edit");
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                default:
                    break;
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP", "", Convert.ToInt32(goodItem.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void Save()
        {
            try
            {
                if (String.IsNullOrEmpty(lblFileName.Text))
                {
                    ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                    return;
                }
                if (!ValidateForm(false))
                    return;
                if (!IsReadExcel)
                {
                    if (dtNPLTotal.Rows.Count < 1 && dtSPTotal.Rows.Count < 1)
                    {
                        ShowMessage("Bạn chưa xử lý báo cáo ", false);
                        return;
                    }
                }
                GetGoodItem();
                if (goodItem != null)
                {
                    goodItemDetail.DeleteBy_GoodItem_ID(goodItem.ID);
                }
                goodItem.InsertUpdateFull();
                SaveGoodItem();
                BindDataGoodItem();
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ShowMessage("Lưu thành công", false);        
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void GetGoodItem()
        {
            try
            {
                goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                goodItem.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                goodItem.MaHQ = GlobalSettings.MA_HAI_QUAN_VNACCS;
                goodItem.NamQuyetToan = Convert.ToDecimal(txtNamQT.Text);
                goodItem.LoaiHinhBaoCao = Convert.ToDecimal(cbbLoaiHinh.SelectedValue.ToString());
                goodItem.DVTBaoCao = Convert.ToDecimal(cbbDVT.SelectedValue.ToString());
                goodItem.GhiChuKhac = txtGhiChu.Text.ToString();
                if (!IsReadExcel)
                {
                    GetDataGoodItem();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void SetGoodItem()
        {
            try
            {
                string TrangThai = goodItem.TrangThaiXuLy.ToString();
                switch (TrangThai)
                {
                    case "0":
                        lblTrangThai.Text = "Chờ duyệt";
                        break;
                    case "-1":
                        lblTrangThai.Text = "Chưa khai báo";
                        break;
                    case "1":
                        lblTrangThai.Text = "Đã duyệt";
                        break;
                    case "2":
                        lblTrangThai.Text = "Không phê duyệt";
                        break;

                }
                txtSoTiepNhan.Text = goodItem.SoTN.ToString();
                dateNgayTN.Value = goodItem.NgayTN;
                txtNamQT.Text = goodItem.NamQuyetToan.ToString();
                cbbDVT.SelectedValue = goodItem.DVTBaoCao.ToString();
                cbbLoaiHinh.SelectedValue = goodItem.LoaiHinhBaoCao.ToString();
                txtGhiChu.Text = goodItem.GhiChuKhac;
                lblFileName.Text = goodItem.FileName;
                goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void GetDataGoodItem()
        {
            try
            {
                //Nguyên phụ liệu
                goodItem.GoodItemsCollection.Clear();
                DataRow[] drNPL = new DataRow[dtNPLTotal.Rows.Count];
                dtNPLTotal.Rows.CopyTo(drNPL, 0);
                int k;
                for (k = 0; k < dtNPLTotal.Rows.Count; k++)
                {
                    goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
                    goodItemDetail.STT = k + 1;
                    goodItemDetail.LoaiHangHoa = 1;
                    goodItemDetail.TaiKhoan = 152;
                    goodItemDetail.TenHangHoa = drNPL[k]["TENNPL"].ToString();
                    goodItemDetail.MaHangHoa = drNPL[k]["MANPL"].ToString();
                    goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(drNPL[k]["DVT"].ToString()));
                    if (goodItem.DVTBaoCao == 1)
                    {
                        goodItemDetail.TonDauKy = Convert.ToDecimal(drNPL[k]["TRIGIATONDK"].ToString());
                        goodItemDetail.NhapTrongKy = Convert.ToDecimal(drNPL[k]["TRIGIANHAPTK"].ToString());
                        goodItemDetail.XuatTrongKy = Convert.ToDecimal(drNPL[k]["TRIGIAXUATTK"].ToString());
                        goodItemDetail.TonCuoiKy = Convert.ToDecimal(drNPL[k]["TRIGIATONCK"].ToString());
                    }
                    else
                    {
                        goodItemDetail.TonDauKy = Convert.ToDecimal(drNPL[k]["LUONGTONDK"].ToString());
                        goodItemDetail.NhapTrongKy = Convert.ToDecimal(drNPL[k]["LUONGNHAPTK"].ToString());
                        goodItemDetail.XuatTrongKy = Convert.ToDecimal(drNPL[k]["LUONGXUATTK"].ToString());
                        goodItemDetail.TonCuoiKy = Convert.ToDecimal(drNPL[k]["LUONGTONCK"].ToString());
                    }

                    goodItemDetail.GhiChu = "";//dr["GHICHU"].ToString();
                    goodItem.GoodItemsCollection.Add(goodItemDetail);
                }

                //Sản phẩm
                DataRow[] drSP = new DataRow[dtSPTotal.Rows.Count];
                dtSPTotal.Rows.CopyTo(drSP, 0);
                for (int i = 0; i < dtSPTotal.Rows.Count; i++)
                {
                    goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail();
                    goodItemDetail.STT = k + 1; ;
                    goodItemDetail.LoaiHangHoa = 2;
                    goodItemDetail.TaiKhoan = 155;
                    goodItemDetail.TenHangHoa = drSP[i]["TENSP"].ToString();
                    if (GlobalSettings.MA_DON_VI != "4000395355")
                    {
                        goodItemDetail.MaHangHoa = drSP[i]["MASP"].ToString();
                    }
                    else
                    {
                        goodItemDetail.MaHangHoa = String.Empty;
                    }
                    goodItemDetail.DVT = DVT_VNACC(DonViTinh_GetID(drSP[i]["DVT"].ToString()));
                    if (goodItem.DVTBaoCao == 1)
                    {
                        goodItemDetail.TonDauKy = Convert.ToDecimal(drSP[i]["TRIGIATONDK"].ToString());
                        goodItemDetail.NhapTrongKy = Convert.ToDecimal(drSP[i]["TRIGIANHAPTK"].ToString());
                        goodItemDetail.XuatTrongKy = Convert.ToDecimal(drSP[i]["TRIGIAXUATTK"].ToString());
                        goodItemDetail.TonCuoiKy = Convert.ToDecimal(drSP[i]["TRIGIATONCK"].ToString());
                    }
                    else
                    {
                        goodItemDetail.TonDauKy = Convert.ToDecimal(drSP[i]["LUONGTONDK"].ToString());
                        goodItemDetail.NhapTrongKy = Convert.ToDecimal(drSP[i]["LUONGNHAPTK"].ToString());
                        goodItemDetail.XuatTrongKy = Convert.ToDecimal(drSP[i]["LUONGXUATTK"].ToString());
                        goodItemDetail.TonCuoiKy = Convert.ToDecimal(drSP[i]["LUONGTONCK"].ToString());
                    }
                    goodItemDetail.GhiChu = "";//dr["GHICHU"].ToString();
                    goodItem.GoodItemsCollection.Add(goodItemDetail);
                    k++;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void Send(string Status)
        {
            if (ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (goodItem.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    goodItem = KDT_VNACCS_BaoCaoQuyetToan_NPLSP.Load(goodItem.ID);
                    goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionDynamic("GoodItem_ID = " + goodItem.ID, " ");
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                sendXML.master_id = goodItem.ID;

                if (sendXML.Load())
                {
                    if (Status == "Send")
                    {
                        if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    goodItem.GuidStr = Guid.NewGuid().ToString();
                    GoodsItems_VNACCS goodsItem_VNACCS = new GoodsItems_VNACCS();
                    goodsItem_VNACCS = Mapper_V4.ToDataTransferGoodsItem(goodItem, goodItemDetail, GlobalSettings.DIA_CHI, GlobalSettings.TEN_DON_VI,Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = goodItem.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ)),
                              Identity = goodItem.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = goodsItem_VNACCS.Issuer,
                              //Function = DeclarationFunction.KHAI_BAO,
                              Reference = goodItem.GuidStr,
                          },
                          goodsItem_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        goodItem.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;                    
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(goodItem.ID, MessageTitle.RegisterGoodItem);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.GoodItem;
                        sendXML.master_id = goodItem.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        goodItem.Update();
                        Feedback();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        public void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = goodItem.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.GoodsItemNew,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.GoodsItemNew,
                };
                subjectBase.Type = DeclarationIssuer.GoodsItem;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = goodItem.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ.Trim())),
                                                  Identity = goodItem.MaHQ
                                              }, subjectBase, null);
                if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(goodItem.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }        
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = goodItem.ID;
                form.DeclarationIssuer = DeclarationIssuer.GoodsItem;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private DataTable OptionProcessNPL()
        {
            try
            {
                DataSet ds = new DataSet();
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DataTable dtNPL = new DataTable();
                bool MaNPL = (bool)cbbMa.SelectedValue;
                bool TenNPL = (bool)cbbTenNPL.SelectedValue;
                string SQLGroup = "";
                string SQLSelect = "";
                if (MaNPL)
                {
                    SQLGroup += " MANPL,";
                    if (TenNPL)
                    {
                        SQLGroup += " TENNPL,";
                    }
                }
                else
                {
                    if (TenNPL)
                    {
                        SQLGroup += " TENNPL,";
                    }
                }
                if (SQLGroup.Length > 0)
                {
                    SQLGroup = SQLGroup.Substring(0, SQLGroup.Length - 1);
                    try
                    {
                        DbCommand cmd = db.GetStoredProcCommand("p_T_GC_NPL_QUYETOAN_SelectGroupByNPL");
                        db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
                        db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
                        db.AddInParameter(cmd, "@VALUEGROUP", DbType.String, SQLGroup);
                        cmd.CommandTimeout = 600;
                        ds = db.ExecuteDataSet(cmd);
                        dtNPL = ds.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("Lỗi tại Procedure : p_T_GC_NPL_QUYETOAN_SelectGroupByNPL");
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    //SQLSelect += "SELECT " + SQLGroup + ", SUM(LUONGTONDK) AS LUONGTONDK,SUM(TRIGIATONDK) AS TRIGIATONDK,SUM(LUONGNHAPTK) AS LUONGNHAPTK,SUM(TRIGIANHAPTK) AS TRIGIANHAPTK,SUM(LUONGXUATTK) AS LUONGXUATTK,SUM(TRIGIAXUATTK) AS TRIGIAXUATTK,SUM(LUONGTONCK) AS LUONGTONCK,SUM(TRIGIATONCK) AS TRIGIATONCK FROM dbo.T_GC_NPL_QUYETOAN";
                    //SQLSelect += " WHERE HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + " GROUP BY " + SQLGroup;
                }
                else
                {
                    //try
                    //{
                    //    DbCommand cmd = db.GetStoredProcCommand("p_T_GC_NPL_QUYETOAN_SelectGroup");
                    //    db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
                    //    db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
                    //    cmd.CommandTimeout = 60;
                    //    ds = db.ExecuteDataSet(cmd);
                    //    dtNPL = ds.Tables[0];
                    //}
                    //catch (Exception ex)
                    //{
                    //    StreamWriter write = File.AppendText("Error.txt");
                    //    write.WriteLine("--------------------------------");
                    //    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                    //    write.WriteLine(ex.StackTrace);
                    //    write.WriteLine("Lỗi là : ");
                    //    write.WriteLine(ex.Message);
                    //    write.WriteLine("Lỗi tại Procedure : p_T_GC_NPL_QUYETOAN_SelectGroup");
                    //    write.WriteLine("--------------------------------");
                    //    write.Flush();
                    //    write.Close();
                    //    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //}
                    SQLSelect += "SELECT MANPL,TENNPL,DVT,HOPDONG_ID, SUM(LUONGTONDK) AS LUONGTONDK,SUM(TRIGIATONDK) AS TRIGIATONDK,SUM(LUONGNHAPTK) AS LUONGNHAPTK,SUM(TRIGIANHAPTK) AS TRIGIANHAPTK,SUM(LUONGXUATTK) AS LUONGXUATTK,SUM(TRIGIAXUATTK) AS TRIGIAXUATTK,SUM(LUONGTONCK) AS LUONGTONCK,SUM(TRIGIATONCK) AS TRIGIATONCK  FROM dbo.T_GC_NPL_QUYETOAN";
                    SQLSelect += " WHERE HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + " GROUP BY MANPL,TENNPL,DVT,HOPDONG_ID ";
                }
                try
                {
                    //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                    SqlCommand cmd = (SqlCommand)db.GetSqlStringCommand(SQLSelect);
                    //DataSet ds = new DataSet();
                    cmd.CommandTimeout = 60;
                    ds = db.ExecuteDataSet(cmd);
                    dtNPL = ds.Tables[0];
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                return dtNPL;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                return null;
            }
        }
        private DataTable OptionProcessSP()
        {
            DataSet ds = new DataSet();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataTable dtSP = new DataTable();
            bool MaSP = (bool)cbbMaSP.SelectedValue;
            bool TenSP = (bool)cbbTenSP.SelectedValue;
            bool NhomSP = (bool)cbbNhomSP.SelectedValue;
            string SQLGroup = "";
            string SQLSelect = "";
            if (MaSP)
            {
                SQLGroup += " MASP,";
                if (TenSP)
                {
                    SQLGroup += " TENSP,";
                }
            }
            else
            {
                if (TenSP)
                {
                    SQLGroup += " TENSP,";
                }
            }
            if (SQLGroup.Length > 0)
            {
                SQLGroup = SQLGroup.Substring(0, SQLGroup.Length - 1);
                //try
                //{
                //    DbCommand cmd = db.GetStoredProcCommand("p_T_GC_SANPHAM_QUYETOAN_SelectGroupBySP");
                //    db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
                //    db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
                //    db.AddInParameter(cmd, "@VALUEGROUP", DbType.String, SQLGroup);
                //    cmd.CommandTimeout = 60;
                //    ds = db.ExecuteDataSet(cmd);
                //    dtSP = ds.Tables[0];
                //}
                //catch (Exception ex)
                //{
                //    StreamWriter write = File.AppendText("Error.txt");
                //    write.WriteLine("--------------------------------");
                //    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                //    write.WriteLine(ex.StackTrace);
                //    write.WriteLine("Lỗi là : ");
                //    write.WriteLine(ex.Message);
                //    write.WriteLine("Lỗi tại Procedure : p_T_GC_SANPHAM_QUYETOAN_SelectGroupBySP");
                //    write.WriteLine("--------------------------------");
                //    write.Flush();
                //    write.Close();
                //    Logger.LocalLogger.Instance().WriteMessage(ex);
                //}
                SQLSelect += " SELECT " + SQLGroup + ", SUM(LUONGTONDK) AS LUONGTONDK,SUM(TRIGIATONDK) AS TRIGIATONDK,SUM(LUONGNHAPTK) AS LUONGNHAPTK,SUM(TRIGIANHAPTK) AS TRIGIANHAPTK,SUM(LUONGXUATTK) AS LUONGXUATTK,SUM(TRIGIAXUATTK) AS TRIGIAXUATTK,SUM(LUONGTONCK) AS LUONGTONCK,SUM(TRIGIATONCK) AS TRIGIATONCK FROM dbo.T_GC_SANPHAM_QUYETOAN";
                SQLSelect += " WHERE HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + " GROUP BY " + SQLGroup;
            }
            else
            {
                //try
                //{
                //    DbCommand cmd = db.GetStoredProcCommand("p_T_GC_SANPHAM_QUYETOAN_SelectGroup");
                //    db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
                //    db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
                //    cmd.CommandTimeout = 60;
                //    ds = db.ExecuteDataSet(cmd);
                //    dtSP = ds.Tables[0];
                //}
                //catch (Exception ex)
                //{
                //    StreamWriter write = File.AppendText("Error.txt");
                //    write.WriteLine("--------------------------------");
                //    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                //    write.WriteLine(ex.StackTrace);
                //    write.WriteLine("Lỗi là : ");
                //    write.WriteLine(ex.Message);
                //    write.WriteLine("Lỗi tại Procedure : p_T_GC_SANPHAM_QUYETOAN_SelectGroup");
                //    write.WriteLine("--------------------------------");
                //    write.Flush();
                //    write.Close();
                //    Logger.LocalLogger.Instance().WriteMessage(ex);
                //    
                //}
                SQLSelect += " SELECT MASP,TENSP,DVT,HOPDONG_ID, SUM(LUONGTONDK) AS LUONGTONDK,SUM(TRIGIATONDK) AS TRIGIATONDK,SUM(LUONGNHAPTK) AS LUONGNHAPTK,SUM(TRIGIANHAPTK) AS TRIGIANHAPTK,SUM(LUONGXUATTK) AS LUONGXUATTK,SUM(TRIGIAXUATTK) AS TRIGIAXUATTK,SUM(LUONGTONCK) AS LUONGTONCK,SUM(TRIGIATONCK) AS TRIGIATONCK  FROM dbo.T_GC_SANPHAM_QUYETOAN  ";
                SQLSelect += " WHERE HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + " GROUP BY MASP,TENSP,DVT,HOPDONG_ID ";
            }
            if (NhomSP)
            {
                try
                {
                    DbCommand cmd = db.GetStoredProcCommand("p_T_GC_SANPHAM_QUYETOAN_SelectGroupByNhomSP");
                    db.AddInParameter(cmd, "@HOPDONG_ID", DbType.String, LIST_HOPDONG_ID);
                    db.AddInParameter(cmd, "@NAMQUYETTOAN", DbType.Int32, NAMQUYETTOAN);
                    cmd.CommandTimeout = 60;
                    ds = db.ExecuteDataSet(cmd);
                    dtSP = ds.Tables[0];
                }
                catch (Exception ex)
                {
                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("Lỗi tại Procedure : p_T_GC_SANPHAM_QUYETOAN_SelectGroupByNhomSP");
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
                //SQLSelect = "";
                //SQLSelect += "";
            }
            try
            {
                //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand cmd = (SqlCommand)db.GetSqlStringCommand(SQLSelect);
                //DataSet ds = new DataSet();
                cmd.CommandTimeout = 60;
                ds = db.ExecuteDataSet(cmd);
                dtSP = ds.Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            return dtSP;
        }
        private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        {
            //Return its own if the column names are empty
            if (columnNamesInDt == string.Empty || groupByColumnNames == string.Empty)
            {
                return dt;
            }

            //Once the columns are added find the distinct rows and group it bu the numbet
            DataTable _dt = dt.DefaultView.ToTable(true, groupByColumnNames);

            //The column names in data table
            string[] _columnNamesInDt = columnNamesInDt.Split(',');

            for (int i = 0; i < _columnNamesInDt.Length; i = i + 1)
            {
                if (_columnNamesInDt[i] != groupByColumnNames)
                {
                    _dt.Columns.Add(_columnNamesInDt[i]);
                }
            }

            //Gets the collection and send it back
            for (int i = 0; i < _dt.Rows.Count; i = i + 1)
            {
                for (int j = 0; j < _columnNamesInDt.Length; j = j + 1)
                {
                    if (_columnNamesInDt[j] != groupByColumnNames)
                    {
                        _dt.Rows[i][j] = dt.Compute(typeOfCalculation + "(" + _columnNamesInDt[j] + ")", groupByColumnNames + " = '" + _dt.Rows[i][groupByColumnNames].ToString() + "'");
                    }
                }
            }

            return _dt;
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            DataTable dtbNPL = Company.GC.BLL.GC.NguyenPhuLieu.SelectAll().Tables[0];
            DataTable dtbSP = Company.GC.BLL.GC.SanPham.SelectAll().Tables[0];
            DataTable dtNPL = new DataTable();
            DataTable dtSP = new DataTable();
            dtNPL = T_GC_NPL_QUYETOAN.SelectDynamic("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MANPL").Tables[0];
            dtSP = T_GC_SANPHAM_QUYETOAN.SelectDynamic("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MASP").Tables[0];
            dtNPLTotal = dtNPL.Clone();
            dtSPTotal = dtSP.Clone();
            DataTable dtGroupedBy = new DataTable();
            DataTable dtSPGroupedBy = new DataTable();
            if (GlobalSettings.MA_DON_VI != "4000395355")
            {
                dtGroupedBy = OptionProcessNPL();
                dtSPGroupedBy = OptionProcessSP();
            }
            else
            {
                 dtGroupedBy = GetGroupedBy(dtNPL, "MANPL,LUONGTONDK,TRIGIATONDK,LUONGNHAPTK,TRIGIANHAPTK,LUONGXUATTK,TRIGIAXUATTK,LUONGTONCK,TRIGIATONCK", "MANPL", "Sum");
                 dtSPGroupedBy = GetGroupedBy(dtSP, "MASP,LUONGTONDK,TRIGIATONDK,LUONGNHAPTK,TRIGIANHAPTK,LUONGXUATTK,TRIGIAXUATTK,LUONGTONCK,TRIGIATONCK", "MASP", "Sum");
            }
            try
            {
                foreach (DataRow dr in dtGroupedBy.Rows)
                {
                    try
                    {
                        DataRow drNew = dtNPLTotal.NewRow();
                        decimal HOPDONG_ID;
                        if (dtGroupedBy.Columns.Contains("MANPL"))
                        {
                            if (dtGroupedBy.Columns.Contains("HOPDONG_ID"))
                            {
                                HOPDONG_ID =Convert.ToDecimal(dr["HOPDONG_ID"].ToString());
                                MaNPL = dr["MANPL"].ToString();
                                drNew["MANPL"] = MaNPL;
                                DataRow[] drNPL = dtbNPL.Select("Ma='" + MaNPL + "' AND HopDong_ID = " + HOPDONG_ID);
                                drNew["TENNPL"] = drNPL[0]["Ten"].ToString();
                                drNew["DVT"] = drNPL[0]["TenDVT"].ToString();
                            }
                            else
                            {
                                MaNPL = dr["MANPL"].ToString();
                                drNew["MANPL"] = MaNPL;
                                DataRow[] drNPL = dtbNPL.Select("Ma='" + MaNPL + "' AND HopDong_ID IN (" + LIST_HOPDONG_ID +")");
                                drNew["TENNPL"] = drNPL[0]["Ten"].ToString();
                                drNew["DVT"] = drNPL[0]["TenDVT"].ToString();
                            }
                        }
                        else
                        {
                            string TENNPL;
                            TENNPL = dr["TENNPL"].ToString();
                            DataRow[] drNPL = dtbNPL.Select("Ten='" + TENNPL + "'");
                            drNew["MANPL"] = drNPL[0]["Ma"].ToString();
                            drNew["TENNPL"] = TENNPL;
                            drNew["DVT"] = drNPL[0]["TenDVT"].ToString();
                        }
                        if (String.IsNullOrEmpty(dr["LUONGTONDK"].ToString()))
                        {
                            LUONGTONDK = 0;
                            TRIGIATONDK = 0;
                        }
                        else
                        {
                            LUONGTONDK = Convert.ToDecimal(dr["LUONGTONDK"].ToString());
                            TRIGIATONDK = Convert.ToDecimal(dr["TRIGIATONDK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGNHAPTK"].ToString()))
                        {
                            LUONGNHAPTK = 0;
                            TRIGIANHAPTK = 0;
                        }
                        else
                        {
                            LUONGNHAPTK = Convert.ToDecimal(dr["LUONGNHAPTK"].ToString());
                            TRIGIANHAPTK = Convert.ToDecimal(dr["TRIGIANHAPTK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGXUATTK"].ToString()))
                        {
                            LUONGXUATTK = 0;
                            TRIGIAXUATTK = 0;
                        }
                        else
                        {
                            LUONGXUATTK = Convert.ToDecimal(dr["LUONGXUATTK"].ToString());
                            TRIGIAXUATTK = Convert.ToDecimal(dr["TRIGIAXUATTK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGTONCK"].ToString()))
                        {
                            LUONGTONCK = 0;
                            TRIGIATONCK = 0;
                        }
                        else
                        {
                            LUONGTONCK = Convert.ToDecimal(dr["LUONGTONCK"].ToString());
                            TRIGIATONCK = Convert.ToDecimal(dr["TRIGIATONCK"].ToString());
                        }
                        drNew["LUONGTONDK"] = LUONGTONDK;
                        drNew["TRIGIATONDK"] = TRIGIATONDK;
                        drNew["LUONGNHAPTK"] = LUONGNHAPTK;
                        drNew["TRIGIANHAPTK"] = TRIGIANHAPTK;
                        drNew["LUONGXUATTK"] = LUONGXUATTK;
                        drNew["TRIGIAXUATTK"] = TRIGIAXUATTK;
                        drNew["LUONGTONCK"] = LUONGTONCK;
                        drNew["TRIGIATONCK"] = TRIGIATONCK;
                        dtNPLTotal.Rows.Add(drNew);
                    }
                    catch (Exception ex)
                    {
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("Lỗi tại Mã NPL : " + MaNPL.ToString());
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        
                    }
                }
                foreach (DataRow dr in dtSPGroupedBy.Rows)
                {
                    try
                    {
                        DataRow drNew = dtSPTotal.NewRow();
                        decimal HOPDONG_ID;
                        MaSP = dr["MASP"].ToString();
                        if (GlobalSettings.MA_DON_VI != "4000395355")
                        {
                            if (dtSPGroupedBy.Columns.Contains("MASP"))
                            {
                                if (dtSPGroupedBy.Columns.Contains("HOPDONG_ID"))
                                {
                                    HOPDONG_ID =Convert.ToDecimal(dr["HOPDONG_ID"].ToString());
                                    DataRow[] drSP = dtbSP.Select(" Ma = '" + MaSP + "' AND HopDong_ID = " + HOPDONG_ID);
                                    MaSP = drSP[0]["Ma"].ToString();
                                    TenSP = drSP[0]["Ten"].ToString();
                                    drNew["MASP"] = MaSP;
                                    drNew["TENSP"] = TenSP;
                                    drNew["DVT"] = drSP[0]["TenDVT"].ToString();                                    
                                }
                                else
                                {
                                    MaSP = dr["MASP"].ToString();
                                    DataRow[] drSP = dtbSP.Select(" Ma = '" + MaSP + "' AND HopDong_ID IN ( " + LIST_HOPDONG_ID +")");
                                    //MaSP = drSP[0]["Ma"].ToString();
                                    TenSP = drSP[0]["Ten"].ToString().ToUpper();
                                    drNew["MASP"] = MaSP;
                                    drNew["TENSP"] = TenSP;
                                    drNew["DVT"] = drSP[0]["TenDVT"].ToString();
                                }

                            }
                            else if (dtSPGroupedBy.Columns.Contains("NHOMSP"))
                            {
                                drNew["TENSP"] = dr["NHOMSP"].ToString();
                                drNew["MASP"] = "";
                                drNew["DVT"] = "";
                            }
                            else 
                            {
                                string TENSP = dr["TENSP"].ToString();
                                DataRow[] drSP = dtbSP.Select(" Ten = '" + TENSP + "'");
                                MaSP = drSP[0]["Ma"].ToString();
                                //TenSP = drSP[0]["Ten"].ToString().ToUpper();
                                drNew["MASP"] = MaSP;
                                drNew["TENSP"] = TENSP;
                                drNew["DVT"] = drSP[0]["TenDVT"].ToString();
                            }
                        }
                        else
                        {
                            DataRow[] drSP = dtbSP.Select(" SUBSTRING(Ma,1,1)= '" + MaSP + "'");
                            MaSP = drSP[0]["Ma"].ToString();
                            TenSP = drSP[0]["Ten"].ToString().ToUpper();
                            //string[] Temp = TenSP.Split(new string[] { MaSP.Substring(1,MaSP.Length-1) }, StringSplitOptions.None);
                            string[] TempNew = TenSP.Split(new string[] { "GIÀY" }, StringSplitOptions.None);
                            if (TempNew.Length > 0)
                            {
                                //TenSP = Temp[0].ToString();
                                if (TenSP.Contains("GIÀY"))
                                {
                                    if (TempNew[0].ToString().ToUpper().Contains("NHÃN"))
                                    {
                                        TenSP = TempNew[0].Replace("NHÃN", "") + " GIÀY CÁC LOẠI";
                                    }
                                    else
                                    {
                                        TenSP = TempNew[0].ToString() + " GIÀY CÁC LOẠI";
                                    }
                                }
                                else
                                {
                                    if (TempNew[0].ToUpper().Contains("09 CM"))
                                    {
                                        TenSP = TempNew[0].ToUpper().ToString().Replace("09 CM", "") + " CÁC LOẠI";
                                    }
                                    else
                                    {
                                        TenSP = TempNew[0].ToString() + " CÁC LOẠI";
                                    }
                                }
                            }
                            drNew["MASP"] = String.Empty;
                            drNew["TENSP"] = TenSP;
                            drNew["DVT"] = drSP[0]["TenDVT"].ToString();
                        }
                        if (String.IsNullOrEmpty(dr["LUONGTONDK"].ToString()))
                        {
                            LUONGTONDK = 0;
                            TRIGIATONDK = 0;
                        }
                        else
                        {
                            LUONGTONDK = Convert.ToDecimal(dr["LUONGTONDK"].ToString());
                            TRIGIATONDK = Convert.ToDecimal(dr["TRIGIATONDK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGNHAPTK"].ToString()))
                        {
                            LUONGNHAPTK = 0;
                            TRIGIANHAPTK = 0;
                        }
                        else
                        {
                            LUONGNHAPTK = Convert.ToDecimal(dr["LUONGNHAPTK"].ToString());
                            TRIGIANHAPTK = Convert.ToDecimal(dr["TRIGIANHAPTK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGXUATTK"].ToString()))
                        {
                            LUONGXUATTK = 0;
                            TRIGIAXUATTK = 0;
                        }
                        else
                        {
                            LUONGXUATTK = Convert.ToDecimal(dr["LUONGXUATTK"].ToString());
                            TRIGIAXUATTK = Convert.ToDecimal(dr["TRIGIAXUATTK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGTONCK"].ToString()))
                        {
                            LUONGTONCK = 0;
                            TRIGIATONCK = 0;
                        }
                        else
                        {
                            LUONGTONCK = Convert.ToDecimal(dr["LUONGTONCK"].ToString());
                            TRIGIATONCK = Convert.ToDecimal(dr["TRIGIATONCK"].ToString());
                        }
                        drNew["LUONGTONDK"] = LUONGTONDK;
                        drNew["TRIGIATONDK"] = TRIGIATONDK;
                        drNew["LUONGNHAPTK"] = LUONGNHAPTK;
                        drNew["TRIGIANHAPTK"] = TRIGIANHAPTK;
                        drNew["LUONGXUATTK"] = LUONGXUATTK;
                        drNew["TRIGIAXUATTK"] = TRIGIAXUATTK;
                        drNew["LUONGTONCK"] = LUONGTONCK;
                        drNew["TRIGIATONCK"] = TRIGIATONCK;
                        dtSPTotal.Rows.Add(drNew);
                    }
                    catch (Exception ex)
                    {
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("Lỗi tại Mã SP: " + MaSP.ToString());
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        
                    }
                }
                for (int i = dtNPLTotal.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = dtNPLTotal.Rows[i];
                    if (Convert.ToDecimal(dr["LUONGTONDK"].ToString()) == 0 && Convert.ToDecimal(dr["LUONGNHAPTK"].ToString()) == 0 && Convert.ToDecimal(dr["LUONGXUATTK"].ToString()) == 0 && Convert.ToDecimal(dr["LUONGTONCK"]) == 0)
                        dr.Delete();
                }
                for (int i = dtSPTotal.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = dtSPTotal.Rows[i];
                    if (Convert.ToDecimal(dr["LUONGTONDK"].ToString()) == 0 && Convert.ToDecimal(dr["LUONGNHAPTK"].ToString()) == 0 && Convert.ToDecimal(dr["LUONGXUATTK"].ToString()) == 0 && Convert.ToDecimal(dr["LUONGTONCK"]) == 0)
                        dr.Delete();
                }
                GetDataGoodItem();
                BindDataGoodItem();
                ShowMessage("Xử lý thành công", false); 
            }
            catch (Exception ex)
            {
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.GoodItemSendHandler(goodItem, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog OpenFileDialog = new OpenFileDialog();

                OpenFileDialog.FileName = "";
                OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                        + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                        + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

                OpenFileDialog.Multiselect = false;
                if (OpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    System.IO.FileInfo fin = new System.IO.FileInfo(OpenFileDialog.FileName);
                    if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                      && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                      && fin.Extension.ToUpper() != ".gif".ToUpper()
                      && fin.Extension.ToUpper() != ".tiff".ToUpper()
                      && fin.Extension.ToUpper() != ".txt".ToUpper()
                      && fin.Extension.ToUpper() != ".xml".ToUpper()
                      && fin.Extension.ToUpper() != ".xsl".ToUpper()
                      && fin.Extension.ToUpper() != ".csv".ToUpper()
                      && fin.Extension.ToUpper() != ".doc".ToUpper()
                      && fin.Extension.ToUpper() != ".mdb".ToUpper()
                      && fin.Extension.ToUpper() != ".pdf".ToUpper()
                      && fin.Extension.ToUpper() != ".ppt".ToUpper()
                      && fin.Extension.ToUpper() != ".xls".ToUpper())
                    {
                        ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                    }
                    else
                    {
                        System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        size = 0;
                        size = fs.Length;
                        data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                        filebase64 = System.Convert.ToBase64String(data);
                        lblFileName.Text = fin.Name;
                        goodItem.FileName = fin.Name;
                        goodItem.Content = filebase64;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void ReadExcelFile_Click(object sender, EventArgs e)
        {
            try
            {
                goodItem.GoodItemsCollection.Clear();
                IsReadExcel = true;
                //Update New
                //ReadExcelFormNPL f = new ReadExcelFormNPL();
                ReadExcelFormNPLSP f = new ReadExcelFormNPLSP();
                f.goodItem = goodItem;
                f.ShowDialog(this);
                BindDataGoodItem();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgListDataSPQT_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                decimal HOPDONG_ID = Convert.ToDecimal(e.Row.Cells["HOPDONG_ID"].Value.ToString());
                e.Row.Cells["HOPDONG_ID"].Text = HopDong.GetNameHDRent(HOPDONG_ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgListNPL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                decimal HOPDONG_ID = Convert.ToDecimal(e.Row.Cells["HOPDONG_ID"].Value.ToString());
                e.Row.Cells["HOPDONG_ID"].Text = HopDong.GetNameHDRent(HOPDONG_ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string fileName = "";
                System.IO.FileStream fs;
                byte[] bytes;
                if (String.IsNullOrEmpty(goodItem.FileName))
                {
                    ShowMessage("Bạn chưa chọn File hoặc Không tồn tại File để xem.", false);
                    return;
                }
                fileName = path + "\\" + goodItem.FileName;
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                bytes = System.Convert.FromBase64String(goodItem.Content);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                System.Diagnostics.Process.Start(fileName);
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}