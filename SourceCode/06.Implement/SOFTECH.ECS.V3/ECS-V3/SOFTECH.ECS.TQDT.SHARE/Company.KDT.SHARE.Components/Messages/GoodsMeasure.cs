﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class GoodsMeasure
    {  
        /// <summary>
        ///  tỉ lệ chuyển đổi
        /// </summary>
        [XmlElement("conversionRate")]
        public string ConversionRate { get; set; }
        /// <summary>
        /// Số lượng(double) n..14,3
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }
        /// <summary>
        /// Khối lượng- trọng lượng n..14,3
        /// </summary>
        [XmlElement("grossMass")]
        public string GrossMass { get; set; }
        /// <summary>
        /// Mã đơn vị tính an..3
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set;}
        [XmlElement("tariff")]
        public string Tariff { get; set; }

        [XmlElement("registerMeasureUnit")]
        public string RegisterMeasureUnit { get; set; }

    }
}
