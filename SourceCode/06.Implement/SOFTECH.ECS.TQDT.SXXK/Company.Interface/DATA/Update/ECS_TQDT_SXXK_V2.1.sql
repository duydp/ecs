/*
Run this script on:

        ecsteam.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 04/24/2012 3:02:15 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViHaiQuan_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
	@ID char(6),
	@Ten nvarchar(50)
AS
INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
(
	[ID],
	[Ten]
)
VALUES
(
	@ID,
	@Ten
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViHaiQuan_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Update]
	@ID char(6),
	@Ten nvarchar(50)
AS

UPDATE
	[dbo].[t_HaiQuan_DonViHaiQuan]
SET
	[Ten] = @Ten
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
	@ID char(6),
	@Ten nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DonViHaiQuan] 
		SET
			[Ten] = @Ten
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
	(
			[ID],
			[Ten]
	)
	VALUES
	(
			@ID,
			@Ten
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViHaiQuan_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
	@ID char(6)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViHaiQuan_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Load]
	@ID char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_DinhMucOfSanPhamDK]'
GO
-- =============================================  
-- Author:  LanNT  
-- Create date:   
-- Description:   
-- =============================================  
ALTER PROCEDURE [dbo].[p_SXXK_DinhMucOfSanPhamDK]   
 -- Add the parameters for the stored procedure here  
  @MaSanPham VARCHAR(30),    
  @MaDoanhNghiep VARCHAR(14),    
  @MaHaiQuan CHAR(6)     
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
  SELECT     dm.MaNguyenPhuLieu, dvt.Ten AS DVT_ID, dm.DinhMucSuDung, dm.TyLeHaoHut, dm.DinhMucChung, npl.Ten AS TenNPL,  
  
  (CASE  WHEN (ISNULL(dm.IsFromVietNam, 0) = 0) THEN  N'Nhập khẩu' ELSE N'Mua tại VN' END) AS FromVietNam  
    
  FROM         t_SXXK_DinhMuc AS dm INNER JOIN  
         t_SXXK_NguyenPhuLieu AS npl ON dm.MaNguyenPhuLieu = npl.Ma AND dm.MaHaiQuan = npl.MaHaiQuan AND   
         dm.MaDoanhNghiep = npl.MaDoanhNghiep INNER JOIN  
         t_HaiQuan_DonViTinh AS dvt ON dvt.ID = npl.DVT_ID  
  WHERE     (dm.MaSanPham = @MaSanPham) AND (npl.MaDoanhNghiep = @MaDoanhNghiep) AND (npl.MaHaiQuan = @MaHaiQuan)  
  
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten]
FROM [dbo].[t_HaiQuan_DonViHaiQuan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='2.1', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('2.1', getdate(), null)
	end 