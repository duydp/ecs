-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_LePhiHQ_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LePhiHQ_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_LePhiHQ_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LePhiHQ_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_LePhiHQ_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LePhiHQ_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_LePhiHQ_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LePhiHQ_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_LePhiHQ_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LePhiHQ_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_LePhiHQ_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LePhiHQ_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_LePhiHQ_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LePhiHQ_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_LePhiHQ_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_LePhiHQ_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LePhiHQ_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LePhiHQ_Insert]
	@TKMD_ID bigint,
	@MaLePhi varchar(50),
	@SoTienLePhi decimal(18, 0),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_LePhiHQ]
(
	[TKMD_ID],
	[MaLePhi],
	[SoTienLePhi]
)
VALUES 
(
	@TKMD_ID,
	@MaLePhi,
	@SoTienLePhi
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LePhiHQ_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LePhiHQ_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaLePhi varchar(50),
	@SoTienLePhi decimal(18, 0)
AS

UPDATE
	[dbo].[t_KDT_LePhiHQ]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaLePhi] = @MaLePhi,
	[SoTienLePhi] = @SoTienLePhi
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LePhiHQ_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LePhiHQ_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaLePhi varchar(50),
	@SoTienLePhi decimal(18, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_LePhiHQ] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_LePhiHQ] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaLePhi] = @MaLePhi,
			[SoTienLePhi] = @SoTienLePhi
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_LePhiHQ]
		(
			[TKMD_ID],
			[MaLePhi],
			[SoTienLePhi]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaLePhi,
			@SoTienLePhi
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LePhiHQ_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LePhiHQ_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_LePhiHQ]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LePhiHQ_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LePhiHQ_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_LePhiHQ] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LePhiHQ_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LePhiHQ_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaLePhi],
	[SoTienLePhi]
FROM
	[dbo].[t_KDT_LePhiHQ]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LePhiHQ_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LePhiHQ_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaLePhi],
	[SoTienLePhi]
FROM [dbo].[t_KDT_LePhiHQ] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_LePhiHQ_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_LePhiHQ_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaLePhi],
	[SoTienLePhi]
FROM
	[dbo].[t_KDT_LePhiHQ]	

GO

