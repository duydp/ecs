﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.Controls;

namespace Company.Interface
{
    public partial class frmThongBaoLoi : Form
    {
       // private DataSet dataset;
        public frmThongBaoLoi()
        {
            InitializeComponent();
        }
        protected MessageBoxControl _MsgBox;
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void LoadData()
        {
            grdThongBao.DataSource = Company.KDT.SHARE.Components.ThongBaoLoi.SelectCollectionAll(); ;
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmThongBaoLoi_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmThongBaoLoi_ChiTiet frm = new frmThongBaoLoi_ChiTiet();
            frm.trangthai = 0;
            frm.ShowDialog();
            LoadData();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string ID = grdThongBao.CurrentRow.Cells["ID"].Text;
            frmThongBaoLoi_ChiTiet frm = new frmThongBaoLoi_ChiTiet();
            frm.trangthai = 1;
            frm.id = Convert.ToInt32(ID);
            frm.ShowDialog();
            LoadData();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(grdThongBao.CurrentRow.Cells["ID"].Text);
            if (ShowMessage("Bạn muốn xóa thông báo này không?", true) == "Yes")
            {
                Company.KDT.SHARE.Components.ThongBaoLoi.DeleteThongBaoLoi(id);
                LoadData();
            }

        }

        private void grdThongBao_DoubleClick(object sender, EventArgs e)
        {
            btnSua_Click(null, null);
        }
    }
}
