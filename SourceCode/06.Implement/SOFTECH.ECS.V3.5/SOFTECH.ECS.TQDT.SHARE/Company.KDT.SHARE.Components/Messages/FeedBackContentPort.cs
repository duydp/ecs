﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP;

namespace Company.KDT.SHARE.Components.Messages
{
    [XmlRoot("Declaration")]
   public class FeedBackContentPort
    {
        /// <summary>
        /// Loại chứng từ
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số tham chiếu đến chứng từ được phản hồi
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày trả lời
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Loại chức năng
        /// 1. Hủy
        /// 5. Sửa
        /// 8. Khai báo
        /// 12. Đang xử lý
        /// 13. Hỏi trạng thái
        /// 27. Từ chối
        /// 29. Cấp số tiếp nhận
        /// 30. Chấp nhận
        /// 31. Lỗi khi xử lý
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nơi trả lời
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Số tiếp nhận chứng từ
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

        /// <summary>
        /// Ngày chấp nhận đăng ký
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        /// <summary>
        /// Điểm thu phí tiếp nhận chứng từ
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }

        /// <summary>
        /// Đơn vị nhận thông báo
        /// </summary>
        [XmlElement("Agent")]
        public Agent Agent { get; set; }

        /// <summary>
        /// Đơn vị XNK
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }

        /// <summary>
        /// Thông tin Thông báo nộp phí (Hệ thống trả về cho DN sau khi tờ khai nộp phí đc chấp nhận)
        /// </summary>
        [XmlElement("CustomsGoodsItems")]
        public CustomsGoodsItems CustomsGoodsItems { get; set; }

        [XmlElement("FileAttachs")]
        public FileAttachs FileAttachs { get; set; }

        /// <summary>
        /// Thông tin Biên lai trả về từ hệ thống
        /// </summary>
        /// 
        [XmlElement("ReceiptOfPayments")]
        public ReceiptOfPayments ReceiptOfPayments { get; set; }


        /// <summary>
        /// Nội dung phản hồi khác
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }

        [XmlElement("MessageContent")]
        public string MessageContent { get; set; }
    }
}
