﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Infragistics.Excel;

using Company.BLL.KDT;

namespace Company.Interface.SXXK
{
    public partial class ToKhaiChuaCoTrongQuanLyTon : BaseForm
    {
        public DataSet ds = new DataSet();
        public ToKhaiChuaCoTrongQuanLyTon()
        {
            InitializeComponent();
        }

        //private void ToKhaiChuaCoTrongQuanLyTon_Load(object sender, EventArgs e)
        //{
        //    this.dgList.DataSource = ds.Tables[0];
        //    try
        //    {
        //        dgList.Refetch();
        //    }
        //    catch
        //    {
        //        dgList.Refresh();
        //    }
        //}

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            //if(e.Row.RowType == RowType.Record)    
            //    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            //Workbook wb = new Workbook();
            //Worksheet ws = wb.Worksheets.Add("SanPhamChuaCoDM");
            //ws.Columns[0].Width = 5000;
            //ws.Columns[1].Width = 5000;
            //ws.Columns[2].Width = 10000;
            //ws.Columns[3].Width = 5000;
            //ws.Columns[4].Width = 5000;
            //WorksheetRowCollection wsrc = ws.Rows;
            //WorksheetRow wsr0 = ws.Rows[0];
            //wsr0.Cells[0].Value = "Số Tờ Khai";
            //wsr0.Cells[1].Value = "Mã SP";
            //wsr0.Cells[2].Value = "Tên SP";
            //wsr0.Cells[3].Value = "Mã HS";
            //wsr0.Cells[4].Value = "ĐVT";
            //DataTable table=this.ds.Tables[0];
   
            //for (int i = 0; i < table.Rows.Count; i++)
            //{
            //    WorksheetRow wsr = ws.Rows[i + 1];
            //    wsr.Cells[0].Value = table.Rows[i]["SoToKhai"].ToString();
            //    wsr.Cells[1].Value = table.Rows[i]["Ma"].ToString();
            //    wsr.Cells[2].Value = table.Rows[i]["Ten"].ToString();
            //    wsr.Cells[3].Value = table.Rows[i]["MaHS"].ToString();
            //    wsr.Cells[4].Value =  this.DonViTinh_GetName(table.Rows[i]["DVT_ID"]); 
            //}
            //DialogResult rs= saveFileDialog1.ShowDialog(this);
            //if (rs == DialogResult.OK)
            //{
            //    string fileName = saveFileDialog1.FileName;
            //    wb.Save(fileName);
            //}
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            string error = "";
            if(ds.Tables[0].Rows.Count>0)
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];

                    ToKhaiMauDich tkSua = new ToKhaiMauDich();
                    tkSua.SoToKhai = dr["SoToKhai"] != DBNull.Value ? Convert.ToInt32(dr["SoToKhai"]) : 0;
                    tkSua.MaLoaiHinh = dr["MaLoaiHinh"] != DBNull.Value ? dr["MaLoaiHinh"].ToString() : "";
                    tkSua.NamDK = dr["NamDangKy"] != DBNull.Value ? Convert.ToInt32(dr["NamDangKy"]) : 0;
                    tkSua.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    tkSua.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    tkSua.Load(tkSua.MaHaiQuan, tkSua.MaDoanhNghiep, tkSua.SoToKhai, tkSua.NamDK, tkSua.MaLoaiHinh);

                    if (tkSua.ID > 0)
                    {
                        try
                        {
                            if (!tkSua.CapNhatThongTinHangToKhaiSua())
                            {
                                error = tkSua.SoToKhai + "/" + tkSua.NamDK + "|| ";
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(tkSua.SoToKhai.ToString() + "", ex);
                            //throw;
                        }
                    }
                }
            if (error != "")
            {
                MessageBox.Show(error, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }

        private void ToKhaiChuaCoTrongQuanLyTon_Load(object sender, EventArgs e)
        {
            this.dgList.DataSource = ds.Tables[0];
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        
    }
}