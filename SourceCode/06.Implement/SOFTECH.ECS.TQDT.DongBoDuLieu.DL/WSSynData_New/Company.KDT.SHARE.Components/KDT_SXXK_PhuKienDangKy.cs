﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Company.KDT.SHARE.Components
{
    public partial class KDT_SXXK_PhuKienDangKy
    {
        List<KDT_SXXK_LoaiPhuKien> _ListLoaiPK = new List<KDT_SXXK_LoaiPhuKien>();

        public List<KDT_SXXK_LoaiPhuKien> ListPhuKien
        {
            set { this._ListLoaiPK = value; }
            get { return this._ListLoaiPK; }
        }
        
        public void InsertPhuKienDongBoDuLieu()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.ID = this.InsertUpdate(transaction);
                    foreach (KDT_SXXK_LoaiPhuKien pk in ListPhuKien)
                    {
                        int stt = 1;
                        pk.Master_ID = this.ID;
                        long pkID = pk.InsertUpdate(transaction);
                        foreach (KDT_SXXK_PhuKienDetail hang in pk.ListPhuKienDeTail)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = pkID;
                            hang.InsertUpdate(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public static KDT_SXXK_PhuKienDangKy LoadFull(long id)
        {
            KDT_SXXK_PhuKienDangKy PKDK = new KDT_SXXK_PhuKienDangKy();
            PKDK = KDT_SXXK_PhuKienDangKy.Load(id);
            if (PKDK.ID == id)
            {
                List<KDT_SXXK_LoaiPhuKien> lisLoaiPK = new List<KDT_SXXK_LoaiPhuKien>();
                lisLoaiPK = KDT_SXXK_LoaiPhuKien.SelectCollectionDynamic("Master_ID = " + PKDK.ID, "");
                foreach (KDT_SXXK_LoaiPhuKien LoaiPK in lisLoaiPK)
                {
                    List<KDT_SXXK_PhuKienDetail> listPKDeTail = new List<KDT_SXXK_PhuKienDetail>();
                    listPKDeTail = KDT_SXXK_PhuKienDetail.SelectCollectionDynamic("Master_ID = " + LoaiPK.ID, "");
                    foreach (KDT_SXXK_PhuKienDetail item in listPKDeTail)
                    {
                        LoaiPK.ListPhuKienDeTail.Add(item);
                    }
                    PKDK.ListPhuKien.Add(LoaiPK);
                }
                return PKDK;
            }
            else
                return null;
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            ListPhuKien = KDT_SXXK_LoaiPhuKien.SelectCollectionDynamic("Master_id =" + this.ID, "");
            foreach (KDT_SXXK_LoaiPhuKien item in ListPhuKien)
            {
                item.DeleteFull(item.ID);
            }
            this.Delete();
        }
        public void TransferDataToTK()
        {
            
        }
    }
}
