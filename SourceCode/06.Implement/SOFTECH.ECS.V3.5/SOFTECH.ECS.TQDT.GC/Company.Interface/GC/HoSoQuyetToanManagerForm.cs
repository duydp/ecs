﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Infragistics.Excel;
using System.Collections.Generic;
using Company.Interface.Report.GC;
using Company.GC.BLL.GC;


namespace Company.Interface.GC
{
    public partial class HoSoQuyetToanManagerForm : BaseForm
    {
        public HopDong HopDongSelected = new HopDong();
        public bool IsBrowseForm = false;
        public bool IsBrowseFormCT = false;
        public HoSoQuyetToan HSQT = new HoSoQuyetToan();

        public HoSoQuyetToanManagerForm()
        {
            InitializeComponent();
        }
        //TODO: Cao Huu Tu updated 26-08-2011:tao VietNam HDcollection
        private List<HopDong> hdcollection = new List<HopDong>();


        public void BindData()
        {
            //hdcollection = Company.GC.BLL.Globals.GetDanhSachHopDongDaDangKy(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, txtSoHopDong.Text.Trim(), Convert.ToInt32(txtNamDangKy.Value));
            //dgList.DataSource = hdcollection;
            try
            {
                dgList.DataSource = HoSoQuyetToan.SelectDynamic(" NamQT=" + txtNamDangKy.Text, "ID").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }


        }

        private void HopDongRegistedForm_Load(object sender, EventArgs e)
        {
            //this.khoitao_DuLieuChuan();
            txtNamDangKy.Text = (DateTime.Now.Year - 1).ToString();
            BindData();
            //setDataToComboUserKB();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                int id = int.Parse(dgList.CurrentRow.Cells["ID"].Text);
                XuatNhapTonForm xnt = new XuatNhapTonForm();
                xnt.isUpdate = true;
                xnt.isDaQuyetToan = true;
                xnt.HS = HoSoQuyetToan.Load(id);
                xnt.ShowDialog(this);


            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnXoaHS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                int id = int.Parse(dgList.CurrentRow.Cells["ID"].Text);
                DialogResult ds = MessageBox.Show("Bạn có muốn xóa hồ sơ?","Thông báo",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
                if (ds == DialogResult.Yes) 
                {
                    HoSoQuyetToan_DSHopDong.DeleteDynamic("Master_ID = " + id);
                    HoSoQuyetToan.DeleteDynamic("ID = " + id);
                    MessageBox.Show("Xóa hồ sơ thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnSearch.PerformClick();
                } 
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            finally 
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
