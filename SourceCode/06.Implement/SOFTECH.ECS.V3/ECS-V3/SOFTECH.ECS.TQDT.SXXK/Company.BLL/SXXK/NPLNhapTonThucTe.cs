﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;

namespace Company.BLL.SXXK
{
    public partial class NPLNhapTonThucTe
    {
        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiep(string maHaiQuan, string MaDoanhNghiep)
        {
            const string sql = "select * from v_SXXK_NPLTon where MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan order by Ma";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            return db.ExecuteDataSet(dbCommand);
        }

        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndTonHonO(string maHaiQuan, string MaDoanhNghiep, DateTime NgayDangKy,int SoThapPhanNPL,int chenhLechNgay)
        {
            string spName = "p_SXXK_SelectNPLTonThucTeBy_MaHaiQuan_AND_MaDoanhNghiepAndNgayDangKyAndTonHonO";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);

            return db.ExecuteDataSet(dbCommand);
        }
        
        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndTonHonO(string maHaiQuan, string MaDoanhNghiep,DateTime NgayDangKy,int SoThapPhanNPL,int chenhLechNgay,SqlTransaction transaction)
        {
            string spName = "p_SXXK_SelectNPLTonThucTeBy_MaHaiQuan_AND_MaDoanhNghiepAndNgayDangKyAndTonHonO";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);

            return db.ExecuteDataSet(dbCommand,transaction);
        }

        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(string maHaiQuan, string MaDoanhNghiep, string MaNPL, DateTime NgayDangKy, int SoThapPhanNPL)
        {
            string spName = "p_SXXK_SelectNPLTonThucTeBy_MaHaiQuan_AND_MaDoanhNghiepAndNgayDangKyAndTonHonOAndMaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand);
        }
        
        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(string maHaiQuan, string MaDoanhNghiep, string MaNPL)
        {
            string sql = "select * from t_SXXK_NPLNhapTonThucTe where MaDoanhNghiep='" + MaDoanhNghiep + "' and MaHaiQuan='" + maHaiQuan + "' and Ton>0 and MaNPL=@MaNPL order by NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand);
        }

        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(string maHaiQuan, string MaDoanhNghiep, string MaNPL, DateTime NgayDangKy,int SoThapPhanNPL, SqlTransaction transaction)
        {
            string spName = "p_SXXK_SelectNPLTonThucTeBy_MaHaiQuan_AND_MaDoanhNghiepAndNgayDangKyAndTonHonOAndMaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand,transaction);
        }

        public static DataSet SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(string maHaiQuan, string MaDoanhNghiep, string MaNPL, SqlTransaction transaction)
        {
            string sql = "select * from t_SXXK_NPLNhapTonThucTe where MaDoanhNghiep='" + MaDoanhNghiep + "' and MaHaiQuan='" + maHaiQuan + "' and Ton>0 and ngaydangky<@ngaydangky and MaNPL=@MaNPL order by NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand, transaction);
        }

        public static NPLNhapTonThucTe Load(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL,SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_SXXK_NPLNhapTonThucTe_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namDangKy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, maNPL);
            NPLNhapTonThucTe entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand,transaction);
            if (reader.Read())
            {
                entity = new NPLNhapTonThucTe();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueCLGia"))) entity.ThueCLGia = reader.GetDouble(reader.GetOrdinal("ThueCLGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
            }
            reader.Close();
            return entity;
        }
        
        public static IList<NPLNhapTonThucTe> SelectCollectionBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(string maHaiQuan, string MaDoanhNghiep, string MaNPL,int SoThapPhanNPL, DateTime NgayDangKy)
        {
            string spName = "p_SXXK_SelectNPLTonThucTeBy_MaHaiQuan_AND_MaDoanhNghiepAndNgayDangKyAndTonHonOAndMaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            IList<NPLNhapTonThucTe> collection = new List<NPLNhapTonThucTe>();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLNhapTonThucTe entity = new NPLNhapTonThucTe();

                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueCLGia"))) entity.ThueCLGia = reader.GetDouble(reader.GetOrdinal("ThueCLGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        public static IList<NPLNhapTonThucTe> SelectCollectionBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(string maHaiQuan, string MaDoanhNghiep, string MaNPL, DateTime NgayDangKy, int SoThapPhanNPL,int chenhLechNgay, SqlTransaction transaction)
        {
            string spName = "p_SXXK_SelectNPLTonThucTeBy_MaHaiQuan_AND_MaDoanhNghiepAndNgayDangKyAndTonHonOAndMaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
            IList<NPLNhapTonThucTe> collection = new List<NPLNhapTonThucTe>();
            IDataReader reader = db.ExecuteReader(dbCommand,transaction);
            while (reader.Read())
            {
                NPLNhapTonThucTe entity = new NPLNhapTonThucTe();

                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueCLGia"))) entity.ThueCLGia = reader.GetDouble(reader.GetOrdinal("ThueCLGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_SXXK_NPLNhapTonThucTe_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, Luong);
            db.AddInParameter(dbCommand, "@Ton", SqlDbType.Decimal, Ton);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, ThueVAT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Float, PhuThu);
            db.AddInParameter(dbCommand, "@ThueCLGia", SqlDbType.Float, ThueCLGia);
            db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, ThueXNKTon);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year == 1753 ? DBNull.Value : (object)NgayDangKy);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


    }
}