﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using System.Data.OleDb;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.VNACCS;
using Infragistics.Excel;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucSendForm_NEW_HOATHO : BaseForm
    {
        public DinhMucDangKy dmDangKy = new DinhMucDangKy();// { TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO };
        //private DinhMucDangKy dmDangKy = new DinhMucDangKy();
        private string maSP = "";
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        //private bool isChuanHoa = false;
        //-----------------------------------------------------------------------------------------
        public DinhMucSendForm_NEW_HOATHO()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }
        private void LaySoTiepNhanDT()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "DM";
            sendXML.master_id = dmDangKy.ID;
            string password = "";
            if (!sendXML.Load())
            {
                MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_SEN03", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                {
                    xmlCurrent = dmDangKy.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmDangKy.SoTiepNhan, "MSG_SEN05", "" + dmDangKy.SoTiepNhan, false);

                    txtSoTiepNhan.Text = this.dmDangKy.SoTiepNhan.ToString();

                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    else
                        lblTrangThai.Text = "Wait to approve";
                }

                //xoa thông tin msg nay trong database
                //sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = dmDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                            {
                                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                dgList.AllowDelete = InheritableBoolean.True;
                                cmdImportExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                //sendXML.Delete();
                            }
                        }
                    }
                    else
                    {
                        MLMessages("LaySoTiepNhanDT(): Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách ĐỊNH MỨC. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = dmDangKy.LayPhanHoi(pass, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmDangKy.SoTiepNhan, "MSG_SEN05", "" + dmDangKy.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.dmDangKy.SoTiepNhan.ToString();
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    else
                        lblTrangThai.Text = "Wait to approve";
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = dmDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                            cmdImportExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            //sendXML.Delete();   
                        }
                    }
                    else
                    {
                        MLMessages("LayPhanHoi(): Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo ĐỊNH MỨC. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void DinhMucSendForm_Load(object sender, EventArgs e)
        {
            btnCheckDVT.Enabled = false;
            btnCapNhatDVT.Enabled = false;
            btnCheckNPLMuaVN.Enabled = false;
            btnChuanHoa.Enabled = false;
            btnKiemTraTK_GhiChu.Enabled = false;
            cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            this.khoitao_DuLieuChuan();
            //txtSoTiepNhan.Text = this.dmDangKy.SoTiepNhan.ToString();
            //lblTrangThai.Text = this.dmDangKy.TrangThaiXuLy.ToString();
            dgList.DataSource = this.dmDangKy.DMCollection;

            if (dmDangKy.ID > 0)
            {
                Company.BLL.KDT.SXXK.MsgSend msg = new Company.BLL.KDT.SXXK.MsgSend();
                msg.master_id = this.dmDangKy.ID;
                msg.LoaiHS = LoaiKhaiBao.DinhMuc;
                if (msg.Load())
                {
                    lblTrangThai.Text = setText("Chờ xác nhận ", "Wait for confirmation");
                    cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                }
            }
            else
            {
                //HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }

            this.cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
           // this.SetCommand();
            SetCommandNew();
            /*if (HD != null)
            {
                HD.ID = dmDangKy.ID_HopDong;
                HD = HopDong.Load(HD.ID);

                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                txtSoHopDong.Text = HD.SoHopDong;
                dmDangKy.LoadCollection();

            }
            txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
            dgList.DataSource = dmDangKy.DMCollection;
             */
        }
        private int CheckNPLVaSPDuyet()
        {
            foreach (DinhMuc dm in this.dmDangKy.DMCollection)
            {
                BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.Ma = dm.MaNguyenPhuLieu;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (!npl.Load())
                    return 0;
                BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = dm.MaSanPham;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (!sp.Load())
                    return 1;
            }
            return 2;
        }
        

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới định mức.
        /// </summary>
        private void add()
        {
            dmDangKy = new DinhMucDangKy();
            try
            {
                dgList.DataSource = this.dmDangKy.DMCollection;
                dgList.Refetch();
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnCheckDVT.Enabled = false;
                btnCapNhatDVT.Enabled = false;
                btnCheckNPLMuaVN.Enabled = false;
                btnChuanHoa.Enabled = false;
                btnKiemTraTK_GhiChu.Enabled = false;
            }
            catch { dgList.Refresh(); }

        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //try
            //{
            //    if (e.Row.RowType == RowType.Record)
            //    {
            //        DinhMucEditForm f = new DinhMucEditForm();
            //        f.DMDetail = (DinhMuc)e.Row.DataRow;
            //        f.dmDangKy = dmDangKy;
            //        //---------------
            //        if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            //            f.OpenType = OpenFormType.View;
            //        else
            //            f.OpenType = OpenFormType.Edit;
            //        f.ShowDialog();
            //        try
            //        {
            //            dgList.Refetch();
            //        }
            //        catch
            //        {
            //            dgList.Refresh();
            //        }

            //        //---------------

            //    }
            //}
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {
            DialogResult rs = MessageBox.Show("Bạn có muốn lưu thông tin?","Thông báo",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (rs == DialogResult.Yes) 
            {
                try
                {
                    if (CheckNPLExist() == false)
                        return;
                    DataTable tb = (DataTable)dgList.DataSource;
                    tb.DefaultView.Sort = "MaNguyenPhuLieu";

                    dmDangKy.DMCollection = new DinhMucCollection();
                    foreach (DataRow dr in tb.Rows)
                    {

                        DinhMuc dm = new DinhMuc();
                        dm.MaSanPham = dr["MaSanPham"].ToString().Trim();
                        dm.MaNguyenPhuLieu = dr["MaNguyenPhuLieu"].ToString().Trim();
                        dm.TenNPL = dr["TenNPL"].ToString();
                        dm.DVT_ID = DonViTinh.GetID(dr["DVT"].ToString().ToUpper());
                        dm.DinhMucSuDung = Convert.ToDecimal(dr["DinhMucSuDung"].ToString());
                        dm.TyLeHaoHut = Convert.ToDecimal(dr["TyLeHaoHut"].ToString());
                        dm.GhiChu = dr["GhiChu"].ToString();
                        this.dmDangKy.DMCollection.Add(dm);
                    }


                    if (this.dmDangKy.DMCollection.Count > 0)
                    {
                        // Master.
                        this.Cursor = Cursors.WaitCursor;
                        this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        if (this.dmDangKy.ID == 0)
                        {
                            this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                            this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            this.dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.dmDangKy.SoTiepNhan = 0;
                            this.dmDangKy.NgayTiepNhan = DateTime.Now;
                            this.dmDangKy.NgayDangKy = DateTime.Now;
                            this.dmDangKy.NgayApDung = DateTime.Now;
                            this.dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                        }
                        // Detail.
                        this.dmDangKy.NgayTiepNhan = DateTime.Now;
                        this.dmDangKy.NgayDangKy = DateTime.Now;
                        this.dmDangKy.NgayApDung = DateTime.Now;
                        this.dmDangKy.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        int sttHang = 1;
                        foreach (DinhMuc nplD in this.dmDangKy.DMCollection)
                        {
                            nplD.STTHang = sttHang++;
                        }
                        //dmDangKy.DeleteDinhMucSXXK();
                        if (this.dmDangKy.InsertUpdateFull())
                        {
                            this.Cursor = Cursors.Default;

                            #region Lưu log thao tác
                            string where = "1 = 1";
                            where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", dmDangKy.ID, LoaiKhaiBao.DinhMuc);
                            List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                            if (listLog.Count > 0)
                            {
                                long idLog = listLog[0].IDLog;
                                string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                long idDK = listLog[0].ID_DK;
                                string guidstr = listLog[0].GUIDSTR_DK;
                                string userKhaiBao = listLog[0].UserNameKhaiBao;
                                DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                string userSuaDoi = GlobalSettings.UserLog;
                                DateTime ngaySuaDoi = DateTime.Now;
                                string ghiChu = listLog[0].GhiChu;
                                bool isDelete = listLog[0].IsDelete;
                                Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                            userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                            }
                            else
                            {
                                Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                                log.LoaiKhaiBao = LoaiKhaiBao.DinhMuc;
                                log.ID_DK = dmDangKy.ID;
                                log.GUIDSTR_DK = dmDangKy.GUIDSTR;
                                log.UserNameKhaiBao = GlobalSettings.UserLog;
                                log.NgayKhaiBao = DateTime.Now;
                                log.UserNameSuaDoi = GlobalSettings.UserLog;
                                log.NgaySuaDoi = DateTime.Now;
                                log.GhiChu = "";
                                log.IsDelete = false;
                                log.Insert();
                            }
                            #endregion

                            //this.cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                            dmDangKy.TransferDataToSXXK();
                            //SetCommand();
                            SetCommandNew();
                            MLMessages("Lưu thành công.", "MSG_SAV02", "", false);
                        }
                        else
                        {
                            this.Cursor = Cursors.Default;
                            MLMessages("Lưu không thành công", "MSG_SAV01", "", false);
                        }
                    }
                    //DinhMucSendForm_Load(null,null);
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Source);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
            
            
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdImportExcel":
                    this.ShowExcelForm();
                    break;
                case "cmdSend":
                    //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        this.SendV3();
                    //else
                    //    this.send();
                    break;
                case "XacNhan":
                    //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        this.FeedBackV3();
                    //else
                    //    this.LaySoTiepNhanDT();
                    break;
                case "SaoChep":
                    this.SaoChepDinhMuc();
                    break;
                case "InDM":
                    this.InDinhMuc();
                    break;
                case "Dinhmuc":
                    this.InDinhMuc();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaDM":
                    this.ChuyenTrangThaiSua();
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.dmDangKy.SoTiepNhan == 0)
            {
                return;
            }
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "ĐỊNH MỨC";
            phieuTN.soTN = this.dmDangKy.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.dmDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void InDinhMuc()
        {
            if (dmDangKy.ID == 0)
            {
                ShowMessage("Lưu thông tin trước ghi in.", false);
                return;
            }
            Report.ReportViewDinhMucForm f = new Company.Interface.Report.ReportViewDinhMucForm();
            f.DMDangKy = this.dmDangKy;
            f.ShowDialog(this);
        }
        private void ShowExcelForm()
        {
            ImportKDTDMForm importExcel = new ImportKDTDMForm();
            importExcel.dmDangKy = dmDangKy;
            importExcel.ShowDialog(this);
            try
            {
                dgList.DataSource = dmDangKy.DMCollection;
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }
        private void SaoChepDinhMuc()
        {
            Company.Interface.SXXK.DinhMucRegistedForm f = new Company.Interface.SXXK.DinhMucRegistedForm();

        }


        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MLMessages("Bạn có muốn xóa định mức này không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
                        // Thực hiện xóa trong CSDL.
                        if (dmSelected.ID > 0)
                        {
                            dmSelected.Delete();
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaDinhMuc(GridEXSelectedItemCollection items)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            List<DinhMuc> itemsDelete = new List<DinhMuc>(); ;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DinhMuc item = (DinhMuc)i.GetRow().DataRow;
                    itemsDelete.Add(item);
                    if (item.ID > 0)
                        msgWarning += string.Format("Mã sp ={0},Mã NPL ={1}, Định mức {2} [Xóa]\r\n", item.MaSanPham, item.MaNguyenPhuLieu, item.DinhMucSuDung);
                }
            }
            try
            {
                msgWarning += "Bạn có đồng ý xóa không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;

                foreach (DinhMuc item in itemsDelete)
                {
                    if (item.ID > 0 && item.CloneToDB(null))
                        item.Delete();
                }
                foreach (DinhMuc item in itemsDelete)
                {

                    dmDangKy.DMCollection.Remove(item);
                }

            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaDinhMuc(items);

            #region old
            //DinhMucCollection DMCollectionTMP = new DinhMucCollection();
            //if (MLMessages("Bạn có muốn xóa định mức này không?", "", "MSG_DEL01", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
            //            // Thực hiện xóa trong CSDL.
            //            if (dmSelected.ID > 0)
            //            {
            //                dmSelected.Delete();
            //                DMCollectionTMP.Add(dmSelected);
            //            }
            //            else
            //            {
            //                ShowMessage("Nhấn 'Lưu thông tin' trước khi xóa", false);
            //                return;
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    return;
            //}
            //foreach (DinhMuc dm in DMCollectionTMP)
            //{
            //    dmDangKy.DMCollection.Remove(dm);
            //}
            #endregion old
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        //----------------------------------------------------------------------------------------------
        #region Send V3 Create by LANNT
        private void SendV3()
        {
            bool isKhaiBaoSua= false;
            if (dmDangKy.ID == 0)
            {
                this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                return;
            }
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (sendXML.Load())
            {
                ShowMessage("Thôn tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                //cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            try
            {
                if (this.dmDangKy.DMCollection.Count > 0)
                {
                    // Master.
                    int s = CheckNPLVaSPDuyet();
                    if (s != 2)
                    {
                        string msg = "";
                        if (s == 0)
                            msg = "Trong danh sách này có nguyên phụ liệu chưa được hải quan duyệt.";
                        else msg = "Trong danh sách này có sản phẩm chưa được hải quan duyệt. ";
                        msg += "\n Bạn có muốn gửi lên hay không ?";

                        string kq = MLMessages(msg, "MSG_PUB20", "", true);
                        if (kq != "Yes")
                            return;
                    }
                    this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    string returnMessage = string.Empty;
                    dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                    if (dmDangKy.TrangThaiXuLy == 5)
                        isKhaiBaoSua = true;
                    else isKhaiBaoSua = false;
                    SXXK_DinhMucSP dmSp = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc(dmDangKy, false,isKhaiBaoSua, GlobalSettings.TEN_DON_VI);
                    //string msgSend = Helpers.BuildSend(
                    //               new NameBase()
                    //               {
                    //                   Name = GlobalSettings.TEN_DON_VI,
                    //                   Identity = dmDangKy.MaDoanhNghiep
                    //               }
                    //                 , new NameBase()
                    //                 {
                    //                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                    //                     Identity = dmDangKy.MaHaiQuan
                    //                 }
                    //              ,
                    //                new SubjectBase()
                    //                {
                    //                    Type = DeclarationIssuer.SXXK_DINHMUC_SP,
                    //                    Function = DeclarationFunction.KHAI_BAO,
                    //                    Reference = dmDangKy.GUIDSTR,
                    //                }
                    //                ,
                    //                dmSp, null, true);
                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = dmDangKy.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan
                              //Identity = dmDangKy.MaHaiQuan
                          },
                          new SubjectBase()
                          {
                              Type = dmSp.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = dmDangKy.GUIDSTR,
                          },
                          dmSp
                        );
                    if (isKhaiBaoSua)
                        msgSend.Subject.Function = Company.KDT.SHARE.Components.DeclarationFunction.SUA;
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.KhaiBaoDinhMuc);
                        //XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        //cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        //cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        btnDelete.Enabled = false;
                        sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                        sendXML.master_id = dmDangKy.ID;
                       // sendXML.msg = msgSend;
                        sendXML.func = 1;
                        sendXML.InsertUpdate();

                        dmDangKy.TransferDataToSXXK();

                        dmDangKy.Update();
                        FeedBackV3();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
                //XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            while (isFeedBack)
            {
                string reference = dmDangKy.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_DINH_MUC,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = dmDangKy.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan.Trim()),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dmDangKy.MaHaiQuan).Trim() : dmDangKy.MaHaiQuan
                                                  //Identity = dmDangKy.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    //if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    //{
                    //    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    //    {
                    //        isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                    //        ShowMessageTQDT(msgInfor, false);
                    //    }
                    //    count--;
                    //}
                    //else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    //{
                    //    ShowMessageTQDT(msgInfor, false);
                    //    isFeedBack = false;
                    //}
                    //else if (!string.IsNullOrEmpty(msgInfor))
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    //else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) SetCommand();
                }

            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dmDangKy, ref msgInfor, e);
        }
        /// <summary>
        /// 11/12/2014 minhnd
        /// </summary>
        private void ChuyenTrangThaiSua()
        {
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                if (ShowMessage("Bạn có muốn chuyển định mức sang trạng thái sửa\r\n" + "Số tiếp nhận: " + dmDangKy.SoTiepNhan.ToString(), true) == "Yes")
                {
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    dmDangKy.DeleteDinhMucSXXK();
                    Company.KDT.SHARE.Components.Globals.SaveMessage("", dmDangKy.ID, dmDangKy.GUIDSTR, (MessageTypes)int.Parse(DeclarationIssuer.GC_DINH_MUC), MessageFunctions.SuaToKhai, "Chuyển trạng thái sửa", "Chuyển khai báo định mức sang trạng thái sửa");
                }
            }
            else
                ShowMessage("Định mức chưa được duyệt", false);
            //this.SetCommand();
            this.SetCommandNew();
            //this.setCommandStatus();
        }
        private void SetCommandNew() 
        {
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                //cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdImportExcel1.Enabled = cmdImportExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InDM.Enabled = InDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else
            {
                //cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //cmdImportExcel1.Enabled = cmdImportExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InDM.Enabled = InDM1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
        }
        private void SetCommand()
        {
            bool allowSend = (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO);
            //XacNhan.Enabled = XacNhan1.Enabled = allowSend ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;
            //cmdSend.Enabled = cmdSend1.Enabled = cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = allowSend ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            btnDelete.Enabled = allowSend;
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Chờ duyệt chính thức";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Đã duyệt";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Không phê duyệt";
            }
                else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Sửa định mức";
                lblTrangThai.Text = dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Not declared yet") : setText("Sửa định mức", "");
                //HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                //InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdSuaDM.Enabled = cmdSuaDM1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Hủy khai báo";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Chờ hủy";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Đã hủy thành công";
            }
        }
        #endregion

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = dmDangKy.ID;
            form.DeclarationIssuer = DeclarationIssuer.SXXK_DINH_MUC;
            form.ShowDialog(this);
        }

        private DataTable makeDTBImport()
        {
            DataTable dtb = new DataTable("DM");
            DataColumn[] col = new DataColumn[11];
            col[0] = new DataColumn("MaSanPham", Type.GetType("System.String"));
            col[1] = new DataColumn("MaNguyenPhuLieu", Type.GetType("System.String"));
            col[2] = new DataColumn("TenNPL", Type.GetType("System.String"));
            col[3] = new DataColumn("ChungLoai", Type.GetType("System.String"));
            col[4] = new DataColumn("ThanhPhan", Type.GetType("System.String"));
            col[5] = new DataColumn("Kho", Type.GetType("System.String"));
            col[6] = new DataColumn("DVT", Type.GetType("System.String"));
            col[7] = new DataColumn("TyLeHaoHut", Type.GetType("System.String"));
            col[8] = new DataColumn("DinhMucSuDung", Type.GetType("System.String"));
            col[9] = new DataColumn("DinhMucThucTe", Type.GetType("System.String"));
            col[10] = new DataColumn("GhiChu", Type.GetType("System.String"));
            
            dtb.Columns.AddRange(col);
            return dtb;
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.DataSource = null;
                this.Cursor = Cursors.WaitCursor;


                Workbook objWB = new Workbook(WorkbookFormat.Excel97To2003);

                try
                {
                    objWB = Workbook.Load(txtFilePath.Text);
                }
                catch
                {
                    string _ts = txtFilePath.Text;
                }

                Worksheet sheet = objWB.Worksheets[txtSheetName.Text];
                WorksheetRowCollection rowColletion = sheet.Rows;
                DataTable dtLayDL = this.makeDTBImport();
                int isRead = 0;
                foreach (WorksheetRow var in rowColletion)
                {
                    if (isRead == 0)
                    {
                        isRead = 1;
                        continue;
                    }
                    DataRow drNew = dtLayDL.NewRow();
                    //for (int i = 0; i < 11; i++)
                    //    drNew[i] = var.Cells[i].Value != null ? var.Cells[i].Value.ToString() : "";
                    drNew["MaSanPham"] = var.Cells[0].Value != null ? var.Cells[0].Value.ToString() : "";
                    drNew["MaNguyenPhuLieu"] = var.Cells[8].Value != null ? var.Cells[8].Value.ToString() : "";
                    
                    
                    
                    //string t = drNew["TenNPL"].ToString();
                    drNew["ChungLoai"] = var.Cells[3].Value != null ? var.Cells[3].Value.ToString() : "";
                    drNew["ThanhPhan"] = var.Cells[4].Value != null ? var.Cells[4].Value.ToString() : "";
                    drNew["Kho"] = var.Cells[6].Value != null ? var.Cells[6].Value.ToString() : "";
                    drNew["TenNPL"] = var.Cells[2].Value != null ? var.Cells[2].Value.ToString() +" "+ drNew["Kho"].ToString() : "";
                    drNew["DVT"] = var.Cells[7].Value != null ? var.Cells[7].Value.ToString() : "";
                    drNew["TyLeHaoHut"] = var.Cells[10].Value != null ? var.Cells[10].Value.ToString() : "";
                    drNew["DinhMucSuDung"] = var.Cells[9].Value != null ? var.Cells[9].Value.ToString() : "";
                    drNew["DinhMucThucTe"] = var.Cells[11].Value != null ? var.Cells[11].Value.ToString() : "";
                    drNew["GhiChu"] = var.Cells[12].Value != null ? var.Cells[12].Value.ToString() : "";

                    dtLayDL.Rows.Add(drNew);
                }
                // this._masterID = 0;
                //string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + txtFilePath.Text + ";Extended Properties=Excel 8.0;";
                //string query = string.Format("SELECT MaSanPham, MaNguyenPhuLieu,TenNPL, ChungLoai,ThanhPhan,Kho,DVT,TyLeHaoHut,DinhMucSuDung,DinhMucThucTe,GhiChu  FROM [{0}$]", txtSheetName.Text);
                ////string query = string.Format("SELECT GhiChu  FROM [{0}$]", txtSheetName.Text);
                //DataTable dtLayDL = this.makeDTBImport();

                //OleDbConnection oldb = new OleDbConnection(connectionString);
                //if (oldb.State == ConnectionState.Open && oldb != null)
                //{
                //    oldb.Close();
                //}
                //OleDbCommand cmd = new OleDbCommand();
                //oldb.Open();
                //cmd.Connection = oldb;
                //cmd.CommandText = query;
                ////OleDbDataAdapter da = new OleDbDataAdapter(query, connectionString);
                //OleDbDataAdapter da = new OleDbDataAdapter(cmd);

                //da.Fill(dtLayDL);
                
                
                //OleDbDataReader drr =  cmd.ExecuteReader();
                //while (drr.Read()) 
                //{
                //    DataRow drNew = dtLayDL.NewRow();
                //    for (int i = 0; i < drr.FieldCount; i ++)
                //        drNew[i] = drr[i];
                //    dtLayDL.Rows.Add(drNew);
                //}
               // DataTable dtLayDL = new DataTable();
                //da.Fill(dtLayDL);
                //DataColumn cl = new DataColumn("MuaVN");
                //cl.DefaultValue = 0;
                //dtLayDL.Columns.Add(cl);
                bool flag = true;
                string error = "";
                foreach (DataRow dr in dtLayDL.Rows)
                {
                    string maSP = dr["MaSanPham"].ToString();
                    if (CheckSPExist(maSP) > 0)
                    {
                        error += "[" + maSP + "] ";
                        flag = false;
                    }
                }
                if (flag=false)
                {
                    MessageBox.Show("Mã SP:"+error+" đã được đăng ký.Kiểm tra lại.","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                else 
                {
                    dgList.DataSource = dtLayDL;
                    btnCheckDVT.Enabled = true;
                    btnCapNhatDVT.Enabled = true;
                    btnCheckNPLMuaVN.Enabled = true;
                    btnChuanHoa.Enabled = false;
                    btnKiemTraTK_GhiChu.Enabled = true;
                    // grdMain.Refresh();
                    //MessageBox.Show("Import thành công, vui lòng kiểm tra và nhấn lưu để cập nhật vào CSDL !","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
                
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Import không thành công. Lý do có thể là : \n\t+ Cấu trúc file không đúng , xem lại template mẫu.\n\t+ Định dạng file sai: Vui lòng chọn WordSheet 97 - 2003.\n\t+ Sai tên Sheet.");
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                txtFilePath.Text = openFileDialog1.FileName;
            }
        }
        private string CheckNPL(string maNPL)
        {
            string sql = "SELECT count(Ma) from t_SXXK_NguyenPhuLieu WHERE Ma = '{0}'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql, maNPL));
            try
            {
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                return dt.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }

        }
        private int CheckSPExist(string maSP)
        {
            //string sql = "SELECT * FROM dbo.t_KDT_SXXK_DinhMuc WHERE MaSanPham = '{0}'";
            string sql = "select * from t_KDT_SXXK_DinhMuc where masanpham = '"+maSP.Trim()+"'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(string.Format(sql, maSP));
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            try
            {
                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                //dt = DinhMuc.SelectDynamic("MaSanPham = '"+maSP+"'","").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return int.Parse(dt.Rows[0][0].ToString());
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //MessageBox.Show(ex.Message,"Lỗi",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return 0;
            }

        }
        private bool CheckNPLExist() 
        {
            
            //string test = "So HD:INV-1502836, Ngay:05/06/2015, So TK:100440814350, So TK:100440814351, So TK:100440814352";
            //string t = "";
            //if (test.Contains("TK:")) 
            //{
            //    string[] tmp = test.Split(new string[] { "TK:" }, StringSplitOptions.None);
            //    if (tmp.Length > 0) 
            //    {

            //        for (int i = 0; i < tmp.Length; i++)
            //        {
            //            if(i!=0)
            //                t += "/" + tmp[i].Substring(0, 12);
            //        }
                    
            //    }

            //}
            //MessageBox.Show(t);

            NguyenPhuLieuSendForm f = new NguyenPhuLieuSendForm();
            try
            {
                DataTable dt = (DataTable)dgList.DataSource;
                if (dt.Rows.Count == 0)
                    return false;
                DataTable temp = new DataTable();
                

                foreach (DataRow dr in dt.Rows)
                {
                    string manpl = dr["MaNguyenPhuLieu"].ToString();
                    if (manpl == "MNLDUC0164")
                    {

                    }
                    string str = CheckNPL(manpl);
                    if (str == "0")
                    {
                        temp.ImportRow(dr);
                        NguyenPhuLieu npl = new NguyenPhuLieu();
                        npl.Ma = dr["MaNguyenPhuLieu"].ToString();
                        npl.Ten = dr["TenNPL"].ToString();
                        npl.MaHS = "";
                        npl.DVT_ID = DonViTinh.GetID(dr["DVT"].ToString().ToUpper());
                        f.nplDangKy.NPLCollection.Add(npl);
                    }
                }
            }
            catch (Exception ex)
            {
                
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
                //throw;
            }

            if (f.nplDangKy.NPLCollection.Count > 0)
            {
                f.ShowDialog();
                return false;
            }
            else 
            {
                MessageBox.Show("Không có NPL nào chưa được khai báo","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                return true;
            }
                
                
        }

        private void btnCheckNPLMuaVN_Click(object sender, EventArgs e)
        {
            
            if (dgList.RowCount > 0) 
            {
                CheckNPLExist();
                btnCapNhatDVT_Click(sender,e);
                //btnCheckNPLMuaVN.Enabled = false;
                btnChuanHoa.Enabled = true;
                btnKiemTraTK_GhiChu.Enabled = false;
            }
            else
                MessageBox.Show("Không có dữ liệu xử lý.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnChuanHoa_Click(object sender, EventArgs e)
        {
            DataTable dttemp = (DataTable)dgList.DataSource;
            //DataTable tb = dttemp;
            try
            {
                
                //dttemp = (DataTable)dgList.DataSource;
                DataTable dtChuan = dttemp.Clone();


                DataTable _c = dttemp.DefaultView.ToTable(true, "MaNguyenPhuLieu");
                //DataTable dtbSave = new DataTable()
                if (_c.Rows.Count > 0)
                {
                    foreach (DataRow dr in _c.Select())
                    {
                        decimal dm = 0;
                        string ghichu = "";
                        decimal tylehh = 0;
                        decimal dmtt = 0;
                        DataRow drChuan = dtChuan.NewRow();
                        foreach (DataRow dr1 in dttemp.Select("MaNguyenPhuLieu = '" + dr["MaNguyenPhuLieu"] + "'", "MaNguyenPhuLieu"))
                        {
                            drChuan = dr1;
                            int tong = dttemp.Select("MaNguyenPhuLieu = '" + dr["MaNguyenPhuLieu"] + "'", "MaNguyenPhuLieu").Length;
                            dm += Convert.ToDecimal(dr1["DinhMucSuDung"].ToString());
                            dmtt += Convert.ToDecimal(dr1["DinhMucThucTe"].ToString());
                            if (dr1["GhiChu"].ToString() != "")
                                ghichu += dr1["GhiChu"].ToString() + ".";
                            if (dr1["TyLeHaoHut"].ToString() == "")
                            {
                                dr1["TyLeHaoHut"] = "0";
                            }
                            if (Convert.ToDecimal(dr1["TyLeHaoHut"].ToString()) != 0)
                                tylehh += Convert.ToDecimal(dr1["TyLeHaoHut"].ToString()) * Convert.ToDecimal(dr1["DinhMucSuDung"].ToString());
                        }
                        if (dm == 4)
                        {

                        }
                        //tylehh = tylehh / dm;
                        drChuan["DinhMucSuDung"] = dm;
                        drChuan["DinhMucThucTe"] = dm * ((tylehh / dm) / 100 + 1);
                        drChuan["GhiChu"] = ghichu;
                        drChuan["TyLeHaoHut"] = tylehh / dm;
                        dtChuan.ImportRow(drChuan);
                    }
                    if (dtChuan.Rows.Count > 0)
                    {
                        dgList.DataSource = dtChuan;
                        MessageBox.Show("Chuẩn hóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnChuanHoa.Enabled = false;
                        btnKiemTraTK_GhiChu.Enabled = true;
                        //cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    }
                }
                else 
                {
                    MessageBox.Show("Không có dữ liệu xử lý. Kiểm tra lại dữ liệu.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                    
            }
            catch (Exception ex)
            {
                btnChuanHoa.Enabled = true;
                MessageBox.Show("Chuẩn hóa không thành công. Có lỗi xảy ra: "+ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            
        }

        private void btnCapNhatDVT_Click(object sender, EventArgs e)
        {
            
            DataTable tb = (DataTable)dgList.DataSource;
            if (tb.Rows.Count > 0)
            {
                bool flag = false;
                string error = "";
                foreach (DataRow dr in tb.Rows)
                {
                    Company.BLL.KDT.ExportToExcel.NguyenPhuLieu npl = new Company.BLL.KDT.ExportToExcel.NguyenPhuLieu();
                    try
                    {
                        if (dr["MaNguyenPhuLieu"].ToString() == "nhanphug") 
                        {

                        }
                        npl = npl.SelectCollectionDynamic("Ma = '" + dr["MaNguyenPhuLieu"].ToString().Trim() + "'", "")[0];
                        if (npl.Ten.Trim() != "")
                        {
                            dr["TenNPL"] = npl.Ten;
                        }
                        
                        dr["DVT"] = DonViTinh_GetName(npl.DVT_ID);
                        if(dr["DVT"]=="")
                            error += "[" + dr["MaNguyenPhuLieu"] + "]ko có DVT , ";
                    }
                    catch (Exception ex)
                    {
                        error += "["+dr["MaNguyenPhuLieu"]+"] không tồn tại, ";
                        flag = true;
                    }
                }
                if(error!="")
                {
                    MLMessages(error + " .Kiểm tra lại các NPL này.", "Lỗi", "A", false);
                    cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }   
                else
                {
                    dgList.DataSource = tb;
                    //btnCapNhatDVT.Enabled = false;
                    btnCheckNPLMuaVN.Enabled = true;
                    btnChuanHoa.Enabled = false;
                    btnKiemTraTK_GhiChu.Enabled = false;
                }
            }
            else 
            {
                MessageBox.Show("Không có dữ liệu xử lý. Kiểm tra lại dữ liệu.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            


        }

        private void btnKiemTraTK_GhiChu_Click(object sender, EventArgs e)
        {
            DataTable tb = (DataTable)dgList.DataSource;
            if (tb.Rows.Count > 0)
            {
                bool flag = false;
                string error = "";
                foreach (DataRow dr in tb.Rows)
                {
                        if (dr["GhiChu"].ToString().Contains("TK:"))
                        {
                            string[] splitTK = dr["GhiChu"].ToString().Split(new string[] { "TK:" }, StringSplitOptions.None);
                            for (int i = 1; i < splitTK.Length; i++)
                            {
                                string sotk = splitTK[i].Substring(0, 12);
                                try
                                {
                                    CapSoToKhai capso = CapSoToKhai.GetFromTKMDVNACCS(Convert.ToDecimal(sotk));
                                    if(capso==null)
                                        error += sotk + ", ";    
                                }
                                catch (Exception ex)
                                {
                                    error += sotk + ", ";
                                    //throw;
                                }
                            }
                        }
                        
                }
                if (error != "")
                {
                    MLMessages(error + " không đúng định dạng hoặc ko tồn tại.\nKiểm tra lại các TK này.", "Lỗi", "A", false);
                    cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    dgList.DataSource = tb;
                    btnKiemTraTK_GhiChu.Enabled = false;
                    cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                    
            }
            else
            {
                MessageBox.Show("Không có dữ liệu xử lý. Kiểm tra lại dữ liệu.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCheckDVT_Click(object sender, EventArgs e)
        {
            DataTable tb = (DataTable)dgList.DataSource;
            string error = "";
            foreach (DataRow dr in tb.Rows)
            {
                try
                {
                    if (string.IsNullOrEmpty(DonViTinh.GetID(dr["DVT"].ToString())))
                    {
                        error += dr["DVT"].ToString() + ", ";
                    }
                }
                catch (Exception ex)
                {
                    error += dr["DVT"].ToString() + ", ";
                    //throw;
                }
                
            }
            if (error != "")
            {
                MLMessages("Các đơn vị tính: " + error + " không có trong danh mục chuẩn.", "Lỗi DVT", "Lỗi", false);
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else 
            {
                MLMessages("Đơn vị tính OK", "Thông báo", "Thông báo", false);
               // btnCheckDVT.Enabled = false;
                btnCapNhatDVT.Enabled = true;
                btnCheckNPLMuaVN.Enabled = true;
                btnChuanHoa.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //btnKiemTraTK_GhiChu.Enabled = false;
            }
        }
    }

        
            

    }
