﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text;

namespace Activation
{
    public class Helpers
    {
        /// <summary>
        /// Sinh đối tượng từ định dạng xml
        /// </summary>
        /// <typeparam name="T">Kiểu đối tượng</typeparam>
        /// <param name="sfmtObjectXml">định dạng đối tượng dưới dạng xml</param>
        /// <returns></returns>
        public static T Deserialize<T>(string sfmtObjectXml)
        {

            using (StringReader reader = new StringReader(sfmtObjectXml))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }
        /// <summary>
        /// Đóng gói xml từ đối tượng
        /// </summary>
        /// <param name="objMapping">Đối tượng sinh</param>
        /// <returns>định dạng xml đối tượng</returns>
        public static string Serializer(object objMapping)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(objMapping.GetType());
                TextWriter writer = new StreamWriter(stream, Encoding.Unicode);
                serializer.Serialize(writer, objMapping);
                int count = (int)stream.Length;
                byte[] arrByte = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(arrByte, 0, count);
                UnicodeEncoding utf = new UnicodeEncoding();
                return utf.GetString(arrByte).Trim();
            }
        }      
    }
}
