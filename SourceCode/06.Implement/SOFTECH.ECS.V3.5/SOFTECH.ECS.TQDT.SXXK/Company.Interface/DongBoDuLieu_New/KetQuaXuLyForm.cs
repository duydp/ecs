﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.DongBoDuLieu_New
{
    public partial class KetQuaXuLyForm : BaseForm
    {
        string maHaiQuan = "";
        string maDoanhNghiep = "";
        public ISyncData_SXXK myService = IsyncDaTa_V3.SyncService();
        public int IDUserDaiLy { set; get; }
        public string UserName { set; get; }
        public DateTime FromDate { set { timePanel1.FromDate = value; } get { return timePanel1.FromDate; } }
        public DateTime ToDate { set { timePanel1.ToDate = value; } get { return timePanel1.ToDate; } }

        public KetQuaXuLyForm()
        {
            InitializeComponent();
        }

        private void KetQuaXuLyForm_Load(object sender, EventArgs e)
        {
            try
            {
                timePanel1.ToDate = ToDate;
                timePanel1.FromDate = FromDate;

                maHaiQuan = GlobalSettings.MA_HAI_QUAN;
                maDoanhNghiep = GlobalSettings.MA_DON_VI;

                Search(maHaiQuan, maDoanhNghiep, UserName);

                dgList.ColumnAutoSizeMode = ColumnAutoSizeMode.AllCellsAndHeader;
                dgList.RootTable.Columns["SoToKhai"].Width = 90;
                dgList.RootTable.Columns["MaLoaiHinh"].Width = 90;
                dgList.RootTable.Columns["NamDangKy"].Width = 90;

                dgList.RootTable.Columns["TrangThaiDongBoTaiLen"].Visible = (FrmDongBoDuLieu.formType == FormType.CLIENT);
                dgList.RootTable.Columns["TrangThaiDongBoTaiXuong"].Visible = (FrmDongBoDuLieu.formType == FormType.SERVER);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    //int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    long SoToKhai = Convert.ToInt64(e.Row.Cells["SoToKhai"].Value);
                    string MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinh"].Value);
                    int NamDK = Convert.ToInt32(e.Row.Cells["NamDangKy"].Value);
                    //Company.KDT.SHARE.Components.Track kqxl = Company.KDT.SHARE.Components.Track.Load(id);
                    Company.KDT.SHARE.Components.Track kqxl = Helpers.Deserialize<Track>(myService.SelectTrack(Config.User, Config.Pass, SoToKhai, NamDK, MaLoaiHinh, maHaiQuan, maDoanhNghiep));

                    if (kqxl == null) return;

                    Company.KDT.SHARE.Components.DaiLy userDaiLy = Helpers.Deserialize<DaiLy>(myService.LoginDaiLy(Config.User, Config.Pass));

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Mã hải quan: " + kqxl.MaHaiQuan);
                    sb.AppendLine("Mã doanh nghiệp: " + kqxl.MaDoanhNghiep);
                    sb.AppendLine("");
                    sb.AppendLine("ID: " + kqxl.TKMD_GUIDSTR);
                    sb.AppendLine("Số tờ khai: " + kqxl.SoToKhai);
                    sb.AppendLine("Năm đăng ký: " + kqxl.NamDangKy.ToString());
                    sb.AppendLine("Mã loại hình: " + kqxl.MaLoaiHinh);
                    sb.AppendLine("");

                    if (FrmDongBoDuLieu.formType == FormType.CLIENT)
                        sb.AppendLine("Trạng thái đồng bộ: " + (kqxl.TrangThaiDongBoTaiLen == 1 ? "Đã tải lên." : "Chưa tải lên."));
                    else if (FrmDongBoDuLieu.formType == FormType.SERVER)
                        sb.AppendLine("Trạng thái đồng bộ: " + (kqxl.TrangThaiDongBoTaiXuong == 1 ? "Đã tải xuống." : "Chưa tải xuống."));
                    sb.AppendLine("Ngày đồng bộ: " + kqxl.NgayDongBo.ToString());
                    sb.AppendLine("Người đồng bộ: " + (userDaiLy != null ? userDaiLy.HO_TEN + " - " + userDaiLy.MO_TA : ""));
                    sb.AppendLine("Ghi chú: " + kqxl.GhiChu);

                    this.ShowMessage(sb.ToString(), false);
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["IDUserDaiLy"].Text = getTenDaiLy(Convert.ToInt32(e.Row.Cells["IDUserDaiLy"].Value));

                if (FrmDongBoDuLieu.formType == FormType.CLIENT)
                    e.Row.Cells["TrangThaiDongBoTaiLen"].Text = (Convert.ToInt32(e.Row.Cells["TrangThaiDongBoTaiLen"].Value) == 1 ? "Đã tải lên." : "Chưa tải lên.");
                else if (FrmDongBoDuLieu.formType == FormType.SERVER)
                    e.Row.Cells["TrangThaiDongBoTaiXuong"].Text = (Convert.ToInt32(e.Row.Cells["TrangThaiDongBoTaiXuong"].Value) == 1 ? "Đã tải xuống." : "Chưa tải xuống.");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private string getTenDaiLy(int idUserDaiLy)
        {
           /* WS.Service myService = new WS.Service();*/
            //Company.KDT.SHARE.Components.DaiLy[] userDaiLyCollection = myService.SelectUserDaiLy(maDoanhNghiep);

            //for (int i = 0; i < userDaiLyCollection.Length; i++)
            //{
            //    if (userDaiLyCollection[i].ID == idUserDaiLy)
            //    {
            //        return userDaiLyCollection[i].USER_NAME;
            //    }
            //}

            return "";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search(maHaiQuan, maDoanhNghiep, UserName);
        }

        private void Search(string maHaiQuan, string maDoanhNghiep, string userName)
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = "1 = 1";
                where += string.Format(" AND MaHaiQuan = '{0}'", maHaiQuan);
                where += string.Format(" AND MaDoanhNghiep = '{0}'", maDoanhNghiep);
                where += " AND NgayDongBo BETWEEN '" + timePanel1.FromDate.ToString("MM/dd/yyyy 00:00:00") + "' AND '" + timePanel1.ToDate.ToString("MM/dd/yyyy 23:59:59") + "'";
                where += string.Format(" AND UserName IN ({0})", userName);

                // Thực hiện tìm kiếm Offline          
                //IList<Company.KDT.SHARE.Components.Track> trackCollection = Company.KDT.SHARE.Components.Track.SelectCollectionDynamic(where, "");
                // Thực hiện tìm kiếm Online
                List<Company.KDT.SHARE.Components.Track> trackCollection = Helpers.Deserialize<List<Track>>(myService.SelectTrackDynamic(Config.User, Config.Pass, where, ""));
                dgList.DataSource = trackCollection;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
