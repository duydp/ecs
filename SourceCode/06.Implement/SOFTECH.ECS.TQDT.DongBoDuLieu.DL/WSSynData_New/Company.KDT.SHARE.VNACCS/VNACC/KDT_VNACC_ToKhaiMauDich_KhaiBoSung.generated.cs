using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich_KhaiBoSung : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public decimal SoToKhaiBoSung { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public string NhomXuLyHoSo { set; get; }
		public int NhomXuLyHoSoID { set; get; }
		public string PhanLoaiXuatNhapKhau { set; get; }
		public decimal SoToKhai { set; get; }
		public string MaLoaiHinh { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public DateTime NgayCapPhep { set; get; }
		public DateTime ThoiHanTaiNhapTaiXuat { set; get; }
		public string MaNguoiKhai { set; get; }
		public string TenNguoiKhai { set; get; }
		public string MaBuuChinh { set; get; }
		public string DiaChiNguoiKhai { set; get; }
		public string SoDienThoaiNguoiKhai { set; get; }
		public string MaLyDoKhaiBoSung { set; get; }
		public string MaTienTeTienThue { set; get; }
		public string MaNganHangTraThueThay { set; get; }
		public decimal NamPhatHanhHanMuc { set; get; }
		public string KiHieuChungTuPhatHanhHanMuc { set; get; }
		public string SoChungTuPhatHanhHanMuc { set; get; }
		public string MaXacDinhThoiHanNopThue { set; get; }
		public string MaNganHangBaoLanh { set; get; }
		public decimal NamPhatHanhBaoLanh { set; get; }
		public string KyHieuPhatHanhChungTuBaoLanh { set; get; }
		public string SoHieuPhatHanhChungTuBaoLanh { set; get; }
		public string MaTienTeTruocKhiKhaiBoSung { set; get; }
		public decimal TyGiaHoiDoaiTruocKhiKhaiBoSung { set; get; }
		public string MaTienTeSauKhiKhaiBoSung { set; get; }
		public decimal TyGiaHoiDoaiSauKhiKhaiBoSung { set; get; }
		public string SoQuanLyTrongNoiBoDoanhNghiep { set; get; }
		public string GhiChuNoiDungLienQuanTruocKhiKhaiBoSung { set; get; }
		public string GhiChuNoiDungLienQuanSauKhiKhaiBoSung { set; get; }
		public decimal SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string PhanLuong { set; get; }
		public string HuongDan { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public decimal SoThongBao { set; get; }
		public DateTime NgayHoanThanhKiemTra { set; get; }
		public DateTime GioHoanThanhKiemTra { set; get; }
		public DateTime NgayDangKyKhaiBoSung { set; get; }
		public DateTime GioDangKyKhaiBoSung { set; get; }
		public string DauHieuBaoQua60Ngay { set; get; }
		public string MaHetThoiHan { set; get; }
		public string MaDaiLyHaiQuan { set; get; }
		public string TenDaiLyHaiQuan { set; get; }
		public string MaNhanVienHaiQuan { set; get; }
		public string PhanLoaiNopThue { set; get; }
		public DateTime NgayHieuLucChungTu { set; get; }
		public decimal SoNgayNopThue { set; get; }
		public decimal SoNgayNopThueDanhChoVATHangHoaDacBiet { set; get; }
		public string HienThiTongSoTienTangGiamThueXuatNhapKhau { set; get; }
		public decimal TongSoTienTangGiamThueXuatNhapKhau { set; get; }
		public string MaTienTeTongSoTienTangGiamThueXuatNhapKhau { set; get; }
		public decimal TongSoTrangToKhaiBoSung { set; get; }
		public decimal TongSoDongHangToKhaiBoSung { set; get; }
		public string LyDo { set; get; }
		public string TenNguoiPhuTrach { set; get; }
		public string TenTruongDonViHaiQuan { set; get; }
		public DateTime NgayDangKyDuLieu { set; get; }
		public DateTime GioDangKyDuLieu { set; get; }
		public int TrangThaiXuLy { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> collection = new List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiMauDich_KhaiBoSung entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiBoSung"))) entity.SoToKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("SoToKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomXuLyHoSo"))) entity.NhomXuLyHoSo = reader.GetString(reader.GetOrdinal("NhomXuLyHoSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomXuLyHoSoID"))) entity.NhomXuLyHoSoID = reader.GetInt32(reader.GetOrdinal("NhomXuLyHoSoID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiXuatNhapKhau"))) entity.PhanLoaiXuatNhapKhau = reader.GetString(reader.GetOrdinal("PhanLoaiXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapPhep"))) entity.NgayCapPhep = reader.GetDateTime(reader.GetOrdinal("NgayCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanTaiNhapTaiXuat"))) entity.ThoiHanTaiNhapTaiXuat = reader.GetDateTime(reader.GetOrdinal("ThoiHanTaiNhapTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhai"))) entity.MaNguoiKhai = reader.GetString(reader.GetOrdinal("MaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhai"))) entity.TenNguoiKhai = reader.GetString(reader.GetOrdinal("TenNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinh"))) entity.MaBuuChinh = reader.GetString(reader.GetOrdinal("MaBuuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiKhai"))) entity.DiaChiNguoiKhai = reader.GetString(reader.GetOrdinal("DiaChiNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNguoiKhai"))) entity.SoDienThoaiNguoiKhai = reader.GetString(reader.GetOrdinal("SoDienThoaiNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLyDoKhaiBoSung"))) entity.MaLyDoKhaiBoSung = reader.GetString(reader.GetOrdinal("MaLyDoKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTienTeTienThue"))) entity.MaTienTeTienThue = reader.GetString(reader.GetOrdinal("MaTienTeTienThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangTraThueThay"))) entity.MaNganHangTraThueThay = reader.GetString(reader.GetOrdinal("MaNganHangTraThueThay"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamPhatHanhHanMuc"))) entity.NamPhatHanhHanMuc = reader.GetDecimal(reader.GetOrdinal("NamPhatHanhHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("KiHieuChungTuPhatHanhHanMuc"))) entity.KiHieuChungTuPhatHanhHanMuc = reader.GetString(reader.GetOrdinal("KiHieuChungTuPhatHanhHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTuPhatHanhHanMuc"))) entity.SoChungTuPhatHanhHanMuc = reader.GetString(reader.GetOrdinal("SoChungTuPhatHanhHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaXacDinhThoiHanNopThue"))) entity.MaXacDinhThoiHanNopThue = reader.GetString(reader.GetOrdinal("MaXacDinhThoiHanNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangBaoLanh"))) entity.MaNganHangBaoLanh = reader.GetString(reader.GetOrdinal("MaNganHangBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamPhatHanhBaoLanh"))) entity.NamPhatHanhBaoLanh = reader.GetDecimal(reader.GetOrdinal("NamPhatHanhBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("KyHieuPhatHanhChungTuBaoLanh"))) entity.KyHieuPhatHanhChungTuBaoLanh = reader.GetString(reader.GetOrdinal("KyHieuPhatHanhChungTuBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPhatHanhChungTuBaoLanh"))) entity.SoHieuPhatHanhChungTuBaoLanh = reader.GetString(reader.GetOrdinal("SoHieuPhatHanhChungTuBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTienTeTruocKhiKhaiBoSung"))) entity.MaTienTeTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MaTienTeTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaHoiDoaiTruocKhiKhaiBoSung"))) entity.TyGiaHoiDoaiTruocKhiKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("TyGiaHoiDoaiTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTienTeSauKhiKhaiBoSung"))) entity.MaTienTeSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("MaTienTeSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaHoiDoaiSauKhiKhaiBoSung"))) entity.TyGiaHoiDoaiSauKhiKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("TyGiaHoiDoaiSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuanLyTrongNoiBoDoanhNghiep"))) entity.SoQuanLyTrongNoiBoDoanhNghiep = reader.GetString(reader.GetOrdinal("SoQuanLyTrongNoiBoDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuNoiDungLienQuanTruocKhiKhaiBoSung"))) entity.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("GhiChuNoiDungLienQuanTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuNoiDungLienQuanSauKhiKhaiBoSung"))) entity.GhiChuNoiDungLienQuanSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("GhiChuNoiDungLienQuanSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetDecimal(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuongDan"))) entity.HuongDan = reader.GetString(reader.GetOrdinal("HuongDan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThongBao"))) entity.SoThongBao = reader.GetDecimal(reader.GetOrdinal("SoThongBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhKiemTra"))) entity.NgayHoanThanhKiemTra = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("GioHoanThanhKiemTra"))) entity.GioHoanThanhKiemTra = reader.GetDateTime(reader.GetOrdinal("GioHoanThanhKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyKhaiBoSung"))) entity.NgayDangKyKhaiBoSung = reader.GetDateTime(reader.GetOrdinal("NgayDangKyKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("GioDangKyKhaiBoSung"))) entity.GioDangKyKhaiBoSung = reader.GetDateTime(reader.GetOrdinal("GioDangKyKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("DauHieuBaoQua60Ngay"))) entity.DauHieuBaoQua60Ngay = reader.GetString(reader.GetOrdinal("DauHieuBaoQua60Ngay"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHetThoiHan"))) entity.MaHetThoiHan = reader.GetString(reader.GetOrdinal("MaHetThoiHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyHaiQuan"))) entity.MaDaiLyHaiQuan = reader.GetString(reader.GetOrdinal("MaDaiLyHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyHaiQuan"))) entity.TenDaiLyHaiQuan = reader.GetString(reader.GetOrdinal("TenDaiLyHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNhanVienHaiQuan"))) entity.MaNhanVienHaiQuan = reader.GetString(reader.GetOrdinal("MaNhanVienHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiNopThue"))) entity.PhanLoaiNopThue = reader.GetString(reader.GetOrdinal("PhanLoaiNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHieuLucChungTu"))) entity.NgayHieuLucChungTu = reader.GetDateTime(reader.GetOrdinal("NgayHieuLucChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayNopThue"))) entity.SoNgayNopThue = reader.GetDecimal(reader.GetOrdinal("SoNgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayNopThueDanhChoVATHangHoaDacBiet"))) entity.SoNgayNopThueDanhChoVATHangHoaDacBiet = reader.GetDecimal(reader.GetOrdinal("SoNgayNopThueDanhChoVATHangHoaDacBiet"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThiTongSoTienTangGiamThueXuatNhapKhau"))) entity.HienThiTongSoTienTangGiamThueXuatNhapKhau = reader.GetString(reader.GetOrdinal("HienThiTongSoTienTangGiamThueXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTienTangGiamThueXuatNhapKhau"))) entity.TongSoTienTangGiamThueXuatNhapKhau = reader.GetDecimal(reader.GetOrdinal("TongSoTienTangGiamThueXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTienTeTongSoTienTangGiamThueXuatNhapKhau"))) entity.MaTienTeTongSoTienTangGiamThueXuatNhapKhau = reader.GetString(reader.GetOrdinal("MaTienTeTongSoTienTangGiamThueXuatNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoTrangToKhaiBoSung"))) entity.TongSoTrangToKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("TongSoTrangToKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoDongHangToKhaiBoSung"))) entity.TongSoDongHangToKhaiBoSung = reader.GetDecimal(reader.GetOrdinal("TongSoDongHangToKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDo"))) entity.LyDo = reader.GetString(reader.GetOrdinal("LyDo"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiPhuTrach"))) entity.TenNguoiPhuTrach = reader.GetString(reader.GetOrdinal("TenNguoiPhuTrach"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenTruongDonViHaiQuan"))) entity.TenTruongDonViHaiQuan = reader.GetString(reader.GetOrdinal("TenTruongDonViHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyDuLieu"))) entity.NgayDangKyDuLieu = reader.GetDateTime(reader.GetOrdinal("NgayDangKyDuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("GioDangKyDuLieu"))) entity.GioDangKyDuLieu = reader.GetDateTime(reader.GetOrdinal("GioDangKyDuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung VALUES(@TKMD_ID, @SoToKhaiBoSung, @CoQuanHaiQuan, @NhomXuLyHoSo, @NhomXuLyHoSoID, @PhanLoaiXuatNhapKhau, @SoToKhai, @MaLoaiHinh, @NgayKhaiBao, @NgayCapPhep, @ThoiHanTaiNhapTaiXuat, @MaNguoiKhai, @TenNguoiKhai, @MaBuuChinh, @DiaChiNguoiKhai, @SoDienThoaiNguoiKhai, @MaLyDoKhaiBoSung, @MaTienTeTienThue, @MaNganHangTraThueThay, @NamPhatHanhHanMuc, @KiHieuChungTuPhatHanhHanMuc, @SoChungTuPhatHanhHanMuc, @MaXacDinhThoiHanNopThue, @MaNganHangBaoLanh, @NamPhatHanhBaoLanh, @KyHieuPhatHanhChungTuBaoLanh, @SoHieuPhatHanhChungTuBaoLanh, @MaTienTeTruocKhiKhaiBoSung, @TyGiaHoiDoaiTruocKhiKhaiBoSung, @MaTienTeSauKhiKhaiBoSung, @TyGiaHoiDoaiSauKhiKhaiBoSung, @SoQuanLyTrongNoiBoDoanhNghiep, @GhiChuNoiDungLienQuanTruocKhiKhaiBoSung, @GhiChuNoiDungLienQuanSauKhiKhaiBoSung, @SoTiepNhan, @NgayTiepNhan, @PhanLuong, @HuongDan, @Notes, @InputMessageID, @MessageTag, @IndexTag, @SoThongBao, @NgayHoanThanhKiemTra, @GioHoanThanhKiemTra, @NgayDangKyKhaiBoSung, @GioDangKyKhaiBoSung, @DauHieuBaoQua60Ngay, @MaHetThoiHan, @MaDaiLyHaiQuan, @TenDaiLyHaiQuan, @MaNhanVienHaiQuan, @PhanLoaiNopThue, @NgayHieuLucChungTu, @SoNgayNopThue, @SoNgayNopThueDanhChoVATHangHoaDacBiet, @HienThiTongSoTienTangGiamThueXuatNhapKhau, @TongSoTienTangGiamThueXuatNhapKhau, @MaTienTeTongSoTienTangGiamThueXuatNhapKhau, @TongSoTrangToKhaiBoSung, @TongSoDongHangToKhaiBoSung, @LyDo, @TenNguoiPhuTrach, @TenTruongDonViHaiQuan, @NgayDangKyDuLieu, @GioDangKyDuLieu, @TrangThaiXuLy)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung SET TKMD_ID = @TKMD_ID, SoToKhaiBoSung = @SoToKhaiBoSung, CoQuanHaiQuan = @CoQuanHaiQuan, NhomXuLyHoSo = @NhomXuLyHoSo, NhomXuLyHoSoID = @NhomXuLyHoSoID, PhanLoaiXuatNhapKhau = @PhanLoaiXuatNhapKhau, SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, NgayKhaiBao = @NgayKhaiBao, NgayCapPhep = @NgayCapPhep, ThoiHanTaiNhapTaiXuat = @ThoiHanTaiNhapTaiXuat, MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, MaBuuChinh = @MaBuuChinh, DiaChiNguoiKhai = @DiaChiNguoiKhai, SoDienThoaiNguoiKhai = @SoDienThoaiNguoiKhai, MaLyDoKhaiBoSung = @MaLyDoKhaiBoSung, MaTienTeTienThue = @MaTienTeTienThue, MaNganHangTraThueThay = @MaNganHangTraThueThay, NamPhatHanhHanMuc = @NamPhatHanhHanMuc, KiHieuChungTuPhatHanhHanMuc = @KiHieuChungTuPhatHanhHanMuc, SoChungTuPhatHanhHanMuc = @SoChungTuPhatHanhHanMuc, MaXacDinhThoiHanNopThue = @MaXacDinhThoiHanNopThue, MaNganHangBaoLanh = @MaNganHangBaoLanh, NamPhatHanhBaoLanh = @NamPhatHanhBaoLanh, KyHieuPhatHanhChungTuBaoLanh = @KyHieuPhatHanhChungTuBaoLanh, SoHieuPhatHanhChungTuBaoLanh = @SoHieuPhatHanhChungTuBaoLanh, MaTienTeTruocKhiKhaiBoSung = @MaTienTeTruocKhiKhaiBoSung, TyGiaHoiDoaiTruocKhiKhaiBoSung = @TyGiaHoiDoaiTruocKhiKhaiBoSung, MaTienTeSauKhiKhaiBoSung = @MaTienTeSauKhiKhaiBoSung, TyGiaHoiDoaiSauKhiKhaiBoSung = @TyGiaHoiDoaiSauKhiKhaiBoSung, SoQuanLyTrongNoiBoDoanhNghiep = @SoQuanLyTrongNoiBoDoanhNghiep, GhiChuNoiDungLienQuanTruocKhiKhaiBoSung = @GhiChuNoiDungLienQuanTruocKhiKhaiBoSung, GhiChuNoiDungLienQuanSauKhiKhaiBoSung = @GhiChuNoiDungLienQuanSauKhiKhaiBoSung, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, PhanLuong = @PhanLuong, HuongDan = @HuongDan, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, SoThongBao = @SoThongBao, NgayHoanThanhKiemTra = @NgayHoanThanhKiemTra, GioHoanThanhKiemTra = @GioHoanThanhKiemTra, NgayDangKyKhaiBoSung = @NgayDangKyKhaiBoSung, GioDangKyKhaiBoSung = @GioDangKyKhaiBoSung, DauHieuBaoQua60Ngay = @DauHieuBaoQua60Ngay, MaHetThoiHan = @MaHetThoiHan, MaDaiLyHaiQuan = @MaDaiLyHaiQuan, TenDaiLyHaiQuan = @TenDaiLyHaiQuan, MaNhanVienHaiQuan = @MaNhanVienHaiQuan, PhanLoaiNopThue = @PhanLoaiNopThue, NgayHieuLucChungTu = @NgayHieuLucChungTu, SoNgayNopThue = @SoNgayNopThue, SoNgayNopThueDanhChoVATHangHoaDacBiet = @SoNgayNopThueDanhChoVATHangHoaDacBiet, HienThiTongSoTienTangGiamThueXuatNhapKhau = @HienThiTongSoTienTangGiamThueXuatNhapKhau, TongSoTienTangGiamThueXuatNhapKhau = @TongSoTienTangGiamThueXuatNhapKhau, MaTienTeTongSoTienTangGiamThueXuatNhapKhau = @MaTienTeTongSoTienTangGiamThueXuatNhapKhau, TongSoTrangToKhaiBoSung = @TongSoTrangToKhaiBoSung, TongSoDongHangToKhaiBoSung = @TongSoDongHangToKhaiBoSung, LyDo = @LyDo, TenNguoiPhuTrach = @TenNguoiPhuTrach, TenTruongDonViHaiQuan = @TenTruongDonViHaiQuan, NgayDangKyDuLieu = @NgayDangKyDuLieu, GioDangKyDuLieu = @GioDangKyDuLieu, TrangThaiXuLy = @TrangThaiXuLy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiBoSung", SqlDbType.Decimal, "SoToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, "NhomXuLyHoSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHoSoID", SqlDbType.Int, "NhomXuLyHoSoID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, "PhanLoaiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, "ThoiHanTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLyDoKhaiBoSung", SqlDbType.VarChar, "MaLyDoKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeTienThue", SqlDbType.VarChar, "MaTienTeTienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangTraThueThay", SqlDbType.VarChar, "MaNganHangTraThueThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhHanMuc", SqlDbType.Decimal, "NamPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTuPhatHanhHanMuc", SqlDbType.VarChar, "SoChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaXacDinhThoiHanNopThue", SqlDbType.VarChar, "MaXacDinhThoiHanNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhBaoLanh", SqlDbType.Decimal, "NamPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, "KyHieuPhatHanhChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, "SoHieuPhatHanhChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaTienTeTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaHoiDoaiTruocKhiKhaiBoSung", SqlDbType.Decimal, "TyGiaHoiDoaiTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeSauKhiKhaiBoSung", SqlDbType.VarChar, "MaTienTeSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaHoiDoaiSauKhiKhaiBoSung", SqlDbType.Decimal, "TyGiaHoiDoaiSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.NVarChar, "SoQuanLyTrongNoiBoDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", SqlDbType.NVarChar, "GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuNoiDungLienQuanSauKhiKhaiBoSung", SqlDbType.NVarChar, "GhiChuNoiDungLienQuanSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongDan", SqlDbType.NVarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThongBao", SqlDbType.Decimal, "SoThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioHoanThanhKiemTra", SqlDbType.DateTime, "GioHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyKhaiBoSung", SqlDbType.DateTime, "NgayDangKyKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioDangKyKhaiBoSung", SqlDbType.DateTime, "GioDangKyKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DauHieuBaoQua60Ngay", SqlDbType.VarChar, "DauHieuBaoQua60Ngay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHetThoiHan", SqlDbType.VarChar, "MaHetThoiHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLyHaiQuan", SqlDbType.VarChar, "MaDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiLyHaiQuan", SqlDbType.NVarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHieuLucChungTu", SqlDbType.DateTime, "NgayHieuLucChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayNopThue", SqlDbType.Decimal, "SoNgayNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayNopThueDanhChoVATHangHoaDacBiet", SqlDbType.Decimal, "SoNgayNopThueDanhChoVATHangHoaDacBiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "HienThiTongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienTangGiamThueXuatNhapKhau", SqlDbType.Decimal, "TongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "MaTienTeTongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTrangToKhaiBoSung", SqlDbType.Decimal, "TongSoTrangToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoDongHangToKhaiBoSung", SqlDbType.Decimal, "TongSoDongHangToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDo", SqlDbType.NVarChar, "LyDo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiPhuTrach", SqlDbType.NVarChar, "TenNguoiPhuTrach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyDuLieu", SqlDbType.DateTime, "NgayDangKyDuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioDangKyDuLieu", SqlDbType.DateTime, "GioDangKyDuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiBoSung", SqlDbType.Decimal, "SoToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, "NhomXuLyHoSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHoSoID", SqlDbType.Int, "NhomXuLyHoSoID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, "PhanLoaiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, "ThoiHanTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLyDoKhaiBoSung", SqlDbType.VarChar, "MaLyDoKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeTienThue", SqlDbType.VarChar, "MaTienTeTienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangTraThueThay", SqlDbType.VarChar, "MaNganHangTraThueThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhHanMuc", SqlDbType.Decimal, "NamPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTuPhatHanhHanMuc", SqlDbType.VarChar, "SoChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaXacDinhThoiHanNopThue", SqlDbType.VarChar, "MaXacDinhThoiHanNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhBaoLanh", SqlDbType.Decimal, "NamPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, "KyHieuPhatHanhChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, "SoHieuPhatHanhChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaTienTeTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaHoiDoaiTruocKhiKhaiBoSung", SqlDbType.Decimal, "TyGiaHoiDoaiTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeSauKhiKhaiBoSung", SqlDbType.VarChar, "MaTienTeSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaHoiDoaiSauKhiKhaiBoSung", SqlDbType.Decimal, "TyGiaHoiDoaiSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.NVarChar, "SoQuanLyTrongNoiBoDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", SqlDbType.NVarChar, "GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuNoiDungLienQuanSauKhiKhaiBoSung", SqlDbType.NVarChar, "GhiChuNoiDungLienQuanSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongDan", SqlDbType.NVarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThongBao", SqlDbType.Decimal, "SoThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioHoanThanhKiemTra", SqlDbType.DateTime, "GioHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyKhaiBoSung", SqlDbType.DateTime, "NgayDangKyKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioDangKyKhaiBoSung", SqlDbType.DateTime, "GioDangKyKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DauHieuBaoQua60Ngay", SqlDbType.VarChar, "DauHieuBaoQua60Ngay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHetThoiHan", SqlDbType.VarChar, "MaHetThoiHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLyHaiQuan", SqlDbType.VarChar, "MaDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiLyHaiQuan", SqlDbType.NVarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHieuLucChungTu", SqlDbType.DateTime, "NgayHieuLucChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayNopThue", SqlDbType.Decimal, "SoNgayNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayNopThueDanhChoVATHangHoaDacBiet", SqlDbType.Decimal, "SoNgayNopThueDanhChoVATHangHoaDacBiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "HienThiTongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienTangGiamThueXuatNhapKhau", SqlDbType.Decimal, "TongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "MaTienTeTongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTrangToKhaiBoSung", SqlDbType.Decimal, "TongSoTrangToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoDongHangToKhaiBoSung", SqlDbType.Decimal, "TongSoDongHangToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDo", SqlDbType.NVarChar, "LyDo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiPhuTrach", SqlDbType.NVarChar, "TenNguoiPhuTrach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyDuLieu", SqlDbType.DateTime, "NgayDangKyDuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioDangKyDuLieu", SqlDbType.DateTime, "GioDangKyDuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung VALUES(@TKMD_ID, @SoToKhaiBoSung, @CoQuanHaiQuan, @NhomXuLyHoSo, @NhomXuLyHoSoID, @PhanLoaiXuatNhapKhau, @SoToKhai, @MaLoaiHinh, @NgayKhaiBao, @NgayCapPhep, @ThoiHanTaiNhapTaiXuat, @MaNguoiKhai, @TenNguoiKhai, @MaBuuChinh, @DiaChiNguoiKhai, @SoDienThoaiNguoiKhai, @MaLyDoKhaiBoSung, @MaTienTeTienThue, @MaNganHangTraThueThay, @NamPhatHanhHanMuc, @KiHieuChungTuPhatHanhHanMuc, @SoChungTuPhatHanhHanMuc, @MaXacDinhThoiHanNopThue, @MaNganHangBaoLanh, @NamPhatHanhBaoLanh, @KyHieuPhatHanhChungTuBaoLanh, @SoHieuPhatHanhChungTuBaoLanh, @MaTienTeTruocKhiKhaiBoSung, @TyGiaHoiDoaiTruocKhiKhaiBoSung, @MaTienTeSauKhiKhaiBoSung, @TyGiaHoiDoaiSauKhiKhaiBoSung, @SoQuanLyTrongNoiBoDoanhNghiep, @GhiChuNoiDungLienQuanTruocKhiKhaiBoSung, @GhiChuNoiDungLienQuanSauKhiKhaiBoSung, @SoTiepNhan, @NgayTiepNhan, @PhanLuong, @HuongDan, @Notes, @InputMessageID, @MessageTag, @IndexTag, @SoThongBao, @NgayHoanThanhKiemTra, @GioHoanThanhKiemTra, @NgayDangKyKhaiBoSung, @GioDangKyKhaiBoSung, @DauHieuBaoQua60Ngay, @MaHetThoiHan, @MaDaiLyHaiQuan, @TenDaiLyHaiQuan, @MaNhanVienHaiQuan, @PhanLoaiNopThue, @NgayHieuLucChungTu, @SoNgayNopThue, @SoNgayNopThueDanhChoVATHangHoaDacBiet, @HienThiTongSoTienTangGiamThueXuatNhapKhau, @TongSoTienTangGiamThueXuatNhapKhau, @MaTienTeTongSoTienTangGiamThueXuatNhapKhau, @TongSoTrangToKhaiBoSung, @TongSoDongHangToKhaiBoSung, @LyDo, @TenNguoiPhuTrach, @TenTruongDonViHaiQuan, @NgayDangKyDuLieu, @GioDangKyDuLieu, @TrangThaiXuLy)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung SET TKMD_ID = @TKMD_ID, SoToKhaiBoSung = @SoToKhaiBoSung, CoQuanHaiQuan = @CoQuanHaiQuan, NhomXuLyHoSo = @NhomXuLyHoSo, NhomXuLyHoSoID = @NhomXuLyHoSoID, PhanLoaiXuatNhapKhau = @PhanLoaiXuatNhapKhau, SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, NgayKhaiBao = @NgayKhaiBao, NgayCapPhep = @NgayCapPhep, ThoiHanTaiNhapTaiXuat = @ThoiHanTaiNhapTaiXuat, MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, MaBuuChinh = @MaBuuChinh, DiaChiNguoiKhai = @DiaChiNguoiKhai, SoDienThoaiNguoiKhai = @SoDienThoaiNguoiKhai, MaLyDoKhaiBoSung = @MaLyDoKhaiBoSung, MaTienTeTienThue = @MaTienTeTienThue, MaNganHangTraThueThay = @MaNganHangTraThueThay, NamPhatHanhHanMuc = @NamPhatHanhHanMuc, KiHieuChungTuPhatHanhHanMuc = @KiHieuChungTuPhatHanhHanMuc, SoChungTuPhatHanhHanMuc = @SoChungTuPhatHanhHanMuc, MaXacDinhThoiHanNopThue = @MaXacDinhThoiHanNopThue, MaNganHangBaoLanh = @MaNganHangBaoLanh, NamPhatHanhBaoLanh = @NamPhatHanhBaoLanh, KyHieuPhatHanhChungTuBaoLanh = @KyHieuPhatHanhChungTuBaoLanh, SoHieuPhatHanhChungTuBaoLanh = @SoHieuPhatHanhChungTuBaoLanh, MaTienTeTruocKhiKhaiBoSung = @MaTienTeTruocKhiKhaiBoSung, TyGiaHoiDoaiTruocKhiKhaiBoSung = @TyGiaHoiDoaiTruocKhiKhaiBoSung, MaTienTeSauKhiKhaiBoSung = @MaTienTeSauKhiKhaiBoSung, TyGiaHoiDoaiSauKhiKhaiBoSung = @TyGiaHoiDoaiSauKhiKhaiBoSung, SoQuanLyTrongNoiBoDoanhNghiep = @SoQuanLyTrongNoiBoDoanhNghiep, GhiChuNoiDungLienQuanTruocKhiKhaiBoSung = @GhiChuNoiDungLienQuanTruocKhiKhaiBoSung, GhiChuNoiDungLienQuanSauKhiKhaiBoSung = @GhiChuNoiDungLienQuanSauKhiKhaiBoSung, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, PhanLuong = @PhanLuong, HuongDan = @HuongDan, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, SoThongBao = @SoThongBao, NgayHoanThanhKiemTra = @NgayHoanThanhKiemTra, GioHoanThanhKiemTra = @GioHoanThanhKiemTra, NgayDangKyKhaiBoSung = @NgayDangKyKhaiBoSung, GioDangKyKhaiBoSung = @GioDangKyKhaiBoSung, DauHieuBaoQua60Ngay = @DauHieuBaoQua60Ngay, MaHetThoiHan = @MaHetThoiHan, MaDaiLyHaiQuan = @MaDaiLyHaiQuan, TenDaiLyHaiQuan = @TenDaiLyHaiQuan, MaNhanVienHaiQuan = @MaNhanVienHaiQuan, PhanLoaiNopThue = @PhanLoaiNopThue, NgayHieuLucChungTu = @NgayHieuLucChungTu, SoNgayNopThue = @SoNgayNopThue, SoNgayNopThueDanhChoVATHangHoaDacBiet = @SoNgayNopThueDanhChoVATHangHoaDacBiet, HienThiTongSoTienTangGiamThueXuatNhapKhau = @HienThiTongSoTienTangGiamThueXuatNhapKhau, TongSoTienTangGiamThueXuatNhapKhau = @TongSoTienTangGiamThueXuatNhapKhau, MaTienTeTongSoTienTangGiamThueXuatNhapKhau = @MaTienTeTongSoTienTangGiamThueXuatNhapKhau, TongSoTrangToKhaiBoSung = @TongSoTrangToKhaiBoSung, TongSoDongHangToKhaiBoSung = @TongSoDongHangToKhaiBoSung, LyDo = @LyDo, TenNguoiPhuTrach = @TenNguoiPhuTrach, TenTruongDonViHaiQuan = @TenTruongDonViHaiQuan, NgayDangKyDuLieu = @NgayDangKyDuLieu, GioDangKyDuLieu = @GioDangKyDuLieu, TrangThaiXuLy = @TrangThaiXuLy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiBoSung", SqlDbType.Decimal, "SoToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, "NhomXuLyHoSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHoSoID", SqlDbType.Int, "NhomXuLyHoSoID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, "PhanLoaiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, "ThoiHanTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLyDoKhaiBoSung", SqlDbType.VarChar, "MaLyDoKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeTienThue", SqlDbType.VarChar, "MaTienTeTienThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangTraThueThay", SqlDbType.VarChar, "MaNganHangTraThueThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhHanMuc", SqlDbType.Decimal, "NamPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTuPhatHanhHanMuc", SqlDbType.VarChar, "SoChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaXacDinhThoiHanNopThue", SqlDbType.VarChar, "MaXacDinhThoiHanNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanhBaoLanh", SqlDbType.Decimal, "NamPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KyHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, "KyHieuPhatHanhChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, "SoHieuPhatHanhChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaTienTeTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaHoiDoaiTruocKhiKhaiBoSung", SqlDbType.Decimal, "TyGiaHoiDoaiTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeSauKhiKhaiBoSung", SqlDbType.VarChar, "MaTienTeSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaHoiDoaiSauKhiKhaiBoSung", SqlDbType.Decimal, "TyGiaHoiDoaiSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.NVarChar, "SoQuanLyTrongNoiBoDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", SqlDbType.NVarChar, "GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuNoiDungLienQuanSauKhiKhaiBoSung", SqlDbType.NVarChar, "GhiChuNoiDungLienQuanSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongDan", SqlDbType.NVarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThongBao", SqlDbType.Decimal, "SoThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioHoanThanhKiemTra", SqlDbType.DateTime, "GioHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyKhaiBoSung", SqlDbType.DateTime, "NgayDangKyKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioDangKyKhaiBoSung", SqlDbType.DateTime, "GioDangKyKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DauHieuBaoQua60Ngay", SqlDbType.VarChar, "DauHieuBaoQua60Ngay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHetThoiHan", SqlDbType.VarChar, "MaHetThoiHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLyHaiQuan", SqlDbType.VarChar, "MaDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDaiLyHaiQuan", SqlDbType.NVarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHieuLucChungTu", SqlDbType.DateTime, "NgayHieuLucChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayNopThue", SqlDbType.Decimal, "SoNgayNopThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNgayNopThueDanhChoVATHangHoaDacBiet", SqlDbType.Decimal, "SoNgayNopThueDanhChoVATHangHoaDacBiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "HienThiTongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTienTangGiamThueXuatNhapKhau", SqlDbType.Decimal, "TongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTeTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "MaTienTeTongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoTrangToKhaiBoSung", SqlDbType.Decimal, "TongSoTrangToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoDongHangToKhaiBoSung", SqlDbType.Decimal, "TongSoDongHangToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDo", SqlDbType.NVarChar, "LyDo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiPhuTrach", SqlDbType.NVarChar, "TenNguoiPhuTrach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyDuLieu", SqlDbType.DateTime, "NgayDangKyDuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GioDangKyDuLieu", SqlDbType.DateTime, "GioDangKyDuLieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiBoSung", SqlDbType.Decimal, "SoToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, "NhomXuLyHoSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHoSoID", SqlDbType.Int, "NhomXuLyHoSoID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, "PhanLoaiXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, "ThoiHanTaiNhapTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinh", SqlDbType.VarChar, "MaBuuChinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLyDoKhaiBoSung", SqlDbType.VarChar, "MaLyDoKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeTienThue", SqlDbType.VarChar, "MaTienTeTienThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangTraThueThay", SqlDbType.VarChar, "MaNganHangTraThueThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhHanMuc", SqlDbType.Decimal, "NamPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTuPhatHanhHanMuc", SqlDbType.VarChar, "SoChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaXacDinhThoiHanNopThue", SqlDbType.VarChar, "MaXacDinhThoiHanNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, "MaNganHangBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanhBaoLanh", SqlDbType.Decimal, "NamPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KyHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, "KyHieuPhatHanhChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, "SoHieuPhatHanhChungTuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeTruocKhiKhaiBoSung", SqlDbType.VarChar, "MaTienTeTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaHoiDoaiTruocKhiKhaiBoSung", SqlDbType.Decimal, "TyGiaHoiDoaiTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeSauKhiKhaiBoSung", SqlDbType.VarChar, "MaTienTeSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaHoiDoaiSauKhiKhaiBoSung", SqlDbType.Decimal, "TyGiaHoiDoaiSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.NVarChar, "SoQuanLyTrongNoiBoDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", SqlDbType.NVarChar, "GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuNoiDungLienQuanSauKhiKhaiBoSung", SqlDbType.NVarChar, "GhiChuNoiDungLienQuanSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongDan", SqlDbType.NVarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThongBao", SqlDbType.Decimal, "SoThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, "NgayHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioHoanThanhKiemTra", SqlDbType.DateTime, "GioHoanThanhKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyKhaiBoSung", SqlDbType.DateTime, "NgayDangKyKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioDangKyKhaiBoSung", SqlDbType.DateTime, "GioDangKyKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DauHieuBaoQua60Ngay", SqlDbType.VarChar, "DauHieuBaoQua60Ngay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHetThoiHan", SqlDbType.VarChar, "MaHetThoiHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLyHaiQuan", SqlDbType.VarChar, "MaDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDaiLyHaiQuan", SqlDbType.NVarChar, "TenDaiLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, "MaNhanVienHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, "PhanLoaiNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHieuLucChungTu", SqlDbType.DateTime, "NgayHieuLucChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayNopThue", SqlDbType.Decimal, "SoNgayNopThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNgayNopThueDanhChoVATHangHoaDacBiet", SqlDbType.Decimal, "SoNgayNopThueDanhChoVATHangHoaDacBiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "HienThiTongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTienTangGiamThueXuatNhapKhau", SqlDbType.Decimal, "TongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTeTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, "MaTienTeTongSoTienTangGiamThueXuatNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoTrangToKhaiBoSung", SqlDbType.Decimal, "TongSoTrangToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoDongHangToKhaiBoSung", SqlDbType.Decimal, "TongSoDongHangToKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDo", SqlDbType.NVarChar, "LyDo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiPhuTrach", SqlDbType.NVarChar, "TenNguoiPhuTrach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, "TenTruongDonViHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyDuLieu", SqlDbType.DateTime, "NgayDangKyDuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GioDangKyDuLieu", SqlDbType.DateTime, "GioDangKyDuLieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ToKhaiMauDich_KhaiBoSung Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicFull(string whereCondition, string orderByExpression)
        {
            const string spName = "p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamicFull";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        public static DataSet ThongKeToKhaiAMA(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_ThongKeToKhaiAMA]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiMauDich_KhaiBoSung(long tKMD_ID, decimal soToKhaiBoSung, string coQuanHaiQuan, string nhomXuLyHoSo, int nhomXuLyHoSoID, string phanLoaiXuatNhapKhau, decimal soToKhai, string maLoaiHinh, DateTime ngayKhaiBao, DateTime ngayCapPhep, DateTime thoiHanTaiNhapTaiXuat, string maNguoiKhai, string tenNguoiKhai, string maBuuChinh, string diaChiNguoiKhai, string soDienThoaiNguoiKhai, string maLyDoKhaiBoSung, string maTienTeTienThue, string maNganHangTraThueThay, decimal namPhatHanhHanMuc, string kiHieuChungTuPhatHanhHanMuc, string soChungTuPhatHanhHanMuc, string maXacDinhThoiHanNopThue, string maNganHangBaoLanh, decimal namPhatHanhBaoLanh, string kyHieuPhatHanhChungTuBaoLanh, string soHieuPhatHanhChungTuBaoLanh, string maTienTeTruocKhiKhaiBoSung, decimal tyGiaHoiDoaiTruocKhiKhaiBoSung, string maTienTeSauKhiKhaiBoSung, decimal tyGiaHoiDoaiSauKhiKhaiBoSung, string soQuanLyTrongNoiBoDoanhNghiep, string ghiChuNoiDungLienQuanTruocKhiKhaiBoSung, string ghiChuNoiDungLienQuanSauKhiKhaiBoSung, decimal soTiepNhan, DateTime ngayTiepNhan, string phanLuong, string huongDan, string notes, string inputMessageID, string messageTag, string indexTag, decimal soThongBao, DateTime ngayHoanThanhKiemTra, DateTime gioHoanThanhKiemTra, DateTime ngayDangKyKhaiBoSung, DateTime gioDangKyKhaiBoSung, string dauHieuBaoQua60Ngay, string maHetThoiHan, string maDaiLyHaiQuan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string phanLoaiNopThue, DateTime ngayHieuLucChungTu, decimal soNgayNopThue, decimal soNgayNopThueDanhChoVATHangHoaDacBiet, string hienThiTongSoTienTangGiamThueXuatNhapKhau, decimal tongSoTienTangGiamThueXuatNhapKhau, string maTienTeTongSoTienTangGiamThueXuatNhapKhau, decimal tongSoTrangToKhaiBoSung, decimal tongSoDongHangToKhaiBoSung, string lyDo, string tenNguoiPhuTrach, string tenTruongDonViHaiQuan, DateTime ngayDangKyDuLieu, DateTime gioDangKyDuLieu, int trangThaiXuLy)
		{
			KDT_VNACC_ToKhaiMauDich_KhaiBoSung entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();	
			entity.TKMD_ID = tKMD_ID;
			entity.SoToKhaiBoSung = soToKhaiBoSung;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHoSo = nhomXuLyHoSo;
			entity.NhomXuLyHoSoID = nhomXuLyHoSoID;
			entity.PhanLoaiXuatNhapKhau = phanLoaiXuatNhapKhau;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.NgayCapPhep = ngayCapPhep;
			entity.ThoiHanTaiNhapTaiXuat = thoiHanTaiNhapTaiXuat;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.MaLyDoKhaiBoSung = maLyDoKhaiBoSung;
			entity.MaTienTeTienThue = maTienTeTienThue;
			entity.MaNganHangTraThueThay = maNganHangTraThueThay;
			entity.NamPhatHanhHanMuc = namPhatHanhHanMuc;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoChungTuPhatHanhHanMuc = soChungTuPhatHanhHanMuc;
			entity.MaXacDinhThoiHanNopThue = maXacDinhThoiHanNopThue;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.NamPhatHanhBaoLanh = namPhatHanhBaoLanh;
			entity.KyHieuPhatHanhChungTuBaoLanh = kyHieuPhatHanhChungTuBaoLanh;
			entity.SoHieuPhatHanhChungTuBaoLanh = soHieuPhatHanhChungTuBaoLanh;
			entity.MaTienTeTruocKhiKhaiBoSung = maTienTeTruocKhiKhaiBoSung;
			entity.TyGiaHoiDoaiTruocKhiKhaiBoSung = tyGiaHoiDoaiTruocKhiKhaiBoSung;
			entity.MaTienTeSauKhiKhaiBoSung = maTienTeSauKhiKhaiBoSung;
			entity.TyGiaHoiDoaiSauKhiKhaiBoSung = tyGiaHoiDoaiSauKhiKhaiBoSung;
			entity.SoQuanLyTrongNoiBoDoanhNghiep = soQuanLyTrongNoiBoDoanhNghiep;
			entity.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung = ghiChuNoiDungLienQuanTruocKhiKhaiBoSung;
			entity.GhiChuNoiDungLienQuanSauKhiKhaiBoSung = ghiChuNoiDungLienQuanSauKhiKhaiBoSung;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.SoThongBao = soThongBao;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			entity.GioHoanThanhKiemTra = gioHoanThanhKiemTra;
			entity.NgayDangKyKhaiBoSung = ngayDangKyKhaiBoSung;
			entity.GioDangKyKhaiBoSung = gioDangKyKhaiBoSung;
			entity.DauHieuBaoQua60Ngay = dauHieuBaoQua60Ngay;
			entity.MaHetThoiHan = maHetThoiHan;
			entity.MaDaiLyHaiQuan = maDaiLyHaiQuan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.NgayHieuLucChungTu = ngayHieuLucChungTu;
			entity.SoNgayNopThue = soNgayNopThue;
			entity.SoNgayNopThueDanhChoVATHangHoaDacBiet = soNgayNopThueDanhChoVATHangHoaDacBiet;
			entity.HienThiTongSoTienTangGiamThueXuatNhapKhau = hienThiTongSoTienTangGiamThueXuatNhapKhau;
			entity.TongSoTienTangGiamThueXuatNhapKhau = tongSoTienTangGiamThueXuatNhapKhau;
			entity.MaTienTeTongSoTienTangGiamThueXuatNhapKhau = maTienTeTongSoTienTangGiamThueXuatNhapKhau;
			entity.TongSoTrangToKhaiBoSung = tongSoTrangToKhaiBoSung;
			entity.TongSoDongHangToKhaiBoSung = tongSoDongHangToKhaiBoSung;
			entity.LyDo = lyDo;
			entity.TenNguoiPhuTrach = tenNguoiPhuTrach;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayDangKyDuLieu = ngayDangKyDuLieu;
			entity.GioDangKyDuLieu = gioDangKyDuLieu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoToKhaiBoSung", SqlDbType.Decimal, SoToKhaiBoSung);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, NhomXuLyHoSo);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSoID", SqlDbType.Int, NhomXuLyHoSoID);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, PhanLoaiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, ThoiHanTaiNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaLyDoKhaiBoSung", SqlDbType.VarChar, MaLyDoKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaTienTeTienThue", SqlDbType.VarChar, MaTienTeTienThue);
			db.AddInParameter(dbCommand, "@MaNganHangTraThueThay", SqlDbType.VarChar, MaNganHangTraThueThay);
			db.AddInParameter(dbCommand, "@NamPhatHanhHanMuc", SqlDbType.Decimal, NamPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoChungTuPhatHanhHanMuc", SqlDbType.VarChar, SoChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@MaXacDinhThoiHanNopThue", SqlDbType.VarChar, MaXacDinhThoiHanNopThue);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBaoLanh", SqlDbType.Decimal, NamPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@KyHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, KyHieuPhatHanhChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, SoHieuPhatHanhChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@MaTienTeTruocKhiKhaiBoSung", SqlDbType.VarChar, MaTienTeTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TyGiaHoiDoaiTruocKhiKhaiBoSung", SqlDbType.Decimal, TyGiaHoiDoaiTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaTienTeSauKhiKhaiBoSung", SqlDbType.VarChar, MaTienTeSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TyGiaHoiDoaiSauKhiKhaiBoSung", SqlDbType.Decimal, TyGiaHoiDoaiSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.NVarChar, SoQuanLyTrongNoiBoDoanhNghiep);
			db.AddInParameter(dbCommand, "@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", SqlDbType.NVarChar, GhiChuNoiDungLienQuanTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@GhiChuNoiDungLienQuanSauKhiKhaiBoSung", SqlDbType.NVarChar, GhiChuNoiDungLienQuanSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.NVarChar, HuongDan);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@SoThongBao", SqlDbType.Decimal, SoThongBao);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@GioHoanThanhKiemTra", SqlDbType.DateTime, GioHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) GioHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@NgayDangKyKhaiBoSung", SqlDbType.DateTime, NgayDangKyKhaiBoSung.Year <= 1753 ? DBNull.Value : (object) NgayDangKyKhaiBoSung);
			db.AddInParameter(dbCommand, "@GioDangKyKhaiBoSung", SqlDbType.DateTime, GioDangKyKhaiBoSung.Year <= 1753 ? DBNull.Value : (object) GioDangKyKhaiBoSung);
			db.AddInParameter(dbCommand, "@DauHieuBaoQua60Ngay", SqlDbType.VarChar, DauHieuBaoQua60Ngay);
			db.AddInParameter(dbCommand, "@MaHetThoiHan", SqlDbType.VarChar, MaHetThoiHan);
			db.AddInParameter(dbCommand, "@MaDaiLyHaiQuan", SqlDbType.VarChar, MaDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.NVarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@NgayHieuLucChungTu", SqlDbType.DateTime, NgayHieuLucChungTu.Year <= 1753 ? DBNull.Value : (object) NgayHieuLucChungTu);
			db.AddInParameter(dbCommand, "@SoNgayNopThue", SqlDbType.Decimal, SoNgayNopThue);
			db.AddInParameter(dbCommand, "@SoNgayNopThueDanhChoVATHangHoaDacBiet", SqlDbType.Decimal, SoNgayNopThueDanhChoVATHangHoaDacBiet);
			db.AddInParameter(dbCommand, "@HienThiTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, HienThiTongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TongSoTienTangGiamThueXuatNhapKhau", SqlDbType.Decimal, TongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaTienTeTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, MaTienTeTongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TongSoTrangToKhaiBoSung", SqlDbType.Decimal, TongSoTrangToKhaiBoSung);
			db.AddInParameter(dbCommand, "@TongSoDongHangToKhaiBoSung", SqlDbType.Decimal, TongSoDongHangToKhaiBoSung);
			db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, LyDo);
			db.AddInParameter(dbCommand, "@TenNguoiPhuTrach", SqlDbType.NVarChar, TenNguoiPhuTrach);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKyDuLieu", SqlDbType.DateTime, NgayDangKyDuLieu.Year <= 1753 ? DBNull.Value : (object) NgayDangKyDuLieu);
			db.AddInParameter(dbCommand, "@GioDangKyDuLieu", SqlDbType.DateTime, GioDangKyDuLieu.Year <= 1753 ? DBNull.Value : (object) GioDangKyDuLieu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiMauDich_KhaiBoSung(long id, long tKMD_ID, decimal soToKhaiBoSung, string coQuanHaiQuan, string nhomXuLyHoSo, int nhomXuLyHoSoID, string phanLoaiXuatNhapKhau, decimal soToKhai, string maLoaiHinh, DateTime ngayKhaiBao, DateTime ngayCapPhep, DateTime thoiHanTaiNhapTaiXuat, string maNguoiKhai, string tenNguoiKhai, string maBuuChinh, string diaChiNguoiKhai, string soDienThoaiNguoiKhai, string maLyDoKhaiBoSung, string maTienTeTienThue, string maNganHangTraThueThay, decimal namPhatHanhHanMuc, string kiHieuChungTuPhatHanhHanMuc, string soChungTuPhatHanhHanMuc, string maXacDinhThoiHanNopThue, string maNganHangBaoLanh, decimal namPhatHanhBaoLanh, string kyHieuPhatHanhChungTuBaoLanh, string soHieuPhatHanhChungTuBaoLanh, string maTienTeTruocKhiKhaiBoSung, decimal tyGiaHoiDoaiTruocKhiKhaiBoSung, string maTienTeSauKhiKhaiBoSung, decimal tyGiaHoiDoaiSauKhiKhaiBoSung, string soQuanLyTrongNoiBoDoanhNghiep, string ghiChuNoiDungLienQuanTruocKhiKhaiBoSung, string ghiChuNoiDungLienQuanSauKhiKhaiBoSung, decimal soTiepNhan, DateTime ngayTiepNhan, string phanLuong, string huongDan, string notes, string inputMessageID, string messageTag, string indexTag, decimal soThongBao, DateTime ngayHoanThanhKiemTra, DateTime gioHoanThanhKiemTra, DateTime ngayDangKyKhaiBoSung, DateTime gioDangKyKhaiBoSung, string dauHieuBaoQua60Ngay, string maHetThoiHan, string maDaiLyHaiQuan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string phanLoaiNopThue, DateTime ngayHieuLucChungTu, decimal soNgayNopThue, decimal soNgayNopThueDanhChoVATHangHoaDacBiet, string hienThiTongSoTienTangGiamThueXuatNhapKhau, decimal tongSoTienTangGiamThueXuatNhapKhau, string maTienTeTongSoTienTangGiamThueXuatNhapKhau, decimal tongSoTrangToKhaiBoSung, decimal tongSoDongHangToKhaiBoSung, string lyDo, string tenNguoiPhuTrach, string tenTruongDonViHaiQuan, DateTime ngayDangKyDuLieu, DateTime gioDangKyDuLieu, int trangThaiXuLy)
		{
			KDT_VNACC_ToKhaiMauDich_KhaiBoSung entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoToKhaiBoSung = soToKhaiBoSung;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHoSo = nhomXuLyHoSo;
			entity.NhomXuLyHoSoID = nhomXuLyHoSoID;
			entity.PhanLoaiXuatNhapKhau = phanLoaiXuatNhapKhau;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.NgayCapPhep = ngayCapPhep;
			entity.ThoiHanTaiNhapTaiXuat = thoiHanTaiNhapTaiXuat;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.MaLyDoKhaiBoSung = maLyDoKhaiBoSung;
			entity.MaTienTeTienThue = maTienTeTienThue;
			entity.MaNganHangTraThueThay = maNganHangTraThueThay;
			entity.NamPhatHanhHanMuc = namPhatHanhHanMuc;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoChungTuPhatHanhHanMuc = soChungTuPhatHanhHanMuc;
			entity.MaXacDinhThoiHanNopThue = maXacDinhThoiHanNopThue;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.NamPhatHanhBaoLanh = namPhatHanhBaoLanh;
			entity.KyHieuPhatHanhChungTuBaoLanh = kyHieuPhatHanhChungTuBaoLanh;
			entity.SoHieuPhatHanhChungTuBaoLanh = soHieuPhatHanhChungTuBaoLanh;
			entity.MaTienTeTruocKhiKhaiBoSung = maTienTeTruocKhiKhaiBoSung;
			entity.TyGiaHoiDoaiTruocKhiKhaiBoSung = tyGiaHoiDoaiTruocKhiKhaiBoSung;
			entity.MaTienTeSauKhiKhaiBoSung = maTienTeSauKhiKhaiBoSung;
			entity.TyGiaHoiDoaiSauKhiKhaiBoSung = tyGiaHoiDoaiSauKhiKhaiBoSung;
			entity.SoQuanLyTrongNoiBoDoanhNghiep = soQuanLyTrongNoiBoDoanhNghiep;
			entity.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung = ghiChuNoiDungLienQuanTruocKhiKhaiBoSung;
			entity.GhiChuNoiDungLienQuanSauKhiKhaiBoSung = ghiChuNoiDungLienQuanSauKhiKhaiBoSung;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.SoThongBao = soThongBao;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			entity.GioHoanThanhKiemTra = gioHoanThanhKiemTra;
			entity.NgayDangKyKhaiBoSung = ngayDangKyKhaiBoSung;
			entity.GioDangKyKhaiBoSung = gioDangKyKhaiBoSung;
			entity.DauHieuBaoQua60Ngay = dauHieuBaoQua60Ngay;
			entity.MaHetThoiHan = maHetThoiHan;
			entity.MaDaiLyHaiQuan = maDaiLyHaiQuan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.NgayHieuLucChungTu = ngayHieuLucChungTu;
			entity.SoNgayNopThue = soNgayNopThue;
			entity.SoNgayNopThueDanhChoVATHangHoaDacBiet = soNgayNopThueDanhChoVATHangHoaDacBiet;
			entity.HienThiTongSoTienTangGiamThueXuatNhapKhau = hienThiTongSoTienTangGiamThueXuatNhapKhau;
			entity.TongSoTienTangGiamThueXuatNhapKhau = tongSoTienTangGiamThueXuatNhapKhau;
			entity.MaTienTeTongSoTienTangGiamThueXuatNhapKhau = maTienTeTongSoTienTangGiamThueXuatNhapKhau;
			entity.TongSoTrangToKhaiBoSung = tongSoTrangToKhaiBoSung;
			entity.TongSoDongHangToKhaiBoSung = tongSoDongHangToKhaiBoSung;
			entity.LyDo = lyDo;
			entity.TenNguoiPhuTrach = tenNguoiPhuTrach;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayDangKyDuLieu = ngayDangKyDuLieu;
			entity.GioDangKyDuLieu = gioDangKyDuLieu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoToKhaiBoSung", SqlDbType.Decimal, SoToKhaiBoSung);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, NhomXuLyHoSo);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSoID", SqlDbType.Int, NhomXuLyHoSoID);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, PhanLoaiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, ThoiHanTaiNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaLyDoKhaiBoSung", SqlDbType.VarChar, MaLyDoKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaTienTeTienThue", SqlDbType.VarChar, MaTienTeTienThue);
			db.AddInParameter(dbCommand, "@MaNganHangTraThueThay", SqlDbType.VarChar, MaNganHangTraThueThay);
			db.AddInParameter(dbCommand, "@NamPhatHanhHanMuc", SqlDbType.Decimal, NamPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoChungTuPhatHanhHanMuc", SqlDbType.VarChar, SoChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@MaXacDinhThoiHanNopThue", SqlDbType.VarChar, MaXacDinhThoiHanNopThue);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBaoLanh", SqlDbType.Decimal, NamPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@KyHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, KyHieuPhatHanhChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, SoHieuPhatHanhChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@MaTienTeTruocKhiKhaiBoSung", SqlDbType.VarChar, MaTienTeTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TyGiaHoiDoaiTruocKhiKhaiBoSung", SqlDbType.Decimal, TyGiaHoiDoaiTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaTienTeSauKhiKhaiBoSung", SqlDbType.VarChar, MaTienTeSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TyGiaHoiDoaiSauKhiKhaiBoSung", SqlDbType.Decimal, TyGiaHoiDoaiSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.NVarChar, SoQuanLyTrongNoiBoDoanhNghiep);
			db.AddInParameter(dbCommand, "@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", SqlDbType.NVarChar, GhiChuNoiDungLienQuanTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@GhiChuNoiDungLienQuanSauKhiKhaiBoSung", SqlDbType.NVarChar, GhiChuNoiDungLienQuanSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.NVarChar, HuongDan);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@SoThongBao", SqlDbType.Decimal, SoThongBao);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@GioHoanThanhKiemTra", SqlDbType.DateTime, GioHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) GioHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@NgayDangKyKhaiBoSung", SqlDbType.DateTime, NgayDangKyKhaiBoSung.Year <= 1753 ? DBNull.Value : (object) NgayDangKyKhaiBoSung);
			db.AddInParameter(dbCommand, "@GioDangKyKhaiBoSung", SqlDbType.DateTime, GioDangKyKhaiBoSung.Year <= 1753 ? DBNull.Value : (object) GioDangKyKhaiBoSung);
			db.AddInParameter(dbCommand, "@DauHieuBaoQua60Ngay", SqlDbType.VarChar, DauHieuBaoQua60Ngay);
			db.AddInParameter(dbCommand, "@MaHetThoiHan", SqlDbType.VarChar, MaHetThoiHan);
			db.AddInParameter(dbCommand, "@MaDaiLyHaiQuan", SqlDbType.VarChar, MaDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.NVarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@NgayHieuLucChungTu", SqlDbType.DateTime, NgayHieuLucChungTu.Year <= 1753 ? DBNull.Value : (object) NgayHieuLucChungTu);
			db.AddInParameter(dbCommand, "@SoNgayNopThue", SqlDbType.Decimal, SoNgayNopThue);
			db.AddInParameter(dbCommand, "@SoNgayNopThueDanhChoVATHangHoaDacBiet", SqlDbType.Decimal, SoNgayNopThueDanhChoVATHangHoaDacBiet);
			db.AddInParameter(dbCommand, "@HienThiTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, HienThiTongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TongSoTienTangGiamThueXuatNhapKhau", SqlDbType.Decimal, TongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaTienTeTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, MaTienTeTongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TongSoTrangToKhaiBoSung", SqlDbType.Decimal, TongSoTrangToKhaiBoSung);
			db.AddInParameter(dbCommand, "@TongSoDongHangToKhaiBoSung", SqlDbType.Decimal, TongSoDongHangToKhaiBoSung);
			db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, LyDo);
			db.AddInParameter(dbCommand, "@TenNguoiPhuTrach", SqlDbType.NVarChar, TenNguoiPhuTrach);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKyDuLieu", SqlDbType.DateTime, NgayDangKyDuLieu.Year <= 1753 ? DBNull.Value : (object) NgayDangKyDuLieu);
			db.AddInParameter(dbCommand, "@GioDangKyDuLieu", SqlDbType.DateTime, GioDangKyDuLieu.Year <= 1753 ? DBNull.Value : (object) GioDangKyDuLieu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiMauDich_KhaiBoSung(long id, long tKMD_ID, decimal soToKhaiBoSung, string coQuanHaiQuan, string nhomXuLyHoSo, int nhomXuLyHoSoID, string phanLoaiXuatNhapKhau, decimal soToKhai, string maLoaiHinh, DateTime ngayKhaiBao, DateTime ngayCapPhep, DateTime thoiHanTaiNhapTaiXuat, string maNguoiKhai, string tenNguoiKhai, string maBuuChinh, string diaChiNguoiKhai, string soDienThoaiNguoiKhai, string maLyDoKhaiBoSung, string maTienTeTienThue, string maNganHangTraThueThay, decimal namPhatHanhHanMuc, string kiHieuChungTuPhatHanhHanMuc, string soChungTuPhatHanhHanMuc, string maXacDinhThoiHanNopThue, string maNganHangBaoLanh, decimal namPhatHanhBaoLanh, string kyHieuPhatHanhChungTuBaoLanh, string soHieuPhatHanhChungTuBaoLanh, string maTienTeTruocKhiKhaiBoSung, decimal tyGiaHoiDoaiTruocKhiKhaiBoSung, string maTienTeSauKhiKhaiBoSung, decimal tyGiaHoiDoaiSauKhiKhaiBoSung, string soQuanLyTrongNoiBoDoanhNghiep, string ghiChuNoiDungLienQuanTruocKhiKhaiBoSung, string ghiChuNoiDungLienQuanSauKhiKhaiBoSung, decimal soTiepNhan, DateTime ngayTiepNhan, string phanLuong, string huongDan, string notes, string inputMessageID, string messageTag, string indexTag, decimal soThongBao, DateTime ngayHoanThanhKiemTra, DateTime gioHoanThanhKiemTra, DateTime ngayDangKyKhaiBoSung, DateTime gioDangKyKhaiBoSung, string dauHieuBaoQua60Ngay, string maHetThoiHan, string maDaiLyHaiQuan, string tenDaiLyHaiQuan, string maNhanVienHaiQuan, string phanLoaiNopThue, DateTime ngayHieuLucChungTu, decimal soNgayNopThue, decimal soNgayNopThueDanhChoVATHangHoaDacBiet, string hienThiTongSoTienTangGiamThueXuatNhapKhau, decimal tongSoTienTangGiamThueXuatNhapKhau, string maTienTeTongSoTienTangGiamThueXuatNhapKhau, decimal tongSoTrangToKhaiBoSung, decimal tongSoDongHangToKhaiBoSung, string lyDo, string tenNguoiPhuTrach, string tenTruongDonViHaiQuan, DateTime ngayDangKyDuLieu, DateTime gioDangKyDuLieu, int trangThaiXuLy)
		{
			KDT_VNACC_ToKhaiMauDich_KhaiBoSung entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoToKhaiBoSung = soToKhaiBoSung;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHoSo = nhomXuLyHoSo;
			entity.NhomXuLyHoSoID = nhomXuLyHoSoID;
			entity.PhanLoaiXuatNhapKhau = phanLoaiXuatNhapKhau;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.NgayCapPhep = ngayCapPhep;
			entity.ThoiHanTaiNhapTaiXuat = thoiHanTaiNhapTaiXuat;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaBuuChinh = maBuuChinh;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.MaLyDoKhaiBoSung = maLyDoKhaiBoSung;
			entity.MaTienTeTienThue = maTienTeTienThue;
			entity.MaNganHangTraThueThay = maNganHangTraThueThay;
			entity.NamPhatHanhHanMuc = namPhatHanhHanMuc;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoChungTuPhatHanhHanMuc = soChungTuPhatHanhHanMuc;
			entity.MaXacDinhThoiHanNopThue = maXacDinhThoiHanNopThue;
			entity.MaNganHangBaoLanh = maNganHangBaoLanh;
			entity.NamPhatHanhBaoLanh = namPhatHanhBaoLanh;
			entity.KyHieuPhatHanhChungTuBaoLanh = kyHieuPhatHanhChungTuBaoLanh;
			entity.SoHieuPhatHanhChungTuBaoLanh = soHieuPhatHanhChungTuBaoLanh;
			entity.MaTienTeTruocKhiKhaiBoSung = maTienTeTruocKhiKhaiBoSung;
			entity.TyGiaHoiDoaiTruocKhiKhaiBoSung = tyGiaHoiDoaiTruocKhiKhaiBoSung;
			entity.MaTienTeSauKhiKhaiBoSung = maTienTeSauKhiKhaiBoSung;
			entity.TyGiaHoiDoaiSauKhiKhaiBoSung = tyGiaHoiDoaiSauKhiKhaiBoSung;
			entity.SoQuanLyTrongNoiBoDoanhNghiep = soQuanLyTrongNoiBoDoanhNghiep;
			entity.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung = ghiChuNoiDungLienQuanTruocKhiKhaiBoSung;
			entity.GhiChuNoiDungLienQuanSauKhiKhaiBoSung = ghiChuNoiDungLienQuanSauKhiKhaiBoSung;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.SoThongBao = soThongBao;
			entity.NgayHoanThanhKiemTra = ngayHoanThanhKiemTra;
			entity.GioHoanThanhKiemTra = gioHoanThanhKiemTra;
			entity.NgayDangKyKhaiBoSung = ngayDangKyKhaiBoSung;
			entity.GioDangKyKhaiBoSung = gioDangKyKhaiBoSung;
			entity.DauHieuBaoQua60Ngay = dauHieuBaoQua60Ngay;
			entity.MaHetThoiHan = maHetThoiHan;
			entity.MaDaiLyHaiQuan = maDaiLyHaiQuan;
			entity.TenDaiLyHaiQuan = tenDaiLyHaiQuan;
			entity.MaNhanVienHaiQuan = maNhanVienHaiQuan;
			entity.PhanLoaiNopThue = phanLoaiNopThue;
			entity.NgayHieuLucChungTu = ngayHieuLucChungTu;
			entity.SoNgayNopThue = soNgayNopThue;
			entity.SoNgayNopThueDanhChoVATHangHoaDacBiet = soNgayNopThueDanhChoVATHangHoaDacBiet;
			entity.HienThiTongSoTienTangGiamThueXuatNhapKhau = hienThiTongSoTienTangGiamThueXuatNhapKhau;
			entity.TongSoTienTangGiamThueXuatNhapKhau = tongSoTienTangGiamThueXuatNhapKhau;
			entity.MaTienTeTongSoTienTangGiamThueXuatNhapKhau = maTienTeTongSoTienTangGiamThueXuatNhapKhau;
			entity.TongSoTrangToKhaiBoSung = tongSoTrangToKhaiBoSung;
			entity.TongSoDongHangToKhaiBoSung = tongSoDongHangToKhaiBoSung;
			entity.LyDo = lyDo;
			entity.TenNguoiPhuTrach = tenNguoiPhuTrach;
			entity.TenTruongDonViHaiQuan = tenTruongDonViHaiQuan;
			entity.NgayDangKyDuLieu = ngayDangKyDuLieu;
			entity.GioDangKyDuLieu = gioDangKyDuLieu;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoToKhaiBoSung", SqlDbType.Decimal, SoToKhaiBoSung);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, NhomXuLyHoSo);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSoID", SqlDbType.Int, NhomXuLyHoSoID);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhapKhau", SqlDbType.VarChar, PhanLoaiXuatNhapKhau);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@ThoiHanTaiNhapTaiXuat", SqlDbType.DateTime, ThoiHanTaiNhapTaiXuat.Year <= 1753 ? DBNull.Value : (object) ThoiHanTaiNhapTaiXuat);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinh", SqlDbType.VarChar, MaBuuChinh);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaLyDoKhaiBoSung", SqlDbType.VarChar, MaLyDoKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaTienTeTienThue", SqlDbType.VarChar, MaTienTeTienThue);
			db.AddInParameter(dbCommand, "@MaNganHangTraThueThay", SqlDbType.VarChar, MaNganHangTraThueThay);
			db.AddInParameter(dbCommand, "@NamPhatHanhHanMuc", SqlDbType.Decimal, NamPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoChungTuPhatHanhHanMuc", SqlDbType.VarChar, SoChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@MaXacDinhThoiHanNopThue", SqlDbType.VarChar, MaXacDinhThoiHanNopThue);
			db.AddInParameter(dbCommand, "@MaNganHangBaoLanh", SqlDbType.VarChar, MaNganHangBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanhBaoLanh", SqlDbType.Decimal, NamPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@KyHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, KyHieuPhatHanhChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhChungTuBaoLanh", SqlDbType.VarChar, SoHieuPhatHanhChungTuBaoLanh);
			db.AddInParameter(dbCommand, "@MaTienTeTruocKhiKhaiBoSung", SqlDbType.VarChar, MaTienTeTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TyGiaHoiDoaiTruocKhiKhaiBoSung", SqlDbType.Decimal, TyGiaHoiDoaiTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@MaTienTeSauKhiKhaiBoSung", SqlDbType.VarChar, MaTienTeSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@TyGiaHoiDoaiSauKhiKhaiBoSung", SqlDbType.Decimal, TyGiaHoiDoaiSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.NVarChar, SoQuanLyTrongNoiBoDoanhNghiep);
			db.AddInParameter(dbCommand, "@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung", SqlDbType.NVarChar, GhiChuNoiDungLienQuanTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@GhiChuNoiDungLienQuanSauKhiKhaiBoSung", SqlDbType.NVarChar, GhiChuNoiDungLienQuanSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.NVarChar, HuongDan);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@SoThongBao", SqlDbType.Decimal, SoThongBao);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTra", SqlDbType.DateTime, NgayHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@GioHoanThanhKiemTra", SqlDbType.DateTime, GioHoanThanhKiemTra.Year <= 1753 ? DBNull.Value : (object) GioHoanThanhKiemTra);
			db.AddInParameter(dbCommand, "@NgayDangKyKhaiBoSung", SqlDbType.DateTime, NgayDangKyKhaiBoSung.Year <= 1753 ? DBNull.Value : (object) NgayDangKyKhaiBoSung);
			db.AddInParameter(dbCommand, "@GioDangKyKhaiBoSung", SqlDbType.DateTime, GioDangKyKhaiBoSung.Year <= 1753 ? DBNull.Value : (object) GioDangKyKhaiBoSung);
			db.AddInParameter(dbCommand, "@DauHieuBaoQua60Ngay", SqlDbType.VarChar, DauHieuBaoQua60Ngay);
			db.AddInParameter(dbCommand, "@MaHetThoiHan", SqlDbType.VarChar, MaHetThoiHan);
			db.AddInParameter(dbCommand, "@MaDaiLyHaiQuan", SqlDbType.VarChar, MaDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@TenDaiLyHaiQuan", SqlDbType.NVarChar, TenDaiLyHaiQuan);
			db.AddInParameter(dbCommand, "@MaNhanVienHaiQuan", SqlDbType.VarChar, MaNhanVienHaiQuan);
			db.AddInParameter(dbCommand, "@PhanLoaiNopThue", SqlDbType.VarChar, PhanLoaiNopThue);
			db.AddInParameter(dbCommand, "@NgayHieuLucChungTu", SqlDbType.DateTime, NgayHieuLucChungTu.Year <= 1753 ? DBNull.Value : (object) NgayHieuLucChungTu);
			db.AddInParameter(dbCommand, "@SoNgayNopThue", SqlDbType.Decimal, SoNgayNopThue);
			db.AddInParameter(dbCommand, "@SoNgayNopThueDanhChoVATHangHoaDacBiet", SqlDbType.Decimal, SoNgayNopThueDanhChoVATHangHoaDacBiet);
			db.AddInParameter(dbCommand, "@HienThiTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, HienThiTongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TongSoTienTangGiamThueXuatNhapKhau", SqlDbType.Decimal, TongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@MaTienTeTongSoTienTangGiamThueXuatNhapKhau", SqlDbType.VarChar, MaTienTeTongSoTienTangGiamThueXuatNhapKhau);
			db.AddInParameter(dbCommand, "@TongSoTrangToKhaiBoSung", SqlDbType.Decimal, TongSoTrangToKhaiBoSung);
			db.AddInParameter(dbCommand, "@TongSoDongHangToKhaiBoSung", SqlDbType.Decimal, TongSoDongHangToKhaiBoSung);
			db.AddInParameter(dbCommand, "@LyDo", SqlDbType.NVarChar, LyDo);
			db.AddInParameter(dbCommand, "@TenNguoiPhuTrach", SqlDbType.NVarChar, TenNguoiPhuTrach);
			db.AddInParameter(dbCommand, "@TenTruongDonViHaiQuan", SqlDbType.NVarChar, TenTruongDonViHaiQuan);
			db.AddInParameter(dbCommand, "@NgayDangKyDuLieu", SqlDbType.DateTime, NgayDangKyDuLieu.Year <= 1753 ? DBNull.Value : (object) NgayDangKyDuLieu);
			db.AddInParameter(dbCommand, "@GioDangKyDuLieu", SqlDbType.DateTime, GioDangKyDuLieu.Year <= 1753 ? DBNull.Value : (object) GioDangKyDuLieu);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiMauDich_KhaiBoSung(long id)
		{
			KDT_VNACC_ToKhaiMauDich_KhaiBoSung entity = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_KhaiBoSung item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}