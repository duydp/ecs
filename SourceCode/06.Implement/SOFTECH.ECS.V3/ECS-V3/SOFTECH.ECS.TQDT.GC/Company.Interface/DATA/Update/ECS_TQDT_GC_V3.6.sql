/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 08/01/2012 4:17:57 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_GC_HangPhuKien]'
GO
ALTER TABLE [dbo].[t_KDT_GC_HangPhuKien] ALTER COLUMN [NuocXX_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_GC_HangPhuKien] ALTER COLUMN [NguyenTe_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_GC_HangPhuKien] ALTER COLUMN [NhomSP] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_DanhSachPhuKienMuaVN]'
GO
EXEC sp_refreshview N'[dbo].[t_View_DanhSachPhuKienMuaVN]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_NPLTuCungUng]'
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
ALTER PROCEDURE [dbo].[p_NPLTuCungUng]  
@MaDoanhNghiep varchar(50),  
@HopDongID int,
@NamTN int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 -- p_NPLTuCungUng '0400101556',23,676  
 SET NOCOUNT ON;   
  
--Declare @MaDoanhNghiep varchar(50);  
--set @MaDoanhNghiep='0100101308'  
Declare @cmdSql nvarchar(4000);  
  
set @cmdSql='SELECT     t_KDT_GC_HopDong.ID AS HopDongID, t_KDT_GC_HopDong.SoHopDong, t_KDT_ToKhaiMauDich.ID AS ToKhaiID, t_KDT_ToKhaiMauDich.SoToKhai, 1 AS IsTKMD,   
                      t_KDT_GC_NPLCungUng.MaHang, t_KDT_GC_NPLCungUng.TenHang, t_KDT_GC_NPLCungUng.MaHS, t_KDT_GC_NPLCungUng.TongLuong,   
                      t_KDT_GC_NPLCungUng.DVT_ID, t_KDT_GC_HopDong.MaDoanhNghiep  
FROM         t_KDT_GC_HopDong INNER JOIN  
                      t_KDT_ToKhaiMauDich ON t_KDT_GC_HopDong.ID = t_KDT_ToKhaiMauDich.IDHopDong INNER JOIN  
                      t_KDT_GC_NPLCungUng ON t_KDT_ToKhaiMauDich.ID = t_KDT_GC_NPLCungUng.TKMDID  
WHERE     (t_KDT_GC_HopDong.MaDoanhNghiep like ''%'+@MaDoanhNghiep+'%'') AND   
                      (t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1)' 
 if(@NamTN<>0) 
 set @cmdSql =@cmdSql+ ' AND (YEAR(t_KDT_ToKhaiMauDich.NgayTiepNhan) =' +@NamTN+')';  
 IF(@HopDongID <> 0)
 SET @cmdSql = @cmdSql+ ' AND (t_KDT_GC_HopDong.ID = +'+@HopDongID+')'; 
 print @cmdSql;  
 EXEC sp_executesql @cmdSql;  

END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.6', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.6', getdate(), null)
	end 