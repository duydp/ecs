﻿namespace Company.Interface
{
    partial class VNACC_TKNhapTriGiaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TKNhapTriGiaForm));
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdInTamMIC1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTamMIC");
            this.cmdThongTinHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThongTinHQ");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdMIC1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMIC");
            this.cmdMIE1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMIE");
            this.cmdMIC2 = new Janus.Windows.UI.CommandBars.UICommand("cmdMIC");
            this.cmdMIE2 = new Janus.Windows.UI.CommandBars.UICommand("cmdMIE");
            this.cmdThongTinHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdThongTinHQ");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdInTamMIC = new Janus.Windows.UI.CommandBars.UICommand("cmdInTamMIC");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.grbDoiTac = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiDoiTac2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDiaChiDoiTac4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoiTac3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.grbDonVi = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoDienThoaiDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSoTienPhiBH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienPhiVC = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoQuanLyNoiBoDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTongTriGiaHD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaDieuKienGiaHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoHouseAWB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoMasterAWB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txtTenMayBayChoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMoTaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).BeginInit();
            this.grbDoiTac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).BeginInit();
            this.grbDonVi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 577), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 577);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 553);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 553);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox13);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox11);
            this.grbMain.Controls.Add(this.uiGroupBox8);
            this.grbMain.Controls.Add(this.uiGroupBox10);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.grbDoiTac);
            this.grbMain.Controls.Add(this.grbDonVi);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(816, 577);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdMIC2,
            this.cmdMIE2,
            this.cmdThongTinHQ,
            this.cmdKetQuaXuLy,
            this.cmdInTamMIC});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdLuu1,
            this.cmdKhaiBao1,
            this.cmdInTamMIC1,
            this.cmdThongTinHQ1,
            this.cmdKetQuaXuLy1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1022, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdInTamMIC1
            // 
            this.cmdInTamMIC1.Key = "cmdInTamMIC";
            this.cmdInTamMIC1.Name = "cmdInTamMIC1";
            // 
            // cmdThongTinHQ1
            // 
            this.cmdThongTinHQ1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThongTinHQ1.Image")));
            this.cmdThongTinHQ1.Key = "cmdThongTinHQ";
            this.cmdThongTinHQ1.Name = "cmdThongTinHQ1";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Image = ((System.Drawing.Image)(resources.GetObject("cmdLuu.Image")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdMIC1,
            this.cmdMIE1});
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdMIC1
            // 
            this.cmdMIC1.Image = ((System.Drawing.Image)(resources.GetObject("cmdMIC1.Image")));
            this.cmdMIC1.Key = "cmdMIC";
            this.cmdMIC1.Name = "cmdMIC1";
            // 
            // cmdMIE1
            // 
            this.cmdMIE1.Image = ((System.Drawing.Image)(resources.GetObject("cmdMIE1.Image")));
            this.cmdMIE1.Key = "cmdMIE";
            this.cmdMIE1.Name = "cmdMIE1";
            // 
            // cmdMIC2
            // 
            this.cmdMIC2.Image = ((System.Drawing.Image)(resources.GetObject("cmdMIC2.Image")));
            this.cmdMIC2.Key = "cmdMIC";
            this.cmdMIC2.Name = "cmdMIC2";
            this.cmdMIC2.Text = "Khai báo tạm MIC";
            // 
            // cmdMIE2
            // 
            this.cmdMIE2.Image = ((System.Drawing.Image)(resources.GetObject("cmdMIE2.Image")));
            this.cmdMIE2.Key = "cmdMIE";
            this.cmdMIE2.Name = "cmdMIE2";
            this.cmdMIE2.Text = "Khai báo chính thức MIE";
            // 
            // cmdThongTinHQ
            // 
            this.cmdThongTinHQ.Key = "cmdThongTinHQ";
            this.cmdThongTinHQ.Name = "cmdThongTinHQ";
            this.cmdThongTinHQ.Text = "Thông tin hải quan";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaXuLy.Image")));
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdInTamMIC
            // 
            this.cmdInTamMIC.Image = ((System.Drawing.Image)(resources.GetObject("cmdInTamMIC.Image")));
            this.cmdInTamMIC.Key = "cmdInTamMIC";
            this.cmdInTamMIC.Name = "cmdInTamMIC";
            this.cmdInTamMIC.Text = "In tờ khai tạm MIC";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1022, 28);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label12);
            this.uiGroupBox3.Controls.Add(this.label26);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox3.Controls.Add(this.label27);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(793, 64);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(167, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(134, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "Phân loại cá nhân /tổ chức";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(504, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 13);
            this.label26.TabIndex = 42;
            this.label26.Text = "Mã bộ phận xữ lý";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 12;
            this.txtSoToKhai.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhai.Enabled = false;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoToKhai.Location = new System.Drawing.Point(59, 9);
            this.txtSoToKhai.MaxLength = 15;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhai.Size = new System.Drawing.Size(107, 21);
            this.txtSoToKhai.TabIndex = 0;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(2, 40);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 13);
            this.label27.TabIndex = 33;
            this.label27.Text = "Cơ quan Hải quan";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Số tờ khai";
            // 
            // grbDoiTac
            // 
            this.grbDoiTac.BackColor = System.Drawing.Color.Transparent;
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac2);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac1);
            this.grbDoiTac.Controls.Add(this.txtMaDoiTac);
            this.grbDoiTac.Controls.Add(this.label6);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac4);
            this.grbDoiTac.Controls.Add(this.txtDiaChiDoiTac3);
            this.grbDoiTac.Controls.Add(this.label15);
            this.grbDoiTac.Controls.Add(this.label14);
            this.grbDoiTac.Controls.Add(this.label13);
            this.grbDoiTac.Controls.Add(this.txtMaBuuChinhDoiTac);
            this.grbDoiTac.Controls.Add(this.label16);
            this.grbDoiTac.Controls.Add(this.txtTenDoiTac);
            this.grbDoiTac.Controls.Add(this.label8);
            this.grbDoiTac.Controls.Add(this.label9);
            this.grbDoiTac.Controls.Add(this.label10);
            this.grbDoiTac.Location = new System.Drawing.Point(0, 247);
            this.grbDoiTac.Name = "grbDoiTac";
            this.grbDoiTac.Size = new System.Drawing.Size(358, 259);
            this.grbDoiTac.TabIndex = 2;
            this.grbDoiTac.Text = "Người xuất khẩu";
            this.grbDoiTac.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac2
            // 
            this.txtDiaChiDoiTac2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac2.Location = new System.Drawing.Point(75, 146);
            this.txtDiaChiDoiTac2.MaxLength = 100;
            this.txtDiaChiDoiTac2.Multiline = true;
            this.txtDiaChiDoiTac2.Name = "txtDiaChiDoiTac2";
            this.txtDiaChiDoiTac2.Size = new System.Drawing.Size(275, 40);
            this.txtDiaChiDoiTac2.TabIndex = 3;
            this.txtDiaChiDoiTac2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac1
            // 
            this.txtDiaChiDoiTac1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDoiTac1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDoiTac1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac1.Location = new System.Drawing.Point(75, 98);
            this.txtDiaChiDoiTac1.MaxLength = 100;
            this.txtDiaChiDoiTac1.Multiline = true;
            this.txtDiaChiDoiTac1.Name = "txtDiaChiDoiTac1";
            this.txtDiaChiDoiTac1.Size = new System.Drawing.Size(275, 40);
            this.txtDiaChiDoiTac1.TabIndex = 2;
            this.txtDiaChiDoiTac1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDoiTac
            // 
            this.txtMaDoiTac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDoiTac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoiTac.Location = new System.Drawing.Point(75, 17);
            this.txtMaDoiTac.MaxLength = 13;
            this.txtMaDoiTac.Name = "txtMaDoiTac";
            this.txtMaDoiTac.Size = new System.Drawing.Size(275, 21);
            this.txtMaDoiTac.TabIndex = 0;
            this.txtMaDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Địa chỉ (1)";
            // 
            // txtDiaChiDoiTac4
            // 
            this.txtDiaChiDoiTac4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac4.Location = new System.Drawing.Point(245, 228);
            this.txtDiaChiDoiTac4.MaxLength = 255;
            this.txtDiaChiDoiTac4.Name = "txtDiaChiDoiTac4";
            this.txtDiaChiDoiTac4.Size = new System.Drawing.Size(105, 21);
            this.txtDiaChiDoiTac4.TabIndex = 7;
            this.txtDiaChiDoiTac4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChiDoiTac3
            // 
            this.txtDiaChiDoiTac3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDoiTac3.Location = new System.Drawing.Point(75, 229);
            this.txtDiaChiDoiTac3.MaxLength = 255;
            this.txtDiaChiDoiTac3.Name = "txtDiaChiDoiTac3";
            this.txtDiaChiDoiTac3.Size = new System.Drawing.Size(105, 21);
            this.txtDiaChiDoiTac3.TabIndex = 6;
            this.txtDiaChiDoiTac3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDoiTac3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(194, 232);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Quốc gia";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1, 232);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Tỉnh/Thành";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Địa chỉ (2)";
            // 
            // txtMaBuuChinhDoiTac
            // 
            this.txtMaBuuChinhDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDoiTac.Location = new System.Drawing.Point(75, 197);
            this.txtMaBuuChinhDoiTac.MaxLength = 255;
            this.txtMaBuuChinhDoiTac.Name = "txtMaBuuChinhDoiTac";
            this.txtMaBuuChinhDoiTac.Size = new System.Drawing.Size(79, 21);
            this.txtMaBuuChinhDoiTac.TabIndex = 4;
            this.txtMaBuuChinhDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(194, 201);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Mã nước";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(75, 47);
            this.txtTenDoiTac.MaxLength = 100;
            this.txtTenDoiTac.Multiline = true;
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(275, 40);
            this.txtTenDoiTac.TabIndex = 1;
            this.txtTenDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Mã bưu chính";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Mã";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Tên";
            // 
            // grbDonVi
            // 
            this.grbDonVi.BackColor = System.Drawing.Color.Transparent;
            this.grbDonVi.Controls.Add(this.txtDiaChiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaDonVi);
            this.grbDonVi.Controls.Add(this.label3);
            this.grbDonVi.Controls.Add(this.txtSoDienThoaiDonVi);
            this.grbDonVi.Controls.Add(this.txtMaBuuChinhDonVi);
            this.grbDonVi.Controls.Add(this.label5);
            this.grbDonVi.Controls.Add(this.txtTenDonVi);
            this.grbDonVi.Controls.Add(this.label4);
            this.grbDonVi.Controls.Add(this.label1);
            this.grbDonVi.Controls.Add(this.label2);
            this.grbDonVi.Location = new System.Drawing.Point(0, 66);
            this.grbDonVi.Name = "grbDonVi";
            this.grbDonVi.Size = new System.Drawing.Size(358, 175);
            this.grbDonVi.TabIndex = 1;
            this.grbDonVi.Text = "Người nhập khẩu";
            this.grbDonVi.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiDonVi
            // 
            this.txtDiaChiDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDiaChiDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiaChiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiDonVi.Location = new System.Drawing.Point(72, 94);
            this.txtDiaChiDonVi.MaxLength = 100;
            this.txtDiaChiDonVi.Multiline = true;
            this.txtDiaChiDonVi.Name = "txtDiaChiDonVi";
            this.txtDiaChiDonVi.Size = new System.Drawing.Size(278, 40);
            this.txtDiaChiDonVi.TabIndex = 2;
            this.txtDiaChiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDonVi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(72, 17);
            this.txtMaDonVi.MaxLength = 13;
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.Size = new System.Drawing.Size(278, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Địa chỉ";
            // 
            // txtSoDienThoaiDonVi
            // 
            this.txtSoDienThoaiDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDienThoaiDonVi.Location = new System.Drawing.Point(255, 144);
            this.txtSoDienThoaiDonVi.MaxLength = 255;
            this.txtSoDienThoaiDonVi.Name = "txtSoDienThoaiDonVi";
            this.txtSoDienThoaiDonVi.Size = new System.Drawing.Size(95, 21);
            this.txtSoDienThoaiDonVi.TabIndex = 4;
            this.txtSoDienThoaiDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDienThoaiDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaBuuChinhDonVi
            // 
            this.txtMaBuuChinhDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBuuChinhDonVi.Location = new System.Drawing.Point(72, 144);
            this.txtMaBuuChinhDonVi.MaxLength = 255;
            this.txtMaBuuChinhDonVi.Name = "txtMaBuuChinhDonVi";
            this.txtMaBuuChinhDonVi.Size = new System.Drawing.Size(106, 21);
            this.txtMaBuuChinhDonVi.TabIndex = 3;
            this.txtMaBuuChinhDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBuuChinhDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(193, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Điện thoại";
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(72, 46);
            this.txtTenDonVi.MaxLength = 100;
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.Size = new System.Drawing.Size(278, 40);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Mã bưu chính";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Mã";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Tên";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.txtSoTienPhiBH);
            this.uiGroupBox1.Controls.Add(this.txtSoTienPhiVC);
            this.uiGroupBox1.Location = new System.Drawing.Point(365, 267);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(428, 67);
            this.uiGroupBox1.TabIndex = 8;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 77;
            this.label7.Text = "Phí bảo hiểm";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(2, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 76;
            this.label11.Text = "Phí vận chuyển";
            // 
            // txtSoTienPhiBH
            // 
            this.txtSoTienPhiBH.DecimalDigits = 20;
            this.txtSoTienPhiBH.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienPhiBH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienPhiBH.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienPhiBH.Location = new System.Drawing.Point(257, 39);
            this.txtSoTienPhiBH.MaxLength = 15;
            this.txtSoTienPhiBH.Name = "txtSoTienPhiBH";
            this.txtSoTienPhiBH.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienPhiBH.Size = new System.Drawing.Size(109, 21);
            this.txtSoTienPhiBH.TabIndex = 4;
            this.txtSoTienPhiBH.Text = "0";
            this.txtSoTienPhiBH.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienPhiBH.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienPhiBH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienPhiVC
            // 
            this.txtSoTienPhiVC.DecimalDigits = 20;
            this.txtSoTienPhiVC.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienPhiVC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienPhiVC.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienPhiVC.Location = new System.Drawing.Point(257, 12);
            this.txtSoTienPhiVC.MaxLength = 15;
            this.txtSoTienPhiVC.Name = "txtSoTienPhiVC";
            this.txtSoTienPhiVC.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienPhiVC.Size = new System.Drawing.Size(109, 21);
            this.txtSoTienPhiVC.TabIndex = 1;
            this.txtSoTienPhiVC.Text = "0";
            this.txtSoTienPhiVC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienPhiVC.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienPhiVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.txtSoQuanLyNoiBoDN);
            this.uiGroupBox11.Location = new System.Drawing.Point(0, 508);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(359, 67);
            this.uiGroupBox11.TabIndex = 12;
            this.uiGroupBox11.Text = "Số quản lý nội bộ doanh nghiệp";
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // txtSoQuanLyNoiBoDN
            // 
            this.txtSoQuanLyNoiBoDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoQuanLyNoiBoDN.Location = new System.Drawing.Point(8, 16);
            this.txtSoQuanLyNoiBoDN.MaxLength = 255;
            this.txtSoQuanLyNoiBoDN.Name = "txtSoQuanLyNoiBoDN";
            this.txtSoQuanLyNoiBoDN.Size = new System.Drawing.Size(342, 21);
            this.txtSoQuanLyNoiBoDN.TabIndex = 0;
            this.txtSoQuanLyNoiBoDN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoQuanLyNoiBoDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.txtGhiChu);
            this.uiGroupBox10.Location = new System.Drawing.Point(365, 506);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(430, 69);
            this.uiGroupBox10.TabIndex = 11;
            this.uiGroupBox10.Text = "Ghi chú";
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(6, 15);
            this.txtGhiChu.MaxLength = 100;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(418, 50);
            this.txtGhiChu.TabIndex = 0;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtTongTriGiaHD);
            this.uiGroupBox2.Controls.Add(this.txtMaDieuKienGiaHD);
            this.uiGroupBox2.Controls.Add(this.label46);
            this.uiGroupBox2.Controls.Add(this.label48);
            this.uiGroupBox2.Controls.Add(this.label49);
            this.uiGroupBox2.Location = new System.Drawing.Point(365, 336);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(430, 66);
            this.uiGroupBox2.TabIndex = 9;
            this.uiGroupBox2.Text = "Trị giá hóa đơn";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtTongTriGiaHD
            // 
            this.txtTongTriGiaHD.BackColor = System.Drawing.Color.White;
            this.txtTongTriGiaHD.DecimalDigits = 20;
            this.txtTongTriGiaHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaHD.FormatString = "G20";
            this.txtTongTriGiaHD.Location = new System.Drawing.Point(82, 38);
            this.txtTongTriGiaHD.Name = "txtTongTriGiaHD";
            this.txtTongTriGiaHD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTriGiaHD.Size = new System.Drawing.Size(172, 21);
            this.txtTongTriGiaHD.TabIndex = 2;
            this.txtTongTriGiaHD.Text = "0";
            this.txtTongTriGiaHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTriGiaHD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDieuKienGiaHD
            // 
            this.txtMaDieuKienGiaHD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDieuKienGiaHD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDieuKienGiaHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDieuKienGiaHD.Location = new System.Drawing.Point(82, 12);
            this.txtMaDieuKienGiaHD.MaxLength = 12;
            this.txtMaDieuKienGiaHD.Name = "txtMaDieuKienGiaHD";
            this.txtMaDieuKienGiaHD.Size = new System.Drawing.Size(88, 21);
            this.txtMaDieuKienGiaHD.TabIndex = 0;
            this.txtMaDieuKienGiaHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDieuKienGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(2, 43);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(61, 13);
            this.label46.TabIndex = 42;
            this.label46.Text = "Tổng trị giá";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(170, 15);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(67, 13);
            this.label48.TabIndex = 41;
            this.label48.Text = "Phân loại giá";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(2, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 43;
            this.label49.Text = "Điều kiện giá";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.txtSoLuong);
            this.uiGroupBox6.Controls.Add(this.label18);
            this.uiGroupBox6.Location = new System.Drawing.Point(365, 65);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(209, 45);
            this.uiGroupBox6.TabIndex = 3;
            this.uiGroupBox6.Text = "Số lượng";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 20;
            this.txtSoLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong.Location = new System.Drawing.Point(3, 17);
            this.txtSoLuong.MaxLength = 15;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong.Size = new System.Drawing.Size(129, 21);
            this.txtSoLuong.TabIndex = 0;
            this.txtSoLuong.Text = "0";
            this.txtSoLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(138, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Gói";
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.txtTrongLuong);
            this.uiGroupBox13.Controls.Add(this.label20);
            this.uiGroupBox13.Location = new System.Drawing.Point(580, 65);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(213, 45);
            this.uiGroupBox13.TabIndex = 4;
            this.uiGroupBox13.Text = "Tổng trọng lượng hàng";
            this.uiGroupBox13.VisualStyleManager = this.vsmMain;
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 20;
            this.txtTrongLuong.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTrongLuong.Location = new System.Drawing.Point(3, 17);
            this.txtTrongLuong.MaxLength = 15;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTrongLuong.Size = new System.Drawing.Size(129, 21);
            this.txtTrongLuong.TabIndex = 0;
            this.txtTrongLuong.Text = "0";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(140, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "KGM";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtSoHouseAWB);
            this.uiGroupBox5.Location = new System.Drawing.Point(365, 111);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(209, 45);
            this.uiGroupBox5.TabIndex = 5;
            this.uiGroupBox5.Text = "Số House AWB";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHouseAWB
            // 
            this.txtSoHouseAWB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoHouseAWB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoHouseAWB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHouseAWB.Location = new System.Drawing.Point(3, 18);
            this.txtSoHouseAWB.MaxLength = 12;
            this.txtSoHouseAWB.Name = "txtSoHouseAWB";
            this.txtSoHouseAWB.Size = new System.Drawing.Size(166, 21);
            this.txtSoHouseAWB.TabIndex = 0;
            this.txtSoHouseAWB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHouseAWB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtSoMasterAWB);
            this.uiGroupBox4.Location = new System.Drawing.Point(580, 111);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(213, 45);
            this.uiGroupBox4.TabIndex = 6;
            this.uiGroupBox4.Text = "Số Master AWB";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtSoMasterAWB
            // 
            this.txtSoMasterAWB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoMasterAWB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoMasterAWB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoMasterAWB.Location = new System.Drawing.Point(4, 18);
            this.txtSoMasterAWB.MaxLength = 12;
            this.txtSoMasterAWB.Name = "txtSoMasterAWB";
            this.txtSoMasterAWB.Size = new System.Drawing.Size(203, 21);
            this.txtSoMasterAWB.TabIndex = 0;
            this.txtSoMasterAWB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoMasterAWB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.label54);
            this.uiGroupBox7.Controls.Add(this.label53);
            this.uiGroupBox7.Controls.Add(this.label52);
            this.uiGroupBox7.Controls.Add(this.label51);
            this.uiGroupBox7.Controls.Add(this.txtTenMayBayChoHang);
            this.uiGroupBox7.Controls.Add(this.label21);
            this.uiGroupBox7.Location = new System.Drawing.Point(364, 155);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(429, 115);
            this.uiGroupBox7.TabIndex = 7;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(245, 41);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(80, 13);
            this.label54.TabIndex = 49;
            this.label54.Text = "Ngày hàng đến";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(1, 92);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(96, 13);
            this.label53.TabIndex = 45;
            this.label53.Text = "Địa điểm xếp hàng";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(1, 69);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(90, 13);
            this.label52.TabIndex = 47;
            this.label52.Text = "Địa điểm dở hàng";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(1, 17);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(179, 13);
            this.label51.TabIndex = 46;
            this.label51.Text = "Mã địa điểm lưu kho chờ thông quan";
            // 
            // txtTenMayBayChoHang
            // 
            this.txtTenMayBayChoHang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenMayBayChoHang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenMayBayChoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenMayBayChoHang.Location = new System.Drawing.Point(120, 37);
            this.txtTenMayBayChoHang.MaxLength = 12;
            this.txtTenMayBayChoHang.Name = "txtTenMayBayChoHang";
            this.txtTenMayBayChoHang.Size = new System.Drawing.Size(121, 21);
            this.txtTenMayBayChoHang.TabIndex = 1;
            this.txtTenMayBayChoHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenMayBayChoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1, 43);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(116, 13);
            this.label21.TabIndex = 42;
            this.label21.Text = "Tên máy bay cho hàng";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.txtMoTaHangHoa);
            this.uiGroupBox8.Controls.Add(this.label24);
            this.uiGroupBox8.Controls.Add(this.label23);
            this.uiGroupBox8.Controls.Add(this.label22);
            this.uiGroupBox8.Controls.Add(this.txtTriGiaTinhThue);
            this.uiGroupBox8.Location = new System.Drawing.Point(365, 403);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(430, 103);
            this.uiGroupBox8.TabIndex = 10;
            this.uiGroupBox8.VisualStyleManager = this.vsmMain;
            // 
            // txtMoTaHangHoa
            // 
            this.txtMoTaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoTaHangHoa.Location = new System.Drawing.Point(7, 28);
            this.txtMoTaHangHoa.MaxLength = 100;
            this.txtMoTaHangHoa.Multiline = true;
            this.txtMoTaHangHoa.Name = "txtMoTaHangHoa";
            this.txtMoTaHangHoa.Size = new System.Drawing.Size(417, 43);
            this.txtMoTaHangHoa.TabIndex = 0;
            this.txtMoTaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMoTaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(237, 78);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 42;
            this.label24.Text = "Trị giá tính thuế";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(4, 79);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 13);
            this.label23.TabIndex = 42;
            this.label23.Text = "Nước xuất xứ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(4, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 13);
            this.label22.TabIndex = 42;
            this.label22.Text = "Mô tả hàng hóa";
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.DecimalDigits = 20;
            this.txtTriGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(320, 74);
            this.txtTriGiaTinhThue.MaxLength = 15;
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(104, 21);
            this.txtTriGiaTinhThue.TabIndex = 2;
            this.txtTriGiaTinhThue.Text = "0";
            this.txtTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // VNACC_TKNhapTriGiaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 611);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "VNACC_TKNhapTriGiaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai nhập trị giá thấp";
            this.Load += new System.EventHandler(this.VNACC_TKNhapTriGiaForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDoiTac)).EndInit();
            this.grbDoiTac.ResumeLayout(false);
            this.grbDoiTac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).EndInit();
            this.grbDonVi.ResumeLayout(false);
            this.grbDonVi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            this.uiGroupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.EditControls.UIGroupBox grbDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoiTac;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac4;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoiTac3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDoiTac;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIGroupBox grbDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonVi;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhDonVi;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonVi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTPhiVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTPhiBH;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhiBaoHiem;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaPhiVC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienPhiBH;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienPhiVC;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrNhomXuLyHS;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaNuocDoiTac;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuanLyNoiBoDN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox10;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaNuoc;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTHoaDon;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDieuKienGiaHD;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHouseAWB;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoMasterAWB;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label54;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenMayBayChoHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMoTaHangHoa;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThue;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHangDen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrPhanLoaiGiaHD;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaDDLuuKho;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaDiaDiemDoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaDiaDiemXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrNuocXuatXu;
        private Janus.Windows.UI.CommandBars.UICommand cmdMIC1;
        private Janus.Windows.UI.CommandBars.UICommand cmdMIC2;
        private Janus.Windows.UI.CommandBars.UICommand cmdThongTinHQ1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdMIE1;
        private Janus.Windows.UI.CommandBars.UICommand cmdMIE2;
        private Janus.Windows.UI.CommandBars.UICommand cmdThongTinHQ;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTamMIC1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTamMIC;
    }
}