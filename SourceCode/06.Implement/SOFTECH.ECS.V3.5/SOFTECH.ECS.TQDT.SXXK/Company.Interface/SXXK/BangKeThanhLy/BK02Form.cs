using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK02Form : BaseForm
    {
        public BKToKhaiXuatCollection bkTKXLucDauCollection = new BKToKhaiXuatCollection();
        public BKToKhaiXuatCollection bkTKXCollection = new BKToKhaiXuatCollection();
        public ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        public ToKhaiMauDichCollection tkmdKhongTheTKCollection = new ToKhaiMauDichCollection();
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();

        public BK02Form()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ToKhaiMauDichCollection tkmds = new ToKhaiMauDichCollection();
                foreach (GridEXRow row in dgTKX.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {
                        ToKhaiMauDich tkmd = (ToKhaiMauDich)row.DataRow;
                        BKToKhaiXuat bk = new BKToKhaiXuat();
                        bk.MaHaiQuan = tkmd.MaHaiQuan;
                        bk.MaLoaiHinh = tkmd.MaLoaiHinh;
                        bk.NamDangKy = tkmd.NamDangKy;
                        bk.NgayDangKy = tkmd.NgayDangKy;
                        bk.NgayThucXuat = tkmd.NGAY_THN_THX;
                        bk.SoToKhai = tkmd.SoToKhai;
                        bk.NgayHoanThanh = tkmd.NgayHoanThanh;
                        bk.SoToKhaiVNACCS = tkmd.SoToKhaiVNACCS;

                        bk.GhiChu = tkmd.GhiChu;

                        this.bkTKXCollection.Add(bk);
                        tkmds.Add(tkmd);
                    }
                }
                foreach (ToKhaiMauDich tkmd in tkmds)
                    this.RemoveTKMD(tkmd);
                dgTKX.Refetch();
                dgTKXTL.Refetch();
            }
            catch (Exception ex)
            {
                dgTKXTL.Refresh();
                dgTKX.Refresh();
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                BKToKhaiXuatCollection bks = new BKToKhaiXuatCollection();
                foreach (GridEXRow row in dgTKXTL.GetCheckedRows())
                {
                    if (row.RowType == RowType.Record)
                    {
                        BKToKhaiXuat bk = (BKToKhaiXuat)row.DataRow;
                        ToKhaiMauDich tkmd = new ToKhaiMauDich();
                        tkmd.MaHaiQuan = bk.MaHaiQuan;
                        tkmd.SoToKhai = bk.SoToKhai;
                        tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                        tkmd.NgayDangKy = bk.NgayDangKy;
                        tkmd.NamDangKy = bk.NamDangKy;
                        tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                        tkmd.GhiChu = bk.GhiChu;
                        this.tkmdCollection.Add(tkmd);
                        bks.Add(bk);
                    }
                }
                foreach (BKToKhaiXuat bk in bks)
                    this.RemoveBKTKX(bk);
                dgTKXTL.Refetch();
                dgTKX.Refetch();
            }
            catch (Exception ex)
            {
                dgTKXTL.Refresh();
                dgTKX.Refresh();
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public int KiemTraTruocKhiLuu()
        {
            BKToKhaiXuatCollection bktkxCollection1 = new BKToKhaiXuat().SelectCollectionDynamic(" BangKeHoSoThanhLy_Id != " + this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].ID, "");
            BKToKhaiXuatCollection bktkxCollection2 = this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion;
            foreach (BKToKhaiXuat bk2 in bktkxCollection2)
            {
                foreach (BKToKhaiXuat bk1 in bktkxCollection1)
                {
                    if (bk2.SoToKhai == bk1.SoToKhai && bk2.MaLoaiHinh == bk1.MaLoaiHinh && bk2.NamDangKy == bk1.NamDangKy && bk2.MaHaiQuan == bk1.MaHaiQuan) return bk2.SoToKhai;

                }
            }
            return 0;
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.bkTKXCollection.Count == 0)
                {
                    // ShowMessage("Danh sách tờ khai xuất cần thanh lý chưa có!", false);
                    MLMessages("Danh sách tờ khai xuất cần thanh lý chưa có!", "MSG_THK78", "", false);
                    return;
                }
                else
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion = this.bkTKXCollection;
                    foreach (BKToKhaiXuat bk in this.bkTKXLucDauCollection)
                    {
                        if (!this.bkTKXCollection.Contains(bk)) bk.DeleteFull();
                    }
                    if (this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].InsertUpdate_BKTKX(this.bkTKXCollection))
                        MessageBox.Show("Lưu thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                //this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void BK02Form_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //Bở sự kiện auto tìm TK khi nhập trên ô text
                txtSoToKhai.TextChanged -= new EventHandler(btnGoTo_Click);
                txtSoToKhaiTL.TextChanged -= new EventHandler(btnGoToTL_Click);

                //Cập nhật tiêu đề
                Text += (GlobalSettings.ThanhKhoanNhieuChiCuc ? " - Đang áp đụng Thanh khoản cho nhiều Chi cục Hải Quan" : "");


                if (this.HSTL.TrangThaiThanhKhoan == 401 || this.HSTL.SoTiepNhan == 1)
                {
                    btnAdd.Enabled = btnDelete.Enabled = false;
                    cmdSave.Enabled = false;
                    dgTKXTL.AllowDelete = InheritableBoolean.False;
                }

                //Mới load form: mặc định Từ ngày lùi về 1 năm so với Đến ngày (hiện tại)
                ccToDate.Value = DateTime.Today;
                ccFromDate.Value = ccToDate.Value.AddDays(-275);

                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                //throw ex; 
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void dgTKX_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                    tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                    tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                    tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                    tkmd.Load();
                    f.TKMD = tkmd;
                    f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgTKXTL_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                    tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                    tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                    tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                    tkmd.Load();
                    f.TKMD = tkmd;
                    f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void RemoveTKMD(ToKhaiMauDich tkmd)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == tkmd.SoToKhai && this.tkmdCollection[i].MaLoaiHinh == tkmd.MaLoaiHinh && this.tkmdCollection[i].NamDangKy == tkmd.NamDangKy && this.tkmdCollection[i].MaHaiQuan == tkmd.MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveTKMD(int SoToKhai, string MaLoaiHinh, short NamDangKy, string MaHaiQuan)
        {
            for (int i = 0; i < this.tkmdCollection.Count; i++)
            {
                if (this.tkmdCollection[i].SoToKhai == SoToKhai && this.tkmdCollection[i].MaLoaiHinh == MaLoaiHinh && this.tkmdCollection[i].NamDangKy == NamDangKy && this.tkmdCollection[i].MaHaiQuan == MaHaiQuan)
                {
                    this.tkmdCollection.Remove(tkmdCollection[i]);
                    break;
                }
            }
        }
        private void RemoveBKTKX(BKToKhaiXuat bk)
        {
            for (int i = 0; i < this.bkTKXCollection.Count; i++)
            {
                if (this.bkTKXCollection[i].SoToKhai == bk.SoToKhai && this.bkTKXCollection[i].MaLoaiHinh == bk.MaLoaiHinh && this.bkTKXCollection[i].NamDangKy == bk.NamDangKy && this.bkTKXCollection[i].MaHaiQuan == bk.MaHaiQuan)
                {
                    this.bkTKXCollection.RemoveAt(i);
                    break;
                }
            }
        }
        private DateTime getMaxDateBKTKN()
        {
            int i = this.HSTL.getBKToKhaiNhap();
            this.HSTL.BKCollection[i].LoadChiTietBangKe();
            BKToKhaiNhapCollection bkTKNCollection = this.HSTL.BKCollection[i].bkTKNCollection;
            DateTime dt = bkTKNCollection[0].NgayDangKy;
            for (int j = 1; j < bkTKNCollection.Count; j++)
            {
                if (dt > bkTKNCollection[j].NgayDangKy) dt = bkTKNCollection[j].NgayDangKy;
            }
            return dt;
        }

        private void dgTKXTL_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    //if (ShowMessage("Khi bạn xóa tờ khai xuất này thì dữ liệu liên quan đến tờ khai xuất trong các bảng kê: BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
                    if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
                    {
                        BKToKhaiXuat bk = (BKToKhaiXuat)e.Row.DataRow;
                        bk.DeleteFull();
                        ToKhaiMauDich tkmd = new ToKhaiMauDich();
                        tkmd.MaHaiQuan = bk.MaHaiQuan;
                        tkmd.SoToKhai = bk.SoToKhai;
                        tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                        tkmd.NgayDangKy = bk.NgayDangKy;
                        tkmd.NamDangKy = bk.NamDangKy;
                        tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                        tkmd.GhiChu = bk.GhiChu;
                        this.tkmdCollection.Add(tkmd);
                        try
                        {
                            dgTKX.Refetch();
                        }
                        catch
                        {
                            dgTKX.Refresh();
                        }
                    }
                    else return;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgTKX.RootTable.Columns["NGAY_THN_THX"].DefaultValue = DateTime.Today;
                dgTKXTL.RootTable.Columns["NgayThucXuat"].DefaultValue = DateTime.Today;

                int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
                DateTime toDate = ccToDate.Value;
                DateTime fromDate = ccFromDate.Value;
                if (fromDate > toDate)
                {
                    MLMessages("Từ ngày phải nhỏ hơn hoặc bằng đến ngày.", "MSG_SEA01", "", false);
                    return;
                }
                if (fromDate < this.getMaxDateBKTKN().AddDays(chenhLech)) fromDate = this.getMaxDateBKTKN().AddDays(0 - chenhLech);

                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);
                ccFromDate.Value = fromDate;

                //Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.                
                if (!GlobalSettings.ThanhKhoanNhieuChiCuc)
                {
                    //this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);
                    this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate_PhanLuong(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);
                }
                else
                {
                    dgTKX.Tables[0].Columns["MaHaiQuan"].Visible = true;
                    dgTKXTL.Tables[0].Columns["MaHaiQuan"].Visible = true;

                    //this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate(GlobalSettings.MA_DON_VI, null, fromDate, toDate);
                    this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKXChuaThanhLyByDate_PhanLuong(GlobalSettings.MA_DON_VI, null, fromDate, toDate);

                    dgTKX.Tables[0].Groups.Clear();
                    dgTKXTL.Tables[0].Groups.Clear();

                    dgTKX.Tables[0].Groups.Add(dgTKX.Tables[0].Columns["MaHaiQuan"]);
                    /*dgTKXTL.Tables[0].Columns["MaHaiQuan"].AllowGroup = true;*/
                    dgTKXTL.Tables[0].Groups.Add(dgTKXTL.Tables[0].Columns["MaHaiQuan"]);
                    dgTKX.Tables[0].Columns["MaHaiQuan"].HideWhenGrouped = InheritableBoolean.True;
                    dgTKXTL.Tables[0].Columns["MaHaiQuan"].HideWhenGrouped = InheritableBoolean.True;
                }

                int i = this.HSTL.getBKToKhaiXuat();
                if (i >= 0)
                {
                    this.bkTKXCollection = this.HSTL.BKCollection[i].bkTKXColletion;
                    foreach (BKToKhaiXuat bk in this.bkTKXCollection)
                    {
                        this.RemoveTKMD(bk.SoToKhai, bk.MaLoaiHinh, bk.NamDangKy, bk.MaHaiQuan);
                    }
                }
                this.dgTKXTL.DataSource = this.bkTKXCollection;
                this.dgTKX.DataSource = this.tkmdCollection;
                this.bkTKXLucDauCollection = (BKToKhaiXuatCollection)this.bkTKXCollection.Clone();

                //Hungtq updated 27/07/2012. Bo sung them thong tin danh sach cac to khai khong the thanh khoan. 
                this.tkmdKhongTheTKCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().GetDSTKX_KhongTheThanhKhoan_ByDate_PhanLuong(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, fromDate, toDate);
                this.dgTKXKhongTheTK.DataSource = this.tkmdKhongTheTKCollection;

                try
                {
                    dgTKX.Refetch();
                }
                catch
                {
                    dgTKX.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                //if (ShowMessage("Khi bạn xóa các tờ khai xuất này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
                if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
                {
                    this.Cursor = Cursors.WaitCursor;
                    BKToKhaiXuatCollection bks = new BKToKhaiXuatCollection();
                    foreach (GridEXRow row in dgTKXTL.GetCheckedRows())
                    {
                        if (row.RowType == RowType.Record)
                        {
                            BKToKhaiXuat bk = (BKToKhaiXuat)row.DataRow;
                            //if (bk.ID > 0) bk.DeleteFull();
                            ToKhaiMauDich tkmd = new ToKhaiMauDich();
                            tkmd.MaHaiQuan = bk.MaHaiQuan;
                            tkmd.SoToKhai = bk.SoToKhai;
                            tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                            tkmd.NgayDangKy = bk.NgayDangKy;
                            tkmd.NamDangKy = bk.NamDangKy;
                            tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                            tkmd.NgayHoanThanh = Convert.ToDateTime(row.Cells["NgayHoanThanh"].Value);
                            tkmd.SoToKhaiVNACCS = bk.SoToKhaiVNACCS;
                            tkmd.GhiChu = bk.GhiChu;
                            this.tkmdCollection.Add(tkmd);
                            bks.Add(bk);
                        }
                    }
                    foreach (BKToKhaiXuat bk in bks)
                        this.RemoveBKTKX(bk);
                    dgTKXTL.Refetch();
                    dgTKX.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                dgTKXTL.Refresh();
                dgTKX.Refresh();
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgTKXTL_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {
                //if (ShowMessage("Khi bạn xóa các tờ khai xuất này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê:  BK05, BK06, BK08 sẽ bị xóa theo.\nBạn có muốn xóa không?", true) == "Yes")
                if (MLMessages("Khi bạn xóa tờ khai nhập này thì dữ liệu liên quan đến tờ khai nhập trong các bảng kê: BK03, BK04, BK05, BK06, BK07, BK08, BK09 sẽ bị xóa theo.\nBạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
                {
                    this.Cursor = Cursors.WaitCursor;
                    GridEXSelectedItemCollection items = dgTKXTL.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {

                        if (i.RowType == RowType.Record)
                        {
                            BKToKhaiXuat bk = (BKToKhaiXuat)i.GetRow().DataRow;
                            //if (bk.ID > 0)
                            //    bk.DeleteFull();
                            ToKhaiMauDich tkmd = new ToKhaiMauDich();
                            tkmd.MaHaiQuan = bk.MaHaiQuan;
                            tkmd.SoToKhai = bk.SoToKhai;
                            tkmd.MaLoaiHinh = bk.MaLoaiHinh;
                            tkmd.NgayDangKy = bk.NgayDangKy;
                            tkmd.NamDangKy = bk.NamDangKy;
                            tkmd.NGAY_THN_THX = bk.NgayThucXuat;
                            tkmd.GhiChu = bk.GhiChu;
                            this.tkmdCollection.Add(tkmd);
                        }
                    }
                    dgTKX.Refetch();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                dgTKX.Refresh();
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void BK02Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.bkTKXCollection = this.bkTKXLucDauCollection;
            this.HSTL.BKCollection[this.HSTL.getBKToKhaiXuat()].bkTKXColletion = this.bkTKXCollection;

        }

        private void dgTKX_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (Convert.ToDateTime(e.Row.Cells["Ngay_THN_THX"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["Ngay_THN_THX"].Text = "";
                if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";

                if (e.Row.Cells["PhanLuong"].Value != null)
                {
                    if (e.Row.Cells["PhanLuong"].Value.ToString() == "1")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Green;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "2")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Yellow;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.Red;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "3")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Red;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }

                    e.Row.Cells["PhanLuong"].Text = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                    e.Row.Cells["SoToKhai"].ToolTipText = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                }
            }
        }

        private void dgTKXTL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (Convert.ToDateTime(e.Row.Cells["NgayThucXuat"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucXuat"].Text = "";
                if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";

                if (e.Row.Cells["PhanLuong"].Value != null)
                {
                    if (e.Row.Cells["PhanLuong"].Value.ToString() == "1")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Green;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "2")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Yellow;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.Red;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "3")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Red;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }

                    e.Row.Cells["PhanLuong"].Text = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                    e.Row.Cells["SoToKhai"].ToolTipText = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                }
            }
        }

        private void dgTKXTL_RecordUpdated(object sender, EventArgs e)
        {
            try
            {
                GridEXRow row = dgTKXTL.GetRow();
                BKToKhaiXuat bk = (BKToKhaiXuat)row.DataRow;
                bk.Update();
                ToKhaiMauDich TKMD = new ToKhaiMauDich();
                TKMD.SoToKhai = bk.SoToKhai;
                TKMD.MaLoaiHinh = bk.MaLoaiHinh;
                TKMD.MaHaiQuan = bk.MaHaiQuan;
                TKMD.NamDangKy = bk.NamDangKy;
                TKMD.Load();
                TKMD.NGAY_THN_THX = bk.NgayThucXuat;
                TKMD.Update();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgTKX_RecordUpdated(object sender, EventArgs e)
        {
            try
            {
                GridEXRow row = dgTKX.GetRow();
                ToKhaiMauDich TKMD = (ToKhaiMauDich)row.DataRow;
                DateTime temp = TKMD.NGAY_THN_THX;
                TKMD.Load();
                TKMD.NGAY_THN_THX = temp;
                TKMD.Update();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnGoTo_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTKX.FindAll(dgTKX.Tables[0].Columns["SoToKhaiVNACCS"], ConditionOperator.Equal, Convert.ToDecimal(txtSoToKhai.Value)) > 0)
                {

                    foreach (GridEXSelectedItem item in dgTKX.SelectedItems)
                        item.GetRow().IsChecked = true;
                }
                else
                {
                    MLMessages("Không có tờ khai này trong hệ thống", "MSG_THK79", "", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnGoToTL_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTKXTL.FindAll(dgTKXTL.Tables[0].Columns["SoToKhaiVNACCS"], ConditionOperator.Equal, Convert.ToInt64(txtSoToKhaiTL.Value)) > 0)
                {

                    foreach (GridEXSelectedItem item in dgTKXTL.SelectedItems)
                        item.GetRow().IsChecked = true;
                }
                else
                {
                    MLMessages("Không có tờ khai này trong danh sách", "MSG_THK79", "", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "BangKeToKhaiXuatDuaVaoThanhKhoan_" + HSTL.LanThanhLy + ".xls";
                sfNPL.Filter = "Excel files| *.xls";

                if (sfNPL.ShowDialog() == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgTKXTL;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            string msgTKNo = "";
            try
            {
                try
                {
                    ImportHSTKTKNForm tkn = new ImportHSTKTKNForm();
                    tkn.maLoaiHinh = "X";
                    tkn.ShowDialog();

                    Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection BKTKXCollectionTemp = tkn.TKMDReadCollection;

                    int sumTKX = 0;

                    foreach (Company.BLL.SXXK.ToKhai.ToKhaiMauDich tk in BKTKXCollectionTemp)
                    {
                        if (dgTKX.FindAll(dgTKX.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, tk.SoToKhai) > 0)
                        {
                            foreach (GridEXSelectedItem item in dgTKX.SelectedItems)
                                item.GetRow().IsChecked = true;
                        }
                        else if (dgTKXTL.FindAll(dgTKXTL.Tables[0].Columns["SoToKhai"], ConditionOperator.Equal, tk.SoToKhai) > 0)
                        {

                        }
                        else
                        {
                            sumTKX += 1;
                            msgTKNo += "Tờ khai số: " + tk.SoToKhai + ", Mã loại hình: " + tk.MaLoaiHinh + ", Ngày đăng ký: " + tk.NgayDangKy + "\n";
                        }
                    }

                    if (msgTKNo != "")
                        MLMessages("Có " + sumTKX + " tờ khai không có trong hệ thống\r\n" + msgTKNo, "MSG_THK79", "", false);
                    else
                        tkn.Close();
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); MessageBox.Show(ex.Message); }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            if (dgTKX.SelectedItems.Count > 0)
            {
                btnAdd_Click(sender, e);
            }
        }

        private void dgTKXKhongTheTK_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (Convert.ToDateTime(e.Row.Cells["Ngay_THN_THX"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["Ngay_THN_THX"].Text = "";
                if (Convert.ToDateTime(e.Row.Cells["NgayHoanThanh"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayHoanThanh"].Text = "";

                if (e.Row.Cells["PhanLuong"].Value != null)
                {
                    if (e.Row.Cells["PhanLuong"].Value.ToString() == "1")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Green;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "2")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Yellow;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.Red;
                    }
                    else if (e.Row.Cells["PhanLuong"].Value.ToString() == "3")
                    {
                        e.Row.Cells["SoToKhai"].FormatStyle = new GridEXFormatStyle();
                        e.Row.Cells["SoToKhai"].FormatStyle.BackColor = Color.Red;
                        e.Row.Cells["SoToKhai"].FormatStyle.ForeColor = Color.White;
                    }

                    e.Row.Cells["PhanLuong"].Text = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                    e.Row.Cells["SoToKhai"].ToolTipText = Company.KDT.SHARE.Components.Globals.GetPhanLuong(e.Row.Cells["PhanLuong"].Text);
                }

                if (e.Row.Cells["TrangThaiXuLy"].Value != null)
                {
                    int trangThai = Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value);

                    if (trangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    }
                    else if (trangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    }
                    else if (trangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    }
                    else if (trangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Sửa tờ khai";
                    }
                    else if (trangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.HUYTKDADUYET)
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Hủy tờ khai";
                    }
                    else if (trangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                    }
                    else if (trangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                    }
                    else if (trangThai == 404)
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không xác định khai báo";
                    }
                }
                else
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không xác định khai báo";
            }
        }

        private void dgTKXKhongTheTK_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                    tkmd.NamDangKy = Convert.ToInt16(Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text).Year);
                    tkmd.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
                    tkmd.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                    tkmd.Load();
                    f.TKMD = tkmd;
                    f.NhomLoaiHinh = tkmd.MaLoaiHinh.Substring(0, 3);
                    f.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

    }
}