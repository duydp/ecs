﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
#if GC_V4
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif

namespace Company.Interface.VNACCS
{
    public partial class VNACC_ListHangHMD : BaseForm
    {
        public enum SelectTion
        {
            SingleSelect,
            MultiSelect
        }
        public long[] ListIDTKMD { get; set; }
        public int TKMD_ID { get; set; }
        public KDT_VNACC_HangMauDich HangMD { get; set; }
#if GC_V4
        public HangMauDich HangMD_V4 { get; set; }
        public HangChuyenTiep HangCT_V4 { get; set; }
        public List<HangMauDich> listHangMD_V4 { get; set; }
        public List<HangChuyenTiep> listHangCT_V4 { get; set; }
#endif
        public List<KDT_VNACC_HangMauDich> listHangMD { get; set; }
        public SelectTion SelectMode { set; get; }
        public bool isV4 = false;
        public bool isV4_TKCT = false;
        public VNACC_ListHangHMD(SelectTion SelectMode, long[] idTKMD)
        {
            InitializeComponent();
            gridEX1.SelectionMode = Janus.Windows.GridEX.SelectionMode.SingleSelection;
            gridEX1.RootTable.Columns["Select"].Visible = SelectMode == SelectTion.MultiSelect;
            this.SelectMode = SelectMode;
            ListIDTKMD = idTKMD;
        }
        private void VNACC_ListHangHMD_Load(object sender, EventArgs e)
        {
            try
            {
                if (isV4)
                {
                    gridEX1.Tables[0].Columns["SoDong"].Visible = false;
                    gridEX1.Tables[0].Columns["MaHangHoa"].Visible = false;
                    gridEX1.Tables[0].Columns["MaSoHang"].Visible = false;
                    gridEX1.Tables[0].Columns["SoLuong1"].Visible = false;
                    gridEX1.Tables[0].Columns["DVTLuong1"].Visible = false;
                    gridEX1.Tables[0].Columns["NuocXuatXu"].Visible = false;
                    gridEX1.Tables[0].Columns["DonGiaHoaDon"].Visible = false;
                    gridEX1.Tables[0].Columns["TriGiaHoaDon"].Visible = false;
                    gridEX1.Tables[0].Columns["TriGiaTinhThue"].Visible = false;
                    gridEX1.Tables[0].Columns["TongThueThuKhac"].Visible = false;
                    gridEX1.Tables[0].Columns["SoTienThue"].Visible = false;
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("SoThuTuHang", ColumnType.Text, EditType.NoEdit) { Caption = "Số dòng" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("MaPhu", ColumnType.Text, EditType.NoEdit) { Caption = "Mã hàng hoá" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("MaHS", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("SoLuong", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("DVT_ID", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("NuocXX_ID", ColumnType.Text, EditType.NoEdit) { Caption = "Xuất xứ" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("DonGiaKB", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá KB" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("TriGiaKB", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá KB" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("DonGiaTT", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá TT" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("TriGiaTT", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá TT" });
                    gridEX1.Tables[0].Columns["SoThuTuHang"].Visible = true;
                    gridEX1.Tables[0].Columns["SoThuTuHang"].Position = 1;
                    gridEX1.Tables[0].Columns["MaPhu"].Visible = true;
                    gridEX1.Tables[0].Columns["MaPhu"].Position = 2;
                    gridEX1.Tables[0].Columns["MaHS"].Visible = true;
                    gridEX1.Tables[0].Columns["MaHS"].Position = 3;
#if GC_V4
                    gridEX1.DataSource = HangMauDich.SelectCollectionBy_TKMD_ID(this.TKMD_ID);
#endif

                }
                else if (isV4_TKCT)
                {
                    gridEX1.Tables[0].Columns["SoDong"].Visible = false;
                    gridEX1.Tables[0].Columns["MaHangHoa"].Visible = false;
                    gridEX1.Tables[0].Columns["MaSoHang"].Visible = false;
                    gridEX1.Tables[0].Columns["SoLuong1"].Visible = false;
                    gridEX1.Tables[0].Columns["DVTLuong1"].Visible = false;
                    gridEX1.Tables[0].Columns["NuocXuatXu"].Visible = false;
                    gridEX1.Tables[0].Columns["DonGiaHoaDon"].Visible = false;
                    gridEX1.Tables[0].Columns["TriGiaHoaDon"].Visible = false;
                    gridEX1.Tables[0].Columns["TriGiaTinhThue"].Visible = false;
                    gridEX1.Tables[0].Columns["TongThueThuKhac"].Visible = false;
                    gridEX1.Tables[0].Columns["SoTienThue"].Visible = false;
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("SoThuTuHang", ColumnType.Text, EditType.NoEdit) { Caption = "Số dòng" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("MaHang", ColumnType.Text, EditType.NoEdit) { Caption = "Mã hàng hoá" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("MaHS", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("SoLuong", ColumnType.Text, EditType.NoEdit) { Caption = "Số lượng" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("ID_DVT", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("ID_NuocXX", ColumnType.Text, EditType.NoEdit) { Caption = "Xuất xứ" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("DonGia", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá KB" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("TriGia", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá KB" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("DonGiaTT", ColumnType.Text, EditType.NoEdit) { Caption = "Đơn giá TT" });
                    gridEX1.Tables[0].Columns.Add(new GridEXColumn("TriGiaTT", ColumnType.Text, EditType.NoEdit) { Caption = "Trị giá TT" });
                    //gridEX1.Tables[0].Columns.Add(new GridEXColumn("MaHS", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS" });
                    gridEX1.Tables[0].Columns["SoThuTuHang"].Visible = true;
                    gridEX1.Tables[0].Columns["SoThuTuHang"].Position = 1;
                    gridEX1.Tables[0].Columns["MaHang"].Visible = true;
                    gridEX1.Tables[0].Columns["MaHang"].Position = 2;
                    gridEX1.Tables[0].Columns["MaHS"].Visible = true;
                    gridEX1.Tables[0].Columns["MaHS"].Position = 3;
#if GC_V4
                    gridEX1.DataSource = HangChuyenTiep.SelectCollectionBy_Master_ID(this.TKMD_ID);
#endif
                }
                else
                {
                    gridEX1.DataSource = KDT_VNACC_HangMauDich.LoadHangMauDichbyID(this.ListIDTKMD);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void btnChonHang_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.SelectMode == SelectTion.SingleSelect)
                {
                    if (isV4)
                    {
#if GC_V4
                        HangMD_V4 = (HangMauDich)gridEX1.SelectedItems[0].GetRow().DataRow;
#endif
                    }
                    else if (isV4_TKCT)
                    {
#if GC_V4
                        HangCT_V4 = (HangChuyenTiep)gridEX1.SelectedItems[0].GetRow().DataRow;   
#endif
                    }
                    else
                    {
                        HangMD = (KDT_VNACC_HangMauDich)gridEX1.SelectedItems[0].GetRow().DataRow;
                    }
                }
                else if (this.SelectMode == SelectTion.MultiSelect)
                {
                    if (isV4)
                    {
#if GC_V4
                        listHangMD_V4 = new List<HangMauDich>();
                        foreach (Janus.Windows.GridEX.GridEXRow item in gridEX1.GetCheckedRows())
                        {
                            listHangMD_V4.Add((HangMauDich)item.DataRow);
                        }
#endif
                    }
                    else if (isV4_TKCT)
                    {
#if GC_V4
                        listHangCT_V4 = new List<HangChuyenTiep>();
                        foreach (Janus.Windows.GridEX.GridEXRow item in gridEX1.GetCheckedRows())
                        {
                            listHangCT_V4.Add((HangChuyenTiep)item.DataRow);
                        }     
#endif                   
                    }
                    else
                    {
                        listHangMD = new List<KDT_VNACC_HangMauDich>();
                        foreach (Janus.Windows.GridEX.GridEXRow item in gridEX1.GetCheckedRows())
                        {
                            listHangMD.Add((KDT_VNACC_HangMauDich)item.DataRow);
                        }
                    }
                }
                this.DialogResult = DialogResult.OK;
            }   
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.ShowMessage(ex.Message, false);
            }

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "HangMauDich_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog(this);

                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridEX1;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void gridEX1_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (isV4)
                {
                    string DVT_ID = e.Row.Cells["DVT_ID"].Value.ToString();
                    if (Char.IsNumber(DVT_ID, 0) && e.Row.Cells["DVT_ID"].Value.ToString() != String.Empty)
                    {
#if GC_V4
                        e.Row.Cells["DVT_ID"].Text = DonViTinh.GetName(DVT_ID);
#endif
                    }
                }
                else if (isV4_TKCT)
                {
                    string ID_DVT = e.Row.Cells["ID_DVT"].Value.ToString();
                    if (Char.IsNumber(ID_DVT, 0) && e.Row.Cells["ID_DVT"].Value.ToString() != String.Empty)
                    {
#if GC_V4
                        e.Row.Cells["ID_DVT"].Text = DonViTinh.GetName(ID_DVT);
#endif
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void gridEX1_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            btnChonHang_Click(null,null);
        }
        
    }
}
