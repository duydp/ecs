﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.SXXK
{
    public partial class NguyenPhuLieuTon
    {
        public DataSet SelectDynamic(string whereCondition, string orderByExpression, SqlTransaction trans)
        {
            string spName = "p_SXXK_NguyenPhuLieuTon_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand, trans);
        }
        //lay luong ton tai thoi diem thanh khoan cuoi cung
        public DataSet SelectNPLTon(string MaHaiQuan, string MaDoanhNghiep)
        {
            string sql = "select * from v_SXXK_NPLTon where MaHaiQuan='" + MaHaiQuan + "' and MaDoanhNghiep='" + MaDoanhNghiep + "'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            return this.db.ExecuteDataSet(dbCommand);
        }
        public bool Load(SqlTransaction trans)
        {
            string spName = "p_SXXK_NguyenPhuLieuTon_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTon"))) this._LuongTon = reader.GetDouble(reader.GetOrdinal("LuongTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) this._LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public ToKhai.ToKhaiMauDichCollection SelectToKhaiChuaThanhKhoan(string MaHaiQuan, string MaDoanhNghiep)
        {
            string sql = "select * from v_SXXK_ToKhaiChuaThanhKhoan where MaHaiQuan='" + MaHaiQuan + "' and MaDoanhNghiep='" + MaDoanhNghiep + "'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection collection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich entity = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                collection.Add(entity);

            }
            reader.Close();
            return collection;
        }

        //public void TinhToanLuongTon(string MaHaiQuan, string MaDoanhNghiep)
        //{
        //    //reset lai so lieu
        //    //Cap nhat lai luong npl ton tai thoi diem thanh khoan bo cuoi cung
        //    //chuyen luong ton tu NPLTOn ben thanh khoan sang
        //    DataTable CollectionNPL = new NguyenPhuLieuTon().SelectNPLTon(MaHaiQuan, MaDoanhNghiep).Tables[0];
        //    ToKhai.ToKhaiMauDichCollection TKMDCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().SelectCollectionDynamic("MaHaiQuan='" + MaHaiQuan + "' and MaDoanhNghiep='" + MaDoanhNghiep + "' and ThanhLy='H' and maLoaiHinh like 'X%'", "");
        //    foreach (ToKhai.ToKhaiMauDich TKMD in TKMDCollection)
        //    {
        //        try
        //        {
        //            TKMD.LoadHMDCollection();
        //            foreach (Company.BLL.SXXK.ToKhai.HangMauDich HMDTon in TKMD.HMDCollection)
        //            {
        //                if (TKMD.MaLoaiHinh == "XSX05")
        //                {
        //                    DataRow[] rowNPL = CollectionNPL.Select("Ma='" + HMDTon.MaPhu + "'");
        //                    DataRow row = null;
        //                    if (rowNPL.Length == 0)
        //                    {
        //                        DataRow newRow = CollectionNPL.NewRow();
        //                        newRow["Ma"] = HMDTon.MaPhu;
        //                        newRow["MaHaiQuan"] = TKMD.MaHaiQuan;
        //                        newRow["MaDoanhNghiep"] = TKMD.MaDoanhNghiep;
        //                        newRow["Ten"] = HMDTon.TenHang;
        //                        newRow["LuongTon"] = "0";
        //                        CollectionNPL.Rows.Add(newRow);
        //                        row = newRow;
        //                    }
        //                    else
        //                        row = rowNPL[0];
        //                    double LuongTon = Convert.ToDouble(row["LuongTon"]) - Convert.ToDouble(HMDTon.SoLuong);
        //                    row["LuongTon"] = LuongTon;
        //                }
        //                else
        //                {
        //                    DataSet dsDM = Company.BLL.SXXK.DinhMuc.getDinhMucOfSanPham(HMDTon.MaPhu, MaHaiQuan, MaDoanhNghiep, HMDTon.SoLuong,sot);

        //                    if (dsDM.Tables[0].Rows.Count == 0)
        //                        throw new Exception("Sản phẩm : " + HMDTon.MaPhu + " chưa có định mức nên không thể cập nhật vào lượng tồn được.");
        //                    foreach (DataRow rowDMIndex in dsDM.Tables[0].Rows)
        //                    {
        //                        string MaNPL = rowDMIndex["MaNguyenPhuLieu"].ToString();
        //                        double SoLuong = Convert.ToDouble(rowDMIndex["SoLuong"].ToString());
        //                        DataRow[] rowNPLDM = CollectionNPL.Select("Ma='" + MaNPL + "'");
        //                        DataRow rowDM = null;
        //                        if (rowNPLDM.Length == 0)
        //                        {
        //                            DataRow newRow = CollectionNPL.NewRow();
        //                            newRow["Ma"] = MaNPL;
        //                            newRow["MaHaiQuan"] = TKMD.MaHaiQuan;
        //                            newRow["MaDoanhNghiep"] = TKMD.MaDoanhNghiep;
        //                            newRow["Ten"] = HMDTon.TenHang;
        //                            newRow["LuongTon"] = "0";
        //                            CollectionNPL.Rows.Add(newRow);
        //                            rowDM = newRow;
        //                        }
        //                        else
        //                            rowDM = rowNPLDM[0];
        //                        double LuongTon = Convert.ToDouble(rowDM["LuongTon"]) - SoLuong;
        //                        rowDM["LuongTon"] = LuongTon;
        //                    }
        //                }
        //            }
        //        }
        //        catch
        //        {

        //        }
        //    }
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        try
        //        {
        //            foreach (DataRow rowNPLTon in CollectionNPL.Rows)
        //            {
        //                NguyenPhuLieuTon NPLTon = new NguyenPhuLieuTon();
        //                NPLTon.MaDoanhNghiep = MaDoanhNghiep;
        //                NPLTon.MaHaiQuan = MaHaiQuan;
        //                NPLTon.Ma = rowNPLTon["Ma"].ToString();
        //                NPLTon.Ten = rowNPLTon["TenNPL"].ToString();
        //                NPLTon.LuongCungUng = Convert.ToDouble(rowNPLTon["LuongCungUng"].ToString());
        //                NPLTon.LuongTon = Convert.ToDouble(rowNPLTon["LuongTon"].ToString());
        //                NPLTon.InsertUpdateTransaction(transaction);
        //            }
        //            UpdateNPLTon(MaHaiQuan, MaDoanhNghiep, transaction);
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        //private void UpdateNPLTon(string MaHaiQuan, string MaDoanhNghiep, SqlTransaction transaction)
        //{
        //    string spName = "CapNhatNguyenPhuLieuTon";
        //    SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

        //    db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
        //    db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
        //    if (transaction != null)
        //        db.ExecuteNonQuery(dbCommand, transaction);
        //    else
        //        db.ExecuteNonQuery(dbCommand);
        //}

    }
}