﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Components;

namespace Company.Interface.VNACCS.HopDong
{
    public partial class VNACC_HopDongDangKyForm : BaseForm
    {
        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        private static string sfmtYear = "yyyy";

        //private FeedBackContent feedbackContent = null;
        //private string msgInfor = string.Empty;
        KDT_VNACC_ToKhaiMauDich TKMD;
        List<KDT_VNACC_ToKhaiMauDich> listTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
        KDT_VNACC_ToKhaiMauDich tkmd = new KDT_VNACC_ToKhaiMauDich();
        KDT_VNACC_HopDongDangKy hopDong = new KDT_VNACC_HopDongDangKy();
        List<KDT_VNACC_HopDongDangKy_ChiTiet> listHDDKChiTiet = new List<KDT_VNACC_HopDongDangKy_ChiTiet>();
        public bool isAddNew = true;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        //public ELoaiThongTin LoaiChungTu = ELoaiThongTin.CT;
        public VNACC_HopDongDangKyForm()
        {
            InitializeComponent();
            //LoaiChungTu = loaiChungTu;
            //SetHandler(this);
            //docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(Contract.CT.ToString());
            //InitializeComponent();
        }
        private void VNACC_HopDongDangKyForm_Load(object sender, EventArgs e)
        {

            //SetHopDong();
        }
        private void bindData() {

            gridList.DataSource = listHDDKChiTiet;
            gridFile.DataSource = listTKMD;
            try
            {
                gridList.Refetch();
                gridFile.Refetch();
            }
            catch
            {  
            }
            HienThiTongDungLuong(listHDDKChiTiet);
        }
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes > 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes > 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes > 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }
        private void HienThiTongDungLuong(List<KDT_VNACC_HopDongDangKy_ChiTiet> list)
        {
            long size = 0;

            for (int i = 0; i < list.Count; i++)
            {
                size += Convert.ToInt64(list[i].FileSize);
            }
        
            lblTongDungLuong.Text = CalculateFileSize(size);
           // lblLuuY.Text = CalculateFileSize(GlobalSettings.FileSize);
        }
        private void btnAddTK_Click(object sender, EventArgs e)
        {
            VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
            f.IsShowChonToKhai = true;
            f.ShowDialog(this);
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave" :
                    Save();
                    break;
                case "cmdSend" :
                    //SendV5();
                    break;
                case "cmdFeedback" :
                    feedbackContent();
                    break;
            }
        }

        private void Save() 
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (listHDDKChiTiet == null || listHDDKChiTiet.Count == 0) 
                {
                    ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                    return;

                }
                if (listTKMD ==null)
                {
                    ShowMessage("Bạn chưa thêm  tờ khai", false);
                    return;

                }
                isAddNew = hopDong.ID == 0;
                GetHopDong();
                hopDong.InsertUpdate();
                bindData();
                ShowMessage("Lưu thành công",false);
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        
        }

        private void GetHopDong()
{
            hopDong.TenNguoiKhaiHQ = txtTenNguoiKhai.Text.ToString();
            hopDong.MaNguoiKhaiHQ = txtMaNguoiKhai.Text.ToString();
            hopDong.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
            hopDong.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            hopDong.SoToKhai = tkmd.SoToKhai;
            hopDong.NgayDangKyTK = tkmd.NgayDangKy;
            hopDong.MaLoaiHinh = tkmd.MaLoaiHinh;
            hopDong.MaHaiQuan = tkmd.CoQuanHaiQuan;
            hopDong.SoHopDong = txtSoHopDong.Text.ToString();
            hopDong.NgayHopDong = Convert.ToDateTime(ctrNgayHopDong.Text.ToString());
            hopDong.ThoiHanThanhToan = Convert.ToDateTime(ctrThoiHanTT.Text.ToString());
            hopDong.TongTriGia = Convert.ToInt32(txtTongTriGia.Text.ToString());
            hopDong.GhiChuKhac = "";
}
        private void SetHopDong()
        {
            txtTenNguoiKhai.Text=hopDong.TenNguoiKhaiHQ;
            txtMaNguoiKhai.Text=hopDong.MaNguoiKhaiHQ;
            txtTenDN.Text=GlobalSettings.TEN_DON_VI;
            txtMaDN.Text=GlobalSettings.MA_DON_VI;
            txtSoHopDong.Text=hopDong.SoHopDong;
            ctrNgayHopDong.Text=hopDong.NgayHopDong.ToString();
            ctrThoiHanTT.Text=hopDong.ThoiHanThanhToan.ToString();
            txtTongTriGia.Text=hopDong.TongTriGia.ToString();
        }
        public void feedbackContent()
        {
        
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                    + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                    + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

            openFileDialog1.Multiselect = true;

            try
            {
                if (openFileDialog1.ShowDialog(this) != DialogResult.Cancel)
                {
                    long size = 0;

                    for (int k = 0; k < openFileDialog1.FileNames.Length; k++)
                    {
                        System.IO.FileInfo fin = new System.IO.FileInfo(openFileDialog1.FileNames[k]);

                        if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                            && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                            && fin.Extension.ToUpper() != ".gif".ToUpper()
                            && fin.Extension.ToUpper() != ".tiff".ToUpper()
                            && fin.Extension.ToUpper() != ".txt".ToUpper()
                            && fin.Extension.ToUpper() != ".xml".ToUpper()
                            && fin.Extension.ToUpper() != ".xsl".ToUpper()
                            && fin.Extension.ToUpper() != ".csv".ToUpper()
                            && fin.Extension.ToUpper() != ".doc".ToUpper()
                            && fin.Extension.ToUpper() != ".mdb".ToUpper()
                            && fin.Extension.ToUpper() != ".pdf".ToUpper()
                            && fin.Extension.ToUpper() != ".ppt".ToUpper()
                            && fin.Extension.ToUpper() != ".xls".ToUpper())
                        {
                            ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                        }
                        else
                        {
                            System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileNames[k], System.IO.FileMode.Open, System.IO.FileAccess.Read);

                            //Cap nhat tong dung luong file.
                            size = 0;

                            for (int i = 0; i < listHDDKChiTiet.Count; i++)
                            {
                                size += Convert.ToInt64(listHDDKChiTiet[i].FileSize);
                            }

                            //+ them dung luong file moi chuan bi them vao danh sach
                            size += fs.Length;

                            //Kiem tra dung luong file
                            if (size > GlobalSettings.FileSize)
                            {
                                this.ShowMessage(string.Format("Tổng dung lượng cho phép {0}\r\nDung lượng bạn nhập {1} đã vượt mức cho phép.", CalculateFileSize(GlobalSettings.FileSize), CalculateFileSize(size)), false);
                                return;
                            }

                            byte[] data = new byte[fs.Length];
                            fs.Read(data, 0, data.Length);
                            filebase64 = System.Convert.ToBase64String(data);
                            filesize = fs.Length;

                            /*
                             * truoc khi them moi file dinh kem vao danh sach, phai kiem tra tong dung luong co hop len khong?.
                             * Neu > dung luong choh phep -> Hien thi thong bao va khong them vao danh sach.
                             */
                            KDT_VNACC_HopDongDangKy_ChiTiet ctctiet = new KDT_VNACC_HopDongDangKy_ChiTiet();
                            ctctiet.ID = hopDong.ID;
                            ctctiet.FileName = fin.Name;
                            ctctiet.FileSize = filesize;
                            ctctiet.NoiDung = data;

                            listHDDKChiTiet.Add(ctctiet);
                            gridList.DataSource = listHDDKChiTiet;
                            try
                            {
                                gridList.Refetch();
                            }
                            catch { gridList.Refresh(); }

                            lblTongDungLuong.Text = CalculateFileSize(size);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (hopDong.ID > 0)
            {
                if (ShowMessage("Bạn có muốn xóa File này không?", true) == "Yes")
                {
                    //Xoa hop dong chi tiet
                    hopDong.LoadListHopDongDangKy_ChiTiet();

                    for (int i = 0; i < hopDong.hopDongDangKyChiTietCollection.Count; i++)
                    {
                        hopDong.hopDongDangKyChiTietCollection[i].Delete();
                    }

                    //Xoa hop dong trong DB
                    hopDong.Delete();
                }
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //kiem tra thu da co muc temp?. Neu chua co -> tao 1 thu muc temp de chua cac file tam thoi.
                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                //Giai ma basecode64 -> file & luu tam vao thu muc Temp moi tao.
                if (gridFile.GetRow() == null)
                {
                    ShowMessage("Chưa chọn file để xem", false);
                    return;
                }
                KDT_VNACC_HopDongDangKy_ChiTiet fileData = (KDT_VNACC_HopDongDangKy_ChiTiet)gridFile.GetRow().DataRow;
                string fileName = path + "\\" + fileData.FileName;

                //Ghi file
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }

                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                fs.Write(fileData.NoiDung, 0, fileData.NoiDung.Length);
                fs.Close();

                System.Diagnostics.Process.Start(fileName);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }


    }
}