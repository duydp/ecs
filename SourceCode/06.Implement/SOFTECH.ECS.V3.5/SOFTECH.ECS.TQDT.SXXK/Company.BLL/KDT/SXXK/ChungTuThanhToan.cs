﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Data;

namespace Company.KDT.SXXK
{
    public partial class ChungTuThanhToan
    {

        public DataSet SelectHopDong(int LanThanhLy)
        {
            string spName = "p_KDT_SXXK_ChungTuThanhToan_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }


    }
}
