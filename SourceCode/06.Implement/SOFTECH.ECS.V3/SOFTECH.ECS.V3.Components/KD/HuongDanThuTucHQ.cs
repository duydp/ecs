﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{
    //NAMNTH
    public class HuongDanThuTucHQ
    {

        /// <summary>
        /// ConfigHuongdanthutucHQ
        /// </summary> 
        /// <returns>string</returns>
        public static string ConfigHuongdanthutucHQ(ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/KD/HuongDanThuTucHQ.xml");
                if (tkmd != null && docXML != null)
                {
                    XElement Root = docXML.Root;
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        

                        //setvalue Invoice
                        XElement Invoice = Root.Element(NodeHuongDanThuTucHQ.Invoice);
                        Invoice = ProcessXML.SetValueInvoice(Invoice, tkmd);
                        
                        
                        int countAddInfo = 0;
                        foreach (XElement element in Root.Elements())
                        {
                            
                            if (element.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.AdditionalInformation.ToUpper()))
                            {
                                if (countAddInfo == 0)//Kết quả phân luồng
                                {
                                    foreach (XElement child in element.Elements())
                                    {
                                        if (child.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.content.ToUpper()))
                                            child.SetValue("");
                                        if (child.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.statement.ToUpper()))
                                            child.SetValue("");
                                    }
                                    countAddInfo = countAddInfo + 1;
                                }
                                else if (countAddInfo == 1)//Tài khoản kho bạc
                                {
                                    foreach (XElement child in element.Elements())
                                    {
                                        if (child.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.content.ToUpper()))
                                            child.SetValue("");
                                        if (child.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.statement.ToUpper()))
                                            child.SetValue("");
                                    }
                                    countAddInfo = countAddInfo + 1;
                                }
                                else if (countAddInfo == 2)//Tên kho bạc
                                {
                                    foreach (XElement child in element.Elements())
                                    {
                                        if (child.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.content.ToUpper()))
                                            child.SetValue("");
                                        if (child.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.statement.ToUpper()))
                                            child.SetValue("");
                                    }
                                }

                            }
                        }

                        
                        
                        //set value for AdditionalDocument
                        XElement AdditionalDocument = Root.Element(NodeHuongDanThuTucHQ.AdditionalDocument);
                        foreach (XElement element in AdditionalDocument.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.issue.ToUpper()))
                            {
                                element.SetValue("");//Ngày ra thông báo thuế
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.reference.ToUpper()))
                            {
                                element.SetValue("");//Số thông báo thuế
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.type.ToUpper()))
                            {
                                element.SetValue("");//Mã thông báo thuế(= 006)
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.expire.ToUpper()))
                            {
                                element.SetValue("");//Ngày hết hạn thông báo thuế
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.DutyTaxFee.ToUpper()))//Thuế
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.adValoremTaxBase.ToUpper()))
                                        elementChild.SetValue("");//Tiền thuế
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.type.ToUpper()))
                                        elementChild.SetValue("");//Sắc thuế
                                }
                            }

                            if (element.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.AdditionalInformation.ToUpper()))
                                foreach (XElement elementChild in element.Elements())//Chương, loại, khoản, mục
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.content.ToUpper()))
                                    {
                                        elementChild.SetValue("");//Nội dung thông tin
                                    }
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHuongDanThuTucHQ.statement.ToUpper()))
                                    {
                                        elementChild.SetValue("");//Loại thông tin
                                    }
                                }
                        }                        
                        
                        //docXML.Save(@"D:\NewHuongDanThuTucHQ.xml");
                        //docXML.Save(@"../../../SOFTECH.ECS.V3.Components/XML/LoadKD/NewHuongDanThuTucHQ.xml");
                        return docXML.ToString();
                    }

                }
                return "";
            }
            catch
            {
                return "";
            }

        }
    }
}
