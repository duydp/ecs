﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Category
{
    public partial class VNACC_Category_OGAUser : BaseForm
    {
        public Company.KDT.SHARE.VNACCS.VNACC_Category_OGAUser oGAUser;
        public string where = "";
        public VNACC_Category_OGAUser()
        {
            InitializeComponent();
        }

        private void VNACC_Category_OGAUser_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = Company.KDT.SHARE.VNACCS.VNACC_Category_OGAUser.SelectCollectionDynamic(GetSearchWhere(), null);
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ClearText()
        {
            //txtCode.Text = String.Empty;
            txtName.Text = String.Empty;
            txtNotes.Text = String.Empty;
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtCode, errorProvider, "Mã hải quan  ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtName, errorProvider, "Mã CCHQ ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNotes, errorProvider, "Ghi chú ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                oGAUser.OfficeOfApplicationCode = txtCode.Text.Trim().ToUpper();
                oGAUser.OfficeOfApplicationName = txtName.Text.Trim().ToUpper();
                oGAUser.Notes = txtNotes.Text.Trim().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void Set()
        {
            try
            {
                txtCode.Text = oGAUser.OfficeOfApplicationCode;
                txtName.Text = oGAUser.OfficeOfApplicationName;
                txtNotes.Text = oGAUser.Notes;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtCode.Text))
                    where += " AND OfficeOfApplicationCode = '" + txtCode.Text.Trim() + "'";
                if (!String.IsNullOrEmpty(txtName.Text))
                    where += " AND OfficeOfApplicationName LIKE N'%" + txtName.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtNotes.Text))
                    where += " AND Notes LIKE N'%" + txtNotes.Text.Trim() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return where;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                Get();
                oGAUser.TableID = "A700A";
                oGAUser.InsertUpdate();
                ShowMessage("Lưu thành công ", false);
                oGAUser = new Company.KDT.SHARE.VNACCS.VNACC_Category_OGAUser();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                if (ShowMessage("Bạn có muốn xóa dòng này không ? ", true) == "Yes")
                {
                    oGAUser.Delete();
                }
                ShowMessage("Xóa thành công ", false);
                oGAUser = new Company.KDT.SHARE.VNACCS.VNACC_Category_OGAUser();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {

                if (e.Row.RowType == RowType.Record)
                {
                    oGAUser = new Company.KDT.SHARE.VNACCS.VNACC_Category_OGAUser();
                    oGAUser = (Company.KDT.SHARE.VNACCS.VNACC_Category_OGAUser)e.Row.DataRow;
                    Set();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
