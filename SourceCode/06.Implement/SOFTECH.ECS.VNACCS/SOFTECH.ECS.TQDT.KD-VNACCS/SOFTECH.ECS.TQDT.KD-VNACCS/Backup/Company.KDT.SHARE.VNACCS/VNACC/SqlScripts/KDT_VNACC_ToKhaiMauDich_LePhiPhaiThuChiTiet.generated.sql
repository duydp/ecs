-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]
	@LePhi_ID bigint,
	@TenSacPhi nvarchar(210),
	@SoPhiPhaiNop numeric(12, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
(
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
)
VALUES 
(
	@LePhi_ID,
	@TenSacPhi,
	@SoPhiPhaiNop
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]
	@ID bigint,
	@LePhi_ID bigint,
	@TenSacPhi nvarchar(210),
	@SoPhiPhaiNop numeric(12, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
SET
	[LePhi_ID] = @LePhi_ID,
	[TenSacPhi] = @TenSacPhi,
	[SoPhiPhaiNop] = @SoPhiPhaiNop
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate]
	@ID bigint,
	@LePhi_ID bigint,
	@TenSacPhi nvarchar(210),
	@SoPhiPhaiNop numeric(12, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] 
		SET
			[LePhi_ID] = @LePhi_ID,
			[TenSacPhi] = @TenSacPhi,
			[SoPhiPhaiNop] = @SoPhiPhaiNop
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
		(
			[LePhi_ID],
			[TenSacPhi],
			[SoPhiPhaiNop]
		)
		VALUES 
		(
			@LePhi_ID,
			@TenSacPhi,
			@SoPhiPhaiNop
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]
	@LePhi_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
WHERE
	[LePhi_ID] = @LePhi_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]
	@LePhi_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]
WHERE
	[LePhi_ID] = @LePhi_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LePhi_ID],
	[TenSacPhi],
	[SoPhiPhaiNop]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]	

GO

