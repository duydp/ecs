﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_TIAForm : BaseForm
    {
        public KDT_VNACCS_TIA TIA = new KDT_VNACCS_TIA();
        DataSet dsHang = new DataSet();
        DataSet dsDVT = new DataSet();
        DataSet dsmaHS = new DataSet();
        public VNACC_TIAForm()
        {
            InitializeComponent();
        }

        private void VNACC_TIAForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                dsmaHS = VNACC_Category_HSCode.SelectAll();
                dsDVT = VNACC_Category_QuantityUnit.SelectAll();
                grListHang.DropDowns[0].DataSource = dsDVT.Tables[0];
                grListHang.DropDowns[1].DataSource = dsDVT.Tables[0];
                grListHang.DropDowns["drdMaSoHangHoa"].DataSource = dsmaHS.Tables[0];

               
                dsHang = KDT_VNACCS_TIA_HangHoa.SelectDynamic("Master_ID=" + TIA.ID, "");

                grListHang.DataSource = dsHang.Tables[0];
                SetTIA();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }


        }

        private void GetTIA()
        {
            TIA.MaNguoiKhaiDauTien = txtMaNguoiKhaiDauTien.Text;
            TIA.TenNguoiKhaiDauTien = txtTenNguoiKhaiDauTien.Text;
            TIA.MaNguoiXuatNhapKhau = txtMaNguoiXuatNhapKhau.Text;
            TIA.TenNguoiXuatNhapKhau = txtTenNguoiXuatNhapKhau.Text;
            TIA.ThoiHanTaiXuatNhap = clcNgayHangDen.Value;
        }

        private void SetTIA()
        {
            txtMaNguoiKhaiDauTien.Text = TIA.MaNguoiKhaiDauTien;
            txtTenNguoiKhaiDauTien.Text = TIA.TenNguoiKhaiDauTien;
            txtMaNguoiXuatNhapKhau.Text = TIA.MaNguoiXuatNhapKhau;
            txtTenNguoiXuatNhapKhau.Text = TIA.TenNguoiXuatNhapKhau;
            clcNgayHangDen.Value = TIA.ThoiHanTaiXuatNhap;
        }
        private void Save()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                GetTIA();
                if (TIA.ID != 0)
                    TIA.ID = TIA.Update();
                else
                {
                    TIA.TrangThaiXuLy = "-1";
                    TIA.ID = TIA.Insert();
                }
                foreach (DataRow row in dsHang.Tables[0].Rows)
                {
                    KDT_VNACCS_TIA_HangHoa hang = new KDT_VNACCS_TIA_HangHoa();
                    hang.Master_ID = TIA.ID;
                    if (row["ID"].ToString().Length != 0)
                    {
                        hang.ID = Convert.ToInt32(row["ID"].ToString());
                    }
                    hang.MaSoHangHoa = row["MaSoHangHoa"].ToString();
                    hang.SoLuongBanDau = Convert.ToInt32(row["SoLuongBanDau"].ToString());
                    hang.DVTSoLuongBanDau = row["DVTSoLuongBanDau"].ToString();
                    hang.SoLuongDaTaiNhapTaiXuat = Convert.ToInt32(row["SoLuongDaTaiNhapTaiXuat"].ToString());
                    hang.DVTSoLuongDaTaiNhapTaiXuat = row["DVTSoLuongDaTaiNhapTaiXuat"].ToString();

                    if (hang.ID != 0)
                        hang.Update();
                    else
                        hang.Insert();
                }
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }



        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdLuu":
                    Save();
                    break;
            }
        }

        private void grListHang_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {

        }

     

    }
}
