using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
	public partial class NPLCungUngDetail
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public int HinhThuc { set; get; }
		public string SoChungTu { set; get; }
		public DateTime NgayChungTu { set; get; }
		public int DongHangTrenCT { set; get; }
		public decimal SoLuong { set; get; }
		public string DVT_ID { set; get; }
		public decimal TyLeQuyDoi { set; get; }
		public string NoiPhatHanh { set; get; }
		public string MaLoaiHinh { set; get; }
		public string MaHaiQuan { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static NPLCungUngDetail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			NPLCungUngDetail entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new NPLCungUngDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThuc"))) entity.HinhThuc = reader.GetInt32(reader.GetOrdinal("HinhThuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DongHangTrenCT"))) entity.DongHangTrenCT = reader.GetInt32(reader.GetOrdinal("DongHangTrenCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeQuyDoi"))) entity.TyLeQuyDoi = reader.GetDecimal(reader.GetOrdinal("TyLeQuyDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiPhatHanh"))) entity.NoiPhatHanh = reader.GetString(reader.GetOrdinal("NoiPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<NPLCungUngDetail> SelectCollectionAll()
		{
			List<NPLCungUngDetail> collection = new List<NPLCungUngDetail>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				NPLCungUngDetail entity = new NPLCungUngDetail();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThuc"))) entity.HinhThuc = reader.GetInt32(reader.GetOrdinal("HinhThuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DongHangTrenCT"))) entity.DongHangTrenCT = reader.GetInt32(reader.GetOrdinal("DongHangTrenCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeQuyDoi"))) entity.TyLeQuyDoi = reader.GetDecimal(reader.GetOrdinal("TyLeQuyDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiPhatHanh"))) entity.NoiPhatHanh = reader.GetString(reader.GetOrdinal("NoiPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<NPLCungUngDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<NPLCungUngDetail> collection = new List<NPLCungUngDetail>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				NPLCungUngDetail entity = new NPLCungUngDetail();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThuc"))) entity.HinhThuc = reader.GetInt32(reader.GetOrdinal("HinhThuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DongHangTrenCT"))) entity.DongHangTrenCT = reader.GetInt32(reader.GetOrdinal("DongHangTrenCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeQuyDoi"))) entity.TyLeQuyDoi = reader.GetDecimal(reader.GetOrdinal("TyLeQuyDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiPhatHanh"))) entity.NoiPhatHanh = reader.GetString(reader.GetOrdinal("NoiPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertNPLCungUngDetail(long master_ID, int hinhThuc, string soChungTu, DateTime ngayChungTu, int dongHangTrenCT, decimal soLuong, string dVT_ID, decimal tyLeQuyDoi, string noiPhatHanh, string maLoaiHinh, string maHaiQuan, string ghiChu)
		{
			NPLCungUngDetail entity = new NPLCungUngDetail();	
			entity.Master_ID = master_ID;
			entity.HinhThuc = hinhThuc;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.DongHangTrenCT = dongHangTrenCT;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.TyLeQuyDoi = tyLeQuyDoi;
			entity.NoiPhatHanh = noiPhatHanh;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@HinhThuc", SqlDbType.Int, HinhThuc);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@DongHangTrenCT", SqlDbType.Int, DongHangTrenCT);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@TyLeQuyDoi", SqlDbType.Decimal, TyLeQuyDoi);
			db.AddInParameter(dbCommand, "@NoiPhatHanh", SqlDbType.NVarChar, NoiPhatHanh);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<NPLCungUngDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NPLCungUngDetail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateNPLCungUngDetail(long id, long master_ID, int hinhThuc, string soChungTu, DateTime ngayChungTu, int dongHangTrenCT, decimal soLuong, string dVT_ID, decimal tyLeQuyDoi, string noiPhatHanh, string maLoaiHinh, string maHaiQuan, string ghiChu)
		{
			NPLCungUngDetail entity = new NPLCungUngDetail();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.HinhThuc = hinhThuc;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.DongHangTrenCT = dongHangTrenCT;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.TyLeQuyDoi = tyLeQuyDoi;
			entity.NoiPhatHanh = noiPhatHanh;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ChungTuKem_NPLCungUngDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@HinhThuc", SqlDbType.Int, HinhThuc);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year == 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@DongHangTrenCT", SqlDbType.Int, DongHangTrenCT);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@TyLeQuyDoi", SqlDbType.Decimal, TyLeQuyDoi);
			db.AddInParameter(dbCommand, "@NoiPhatHanh", SqlDbType.NVarChar, NoiPhatHanh);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<NPLCungUngDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NPLCungUngDetail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateNPLCungUngDetail(long id, long master_ID, int hinhThuc, string soChungTu, DateTime ngayChungTu, int dongHangTrenCT, decimal soLuong, string dVT_ID, decimal tyLeQuyDoi, string noiPhatHanh, string maLoaiHinh, string maHaiQuan, string ghiChu)
		{
			NPLCungUngDetail entity = new NPLCungUngDetail();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.HinhThuc = hinhThuc;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.DongHangTrenCT = dongHangTrenCT;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.TyLeQuyDoi = tyLeQuyDoi;
			entity.NoiPhatHanh = noiPhatHanh;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.MaHaiQuan = maHaiQuan;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@HinhThuc", SqlDbType.Int, HinhThuc);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year == 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@DongHangTrenCT", SqlDbType.Int, DongHangTrenCT);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@TyLeQuyDoi", SqlDbType.Decimal, TyLeQuyDoi);
			db.AddInParameter(dbCommand, "@NoiPhatHanh", SqlDbType.NVarChar, NoiPhatHanh);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.NChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<NPLCungUngDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NPLCungUngDetail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteNPLCungUngDetail(long id)
		{
			NPLCungUngDetail entity = new NPLCungUngDetail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_NPLCungUngDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<NPLCungUngDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NPLCungUngDetail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}