using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmInvoiceDetailInfo : DevExpress.XtraEditors.XtraForm
    {
        private InvoiceDetail InvDetail;
        public Boolean isEdit = false;
        private DateTime Now;
        private string fromInvoice;

        public FrmInvoiceDetailInfo(string ID, string FromInvoice)
        {
            InitializeComponent();
            cboxLicID.EditValue = DBNull.Value;
            cboxProdID.EditValue = DBNull.Value;
            cboxProdID.Properties.DataSource = Product.SelectAll().Tables[0];
            cboxProdID.Properties.DisplayMember = "Name";
            cboxProdID.Properties.ValueMember = "ID";

            cboxLicID.Properties.DataSource = Activation.Server.License.SelectAll().Tables[0];
            cboxLicID.Properties.DisplayMember = "Name";
            cboxLicID.Properties.ValueMember = "ID";
            Now = DateTime.Now;
            fromInvoice = FromInvoice;
            if (ID == null)
            {
                this.NewInvoice();
            }
            else
            {
                
                InvDetail = InvoiceDetail.Load(ID);
                this.Name = "InvD_" + InvDetail.ID;
                lblCaption.Text = "Hóa đơn chi tiết: " + InvDetail.ID;
                this.FillData();
            }
        }

        private void FrmInvoiceDetailInfo_Load(object sender, EventArgs e)
        {
            this.DisableButton(this.isEdit);
        }

        #region Combobox Change.
        private void btnGenCode_Click(object sender, EventArgs e)
        {
            if (cboxLicID.Text != cboxLicID.Properties.NullText &&
                cboxProdID.Text != cboxProdID.Properties.NullText)
            {
                string str = cboxLicID.Text + cboxProdID.Text + Now.ToString();
                txtSerial.Text = Activation.Server.ClassLib.encryptString(str);
            }
            else MessageBox.Show("Kiểm tra lại thông tin");
        }

        private void cboxProdID_EditValueChanged(object sender, EventArgs e)
        {
            this.FillSerialBox();
        }

        private void cboxLicID_EditValueChanged(object sender, EventArgs e)
        {
            this.FillSerialBox();
        }

        private void cboxSerial_EditValueChanged(object sender, EventArgs e)
        {
            txtSerial.Text = cboxSerial.EditValue.ToString();
        }
        #endregion

        #region Add Methods.
        private void FillSerialBox()
        {
            if ((cboxProdID.EditValue != DBNull.Value) && (cboxLicID.EditValue != DBNull.Value))
            {
                string WhereCondition = "LicenseID = '" + cboxLicID.EditValue.ToString() + "'";
                WhereCondition += " AND ProductID = '" + cboxProdID.EditValue.ToString() + "'";
                WhereCondition += " AND Status = 'false'";

                cboxSerial.Properties.DataSource = Serial.SelectDynamic(WhereCondition, "CreateDate").Tables[0];
            }
        }

        private void NewInvoice()
        {
            InvDetail = new InvoiceDetail();
            this.isEdit = true;
            this.Name = "InvD_New";
            InvDetail.InvoiceID = fromInvoice;
            txtID.Text = Math.Abs(Now.GetHashCode()).ToString();
            lblCaption.Text = "Hóa đơn chi tiết: " + txtID.Text;
            btnAddnew.Enabled = false;
        }

        private void GetData()
        {
            InvDetail.ID = txtID.Text;
            InvDetail.ProductID = cboxProdID.EditValue.ToString();
            InvDetail.LicenseID = cboxLicID.EditValue.ToString();
            InvDetail.SerialCode = txtSerial.Text;
            InvDetail.Price = double.Parse(txtPrice.EditValue.ToString());
            InvDetail.PaymentTerms = Activation.Server.License.Load(InvDetail.LicenseID).Term;
        }

        private void FillData()
        {
            txtID.Text = InvDetail.ID;
            cboxProdID.EditValue = InvDetail.ProductID;
            cboxLicID.EditValue = InvDetail.LicenseID;
            txtSerial.Text = InvDetail.SerialCode;
            txtPrice.EditValue = InvDetail.Price;
        }

        private void DisableButton(Boolean val)
        {
            btnCancel.Enabled = val;
            btnSave.Enabled = val;
            groupInfo.Enabled = val;
            btnEdit.Enabled = !val;
        }

        private Boolean ValidForm()
        {
            if (cboxLicID.EditValue == DBNull.Value)
                return false;
            else if (cboxProdID.EditValue == DBNull.Value)
                return false;
            else if (txtSerial.Text == "")
                return false;
            return true;
        }
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.ValidForm())
            {
                this.GetData();
                int countSerial = Serial.SelectDynamic("Code = '" + txtSerial.Text + "'", "CreateDate").Tables[0].Rows.Count;
                Serial Ser = new Serial();
                if (countSerial == 0)
                {
                    Ser.Code = txtSerial.Text;
                    Ser.CreateDate = Now;
                    Ser.EmployeeID = Program.LoginID;
                    Ser.LicenseID = cboxLicID.EditValue.ToString();
                    Ser.ProductID = cboxProdID.EditValue.ToString();
                    Ser.Status = true;
                    Ser.Insert();
                    lblCaption.Text = "Hóa đơn chi tiết: " + InvDetail.ID;
                }
                else
                {
                    Ser = Serial.Load(txtSerial.Text);
                    Ser.Status = true;
                    Ser.Update();
                }
                if (this.Name == "InvD_New")
                {
                    InvDetail.Insert();
                    this.Name = "InvD_" + InvDetail.ID;
                }
                else
                {
                    InvDetail.Update();
                }
                this.DisableButton(false);
                btnAddnew.Enabled = true;

                FrmInvoiceInfo obj = (FrmInvoiceInfo)Application.OpenForms["Inv_" + fromInvoice];
                obj.RefershListOrder();

            }
            else MessageBox.Show("Kiểm tra lại thông tin");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.Name == "InvD_New")
            {
                this.Close();
            }
            else
            {
                this.FillData();
                this.DisableButton(false);
            }
        }

        private void btnAddnew_Click(object sender, EventArgs e)
        {
            Now = DateTime.Now;
            this.NewInvoice();
            this.DisableButton(true);
            cboxLicID.EditValue = DBNull.Value;
            cboxProdID.EditValue = DBNull.Value;
            cboxSerial.Properties.DataSource = null;
            txtSerial.Text = "";
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.isEdit = true;
            this.DisableButton(true);
        }

    }
}