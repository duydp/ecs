﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using SignRemote;
using System.Security.Cryptography;
using System.Web.Services;
using Company.KDT.SHARE.Components;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace SignRemote.BLL
{

   public class ProcessAPINew
    {
        public event EventHandler<SignEventArgs> Sign;
        private TimeSpan _usedTime = new TimeSpan();
        private DateTime _lastStartTime;
        private List<MessageSend> _queueMessages = new List<MessageSend>();
        private bool _flagSign = false;
        public TimeSpan TotalUsedTime
        {
            get
            {
                return _usedTime.Add(DateTime.Now - _lastStartTime);
            }
        }
        private void OnSign(SignEventArgs e)
        {
            if (Sign != null)
            {
                Sign(this, e);
            }
        }
        private bool _isSignLocal = false;
        public ProcessAPINew(bool isSignLocal)
        {
            _isSignLocal = isSignLocal;
        }

        public void DoProcess()
        {
            ThreadPool.QueueUserWorkItem(DoGetContentLocal);//ThreadPool.QueueUserWorkItem(DoGetContentLocal);

        }
        public static bool IsAbort = false;
        private void DoGetContentLocal(object obj)
        {
            while (!IsAbort)
            {
                try {	        
                       #region New
                       Company.KDT.SHARE.Components.Globals.SERVER_NAME = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ServerName").ToString();
                       Company.KDT.SHARE.Components.Globals.USER = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("user").ToString();
                       Company.KDT.SHARE.Components.Globals.PASS = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("pass").ToString();
                       Company.KDT.SHARE.Components.Globals.DataSignLan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DataSignLan").ToString();
                       string cnnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { Company.KDT.SHARE.Components.Globals.SERVER_NAME, Company.KDT.SHARE.Components.Globals.DataSignLan, Company.KDT.SHARE.Components.Globals.USER, Company.KDT.SHARE.Components.Globals.PASS });
                       SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                       SqlConnection cnn = new SqlConnection(cnnString);
                       cnn.Open();
                       string cmd = string.Format("Select top(1) * from t_MSG_INBOX order by CREATE_TIME desc");
                       SqlCommand dbCommand = new SqlCommand(cmd, cnn);
                       IDataReader reader = db.ExecuteReader(dbCommand);
                       cnn.Close();
                       INBOX inBoxRoot = null;
                       List<INBOX> InboxCollection = new List<INBOX>();
                       #endregion
                       if (reader.Read())
                       {
                           inBoxRoot = new INBOX();
                           if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) inBoxRoot.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
                           if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) inBoxRoot.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                           if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) inBoxRoot.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
                           InboxCollection.Add(inBoxRoot);
                       }
                       reader.Close();
                       //if (inBoxRoot != null)
                       //{
                       //    cmd = string.Format("Delete t_MSG_INBOX where GUIDSTR='{0}'", inBoxRoot.GUIDSTR);
                       //    dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
                       //    db.ExecuteNonQuery(dbCommand);
                       //}
                    //INBOX inBoxRoot = INBOX.GetTopDataContent();
                    if (inBoxRoot != null)
                    {
                        MessageSend msgSend = new MessageSend()
                        {
                            From = Globals.UserNameLogin,
                            Password = Globals.PasswordLogin,
                            SendStatus = SendStatusInfo.Request,
                            Message = new SignRemote.Message()
                            {
                                Content = inBoxRoot.MSG_ORIGIN,
                                GuidStr = inBoxRoot.GUIDSTR
                            },
                            TypeMessages = inBoxRoot.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                        };
                        _queueMessages.Add(msgSend);
                        if (!_flagSign)
                        {
                            ThreadPool.QueueUserWorkItem(DoSignContent);
                        }
                    }
                    else
                        Thread.Sleep(new TimeSpan(0, 0, 5));
                }
                catch (Exception ex)
                {
                    OnSign(new SignEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }

        }
        private void DoSignContent(object obj)
        {
            _flagSign = _queueMessages.Count != 0;
            while (!IsAbort && _flagSign)
            {
                try
                {
                    bool isSignVNACCS = false;
                    int index = 0;
                    MessageSend msgSend = _queueMessages[index];
                    this._lastStartTime = DateTime.Now;

                    #region SignData
                    isSignVNACCS = msgSend.TypeMessages == "VNACCS";

                    string content;
                    MessageSignature signInfo;
                    if (!isSignVNACCS)
                    {
                        content = Helpers.ConvertFromBase64(msgSend.Message.Content);
                        signInfo = Helpers.GetSignature(content);
                    }
                    else
                    {
                        content = Helpers.ConvertFromBase64VNACCS(msgSend.Message.Content);
                        Helpers.GetSignature("Test");
                        System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = Cryptography.GetStoreX509Certificate2New(Globals.GetX509CertificatedName);
                        signInfo = new MessageSignature()
                        {
                            Data = SignRemoteVNACCS.SignVNACCS.signEdi(content, x509Certificate2, Globals.PasswordSign),
                        };
                    }

                    OUTBOX.InsertDataSignLocal(msgSend.Message.GuidStr, signInfo.Data, signInfo.FileCert);
                    #endregion
                    OnSign(new SignEventArgs(msgSend, this.TotalUsedTime, null));
                    _queueMessages.RemoveAt(index);
                    if (_queueMessages.Count == 0) _flagSign = false;
                }
                catch (Exception ex)
                {
                    OnSign(new SignEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }


        }
    }
}
