﻿namespace Company.Interface.DanhMucChuan
{
    partial class DoiTacEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DoiTacEditForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtFax = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMail = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDienThoai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTenCongTy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiButton1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiButton3);
            this.grbMain.Controls.Add(this.uiButton2);
            this.grbMain.Size = new System.Drawing.Size(669, 178);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiButton3
            // 
            this.uiButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton3.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton3.Icon")));
            this.uiButton3.Location = new System.Drawing.Point(427, 152);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(75, 23);
            this.uiButton3.TabIndex = 10;
            this.uiButton3.Text = "Đóng";
            this.uiButton3.VisualStyleManager = this.vsmMain;
            this.uiButton3.Click += new System.EventHandler(this.uiButton3_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(230, 152);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 8;
            this.uiButton2.Text = "Lưu";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtFax);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtMail);
            this.uiGroupBox2.Controls.Add(this.txtGhiChu);
            this.uiGroupBox2.Controls.Add(this.txtMa);
            this.uiGroupBox2.Controls.Add(this.txtDienThoai);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.txtTenCongTy);
            this.uiGroupBox2.Controls.Add(this.txtDiaChi);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(645, 130);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtFax
            // 
            this.txtFax.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFax.Location = new System.Drawing.Point(405, 22);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(234, 21);
            this.txtFax.TabIndex = 5;
            this.txtFax.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtFax.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtFax.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(336, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Ghi chú";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(336, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Email";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(336, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Fax";
            // 
            // txtMail
            // 
            this.txtMail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMail.Location = new System.Drawing.Point(405, 49);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(234, 21);
            this.txtMail.TabIndex = 6;
            this.txtMail.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMail.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMail.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(405, 76);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(234, 39);
            this.txtGhiChu.TabIndex = 7;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtGhiChu.VisualStyleManager = this.vsmMain;
            // 
            // txtMa
            // 
            this.txtMa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Location = new System.Drawing.Point(85, 17);
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(234, 21);
            this.txtMa.TabIndex = 1;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMa.VisualStyleManager = this.vsmMain;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.Location = new System.Drawing.Point(85, 98);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(234, 21);
            this.txtDienThoai.TabIndex = 4;
            this.txtDienThoai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDienThoai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDienThoai.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Địa chỉ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên công ty";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(16, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(21, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã";
            // 
            // txtTenCongTy
            // 
            this.txtTenCongTy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenCongTy.Location = new System.Drawing.Point(85, 44);
            this.txtTenCongTy.Name = "txtTenCongTy";
            this.txtTenCongTy.Size = new System.Drawing.Size(234, 21);
            this.txtTenCongTy.TabIndex = 2;
            this.txtTenCongTy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenCongTy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenCongTy.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(85, 71);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(234, 21);
            this.txtDiaChi.TabIndex = 3;
            this.txtDiaChi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Điện thoại";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenCongTy;
            this.rfvTen.ErrorMessage = "\"Tên công ty\" không được để trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMa;
            this.rfvMaHS.ErrorMessage = "\"Mã\" không được để trống.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(311, 152);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(110, 23);
            this.uiButton1.TabIndex = 9;
            this.uiButton1.Text = "Lưu và tạo mới";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // DoiTacEditForm
            // 
            this.ClientSize = new System.Drawing.Size(669, 178);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "DoiTacEditForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Phương thức vận tải";
            this.Load += new System.EventHandler(this.PTVTForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton uiButton3;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCongTy;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoai;
        private Janus.Windows.GridEX.EditControls.EditBox txtFax;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtMail;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Janus.Windows.EditControls.UIButton uiButton1;
    }
}
