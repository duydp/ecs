/*
Run this script on:

        DIRECTOR-SD\SQLKHANH.ECS_TQDT_KD    -  This database will be modified

to synchronize it with:

        ECSTEAM.ECS_TQDT_KD

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 06/12/2012 5:37:54 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[t_HaiQuan_NhomCuaKhau]'
GO
CREATE TABLE [dbo].[t_HaiQuan_NhomCuaKhau]
(
[Cuc_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CuaKhau_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_NhomCuaKhau] on [dbo].[t_HaiQuan_NhomCuaKhau]'
GO
ALTER TABLE [dbo].[t_HaiQuan_NhomCuaKhau] ADD CONSTRAINT [PK_t_HaiQuan_NhomCuaKhau] PRIMARY KEY CLUSTERED  ([Cuc_ID], [CuaKhau_ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_InsertUpdate]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS
IF EXISTS(SELECT [Cuc_ID], [CuaKhau_ID] FROM [dbo].[t_HaiQuan_NhomCuaKhau] WHERE [Cuc_ID] = @Cuc_ID AND [CuaKhau_ID] = @CuaKhau_ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_NhomCuaKhau] 
		SET
		[Cuc_ID] = @Cuc_ID,
		[CuaKhau_ID] = @CuaKhau_ID
		WHERE
			[Cuc_ID] = @Cuc_ID
			AND [CuaKhau_ID] = @CuaKhau_ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_NhomCuaKhau]
	(
			[Cuc_ID],
			[CuaKhau_ID]
	)
	VALUES
	(
			@Cuc_ID,
			@CuaKhau_ID
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhau_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_Load]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Load]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Cuc_ID],
	[CuaKhau_ID]
FROM
	[dbo].[t_HaiQuan_NhomCuaKhau]
WHERE
	[Cuc_ID] = @Cuc_ID
	AND [CuaKhau_ID] = @CuaKhau_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Cuc_ID],
	[CuaKhau_ID]
FROM
	[dbo].[t_HaiQuan_NhomCuaKhau]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhau_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_Update]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Update]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS

UPDATE
	[dbo].[t_HaiQuan_NhomCuaKhau]
SET
[Cuc_ID] = @Cuc_ID,
[CuaKhau_ID] = @CuaKhau_ID
WHERE
	[Cuc_ID] = @Cuc_ID
	AND [CuaKhau_ID] = @CuaKhau_ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhau_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_Delete]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Delete]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_NhomCuaKhau]
WHERE
	[Cuc_ID] = @Cuc_ID
	AND [CuaKhau_ID] = @CuaKhau_ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhau_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_Insert]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_Insert]
	@Cuc_ID varchar(10),
	@CuaKhau_ID varchar(10)
AS
INSERT INTO [dbo].[t_HaiQuan_NhomCuaKhau]
(
	[Cuc_ID],
	[CuaKhau_ID]
)
VALUES
(
	@Cuc_ID,
	@CuaKhau_ID
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhauGetDonVi]'
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[p_HaiQuan_NhomCuaKhauGetDonVi]
	-- Add the parameters for the stored procedure here
	@CuaKhau_ID NVARCHAR(10)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT     t_HaiQuan_DonViHaiQuan.Ten
FROM         t_HaiQuan_DonViHaiQuan INNER JOIN
                      t_HaiQuan_NhomCuaKhau ON t_HaiQuan_DonViHaiQuan.ID = t_HaiQuan_NhomCuaKhau.Cuc_ID
WHERE     (t_HaiQuan_NhomCuaKhau.CuaKhau_ID = @CuaKhau_ID)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]'
GO

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhauGetCuaKhau]    
 -- Add the parameters for the stored procedure here    
 @Cuc_ID nvarchar(10)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
  SELECT     t_HaiQuan_CuaKhau.ID, t_HaiQuan_CuaKhau.Ten    
  FROM         t_HaiQuan_NhomCuaKhau INNER JOIN    
         t_HaiQuan_CuaKhau ON t_HaiQuan_NhomCuaKhau.CuaKhau_ID = t_HaiQuan_CuaKhau.ID    
 WHERE     (t_HaiQuan_NhomCuaKhau.Cuc_ID like '%' +@Cuc_ID +'%')   
    
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[Cuc_ID],
	[CuaKhau_ID]
FROM [dbo].[t_HaiQuan_NhomCuaKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, September 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_NhomCuaKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_NhomCuaKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='2.1', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('2.1', getdate(), null)
	end 
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
