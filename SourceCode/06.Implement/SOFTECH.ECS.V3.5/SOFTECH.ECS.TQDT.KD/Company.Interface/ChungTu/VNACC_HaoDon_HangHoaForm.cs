﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_HaoDon_HangHoaForm : BaseForm
    {
        public KDT_VNACC_HoaDon hoaDon;
        public KDT_VNACC_HangHoaDon hangHoaDon = null;

        public VNACC_HaoDon_HangHoaForm()
        {
            InitializeComponent();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }
        //private DataTable dtHS = new DataTable();
        //private void khoitao_DuLieuChuan()
        //{
        //    System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
        //    dtHS = MaHS.SelectAll();
        //    foreach (DataRow dr in dtHS.Rows)
        //        col.Add(dr["HS10So"].ToString());
        //    ctrMaSoHang.AutoCompleteCustomSource = col;
        //    this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        //    cbDVT_SoLuong1.DataSource = this._DonViTinh;
        //    cbDVT_SoLuong2.DataSource = this._DonViTinh;
        //}
        private void VNACC_HaoDon_HangHoaForm_Load(object sender, EventArgs e)
        {
            SetIDControl();

            //  khoitao_DuLieuChuan();
            grdList.DataSource = hoaDon.HangCollection;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                bool isAddNew = false;
                if (hangHoaDon == null)
                {
                    hangHoaDon = new KDT_VNACC_HangHoaDon();
                    isAddNew = true;
                }
                GetHangHoaDon(hangHoaDon);
                if (isAddNew)
                    hoaDon.HangCollection.Add(hangHoaDon);
                grdList.DataSource = hoaDon.HangCollection;
                grdList.Refetch();
                hangHoaDon = null;
                //SetHangHoaDon(hangHoaDon);
                this.Cursor = Cursors.Default;
                // ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        private void GetHangHoaDon(KDT_VNACC_HangHoaDon hang)
        {
            hang.HoaDon_ID = hoaDon.ID;
            hang.MaHang = txtMaHang.Text;
            hang.MaSoHang = ctrMaSoHang.Code;
            hang.TenHang = txtTenHang.Text;
            hang.MaNuocXX = ctrNuocXX.Code;
            hang.TenNuocXX = ctrNuocXX.Name_VN;
            hang.SoKienHang = txtSoKienHang.Text;
            hang.SoLuong1 = Convert.ToDecimal(txtSoLuong_1.Text);
            hang.MaDVT_SoLuong1 = ctrDVTLuong1.Code;
            hang.SoLuong2 = Convert.ToDecimal(txtSoLuong_2.Text);
            hang.MaDVT_SoLuong2 = ctrDVTLuong2.Code;
            hang.DonGiaHoaDon = Convert.ToDecimal(txtDonGia_HoaDon.Text);
            hang.MaTT_DonGia = ctrMaTT_DonGia.Code;
            hang.TriGiaHoaDon = Convert.ToDecimal(txtTriGia_HoaDon.Text);
            hang.MaTT_GiaTien = ctrMaTT_TriGia.Code;
            hang.LoaiKhauTru = txtLoaiKhauTru.Text;
            hang.SoTienKhauTru = Convert.ToDecimal(txtSoTienKhauTru.Text);
            hang.MaTT_TienKhauTru = ctrMaTT_KhauTru.Code;
        }

        private void SetHangHoaDon(KDT_VNACC_HangHoaDon hang)
        {
            txtMaHang.Text = hang.MaHang;
            ctrMaSoHang.Code = hang.MaSoHang;
            txtTenHang.Text = hang.TenHang;
            ctrNuocXX.Code = hang.MaNuocXX;
            //ctrNuocXX.Name.ToString()=hang.TenNuocXX;
            txtSoKienHang.Text = hang.SoKienHang;
            txtSoLuong_1.Text = hang.SoLuong1.ToString();
            ctrDVTLuong1.Code = hang.MaDVT_SoLuong1;
            txtSoLuong_2.Text = hang.SoLuong2.ToString();
            ctrDVTLuong2.Code = hang.MaDVT_SoLuong2;
            txtDonGia_HoaDon.Text = hang.DonGiaHoaDon.ToString();
            ctrMaTT_DonGia.Code = hang.MaTT_DonGia;
            txtTriGia_HoaDon.Text = hang.TriGiaHoaDon.ToString();
            ctrMaTT_TriGia.Code = hang.MaTT_GiaTien;
            txtLoaiKhauTru.Text = hang.LoaiKhauTru;
            txtSoTienKhauTru.Text = hang.SoTienKhauTru.ToString();
            ctrMaTT_KhauTru.Code = hang.MaTT_TienKhauTru;
        }

        private void grdList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = grdList.SelectedItems;
            // List<KDT_VNACC_HangGiayPhep> hangColl = new List<KDT_VNACC_HangGiayPhep>();
            if (grdList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            hangHoaDon = (KDT_VNACC_HangHoaDon)items[0].GetRow().DataRow;
            SetHangHoaDon(hangHoaDon);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = grdList.SelectedItems;
                List<KDT_VNACC_HangHoaDon> hangColl = new List<KDT_VNACC_HangHoaDon>();
                if (grdList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_HangHoaDon)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangHoaDon hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();
                        hoaDon.HangCollection.Remove(hmd);
                    }

                    grdList.DataSource = hoaDon.HangCollection;
                    grdList.Refetch();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            hangHoaDon = new KDT_VNACC_HangHoaDon();
            SetHangHoaDon(hangHoaDon);
            hangHoaDon = null;
        }

        private void grdList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                //txtSoDong.Tag = "RNO"; //Số dòng
                txtMaHang.Tag = "SNO"; //Mã hàng hóa
                ctrMaSoHang.TagName = "CMD"; //Mã số hàng hóa
                txtTenHang.Tag = "CMN"; //Mô tả hàng hóa
                ctrNuocXX.TagName = "ORC"; //Mã nước xuất xứ
                //txtTenNuocXuatXu.Tag = "ORN"; //Tên nước xuất xứ
                txtSoKienHang.Tag = "KBN"; //Số kiện hàng
                txtSoLuong_1.Tag = "QN1"; //Số lượng (1)
                ctrDVTLuong1.TagName = "QT1"; //Mã đơn vị tính số lượng (1)
                txtSoLuong_2.Tag = "QN2"; //Số lượng (2)
                ctrDVTLuong2.TagName = "QT2"; //Mã đơn vị tính số lượng (2)
                txtDonGia_HoaDon.Tag = "TNK"; //Đơn giá hóa đơn
                ctrMaTT_DonGia.TagName = "TNC"; //Mã tiền tệ của đơn giá hóa đơn
                //ctrDonViCuaDonGiaHoaDonVaSoLuong.Tag = "TSC"; //Đơn vị của đơn giá hóa đơn và số lượng
                txtTriGia_HoaDon.Tag = "KKT"; //Trị giá hóa đơn
                ctrMaTT_TriGia.TagName = "KKC"; //Mã tiền tệ giá tiền
                txtLoaiKhauTru.Tag = "NSR"; //Loại khấu trừ (bao gồm cả phân loại miễn trả tiền)
                txtSoTienKhauTru.Tag = "NGA"; //Số tiền khấu trừ
                ctrMaTT_KhauTru.TagName = "NTC"; //Mã tiền tệ số tiền khấu trừ
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

    }
}
