﻿namespace Company.Interface.Report
{
    partial class TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableHang3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableThue3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableHang2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader2 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter2 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableThue2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableHang4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader4 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableThue4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableHang1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader3 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter3 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableThue1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoPhuLucToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblChiCucHQDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblChiCuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiCucHQCuaKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTieuDeGocPhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLoaiToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayThangNam = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableHang5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter4 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableThue5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport5 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader5 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter5 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongCont = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongKien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter1,
            this.ReportHeader1});
            this.DetailReport1.Level = 2;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHang3});
            this.Detail1.HeightF = 20F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTableHang3
            // 
            this.xrTableHang3.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableHang3.Name = "xrTableHang3";
            this.xrTableHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableHang3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTableHang3.SizeF = new System.Drawing.SizeF(745F, 20F);
            this.xrTableHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang3,
            this.lblMoTaHang3,
            this.lblMaHSHang3,
            this.lblXuatXuHang3,
            this.lblCheDoUuDaiHang3,
            this.lblLuongHang3,
            this.lblDVTHang3,
            this.lblDonGiaHang3,
            this.lblTGNTHang3});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.75757575757575757;
            // 
            // lblSTTHang3
            // 
            this.lblSTTHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTHang3.Name = "lblSTTHang3";
            this.lblSTTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTHang3.StylePriority.UseTextAlignment = false;
            this.lblSTTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang3.Weight = 0.052770448548812666;
            // 
            // lblMoTaHang3
            // 
            this.lblMoTaHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMoTaHang3.Multiline = true;
            this.lblMoTaHang3.Name = "lblMoTaHang3";
            this.lblMoTaHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang3.Weight = 0.27660404321578297;
            // 
            // lblMaHSHang3
            // 
            this.lblMaHSHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaHSHang3.Name = "lblMaHSHang3";
            this.lblMaHSHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHSHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang3.Weight = 0.0853959700048442;
            // 
            // lblXuatXuHang3
            // 
            this.lblXuatXuHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXuatXuHang3.Multiline = true;
            this.lblXuatXuHang3.Name = "lblXuatXuHang3";
            this.lblXuatXuHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblXuatXuHang3.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang3.Weight = 0.087248324633543942;
            // 
            // lblCheDoUuDaiHang3
            // 
            this.lblCheDoUuDaiHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCheDoUuDaiHang3.Name = "lblCheDoUuDaiHang3";
            this.lblCheDoUuDaiHang3.StylePriority.UseBorders = false;
            this.lblCheDoUuDaiHang3.Weight = 0.087248323660803248;
            // 
            // lblLuongHang3
            // 
            this.lblLuongHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongHang3.Multiline = true;
            this.lblLuongHang3.Name = "lblLuongHang3";
            this.lblLuongHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang3.StylePriority.UseBorders = false;
            this.lblLuongHang3.StylePriority.UseTextAlignment = false;
            this.lblLuongHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang3.Weight = 0.10202575503636725;
            // 
            // lblDVTHang3
            // 
            this.lblDVTHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVTHang3.Name = "lblDVTHang3";
            this.lblDVTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVTHang3.StylePriority.UseBorders = false;
            this.lblDVTHang3.StylePriority.UseTextAlignment = false;
            this.lblDVTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang3.Weight = 0.076469935844961645;
            // 
            // lblDonGiaHang3
            // 
            this.lblDonGiaHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang3.Name = "lblDonGiaHang3";
            this.lblDonGiaHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHang3.StylePriority.UseBorders = false;
            this.lblDonGiaHang3.StylePriority.UsePadding = false;
            this.lblDonGiaHang3.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang3.Weight = 0.10950313036552974;
            // 
            // lblTGNTHang3
            // 
            this.lblTGNTHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTGNTHang3.Name = "lblTGNTHang3";
            this.lblTGNTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGNTHang3.StylePriority.UsePadding = false;
            this.lblTGNTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang3.Weight = 0.12273406868935463;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableThue3});
            this.ReportFooter1.HeightF = 90F;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrTableThue3
            // 
            this.xrTableThue3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableThue3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableThue3.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableThue3.Name = "xrTableThue3";
            this.xrTableThue3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableThue3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow10,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTableThue3.SizeF = new System.Drawing.SizeF(745F, 90F);
            this.xrTableThue3.StylePriority.UseBorders = false;
            this.xrTableThue3.StylePriority.UseFont = false;
            this.xrTableThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow5.Weight = 0.51489361702127678;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Multiline = true;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Text = "Loại thuế";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell34.Weight = 0.2744063324538259;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Multiline = true;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.314860798415804;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.Multiline = true;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.10202574511981608;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Text = "Tiền thuế";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell37.Weight = 0.30870712401055411;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.lblTGTTNKHang3,
            this.lblThueSuatNKHang3,
            this.lblTienThueNKHang3});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.36271570291084732;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = "26. Thuế nhập khẩu";
            this.xrTableCell53.Weight = 0.2744063324538259;
            // 
            // lblTGTTNKHang3
            // 
            this.lblTGTTNKHang3.Name = "lblTGTTNKHang3";
            this.lblTGTTNKHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTNKHang3.StylePriority.UsePadding = false;
            this.lblTGTTNKHang3.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang3.Weight = 0.314860798415804;
            // 
            // lblThueSuatNKHang3
            // 
            this.lblThueSuatNKHang3.Name = "lblThueSuatNKHang3";
            this.lblThueSuatNKHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatNKHang3.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatNKHang3.Weight = 0.10202574511981608;
            // 
            // lblTienThueNKHang3
            // 
            this.lblTienThueNKHang3.Name = "lblTienThueNKHang3";
            this.lblTienThueNKHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueNKHang3.StylePriority.UseBorders = false;
            this.lblTienThueNKHang3.StylePriority.UsePadding = false;
            this.lblTienThueNKHang3.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang3.Weight = 0.30870712401055411;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.lblTGTTVATHang3,
            this.lblThueSuatVATHang3,
            this.lblTienThueVATHang3});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.36413718835661141;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Text = "29.Thuế GTGT";
            this.xrTableCell76.Weight = 0.2744063324538259;
            // 
            // lblTGTTVATHang3
            // 
            this.lblTGTTVATHang3.Name = "lblTGTTVATHang3";
            this.lblTGTTVATHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTVATHang3.StylePriority.UsePadding = false;
            this.lblTGTTVATHang3.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang3.Weight = 0.31486079841580406;
            // 
            // lblThueSuatVATHang3
            // 
            this.lblThueSuatVATHang3.Name = "lblThueSuatVATHang3";
            this.lblThueSuatVATHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatVATHang3.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatVATHang3.Weight = 0.10202574511981608;
            // 
            // lblTienThueVATHang3
            // 
            this.lblTienThueVATHang3.Name = "lblTienThueVATHang3";
            this.lblTienThueVATHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueVATHang3.StylePriority.UsePadding = false;
            this.lblTienThueVATHang3.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang3.Weight = 0.30870712401055411;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.lblTongThueHang3});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.36500587390680089;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.StylePriority.UseFont = false;
            this.xrTableCell81.StylePriority.UseTextAlignment = false;
            this.xrTableCell81.Text = "Cộng:";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell81.Weight = 0.691292875989446;
            // 
            // lblTongThueHang3
            // 
            this.lblTongThueHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTongThueHang3.Name = "lblTongThueHang3";
            this.lblTongThueHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueHang3.StylePriority.UseBorders = false;
            this.lblTongThueHang3.StylePriority.UsePadding = false;
            this.lblTongThueHang3.StylePriority.UseTextAlignment = false;
            this.lblTongThueHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueHang3.Weight = 0.30870712401055411;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.ReportHeader1.HeightF = 30F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable11
            // 
            this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable11.SizeF = new System.Drawing.SizeF(745F, 30F);
            this.xrTable11.StylePriority.UseFont = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell22,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.StylePriority.UseTextAlignment = false;
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow14.Weight = 0.51489361702127678;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.Text = "Số TT";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell61.Weight = 0.052770448548812666;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Multiline = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.Text = "18.Mô tả hàng hóa";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 0.276604043215783;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Multiline = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.Text = "19.Mã số hàng hóa";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.085395970004844141;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.Text = "20.Xuất xứ";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 0.087248324633543942;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Text = "21. Chế độ ưu đãi";
            this.xrTableCell22.Weight = 0.087248323985050127;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Multiline = true;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.Text = "22.Lượng";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell65.Weight = 0.10202576560141212;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Multiline = true;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.Text = "23.Đơn vị tính";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell66.Weight = 0.076469802714587309;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.Text = "24.Đơn giá nguyên tệ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 0.10950335566308631;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.Text = "25.Trị giá nguyên tệ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.12273396563288051;
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader2,
            this.ReportFooter2});
            this.DetailReport2.Level = 1;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHang2});
            this.Detail2.HeightF = 20F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTableHang2
            // 
            this.xrTableHang2.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableHang2.Name = "xrTableHang2";
            this.xrTableHang2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTableHang2.SizeF = new System.Drawing.SizeF(745F, 20F);
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang2,
            this.lblMoTaHang2,
            this.lblMaHSHang2,
            this.lblXuatXuHang2,
            this.lblCheDoUuDaiHang2,
            this.lblLuongHang2,
            this.lblDVTHang2,
            this.lblDonGiaHang2,
            this.lblTGNTHang2});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 1;
            // 
            // lblSTTHang2
            // 
            this.lblSTTHang2.Name = "lblSTTHang2";
            this.lblSTTHang2.StylePriority.UseTextAlignment = false;
            this.lblSTTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang2.Weight = 0.15798852508367128;
            // 
            // lblMoTaHang2
            // 
            this.lblMoTaHang2.Name = "lblMoTaHang2";
            this.lblMoTaHang2.StylePriority.UseTextAlignment = false;
            this.lblMoTaHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang2.Weight = 0.82981209945996193;
            // 
            // lblMaHSHang2
            // 
            this.lblMaHSHang2.Name = "lblMaHSHang2";
            this.lblMaHSHang2.StylePriority.UseTextAlignment = false;
            this.lblMaHSHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang2.Weight = 0.25618792090382442;
            // 
            // lblXuatXuHang2
            // 
            this.lblXuatXuHang2.Name = "lblXuatXuHang2";
            this.lblXuatXuHang2.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang2.Weight = 0.2617449694287266;
            // 
            // lblCheDoUuDaiHang2
            // 
            this.lblCheDoUuDaiHang2.Name = "lblCheDoUuDaiHang2";
            this.lblCheDoUuDaiHang2.StylePriority.UseTextAlignment = false;
            this.lblCheDoUuDaiHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblCheDoUuDaiHang2.Weight = 0.26174496203859926;
            // 
            // lblLuongHang2
            // 
            this.lblLuongHang2.Name = "lblLuongHang2";
            this.lblLuongHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang2.StylePriority.UsePadding = false;
            this.lblLuongHang2.StylePriority.UseTextAlignment = false;
            this.lblLuongHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang2.Weight = 0.306400348222692;
            // 
            // lblDVTHang2
            // 
            this.lblDVTHang2.Name = "lblDVTHang2";
            this.lblDVTHang2.StylePriority.UseTextAlignment = false;
            this.lblDVTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang2.Weight = 0.22940878183384764;
            // 
            // lblDonGiaHang2
            // 
            this.lblDonGiaHang2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang2.Name = "lblDonGiaHang2";
            this.lblDonGiaHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHang2.StylePriority.UseBorders = false;
            this.lblDonGiaHang2.StylePriority.UsePadding = false;
            this.lblDonGiaHang2.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang2.Weight = 0.32851036624213004;
            // 
            // lblTGNTHang2
            // 
            this.lblTGNTHang2.Name = "lblTGNTHang2";
            this.lblTGNTHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGNTHang2.StylePriority.UsePadding = false;
            this.lblTGNTHang2.StylePriority.UseTextAlignment = false;
            this.lblTGNTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang2.Weight = 0.36820202678654684;
            // 
            // ReportHeader2
            // 
            this.ReportHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.ReportHeader2.HeightF = 30F;
            this.ReportHeader2.Name = "ReportHeader2";
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(745F, 30F);
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell69});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow3.Weight = 0.38617021276595759;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Text = "Số TT";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.05272440721786404;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Text = "18.Mô tả hàng hóa";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.2766040419187954;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Text = "19.Mã số hàng hóa";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.085395970004844127;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "20.Xuất xứ";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.087248324633543942;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Multiline = true;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Text = "21.Chế độ ưu đãi";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.087248323985050114;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Multiline = true;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Text = "22.Lượng hàng";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.10207180822934836;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Text = "23.Đơn vị tính";
            this.xrTableCell17.Weight = 0.076469802390340388;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Text = "24.Đơn giá nguyên tệ";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 0.109503356635827;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Text = "21.Trị giá nguyên tệ";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell69.Weight = 0.12273396498438673;
            // 
            // ReportFooter2
            // 
            this.ReportFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableThue2});
            this.ReportFooter2.HeightF = 90F;
            this.ReportFooter2.Name = "ReportFooter2";
            // 
            // xrTableThue2
            // 
            this.xrTableThue2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableThue2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableThue2.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableThue2.Name = "xrTableThue2";
            this.xrTableThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableThue2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow15,
            this.xrTableRow21,
            this.xrTableRow22});
            this.xrTableThue2.SizeF = new System.Drawing.SizeF(745F, 90F);
            this.xrTableThue2.StylePriority.UseBorders = false;
            this.xrTableThue2.StylePriority.UseFont = false;
            this.xrTableThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell84});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow1.Weight = 0.51489361702127678;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Text = "Loại thuế";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.2744063324538259;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.314860798415804;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.10202574511981606;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Text = "Tiền thuế";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell84.Weight = 0.30870712401055411;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.lblTGTTNKHang2,
            this.lblThueSuatNKHang2,
            this.lblTienThueNKHang2});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.34755319148936187;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Text = "26. Thuế nhập khẩu";
            this.xrTableCell85.Weight = 0.2744063324538259;
            // 
            // lblTGTTNKHang2
            // 
            this.lblTGTTNKHang2.Name = "lblTGTTNKHang2";
            this.lblTGTTNKHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTNKHang2.StylePriority.UsePadding = false;
            this.lblTGTTNKHang2.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang2.Weight = 0.314860798415804;
            // 
            // lblThueSuatNKHang2
            // 
            this.lblThueSuatNKHang2.Name = "lblThueSuatNKHang2";
            this.lblThueSuatNKHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatNKHang2.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatNKHang2.Weight = 0.10202574511981606;
            // 
            // lblTienThueNKHang2
            // 
            this.lblTienThueNKHang2.Name = "lblTienThueNKHang2";
            this.lblTienThueNKHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueNKHang2.StylePriority.UseBorders = false;
            this.lblTienThueNKHang2.StylePriority.UsePadding = false;
            this.lblTienThueNKHang2.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang2.Weight = 0.30870712401055411;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.lblTGTTVATHang2,
            this.lblThueSuatVATHang2,
            this.lblTienThueVATHang2});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.34894479585968952;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Text = "29.Thuế GTGT";
            this.xrTableCell97.Weight = 0.2744063324538259;
            // 
            // lblTGTTVATHang2
            // 
            this.lblTGTTVATHang2.Name = "lblTGTTVATHang2";
            this.lblTGTTVATHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTVATHang2.StylePriority.UsePadding = false;
            this.lblTGTTVATHang2.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang2.Weight = 0.314860798415804;
            // 
            // lblThueSuatVATHang2
            // 
            this.lblThueSuatVATHang2.Name = "lblThueSuatVATHang2";
            this.lblThueSuatVATHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatVATHang2.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatVATHang2.Weight = 0.10202574511981606;
            // 
            // lblTienThueVATHang2
            // 
            this.lblTienThueVATHang2.Name = "lblTienThueVATHang2";
            this.lblTienThueVATHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueVATHang2.StylePriority.UsePadding = false;
            this.lblTienThueVATHang2.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang2.Weight = 0.30870712401055411;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.lblTongThueHang2});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.34984933870040269;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell103.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.StylePriority.UseFont = false;
            this.xrTableCell103.StylePriority.UseTextAlignment = false;
            this.xrTableCell103.Text = "Cộng:";
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell103.Weight = 0.69129287598944589;
            // 
            // lblTongThueHang2
            // 
            this.lblTongThueHang2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTongThueHang2.Name = "lblTongThueHang2";
            this.lblTongThueHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueHang2.StylePriority.UseBorders = false;
            this.lblTongThueHang2.StylePriority.UsePadding = false;
            this.lblTongThueHang2.StylePriority.UseTextAlignment = false;
            this.lblTongThueHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueHang2.Weight = 0.30870712401055411;
            // 
            // DetailReport4
            // 
            this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.ReportHeader4,
            this.ReportFooter});
            this.DetailReport4.Level = 3;
            this.DetailReport4.Name = "DetailReport4";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHang4});
            this.Detail4.HeightF = 20F;
            this.Detail4.Name = "Detail4";
            // 
            // xrTableHang4
            // 
            this.xrTableHang4.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableHang4.Name = "xrTableHang4";
            this.xrTableHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableHang4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTableHang4.SizeF = new System.Drawing.SizeF(745F, 20F);
            this.xrTableHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang4,
            this.lblMoTaHang4,
            this.lblMaHSHang4,
            this.lblXuatXuHang4,
            this.lblCheDoUuDaiHang4,
            this.lblLuongHang4,
            this.lblDVTHang4,
            this.lblDonGiaHang4,
            this.lblTGNTHang4});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 0.75757575757575757;
            // 
            // lblSTTHang4
            // 
            this.lblSTTHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTHang4.Name = "lblSTTHang4";
            this.lblSTTHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTHang4.StylePriority.UseTextAlignment = false;
            this.lblSTTHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang4.Weight = 0.052770448548812666;
            // 
            // lblMoTaHang4
            // 
            this.lblMoTaHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMoTaHang4.Multiline = true;
            this.lblMoTaHang4.Name = "lblMoTaHang4";
            this.lblMoTaHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang4.Weight = 0.27660404321578297;
            // 
            // lblMaHSHang4
            // 
            this.lblMaHSHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaHSHang4.Name = "lblMaHSHang4";
            this.lblMaHSHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHSHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang4.Weight = 0.0853959700048442;
            // 
            // lblXuatXuHang4
            // 
            this.lblXuatXuHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXuatXuHang4.Multiline = true;
            this.lblXuatXuHang4.Name = "lblXuatXuHang4";
            this.lblXuatXuHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblXuatXuHang4.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang4.Weight = 0.087248324633543942;
            // 
            // lblCheDoUuDaiHang4
            // 
            this.lblCheDoUuDaiHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCheDoUuDaiHang4.Name = "lblCheDoUuDaiHang4";
            this.lblCheDoUuDaiHang4.StylePriority.UseBorders = false;
            this.lblCheDoUuDaiHang4.Weight = 0.087248323660803248;
            // 
            // lblLuongHang4
            // 
            this.lblLuongHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongHang4.Multiline = true;
            this.lblLuongHang4.Name = "lblLuongHang4";
            this.lblLuongHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang4.StylePriority.UseBorders = false;
            this.lblLuongHang4.StylePriority.UseTextAlignment = false;
            this.lblLuongHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang4.Weight = 0.10202567310998302;
            // 
            // lblDVTHang4
            // 
            this.lblDVTHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVTHang4.Name = "lblDVTHang4";
            this.lblDVTHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVTHang4.StylePriority.UseBorders = false;
            this.lblDVTHang4.StylePriority.UseTextAlignment = false;
            this.lblDVTHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang4.Weight = 0.0764698436777794;
            // 
            // lblDonGiaHang4
            // 
            this.lblDonGiaHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang4.Name = "lblDonGiaHang4";
            this.lblDonGiaHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHang4.StylePriority.UseBorders = false;
            this.lblDonGiaHang4.StylePriority.UsePadding = false;
            this.lblDonGiaHang4.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang4.Weight = 0.10950321229191395;
            // 
            // lblTGNTHang4
            // 
            this.lblTGNTHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTGNTHang4.Name = "lblTGNTHang4";
            this.lblTGNTHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGNTHang4.StylePriority.UsePadding = false;
            this.lblTGNTHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang4.Weight = 0.12273416085653689;
            // 
            // ReportHeader4
            // 
            this.ReportHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.ReportHeader4.HeightF = 30F;
            this.ReportHeader4.Name = "ReportHeader4";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(745F, 30F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell5,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell19,
            this.xrTableCell28,
            this.xrTableCell30});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow2.Weight = 0.51489361702127678;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.Text = "Số TT";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.052770448548812666;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.Text = "18.Mô tả hàng hóa";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.276604043215783;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.Text = "19.Mã số hàng hóa";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell9.Weight = 0.085395970004844141;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "20.Xuất xứ";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.087248324633543942;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Text = "21. Chế độ ưu đãi";
            this.xrTableCell13.Weight = 0.087248323985050127;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Multiline = true;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "22.Lượng";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.10202576560141212;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Multiline = true;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.Text = "23.Đơn vị tính";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 0.076469802714587309;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.Text = "24.Đơn giá nguyên tệ";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell28.Weight = 0.10950335566308631;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.Text = "25.Trị giá nguyên tệ";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell30.Weight = 0.12273396563288051;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableThue4});
            this.ReportFooter.HeightF = 90F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTableThue4
            // 
            this.xrTableThue4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableThue4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableThue4.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableThue4.Name = "xrTableThue4";
            this.xrTableThue4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableThue4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow11,
            this.xrTableRow12});
            this.xrTableThue4.SizeF = new System.Drawing.SizeF(745F, 90F);
            this.xrTableThue4.StylePriority.UseBorders = false;
            this.xrTableThue4.StylePriority.UseFont = false;
            this.xrTableThue4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow7.StylePriority.UseTextAlignment = false;
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow7.Weight = 0.51489361702127678;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Multiline = true;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Text = "Loại thuế";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell45.Weight = 0.2744063324538259;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.Multiline = true;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell46.Weight = 0.314860798415804;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.Multiline = true;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell47.Weight = 0.10202574511981608;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Text = "Tiền thuế";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell48.Weight = 0.30870712401055411;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.lblTGTTNKHang4,
            this.lblThueSuatNKHang4,
            this.lblTienThueNKHang4});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.36271570291084732;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Text = "26. Thuế nhập khẩu";
            this.xrTableCell49.Weight = 0.2744063324538259;
            // 
            // lblTGTTNKHang4
            // 
            this.lblTGTTNKHang4.Name = "lblTGTTNKHang4";
            this.lblTGTTNKHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTNKHang4.StylePriority.UsePadding = false;
            this.lblTGTTNKHang4.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang4.Weight = 0.314860798415804;
            // 
            // lblThueSuatNKHang4
            // 
            this.lblThueSuatNKHang4.Name = "lblThueSuatNKHang4";
            this.lblThueSuatNKHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatNKHang4.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang4.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatNKHang4.Weight = 0.10202574511981608;
            // 
            // lblTienThueNKHang4
            // 
            this.lblTienThueNKHang4.Name = "lblTienThueNKHang4";
            this.lblTienThueNKHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueNKHang4.StylePriority.UseBorders = false;
            this.lblTienThueNKHang4.StylePriority.UsePadding = false;
            this.lblTienThueNKHang4.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang4.Weight = 0.30870712401055411;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.lblTGTTVATHang4,
            this.lblThueSuatVATHang4,
            this.lblTienThueVATHang4});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.36413718835661141;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "29.Thuế GTGT";
            this.xrTableCell54.Weight = 0.2744063324538259;
            // 
            // lblTGTTVATHang4
            // 
            this.lblTGTTVATHang4.Name = "lblTGTTVATHang4";
            this.lblTGTTVATHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTVATHang4.StylePriority.UsePadding = false;
            this.lblTGTTVATHang4.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang4.Weight = 0.31486079841580406;
            // 
            // lblThueSuatVATHang4
            // 
            this.lblThueSuatVATHang4.Name = "lblThueSuatVATHang4";
            this.lblThueSuatVATHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatVATHang4.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang4.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatVATHang4.Weight = 0.10202574511981608;
            // 
            // lblTienThueVATHang4
            // 
            this.lblTienThueVATHang4.Name = "lblTienThueVATHang4";
            this.lblTienThueVATHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueVATHang4.StylePriority.UsePadding = false;
            this.lblTienThueVATHang4.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang4.Weight = 0.30870712401055411;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.lblTongThueHang4});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.36500587390680089;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "Cộng:";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell58.Weight = 0.691292875989446;
            // 
            // lblTongThueHang4
            // 
            this.lblTongThueHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTongThueHang4.Name = "lblTongThueHang4";
            this.lblTongThueHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueHang4.StylePriority.UseBorders = false;
            this.lblTongThueHang4.StylePriority.UsePadding = false;
            this.lblTongThueHang4.StylePriority.UseTextAlignment = false;
            this.lblTongThueHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueHang4.Weight = 0.30870712401055411;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.ReportHeader3,
            this.ReportFooter3});
            this.DetailReport3.Level = 0;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHang1});
            this.Detail3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.Detail3.HeightF = 20F;
            this.Detail3.Name = "Detail3";
            this.Detail3.StylePriority.UseFont = false;
            // 
            // xrTableHang1
            // 
            this.xrTableHang1.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableHang1.Name = "xrTableHang1";
            this.xrTableHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableHang1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTableHang1.SizeF = new System.Drawing.SizeF(745F, 20F);
            this.xrTableHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang1,
            this.lblMoTaHang1,
            this.lblMaHSHang1,
            this.lblXuatXuHang1,
            this.lblCheDoUuDaiHang1,
            this.lblLuongHang1,
            this.lblDVTHang1,
            this.lblDonGiaHang1,
            this.lblTGNTHang1});
            this.xrTableRow25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow25.StylePriority.UseFont = false;
            this.xrTableRow25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow25.Weight = 0.60606060606060608;
            // 
            // lblSTTHang1
            // 
            this.lblSTTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTHang1.Name = "lblSTTHang1";
            this.lblSTTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTHang1.StylePriority.UseTextAlignment = false;
            this.lblSTTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang1.Weight = 0.052770448548812666;
            // 
            // lblMoTaHang1
            // 
            this.lblMoTaHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMoTaHang1.Multiline = true;
            this.lblMoTaHang1.Name = "lblMoTaHang1";
            this.lblMoTaHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang1.Weight = 0.276604043215783;
            // 
            // lblMaHSHang1
            // 
            this.lblMaHSHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaHSHang1.Name = "lblMaHSHang1";
            this.lblMaHSHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHSHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang1.Weight = 0.08539597000484421;
            // 
            // lblXuatXuHang1
            // 
            this.lblXuatXuHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXuatXuHang1.Multiline = true;
            this.lblXuatXuHang1.Name = "lblXuatXuHang1";
            this.lblXuatXuHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblXuatXuHang1.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang1.Weight = 0.087248324633543942;
            // 
            // lblCheDoUuDaiHang1
            // 
            this.lblCheDoUuDaiHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCheDoUuDaiHang1.Name = "lblCheDoUuDaiHang1";
            this.lblCheDoUuDaiHang1.StylePriority.UseBorders = false;
            this.lblCheDoUuDaiHang1.Weight = 0.087248323660803248;
            // 
            // lblLuongHang1
            // 
            this.lblLuongHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongHang1.Multiline = true;
            this.lblLuongHang1.Name = "lblLuongHang1";
            this.lblLuongHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang1.StylePriority.UseBorders = false;
            this.lblLuongHang1.StylePriority.UseTextAlignment = false;
            this.lblLuongHang1.Text = "0";
            this.lblLuongHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang1.Weight = 0.10202583696275147;
            // 
            // lblDVTHang1
            // 
            this.lblDVTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVTHang1.Name = "lblDVTHang1";
            this.lblDVTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVTHang1.StylePriority.UseBorders = false;
            this.lblDVTHang1.StylePriority.UseTextAlignment = false;
            this.lblDVTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang1.Weight = 0.07783630408111937;
            // 
            // lblDonGiaHang1
            // 
            this.lblDonGiaHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang1.Name = "lblDonGiaHang1";
            this.lblDonGiaHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHang1.StylePriority.UseBorders = false;
            this.lblDonGiaHang1.StylePriority.UsePadding = false;
            this.lblDonGiaHang1.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang1.Weight = 0.10813676212937204;
            // 
            // lblTGNTHang1
            // 
            this.lblTGNTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTGNTHang1.Name = "lblTGNTHang1";
            this.lblTGNTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGNTHang1.StylePriority.UsePadding = false;
            this.lblTGNTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang1.Weight = 0.12273398676297041;
            // 
            // ReportHeader3
            // 
            this.ReportHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.ReportHeader3.HeightF = 30F;
            this.ReportHeader3.Name = "ReportHeader3";
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable8.SizeF = new System.Drawing.SizeF(745F, 30F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell112});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow24.StylePriority.UseTextAlignment = false;
            this.xrTableRow24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow24.Weight = 0.51489361702127678;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell104.Text = "Số TT";
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell104.Weight = 0.052770448548812666;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell105.Multiline = true;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell105.Text = "18.Mô tả hàng hóa";
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell105.Weight = 0.276604043215783;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Multiline = true;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell106.Text = "19.Mã số hàng hóa";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell106.Weight = 0.085395970004844141;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.Text = "20.Xuất xứ";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell107.Weight = 0.087248324633543942;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Text = "21. Chế độ ưu đãi";
            this.xrTableCell108.Weight = 0.087248323985050127;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell109.Multiline = true;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell109.StylePriority.UseTextAlignment = false;
            this.xrTableCell109.Text = "22.Lượng";
            this.xrTableCell109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell109.Weight = 0.10202583728699832;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Multiline = true;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell110.Text = "23.Đơn vị tính";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell110.Weight = 0.077836339923912493;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell111.Text = "24.Đơn giá nguyên tệ";
            this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell111.Weight = 0.10813668532338674;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell112.Text = "25.Trị giá nguyên tệ";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell112.Weight = 0.12273402707766869;
            // 
            // ReportFooter3
            // 
            this.ReportFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableThue1});
            this.ReportFooter3.HeightF = 90F;
            this.ReportFooter3.Name = "ReportFooter3";
            // 
            // xrTableThue1
            // 
            this.xrTableThue1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableThue1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableThue1.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableThue1.Name = "xrTableThue1";
            this.xrTableThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableThue1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow30,
            this.xrTableRow31});
            this.xrTableThue1.SizeF = new System.Drawing.SizeF(745F, 90F);
            this.xrTableThue1.StylePriority.UseBorders = false;
            this.xrTableThue1.StylePriority.UseFont = false;
            this.xrTableThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125});
            this.xrTableRow26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow26.StylePriority.UseFont = false;
            this.xrTableRow26.StylePriority.UseTextAlignment = false;
            this.xrTableRow26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow26.Weight = 0.454243571335335;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell122.Multiline = true;
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Text = "Loại thuế";
            this.xrTableCell122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell122.Weight = 0.2744063324538259;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell123.Multiline = true;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell123.Weight = 0.314860798415804;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell124.Multiline = true;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell124.Weight = 0.10202574511981608;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell125.StylePriority.UseBorders = false;
            this.xrTableCell125.Text = "Tiền thuế";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell125.Weight = 0.30870712401055411;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.lblTGTTNKHang1,
            this.lblThueSuatNKHang1,
            this.lblTienThueNKHang1});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.30206565722490553;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "26. Thuế nhập khẩu";
            this.xrTableCell126.Weight = 0.2744063324538259;
            // 
            // lblTGTTNKHang1
            // 
            this.lblTGTTNKHang1.Name = "lblTGTTNKHang1";
            this.lblTGTTNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTNKHang1.StylePriority.UsePadding = false;
            this.lblTGTTNKHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang1.Weight = 0.314860798415804;
            // 
            // lblThueSuatNKHang1
            // 
            this.lblThueSuatNKHang1.Name = "lblThueSuatNKHang1";
            this.lblThueSuatNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatNKHang1.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatNKHang1.Weight = 0.10202574511981608;
            // 
            // lblTienThueNKHang1
            // 
            this.lblTienThueNKHang1.Name = "lblTienThueNKHang1";
            this.lblTienThueNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueNKHang1.StylePriority.UseBorders = false;
            this.lblTienThueNKHang1.StylePriority.UsePadding = false;
            this.lblTienThueNKHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang1.Weight = 0.30870712401055411;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138,
            this.lblTGTTVATHang1,
            this.lblThueSuatVATHang1,
            this.lblTienThueVATHang1});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.30348714267066962;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "29.Thuế GTGT";
            this.xrTableCell138.Weight = 0.2744063324538259;
            // 
            // lblTGTTVATHang1
            // 
            this.lblTGTTVATHang1.Name = "lblTGTTVATHang1";
            this.lblTGTTVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTVATHang1.StylePriority.UsePadding = false;
            this.lblTGTTVATHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang1.Weight = 0.31486079841580406;
            // 
            // lblThueSuatVATHang1
            // 
            this.lblThueSuatVATHang1.Name = "lblThueSuatVATHang1";
            this.lblThueSuatVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatVATHang1.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatVATHang1.Weight = 0.10202574511981608;
            // 
            // lblTienThueVATHang1
            // 
            this.lblTienThueVATHang1.Name = "lblTienThueVATHang1";
            this.lblTienThueVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueVATHang1.StylePriority.UsePadding = false;
            this.lblTienThueVATHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang1.Weight = 0.30870712401055411;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.lblTongThueHang1});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.30435582822085905;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.StylePriority.UseFont = false;
            this.xrTableCell142.StylePriority.UseTextAlignment = false;
            this.xrTableCell142.Text = "Cộng:";
            this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell142.Weight = 0.691292875989446;
            // 
            // lblTongThueHang1
            // 
            this.lblTongThueHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongThueHang1.Name = "lblTongThueHang1";
            this.lblTongThueHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueHang1.StylePriority.UseBorders = false;
            this.lblTongThueHang1.StylePriority.UsePadding = false;
            this.lblTongThueHang1.StylePriority.UseTextAlignment = false;
            this.lblTongThueHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueHang1.Weight = 0.30870712401055411;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14,
            this.xrLabel2,
            this.lblTieuDeGocPhai,
            this.xrLabel4,
            this.lblLoaiToKhai,
            this.winControlContainer1});
            this.GroupHeader1.HeightF = 158F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(33F, 83F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32,
            this.xrTableRow34,
            this.xrTableRow33});
            this.xrTable14.SizeF = new System.Drawing.SizeF(745F, 75F);
            this.xrTable14.StylePriority.UseBorders = false;
            this.xrTable14.StylePriority.UseFont = false;
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTableCell21,
            this.lblSoPhuLucToKhai,
            this.xrTableCell4,
            this.lblSoToKhai});
            this.xrTableRow32.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.StylePriority.UseFont = false;
            this.xrTableRow32.Weight = 0.40602764026080373;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UsePadding = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "Chi cục Hải quan đăng ký tờ khai:";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell31.Weight = 0.51574879141506269;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "Phụ lục số:";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.087153583255122069;
            // 
            // lblSoPhuLucToKhai
            // 
            this.lblSoPhuLucToKhai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoPhuLucToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoPhuLucToKhai.Name = "lblSoPhuLucToKhai";
            this.lblSoPhuLucToKhai.StylePriority.UseBorders = false;
            this.lblSoPhuLucToKhai.StylePriority.UseFont = false;
            this.lblSoPhuLucToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoPhuLucToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoPhuLucToKhai.Weight = 0.17678100263852239;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Số tờ khai:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.078496042216358822;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.StylePriority.UseBorders = false;
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "312";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoToKhai.Weight = 0.14182058047493404;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblChiCucHQDangKy,
            this.xrTableCell20,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell26});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.StylePriority.UseBorders = false;
            this.xrTableRow34.Weight = 0.40602764026080373;
            // 
            // lblChiCucHQDangKy
            // 
            this.lblChiCucHQDangKy.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblChiCucHQDangKy.Name = "lblChiCucHQDangKy";
            this.lblChiCucHQDangKy.StylePriority.UseBorders = false;
            this.lblChiCucHQDangKy.Weight = 0.51574879141506269;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Weight = 0.0871535832551221;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Weight = 0.17678100263852239;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.078496042216358808;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Weight = 0.14182058047493404;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblChiCuc,
            this.lblChiCucHQCuaKhau,
            this.xrTableCell23,
            this.lblNgayDangKy,
            this.xrTableCell29,
            this.lblLoaiHinh});
            this.xrTableRow33.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseBorders = false;
            this.xrTableRow33.StylePriority.UseFont = false;
            this.xrTableRow33.Weight = 0.40602768017805674;
            // 
            // lblChiCuc
            // 
            this.lblChiCuc.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.lblChiCuc.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.lblChiCuc.Name = "lblChiCuc";
            this.lblChiCuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.lblChiCuc.StylePriority.UseBorders = false;
            this.lblChiCuc.StylePriority.UseFont = false;
            this.lblChiCuc.StylePriority.UsePadding = false;
            this.lblChiCuc.StylePriority.UseTextAlignment = false;
            this.lblChiCuc.Text = "Chi cục Hải quan cửa khẩu nhập:";
            this.lblChiCuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCuc.Weight = 0.22442758229887902;
            // 
            // lblChiCucHQCuaKhau
            // 
            this.lblChiCucHQCuaKhau.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblChiCucHQCuaKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblChiCucHQCuaKhau.Name = "lblChiCucHQCuaKhau";
            this.lblChiCucHQCuaKhau.StylePriority.UseBorders = false;
            this.lblChiCucHQCuaKhau.StylePriority.UseFont = false;
            this.lblChiCucHQCuaKhau.StylePriority.UseTextAlignment = false;
            this.lblChiCucHQCuaKhau.Text = "c34c";
            this.lblChiCucHQCuaKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCucHQCuaKhau.Weight = 0.29138053160029054;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "Ngày, giờ đăng ký:";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell23.Weight = 0.1344265198066264;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayDangKy.Multiline = true;
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.StylePriority.UseBorders = false;
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayDangKy.Text = "22/22/2012 10:00\r\n";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayDangKy.Weight = 0.12944868280661687;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UsePadding = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "Loại hình:";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell29.Weight = 0.077176841798932888;
            // 
            // lblLoaiHinh
            // 
            this.lblLoaiHinh.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblLoaiHinh.Name = "lblLoaiHinh";
            this.lblLoaiHinh.StylePriority.UseBorders = false;
            this.lblLoaiHinh.StylePriority.UseFont = false;
            this.lblLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblLoaiHinh.Text = "KD";
            this.lblLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblLoaiHinh.Weight = 0.14313984168865437;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(40F, 15F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(192F, 20F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "HẢI QUAN VIỆT NAM";
            // 
            // lblTieuDeGocPhai
            // 
            this.lblTieuDeGocPhai.LocationFloat = new DevExpress.Utils.PointFloat(608F, 67F);
            this.lblTieuDeGocPhai.Name = "lblTieuDeGocPhai";
            this.lblTieuDeGocPhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTieuDeGocPhai.SizeF = new System.Drawing.SizeF(175F, 15F);
            this.lblTieuDeGocPhai.StylePriority.UseTextAlignment = false;
            this.lblTieuDeGocPhai.Text = "HQ/2012-PLTKNK";
            this.lblTieuDeGocPhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(208F, 35F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(392F, 25F);
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "PHỤ LỤC TỜ KHAI HẢI QUAN ĐIỆN TỬ";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblLoaiToKhai
            // 
            this.lblLoaiToKhai.AutoWidth = true;
            this.lblLoaiToKhai.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblLoaiToKhai.LocationFloat = new DevExpress.Utils.PointFloat(342F, 60F);
            this.lblLoaiToKhai.Name = "lblLoaiToKhai";
            this.lblLoaiToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLoaiToKhai.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lblLoaiToKhai.StylePriority.UseFont = false;
            this.lblLoaiToKhai.StylePriority.UseTextAlignment = false;
            this.lblLoaiToKhai.Text = "Nhập khẩu";
            this.lblLoaiToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblLoaiToKhai.WordWrap = false;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(608F, 33F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(175F, 25F);
            this.winControlContainer1.WinControl = this.label1;
            // 
            // label1
            // 
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 24);
            this.label1.TabIndex = 0;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.lblNgayThangNam,
            this.xrLabel9});
            this.GroupFooter1.HeightF = 92F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(508F, 17F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(258F, 33F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "33.Tôi xin cam đoan, chịu trách nhiệm\r\n trước pháp luật về nội dung khai trên tờ " +
                "khai";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayThangNam
            // 
            this.lblNgayThangNam.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNgayThangNam.LocationFloat = new DevExpress.Utils.PointFloat(567F, 50F);
            this.lblNgayThangNam.Name = "lblNgayThangNam";
            this.lblNgayThangNam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayThangNam.SizeF = new System.Drawing.SizeF(175F, 17F);
            this.lblNgayThangNam.StylePriority.UseFont = false;
            this.lblNgayThangNam.Text = "Ngày      tháng      năm";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(542F, 67F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(208F, 23F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "(Người khai ký, ghi rõ họ tên, đóng dấu)";
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.ReportHeader,
            this.ReportFooter4});
            this.DetailReport.Level = 4;
            this.DetailReport.Name = "DetailReport";
            this.DetailReport.Visible = false;
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHang5});
            this.Detail5.HeightF = 20F;
            this.Detail5.Name = "Detail5";
            // 
            // xrTableHang5
            // 
            this.xrTableHang5.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableHang5.Name = "xrTableHang5";
            this.xrTableHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableHang5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTableHang5.SizeF = new System.Drawing.SizeF(745F, 20F);
            this.xrTableHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang5,
            this.lblMoTaHang5,
            this.lblMaHSHang5,
            this.lblXuatXuHang5,
            this.lblCheDoUuDaiHang5,
            this.lblLuongHang5,
            this.lblDVTHang5,
            this.lblDonGiaHang5,
            this.lblTGNTHang5});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow23.Weight = 0.75757575757575757;
            // 
            // lblSTTHang5
            // 
            this.lblSTTHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTHang5.Name = "lblSTTHang5";
            this.lblSTTHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTHang5.StylePriority.UseTextAlignment = false;
            this.lblSTTHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang5.Weight = 0.052770448548812666;
            // 
            // lblMoTaHang5
            // 
            this.lblMoTaHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMoTaHang5.Multiline = true;
            this.lblMoTaHang5.Name = "lblMoTaHang5";
            this.lblMoTaHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang5.Weight = 0.27660404321578297;
            // 
            // lblMaHSHang5
            // 
            this.lblMaHSHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaHSHang5.Name = "lblMaHSHang5";
            this.lblMaHSHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHSHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang5.Weight = 0.0853959700048442;
            // 
            // lblXuatXuHang5
            // 
            this.lblXuatXuHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXuatXuHang5.Multiline = true;
            this.lblXuatXuHang5.Name = "lblXuatXuHang5";
            this.lblXuatXuHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblXuatXuHang5.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang5.Weight = 0.087248324633543942;
            // 
            // lblCheDoUuDaiHang5
            // 
            this.lblCheDoUuDaiHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCheDoUuDaiHang5.Name = "lblCheDoUuDaiHang5";
            this.lblCheDoUuDaiHang5.StylePriority.UseBorders = false;
            this.lblCheDoUuDaiHang5.Weight = 0.087248323660803248;
            // 
            // lblLuongHang5
            // 
            this.lblLuongHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongHang5.Multiline = true;
            this.lblLuongHang5.Name = "lblLuongHang5";
            this.lblLuongHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang5.StylePriority.UseBorders = false;
            this.lblLuongHang5.StylePriority.UseTextAlignment = false;
            this.lblLuongHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang5.Weight = 0.10339106847032803;
            // 
            // lblDVTHang5
            // 
            this.lblDVTHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVTHang5.Name = "lblDVTHang5";
            this.lblDVTHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVTHang5.StylePriority.UseBorders = false;
            this.lblDVTHang5.StylePriority.UseTextAlignment = false;
            this.lblDVTHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang5.Weight = 0.075104397113444243;
            // 
            // lblDonGiaHang5
            // 
            this.lblDonGiaHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang5.Name = "lblDonGiaHang5";
            this.lblDonGiaHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHang5.StylePriority.UseBorders = false;
            this.lblDonGiaHang5.StylePriority.UsePadding = false;
            this.lblDonGiaHang5.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang5.Weight = 0.10950337614468242;
            // 
            // lblTGNTHang5
            // 
            this.lblTGNTHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTGNTHang5.Name = "lblTGNTHang5";
            this.lblTGNTHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGNTHang5.StylePriority.UsePadding = false;
            this.lblTGNTHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang5.Weight = 0.12273404820775857;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.ReportHeader.HeightF = 30F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable7.SizeF = new System.Drawing.SizeF(745F, 30F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell96,
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow28.StylePriority.UseTextAlignment = false;
            this.xrTableRow28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow28.Weight = 0.51489361702127678;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell96.Text = "Số TT";
            this.xrTableCell96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell96.Weight = 0.052770448548812666;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell98.Multiline = true;
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell98.Text = "18.Mô tả hàng hóa";
            this.xrTableCell98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell98.Weight = 0.276604043215783;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Multiline = true;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell99.Text = "19.Mã số hàng hóa";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell99.Weight = 0.085395970004844141;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.Text = "20.Xuất xứ";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell100.Weight = 0.087248324633543942;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.Text = "21. Chế độ ưu đãi";
            this.xrTableCell101.Weight = 0.087248323985050127;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell102.Multiline = true;
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell102.StylePriority.UseTextAlignment = false;
            this.xrTableCell102.Text = "22.Lượng";
            this.xrTableCell102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell102.Weight = 0.10202576560141212;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell113.Multiline = true;
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell113.Text = "23.Đơn vị tính";
            this.xrTableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell113.Weight = 0.076469802714587309;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell114.Text = "24.Đơn giá nguyên tệ";
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell114.Weight = 0.10950335566308631;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell115.Text = "25.Trị giá nguyên tệ";
            this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell115.Weight = 0.12273396563288051;
            // 
            // ReportFooter4
            // 
            this.ReportFooter4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableThue5});
            this.ReportFooter4.HeightF = 90F;
            this.ReportFooter4.Name = "ReportFooter4";
            // 
            // xrTableThue5
            // 
            this.xrTableThue5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableThue5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableThue5.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTableThue5.Name = "xrTableThue5";
            this.xrTableThue5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableThue5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow16,
            this.xrTableRow19,
            this.xrTableRow20});
            this.xrTableThue5.SizeF = new System.Drawing.SizeF(745F, 90F);
            this.xrTableThue5.StylePriority.UseBorders = false;
            this.xrTableThue5.StylePriority.UseFont = false;
            this.xrTableThue5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow13.StylePriority.UseTextAlignment = false;
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow13.Weight = 0.51489361702127678;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell60.Multiline = true;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.Text = "Loại thuế";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell60.Weight = 0.2744063324538259;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Multiline = true;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell70.Weight = 0.314860798415804;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell71.Multiline = true;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell71.Weight = 0.10202574511981608;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.Text = "Tiền thuế";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell72.Weight = 0.30870712401055411;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.lblTGTTNKHang5,
            this.lblThueSuatNKHang5,
            this.lblTienThueNKHang5});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.36271570291084732;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Text = "26. Thuế nhập khẩu";
            this.xrTableCell73.Weight = 0.2744063324538259;
            // 
            // lblTGTTNKHang5
            // 
            this.lblTGTTNKHang5.Name = "lblTGTTNKHang5";
            this.lblTGTTNKHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTNKHang5.StylePriority.UsePadding = false;
            this.lblTGTTNKHang5.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang5.Weight = 0.314860798415804;
            // 
            // lblThueSuatNKHang5
            // 
            this.lblThueSuatNKHang5.Name = "lblThueSuatNKHang5";
            this.lblThueSuatNKHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatNKHang5.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang5.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatNKHang5.Weight = 0.10202574511981608;
            // 
            // lblTienThueNKHang5
            // 
            this.lblTienThueNKHang5.Name = "lblTienThueNKHang5";
            this.lblTienThueNKHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueNKHang5.StylePriority.UseBorders = false;
            this.lblTienThueNKHang5.StylePriority.UsePadding = false;
            this.lblTienThueNKHang5.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang5.Weight = 0.30870712401055411;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell78,
            this.lblTGTTVATHang5,
            this.lblThueSuatVATHang5,
            this.lblTienThueVATHang5});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.36413718835661141;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "29.Thuế GTGT";
            this.xrTableCell78.Weight = 0.2744063324538259;
            // 
            // lblTGTTVATHang5
            // 
            this.lblTGTTVATHang5.Name = "lblTGTTVATHang5";
            this.lblTGTTVATHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTVATHang5.StylePriority.UsePadding = false;
            this.lblTGTTVATHang5.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang5.Weight = 0.31486079841580406;
            // 
            // lblThueSuatVATHang5
            // 
            this.lblThueSuatVATHang5.Name = "lblThueSuatVATHang5";
            this.lblThueSuatVATHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatVATHang5.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang5.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThueSuatVATHang5.Weight = 0.10202574511981608;
            // 
            // lblTienThueVATHang5
            // 
            this.lblTienThueVATHang5.Name = "lblTienThueVATHang5";
            this.lblTienThueVATHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueVATHang5.StylePriority.UsePadding = false;
            this.lblTienThueVATHang5.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang5.Weight = 0.30870712401055411;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.lblTongThueHang5});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.36500587390680089;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.StylePriority.UseFont = false;
            this.xrTableCell83.StylePriority.UseTextAlignment = false;
            this.xrTableCell83.Text = "Cộng:";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell83.Weight = 0.691292875989446;
            // 
            // lblTongThueHang5
            // 
            this.lblTongThueHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongThueHang5.Name = "lblTongThueHang5";
            this.lblTongThueHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueHang5.StylePriority.UseBorders = false;
            this.lblTongThueHang5.StylePriority.UsePadding = false;
            this.lblTongThueHang5.StylePriority.UseTextAlignment = false;
            this.lblTongThueHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueHang5.Weight = 0.30870712401055411;
            // 
            // DetailReport5
            // 
            this.DetailReport5.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.ReportHeader5,
            this.ReportFooter5});
            this.DetailReport5.Level = 5;
            this.DetailReport5.Name = "DetailReport5";
            // 
            // Detail6
            // 
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail6.HeightF = 80F;
            this.Detail6.Name = "Detail6";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39});
            this.xrTable4.SizeF = new System.Drawing.SizeF(745F, 80F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont1,
            this.lblSoHieuCont1,
            this.lblSoLuongKienCont1,
            this.lblTrongLuongCont1,
            this.lblDiaDiem1});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.StylePriority.UseBorders = false;
            this.xrTableRow36.Weight = 1.6800000000000002;
            // 
            // lblSTTCont1
            // 
            this.lblSTTCont1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTCont1.Name = "lblSTTCont1";
            this.lblSTTCont1.StylePriority.UseBorders = false;
            this.lblSTTCont1.StylePriority.UseTextAlignment = false;
            this.lblSTTCont1.Text = "1";
            this.lblSTTCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont1.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont1
            // 
            this.lblSoHieuCont1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoHieuCont1.Name = "lblSoHieuCont1";
            this.lblSoHieuCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont1.StylePriority.UseBorders = false;
            this.lblSoHieuCont1.StylePriority.UsePadding = false;
            this.lblSoHieuCont1.Weight = 0.75490782879708163;
            // 
            // lblSoLuongKienCont1
            // 
            this.lblSoLuongKienCont1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoLuongKienCont1.Name = "lblSoLuongKienCont1";
            this.lblSoLuongKienCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont1.StylePriority.UseBorders = false;
            this.lblSoLuongKienCont1.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont1.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont1.Weight = 0.64356572399992884;
            // 
            // lblTrongLuongCont1
            // 
            this.lblTrongLuongCont1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTrongLuongCont1.Name = "lblTrongLuongCont1";
            this.lblTrongLuongCont1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont1.StylePriority.UseBorders = false;
            this.lblTrongLuongCont1.StylePriority.UsePadding = false;
            this.lblTrongLuongCont1.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont1.Weight = 0.81908767331904864;
            // 
            // lblDiaDiem1
            // 
            this.lblDiaDiem1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDiaDiem1.Name = "lblDiaDiem1";
            this.lblDiaDiem1.StylePriority.UseBorders = false;
            this.lblDiaDiem1.Weight = 0.68217492164119642;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont2,
            this.lblSoHieuCont2,
            this.lblSoLuongKienCont2,
            this.lblTrongLuongCont2,
            this.lblDiaDiem2});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.StylePriority.UseBorders = false;
            this.xrTableRow37.Weight = 1.6800000000000002;
            // 
            // lblSTTCont2
            // 
            this.lblSTTCont2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTCont2.Name = "lblSTTCont2";
            this.lblSTTCont2.StylePriority.UseBorders = false;
            this.lblSTTCont2.StylePriority.UseTextAlignment = false;
            this.lblSTTCont2.Text = "2";
            this.lblSTTCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont2.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont2
            // 
            this.lblSoHieuCont2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoHieuCont2.Name = "lblSoHieuCont2";
            this.lblSoHieuCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont2.StylePriority.UseBorders = false;
            this.lblSoHieuCont2.StylePriority.UsePadding = false;
            this.lblSoHieuCont2.Weight = 0.75490782879708163;
            // 
            // lblSoLuongKienCont2
            // 
            this.lblSoLuongKienCont2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoLuongKienCont2.Name = "lblSoLuongKienCont2";
            this.lblSoLuongKienCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont2.StylePriority.UseBorders = false;
            this.lblSoLuongKienCont2.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont2.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont2.Weight = 0.64356572399992884;
            // 
            // lblTrongLuongCont2
            // 
            this.lblTrongLuongCont2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTrongLuongCont2.Name = "lblTrongLuongCont2";
            this.lblTrongLuongCont2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont2.StylePriority.UseBorders = false;
            this.lblTrongLuongCont2.StylePriority.UsePadding = false;
            this.lblTrongLuongCont2.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont2.Weight = 0.81908767331904864;
            // 
            // lblDiaDiem2
            // 
            this.lblDiaDiem2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDiaDiem2.Name = "lblDiaDiem2";
            this.lblDiaDiem2.StylePriority.UseBorders = false;
            this.lblDiaDiem2.Weight = 0.68217492164119642;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont3,
            this.lblSoHieuCont3,
            this.lblSoLuongKienCont3,
            this.lblTrongLuongCont3,
            this.lblDiaDiem3});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.StylePriority.UseBorders = false;
            this.xrTableRow38.Weight = 1.6800000000000002;
            // 
            // lblSTTCont3
            // 
            this.lblSTTCont3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTCont3.Name = "lblSTTCont3";
            this.lblSTTCont3.StylePriority.UseBorders = false;
            this.lblSTTCont3.StylePriority.UseTextAlignment = false;
            this.lblSTTCont3.Text = "3";
            this.lblSTTCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont3.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont3
            // 
            this.lblSoHieuCont3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoHieuCont3.Name = "lblSoHieuCont3";
            this.lblSoHieuCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoHieuCont3.StylePriority.UseBorders = false;
            this.lblSoHieuCont3.StylePriority.UsePadding = false;
            this.lblSoHieuCont3.Weight = 0.75490782879708163;
            // 
            // lblSoLuongKienCont3
            // 
            this.lblSoLuongKienCont3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoLuongKienCont3.Name = "lblSoLuongKienCont3";
            this.lblSoLuongKienCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongKienCont3.StylePriority.UseBorders = false;
            this.lblSoLuongKienCont3.StylePriority.UsePadding = false;
            this.lblSoLuongKienCont3.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont3.Weight = 0.64356572399992884;
            // 
            // lblTrongLuongCont3
            // 
            this.lblTrongLuongCont3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTrongLuongCont3.Name = "lblTrongLuongCont3";
            this.lblTrongLuongCont3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTrongLuongCont3.StylePriority.UseBorders = false;
            this.lblTrongLuongCont3.StylePriority.UsePadding = false;
            this.lblTrongLuongCont3.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont3.Weight = 0.81908767331904864;
            // 
            // lblDiaDiem3
            // 
            this.lblDiaDiem3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDiaDiem3.Name = "lblDiaDiem3";
            this.lblDiaDiem3.StylePriority.UseBorders = false;
            this.lblDiaDiem3.Weight = 0.68217492164119642;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.lblSoHieuCont4,
            this.lblSoLuongKienCont4,
            this.lblTrongLuongCont4,
            this.lblDiaDiem4});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1.6800000000000002;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Text = "4";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell42.Weight = 0.10026385224274405;
            // 
            // lblSoHieuCont4
            // 
            this.lblSoHieuCont4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoHieuCont4.Multiline = true;
            this.lblSoHieuCont4.Name = "lblSoHieuCont4";
            this.lblSoHieuCont4.StylePriority.UseBorders = false;
            this.lblSoHieuCont4.Text = "\r\n";
            this.lblSoHieuCont4.Weight = 0.75490782879708163;
            // 
            // lblSoLuongKienCont4
            // 
            this.lblSoLuongKienCont4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoLuongKienCont4.Name = "lblSoLuongKienCont4";
            this.lblSoLuongKienCont4.StylePriority.UseBorders = false;
            this.lblSoLuongKienCont4.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont4.Weight = 0.64356572399992884;
            // 
            // lblTrongLuongCont4
            // 
            this.lblTrongLuongCont4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTrongLuongCont4.Name = "lblTrongLuongCont4";
            this.lblTrongLuongCont4.StylePriority.UseBorders = false;
            this.lblTrongLuongCont4.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont4.Weight = 0.81908767331904864;
            // 
            // lblDiaDiem4
            // 
            this.lblDiaDiem4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDiaDiem4.Name = "lblDiaDiem4";
            this.lblDiaDiem4.StylePriority.UseBorders = false;
            this.lblDiaDiem4.Weight = 0.68217492164119642;
            // 
            // ReportHeader5
            // 
            this.ReportHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.ReportHeader5.HeightF = 47F;
            this.ReportHeader5.Name = "ReportHeader5";
            // 
            // xrTable6
            // 
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29,
            this.xrTableRow35});
            this.xrTable6.SizeF = new System.Drawing.SizeF(745F, 47F);
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32});
            this.xrTableRow29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.StylePriority.UseBorders = false;
            this.xrTableRow29.StylePriority.UseFont = false;
            this.xrTableRow29.Weight = 0.79999999999999993;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "31.Lượng hàng, số hiệu container";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell32.Weight = 3;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.StylePriority.UseBorders = false;
            this.xrTableRow35.StylePriority.UseFont = false;
            this.xrTableRow35.StylePriority.UseTextAlignment = false;
            this.xrTableRow35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow35.Weight = 1.08;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Multiline = true;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.Text = "Số\r\n TT";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell33.Weight = 0.10026385224274405;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "a.Số hiệu container";
            this.xrTableCell38.Weight = 0.75490782879708174;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Text = "b.Số lượng kiện trong container";
            this.xrTableCell39.Weight = 0.64356572399992884;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "c.Trọng lượng hàng trong container";
            this.xrTableCell40.Weight = 0.81908767331904864;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "d. Địa điểm đóng hàng";
            this.xrTableCell41.Weight = 0.68217492164119642;
            // 
            // ReportFooter5
            // 
            this.ReportFooter5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter5.HeightF = 20F;
            this.ReportFooter5.Name = "ReportFooter5";
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40});
            this.xrTable5.SizeF = new System.Drawing.SizeF(745F, 20F);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.lblTongCont,
            this.xrTableCell44,
            this.lblTongKien,
            this.xrTableCell50,
            this.lblTongTrongLuong,
            this.xrTableCell51});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1.6800000000000002;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell43.Weight = 0.10026385224274405;
            // 
            // lblTongCont
            // 
            this.lblTongCont.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.lblTongCont.Name = "lblTongCont";
            this.lblTongCont.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTongCont.StylePriority.UseFont = false;
            this.lblTongCont.StylePriority.UsePadding = false;
            this.lblTongCont.StylePriority.UseTextAlignment = false;
            this.lblTongCont.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTongCont.Weight = 0.75490782879708163;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.StylePriority.UseFont = false;
            this.xrTableCell44.StylePriority.UsePadding = false;
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            this.xrTableCell44.Text = "Cộng";
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell44.Weight = 0.15668219085902477;
            // 
            // lblTongKien
            // 
            this.lblTongKien.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongKien.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongKien.Name = "lblTongKien";
            this.lblTongKien.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongKien.StylePriority.UseBorders = false;
            this.lblTongKien.StylePriority.UseFont = false;
            this.lblTongKien.StylePriority.UsePadding = false;
            this.lblTongKien.StylePriority.UseTextAlignment = false;
            this.lblTongKien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongKien.Weight = 0.48688353314090393;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.StylePriority.UsePadding = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = "Cộng";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell50.Weight = 0.1467921588071755;
            // 
            // lblTongTrongLuong
            // 
            this.lblTongTrongLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTrongLuong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongTrongLuong.Name = "lblTongTrongLuong";
            this.lblTongTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongTrongLuong.StylePriority.UseBorders = false;
            this.lblTongTrongLuong.StylePriority.UseFont = false;
            this.lblTongTrongLuong.StylePriority.UsePadding = false;
            this.lblTongTrongLuong.StylePriority.UseTextAlignment = false;
            this.lblTongTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTrongLuong.Weight = 0.67430893733066521;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell51.Weight = 0.68016149882240462;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 4F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 18F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.DetailReport1,
            this.DetailReport2,
            this.DetailReport4,
            this.DetailReport3,
            this.GroupHeader1,
            this.GroupFooter1,
            this.DetailReport,
            this.DetailReport5,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Margins = new System.Drawing.Printing.Margins(5, 16, 4, 18);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTableHang3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang3;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable xrTableHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader2;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang3;
        private DevExpress.XtraReports.UI.XRTable xrTableThue3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueHang3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang2;
        private DevExpress.XtraReports.UI.XRTable xrTableThue2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueHang2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTableHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter3;
        private DevExpress.XtraReports.UI.XRTable xrTableThue1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueHang1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell lblSoPhuLucToKhai;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell lblChiCucHQCuaKhau;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblTieuDeGocPhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayThangNam;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell lblChiCuc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell lblChiCucHQDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRLabel lblLoaiToKhai;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraReports.UI.XRTable xrTableHang4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang4;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRTable xrTableThue4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang4;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueHang4;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.XRTable xrTableHang5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang5;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter4;
        private DevExpress.XtraReports.UI.XRTable xrTableThue5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang5;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueHang5;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport5;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader5;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter5;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem4;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell lblTongCont;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell lblTongKien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
    }
}
