@echo off
cls
echo.
echo 	*********************************************************
echo 		Windows 2003 DDK must be installed to build
echo 		Please use Build Environments shortcut
echo 		to run this file.
echo 					   SecureMetric 2008.9.29
echo 	*********************************************************
echo.
echo Copying file...
copy ..\..\include\SDX.h SDX.h

echo Building file...
build -c
echo Delete File...
del SDX.h
@echo off
echo Build succeeded