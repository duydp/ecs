using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmLicenseInfo : DevExpress.XtraEditors.XtraForm
    {
        private Activation.Server.License Lic;
        public Boolean isEdit = false;
        public FrmLicenseInfo(string ID)
        {
            InitializeComponent();
            if (ID == null)
            {
                txtLicID.Text = Math.Abs(DateTime.Now.GetHashCode()).ToString();
                this.Text = "Thêm  bản quyền: " + txtLicID.Text;
                lblCaption.Text = "Thêm  bản quyền: " + txtLicID.Text;
                Lic = new Activation.Server.License();
                this.Name = "Lic_New";
                this.isEdit = true;
            }
            else
            {
                txtLicID.Text = ID;
                this.Text = "Bản quyền: " + txtLicID.Text;
                lblCaption.Text = "Bản quyền: " + txtLicID.Text;
                Lic = Activation.Server.License.Load(ID);
                this.Name = "Lic_" + ID;
                this.FillData();
            }
        }

        #region Add Methods.
        private void GetData()
        {
            Lic.ID = txtLicID.Text;
            Lic.Name = txtLicName.Text;
            Lic.Description = txtLicDesc.Text;
            Lic.NumberClient = int.Parse(txtLicNumberClient.Value.ToString());
            Lic.Term = int.Parse(txtLicTerm.Value.ToString());
        }

        private void FillData()
        {
            txtLicID.Text = Lic.ID;
            txtLicName.Text = Lic.Name;
            txtLicDesc.Text = Lic.Description;
            txtLicNumberClient.Value = Lic.NumberClient;
            txtLicTerm.Value = Lic.Term;
        }

        private void DisableButton(Boolean val)
        {
            btnCancel.Enabled = val;
            btnSave.Enabled = val;
            groupLicenseInfo.Enabled = val;
            btnEdit.Enabled = !val;
        }
        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.GetData();
            if (this.Name == "Lic_New")
            {
                Lic.Insert();
                this.Text = "Bản quyền: " + Lic.ID;
                lblCaption.Text = "Bản quyền: " + Lic.ID;
                this.Name = "Lic_" + Lic.ID;
            }
            else
            {
                Lic.Update();
            }
            this.DisableButton(false);
            if (ClassLib.IsFormShowed("FrmLicenseManager"))
            {
                FrmLicenseManager obj = (FrmLicenseManager)Application.OpenForms["FrmLicenseManager"];
                obj.RefreshData();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.isEdit = true;
            this.DisableButton(true);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.Name == "Lic_New")
            {
                this.Close();
            }
            else
            {
                this.FillData();
                this.DisableButton(false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmLicenseInfo_Load(object sender, EventArgs e)
        {
            this.DisableButton(isEdit);
        }

    }
}