﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;

using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.Report.GC;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.SXXK;
using System.Data.SqlClient;
using Company.Interface.KDT.GC;
namespace Company.Interface
{
    public partial class XuLyThanhKhoan_BC06_TT38 : BaseForm
    {
        public HopDong HD = new HopDong();
        private DataTable tb = new DataTable();
        private DataTable tbDM_SP = new DataTable();
        private DataTable tb_M15 = new DataTable();
        public string ListHD = "";
        int totalRows = 0;
        int index = 0;
        public XuLyThanhKhoan_BC06_TT38()
        {
            InitializeComponent();

        }

        public DataSet GetXNT()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();

            //Lấy danh sách SP chưa có định mức
            try
            {
                //Check HopDong
                string test = "if exists(select * from t_KDT_ToKhaiMauDich where SoHopDong ='' or SoHopDong is null) " +
    "update t_kdt_tokhaimaudich  set SoHopDong = (select top 1 sohopdong from t_KDT_GC_HopDong h where h.id = t_kdt_tokhaimaudich.IDHopDong) where t_kdt_tokhaimaudich.soHopDong='' " +
" if exists(select * from t_kdt_gc_tokhaichuyentiep where SoHopDongDV ='' or SoHopDongDV is null)		" +
"	update t_kdt_gc_tokhaichuyentiep  set SoHopDongDV = (select top 1 sohopdong from t_KDT_GC_HopDong h where h.id = t_kdt_gc_tokhaichuyentiep.IDHopDong) where t_kdt_gc_tokhaichuyentiep.SoHopDongDV is null";
                DbCommand cmd = db.GetSqlStringCommand(test);
                db.ExecuteNonQuery(cmd);
                //Check HopDong
                #region
                string sql = " SELECT  0 AS STT ,																										" +
                "         152 AS SoTaiKhoan ,                                                                                              " +
                "         n.OldHD_ID AS [HopDong_ID] ,                                                                                     " +
                "         hd.SoHopDong ,                                                                                                   " +
                "         t.SoToKhai ,                                                                                                     " +
                "         t.SoToKhaiVnacc ,                                                                                                " +
                "         t.NgayDangKy ,                                                                                                   " +
                "         t.MaLoaiHinh ,                                                                                                   " +
                "         [Ma] ,                                                                                                           " +
                "         [Ten] ,                                                                                                          " +
                "         '' AS [MaHS] ,                                                                                                   " +
                "         n.DVT AS DVT_ID ,                                                                                                " +
                "         TongLuongNK AS [SoLuongDangKy] ,                                                                                 " +
                "         TongLuongNK AS [SoLuongDaNhap] ,                                                                                 " +
                "         TongLuongXK AS [SoLuongDaDung] ,                                                                                 " +
                "         TongLuongCU AS [SoLuongCungUng] ,                                                                                " +
                "         ( TongLuongNK + TongLuongCU ) - TongLuongXK AS LuongTon ,                                                        " +
                "         t.SoLuong ,                                                                                                      " +
                "         t.SoLuong AS LuongSD_TK ,                                                                                        " +
                "         t.SoLuong AS LuongTon_TK ,                                                                                       " +
                "         CASE WHEN t.TyGiaTT>0 THEN CONVERT(DECIMAL(24, 6), ( t.DonGiaTT / t.TyGiaTT )) ELSE 0 END AS DonGiaKB ,          " +
                "         CONVERT(DECIMAL(24, 6), t.DonGiaTT) AS DonGiaTT ,                                                                " +
                "         CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaTT ,                                                                " +
                "         CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaSuDung ,                                                            " +
                "         CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TonTriGia ,                                                               " +
                "         t.TyGiaTT ,                                                                                                      " +
                "         t.MaDVT                                                                                                          " +
                " FROM    [dbo].[t_GC_ThanhKhoanHDGC] n                                                                                    " +
                "         JOIN ( " +
                " SELECT   dk.HopDong_ID AS IDHopDong ,                                                " +
                "                         dk.SoTiepNhan AS SoToKhai ,                                  " +
                "                         dk.SoTiepNhan AS SoToKhaiVnacc ,                             " +
                "                         dk.NgayTiepNhan AS NgayDangKy ,                              " +
                "                         'NLHUY' AS MaLoaiHinh ,                                      " +
                "                         cu.MaNPL AS MaPhu ,                                          " +
                "                         cu.TenNPL AS TenHang ,                                       " +
                "                         cu.LuongNPLHuy AS SoLuong ,                                  " +
                "                         1 AS DonGiaTT ,                                              " +
                "                         1 AS TriGiaTT ,                                              " +
                "                         1 AS TyGiaTT ,                                               " +
                "                         'VND' AS MaDVT                                               " +
                "                FROM     t_KDT_GC_NPLHuyDangKy AS dk                                  " +
                "                         JOIN dbo.t_KDT_GC_NPLHuy AS cu ON dk.ID = cu.Master_ID       " +
                "                WHERE    dk.HopDong_ID IN ( " + this.HD.ID + " )                          " +
                " UNION ALL " +
                " SELECT   dk.HopDong_ID AS IDHopDong ,                                                       " +
                "                         dk.SoTiepNhan AS SoToKhai ,                                         " +
                "                         dk.SoTiepNhan AS SoToKhaiVnacc ,                                    " +
                "                         dk.NgayTiepNhan AS NgayDangKy ,                                     " +
                "                         'NPLCU' AS MaLoaiHinh ,                                             " +
                "                         cu.MaNPL AS MaPhu ,                                                 " +
                "                         cu.TenNPL AS TenHang ,                                              " +
                "                         cu.LuongCungUng AS SoLuong ,                                        " +
                "                         1 AS DonGiaTT ,                                                     " +
                "                         1 AS TriGiaTT ,                                                     " +
                "                         1 AS TyGiaTT ,                                                      " +
                "                         'VND' AS MaDVT                                                      " +
                "                FROM     t_KDT_GC_CungUngDangKy AS dk                                        " +
                "                         JOIN dbo.t_KDT_GC_CungUng AS cu ON dk.ID = cu.Master_ID             " +
                "                WHERE    dk.HopDong_ID IN ( " + this.HD.ID + " )                                 " +
                "                   UNION ALL " +
                                    " SELECT   tkmd.IDHopDong ,                                                                                 " +
                "                         tkmd.SoToKhai ,                                                                                  " +
                "                         tkmd.Huongdan_PL AS SoToKhaiVnacc ,                                                              " +
                "                         tkmd.NgayDangKy ,                                                                                " +
                "                         tkmd.MaLoaiHinh ,                                                                                " +
                "                         hmd.MaHang AS MaPhu ,                                                                            " +
                "                         hmd.TenHang ,                                                                                    " +
                "                         hmd.SoLuong ,                                                                                    " +
                "                         hmd.DonGiaTT ,                                                                                   " +
                "                         hmd.TriGiaTT ,                                                                                   " +
                "                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
                "                              THEN ( SELECT TOP 1 g.TyGiaTinhThue                                                              " +
                "                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
                "                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
                "                                                               ID                                                         " +
                "                                                           FROM                                                           " +
                "                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
                "                                                           WHERE                                                          " +
                "                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
                "                                                         )                                                                " +
                "                                   )                                                                                      " +
                "                              ELSE 0                                                                                      " +
                "                         END AS TyGiaTT ,                                                                                 " +
                "                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
                "                              THEN ( SELECT TOP 1  g.MaTTTyGiaTinhThue                                                          " +
                "                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
                "                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
                "                                                               ID                                                         " +
                "                                                           FROM                                                           " +
                "                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
                "                                                           WHERE                                                          " +
                "                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)                           " +
                "                                                         )                                                                " +
                "                                   )                                                                                      " +
                "                              ELSE ''                                                                                     " +
                "                         END AS MaDVT                                                                                     " +
                "                FROM     t_KDT_GC_HangChuyenTiep hmd                                                                      " +
                "                         INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID                             " +
                "                WHERE    tkmd.IDHopDong IN ( " + this.HD.ID + " )                                                                " +
                "                         AND maloaihinh LIKE '%NV%'                                                                       " +
                "                         AND LoaiHangHoa = 'N'                                                                            " +
                "                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
                    //"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'                  " +
                "                UNION ALL                                                                                                     " +
                "                SELECT   tkmd.IDHopDong ,                                                                                 " +
                "                         tkmd.SoToKhai ,                                                                                  " +
                "                         tkmd.Huongdan_PL AS SoToKhaiVnacc ,                                                              " +
                "                         tkmd.NgayDangKy ,                                                                                " +
                "                         tkmd.MaLoaiHinh ,                                                                                " +
                "                         hmd.MaHang AS MaPhu ,                                                                            " +
                "                         hmd.TenHang ,                                                                                    " +
                "                         hmd.SoLuong ,                                                                                    " +
                "                         hmd.DonGiaTT ,                                                                                   " +
                "                         hmd.TriGiaTT ,                                                                                   " +
                "                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
                "                              THEN ( SELECT TOP 1  g.TyGiaTinhThue                                                              " +
                "                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
                "                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
                "                                                               ID                                                         " +
                "                                                           FROM                                                           " +
                "                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
                "                                                           WHERE                                                          " +
                "                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
                "                                                         )                                                                " +
                "                                   )                                                                                      " +
                "                              ELSE 0                                                                                      " +
                "                         END AS TyGiaTT ,                                                                                 " +
                "                         CASE WHEN tkmd.Huongdan_PL <> ''                                                                 " +
                "                              THEN ( SELECT TOP 1 g.MaTTTyGiaTinhThue                                                          " +
                "                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
                "                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
                "                                                               ID                                                         " +
                "                                                           FROM                                                           " +
                "                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
                "                                                           WHERE                                                          " +
                "                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
                "                                                         )                                                                " +
                "                                   )                                                                                      " +
                "                              ELSE ''                                                                                     " +
                "                         END AS MaDVT                                                                                     " +
                "                FROM     t_KDT_GC_HangChuyenTiep hmd                                                                      " +
                "                         INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID                             " +
                "                WHERE    tkmd.IDHopDong IN ( " + this.HD.ID + " )                                                                " +
                "                         AND maloaihinh LIKE '%XV%'                                                                       " +
                "                         AND LoaiHangHoa = 'N'                                                                            " +
                "                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
                    //"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'                  " +
                "                UNION ALL                                                                                                     " +
                "                SELECT   tkmd.IDHopDong ,                                                                                 " +
                "                         tkmd.SoToKhai ,                                                                                  " +
                "                         tkmd.LoaiVanDon AS SoToKhaiVnacc ,                                                               " +
                "                         tkmd.NgayDangKy ,                                                                                " +
                "                         tkmd.MaLoaiHinh ,                                                                                " +
                "                         hmd.MaPhu ,                                                                                      " +
                "                         hmd.TenHang ,                                                                                    " +
                "                         hmd.SoLuong ,                                                                                    " +
                "                         hmd.DonGiaTT ,                                                                                   " +
                "                         hmd.TriGiaTT ,                                                                                   " +
                "                         CASE WHEN tkmd.LoaiVanDon <> ''                                                                  " +
                "                              THEN ( SELECT TOP 1 g.TyGiaTinhThue                                                              " +
                "                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
                "                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
                "                                                               ID                                                         " +
                "                                                           FROM                                                           " +
                "                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
                "                                                           WHERE                                                          " +
                "                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
                "                                                         )                                                                " +
                "                                   )                                                                                      " +
                "                              ELSE 0                                                                                      " +
                "                         END AS TyGiaTT ,                                                                                 " +
                "                         CASE WHEN tkmd.LoaiVanDon <> ''                                                                  " +
                "                              THEN ( SELECT TOP 1  g.MaTTTyGiaTinhThue                                                          " +
                "                                     FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g                                               " +
                "                                     WHERE   Master_ID = ( SELECT TOP 1                                                   " +
                "                                                               ID                                                         " +
                "                                                           FROM                                                           " +
                "                                                               t_KDT_VNACC_ToKhaiMauDich t                                " +
                "                                                           WHERE                                                          " +
                "                                                               tkmd.SoToKhai = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS=t.SoToKhai)" +
                "                                                         )                                                                " +
                "                                   )                                                                                      " +
                "                              ELSE ''                                                                                     " +
                "                         END AS MaDVT                                                                                     " +
                "                FROM     t_KDT_HangMauDich hmd                                                                            " +
                "                         INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID                                     " +
                "                WHERE    tkmd.IDHopDong IN ( " + this.HD.ID + " )                                                                  " +
                "                         AND maloaihinh LIKE '%NV%'                                                                       " +
                "                         AND LoaiHangHoa = 'N'                                                                            " +
                "                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
                    //"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'          " +
                "                UNION ALL                                                                                                     " +
                "                SELECT   tkmd.IDHopDong ,                                                                                 " +
                "                         tkmd.SoToKhai ,                                                                                  " +
                "                         CONVERT(VARCHAR(18), tkmd.LoaiVanDon) AS SoToKhaiVnacc ,                                           " +
                "                         tkmd.NgayDangKy ,                                                                                " +
                "                         tkmd.MaLoaiHinh ,                                                                                " +
                "                         hmd.MaPhu ,                                                                                      " +
                "                         hmd.TenHang ,                                                                                    " +
                "                         hmd.SoLuong ,                                                                                    " +
                "                         hmd.DonGiaTT ,                                                                                   " +
                "                         hmd.TriGiaTT ,                                                                                   " +
                "                         tkmd.TyGiaTinhThue AS TyGiaTT ,                                                                       " +
                "                         tkmd.NguyenTe_ID AS MaDVT                                                                                   " +
                "                FROM     t_KDT_HangMauDich hmd                                                                            " +
                "                         INNER JOIN dbo.t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID                                 " +
                "                WHERE    tkmd.IDHopDong IN ( " + this.HD.ID + " )                                                                  " +
                "                         AND maloaihinh NOT LIKE '%V%'                                                                    " +
                "                         AND LoaiHangHoa = 'N'                                                                            " +
                "                         AND tkmd.TrangThaiXuLy = 1                                                                       " +
                    //"                         AND tkmd.NgayDangKy BETWEEN '" + ngayDauKy + "' AND '" + ngayCuoiKy + "'          " +
                "              ) t ON n.OldHD_ID = t.IDHopDong                                                                             " +
                "                     AND n.Ma = t.MaPhu                                                                                   " +
                "         JOIN t_kdt_gc_hopdong hd ON hd.ID = n.OldHD_ID                                                                   " +
                " WHERE   hd.ID IN ( " + this.HD.ID + " )                                                                                  " +
                " ORDER BY SoTaiKhoan ASC ,                                                                                                " +
                "         HopDong_ID ,                                                                                                     " +
                "         n.Ma ,                                                                                                           " +
                "         NgayDangKy                                                                                                       ";
                #endregion

                #region
                //                string sql = " SELECT  0 AS STT ,														  										   "+
                //"         155 AS SoTaiKhoan ,                                                                                              " +
                //"         tk.OldHD_ID AS SoHopDong,                                                                                              " +
                //"         tk.OldHD_ID AS [HopDong_ID] ,                                                                                     " +
                //"         tk.Ma ,																		  										   "+
                //"         tk.Ten ,																		  										   "+
                //"         tk.DVT ,																		  										   "+
                //"         tk.TongLuongNK ,																  										   "+
                //"         tk.TongLuongCU ,																  										   "+
                //"         tk.TongLuongXK ,																  										   "+
                //"         t.SoToKhai ,																	  										   "+
                //"         case when t.MaLoaiHinh like '%V%' then t.LoaiVanDon else convert(varchar(12),t.SoToKhai) end as SoTKVnacc,																  							   "+
                //"         t.NgayDangKy ,																  										   " +
                //"         t.MaLoaiHinh ,																  										   "+
                //"         t.SoLuong,																											   "+
                //"		 t.LuongXuatTra,																										   "+
                //"		 t.SoLuong as LuongSD_TK,																								   "+
                //"	     t.SoLuong as LuongTon_TK,																								   "+
                //"		 tk.ChenhLech,																											   "+
                //"		 tk.KetLuanXLCL																  											   "+
                //" FROM    t_GC_ThanhKhoanHDGC AS tk														  										   "+
                //"         JOIN ( "+
                ////minhnd
                //" SELECT  "+
                //"                         cu.MaNPL AS MaPhu ,                                          " +
                //" dk.HopDong_ID AS IDHopDong ,                                                " +
                //"                         dk.SoTiepNhan AS SoToKhai ,                                  " +
                //"                         dk.SoTiepNhan AS LoaiVanDon ,                             " +
                //"                         dk.NgayTiepNhan AS NgayDangKy ,                              " +
                //"                         'NLHUY' AS MaLoaiHinh ,                                      " +

                //"                         cu.LuongNPLHuy AS SoLuong,                                   " +
                //" 0 as LuongXuatTra " +
                //"                FROM     t_KDT_GC_NPLHuyDangKy AS dk                                  " +
                //"                         JOIN dbo.t_KDT_GC_NPLHuy AS cu ON dk.ID = cu.Master_ID       " +
                //"                WHERE    dk.HopDong_ID IN ( " + this.HD.ID + " )                          " +
                //" UNION " +
                //" SELECT  "+
                //"                         cu.MaNPL AS MaPhu ,                                                 " +
                //" dk.HopDong_ID AS IDHopDong ,                                                       " +
                //"                         dk.SoTiepNhan AS SoToKhai ,                                         " +
                //"                         dk.SoTiepNhan AS LoaiVanDon ,                                    " +
                //"                         dk.NgayTiepNhan AS NgayDangKy ,                                     " +
                //"                         'NPLCU' AS MaLoaiHinh ,                                             " +

                //"                         cu.LuongCungUng AS SoLuong,                                         " +
                //" 0 as LuongXuatTra " +
                //"                               "+
                //"                FROM     t_KDT_GC_CungUngDangKy AS dk                                        " +
                //"                         JOIN dbo.t_KDT_GC_CungUng AS cu ON dk.ID = cu.Master_ID             " +
                //"                WHERE    dk.HopDong_ID IN ( " + this.HD.ID + " )                                 " +
                //"                   UNION " +
                ////minhnd
                //" SELECT   hmd.MaPhu ,													  										   "+
                //"                         tkmd.IDHopDong ,												  										   "+
                //"                         tkmd.SoToKhai ,												  										   "+
                //"                         tkmd.LoaiVanDon ,												  										   "+
                //"                         tkmd.NgayDangKy ,												  										   "+
                //"                         tkmd.MaLoaiHinh ,												  										   "+
                //"                         hmd.SoLuong,																							   "+
                //"						 0 as LuongXuatTra													  									   "+
                //"                FROM     dbo.t_KDT_ToKhaiMauDich AS tkmd								  										   "+
                //"                         JOIN dbo.t_KDT_HangMauDich AS hmd ON tkmd.ID = hmd.TKMD_ID	  										   "+
                //"                WHERE    tkmd.MaLoaiHinh LIKE '%N%' and tkmd.TrangThaiXuly = 1									  				   "+
                //"                         AND tkmd.IDHopDong = "+this.HD.ID+"																				   "+
                //"				union 																									   "+
                //"				SELECT   hmd.MaHang as MaPhu ,													  								   "+
                //"                         tkmd.IDHopDong ,												  										   "+
                //"                         tkmd.SoToKhai ,												  										   "+
                //"                         tkmd.Huongdan_PL as LoaiVanDon ,												  						   "+
                //"                         tkmd.NgayDangKy ,												  										   "+
                //"                         tkmd.MaLoaiHinh ,												  										   "+
                //"                         0 as SoLuong,																							   "+
                //"						 hmd.SoLuong as LuongXuatTra											  								   "+
                //"                FROM     dbo.t_kdt_GC_Tokhaichuyentiep AS tkmd								  									   "+
                //"                         JOIN dbo.t_KDT_GC_HangChuyenTiep AS hmd ON tkmd.ID = hmd.Master_ID	  								   "+
                //"                WHERE    tkmd.TrangThaiXuly = 1	AND ((LoaiHangHoa = 'N' AND MaLoaiHinh LIKE 'X%') OR (MaLoaiHinh Like 'PHPLX'))"+							  
                //"                         AND tkmd.IDHopDong = "+this.HD.ID+"																				   "+
                //"				union 																									   " +
                //"				SELECT   hmd.MaHang as MaPhu ,													  								   " +
                //"                         tkmd.IDHopDong ,												  										   " +
                //"                         tkmd.SoToKhai ,												  										   " +
                //"                         tkmd.Huongdan_PL as LoaiVanDon ,												  						   " +
                //"                         tkmd.NgayDangKy ,												  										   " +
                //"                         tkmd.MaLoaiHinh ,												  										   " +
                //"                         hmd.SoLuong,																							   " +
                //"						  0 as LuongXuatTra											  								   " +
                //"                FROM     dbo.t_kdt_GC_Tokhaichuyentiep AS tkmd								  									   " +
                //"                         JOIN dbo.t_KDT_GC_HangChuyenTiep AS hmd ON tkmd.ID = hmd.Master_ID	  								   " +
                //"                WHERE    tkmd.TrangThaiXuly = 1	AND ((LoaiHangHoa = 'N') OR (MaLoaiHinh Like 'PHPLX'))" +
                //"                         AND tkmd.IDHopDong = " + this.HD.ID + "																				   " +
                //"				union																											   "+
                //"				SELECT   hmd.MaPhu ,													  										   "+
                //"                         tkmd.IDHopDong ,												  										   "+
                //"                         tkmd.SoToKhai ,												  										   "+
                //"                         tkmd.LoaiVanDon ,												  										   "+
                //"                         tkmd.NgayDangKy ,												  										   "+
                //"                         tkmd.MaLoaiHinh ,												  										   "+
                //"                         0 as SoLuong,																							   "+
                //"						 hmd.SoLuong as LuongXuatTra										  									   "+
                //"                FROM     dbo.t_kdt_ToKhaiMauDich AS tkmd								  										   "+
                //"                         JOIN dbo.t_KDT_HangMauDich AS hmd ON tkmd.ID = hmd.TKMD_ID	  										   "+
                //"                WHERE    tkmd.TrangThaiXuly = 1	AND (LoaiHangHoa = 'N' AND MaLoaiHinh LIKE 'X%')							   "+  
                //"                         AND tkmd.IDHopDong = "+this.HD.ID+"										  										   "+
                //"                UNION " +
                //"				 SELECT  dbo.t_GC_DinhMuc.MaNguyenPhuLieu AS MaPhu," +
                //"						tkmd.IDHopDong," +
                //"						tkmd.SoToKhai," +
                //"						tkmd.LoaiVanDon," +
                //"						tkmd.NgayDangKy," +
                //"						tkmd.MaLoaiHinh," +
                //"						hmd.SoLuong," +
                //"						hmd.SoLuong * ( dbo.t_GC_DinhMuc.DinhMucSuDung/ 100 * dbo.t_GC_DinhMuc.TyLeHaoHut + dbo.t_GC_DinhMuc.DinhMucSuDung ) AS LuongXuatTra " +
                //"				FROM    dbo.t_KDT_ToKhaiMauDich tkmd " +
                //"				INNER JOIN dbo.t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
                //"				INNER JOIN dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = hmd.MaPhu " +
                //"                                          AND dbo.t_GC_DinhMuc.HopDong_ID = tkmd.IDHopDong " +
                //"				INNER JOIN dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID " +
                //"                                                AND dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma " +
                //"              WHERE    tkmd.TrangThaiXuLy = 1 " +
                //"                        AND ( LoaiHangHoa = 'S' AND MaLoaiHinh LIKE 'X%') " +
                //"                        AND tkmd.IDHopDong = "+this.HD.ID+ " "+
                //"              ) AS t ON tk.OldHD_ID = t.IDHopDong										  										   "+
                //"                        AND tk.Ma = t.MaPhu											  										   "+
                //" WHERE   tk.OldHD_ID = "+this.HD.ID+"																  										   "+			  
                //"         AND t.NgayDangKy <= '"+dtpDenNgay.Value.Year+"-"+dtpDenNgay.Value.Month+"-"+dtpDenNgay.Value.Day+"'							   "+
                //"         AND t.NgayDangKy >= '" + dtpTuNgay.Value.Year + "-" + dtpTuNgay.Value.Month + "-" + dtpTuNgay.Value.Day + "'							   " +
                //" ORDER BY tk.Ma ,																		  										   "+
                //"         t.NgayDangKy																	  										   ";
                #endregion
                cmd = db.GetSqlStringCommand(sql);

                cmd.CommandTimeout = 60000;
                ds = db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //throw (ex);
            }
            return ds;

        }

        public DataSet GetDM_SP_BC06_TT38()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();

            //Lấy danh sách SP chưa có định mức
            try
            {
                string proc = "p_GC_GetDinhMucSanPhamForBC06_TT38";
                //DbCommand cmd = db.GetStoredProcCommand(proc);
                SqlCommand cmd = (SqlCommand)db.GetStoredProcCommand(proc);
                db.AddInParameter(cmd, "@IDHopDong", SqlDbType.BigInt, this.HD.ID);
                db.AddInParameter(cmd, "@MaHaiQuan", SqlDbType.Char, GlobalSettings.MA_HAI_QUAN);
                db.AddInParameter(cmd, "@MaDoanhNghiep", SqlDbType.NVarChar, GlobalSettings.MA_DON_VI);
                db.AddInParameter(cmd, "@TuNgay", SqlDbType.DateTime, dtpTuNgay.Value);
                db.AddInParameter(cmd, "@DenNgay", SqlDbType.DateTime, dtpDenNgay.Value);
                cmd.CommandTimeout = 60000;
                ds = db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //throw (ex);
            }
            return ds;

        }
        private void XuatNhapTonForm_Load(object sender, EventArgs e)
        {
            dgList.HideColumnsWhenGrouped = InheritableBoolean.True;
            dtpDenNgay.Value = DateTime.Now;
            dtpTuNgay.Value = HD.NgayKy;


        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                //Tab NPL Nhập
                if (uiTab1.SelectedTab.Name == "uiTabPage1")
                    ExportExcel("XuatNhapTon_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls", dgList);
                else if (uiTab1.SelectedTab.Name == "uiTabPage2")
                    ExportExcel("NPLXuatTheoDM_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls", dgDinhMuc_SanPham);
                else if (uiTab1.SelectedTab.Name == "uiTabPage3")
                    ExportExcel("NPLCanCU_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls", dgNPLCU);
                else if (uiTab1.SelectedTab.Name == "uiTabPage4")
                    ExportExcel("NPLHuy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls", dgNPLHuy);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }

        }

        private void ExportExcel(string fileName, GridEX gridData)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = fileName;
            sfNPL.Filter = "Excel files (*.xls)|*.xls";
            if (sfNPL.ShowDialog(this) == DialogResult.OK)
            {
                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridData;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
        }

        private void btnInBaoCao_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                BangKe06_HSTK_TT38 xnt = new BangKe06_HSTK_TT38();
                xnt.HD = this.HD;
                xnt.dsBK = tb;
                xnt.BindReport();
                xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXuatMau15_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                Mau15_TT38_ChiTiet xnt = new Mau15_TT38_ChiTiet();
                //DataTable temp = tb.DefaultView.ToTable(true, "SoHopDong", "Ma", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");
                backgroundWorker1.RunWorkerAsync();
                Mau15_TT38_ChiTiet.tb_Mau15Chuan = tb;
                xnt.BindReport(tbDM_SP, false);
                xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnXuLyXNT_Mau15_Click(object sender, EventArgs e)
        {
            progressBar1.ResetText();
            progressBar1.Refresh();
            lblPercen.Text = "0 %";
            index = 0;
            lblXuLy.Text = "Đang xử lý...";
            btnXuLyXNT_Mau15.Enabled = false;
            backgroundWorker1.RunWorkerAsync();
            dgList.DataSource = tb;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (lblXuLy.Text == "Đã xử lý")
            {
                //foreach (DataRow dr in tb.Rows)
                //{
                //    if(dr[""])
                //}
            }
            else
            {
                #region Tính Lương tồn
                DataTable temp = tb.DefaultView.ToTable(true, "SoTaiKhoan", "SoHopDong", "Ma", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");

                totalRows = temp.Select("SoTaiKhoan='152'").Length;
                progressBar1.Minimum = 0;
                try
                {
                    progressBar1.Maximum = 100;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

                backgroundWorker1.WorkerReportsProgress = true;
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    //decimal tongNhap = 0;
                    decimal tongSuDung = 0;
                    //decimal tongTon = 0;
                    #region Tính toán
                    foreach (DataRow dr in temp.Select())
                    {
                        //string ma1 = dr["Ma"].ToString();
                        //tongNhap = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                        tongSuDung = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
                        //tongTon = Convert.ToDecimal(dr["LuongTon"].ToString());

                        decimal luongTK = 0;
                        //decimal luongSD = 0;
                        //decimal luongTon = 0;
                        decimal triGiaTK = 0;
                        //decimal triGiaSD = 0;
                        //decimal triGiaTon = 0;
                        decimal donGia = 0;
                        //decimal tyGia = 0;
                        if (dr["Ma"].ToString() == "PL05")
                        {

                        }
                        foreach (DataRow dr1 in tb.Select("SoTaiKhoan='" + dr["SoTaiKhoan"].ToString() + "' and Ma='" + dr["Ma"].ToString() + "' and SoHopDong = '" + dr["SoHopDong"].ToString() + "'", "NgayDangKy"))
                        {

                            if (dr["Ma"].ToString() == "PL05")
                            {

                            }
                            luongTK = Convert.ToDecimal(dr1["SoLuong"].ToString());
                            //luongSD = Convert.ToDecimal(dr1["LuongSD_TK"].ToString());
                            //luongTon = Convert.ToDecimal(dr1["LuongTon_TK"].ToString());
                            triGiaTK = Convert.ToDecimal(dr1["TriGiaTT"].ToString());
                            //try
                            //{
                            //    triGiaSD = Convert.ToDecimal(dr1["TriGiaSuDung"].ToString());
                            //}
                            //catch (Exception ex)
                            //{
                            //}
                            //triGiaTon = Convert.ToDecimal(dr1["TonTriGia"].ToString());
                            donGia = Convert.ToDecimal(dr1["DonGiaTT"].ToString());
                            //try
                            //{
                            //    tyGia = dr1["TyGiaTT"] != DBNull.Value ? Convert.ToDecimal(dr1["TyGiaTT"].ToString()) : 0;
                            //}
                            //catch (Exception ex)
                            //{

                            //    //throw;
                            //}

                            if (dr1["SoTaiKhoan"].ToString() == "152" && dr1["MaLoaiHinh"].ToString().Contains("N"))
                            {
                                #region Tài khoản 152
                                try
                                {
                                    #region Tính lượng vs trị giá
                                    if (tongSuDung >= luongTK)
                                    {
                                        dr1["LuongSD_TK"] = luongTK;
                                        dr1["LuongTon_TK"] = 0;
                                        try
                                        {
                                            dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                            dr1["TriGiaSuDung"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }
                                        dr1["TonTriGia"] = 0;
                                        tongSuDung = tongSuDung - luongTK;
                                    }
                                    else if (tongSuDung > 0 && tongSuDung < luongTK)
                                    {
                                        dr1["LuongSD_TK"] = tongSuDung;
                                        dr1["LuongTon_TK"] = luongTK - tongSuDung;

                                        try
                                        {
                                            dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                            dr1["TriGiaSuDung"] = Math.Round(tongSuDung * donGia, 0).ToString();
                                            dr1["TonTriGia"] = Math.Round(triGiaTK - (tongSuDung * donGia), 0).ToString();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }
                                        tongSuDung = 0;
                                    }
                                    else
                                    {
                                        dr1["LuongSD_TK"] = 0;
                                        dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                        dr1["TriGiaSuDung"] = 0;
                                        dr1["TonTriGia"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                    }

                                    #endregion

                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                                #endregion Tài khoản 152

                            }
                            else if (dr1["SoTaiKhoan"].ToString() == "152" && dr1["MaLoaiHinh"].ToString().Contains("X"))
                            {

                                #region Tài Khoản 152 chuyển tiếp
                                try
                                {
                                    #region Tính lượng vs trị giá

                                    dr1["SoLuong"] = luongTK;
                                    dr1["LuongSD_TK"] = luongTK;
                                    dr1["LuongTon_TK"] = 0;
                                    dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                    dr1["TriGiaSuDung"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                    dr1["TonTriGia"] = 0;
                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                                #endregion Tài Khoản chuyển tiếp
                            }
                        }

                        index++;
                        try
                        {
                            int i = (index * 100) / totalRows;
                            backgroundWorker1.ReportProgress(i);
                        }
                        catch (Exception)
                        {

                        }

                    }
                    #endregion
                }

                #endregion
                //#region Tính Lương tồn
                //DataTable temp = tb.DefaultView.ToTable(true, "SoTaiKhoan", "SoHopDong", "Ma", "SoLuongDaNhap", "SoLuongCungUng", "SoLuongDaDung", "LuongTon");
                ////DataTable temp = tb.DefaultView.ToTable(true, "Ma", "TongLuongNK", "TongLuongCU", "TongLuongXK");

                //totalRows = temp.Rows.Count;
                //progressBar1.Minimum = 0;
                //try
                //{
                //    progressBar1.Maximum = 100;
                //}
                //catch (Exception ex)
                //{

                //    //throw;
                //}

                //backgroundWorker1.WorkerReportsProgress = true;
                //if (backgroundWorker1.CancellationPending)
                //{
                //    e.Cancel = true;
                //}
                //else
                //{
                //    //decimal tongNhap = 0;
                //    decimal tongSuDung = 0;
                //    //decimal tongTon = 0;
                //    foreach (DataRow dr in temp.Select())
                //    {
                //        //string ma1 = dr["Ma"].ToString();
                //        //tongNhap = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                //        tongSuDung = Convert.ToDecimal(dr["TongLuongXK"].ToString());
                //        //tongTon = Convert.ToDecimal(dr["LuongTon"].ToString());

                //        decimal luongTK = 0;


                //        foreach (DataRow dr1 in tb.Select(" Ma='" + dr["Ma"].ToString() + "'", "NgayDangKy"))
                //        {


                //            luongTK = Convert.ToDecimal(dr1["SoLuong"].ToString());




                //                #region xử lý
                //                try
                //                {
                //                    #region Tính lượng vs trị giá

                //                    if (tongSuDung > luongTK)
                //                    {
                //                        dr1["LuongSD_TK"] = luongTK;
                //                        dr1["LuongTon_TK"] = 0;
                //                        //try
                //                        //{
                //                        //    dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                //                        //    dr1["TriGiaSuDung"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                //                        //}
                //                        //catch (Exception ex)
                //                        //{
                //                        //}
                //                        //dr1["TonTriGia"] = 0;
                //                        tongSuDung = tongSuDung - luongTK;
                //                    }
                //                    else if (tongSuDung > 0 && tongSuDung < luongTK)
                //                    {
                //                        dr1["LuongSD_TK"] = tongSuDung;
                //                        dr1["LuongTon_TK"] = luongTK - tongSuDung;

                //                        //try
                //                        //{
                //                        //    dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                //                        //    dr1["TriGiaSuDung"] = Math.Round(tongSuDung * donGia, 0).ToString();
                //                        //    dr1["TonTriGia"] = Math.Round(triGiaTK - (tongSuDung * donGia), 0).ToString();
                //                        //}
                //                        //catch (Exception ex)
                //                        //{

                //                        //}
                //                        tongSuDung = 0;
                //                    }
                //                    else
                //                    {
                //                        dr1["LuongSD_TK"] = 0;
                //                        //dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                //                        dr1["TriGiaSuDung"] = 0;
                //                        //dr1["TonTriGia"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                //                    }

                //                    #endregion

                //                }
                //                catch (Exception ex)
                //                {

                //                    // throw;
                //                }
                //                #endregion xử lý



                //        }

                //        index++;
                //        try
                //        {
                //            int i = (index * 100) / totalRows;
                //            backgroundWorker1.ReportProgress(i);
                //        }
                //        catch (Exception)
                //        {

                //        }

                //    }
                //}

                //#endregion

            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {

                progressBar1.Value = e.ProgressPercentage;
                lblPercen.Text = e.ProgressPercentage.ToString() + " %";
                lblXuLy.Text = "Đang xử lý...(" + index + "/" + totalRows + ")";
            }
            catch (Exception)
            {

                //    throw;
            }

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblXuLy.Text = "Đã xử lý";
            btnXuLyXNT_Mau15.Enabled = false;
            btnLayDuLieu.Enabled = true;
            btnInBaoCao.Enabled = true;


            try
            {
                dgNPLHuy.DataSource = null;
                dgNPLCU.DataSource = BangKe06_HSTK_TT38_Tong.GetLuongCungUngOrHuy(tb, tbDM_SP, HD, true);
                var c = BangKe06_HSTK_TT38_Tong.GetLuongCungUngOrHuy(tb, tbDM_SP, HD, false);
                dgNPLHuy.DataSource = c;//BangKe06_HSTK_TT38_Tong.GetLuongCungUngOrHuy(tb, tbDM_SP, HD, false);
                dgNPLHuy.Refresh();

                dgNPLHuy.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            //btnBCTong.PerformClick();
        }

        private void btnLayDuLieu_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                //lấy dữ liệu tờ khai nhập NPL
                tb = GetXNT().Tables[0];
                dgList.DataSource = tb;

                //lấy dữ liệu tờ khai xuất SP_DM_NPL
                tbDM_SP = GetDM_SP_BC06_TT38().Tables[0];
                dgDinhMuc_SanPham.DataSource = tbDM_SP;


                btnXuLyXNT_Mau15.Enabled = true;
                progressBar1.ResetText();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
                btnLayDuLieu.Enabled = true;
            }

        }

        private void btnBCTong_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                BangKe06_HSTK_TT38_Tong xnt = new BangKe06_HSTK_TT38_Tong();
                xnt.HD = this.HD;
                xnt.dtDM_SP = this.tbDM_SP;
                xnt.dsBK = this.tb;
                xnt.BindReport();
                xnt.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void toolStripXuatExcelNPL_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "XuatNhapTon_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void toolStripXuatExcelDM_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "NPLSuDungTheoDM_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgDinhMuc_SanPham;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void btnTinhNPL_CU_Click(object sender, EventArgs e)
        {
            dgNPLCU.DataSource = BangKe06_HSTK_TT38_Tong.GetLuongCungUngOrHuy(tb, tbDM_SP, HD, true);
            uiTabPage3.Selected = true;
        }

        private void toolStripXuatEXNPL_CU_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "NPLCU_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgNPLCU;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void toolStripXuatEX_NPLHuy_Click(object sender, EventArgs e)
        {
            if (btnXuLyXNT_Mau15.Enabled == true)
            {
                MessageBox.Show("Sử dụng chức năng 'Xử lý XNT' trước khi xuất báo cáo.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "NPLCU_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgNPLCU;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void btnChuyenNPLHuy_Click(object sender, EventArgs e)
        {

            DataTable temp = (DataTable)dgNPLHuy.DataSource;
            DataTable tbChuyenNPLHuy = new DataTable();
            tbChuyenNPLHuy = temp.Clone();
            foreach (GridEXSelectedItem item in dgNPLHuy.SelectedItems)
            {
                int i = item.Position;
                DataRow dr = temp.Rows[i];
                tbChuyenNPLHuy.ImportRow(dr);
            }

            NPLHuyForm f = new NPLHuyForm();
            f.isAuto = true;
            f.tbNPLHuy = tbChuyenNPLHuy;
            f.HD = this.HD;
            f.ShowDialog();

        }

        private void btnChuyenNPLCungUng_Click(object sender, EventArgs e)
        {
            DataTable temp = (DataTable)dgNPLCU.DataSource;
            DataTable tbChuyenNPLCU = new DataTable();
            tbChuyenNPLCU = temp.Clone();

            foreach (GridEXSelectedItem item in dgNPLCU.SelectedItems)
            {
                int i = item.Position;
                DataRow dr = temp.Rows[i];
                tbChuyenNPLCU.ImportRow(dr);
            }

            CungUngForm f = new CungUngForm();
            f.isAuto = true;
            f.tbNPLCU = tbChuyenNPLCU;
            f.HD = this.HD;
            f.ShowDialog();
            //dgNPLHuy.SelectedItems.Clear();
        }

    }
}