﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.Controls
{
    public partial class DonViTinhQuyDoiControl : UserControl
    {
        public DonViTinhQuyDoiControl()
        {
            InitializeComponent();
        }
        public long Master_Id = 0;
        public string Type = "";
        public DonViTinhQuyDoi DVT_QuyDoi;
        public bool IsEnable = false;
        private void chkIsEnable_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                cmbDonViTinh.Enabled = txtTyLeQuyDoi.Enabled = IsEnable = chkIsEnable.Checked;
            }
        }
        private void KhoiTao()
        {
            cmbDonViTinh.DataSource = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            cmbDonViTinh.DisplayMember = "Ten";
            cmbDonViTinh.ValueMember = "ID";
        }
        public void LoadData()
        {
//             if (DVT_QuyDoi == null)
//             {
                if (Master_Id > 0)
                {

                    List<DonViTinhQuyDoi> listdvt = (List<DonViTinhQuyDoi>)DonViTinhQuyDoi.SelectCollectionDynamic(string.Format("Master_ID = {0} AND Type = '{1}'", Master_Id, Type), null);
                    if (listdvt != null && listdvt.Count > 0)
                        DVT_QuyDoi = listdvt[0];
                    else
                        DVT_QuyDoi = null;
                }
           /* }*/
            BindData();
        }
        public void BindData()
        {
            if (DVT_QuyDoi != null)
            {
                cmbDonViTinh.SelectedValue = DVT_QuyDoi.DVT_ID;
                txtTyLeQuyDoi.Value = DVT_QuyDoi.TyLeQuyDoi;
                chkIsEnable.Checked = DVT_QuyDoi.IsUse;
            }
            else
            {
                chkIsEnable.Checked = false;
                txtTyLeQuyDoi.Text = "";
                cmbDonViTinh.Text = "";
            }
        }
        public void Reset()
        {
            chkIsEnable.Checked = false;
            txtTyLeQuyDoi.Value = 0;
        }
        public void DonViTinhQuyDoiControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                chkIsEnable.Checked = false;
                cmbDonViTinh.Enabled = false;
                txtTyLeQuyDoi.Enabled = false;
                KhoiTao();
                LoadData();
            }
        }

        private void cmbDonViTinh_Leave(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                if (DVT_QuyDoi == null)
                    DVT_QuyDoi = new DonViTinhQuyDoi();
                DVT_QuyDoi.TyLeQuyDoi = Convert.ToDouble(txtTyLeQuyDoi.Value);
                DVT_QuyDoi.DVT_ID = cmbDonViTinh.SelectedValue.ToString();
                DVT_QuyDoi.IsUse = chkIsEnable.Checked;
            }
        }
    }
}
