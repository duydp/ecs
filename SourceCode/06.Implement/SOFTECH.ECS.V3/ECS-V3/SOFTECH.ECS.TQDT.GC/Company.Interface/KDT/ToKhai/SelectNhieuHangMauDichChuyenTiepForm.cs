﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
//using Company.GC.BLL.KDT;

using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using System.Data;
using Company.Interface.GC;
using HangChuyenTiep = Company.GC.BLL.KDT.GC.HangChuyenTiep;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;

namespace Company.Interface.KDT.ToKhai
{
    public partial class SelectNhieuHangMauDichChuyenTiepForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------       
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        //public ToKhaiMauDich TKMD; 
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT;
        //public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public List<HangChuyenTiep> HCTCollection = new List<HangChuyenTiep>();
        public decimal TyGiaTT;
        public string NhomLH = "";
        public string LoaiHangHoa = "";
        public bool ismoreGoods;
        //-----------------------------------------------------------------------------------------

        public SelectNhieuHangMauDichChuyenTiepForm()
        {
            InitializeComponent();
        }

        private void khoitao_GiaoDien()
        {
            foreach (DataRow rowNuoc in this._Nuoc.Rows)
            {
                dgList.RootTable.Columns["ID_NuocXX"].ValueList.Add(rowNuoc["ID"].ToString(), rowNuoc["Ten"].ToString());
            }
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            List<HangChuyenTiep> hangChuyenTieps = new List<HangChuyenTiep>();
            if (TKCT.LoaiHangHoa == "N")
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = TKCT.IDHopDong;

                foreach (NguyenPhuLieu item in npl.SelectCollectionBy_HopDong_ID())
                {
                    HangChuyenTiep hmd = new HangChuyenTiep()
                    {
                        MaHang = item.Ma,
                        TenHang = item.Ten,
                        ID_DVT = item.DVT_ID,
                        MaHS = item.MaHS

                    };
                    hangChuyenTieps.Add(hmd);
                }
            }
            else if (TKCT.LoaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKCT.IDHopDong;
                foreach (SanPham item in sp.SelectCollectionBy_HopDong_ID())
                {
                    HangChuyenTiep hmd = new HangChuyenTiep()
                    {
                        MaHang = item.Ma,
                        TenHang = item.Ten,
                        ID_DVT = item.DVT_ID,
                        MaHS = item.MaHS

                    };
                    hangChuyenTieps.Add(hmd);

                }
            }
            else if (TKCT.LoaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = TKCT.IDHopDong;
                foreach (ThietBi item in tb.SelectCollectionBy_HopDong_ID())
                {
                    HangChuyenTiep hmd = new HangChuyenTiep()
                    {
                        MaHang = item.Ma,
                        TenHang = item.Ten,
                        ID_DVT = item.DVT_ID,
                        MaHS = item.MaHS

                    };
                    hangChuyenTieps.Add(hmd);

                }
            }
            else if (TKCT.LoaiHangHoa == "H")
            {
                foreach (HangMau item in HangMau.SelectCollectionDynamic("HopDong_ID=" + TKCT.IDHopDong, ""))
                {
                    HangChuyenTiep hmd = new HangChuyenTiep()
                    {
                        MaHang = item.Ma,
                        TenHang = item.Ten,
                        ID_DVT = item.DVT_ID,
                        MaHS = item.MaHS

                    };
                    hangChuyenTieps.Add(hmd);
                    hangChuyenTieps.Add(hmd);
                }
            }
            dgList.DataSource = hangChuyenTieps;
        }


        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangChuyenTiep hmd in this.HCTCollection)
            {
                if (hmd.MaHang.Trim().ToUpper() == maHang.Trim().ToUpper()) return true;
            }
            return false;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();

            foreach (GridEXRow i in items)
            {
                Company.GC.BLL.KDT.GC.HangChuyenTiep hct = (Company.GC.BLL.KDT.GC.HangChuyenTiep)i.DataRow;
                string sfmt = string.Format("Mã hàng '{0}'\r\nTên hàng '{1}\r\n", hct.MaHang, hct.TenHang);

                //if (hmd.nu.NuocXX_ID == "")
                //{
                //    ShowMessage("Nước xuất xứ của hàng thứ : "+(i.RowIndex+1)+" không hợp lệ.", false);
                //    return;
                //}
                if (hct.SoLuong <= 0)
                {
                    showMsg(sfmt + "Số lượng hàng không hợp lệ");
                    //MLMessages("Số lượng của hàng thứ : " + (i.RowIndex + 1) + " không hợp lệ.","MSG_EXC09",Convert.ToString(i.RowIndex + 1), false);
                    return;
                }
                if (hct.DonGia <= 0)
                {
                    showMsg(sfmt + "Đơn giá không hợp lệ");
                    //MLMessages("Đơn giá nguyên tệ của hàng thứ : " + (i.RowIndex + 1) + " không hợp lệ.", "MSG_EXC09", Convert.ToString(i.RowIndex + 1), false);
                    return;
                }
            }
            this.TKCT.HCTCollection.Clear();
            foreach (HangChuyenTiep HCT in this.HCTCollection)
            {
                this.TKCT.HCTCollection.Add(HCT.SaoChepHCT());
            }
            this.Close();
        }
        private void reset()
        {
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }

        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();


            //decimal tongTGKB = 0;
            ////TH
            //foreach (HangChuyenTiep  hct in this.HCTCollection )
            //{
            //   tongTGKB += hct.TriGia ;
            //}
            //dgList.DataSource = this.HCTCollection;

            //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", tongTGKB.ToString("N"), this.TKCT.NguyenTe_ID);
            //lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");
            if (this.TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                btnAddNew.Visible = false;
        }

        //-----------------------------------------------------------------------------------------------       

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //if (ismoreGoods)
            //{
            //    ismoreGoods = false;
            //    return;
            //}
            //else
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            //HangMauDichEditForm f = new HangMauDichEditForm();
            //            Company.Interface.KDT.GC.HangChuyenTiepEditForm f = new Company.Interface.KDT.GC.HangChuyenTiepEditForm();
            //            f.HCT = (HangChuyenTiep)e.Row.DataRow;
            //            f.HD.ID = this.TKCT.IDHopDong;
            //            f.TKCT = this.TKCT;
            //            // f.TyGiaTT = this.TyGiaTT;
            //            if (this.TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            //                f.OpenType = OpenFormType.Edit;
            //            else
            //                f.OpenType = OpenFormType.View;
            //            f.ShowDialog();
            //            try
            //            {
            //                dgList.Refetch();
            //            }
            //            catch { dgList.Refresh(); }
            //            decimal tongTGKB = 0;
            //            foreach (HangChuyenTiep hct in this.HCTCollection)
            //            {
            //                // Tính lại thuế.
            //                // hct..TinhThue(this.TyGiaTT);
            //                // Tổng trị giá khai báo.
            //                tongTGKB += hct.TriGia;
            //            }
            //            //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", tongTGKB.ToString("N"), this.TKCT.NguyenTe_ID);
            //        }
            //        break;
            //    }
            //}
        }


        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (showMsg("MSG_DEL01", true) == "Yes")
            ////if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            HangChuyenTiep  hmd = (HangChuyenTiep )i.GetRow().DataRow;
            //            if (hmd.ID > 0)
            //            {
            //                hmd.Delete( TKCT.IDHopDong,LoaiHangHoa, TKCT.MaLoaiHinh );
            //            }
            //        }
            //    }               
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {

            if (LoaiHangHoa.Substring(2, 2).Equals("PL"))
            {
                NguyenPhuLieuChuyenTiepRegistedForm f = new NguyenPhuLieuChuyenTiepRegistedForm();
                f.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                f.TKCT = TKCT;
                f.HCTCollection = HCTCollection;
                f.ShowDialog();
            }

            else if (LoaiHangHoa.Substring(2, 2).Equals("SP"))
            {
                SanPhamChuyenTiepRegistedForm f = new SanPhamChuyenTiepRegistedForm();
                f.SanPhamSelected.HopDong_ID = HD.ID;
                f.TKCT = TKCT;
                f.HCTCollection = HCTCollection;
                f.ShowDialog();
            }
            else
            {
                ThietBiChuyenTiepRegistedForm f = new ThietBiChuyenTiepRegistedForm();
                f.ThietBiSelected.HopDong_ID = HD.ID;
                f.TKCT = TKCT;
                f.HCTCollection = HCTCollection;
                f.ShowDialog();
            }

            dgList.DataSource = HCTCollection;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }

        private bool KiemTraLuongHang(string MaHang, decimal SoLuong)
        {
            decimal LuongCon = 0;
            //if (this.TKCT.MaLoaiHinh.Substring(2,2).Equals("PL"))
            if (LoaiHangHoa.Substring(2, 2).Equals("PL"))
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = MaHang;
                if (npl.Load())
                {
                    if (this.TKCT.MaLoaiHinh.EndsWith("X"))
                    {
                        LuongCon = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203028", LuongCon, true) != "Yes")
                                //if (MLMessages("Không được nhập quá số lượng còn được nhập là : " + LuongCon + " .Bạn có muốn tiếp tục không ?","MSG_WRN32",LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }
                    else
                    {
                        LuongCon = npl.SoLuongDaNhap + npl.SoLuongCungUng - npl.SoLuongDaDung;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                                //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }

                }
                else
                {
                }
            }
            else if (LoaiHangHoa.Substring(2, 2).Equals("SP"))
            {
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = MaHang;
                if (sp.Load())
                {
                    LuongCon = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                    if (LuongCon < SoLuong)
                    {
                        if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                            //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                            return false;
                    }
                }
                else
                {
                    ;
                }
            }
            else if (LoaiHangHoa.Substring(2, 2).Equals("TB"))
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = HD.ID;
                tb.Ma = MaHang;
                if (tb.Load())
                {
                    if (this.TKCT.MaLoaiHinh.EndsWith("N"))
                    {
                        LuongCon = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203028", LuongCon, true) != "Yes")
                                //if (MLMessages("Không được nhập quá số lượng còn được nhập là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }
                    else
                    {
                        LuongCon = tb.SoLuongDaNhap;
                        if (LuongCon < SoLuong)
                        {
                            if (showMsg("MSG_0203029", LuongCon, true) != "Yes")
                                //if (MLMessages("Không được xuất quá số lượng còn được xuất là : " + LuongCon + " .Bạn có muốn tiếp tục không ?", "MSG_WRN32", LuongCon.ToString(), true) != "Yes")
                                return false;
                        }
                    }
                }
                else
                {
                    ;
                }
            }
            return true;
        }

        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "DonGiaKB" || e.Column.Key == "SoLuong")
            {
                decimal SoLuong = Convert.ToDecimal(e.Value);
                decimal DonGia = 0;
                if (SoLuong <= 0)
                {
                    showMsg("MSG_WRN33");
                    //MLMessages("Dữ liệu không hợp lệ","MSG_WRN33","", false);
                    e.Cancel = true;
                }
                if (e.Column.Key == "SoLuong")
                {
                    HangChuyenTiep HCT = (HangChuyenTiep)dgList.GetRow().DataRow;
                    if (!KiemTraLuongHang(HCT.MaHang.Trim(), SoLuong))
                        e.Cancel = true;
                }
            }
        }

        private void dgList_RecordUpdated(object sender, EventArgs e)
        {
            HangChuyenTiep HCT = (HangChuyenTiep)dgList.GetRow().DataRow;
            HCT.TriGia = HCT.DonGia * HCT.SoLuong;
            //HMD.TriGiaKB_VND = HMD.TriGiaKB * TKCT.TyGiaTinhThue;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }

    }
}
