﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;


namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    public partial class PKGiahanHDForm : BaseForm
    {        
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
        public HopDong HD = new HopDong();
        public bool boolFlag;
        public PKGiahanHDForm()
        {            
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {            
            this.Close();
        }
   
        private void PKGiahanHDForm_Load(object sender, EventArgs e)
        {
            txtSoHD.Text = HD.SoHopDong;
            ccNgayGiaHanCu.Text = "";
            txtNgayKy.Text = HD.NgayKy.ToString("dd/MM/yyyy");
            if (HD.NgayGiaHan.Year > 1900)
                ccNgayGiaHanCu.Value = HD.NgayGiaHan;
            else
                ccNgayGiaHanCu.Value = HD.NgayHetHan;
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK in pkdk.PKCollection)
            {
                if (LoaiPK.MaPhuKien.Trim() == "201")
                {
                    ccNgayGiaHanMoi.Text = LoaiPK.ThongTinMoi;
                    if (pkdk.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        LoaiPK.LoadCollection();
                    }  
                    break;
                }
            }
            if (pkdk.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET || pkdk.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                btnAdd.Enabled = true;

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = null;
                if (ccNgayGiaHanMoi.Value <= DateTime.Today)
                {
                    showMsg("MSG_240225");
                    //ShowMessage("Ngày gia hạn phải lớn hơn ngày hiện tại", false);
                    return;
                }
                foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK1 in pkdk.PKCollection)
                {
                    if (LoaiPK1.MaPhuKien.Trim() == "201")
                    {
                        LoaiPK = LoaiPK1;
                        break;
                    }
                }
                if (LoaiPK == null)
                {
                    LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "201";
                    LoaiPK.NoiDung = LoaiPhuKien_GetName("201");
                    pkdk.PKCollection.Add(LoaiPK);
                }
                LoaiPK.ThongTinMoi = ccNgayGiaHanMoi.Value.ToString("dd/MM/yyyy");
                LoaiPK.ThongTinCu = ccNgayGiaHanCu.Text;
                LoaiPK.Master_ID = pkdk.ID;
                if(boolFlag == true )
                {
                    LoaiPK.InsertUpdateGiaHanHD(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else
                {
                    LoaiPK.InsertUpdateGiaHanHD(pkdk.HopDong_ID);
                }
                
                //PhuKienGCSendForm.isEdit = true;
                this.Close();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

       
    }
}
