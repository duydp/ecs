﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseNPLSPMapperReadExcel : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_NGUYENPHULIEU NPL;
        public List<T_KHOKETOAN_NGUYENPHULIEU> NPLImportTotal = new List<T_KHOKETOAN_NGUYENPHULIEU>();
        public T_KHOKETOAN_NGUYENPHULIEU_MAP NPLMap;

        public T_KHOKETOAN_SANPHAM SP;
        public List<T_KHOKETOAN_SANPHAM> SPImportTotal = new List<T_KHOKETOAN_SANPHAM>();
        public T_KHOKETOAN_SANPHAM_MAP SPMap;

        public HopDong HD;
        public string Type = "NPL";
        public WareHouseNPLSPMapperReadExcel()
        {
            InitializeComponent();
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            #region Old
            int beginRow = Convert.ToInt32(txtRowIndex.Value) -1;
            if (beginRow < 0)
            {
                return;
            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;

            char MaHHColumn = Convert.ToChar(txtMaHangHoa.Text.Trim());
            int  MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangHoa.Text.Trim());
            int  TenHHCol = ConvertCharToInt(TenHHColumn);

            char DVTColumn = Convert.ToChar(txtDVT.Text.Trim());
            int  DVTCol = ConvertCharToInt(DVTColumn);

            char MaHSColumn = Convert.ToChar(txtMaHS.Text.Trim());
            int  MaHSCol = ConvertCharToInt(MaHSColumn);

            char TenTAColumn = Convert.ToChar(txtTenTiengAnh.Text.Trim());
            int  TenTACol = ConvertCharToInt(TenTAColumn);

            char MaKhoColumn = Convert.ToChar(txtMaKho.Text.Trim());
            int  MaKhoCol = ConvertCharToInt(MaKhoColumn);

            char DonHangColumn = Convert.ToChar(txtDonHang.Text.Trim());
            int  DonHangCol = ConvertCharToInt(DonHangColumn);

            char NguoiBanColumn = Convert.ToChar(txtNguoiBan.Text.Trim());
            int  NguoiBanCol = ConvertCharToInt(NguoiBanColumn);

            char KichThuocColumn = Convert.ToChar(txtKichThuoc.Text.Trim());
            int  KichThuocCol = ConvertCharToInt(KichThuocColumn);

            char MauSacColumn = Convert.ToChar(txtMauSac.Text.Trim());
            int  MauSacCol = ConvertCharToInt(MauSacColumn);

            char TaiKhoanColumn = Convert.ToChar(txtTaiKhoan.Text.Trim());
            int  TaiKhoanCol = ConvertCharToInt(TaiKhoanColumn);

            char GhiChuColumn = Convert.ToChar(txtGhiChu.Text.Trim());
            int  GhiChuCol = ConvertCharToInt(GhiChuColumn);

            char MaHHMapColumn = Convert.ToChar(txtMaHangMap.Text.Trim());
            int  MaHHMapCol = ConvertCharToInt(MaHHMapColumn);

            char TenHHMapColumn = Convert.ToChar(txtTenHangMap.Text.Trim());
            int  TenHHMapCol = ConvertCharToInt(TenHHMapColumn);

            char DVTMapColumn = Convert.ToChar(txtDVTMap.Text.Trim());
            int  DVTMapCol = ConvertCharToInt(DVTMapColumn);

            char MaHSMapColumn = Convert.ToChar(txtMaHSMap.Text.Trim());
            int MaHSMapCol = ConvertCharToInt(MaHSMapColumn);

            char TLQDColumn = Convert.ToChar(txtTLQD.Text.Trim());

            int TLQDCol = ConvertCharToInt(TLQDColumn);
            string errorTotal = "";
            string errorMANPL = "";
            string errorMANPLExits = "";
            string errorTENNPL = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorMAHS = "";
            string errorMAKHO = "";
            string errorTAIKHOAN = "";
            string errorMAKHOExits = "";
            string errorTAIKHOANExits = "";
            string errorMANPLMAP = "";
            string errorMANPLMAPExits = "";
            string errorTENNPLMAP = "";
            string errorDVTMAP = "";
            string errorTLQD = "";
            List<NguyenPhuLieu> NPLHDCollection = NguyenPhuLieu.SelectCollectionDynamic("HopDong_ID =" + HD.ID, "");
            List<T_KHOKETOAN_NGUYENPHULIEU> NPLCollection = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID, "");
            List<KDT_GC_SanPham> SPHDCollection = KDT_GC_SanPham.SelectCollectionDynamic("HopDong_ID =" + HD.ID, "");
            List<T_KHOKETOAN_SANPHAM> SPCollection = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID, "");
            List<T_KHOKETOAN_TAIKHOAN> TKCollection = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            List<T_KHOKETOAN_DANHSACHKHO> KhoCollection = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            List<HaiQuan_DonViTinh> DVTCollection = HaiQuan_DonViTinh.SelectCollectionAll();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        #region NPL
                        if (Type=="NPL")
                        {
                            string MANPLBefore = "";
                            string MANPLAfter = "";
                            MANPLAfter = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                            if (wsr.Index == beginRow)
                            {
                                //Dòng đầu tiên
                                MANPLBefore = MANPLAfter;
                                NPL = new T_KHOKETOAN_NGUYENPHULIEU();
                                NPL.HOPDONG_ID = HD.ID;
                            }
                            if (MANPLAfter != MANPLBefore)
                            {
                                NPLImportTotal.Add(NPL);
                                NPL = new T_KHOKETOAN_NGUYENPHULIEU();
                                NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                                NPL.HOPDONG_ID = HD.ID;
                                NPL.MANPL = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (NPL.MANPL.ToString().Length == 0)
                                {
                                    errorMANPL += "[" + (wsr.Index + 1) + "]-[" + NPL.MANPL + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ HÀNG HÓA NÀY ĐÃ MAP HAY CHƯA
                                    foreach (T_KHOKETOAN_NGUYENPHULIEU item in NPLCollection)
                                    {
                                        if (item.MANPL == NPL.MANPL)
                                        {
                                            errorMANPLExits += "[" + (wsr.Index + 1) + "]-[" + NPL.MANPL + "],";
                                            isAdd = false;
                                        }
                                    }
                                }
                                NPL.TENNPL = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (NPL.TENNPL.ToString().Trim().Length == 0)
                                {
                                    errorTENNPL += "[" + (wsr.Index + 1) + "]-[" + NPL.TENNPL + "],";
                                    isAdd = false;
                                }
                                NPL.MAHS = Convert.ToString(wsr.Cells[MaHSCol].Value).Trim();
                                if (NPL.MAHS.ToString().Trim().Length == 0)
                                {
                                    errorMAHS += "[" + (wsr.Index + 1) + "]-[" + NPL.MAHS + "],";
                                    isAdd = false;
                                }
                                NPL.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                                bool isExits = false;
                                if (NPL.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + NPL.DVT + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT                            
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == NPL.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + NPL.DVT + "],";
                                        isAdd = false;
                                    }
                                }
                                isExits = false;
                                NPL.TENTIENGANH = Convert.ToString(wsr.Cells[TenTACol].Value).Trim();
                                NPL.GHICHU = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();

                                NPL.MAKHO = Convert.ToString(wsr.Cells[MaKhoCol].Value).Trim();
                                if (NPL.MAKHO.ToString().Trim().Length == 0)
                                {
                                    errorMAKHO += "[" + (wsr.Index + 1) + "]-[" + NPL.MAKHO + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ KHO                            
                                    foreach (T_KHOKETOAN_DANHSACHKHO item in KhoCollection)
                                    {
                                        if (item.MAKHO == NPL.MAKHO)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMAKHOExits += "[" + (wsr.Index + 1) + "]-[" + NPL.MAKHO + "],";
                                        isAdd = false;
                                    }
                                }
                                isExits = false;
                                NPL.TAIKHOAN = Convert.ToString(wsr.Cells[TaiKhoanCol].Value).Trim();
                                if (NPL.TAIKHOAN.ToString().Trim().Length == 0)
                                {
                                    errorTAIKHOAN += "[" + (wsr.Index + 1) + "]-[" + NPL.TAIKHOAN + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    // KIỂM TRA TÀI KHOẢN
                                    foreach (T_KHOKETOAN_TAIKHOAN item in TKCollection)
                                    {
                                        if (item.MATK == NPL.TAIKHOAN)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorTAIKHOANExits += "[" + (wsr.Index + 1) + "]-[" + NPL.TAIKHOAN + "],";
                                        isAdd = false;
                                    }
                                }
                                NPL.DONHANG = Convert.ToString(wsr.Cells[DonHangCol].Value).Trim();
                                NPL.NGUOIBAN = Convert.ToString(wsr.Cells[NguoiBanCol].Value).Trim();
                                NPL.KICHTHUOC = Convert.ToString(wsr.Cells[KichThuocCol].Value).Trim();
                                NPL.MAUSAC = Convert.ToString(wsr.Cells[MauSacCol].Value).Trim();

                                NPLMap.MANPL = Convert.ToString(wsr.Cells[MaHHMapCol].Value).Trim();
                                if (NPLMap.MANPL.ToString().Length == 0)
                                {
                                    errorMANPLMAP += "[" + (wsr.Index + 1) + "]-[" + NPLMap.MANPL + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ NPL ĐÃ ĐƯỢC ĐĂNG KÝ TRONG HỢP ĐỒNG
                                    foreach (NguyenPhuLieu item in NPLHDCollection)
                                    {
                                        if (item.Ma == NPLMap.MANPL)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMANPLMAPExits += "[" + (wsr.Index + 1) + "]-[" + NPLMap.MANPL + "],";
                                        isAdd = false;
                                    }
                                    isExits = false;
                                }
                                NPLMap.TENNPL = Convert.ToString(wsr.Cells[TenHHMapCol].Value).Trim();
                                if (NPLMap.TENNPL.ToString().Trim().Length == 0)
                                {
                                    errorTENNPLMAP += "[" + (wsr.Index + 1) + "]-[" + NPLMap.TENNPL + "],";
                                    isAdd = false;
                                }
                                NPLMap.DVT = Convert.ToString(wsr.Cells[DVTMapCol].Value).Trim();
                                if (NPLMap.DVT.Trim().Length == 0)
                                {
                                    errorDVTMAP += "[" + (wsr.Index + 1) + "]-[" + NPLMap.DVT + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == NPLMap.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + NPLMap.DVT + "],";
                                        isAdd = false;
                                    }
                                    isExits = false;
                                }
                                string TLQD = Convert.ToString(wsr.Cells[TLQDCol].Value).Trim();
                                if (TLQD.Trim().Length == 0)
                                {
                                    errorTLQD += "[" + (wsr.Index + 1) + "]-[" + NPLMap.TYLEQD + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    NPLMap.TYLEQD = Convert.ToDecimal(TLQD);
                                }
                                NPLMap.MANPLMAP = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (NPLMap.MANPL.ToString().Length == 0)
                                {
                                    isAdd = false;
                                }
                                NPLMap.TENNPLMAP = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (NPLMap.TENNPL.ToString().Trim().Length == 0)
                                {
                                    isAdd = false;
                                }
                                NPLMap.DVTMAP = Convert.ToString(wsr.Cells[DVTCol].Value).Trim().Trim();
                                if (NPLMap.DVT.Trim().Length == 0)
                                {
                                    isAdd = false;
                                }
                                NPL.NPLMapCollection.Add(NPLMap);
                                MANPLBefore = MANPLAfter;
                            }
                            else
                            {
                                NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                                NPL.MANPL = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (NPL.MANPL.ToString().Length == 0)
                                {
                                    errorMANPL += "[" + (wsr.Index + 1) + "]-[" + NPL.MANPL + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ HÀNG HÓA NÀY ĐÃ MAP HAY CHƯA
                                    foreach (T_KHOKETOAN_NGUYENPHULIEU item in NPLCollection)
                                    {
                                        if (item.MANPL == NPL.MANPL)
                                        {
                                            errorMANPLExits += "[" + (wsr.Index + 1) + "]-[" + NPL.MANPL + "],";
                                            isAdd = false;
                                        }
                                    }
                                }
                                NPL.TENNPL = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (NPL.TENNPL.ToString().Trim().Length == 0)
                                {
                                    errorTENNPL += "[" + (wsr.Index + 1) + "]-[" + NPL.TENNPL + "],";
                                    isAdd = false;
                                }
                                NPL.MAHS = Convert.ToString(wsr.Cells[MaHSCol].Value).Trim();
                                if (NPL.MAHS.ToString().Trim().Length == 0)
                                {
                                    errorMAHS += "[" + (wsr.Index + 1) + "]-[" + NPL.MAHS + "],";
                                    isAdd = false;
                                }
                                NPL.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                                bool isExits = false;
                                if (NPL.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + NPL.DVT + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT                            
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == NPL.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + NPL.DVT + "],";
                                        isAdd = false;
                                    }
                                }
                                isExits = false;
                                NPL.TENTIENGANH = Convert.ToString(wsr.Cells[TenTACol].Value).Trim();
                                NPL.GHICHU = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();

                                NPL.MAKHO = Convert.ToString(wsr.Cells[MaKhoCol].Value).Trim();
                                if (NPL.MAKHO.ToString().Trim().Length == 0)
                                {
                                    errorMAKHO += "[" + (wsr.Index + 1) + "]-[" + NPL.MAKHO + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ KHO                            
                                    foreach (T_KHOKETOAN_DANHSACHKHO item in KhoCollection)
                                    {
                                        if (item.MAKHO == NPL.MAKHO)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMAKHOExits += "[" + (wsr.Index + 1) + "]-[" + NPL.MAKHO + "],";
                                        isAdd = false;
                                    }
                                }
                                isExits = false;
                                NPL.TAIKHOAN = Convert.ToString(wsr.Cells[TaiKhoanCol].Value).Trim();
                                if (NPL.TAIKHOAN.ToString().Trim().Length == 0)
                                {
                                    errorTAIKHOAN += "[" + (wsr.Index + 1) + "]-[" + NPL.TAIKHOAN + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    // KIỂM TRA TÀI KHOẢN
                                    foreach (T_KHOKETOAN_TAIKHOAN item in TKCollection)
                                    {
                                        if (item.MATK == NPL.TAIKHOAN)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorTAIKHOANExits += "[" + (wsr.Index + 1) + "]-[" + NPL.TAIKHOAN + "],";
                                        isAdd = false;
                                    }
                                }
                                NPL.DONHANG = Convert.ToString(wsr.Cells[DonHangCol].Value).Trim();
                                NPL.NGUOIBAN = Convert.ToString(wsr.Cells[NguoiBanCol].Value).Trim();
                                NPL.KICHTHUOC = Convert.ToString(wsr.Cells[KichThuocCol].Value).Trim();
                                NPL.MAUSAC = Convert.ToString(wsr.Cells[MauSacCol].Value).Trim();

                                NPLMap.MANPL = Convert.ToString(wsr.Cells[MaHHMapCol].Value).Trim();
                                if (NPLMap.MANPL.ToString().Length == 0)
                                {
                                    errorMANPLMAP += "[" + (wsr.Index + 1) + "]-[" + NPLMap.MANPL + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ NPL ĐÃ ĐƯỢC ĐĂNG KÝ TRONG HỢP ĐỒNG
                                    foreach (NguyenPhuLieu item in NPLHDCollection)
                                    {
                                        if (item.Ma == NPLMap.MANPL)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMANPLMAPExits += "[" + (wsr.Index + 1) + "]-[" + NPLMap.MANPL + "],";
                                        isAdd = false;
                                    }
                                    isExits = false;
                                }
                                NPLMap.TENNPL = Convert.ToString(wsr.Cells[TenHHMapCol].Value).Trim();
                                if (NPLMap.TENNPL.ToString().Trim().Length == 0)
                                {
                                    errorTENNPLMAP += "[" + (wsr.Index + 1) + "]-[" + NPLMap.TENNPL + "],";
                                    isAdd = false;
                                }
                                NPLMap.DVT = Convert.ToString(wsr.Cells[DVTMapCol].Value).Trim();
                                if (NPLMap.DVT.Trim().Length == 0)
                                {
                                    errorDVTMAP += "[" + (wsr.Index + 1) + "]-[" + NPLMap.DVT + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == NPLMap.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + NPLMap.DVT + "],";
                                        isAdd = false;
                                    }
                                    isExits = false;
                                }
                                string TLQD = Convert.ToString(wsr.Cells[TLQDCol].Value).Trim();
                                if (TLQD.Trim().Length == 0)
                                {
                                    errorTLQD += "[" + (wsr.Index + 1) + "]-[" + NPLMap.TYLEQD + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    NPLMap.TYLEQD = Convert.ToDecimal(TLQD);
                                }
                                NPLMap.MANPLMAP = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (NPLMap.MANPL.ToString().Length == 0)
                                {
                                    isAdd = false;
                                }
                                NPLMap.TENNPLMAP = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (NPLMap.TENNPL.ToString().Trim().Length == 0)
                                {
                                    isAdd = false;
                                }
                                NPLMap.DVTMAP = Convert.ToString(wsr.Cells[DVTCol].Value).Trim().Trim();
                                if (NPLMap.DVT.Trim().Length == 0)
                                {
                                    isAdd = false;
                                }
                                if(isAdd)
                                    NPL.NPLMapCollection.Add(NPLMap);
                                MANPLBefore = MANPLAfter;
                            }
                        }
                        #endregion 
                        #region SP
                        else
                        {
                            // SẢN PHẨM
                            string MASPBefore = "";
                            string MASPAfter = "";
                            MASPAfter = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                            if (wsr.Index == beginRow)
                            {
                                //Dòng đầu tiên
                                MASPBefore = MASPAfter;
                                SP = new T_KHOKETOAN_SANPHAM();
                                SP.HOPDONG_ID = HD.ID;
                            }
                            if (MASPAfter != MASPBefore)
                            {
                                SPImportTotal.Add(SP);
                                SP = new T_KHOKETOAN_SANPHAM();
                                SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                                SP.HOPDONG_ID = HD.ID;
                                SP.MASP = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (SP.MASP.ToString().Length == 0)
                                {
                                    errorMANPL += "[" + (wsr.Index + 1) + "]-[" + SP.MASP + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ HÀNG HÓA NÀY ĐÃ MAP HAY CHƯA
                                    foreach (T_KHOKETOAN_SANPHAM item in SPCollection)
                                    {
                                        if (item.MASP == SP.MASP)
                                        {
                                            errorMANPLExits += "[" + (wsr.Index + 1) + "]-[" + SP.MASP + "],";
                                            isAdd = false;
                                        }
                                    }
                                }
                                SP.TENSP = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (SP.TENSP.ToString().Trim().Length == 0)
                                {
                                    errorTENNPL += "[" + (wsr.Index + 1) + "]-[" + SP.TENSP + "],";
                                    isAdd = false;
                                }
                                SP.MAHS = Convert.ToString(wsr.Cells[MaHSCol].Value).Trim();
                                if (SP.MAHS.ToString().Trim().Length == 0)
                                {
                                    errorMAHS += "[" + (wsr.Index + 1) + "]-[" + SP.MAHS + "],";
                                    isAdd = false;
                                }
                                SP.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                                bool isExits = false;
                                if (SP.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + SP.DVT + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT                            
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == SP.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + SP.DVT + "],";
                                        isAdd = false;
                                    }
                                }
                                isExits = false;
                                SP.TENTIENGANH = Convert.ToString(wsr.Cells[TenTACol].Value).Trim();
                                SP.GHICHU = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();

                                SP.MAKHO = Convert.ToString(wsr.Cells[MaKhoCol].Value).Trim();
                                if (SP.MAKHO.ToString().Trim().Length == 0)
                                {
                                    errorMAKHO += "[" + (wsr.Index + 1) + "]-[" + SP.MAKHO + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ KHO                            
                                    foreach (T_KHOKETOAN_DANHSACHKHO item in KhoCollection)
                                    {
                                        if (item.MAKHO == SP.MAKHO)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMAKHOExits += "[" + (wsr.Index + 1) + "]-[" + SP.MAKHO + "],";
                                        isAdd = false;
                                    }
                                }
                                isExits = false;
                                SP.TAIKHOAN = Convert.ToString(wsr.Cells[TaiKhoanCol].Value).Trim();
                                if (SP.TAIKHOAN.ToString().Trim().Length == 0)
                                {
                                    errorTAIKHOAN += "[" + (wsr.Index + 1) + "]-[" + SP.TAIKHOAN + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    // KIỂM TRA TÀI KHOẢN
                                    foreach (T_KHOKETOAN_TAIKHOAN item in TKCollection)
                                    {
                                        if (item.MATK == SP.TAIKHOAN)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorTAIKHOANExits += "[" + (wsr.Index + 1) + "]-[" + SP.TAIKHOAN + "],";
                                        isAdd = false;
                                    }
                                }
                                SP.DONHANG = Convert.ToString(wsr.Cells[DonHangCol].Value).Trim();
                                SP.NGUOIBAN = Convert.ToString(wsr.Cells[NguoiBanCol].Value).Trim();
                                SP.KICHTHUOC = Convert.ToString(wsr.Cells[KichThuocCol].Value).Trim();
                                SP.MAUSAC = Convert.ToString(wsr.Cells[MauSacCol].Value).Trim();

                                SPMap.MASP = Convert.ToString(wsr.Cells[MaHHMapCol].Value).Trim();
                                if (SPMap.MASP.ToString().Length == 0)
                                {
                                    errorMANPLMAP += "[" + (wsr.Index + 1) + "]-[" + SPMap.MASP + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ SP ĐÃ ĐƯỢC ĐĂNG KÝ TRONG HỢP ĐỒNG
                                    foreach (KDT_GC_SanPham item in SPHDCollection)
                                    {
                                        if (item.Ma == SPMap.MASP)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMANPLMAPExits += "[" + (wsr.Index + 1) + "]-[" + SPMap.MASP + "],";
                                        isAdd = false;
                                    }
                                    isExits = false;
                                }
                                SPMap.TENSP = Convert.ToString(wsr.Cells[TenHHMapCol].Value).Trim();
                                if (SPMap.TENSP.ToString().Trim().Length == 0)
                                {
                                    errorTENNPLMAP += "[" + (wsr.Index + 1) + "]-[" + SPMap.TENSP + "],";
                                    isAdd = false;
                                }
                                SPMap.DVT = Convert.ToString(wsr.Cells[DVTMapCol].Value).Trim();
                                if (SPMap.DVT.Trim().Length == 0)
                                {
                                    errorDVTMAP += "[" + (wsr.Index + 1) + "]-[" + SPMap.DVT + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == SPMap.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + SPMap.DVT + "],";
                                        isAdd = false;
                                    }
                                    isExits = false;
                                }
                                string TLQD = Convert.ToString(wsr.Cells[TLQDCol].Value).Trim();
                                if (TLQD.Trim().Length == 0)
                                {
                                    errorTLQD += "[" + (wsr.Index + 1) + "]-[" + SPMap.TYLEQD + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    SPMap.TYLEQD = Convert.ToDecimal(TLQD);
                                }
                                SPMap.MASPMAP = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (SPMap.MASPMAP.ToString().Length == 0)
                                {
                                    isAdd = false;
                                }
                                SPMap.TENSPMAP = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (SPMap.TENSPMAP.ToString().Trim().Length == 0)
                                {
                                    isAdd = false;
                                }
                                SPMap.DVTMAP = Convert.ToString(wsr.Cells[DVTCol].Value).Trim().Trim();
                                if (SPMap.DVTMAP.Trim().Length == 0)
                                {
                                    isAdd = false;
                                }
                                if(isAdd)
                                    SP.SPCollection.Add(SPMap);
                                MASPBefore = MASPAfter;
                            }
                            else
                            {
                                SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                                SP.HOPDONG_ID = HD.ID;
                                SP.MASP = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (SP.MASP.ToString().Length == 0)
                                {
                                    errorMANPL += "[" + (wsr.Index + 1) + "]-[" + SP.MASP + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ HÀNG HÓA NÀY ĐÃ MAP HAY CHƯA
                                    foreach (T_KHOKETOAN_SANPHAM item in SPCollection)
                                    {
                                        if (item.MASP == SP.MASP)
                                        {
                                            errorMANPLExits += "[" + (wsr.Index + 1) + "]-[" + SP.MASP + "],";
                                            isAdd = false;
                                        }
                                    }
                                }
                                SP.TENSP = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (SP.TENSP.ToString().Trim().Length == 0)
                                {
                                    errorTENNPL += "[" + (wsr.Index + 1) + "]-[" + SP.TENSP + "],";
                                    isAdd = false;
                                }
                                SP.MAHS = Convert.ToString(wsr.Cells[MaHSCol].Value).Trim();
                                if (SP.MAHS.ToString().Trim().Length == 0)
                                {
                                    errorMAHS += "[" + (wsr.Index + 1) + "]-[" + SP.MAHS + "],";
                                    isAdd = false;
                                }
                                SP.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                                bool isExits = false;
                                if (SP.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + SP.DVT + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT                            
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == SP.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + SP.DVT + "],";
                                        isAdd = false;
                                    }
                                }
                                isExits = false;
                                SP.TENTIENGANH = Convert.ToString(wsr.Cells[TenTACol].Value).Trim();
                                SP.GHICHU = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();

                                SP.MAKHO = Convert.ToString(wsr.Cells[MaKhoCol].Value).Trim();
                                if (SP.MAKHO.ToString().Trim().Length == 0)
                                {
                                    errorMAKHO += "[" + (wsr.Index + 1) + "]-[" + SP.MAKHO + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ KHO                            
                                    foreach (T_KHOKETOAN_DANHSACHKHO item in KhoCollection)
                                    {
                                        if (item.MAKHO == SP.MAKHO)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMAKHOExits += "[" + (wsr.Index + 1) + "]-[" + SP.MAKHO + "],";
                                        isAdd = false;
                                    }
                                }
                                isExits = false;
                                SP.TAIKHOAN = Convert.ToString(wsr.Cells[TaiKhoanCol].Value).Trim();
                                if (SP.TAIKHOAN.ToString().Trim().Length == 0)
                                {
                                    errorTAIKHOAN += "[" + (wsr.Index + 1) + "]-[" + SP.TAIKHOAN + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    // KIỂM TRA TÀI KHOẢN
                                    foreach (T_KHOKETOAN_TAIKHOAN item in TKCollection)
                                    {
                                        if (item.MATK == SP.TAIKHOAN)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorTAIKHOANExits += "[" + (wsr.Index + 1) + "]-[" + SP.TAIKHOAN + "],";
                                        isAdd = false;
                                    }
                                }
                                SP.DONHANG = Convert.ToString(wsr.Cells[DonHangCol].Value).Trim();
                                SP.NGUOIBAN = Convert.ToString(wsr.Cells[NguoiBanCol].Value).Trim();
                                SP.KICHTHUOC = Convert.ToString(wsr.Cells[KichThuocCol].Value).Trim();
                                SP.MAUSAC = Convert.ToString(wsr.Cells[MauSacCol].Value).Trim();

                                SPMap.MASP = Convert.ToString(wsr.Cells[MaHHMapCol].Value).Trim();
                                if (SPMap.MASP.ToString().Length == 0)
                                {
                                    errorMANPLMAP += "[" + (wsr.Index + 1) + "]-[" + SPMap.MASP + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA MÃ SP ĐÃ ĐƯỢC ĐĂNG KÝ TRONG HỢP ĐỒNG
                                    foreach (KDT_GC_SanPham item in SPHDCollection)
                                    {
                                        if (item.Ma == SPMap.MASP)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMANPLMAPExits += "[" + (wsr.Index + 1) + "]-[" + SPMap.MASP + "],";
                                        isAdd = false;
                                    }
                                    isExits = false;
                                }
                                SPMap.TENSP = Convert.ToString(wsr.Cells[TenHHMapCol].Value).Trim();
                                if (SPMap.TENSP.ToString().Trim().Length == 0)
                                {
                                    errorTENNPLMAP += "[" + (wsr.Index + 1) + "]-[" + SPMap.TENSP + "],";
                                    isAdd = false;
                                }
                                SPMap.DVT = Convert.ToString(wsr.Cells[DVTMapCol].Value).Trim();
                                if (SPMap.DVT.Trim().Length == 0)
                                {
                                    errorDVTMAP += "[" + (wsr.Index + 1) + "]-[" + SPMap.DVT + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT
                                    foreach (HaiQuan_DonViTinh item in DVTCollection)
                                    {
                                        if (item.Ten == SPMap.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + SPMap.DVT + "],";
                                        isAdd = false;
                                    }
                                    isExits = false;
                                }
                                string TLQD = Convert.ToString(wsr.Cells[TLQDCol].Value).Trim();
                                if (TLQD.Trim().Length == 0)
                                {
                                    errorTLQD += "[" + (wsr.Index + 1) + "]-[" + SPMap.TYLEQD + "],";
                                    isAdd = false;
                                }
                                else
                                {
                                    SPMap.TYLEQD = Convert.ToDecimal(TLQD);
                                }
                                SPMap.MASPMAP = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                if (SPMap.MASPMAP.ToString().Length == 0)
                                {
                                    isAdd = false;
                                }
                                SPMap.TENSPMAP = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (SPMap.TENSPMAP.ToString().Trim().Length == 0)
                                {
                                    isAdd = false;
                                }
                                SPMap.DVTMAP = Convert.ToString(wsr.Cells[DVTCol].Value).Trim().Trim();
                                if (SPMap.DVTMAP.Trim().Length == 0)
                                {
                                    isAdd = false;
                                }
                                if (isAdd)
                                    SP.SPCollection.Add(SPMap);
                                MASPBefore = MASPAfter;
                            }
                        #endregion
                        }                                             
                    }
                    catch (Exception ex)
                    {
                       Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMANPLExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : " + errorMANPLExits + "đã được đăng ký";
            if (!String.IsNullOrEmpty(errorMANPL))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : " + errorMANPL + "không được để trống";
            if (!String.IsNullOrEmpty(errorTENNPL))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : " + errorTENNPL + "không được để trống";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : " + errorDVT + "không được để trống";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : " + errorDVTExits + "không hợp lệ";
            if (!String.IsNullOrEmpty(errorMAHS))
                errorTotal += "\n - [STT] -[MÃ HS] : " + errorMAHS + "không được để trống";
            if (!String.IsNullOrEmpty(errorMAKHOExits))
                errorTotal += "\n - [STT] -[MÃ KHO] : " + errorMAKHOExits + "chưa được đăng ký";
            if (!String.IsNullOrEmpty(errorTAIKHOAN))
                errorTotal += "\n - [STT] -[MÃ KHO] : " + errorMAKHO + "không được để trống";
            if (!String.IsNullOrEmpty(errorTAIKHOANExits))
                errorTotal += "\n - [STT] -[TÀI KHOẢN] : " + errorTAIKHOANExits + "chưa được đăng ký";
            if (!String.IsNullOrEmpty(errorTAIKHOAN))
                errorTotal += "\n - [STT] -[TÀI KHOẢN] : " + errorTAIKHOAN + "không được để trống";
            if (!String.IsNullOrEmpty(errorMANPLMAP))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA VNACCS] : " + errorMANPLMAP + "không được để trống";
            if (!String.IsNullOrEmpty(errorMANPLMAPExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA VNACCS] : " + errorMANPLMAPExits + "chưa được đăng ký";
            if (!String.IsNullOrEmpty(errorTENNPLMAP))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA VNACCS] : " + errorTENNPLMAP + "không được để trống";
            if (!String.IsNullOrEmpty(errorDVTMAP))
                errorTotal += "\n - [STT] -[ĐVT HÀNG HÓA VNACCS] : " + errorDVTMAP + "không được để trống";
            if (!String.IsNullOrEmpty(errorTLQD))
                errorTotal += "\n - [STT] -[TỶ LỆ QUY ĐỔI] : " + errorDVTMAP + "không được để trống";
            if (!String.IsNullOrEmpty(errorTotal))
            {
                ShowMessageTQDT("Nhập từ Excel không thành công ." + errorTotal, false);
                return;
            }
            else
            {
                ShowMessageTQDT("Nhập từ Excel thành công .", false);
            }
            this.Close();
            #endregion
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkFileExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GoodItem();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
    }
}
