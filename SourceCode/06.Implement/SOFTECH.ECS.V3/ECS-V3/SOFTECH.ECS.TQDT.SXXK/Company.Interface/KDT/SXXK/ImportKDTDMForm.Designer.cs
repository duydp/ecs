namespace Company.Interface.KDT.SXXK
{
    partial class ImportKDTDMForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportKDTDMForm));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkExcelMau = new System.Windows.Forms.LinkLabel();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtCungUngValue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTLHHColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtCotCungUng = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDMSDColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPLColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaSPColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkTinhGop = new Janus.Windows.EditControls.UICheckBox();
            this.chIsFromVietnam = new Janus.Windows.EditControls.UICheckBox();
            this.btnMorong = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.chkOverwrite = new Janus.Windows.EditControls.UICheckBox();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSP = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(556, 227);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 97 - 2003 files (*.xls)|*.xls";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(198, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mã SP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(284, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Mã NPL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(370, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "ĐMSD ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(450, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "TLHH";
            // 
            // txtRow
            // 
            this.txtRow.Location = new System.Drawing.Point(119, 39);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(74, 21);
            this.txtRow.TabIndex = 2;
            this.txtRow.Text = "2";
            this.txtRow.Value = 2;
            this.txtRow.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRow.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Sheet";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(116, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Dòng đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.linkExcelMau);
            this.uiGroupBox1.Controls.Add(this.txtSheet);
            this.uiGroupBox1.Controls.Add(this.txtCungUngValue);
            this.uiGroupBox1.Controls.Add(this.txtTLHHColumn);
            this.uiGroupBox1.Controls.Add(this.txtCotCungUng);
            this.uiGroupBox1.Controls.Add(this.txtDMSDColumn);
            this.uiGroupBox1.Controls.Add(this.txtMaNPLColumn);
            this.uiGroupBox1.Controls.Add(this.txtMaSPColumn);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtRow);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(529, 119);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // linkExcelMau
            // 
            this.linkExcelMau.AutoSize = true;
            this.linkExcelMau.Location = new System.Drawing.Point(419, 99);
            this.linkExcelMau.Name = "linkExcelMau";
            this.linkExcelMau.Size = new System.Drawing.Size(105, 13);
            this.linkExcelMau.TabIndex = 28;
            this.linkExcelMau.TabStop = true;
            this.linkExcelMau.Text = "Mở file excel mẫu";
            this.linkExcelMau.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkExcelMau_LinkClicked);
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(22, 39);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(91, 21);
            this.txtSheet.TabIndex = 1;
            this.txtSheet.VisualStyleManager = this.vsmMain;
            // 
            // txtCungUngValue
            // 
            this.txtCungUngValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCungUngValue.Location = new System.Drawing.Point(172, 66);
            this.txtCungUngValue.MaxLength = 1;
            this.txtCungUngValue.Name = "txtCungUngValue";
            this.txtCungUngValue.Size = new System.Drawing.Size(195, 21);
            this.txtCungUngValue.TabIndex = 7;
            this.txtCungUngValue.Text = "MUA VN";
            this.txtCungUngValue.VisualStyleManager = this.vsmMain;
            // 
            // txtTLHHColumn
            // 
            this.txtTLHHColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTLHHColumn.Location = new System.Drawing.Point(453, 39);
            this.txtTLHHColumn.MaxLength = 1;
            this.txtTLHHColumn.Name = "txtTLHHColumn";
            this.txtTLHHColumn.Size = new System.Drawing.Size(71, 21);
            this.txtTLHHColumn.TabIndex = 6;
            this.txtTLHHColumn.Text = "E";
            this.txtTLHHColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtCotCungUng
            // 
            this.txtCotCungUng.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCotCungUng.Location = new System.Drawing.Point(405, 66);
            this.txtCotCungUng.MaxLength = 1;
            this.txtCotCungUng.Name = "txtCotCungUng";
            this.txtCotCungUng.Size = new System.Drawing.Size(119, 21);
            this.txtCotCungUng.TabIndex = 5;
            this.txtCotCungUng.Text = "F";
            this.txtCotCungUng.VisualStyleManager = this.vsmMain;
            // 
            // txtDMSDColumn
            // 
            this.txtDMSDColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDMSDColumn.Location = new System.Drawing.Point(373, 39);
            this.txtDMSDColumn.MaxLength = 1;
            this.txtDMSDColumn.Name = "txtDMSDColumn";
            this.txtDMSDColumn.Size = new System.Drawing.Size(74, 21);
            this.txtDMSDColumn.TabIndex = 5;
            this.txtDMSDColumn.Text = "D";
            this.txtDMSDColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMaNPLColumn
            // 
            this.txtMaNPLColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaNPLColumn.Location = new System.Drawing.Point(287, 39);
            this.txtMaNPLColumn.MaxLength = 1;
            this.txtMaNPLColumn.Name = "txtMaNPLColumn";
            this.txtMaNPLColumn.Size = new System.Drawing.Size(80, 21);
            this.txtMaNPLColumn.TabIndex = 4;
            this.txtMaNPLColumn.Text = "B";
            this.txtMaNPLColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMaSPColumn
            // 
            this.txtMaSPColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaSPColumn.Location = new System.Drawing.Point(199, 39);
            this.txtMaSPColumn.MaxLength = 1;
            this.txtMaSPColumn.Name = "txtMaSPColumn";
            this.txtMaSPColumn.Size = new System.Drawing.Size(82, 21);
            this.txtMaSPColumn.TabIndex = 3;
            this.txtMaSPColumn.Text = "A";
            this.txtMaSPColumn.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(373, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Cột";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Giá trị cột npl trong nước";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.chkTinhGop);
            this.uiGroupBox2.Controls.Add(this.chIsFromVietnam);
            this.uiGroupBox2.Controls.Add(this.btnMorong);
            this.uiGroupBox2.Controls.Add(this.uiButton1);
            this.uiGroupBox2.Controls.Add(this.chkOverwrite);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 137);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(529, 80);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // chkTinhGop
            // 
            this.chkTinhGop.BackColor = System.Drawing.Color.Transparent;
            this.chkTinhGop.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTinhGop.Location = new System.Drawing.Point(275, 51);
            this.chkTinhGop.Name = "chkTinhGop";
            this.chkTinhGop.Size = new System.Drawing.Size(244, 23);
            this.chkTinhGop.TabIndex = 7;
            this.chkTinhGop.Text = "Tính gộp định mức có mã NPL trùng nhau";
            this.chkTinhGop.Visible = false;
            this.chkTinhGop.VisualStyleManager = this.vsmMain;
            // 
            // chIsFromVietnam
            // 
            this.chIsFromVietnam.BackColor = System.Drawing.Color.Transparent;
            this.chIsFromVietnam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsFromVietnam.Location = new System.Drawing.Point(78, 51);
            this.chIsFromVietnam.Name = "chIsFromVietnam";
            this.chIsFromVietnam.Size = new System.Drawing.Size(191, 23);
            this.chIsFromVietnam.TabIndex = 6;
            this.chIsFromVietnam.Text = "Nguyên phụ liệu trong nước";
            this.chIsFromVietnam.VisualStyleManager = this.vsmMain;
            this.chIsFromVietnam.CheckedChanged += new System.EventHandler(this.chIsFromVietnam_CheckedChanged);
            // 
            // btnMorong
            // 
            this.btnMorong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMorong.ImageIndex = 5;
            this.btnMorong.ImageList = this.ImageList1;
            this.btnMorong.Location = new System.Drawing.Point(420, 50);
            this.btnMorong.Name = "btnMorong";
            this.btnMorong.Size = new System.Drawing.Size(88, 23);
            this.btnMorong.TabIndex = 5;
            this.btnMorong.Text = "Mở rộng";
            this.btnMorong.Visible = false;
            this.btnMorong.VisualStyleManager = this.vsmMain;
            this.btnMorong.Click += new System.EventHandler(this.btnMorong_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "Stock Index Up_32x32.png");
            this.ImageList1.Images.SetKeyName(5, "Stock Index Up_32x32 - Copy.png");
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(420, 19);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(88, 23);
            this.uiButton1.TabIndex = 1;
            this.uiButton1.Text = "Đọc file";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.BackColor = System.Drawing.Color.Transparent;
            this.chkOverwrite.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOverwrite.Location = new System.Drawing.Point(13, 51);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(58, 23);
            this.chkOverwrite.TabIndex = 3;
            this.chkOverwrite.Text = "Ghi đè";
            this.chkOverwrite.VisualStyleManager = this.vsmMain;
            // 
            // txtFilePath
            // 
            this.txtFilePath.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Multiline = true;
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(401, 23);
            this.txtFilePath.TabIndex = 0;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            this.txtFilePath.ButtonClick += new System.EventHandler(this.txtFilePath_ButtonClick);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaSPColumn;
            this.rfvMa.ErrorMessage = "\"Cột mã SP\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.txtSheet;
            this.rfvTenSheet.ErrorMessage = "\"Tên sheet\" bắt buộc phải nhập.";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            this.rfvTenSheet.Tag = "rfvTenSheet";
            // 
            // rfvDVT
            // 
            this.rfvDVT.ControlToValidate = this.txtTLHHColumn;
            this.rfvDVT.ErrorMessage = "\"Cột đơn vị tính\" bắt buộc phải chọn.";
            this.rfvDVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDVT.Icon")));
            this.rfvDVT.Tag = "rfvDVT";
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "\"Đường dẫn\" bắt buộc phải chọn.";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // rfvTenSP
            // 
            this.rfvTenSP.ControlToValidate = this.txtMaNPLColumn;
            this.rfvTenSP.ErrorMessage = "\"Cột tên SP\" bắt buộc phải chọn.";
            this.rfvTenSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSP.Icon")));
            this.rfvTenSP.Tag = "rfvTenSP";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtDMSDColumn;
            this.rfvMaHS.ErrorMessage = "\"Cột mã HS\" bắt buộc phải chọn.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // ImportKDTDMForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(556, 227);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MinimumSize = new System.Drawing.Size(564, 261);
            this.Name = "ImportKDTDMForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Import định mức từ Excel";
            this.Load += new System.EventHandler(this.ImportDMForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtDMSDColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPLColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSPColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTLHHColumn;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDVT;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSP;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UICheckBox chkOverwrite;
        private Janus.Windows.EditControls.UIButton btnMorong;
        private Janus.Windows.EditControls.UICheckBox chIsFromVietnam;
        private Janus.Windows.EditControls.UICheckBox chkTinhGop;
        private Janus.Windows.GridEX.EditControls.EditBox txtCungUngValue;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtCotCungUng;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.LinkLabel linkExcelMau;
    }
}