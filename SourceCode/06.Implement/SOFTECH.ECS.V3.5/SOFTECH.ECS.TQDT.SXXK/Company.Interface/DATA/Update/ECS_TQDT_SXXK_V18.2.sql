
/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 07/03/2014 15:19:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
    @MaDoanhNghiep VARCHAR(14) ,
    @MaHaiQuan CHAR(6) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
IF(@MaHaiQuan IS not NULL AND @MaHaiQuan <> '')
BEGIN
    SELECT  A.MaHaiQuan ,
            CASE WHEN A.MaLoaiHinh like'%V%' THEN (select Top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh ,
		    A.MaLoaiHinh,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'X%'
                  --OR A.MaLoaiHinh LIKE 'XGC%'
                  --OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   MaHaiQuan = @MaHaiQuan
                    AND MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh       
            
END
ELSE
	BEGIN
		  SELECT  A.MaHaiQuan ,
            CASE WHEN A.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh,
				A.MaLoaiHinh,
            A.NamDangKy ,
            A.NgayDangKy ,
            A.NGAY_THN_THX ,
            A.NgayHoanThanh ,
            e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM    t_SXXK_ToKhaiMauDich A
            LEFT JOIN ( SELECT DISTINCT
                                SoToKhai ,
                                MaLoaiHinh ,
                                NamDangKy ,
                                MaHaiQuan
                        FROM    t_KDT_SXXK_BKToKhaiXuat
                        --WHERE   MaHaiQuan = @MaHaiQuan
                      ) D ON A.SoToKhai = D.SoToKhai
                             AND A.MaLoaiHinh = D.MaLoaiHinh
                             AND A.NamDangKy = D.NamDangKy
                             AND A.MaHaiQuan = D.MaHaiQuan        
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
            LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai
                                                   AND a.MaLoaiHinh = e.MaLoaiHinh
                                                   AND a.NamDangKy = YEAR(e.NgayDangKy)
    WHERE   A.MaDoanhNghiep = @MaDoanhNghiep
            --AND A.MaHaiQuan = @MaHaiQuan
            AND ( A.MaLoaiHinh LIKE 'X%'
                  --OR A.MaLoaiHinh LIKE 'XGC%'
                  --OR A.MaLoaiHinh LIKE 'XCX%'
                ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
            AND A.NgayDangKy BETWEEN @TuNgay AND @DenNgay
            AND ( A.NGAY_THN_THX IS NOT NULL
                  OR YEAR(A.NGAY_THN_THX) != 1900
                )
            AND D.SoToKhai IS NULL         
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
            AND CAST(a.SoToKhai AS VARCHAR(50))
            + CAST(a.MaLoaiHinh AS VARCHAR(50))
            + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (
            SELECT  CAST(SoToKhai AS VARCHAR(50))
                    + CAST(MaLoaiHinh AS VARCHAR(50))
                    + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
            FROM    dbo.t_KDT_ToKhaiMauDich
            WHERE   --MaHaiQuan = @MaHaiQuan AND
                     MaDoanhNghiep = @MaDoanhNghiep
                    AND TrangThaiXuLy != 1
                    AND SoToKhai != 0 )
    ORDER BY A.NgayDangKy ,
            A.SoToKhai ,
            A.MaLoaiHinh       
	END 
	
	
	GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]    Script Date: 07/03/2014 15:37:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[p_SXXK_GetDSTKNChuaThanhLyDate]    
@MaDoanhNghiep VARCHAR (14),              
@MaHaiQuan char(6),              
@TuNgay datetime,              
@DenNgay datetime,              
@UserName varchar(50),              
@TenChuHang nvarchar(200)              
              
AS
IF (@MaHaiQuan IS NOT NULL AND @MaHaiQuan <> '')
BEGIN
SELECT               
  A.MaHaiQuan,              
     CASE WHEN A.MaLoaiHinh like'%V%' THEN (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh 
				A.MaLoaiHinh,           
  A.NamDangKy,              
  A.NgayDangKy,              
  A.NGAY_THN_THX,     
   A.NgayHoanThanh 
   ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.      
FROM t_SXXK_ToKhaiMauDich A              
 LEFT JOIN (SELECT DISTINCT              
    SoToKhai,              
    MaLoaiHinh,              
    NamDangKy,              
    MaHaiQuan              
   FROM t_KDT_SXXK_BKToKhaiNhap              
   WHERE              
    UserName != @UserName) D ON              
  A.SoToKhai=D.SoToKhai AND              
  A.MaLoaiHinh=D.MaLoaiHinh AND              
  A.NamDangKy=D.NamDangKy AND              
  A.MaHaiQuan=D.MaHaiQuan
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy)
              
WHERE              
 A.MaDoanhNghiep = @MaDoanhNghiep AND              
 A.MaHaiQuan = @MaHaiQuan AND              
-- D.SoToKhai IS NULL AND              
 A.MaLoaiHinh LIKE 'N%'AND              
 A.TenChuHang LIKE '%' + @TenChuHang + '%' AND              
 A.NgayDangKy between @TuNgay and @DenNgay AND               
 A.THANHLY <> 'H'     
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.   
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep AND TrangThaiXuLy != 1 AND SoToKhai != 0)
ORDER BY A.NgayDangKy  DESC, A.SoToKhai, A.MaLoaiHinh
END
ELSE
	BEGIN
		SELECT               
  A.MaHaiQuan,              
     CASE WHEN A.MaLoaiHinh like'%V%' THEN (select SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=A.SoToKhai)
		ELSE A.SoToKhai END AS SoToKhaiVNACCS,
            A.SoToKhai ,
    --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
				--else A.MaLoaiHinh end as MaLoaiHinh,  
				A.MaLoaiHinh,
  A.NamDangKy,              
  A.NgayDangKy,              
  A.NGAY_THN_THX,     
   A.NgayHoanThanh 
   ,e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.      
FROM t_SXXK_ToKhaiMauDich A              
 LEFT JOIN (SELECT DISTINCT              
    SoToKhai,              
    MaLoaiHinh,              
    NamDangKy,              
    MaHaiQuan              
   FROM t_KDT_SXXK_BKToKhaiNhap              
   WHERE              
    UserName != @UserName) D ON              
  A.SoToKhai=D.SoToKhai AND              
  A.MaLoaiHinh=D.MaLoaiHinh AND              
  A.NamDangKy=D.NamDangKy              
  AND A.MaHaiQuan=D.MaHaiQuan
  --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai. 
  LEFT JOIN dbo.t_KDT_ToKhaiMauDich e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = year(e.NgayDangKy) AND a.MaHaiQuan = e.MaHaiQuan
              
WHERE              
 A.MaDoanhNghiep = @MaDoanhNghiep AND              
 --A.MaHaiQuan = @MaHaiQuan AND              
-- D.SoToKhai IS NULL AND              
 A.MaLoaiHinh LIKE 'N%'AND              
 A.TenChuHang LIKE '%' + @TenChuHang + '%' AND              
 A.NgayDangKy between @TuNgay and @DenNgay AND               
 A.THANHLY <> 'H'     
 --HungTQ updated 22/06/2011. Khong lay to khai da Huy.   
 AND CAST(a.SoToKhai AS VARCHAR(50)) + CAST(a.MaLoaiHinh AS VARCHAR(50)) + CAST(a.NamDangKy AS VARCHAR(50)) NOT IN (SELECT  CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50)) + CAST(YEAR(NgayDangKy) AS VARCHAR(50)) FROM dbo.t_KDT_ToKhaiMauDich WHERE /*MaHaiQuan = @MaHaiQuan AND*/ MaDoanhNghiep = @MaDoanhNghiep AND TrangThaiXuLy != 1 AND SoToKhai != 0)
ORDER BY A.NgayDangKy  DESC, A.SoToKhai, A.MaLoaiHinh
	END
	
Go
update t_VNACC_Category_CityUNLOCODE set CityNameOrStateName='...' where locode like '%ZZZ'
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.2',GETDATE(), N' cập nhật hồ sơ thanh khoản')
END	
	
