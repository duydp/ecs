﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT;
//using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Collections.Generic;

namespace Company.Interface.Report.SXXK
{
    public partial class PhuLucToKhaiNhapA4 : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public int SoToKhai;
        public DateTime NgayDangKy;
        public string MaLoaiHinh = "";
        public Report.ReportViewTKNA4Form report;
        public bool BanLuuHaiQuan;
        public PhuLucToKhaiNhapA4()
        {
            InitializeComponent();
        }
        public void BindReport(bool inMaHang)
        {
            if (BanLuuHaiQuan)
            {
                lblBanLuuHQ.Text = "Bản lưu Hải quan";
            }
            else
            {
                lblBanLuuHQ.Text = "Bản lưu người khai Hải quan";
            }
            this.PrintingSystem.ShowMarginsWarning = false;
            lblThongBaoMienThue.Text = GlobalSettings.TieuDeInDinhMuc;
            decimal tongTriGiaNT = 0;
            decimal tongThueXNK = 0;
            if (this.MaLoaiHinh.Trim() == "NSX03")
            {
                lblTongTienThueXNK.Visible = false;
                xrTable2.Visible = false;
                lblThongBaoMienThue.Location = new Point(167, 683);
                lblThongBaoMienThue.Size = new Size(457, 83);
                lblThongBaoMienThue.Text = "DOANH NGHIỆP KHU CHẾ XUẤT KHÔNG NỘP THUẾ";
            }
            //lblMaHaiQuan.Text = GlobalSe1ttings.MA_HAI_QUAN;
            if(NgayDangKy > new DateTime(1900,1,1))
                lblNgayDangKy.Text = NgayDangKy.ToString("dd/MM/yyyy");
            if (SoToKhai != 0)
                lblSoToKhai.Text = "..." + SoToKhai + "...";
            else
                lblSoToKhai.Text = ".............";
            for (int i = 0; i < this.HMDCollection.Count; i++)
            {
                XRControl control = new XRControl();
                HangMauDich hmd = this.HMDCollection[i];
                control = this.xrTable1.Rows[i + 2].Controls["TenHang" + (i + 1)];
                control.Text = hmd.TenHang;
                if (hmd.TenHang.Length > 40) control.Font = new Font("Times New Roman", 7f);
                if (inMaHang)
                {
                    control.Text += " / " + hmd.MaPhu;
                    control.Font = new Font("Times New Roman", 7f);
                }
                control = this.xrTable1.Rows[i + 2].Controls["MaHS" + (i + 1)];
                control.Text = hmd.MaHS;
                control = this.xrTable1.Rows[i + 2].Controls["XuatXu" + (i + 1)];
                control.Text = hmd.NuocXX_ID;
                control = this.xrTable1.Rows[i + 2].Controls["Luong" + (i + 1)];
                control.Text = hmd.SoLuong.ToString("G15");
                control = this.xrTable1.Rows[i + 2].Controls["DVT" + (i + 1)];
                control.Text = DonViTinh.GetName((object)hmd.DVT_ID);
                control = this.xrTable1.Rows[i + 2].Controls["DonGiaNT" + (i + 1)];
                control.Text = hmd.DonGiaKB.ToString("G10");
                control = this.xrTable1.Rows[i + 2].Controls["TriGiaNT" + (i + 1)];
                control.Text = hmd.TriGiaKB.ToString("N2");
                control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                control.Text = hmd.TriGiaTT.ToString("N0");
                control = this.xrTable2.Rows[i + 2].Controls["ThueSuatXNK" + (i + 1)];
                if (hmd.ThueSuatXNKGiam == 0)
                    control.Text = hmd.ThueSuatXNK.ToString("N0");
                else
                    control.Text = hmd.ThueSuatXNKGiam.ToString("N0");

                control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                control.Text = hmd.ThueXNK.ToString("N0");

                tongTriGiaNT += Math.Round(hmd.TriGiaKB,2,MidpointRounding.AwayFromZero);
                tongThueXNK += hmd.ThueXNK;
            }
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
        }
        public void setVisibleImage(bool t)
        {
            //xrPictureBox1.Visible = t;
        }
        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }
        private void TenHang_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
    }
}
