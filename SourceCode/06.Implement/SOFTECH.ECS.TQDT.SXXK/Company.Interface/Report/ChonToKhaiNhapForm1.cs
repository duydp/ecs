using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.Report
{
    public partial class ChonToKhaiNhapForm1 : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKToKhaiNhapCollection TKNHoanThueCollection = new BKToKhaiNhapCollection();
        public BKToKhaiNhapCollection TKNKhongThuCollection = new BKToKhaiNhapCollection();
        public ChonToKhaiNhapForm1()
        {
            InitializeComponent();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            BKToKhaiNhapCollection temp = new BKToKhaiNhapCollection();
            foreach (GridEXRow row in dgList1.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    BKToKhaiNhap tkn = (BKToKhaiNhap)row.DataRow;
                    this.TKNKhongThuCollection.Add(tkn);
                    temp.Add(tkn);
                }
            }
            foreach(BKToKhaiNhap tkn in temp)
                this.TKNHoanThueCollection.Remove(tkn);
            try
            {
                dgList1.Refetch();
            }
            catch
            {
                dgList1.Refresh();
            }
            try
            {
                dgList2.Refetch();
            }
            catch
            {
                dgList2.Refresh();
            }
        }

        private void ChonToKhaiNhapForm1_Load(object sender, EventArgs e)
        {
            this.HSTL.LoadBKCollection();
            this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].LoadChiTietBangKe();
            BKToKhaiNhapCollection TKN = this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].bkTKNCollection;
            TKNHoanThueCollection = (BKToKhaiNhapCollection)TKN.Clone();
            dgList1.DataSource = this.TKNHoanThueCollection;
            dgList1.Refetch();
            dgList2.DataSource = this.TKNKhongThuCollection;
            dgList2.Refetch();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            BKToKhaiNhapCollection temp = new BKToKhaiNhapCollection();
            foreach (GridEXRow row in dgList2.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    BKToKhaiNhap tkn = (BKToKhaiNhap)row.DataRow;
                    this.TKNHoanThueCollection.Add(tkn);
                    temp.Add(tkn);
                }
            }
            foreach (BKToKhaiNhap tkn in temp)
                this.TKNKhongThuCollection.Remove(tkn);
            try
            {
                dgList1.Refetch();
            }
            catch
            {
                dgList1.Refresh();
            }
            try
            {
                dgList2.Refetch();
            }
            catch
            {
                dgList2.Refresh();
            }
        }

        private void dgList2_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                BKToKhaiNhap tkn = (BKToKhaiNhap)e.Row.DataRow;
                this.TKNHoanThueCollection.Add(tkn);
                this.TKNKhongThuCollection.Remove(tkn);
                try
                {
                    dgList1.Refetch();
                }
                catch
                {
                    dgList1.Refresh();
                }
                try
                {
                    dgList2.Refetch();
                }
                catch
                {
                    dgList2.Refresh();
                }
            }
           
        }

        private void dgList2_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["NgayThucNhap"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucNhap"].Text = "";

        }

        private void dgList1_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (Convert.ToDateTime(e.Row.Cells["NgayThucNhap"].Value) <= new DateTime(1900, 1, 1)) e.Row.Cells["NgayThucNhap"].Text = "";

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

