/*
Run this script on:

        ECSTEAM.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ECSTEAM.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 10/18/2011 10:11:23 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DinhMucDangKy_Copy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Copy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--select * from dbo.t_KDT_SXXK_MsgSend

    -- Insert statements for procedure here
   --delete from t_KDT_SXXK_DinhMuc_Delete
   -- delete from t_KDT_SXXK_DinhMucDangKy_Delete
	INSERT INTO t_KDT_SXXK_DinhMuc_Delete
	([ID]
      ,[MaSanPham]
      ,[MaNguyenPhuLieu]
      ,[DVT_ID]
      ,[DinhMucSuDung]
      ,[TyLeHaoHut]
      ,[GhiChu]
      ,[STTHang]
      ,[Master_ID])
	SELECT     t_KDT_SXXK_DinhMuc.[ID]
      ,t_KDT_SXXK_DinhMuc.[MaSanPham]
      ,t_KDT_SXXK_DinhMuc.[MaNguyenPhuLieu]
      ,t_KDT_SXXK_DinhMuc.[DVT_ID]
      ,t_KDT_SXXK_DinhMuc.[DinhMucSuDung]
      ,t_KDT_SXXK_DinhMuc.[TyLeHaoHut]
      ,t_KDT_SXXK_DinhMuc.[GhiChu]
      ,t_KDT_SXXK_DinhMuc.[STTHang]
      ,t_KDT_SXXK_DinhMuc.[Master_ID]
	FROM         t_KDT_SXXK_DinhMuc INNER JOIN
						  t_KDT_SXXK_DinhMucDangKy ON t_KDT_SXXK_DinhMuc.Master_ID = t_KDT_SXXK_DinhMucDangKy.ID						  
	WHERE     (t_KDT_SXXK_DinhMucDangKy.ID = @ID)    
	
	
	INSERT INTO t_KDT_SXXK_DinhMucDangKy_Delete
	
	SELECT * FROM dbo.t_KDT_SXXK_DinhMucDangKy WHERE ID=@ID
	
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_DinhMuc_Copy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Copy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--select * from t_KDT_SXXK_SanPham_Delete
	SET NOCOUNT ON;
	INSERT INTO t_KDT_SXXK_DinhMuc_Delete
	([ID]
      ,[MaSanPham]
      ,[MaNguyenPhuLieu]
      ,[DVT_ID]
      ,[DinhMucSuDung]
      ,[TyLeHaoHut]
      ,[GhiChu]
      ,[STTHang]
      ,[Master_ID])
	SELECT [ID]
      ,[MaSanPham]
      ,[MaNguyenPhuLieu]
      ,[DVT_ID]
      ,[DinhMucSuDung]
      ,[TyLeHaoHut]
      ,[GhiChu]
      ,[STTHang]
      ,[Master_ID] FROM  t_kdt_sxxk_DinhMuc
	WHERE  ID=@ID
    -- Insert statements for procedure here

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[DinhMucDangKyCopy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DinhMucDangKyCopy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	INSERT INTO  dbo.t_KDT_SXXK_DinhMuc_Delete
	([ID]
      ,[MaSanPham]
      ,[MaNguyenPhuLieu]
      ,[DVT_ID]
      ,[DinhMucSuDung]
      ,[TyLeHaoHut]
      ,[GhiChu]
      ,[STTHang]
      ,[Master_ID])
	SELECT     t_KDT_SXXK_DinhMuc.[ID]
      ,t_KDT_SXXK_DinhMuc.[MaSanPham]
      ,t_KDT_SXXK_DinhMuc.[MaNguyenPhuLieu]
      ,t_KDT_SXXK_DinhMuc.[DVT_ID]
      ,t_KDT_SXXK_DinhMuc.[DinhMucSuDung]
      ,t_KDT_SXXK_DinhMuc.[TyLeHaoHut]
      ,t_KDT_SXXK_DinhMuc.[GhiChu]
      ,t_KDT_SXXK_DinhMuc.[STTHang]
      ,t_KDT_SXXK_DinhMuc.[Master_ID]
	FROM         t_KDT_SXXK_DinhMuc INNER JOIN
						  t_KDT_SXXK_DinhMucDangKy ON t_KDT_SXXK_DinhMuc.Master_ID = t_KDT_SXXK_DinhMucDangKy.ID
	WHERE     (t_KDT_SXXK_DinhMucDangKy.ID = @ID)
	
	
	INSERT INTO dbo.t_KDT_SXXK_DinhMucDangKy_Delete
	
	SELECT * FROM dbo.t_KDT_SXXK_DinhMucDangKy WHERE ID=@ID
	
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE dbo.t_HaiQuan_CuaKhau SET Ten = N'Cảng Đà nẵng' WHERE ID = 'C021'

GO
UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.2.3', Date = getdate()