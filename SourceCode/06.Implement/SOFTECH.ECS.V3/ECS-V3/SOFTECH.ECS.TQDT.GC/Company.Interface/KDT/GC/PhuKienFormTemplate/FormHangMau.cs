﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using System.Linq;
namespace Company.Interface.KDT.GC
{
    public partial class FormHangMau : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public string MaLoaiPK = string.Empty;
        public bool boolFlag;

        public FormHangMau()
        {
            InitializeComponent();
            this.Load+=new EventHandler(FormHangMau_Load);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void FormHangMau_Load(object sender, EventArgs e)
        {
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;

            bool isSua = MaLoaiPK == "505";
            dgList.RootTable.Columns["ThongTinCu"].Visible = txtMaCu.ReadOnly =
            lblCu.Visible = txtMaCu.Visible = lblTenCu.Visible = isSua;
            btnChon.Visible = MaLoaiPK != "805";

            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);

            if (pkdk.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                btnAdd.Enabled = false;
                btnXoa.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False; ;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == MaLoaiPK)
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
            }
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            LoaiPK.MaPhuKien = MaLoaiPK;
            if (LoaiPK.HPKCollection.Count == 0)
                LoaiPK.LoadCollection();
            dgList.DataSource = LoaiPK.HPKCollection;


        }
        private void reset()
        {
            txtMa.Text = "";
            txtTen.Text = "";
            txtMaHS.Text = "";
            txtSoLuong.Text = "";
            error.SetError(txtMaHS, null);
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);

            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            HangPK = new HangPhuKien();


        }
        private void setError()
        {
            error.Clear();
            error.SetError(txtTen, null);
            error.SetError(txtMa, null);
            error.SetError(txtMaHS, null);

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {

            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                checkExitsAndSTTHang();
            }
        }
        private void checkExitsAndSTTHang()
        {
            if (string.IsNullOrEmpty(MaLoaiPK))
            {
                ShowMessage("Mã loại phụ kiện chưa thiết lập", false);
                DialogResult = DialogResult.Cancel;
            }

            LoaiPK.HPKCollection.Remove(HangPK);
            foreach (HangPhuKien p in LoaiPK.HPKCollection)
            {
                if (p.MaHang.Trim() == txtMa.Text)
                {
                    //ShowMessage("Đã có loại sản phẩm này trong danh sách.", false);
                    showMsg("Mã này đã được thêm vào rồi");
                    if (HangPK.MaHang.Trim() != "")
                        LoaiPK.HPKCollection.Add(HangPK);
                    txtMa.Focus();
                    return;
                }
            }
            HangPK.MaHang = txtMa.Text;
            HangPK.TenHang = txtTen.Text;
            HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            HangPK.MaHS = txtMaHS.Text;
            HangPK.ThongTinCu = txtMaCu.Text;
            HangPK.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            LoaiPK.HPKCollection.Add(HangPK);
            LoaiPK.Master_ID = pkdk.ID;
            LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
            if (boolFlag == true)
            {
                LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                boolFlag = false;
            }
            else
            {
                LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
            }

            Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
            if (lpk == null)
                pkdk.PKCollection.Add(LoaiPK);
            dgList.DataSource = LoaiPK.HPKCollection;
            reset();
            this.setErro();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

            HangPK = (Company.GC.BLL.KDT.GC.HangPhuKien)e.Row.DataRow;
            if (HangPK != null)
            {
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                txtMa.Text = HangPK.MaHang;
                txtTen.Text = HangPK.TenHang;
                txtMaHS.Text = HangPK.MaHS;
                txtMaCu.Text = HangPK.ThongTinCu;
                txtSoLuong.Value = HangPK.SoLuong;
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                error.Clear();
            }
        }



        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

        private void ctrNguyenTe_Load(object sender, EventArgs e)
        {

        }

        private void dgHangMau_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {

            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.HangMau tbDelete = (Company.GC.BLL.KDT.GC.HangMau)row.GetRow().DataRow;
                        tbDelete.Delete();

                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            error.SetError(txtMaHS, null);

        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            HangPhuKienCollection hpkColl = new HangPhuKienCollection();
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa thông tin đã chọn không ?", true) == "Yes")
            {
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                        }
                        hpkColl.Add(pkDelete);
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hpkk in hpkColl)
                {
                    try
                    {
                        LoaiPK.HPKCollection.Remove(hpkk);
                    }
                    catch { }

                }
                dgList.DataSource = LoaiPK.HPKCollection;
                this.reset();
                this.setErro();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }
        private void btnChon_Click(object sender, EventArgs e)
        {
            FormChonHangMau frm = new FormChonHangMau();
            frm.HD = HD;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    foreach (HangPhuKien hangPK in frm.HPKCollection)
                    {
                        hangPK.Master_ID = LoaiPK.ID;
                        LoaiPK.HPKCollection.Add(hangPK);
                    }
                    LoaiPK.Master_ID = pkdk.ID;
                    LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                    if (boolFlag == true)
                    {
                        LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                        boolFlag = false;
                    }
                    else
                    {
                        LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                    }
                    Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                    if (lpk == null)
                        pkdk.PKCollection.Add(LoaiPK);
                    dgList.DataSource = LoaiPK.HPKCollection;
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                }
                reset();
                this.setErro();
            }
        }
    }
}