namespace Company.Interface.GC
{
    partial class SelectNPLTaiXuatTKCTForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListTon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListTon_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgLispbTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgLispbTKX_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectNPLTaiXuatTKCTForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.dgListTon = new Janus.Windows.GridEX.GridEX();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmdSave = new Janus.Windows.EditControls.UIButton();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvToKhai = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblTong = new System.Windows.Forms.Label();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.txtLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtLuongTaiXuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbhang = new Janus.Windows.EditControls.UIComboBox();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.dgLispbTKX = new Janus.Windows.GridEX.GridEX();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.label4 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgLispbTKX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(933, 587);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // dgListTon
            // 
            this.dgListTon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTon.AlternatingColors = true;
            this.dgListTon.AutomaticSort = false;
            this.dgListTon.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgListTon.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTon.ColumnAutoResize = true;
            dgListTon_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListTon_DesignTimeLayout_Reference_0.Instance")));
            dgListTon_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListTon_DesignTimeLayout_Reference_0});
            dgListTon_DesignTimeLayout.LayoutString = resources.GetString("dgListTon_DesignTimeLayout.LayoutString");
            this.dgListTon.DesignTimeLayout = dgListTon_DesignTimeLayout;
            this.dgListTon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTon.GroupByBoxVisible = false;
            this.dgListTon.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTon.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTon.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTon.ImageList = this.ImageList1;
            this.dgListTon.Location = new System.Drawing.Point(3, 8);
            this.dgListTon.Name = "dgListTon";
            this.dgListTon.RecordNavigator = true;
            this.dgListTon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTon.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTon.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTon.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTon.Size = new System.Drawing.Size(927, 129);
            this.dgListTon.TabIndex = 19;
            this.dgListTon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTon.VisualStyleManager = this.vsmMain;
            this.dgListTon.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList1_RowDoubleClick);
            this.dgListTon.SelectionChanged += new System.EventHandler(this.dgListTon_SelectionChanged);
            this.dgListTon.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList1_LoadingRow);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(837, 18);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 291;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Location = new System.Drawing.Point(686, 18);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(140, 23);
            this.cmdSave.TabIndex = 292;
            this.cmdSave.Text = "Lưu bảng phân bổ";
            this.cmdSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // rfvToKhai
            // 
            this.rfvToKhai.ControlToValidate = this.txtSoToKhai;
            this.rfvToKhai.ErrorMessage = "\"Tờ khai\" bắt buộc phải chọn.";
            this.rfvToKhai.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvToKhai.Icon")));
            this.rfvToKhai.Tag = "rfvToKhai";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Location = new System.Drawing.Point(61, 29);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.Size = new System.Drawing.Size(100, 21);
            this.txtSoToKhai.TabIndex = 292;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.uiGroupBox1;
            this.cvError.HostingForm = this;
            this.cvError.ValidationDepth = Company.Controls.CustomValidation.ValidationDepth.ContainerOnly;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.lblTong);
            this.uiGroupBox1.Controls.Add(this.btnAdd);
            this.uiGroupBox1.Controls.Add(this.txtLuong);
            this.uiGroupBox1.Controls.Add(this.txtMaNPL);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 207);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(933, 83);
            this.uiGroupBox1.TabIndex = 298;
            this.uiGroupBox1.Text = "Thông tin NPL tái xuất";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // lblTong
            // 
            this.lblTong.AutoSize = true;
            this.lblTong.BackColor = System.Drawing.Color.Transparent;
            this.lblTong.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTong.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblTong.Location = new System.Drawing.Point(3, 66);
            this.lblTong.Name = "lblTong";
            this.lblTong.Size = new System.Drawing.Size(225, 14);
            this.lblTong.TabIndex = 300;
            this.lblTong.Text = "Danh sách nguyên phụ liệu tái xuất";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(775, 28);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(110, 23);
            this.btnAdd.TabIndex = 290;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // txtLuong
            // 
            this.txtLuong.DecimalDigits = 3;
            this.txtLuong.Location = new System.Drawing.Point(565, 29);
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.Size = new System.Drawing.Size(204, 21);
            this.txtLuong.TabIndex = 294;
            this.txtLuong.Text = "0.000";
            this.txtLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.Location = new System.Drawing.Point(217, 29);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.ReadOnly = true;
            this.txtMaNPL.Size = new System.Drawing.Size(259, 21);
            this.txtMaNPL.TabIndex = 293;
            this.txtMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(482, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 291;
            this.label3.Text = "Lượng tái xuất";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(166, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 291;
            this.label2.Text = "Mã NPL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(10, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 290;
            this.label1.Text = "Tờ khai";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.txtLuongTaiXuat);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.cbhang);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(933, 53);
            this.uiGroupBox4.TabIndex = 293;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(16, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 296;
            this.label7.Text = "Mã NPL";
            // 
            // txtLuongTaiXuat
            // 
            this.txtLuongTaiXuat.DecimalDigits = 3;
            this.txtLuongTaiXuat.Location = new System.Drawing.Point(565, 20);
            this.txtLuongTaiXuat.Name = "txtLuongTaiXuat";
            this.txtLuongTaiXuat.ReadOnly = true;
            this.txtLuongTaiXuat.Size = new System.Drawing.Size(204, 21);
            this.txtLuongTaiXuat.TabIndex = 294;
            this.txtLuongTaiXuat.Text = "0.000";
            this.txtLuongTaiXuat.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtLuongTaiXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(482, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 294;
            this.label6.Text = "Lượng tái xuất";
            // 
            // cbhang
            // 
            this.cbhang.Location = new System.Drawing.Point(69, 20);
            this.cbhang.Name = "cbhang";
            this.cbhang.Size = new System.Drawing.Size(407, 21);
            this.cbhang.TabIndex = 0;
            this.cbhang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbhang.SelectedIndexChanged += new System.EventHandler(this.cbhang_SelectedIndexChanged);
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToCompare = this.txtLuongTaiXuat;
            this.cvSoLuong.ControlToValidate = this.txtLuong;
            this.cvSoLuong.ErrorMessage = "Lượng tái xuất phải nhỏ hơn hoặc bằng lượng nguyên phụ liệu tồn của tờ khai";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Tag = "cvSoLuong";
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // gridEXExporter1
            // 
            this.gridEXExporter1.GridEX = this.dgLispbTKX;
            // 
            // dgLispbTKX
            // 
            this.dgLispbTKX.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgLispbTKX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgLispbTKX.AlternatingColors = true;
            this.dgLispbTKX.AutomaticSort = false;
            this.dgLispbTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgLispbTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgLispbTKX.ColumnAutoResize = true;
            this.dgLispbTKX.ColumnAutoSizeMode = Janus.Windows.GridEX.ColumnAutoSizeMode.DisplayedCellsAndHeader;
            dgLispbTKX_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgLispbTKX_DesignTimeLayout_Reference_0.Instance")));
            dgLispbTKX_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgLispbTKX_DesignTimeLayout_Reference_0});
            dgLispbTKX_DesignTimeLayout.LayoutString = resources.GetString("dgLispbTKX_DesignTimeLayout.LayoutString");
            this.dgLispbTKX.DesignTimeLayout = dgLispbTKX_DesignTimeLayout;
            this.dgLispbTKX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgLispbTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgLispbTKX.GroupByBoxVisible = false;
            this.dgLispbTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgLispbTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgLispbTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgLispbTKX.ImageList = this.ImageList1;
            this.dgLispbTKX.Location = new System.Drawing.Point(3, 8);
            this.dgLispbTKX.Name = "dgLispbTKX";
            this.dgLispbTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgLispbTKX.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgLispbTKX.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgLispbTKX.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgLispbTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgLispbTKX.Size = new System.Drawing.Size(927, 220);
            this.dgLispbTKX.TabIndex = 19;
            this.dgLispbTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgLispbTKX.VisualStyleManager = this.vsmMain;
            this.dgLispbTKX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList2_RowDoubleClick);
            this.dgLispbTKX.SelectionChanged += new System.EventHandler(this.dgLispbTKX_SelectionChanged);
            this.dgLispbTKX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList2_LoadingRow);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Image = ((System.Drawing.Image)(resources.GetObject("uiButton2.Image")));
            this.uiButton2.Location = new System.Drawing.Point(8, 18);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(124, 23);
            this.uiButton2.TabIndex = 295;
            this.uiButton2.Text = "Thêm từ excel";
            this.uiButton2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(0, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(420, 14);
            this.label4.TabIndex = 296;
            this.label4.Text = "Danh sách nguyên phụ liệu của tờ khai nhập khẩu đưa vào tái xuất";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgListTon);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 67);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(933, 140);
            this.uiGroupBox2.TabIndex = 297;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(0, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(225, 14);
            this.label5.TabIndex = 299;
            this.label5.Text = "Danh sách nguyên phụ liệu tái xuất";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiButton2);
            this.uiGroupBox3.Controls.Add(this.cmdSave);
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 535);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(933, 52);
            this.uiGroupBox3.TabIndex = 300;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.dgLispbTKX);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 304);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(933, 231);
            this.uiGroupBox5.TabIndex = 301;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // SelectNPLTaiXuatTKCTForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(933, 587);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "SelectNPLTaiXuatTKCTForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "";
            this.Load += new System.EventHandler(this.BK07Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgLispbTKX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.GridEX.GridEX dgListTon;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton cmdSave;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvToKhai;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.CompareValidator cvSoLuong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIComboBox cbhang;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongTaiXuat;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.GridEX dgLispbTKX;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private System.Windows.Forms.Label lblTong;
    }
}