﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiSuaHangHoaXuatKhau1 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiSuaHangHoaXuatKhau1()
        {
            InitializeComponent();
        }
        public void BindReport(VAE2LE0 Vae2le)
        {
            KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();


            //lblSoTrang.Text = "1/" + Vae2le.K69.GetValue().ToString();
            int HangHoa = Vae2le.HangMD.Count;
            if (HangHoa <= 2)
            {
                lblSoTrang.Text = "1/" + System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblSoTrang.Text = "1/" + System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSotokhai.Text = Vae2le.ECN.GetValue().ToString();
            lblSotokhaidautien.Text = Vae2le.FIC.GetValue().ToString();
            lblSonhanhtokhaichianho.Text = Vae2le.BNO.GetValue().ToString();
            lblTongsotokhaichianho.Text = Vae2le.DNO.GetValue().ToString();
            lblSotokhaitamnhaptaixuat.Text = Vae2le.TDN.GetValue().ToString();
            lblMaphanloaikiemtra.Text = Vae2le.K07.GetValue().ToString();
            lblMaloaihinh.Text = Vae2le.ECB.GetValue().ToString();
            lblMaphanloaihanghoa.Text = Vae2le.CCC.GetValue().ToString();
            lblMahieuphuongthucvanchuyen.Text = Vae2le.MTC.GetValue().ToString();
            lblMasothuedaidien.Text = Vae2le.K01.GetValue().ToString();
            lblTencoquanhaiquantiepnhantokhai.Text = Vae2le.K08.GetValue().ToString();
            lblNhomxulyhoso.Text = Vae2le.CHB.GetValue().ToString();
            if
             (Convert.ToDateTime(Vae2le.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydangky.Text = Convert.ToDateTime(Vae2le.K10.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydangky.Text = "";
            }

            if (Vae2le.AD1.GetValue().ToString() != "" && Vae2le.AD1.GetValue().ToString() != "0")
            {
                lblGiodangky.Text = Vae2le.AD1.GetValue().ToString().Substring(0, 2) + ":" + Vae2le.AD1.GetValue().ToString().Substring(2, 2) + ":" + Vae2le.AD1.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiodangky.Text = "";
            }
            if
                (Convert.ToDateTime(Vae2le.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaythaydoidangky.Text = Convert.ToDateTime(Vae2le.AD2.GetValue()).ToString("dd/MM/yyyy");

            }
            else
            {
                lblNgaythaydoidangky.Text = "";
            }

            if (Vae2le.AD3.GetValue().ToString() != "" && Vae2le.AD3.GetValue().ToString() != "0")
            {
                lblGiothaydoidangky.Text = Vae2le.AD3.GetValue().ToString().Substring(0, 2) + ":" + Vae2le.AD3.GetValue().ToString().Substring(2, 2) + ":" + Vae2le.AD3.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiothaydoidangky.Text = "";
            }
            if
                (Convert.ToDateTime(Vae2le.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoihantainhapxuat.Text = Convert.ToDateTime(Vae2le.RID.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoihantainhapxuat.Text = "";
            }
            lblBieuthitruonghophethan.Text = Vae2le.AAA.GetValue().ToString();
            lblMaxuatkhau.Text = Vae2le.EPC.GetValue().ToString();
            lblTenxuatkhau.Text = Vae2le.EPN.GetValue().ToString();
            lblMaBuuChinhXK.Text = Vae2le.EPP.GetValue().ToString();
            lblDiachinguoixuat.Text = Vae2le.EPA.GetValue().ToString();
            lblSodtnguoixuat.Text = Vae2le.EPT.GetValue().ToString();
            lblManguoiuythacxuatkhau.Text = Vae2le.EXC.GetValue().ToString();
            lblTennguoiuythacxuatkhau.Text = Vae2le.EXN.GetValue().ToString();
            lblManguoinhapkhau.Text = Vae2le.CGC.GetValue().ToString();
            lblTennguoinhapkhau.Text = Vae2le.CGN.GetValue().ToString();
            lblMabuuchinh.Text = Vae2le.CGP.GetValue().ToString();
            lblDiachi1.Text = Vae2le.CGA.GetValue().ToString();
            lblDiachi2.Text = Vae2le.CAT.GetValue().ToString();
            lblDiachi3.Text = Vae2le.CAC.GetValue().ToString();
            lblDiachi4.Text = Vae2le.CAS.GetValue().ToString();
            lblManuoc.Text = Vae2le.CGK.GetValue().ToString();
            lblMadailyhaiquan.Text = Vae2le.K32.GetValue().ToString();
            lblTendailyhaiquan.Text = Vae2le.K33.GetValue().ToString();
            lblManhanvienhaiquan.Text = Vae2le.K34.GetValue().ToString();
            lblSovandon.Text = Vae2le.EKN.GetValue().ToString();
            lblSoluong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(Vae2le.NO.GetValue().ToString().Replace(".", ",")));
            lblMadonvitinh.Text = Vae2le.NOT.GetValue().ToString();
            lblTongtrongluonghang.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(Vae2le.GW.GetValue().ToString().Replace(".", ",")));
            lblMadonvitinhtrongluong.Text = Vae2le.GWT.GetValue().ToString();
            lblMadiadiemluukho.Text = Vae2le.ST.GetValue().ToString();
            lblTendiadiemluukho.Text = Vae2le.K42.GetValue().ToString();
            lblMadiadiemnhanhangcuoi.Text = Vae2le.DSC.GetValue().ToString();
            lblTendiadiemnhanhangcuoi.Text = Vae2le.DSN.GetValue().ToString();
            lblMadiadiemxephang.Text = Vae2le.PSC.GetValue().ToString();
            lblTendiadiemxephang.Text = Vae2le.PSN.GetValue().ToString();
            lblMaphuongtienvanchuyen.Text = Vae2le.VSC.GetValue().ToString();
            lblTenphuongtienvanchuyen.Text = Vae2le.VSN.GetValue().ToString();
            if
            (Convert.ToDateTime(Vae2le.SYM.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayhangdidukien.Text = Convert.ToDateTime(Vae2le.SYM.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayhangdidukien.Text = "";
            }
            lblKyhieusohieu.Text = Vae2le.MRK.GetValue().ToString();
            for (int i = 0; i < Vae2le.SS_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:

                        lblPhanloaigiayphepxuatkhau1.Text = Vae2le.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep1.Text = Vae2le.SS_.listAttribute[1].GetValueCollection(i).ToString();

                        break;
                    case 1:

                        lblPhanloaigiayphepxuatkhau2.Text = Vae2le.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep2.Text = Vae2le.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 2:

                        lblPhanloaigiayphepxuatkhau3.Text = Vae2le.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep3.Text = Vae2le.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 3:

                        lblPhanloaigiayphepxuatkhau4.Text = Vae2le.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep4.Text = Vae2le.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 4:

                        lblPhanloaigiayphepxuatkhau5.Text = Vae2le.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep5.Text = Vae2le.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;

                }

            }
            lblPhanloaihinhthuchoadon.Text = Vae2le.IV1.GetValue().ToString();

            lblSohoadon.Text = Vae2le.IV3.GetValue().ToString();
            lblSotiepnhanhoadondientu.Text = Vae2le.IV2.GetValue().ToString();
            if
                (Convert.ToDateTime(Vae2le.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayphathanh.Text = Convert.ToDateTime(Vae2le.IVD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayphathanh.Text = "";
            }
            lblPhuongthucthanhtoan.Text = Vae2le.IVP.GetValue().ToString();
            lblMadkgiahoadon.Text = Vae2le.IP2.GetValue().ToString();
            lblMadongtienhoadon.Text = Vae2le.IP3.GetValue().ToString();
            decimal Tongtrigiahoadon = Convert.ToDecimal(Vae2le.IP4.GetValue().ToString().Replace(".", ","));
            decimal Tygiatinhthue = Convert.ToDecimal(Vae2le.RC_.listAttribute[1].GetValueCollection(0).ToString().Replace(".", ","));
            decimal Tongthue = Tongtrigiahoadon * Tygiatinhthue;
            lblTongtrigiahoadon.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae2le.IP4.GetValue().ToString().Replace(".", ",")));
            lblMaphanloaigiahoadon.Text = Vae2le.IP1.GetValue().ToString();
            //lblMadongtientongthue.Text = Vae2le.FCD.GetValue().ToString();
            lblMadongtientongthue.Text = "VND";
            //lblTongthue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae2le.FKK.GetValue().ToString().Replace(".", ",")));
            lblTongthue.Text = String.Format("{0:N2}", Tongthue);
            for (int i = 0; i < Vae2le.RC_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMadongtientygiathue1.Text = Vae2le.RC_.listAttribute[0].GetValueCollection(i).ToString();
                        lblTygiatinhthue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae2le.RC_.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 1:
                        lblMadongtientygiathue2.Text = Vae2le.RC_.listAttribute[0].GetValueCollection(i).ToString();
                        lblTygiatinhthue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae2le.RC_.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;


                }

            }

            //minhnd Fix Trị giá Tờ khai
            TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(lblSotokhai.Text);
            decimal soToKhai = Convert.ToDecimal(TKMD.SoToKhai.ToString().Substring(11, 1));
            if (TKMD.TrangThaiXuLy == "1")
            {
                lblTenThongTinXuat.Text = "Tờ khai tạm hàng hóa xuất khẩu";
            }
            if (TKMD.TrangThaiXuLy == "2" && soToKhai >= 1)
            {
                lblTenThongTinXuat.Text = "Tờ khai bổ sung hàng hóa xuất khẩu (người khai Hải quan thực hiện)";
            }
            if (TKMD.TrangThaiXuLy == "3" && soToKhai >= 1)
            {
                lblTenThongTinXuat.Text = "Tờ khai bổ sung hàng hóa xuất khẩu (người khai Hải quan thực hiện)";
            }
            decimal triGiaHang = 0;
            TKMD.LoadFull();
            foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
            {
                triGiaHang += hmd.TriGiaHoaDon;
            }
            lblTonghesophanbothue.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
            //minhnd Fix Trị giá Tờ khai
            //lblTonghesophanbothue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae2le.TP.GetValue().ToString().Replace(".", ",")));

            lblMaphanloainhaplieutonggia.Text = Vae2le.K68.GetValue().ToString();
            lblPhanloaikhongcanquydoi.Text = Vae2le.CNV.GetValue().ToString();

            for (int i = 0; i < Vae2le.EA_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanloaidinhkemkhaibaodientu1.Text = Vae2le.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao1.Text = Vae2le.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 1:
                        lblPhanloaidinhkemkhaibaodientu2.Text = Vae2le.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao2.Text = Vae2le.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 2:
                        lblPhanloaidinhkemkhaibaodientu3.Text = Vae2le.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao3.Text = Vae2le.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                }

            }

            lblNguoinopthue.Text = Vae2le.TPM.GetValue().ToString();
            lblMaxacdinhthoihannopthue.Text = Vae2le.ENC.GetValue().ToString();
            lblPhanloainopthue.Text = Vae2le.PAY.GetValue().ToString();
            //lblTongsotienthuexuatkhau.Text = Vae2le.ETA.GetValue().ToString();
            lblMatientetongtienthuexuat.Text = Vae2le.AD7.GetValue().ToString();
            lblTongsotienthuexuatkhau.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae2le.ETA.GetValue().ToString().Replace(".", ",")));
            lblTongsotienlephi.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae2le.TCO.GetValue()));
            //lblTongsotienbaolanh.Text = Vae2le.SAM.GetValue().ToString();
            lblMatientetienbaolanh.Text = Vae2le.AD8.GetValue().ToString();
            lblTongsotienbaolanh.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae2le.SAM.GetValue().ToString().Replace(".", ",")));
            lblTongsotokhaicuatrang.Text = Vae2le.K69.GetValue().ToString();
            lblTongsodonghangtokhai.Text = Vae2le.K70.GetValue().ToString();
            lblPhanghichu.Text = Vae2le.NT2.GetValue().ToString();
            lblSoquanlynoibodoanhnghiep.Text = Vae2le.REF.GetValue().ToString();
            lblSoquanlynguoisudung.Text = Vae2le.K82.GetValue().ToString();
            if
                (Convert.ToDateTime(Vae2le.DPD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaykhoihanh.Text = Convert.ToDateTime(Vae2le.DPD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaykhoihanh.Text = "";
            }
            for (int i = 0; i < Vae2le.ST_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblDiadiem.Text = Vae2le.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if (Convert.ToDateTime(Vae2le.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen.Text = Convert.ToDateTime(Vae2le.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen.Text = "";
                        }

                        if (Convert.ToDateTime(Vae2le.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen.Text = Convert.ToDateTime(Vae2le.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen.Text = "";
                        }
                        break;
                    case 1:
                        lblDiadiem2.Text = Vae2le.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if (Convert.ToDateTime(Vae2le.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen2.Text = Convert.ToDateTime(Vae2le.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen2.Text = "";
                        }
                        if (Convert.ToDateTime(Vae2le.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen2.Text = Convert.ToDateTime(Vae2le.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen2.Text = "";
                        }
                        break;
                    case 2:
                        lblDiadiem3.Text = Vae2le.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if (Convert.ToDateTime(Vae2le.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen3.Text = Convert.ToDateTime(Vae2le.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen3.Text = "";
                        }
                        if (Convert.ToDateTime(Vae2le.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen3.Text = Convert.ToDateTime(Vae2le.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen3.Text = "";
                        }
                        break;


                }

            }

            lblDiadiemdich.Text = Vae2le.ARP.GetValue().ToString();
            if
                (Convert.ToDateTime(Vae2le.ADT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydukienden.Text = Convert.ToDateTime(Vae2le.ADT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydukienden.Text = "";
            }





        }

        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }
    }
}
