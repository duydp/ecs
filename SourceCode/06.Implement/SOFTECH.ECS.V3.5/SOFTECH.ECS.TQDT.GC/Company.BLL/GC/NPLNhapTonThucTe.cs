using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Transactions;


namespace Company.GC.BLL.GC
{
    public partial class NPLNhapTonThucTe
    {
        public void ChuyenHopDong(long idHopDong)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "UPDATE t_GC_NPLNhapTonThucTe SET ID_HopDong = " + idHopDong + " WHERE SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' AND NamDangKy = " + this.NamDangKy;
            DbCommand command = db.GetSqlStringCommand(sql);
            db.ExecuteNonQuery(command);
        }
        public void XoaDuLieuSai(long idHopDong)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "delete from t_GC_NPLNhapTonThucTe where ID_HopDong = "+idHopDong+" and SoToKhai not in(select SoTK from t_VNACCS_CapSoToKhai where SUBSTRING(CONVERT(varchar(12),SoTKVNACCS),1,1)='1') and MaLoaiHinh like '%V%'";
            DbCommand command = db.GetSqlStringCommand(sql);
            db.ExecuteNonQuery(command);
        }
        public static DataTable SelectNPLTonThucTeNgayDangKyNhoHonTKXuat(DateTime NgayDangKy, long HopDongID)
        {
            string sql = "select * from t_GC_NPLNhapTonThucTe where ID_HopDong=@ID_HopDong and NgayDangKy<=@NgayDangKy order by NgayDangKy,SoToKhai,MaLoaiHinh";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, HopDongID);
            if (NgayDangKy.Year == 1900)
                db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, DateTime.Today);
            else
                db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public static DataSet SelectBy_HopDongAndMaNPLAndTonHonO(long IDHopDong, string MaNPL, DateTime ngayDangKy)
        {
            string sql = "select * from t_GC_NPLNhapTonThucTe where ID_HopDong=@ID_HopDong and Ton>0 and ngaydangky<=@ngaydangky and MaNPL=@MaNPL  order by NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
            db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, IDHopDong);
            if (ngayDangKy.Year <= 1900) ngayDangKy = DateTime.Today;
            db.AddInParameter(dbCommand, "@ngaydangky", SqlDbType.DateTime, ngayDangKy);
            return db.ExecuteDataSet(dbCommand);
        }

        public static DataSet SelectBy_HopDongAndTonHonO(long IDHopDong, DateTime NgayDangKy, int SoThapPhanNPL)
        {
            string spName = "SELECT id,id_HopDong,SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, MaNPL, MaDoanhNghiep, Round(Luong,@SoThapPhanNPL) as Luong ,Round(Ton,@SoThapPhanNPL) as Ton, NgayDangKy,NgayHangVeKho " +
                            "from t_GC_NPLNhapTonThucTe " +
                            "where ID_HopDong=@ID_HopDong and NgayDangKy<=@NgayDangKy and Ton>0 " +
                            "order by NgayDangKy,SoToKhai,MaLoaiHinh,MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            if (NgayDangKy.Year <= 1900)
                NgayDangKy = DateTime.Today;
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);


            return db.ExecuteDataSet(dbCommand);
        }

        public static NPLNhapTonThucTe Load(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL, SqlTransaction transaction)
        {
            const string sql = "select * from t_GC_NPLNhapTonThucTe where SoToKhai=@SoToKhai AND MaLoaiHinh=@MaLoaiHinh and NamDangKy=@NamDangKy and MaHaiQuan=@MaHaiQuan and MaNPL=@MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namDangKy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, maNPL);

            NPLNhapTonThucTe entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                entity = new NPLNhapTonThucTe();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHangVeKho"))) entity.NgayHangVeKho = reader.GetDateTime(reader.GetOrdinal("NgayHangVeKho"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
            }
            reader.Close();
            return entity;
        }
        public static NPLNhapTonThucTe Load(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            const string sql = "select * from t_GC_NPLNhapTonThucTe where SoToKhai=@SoToKhai AND MaLoaiHinh=@MaLoaiHinh and NamDangKy=@NamDangKy and MaHaiQuan=@MaHaiQuan and MaNPL=@MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh.Trim());
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namDangKy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, maNPL);

            NPLNhapTonThucTe entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new NPLNhapTonThucTe();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHangVeKho"))) entity.NgayHangVeKho = reader.GetDateTime(reader.GetOrdinal("NgayHangVeKho"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
            }
            reader.Close();
            return entity;
        }
        public static NPLNhapTonThucTe Load(decimal soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            const string sql = "select * from t_GC_NPLNhapTonThucTe where SoToKhai= (select top 1 SoTK from t_VNACCS_CapSoToKhai where SoTKVNACCS = @SoToKhai) AND MaLoaiHinh=@MaLoaiHinh and NamDangKy=@NamDangKy and MaHaiQuan=@MaHaiQuan and MaNPL=@MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh.Trim());
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namDangKy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, maNPL);

            NPLNhapTonThucTe entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new NPLNhapTonThucTe();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHangVeKho"))) entity.NgayHangVeKho = reader.GetDateTime(reader.GetOrdinal("NgayHangVeKho"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ton"))) entity.Ton = reader.GetDecimal(reader.GetOrdinal("Ton"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.MuaVN = reader.GetInt32(reader.GetOrdinal("MuaVN"));
            }
            reader.Close();
            return entity;
        }
        public static DataTable SelectNPLTonThucTeByHopDong(long HopDongID)
        {
            //string sql = "SELECT v.*, w.TongTon AS TongTonTheoTK, w.TongTon - v.Ton AS ChenhLech from ( " +
            //    "SELECT temp1.*, temp2.Ten, temp2.DVT_ID, temp2.MaHS FROM ( " +
            //    "SELECT MaNPL as Ma, sum(Ton) as Ton,  sum(TonTriGiaKB) as TriGiaNTTon ,ID_HopDong as HopDong_ID  FROM " +
            //    "( " +
            //    "SELECT 	a.*, b.TenHang as TenNPL,b.MaHS,b.DVT_ID, b.TriGiaKB, a.Ton * b.TriGiaKB / a.Luong  as TonTriGiaKB " +
            //     "FROM (SELECT * FROM [dbo].[t_GC_NPLNhapTonThucTe] WHERE MaLoaiHinh LIKE 'N%')a " +
            //     "INNER JOIN v_GC_HangToKhai b ON a.SoToKhai = b.SoToKhai AND Year(a.NgayDangKy) = Year(b.NgayDangKY) AND a.MaLoaiHinh = b.MaLoaiHinh AND a.MaNPL = b.MaPhu AND a.Luong = b.SoLuong " +
            //    "UNION  " +
            //     "SELECT 	a.*, b.TenHang as TenNPL,b.MaHS,b.ID_DVT as DVT_ID, b.TriGia, a.Ton * b.TriGia / a.Luong  as TonTriGiaKB " +
            //     "FROM (SELECT * FROM [dbo].[t_GC_NPLNhapTonThucTe] WHERE MaLoaiHinh LIKE 'PH%' OR MaLoaiHinh LIKE 'N%' OR MaLoaiHinh = 'NPLCU')a " +
            //     "INNER JOIN v_GC_HangChuyenTiep b ON a.SoToKhai = b.SoToKhai AND Year(a.NgayDangKy) = Year(b.NgayDangKY) AND a.MaLoaiHinh = b.MaLoaiHinh AND a.MaNPL = b.MaHang AND a.Luong = b.SoLuong " +
            //    //minhnd Thêm cung ứng cho Phân bổ
            //     "UNION  " +
            //     "SELECT 	a.*, b.TenNPL as TenNPL,b.MaHS,b.DVT_ID as DVT_ID, 0 AS TriGia, 0  as TonTriGiaKB  " +
            //     "FROM (SELECT * FROM [dbo].[t_GC_NPLNhapTonThucTe] WHERE MaLoaiHinh LIKE 'NPL%' )a  " +
            //     "INNER JOIN t_KDT_GC_CungUng b ON a.SoToKhai = b.Master_ID AND a.MaLoaiHinh = 'NPLCU' AND a.MaNPL = b.MaNPL " +
            //    //minhnd Thêm cung ứng cho Phân bổ
            //    ")temp " +
            //    "Group by MaNPL,  ID_HopDong " +
            //    ")temp1 " +
            //    "INNER JOIN t_GC_NguyenPhuLieu temp2 ON temp1.Ma = temp2.Ma AND temp1.HopDong_ID = temp2.HopDong_ID " +
            //    ") AS v LEFT JOIN ( " +
            //    "SELECT sum(luong) AS TongLuong, SUM(ton) AS TongTon, MaNPL, ID_HopDong " +
            //    "from t_gc_nplnhaptonthucte " +
            //    "group by MaNPL, id_hopdong " +
            //    ") AS w ON v.HopDong_ID = w.ID_HopDong AND v.Ma = w.MaNPL " +
            //    "WHERE v.HopDong_ID = @HopDong_ID ";

            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            const string spName = "[dbo].[p_SelectNPLTonThucTeByHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.VarChar, HopDongID);
            return db.ExecuteDataSet(dbCommand).Tables[0];

        }

        public static void DeleteNPLTonByToKhai(int SoToKhai, short namdangky, string MaLoaiHinh, string MaHaiQuan, SqlTransaction transaction)
        {
            string sql = "delete from t_GC_NPLNhapTonThucTe where SoToKhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and NamDangKy=@NamDangKy";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namdangky);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.ExecuteNonQuery(dbCommand, transaction);
        }
        public static void UpdateNguyenPhuLieuTonThucTeByToKhai(Company.GC.BLL.KDT.ToKhaiMauDich TKMD)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        //if (TKMD.ID == 0)
                        //    TKMD.InsertTransaction(transaction);
                        //else
                        //    TKMD.UpdateTransaction(transaction);
                        foreach (Company.GC.BLL.KDT.HangMauDich item in TKMD.HMDCollection)
                        {
                            Company.GC.BLL.KDT.HangMauDich HMDcu = new Company.GC.BLL.KDT.HangMauDich();
                            HMDcu.ID = item.ID;
                            HMDcu.Load(transaction);
                            item.TKMD_ID = TKMD.ID;
                            //if (item.ID > 0)
                            //    item.UpdateTransaction(transaction);
                            //else
                            //    item.InsertTransaction(transaction);

                            NPLNhapTonThucTe NPLTon = NPLNhapTonThucTe.Load(TKMD.SoToKhai, TKMD.MaLoaiHinh, (short)TKMD.NgayDangKy.Year, TKMD.MaHaiQuan, HMDcu.MaPhu, transaction);
                            if (NPLTon == null)
                                NPLTon = new NPLNhapTonThucTe();
                            NPLTon.Luong = item.SoLuong;
                            NPLTon.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                            NPLTon.MaHaiQuan = TKMD.MaHaiQuan;
                            NPLTon.MaLoaiHinh = TKMD.MaLoaiHinh;
                            NPLTon.MaNPL = item.MaPhu;
                            NPLTon.NamDangKy = (short)TKMD.NgayDangKy.Year;
                            NPLTon.NgayDangKy = TKMD.NgayDangKy;
                            NPLTon.NgayHangVeKho = TKMD.NgayDangKy;
                            NPLTon.SoToKhai = TKMD.SoToKhai;
                            NPLTon.Ton = item.SoLuong;
                            NPLTon.ID_HopDong = TKMD.IDHopDong;
                            NPLTon.MuaVN = 0;
                            if (NPLTon.ID > 0)
                                NPLTon.Update(transaction);
                            else
                                NPLTon.Insert(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public static void UpdateNguyenPhuLieuTonThucTeByTKCT(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        //if (TKCT.ID == 0)
                        //    TKCT.InsertTransaction(transaction);
                        //else
                        //    TKCT.UpdateTransaction(transaction);
                        foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep item in TKCT.HCTCollection)
                        {
                            Company.GC.BLL.KDT.GC.HangChuyenTiep HCTcu = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
                            HCTcu.ID = item.ID;
                            HCTcu.Load(transaction);
                            item.Master_ID = TKCT.ID;
                            //if (item.ID > 0)
                            //    item.UpdateTransaction(transaction);
                            //else
                            //    item.InsertTransaction(transaction);

                            NPLNhapTonThucTe NPLTon = NPLNhapTonThucTe.Load(Convert.ToInt32(TKCT.SoToKhai), TKCT.MaLoaiHinh, (short)TKCT.NgayDangKy.Year, TKCT.MaHaiQuanTiepNhan, HCTcu.MaHang, transaction);
                            if (NPLTon == null)
                                NPLTon = new NPLNhapTonThucTe();
                            NPLTon.Luong = item.SoLuong;
                            NPLTon.MaDoanhNghiep = TKCT.MaDoanhNghiep;
                            NPLTon.MaHaiQuan = TKCT.MaHaiQuanTiepNhan;
                            NPLTon.MaLoaiHinh = TKCT.MaLoaiHinh;
                            NPLTon.MaNPL = item.MaHang;
                            NPLTon.NamDangKy = (short)TKCT.NgayDangKy.Year;
                            NPLTon.NgayDangKy = TKCT.NgayDangKy;
                            NPLTon.NgayHangVeKho = TKCT.NgayDangKy;
                            NPLTon.SoToKhai = Convert.ToInt32(TKCT.SoToKhai);
                            NPLTon.Ton = item.SoLuong;
                            NPLTon.ID_HopDong = TKCT.IDHopDong;
                            NPLTon.MuaVN = 0;
                            if (NPLTon.ID > 0)
                                NPLTon.Update(transaction);
                            else
                                NPLTon.Insert(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }


        public static void UpdateNguyenPhuLieuTonThucTeByToKhaiAndHMD(Company.GC.BLL.KDT.ToKhaiMauDich TKMD)
        {
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    foreach (Company.GC.BLL.KDT.HangMauDich item in TKMD.HMDCollection)
                    {
                        Company.GC.BLL.KDT.HangMauDich HMDcu = new Company.GC.BLL.KDT.HangMauDich();
                        HMDcu.ID = item.ID;
                        HMDcu.Load();
                        item.TKMD_ID = TKMD.ID;
                        //if (item.ID > 0)
                        //    item.UpdateTransaction(transaction);
                        //else
                        //    item.InsertTransaction(transaction);

                        NPLNhapTonThucTe NPLTon = NPLNhapTonThucTe.Load(TKMD.SoToKhai, TKMD.MaLoaiHinh, (short)TKMD.NgayDangKy.Year, TKMD.MaHaiQuan, HMDcu.MaPhu);
                        if (NPLTon == null)
                        {
                            NPLTon = new NPLNhapTonThucTe();
                            NPLTon.Ton = item.SoLuong;
                        }
                        else
                        {
                            if (HMDcu.SoLuong != item.SoLuong)
                            {
                                NPLTon.Ton = item.SoLuong;
                            }
                        }
                        NPLTon.Luong = item.SoLuong;
                        NPLTon.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                        NPLTon.MaHaiQuan = TKMD.MaHaiQuan;
                        NPLTon.MaLoaiHinh = TKMD.MaLoaiHinh;
                        NPLTon.MaNPL = item.MaPhu;
                        NPLTon.NamDangKy = (short)TKMD.NgayDangKy.Year;
                        NPLTon.NgayDangKy = TKMD.NgayDangKy;
                        NPLTon.NgayHangVeKho = TKMD.NgayDangKy;
                        NPLTon.SoToKhai = TKMD.SoToKhai;
                        NPLTon.ID_HopDong = TKMD.IDHopDong;
                        NPLTon.MuaVN = 0;
                        if (NPLTon.ID > 0)
                            NPLTon.Update();
                        else
                            NPLTon.Insert();
                    }
                    TKMD.InsertUpdateFullNoneTransaction();
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public decimal GetLuongTonDung(SqlTransaction transaction)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT LuongTonCuoi FROM t_GC_PhanBoToKhaiNhap WHERE MaNPL ='" + this.MaNPL + "' AND SoToKhaiNhap =" + this.SoToKhai +
                         " AND NamDangKyNhap =" + this.NamDangKy + " AND MaLoaiHinhNhap ='" + this.MaLoaiHinh + "' ORDER BY ID DESC";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            dbCommand.CommandTimeout = Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60;
            decimal temp = 0;
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand, transaction));
            }
            catch
            {
                temp = 0;
            }
            return temp;

        }

        /// <summary>
        /// Fix loi so to khai = 0 trong npl nhap ton thuc te.
        /// </summary>
        /// <param name="idHopDong"></param>
        public static void FixSoToKhaiZeroInTonThucTe(long idHopDong)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                string sql = "DELETE FROM t_GC_NPLNhapTonThucTe WHERE ID_HopDong = " + idHopDong + " AND SoToKhai = 0";
                DbCommand command = db.GetSqlStringCommand(sql);
                db.ExecuteNonQuery(command);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}