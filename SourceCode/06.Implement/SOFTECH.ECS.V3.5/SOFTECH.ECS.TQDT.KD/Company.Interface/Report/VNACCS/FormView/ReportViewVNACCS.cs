using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.Interface;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Logger;
using Company.KDT.SHARE.VNACCS;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.Report;
using Logger;
using Company.Interface.Report.SXXK;
using Company.KDT.SHARE.VNACCS.Messages.Recived;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS.Messages.Recived;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS.Messages.Recived;

#endif

namespace Company.Interface.Report
{
    public partial class ReportViewVNACCS : DevExpress.XtraEditors.XtraForm
    {

        #region Properties

        public Company.Interface.Report.TQDTToKhaiNK_TT196 ToKhaiChinhReport = new Company.Interface.Report.TQDTToKhaiNK_TT196();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196 PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196();
        //Tờ khai nhập khẩu tại chỗ
        public Company.Interface.Report.TQDTToKhaiNK_TaiCho_TT196 ToKhaiChinhTaiChoReport = new Company.Interface.Report.TQDTToKhaiNK_TaiCho_TT196();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196 PhuLucTaiChoReport = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196();
#if KD_V4 || KD_V3
        // Tờ khai trị giá
        public ToKhaiTriGiaA4 ToKhaiChinhTriGia = new ToKhaiTriGiaA4();
        public PhuLucTriGiaA4 PhuLucTKTriGia = new PhuLucTriGiaA4();
        private DataTable dtTKTG = new DataTable();
#endif

        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK PhuLucReportThueXNK = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue PhuLucReportMienThue = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB PhuLucReportThueTTDB = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB();
        public Company.Interface.Report.BangkeSoContainer_TT196_CV3742 PhuLucContainer = new Company.Interface.Report.BangkeSoContainer_TT196_CV3742();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        private bool InMaHang;
        private bool InTrigiaTT;
        private bool InThueBVMT;
        public int index = 0;
        //public LoaiPhuLuc LoaiPLCuaTK;
        public int SoLuongPhuLuc;
        private DevExpress.XtraPrinting.Native.ReflectorBar reflectorBar;
        private ProgressBarControl VisualProcessBar = new ProgressBarControl();
        private int soDongHang = 3;
        public List<ItemComboBoxRibbon> listBanIn = new List<ItemComboBoxRibbon>();
        private bool IsToKhaiNhapTaiCho { get; set; }
        string SoHoaDonGTGT = "";
        string NgayHoaDonGTGT = "";
        private bool IsThongQuan;
        public VAF8010 Vaf801;
        public VAF8020 Vaf802;
        public VAF8030 Vaf803;
        public VAF8040 Vaf804;
        public VAD4870 Vad487;//Dùng chung VAE474
        public VAL0870 Val087;
        public VAL0870_IVA Val087_IVA;
        public VAD8000 vad8000;
        public VAD8010 vad8010;
        public VAD8020 vad8020;
        public VAD8030 vad8030;
        public VAS5050 Vas5050;
        public VAS5030 Vas5030;
        public VAS501 Vas5010;
        public VAL0020 Val0020;
        public VAL0020 Val0040;
        public VAL2800 Val2800;
        public VAL2800 Val2900;
        public VAL8000 Val8000;
        public VAHP010 Vahp010;
        public VAHP030 Vahp030;
        public VAHP060 Vahp060;
        public VAGP010 Vagp010;
        public VAJP010 Vajp010;
        public VAGP110 Vagp110;
        public VAE8000 Vae8000;
        public KDT_VNACC_ToKhaiMauDich tkmd;
        public string manghiepvu;
        public decimal TongSoTrang;

        #endregion



        public ReportViewVNACCS(bool isBanThongQuan)
        {
            InitializeComponent();
            //chkInMaHang_edit.CheckedChanged += new EventHandler(chkInMaHang_edit_CheckedChanged);
            //chkInTriGiaTinhThue_edit.CheckedChanged += new EventHandler(chkInTriGiaTinhThue_edit_CheckedChanged);
            //chkInTriGiaTTBVMT_edit.CheckedChanged += new EventHandler(chkInTriGiaTTBVMT_edit_CheckedChanged);
            //InCV3742_Edit.CheckedChanged += new EventHandler(InCV3742_Edit_CheckedChanged);
            //InCV3742.EditValue = Company.KDT.SHARE.Components.Globals.InCV3742;
            reflectorBar = new DevExpress.XtraPrinting.Native.ReflectorBar(VisualProcessBar);
            //VisualProcessBar.PositionChanged += new EventHandler(VisualProcessBar_PositionChanged);
            ExportAll_Excel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ExportAll_Excel_ItemClick);
            ExportAll_Pdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(ExportAll_Pdf_ItemClick);
            //this.IsThongQuan = IsThongQuan;
            //IsToKhaiNhapTaiCho = isToKhaiNhapTaiCho;
            //PGThongTinToKhaiTaiCho.Visible = isToKhaiNhapTaiCho;

        }

        private void ReportViewBC03_TT117NewForm_Load(object sender, EventArgs e)
        {
            //listBanIn = new List<ItemComboBoxRibbon>();
            try
            {

                listBanIn.Add(new ItemComboBoxRibbon("-<CHỌN TẤT CẢ>-", ItemComboBoxRibbon.TypeItems.All));
                cmbToKhaiEdit.DataSource = listBanIn;
                cmbToKhaiEdit.DisplayMember = "Caption";
                cmbToKhaiEdit.PopulateColumns();
                cmbToKhaiEdit.Columns["ValueItem"].Visible = false;
                cmbToKhaiEdit.Columns["Type"].Visible = false;
                cmbToKhai.EditValue = listBanIn[0];
                this.WindowState = FormWindowState.Maximized;
                GetAllReport(manghiepvu);

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }


        #region Old
        //         private void cmbToKhai_EditValueChanged(object sender, EventArgs e)
        //         {
        //             try
        //             {
        //                 ItemComboBoxRibbon item = (ItemComboBoxRibbon)cmbToKhai.EditValue;
        //                 if (item == null) return;
        //                 if (item.Type == ItemComboBoxRibbon.TypeItems.TK)
        //                 {
        //                     #region Khanhhn - 28/05/2013 chuyển thành Hàm GetTKChinh
        //                     //this.ToKhaiChinhReport.TKMD = this.TKMD;
        //                     ////this.ToKhaiChinhReport.report = this;
        //                     //this.ToKhaiChinhReport.inTriGiaTT = InTrigiaTT;
        //                     //this.ToKhaiChinhReport.inThueBVMT = InThueBVMT;
        //                     //this.ToKhaiChinhReport.InMaHang = InMaHang;
        //                     //this.ToKhaiChinhReport.inThueBVMT = InThueBVMT;
        //                     //this.ToKhaiChinhReport.SoLuongPhuLuc = SoLuongPhuLuc.ToString("N0");
        //                     //this.ToKhaiChinhReport.BindReport();
        //                     #endregion
        //                     if (IsToKhaiNhapTaiCho)
        //                     {
        //                         this.ToKhaiChinhTaiChoReport = (Company.Interface.Report.TQDTToKhaiNK_TaiCho_TT196) GetTKChinh();
        //                         printControl1.PrintingSystem = ToKhaiChinhTaiChoReport.PrintingSystem;
        //                         this.ToKhaiChinhTaiChoReport.CreateDocument(true);
        //                     }
        //                     else
        //                     {
        //                         this.ToKhaiChinhReport =  (Company.Interface.Report.TQDTToKhaiNK_TT196) GetTKChinh();
        //                         printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
        //                         this.ToKhaiChinhReport.CreateDocument(true);
        //                     }
        //                 }
        //                 else if (item.Type == ItemComboBoxRibbon.TypeItems.PL)
        //                 {
        //                     try
        //                     {
        //                         index = (int)item.ValueItem;
        //                     }
        //                     catch { index = 1; }
        //                     if (LoaiPLCuaTK == LoaiPhuLuc.TT196)
        //                     {
        //                         #region 
        //                         //List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
        //                         //// chia hang theo phu luc
        //                         //int begin = (index - 1) * 3; //Comment by Hungtq
        //                         //int end = index * 3; //Comment by Hungtq
        // 
        //                         //if (begin <= this.TKMD.HMDCollection.Count - 1)
        //                         //{
        //                         //    if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
        //                         //    for (int i = begin; i < end; i++)
        //                         //        HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
        //                         //}
        //                         //if (TKMD.HMDCollection.Count == 1)
        //                         //    HMDReportCollection = null;
        //                         ////chia container theo phu luc
        //                         //List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        //                         //int beginContainer = (index) * 4 + 1;
        //                         //int endContainer = (index + 1) * 4;
        //                         ////edit by KhanhHn - fix l?i v?n don null
        //                         //if (TKMD.VanTaiDon != null)
        //                         //{
        //                         //    if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count)
        //                         //    {
        //                         //        if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
        //                         //            endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
        //                         //        for (int j = beginContainer - 1; j < endContainer; j++)
        //                         //            ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
        //                         //    }
        //                         //}
        //                         //this.PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196();
        //                         ////this.PhuLucReport.report = this;
        //                         //this.PhuLucReport.TKMD = this.TKMD;
        //                         //if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
        //                         //    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
        //                         //this.PhuLucReport.HMDCollection = HMDReportCollection;
        //                         //this.PhuLucReport.ContCollection = ContainerReportCo;
        //                         //this.PhuLucReport.SoDongHang = soDongHang;
        //                         //this.PhuLucReport.Phulucso = index;
        //                         //this.PhuLucReport.InMaHang = InMaHang;
        //                         //this.PhuLucReport.inThueBVMT = InThueBVMT;
        //                         //this.PhuLucReport.inTriGiaTT = InTrigiaTT;
        //                         //this.PhuLucReport.inThueBVMT = InThueBVMT;
        //                         //this.PhuLucReport.BindReport(index.ToString());
        //                         #endregion
        //                         if (IsToKhaiNhapTaiCho)
        //                         {
        //                             this.PhuLucTaiChoReport = (Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196) GetPL_TT196(index);
        //                             printControl1.PrintingSystem = PhuLucTaiChoReport.PrintingSystem;
        //                             this.PhuLucTaiChoReport.CreateDocument(true);
        //                         }
        //                         else
        //                         {
        //                             this.PhuLucReport = (Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196) GetPL_TT196(index);
        //                             printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
        //                             this.PhuLucReport.CreateDocument(true);
        //                         }
        //                     }
        //                     else
        //                     {
        //                         switch (LoaiPLCuaTK)
        //                         {
        //                             case LoaiPhuLuc.Full:
        //                                 {
        //                                     PhuLucReportThueTTDB = InPhuLucCoThueTTDB_CV3742((int)item.ValueItem);
        //                                     printControl1.PrintingSystem = PhuLucReportThueTTDB.PrintingSystem;
        //                                     this.PhuLucReportThueTTDB.CreateDocument(true);
        //                                 }
        //                                 break;
        //                             case LoaiPhuLuc.MienThue_TatCa:
        //                                 {
        //                                     this.PhuLucReportMienThue = InPhuLucMienThue_CV3742((int)item.ValueItem);
        //                                     printControl1.PrintingSystem = PhuLucReportMienThue.PrintingSystem;
        //                                     this.PhuLucReportMienThue.CreateDocument(true);
        //                                 }
        //                                 break;
        //                             case LoaiPhuLuc.MienThue_TTDB_Va_BVMT:
        //                                 {
        //                                     this.PhuLucReportThueXNK = InPhuLucCoThueXNK_CV3742((int)item.ValueItem);
        //                                     printControl1.PrintingSystem = PhuLucReportThueXNK.PrintingSystem;
        //                                     this.PhuLucReportThueXNK.CreateDocument(true);
        //                                 }
        //                                 break;
        //                             default:
        //                                 break;
        //                         }
        //                     }
        //                 }
        //                 else if (item.Type == ItemComboBoxRibbon.TypeItems.Cont)
        //                 {
        //                     int indexPLCont = 1;
        //                     try
        //                     {
        //                         indexPLCont = Convert.ToInt16(item.ValueItem);
        //                     }
        //                     catch (System.Exception ex)
        //                     {
        //                         indexPLCont = 1;
        //                         Logger.LocalLogger.Instance().WriteMessage(ex);
        //                     }
        //                     this.PhuLucContainer = GetContainer_3742(indexPLCont, Convert.ToInt16(item.Caption.Remove(0, 18)));
        //                     printControl1.PrintingSystem = PhuLucContainer.PrintingSystem;
        //                     this.PhuLucContainer.CreateDocument(true);
        //                 }
        //                 else if (item.Type == ItemComboBoxRibbon.TypeItems.BangKeCont)
        //                 {
        //                     Company.Interface.Report.SXXK.BangkeSoContainer_TT196 f = new Company.Interface.Report.SXXK.BangkeSoContainer_TT196();
        //                     f.tkmd = TKMD;
        //                     f.BindReport();
        //                     printControl1.PrintingSystem = f.PrintingSystem;
        //                     f.PrintingSystem.ProgressReflector = reflectorBar;
        //                     f.CreateDocument(true);
        //                 }
        //                 else if(item.Type == ItemComboBoxRibbon.TypeItems.All)
        //                 {
        //                     GetAllReport();
        //                     
        //                 }
        // #if KD_V4 || KD_V3
        //                 else if (item.Type == ItemComboBoxRibbon.TypeItems.TKTG)
        //                 {
        //                     ToKhaiChinhTriGia = GetToKhaiTriGia(TKMD.TKTGCollection[0]);
        //                     ToKhaiChinhTriGia.CreateDocument();
        //                     printControl1.PrintingSystem = ToKhaiChinhTriGia.PrintingSystem;
        //                 }
        //                 else if (item.Type == ItemComboBoxRibbon.TypeItems.PLTKTG)
        //                 {
        //                     PhuLucTKTriGia = GetPhuLucTKTG(TKMD.TKTGCollection[0]);
        //                     PhuLucTKTriGia.CreateDocument();
        //                     printControl1.PrintingSystem = PhuLucTKTriGia.PrintingSystem;
        //                 }
        // #endif
        //                 printControl1.PrintingSystem.ProgressReflector = reflectorBar;
        //                 btnExportAll.Enabled = item.Type == ItemComboBoxRibbon.TypeItems.All;
        //                 btnExportSingle.Enabled = !btnExportAll.Enabled;
        //             }
        //             catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        //             finally
        //             {
        //                 if (this.ToKhaiChinhReport != null) this.ToKhaiChinhReport.PrintingSystem.ResetProgressReflector();
        //                 else if (this.PhuLucReport != null) this.PhuLucReport.PrintingSystem.ResetProgressReflector();
        //                 else if (this.PhuLucReportMienThue != null) this.PhuLucReportMienThue.PrintingSystem.ResetProgressReflector();
        //                 else if (this.PhuLucReportThueTTDB != null) this.PhuLucReportThueTTDB.PrintingSystem.ResetProgressReflector();
        //                 else if (this.PhuLucReportThueXNK != null) this.PhuLucReportThueXNK.PrintingSystem.ResetProgressReflector();
        //                 else if (this.PhuLucContainer != null) this.PhuLucContainer.PrintingSystem.ResetProgressReflector();
        //             }
        //             
        //         }
        // 
        //         public LoaiPhuLuc KiemTraHang(ToKhaiMauDich TKMD)
        //         {
        //             try
        //             {
        //                 LoaiPhuLuc temp = LoaiPhuLuc.MienThue_TatCa;
        //                 foreach (HangMauDich hmd in TKMD.HMDCollection)
        //                 {
        //                     if (hmd.ThueBVMT > 0 || hmd.ThueTTDB > 0)
        //                         return LoaiPhuLuc.Full;
        //                     else if (hmd.ThueSuatXNK > 0 || hmd.ThueGTGT > 0)
        //                         temp = LoaiPhuLuc.MienThue_TTDB_Va_BVMT;
        //                 }
        //                 return temp;
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 LocalLogger.Instance().WriteMessage(ex);
        //                 return LoaiPhuLuc.Full;
        //             }
        //         }




        // 
        //         #region Envent loading
        // 
        // 
        //         void VisualProcessBar_PositionChanged(object sender, EventArgs e)
        //         {
        //             progressBarEditItem1.EditValue = VisualProcessBar.Position;
        //             if (VisualProcessBar.Position == 100 || VisualProcessBar.Position == 0)
        //                 progressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
        //             else
        //                 progressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
        //             
        //         }
        // #endregion
        // 
        //         #region Event check box
        //  void chkInTriGiaTTBVMT_edit_CheckedChanged(object sender, EventArgs e)
        //         {
        //             CheckEdit editCheck = (CheckEdit)sender;
        //             InThueBVMT = editCheck.Checked;
        //             cmbToKhai_EditValueChanged(null, null);
        //         }
        //         void chkInTriGiaTinhThue_edit_CheckedChanged(object sender, EventArgs e)
        //         {
        //             CheckEdit editCheck = (CheckEdit)sender;
        //             InTrigiaTT = editCheck.Checked;
        //             cmbToKhai_EditValueChanged(null, null);
        //         }
        //         void chkInMaHang_edit_CheckedChanged(object sender, EventArgs e)
        //         {
        //             CheckEdit editCheck = (CheckEdit)sender;
        //             InMaHang = editCheck.Checked;
        //             cmbToKhai_EditValueChanged(null, null);
        //         }
        //         void InCV3742_Edit_CheckedChanged(object sender, EventArgs e)
        //         {
        //             CheckEdit edit = (CheckEdit)sender;
        //             Company.KDT.SHARE.Components.Globals.InCV3742 = edit.Checked;
        //             listBanIn = new List<ItemComboBoxRibbon>();
        //             ReportViewBC03_TT117NewForm_Load(null, null);
        //             
        //         }
        //       
        //  #endregion

        // 
        // 
        //         #region generate report
        //         private object GetTKChinh()
        //         {
        //             Company.Interface.Report.TQDTToKhaiNK_TT196 report;
        //             Company.Interface.Report.TQDTToKhaiNK_TaiCho_TT196 reportTaiCho;
        //             try
        //             {
        //                 if (IsToKhaiNhapTaiCho)
        //                 {
        //                     reportTaiCho = new TQDTToKhaiNK_TaiCho_TT196();
        //                     reportTaiCho.TKMD = this.TKMD;
        //                     //this.ToKhaiChinhReport.report = this;
        //                     reportTaiCho.inTriGiaTT = InTrigiaTT;
        //                     reportTaiCho.inThueBVMT = InThueBVMT;
        //                     reportTaiCho.InMaHang = InMaHang;
        //                     reportTaiCho.inThueBVMT = InThueBVMT;
        //                     reportTaiCho.SoLuongPhuLucTK = SoLuongPhuLuc.ToString("N0");
        //                     reportTaiCho.SoHoaDonGTGT = SoHoaDonGTGT;
        //                     reportTaiCho.NgayHoaDonGTGT = NgayHoaDonGTGT;
        //                     reportTaiCho.BindReport();
        //                     reportTaiCho.PrintingSystem.ProgressReflector = reflectorBar;
        //                     return reportTaiCho;
        //                 }
        //                 else
        //                 {
        //                     report = new TQDTToKhaiNK_TT196();
        //                     report.TKMD = this.TKMD;
        //                     //this.ToKhaiChinhReport.report = this;
        //                     report.inTriGiaTT = InTrigiaTT;
        //                     report.inThueBVMT = InThueBVMT;
        //                     report.InMaHang = InMaHang;
        //                     report.inThueBVMT = InThueBVMT;
        //                     report.SoLuongPhuLuc = SoLuongPhuLuc.ToString("N0");
        //                     report.BindReport();
        //                     report.PrintingSystem.ProgressReflector = reflectorBar;
        //                     return report;
        //                 }
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 Logger.LocalLogger.Instance().WriteMessage(ex);
        //                 return null;
        //             }
        // 
        //         }
        //         private object GetPL_TT196(int _index)
        //         {
        //             Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196 PLTT196;
        //             Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196 PLTT196_TaiCho;
        //             try
        //             {
        //                 List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
        //                 // chia hang theo phu luc
        //                 int begin = (_index - 1) * 3; //Comment by Hungtq
        //                 int end = _index * 3; //Comment by Hungtq
        // 
        //                 if (begin <= this.TKMD.HMDCollection.Count - 1)
        //                 {
        //                     if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
        //                     for (int i = begin; i < end; i++)
        //                         HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
        //                 }
        //                 if (TKMD.HMDCollection.Count == 1)
        //                     HMDReportCollection = null;
        //                 //chia container theo phu luc
        //                 List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        //                 int beginContainer = (_index) * 4 + 1;
        //                 int endContainer = (_index + 1) * 4;
        //                 //edit by KhanhHn - fix l?i v?n don null
        //                 if (TKMD.VanTaiDon != null)
        //                 {
        //                     if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count)
        //                     {
        //                         if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
        //                             endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
        //                         for (int j = beginContainer - 1; j < endContainer; j++)
        //                             ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
        //                     }
        //                 }
        //                 if (IsToKhaiNhapTaiCho)
        //                 {
        //                     PLTT196_TaiCho = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196();
        //                     PLTT196_TaiCho.TKMD = this.TKMD;
        //                     if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
        //                         PLTT196_TaiCho.NgayDangKy = this.TKMD.NgayDangKy;
        //                     PLTT196_TaiCho.HMDCollection = HMDReportCollection;
        //                     PLTT196_TaiCho.ContCollection = ContainerReportCo;
        //                     PLTT196_TaiCho.SoDongHang = soDongHang;
        //                     PLTT196_TaiCho.Phulucso = _index;
        //                     PLTT196_TaiCho.InMaHang = InMaHang;
        //                     PLTT196_TaiCho.inThueBVMT = InThueBVMT;
        //                     PLTT196_TaiCho.inTriGiaTT = InTrigiaTT;
        //                     PLTT196_TaiCho.inThueBVMT = InThueBVMT;
        //                     PLTT196_TaiCho.BindReport(_index.ToString());
        //                     PLTT196_TaiCho.PrintingSystem.ProgressReflector = reflectorBar;
        //                     return PLTT196_TaiCho;
        //                 }
        //                 else
        //                 {
        //                     PLTT196 = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196();
        //                     PLTT196.TKMD = this.TKMD;
        //                     if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
        //                         PLTT196.NgayDangKy = this.TKMD.NgayDangKy;
        //                     PLTT196.HMDCollection = HMDReportCollection;
        //                     PLTT196.ContCollection = ContainerReportCo;
        //                     PLTT196.SoDongHang = soDongHang;
        //                     PLTT196.Phulucso = _index;
        //                     PLTT196.InMaHang = InMaHang;
        //                     PLTT196.inThueBVMT = InThueBVMT;
        //                     PLTT196.inTriGiaTT = InTrigiaTT;
        //                     PLTT196.inThueBVMT = InThueBVMT;
        //                     PLTT196.BindReport(_index.ToString());
        //                     PLTT196.PrintingSystem.ProgressReflector = reflectorBar;
        //                     return PLTT196;
        //                 }
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 LocalLogger.Instance().WriteMessage(ex);
        //                 return null;
        //             }
        //         }
        //         private Company.Interface.Report.BangkeSoContainer_TT196_CV3742 GetContainer_3742(int _indexPLCont, int soPhuLuc)
        //         {
        //             Company.Interface.Report.BangkeSoContainer_TT196_CV3742 _PhuLucContainer = new Company.Interface.Report.BangkeSoContainer_TT196_CV3742();
        //             try
        //             {
        //            
        //             int beginContainer = (_indexPLCont - 1) * 20 + 1;
        //             int endContainer = _indexPLCont * 20;
        //             if (endContainer > TKMD.VanTaiDon.ContainerCollection.Count) endContainer = TKMD.VanTaiDon.ContainerCollection.Count;
        //             List<Company.KDT.SHARE.QuanLyChungTu.Container> listCont = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        //             for (int i = beginContainer - 1; i < endContainer; i++)
        //             {
        //                 listCont.Add( new Company.KDT.SHARE.QuanLyChungTu.Container()
        //                 {
        //                     SoHieu = TKMD.VanTaiDon.ContainerCollection[i].SoHieu,
        //                     SoKien = TKMD.VanTaiDon.ContainerCollection[i].SoKien,
        //                     Seal_No = TKMD.VanTaiDon.ContainerCollection[i].Seal_No,
        //                     TrongLuong = TKMD.VanTaiDon.ContainerCollection[i].TrongLuong,
        //                     TrongLuongNet = TKMD.VanTaiDon.ContainerCollection[i].TrongLuongNet
        //                 });
        //             }
        //             _PhuLucContainer = new BangkeSoContainer_TT196_CV3742();
        //             //this.PhuLucContainer.report = this;
        //             _PhuLucContainer.Phulucso = soPhuLuc;
        //             _PhuLucContainer.indexPL = _indexPLCont;
        //             _PhuLucContainer.TKMD = TKMD;
        //             _PhuLucContainer.ContainerCollection = listCont;
        //             _PhuLucContainer.IsToKhaiTaiCho = IsToKhaiNhapTaiCho;
        //             _PhuLucContainer.BindReport();
        //             _PhuLucContainer.PrintingSystem.ProgressReflector = reflectorBar;    
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 LocalLogger.Instance().WriteMessage(ex);
        //             }
        //             return _PhuLucContainer;
        //         }
        //         private Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK InPhuLucCoThueXNK_CV3742(int IndexPL)
        //         {
        //             Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK PhuLucThueXNK = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK();
        //             try
        //             {
        //             
        //             List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
        //             // chia hang theo phu luc
        // 
        // 
        //             int begin = (IndexPL - 1) * 4;
        //             int end = IndexPL * 4;
        // 
        //             if (begin <= this.TKMD.HMDCollection.Count - 1)
        //             {
        //                 if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
        //                 for (int i = begin; i < end; i++)
        //                     HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
        //             }
        //             if (TKMD.HMDCollection.Count == 1)
        //                 HMDReportCollection = null;
        //             #region 
        //  //chia container theo phu luc
        //             //             List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        //             //             int beginContainer = (cboToKhai.SelectedIndex) * 4 + 1;
        //             //             int endContainer = (cboToKhai.SelectedIndex + 1) * 4;
        //             //edit by KhanhHn - fix l?i v?n don null
        //             //             if (TKMD.VanTaiDon != null)
        //             //             {
        //             //                 if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count)
        //             //                 {
        //             //                     if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
        //             //                         endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
        //             //                     for (int j = beginContainer - 1; j < endContainer; j++)
        //             //                         ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
        //             //                 }
        //             //             }
        //             //this.PhuLucReportThueXNK = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK();
        //             //this.PhuLucReportThueXNK.report = this;
        //  #endregion
        // 
        //             PhuLucThueXNK.TKMD = this.TKMD;
        //             if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
        //                 PhuLucThueXNK.NgayDangKy = this.TKMD.NgayDangKy;
        //             PhuLucThueXNK.HMDCollection = HMDReportCollection;
        //             //this.PhuLucReportThueXNK.ContCollection = ContainerReportCo;
        //             PhuLucThueXNK.SoDongHang = soDongHang;
        //             PhuLucThueXNK.Phulucso = IndexPL;
        //             PhuLucThueXNK.InMaHang = InMaHang;
        //             PhuLucThueXNK.inThueBVMT = InThueBVMT;
        //             PhuLucThueXNK.inTriGiaTT = InTrigiaTT;
        //             PhuLucThueXNK.IsToKhaiTaiCho = IsToKhaiNhapTaiCho;
        //             PhuLucThueXNK.BindReport(index.ToString());
        //             PhuLucThueXNK.PrintingSystem.ProgressReflector = reflectorBar;
        //       
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 LocalLogger.Instance().WriteMessage(ex);
        //             }
        //             return PhuLucThueXNK;
        //         }
        //         private Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue InPhuLucMienThue_CV3742(int IndexPL)
        //         {
        //             Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue PhuLucMienThue = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue();
        //             try
        //             {
        //             
        //             List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
        //             // chia hang theo phu luc
        // 
        // 
        //             int begin = (IndexPL - 1) * 20;
        //             int end = IndexPL * 20;
        // 
        //             if (begin <= this.TKMD.HMDCollection.Count - 1)
        //             {
        //                 if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
        //                 for (int i = begin; i < end; i++)
        //                     HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
        //             }
        //             if (TKMD.HMDCollection.Count == 1)
        //                 HMDReportCollection = null;
        //             //chia container theo phu luc
        //             List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        //             int beginContainer = (IndexPL - 1) * 4 + 1;
        //             int endContainer = (IndexPL) * 4;
        //             //edit by KhanhHn - fix l?i v?n don null
        //             if (TKMD.VanTaiDon != null)
        //             {
        //                 if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count)
        //                 {
        //                     if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
        //                         endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
        //                     for (int j = beginContainer - 1; j < endContainer; j++)
        //                         ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
        //                 }
        //             }
        //             //this.PhuLucReportMienThue = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue();
        //             //this.PhuLucReportMienThue.report = this;
        //             PhuLucMienThue.TKMD = this.TKMD;
        //             if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
        //                 PhuLucMienThue.NgayDangKy = this.TKMD.NgayDangKy;
        //             PhuLucMienThue.HMDCollection = HMDReportCollection;
        //             PhuLucMienThue.ContCollection = ContainerReportCo;
        //             PhuLucMienThue.SoDongHang = soDongHang;
        //             PhuLucMienThue.Phulucso = IndexPL;
        //             PhuLucMienThue.InMaHang = InMaHang;
        //             PhuLucMienThue.inThueBVMT = InThueBVMT;
        //             PhuLucMienThue.inTriGiaTT = InTrigiaTT;
        //             PhuLucMienThue.IsToKhaiTaiCho = IsToKhaiNhapTaiCho;
        //             PhuLucMienThue.BindReport(index.ToString());
        //             PhuLucMienThue.PrintingSystem.ProgressReflector = reflectorBar;
        //             
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 LocalLogger.Instance().WriteMessage(ex);
        //             }
        //             return PhuLucMienThue;
        //         }
        //         private Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB InPhuLucCoThueTTDB_CV3742(int IndexPL)
        //         {
        //             Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB PhuLucThueTTDB = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB();
        //             try
        //             {
        //           
        //             List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
        //             // chia hang theo phu luc
        // 
        // 
        //             int begin = (IndexPL - 1) * 4;
        //             int end = IndexPL * 4;
        // 
        //             if (begin <= this.TKMD.HMDCollection.Count - 1)
        //             {
        //                 if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
        //                 for (int i = begin; i < end; i++)
        //                     HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
        //             }
        //             if (TKMD.HMDCollection.Count == 1)
        //                 HMDReportCollection = null;
        //             #region 
        //  //chia container theo phu luc
        //             //             List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        //             //             int beginContainer = (cboToKhai.SelectedIndex) * 4 + 1;
        //             //             int endContainer = (cboToKhai.SelectedIndex + 1) * 4;
        //             //edit by KhanhHn - fix l?i v?n don null
        //             //             if (TKMD.VanTaiDon != null)
        //             //             {
        //             //                 if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count)
        //             //                 {
        //             //                     if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
        //             //                         endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
        //             //                     for (int j = beginContainer - 1; j < endContainer; j++)
        //             //                         ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
        //             //                 }
        //             //             }
        //             //PhuLucThueTTDB = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB();
        //             //this.PhuLucReportThueTTDB.report = this;
        //  #endregion
        //             PhuLucThueTTDB.TKMD = this.TKMD;
        //             if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
        //                 PhuLucThueTTDB.NgayDangKy = this.TKMD.NgayDangKy;
        //             PhuLucThueTTDB.HMDCollection = HMDReportCollection;
        //             /*this.PhuLucReportThueTTDB.ContCollection = ContainerReportCo;*/
        //             PhuLucThueTTDB.SoDongHang = soDongHang;
        //             PhuLucThueTTDB.Phulucso = IndexPL;
        //             PhuLucThueTTDB.InMaHang = InMaHang;
        //             PhuLucThueTTDB.inThueBVMT = InThueBVMT;
        //             PhuLucThueTTDB.inTriGiaTT = InTrigiaTT;
        //             PhuLucThueTTDB.IsToKhaiTaiCho = IsToKhaiNhapTaiCho;
        //             PhuLucThueTTDB.BindReport(index.ToString());
        //             PhuLucThueTTDB.PrintingSystem.ProgressReflector = reflectorBar;
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 LocalLogger.Instance().WriteMessage(ex);
        //             }
        //             return PhuLucThueTTDB;
        //         }
        // 
        // #if KD_V4 || KD_V3
        //         private ToKhaiTriGiaA4 GetToKhaiTriGia(ToKhaiTriGia TKTG)
        //         {
        //             try
        //             {
        //                 ToKhaiTriGiaA4 TKTGreport = new ToKhaiTriGiaA4();
        //                 TKTGreport.TKTG = TKTG;
        //                 TKTGreport.TKMD = this.TKMD;
        //                 TKTGreport.BindReport();
        //                 return TKTGreport;
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 Logger.LocalLogger.Instance().WriteMessage(ex);
        //                 return null;
        //             }
        //         }
        //         private PhuLucTriGiaA4 GetPhuLucTKTG(ToKhaiTriGia TKTG)
        //         {
        //             PhuLucTriGiaA4 PLTKTG = new PhuLucTriGiaA4();
        //             try
        //             {
        //                 HangTriGiaCollection HTGReportCollection = new HangTriGiaCollection();
        //                 //             int begin = (cboToKhai.SelectedIndex - 1) * 8;
        //                 //             int end = cboToKhai.SelectedIndex * 8;
        //                 //   if (end > this.TKTG.HTGCollection.Count) end = this.TKTG.HTGCollection.Count;
        //                 //    for (int i = begin; i < end; i++)
        //                 //       HTGReportCollection.Add(this.TKTG.HTGCollection[i]);
        //                 PLTKTG.dt = this.dtTKTG;
        //                 //PLTKTG.report = this;
        //                 PLTKTG.TKTG = TKTG;
        //                 PLTKTG.TKMD = this.TKMD;
        //                 //if(this.TKTG.NgayKhaiBao != new DateTime(1900,1,1))
        //                 //e  this.PhuLucReport.ng = this.TKTG.NgayKhaiBao;
        //                 PLTKTG.HTGCollection = HTGReportCollection;
        //                 PLTKTG.BindReport();
        //             }
        //             catch (System.Exception ex)
        //             {
        //                 LocalLogger.Instance().WriteMessage(ex);
        //             }
        //             return PLTKTG;
        //         }
        // #endif
        // 
        //         private void GetAllReport()
        //         {
        //             try
        //             {
        //                 
        //                 /*printControl1.PrintingSystem = new PrintingSystem();*/
        //                 printingSystem1 = new PrintingSystem();
        //                 printingSystem1.ContinuousPageNumbering = true;
        //                 printingSystem1.ProgressReflector = reflectorBar;
        //                 foreach (ItemComboBoxRibbon item in listBanIn)
        //                 {
        //                     if (item.Type == ItemComboBoxRibbon.TypeItems.TK)
        //                     {
        //                         if (IsToKhaiNhapTaiCho)
        //                         {
        //                             this.ToKhaiChinhTaiChoReport = (Company.Interface.Report.TQDTToKhaiNK_TaiCho_TT196) GetTKChinh();
        //                             this.ToKhaiChinhTaiChoReport.CreateDocument();
        //                             printingSystem1.Pages.AddRange(ToKhaiChinhTaiChoReport.Pages);
        //                         }
        //                         else
        //                         {
        // 
        //                             this.ToKhaiChinhReport = (Company.Interface.Report.TQDTToKhaiNK_TT196) GetTKChinh();
        //                             this.ToKhaiChinhReport.CreateDocument();
        //                             printingSystem1.Pages.AddRange(ToKhaiChinhReport.Pages);
        //                         }                        
        //                     }
        //                     else if (item.Type == ItemComboBoxRibbon.TypeItems.PL)
        //                     {
        //                         try
        //                         {
        //                             index = (int)item.ValueItem;
        //                         }
        //                         catch { index = 1; }
        //                         if (LoaiPLCuaTK == LoaiPhuLuc.TT196)
        //                         {
        //                             if (IsToKhaiNhapTaiCho)
        //                             {
        //                                 this.PhuLucTaiChoReport =  (TQDTPhuLucToKhaiNhap_TaiCho_TT196) GetPL_TT196(index);
        //                                 this.PhuLucTaiChoReport.CreateDocument();
        //                                 printingSystem1.Pages.AddRange(PhuLucTaiChoReport.Pages);
        //                             }
        //                             else
        //                             {
        //                                 this.PhuLucReport = (Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196) GetPL_TT196(index);
        //                                 this.PhuLucReport.CreateDocument();
        //                                 printingSystem1.Pages.AddRange(PhuLucReport.Pages);
        //                             }
        //                             
        //                         }
        //                         else
        //                         {
        //                             switch (LoaiPLCuaTK)
        //                             {
        //                                 case LoaiPhuLuc.Full:
        //                                     {
        //                                         PhuLucReportThueTTDB = InPhuLucCoThueTTDB_CV3742((int)item.ValueItem);
        //                                         this.PhuLucReportThueTTDB.CreateDocument();
        //                                         printingSystem1.Pages.AddRange(PhuLucReportThueTTDB.Pages);
        //                                     }
        //                                     break;
        //                                 case LoaiPhuLuc.MienThue_TatCa:
        //                                     {
        //                                         this.PhuLucReportMienThue = InPhuLucMienThue_CV3742((int)item.ValueItem);
        //                                         this.PhuLucReportMienThue.CreateDocument();
        //                                         printingSystem1.Pages.AddRange(PhuLucReportMienThue.Pages);
        //                                     }
        //                                     break;
        //                                 case LoaiPhuLuc.MienThue_TTDB_Va_BVMT:
        //                                     {
        //                                         this.PhuLucReportThueXNK = InPhuLucCoThueXNK_CV3742((int)item.ValueItem);
        //                                         this.PhuLucReportThueXNK.CreateDocument();
        //                                         printingSystem1.Pages.AddRange(PhuLucReportThueXNK.Pages);
        //                                     }
        //                                     break;
        //                                 default:
        //                                     break;
        //                             }
        //                         }
        //                     }
        //                     else if (item.Type == ItemComboBoxRibbon.TypeItems.Cont)
        //                     {
        //                         int indexPLCont = 1;
        //                         try
        //                         {
        //                             indexPLCont = Convert.ToInt16(item.ValueItem);
        //                         }
        //                         catch (System.Exception ex)
        //                         {
        //                             indexPLCont = 1;
        //                             Logger.LocalLogger.Instance().WriteMessage(ex);
        //                         }
        //                         this.PhuLucContainer = GetContainer_3742(indexPLCont, Convert.ToInt16(item.Caption.Remove(0, 18)));
        //                         this.PhuLucContainer.CreateDocument();
        //                         printingSystem1.Pages.AddRange(PhuLucContainer.Pages);
        //                     }
        //                     else if (item.Type == ItemComboBoxRibbon.TypeItems.BangKeCont)
        //                     {
        //                         Company.Interface.Report.SXXK.BangkeSoContainer_TT196 f = new Company.Interface.Report.SXXK.BangkeSoContainer_TT196();
        //                         f.tkmd = TKMD;
        //                         f.BindReport();
        //                         f.PrintingSystem.ProgressReflector = reflectorBar;
        //                         f.CreateDocument();
        //                         printingSystem1.Pages.AddRange(f.Pages);
        //                     }
        // #if KD_V4 || KD_V3
        //                     else if (item.Type == ItemComboBoxRibbon.TypeItems.TKTG)
        //                     {
        //                         ToKhaiChinhTriGia = GetToKhaiTriGia(TKMD.TKTGCollection[0]);
        //                         ToKhaiChinhTriGia.CreateDocument();
        //                         printingSystem1.Pages.AddRange(ToKhaiChinhTriGia.Pages);
        //                     }
        //                     else if (item.Type == ItemComboBoxRibbon.TypeItems.PLTKTG)
        //                     {
        //                         PhuLucTKTriGia = GetPhuLucTKTG(TKMD.TKTGCollection[0]);
        //                         PhuLucTKTriGia.CreateDocument();
        //                         printingSystem1.Pages.AddRange(PhuLucTKTriGia.Pages);
        //                     }
        // #endif
        //                 }
        //                 printControl1.PrintingSystem = printingSystem1;
        //             }
        //             catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        //         }
        // 
        //     
        // 
        // 
        // 
        // #endregion
        //  
        //         #region Enum 
        // 
        //         public enum LoaiPhuLuc
        //         {
        //             Full, // In theo CV 3742 nhung m?t trong các hàng hóa có thu? TTDB ho?c BVMT
        //             MienThue_TatCa, // In theo CV 3742 nhung t?t c? các hàng du?c mi?n thu?
        //             MienThue_TTDB_Va_BVMT, // In theo CV 3742 nhung t?t c? các hàng du?c mi?n thu? TTDB và BVMT
        //             TT196 // In theo TT 196
        //         }
        // #endregion
        // 
        //         private void btnInHoaDonGTGT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        //         {
        //             SoHoaDonGTGT = txtSoHoaDonGTGT.EditValue == null ? string.Empty : txtSoHoaDonGTGT.EditValue.ToString();
        //             NgayHoaDonGTGT = Convert.ToDateTime(txtNgayHoaDonGTGT.EditValue).ToString("dd/MM/yyyy");
        //             cmbToKhai_EditValueChanged(null, null);
        //         }
        // 
        //         private string convert(double value)
        //         {
        //             if (value > 0)
        //                 return value.ToString("N2");
        //             else
        //                 return "";
        //         }

        #endregion

        #region Export All
        void ExportAll_Pdf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "File PDF |*.pdf";
                save.CheckPathExists = true;
                if (Vaf801 != null)
                {
                    save.FileName = "ChungTuGhiSoThuePhaiThu_TKHQ_" + Vaf801.ICN.GetValue().ToString() + "";
                }
                else if (Vaf802 != null)
                {
                    save.FileName = "ThongBaoAnDinhThueHangHoaXNK_TKHQ_" + Vaf802.ICN.GetValue().ToString() + "";
                }
                else if (Vaf803 != null)
                {
                    save.FileName = "ChungTuGhiSoLePhiPhaiThu_TKHQ_" + Vaf803.ICN.GetValue().ToString() + "";
                }
                else if (Vaf804 != null)
                {
                    save.FileName = "ChungTuGhiSoLePhiPhaiThu_TKHQ_" + Vaf804.ICN.GetValue().ToString() + "";
                }
                else if (Vad487 != null)
                {
                    save.FileName = "PhanLoaiKiemTraThucTe_TKHQ_" + Vad487.K02.GetValue().ToString() + "";
                }
                else if (Val0020 != null)
                {
                    save.FileName = "BanLuuThongTinToKhaiPhoThong_TKHQ_" + Val0020.JNO.GetValue().ToString() + "";
                }
                else if (Val0040 != null)
                {
                    save.FileName = "BanLuuThongTinSuaDoiToKhaiPhoThong_TKHQ_" + Val0040.JNO.GetValue().ToString() + "";
                }
                else if (Val2800 != null)
                {
                    save.FileName = "ThongTinToKhaiPhoThong_TKHQ_" + Val2800.JNO.GetValue().ToString() + "";
                }
                else if (Val2900 != null)
                {
                    save.FileName = "ThongTinSuaDoiToKhaiPhoThong_TKHQ_" + Val2900.JNO.GetValue().ToString() + "";
                }
                else if (Vas5050 != null)
                {
                    save.FileName = "ThongBaoPheDuyetKhaiBaoVanChuyen_TKHQ_" + Vas5050.AE.GetValue().ToString().ToUpper() + "";
                }
                else if (Vas5030 != null)
                {
                    save.FileName = "BanSaoKhaiBaoVanChuyen_TKHQ_" + Vas5030.AE.GetValue().ToString().ToUpper() + "";
                }
                else if (Vas5010 != null)
                {
                    save.FileName = "BanSaoKhaiBaoVanChuyen_TKHQ_" + Vas5010.AE.GetValue().ToString().ToUpper() + "";
                }
                else if (vad8010 != null)
                {
                    save.FileName = "BanXacNhanKhaiBaoDMMienThueNK_" + vad8010.A01.GetValue().ToString().ToUpper() + "";
                }
                else if (vad8030 != null)
                {
                    save.FileName = "ToKhaiDanhMucMienThueNhapKhau_" + vad8030.A01.GetValue().ToString().ToUpper() + "";
                }
                else if (vad8020 != null)
                {
                    save.FileName = "BanXacNhanKhaiBaoDMMienThueNK_" + vad8020.A01.GetValue().ToString().ToUpper() + "";
                }
                else if (Vae8000 != null)
                {
                    save.FileName = "BanChiThiSuaDoiCuaHaiQuan_TKHQ_" + Vae8000.ECN.GetValue().ToString().ToUpper() + "";
                }
                else if (Vahp010 != null)
                {
                    save.FileName = "DonDeNghiCapGiayPhepXNKVatLieuNoCN_" + Vahp010.A08.GetValue().ToString().ToUpper() + "";
                }
                else if (Vahp030 != null)
                {
                    save.FileName = "QuyetDinhCapGiayPhepXNKVatLieuNoCN_" + Vahp030.A08.GetValue().ToString().ToUpper() + "";
                }
                else if (Vahp060 != null)
                {
                    save.FileName = "ThongBaoHuongDanCapPhepXNKVatLieuNoCN_" + Vahp060.APN.GetValue().ToString().ToUpper() + "";
                }
                else if (Vajp010 != null)
                {
                    save.FileName = "GiayDangKyKiemDichDongVatNK_" + Vajp010.ONO.GetValue().ToString().ToUpper() + "";
                }
                else if (Vagp110 != null)
                {
                    save.FileName = "GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_" + Vagp110.ONO.GetValue().ToString().ToUpper() + "";
                }
                else if (Vagp010 != null)
                {
                    save.FileName = "GiayDangKyKiemTraThucPhamNK_" + Vagp010.A08.GetValue().ToString().ToUpper() + "";
                }
                else if (Val087_IVA != null)
                {
                    save.FileName = "BanKhaiThongTinHoaDon_" + Val087_IVA.NIV.GetValue().ToString() + "";
                }
                else if (Val8000 != null)
                {
                    save.FileName = "BanXacNhanNoiDungKhaiSuaDoiBoSung_TKHQ_" + Val8000.A03.Value.ToString() + "";
                }
                else if (vad8000 != null)
                {
                    save.FileName = "BanChiThiSuaDoiCuaHaiQuan_TKHQ_" + vad8000.ICN.GetValue().ToString().ToUpper() + "";
                }
                else
                {

                }
                save.ShowDialog(this);
                if (!string.IsNullOrEmpty(save.FileName))
                {
                    PdfExportOptions pdfOp = new PdfExportOptions();
                    printControl1.PrintingSystem.ExportToPdf(save.FileName, pdfOp);
                    System.Diagnostics.Process.Start(save.FileName);
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        void ExportAll_Excel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string thongBao = "Luu ý: \r\n";
                thongBao += "          Doanh nghiệp chịu hoàn toàn trách nhiệm trước pháp luật khi thay đổi nội dung tờ khai trên file Excel.";
                if (new BaseForm().ShowMessage(thongBao, true) == "Yes")
                {
                    Company.KDT.SHARE.Components.Globals.SaveMessage("Xuat Excel", TKMD.ID, "Xu?t t? khai ra Excel", thongBao);
                    SaveFileDialog save = new SaveFileDialog();
                    save.Filter = "Excel 2007 -> 2013 |*.xlsx";
                    save.CheckPathExists = true;
                    if (Vaf801 != null)
                    {
                        save.FileName = "ChungTuGhiSoThuePhaiThu_TKHQ_" + Vaf801.ICN.GetValue().ToString() + "";
                    }
                    else if (Vaf802 != null)
                    {
                        save.FileName = "ThongBaoAnDinhThueHangHoaXNK_TKHQ_" + Vaf802.ICN.GetValue().ToString() + "";
                    }
                    else if (Vaf803 != null)
                    {
                        save.FileName = "ChungTuGhiSoLePhiPhaiThu_TKHQ_" + Vaf803.ICN.GetValue().ToString() + "";
                    }
                    else if (Vaf804 != null)
                    {
                        save.FileName = "ChungTuGhiSoLePhiPhaiThu_TKHQ_" + Vaf804.ICN.GetValue().ToString() + "";
                    }
                    else if (Vad487 != null)
                    {
                        save.FileName = "PhanLoaiKiemTraThucTe_TKHQ_" + Vad487.K02.GetValue().ToString() + "";
                    }
                    else if (Val0020 != null)
                    {
                        save.FileName = "BanLuuThongTinToKhaiPhoThong_TKHQ_" + Val0020.JNO.GetValue().ToString() + "";
                    }
                    else if (Val0040 != null)
                    {
                        save.FileName = "BanLuuThongTinSuaDoiToKhaiPhoThong_TKHQ_" + Val0040.JNO.GetValue().ToString() + "";
                    }
                    else if (Val2800 != null)
                    {
                        save.FileName = "ThongTinToKhaiPhoThong_TKHQ_" + Val2800.JNO.GetValue().ToString() + "";
                    }
                    else if (Val2900 != null)
                    {
                        save.FileName = "ThongTinSuaDoiToKhaiPhoThong_TKHQ_" + Val2900.JNO.GetValue().ToString() + "";
                    }
                    else if (Vas5050 != null)
                    {
                        save.FileName = "ThongBaoPheDuyetKhaiBaoVanChuyen_TKHQ_" + Vas5050.AE.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vas5030 != null)
                    {
                        save.FileName = "BanSaoKhaiBaoVanChuyen_TKHQ_" + Vas5030.AE.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vas5010 != null)
                    {
                        save.FileName = "BanSaoKhaiBaoVanChuyen_TKHQ_" + Vas5010.AE.GetValue().ToString().ToUpper() + "";
                    }
                    else if (vad8010 != null)
                    {
                        save.FileName = "BanXacNhanKhaiBaoDMMienThueNK_" + vad8010.A01.GetValue().ToString().ToUpper() + "";
                    }
                    else if (vad8030 != null)
                    {
                        save.FileName = "ToKhaiDanhMucMienThueNhapKhau_" + vad8030.A01.GetValue().ToString().ToUpper() + "";
                    }
                    else if (vad8020 != null)
                    {
                        save.FileName = "BanXacNhanKhaiBaoDMMienThueNK_" + vad8020.A01.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vae8000 != null)
                    {
                        save.FileName = "BanChiThiSuaDoiCuaHaiQuan_TKHQ_" + Vae8000.ECN.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vahp010 != null)
                    {
                        save.FileName = "DonDeNghiCapGiayPhepXNKVatLieuNoCN_" + Vahp010.A08.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vahp030 != null)
                    {
                        save.FileName = "QuyetDinhCapGiayPhepXNKVatLieuNoCN_" + Vahp030.A08.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vahp060 != null)
                    {
                        save.FileName = "ThongBaoHuongDanCapPhepXNKVatLieuNoCN_" + Vahp060.APN.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vajp010 != null)
                    {
                        save.FileName = "GiayDangKyKiemDichDongVatNK_" + Vajp010.ONO.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vagp110 != null)
                    {
                        save.FileName = "GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_" + Vagp110.ONO.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Vagp010 != null)
                    {
                        save.FileName = "GiayDangKyKiemTraThucPhamNK_" + Vagp010.A08.GetValue().ToString().ToUpper() + "";
                    }
                    else if (Val087_IVA != null)
                    {
                        save.FileName = "BanKhaiThongTinHoaDon_" + Val087_IVA.NIV.GetValue().ToString() + "";
                    }
                    else if (Val8000 != null)
                    {
                        save.FileName = "BanXacNhanNoiDungKhaiSuaDoiBoSung_TKHQ_" + Val8000.A03.Value.ToString() + "";
                    }
                    else if (vad8000 != null)
                    {
                        save.FileName = "BanChiThiSuaDoiCuaHaiQuan_TKHQ_" + vad8000.ICN.GetValue().ToString().ToUpper() + "";
                    }
                    else
                    {

                    }
                    save.ShowDialog(this);
                    if (!string.IsNullOrEmpty(save.FileName))
                    {
                        XlsxExportOptions xlsxOp = new XlsxExportOptions();
                        xlsxOp.ExportMode = XlsxExportMode.SingleFilePageByPage;
                        printControl1.PrintingSystem.ExportToXlsx(save.FileName, xlsxOp);

                        #region Đổi tên sheet
                        //try
                        //{

                        //                             Infragistics.Excel.Workbook wb = new Infragistics.Excel.Workbook();
                        //                             wb = Infragistics.Excel.Workbook.Load(save.FileName);
                        //                             for (int i = 0; i < wb.Worksheets.Count - 1; i++)
                        //                             {
                        //                                 wb.Worksheets[i].Name = listBanIn[i].Caption;
                        //                             }
                        //                             wb.Save(save.FileName);
                        //                         }
                        //                         catch (System.Exception ex)
                        //                         {
                        //                             Logger.LocalLogger.Instance().WriteMessage(ex);
                        //                         }

                        #endregion

                        System.Diagnostics.Process.Start(save.FileName);
                    }
                }

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        #endregion

        #region VNACCS
        private void GetAllReport(string IDMessage)
        {
            if (IDMessage == "VAF8010")
            {
                //printingSystem1.Pages.AddRange(PhuLucContainer.Pages);
                Company.Interface.Report.VNACCS.ChungTuGhiSoThuePhaiThu ChungTu = ChungTuGhiSoThue();
                ChungTu.BindReport(Vaf801);
                ChungTu.CreateDocument();
                printingSystem1.Pages.AddRange(ChungTu.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ChungTuGhiSoThuePhaiThu_TKHQ_" + Vaf801.ICN.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAF8020")
            {
                Company.Interface.Report.VNACCS.ThongBaoAnDinhThueHangHoaXNK report = ThongBaoAnDinhThue();
                report.BindReport(Vaf802);
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ThongBaoAnDinhThueHangHoaXNK_TKHQ_" + Vaf802.ICN.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAF8030")
            {
                Company.Interface.Report.VNACCS.ChungTuGhiSoLePhiPhaiThu report = ChungTuGhiSoLePhi();
                report.BindReport(Vaf803);
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ChungTuGhiSoLePhiPhaiThu_TKHQ_" + Vaf803.ICN.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAF8030")
            {
                Company.Interface.Report.VNACCS.ChungTuGhiSoLePhiPhaiThu_1 report = ChungTuGhiSoLePhi_1();
                report.BindReport(Vaf804);
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ChungTuGhiSoLePhiPhaiThu_TKHQ_" + Vaf804.ICN.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAD4870" || IDMessage == "VAE4740")
            {
                Company.Interface.Report.VNACCS.PhanLoaiKiemTraThucTe_VAD4870 report = PhanLoaiKiemTraThucTe();
                report.BindReport(Vad487);
                report.maNV = IDMessage;
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "PhanLoaiKiemTraThucTe_TKHQ_" + Vad487.K02.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAL0020")
            {
                Company.Interface.Report.VNACCS.BanLuuThongTinToKhaiPhoThong report = BanLuuThongTinToKhaiPhoThong();
                report.BindingReport(Val0020);
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanLuuThongTinToKhaiPhoThong_TKHQ_" + Val0020.JNO.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAL0040")
            {
                Company.Interface.Report.VNACCS.BanLuuThongTinSuaDoiToKhaiPhoThong report = BanLuuThongTinSuaDoiToKhaiPhoThong();
                report.BindingReport(Val0040);
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanLuuThongTinSuaDoiToKhaiPhoThong_TKHQ_" + Val0040.JNO.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAL2800")
            {
                Company.Interface.Report.VNACCS.ThongTinToKhaiPhoThong report = ThongTinToKhaiPhoThong();
                report.BindingReport(Val2800);
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ThongTinToKhaiPhoThong_TKHQ_" + Val2800.JNO.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAL2900")
            {
                Company.Interface.Report.VNACCS.ThongTinSuaDoiToKhaiPhoThong report = ThongTinSuaDoiToKhaiPhoThong();
                report.BindingReport(Val2900);
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ThongTinSuaDoiToKhaiPhoThong_TKHQ_" + Val2900.JNO.GetValue().ToString() + "";
            }
            //Tạm nhập - tái xuất
            else if (IDMessage == "VAL8110" || IDMessage == "VAL8130" || IDMessage == "VAL8140")
            {
                MessageBox.Show("Không có message", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Company.Interface.Report.VNACCS.ThongTinSuaDoiToKhaiPhoThong report = ThongTinSuaDoiToKhaiPhoThong();
                //report.BindingReport(Val2900);
                //report.CreateDocument();
                //printingSystem1.Pages.AddRange(report.Pages);
            }
            else if (IDMessage == "VAD4870")
            {
                Company.Interface.Report.VNACCS.PhanLoaiKiemTraThucTe_VAD4870 report = PhanLoaiKiemTraThucTe();
                report.BindReport(Vad487);
                report.CreateDocument();
                printingSystem1.Pages.AddRange(report.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "PhanLoaiKiemTraThucTe_TKHQ_" + Vad487.K02.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAS5040" || IDMessage == "VAS5050" || IDMessage == "VAS5060" || IDMessage == "VAS5070")
            {
                //for (int i = 0; i < 21; i++)
                //{
                //    VAS5050_HANG hang = new VAS5050_HANG();
                //    hang.DA.SetValue(i + 2);
                //    Vas5050.HangHoa.Add(hang);
                //}
                //int SoCont = Vas5050.HangHoa.Count;
                //decimal TongSoTrang;
                //if (SoCont <= 20)
                //    TongSoTrang = 5;
                //else
                //{
                //    TongSoTrang = 5 + Math.Round((decimal)SoCont / 20, 0, MidpointRounding.AwayFromZero);
                //}

                Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_1 TKVC1 = ThongBaoPheDuyetKhaiBaoVanChuyen1();
                TKVC1.BindingReport(Vas5050);
                TKVC1.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC1.Pages);
                Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_2 TKVC2 = ThongBaoPheDuyetKhaiBaoVanChuyen2();
                TKVC2.BindingReport(Vas5050);
                TKVC2.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC2.Pages);
                Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_4 TKVC4 = ThongBaoPheDuyetKhaiBaoVanChuyen4();
                TKVC4.BindingReport(Vas5050);
                TKVC4.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC4.Pages);
                Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_5 TKVC5 = ThongBaoPheDuyetKhaiBaoVanChuyen5();
                TKVC5.BindingReport(Vas5050);
                TKVC5.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC5.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ThongBaoPheDuyetKhaiBaoVanChuyen_TKHQ_" + Vas5050.AE.GetValue().ToString().ToUpper() + "";

            }
            else if (IDMessage == "VAS5030")
            {
                //for (int i = 0; i < 21; i++)
                //{
                //    VAS5050_HANG hang = new VAS5050_HANG();
                //    hang.DA.SetValue(i + 2);
                //    Vas5050.HangHoa.Add(hang);
                //}
                //int SoCont = Vas5050.HangHoa.Count;
                //decimal TongSoTrang;
                //if (SoCont <= 20)
                //    TongSoTrang = 5;
                //else
                //{
                //    TongSoTrang = 5 + Math.Round((decimal)SoCont / 20, 0, MidpointRounding.AwayFromZero);
                //}

                Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_1 TKVC1 = new Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_1();
                TKVC1.BindingReport(Vas5030);
                TKVC1.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC1.Pages);
                Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_2 TKVC2 = new Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_2();
                TKVC2.BindingReport(Vas5030);
                TKVC2.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC2.Pages);
                Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_4 TKVC4 = new Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_4();
                TKVC4.BindingReport(Vas5030);
                TKVC4.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC4.Pages);
                Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_5 TKVC5 = new Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_5();
                TKVC5.BindingReport(Vas5030);
                TKVC5.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC5.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanSaoKhaiBaoVanChuyen_TKHQ_" + Vas5030.AE.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAS5010")
            {
                //for (int i = 0; i < 21; i++)
                //{
                //    VAS5050_HANG hang = new VAS5050_HANG();
                //    hang.DA.SetValue(i + 2);
                //    Vas5050.HangHoa.Add(hang);
                //}
                //int SoCont = Vas5050.HangHoa.Count;
                //decimal TongSoTrang;
                //if (SoCont <= 20)
                //    TongSoTrang = 5;
                //else
                //{
                //    TongSoTrang = 5 + Math.Round((decimal)SoCont / 20, 0, MidpointRounding.AwayFromZero);
                //}

                Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_1 TKVC1 = new Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_1();
                TKVC1.BindingReport(Vas5010);
                TKVC1.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC1.Pages);
                Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_2 TKVC2 = new Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_2();
                TKVC2.BindingReport(Vas5010);
                TKVC2.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC2.Pages);
                Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_4 TKVC4 = new Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_4();
                TKVC4.BindingReport(Vas5010);
                TKVC4.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC4.Pages);
                Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_5 TKVC5 = new Company.Interface.Report.VNACCS.BanSaoKhaiBaoVanChuyen_5();
                TKVC5.BindingReport(Vas5010);
                TKVC5.CreateDocument();
                printingSystem1.Pages.AddRange(TKVC5.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanSaoKhaiBaoVanChuyen_TKHQ_" + Vas5010.AE.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAD8010" || IDMessage == "VAE8010")
            {
                // Tờ khai danh mục miễn thuế
                Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_1 DMMT1 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_1();
                DMMT1.BindingReport(vad8010, IDMessage);
                DMMT1.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT1.Pages);
                Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_2 DMMT2 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_2();
                DMMT2.BindingReport(vad8010, IDMessage);
                DMMT2.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT2.Pages);
                Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_3 DMMT3 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_3();
                DMMT3.BindingReport(vad8010, IDMessage);
                DMMT3.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT3.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanXacNhanKhaiBaoDMMienThueNK_" + vad8010.A01.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAD8030")
            {
                // Tờ khai danh mục miễn thuế nhập khẩu (đã đăng ký)
                Company.Interface.Report.VNACCS.ToKhaiDanhMucMienThueNhapKhau_1 DMMT1 = new Company.Interface.Report.VNACCS.ToKhaiDanhMucMienThueNhapKhau_1();
                DMMT1.BindingReport(vad8030, IDMessage);
                DMMT1.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT1.Pages);
                Company.Interface.Report.ToKhaiDanhMucMienThueNhapKhau_2 DMMT2 = new Company.Interface.Report.ToKhaiDanhMucMienThueNhapKhau_2();
                DMMT2.BindingReport(vad8030, IDMessage);
                DMMT2.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT2.Pages);
                Company.Interface.Report.ToKhaiDanhMucMienThueNhapKhau_3 DMMT3 = new Company.Interface.Report.ToKhaiDanhMucMienThueNhapKhau_3();
                DMMT3.BindingReport(vad8030, IDMessage);
                DMMT3.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT3.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ToKhaiDanhMucMienThueNhapKhau_" + vad8030.A01.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAD8020")
            {
                // Tờ khai sửa đổi danh mục miễn thuế nhập khẩu
                Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_1 DMMT1 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_1();
                DMMT1.BindingReport(vad8020, IDMessage);
                DMMT1.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT1.Pages);
                Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_2 DMMT2 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_2();
                DMMT2.BindingReport(vad8020, IDMessage);
                DMMT2.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT2.Pages);
                Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_3 DMMT3 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_3();
                DMMT3.BindingReport(vad8020, IDMessage);
                DMMT3.CreateDocument();
                printingSystem1.Pages.AddRange(DMMT3.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanXacNhanKhaiBaoDMMienThueNK_" + vad8020.A01.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAE8000")
            {
                Company.Interface.Report.VNACCS.BanChiThiSuaDoiCuaHaiQuan chithihaiquan = new Company.Interface.Report.VNACCS.BanChiThiSuaDoiCuaHaiQuan();
                chithihaiquan.BindingReportVAE8000(Vae8000);
                chithihaiquan.CreateDocument();
                printingSystem1.Pages.AddRange(chithihaiquan.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanChiThiSuaDoiCuaHaiQuan_TKHQ_" + Vae8000.ECN.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAHP010")
            {
                Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_1 GP1 = new Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_1();
                GP1.BindingReport(Vahp010);
                GP1.CreateDocument();
                printingSystem1.Pages.AddRange(GP1.Pages);
                Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_2 GP2 = new Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_2();
                GP2.BindingReport(Vahp010);
                GP2.CreateDocument();
                printingSystem1.Pages.AddRange(GP2.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "DonDeNghiCapGiayPhepXNKVatLieuNoCN_" + Vahp010.A08.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAHP030")
            {
                Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_1 GP1 = new Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_1();
                GP1.BindingReport(Vahp030);
                GP1.CreateDocument();
                printingSystem1.Pages.AddRange(GP1.Pages);
                Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_2 GP2 = new Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_2();
                GP2.BindingReport(Vahp030);
                GP2.CreateDocument();
                printingSystem1.Pages.AddRange(GP2.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "QuyetDinhCapGiayPhepXNKVatLieuNoCN_" + Vahp030.A08.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAHP060")
            {
                Company.Interface.Report.VNACCS.ThongBaoHuongDanCapPhepXNKVatLieuNoCN GP1 = new Company.Interface.Report.VNACCS.ThongBaoHuongDanCapPhepXNKVatLieuNoCN();
                GP1.BindingReport(Vahp060);
                GP1.CreateDocument();
                printingSystem1.Pages.AddRange(GP1.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "ThongBaoHuongDanCapPhepXNKVatLieuNoCN_" + Vahp060.APN.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAGP010")
            {
                Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_1 GP1 = new Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_1();
                GP1.BindingReport(Vagp010);
                GP1.CreateDocument();
                printingSystem1.Pages.AddRange(GP1.Pages);
                Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_2 GP2 = new Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_2();
                GP2.BindingReport(Vagp010);
                GP2.CreateDocument();
                printingSystem1.Pages.AddRange(GP2.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "GiayDangKyKiemTraThucPhamNK_" + Vagp010.A08.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAJP010")
            {
                Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_1 GP1 = new Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_1();
                GP1.BindingReport(Vajp010);
                GP1.CreateDocument();
                printingSystem1.Pages.AddRange(GP1.Pages);
                Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_2 GP2 = new Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_2();
                GP2.BindingReport(Vajp010);
                GP2.CreateDocument();
                printingSystem1.Pages.AddRange(GP2.Pages);
                Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_3 GP3 = new Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_3();
                GP3.BindingReport(Vajp010);
                GP3.CreateDocument();
                printingSystem1.Pages.AddRange(GP3.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "GiayDangKyKiemDichDongVatNK_" + Vajp010.ONO.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAGP110")
            {
                Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1 GP1 = new Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1();
                GP1.BindingReport(Vagp110);
                GP1.CreateDocument();
                printingSystem1.Pages.AddRange(GP1.Pages);
                Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2 GP2 = new Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2();
                GP2.BindingReport(Vagp110);
                GP2.CreateDocument();
                printingSystem1.Pages.AddRange(GP2.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_" + Vagp110.ONO.GetValue().ToString().ToUpper() + "";
            }
            else if (IDMessage == "VAL0870")
            {
                Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon BanKhaiHoaDon = BanKhai_HoaDon();
                BanKhaiHoaDon.BindReport(Val087_IVA);
                BanKhaiHoaDon.CreateDocument();
                printingSystem1.Pages.AddRange(BanKhaiHoaDon.Pages);
                Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon_1 BanKhaiHoaDon1 = BanKhai_HoaDon_1();
                BanKhaiHoaDon1.BindReport(Val087_IVA);
                BanKhaiHoaDon1.CreateDocument();
                printingSystem1.Pages.AddRange(BanKhaiHoaDon1.Pages);

                int spl = (Val087_IVA.HangHoaTrongHoaDon.Count - 2) / 4;
                int Mid = (Val087_IVA.HangHoaTrongHoaDon.Count - 2) % 4;
                if (Mid > 0)
                    spl = spl + 1;
                if (Val087_IVA.HangHoaTrongHoaDon.Count > 3)
                {
                    for (int i = 1; i <= spl; i++)
                    {

                        Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon_2 BanKhaiHoaDon2 = BanKhai_HoaDon_2();
                        BanKhaiHoaDon2.spl = i;
                        BanKhaiHoaDon2.BindReport(Val087_IVA);
                        BanKhaiHoaDon2.CreateDocument();
                        printingSystem1.Pages.AddRange(BanKhaiHoaDon2.Pages);
                    }
                }
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanKhaiThongTinHoaDon_" + Val087_IVA.NIV.GetValue().ToString() + "";
            }
            else if (IDMessage == "VAL8020" || IDMessage == "VAL8000")
            {
                Company.Interface.Report.VNACCS.BanXacNhanNoiDungKhaiSuaDoiBoSung_1 BoSung1 = XacNhanNoiDungKhaiSuaDoiBoSung();
                BoSung1.BindReport(Val8000, IDMessage);
                BoSung1.CreateDocument();
                printingSystem1.Pages.AddRange(BoSung1.Pages);
                Company.Interface.Report.VNACCS.BanXacNhanNoiDungKhaiSuaDoiBoSung_2 BoSung2 = XacNhanNoiDungKhaiSuaDoiBoSung2();
                //BoSung2.spl = i;
                BoSung2.BindReport(Val8000, IDMessage);
                BoSung2.CreateDocument();
                printingSystem1.Pages.AddRange(BoSung2.Pages);

                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanXacNhanNoiDungKhaiSuaDoiBoSung_TKHQ_" + Val8000.A03.Value.ToString() + "";
            }
            else if (IDMessage == "VAL8030" || IDMessage == "VAL8040" || IDMessage == "VAL8050" || IDMessage == "VAL8060")
            {
                Company.Interface.Report.VNACCS.QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1 BoSung1 = QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1();
                BoSung1.BindReport(Val8000, IDMessage);
                BoSung1.CreateDocument();
                printingSystem1.Pages.AddRange(BoSung1.Pages);
                //for (int i = 0; i <= Val8000.HangMD.Count; i++)
                //{
                Company.Interface.Report.VNACCS.QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2 BoSung2 = QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2();
                //BoSung2.spl = i;
                BoSung2.BindReport(Val8000, IDMessage);
                BoSung2.CreateDocument();
                printingSystem1.Pages.AddRange(BoSung2.Pages);
                //} 

                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_TKHQ_" + Val8000.A03.Value.ToString() + "";
            }
            else if (IDMessage == "VAD8000")
            {
                Company.Interface.Report.VNACCS.BanChiThiSuaDoiCuaHaiQuan chithihaiquan = new Company.Interface.Report.VNACCS.BanChiThiSuaDoiCuaHaiQuan();
                chithihaiquan.BindingReportVAD8000(vad8000);
                chithihaiquan.CreateDocument();
                printingSystem1.Pages.AddRange(chithihaiquan.Pages);
                printControl1.PrintingSystem.ExportOptions.PrintPreview.DefaultFileName = "BanChiThiSuaDoiCuaHaiQuan_TKHQ_" + vad8000.ICN.GetValue().ToString().ToUpper() + "";
            }
            printControl1.PrintingSystem = printingSystem1;
        }
        private Company.Interface.Report.VNACCS.ChungTuGhiSoThuePhaiThu ChungTuGhiSoThue()
        {
            Company.Interface.Report.VNACCS.ChungTuGhiSoThuePhaiThu ChungTuGhiSoThue = new Company.Interface.Report.VNACCS.ChungTuGhiSoThuePhaiThu();

            return ChungTuGhiSoThue;
        }
        private Company.Interface.Report.VNACCS.ChungTuGhiSoLePhiPhaiThu ChungTuGhiSoLePhi()
        {
            Company.Interface.Report.VNACCS.ChungTuGhiSoLePhiPhaiThu ChungTuGhiSoLePhi = new Company.Interface.Report.VNACCS.ChungTuGhiSoLePhiPhaiThu();

            return ChungTuGhiSoLePhi;
        }
        private Company.Interface.Report.VNACCS.ChungTuGhiSoLePhiPhaiThu_1 ChungTuGhiSoLePhi_1()
        {
            Company.Interface.Report.VNACCS.ChungTuGhiSoLePhiPhaiThu_1 ChungTuGhiSoLePhi_1 = new Company.Interface.Report.VNACCS.ChungTuGhiSoLePhiPhaiThu_1();

            return ChungTuGhiSoLePhi_1;
        }
        private Company.Interface.Report.VNACCS.ThongBaoAnDinhThueHangHoaXNK ThongBaoAnDinhThue()
        {
            Company.Interface.Report.VNACCS.ThongBaoAnDinhThueHangHoaXNK ThongBaoAnDinhThue = new Company.Interface.Report.VNACCS.ThongBaoAnDinhThueHangHoaXNK();

            return ThongBaoAnDinhThue;
        }
        private Company.Interface.Report.VNACCS.PhanLoaiKiemTraThucTe_VAD4870 PhanLoaiKiemTraThucTe()
        {
            Company.Interface.Report.VNACCS.PhanLoaiKiemTraThucTe_VAD4870 PhanLoaiKiemTraThucTe = new Company.Interface.Report.VNACCS.PhanLoaiKiemTraThucTe_VAD4870();

            return PhanLoaiKiemTraThucTe;
        }
        private Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon BanKhai_HoaDon()
        {
            Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon BanKhaiHoaDon = new Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon();

            return BanKhaiHoaDon;
        }
        private Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon_1 BanKhai_HoaDon_1()
        {
            Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon_1 BanKhaiHoaDon1 = new Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon_1();

            return BanKhaiHoaDon1;
        }
        private Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon_2 BanKhai_HoaDon_2()
        {
            Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon_2 BanKhaiHoaDon2 = new Company.Interface.Report.VNACCS.BanKhaiThongTinHoaDon_2();

            return BanKhaiHoaDon2;
        }
        private Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_1 ThongBaoPheDuyetKhaiBaoVanChuyen1()
        {
            Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_1 ThongBaoPheDuyetKhaiBaoVanChuyen1 = new Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_1();

            return ThongBaoPheDuyetKhaiBaoVanChuyen1;
        }
        private Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_2 ThongBaoPheDuyetKhaiBaoVanChuyen2()
        {
            Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_2 ThongBaoPheDuyetKhaiBaoVanChuyen2 = new Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_2();

            return ThongBaoPheDuyetKhaiBaoVanChuyen2;
        }
        private Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_4 ThongBaoPheDuyetKhaiBaoVanChuyen4()
        {
            Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_4 ThongBaoPheDuyetKhaiBaoVanChuyen4 = new Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_4();

            return ThongBaoPheDuyetKhaiBaoVanChuyen4;
        }
        private Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_5 ThongBaoPheDuyetKhaiBaoVanChuyen5()
        {
            Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_5 ThongBaoPheDuyetKhaiBaoVanChuyen5 = new Company.Interface.Report.VNACCS.ThongBaoPheDuyetKhaiBaoVanChuyen_5();

            return ThongBaoPheDuyetKhaiBaoVanChuyen5;
        }
        private Company.Interface.Report.VNACCS.BanLuuThongTinSuaDoiToKhaiPhoThong BanLuuThongTinSuaDoiToKhaiPhoThong()
        {
            Company.Interface.Report.VNACCS.BanLuuThongTinSuaDoiToKhaiPhoThong BanLuuThongTinSuaDoiToKhaiPhoThong = new Company.Interface.Report.VNACCS.BanLuuThongTinSuaDoiToKhaiPhoThong();

            return BanLuuThongTinSuaDoiToKhaiPhoThong;
        }
        private Company.Interface.Report.VNACCS.BanLuuThongTinToKhaiPhoThong BanLuuThongTinToKhaiPhoThong()
        {
            Company.Interface.Report.VNACCS.BanLuuThongTinToKhaiPhoThong BanLuuThongTinToKhaiPhoThong = new Company.Interface.Report.VNACCS.BanLuuThongTinToKhaiPhoThong();

            return BanLuuThongTinToKhaiPhoThong;
        }
        private Company.Interface.Report.VNACCS.ThongTinSuaDoiToKhaiPhoThong ThongTinSuaDoiToKhaiPhoThong()
        {
            Company.Interface.Report.VNACCS.ThongTinSuaDoiToKhaiPhoThong ThongTinSuaDoiToKhaiPhoThong = new Company.Interface.Report.VNACCS.ThongTinSuaDoiToKhaiPhoThong();

            return ThongTinSuaDoiToKhaiPhoThong;
        }
        private Company.Interface.Report.VNACCS.ThongTinToKhaiPhoThong ThongTinToKhaiPhoThong()
        {
            Company.Interface.Report.VNACCS.ThongTinToKhaiPhoThong ThongTinToKhaiPhoThong = new Company.Interface.Report.VNACCS.ThongTinToKhaiPhoThong();

            return ThongTinToKhaiPhoThong;
        }
        private Company.Interface.Report.VNACCS.BanXacNhanNoiDungKhaiSuaDoiBoSung_1 XacNhanNoiDungKhaiSuaDoiBoSung()
        {
            Company.Interface.Report.VNACCS.BanXacNhanNoiDungKhaiSuaDoiBoSung_1 XacNhanNoiDungKhaiSuaDoiBoSung = new Company.Interface.Report.VNACCS.BanXacNhanNoiDungKhaiSuaDoiBoSung_1();

            return XacNhanNoiDungKhaiSuaDoiBoSung;
        }
        private Company.Interface.Report.VNACCS.BanXacNhanNoiDungKhaiSuaDoiBoSung_2 XacNhanNoiDungKhaiSuaDoiBoSung2()
        {
            Company.Interface.Report.VNACCS.BanXacNhanNoiDungKhaiSuaDoiBoSung_2 XacNhanNoiDungKhaiSuaDoiBoSung2 = new Company.Interface.Report.VNACCS.BanXacNhanNoiDungKhaiSuaDoiBoSung_2();

            return XacNhanNoiDungKhaiSuaDoiBoSung2;
        }
        private Company.Interface.Report.VNACCS.QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1 QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1()
        {
            Company.Interface.Report.VNACCS.QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1 QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1 = new Company.Interface.Report.VNACCS.QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1();

            return QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_1;
        }
        private Company.Interface.Report.VNACCS.QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2 QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2()
        {
            Company.Interface.Report.VNACCS.QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2 QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2 = new Company.Interface.Report.VNACCS.QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2();

            return QuyetDinhXacNhanNoiDungKhaiSuaDoiBoSung_2;
        }
        private Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_1 BanXacNhanKhaiBaoDMMienThueNK_1()
        {
            Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_1 BanXacNhanKhaiBaoDMMienThueNK_1 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_1();

            return BanXacNhanKhaiBaoDMMienThueNK_1;
        }
        private Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_2 BanXacNhanKhaiBaoDMMienThueNK_2()
        {
            Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_2 BanXacNhanKhaiBaoDMMienThueNK_2 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_2();

            return BanXacNhanKhaiBaoDMMienThueNK_2;
        }
        private Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_3 BanXacNhanKhaiBaoDMMienThueNK_3()
        {
            Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_3 BanXacNhanKhaiBaoDMMienThueNK_3 = new Company.Interface.Report.VNACCS.BanXacNhanKhaiBaoDMMienThueNK_3();

            return BanXacNhanKhaiBaoDMMienThueNK_3;
        }
        private Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_1 DonDeNghiCapGiayPhepXNKVatLieuNoCN_1()
        {
            Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_1 DonDeNghiCapGiayPhepXNKVatLieuNoCN_1 = new Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_1();

            return DonDeNghiCapGiayPhepXNKVatLieuNoCN_1;
        }
        private Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_2 DonDeNghiCapGiayPhepXNKVatLieuNoCN_2()
        {
            Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_2 DonDeNghiCapGiayPhepXNKVatLieuNoCN_2 = new Company.Interface.Report.VNACCS.DonDeNghiCapGiayPhepXNKVatLieuNoCN_2();

            return DonDeNghiCapGiayPhepXNKVatLieuNoCN_2;
        }
        private Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_1 QuyetDinhCapGiayPhepXNKVatLieuNoCN_1()
        {
            Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_1 QuyetDinhCapGiayPhepXNKVatLieuNoCN_1 = new Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_1();

            return QuyetDinhCapGiayPhepXNKVatLieuNoCN_1;
        }
        private Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_2 QuyetDinhCapGiayPhepXNKVatLieuNoCN_2()
        {
            Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_2 QuyetDinhCapGiayPhepXNKVatLieuNoCN_2 = new Company.Interface.Report.VNACCS.QuyetDinhCapGiayPhepXNKVatLieuNoCN_2();

            return QuyetDinhCapGiayPhepXNKVatLieuNoCN_2;
        }
        private Company.Interface.Report.VNACCS.ThongBaoHuongDanCapPhepXNKVatLieuNoCN ThongBaoHuongDanCapPhepXNKVatLieuNoCN()
        {
            Company.Interface.Report.VNACCS.ThongBaoHuongDanCapPhepXNKVatLieuNoCN ThongBaoHuongDanCapPhepXNKVatLieuNoCN = new Company.Interface.Report.VNACCS.ThongBaoHuongDanCapPhepXNKVatLieuNoCN();

            return ThongBaoHuongDanCapPhepXNKVatLieuNoCN;
        }
        private Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_1 GiayDangKyKiemTraThucPhamNK_1()
        {
            Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_1 GiayDangKyKiemTraThucPhamNK_1 = new Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_1();

            return GiayDangKyKiemTraThucPhamNK_1;
        }
        private Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_2 GiayDangKyKiemTraThucPhamNK_2()
        {
            Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_2 GiayDangKyKiemTraThucPhamNK_2 = new Company.Interface.Report.VNACCS.GiayDangKyKiemTraThucPhamNK_2();

            return GiayDangKyKiemTraThucPhamNK_2;
        }
        private Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_1 GiayDangKyKiemDichDongVatNK_1()
        {
            Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_1 GiayDangKyKiemDichDongVatNK_1 = new Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_1();

            return GiayDangKyKiemDichDongVatNK_1;
        }
        private Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_2 GiayDangKyKiemDichDongVatNK_2()
        {
            Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_2 GiayDangKyKiemDichDongVatNK_2 = new Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_2();

            return GiayDangKyKiemDichDongVatNK_2;
        }
        private Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_3 GiayDangKyKiemDichDongVatNK_3()
        {
            Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_3 GiayDangKyKiemDichDongVatNK_3 = new Company.Interface.Report.VNACCS.GiayDangKyKiemDichDongVatNK_3();

            return GiayDangKyKiemDichDongVatNK_3;
        }
        private Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1 GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1()
        {
            Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1 GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1 = new Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1();

            return GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1;
        }
        private Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2 GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2()
        {
            Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2 GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2 = new Company.Interface.Report.VNACCS.GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2();

            return GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2;
        }
        //private Company.Interface.Report.VNACCS.BanXacNhanNoiDungToKhaiHangHoaXuatKhau_3 NhapKhau_BanXacNhan_3()
        //{
        //    Company.Interface.Report.VNACCS.BanXacNhanNoiDungToKhaiHangHoaXuatKhau_3 NhapKhau_BanXacNhan_3 = new Company.Interface.Report.VNACCS.BanXacNhanNoiDungToKhaiHangHoaXuatKhau_3();

        //    return NhapKhau_BanXacNhan_3;
        //}

        //private Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_1 NhapKhau_QuyetDinhTQ_1()
        //{
        //    Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_1 NhapKhau_QuyenDinh_1 = new Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_1();

        //    return NhapKhau_QuyenDinh_1;
        //}
        //private Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_2 NhapKhau_QuyetDinhTQ_2()
        //{
        //    Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_2 NhapKhau_QuyenDinh_2 = new Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_2();

        //    return NhapKhau_QuyenDinh_2;
        //}
        //private Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_3 NhapKhau_QuyetDinhTQ_3()
        //{
        //    Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_3 NhapKhau_QuyenDinh_3 = new Company.Interface.Report.VNACCS.QuyetDinhThongQuanHangHoaXuatKhau_3();

        //    return NhapKhau_QuyenDinh_3;
        //}

        #endregion

        public static DataTable ConvertListToTable(GroupAttribute group, int loop)
        {
            int STT = 0;
            DataTable gr = new DataTable();
            foreach (PropertiesAttribute attribute in group.listAttribute)
            {
                gr.Columns.Add(attribute.GroupID, attribute.OfType == typeof(int) ? typeof(decimal) : attribute.OfType);
            }
            gr.Columns.Add("STT", typeof(string));
            for (int i = 0; i < loop; i++)
            {
                STT++;
                DataRow dr = gr.NewRow();
                foreach (PropertiesAttribute attribute in group.listAttribute)
                {

                    if (attribute.OfType == typeof(DateTime))
                    {
                        if (System.Convert.ToDateTime(attribute.GetValueCollection(i)).Year > 1900)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else if (attribute.OfType == typeof(decimal) || attribute.OfType == typeof(int))
                    {
                        if (System.Convert.ToDecimal(attribute.GetValueCollection(i)) != 0)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else
                        dr[attribute.GroupID] = attribute.GetValueCollection(i);

                }
                dr["STT"] = STT.ToString().ToUpper();
                gr.Rows.Add(dr);
            }
            return gr;
        }
    }



}
