﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;

namespace DongBoDuLieu
{
    public partial class Login : Form
    {
        protected DongBoDuLieu.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new DongBoDuLieu.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog();
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                   ShowMessage("Đăng nhập không thành công.", false);
                   MainForm.isLoginSuccess = false;
                }
                else
                {
                    MainForm.isLoginSuccess = true;
                    //Properties.Settings.Default.UserLog = txtUser.Text.Trim();
                    //Properties.Settings.Default.PassLog = txtMatKhau.Text.Trim();
                    //Properties.Settings.Default.Save();
                    this.Close();
                }
            }
            catch
            {
                MessageBox.Show("Không kết nối tới cơ sở dữ liệu");
                linkLabel1_LinkClicked(null, null);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            linkLabel1.Text = "Cấu hình thông số kết nối ";
            uiButton3.Text = "Đăng nhập";
            uiButton2.Text = "Thoát";
            try
            {
                this.BackgroundImage = System.Drawing.Image.FromFile("loginv.png");
            }
            catch { }
        }
    }
}