﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Company.KDT.SHARE.Components.Nodes.GC;
using SOFTECH.ECS.V3.Components.GC;
using SOFTECH.ECS.V3.Components.SHARE;
using System.Xml;
using Company.KDT.SHARE.Components.Utils;
// SiHV
namespace SOFTECH.ECS.V3.Components.GC.SHARE
{
    public partial class ProcessXML
    {
        /// <summary>
        /// SetValueAgent
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueAgent(XElement Parent, ToKhaiMauDich tkmd)
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())                                   // Đại lý
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.name.ToUpper()))      // Tên đại lý
                    {
                        element.SetValue(tkmd.TenDaiLyTTHQ);// Tên người được cấp giấy phép
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Identity.ToUpper()))  // Mã đại lý
                    {
                        element.SetValue(tkmd.MaDaiLyTTHQ);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.status.ToUpper()))    //??????????????????????/
                    {
                        element.SetValue("");
                    }
                }
            }
            return Parent;
        }                // Đại lý
        /// <summary>
        /// SetValueImporter
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueImporter(XElement Parent, ToKhaiMauDich tkmd)                // người được cấp giấy phép
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.name.ToUpper()))      // Tên người được cấp giấy phép
                    {
                        element.SetValue(tkmd.TenDoanhNghiep);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Identity.ToUpper()))  // Ma người được cấp giấy phép
                    {
                        element.SetValue(tkmd.MaDoanhNghiep);
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueExporter
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueExporter(XElement Parent, ToKhaiMauDich tkmd)                // người được xuat khau
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.name.ToUpper()))          // Tên người xuat khau
                    {
                        element.SetValue(tkmd.TenDonViDoiTac);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Identity.ToUpper()))      // Ma người xuat
                    {
                        element.SetValue("");//???????????????????????????
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueDeclarationDocument
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueDeclarationDocument(XElement Parent, ToKhaiMauDich tkmd)     // To Khai
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.reference.ToUpper()))         //Số TK
                    {
                        element.SetValue(tkmd.SoToKhai);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issue.ToUpper()))             //Ngày TK	1	an19	YYYY-MM-DD HH:mm:ss ?????????????????????
                    {
                        element.SetValue(tkmd.NgayDangKy);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.natureOfTransaction.ToUpper()))//Mã LH	1	an..10	Danh mục chuẩn
                    {
                        element.SetValue(tkmd.MaLoaiHinh);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.declarationOffice.ToUpper())) //Mã hải quan 	1	an..6	Danh mục chuẩn
                    {
                        element.SetValue(tkmd.MaHaiQuan);
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueCurrencyExchange
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueCurrencyExchange(XElement Parent, ToKhaiMauDich tkmd)        // Nguyên tệ
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                NguyenTe nt = NguyenTe.Load(tkmd.NguyenTe_ID);
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.CurrencyType.ToUpper()))  //Mã nguyên tệ	1	a3	Danh mục chuẩn
                    {
                        if (nt != null)
                            element.SetValue(nt.ID);
                        else
                            element.SetValue("");

                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.rate.ToUpper()))          //Ngày TK	1	an19	YYYY-MM-DD HH:mm:ss ?????????????????????
                    {
                        if (nt != null)
                            element.SetValue(nt.Ten);
                        else
                            element.SetValue("");
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueDeclarationPackaging (So kien)
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueDeclarationPackaging(XElement Parent, ToKhaiMauDich tkmd)    // Số kiện
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
                {
                    foreach (XElement element in Parent.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeTKMD.quantity.ToUpper()))
                        {
                            element.SetValue(tkmd.SoKien);
                        }
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueInvoice
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueInvoice(XElement Parent, ToKhaiMauDich tkmd)                 // Hóa đơn thương mại
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issue.ToUpper()))      //Ngày hóa đơn	1	an10	YYYY-MM-DD
                    {
                        element.SetValue(tkmd.NgayHoaDonThuongMai);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.reference.ToUpper()))  //Số hóa đơn	1	an..50	
                    {
                        element.SetValue(tkmd.SoHoaDonThuongMai);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.type.ToUpper()))        //Mã loại hóa đơn thương mại	1	an..3	Danh mục chuẩn
                    {
                        element.SetValue("");                                                     //???????????????????????
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueRepresentativePerson
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueRepresentativePerson(XElement Parent, ToKhaiMauDich tkmd)     // Người đại diện doanh nghiệp	1	none	Ký, đóng dấu trên tờ khai
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.RepresentativePerson.ToUpper()))  //Chức vụ	1	a..256	Bắt buộc phải có
                    {
                        element.SetValue(tkmd.MaDonViUT);
                    }

                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.name.ToUpper()))                   //Tên	1	a..35	Bắt buộc phải có
                    {
                        element.SetValue(tkmd.TenDonViUT);

                    }
                }
            }
            return Parent;
        }

        public static XElement SetValueContractReference(XElement Parent, ToKhaiMauDich tkmd)
        {
            HopDong hdong = HopDong.Load(tkmd.IDHopDong);
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {

                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.reference.ToUpper())) // Số hợp đồng
                    {
                        element.SetValue(tkmd.SoHopDong);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issue.ToUpper())) // Ngày ký hợp đồng
                    {
                        element.SetValue(tkmd.NgayHopDong);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.declarationOffice.ToUpper())) // Mã HQ tiếp nhận hợp đồng
                    {
                        if (hdong != null)
                        {
                            element.SetValue(hdong.MaHaiQuan);
                        }
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// AddVouchers (Add chung tu)
        /// </summary>
        /// <param name="documentXML">XDocument</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <param name="issuer">int</param>
        /// <param name="function">int</param>
        /// <param name="status">int</param>
        /// <param name="declarationOffice">int</param>
        /// <returns>XDocument</returns>
        public static XElement AddVouchers(XElement Parent, ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)// Add Chung tu TKMD
        {
            if (Parent != null && tkmd != null)
            {
                CuaKhau ck = CuaKhau.Load(tkmd.MaHaiQuan);
                DonViHaiQuan dvhq = DonViHaiQuan.Load(tkmd.MaHaiQuan);
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issuer.ToUpper()))                //issuer 	Loại chứng từ
                    {
                        element.SetValue(issuer);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.reference.ToUpper()))        // Số tham chiếu tờ khai
                    {
                        element.SetValue(tkmd.ID);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issue.ToUpper()))            // Ngày khai báo
                    {
                        element.SetValue(tkmd.NgayGiayPhep);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.function.ToUpper()))         // Chức năng
                    {
                        element.SetValue(function);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issueLocation.ToUpper()))    // Nơi khai báo
                    {
                        if (ck != null)
                            element.SetValue(ck.Ten);
                        else element.SetValue("");
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.status.ToUpper()))           // Trạng thái của chứng từ
                    {
                        element.SetValue(status);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.customsReference.ToUpper())) // Số tiếp nhận tờ khai; Dùng trong trường hợp khai sửa bản đăng ký
                    {
                        element.SetValue(tkmd.GUIDSTR);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.acceptance.ToUpper()))       // Ngày đăng ký chứng từ 
                    {
                        element.SetValue(tkmd.NgayDangKy);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.declarationOffice.ToUpper()))// Đơn vị HQ khai báo
                    {
                        if (dvhq != null)
                            element.SetValue(dvhq.Ten);
                        else
                            element.SetValue("");
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.GoodsItem.ToUpper()))        // Số lượng hàng
                    {
                        element.SetValue(tkmd.SoHang);// ????????????????????????????????????????
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.loadingList.ToUpper()))      // Số lượng chứng từ, phụ lục đính kèm
                    {
                        element.SetValue(tkmd.SoLuongPLTK);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.totalGrossMass.ToUpper()))   // Trọng lượng (kg)
                    {
                        element.SetValue(tkmd.TrongLuong);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.natureOfTransaction.ToUpper()))// Mã loại hình
                    {
                        element.SetValue(tkmd.MaLoaiHinh);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Payment.ToUpper()))          // Mã phương thức thanh toán
                    {
                        element.SetValue(tkmd.PTTT_ID);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Agent.ToUpper()))            // Đại lý
                    {
                        SetValueAgent(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.ContractReference.ToUpper()))// hợp đồng
                    {
                        SetValueContractReference(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.CurrencyExchange.ToUpper())) // Nguyên tệ
                    {
                        SetValueCurrencyExchange(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.DeclarationPackaging.ToUpper())) // Số kiện
                    {
                        SetValueDeclarationPackaging(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Importer.ToUpper()))             //Người nhập khẩu
                    {
                        SetValueImporter(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Exporter.ToUpper()))             //Người xuất khẩu
                    {
                        SetValueExporter(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.RepresentativePerson.ToUpper()))  // Người đại diện doanh nghiệp
                    {
                        SetValueRepresentativePerson(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.DeclarationDocument.ToUpper()))
                    {
                        SetValueDeclarationDocument(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.AdditionalInfomation.ToUpper())) // Ghi chú khác 
                    {
                        element.SetValue(tkmd.HUONGDAN);    // ????????????????????????????????????????????????????????????????
                    }

                }
            }
            return Parent;

        }

        /// <summary>
        /// AddVouchers (Add chung tu)
        /// </summary>
        /// <param name="documentXML">XDocument</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <param name="issuer">int</param>
        /// <param name="function">int</param>
        /// <param name="status">int</param>
        /// <param name="declarationOffice">int</param>
        /// <returns>XDocument</returns>
        public static XElement AddVouchers(XElement Parent, HopDong hd, int issuer, int function, int status, int declarationOffice)// Add Chung tu TKMD
        {
            if (Parent != null && hd != null)
            {
                CuaKhau ck = CuaKhau.Load(hd.MaHaiQuan);
                DonViHaiQuan dvhq = DonViHaiQuan.Load(hd.MaHaiQuan);
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issuer.ToUpper()))                //issuer 	Loại chứng từ
                    {
                        element.SetValue(issuer);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.reference.ToUpper()))        // Số tham chiếu tờ khai
                    {
                        element.SetValue(hd.ID);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issue.ToUpper()))            // Ngày khai báo
                    {
                        element.SetValue(hd.NgayDangKy);    // ????????????????????????????????????????????????????????????????????
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.function.ToUpper()))         // Chức năng
                    {
                        element.SetValue(function);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.issueLocation.ToUpper()))    // Nơi khai báo
                    {
                        if (ck != null)
                            element.SetValue(ck.Ten);
                        else element.SetValue("");
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.status.ToUpper()))           // Trạng thái của chứng từ
                    {
                        element.SetValue(status);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.customsReference.ToUpper())) // Số tiếp nhận tờ khai; Dùng trong trường hợp khai sửa bản đăng ký
                    {
                        element.SetValue(hd.GUIDSTR);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.acceptance.ToUpper()))       // Ngày đăng ký chứng từ 
                    {
                        element.SetValue(hd.NgayDangKy);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.declarationOffice.ToUpper()))// Đơn vị HQ khai báo
                    {
                        if (dvhq != null)
                            element.SetValue(dvhq.Ten);
                        else
                            element.SetValue("");
                    }



                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Agent.ToUpper()))            // Đại lý
                    //{
                    //    SetValueAgent(element, hd);
                    //}
                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.ContractReference.ToUpper()))// hợp đồng
                    //{
                    //    SetValueContractReference(element, hd);
                    //}
                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.CurrencyExchange.ToUpper())) // Nguyên tệ
                    //{
                    //    SetValueCurrencyExchange(element, hd);
                    //}
                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.DeclarationPackaging.ToUpper())) // Số kiện
                    //{
                    //    SetValueDeclarationPackaging(element, hd);
                    //}
                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Importer.ToUpper()))             //Người nhập khẩu
                    //{
                    //    SetValueImporter(element, hd);
                    //}
                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.Exporter.ToUpper()))             //Người xuất khẩu
                    //{
                    //    SetValueExporter(element, hd);
                    //}
                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.RepresentativePerson.ToUpper()))  // Người đại diện doanh nghiệp
                    //{
                    //    SetValueRepresentativePerson(element, hd);
                    //}
                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.DeclarationDocument.ToUpper()))
                    //{
                    //    SetValueDeclarationDocument(element, hd);
                    //}
                    //else if (element.Name.ToString().ToUpper().Equals(NodeTKMD.AdditionalInfomation.ToUpper())) // Ghi chú khác 
                    //{
                    //    element.SetValue(hd.HUONGDAN);    // ????????????????????????????????????????????????????????????????
                    //}

                }
            }
            return Parent;

        }

        public static string SendTKNhapGCCT(string pass, long idTKMD)
        {
            //Get object TK GC Chuyen tiep
            //SOFTECH.ECS.V3.Components.GC.ToKhaiChuyenTiep tkmd = SOFTECH.ECS.V3.Components.GC.ToKhaiChuyenTiep.Load(idTKMD);
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
            tkct.LoadByID(idTKMD);

            //Generate XML
            string messageXML = SOFTECH.ECS.V3.Components.GC.ToKhaiNhapChuyenTiep.ConfigTKNhapChuyenTiep(tkct,
                tkct.MaLoaiHinh.Contains("N")?Message_Types.To_khai_nhap_khau_Gia_cong_Chuyen_Tiep:Message_Types.To_khai_xuat_khau_Gia_cong_Chuyen_Tiep,
                tkct.TrangThaiXuLy.Equals(Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)?Message_Functions.Sua : Message_Functions.Khai_bao, 1, tkct.MaHaiQuanKH);
            
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(messageXML);
            
            //luu vao string
            XmlNode root = doc.SelectSingleNode("Envelope/Body/Content/Declaration"); 
            XmlNode node = null;
            
            //Giay phep
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep.ConvertCollectionGiayPhepToXML(doc, tkct.CTKGiayPhepCollection, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            //ContractDocuments - Hợp đồng
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong.ConvertCollectionHopDongToXML(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            // CommercialInvoices - Hóa đơn
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon.ConvertCollectionHoaDonThuongMaiToXML(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            // CustomsOfficeChangedRequests - Đề nghị chuyển cửa khẩu
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCKToXML(doc, null, tkct.ID);
            if (node != null)
                root.AppendChild(node);

            // AttachDocuments - Chứng từ kèm dạng ảnh
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh.ConvertCollectionCTDinhKemToXMLV3(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            messageXML = doc.InnerXml;
            //Send message XML
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            
            try
            {
                kq = kdt.Send(messageXML, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(messageXML, idTKMD,
                    Company.KDT.SHARE.Components.MessageTitle.KhaiBaoToKhaiCT);
                
            }
            catch (Exception ex)
            {

                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                XmlNode statement = docResult.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
                if (statement.InnerText == "12")
                {
                    //XmlNode Declaration = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
                    //Declaration.RemoveAll();
                    return doc.InnerXml;
                    //return messageXML;
                }
                else
                {
                    msgError =FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").InnerText + "|");

                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, idTKMD,
                          Company.KDT.SHARE.Components.MessageTitle.Error, msgError);
                    throw ex;
                }
                else Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            //return "";
        }

        public static string HuyKhaiBao(string pass, string messageXML, Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(messageXML);

            messageXML = SOFTECH.ECS.V3.Components.GC.ToKhaiNhapChuyenTiep.ConfigHuyKhaiBaoTKCT(tkct, messageXML);

            //
            //messageXML = doc.InnerXml;
            //Send message XML
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(messageXML, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(
                    messageXML, tkct.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhaiCT);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            XmlNode statement = docResult.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
            if (statement.InnerText == "12")
            {
                //XmlNode Declaration = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
                //Declaration.RemoveAll();
                return doc.InnerXml;
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").InnerText + "|"));
            }
        }
        public static string HuyToKhai(string pass, string messageXML, Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct)
        {
            messageXML = SOFTECH.ECS.V3.Components.GC.ToKhaiNhapChuyenTiep.ConfigHuyTKCT(tkct, messageXML, tkct.MaLoaiHinh.Contains("N") ? Message_Types.To_khai_nhap_khau_Gia_cong_Chuyen_Tiep : Message_Types.To_khai_xuat_khau_Gia_cong_Chuyen_Tiep, Message_Functions.Huy);
            ///messageXML = SOFTECH.ECS.V3.Components.GC.ToKhaiNhapChuyenTiep.ConfigHuyTKCT(tkct, messageXML);
            //
            //messageXML = doc.InnerXml;
            //Send message XML
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(messageXML, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(
                    messageXML, tkct.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhaiCT);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            XmlNode statement = docResult.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
            if (statement.InnerText == "12")
            {
                //XmlNode Declaration = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
                //Declaration.RemoveAll();
               return messageXML;
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").InnerText + "|"));
            }
        }

        public static string SendTKGCCT_SUA(string pass, long idTKMD)
        {
            //Get object TK GC Chuyen tiep
            //SOFTECH.ECS.V3.Components.GC.ToKhaiChuyenTiep tkmd = SOFTECH.ECS.V3.Components.GC.ToKhaiChuyenTiep.Load(idTKMD);
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
            tkct.LoadByID(idTKMD);

            //Generate XML
            string messageXML = SOFTECH.ECS.V3.Components.GC.ToKhaiNhapChuyenTiep.ConfigTKNhapChuyenTiep(tkct,
                tkct.MaLoaiHinh.Contains("N") ? Message_Types.To_khai_nhap_khau_Gia_cong_Chuyen_Tiep : Message_Types.To_khai_xuat_khau_Gia_cong_Chuyen_Tiep,
                 Message_Functions.Sua, 1, tkct.MaHaiQuanKH);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(messageXML);

            //luu vao string
            XmlNode root = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
            XmlNode node = null;

            //Giay phep
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep.ConvertCollectionGiayPhepToXML(doc, tkct.CTKGiayPhepCollection, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            //ContractDocuments - Hợp đồng
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong.ConvertCollectionHopDongToXML(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            // CommercialInvoices - Hóa đơn
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon.ConvertCollectionHoaDonThuongMaiToXML(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            // CustomsOfficeChangedRequests - Đề nghị chuyển cửa khẩu
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCKToXML(doc, null, tkct.ID);
            if (node != null)
                root.AppendChild(node);

            // AttachDocuments - Chứng từ kèm dạng ảnh
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh.ConvertCollectionCTDinhKemToXMLV3(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            messageXML = doc.InnerXml;
            //Send message XML
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(messageXML, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(
                    messageXML, tkct.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoSuaToKhaiCT);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            XmlNode statement = docResult.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
            if (statement.InnerText == "12")
            {
                //XmlNode Declaration = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
                //Declaration.RemoveAll();
                return doc.InnerXml;
                //return messageXML;
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").InnerText + "|"));
            }

            //return "";
        }

    }
}
