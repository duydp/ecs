﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.BLL.SXXK
{
    public partial class PhanBoToKhaiNhap
    {
        public static int DeleteBy_TKXuat_ID(long tKXuat_ID, SqlTransaction transaction)
        {
            string spName = "[dbo].[p_SXXK_PhanBoToKhaiNhap_DeleteBy_TKXuat_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, tKXuat_ID);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionBy_TKXuat_ID(long tKXuat_ID, SqlTransaction transaction)
        {
            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();
            IDataReader reader = SelectReaderBy_TKXuat_ID(tKXuat_ID, transaction);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public static IDataReader SelectReaderBy_TKXuat_ID(long tKXuat_ID, SqlTransaction transaction)
        {
            const string spName = "p_SXXK_PhanBoToKhaiNhap_SelectBy_TKXuat_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKXuat_ID", SqlDbType.BigInt, tKXuat_ID);

            return db.ExecuteReader(dbCommand, transaction);
        }

        public static PhanBoToKhaiXuat SelectToKhaiXuatDuocPhanBoDauTienChoToKhaiNhap(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDK)
        {
            string sql = "select TKXuat_ID " +
                        "from t_SXXK_PhanBoToKhaiNhap " +
                        "where id= " +
                        "( " +
                            "select min(id) from t_SXXK_PhanBoToKhaiNhap  " +
                            "where SoToKhaiNhap=@SoToKhaiNhap " +
                            "and MaLoaiHinhNhap=@MaLoaiHinhNhap " +
                            "and MaHaiQuanNhap=@MaHaiQuanNhap " +
                            "and NamDangKyNhap=@NamDangKyNhap " +
                        ")";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuanNhap", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKyNhap", SqlDbType.SmallInt, NamDK);
            long id = Convert.ToInt64(db.ExecuteScalar(dbCommand));
            if (id == 0)
                return null;
            return PhanBoToKhaiXuat.Load(id);


        }
        //public static DataTable SelectPhanBoToKhaiXuat(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy, int SoThapPhanNPL)
        //{

        //    string spName = "select * from t_SXXK_PhanBoToKhaiNhap where TKXuat_ID in " +
        //                    "( select id from t_SXXK_PhanBoToKhaiXuat where  SoToKhaiXuat=@SoToKhai  and NamDangKyXuat=@NamDangKy and MaLoaiHinhXuat=@MaLoaiHinh and MaHaiQuanXuat=@MaHaiQuan)";
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
        //    db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
        //    db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
        //    db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
        //    db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
        //    db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);


           

        //}
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKMD(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy, int SoThapPhanNPL)
        {

            string spName = "select * from t_SXXK_PhanBoToKhaiNhap where TKXuat_ID in " +
                            "( select id from t_SXXK_PhanBoToKhaiXuat where  SoToKhaiXuat=@SoToKhai  and NamDangKyXuat=@NamDangKy and MaLoaiHinhXuat=@MaLoaiHinh and MaHaiQuanXuat=@MaHaiQuan)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);


            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;

        }

        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSP(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy, string MaSP, int SoThapPhanNPL)
        {
            string spName = "select * from t_SXXK_PhanBoToKhaiNhap where TKXuat_ID in " +
                           "( select id from t_SXXK_PhanBoToKhaiXuat where  SoToKhaiXuat=@SoToKhai  and NamDangKyXuat=@NamDangKy and MaLoaiHinhXuat=@MaLoaiHinh and MaHaiQuanXuat=@MaHaiQuan and MaSP=@MaSP)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);

            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        public static DataSet SelectViewPhanBoToKhaiNhap(string where, string order)
        {

            string spName = "select *, (SoLuongXuat * DinhMucChung) as NhuCau from v_SXXK_PhanBoToKhai ";
            if (where != "")
            {
                spName += " where " + where + order;

            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachTKN(string where)
        {

            string spName = "SELECT DISTINCT SoToKhaiNhap, MaLoaiHinhNhap, NamDangKyNhap, MaHaiQuanNhap from v_SXXK_PhanBoToKhai ";
            if (where != "")
            {
                spName += " WHERE " + where;

            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoToKhai(string MaHaiQuan, string MaLoaiHinh, int SoToKhai, short NamDangKy)
        {

            string spName = "SELECT DISTINCT a.MaNPL, b.Ten AS TenNPL, b.DVT_ID AS DVT_ID FROM v_SXXK_PhanBoToKhai a "+
                            "INNER JOIN  t_SXXK_NguyenPhuLieu b ON a.MaNPL = b.Ma "+
                            "WHERE a.SoToKhaiXuat=" + SoToKhai + " and a.NamDangKyXuat=" + NamDangKy + " and a.MaLoaiHinhXuat='" + MaLoaiHinh + "' and a.MaHaiQuanXuat='" + MaHaiQuan + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
    }
}