﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiGCCTA4 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewGCCTA4Form report;
       // public ToKhaiMauDich TKCT = new ToKhaiMauDich();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public int temp = 1;
        public bool BanLuuHaiQuan = false;
        public ToKhaiGCCTA4()
        {
            InitializeComponent();
        }
        public void BindReport(bool inMaHang)
        {
           // lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
            
            if (BanLuuHaiQuan)
                lblBanLuuHaiQuan.Text = "Bản lưu Hải quan";
            else
                lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
           // lblThongBaoMienThue.Text = GlobalSettings.TieuDeInDinhMuc;
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTriGiaTT = 0;
            //lblNgaygiaohang.Text = "Ngay " + 
            DateTime minDate = new DateTime(1900, 1, 1);
            lblDiaDiemGiaoHang.Text = this.TKCT.DiaDiemXepHang;
             if (this.TKCT.MaDaiLy != "")
                 lblMaDailyLTT.Text = this.ToStringForReport(this.TKCT.MaDaiLy);
            lblTenDaiLyTTHQ.Text = "";
            //if (this.TKCT.SoTiepNhan != 0)
             //   this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKCT.SoTiepNhan; 
           
            if (this.TKCT.MaLoaiHinh.Contains("X"))
            {
                lblNguoiGiao.Text = this.ToStringForReport(this.TKCT.MaDoanhNghiep);
                lblTenDoanhNghiep1.Text = GlobalSettings.TEN_DON_VI.ToUpper() + "\r\n" + GlobalSettings.DIA_CHI.ToUpper(); ;
                lblNguoiNhan.Text = this.ToStringForReport(this.TKCT.MaKhachHang);
                lblTenDoiTac.Text = this.TKCT.TenKH;
                //Thong tin hop dong nhan :
                if (this.TKCT.SoHopDongDV != "")
                   lblSoHDGiao.Text = " Số : " + this.TKCT.SoHopDongDV;
                if (this.TKCT.NgayHDDV > minDate)
                  lblNgayHDGiao.Text = "Ngày :" + this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
                if (this.TKCT.NgayHetHanHDDV > minDate)
                    lblNgayHHHDGiao.Text = "Ngày HH :" + this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
                // Thong tin hop dong giao :
                if (this.TKCT.SoHDKH != "")
                    lblsoHDNhan.Text = "Số :" + this.TKCT.SoHDKH;
                if (this.TKCT.NgayHDKH > minDate)
                    lblNgayHDNhan.Text = "Ngày : " + this.TKCT.NgayHDKH.ToString("dd/MM/yyyy");
                if (this.TKCT.NgayHetHanHDKH > minDate)
                    lblNgayHHHDNhan.Text = "Ngày HH : " + this.TKCT.NgayHetHanHDKH.ToString("dd/MM/yyyy");
                lblNguoiChiDinhGiaoHang.Text = this.TKCT.NguoiChiDinhDV;
                lblNguoiChiDinhNhanHang.Text = this.TKCT.NguoiChiDinhKH;
                // hai quan nhận :
                //lblCucHQ1.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
                //lblChiCucHQ1.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            }
            else
            {
                lblNguoiGiao.Text = this.ToStringForReport(this.TKCT.MaKhachHang);
                lblTenDoanhNghiep1.Text = this.TKCT.TenKH;
                lblNguoiNhan.Text = this.ToStringForReport(this.TKCT.MaDoanhNghiep);
                lblTenDoiTac.Text = GlobalSettings.TEN_DON_VI.ToUpper() + "\r\n" + GlobalSettings.DIA_CHI.ToUpper(); ;
                //Thong tin hop dong nhan :
                if (this.TKCT.SoHDKH != "")
                    lblSoHDGiao.Text = " Số : " + this.TKCT.SoHDKH;
                if (this.TKCT.NgayHDKH > minDate)
                    lblNgayHDGiao.Text = "Ngày :" + this.TKCT.NgayHDKH.ToString("dd/MM/yyyy");
                if (this.TKCT.NgayHetHanHDKH > minDate)
                    lblNgayHHHDGiao.Text = "Ngày HH :" + this.TKCT.NgayHetHanHDKH.ToString("dd/MM/yyyy");
                // Thong tin hop dong giao :
                if (this.TKCT.SoHopDongDV != "")
                    lblsoHDNhan.Text = "Số :" +  this.TKCT.SoHopDongDV;
                if (this.TKCT.NgayHDDV > minDate)
                    lblNgayHDNhan.Text = "Ngày : " + this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
                if (this.TKCT.NgayHetHanHDDV > minDate)
                    lblNgayHHHDNhan.Text = "Ngày HH : " + this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
                lblNguoiChiDinhGiaoHang.Text = this.TKCT.NguoiChiDinhKH;
                lblNguoiChiDinhNhanHang.Text = this.TKCT.NguoiChiDinhDV;
                //Hai quan nhận :
                //lblCucHQ2.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
                //lblChiCucHQ2.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            }
            if (this.TKCT.MaLoaiHinh.Contains("PL"))
            {
                xrNhanNLD.Text = "X";
                xrGiaoNLD.Text = "X";
            }
            if (this.TKCT.MaLoaiHinh.Contains("SP"))
            {
              xrNhanSPGCCT.Text = "X";
               xrGiaoSPGCCT.Text = "X";
            }
            if (this.TKCT.MaLoaiHinh.Contains("TB"))
            {
                xrGiaoMMTB.Text = "X";
                xrNhanMMTB.Text = "X";
            }
                    
            if (this.TKCT.HCTCollection.Count <= 19)
            {
                if (this.TKCT.HCTCollection.Count >= 1)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[0];
                    if (hmd.TenHang.Length >= 40) TenHang1.Font = new Font("Times New Roman", 7f);
                    TenHang1.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang1.Text += " / " + hmd.MaHang;
                        TenHang1.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS1.Text = hmd.MaHS;
                    Luong1.Text = hmd.SoLuong.ToString("G15");
                    DVT1.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT1.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT1.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;                  
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);                   
                }
                if (this.TKCT.HCTCollection.Count >= 2)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[1];
                    if (hmd.TenHang.Length >= 40) TenHang2.Font = new Font("Times New Roman", 7f);
                    TenHang2.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang2.Text += " / " + hmd.MaHang;
                        TenHang2.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS2.Text = hmd.MaHS;                     
                    Luong2.Text = hmd.SoLuong.ToString("G15");
                    DVT2.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT2.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT2.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;                    
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);
                    
                }
                if (this.TKCT.HCTCollection.Count >= 3)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[2];
                    if (hmd.TenHang.Length >= 40) TenHang3.Font = new Font("Times New Roman", 7f);
                    TenHang3.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang3.Text += " / " + hmd.MaHang;
                        TenHang3.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS3.Text = hmd.MaHS;                   
                    Luong3.Text = hmd.SoLuong.ToString("G15");
                    DVT3.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT3.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT3.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;                 
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);
                    
                }
                if (this.TKCT.HCTCollection.Count >= 4)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[3];
                    if (hmd.TenHang.Length >= 40) TenHang4.Font = new Font("Times New Roman", 7f);
                    TenHang4.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang4.Text += " / " + hmd.MaHang;
                        TenHang4.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS4.Text = hmd.MaHS;
                    Luong4.Text = hmd.SoLuong.ToString("G15");
                    DVT4.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT4.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT4.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 5)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[4];
                    if (hmd.TenHang.Length >= 40) TenHang5.Font = new Font("Times New Roman", 7f);
                    TenHang5.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang5.Text += " / " + hmd.MaHang;
                        TenHang5.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS5.Text = hmd.MaHS;
                    Luong5.Text = hmd.SoLuong.ToString("G15");
                    DVT5.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT5.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT5.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 6)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[5];
                    if (hmd.TenHang.Length >= 40) TenHang6.Font = new Font("Times New Roman", 7f);
                    TenHang6.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang6.Text += " / " + hmd.MaHang;
                        TenHang6.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS6.Text = hmd.MaHS;
                    Luong6.Text = hmd.SoLuong.ToString("G15");
                    DVT6.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT6.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT6.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 7)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[6];
                    if (hmd.TenHang.Length >= 40) TenHang7.Font = new Font("Times New Roman", 7f);
                    TenHang7.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang7.Text += " / " + hmd.MaHang;
                        TenHang7.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS7.Text = hmd.MaHS;
                    Luong7.Text = hmd.SoLuong.ToString("G15");
                    DVT7.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT7.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT7.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 8)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[7];
                    if (hmd.TenHang.Length >= 40) TenHang8.Font = new Font("Times New Roman", 7f);
                    TenHang8.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang8.Text += " / " + hmd.MaHang;
                        TenHang8.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS8.Text = hmd.MaHS;
                    Luong8.Text = hmd.SoLuong.ToString("G15");
                    DVT8.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT8.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT8.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 9)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[8];
                    if (hmd.TenHang.Length >= 40) TenHang9.Font = new Font("Times New Roman", 7f);
                    TenHang9.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang9.Text += " / " + hmd.MaHang;
                        TenHang9.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS9.Text = hmd.MaHS;
                    Luong9.Text = hmd.SoLuong.ToString("G15");
                    DVT9.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT9.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT9.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 10)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[9];
                    if (hmd.TenHang.Length >= 40) TenHang10.Font = new Font("Times New Roman", 7f);
                    TenHang10.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang10.Text += " / " + hmd.MaHang;
                        TenHang10.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS10.Text = hmd.MaHS;
                    Luong10.Text = hmd.SoLuong.ToString("G15");
                    DVT10.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT10.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT10.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 11)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[10];
                    if (hmd.TenHang.Length >= 40) TenHang11.Font = new Font("Times New Roman", 7f);
                    TenHang11.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang11.Text += " / " + hmd.MaHang;
                        TenHang11.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS11.Text = hmd.MaHS;
                    Luong11.Text = hmd.SoLuong.ToString("G15");
                    DVT11.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT11.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT11.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 12)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[11];
                    if (hmd.TenHang.Length >= 40) TenHang12.Font = new Font("Times New Roman", 7f);
                    TenHang12.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang12.Text += " / " + hmd.MaHang;
                        TenHang12.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS12.Text = hmd.MaHS;
                    Luong12.Text = hmd.SoLuong.ToString("G15");
                    DVT12.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT12.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT12.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 13)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[12];
                    if (hmd.TenHang.Length >= 40) TenHang13.Font = new Font("Times New Roman", 7f);
                    TenHang13.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang13.Text += " / " + hmd.MaHang;
                        TenHang13.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS13.Text = hmd.MaHS;
                    Luong13.Text = hmd.SoLuong.ToString("G15");
                    DVT13.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT13.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT13.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 14)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[13];
                    if (hmd.TenHang.Length >= 40) TenHang14.Font = new Font("Times New Roman", 7f);
                    TenHang14.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang14.Text += " / " + hmd.MaHang;
                        TenHang14.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS14.Text = hmd.MaHS;
                    Luong14.Text = hmd.SoLuong.ToString("G15");
                    DVT14.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT14.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT14.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 15)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[14];
                    if (hmd.TenHang.Length >= 40) TenHang15.Font = new Font("Times New Roman", 7f);
                    TenHang15.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang15.Text += " / " + hmd.MaHang;
                        TenHang15.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS15.Text = hmd.MaHS;
                    Luong15.Text = hmd.SoLuong.ToString("G15");
                    DVT15.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT15.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT15.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 16)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[15];
                    if (hmd.TenHang.Length >= 40) TenHang16.Font = new Font("Times New Roman", 7f);
                    TenHang16.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang16.Text += " / " + hmd.MaHang;
                        TenHang16.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS16.Text = hmd.MaHS;
                    Luong16.Text = hmd.SoLuong.ToString("G15");
                    DVT16.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT16.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT16.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 17)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[16];
                    if (hmd.TenHang.Length >= 40) TenHang17.Font = new Font("Times New Roman", 7f);
                    TenHang17.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang17.Text += " / " + hmd.MaHang;
                        TenHang17.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS17.Text = hmd.MaHS;
                    Luong17.Text = hmd.SoLuong.ToString("G15");
                    DVT17.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT17.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT17.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 18)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[16];
                    if (hmd.TenHang.Length >= 40) TenHang18.Font = new Font("Times New Roman", 7f);
                    TenHang18.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang18.Text += " / " + hmd.MaHang;
                        TenHang18.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS18.Text = hmd.MaHS;
                    Luong18.Text = hmd.SoLuong.ToString("G15");
                    DVT18.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT18.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT18.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
                if (this.TKCT.HCTCollection.Count >= 19)
                {
                    HangChuyenTiep hmd = this.TKCT.HCTCollection[18];
                    if (hmd.TenHang.Length >= 40) TenHang19.Font = new Font("Times New Roman", 7f);
                    TenHang19.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang19.Text += " / " + hmd.MaHang;
                        TenHang19.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS19.Text = hmd.MaHS;
                    Luong19.Text = hmd.SoLuong.ToString("G15");
                    DVT19.Text = DonViTinh.GetName(hmd.ID_DVT);
                    DonGiaNT19.Text = hmd.DonGia.ToString("G10") + "" + this.TKCT.NguyenTe_ID;
                    TriGiaNT19.Text = hmd.TriGia.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);

                }
            }
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2") + "" + this.TKCT.NguyenTe_ID;
                /*
            else
            {
                TenHang1.Text = "NGUYÊN PHỤ LIỆU GIA CÔNG";
                TenHang2.Text = "(CÓ PHỤ LỤC ĐÍNH KÈM)";
                foreach (HangMauDich hmd in this.TKCT.HMDCollection)
                {
                    tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);
                    tongTienThueXNK += hmd.ThueXNK;
                    tongTriGiaTT += hmd.TriGiaTT;
                }
                //XuatXu1.Text = Nuoc.GetName(this.TKCT.HMDCollection[0].NuocXX_ID);
                TriGiaNT1.Text = tongTriGiaNT.ToString("N2");
                //TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                //TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
            }
                 * */
            //tongTriGiaNT += this.TKCT.PhiBaoHiem + TKCT.PhiKhac + TKCT.PhiVanChuyen;
           
            //lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
            //lblTongThueXNKSo.Text = this.TinhTongThueHMD().ToString("N0");
            //string s = Company.BLL.Utils.VNCurrency.ToString(this.TinhTongThueHMD()).Trim();
            //s = s[0].ToString().ToUpper() + s.Substring(1);
            //lblTongThueXNKChu.Text = s.Replace("  ", " ");
            XRControl control = new XRControl();
            int i = 5;
            //try
            //{
            //    foreach (ChungTu ct in this.TKCT.ChungTuTKCollection)
            //    {
            //        if (ct.LoaiCT < 5)
            //        {
            //            control = this.Detail.Controls["lblSoBanChinh" + ct.LoaiCT];
            //            control.Text = ct.SoBanChinh + "";
            //            control = this.Detail.Controls["lblSoBanSao" + ct.LoaiCT];
            //            control.Text = ct.SoBanSao + "";
            //        }
            //        else
            //        {
            //            if (i == 7) return;
            //            control = this.Detail.Controls["lblTenChungTu" + i];
            //            control.Text = ct.TenChungTu;
            //            control = this.Detail.Controls["lblSoBanChinh" + i];
            //            control.Text = ct.SoBanChinh + "";
            //            control = this.Detail.Controls["lblSoBanSao" + i];
            //            control.Text = ct.SoBanSao + "";
            //            i++;
            //        }
            //    }
            //}
            //catch { }
        }
        //public decimal TinhTongThueHMD()
        //{
        //    decimal tong = 0;
        //    foreach (HangMauDich hmd in this.TKCT.HMDCollection)
        //        tong += hmd.ThueXNK;
        //    return tong;
        //}
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + "    ";
            temp += s[s.Length - 1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            //ptbImage.Visible = t;
            //lblBanLuuHaiQuan.Visible = t;
        }
        public void setNhomHang(string tenNhomHang)
        {
            switch (temp)
            {
                case 1:
                    TenHang1.Text = tenNhomHang;
                    break;
                case 2:
                    TenHang2.Text = tenNhomHang;
                    break;
                case 3:
                    TenHang3.Text = tenNhomHang;
                    break;
                case 4:
                    TenHang4.Text = tenNhomHang;
                    break;
                case 5:
                    TenHang5.Text = tenNhomHang;
                    break;
                case 6:
                    TenHang6.Text = tenNhomHang;
                    break;
                case 7:
                    TenHang7.Text = tenNhomHang;
                    break;
                case 8:
                    TenHang8.Text = tenNhomHang;
                    break;
                case 9:
                    TenHang9.Text = tenNhomHang;
                    break;
                case 10:
                    TenHang10.Text = tenNhomHang;
                    break;
                case 11:
                    TenHang11.Text = tenNhomHang;
                    break;
                case 12:
                    TenHang12.Text = tenNhomHang;
                    break;
                case 13:
                    TenHang13.Text = tenNhomHang;
                    break;
                case 14:
                    TenHang14.Text = tenNhomHang;
                    break;
                case 15:
                    TenHang15.Text = tenNhomHang;
                    break;
                case 16:
                    TenHang16.Text = tenNhomHang;
                    break;
                case 17:
                    TenHang17.Text = tenNhomHang;
                    break;
                case 18:
                    TenHang18.Text = tenNhomHang;
                    break;
                case 19:
                    TenHang19.Text = tenNhomHang;
                    break;
            }


        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
    }
}
