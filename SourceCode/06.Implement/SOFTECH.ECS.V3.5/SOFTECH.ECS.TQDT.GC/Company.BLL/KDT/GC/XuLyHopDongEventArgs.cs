﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.GC.BLL.KDT.GC
{
    class XuLyHopDongEventArgs : EventArgs
    {
        public Exception Error { get; private set; }
        public string StatusStr { get; private set; }
        public int ProcessValue { get; private set; }

        public XuLyHopDongEventArgs(Exception Error, string sts, int valueProcess)
        {
            this.Error = Error;
            this.StatusStr = sts;
            this.ProcessValue = valueProcess;
        }


    }
}
