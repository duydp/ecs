﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Common;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.QuanLyChungTu;
namespace Company.GC.BLL.KDT.GC
{
    public partial class HopDong
    {
        public event EventHandler<HopDongEventArgs> HopDongEventArg;
        public DateTime dateFrom;
        public DateTime dateTo;

        public void OnProcess(HopDongEventArgs e)
        {
            if (HopDongEventArg != null)
            {
                HopDongEventArg(this, e);
            }
        }

        public List<NguyenPhuLieuBoSung> NPLBoSungCollection;
        public List<NguyenPhuLieu> NPLCollection;
        public SanPhamCollection SPCollection;
        public List<NhomSanPham> NhomSPCollection;
        public List<ThietBi> TBCollection;
        public List<HangMau> HangMauCollection;
        public DinhMucDangKyCollection DMDKCollection;
        public PhuKienDangKyCollection PKDKCollection;
        public ToKhaiMauDichCollection TKMDCollection;
        public List<ToKhaiChuyenTiep> TKCTCollection;
        public BKCungUngDangKyCollection BKCUDKCollection;

        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

        //public long SoTiepNhanMax()
        //{
        //    string spName = "SELECT MAX(SoTiepNhan) FROM dbo.t_KDT_GC_HopDong";
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
        //    object obj = db.ExecuteScalar(dbCommand);
        //    if (obj == null || string.IsNullOrEmpty(obj.ToString()))
        //        return 0;
        //    else
        //        return (Int64)obj;
        //}
        public static long GetID(string SoHopDong)
        {
            string spName = "SELECT TOP 1 ID FROM dbo.t_KDT_GC_HopDong WHERE SoHopDong = '" + SoHopDong + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return 0;
            else
                return (long)obj;
        }
        public SanPhamCollection GetSanPhamThuocNhom(string nhomSP)
        {
            return new SanPham().SelectCollectionDynamic(" HopDong_ID = " + this.ID + " AND NhomSanPham_ID = '" + nhomSP + "' AND Ma in (SELECT DISTINCT MaSanPham FROM t_KDT_GC_DinhMuc)", "");
        }
        public int GetThongTinCoBanHopDong(string table)
        {
            string sql = "SELECT COUNT(*) FROM " + table + " WHERE HopDong_ID = " + this.ID;
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            int temp = 0;
            try
            {
                temp = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            }
            catch { }

            return temp;
        }
        public int GetTongSoSanPhamCoDinhMuc()
        {
            string sql = "SELECT COUNT(DISTINCT MaSanPham) FROM t_GC_DinhMuc WHERE HopDong_ID = " + this.ID;
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            int temp = 0;
            try
            {
                temp = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            }
            catch { }

            return temp;
        }
        public int GetThongTinToKhaiHopDong(int TrangThai, string maLoaiHinh, string loaiHangHoa)
        {
            string sql = "SELECT COUNT(ID) FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = " + this.ID + " AND TrangThaiXuLy =" + TrangThai + " AND MaLoaiHinh LIKE '" + maLoaiHinh + "%' AND LoaiHangHoa = '" + loaiHangHoa + "'";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            int temp = 0;
            try
            {
                temp = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            }
            catch { }

            return temp;
        }
        public int GetThongTinToKhaiChuyenTiepHopDong(int TrangThai, string maLoaiHinh)
        {
            string sql = "";
            if (maLoaiHinh.Equals("PHPLN"))
                sql = "SELECT COUNT(ID) FROM t_KDT_GC_ToKhaiChuyenTiep WHERE IDHopDong = " + this.ID + " AND TrangThaiXuLy =" + TrangThai + " AND (MaLoaiHinh = '" + maLoaiHinh + "' or MaLoaiHinh = 'NGC18')";
            else if (maLoaiHinh.Equals("PHPLX"))
                sql = "SELECT COUNT(ID) FROM t_KDT_GC_ToKhaiChuyenTiep WHERE IDHopDong = " + this.ID + " AND TrangThaiXuLy =" + TrangThai + " AND (MaLoaiHinh = '" + maLoaiHinh + "' or MaLoaiHinh = 'XGC18')";
            else
                sql = "SELECT COUNT(ID) FROM t_KDT_GC_ToKhaiChuyenTiep WHERE IDHopDong = " + this.ID + " AND TrangThaiXuLy =" + TrangThai + " AND MaLoaiHinh LIKE '%" + maLoaiHinh + "%'";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            int temp = 0;
            try
            {
                temp = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            }
            catch { }

            return temp;
        }
        public DataSet GetSanPhamDaXuat(long IDHopDong)
        {
            string sql = "SELECT DISTINCT MaPhu FROM v_GC_HangToKhai WHERE MaLoaiHinh LIKE 'XGC%' AND LoaiHangHoa = 'S' AND IDHopDong = " + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public void UpdateMaNPLFull(string maCu, string maMoi)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "UPDATE t_GC_NguyenPhuLieu SET Ma = @maMoi WHERE Ma = @maCu AND HopDong_ID = " + this.ID;
                    SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@maMoi", SqlDbType.VarChar, maMoi);
                    db.AddInParameter(command, "@maCu", SqlDbType.VarChar, maCu);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_KDT_HangMauDich SET MaPhu = @maMoi WHERE MaPhu = @maCu " +
                                 "AND TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = " + this.ID + " AND LoaiHangHoa = 'N')";
                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@maMoi", SqlDbType.VarChar, maMoi);
                    db.AddInParameter(command, "@maCu", SqlDbType.VarChar, maCu);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_GC_NPLNhapTonThucTe SET MaNPL = @maMoi WHERE MaNPL = @maCu AND ID_HopDong =" + this.ID;

                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@maMoi", SqlDbType.VarChar, maMoi);
                    db.AddInParameter(command, "@maCu", SqlDbType.VarChar, maCu);
                    db.ExecuteNonQuery(command, transaction);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void UpdateTenNPLFull(string maNPL, string tenNPLMoi)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "UPDATE t_GC_NguyenPhuLieu SET Ten = @TenNPLMoi WHERE Ma = @MaNPL AND HopDong_ID = " + this.ID;
                    SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@TenNPLMoi", SqlDbType.NVarChar, tenNPLMoi);
                    db.AddInParameter(command, "@MaNPL", SqlDbType.VarChar, maNPL);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_KDT_HangMauDich SET TenHang = @TenNPLMoi WHERE MaPhu = @MaNPL " +
                                 "AND TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = " + this.ID + " AND LoaiHangHoa = 'N')";
                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@TenNPLMoi", SqlDbType.NVarChar, tenNPLMoi);
                    db.AddInParameter(command, "@MaNPL", SqlDbType.VarChar, maNPL);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_GC_DinhMuc SET TenNPL = @TenNPLMoi WHERE MaNguyenPhuLieu = @MaNPL AND HopDong_ID = " + this.ID;
                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@TenNPLMoi", SqlDbType.NVarChar, tenNPLMoi);
                    db.AddInParameter(command, "@MaNPL", SqlDbType.VarChar, maNPL);
                    db.ExecuteNonQuery(command, transaction);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void UpdateMaSPFull(string maCu, string maMoi)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "UPDATE t_GC_SanPham SET Ma = @maMoi WHERE Ma = @maCu AND HopDong_ID = " + this.ID;
                    SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@maMoi", SqlDbType.VarChar, maMoi);
                    db.AddInParameter(command, "@maCu", SqlDbType.VarChar, maCu);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_KDT_HangMauDich SET MaPhu = @maMoi WHERE MaPhu = @maCu " +
                                 "AND TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = " + this.ID + " AND LoaiHangHoa = 'S')";
                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@maMoi", SqlDbType.VarChar, maMoi);
                    db.AddInParameter(command, "@maCu", SqlDbType.VarChar, maCu);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_GC_DinhMuc SET MaSanPham = @maMoi WHERE MaSanPham = @maCu AND HopDong_ID =" + this.ID;

                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@maMoi", SqlDbType.VarChar, maMoi);
                    db.AddInParameter(command, "@maCu", SqlDbType.VarChar, maCu);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_KDT_GC_DinhMuc SET MaSanPham = @maMoi WHERE MaSanPham = @maCu AND Master_ID IN (SELECT ID FROM t_KDT_GC_DinhMucDangKy WHERE ID_HopDong =" + this.ID + ")";

                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@maMoi", SqlDbType.VarChar, maMoi);
                    db.AddInParameter(command, "@maCu", SqlDbType.VarChar, maCu);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_GC_PhanBoToKhaiXuat SET MaSP = @maMoi WHERE MaSP = @maCu " +
                                 "AND ID_TKMD IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = " + this.ID + " AND LoaiHangHoa = 'S')";
                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@maMoi", SqlDbType.VarChar, maMoi);
                    db.AddInParameter(command, "@maCu", SqlDbType.VarChar, maCu);
                    db.ExecuteNonQuery(command, transaction);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void UpdateTenSPFull(string maSP, string tenSPMoi)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "UPDATE t_GC_SanPham SET Ten = @TenSPMoi WHERE Ma = @MaSP AND HopDong_ID = " + this.ID;
                    SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@TenSPMoi", SqlDbType.NVarChar, tenSPMoi);
                    db.AddInParameter(command, "@MaSP", SqlDbType.VarChar, maSP);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_KDT_HangMauDich SET TenHang = @TenSPMoi WHERE MaPhu = @MaSP " +
                                 "AND TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE IDHopDong = " + this.ID + " AND LoaiHangHoa = 'S')";
                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@TenSPMoi", SqlDbType.NVarChar, tenSPMoi);
                    db.AddInParameter(command, "@MaSP", SqlDbType.VarChar, maSP);
                    db.ExecuteNonQuery(command, transaction);

                    sql = "UPDATE t_GC_DinhMuc SET TenSanPham = @TenSPMoi WHERE MaSanPham = @MaSP AND HopDong_ID = " + this.ID;
                    command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@TenSPMoi", SqlDbType.NVarChar, tenSPMoi);
                    db.AddInParameter(command, "@MaSP", SqlDbType.VarChar, maSP);
                    db.ExecuteNonQuery(command, transaction);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void UpdateSoLuongDangKySP(string maSP, decimal soLuongMoi)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "UPDATE t_GC_SanPham SET SoLuongDangKy = @SoLuongMoi WHERE Ma = @MaSP AND HopDong_ID = " + this.ID;
                    SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.AddInParameter(command, "@SoLuongMoi", SqlDbType.Decimal, soLuongMoi);
                    db.AddInParameter(command, "@MaSP", SqlDbType.VarChar, maSP);
                    db.ExecuteNonQuery(command, transaction);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public DataTable Report7_qr1()
        {
            string qr_HMD_TN = " SELECT     dbo.t_KDT_HangMauDich.ID, dbo.t_KDT_HangMauDich.TKMD_ID, dbo.t_KDT_HangMauDich.MaPhu, dbo.t_KDT_HangMauDich.TenHang, "
                                + " dbo.t_KDT_HangMauDich.DVT_ID, dbo.t_KDT_ToKhaiMauDich.SoToKhai, dbo.t_KDT_ToKhaiMauDich.NgayDangKy, "
                                + " dbo.t_KDT_HangMauDich.SoLuong, dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh "
                                + " FROM         dbo.t_KDT_HangMauDich INNER JOIN "
                                + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
                                + " WHERE     (dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa = 'T') AND (dbo.t_KDT_ToKhaiMauDich.IDHopDong = @HD_ID) AND "
                                + " (dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%NGC%')";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(qr_HMD_TN);
            db.AddInParameter(dbCommand, "@HD_ID", SqlDbType.Int, this.ID);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public DataTable Report7_qr2()
        {
            string qr_HCT_TN = " SELECT     dbo.t_KDT_GC_HangChuyenTiep.ID, dbo.t_KDT_GC_HangChuyenTiep.Master_ID, dbo.t_KDT_GC_HangChuyenTiep.MaHang, "
                                + " dbo.t_KDT_GC_HangChuyenTiep.TenHang, dbo.t_KDT_GC_HangChuyenTiep.ID_DVT, dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, "
                                + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, dbo.t_KDT_GC_HangChuyenTiep.SoLuong, dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh "
                                + " FROM         dbo.t_KDT_GC_ToKhaiChuyenTiep INNER JOIN "
                                + " dbo.t_KDT_GC_HangChuyenTiep ON dbo.t_KDT_GC_ToKhaiChuyenTiep.ID = dbo.t_KDT_GC_HangChuyenTiep.Master_ID "
                                + " WHERE     (dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @HD_ID) AND (dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%TBN%' OR dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%NGC20%')";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(qr_HCT_TN);
            db.AddInParameter(dbCommand, "@HD_ID", SqlDbType.Int, this.ID);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable Report7_qr3()
        {
            string qr_HCT_TN = " SELECT     dbo.t_KDT_HangMauDich.ID, dbo.t_KDT_HangMauDich.TKMD_ID, dbo.t_KDT_HangMauDich.MaPhu, dbo.t_KDT_HangMauDich.TenHang, "
                              + " dbo.t_KDT_HangMauDich.DVT_ID, dbo.t_KDT_ToKhaiMauDich.SoToKhai, dbo.t_KDT_ToKhaiMauDich.NgayDangKy, "
                              + " dbo.t_KDT_HangMauDich.SoLuong, dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh "
                              + " FROM         dbo.t_KDT_HangMauDich INNER JOIN "
                              + " dbo.t_KDT_ToKhaiMauDich ON dbo.t_KDT_HangMauDich.TKMD_ID = dbo.t_KDT_ToKhaiMauDich.ID "
                              + " WHERE     (dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa = 'T') AND (dbo.t_KDT_ToKhaiMauDich.IDHopDong = @HD_ID) AND "
                              + " (dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%XGC%')";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(qr_HCT_TN);
            db.AddInParameter(dbCommand, "@HD_ID", SqlDbType.Int, this.ID);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public DataTable Report7_qr4()
        {
            string qr_HCT_TN = " SELECT     dbo.t_KDT_GC_HangChuyenTiep.ID, dbo.t_KDT_GC_HangChuyenTiep.Master_ID, dbo.t_KDT_GC_HangChuyenTiep.MaHang, "
                              + " dbo.t_KDT_GC_HangChuyenTiep.TenHang, dbo.t_KDT_GC_HangChuyenTiep.ID_DVT, dbo.t_KDT_GC_ToKhaiChuyenTiep.SoToKhai, "
                              + " dbo.t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, dbo.t_KDT_GC_HangChuyenTiep.SoLuong, dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh "
                              + " FROM         dbo.t_KDT_GC_ToKhaiChuyenTiep INNER JOIN "
                              + " dbo.t_KDT_GC_HangChuyenTiep ON dbo.t_KDT_GC_ToKhaiChuyenTiep.ID = dbo.t_KDT_GC_HangChuyenTiep.Master_ID "
                              + " WHERE     (dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @HD_ID) AND (dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%TBX%' OR dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%XGC20%')";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(qr_HCT_TN);
            db.AddInParameter(dbCommand, "@HD_ID", SqlDbType.Int, this.ID);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public HopDong()
        {
            NPLCollection = new List<NguyenPhuLieu>();
            SPCollection = new SanPhamCollection();
            TBCollection = new List<ThietBi>();
            NhomSPCollection = new List<NhomSanPham>();
            HangMauCollection = new List<HangMau>();

        }

        public bool Load(SqlTransaction tran)
        {
            string spName = "p_KDT_GC_HopDong_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand, tran);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaHan"))) NgayGiaHan = reader.GetDateTime(reader.GetOrdinal("NgayGiaHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoTheoDoi"))) CanBoTheoDoi = reader.GetString(reader.GetOrdinal("CanBoTheoDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool Load(SqlTransaction tran, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_HopDong_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            IDataReader reader = null;
            if (tran != null)
                reader = this.db.ExecuteReader(dbCommand, tran);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaHan"))) NgayGiaHan = reader.GetDateTime(reader.GetOrdinal("NgayGiaHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoTheoDoi"))) CanBoTheoDoi = reader.GetString(reader.GetOrdinal("CanBoTheoDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public Company.GC.BLL.KDT.GC.HopDong LoadSoHopDongByKTX(string dbName, string soHopDong, DateTime ngayKy, string maHaiQuan, string maDoanhNghiep)
        {
            SetDabaseMoi(dbName);

            string whereCondition = string.Format("MaHaiQuan = '{0}' AND MaDoanhNghiep = '{1}' AND SoHopDong = N'{2}' AND NgayKy = '{3}'", maHaiQuan, maDoanhNghiep, soHopDong, ngayKy.ToShortDateString());
            string orderBy = "";

            HopDong entity = new HopDong();
            IDataReader reader = SelectReaderDynamic(whereCondition, orderBy);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaHan"))) entity.NgayGiaHan = reader.GetDateTime(reader.GetOrdinal("NgayGiaHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) entity.NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) entity.DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) entity.DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoTheoDoi"))) entity.CanBoTheoDoi = reader.GetString(reader.GetOrdinal("CanBoTheoDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) entity.CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                reader.Close();
                return entity;
            }
            reader.Close();
            return entity;
        }

        public Company.GC.BLL.KDT.GC.HopDong LoadSoHopDongByTQDT(string dbName, string soHopDong, DateTime ngayKy, string maHaiQuan, string maDoanhNghiep)
        {
            SetDabaseMoi(dbName);

            string whereCondition = string.Format("MaHaiQuan = '{0}' AND MaDoanhNghiep = '{1}' AND SoHopDong = N'{2}' AND NgayKy = '{3}'", maHaiQuan, maDoanhNghiep, soHopDong, ngayKy.ToShortDateString());
            string orderBy = "";

            HopDong entity = new HopDong();
            IDataReader reader = SelectReaderDynamic(whereCondition, orderBy);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaHan"))) entity.NgayGiaHan = reader.GetDateTime(reader.GetOrdinal("NgayGiaHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) entity.NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) entity.DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) entity.DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoTheoDoi"))) entity.CanBoTheoDoi = reader.GetString(reader.GetOrdinal("CanBoTheoDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) entity.CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetInt32(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) entity.HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
                reader.Close();
                return entity;
            }
            reader.Close();
            return entity;
        }
        public void SetDabaseMoi(string nameDatabase)
        {
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
        }
        public bool LoadBy(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_HopDong_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaHan"))) NgayGiaHan = reader.GetDateTime(reader.GetOrdinal("NgayGiaHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoTheoDoi"))) CanBoTheoDoi = reader.GetString(reader.GetOrdinal("CanBoTheoDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) NamTN = reader.GetInt32(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool LoadTQDT(SqlTransaction tran, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_HopDong_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            IDataReader reader = null;
            if (tran != null)
                reader = this.db.ExecuteReader(dbCommand, tran);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaHan"))) NgayGiaHan = reader.GetDateTime(reader.GetOrdinal("NgayGiaHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoTheoDoi"))) CanBoTheoDoi = reader.GetString(reader.GetOrdinal("CanBoTheoDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) NamTN = reader.GetInt32(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public HopDong LoadHopDongTQDTEntity(long idHopDong, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_HopDong_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, idHopDong);

            HopDong entity = null;
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new HopDong();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaHan"))) entity.NgayGiaHan = reader.GetDateTime(reader.GetOrdinal("NgayGiaHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) entity.NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) entity.DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) entity.DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoTheoDoi"))) entity.CanBoTheoDoi = reader.GetString(reader.GetOrdinal("CanBoTheoDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) entity.CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetInt32(reader.GetOrdinal("NamTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) entity.HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
                reader.Close();
                return entity;
            }
            reader.Close();
            return null;
        }
        public DataSet LoadHopDongTQDTDataSet(long idHopDong, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_HopDong_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            string whereCondition = "ID = " + idHopDong;
            string orderByExpression = "";

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_HopDong_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan);
            this.db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan);
            this.db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
            this.db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
            this.db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.Int, NamTN);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            this.db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertUpdateHopDong()
        {
            //bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(System.Data.IsolationLevel.Serializable);

                long id = this.ID;
                try
                {
                    if (this.ID == 0)
                        Insert(transaction);
                    else
                        Update(transaction);
                    if (ID > 0)
                    {
                        int i = 1;
                        //xoa ben npl dung cho xu ly du lieu
                        BLL.GC.NguyenPhuLieu nplXuLy = new Company.GC.BLL.GC.NguyenPhuLieu();
                        nplXuLy.HopDong_ID = this.ID;
                        nplXuLy.DeleteBy_HopDong_ID(transaction);
                        NguyenPhuLieu nplHD = new NguyenPhuLieu();
                        nplHD.HopDong_ID = this.ID;
                        nplHD.DeleteBy_HopDong_ID(transaction);
                        foreach (NguyenPhuLieu item in NPLCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.Insert(transaction);
                            //cap nhat sang du lieu xu ly
                            BLL.GC.NguyenPhuLieu entity = new Company.GC.BLL.GC.NguyenPhuLieu();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.MaHS = item.MaHS;
                            entity.DVT_ID = item.DVT_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.DonGia = item.DonGia;
                            entity.InsertTransaction(transaction);

                            NguyenPhuLieuBoSung nplBs = new NguyenPhuLieuBoSung();
                            nplBs.HopDong_ID = ID;
                            nplBs.Ma = item.Ma;
                            nplBs.NguonCungCap = item.NguonCungCap;
                            nplBs.TuCungUng = item.TuCungUng;
                            nplBs.InsertUpdate(transaction);
                        }
                        i = 1;
                        BLL.GC.NhomSanPham ItemDelete = new Company.GC.BLL.GC.NhomSanPham();
                        ItemDelete.HopDong_ID = this.ID;
                        ItemDelete.DeleteBy_HopDong_ID(transaction);
                        NhomSanPham ItemHD = new NhomSanPham();
                        ItemHD.HopDong_ID = this.ID;
                        ItemHD.DeleteBy_HopDong_ID(transaction);
                        foreach (NhomSanPham item in NhomSPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdate(transaction);

                            BLL.GC.NhomSanPham entity = new Company.GC.BLL.GC.NhomSanPham();
                            entity.HopDong_ID = this.ID;
                            entity.MaSanPham = item.MaSanPham;
                            entity.GiaGiaCong = item.GiaGiaCong;
                            entity.SoLuong = item.SoLuong;
                            entity.STTHang = item.STTHang;
                            entity.TenSanPham = item.TenSanPham;
                            entity.InsertTransaction(transaction);
                        }
                        i = 1;
                        BLL.GC.SanPham SPDelete = new Company.GC.BLL.GC.SanPham();
                        SPDelete.HopDong_ID = this.ID;
                        SPDelete.DeleteBy_HopDong_ID(transaction);

                        SanPham SPHD = new SanPham();
                        SPHD.HopDong_ID = this.ID;
                        SPHD.DeleteBy_HopDong_ID(transaction);
                        foreach (SanPham item in SPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction);

                            BLL.GC.SanPham entity = new Company.GC.BLL.GC.SanPham();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.NhomSanPham_ID = item.NhomSanPham_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.DonGia = item.DonGia;
                            entity.InsertTransaction(transaction);
                        }

                        i = 1;

                        BLL.GC.ThietBi TBDelete = new Company.GC.BLL.GC.ThietBi();
                        TBDelete.HopDong_ID = this.ID;
                        int v = TBDelete.DeleteBy_HopDong_ID(transaction);

                        ThietBi TBHD = new ThietBi();
                        TBHD.HopDong_ID = this.ID;
                        TBHD.Delete(transaction);

                        foreach (ThietBi item in TBCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdate(transaction);

                            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.DonGia = item.DonGia;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.NguyenTe_ID = item.NguyenTe_ID;
                            entity.NuocXX_ID = item.NuocXX_ID;
                            entity.TinhTrang = item.TinhTrang;
                            entity.TriGia = item.TriGia;
                            long val = entity.InsertTransaction(transaction);
                        }

                        BLL.GC.HangMau hangmauDK = new Company.GC.BLL.GC.HangMau();
                        hangmauDK.HopDong_ID = this.ID;
                        hangmauDK.DeleteBy_HopDong_ID(transaction);
                        HangMau.DeleteBy_HopDong_ID(transaction, this.ID);

                        foreach (HangMau item in HangMauCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdate(transaction);

                            hangmauDK = new Company.GC.BLL.GC.HangMau();
                            hangmauDK.HopDong_ID = this.ID;
                            hangmauDK.Ma = item.Ma;
                            hangmauDK.Ten = item.Ten;
                            hangmauDK.DVT_ID = item.DVT_ID;
                            hangmauDK.MaHS = item.MaHS;
                            hangmauDK.SoLuongDangKy = item.SoLuongDangKy;
                            hangmauDK.STTHang = item.STTHang;
                            hangmauDK.InsertUpdate(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdateHopDongDongBoDuLieu()
        {
            //bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.ID == 0)
                        Insert(transaction);
                    else
                        Update(transaction);

                    if (ID > 0)
                    {
                        int i = 1;
                        //xoa ben npl dung cho xu ly du lieu
                        BLL.GC.NguyenPhuLieu nplXuLy = new Company.GC.BLL.GC.NguyenPhuLieu();
                        nplXuLy.HopDong_ID = this.ID;
                        nplXuLy.DeleteBy_HopDong_ID(transaction);
                        NguyenPhuLieu nplHD = new NguyenPhuLieu();
                        nplHD.HopDong_ID = this.ID;
                        nplHD.DeleteBy_HopDong_ID(transaction);
                        foreach (NguyenPhuLieu item in NPLCollection)
                        {
                            try
                            {
                                item.HopDong_ID = ID;
                                item.STTHang = i++;
                                item.Insert(transaction);
                                //cap nhat sang du lieu xu ly
                                BLL.GC.NguyenPhuLieu entity = new Company.GC.BLL.GC.NguyenPhuLieu();
                                entity.HopDong_ID = this.ID;
                                entity.Ma = item.Ma;
                                entity.MaHS = item.MaHS;
                                entity.DVT_ID = item.DVT_ID;
                                entity.SoLuongDangKy = item.SoLuongDangKy;
                                entity.STTHang = item.STTHang;
                                entity.Ten = item.Ten;
                                try
                                {
                                    entity.InsertTransaction(transaction);
                                }
                                catch { }
                            }
                            catch { }
                        }
                        i = 1;
                        BLL.GC.NhomSanPham ItemDelete = new Company.GC.BLL.GC.NhomSanPham();
                        ItemDelete.HopDong_ID = this.ID;
                        ItemDelete.DeleteBy_HopDong_ID(transaction);
                        NhomSanPham ItemHD = new NhomSanPham();
                        ItemHD.HopDong_ID = this.ID;
                        ItemHD.DeleteBy_HopDong_ID(transaction);
                        foreach (NhomSanPham item in NhomSPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdate(transaction);

                            BLL.GC.NhomSanPham entity = new Company.GC.BLL.GC.NhomSanPham();
                            entity.HopDong_ID = this.ID;
                            entity.MaSanPham = item.MaSanPham;
                            entity.GiaGiaCong = item.GiaGiaCong;
                            entity.SoLuong = item.SoLuong;
                            entity.STTHang = item.STTHang;
                            entity.TenSanPham = item.TenSanPham;
                            entity.InsertTransaction(transaction);
                        }
                        i = 1;
                        BLL.GC.SanPham SPDelete = new Company.GC.BLL.GC.SanPham();
                        SPDelete.HopDong_ID = this.ID;
                        SPDelete.DeleteBy_HopDong_ID(transaction);

                        SanPham SPHD = new SanPham();
                        SPHD.HopDong_ID = this.ID;
                        SPHD.DeleteBy_HopDong_ID(transaction);
                        foreach (SanPham item in SPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction);

                            BLL.GC.SanPham entity = new Company.GC.BLL.GC.SanPham();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.NhomSanPham_ID = item.NhomSanPham_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            try
                            {
                                entity.InsertTransaction(transaction);
                            }
                            catch { }
                        }
                        i = 1;
                        BLL.GC.ThietBi TBDelete = new Company.GC.BLL.GC.ThietBi();
                        TBDelete.HopDong_ID = this.ID;
                        TBDelete.DeleteBy_HopDong_ID(transaction);

                        ThietBi TBHD = new ThietBi();
                        TBHD.HopDong_ID = this.ID;
                        TBHD.Delete(transaction);
                        foreach (ThietBi item in TBCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdate(transaction);

                            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.DonGia = item.DonGia;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.NguyenTe_ID = item.NguyenTe_ID;
                            entity.NuocXX_ID = item.NuocXX_ID;
                            entity.TinhTrang = item.TinhTrang;
                            entity.TriGia = item.TriGia;
                            try
                            {
                                entity.InsertTransaction(transaction);
                            }
                            catch { }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdateHopDongDongBoDuLieuTQDT(string dbName)
        {
            SetDabaseMoi(dbName);

            ToKhaiMauDich tkmdTemp = new ToKhaiMauDich();
            ToKhaiChuyenTiep tkctTemp = new ToKhaiChuyenTiep();

            //bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.ID == 0)
                        this.ID = InsertTransactionTQDT(transaction, dbName);
                    else
                        UpdateTransactionTQDT(transaction, dbName);

                    if (ID > 0)
                    {
                        #region Nhom SP, NPL, SP, TB
                        int i = 1;
                        //xoa ben npl dung cho xu ly du lieu
                        //BLL.GC.NguyenPhuLieu nplXuLy = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //nplXuLy.HopDong_ID = this.ID;
                        //nplXuLy.DeleteBy_HopDong_ID(transaction, dbName);

                        //BLL.KDT.GC.NguyenPhuLieu nplHD = new BLL.KDT.GC.NguyenPhuLieu();
                        //nplHD.HopDong_ID = this.ID;
                        //nplHD.DeleteBy_HopDong_ID(transaction, dbName);

                        foreach (BLL.KDT.GC.NguyenPhuLieu item in NPLCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction, dbName);

                            //cap nhat sang du lieu xu ly
                            BLL.GC.NguyenPhuLieu entity = new Company.GC.BLL.GC.NguyenPhuLieu();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.MaHS = item.MaHS;
                            entity.DVT_ID = item.DVT_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.InsertUpdateTransaction(transaction, dbName);
                        }
                        i = 1;
                        //BLL.GC.NhomSanPham ItemDelete = new Company.GC.BLL.GC.NhomSanPham();
                        //ItemDelete.HopDong_ID = this.ID;
                        //ItemDelete.DeleteBy_HopDong_ID(transaction, dbName);

                        //NhomSanPham ItemHD = new NhomSanPham();
                        //ItemHD.HopDong_ID = this.ID;
                        //ItemHD.DeleteBy_HopDong_ID(transaction, dbName);

                        foreach (NhomSanPham item in NhomSPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction, dbName);

                            BLL.GC.NhomSanPham entity = new Company.GC.BLL.GC.NhomSanPham();
                            entity.HopDong_ID = this.ID;
                            entity.MaSanPham = item.MaSanPham;
                            entity.GiaGiaCong = item.GiaGiaCong;
                            entity.SoLuong = item.SoLuong;
                            entity.STTHang = item.STTHang;
                            entity.TenSanPham = item.TenSanPham;
                            entity.InsertUpdateTransaction(transaction, dbName);
                        }
                        i = 1;
                        //BLL.GC.SanPham SPDelete = new Company.GC.BLL.GC.SanPham();
                        //SPDelete.HopDong_ID = this.ID;
                        //SPDelete.DeleteBy_HopDong_ID(transaction, dbName);

                        //SanPham SPHD = new SanPham();
                        //SPHD.HopDong_ID = this.ID;
                        //SPHD.DeleteBy_HopDong_ID(transaction, dbName);

                        foreach (SanPham item in SPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransactionTQDT(transaction, dbName);

                            BLL.GC.SanPham entity = new Company.GC.BLL.GC.SanPham();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.NhomSanPham_ID = item.NhomSanPham_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.InsertUpdateTransaction(transaction, dbName);
                        }
                        i = 1;
                        //BLL.GC.ThietBi TBDelete = new Company.GC.BLL.GC.ThietBi();
                        //TBDelete.HopDong_ID = this.ID;
                        //TBDelete.DeleteBy_HopDong_ID(transaction, dbName);

                        //ThietBi TBHD = new ThietBi();
                        //TBHD.HopDong_ID = this.ID;
                        //TBHD.DeleteTransaction(transaction, dbName);

                        foreach (ThietBi item in TBCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction, dbName);

                            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.DonGia = item.DonGia;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.NguyenTe_ID = item.NguyenTe_ID;
                            entity.NuocXX_ID = item.NuocXX_ID;
                            entity.TinhTrang = item.TinhTrang;
                            entity.TriGia = item.TriGia;
                            entity.InsertUpdateTransaction(transaction, dbName);
                        }
                        #endregion

                        #region Dinh Muc
                        foreach (DinhMucDangKy dmdk in DMDKCollection)
                        {
                            dmdk.ID = 0;
                            dmdk.ID_HopDong = this.ID;
                            dmdk.InsertUpdate(transaction, dbName);

                            DinhMucDangKy dmdkTemp = new DinhMucDangKy();
                            dmdkTemp.SoTiepNhan = dmdk.SoTiepNhan;
                            dmdkTemp.NgayTiepNhan = dmdk.NgayTiepNhan;
                            dmdkTemp.ID_HopDong = dmdk.ID_HopDong;
                            dmdkTemp.MaHaiQuan = dmdk.MaHaiQuan;
                            dmdkTemp.MaDoanhNghiep = dmdk.MaDoanhNghiep;
                            dmdkTemp.Load(transaction, dbName);

                            int cnt1 = 1;
                            foreach (DinhMuc dm in dmdk.DMCollection)
                            {
                                dm.Master_ID = dmdkTemp.ID;// dmdk.ID;
                                dm.STTHang = cnt1++;
                                dm.InsertUpdateTransaction(transaction, dbName);
                            }

                            dmdk.TransferGCTransaction(transaction, dbName);
                        }
                        #endregion

                        #region Phu kien
                        foreach (PhuKienDangKy item in PKDKCollection)
                        {
                            item.ID = 0;
                            item.HopDong_ID = this.ID;
                            item.InsertUpdate(transaction, dbName);

                            PhuKienDangKy itemTemp = new PhuKienDangKy();
                            itemTemp.SoPhuKien = item.SoPhuKien;
                            itemTemp.NgayPhuKien = item.NgayPhuKien;
                            itemTemp.HopDong_ID = item.HopDong_ID;
                            itemTemp.MaHaiQuan = item.MaHaiQuan;
                            itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                            itemTemp.Load(transaction, dbName);

                            foreach (LoaiPhuKien loaipk in item.PKCollection)
                            {
                                int stt = 1;
                                loaipk.Master_ID = itemTemp.ID;
                                loaipk.InsertUpdateTransaction(transaction, dbName);

                                LoaiPhuKien loaipkTemp = new LoaiPhuKien();
                                loaipkTemp.MaPhuKien = loaipk.MaPhuKien;
                                loaipkTemp.NoiDung = loaipk.NoiDung;
                                loaipkTemp.Master_ID = itemTemp.ID;
                                loaipkTemp.Load(transaction, dbName);

                                foreach (HangPhuKien hangpk in loaipk.HPKCollection)
                                {
                                    hangpk.STTHang = stt++;
                                    hangpk.Master_ID = loaipkTemp.ID;
                                    hangpk.InsertUpdateTransaction(transaction, dbName);
                                }
                            }
                        }
                        #endregion

                        #region To khai mau dich
                        foreach (ToKhaiMauDich item in TKMDCollection)
                        {
                            item.ID = 0;
                            item.IDHopDong = this.ID;
                            item.InsertUpdateTransaction(transaction, dbName);

                            ToKhaiMauDich itemTemp = new ToKhaiMauDich();
                            itemTemp.IDHopDong = item.IDHopDong;
                            itemTemp.SoHopDong = item.SoHopDong;
                            itemTemp.SoToKhai = item.SoToKhai;
                            itemTemp.NgayDangKy = item.NgayDangKy;
                            itemTemp.MaLoaiHinh = item.MaLoaiHinh;
                            itemTemp.MaHaiQuan = item.MaHaiQuan;
                            itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                            itemTemp.Load(transaction, dbName);

                            if (item.TrangThaiXuLy != itemTemp.TrangThaiXuLy)
                            {
                                itemTemp.TrangThaiXuLy = TrangThaiXuLy;
                                itemTemp.UpdateTransaction(transaction);
                            }

                            foreach (HangMauDich hmd in item.HMDCollection)
                            {
                                hmd.TKMD_ID = itemTemp.ID;
                                hmd.ID = hmd.InsertUpdateTransaction(transaction, dbName);
                            }
                        }
                        #endregion

                        #region To khai chuyen tiep
                        foreach (ToKhaiChuyenTiep item in TKCTCollection)
                        {
                            item.ID = 0;
                            item.IDHopDong = this.ID;
                            item.InsertUpdateTransaction(transaction, dbName);

                            ToKhaiChuyenTiep itemTemp = new ToKhaiChuyenTiep();
                            itemTemp.IDHopDong = item.IDHopDong;
                            itemTemp.SoToKhai = item.SoToKhai;
                            itemTemp.NgayDangKy = item.NgayDangKy;
                            itemTemp.MaLoaiHinh = item.MaLoaiHinh;
                            itemTemp.MaHaiQuanTiepNhan = item.MaHaiQuanTiepNhan;
                            itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                            itemTemp.Load(transaction, dbName);

                            if (item.TrangThaiXuLy != itemTemp.TrangThaiXuLy)
                            {
                                itemTemp.TrangThaiXuLy = TrangThaiXuLy;
                                itemTemp.Update(transaction);
                            }

                            foreach (HangChuyenTiep hct in item.HCTCollection)
                            {
                                hct.Master_ID = itemTemp.ID;
                                hct.ID = hct.InsertUpdate(transaction, dbName);

                            }
                        }
                        #endregion

                        #region Bang ke cung ung
                        bool isTKMD, isTKCT;
                        foreach (BKCungUngDangKy item in BKCUDKCollection)
                        {
                            tkmdTemp.SoToKhai = item.SoToKhai;
                            tkmdTemp.MaLoaiHinh = item.MaLoaiHinh;
                            tkmdTemp.NamDK = item.NamDK;
                            tkmdTemp.MaHaiQuan = item.MaHaiQuan;
                            tkmdTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                            isTKMD = tkmdTemp.LoadBySoToKhai(transaction, item.NamDK, dbName);

                            tkctTemp.SoToKhai = item.SoToKhai;
                            tkctTemp.MaLoaiHinh = item.MaLoaiHinh;
                            tkctTemp.NamDK = item.NamDK;
                            tkctTemp.MaHaiQuanTiepNhan = item.MaHaiQuan;
                            tkctTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                            isTKCT = tkctTemp.LoadBySoToKhai(transaction, item.NamDK, dbName);

                            item.ID = 0;
                            item.TKMD_ID = isTKMD == true ? tkmdTemp.ID : 0;
                            item.TKCT_ID = isTKCT == true ? tkctTemp.ID : 0;
                            item.InsertUpdateTransaction(transaction, dbName);

                            BKCungUngDangKy bkcuTemp = new BKCungUngDangKy();
                            bkcuTemp.SoTiepNhan = item.SoTiepNhan;
                            bkcuTemp.NgayTiepNhan = item.NgayTiepNhan;
                            bkcuTemp.TKMD_ID = item.TKMD_ID;
                            bkcuTemp.TKCT_ID = item.TKCT_ID;
                            bkcuTemp.MaHaiQuan = item.MaHaiQuan;
                            bkcuTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                            bkcuTemp.SoBangKe = item.SoBangKe;
                            bkcuTemp.Load(transaction, dbName);

                            foreach (SanPhanCungUng sp in item.SanPhamCungUngCollection)
                            {
                                sp.Master_ID = bkcuTemp.ID;
                                sp.ID = sp.InsertUpdateTransaction(transaction, dbName);

                                SanPhanCungUng spcuTemp = new SanPhanCungUng();
                                spcuTemp.Master_ID = bkcuTemp.ID;
                                spcuTemp.MaSanPham = sp.MaSanPham;
                                spcuTemp.Load(transaction, dbName);

                                foreach (NguyenPhuLieuCungUng npl in sp.NPLCungUngCollection)
                                {
                                    npl.Master_ID = spcuTemp.ID;
                                    npl.ID = npl.InsertUpdateTransaction(transaction, dbName);
                                }
                            }
                        }
                        #endregion
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdateHopDongDongBoDuLieuKTX(string databaseName)
        {
            SetDabaseMoi(databaseName);
            //bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    if (this.ID == 0)
                        InsertTransactionKTX(transaction, databaseName);
                    else
                        UpdateTransactionKTX(transaction, databaseName);

                    if (ID > 0)
                    {
                        //NGUYEN PHU LIEU
                        int i = 1;
                        //xoa ben npl dung cho xu ly du lieu
                        //BLL.GC.NguyenPhuLieu nplXuLy = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //nplXuLy.HopDong_ID = this.ID;
                        //nplXuLy.DeleteBy_HopDong_ID(transaction, databaseName);

                        //BLL.KDT.GC.NguyenPhuLieu nplHD = new BLL.KDT.GC.NguyenPhuLieu();
                        //nplHD.HopDong_ID = this.ID;
                        //nplHD.DeleteBy_HopDong_ID(transaction, databaseName);

                        foreach (BLL.KDT.GC.NguyenPhuLieu item in NPLCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransactionKTX(transaction, databaseName);

                            //cap nhat sang du lieu xu ly
                            BLL.GC.NguyenPhuLieu entity = new Company.GC.BLL.GC.NguyenPhuLieu();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.MaHS = item.MaHS;
                            entity.DVT_ID = item.DVT_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;

                            entity.InsertUpdateTransaction(transaction, databaseName);


                        }
                        //NHOM SAN PHAM
                        i = 1;
                        //BLL.GC.NhomSanPham ItemDelete = new Company.GC.BLL.GC.NhomSanPham();
                        //ItemDelete.HopDong_ID = this.ID;
                        //ItemDelete.DeleteBy_HopDong_ID(transaction, databaseName);

                        //NhomSanPham ItemHD = new NhomSanPham();
                        //ItemHD.HopDong_ID = this.ID;
                        //ItemHD.DeleteBy_HopDong_ID(transaction, databaseName);

                        foreach (NhomSanPham item in NhomSPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction, databaseName);

                            BLL.GC.NhomSanPham entity = new Company.GC.BLL.GC.NhomSanPham();
                            entity.HopDong_ID = this.ID;
                            entity.MaSanPham = item.MaSanPham;
                            entity.GiaGiaCong = item.GiaGiaCong;
                            entity.SoLuong = item.SoLuong;
                            entity.STTHang = item.STTHang;
                            entity.TenSanPham = item.TenSanPham;
                            entity.InsertUpdateTransaction(transaction, databaseName);
                        }
                        //SAN PHAM
                        i = 1;
                        //BLL.GC.SanPham SPDelete = new Company.GC.BLL.GC.SanPham();
                        //SPDelete.HopDong_ID = this.ID;
                        //SPDelete.DeleteBy_HopDong_ID(transaction, databaseName);

                        //SanPham SPHD = new SanPham();
                        //SPHD.HopDong_ID = this.ID;
                        //SPHD.DeleteBy_HopDong_ID(transaction, databaseName);

                        foreach (SanPham item in SPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransactionKTX(transaction, databaseName);

                            BLL.GC.SanPham entity = new Company.GC.BLL.GC.SanPham();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.NhomSanPham_ID = item.NhomSanPham_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;

                            entity.InsertUpdateTransactionKTX(transaction, databaseName);

                        }
                        //THIET BI
                        i = 1;
                        //BLL.GC.ThietBi TBDelete = new Company.GC.BLL.GC.ThietBi();
                        //TBDelete.HopDong_ID = this.ID;
                        //TBDelete.DeleteBy_HopDong_ID(transaction, databaseName);

                        //ThietBi TBHD = new ThietBi();
                        //TBHD.HopDong_ID = this.ID;
                        //TBHD.DeleteTransaction(transaction, databaseName);

                        foreach (ThietBi item in TBCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertUpdateTransaction(transaction, databaseName);

                            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.DonGia = item.DonGia;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.NguyenTe_ID = item.NguyenTe_ID;
                            entity.NuocXX_ID = item.NuocXX_ID;
                            entity.TinhTrang = item.TinhTrang;
                            entity.TriGia = item.TriGia;

                            entity.InsertUpdateTransaction(transaction, databaseName);

                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransactionTQDT(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_HopDong_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan);
            this.db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan);
            this.db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
            this.db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
            this.db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.Int, NamTN);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            this.db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_HopDong_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan);
            this.db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan);
            this.db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
            this.db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
            this.db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }
        public long InsertTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_HopDong_Insert_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan);
            this.db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan);
            this.db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
            this.db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
            this.db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransactionTQDT(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_HopDong_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan);
            this.db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan);
            this.db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
            this.db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
            this.db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            this.db.AddInParameter(dbCommand, "@NamTN", SqlDbType.Int, NamTN);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            this.db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_HopDong_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan);
            this.db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan);
            this.db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
            this.db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
            this.db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        public int UpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_HopDong_Update_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan);
            this.db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan);
            this.db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
            this.db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
            this.db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
            this.db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public HopDong copyHD()
        {
            this.LoadCollection();
            HopDong HDCopy = new HopDong();
            HDCopy.MaDoanhNghiep = this.MaDoanhNghiep;
            HDCopy.MaHaiQuan = this.MaHaiQuan;
            HDCopy.ID = 0;
            HDCopy.NPLCollection = this.NPLCollection;
            HDCopy.SPCollection = this.SPCollection;
            HDCopy.TBCollection = this.TBCollection;
            HDCopy.NhomSPCollection = this.NhomSPCollection;
            HDCopy.NPLBoSungCollection = this.NPLBoSungCollection;

            return HDCopy;
        }
        public void LoadCollection()
        {
            NhomSanPham nhomsp = new NhomSanPham();
            nhomsp.HopDong_ID = this.ID;
            NhomSPCollection = (List<NhomSanPham>)NhomSanPham.SelectCollectionBy_HopDong_ID(this.ID);
            NguyenPhuLieu npl = new NguyenPhuLieu();
            npl.HopDong_ID = this.ID;
            NPLCollection = npl.SelectCollectionBy_HopDong_ID();
            SanPham sp = new SanPham();
            sp.HopDong_ID = this.ID;
            SPCollection = sp.SelectCollectionBy_HopDong_ID();
            ThietBi tb = new ThietBi();
            tb.HopDong_ID = this.ID;
            TBCollection = tb.SelectCollectionBy_HopDong_ID();
            HangMauCollection = HangMau.SelectCollectionDynamic("HopDong_ID=" + this.ID, "");

            NPLBoSungCollection = (List<NguyenPhuLieuBoSung>)NguyenPhuLieuBoSung.SelectCollectionDynamic("HopDong_ID=" + this.ID, "");

            foreach (NguyenPhuLieu item in NPLCollection)
            {
                NguyenPhuLieuBoSung nplBs = FindNPLBoSung(item.Ma);
                item.NguonCungCap = nplBs.NguonCungCap;
                item.TuCungUng = nplBs.TuCungUng;
            }
        }
        public void LoadCollection(HopDong HD)
        {

            this.NhomSPCollection = HD.NhomSPCollection;
            this.NPLCollection = HD.NPLCollection;
            this.SPCollection = HD.SPCollection;
            this.TBCollection = HD.TBCollection;
            this.HangMauCollection = HD.HangMauCollection;
            this.NPLBoSungCollection = HD.NPLBoSungCollection;
        }
        private NguyenPhuLieuBoSung FindNPLBoSung(string maNPL)
        {
            foreach (NguyenPhuLieuBoSung item in this.NPLBoSungCollection)
            {
                if (item.Ma == maNPL)
                {
                    return item;
                }
            }

            return new NguyenPhuLieuBoSung();
        }

        public void LoadCollection(string dbName)
        {
            NhomSanPham nhomsp = new NhomSanPham();
            SetDabaseMoi(dbName);
            nhomsp.HopDong_ID = this.ID;
            NhomSPCollection = (List<NhomSanPham>)NhomSanPham.SelectCollectionBy_HopDong_ID(this.ID);
            NguyenPhuLieu npl = new NguyenPhuLieu();
            npl.HopDong_ID = this.ID;
            SetDabaseMoi(dbName);
            NPLCollection = npl.SelectCollectionBy_HopDong_ID();

            SanPham sp = new SanPham();
            sp.HopDong_ID = this.ID;
            SetDabaseMoi(dbName);
            SPCollection = sp.SelectCollectionBy_HopDong_ID();
            ThietBi tb = new ThietBi();
            tb.HopDong_ID = this.ID;
            SetDabaseMoi(dbName);
            TBCollection = tb.SelectCollectionBy_HopDong_ID();
            SetDabaseMoi(dbName);
            HangMauCollection = HangMau.SelectCollectionDynamic("HopDong_ID=" + this.ID, "");

            NPLBoSungCollection = (List<NguyenPhuLieuBoSung>)NguyenPhuLieuBoSung.SelectCollectionDynamic("HopDong_ID=" + this.ID, "");
        }

        public void LoadCollectionKTX(string dbName)
        {
            NhomSanPham nhomsp = new NhomSanPham();
            SetDabaseMoi(dbName);
            nhomsp.HopDong_ID = this.ID;
            NhomSPCollection = nhomsp.SelectCollectionBy_HopDong_ID(null, dbName);
            NguyenPhuLieu npl = new NguyenPhuLieu();
            npl.HopDong_ID = this.ID;
            SetDabaseMoi(dbName);
            NPLCollection = npl.SelectCollectionByHopDongIDByKTX(null, dbName);
            SanPham sp = new SanPham();
            sp.HopDong_ID = this.ID;
            SetDabaseMoi(dbName);
            SPCollection = sp.SelectCollectionBy_HopDong_ID_ByKTX(null, dbName);
            ThietBi tb = new ThietBi();
            tb.HopDong_ID = this.ID;
            SetDabaseMoi(dbName);
            TBCollection = tb.SelectCollectionBy_HopDong_ID(null, dbName);

            NPLBoSungCollection = (List<NguyenPhuLieuBoSung>)NguyenPhuLieuBoSung.SelectCollectionDynamic("HopDong_ID=" + this.ID, "");
        }


        public bool checkSoHopDongExit(string SoHopDong, string maHQ, string maDV)
        {
            string sql = "select sohopdong from t_KDT_GC_HopDong where SoHopDong=@SoHopDong and MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHQ);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDV);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            return true;
        }

        public bool checkSoHopDongExit(string SoHopDong, string maHQ, string maDV, string dbName)
        {
            SetDabaseMoi(dbName);

            string sql = "select sohopdong from t_KDT_GC_HopDong where SoHopDong=@SoHopDong and MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHQ);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDV);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            return true;
        }

        public bool checkIDHopDongExit(long ID, string maHQ, string maDV)
        {
            string sql = "select sohopdong from t_KDT_GC_HopDong where ID=@ID and MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHQ);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDV);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            return true;
        }

        public bool checkIDHopDongExit(long ID, string maHQ, string maDV, string dbName)
        {
            SetDabaseMoi(dbName);

            string sql = "select sohopdong from t_KDT_GC_HopDong where ID=@ID and MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHQ);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDV);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            return true;
        }

        public long GetIDHopDongExit(string SoHopDong, string maHQ, string maDV, DateTime NgayKy)
        {
            string sql = "select ID from t_KDT_GC_HopDong where SoHopDong=@SoHopDong and MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan and NgayKy=@NgayKy";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHQ);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDV);
            db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }

        public DataSet GetHopDongFromUserNameKhaiBao(string userName)
        {
            string query = "SELECT * FROM dbo.t_KDT_GC_HopDong WHERE ID IN (SELECT ID_DK FROM dbo.t_KDT_SXXK_LogKhaiBao " +
                           "WHERE LoaiKhaiBao = 'HD' AND UserNameKhaiBao = '" + userName + "')";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }

        public DataSet GetHopDongFromUserNameKhaiBao(string userName, string soHopDong)
        {
            string query = "SELECT * FROM dbo.t_KDT_GC_HopDong WHERE ID IN (SELECT ID_DK FROM dbo.t_KDT_SXXK_LogKhaiBao " +
                           "WHERE LoaiKhaiBao = 'HD' AND UserNameKhaiBao = '" + userName + "') AND SoHopDong LIKE '%" + soHopDong + "%'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }

        public long GetIDHopDongExit(string SoHopDong, string maHQ, string maDV, DateTime NgayKy, string dbName)
        {
            SetDabaseMoi(dbName);

            string sql = "select ID from t_KDT_GC_HopDong where SoHopDong=@SoHopDong and MaDoanhNghiep=@MaDoanhNghiep and MaHaiQuan=@MaHaiQuan and NgayKy=@NgayKy";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHQ);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDV);
            db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy);
            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }

        public Company.GC.BLL.GC.NguyenPhuLieuCollection GetNPL()
        {
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            return npl.SelectCollectionDynamic1(this.ID);
            //return npl.SelectCollectionDynamic_FULL(this.ID);
        }
        public DataSet GetNPLDS()
        {
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            return npl.SelectDynamic1(this.ID);
        }
        public DataSet GetNPLDS(string databaseName)
        {
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            return npl.SelectDynamic1(this.ID, databaseName);
        }

        public DataSet GetHMDHD()
        {
            string sql = "SELECT     dbo.t_KDT_HangMauDich.* " +
                         "FROM         dbo.t_KDT_ToKhaiMauDich INNER JOIN " +
                        "dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID " +
                        "WHERE     dbo.t_KDT_ToKhaiMauDich.idhopdong=" + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetHMDHD(string databaseName)
        {
            string sql = "SELECT     dbo.t_KDT_HangMauDich.* " +
                         "FROM         dbo.t_KDT_ToKhaiMauDich INNER JOIN " +
                        "dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID " +
                        "WHERE     dbo.t_KDT_ToKhaiMauDich.idhopdong=" + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetNPLDST()
        {
            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
            npl.SetDabaseMoi("MSSQL");
            return npl.SelectDynamic1(this.ID);
        }
        public bool DeleteHopDongGC(long idHD)
        {
            string sqlCmd = @"  DELETE from t_GC_NguyenPhuLieu where HopDong_ID =@HopDong_ID
                                DELETE FROM t_GC_NhomSanPham WHERE HopDong_ID=@HopDong_ID
                                DELETE FROM t_GC_SanPham WHERE  HopDong_ID=@HopDong_ID
                                DELETE FROM t_GC_ThietBi WHERE  HopDong_ID=@HopDong_ID
                                DELETE FROM t_GC_DinhMuc WHERE  HopDong_ID=@HopDong_ID
                                DELETE FROM t_KDT_GC_PhuKienDangKy WHERE HopDong_ID=@HopDong_ID
                                DELETE from t_KDT_GC_BKCungUngDangKy where TrangThaiXuLy=1 and TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE TrangThaiXuLy=1 and IDHopDong =@HopDong_ID)
                                DELETE from t_KDT_ToKhaiMauDich where TrangThaiXuLy = 1 and  IDHopDong = @HopDong_ID
                                DELETE from t_KDT_GC_ToKhaiChuyenTiep where TrangThaiXuLy = 1 and  IDHopDong =@HopDong_ID                                                                  
                                DELETE FROM t_KDT_GC_HopDong WHERE ID = @HopDong_ID";
            bool ret = false;
            string[] sqlDelete = sqlCmd.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    foreach (string item in sqlDelete)
                    {
                        SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(item.Trim());
                        this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, idHD);
                        db.ExecuteNonQuery(dbCommand, transaction);
                    }
                    ret = true;
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);

                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;

        }
        public DataSet GetNPLDST_TQDT(string dbName)
        {
            Company.GC.BLL.KDT.GC.NguyenPhuLieu npl = new Company.GC.BLL.KDT.GC.NguyenPhuLieu();
            npl.SetDabaseMoi(dbName);
            npl.HopDong_ID = this.ID;
            return npl.SelectBy_HopDong_ID();
        }
        public Company.GC.BLL.GC.SanPhamCollection GetSP()
        {
            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
            return sp.SelectCollectionDynamic("HopDong_ID = " + this.ID, "Ma");

        }

        public DataSet GetSPDS()
        {
            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
            return sp.SelectDynamic("HopDong_ID = " + this.ID, "Ma");
        }
        public DataSet GetSPDS(string databaseName)
        {
            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();

            return sp.SelectDynamic("HopDong_ID = " + this.ID, "Ma", databaseName);
        }
        public DataSet GetSPDST()
        {
            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
            sp.SetDabaseMoi("MSSQL");
            return sp.SelectDynamic("HopDong_ID = " + this.ID, "Ma");
        }
        public DataSet GetSPDST_TQDT(string dbName)
        {
            Company.GC.BLL.KDT.GC.SanPham sp = new Company.GC.BLL.KDT.GC.SanPham();
            sp.SetDabaseMoi(dbName);
            sp.HopDong_ID = this.ID;
            return sp.SelectBy_HopDong_ID();
        }
        public Company.GC.BLL.GC.ThietBiCollection GetTB()
        {
            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
            return tb.SelectCollectionDynamic("HopDong_ID = " + this.ID, "Ma");

        }
        public List<Company.GC.BLL.GC.HangMau> GetHM()
        {
            return Company.GC.BLL.GC.HangMau.SelectCollectionDynamic("HopDong_ID = " + this.ID, "Ma");

        }
        public DataSet GetTBDS()
        {
            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
            return tb.SelectDynamic("HopDong_ID = " + this.ID, "Ma");

        }
        public DataSet GetTBDS(string databaseName)
        {
            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();

            return tb.SelectDynamic("HopDong_ID = " + this.ID, "Ma", databaseName);

        }
        public DataSet GetTBDST()
        {
            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
            tb.SetDabaseMoi("MSSQL");
            return tb.SelectDynamic("HopDong_ID = " + this.ID, "Ma");

        }
        public DataSet GetTBDST_TQDT(string dbName)
        {
            Company.GC.BLL.KDT.GC.ThietBi tb = new Company.GC.BLL.KDT.GC.ThietBi();
            tb.SetDabaseMoi(dbName);
            tb.HopDong_ID = this.ID;
            return tb.SelectBy_HopDong_ID();

        }
        public List<NhomSanPham> GetLoaiSP()
        {
            return NhomSanPham.SelectCollectionDynamic("HopDong_ID = " + this.ID, "MaSanPham");

        }
        public DataSet GetLoaiSPDS()
        {
            Company.GC.BLL.GC.NhomSanPham nhomSP = new Company.GC.BLL.GC.NhomSanPham();
            return nhomSP.SelectDynamic("HopDong_ID = " + this.ID, "MaSanPham");
        }
        public DataSet GetLoaiSPDS(string databaseName)
        {
            Company.GC.BLL.GC.NhomSanPham nhomSP = new Company.GC.BLL.GC.NhomSanPham();

            return nhomSP.SelectDynamic("HopDong_ID = " + this.ID, "MaSanPham", databaseName);
        }
        public DataSet GetLoaiSPDST()
        {
            Company.GC.BLL.GC.NhomSanPham nhomSP = new Company.GC.BLL.GC.NhomSanPham();
            nhomSP.SetDabaseMoi("MSSQL");
            return nhomSP.SelectDynamic("HopDong_ID = " + this.ID, "MaSanPham");
        }
        public DataSet GetLoaiSPDST_TQDT(string dbName)
        {
            Company.GC.BLL.KDT.GC.NhomSanPham nhomSP = new Company.GC.BLL.KDT.GC.NhomSanPham();
            nhomSP.SetDabaseMoi(dbName);
            nhomSP.HopDong_ID = this.ID;
            return NhomSanPham.SelectBy_HopDong_ID(this.ID);
        }
        public Company.GC.BLL.GC.DinhMucCollection GetDinhMuc()
        {
            Company.GC.BLL.GC.DinhMuc dmuc = new Company.GC.BLL.GC.DinhMuc();
            return dmuc.SelectCollectionDynamic("HopDong_ID = " + this.ID, "MaSanPham");

        }
        public DataSet GetDinhMucDS()
        {
            Company.GC.BLL.GC.DinhMuc dmuc = new Company.GC.BLL.GC.DinhMuc();
            return dmuc.SelectDynamic("HopDong_ID = " + this.ID, "MaSanPham");
        }
        public DataSet GetDinhMucDS(string databaseName)
        {
            Company.GC.BLL.GC.DinhMuc dmuc = new Company.GC.BLL.GC.DinhMuc();

            return dmuc.SelectDynamic("HopDong_ID = " + this.ID, "MaSanPham", databaseName);
        }
        public DataSet GetDinhMucDST()
        {
            Company.GC.BLL.GC.DinhMuc dmuc = new Company.GC.BLL.GC.DinhMuc();
            dmuc.SetDabaseMoi("MSSQL");
            return dmuc.SelectDynamic("HopDong_ID = " + this.ID, "MaSanPham");
        }
        public DataSet GetDinhMucDST(string databaseName)
        {
            Company.GC.BLL.GC.DinhMuc dmuc = new Company.GC.BLL.GC.DinhMuc();
            dmuc.SetDabaseMoi(databaseName);
            return dmuc.SelectDynamic("HopDong_ID = " + this.ID, "MaSanPham");
        }
        public DataSet GetDinhMucDangKy_TQDT()
        {
            Company.GC.BLL.KDT.GC.DinhMucDangKy dmuc = new Company.GC.BLL.KDT.GC.DinhMucDangKy();
            dmuc.SetDabaseMoi("MSSQL");
            dmuc.ID_HopDong = this.ID;
            return dmuc.SelectBy_ID_HopDong();
        }
        public ToKhaiMauDichCollection GetTKNK()
        {
            Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();
            return tk.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND MaLoaiHinh Like 'N%'", "");
        }
        public ToKhaiMauDichCollection GetTKMD()
        {
            Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();

            return tk.SelectCollectionDynamic("IDHopDong = " + this.ID, "");
        }
        public DataSet GetTKMDDS()
        {
            string sql = "Select year(NgayDangKy) as NamDK, *  from t_KDT_ToKhaiMauDich where TrangThaiXuLy = 1 and  IDHopDong = " + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetTKMDDS(string databaseName)
        {
            string sql = "Select year(NgayDangKy) as NamDK, *  from t_KDT_ToKhaiMauDich where TrangThaiXuLy = 1 and  IDHopDong = " + this.ID;
            //Updated by HUNGTQ, 06/05/2011.
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetTKMDDST()
        {
            string sql = "Select year(NgayDangKy) as NamDK, * from t_KDT_ToKhaiMauDich where TrangThaiXuLy = 1 and  IDHopDong = " + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }
        public DataSet GetTKMDDST(string databaseName)
        {
            string sql = "Select year(NgayDangKy) as NamDK, * from t_KDT_ToKhaiMauDich where TrangThaiXuLy = 1 and  IDHopDong = " + this.ID;
            //Updated by HUNGTQ, 06/05/2011.
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }
        public List<ToKhaiChuyenTiep> GetTKCT()
        {
            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID, "");
        }
        public List<ToKhaiChuyenTiep> GetTKCT(string databaseName)
        {

            //Updated by HUNGTQ, 06/05/2011.
            SetDabaseMoi(databaseName);

            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID, "");
        }
        public DataSet GetTKCTDS()
        {
            string sql = "Select *, year(NgayDangKy) as NamDK from t_KDT_GC_ToKhaiChuyenTiep where TrangThaiXuLy = 1 and  IDHopDong = " + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetTKCTDS(string databaseName)
        {
            string sql = "Select *, year(NgayDangKy) as NamDK from t_KDT_GC_ToKhaiChuyenTiep where TrangThaiXuLy = 1 and  IDHopDong = " + this.ID;
            //Updated by HUNGTQ, 06/05/2011.
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetTKCTDST()
        {
            string sql = "Select *, year(NgayDangKy) as NamDK from t_KDT_GC_ToKhaiChuyenTiep where TrangThaiXuLy = 1 and  IDHopDong = " + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetTKCTDST(string databaseName)
        {
            string sql = "Select *, year(NgayDangKy) as NamDK from t_KDT_GC_ToKhaiChuyenTiep where TrangThaiXuLy = 1 and  IDHopDong = " + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public ToKhaiMauDichCollection GetTKNKBCNew()
        {
            Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();
            return tk.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND MaLoaiHinh Like 'NGC%' AND TrangThaiXuLy =1 AND LoaiHangHoa = 'N'", "");
        }

        public ToKhaiMauDichCollection GetTKNKBC03New()
        {
            Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();
            return tk.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND MaLoaiHinh Like 'XGC%' AND TrangThaiXuLy =1 AND LoaiHangHoa = 'N'", "");
        }

        public ToKhaiMauDichCollection GetTKXKBCSanPhamNew()
        {
            Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();
            return tk.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND MaLoaiHinh Like 'XGC%' AND TrangThaiXuLy =1 AND LoaiHangHoa = 'S'", "");
        }

        public DataSet GetTKNKNew()
        {
            return Company.GC.BLL.KDT.ToKhaiMauDich.GetToKhaiNK(this.ID);
        }

        public ToKhaiMauDichCollection GetTKXK()
        {
            Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();
            return tk.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND MaLoaiHinh Like 'X%'  ", " NgayDangKy, SoToKhai");
        }

        public ToKhaiMauDichCollection GetTKXKPhanBo()
        {
            Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();
            return tk.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND MaLoaiHinh Like 'X%' and LoaiHangHoa='S' ", " NgayDangKy, SoToKhai");
        }
        public Company.GC.BLL.GC.NguyenPhuLieuCollection GetNPLCungUngPhanBo()
        {
            return new Company.GC.BLL.GC.NguyenPhuLieu().SelectCollectionDynamic(" HopDong_ID = " + this.ID + "AND (SoLuongCungUng >0 OR Ma IN (SELECT DISTINCT MaNPL FROM t_GC_PhanBoToKhaiNhap WHERE MaLoaiHinhNhap LIKE 'NSX%') )", " Ma");
        }
        public DataSet GetSanPhamHopDongCoDinhMuc()
        {
            string sql = "Select * from v_SanPhamHopDongCoDinhMuc where HopDong_ID = " + this.ID;
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public PhuKienDangKyCollection GetPK()
        {
            Company.GC.BLL.KDT.GC.PhuKienDangKy pk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
            return pk.SelectCollectionDynamic("HopDong_ID = " + this.ID, "");
        }
        public DataSet GetPKDS()
        {
            Company.GC.BLL.KDT.GC.PhuKienDangKy pk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
            return pk.SelectDynamic("HopDong_ID = " + this.ID, "");
        }
        public DataSet GetPKDS(string databaseName)
        {
            Company.GC.BLL.KDT.GC.PhuKienDangKy pk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();

            return pk.SelectDynamic("HopDong_ID = " + this.ID, "", databaseName);
        }
        public DataSet GetPKDST()
        {
            Company.GC.BLL.KDT.GC.PhuKienDangKy pk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
            pk.SetDabaseMoi("MSSQL");
            return pk.SelectDynamic("HopDong_ID = " + this.ID, "");
        }
        public DataSet GetPKDST(string databaseName)
        {
            Company.GC.BLL.KDT.GC.PhuKienDangKy pk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
            pk.SetDabaseMoi(databaseName);
            return pk.SelectDynamic("HopDong_ID = " + this.ID, "");
        }
        public List<ToKhaiChuyenTiep> GetTKCTNhap()
        {

            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND MaLoaiHinh LIKE '%N%'", "");
        }
        public List<ToKhaiChuyenTiep> GetTKCTXuat()
        {

            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND MaLoaiHinh LIKE '%X%'", "");
        }
        public List<ToKhaiChuyenTiep> GetTKCTXuatSPNPL()
        {
            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND (MaLoaiHinh = 'PHPLX' OR MaLoaiHinh = 'PHSPX' OR MaLoaiHinh = 'XGC18' OR MaLoaiHinh = 'XGC19' )", "");
        }

        public List<ToKhaiChuyenTiep> GetTKCTXuatSPNPLPhanBo()
        {
            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND (MaLoaiHinh = 'PHPLX' OR MaLoaiHinh = 'PHSPX' OR MaLoaiHinh = 'XGC18' OR MaLoaiHinh = 'XGC19')", "");
        }

        public List<ToKhaiChuyenTiep> GetTKCTXuatNPL()
        {
            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND (MaLoaiHinh = 'PHPLX' OR MaLoaiHinh = 'XGC18') AND TrangThaiXuLy =1", "");
        }

        public List<ToKhaiChuyenTiep> GetTKCTXuatSP()
        {

            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND (MaLoaiHinh = 'PHSPX' OR MaLoaiHinh = 'XGC19') AND TrangThaiXuLy =1", "");
        }

        public List<ToKhaiChuyenTiep> GetTKCTNhapNPLPhanBo()
        {
            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND (MaLoaiHinh = 'PHSPN' OR MaLoaiHinh = 'NGC19') AND TrangThaiXuLy =1", "");
        }

        public List<ToKhaiChuyenTiep> GetTKCTNhapNPL()
        {
            return Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.SelectCollectionDynamic("IDHopDong = " + this.ID + " AND (MaLoaiHinh = 'PHPLN' OR MaLoaiHinh = 'PHSPN' OR MaLoaiHinh = 'NGC18' OR MaLoaiHinh = 'NGC19') AND TrangThaiXuLy =1 ", "");
        }

        public BKCungUngDangKyCollection GetNPLCU()
        {
            Company.GC.BLL.KDT.GC.BKCungUngDangKy bkCU = new Company.GC.BLL.KDT.GC.BKCungUngDangKy();
            return bkCU.SelectCollectionDynamic("TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE IDHopDong =" + this.ID + ")", "");
        }
        public DataSet GetNPLCUDS()
        {
            string sql = "select *,year(NgayTiepNhan) as NamTN from t_KDT_GC_BKCungUngDangKy where TrangThaiXuLy=1 and TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE TrangThaiXuLy=1 and IDHopDong =" + this.ID + ")";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetNPLCUDS(string databaseName)
        {
            string sql = "select *,year(NgayTiepNhan) as NamTN from t_KDT_GC_BKCungUngDangKy where TrangThaiXuLy=1 and TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE TrangThaiXuLy=1 and IDHopDong =" + this.ID + ")";
            //Updated by HUNGTQ, 06/05/2011.
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetNPLCUDST()
        {
            string sql = "select *,year(NgayTiepNhan) as NamTN from t_KDT_GC_BKCungUngDangKy where TrangThaiXuLy=1 and TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE TrangThaiXuLy=1 and IDHopDong =" + this.ID + ")";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetNPLCUDST(string databaseName)
        {
            string sql = "select *,year(NgayTiepNhan) as NamTN from t_KDT_GC_BKCungUngDangKy where TrangThaiXuLy=1 and TKMD_ID IN (SELECT ID FROM t_KDT_ToKhaiMauDich WHERE TrangThaiXuLy=1 and IDHopDong =" + this.ID + ")";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(databaseName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataTable GetToKhaiNhapNPL(string maNPL)
        {
            //string sql = "SELECT tkmd.ID,CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai) END AS SoToKhaiVNACCS,tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaPhu, hmd.SoLuong  " +
            //             "FROM t_KDT_ToKhaiMauDich tkmd INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
            //             "WHERE hmd.MaPhu = @MaPhu AND tkmd.MaLoaiHinh LIKE 'N%' AND tkmd.LoaiHangHoa = 'N' AND tkmd.IDHopDong=" + this.ID + " AND tkmd.TrangThaiXuLy not in (10,11)";
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            //return db.ExecuteDataSet(dbCommand).Tables[0];

            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapNPL]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.NVarChar, this.ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, maNPL);
            return db.ExecuteDataSet(dbCommand).Tables[0];

        }
        public DataTable GetToKhaiTaiXuatNPL(string maNPL)
        {
            //string sql = "SELECT tkmd.ID,tkmd.SoToKhai,CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai) END AS SoToKhaiVNACCS,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaPhu, hmd.SoLuong  " +
            //             "FROM t_KDT_ToKhaiMauDich tkmd INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
            //             "WHERE hmd.MaPhu = @MaPhu AND tkmd.MaLoaiHinh LIKE 'X%' AND tkmd.LoaiHangHoa = 'N' AND tkmd.IDHopDong=" + this.ID + " AND tkmd.TrangThaiXuLy not in (10,11)";
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            //return db.ExecuteDataSet(dbCommand).Tables[0];

            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiTaiXuatNPL]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this.ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, maNPL);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable GetToKhaiXuatSP(string maSP)
        {
            //string sql = "SELECT tkmd.ID,CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai) END AS SoToKhaiVNACCS,tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaPhu, hmd.SoLuong  " +
            //             "FROM t_KDT_ToKhaiMauDich tkmd INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
            //             "WHERE hmd.MaPhu = @MaPhu AND tkmd.MaLoaiHinh LIKE 'X%' AND tkmd.LoaiHangHoa = 'S' AND tkmd.IDHopDong=" + this.ID + " AND tkmd.TrangThaiXuLy not in (10,11)";
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maSP);
            //return db.ExecuteDataSet(dbCommand).Tables[0];

            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatSanPham]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this.ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, maSP);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable GetToKhaiNhapTB(string maTB)
        {
            //string sql = "SELECT tkmd.ID,tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaPhu, hmd.SoLuong  " +
            //             "FROM t_KDT_ToKhaiMauDich tkmd INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
            //             "WHERE hmd.MaPhu = @MaPhu AND tkmd.MaLoaiHinh LIKE 'N%' AND tkmd.LoaiHangHoa = 'T' AND tkmd.IDHopDong=" + this.ID + " AND tkmd.TrangThaiXuLy not in (10,11)"; ;
            //TODO: Hungtq, 18/10/2014. Bo sung lay thong tin so to khai vnaccs
            //string sql = "SELECT tkmd.ID, " +
            //             "CASE WHEN SUBSTRING(tkmd.MaLoaiHinh, 2, 1) = 'V' THEN v.SoTKVNACCS ELSE tkmd.SoToKhai END AS SoToKhai, " +
            //             "tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaPhu, hmd.SoLuong  " +
            //             "FROM t_KDT_ToKhaiMauDich tkmd INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
            //             "LEFT JOIN dbo.t_VNACCS_CapSoToKhai v ON tkmd.SoToKhai = v.SoTK AND tkmd.NamDK = v.NamDangKy AND RIGHT(tkmd.MaLoaiHinh, 3) = v.MaLoaiHinh " +
            //             "WHERE hmd.MaPhu = @MaPhu AND tkmd.MaLoaiHinh LIKE 'N%' AND tkmd.LoaiHangHoa = 'T' AND tkmd.IDHopDong=" + this.ID + " AND tkmd.TrangThaiXuLy not in (10,11)"; ;
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maTB);
            //return db.ExecuteDataSet(dbCommand).Tables[0];

            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiNhapTB]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this.ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, maTB);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable GetToKhaiXuatTB(string maTB)
        {
            //string sql = "SELECT tkmd.ID,tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaPhu, hmd.SoLuong " +
            //             "FROM t_KDT_ToKhaiMauDich tkmd INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
            //             "WHERE hmd.MaPhu = @MaPhu AND tkmd.MaLoaiHinh LIKE 'X%' AND tkmd.LoaiHangHoa = 'T' AND tkmd.IDHopDong=" + this.ID
            //              + " AND tkmd.TrangThaiXuLy not in (10,11)";
            //TODO: Hungtq, 18/10/2014. Bo sung lay thong tin so to khai vnaccs
            //string sql = "SELECT tkmd.ID, " +
            //             "CASE WHEN SUBSTRING(tkmd.MaLoaiHinh, 2, 1) = 'V' THEN v.SoTKVNACCS ELSE tkmd.SoToKhai END AS SoToKhai, " +
            //             "tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaPhu, hmd.SoLuong " +
            //             "FROM t_KDT_ToKhaiMauDich tkmd INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
            //             "LEFT JOIN dbo.t_VNACCS_CapSoToKhai v ON tkmd.SoToKhai = v.SoTK AND tkmd.NamDK = v.NamDangKy AND RIGHT(tkmd.MaLoaiHinh, 3) = v.MaLoaiHinh " +
            //             "WHERE hmd.MaPhu = @MaPhu AND tkmd.MaLoaiHinh LIKE 'X%' AND tkmd.LoaiHangHoa = 'T' AND tkmd.IDHopDong=" + this.ID
            //              + " AND tkmd.TrangThaiXuLy not in (10,11)";
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maTB);
            //return db.ExecuteDataSet(dbCommand).Tables[0];

            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectToKhaiXuatTB]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this.ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, maTB);
            return db.ExecuteDataSet(dbCommand).Tables[0];


        }
        public DataTable GetToKhaiChuyenTiepNPL(string maNPL)
        {
            //string sql = "SELECT tkmd.ID,CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.HuongDan_PL ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai) END AS SoToKhaiVNACCS,tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaHang as MaPhu, hmd.SoLuong " +
            //             "FROM t_KDT_GC_ToKhaiChuyenTiep tkmd INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID " +
            //             "WHERE hmd.MaHang = @MaPhu AND ( tkmd.MaLoaiHinh LIKE '%PL%' OR tkmd.MaLoaiHinh LIKE '%18%' OR tkmd.MaLoaiHinh like '%V%' ) AND tkmd.IDHopDong=" + this.ID + " AND tkmd.TrangThaiXuLy not in (10,11)"; //Bổ sung ko lấy tờ khai hủy - Khanhhn - 11/01/2014
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            //return db.ExecuteDataSet(dbCommand).Tables[0];

            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepNPL]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this.ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, maNPL);
            return db.ExecuteDataSet(dbCommand).Tables[0];

        }
        public DataTable GetToKhaiChuyenTiepSP(string maSP)
        {
            //string sql = "SELECT tkmd.ID,CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.HuongDan_PL ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai) END AS SoToKhaiVNACCS,tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaHang as MaPhu, hmd.SoLuong " +
            //             "FROM t_KDT_GC_ToKhaiChuyenTiep tkmd INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID " +
            //             "WHERE hmd.MaHang = @MaPhu AND ( tkmd.MaLoaiHinh LIKE '%SP%' OR tkmd.MaLoaiHinh LIKE '%19%') AND tkmd.IDHopDong=" + this.ID + " AND tkmd.TrangThaiXuLy not in (10,11)";
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maSP);
            //return db.ExecuteDataSet(dbCommand).Tables[0];

            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepSP]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this.ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, maSP);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        //TODO:, 20/10/2014. Bo sung moi
        public DataTable GetToKhaiChuyenTiepTB(string maTB)
        {
            //string sql = "SELECT tkmd.ID,CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.HuongDan_PL ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai) END AS SoToKhaiVNACCS,tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaHang as MaPhu, hmd.SoLuong " +
            //             "FROM t_KDT_GC_ToKhaiChuyenTiep tkmd INNER JOIN t_KDT_GC_HangChuyenTiep hmd ON tkmd.ID = hmd.Master_ID " +
            //             "WHERE hmd.MaHang = @MaPhu AND ( tkmd.MaLoaiHinh LIKE '%TB%' OR tkmd.MaLoaiHinh LIKE '%20%') AND tkmd.IDHopDong=" + this.ID + " AND tkmd.TrangThaiXuLy not in (10,11)";
            //SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maTB);
            //return db.ExecuteDataSet(dbCommand).Tables[0];

            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectToKhaiChuyenTiepTB]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this.ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, maTB);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable GetToKhaiXuatNPL()
        {
            string sql = "";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }


        //cap nhat lai thong tin hop dong sau khi dong bo
        public void InsertUpdateHopDongXuLyDuLieu(SqlTransaction transaction)
        {
            BLL.GC.NguyenPhuLieu nplXuLy = new Company.GC.BLL.GC.NguyenPhuLieu();
            nplXuLy.HopDong_ID = this.ID;
            nplXuLy.DeleteBy_HopDong_ID(transaction);
            NguyenPhuLieu nplHD = new NguyenPhuLieu();
            nplHD.HopDong_ID = this.ID;
            nplHD.DeleteBy_HopDong_ID(transaction);
            int i = 1;
            foreach (NguyenPhuLieu item in NPLCollection)
            {
                item.STTHang = i++;
                //cap nhat sang du lieu xu ly
                BLL.GC.NguyenPhuLieu entity = new Company.GC.BLL.GC.NguyenPhuLieu();
                entity.HopDong_ID = this.ID;
                entity.Ma = item.Ma;
                entity.MaHS = item.MaHS;
                entity.DVT_ID = item.DVT_ID;
                entity.SoLuongDangKy = item.SoLuongDangKy;
                entity.STTHang = item.STTHang;
                entity.Ten = item.Ten;
                entity.InsertTransaction(transaction);
            }
            i = 1;
            BLL.GC.NhomSanPham ItemDelete = new Company.GC.BLL.GC.NhomSanPham();
            ItemDelete.HopDong_ID = this.ID;
            ItemDelete.DeleteBy_HopDong_ID(transaction);
            NhomSanPham ItemHD = new NhomSanPham();
            ItemHD.HopDong_ID = this.ID;
            ItemHD.DeleteBy_HopDong_ID(transaction);
            foreach (NhomSanPham item in NhomSPCollection)
            {
                item.STTHang = i++;
                BLL.GC.NhomSanPham entity = new Company.GC.BLL.GC.NhomSanPham();
                entity.HopDong_ID = this.ID;
                entity.MaSanPham = item.MaSanPham;
                entity.GiaGiaCong = item.GiaGiaCong;
                entity.SoLuong = item.SoLuong;
                entity.STTHang = item.STTHang;
                entity.TenSanPham = item.TenSanPham;
                entity.InsertTransaction(transaction);
            }
            i = 1;
            BLL.GC.SanPham SPDelete = new Company.GC.BLL.GC.SanPham();
            SPDelete.HopDong_ID = this.ID;
            SPDelete.DeleteBy_HopDong_ID(transaction);

            SanPham SPHD = new SanPham();
            SPHD.HopDong_ID = this.ID;
            SPHD.DeleteBy_HopDong_ID(transaction);
            foreach (SanPham item in SPCollection)
            {
                item.STTHang = i++;

                BLL.GC.SanPham entity = new Company.GC.BLL.GC.SanPham();
                entity.HopDong_ID = this.ID;
                entity.Ma = item.Ma;
                entity.DVT_ID = item.DVT_ID;
                entity.MaHS = item.MaHS;
                entity.NhomSanPham_ID = item.NhomSanPham_ID;
                entity.SoLuongDangKy = item.SoLuongDangKy;
                entity.STTHang = item.STTHang;
                entity.Ten = item.Ten;
                entity.InsertTransaction(transaction);
            }
            i = 1;
            BLL.GC.ThietBi TBDelete = new Company.GC.BLL.GC.ThietBi();
            TBDelete.HopDong_ID = this.ID;
            TBDelete.DeleteBy_HopDong_ID(transaction);

            ThietBi TBHD = new ThietBi();
            TBHD.HopDong_ID = this.ID;
            TBHD.Delete(transaction);
            foreach (ThietBi item in TBCollection)
            {
                item.STTHang = i++;

                BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                entity.HopDong_ID = this.ID;
                entity.Ma = item.Ma;
                entity.DVT_ID = item.DVT_ID;
                entity.MaHS = item.MaHS;
                entity.DonGia = item.DonGia;
                entity.SoLuongDangKy = item.SoLuongDangKy;
                entity.STTHang = item.STTHang;
                entity.Ten = item.Ten;
                entity.NguyenTe_ID = item.NguyenTe_ID;
                entity.NuocXX_ID = item.NuocXX_ID;
                entity.TinhTrang = item.TinhTrang;
                entity.TriGia = item.TriGia;
                entity.InsertTransaction(transaction);
            }
        }
        //xu ly du lieu sau khi dong bo
        public void XuLyHopDong()
        {

            HopDong.Load(this.ID);
            this.LoadCollection();
            PhuKienDangKy PhuKienDK = new PhuKienDangKy();
            PhuKienDK.HopDong_ID = this.ID;
            PhuKienDangKyCollection PhuKienCollection = PhuKienDK.SelectCollectionDynamic("HopDong_ID=" + this.ID, "NgayPhuKien");
            foreach (PhuKienDangKy pkdk in PhuKienCollection)
            {
                pkdk.LoadCollection();
                foreach (LoaiPhuKien LoaiPK in pkdk.PKCollection)
                {
                    LoaiPK.LoadCollection();
                }
            }
            //ToKhaiChuyenTiep TKCT=new ToKhaiChuyenTiep();
            //TKCT.IDHopDong=this.ID;
            //List<ToKhaiChuyenTiep> TKCTCollection = TKCT.SelectCollectionBy_IDHopDong();
            //foreach (ToKhaiChuyenTiep tkct in TKCTCollection)
            //{
            //    tkct.LoadHCTCollection();
            //}

            //ToKhaiMauDich TKMD = new ToKhaiMauDich();
            //TKMD.IDHopDong = this.ID;
            //ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionBy_IDHopDong();            
            //foreach (ToKhaiMauDich tkmd in TKMDCollection)
            //{
            //    tkmd.LoadHMDCollection();
            //}

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                long id = this.ID;
                try
                {
                    #region XuLyHopDong

                    //InsertUpdateHopDongXuLyDuLieu(transaction);
                    #endregion XuLyHopDong

                    #region Phu kien

                    foreach (PhuKienDangKy pkdk in PhuKienCollection)
                    {
                        pkdk.InsertUpDatePhuKienXuLyDuLieu(transaction);
                    }

                    #endregion Phu kien

                    #region To khai mau dinh
                    //foreach (ToKhaiMauDich tkmd in TKMDCollection)
                    //{
                    //    tkmd.InsertUpdateToKhaiXuLyDuLieu(transaction);
                    //    if(tkmd.MaLoaiHinh.StartsWith("X") || tkmd.MaLoaiHinh.Substring(0,1).Equals("X"))
                    //    {
                    //        BKCungUngDangKy BKCU=new BKCungUngDangKy();
                    //        BKCU.TKMD_ID=tkmd.ID;
                    //        BKCungUngDangKyCollection BKCUCollection=BKCU.SelectCollectionDynamic("TKMD_ID="+tkmd.ID,"",transaction);
                    //        foreach(BKCungUngDangKy BKCungUng in BKCUCollection)
                    //        {
                    //            BKCungUng.LoadSanPhamCungUngCollection(transaction);
                    //            foreach(SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                    //            {
                    //                spCungUng.LoadNPLCungUngCollection(transaction);
                    //            }
                    //            BKCungUng.InsertUpdateBKCungUngXuLyDuLieu(transaction);
                    //        }
                    //    }
                    //}
                    //#endregion To khai mau dich

                    //#region To khai chuyen tiep
                    //foreach (ToKhaiChuyenTiep tkct in TKCTCollection)
                    //{
                    //    tkct.InsertUpdateToKhaiCTXuLyDuLieu(transaction);
                    //    if(tkct.MaLoaiHinh.EndsWith("SPX"))
                    //    {
                    //        BKCungUngDangKy BKCU=new BKCungUngDangKy();
                    //        BKCU.TKCT_ID=tkct.ID;
                    //        BKCungUngDangKyCollection BKCUCollection=BKCU.SelectCollectionDynamic("TKCT_ID="+tkct.ID,"",transaction);
                    //        foreach(BKCungUngDangKy BKCungUng in BKCUCollection)
                    //        {
                    //            BKCungUng.LoadSanPhamCungUngCollection(transaction);
                    //            foreach(SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                    //            {
                    //                spCungUng.LoadNPLCungUngCollection(transaction);
                    //            }
                    //            BKCungUng.InsertUpdateBKCungUngXuLyDuLieu(transaction);
                    //        }
                    //    }
                    //}
                    #endregion To khai chuyen tiep

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            //TinhToanCanDoiHopDong();
        }

        //Tinh Toan lai du lieu cua hop dong   
        private KDT_GC_QuyetToan_NguyenPhuLieu CheckNPLQT(string MaNPL, List<KDT_GC_QuyetToan_NguyenPhuLieu> collection)
        {
            foreach (KDT_GC_QuyetToan_NguyenPhuLieu npl in collection)
            {
                if (npl.MaNPL.ToUpper().Trim() == MaNPL.ToUpper().Trim())
                    return npl;
            }
            return null;
        }
        private KDT_GC_QuyetToan_SanPham CheckSPQT(string MaSP, List<KDT_GC_QuyetToan_SanPham> collection)
        {
            foreach (KDT_GC_QuyetToan_SanPham sp in collection)
            {
                if (sp.MaSP.ToUpper().Trim() == MaSP.ToUpper().Trim())
                    return sp;
            }
            return null;
        }

        private KDT_GC_QuyetToan_ThietBi CheckTBQT(string MaTB, List<KDT_GC_QuyetToan_ThietBi> collection)
        {
            foreach (KDT_GC_QuyetToan_ThietBi tb in collection)
            {
                if (tb.Ma.ToUpper().Trim() == MaTB.ToUpper().Trim())
                    return tb;
            }
            return null;
        }
        private KDT_GC_QuyetToan_HangMau CheckHMQT(string MaHM, List<KDT_GC_QuyetToan_HangMau> collection)
        {
            foreach (KDT_GC_QuyetToan_HangMau hm in collection)
            {
                if (hm.Ma.ToUpper().Trim() == MaHM.ToUpper().Trim())
                    return hm;
            }
            return null;
        }
        private Company.GC.BLL.GC.NguyenPhuLieu getNPL(string MaNPL, BLL.GC.NguyenPhuLieuCollection collection)
        {
            foreach (Company.GC.BLL.GC.NguyenPhuLieu npl in collection)
            {
                if (npl.Ma.ToUpper().Trim() == MaNPL.ToUpper().Trim())
                    return npl;
            }
            return null;
        }

        private Company.GC.BLL.GC.SanPham getSP(string MaSP, BLL.GC.SanPhamCollection collection)
        {
            foreach (Company.GC.BLL.GC.SanPham sp in collection)
            {
                if (sp.Ma.ToUpper().Trim() == MaSP.ToUpper().Trim())
                    return sp;
            }
            return null;
        }

        private Company.GC.BLL.GC.ThietBi getTB(string MaSP, BLL.GC.ThietBiCollection collection)
        {
            foreach (Company.GC.BLL.GC.ThietBi tb in collection)
            {
                if (tb.Ma.ToUpper().Trim() == MaSP.ToUpper().Trim())
                    return tb;
            }
            return null;
        }
        private Company.GC.BLL.GC.HangMau getHM(string MaSP, List<BLL.GC.HangMau> collection)
        {
            foreach (Company.GC.BLL.GC.HangMau hm in collection)
            {
                if (hm.Ma.ToUpper().Trim() == MaSP.ToUpper().Trim())
                    return hm;
            }
            return null;
        }

        private KDT_GC_BCXNT_NguyenPhuLieu getNPL(string MaNPL, List<KDT_GC_BCXNT_NguyenPhuLieu> collection)
        {
            foreach (KDT_GC_BCXNT_NguyenPhuLieu NPL in collection)
            {
                if (NPL.MaNPL.ToUpper().Trim() == MaNPL.ToUpper().Trim())
                    return NPL;
            }
            return null;
        }

        public void ProcessBCXuatNhapTon(int SoThapPhanNPL, int SoThapPhanSP, int SoThapPhanDM, int SoThapPhanTLHH, DateTime dateFrom, DateTime dateTo)
        {
            string WhereCondition = " IDHopDong=" + this.ID + " AND NgayDangKy BETWEEN '" + dateFrom.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + dateTo.ToString("yyyy-MM-dd 23:59:59") + "'";
            bool isXuLySP = true;//false;
            this.OnProcess(new HopDongEventArgs("LOAD HỢP ĐỒNG", 1));
            HopDong.Load(this.ID);
            DataTable dsMuaVN = KDT_GC_CungUng.GetDSNPLCungUngByHopDong_New(this.ID);

            #region Xu ly NPL
            this.OnProcess(new HopDongEventArgs("XỬ LÝ NGUYÊN PHỤ LIỆU", 4));
            Company.GC.BLL.GC.NguyenPhuLieu NPLData = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLData.HopDong_ID = this.ID;

            List<KDT_GC_BCXNT_NguyenPhuLieu> NPLCollectionData = new List<KDT_GC_BCXNT_NguyenPhuLieu>();

            NguyenPhuLieuCollection NPLCollectionDataTemp = NPLData.SelectCollectionBy_HopDong_ID();

            foreach (Company.GC.BLL.GC.NguyenPhuLieu item in NPLCollectionDataTemp)
            {
                KDT_GC_BCXNT_NguyenPhuLieu NPL = new KDT_GC_BCXNT_NguyenPhuLieu();
                NPL.STTHang = item.STTHang;
                NPL.HopDong_ID = item.HopDong_ID;
                NPL.MaNPL = item.Ma;
                NPL.TenNPL = item.Ten;
                NPL.MaHS = item.MaHS;
                NPL.DVT = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(item.DVT_ID);
                NPL.LuongNhap = 0;
                NPL.LuongXuat = 0;
                NPL.LuongTon = 0;
                NPL.TongNhuCau = 0;
                NPL.TrangThai = item.TrangThai;
                NPL.TuNgay = new DateTime(dateFrom.Year,dateFrom.Month,dateFrom.Day);
                NPL.DenNgay = new DateTime(dateTo.Year, dateTo.Month, dateTo.Day);
                NPLCollectionData.Add(NPL);
            }
            #endregion Xu ly NPL

            #region Xu ly To khai chuyen tiep
            string ErrorTotal = String.Empty;
            string ErrorNPL = String.Empty;
            int STTNPL = 1;
            string ErrorSP = String.Empty;
            int STTSP = 1;
            string ErrorTB = String.Empty;
            int STTTB = 1;
            String ErrorHM = String.Empty;
            int STTHM = 1;
            String ErrorDM = String.Empty;
            int STTDM = 1;
            String ErrorNPLDM = String.Empty;
            int STTNPLDM = 1;
            String ErrorSPBK = String.Empty;
            int STTSPBK = 1;
            String ErrorNPLBK = String.Empty;
            int STTNPLBK = 1;

            this.OnProcess(new HopDongEventArgs("XỬ LÝ TỜ KHAI CHUYỂN TIẾP", 9));
            //List<KDT_VNACC_ToKhaiMauDich> TKCTVNACCSCollection = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic(WhereCondition,"");

            List<ToKhaiChuyenTiep> TKCTCollection = (List<ToKhaiChuyenTiep>)ToKhaiChuyenTiep.SelectCollectionDynamic(WhereCondition,"");

            foreach (ToKhaiChuyenTiep tkct in TKCTCollection)
            {
                //TODO: Hungtq updated 03/04/2013. Loai tru to khai co trang thai: Cho huy (11), Da huy (10)
                if (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || (tkct.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && tkct.MaLoaiHinh.Substring(0, 1) == "N")
                    )
                {
                    continue;
                }
                int dayTKCT = tkct.NgayDangKy.Day;
                int monthTKCT = tkct.NgayDangKy.Month;
                int yearTKCT = tkct.NgayDangKy.Year;
                DateTime dateTKCT = new DateTime(yearTKCT, monthTKCT, dayTKCT);
                #region xu ly to khai chuyen tiep truoc
                tkct.LoadHCTCollection();
                string LoaiHangHoa = "N";
                if (tkct.MaLoaiHinh.IndexOf("SP") > 0 || tkct.MaLoaiHinh.IndexOf("19") > 0)
                {
                    if (tkct.MaLoaiHinh.EndsWith("X") || tkct.MaLoaiHinh.StartsWith("X"))
                        LoaiHangHoa = "S";
                }
                else if (tkct.MaLoaiHinh.IndexOf("TB") > 0 || tkct.MaLoaiHinh.IndexOf("20") > 0)
                    LoaiHangHoa = "T";
                else if (!string.IsNullOrEmpty(tkct.LoaiHangHoa))
                    LoaiHangHoa = tkct.LoaiHangHoa;
                foreach (HangChuyenTiep HCT in tkct.HCTCollection)
                {
                    decimal i = 1;

                    if (!tkct.MaLoaiHinh.Trim().EndsWith("N") && !tkct.MaLoaiHinh.Trim().Substring(0, 1).Equals("N"))
                    {
                        i = -1;
                    }
                    else
                    {
                        i = 1;
                    }

                    if (LoaiHangHoa == "N")
                    {
                        KDT_GC_BCXNT_NguyenPhuLieu NPL = new KDT_GC_BCXNT_NguyenPhuLieu();
                        NPL = getNPL(HCT.MaHang, NPLCollectionData);

                        if (NPL == null)
                        {
                            decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                            if (SoTKVNACCS != 0)
                            {
                                ErrorNPL += "[" + STTNPL + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString() + "]\n";
                            }
                            else
                            {
                                ErrorNPL += "[" + STTNPL + "]-[" + HCT.MaHang.ToString() + "]-[" + tkct.SoToKhai.ToString() + "]-[" + tkct.NgayDangKy.ToString() + "]\n";
                            }
                            STTNPL++;
                            continue;
                        }
                        if (i > 0)
                            NPL.LuongNhap += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        else
                            NPL.LuongXuat += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                    }
                }
                #endregion
            }
            #endregion Xu ly To khai chuyen tiep

            #region xu ly to khai mau dich
            this.OnProcess(new HopDongEventArgs("XỬ LÝ TỜ KHAI MẬU DỊCH", 12));
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.IDHopDong = this.ID;
            ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionDynamic(WhereCondition,"");
            int indexTKMD = 0;
            foreach (ToKhaiMauDich tkmd in TKMDCollection)
            {
                indexTKMD++;
                this.OnProcess(new HopDongEventArgs("XỬ LÝ TỜ KHAI MẬU DỊCH", (indexTKMD * 80 / TKMDCollection.Count) + 12));
                //TODO: Hungtq updated 03/04/2013. Loai tru to khai co trang thai: Cho huy (11), Da huy (10)
                if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || (tkmd.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && tkmd.MaLoaiHinh.Substring(0, 1) == "N")
                    )
                {
                    continue;
                }
                int dayTKMD = tkmd.NgayDangKy.Day;
                int monthTKMD = tkmd.NgayDangKy.Month;
                int yearTKMD = tkmd.NgayDangKy.Year;
                DateTime dateTKMD = new DateTime(yearTKMD, monthTKMD, dayTKMD);
                #region xu ly to khai truoc
                tkmd.LoadHMDCollection();

                string LoaiHangHoa = tkmd.LoaiHangHoa;
                foreach (HangMauDich HMD in tkmd.HMDCollection)
                {
                    if (tkmd.MaLoaiHinh.StartsWith("N"))
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Nhap NPL
                            KDT_GC_BCXNT_NguyenPhuLieu NPL = new KDT_GC_BCXNT_NguyenPhuLieu();
                            NPL = getNPL(HMD.MaPhu, NPLCollectionData);
                            if (NPL != null)
                            {
                                NPL.LuongNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //NPL.InsertUpdate();
                            }

                            #endregion Nhap NPL

                        }
                        else if (LoaiHangHoa == "S")
                        {
                        }
                    }
                    else
                    {
                        //To khai xuat ben loai hinh gc
                        if (tkmd.MaLoaiHinh.StartsWith("X"))
                        {
                            if (LoaiHangHoa == "N")
                            {
                                #region Xuat NPL
                                KDT_GC_BCXNT_NguyenPhuLieu NPL = new KDT_GC_BCXNT_NguyenPhuLieu();
                                NPL = getNPL(HMD.MaPhu, NPLCollectionData);
                                if (NPL == null)
                                {
                                    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    if (SoTKVNACCS != 0)
                                    {
                                        ErrorNPL += "[" + STTNPL + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    else
                                    {
                                        ErrorNPL += "[" + STTNPL + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    STTNPL++;
                                    continue;
                                }
                                NPL.LuongXuat += Math.Round(HMD.SoLuong, SoThapPhanNPL);

                                #endregion Xuat NPL
                            }
                            else if(LoaiHangHoa=="S")
                            {
                                //minhnd
                                #region xuất sản phẩm
                                if (isXuLySP)
                                {

                                    DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL);
                                    //Nếu là tái xuất thì không xử lý định mức
                                    if (tkmd.MaLoaiHinh.Contains("B13"))
                                    {
                                    }
                                    else
                                    {
                                        if (dsLuongNPL.Tables[0].Rows.Count == 0)
                                        {
                                            if (false)
                                            {
                                                decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                                if (SoTKVNACCS != 0)
                                                {
                                                    ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                                }
                                                else
                                                {
                                                    ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                                }
                                                STTDM++;
                                                continue;
                                            }
                                            else
                                            {
                                                // Lấy lượng NPL theo định mức đã nhập liệu nhưng chưa đăng ký
                                                DataSet dsLuongNPLTemp = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDMChuaDangKy(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL);
                                                if (dsLuongNPLTemp.Tables[0].Rows.Count == 0)
                                                {
                                                    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                                    if (SoTKVNACCS != 0)
                                                    {
                                                        ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                                    }
                                                    else
                                                    {
                                                        ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                                    }
                                                    STTDM++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                                    {
                                                        KDT_GC_BCXNT_NguyenPhuLieu NPL = new KDT_GC_BCXNT_NguyenPhuLieu();
                                                        string maNPL = row["MaNguyenPhuLieu"].ToString();
                                                        NPL = getNPL(maNPL, NPLCollectionData);
                                                        if (NPL == null)
                                                        {
                                                            decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                                            if (SoTKVNACCS != 0)
                                                            {
                                                                ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                                            }
                                                            else
                                                            {
                                                                ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                                            }
                                                            STTNPLDM++;
                                                            continue;
                                                        }
                                                        NPL.LuongXuat += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);

                                                    }
                                                }
                                            }
                                        }
                                        foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                        {
                                            KDT_GC_BCXNT_NguyenPhuLieu NPL = new KDT_GC_BCXNT_NguyenPhuLieu();
                                            string maNPL = row["MaNguyenPhuLieu"].ToString();
                                            NPL = getNPL(maNPL, NPLCollectionData);
                                            if (NPL == null)
                                            {
                                                decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                                if (SoTKVNACCS != 0)
                                                {
                                                    ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                                }
                                                else
                                                {
                                                    ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                                }
                                                STTNPLDM++;
                                                continue;
                                            }
                                            NPL.LuongXuat += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);

                                        }
                                    }

                                }
                                #endregion xuất sản phẩm

                            }
                        }
                    }

                }
                #endregion
            }
            #endregion xu ly to khai mau dich
            this.OnProcess(new HopDongEventArgs("LƯU DỮ LIỆU", 95));
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (KDT_GC_BCXNT_NguyenPhuLieu NPLDataItem in NPLCollectionData)
                    {
                        NPLDataItem.LuongTon = NPLDataItem.LuongNhap - NPLDataItem.LuongXuat;
                        NPLDataItem.InsertUpdate();
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    this.OnProcess(new HopDongEventArgs(ex, "Lỗi :", 0));
                }
                finally
                {
                    connection.Close();
                }
            }
            if (!String.IsNullOrEmpty(ErrorNPL))
                ErrorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorNPL + " CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ NGUYÊN PHỤ LIỆU NÀY\n";
            if (!String.IsNullOrEmpty(ErrorSP))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorSP + " CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ NGUYÊN SẢN PHẨM NÀY\n";
            if (!String.IsNullOrEmpty(ErrorTB))
                ErrorTotal += "\n - [STT] -[MÃ THIẾT BỊ]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorTB + " CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ THIẾT BỊ NÀY\n";
            if (!String.IsNullOrEmpty(ErrorHM))
                ErrorTotal += "\n - [STT] -[MÃ HÀNG MẪU]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorHM + " CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ HÀNG MẪU NÀY\n";
            if (!String.IsNullOrEmpty(ErrorDM))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorDM + " CHƯA CÓ ĐỊNH MỨC .GIẢI PHÁP : KHAI BÁO ĐỊNH MỨC CỦA NHỮNG SẢN PHẨM NÀY\n";
            if (!String.IsNullOrEmpty(ErrorNPLDM))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ]-[MÃ NGUYÊN PHỤ LIỆU] : \n" + ErrorNPLDM + " CÓ ĐỊNH MỨC VỚI NPL CÓ MÃ NÀY TRONG DANH SÁCH ĐỊNH MỨC NHƯNG KHÔNG CÓ TRONG DANH SÁCH NPL ĐƯỢC ĐĂNG KÝ . GIẢI PHÁP : KIỂM TRA VÀ KHAI BÁO PHỤ KIỆN BỔ SUNG NHỮNG MÃ NPL NÀY \n";
            if (!String.IsNullOrEmpty(ErrorSPBK))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG BẢNG KÊ SỐ]-[CÓ SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN] : \n" + ErrorSPBK + " CHƯA ĐƯỢC ĐĂNG KÝ .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ NGUYÊN SẢN PHẨM NÀY \n";
            if (!String.IsNullOrEmpty(ErrorNPLBK))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG BẢNG KÊ SỐ]-[CÓ SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[MÃ NGUYÊN PHỤ LIỆU] : \n" + ErrorSPBK + " CÓ ĐỊNH MỨC VỚI NPL CÓ MÃ NÀY TRONG DANH SÁCH ĐỊNH MỨC NHƯNG KHÔNG CÓ TRONG DANH SÁCH NPL ĐƯỢC ĐĂNG KÝ . GIẢI PHÁP : KIỂM TRA VÀ KHAI BÁO PHỤ KIỆN BỔ SUNG NHỮNG MÃ NPL NÀY \n";

            if (!String.IsNullOrEmpty(ErrorTotal))
            {
                throw new Exception(ErrorTotal);
            }
        }



        public void TinhToanCanDoiHopDong(int SoThapPhanNPL, int SoThapPhanSP, int SoThapPhanDM, int SoThapPhanTLHH ,bool isDinhMucChuaDangKy)
        {
            //string datefrom = String.Concat(dateFrom.ToString("yyyy-MM-dd"), " 00:00:00.00");
            //string dateto = String.Concat(dateTo.ToString("yyyy-MM-dd"), " 23:59:59.999");
            dateFrom = new DateTime(dateFrom.Year, dateFrom.Month, dateFrom.Day);
            dateTo = new DateTime(dateTo.Year, dateTo.Month, dateTo.Day);
            bool isXuLySP = true;//false;
            this.OnProcess(new HopDongEventArgs("LOAD HỢP ĐỒNG", 1));
            HopDong.Load(this.ID);
            //DataTable dsMuaVN = KDT_GC_CungUng.GetDSNPLCungUngByHopDong(this.ID);
            DataTable dsMuaVN = KDT_GC_CungUng.GetDSNPLCungUngByHopDong_New(this.ID);
            #region Xu ly PK
            this.OnProcess(new HopDongEventArgs("XỬ LÝ PHỤ KIỆN", 2));
            PhuKienDangKy PhuKienDK = new PhuKienDangKy();
            PhuKienDK.HopDong_ID = this.ID;
            PhuKienDangKyCollection PKCollectionData = PhuKienDK.SelectCollectionDynamic("HopDong_ID=" + this.ID + " AND TrangThaiXuLy IN (0,1)", "NgayTiepNhan");
            foreach (PhuKienDangKy pkdk in PKCollectionData)
            {
                pkdk.LoadCollection();
                foreach (LoaiPhuKien LoaiPK in pkdk.PKCollection)
                {
                    LoaiPK.LoadCollection();
                }
            }

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {

                    foreach (PhuKienDangKy pkdk in PKCollectionData)
                    {                        
                        pkdk.updateTrangThaiDuLieu();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            #endregion Xu ly PK

            #region Xu ly NPL
            this.OnProcess(new HopDongEventArgs("XỬ LÝ NGUYÊN PHỤ LIỆU", 4));
            Company.GC.BLL.GC.NguyenPhuLieu NPLData = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLData.HopDong_ID = this.ID;

            NguyenPhuLieuCollection NPLCollectionData = new Company.GC.BLL.GC.NguyenPhuLieuCollection();

            NguyenPhuLieuCollection NPLCollectionDataTemp = NPLData.SelectCollectionBy_HopDong_ID();

            foreach (Company.GC.BLL.GC.NguyenPhuLieu item in NPLCollectionDataTemp)
            {
                item.SoLuongCungUng = 0;
                item.SoLuongCungUngDauKy = 0;
                item.SoLuongCungUngNgoaiKy = 0;
                item.SoLuongDaDung = 0;
                item.SoLuongDaDungDauKy = 0;
                item.SoLuongDaDungNgoaiKy = 0;
                item.SoLuongDaNhapDauKy = 0;
                item.SoLuongDaNhapNgoaiKy = 0;
                item.SoLuongDaNhap = 0;

                Company.GC.BLL.GC.NguyenPhuLieu itemNew = Company.GC.BLL.GC.NguyenPhuLieu.Clone(item);

                NPLCollectionData.Add(itemNew);
            }
            #endregion Xu ly NPL
            //comment by minhnd
            //DialogResult rs = MessageBox.Show("Bạn có muốn xử lý sản phẩm của hợp đồng?","Thông báo",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            //if (rs == DialogResult.Yes) 
            //{
            //    isXuLySP = true;
            //}

            #region Xu ly SP
            this.OnProcess(new HopDongEventArgs("XỬ LÝ SẢN PHẨM", 6));
            Company.GC.BLL.GC.SanPham SPData = new Company.GC.BLL.GC.SanPham();
            SPData.HopDong_ID = this.ID;
            BLL.GC.SanPhamCollection SPCollectionData = SPData.SelectCollectionBy_HopDong_ID();
            foreach (Company.GC.BLL.GC.SanPham SPDataItem in SPCollectionData)
            {
                SPDataItem.SoLuongDaXuat = 0;
            }
            #endregion Xu ly SP
            #region Xu ly TB
            this.OnProcess(new HopDongEventArgs("XỬ LÝ THIẾT BỊ", 8));
            Company.GC.BLL.GC.ThietBi TBData = new Company.GC.BLL.GC.ThietBi();
            TBData.HopDong_ID = this.ID;
            BLL.GC.ThietBiCollection TBCollectionData = TBData.SelectCollectionBy_HopDong_ID();
            foreach (Company.GC.BLL.GC.ThietBi TBDataItem in TBCollectionData)
            {
                TBDataItem.SoLuongDaNhap = 0;
            }
            #endregion Xu ly TB
            #region Xu ly HM
            this.OnProcess(new HopDongEventArgs("XỬ LÝ HÀNG MẪU", 8));
            Company.GC.BLL.GC.HangMau HMData = new Company.GC.BLL.GC.HangMau();


            HMData.HopDong_ID = this.ID;
            List<BLL.GC.HangMau> HMCollectionData = HMData.SelectCollectionDynamic_1(" HopDong_ID = " + ID, "");
            foreach (Company.GC.BLL.GC.HangMau HMDataItem in HMCollectionData)
            {
                HMDataItem.SoLuongDaNhap = 0;
            }
            #endregion Xu ly HM

            string ErrorTotal = String.Empty;
            string ErrorNPL = String.Empty;
            int STTNPL = 1;
            string ErrorSP = String.Empty;
            int STTSP = 1;
            string ErrorTB = String.Empty;
            int STTTB = 1;
            String ErrorHM = String.Empty;
            int STTHM = 1;
            String ErrorDM = String.Empty;
            int STTDM = 1;
            String ErrorNPLDM = String.Empty;
            int STTNPLDM = 1;
            String ErrorSPBK = String.Empty;
            int STTSPBK = 1;
            String ErrorNPLBK = String.Empty;
            int STTNPLBK = 1;

            String ErrorLSX = String.Empty;
            int STTLSX = 1;

            #region Xu ly To khai chuyen tiep
            this.OnProcess(new HopDongEventArgs("XỬ LÝ TỜ KHAI CHUYỂN TIẾP", 9));
            //LỆNH SẢN XUẤT
            List<KDT_LenhSanXuat> LenhSanXuatCollection = new List<KDT_LenhSanXuat>();
            LenhSanXuatCollection = KDT_LenhSanXuat.SelectCollectionDynamic("HopDong_ID = " + this.ID, "");
            foreach (KDT_LenhSanXuat item in LenhSanXuatCollection)
            {
                item.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
            }

            List<ToKhaiChuyenTiep> TKCTCollection = (List<ToKhaiChuyenTiep>)ToKhaiChuyenTiep.SelectCollectionBy_IDHopDong(this.ID);
            // Kiểm tra tờ khai trùng và xóa tờ khai đó
            List<ToKhaiChuyenTiep> TKCTCollectionDuplicate = new List<ToKhaiChuyenTiep>();

            DataSet dsDuplicateTKCT = ToKhaiChuyenTiep.CheckDuplicateTK(this.ID);
            if (dsDuplicateTKCT != null && dsDuplicateTKCT.Tables.Count > 0 && dsDuplicateTKCT.Tables[0].Rows.Count > 0)
            {
                for (int t = 0; t < dsDuplicateTKCT.Tables[0].Rows.Count; t++)
                {
                    ToKhaiChuyenTiep TKTemp = new ToKhaiChuyenTiep();
                    TKTemp.Huongdan_PL = dsDuplicateTKCT.Tables[0].Rows[t]["Huongdan_PL"].ToString();
                    TKTemp.MaLoaiHinh = dsDuplicateTKCT.Tables[0].Rows[t]["MaLoaiHinh"].ToString();
                    TKTemp.NgayDangKy = Convert.ToDateTime(dsDuplicateTKCT.Tables[0].Rows[t]["NgayDangKy"].ToString());
                    TKCTCollectionDuplicate.Add(TKTemp);
                }
                foreach (ToKhaiChuyenTiep item in TKCTCollection)
                {
                    foreach (ToKhaiChuyenTiep items in TKCTCollectionDuplicate)
                    {
                        if (item.Huongdan_PL == items.Huongdan_PL)
                        {
                            items.ID = item.ID;
                        }
                    }
                }
            }
            if (TKCTCollectionDuplicate.Count >= 1)
            {
                foreach (ToKhaiChuyenTiep tkct in TKCTCollectionDuplicate)
                {
                    if (tkct.MaLoaiHinh.Contains("X"))
                    {
                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                        {
                            break;
                        }
                    }

                    //Load thong tin chung tu kem
                    tkct.LoadChungTuKem();

                    //Xoa chung tu kem
                    Company.KDT.SHARE.QuanLyChungTu.AnHanThue.DeleteBy_TKMD_ID(tkct.ID);
                    Company.KDT.SHARE.QuanLyChungTu.SoContainer.DeleteBy_TKMD_ID(tkct.ID);
                    Company.KDT.SHARE.QuanLyChungTu.DamBaoNghiaVuNT.DeleteBy_TKMD_ID(tkct.ID);
                    Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.DeleteBy_TKMD_ID(tkct.ID);

                    foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuNo ctn in tkct.ChungTuNoCollection)
                    {
                        ctn.Delete();
                    }

                    Company.GC.BLL.KDT.GC.PhuLucHopDong.DeleteBy_TKCT_ID(tkct.ID);

                    tkct.Delete();
                }
                TKCTCollection = (List<ToKhaiChuyenTiep>)ToKhaiChuyenTiep.SelectCollectionBy_IDHopDong(this.ID);
            }
            //List<ToKhaiChuyenTiep> TKCTCollection = (List<ToKhaiChuyenTiep>)ToKhaiChuyenTiep.SelectCollectionBy_IDHopDong_XNT(this.ID,dateFrom,dateTo);

            foreach (ToKhaiChuyenTiep tkct in TKCTCollection)
            {
                //TODO: Hungtq updated 03/04/2013. Loai tru to khai co trang thai: Cho huy (11), Da huy (10)
                if (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || (tkct.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && tkct.MaLoaiHinh.Substring(0, 1) == "N")
                    )
                {
                    continue;
                }
                int dayTKCT = tkct.NgayDangKy.Day;
                int monthTKCT = tkct.NgayDangKy.Month;
                int yearTKCT = tkct.NgayDangKy.Year;
                DateTime dateTKCT = new DateTime(yearTKCT, monthTKCT, dayTKCT);
                #region xu ly to khai chuyen tiep truoc
                tkct.LoadHCTCollection();
                string LoaiHangHoa = "N";
                if (tkct.MaLoaiHinh.IndexOf("SP") > 0 || tkct.MaLoaiHinh.IndexOf("19") > 0)
                {
                    if (tkct.MaLoaiHinh.EndsWith("X") || tkct.MaLoaiHinh.StartsWith("X"))
                        LoaiHangHoa = "S";
                }
                else if (tkct.MaLoaiHinh.IndexOf("TB") > 0 || tkct.MaLoaiHinh.IndexOf("20") > 0)
                    LoaiHangHoa = "T";
                else if (!string.IsNullOrEmpty(tkct.LoaiHangHoa))
                    LoaiHangHoa = tkct.LoaiHangHoa;
                foreach (HangChuyenTiep HCT in tkct.HCTCollection)
                {
                    decimal i = 1;

                    if (!tkct.MaLoaiHinh.Trim().EndsWith("N") && !tkct.MaLoaiHinh.Trim().Substring(0, 1).Equals("N"))
                    {
                        i = -1;
                    }
                    else
                    {
                        i = 1;
                    }

                    if (LoaiHangHoa == "N")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl = getNPL(HCT.MaHang, NPLCollectionData);

                        if (npl == null)
                        {
                            //continue;
                            decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                            if (SoTKVNACCS != 0)
                            {
                                ErrorNPL += "[" + STTNPL + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                            }
                            else
                            {
                                ErrorNPL += "[" + STTNPL + "]-[" + HCT.MaHang.ToString() + "]-[" + tkct.SoToKhai.ToString() + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                            }
                            STTNPL++;
                            continue;
                            //string msg = "Mã nguyên phụ liệu : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            //if (tkct.SoToKhai == 0)
                            //    msg += "ID=" + tkct.ID;
                            //else
                            //{
                            //    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            //}
                            //msg += " chưa được đăng ký.";
                            //throw new Exception(msg);
                        }
                        if (i > 0)
                            npl.SoLuongDaNhap += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        else
                            npl.SoLuongDaDung += Math.Round(HCT.SoLuong, SoThapPhanNPL);

                        //if (dateTKCT < dateFrom)
                        //{
                        //    if (i > 0)
                        //        npl.SoLuongDaNhapDauKy += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        //    else
                        //        npl.SoLuongDaDungDauKy += Math.Round(HCT.SoLuong, SoThapPhanNPL);

                        //}
                        //else if (dateTKCT <= dateTo)
                        //{
                        //    if (i > 0)
                        //        npl.SoLuongDaNhap += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        //    else
                        //        npl.SoLuongDaDung += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        //}
                        //else
                        //{
                        //    if (i > 0)
                        //        npl.SoLuongDaNhapNgoaiKy += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        //    else
                        //        npl.SoLuongDaDungNgoaiKy += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        //}


                    }
                    else if (LoaiHangHoa == "T")
                    {
                        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                        tb = getTB(HCT.MaHang, TBCollectionData);
                        if (tb == null)
                        {
                            //continue;
                            //string msg = "Mã thiết bị : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            //if (tkct.SoToKhai == 0)
                            //    msg += "ID=" + tkct.ID;
                            //else
                            //{
                            //    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            //}
                            //msg += " chưa được đăng ký.";
                            //throw new Exception(msg);

                            decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                            if (SoTKVNACCS != 0)
                            {
                                ErrorTB += "[" + STTTB + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                            }
                            else
                            {
                                ErrorTB += "[" + STTTB + "]-[" + HCT.MaHang.ToString() + "]-[" + tkct.SoToKhai.ToString() + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                            }
                            STTTB++;
                            continue;
                        }
                        tb.SoLuongDaNhap += HCT.SoLuong * i;
                        //if (dateTKCT < dateFrom)
                        //{
                        //    tb.SoLuongDaNhapDauKy += HCT.SoLuong * i;
                        //}
                        //else if (dateTKCT <= dateTo)
                        //{
                        //    tb.SoLuongDaNhap += HCT.SoLuong * i;
                        //}
                        //else
                        //{
                        //    tb.SoLuongDaNhapNgoaiKy += HCT.SoLuong * i;
                        //}

                    }
                    else
                    {
                        //minhnd xử lý sản phẩm
                        #region xử lý SP
                        if (isXuLySP)
                            if (tkct.MaLoaiHinh.Trim().EndsWith("X") || tkct.MaLoaiHinh.Trim().StartsWith("X"))
                            {
                                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                sp = getSP(HCT.MaHang, SPCollectionData);
                                if (sp == null)
                                {
                                    //continue;
                                    //string msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                    //if (tkct.SoToKhai == 0)
                                    //    msg += "ID=" + tkct.ID;
                                    //else
                                    //{
                                    //    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                    //}
                                    //msg += " chưa được đăng ký.";
                                    //throw new Exception(msg);
                                    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                                    if (SoTKVNACCS != 0)
                                    {
                                        ErrorSP += "[" + STTSP + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                    }
                                    else
                                    {
                                        ErrorSP += "[" + STTSP + "]-[" + HCT.MaHang.ToString() + "]-[" + tkct.SoToKhai.ToString() + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                    }
                                    STTSP++;
                                    continue;
                                }
                                sp.SoLuongDaXuat += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);
                                //if (dateTKCT < dateFrom)
                                //{
                                //    sp.SoLuongDaXuatDauKy += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);
                                //}
                                //else if (dateTKCT <= dateTo)
                                //{
                                //    sp.SoLuongDaXuat += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);
                                //}
                                //else
                                //{
                                //    sp.SoLuongDaXuatNgoaiKy += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);
                                //}

                                bool isExitsLenhSX = false;
                                if (LenhSanXuatCollection.Count >=1)
                                {
                                    foreach (KDT_LenhSanXuat item in LenhSanXuatCollection)
                                    {
                                        item.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
                                        foreach (KDT_LenhSanXuat_SP lenhsp in item.SPCollection)
                                        {
                                            if (HCT.MaHang == lenhsp.MaSanPham)
                                            {
                                                isExitsLenhSX = true;
                                                if (tkct.NgayDangKy >= item.TuNgay && tkct.NgayDangKy <= item.DenNgay)
                                                {
                                                    DataSet dsLuongNPLLSX = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoLenhSX(HCT.MaHang.Trim(), HCT.SoLuong, this.ID, SoThapPhanNPL, item.ID);
                                                    if (dsLuongNPLLSX.Tables[0].Rows.Count == 0)
                                                    {
                                                        decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                                                        if (SoTKVNACCS != 0)
                                                        {
                                                            ErrorDM += "[" + STTDM + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                                        }
                                                        else
                                                        {
                                                            ErrorDM += "[" + STTDM + "]-[" + HCT.MaHang.ToString() + "]-[" + tkct.SoToKhai.ToString() + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                                        }
                                                        STTDM++;
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        foreach (DataRow row in dsLuongNPLLSX.Tables[0].Rows)
                                                        {
                                                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                                            string maNPL = row["MaNguyenPhuLieu"].ToString();
                                                            npl = getNPL(maNPL, NPLCollectionData);
                                                            if (npl == null)
                                                            {
                                                                decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                                                                if (SoTKVNACCS != 0)
                                                                {
                                                                    ErrorNPLDM += "[" + STTNPLDM + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]-[" + maNPL + "]\n";
                                                                }
                                                                else
                                                                {
                                                                    ErrorNPLDM += "[" + STTNPLDM + "]-[" + HCT.MaHang.ToString() + "]-[" + tkct.SoToKhai.ToString() + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]-[" + maNPL + "]\n";
                                                                }
                                                                STTNPLDM++;
                                                                continue;
                                                            }
                                                            npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);

                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (!isExitsLenhSX)
                                    {
                                        decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                                        ErrorLSX += "[" + STTLSX + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                        STTLSX++;
                                    }
                                }
                                else
                                {
                                    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                                    ErrorLSX += "[" + STTLSX + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                    STTLSX++;
                                }
                                //DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCT.MaHang.Trim(), Math.Round(HCT.SoLuong, SoThapPhanSP), this.ID, SoThapPhanNPL);
                                //if (dsLuongNPL.Tables[0].Rows.Count == 0)
                                //{
                                //    //continue;
                                //    //string msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                //    //if (tkct.SoToKhai == 0)
                                //    //    msg += "ID=" + tkct.ID;
                                //    //else
                                //    //{
                                //    //    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                //    //}
                                //    //msg += " chưa có định mức.";
                                //    //throw new Exception(msg);
                                //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                                //    if (SoTKVNACCS != 0)
                                //    {
                                //        ErrorDM += "[" + STTDM + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString() + "]\n";
                                //    }
                                //    else
                                //    {
                                //        ErrorDM += "[" + STTDM + "]-[" + HCT.MaHang.ToString() + "]-[" + tkct.SoToKhai.ToString() + "]-[" + tkct.NgayDangKy.ToString() + "]\n";
                                //    }
                                //    STTDM++;
                                //    continue;
                                //}
                                //foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                //{
                                //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                //    string maNPL = row["MaNguyenPhuLieu"].ToString();
                                //    npl = getNPL(maNPL, NPLCollectionData);
                                //    if (npl == null)
                                //    {
                                //        //continue;
                                //        //string msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                //        //if (tkct.SoToKhai == 0)
                                //        //    msg += "ID=" + tkct.ID;
                                //        //else
                                //        //{
                                //        //    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                //        //}
                                //        //msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                //        //throw new Exception(msg);

                                //        decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkct.SoToKhai));
                                //        if (SoTKVNACCS != 0)
                                //        {
                                //            ErrorNPLDM += "[" + STTNPLDM + "]-[" + HCT.MaHang.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkct.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                //        }
                                //        else
                                //        {
                                //            ErrorNPLDM += "[" + STTNPLDM + "]-[" + HCT.MaHang.ToString() + "]-[" + tkct.SoToKhai.ToString() + "]-[" + tkct.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                //        }
                                //        STTNPLDM++;
                                //        continue;
                                //    }
                                //    npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                //    //if (dateTKCT < dateFrom)
                                //    //{
                                //    //    npl.SoLuongDaDungDauKy += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                //    //}
                                //    //else if (dateTKCT <= dateTo)
                                //    //{
                                //    //    npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                //    //}
                                //    //else
                                //    //{
                                //    //    npl.SoLuongDaDungNgoaiKy += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                //    //}

                                //}
                            }
                        #endregion xử lý SP

                    }
                }
                #endregion

                #region xu ly bang ke cung ung cho to khai chuyen tiep nay

                if (tkct.MaLoaiHinh.Trim().EndsWith("SPX") || tkct.MaLoaiHinh.Trim().Equals("XGC19"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkct.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKCT_ID=" + tkct.ID, "");
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection();
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                //continue;
                                //string msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                //if (BKCungUng.SoBangKe == 0)
                                //    msg += "ID=" + BKCungUng.ID;
                                //else
                                //{
                                //    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                //}
                                //msg += " chưa được đăng ký.";
                                //throw new Exception(msg);

                                ErrorSPBK += "[" + STTSPBK + "]-[" + spCungUng.MaSanPham.ToString() + "]-[" + BKCungUng.SoBangKe + "]-[" + BKCungUng.SoTiepNhan.ToString() + "]-[" + BKCungUng.NgayTiepNhan + "]\n";
                                STTSPBK++;
                            }
                            spCungUng.LoadNPLCungUngCollection();
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    //continue;
                                    //string msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    //if (BKCungUng.SoBangKe == 0)
                                    //    msg += "ID=" + BKCungUng.ID;
                                    //else
                                    //{
                                    //    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    //}
                                    //msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    //throw new Exception(msg);
                                    ErrorNPLBK += "[" + STTNPLBK + "]-[" + spCungUng.MaSanPham.ToString() + "]-[" + BKCungUng.SoBangKe + "]-[" + BKCungUng.SoTiepNhan.ToString() + "]-[" + BKCungUng.NgayTiepNhan + "]-[" + maNPL + "]\n";
                                    STTNPLBK++;
                                }
                                NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                            }
                        }
                    }
                }

                #endregion xu ly bang ke cung ung cho to khai chuyen tiep nay
            }
            #endregion Xu ly To khai chuyen tiep

            #region xu ly to khai mau dich
            this.OnProcess(new HopDongEventArgs("XỬ LÝ TỜ KHAI MẬU DỊCH", 12));
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.IDHopDong = this.ID;
            //ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionBy_IDHopDong_XNT();
            ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionBy_IDHopDong();
            // Kiểm tra và xóa các tờ khai bị trùng
            List<ToKhaiMauDich> TKMDCollectionDuplicate = new List<ToKhaiMauDich>();
            DataSet dsDuplicateTKMD = ToKhaiMauDich.CheckDuplicateTK(this.ID);
            if (dsDuplicateTKMD != null && dsDuplicateTKMD.Tables.Count > 0 && dsDuplicateTKMD.Tables[0].Rows.Count > 0)
            {
                for (int t = 0; t < dsDuplicateTKMD.Tables[0].Rows.Count; t++)
                {
                    ToKhaiMauDich TKTemp = new ToKhaiMauDich();
                    TKTemp.LoaiVanDon = dsDuplicateTKMD.Tables[0].Rows[t]["LoaiVanDon"].ToString();
                    TKTemp.MaLoaiHinh = dsDuplicateTKMD.Tables[0].Rows[t]["MaLoaiHinh"].ToString();
                    TKTemp.NgayDangKy = Convert.ToDateTime(dsDuplicateTKMD.Tables[0].Rows[t]["NgayDangKy"].ToString());
                    TKMDCollectionDuplicate.Add(TKTemp);
                }
                foreach (ToKhaiMauDich item in TKMDCollection)
                {
                    foreach (ToKhaiMauDich items in TKMDCollectionDuplicate)
                    {
                        if (item.LoaiVanDon==items.LoaiVanDon)
                        {
                            items.ID = item.ID;
                        }
                    }
                }
            }
            if (TKMDCollectionDuplicate.Count>=1)
            {
                foreach (ToKhaiMauDich tkmd in TKMDCollectionDuplicate)
                {
                    if (tkmd.MaLoaiHinh.Contains("XGC"))
                    {
                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(tkmd.ID))
                        {
                            break;
                        }
                    }
                    //Load thong tin chung tu kem
                    tkmd.LoadChungTuKem();

                    tkmd.LoadChungTuHaiQuan();

                    //Xoa chung tu kem
                    for (int j = 0; j < tkmd.listCTDK.Count; j++)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.DeleteBy_TKMDID(tkmd.ID);
                    }

                    Company.KDT.SHARE.QuanLyChungTu.AnHanThue.DeleteBy_TKMD_ID(tkmd.ID);
                    Company.KDT.SHARE.QuanLyChungTu.SoContainer.DeleteBy_TKMD_ID(tkmd.ID);
                    Company.KDT.SHARE.QuanLyChungTu.DamBaoNghiaVuNT.DeleteBy_TKMD_ID(tkmd.ID);

                    if (tkmd.GiayKiemTraCollection != null && tkmd.GiayKiemTraCollection.Count > 0)
                    {
                        foreach (GiayKiemTra gkt in tkmd.GiayKiemTraCollection)
                        {
                            foreach (HangGiayKiemTra hgkt in gkt.HangCollection)
                            {
                                hgkt.Delete();
                            }
                        }
                    }
                    Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra.DeleteBy_TKMD_ID(tkmd.ID);

                    if (tkmd.ChungThuGD != null && tkmd.ChungThuGD.ListHang.Count > 0)
                    {
                        foreach (HangGiamDinh hgd in tkmd.ChungThuGD.ListHang)
                        {
                            hgd.Delete();
                        }
                    }
                    Company.KDT.SHARE.QuanLyChungTu.ChungThuGiamDinh.DeleteBy_TKMD_ID(tkmd.ID);

                    tkmd.Delete();
                }
                TKMDCollection = TKMD.SelectCollectionBy_IDHopDong();
            }
            int indexTKMD = 0;
            foreach (ToKhaiMauDich tkmd in TKMDCollection)
            {
                indexTKMD++;
                this.OnProcess(new HopDongEventArgs("Xử lý Tờ khai mậu dịch", (indexTKMD * 80 / TKMDCollection.Count) + 12));
                //TODO: Hungtq updated 03/04/2013. Loai tru to khai co trang thai: Cho huy (11), Da huy (10)
                if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || (tkmd.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && tkmd.MaLoaiHinh.Substring(0, 1) == "N")
                    )
                {
                    continue;
                }
                int dayTKMD = tkmd.NgayDangKy.Day;
                int monthTKMD = tkmd.NgayDangKy.Month;
                int yearTKMD = tkmd.NgayDangKy.Year;
                DateTime dateTKMD = new DateTime(yearTKMD, monthTKMD, dayTKMD);
                //;// Logger.LocalLogger.Instance().WriteMessage(new Exception("Tờ khai " + tkmd.SoToKhai + "/" + tkmd.MaLoaiHinh));
                #region xu ly to khai truoc
                tkmd.LoadHMDCollection();

                string LoaiHangHoa = tkmd.LoaiHangHoa;
                foreach (HangMauDich HMD in tkmd.HMDCollection)
                {
                    // Logger.LocalLogger.Instance().WriteMessage(new Exception("Tờ khai " + tkmd.SoToKhai + "/" + tkmd.MaLoaiHinh + "/MaNPL = " + HMD.MaPhu) );
                    if (tkmd.MaLoaiHinh.StartsWith("N"))
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Nhap NPL
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl = getNPL(HMD.MaPhu, NPLCollectionData);
                            if (npl == null)
                            {
                                #region xử lý hàng mẫu trong tờ khai nhập về dạng NPL
                                Company.GC.BLL.GC.HangMau hm = new Company.GC.BLL.GC.HangMau();
                                hm = getHM(HMD.MaPhu, HMCollectionData);
                                if (hm == null)
                                {
                                    //hm = new Company.GC.BLL.GC.HangMau();
                                    //hm.Ma = HMD.MaPhu;
                                    //hm.Ten = HMD.Ten;
                                    //hm.DVT_ID = HMD.DVT_ID;
                                    //hm.SoLuongDaNhap = HMD.SoLuong;
                                    //hm.Insert();
                                    //string msg = "Mã nguyên phụ liệu/hàng mẫu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    //if (tkmd.SoToKhai == 0)
                                    //    msg += "ID=" + tkmd.ID;
                                    //else
                                    //{
                                    //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    //    if (SoTKVNACCS !=0)
                                    //    {
                                    //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                    //    }
                                    //    else
                                    //    {
                                    //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    //    }
                                    //}
                                    //msg += " chưa được đăng ký.";
                                    //throw new Exception(msg);

                                    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    if (SoTKVNACCS != 0)
                                    {
                                        ErrorNPL += "[" + STTNPL + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    else
                                    {
                                        ErrorNPL += "[" + STTNPL + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    STTNPL++;
                                    continue;
                                }
                                else
                                    hm.SoLuongDaNhap += HMD.SoLuong;
                                //    if (dateTKMD < dateFrom)
                                //{
                                //    hm.SoLuongDaNhapDauKy += HMD.SoLuong;
                                //}
                                //    else if (dateTKMD <= dateTo)
                                //{
                                //    hm.SoLuongDaNhap += HMD.SoLuong;
                                //}
                                //else
                                //{
                                //    hm.SoLuongDaNhapNgoaiKy += HMD.SoLuong;
                                //}

                                #endregion xử lý hàng mẫu trong tờ khai nhập về dạng NPL
                                //lỗi

                            }
                            if (npl != null)
                            {
                                npl.SoLuongDaNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                npl.InsertUpdate();
                                //if (dateTKMD < dateFrom)
                                //{
                                //    npl.SoLuongDaNhapDauKy += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //    npl.InsertUpdate();
                                //}
                                //else if (dateTKMD <= dateTo)
                                //{
                                //    npl.SoLuongDaNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //    npl.InsertUpdate();
                                //}
                                //else
                                //{
                                //    npl.SoLuongDaNhapNgoaiKy += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //    npl.InsertUpdate();
                                //}

                            }

                            #endregion Nhap NPL


                        }
                        else if (LoaiHangHoa == "S")
                        {
                            if (isXuLySP)
                            {
                                //Bỏ qua tờ khai nhập hàng hóa xuất khẩu bị trả lại
                                if (tkmd.MaLoaiHinh.Contains("A31"))
                                {
                                }
                                else
                                {
                                    //minhnd: fix nhap SP
                                    #region Nhap SP
                                    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                    sp = getSP(HMD.MaPhu, SPCollectionData);
                                    if (sp == null)
                                    {
                                        //continue;
                                        //string msg = "Mã Sản Phảm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                        //if (tkmd.SoToKhai == 0)
                                        //    msg += "ID=" + tkmd.ID;
                                        //else
                                        //{
                                        //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                        //    if (SoTKVNACCS != 0)
                                        //    {
                                        //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                        //    }
                                        //    else
                                        //    {
                                        //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                        //    }
                                        //}
                                        //msg += " chưa được đăng ký.";
                                        //throw new Exception(msg);
                                        decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                        if (SoTKVNACCS != 0)
                                        {
                                            ErrorSP += "[" + STTSP + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                        }
                                        else
                                        {
                                            ErrorSP += "[" + STTSP + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                        }
                                        STTSP++;
                                        continue;
                                    }
                                    sp.SoLuongConLai += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    //if (dateTKMD < dateFrom)
                                    //{
                                    //    sp.SoLuongConLaiDauKy += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    //}
                                    //else if (dateTKMD <= dateTo)
                                    //{
                                    //    sp.SoLuongConLai += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    //}
                                    //else
                                    //{
                                    //    sp.SoLuongConLaiNgoaiKy += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    //}                                

                                    #endregion Nhap SP
                                    //minhnd: fix nhap SP
                                }
                            }

                        }
                        else if (LoaiHangHoa == "T")
                        {
                            #region Nhap Thiet BI
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb = getTB(HMD.MaPhu, TBCollectionData);
                            if (tb == null)
                            {
                                //tb = new Company.GC.BLL.GC.ThietBi();
                                //tb.Ma = HMD.MaPhu;
                                //tb.Ten = HMD.Ten;
                                //tb.DVT_ID = HMD.DVT_ID;
                                //tb.SoLuongDaNhap = HMD.SoLuong;
                                //tb.SoLuongConLai = HMD.SoLuong;
                                //tb.Insert();
                                //if (dateTKMD < dateFrom)
                                //{
                                //    tb = new Company.GC.BLL.GC.ThietBi();
                                //    tb.Ma = HMD.MaPhu;
                                //    tb.Ten = HMD.Ten;
                                //    tb.DVT_ID = HMD.DVT_ID;
                                //    tb.SoLuongDaNhapDauKy = HMD.SoLuong;
                                //    tb.SoLuongConLai = HMD.SoLuong;
                                //    tb.Insert();
                                //}
                                //else if (dateTKMD <= dateTo)
                                //{
                                //    tb = new Company.GC.BLL.GC.ThietBi();
                                //    tb.Ma = HMD.MaPhu;
                                //    tb.Ten = HMD.Ten;
                                //    tb.DVT_ID = HMD.DVT_ID;
                                //    tb.SoLuongDaNhap = HMD.SoLuong;
                                //    tb.SoLuongConLai = HMD.SoLuong;
                                //    tb.Insert();
                                //}
                                //else
                                //{
                                //    tb = new Company.GC.BLL.GC.ThietBi();
                                //    tb.Ma = HMD.MaPhu;
                                //    tb.Ten = HMD.Ten;
                                //    tb.DVT_ID = HMD.DVT_ID;
                                //    tb.SoLuongDaNhapNgoaiKy = HMD.SoLuong;
                                //    tb.SoLuongConLai = HMD.SoLuong;
                                //    tb.Insert();
                                //}

                                //continue;
                                //string msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                //if (tkmd.SoToKhai == 0)
                                //    msg += "ID=" + tkmd.ID;
                                //else
                                //{
                                //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                //    if (SoTKVNACCS != 0)
                                //    {
                                //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                //    }
                                //    else
                                //    {
                                //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                //    }
                                //}
                                //msg += " chưa được đăng ký.";
                                //throw new Exception(msg);

                                decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                if (SoTKVNACCS != 0)
                                {
                                    ErrorTB += "[" + STTTB + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                }
                                else
                                {
                                    ErrorTB += "[" + STTTB + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                }
                                STTTB++;
                                continue;
                            }
                            else
                                tb.SoLuongDaNhap += HMD.SoLuong;
                            //    if (dateTKMD < dateFrom)
                            //{
                            //    tb.SoLuongDaNhapDauKy += HMD.SoLuong;
                            //}
                            //    else if (dateTKMD <= dateTo)
                            //{
                            //    tb.SoLuongDaNhap += HMD.SoLuong;
                            //}
                            //else
                            //{
                            //    tb.SoLuongDaNhapNgoaiKy += HMD.SoLuong;
                            //}                

                            #endregion Nhap Thiet BI
                        }
                        else
                        {
                            #region Nhap Hang Mau
                            Company.GC.BLL.GC.HangMau hm = new Company.GC.BLL.GC.HangMau();
                            hm = getHM(HMD.MaPhu, HMCollectionData);
                            if (hm == null)
                            {
                                //hm = new Company.GC.BLL.GC.HangMau();
                                //hm.Ma = HMD.MaPhu;
                                //hm.Ten = HMD.Ten;
                                //hm.DVT_ID = HMD.DVT_ID;
                                //hm.SoLuongDaNhap = HMD.SoLuong;
                                ////hm.s = HMD.SoLuong;
                                //hm.Insert();
                                ////continue;
                                ////string msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                ////if (tkmd.SoToKhai == 0)
                                ////    msg += "ID=" + tkmd.ID;
                                ////else
                                ////{
                                ////    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                ////}
                                ////msg += " chưa được đăng ký.";
                                ////throw new Exception(msg);
                                #region Nhap NPL
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl = getNPL(HMD.MaPhu, NPLCollectionData);
                                if (npl == null)
                                {
                                    //#region xử lý hàng mẫu trong tờ khai nhập về dạng NPL
                                    //Company.GC.BLL.GC.HangMau hm = new Company.GC.BLL.GC.HangMau();
                                    //hm = getHM(HMD.MaPhu, HMCollectionData);
                                    //if (hm == null)
                                    //{
                                    //    //hm = new Company.GC.BLL.GC.HangMau();
                                    //    //hm.Ma = HMD.MaPhu;
                                    //    //hm.Ten = HMD.Ten;
                                    //    //hm.DVT_ID = HMD.DVT_ID;
                                    //    //hm.SoLuongDaNhap = HMD.SoLuong;
                                    //    //hm.Insert();
                                    //string msg = "Mã nguyên phụ liệu/hàng mẫu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    //if (tkmd.SoToKhai == 0)
                                    //    msg += "ID=" + tkmd.ID;
                                    //else
                                    //{
                                    //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    //    if (SoTKVNACCS != 0)
                                    //    {
                                    //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                    //    }
                                    //    else
                                    //    {
                                    //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    //    }
                                    //}
                                    //msg += " chưa được đăng ký.";
                                    //throw new Exception(msg);
                                    //}
                                    //else
                                    //    hm.SoLuongDaNhap += HMD.SoLuong;
                                    //lỗi

                                    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    if (SoTKVNACCS != 0)
                                    {
                                        ErrorHM += "[" + STTHM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    else
                                    {
                                        ErrorHM += "[" + STTHM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    STTHM++;
                                    continue;

                                }
                                npl.SoLuongDaNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                npl.InsertUpdate();
                                //if (dateTKMD < dateFrom)
                                //{
                                //    npl.SoLuongDaNhapDauKy += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //    npl.InsertUpdate();
                                //}
                                //else if (dateTKMD <= dateTo)
                                //{
                                //    npl.SoLuongDaNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //    npl.InsertUpdate();
                                //}
                                //else
                                //{
                                //    npl.SoLuongDaNhapNgoaiKy += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //    npl.InsertUpdate();
                                //}

                                #endregion Nhap NPL
                            }
                            else
                                hm.SoLuongDaNhap += HMD.SoLuong;
                            //    if (dateTKMD < dateFrom)
                            //{
                            //    hm.SoLuongDaNhapDauKy += HMD.SoLuong;
                            //}
                            //    else if (dateTKMD <= dateTo)
                            //{
                            //    hm.SoLuongDaNhap += HMD.SoLuong;
                            //}
                            //else
                            //{
                            //    hm.SoLuongDaNhapNgoaiKy += HMD.SoLuong;
                            //}

                            #endregion Nhap Hang Mau
                        }
                    }
                    //else if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    //{
                    //    #region Nhap NPL ben loai hinh sxxk
                    //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                    //    if (npl == null)
                    //    {
                    //        
                    //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                    //        if (tkmd.SoToKhai == 0)
                    //            msg += "ID=" + tkmd.ID;
                    //        else
                    //        {
                    //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                    //        }
                    //        msg += " chưa được đăng ký.";
                    //        throw new Exception(msg);
                    //    }
                    //    npl.SoLuongCungUng += Math.Round(HMD.SoLuong,SoThapPhanNPL);
                    //    #endregion Nhap NPL
                    //}
                    else
                    {
                        //to khai xuat ben loai hinh gc
                        if (tkmd.MaLoaiHinh.StartsWith("X"))
                        {
                            if (LoaiHangHoa == "N")
                            {
                                #region Xuat NPL
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl = getNPL(HMD.MaPhu, NPLCollectionData);
                                if (npl == null)
                                {
                                    //continue;
                                    //string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    //if (tkmd.SoToKhai == 0)
                                    //    msg += "ID=" + tkmd.ID;
                                    //else
                                    //{
                                    //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    //    if (SoTKVNACCS != 0)
                                    //    {
                                    //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                    //    }
                                    //    else
                                    //    {
                                    //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    //    }
                                    //}
                                    //msg += " chưa được đăng ký.";
                                    //throw new Exception(msg);

                                    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    if (SoTKVNACCS != 0)
                                    {
                                        ErrorNPL += "[" + STTNPL + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    else
                                    {
                                        ErrorNPL += "[" + STTNPL + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    STTNPL++;
                                    continue;
                                }
                                npl.SoLuongDaDung += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //if (dateTKMD < dateFrom)
                                //{
                                //    npl.SoLuongDaDungDauKy += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //}
                                //else if (dateTKMD <= dateTo)
                                //{
                                //    npl.SoLuongDaDung += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //}
                                //else
                                //{
                                //    npl.SoLuongDaDungNgoaiKy += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //}

                                #endregion Xuat NPL
                            }
                            else if (LoaiHangHoa == "T")
                            {
                                #region Xuat TB
                                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                                tb = getTB(HMD.MaPhu, TBCollectionData);
                                if (tb == null)
                                {
                                    // continue;
                                    //string msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    //if (tkmd.SoToKhai == 0)
                                    //    msg += "ID=" + tkmd.ID;
                                    //else
                                    //{
                                    //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    //    if (SoTKVNACCS != 0)
                                    //    {
                                    //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                    //    }
                                    //    else
                                    //    {
                                    //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    //    }
                                    //}
                                    //msg += " chưa được đăng ký.";
                                    //throw new Exception(msg);
                                    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    if (SoTKVNACCS != 0)
                                    {
                                        ErrorTB += "[" + STTTB + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    else
                                    {
                                        ErrorTB += "[" + STTTB + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    }
                                    STTTB++;
                                    continue;
                                }
                                tb.SoLuongDaNhap -= HMD.SoLuong;
                                //if (dateTKMD < dateFrom)
                                //{
                                //    tb.SoLuongDaNhapDauKy -= HMD.SoLuong;
                                //}
                                //else if (dateTKMD <= dateTo)
                                //{
                                //    tb.SoLuongDaNhap -= HMD.SoLuong;
                                //}
                                //else
                                //{
                                //    tb.SoLuongDaNhapNgoaiKy -= HMD.SoLuong;
                                //}

                                #endregion Xuat Thiet bi
                            }
                            else
                            {
                                //minhnd
                                #region xuất sản phẩm
                                if (isXuLySP)
                                {
                                    if (tkmd.MaLoaiHinh.Contains("B13"))
                                    {
                                    }
                                    else
                                    {
                                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                        sp = getSP(HMD.MaPhu, SPCollectionData);
                                        if (sp == null)
                                        {
                                            // continue;
                                            //string msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                            //if (tkmd.SoToKhai == 0)
                                            //    msg += "ID=" + tkmd.ID;
                                            //else
                                            //{
                                            //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                            //    if (SoTKVNACCS != 0)
                                            //    {
                                            //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                            //    }
                                            //    else
                                            //    {
                                            //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                            //    }
                                            //}
                                            //msg += " chưa được đăng ký.";
                                            //throw new Exception(msg);

                                            decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                            if (SoTKVNACCS != 0)
                                            {
                                                ErrorSP += "[" + STTSP + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                            }
                                            else
                                            {
                                                ErrorSP += "[" + STTSP + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                            }
                                            STTSP++;
                                            continue;
                                        }
                                        sp.SoLuongDaXuat += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                    }
                                    //if (dateTKMD < dateFrom)
                                    //{
                                    //    sp.SoLuongDaXuatDauKy += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                    //}
                                    //else if (dateTKMD <= dateTo)
                                    //{
                                    //    sp.SoLuongDaXuat += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                    //}
                                    //else
                                    //{
                                    //    sp.SoLuongDaXuatNgoaiKy += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                    //}
                                    // Bỏ qua tờ khai xuất trả hàng nhập khẩu
                                    if (tkmd.MaLoaiHinh.Contains("B13"))
                                    {
                                    }
                                    else 
                                    {
                                        bool isExitsLenhSX = false;
                                        if (LenhSanXuatCollection.Count >=1)
                                        {
                                            if (tkmd.LoaiVanDon=="302986904310")
                                            {
                                                
                                            }
                                            foreach (KDT_LenhSanXuat item in LenhSanXuatCollection)
                                            {
                                                item.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
                                                foreach (KDT_LenhSanXuat_SP lenhsp in item.SPCollection)
                                                {
                                                    if (HMD.MaPhu == lenhsp.MaSanPham)
                                                    {
                                                        isExitsLenhSX = true;
                                                        if (tkmd.NgayDangKy >= item.TuNgay && tkmd.NgayDangKy <= item.DenNgay)
                                                        {
                                                            DataSet dsLuongNPLLSX = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoLenhSX(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL, item.ID);
                                                            if (dsLuongNPLLSX.Tables[0].Rows.Count == 0)
                                                            {
                                                                decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                                                if (SoTKVNACCS != 0)
                                                                {
                                                                    ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                                                }
                                                                else
                                                                {
                                                                    ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                                                }
                                                                STTDM++;
                                                                continue;
                                                            }
                                                            else
                                                            {
                                                                foreach (DataRow row in dsLuongNPLLSX.Tables[0].Rows)
                                                                {
                                                                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                                                    string maNPL = row["MaNguyenPhuLieu"].ToString();
                                                                    npl = getNPL(maNPL, NPLCollectionData);
                                                                    if (npl == null)
                                                                    {
                                                                        decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                                                        if (SoTKVNACCS != 0)
                                                                        {
                                                                            ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                                                        }
                                                                        else
                                                                        {
                                                                            ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                                                        }
                                                                        STTNPLDM++;
                                                                        continue;
                                                                    }
                                                                    npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);

                                                                }
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (!isExitsLenhSX)
                                            {
                                                decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                                ErrorLSX += "[" + STTLSX + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                                STTLSX++;
                                            }
                                        }
                                        else
                                        {
                                            decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                            ErrorLSX += "[" + STTLSX + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString("dd/MM/yyyy HH:mm:ss") + "]\n";
                                            STTLSX++;
                                        }
                                    }
                                    #region
                                    //DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL);
                                    ////Nếu là tái xuất thì không xử lý định mức
                                    //if (tkmd.MaLoaiHinh.Contains("B13"))
                                    //{
                                    //}
                                    //else
                                    //{
                                    //    if (dsLuongNPL.Tables[0].Rows.Count == 0)
                                    //    {
                                    //        //continue;
                                    //        if (!isDinhMucChuaDangKy)
                                    //        {
                                    //            //string msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    //            //if (tkmd.SoToKhai == 0)
                                    //            //    msg += "ID=" + tkmd.ID;
                                    //            //else
                                    //            //{
                                    //            //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    //            //    if (SoTKVNACCS != 0)
                                    //            //    {
                                    //            //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                    //            //    }
                                    //            //    else
                                    //            //    {
                                    //            //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    //            //    }
                                    //            //}
                                    //            //msg += " chưa có định mức.";
                                    //            //throw new Exception(msg);

                                    //            decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                    //            if (SoTKVNACCS != 0)
                                    //            {
                                    //                ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    //            }
                                    //            else
                                    //            {
                                    //                ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    //            }
                                    //            STTDM++;
                                    //            continue;
                                    //        }
                                    //        else
                                    //        {
                                    //            // Lấy lượng NPL theo định mức đã nhập liệu nhưng chưa đăng ký
                                    //            DataSet dsLuongNPLTemp = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDMChuaDangKy(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL);
                                    //            if (dsLuongNPLTemp.Tables[0].Rows.Count == 0)
                                    //            {
                                    //                //string msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    //                //if (tkmd.SoToKhai == 0)
                                    //                //    msg += "ID=" + tkmd.ID;
                                    //                //else
                                    //                //{
                                    //                //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    //                //    if (SoTKVNACCS != 0)
                                    //                //    {
                                    //                //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                    //                //    }
                                    //                //    else
                                    //                //    {
                                    //                //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    //                //    }
                                    //                //}
                                    //                //msg += " chưa nhập định mức.";
                                    //                //throw new Exception(msg);

                                    //                decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                    //                if (SoTKVNACCS != 0)
                                    //                {
                                    //                    ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    //                }
                                    //                else
                                    //                {
                                    //                    ErrorDM += "[" + STTDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]\n";
                                    //                }
                                    //                STTDM++;
                                    //                continue;
                                    //            }
                                    //            else
                                    //            {
                                    //                foreach (DataRow row in dsLuongNPLTemp.Tables[0].Rows)
                                    //                {
                                    //                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                    //                    string maNPL = row["MaNguyenPhuLieu"].ToString();
                                    //                    npl = getNPL(maNPL, NPLCollectionData);
                                    //                    if (npl == null)
                                    //                    {
                                    //                        //continue;
                                    //                        //string msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    //                        //if (tkmd.SoToKhai == 0)
                                    //                        //    msg += "ID=" + tkmd.ID;
                                    //                        //else
                                    //                        //{
                                    //                        //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    //                        //    if (SoTKVNACCS != 0)
                                    //                        //    {
                                    //                        //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                    //                        //    }
                                    //                        //    else
                                    //                        //    {
                                    //                        //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    //                        //    }
                                    //                        //}
                                    //                        //msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    //                        //throw new Exception(msg);

                                    //                        decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                    //                        if (SoTKVNACCS != 0)
                                    //                        {
                                    //                            ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                    //                        }
                                    //                        else
                                    //                        {
                                    //                            ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                    //                        }
                                    //                        STTNPLDM++;
                                    //                        continue;
                                    //                    }
                                    //                    npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);

                                    //                }
                                    //            }
                                    //        }
                                    //    }
                                    //    foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                    //    {
                                    //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                    //        string maNPL = row["MaNguyenPhuLieu"].ToString();
                                    //        npl = getNPL(maNPL, NPLCollectionData);

                                    //        if (npl == null)
                                    //        {
                                    //            //continue;
                                    //            //string msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    //            //if (tkmd.SoToKhai == 0)
                                    //            //    msg += "ID=" + tkmd.ID;
                                    //            //else
                                    //            //{
                                    //            //    decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(tkmd.SoToKhai);
                                    //            //    if (SoTKVNACCS != 0)
                                    //            //    {
                                    //            //        msg += "Số tờ khai mậu dịch VNACCS : " + SoTKVNACCS.ToString();
                                    //            //    }
                                    //            //    else
                                    //            //    {
                                    //            //        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    //            //    }
                                    //            //}
                                    //            //msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    //            //throw new Exception(msg);

                                    //            decimal SoTKVNACCS = CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(tkmd.SoToKhai));
                                    //            if (SoTKVNACCS != 0)
                                    //            {
                                    //                ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + SoTKVNACCS + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                    //            }
                                    //            else
                                    //            {
                                    //                ErrorNPLDM += "[" + STTNPLDM + "]-[" + HMD.MaPhu.ToString() + "]-[" + tkmd.SoToKhai.ToString() + "]-[" + tkmd.NgayDangKy.ToString() + "]-[" + maNPL + "]\n";
                                    //            }
                                    //            STTNPLDM++;
                                    //            continue;

                                    //        }
                                    //        npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                    //        //if (dateTKMD < dateFrom)
                                    //        //{
                                    //        //    npl.SoLuongDaDungDauKy += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                    //        //}
                                    //        //else if (dateTKMD <= dateTo)
                                    //        //{
                                    //        //    npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                    //        //}
                                    //        //else
                                    //        //{
                                    //        //    npl.SoLuongDaDungNgoaiKy += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                    //        //}

                                    //    }
                                    //}
                                    #endregion
                                }
                                    #endregion xuất sản phẩm

                            }
                        }
                        //else
                        //{
                        //    #region Tai Xuat NPL ben loai hinh SXXK
                        //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                        //    if (npl == null)
                        //    {

                        //        
                        //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                        //        if (tkmd.SoToKhai == 0)
                        //            msg += "ID=" + tkmd.ID;
                        //        else
                        //        {
                        //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                        //        }
                        //        msg += " chưa được đăng ký.";
                        //        throw new Exception(msg);
                        //    }
                        //    if (npl.Ma == "410799000015")
                        //    {

                        //    }
                        //    npl.SoLuongCungUng -=Math.Round(HMD.SoLuong,SoThapPhanNPL);
                        //    #endregion Xuat NPL
                        //}
                    }

                }
                #endregion

                #region xu ly bang ke cung ung cho to khai

                if (tkmd.MaLoaiHinh.StartsWith("X") || tkmd.MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkmd.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKMD_ID=" + tkmd.ID, "");
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection();
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                //continue;
                                //String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                //if (BKCungUng.SoBangKe == 0)
                                //    msg += "ID=" + BKCungUng.ID;
                                //else
                                //{
                                //    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                //}
                                //msg += " chưa được đăng ký.";
                                //throw new Exception(msg);

                                ErrorSPBK += "[" + STTSPBK + "]-[" + spCungUng.MaSanPham.ToString() + "]-[" + BKCungUng.SoBangKe + "]-[" + BKCungUng.SoTiepNhan.ToString() + "]-[" + BKCungUng.NgayTiepNhan + "]\n";
                                STTSPBK++;
                                continue;
                            }
                            spCungUng.LoadNPLCungUngCollection();
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    //continue;
                                    //String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    //if (BKCungUng.SoBangKe == 0)
                                    //    msg += "ID=" + BKCungUng.ID;
                                    //else
                                    //{
                                    //    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    //}
                                    //msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    //throw new Exception(msg);
                                    ErrorNPLBK += "[" + STTNPLBK + "]-[" + spCungUng.MaSanPham.ToString() + "]-[" + BKCungUng.SoBangKe + "]-[" + BKCungUng.SoTiepNhan.ToString() + "]-[" + BKCungUng.NgayTiepNhan + "]-[" + maNPL + "]\n";
                                    STTNPLBK++;
                                    continue;
                                }
                                NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                            }
                        }
                    }
                }

                #endregion
            }
            #endregion xu ly to khai mau dich

            #region Xu ly phu kien mua vn
            this.OnProcess(new HopDongEventArgs("XỬ LÝ CUNG ỨNG", 93));
            //DataSet dsMuaVN = KDT_GC_CungUng.GetDSNPLCungUngByHopDong(this.ID);
            if (dsMuaVN != null && dsMuaVN.Rows.Count > 0)
            {
                foreach (Company.GC.BLL.GC.NguyenPhuLieu item in NPLCollectionData)
                {

                    foreach (DataRow row in dsMuaVN.Rows)
                    {
                        if (row["MaNPL"].ToString().Trim().ToUpper() == item.Ma.Trim().ToUpper())
                        {
                            item.SoLuongCungUng += Convert.ToDecimal(row["LuongCungUng"]);
                            break;
                        }
                    }
                }
            }


            #endregion Xu ly phu kien mua vn
            this.OnProcess(new HopDongEventArgs("LƯU DỮ LIỆU", 95));
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
                    {
                        NPLDataItem.UpdateTransaction(transaction);
                    }
                    //minhnd xử lý cho những hợp đồng không xuất.
                    if (isXuLySP)
                    {
                        foreach (Company.GC.BLL.GC.SanPham SPDataItem in SPCollectionData)
                        {
                            SPDataItem.UpdateTransaction(transaction);
                        }
                    }

                    foreach (Company.GC.BLL.GC.ThietBi TBDataItem in TBCollectionData)
                    {
                        TBDataItem.UpdateTransaction(transaction);
                    }
                    foreach (Company.GC.BLL.GC.HangMau HMDataItem in HMCollectionData)
                    {
                        HMDataItem.Update(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw ex;
                    this.OnProcess(new HopDongEventArgs(ex, "Lỗi :", 0));
                }
                finally
                {
                    connection.Close();
                }
            }

            if (!String.IsNullOrEmpty(ErrorNPL))
                ErrorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorNPL + " CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ NGUYÊN PHỤ LIỆU NÀY\n";
            if (!String.IsNullOrEmpty(ErrorSP))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorSP + " CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ NGUYÊN SẢN PHẨM NÀY\n";
            if (!String.IsNullOrEmpty(ErrorTB))
                ErrorTotal += "\n - [STT] -[MÃ THIẾT BỊ]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorTB + " CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ THIẾT BỊ NÀY\n";
            if (!String.IsNullOrEmpty(ErrorHM))
                ErrorTotal += "\n - [STT] -[MÃ HÀNG MẪU]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorHM + " CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ HÀNG MẪU NÀY\n";
            if (!String.IsNullOrEmpty(ErrorDM))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorDM + " CHƯA CÓ ĐỊNH MỨC .GIẢI PHÁP : KHAI BÁO ĐỊNH MỨC CỦA NHỮNG SẢN PHẨM NÀY\n";
            if (!String.IsNullOrEmpty(ErrorNPLDM))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ]-[MÃ NGUYÊN PHỤ LIỆU] : \n" + ErrorNPLDM + " CÓ ĐỊNH MỨC VỚI NPL CÓ MÃ NÀY TRONG DANH SÁCH ĐỊNH MỨC NHƯNG KHÔNG CÓ TRONG DANH SÁCH NPL ĐƯỢC ĐĂNG KÝ . GIẢI PHÁP : KIỂM TRA VÀ KHAI BÁO PHỤ KIỆN BỔ SUNG NHỮNG MÃ NPL NÀY \n";
            if (!String.IsNullOrEmpty(ErrorSPBK))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG BẢNG KÊ SỐ]-[CÓ SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN] : \n" + ErrorSPBK + " CHƯA ĐƯỢC ĐĂNG KÝ .GIẢI PHÁP : KHAI PHỤ KIỆN BỔ SUNG MÃ NGUYÊN SẢN PHẨM NÀY \n";
            if (!String.IsNullOrEmpty(ErrorNPLBK))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG BẢNG KÊ SỐ]-[CÓ SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[MÃ NGUYÊN PHỤ LIỆU] : \n" + ErrorSPBK + " CÓ ĐỊNH MỨC VỚI NPL CÓ MÃ NÀY TRONG DANH SÁCH ĐỊNH MỨC NHƯNG KHÔNG CÓ TRONG DANH SÁCH NPL ĐƯỢC ĐĂNG KÝ . GIẢI PHÁP : KIỂM TRA VÀ KHAI BÁO PHỤ KIỆN BỔ SUNG NHỮNG MÃ NPL NÀY \n";

            if (!String.IsNullOrEmpty(ErrorLSX))
                ErrorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[TRONG TỜ KHAI]-[CÓ NGÀY ĐĂNG KÝ] : \n" + ErrorLSX + " CHƯA ĐĂNG KÝ HOẶC CẬP NHẬT LỆNH SẢN XUẤT\n";

            if (!String.IsNullOrEmpty(ErrorTotal))
            {
                throw new Exception(ErrorTotal);
            }
        }

        public void TinhToanCanDoiHopDong(int SoThapPhanNPL, int SoThapPhanSP, int SoThapPhanDM, int SoThapPhanTLHH, string dbName)
        {
            this.Load(null, dbName);

            #region Xu ly NPL
            Company.GC.BLL.GC.NguyenPhuLieu NPLData = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLData.HopDong_ID = this.ID;
            NguyenPhuLieuCollection NPLCollectionData = NPLData.SelectCollectionBy_HopDong_ID(null, dbName);

            foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
            {
                NPLDataItem.SoLuongCungUng = 0;
                NPLDataItem.SoLuongDaDung = 0;
                NPLDataItem.SoLuongDaNhap = 0;
            }
            #endregion Xu ly NPL

            PhuKienDangKy PhuKienDK = new PhuKienDangKy();
            PhuKienDK.HopDong_ID = this.ID;
            PhuKienDangKyCollection PhuKienCollection = PhuKienDK.SelectCollectionDynamic("HopDong_ID=" + this.ID, "NgayPhuKien", null, dbName);
            foreach (PhuKienDangKy pkdk in PhuKienCollection)
            {
                pkdk.LoadCollection(null, dbName);
                foreach (LoaiPhuKien LoaiPK in pkdk.PKCollection)
                {
                    LoaiPK.LoadCollection(null, dbName);
                }
            }

            #region Xu ly SP
            Company.GC.BLL.GC.SanPham SPData = new Company.GC.BLL.GC.SanPham();
            SPData.HopDong_ID = this.ID;
            BLL.GC.SanPhamCollection SPCollectionData = SPData.SelectCollectionBy_HopDong_ID(null, dbName);
            foreach (Company.GC.BLL.GC.SanPham SPDataItem in SPCollectionData)
            {
                SPDataItem.SoLuongDaXuat = 0;
            }
            #endregion Xu ly SP

            #region Xu ly TB
            Company.GC.BLL.GC.ThietBi TBData = new Company.GC.BLL.GC.ThietBi();
            TBData.HopDong_ID = this.ID;
            BLL.GC.ThietBiCollection TBCollectionData = TBData.SelectCollectionBy_HopDong_ID(null, dbName);
            foreach (Company.GC.BLL.GC.ThietBi TBDataItem in TBCollectionData)
            {
                TBDataItem.SoLuongDaNhap = 0;
            }
            #endregion Xu ly TB

            #region Xu ly To khai chuyen tiep
            ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
            TKCT.IDHopDong = this.ID;
            List<ToKhaiChuyenTiep> TKCTCollection = TKCT.SelectCollectionBy_IDHopDong(null, dbName);

            foreach (ToKhaiChuyenTiep tkct in TKCTCollection)
            {
                //TODO: Hungtq updated 03/04/2013. Loai tru to khai co trang thai: Cho huy (11), Da huy (10)
                if (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || (tkct.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && tkct.MaLoaiHinh.Substring(0, 1) == "N")
                    )
                {
                    continue;
                }

                #region xu ly to khai chuyen tiep truoc
                tkct.LoadHCTCollection(null, dbName);
                string LoaiHangHoa = "N";
                if (tkct.MaLoaiHinh.IndexOf("SP") > 0 || tkct.MaLoaiHinh.IndexOf("19") > 0)
                {
                    if (tkct.MaLoaiHinh.EndsWith("X") || tkct.MaLoaiHinh.StartsWith("X"))
                        LoaiHangHoa = "S";
                }
                else if (tkct.MaLoaiHinh.IndexOf("TB") > 0 || tkct.MaLoaiHinh.IndexOf("20") > 0)
                    LoaiHangHoa = "T";
                foreach (HangChuyenTiep HCT in tkct.HCTCollection)
                {
                    decimal i = 1;
                    if (!tkct.MaLoaiHinh.Trim().EndsWith("N") || !tkct.MaLoaiHinh.Trim().StartsWith("N"))
                    {
                        i = -1;
                    }
                    else
                    {
                        i = 1;
                    }
                    if (LoaiHangHoa == "N")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl = getNPL(HCT.MaHang, NPLCollectionData);

                        if (npl == null)
                        {
                            continue;
                            String msg = "Mã nguyên phụ liệu : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            if (tkct.SoToKhai == 0)
                                msg += "ID=" + tkct.ID;
                            else
                            {
                                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            }
                            msg += " chưa được đăng ký.";
                            throw new Exception(msg);
                        }

                        if (i > 0)
                            npl.SoLuongDaNhap += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        else
                            npl.SoLuongDaDung += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                    }
                    else if (LoaiHangHoa == "T")
                    {
                        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                        tb = getTB(HCT.MaHang, TBCollectionData);
                        if (tb == null)
                        {
                            continue;
                            String msg = "Mã thiết bị : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            if (tkct.SoToKhai == 0)
                                msg += "ID=" + tkct.ID;
                            else
                            {
                                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            }
                            msg += " chưa được đăng ký.";
                            throw new Exception(msg);
                        }
                        tb.SoLuongDaNhap += HCT.SoLuong * i;
                    }
                    else
                    {

                        if (tkct.MaLoaiHinh.Trim().EndsWith("X") || tkct.MaLoaiHinh.Trim().StartsWith("X"))
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(HCT.MaHang, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                if (tkct.SoToKhai == 0)
                                    msg += "ID=" + tkct.ID;
                                else
                                {
                                    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            sp.SoLuongDaXuat += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);
                            ////minhnd
                            //#region xử lý Sản phẩm
                            //DataSet dsLuongNPL = new DataSet();
                            //if (isXuLySP)
                            //{
                            //    dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCT.MaHang.Trim(), Math.Round(HCT.SoLuong, SoThapPhanSP), this.ID, SoThapPhanNPL, null, dbName);
                            //    if (dsLuongNPL.Tables[0].Rows.Count == 0)
                            //    {
                            //        continue;
                            //        string msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            //        if (tkct.SoToKhai == 0)
                            //            msg += "ID=" + tkct.ID;
                            //        else
                            //        {
                            //            msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            //        }
                            //        msg += " chưa có định mức.";
                            //        throw new Exception(msg);
                            //    }
                            //    foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                            //    {
                            //        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            //        string maNPL = row["MaNguyenPhuLieu"].ToString();
                            //        npl = getNPL(maNPL, NPLCollectionData);
                            //        if (npl == null)
                            //        {
                            //            continue;
                            //            string msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            //            if (tkct.SoToKhai == 0)
                            //                msg += "ID=" + tkct.ID;
                            //            else
                            //            {
                            //                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            //            }
                            //            msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                            //            throw new Exception(msg);
                            //        }
                            //        npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                            //    }
                            //}
                            //#endregion
                            DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCT.MaHang.Trim(), Math.Round(HCT.SoLuong, SoThapPhanSP), this.ID, SoThapPhanNPL, null, dbName);
                            if (dsLuongNPL.Tables[0].Rows.Count == 0)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                if (tkct.SoToKhai == 0)
                                    msg += "ID=" + tkct.ID;
                                else
                                {
                                    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                }
                                msg += " chưa có định mức.";
                                throw new Exception(msg);
                            }
                            foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = row["MaNguyenPhuLieu"].ToString();
                                npl = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                    if (tkct.SoToKhai == 0)
                                        msg += "ID=" + tkct.ID;
                                    else
                                    {
                                        msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                            }
                        }

                    }
                }
                #endregion xu ly to khai chuyen tiep truoc

                #region xu ly bang ke cung ung cho to khai chuyen tiep nay

                if (tkct.MaLoaiHinh.Trim().EndsWith("SPX") || tkct.MaLoaiHinh.Trim().Equals("XGC19"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkct.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKCT_ID=" + tkct.ID, "", null, dbName);
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection(null, dbName);
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                if (BKCungUng.SoBangKe == 0)
                                    msg += "ID=" + BKCungUng.ID;
                                else
                                {
                                    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            spCungUng.LoadNPLCungUngCollection(null, dbName);
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    if (BKCungUng.SoBangKe == 0)
                                        msg += "ID=" + BKCungUng.ID;
                                    else
                                    {
                                        msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                            }
                        }
                    }
                }

                #endregion xu ly bang ke cung ung cho to khai chuyen tiep nay
            }
            #endregion Xu ly To khai chuyen tiep

            #region xu ly to khai mau dich
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.IDHopDong = this.ID;
            ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionBy_IDHopDong(null, dbName);


            foreach (ToKhaiMauDich tkmd in TKMDCollection)
            {
                //TODO: Hungtq updated 03/04/2013. Loai tru to khai co trang thai: Cho huy (11), Da huy (10)
                if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || (tkmd.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && tkmd.MaLoaiHinh.Substring(0, 1) == "N")
                    )
                {
                    continue;
                }
                #region xu ly to khai truoc
                tkmd.LoadHMDCollection(null, dbName);

                string LoaiHangHoa = tkmd.LoaiHangHoa;
                foreach (HangMauDich HMD in tkmd.HMDCollection)
                {
                    if (tkmd.MaLoaiHinh.StartsWith("NGC"))
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Nhap NPL
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl = getNPL(HMD.MaPhu, NPLCollectionData);
                            if (npl == null)
                            {
                                continue;
                                String msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                if (tkmd.SoToKhai == 0)
                                    msg += "ID=" + tkmd.ID;
                                else
                                {
                                    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            npl.SoLuongDaNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                            #endregion Nhap NPL
                        }
                        else
                        {
                            #region Nhap Thiet BI
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb = getTB(HMD.MaPhu, TBCollectionData);
                            if (tb == null)
                            {
                                continue;
                                String msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                if (tkmd.SoToKhai == 0)
                                    msg += "ID=" + tkmd.ID;
                                else
                                {
                                    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            tb.SoLuongDaNhap += HMD.SoLuong;
                            #endregion Nhap Thiet BI
                        }
                    }
                    //else if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    //{
                    //    #region Nhap NPL ben loai hinh sxxk
                    //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                    //    if (npl == null)
                    //    {
                    //        
                    //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                    //        if (tkmd.SoToKhai == 0)
                    //            msg += "ID=" + tkmd.ID;
                    //        else
                    //        {
                    //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                    //        }
                    //        msg += " chưa được đăng ký.";
                    //        throw new Exception(msg);
                    //    }
                    //    npl.SoLuongCungUng += Math.Round(HMD.SoLuong,SoThapPhanNPL);
                    //    #endregion Nhap NPL
                    //}
                    else
                    {
                        //to khai xuat ben loai hinh gc
                        if (tkmd.MaLoaiHinh.StartsWith("XGC"))
                        {
                            if (LoaiHangHoa == "N")
                            {
                                #region Xuat NPL
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl = getNPL(HMD.MaPhu, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                npl.SoLuongDaDung += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                #endregion Xuat NPL
                            }
                            else if (LoaiHangHoa == "T")
                            {
                                #region Xuat TB
                                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                                tb = getTB(HMD.MaPhu, TBCollectionData);
                                if (tb == null)
                                {
                                    continue;
                                    String msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                tb.SoLuongDaNhap -= HMD.SoLuong;
                                #endregion Xuat Thiet bi
                            }
                            else
                            {
                                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                sp = getSP(HMD.MaPhu, SPCollectionData);
                                if (sp == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch  : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                sp.SoLuongDaXuat += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL, null, dbName);
                                if (dsLuongNPL.Tables[0].Rows.Count == 0)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa có định mức.";
                                    throw new Exception(msg);
                                }
                                foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                {
                                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                    string maNPL = row["MaNguyenPhuLieu"].ToString();
                                    npl = getNPL(maNPL, NPLCollectionData);
                                    if (npl == null)
                                    {
                                        continue;
                                        String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                        if (tkmd.SoToKhai == 0)
                                            msg += "ID=" + tkmd.ID;
                                        else
                                        {
                                            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                        }
                                        msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                        throw new Exception(msg);
                                    }
                                    npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                }
                            }
                        }
                        //else
                        //{
                        //    #region Tai Xuat NPL ben loai hinh SXXK
                        //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                        //    if (npl == null)
                        //    {

                        //        
                        //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                        //        if (tkmd.SoToKhai == 0)
                        //            msg += "ID=" + tkmd.ID;
                        //        else
                        //        {
                        //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                        //        }
                        //        msg += " chưa được đăng ký.";
                        //        throw new Exception(msg);
                        //    }
                        //    if (npl.Ma == "410799000015")
                        //    {

                        //    }
                        //    npl.SoLuongCungUng -=Math.Round(HMD.SoLuong,SoThapPhanNPL);
                        //    #endregion Xuat NPL
                        //}
                    }

                }
                #endregion xu ly to khai chuyen tiep truoc

                #region xu ly bang ke cung ung cho to khai chuyen tiep nay

                if (tkmd.MaLoaiHinh.StartsWith("X") || tkmd.MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkmd.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKMD_ID=" + tkmd.ID, "", null, dbName);
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection(null, dbName);
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                if (BKCungUng.SoBangKe == 0)
                                    msg += "ID=" + BKCungUng.ID;
                                else
                                {
                                    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            spCungUng.LoadNPLCungUngCollection(null, dbName);
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    if (BKCungUng.SoBangKe == 0)
                                        msg += "ID=" + BKCungUng.ID;
                                    else
                                    {
                                        msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                            }
                        }
                    }
                }

                #endregion xu ly bang ke cung ung cho to khai chuyen tiep nay
            }
            #endregion xu ly to khai mau dich

            #region Xu ly phu kien mua vn

            DataSet dsMuaVN = PhuKienDangKy.getDanhSachPhuKienMuaVN(this.ID, null, dbName);
            foreach (DataRow row in dsMuaVN.Tables[0].Rows)
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                string MaNPL = row["MaHang"].ToString();
                long ID_PhuKien = Convert.ToInt64(row["id"].ToString());
                npl = getNPL(MaNPL, NPLCollectionData);
                if (npl == null)
                {
                    string msg = "Mã nguyên phụ liệu : " + MaNPL + " trong tờ khai mậu dịch có ";
                    msg += "ID=" + ID_PhuKien;
                    msg += " chưa đăng ký.";
                    throw new Exception(msg);
                }
                npl.SoLuongCungUng += Convert.ToDecimal(row["SoLuong"].ToString());
            }

            #endregion Xu ly phu kien mua vn

            SetDabaseMoi(dbName);

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
                    {
                        NPLDataItem.UpdateTransaction(transaction, dbName);
                    }

                    foreach (Company.GC.BLL.GC.SanPham SPDataItem in SPCollectionData)
                    {
                        SPDataItem.UpdateTransaction(transaction, dbName);
                    }
                    foreach (Company.GC.BLL.GC.ThietBi TBDataItem in TBCollectionData)
                    {
                        TBDataItem.UpdateTransaction(transaction, dbName);
                    }

                    //foreach (PhuKienDangKy pkdk in PhuKienCollection)
                    //{
                    //   pkdk.InsertUpDatePhuKienXuLyDuLieu(transaction);
                    //}

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void TinhToanCanDoiHopDongKTX(int SoThapPhanNPL, int SoThapPhanSP, int SoThapPhanDM, int SoThapPhanTLHH, string dbName)
        {
            this.Load(null, dbName);

            #region Xu ly NPL
            Company.GC.BLL.GC.NguyenPhuLieu NPLData = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLData.HopDong_ID = this.ID;
            NguyenPhuLieuCollection NPLCollectionData = NPLData.SelectCollectionBy_HopDong_ID(null, dbName);

            foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
            {
                NPLDataItem.SoLuongCungUng = 0;
                NPLDataItem.SoLuongDaDung = 0;
                NPLDataItem.SoLuongDaNhap = 0;
            }
            #endregion Xu ly NPL

            PhuKienDangKy PhuKienDK = new PhuKienDangKy();
            PhuKienDK.HopDong_ID = this.ID;
            PhuKienDangKyCollection PhuKienCollection = PhuKienDK.SelectCollectionDynamicKTX("HopDong_ID=" + this.ID, "NgayPhuKien", null, dbName);
            foreach (PhuKienDangKy pkdk in PhuKienCollection)
            {
                pkdk.LoadCollection(null, dbName);
                foreach (LoaiPhuKien LoaiPK in pkdk.PKCollection)
                {
                    LoaiPK.LoadCollection(null, dbName);
                }
            }

            #region Xu ly SP
            Company.GC.BLL.GC.SanPham SPData = new Company.GC.BLL.GC.SanPham();
            SPData.HopDong_ID = this.ID;
            BLL.GC.SanPhamCollection SPCollectionData = SPData.SelectCollectionBy_HopDong_IDKTX(null, dbName);
            foreach (Company.GC.BLL.GC.SanPham SPDataItem in SPCollectionData)
            {
                SPDataItem.SoLuongDaXuat = 0;
            }
            #endregion Xu ly SP

            #region Xu ly TB
            Company.GC.BLL.GC.ThietBi TBData = new Company.GC.BLL.GC.ThietBi();
            TBData.HopDong_ID = this.ID;
            BLL.GC.ThietBiCollection TBCollectionData = TBData.SelectCollectionBy_HopDong_ID(null, dbName);
            foreach (Company.GC.BLL.GC.ThietBi TBDataItem in TBCollectionData)
            {
                TBDataItem.SoLuongDaNhap = 0;
            }
            #endregion Xu ly TB

            #region Xu ly To khai chuyen tiep
            ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
            TKCT.IDHopDong = this.ID;
            List<ToKhaiChuyenTiep> TKCTCollection = TKCT.SelectCollectionBy_IDHopDong_KTX(null, dbName);
            foreach (ToKhaiChuyenTiep tkct in TKCTCollection)
            {
                #region xu ly to khai chuyen tiep truoc
                tkct.LoadHCTCollectionKTX(null, dbName);
                string LoaiHangHoa = "N";
                if (tkct.MaLoaiHinh.IndexOf("SP") > 0 || tkct.MaLoaiHinh.IndexOf("19") > 0)
                {
                    if (tkct.MaLoaiHinh.EndsWith("X") || tkct.MaLoaiHinh.StartsWith("X"))
                        LoaiHangHoa = "S";
                }
                else if (tkct.MaLoaiHinh.IndexOf("TB") > 0 || tkct.MaLoaiHinh.IndexOf("20") > 0)
                    LoaiHangHoa = "T";
                foreach (HangChuyenTiep HCT in tkct.HCTCollection)
                {
                    decimal i = 1;
                    if (!tkct.MaLoaiHinh.Trim().EndsWith("N") || !tkct.MaLoaiHinh.Trim().StartsWith("N"))
                    {
                        i = -1;
                    }
                    else
                    {
                        i = 1;
                    }
                    if (LoaiHangHoa == "N")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl = getNPL(HCT.MaHang, NPLCollectionData);

                        if (npl == null)
                        {
                            continue;
                            String msg = "Mã nguyên phụ liệu : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            if (tkct.SoToKhai == 0)
                                msg += "ID=" + tkct.ID;
                            else
                            {
                                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            }
                            msg += " chưa được đăng ký.";
                            throw new Exception(msg);
                        }

                        if (i > 0)
                            npl.SoLuongDaNhap += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        else
                            npl.SoLuongDaDung += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                    }
                    else if (LoaiHangHoa == "T")
                    {
                        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                        tb = getTB(HCT.MaHang, TBCollectionData);
                        if (tb == null)
                        {
                            continue;
                            String msg = "Mã thiết bị : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            if (tkct.SoToKhai == 0)
                                msg += "ID=" + tkct.ID;
                            else
                            {
                                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            }
                            msg += " chưa được đăng ký.";
                            throw new Exception(msg);
                        }
                        tb.SoLuongDaNhap += HCT.SoLuong * i;
                    }
                    else
                    {
                        if (tkct.MaLoaiHinh.Trim().EndsWith("X") || tkct.MaLoaiHinh.Trim().StartsWith("X"))
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(HCT.MaHang, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                if (tkct.SoToKhai == 0)
                                    msg += "ID=" + tkct.ID;
                                else
                                {
                                    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            sp.SoLuongDaXuat += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);

                            DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCT.MaHang.Trim(), Math.Round(HCT.SoLuong, SoThapPhanSP), this.ID, SoThapPhanNPL, null, dbName);
                            if (dsLuongNPL.Tables[0].Rows.Count == 0)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                if (tkct.SoToKhai == 0)
                                    msg += "ID=" + tkct.ID;
                                else
                                {
                                    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                }
                                msg += " chưa có định mức.";
                                throw new Exception(msg);
                            }
                            foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = row["MaNguyenPhuLieu"].ToString();
                                npl = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                    if (tkct.SoToKhai == 0)
                                        msg += "ID=" + tkct.ID;
                                    else
                                    {
                                        msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                            }
                        }

                    }
                }
                #endregion xu ly to khai chuyen tiep truoc

                #region xu ly bang ke cung ung cho to khai chuyen tiep nay

                if (tkct.MaLoaiHinh.Trim().EndsWith("SPX") || tkct.MaLoaiHinh.Trim().Equals("XGC19"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkct.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKCT_ID=" + tkct.ID, "", null, dbName);
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection(null, dbName);
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                if (BKCungUng.SoBangKe == 0)
                                    msg += "ID=" + BKCungUng.ID;
                                else
                                {
                                    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            spCungUng.LoadNPLCungUngCollection(null, dbName);
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    if (BKCungUng.SoBangKe == 0)
                                        msg += "ID=" + BKCungUng.ID;
                                    else
                                    {
                                        msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                            }
                        }
                    }
                }

                #endregion xu ly bang ke cung ung cho to khai chuyen tiep nay
            }
            #endregion Xu ly To khai chuyen tiep

            #region xu ly to khai mau dich
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.IDHopDong = this.ID;
            ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionBy_IDHopDong_KTX(null, dbName);
            foreach (ToKhaiMauDich tkmd in TKMDCollection)
            {
                #region xu ly to khai truoc
                tkmd.LoadHMDCollection(null, dbName);

                string LoaiHangHoa = tkmd.LoaiHangHoa;
                foreach (HangMauDich HMD in tkmd.HMDCollection)
                {
                    if (tkmd.MaLoaiHinh.StartsWith("NGC"))
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Nhap NPL
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl = getNPL(HMD.MaPhu, NPLCollectionData);
                            if (npl == null)
                            {
                                continue;
                                String msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                if (tkmd.SoToKhai == 0)
                                    msg += "ID=" + tkmd.ID;
                                else
                                {
                                    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            npl.SoLuongDaNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                            #endregion Nhap NPL
                        }
                        else
                        {
                            #region Nhap Thiet BI
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb = getTB(HMD.MaPhu, TBCollectionData);
                            if (tb == null)
                            {
                                continue;
                                String msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                if (tkmd.SoToKhai == 0)
                                    msg += "ID=" + tkmd.ID;
                                else
                                {
                                    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            tb.SoLuongDaNhap += HMD.SoLuong;
                            #endregion Nhap Thiet BI
                        }
                    }
                    //else if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    //{
                    //    #region Nhap NPL ben loai hinh sxxk
                    //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                    //    if (npl == null)
                    //    {
                    //        
                    //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                    //        if (tkmd.SoToKhai == 0)
                    //            msg += "ID=" + tkmd.ID;
                    //        else
                    //        {
                    //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                    //        }
                    //        msg += " chưa được đăng ký.";
                    //        throw new Exception(msg);
                    //    }
                    //    npl.SoLuongCungUng += Math.Round(HMD.SoLuong,SoThapPhanNPL);
                    //    #endregion Nhap NPL
                    //}
                    else
                    {
                        //to khai xuat ben loai hinh gc
                        if (tkmd.MaLoaiHinh.StartsWith("XGC"))
                        {
                            if (LoaiHangHoa == "N")
                            {
                                #region Xuat NPL
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl = getNPL(HMD.MaPhu, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                npl.SoLuongDaDung += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                #endregion Xuat NPL
                            }
                            else if (LoaiHangHoa == "T")
                            {
                                #region Xuat TB
                                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                                tb = getTB(HMD.MaPhu, TBCollectionData);
                                if (tb == null)
                                {
                                    continue;
                                    String msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                tb.SoLuongDaNhap -= HMD.SoLuong;
                                #endregion Xuat Thiet bi
                            }
                            else
                            {
                                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                sp = getSP(HMD.MaPhu, SPCollectionData);
                                if (sp == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch  : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                sp.SoLuongDaXuat += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL, null, dbName);
                                if (dsLuongNPL.Tables[0].Rows.Count == 0)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa có định mức.";
                                    throw new Exception(msg);
                                }
                                foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                {
                                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                    string maNPL = row["MaNguyenPhuLieu"].ToString();
                                    npl = getNPL(maNPL, NPLCollectionData);
                                    if (npl == null)
                                    {
                                        continue;
                                        String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                        if (tkmd.SoToKhai == 0)
                                            msg += "ID=" + tkmd.ID;
                                        else
                                        {
                                            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                        }
                                        msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                        throw new Exception(msg);
                                    }
                                    npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                }
                            }
                        }
                        //else
                        //{
                        //    #region Tai Xuat NPL ben loai hinh SXXK
                        //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                        //    if (npl == null)
                        //    {

                        //        
                        //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                        //        if (tkmd.SoToKhai == 0)
                        //            msg += "ID=" + tkmd.ID;
                        //        else
                        //        {
                        //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                        //        }
                        //        msg += " chưa được đăng ký.";
                        //        throw new Exception(msg);
                        //    }
                        //    if (npl.Ma == "410799000015")
                        //    {

                        //    }
                        //    npl.SoLuongCungUng -=Math.Round(HMD.SoLuong,SoThapPhanNPL);
                        //    #endregion Xuat NPL
                        //}
                    }

                }
                #endregion xu ly to khai chuyen tiep truoc

                #region xu ly bang ke cung ung cho to khai chuyen tiep nay

                if (tkmd.MaLoaiHinh.StartsWith("X") || tkmd.MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkmd.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKMD_ID=" + tkmd.ID, "", null, dbName);
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection(null, dbName);
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                if (BKCungUng.SoBangKe == 0)
                                    msg += "ID=" + BKCungUng.ID;
                                else
                                {
                                    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            spCungUng.LoadNPLCungUngCollection(null, dbName);
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    if (BKCungUng.SoBangKe == 0)
                                        msg += "ID=" + BKCungUng.ID;
                                    else
                                    {
                                        msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                            }
                        }
                    }
                }

                #endregion xu ly bang ke cung ung cho to khai chuyen tiep nay
            }
            #endregion xu ly to khai mau dich

            #region Xu ly phu kien mua vn

            DataSet dsMuaVN = PhuKienDangKy.getDanhSachPhuKienMuaVN(this.ID, null, dbName);
            foreach (DataRow row in dsMuaVN.Tables[0].Rows)
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                string MaNPL = row["MaHang"].ToString();
                long ID_PhuKien = Convert.ToInt64(row["id"].ToString());
                npl = getNPL(MaNPL, NPLCollectionData);
                if (npl == null)
                {
                    string msg = "Mã nguyên phụ liệu : " + MaNPL + " trong tờ khai mậu dịch có ";
                    msg += "ID=" + ID_PhuKien;
                    msg += " chưa đăng ký.";
                    throw new Exception(msg);
                }
                npl.SoLuongCungUng += Convert.ToDecimal(row["SoLuong"].ToString());
            }

            #endregion Xu ly phu kien mua vn

            SetDabaseMoi(dbName);

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
                    {
                        NPLDataItem.UpdateTransaction(transaction, dbName);
                    }

                    foreach (Company.GC.BLL.GC.SanPham SPDataItem in SPCollectionData)
                    {
                        SPDataItem.UpdateTransactionKTX(transaction, dbName);
                    }
                    foreach (Company.GC.BLL.GC.ThietBi TBDataItem in TBCollectionData)
                    {
                        TBDataItem.UpdateTransaction(transaction, dbName);
                    }

                    //foreach (PhuKienDangKy pkdk in PhuKienCollection)
                    //{
                    //   pkdk.InsertUpDatePhuKienXuLyDuLieu(transaction);
                    //}

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void TinhToanCanDoiHopDong(int SoThapPhanNPL, int SoThapPhanSP, int SoThapPhanDM, int SoThapPhanTLHH, SqlTransaction trans, string dbName)
        {
            this.Load(trans, dbName);

            #region Xu ly NPL
            Company.GC.BLL.GC.NguyenPhuLieu NPLData = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLData.HopDong_ID = this.ID;
            NguyenPhuLieuCollection NPLCollectionData = NPLData.SelectCollectionBy_HopDong_ID(trans, dbName);

            foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
            {
                NPLDataItem.SoLuongCungUng = 0;
                NPLDataItem.SoLuongDaDung = 0;
                NPLDataItem.SoLuongDaNhap = 0;
            }
            #endregion Xu ly NPL

            PhuKienDangKy PhuKienDK = new PhuKienDangKy();
            PhuKienDK.HopDong_ID = this.ID;
            PhuKienDangKyCollection PhuKienCollection = PhuKienDK.SelectCollectionDynamic("HopDong_ID=" + this.ID, "NgayPhuKien", trans, dbName);
            foreach (PhuKienDangKy pkdk in PhuKienCollection)
            {
                pkdk.LoadCollection(trans, dbName);
                foreach (LoaiPhuKien LoaiPK in pkdk.PKCollection)
                {
                    LoaiPK.LoadCollection(trans, dbName);
                }
            }

            #region Xu ly SP
            Company.GC.BLL.GC.SanPham SPData = new Company.GC.BLL.GC.SanPham();
            SPData.HopDong_ID = this.ID;
            BLL.GC.SanPhamCollection SPCollectionData = SPData.SelectCollectionBy_HopDong_ID(trans, dbName);
            foreach (Company.GC.BLL.GC.SanPham SPDataItem in SPCollectionData)
            {
                SPDataItem.SoLuongDaXuat = 0;
            }
            #endregion Xu ly SP

            #region Xu ly TB
            Company.GC.BLL.GC.ThietBi TBData = new Company.GC.BLL.GC.ThietBi();
            TBData.HopDong_ID = this.ID;
            BLL.GC.ThietBiCollection TBCollectionData = TBData.SelectCollectionBy_HopDong_ID(trans, dbName);
            foreach (Company.GC.BLL.GC.ThietBi TBDataItem in TBCollectionData)
            {
                TBDataItem.SoLuongDaNhap = 0;
            }
            #endregion Xu ly TB

            #region Xu ly To khai chuyen tiep
            ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
            TKCT.IDHopDong = this.ID;
            List<ToKhaiChuyenTiep> TKCTCollection = TKCT.SelectCollectionBy_IDHopDong(trans, dbName);
            foreach (ToKhaiChuyenTiep tkct in TKCTCollection)
            {
                #region xu ly to khai chuyen tiep truoc
                tkct.LoadHCTCollection(trans, dbName);
                string LoaiHangHoa = "N";
                if (tkct.MaLoaiHinh.IndexOf("SP") > 0 || tkct.MaLoaiHinh.IndexOf("19") > 0)
                {
                    if (tkct.MaLoaiHinh.EndsWith("X") || tkct.MaLoaiHinh.StartsWith("X"))
                        LoaiHangHoa = "S";
                }
                else if (tkct.MaLoaiHinh.IndexOf("TB") > 0 || tkct.MaLoaiHinh.IndexOf("20") > 0)
                    LoaiHangHoa = "T";
                foreach (HangChuyenTiep HCT in tkct.HCTCollection)
                {
                    decimal i = 1;
                    if (!tkct.MaLoaiHinh.Trim().EndsWith("N") || !tkct.MaLoaiHinh.Trim().StartsWith("N"))
                    {
                        i = -1;
                    }
                    else
                    {
                        i = 1;
                    }
                    if (LoaiHangHoa == "N")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl = getNPL(HCT.MaHang, NPLCollectionData);

                        if (npl == null)
                        {
                            continue;
                            String msg = "Mã nguyên phụ liệu : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            if (tkct.SoToKhai == 0)
                                msg += "ID=" + tkct.ID;
                            else
                            {
                                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            }
                            msg += " chưa được đăng ký.";
                            throw new Exception(msg);
                        }

                        if (i > 0)
                            npl.SoLuongDaNhap += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                        else
                            npl.SoLuongDaDung += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                    }
                    else if (LoaiHangHoa == "T")
                    {
                        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                        tb = getTB(HCT.MaHang, TBCollectionData);
                        if (tb == null)
                        {
                            continue;
                            String msg = "Mã thiết bị : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            if (tkct.SoToKhai == 0)
                                msg += "ID=" + tkct.ID;
                            else
                            {
                                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            }
                            msg += " chưa được đăng ký.";
                            throw new Exception(msg);
                        }
                        tb.SoLuongDaNhap += HCT.SoLuong * i;
                    }
                    else
                    {
                        if (tkct.MaLoaiHinh.Trim().EndsWith("X") || tkct.MaLoaiHinh.Trim().StartsWith("X"))
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(HCT.MaHang, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                if (tkct.SoToKhai == 0)
                                    msg += "ID=" + tkct.ID;
                                else
                                {
                                    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            sp.SoLuongDaXuat += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);

                            DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCT.MaHang.Trim(), Math.Round(HCT.SoLuong, SoThapPhanSP), this.ID, SoThapPhanNPL, trans, dbName);
                            if (dsLuongNPL.Tables[0].Rows.Count == 0)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                if (tkct.SoToKhai == 0)
                                    msg += "ID=" + tkct.ID;
                                else
                                {
                                    msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                }
                                msg += " chưa có định mức.";
                                throw new Exception(msg);
                            }
                            foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = row["MaNguyenPhuLieu"].ToString();
                                npl = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                    if (tkct.SoToKhai == 0)
                                        msg += "ID=" + tkct.ID;
                                    else
                                    {
                                        msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                            }
                        }

                    }
                }
                #endregion xu ly to khai chuyen tiep truoc

                #region xu ly bang ke cung ung cho to khai chuyen tiep nay

                if (tkct.MaLoaiHinh.Trim().EndsWith("SPX") || tkct.MaLoaiHinh.Trim().Equals("XGC19"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkct.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKCT_ID=" + tkct.ID, "", trans, dbName);
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection(trans, dbName);
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                if (BKCungUng.SoBangKe == 0)
                                    msg += "ID=" + BKCungUng.ID;
                                else
                                {
                                    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            spCungUng.LoadNPLCungUngCollection(trans, dbName);
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    if (BKCungUng.SoBangKe == 0)
                                        msg += "ID=" + BKCungUng.ID;
                                    else
                                    {
                                        msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                            }
                        }
                    }
                }

                #endregion xu ly bang ke cung ung cho to khai chuyen tiep nay
            }
            #endregion Xu ly To khai chuyen tiep

            #region xu ly to khai mau dich
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.IDHopDong = this.ID;
            ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionBy_IDHopDong(trans, dbName);
            foreach (ToKhaiMauDich tkmd in TKMDCollection)
            {
                #region xu ly to khai truoc
                tkmd.LoadHMDCollection(trans, dbName);

                string LoaiHangHoa = tkmd.LoaiHangHoa;
                foreach (HangMauDich HMD in tkmd.HMDCollection)
                {
                    if (tkmd.MaLoaiHinh.StartsWith("NGC"))
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Nhap NPL
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl = getNPL(HMD.MaPhu, NPLCollectionData);
                            if (npl == null)
                            {
                                continue;
                                String msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                if (tkmd.SoToKhai == 0)
                                    msg += "ID=" + tkmd.ID;
                                else
                                {
                                    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            npl.SoLuongDaNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                            #endregion Nhap NPL
                        }
                        else
                        {
                            #region Nhap Thiet BI
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb = getTB(HMD.MaPhu, TBCollectionData);
                            if (tb == null)
                            {
                                continue;
                                String msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                if (tkmd.SoToKhai == 0)
                                    msg += "ID=" + tkmd.ID;
                                else
                                {
                                    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            tb.SoLuongDaNhap += HMD.SoLuong;
                            #endregion Nhap Thiet BI
                        }
                    }
                    //else if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    //{
                    //    #region Nhap NPL ben loai hinh sxxk
                    //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                    //    if (npl == null)
                    //    {
                    //        
                    //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                    //        if (tkmd.SoToKhai == 0)
                    //            msg += "ID=" + tkmd.ID;
                    //        else
                    //        {
                    //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                    //        }
                    //        msg += " chưa được đăng ký.";
                    //        throw new Exception(msg);
                    //    }
                    //    npl.SoLuongCungUng += Math.Round(HMD.SoLuong,SoThapPhanNPL);
                    //    #endregion Nhap NPL
                    //}
                    else
                    {
                        //to khai xuat ben loai hinh gc
                        if (tkmd.MaLoaiHinh.StartsWith("XGC"))
                        {
                            if (LoaiHangHoa == "N")
                            {
                                #region Xuat NPL
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl = getNPL(HMD.MaPhu, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                npl.SoLuongDaDung += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                #endregion Xuat NPL
                            }
                            else if (LoaiHangHoa == "T")
                            {
                                #region Xuat TB
                                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                                tb = getTB(HMD.MaPhu, TBCollectionData);
                                if (tb == null)
                                {
                                    continue;
                                    String msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                tb.SoLuongDaNhap -= HMD.SoLuong;
                                #endregion Xuat Thiet bi
                            }
                            else
                            {
                                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                sp = getSP(HMD.MaPhu, SPCollectionData);
                                if (sp == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch  : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                sp.SoLuongDaXuat += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL, trans, dbName);
                                if (dsLuongNPL.Tables[0].Rows.Count == 0)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa có định mức.";
                                    throw new Exception(msg);
                                }
                                foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                {
                                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                    string maNPL = row["MaNguyenPhuLieu"].ToString();
                                    npl = getNPL(maNPL, NPLCollectionData);
                                    if (npl == null)
                                    {
                                        continue;
                                        String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                        if (tkmd.SoToKhai == 0)
                                            msg += "ID=" + tkmd.ID;
                                        else
                                        {
                                            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                        }
                                        msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                        throw new Exception(msg);
                                    }
                                    npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                }
                            }
                        }
                        //else
                        //{
                        //    #region Tai Xuat NPL ben loai hinh SXXK
                        //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                        //    if (npl == null)
                        //    {

                        //        
                        //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                        //        if (tkmd.SoToKhai == 0)
                        //            msg += "ID=" + tkmd.ID;
                        //        else
                        //        {
                        //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                        //        }
                        //        msg += " chưa được đăng ký.";
                        //        throw new Exception(msg);
                        //    }
                        //    if (npl.Ma == "410799000015")
                        //    {

                        //    }
                        //    npl.SoLuongCungUng -=Math.Round(HMD.SoLuong,SoThapPhanNPL);
                        //    #endregion Xuat NPL
                        //}
                    }

                }
                #endregion xu ly to khai chuyen tiep truoc

                #region xu ly bang ke cung ung cho to khai chuyen tiep nay

                if (tkmd.MaLoaiHinh.StartsWith("X") || tkmd.MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkmd.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKMD_ID=" + tkmd.ID, "", trans, dbName);
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection(trans, dbName);
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp = getSP(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                if (BKCungUng.SoBangKe == 0)
                                    msg += "ID=" + BKCungUng.ID;
                                else
                                {
                                    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            spCungUng.LoadNPLCungUngCollection(trans, dbName);
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu NPLDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = getNPL(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    if (BKCungUng.SoBangKe == 0)
                                        msg += "ID=" + BKCungUng.ID;
                                    else
                                    {
                                        msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                            }
                        }
                    }
                }

                #endregion xu ly bang ke cung ung cho to khai chuyen tiep nay
            }
            #endregion xu ly to khai mau dich

            #region Xu ly phu kien mua vn

            DataSet dsMuaVN = PhuKienDangKy.getDanhSachPhuKienMuaVN(this.ID, trans, dbName);
            foreach (DataRow row in dsMuaVN.Tables[0].Rows)
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                string MaNPL = row["MaHang"].ToString();
                long ID_PhuKien = Convert.ToInt64(row["id"].ToString());
                npl = getNPL(MaNPL, NPLCollectionData);
                if (npl == null)
                {
                    string msg = "Mã nguyên phụ liệu : " + MaNPL + " trong tờ khai mậu dịch có ";
                    msg += "ID=" + ID_PhuKien;
                    msg += " chưa đăng ký.";
                    throw new Exception(msg);
                }
                npl.SoLuongCungUng += Convert.ToDecimal(row["SoLuong"].ToString());
            }

            #endregion Xu ly phu kien mua vn

            SetDabaseMoi(dbName);

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                //SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
                    {
                        NPLDataItem.UpdateTransaction(trans, dbName);
                    }

                    foreach (Company.GC.BLL.GC.SanPham SPDataItem in SPCollectionData)
                    {
                        SPDataItem.UpdateTransaction(trans, dbName);
                    }
                    foreach (Company.GC.BLL.GC.ThietBi TBDataItem in TBCollectionData)
                    {
                        TBDataItem.UpdateTransaction(trans, dbName);
                    }

                    //foreach (PhuKienDangKy pkdk in PhuKienCollection)
                    //{
                    //   pkdk.InsertUpDatePhuKienXuLyDuLieu(transaction);
                    //}

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void ProcessQuyetToanHopDong(int SoThapPhanNPL, int SoThapPhanSP, int SoThapPhanDM, int SoThapPhanTLHH, DateTime dateFrom, DateTime dateTo)
        {
            //string datefrom = String.Concat(dateFrom.ToString("yyyy-MM-dd"), " 00:00:00.00");
            //string dateto = String.Concat(dateTo.ToString("yyyy-MM-dd"), " 23:59:59.999");
            dateFrom = new DateTime(dateFrom.Year, dateFrom.Month, dateFrom.Day,00,00,00);
            dateTo = new DateTime(dateTo.Year, dateTo.Month, dateTo.Day,23,59,59);
            bool isXuLySP = true;//false;
            this.OnProcess(new HopDongEventArgs("Load hợp đồng", 1));
            HopDong.Load(this.ID);
            //DataTable dsMuaVN = KDT_GC_CungUng.GetDSNPLCungUngByHopDong(this.ID);
            DataTable dsMuaVN = KDT_GC_CungUng.GetDSNPLCungUngByHopDong_New(this.ID);
            #region Xu ly PK
            this.OnProcess(new HopDongEventArgs("Xử lý phụ kiện", 2));
            PhuKienDangKy PhuKienDK = new PhuKienDangKy();
            PhuKienDK.HopDong_ID = this.ID;
            PhuKienDangKyCollection PKCollectionData = PhuKienDK.SelectCollectionDynamic("HopDong_ID=" + this.ID, "NgayPhuKien");
            foreach (PhuKienDangKy pkdk in PKCollectionData)
            {
                pkdk.LoadCollection();
                foreach (LoaiPhuKien LoaiPK in pkdk.PKCollection)
                {
                    LoaiPK.LoadCollection();
                }
            }

            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {

                    foreach (PhuKienDangKy pkdk in PKCollectionData)
                    {
                        pkdk.InsertUpDatePhuKienXuLyDuLieu(transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            #endregion Xu ly PK

            #region Xu ly NPL
            this.OnProcess(new HopDongEventArgs("Xử lý Nguyên liệu", 4));
            KDT_GC_QuyetToan_NguyenPhuLieu NPLData = new KDT_GC_QuyetToan_NguyenPhuLieu();
            NPLData.HopDong_ID = this.ID;

            NPLData.DeleteDynamic("HopDong_ID =" + this.ID);
            List<KDT_GC_QuyetToan_NguyenPhuLieu> NPLCollectionData = new List<KDT_GC_QuyetToan_NguyenPhuLieu>();

            List<KDT_GC_QuyetToan_NguyenPhuLieu> NPLCollectionDataTemp = NPLData.SelectCollectionDynamicByHD_ID("HopDong_ID =" + this.ID, null);

            foreach (KDT_GC_QuyetToan_NguyenPhuLieu item in NPLCollectionDataTemp)
            {
                item.LuongNhapDK = 0;
                item.TriGiaNhapDK = 0;
                item.LuongSuDungDK = 0;
                item.TriGiaSuDungDK = 0;
                item.LuongNhapTK = 0;
                item.TriGiaNhapTK = 0;
                item.LuongSuDungTK = 0;
                item.TriGiaSuDungTK = 0;
                item.LuongSuDungNK = 0;
                item.TriGiaSuDungNK = 0;
                item.LuongTonCK = 0;
                item.TriGiaTonCK = 0;
                item.NamQT = dateFrom.Year.ToString();
                NPLCollectionData.Add(item);
            }
            #endregion Xu ly NPL
            #region Xu ly SP
            this.OnProcess(new HopDongEventArgs("Xử lý sản phẩm", 6));
            KDT_GC_QuyetToan_SanPham SPData = new KDT_GC_QuyetToan_SanPham();
            SPData.HopDong_ID = this.ID;
            SPData.DeleteDynamic("HopDong_ID =" + this.ID);
            List<KDT_GC_QuyetToan_SanPham> SPCollectionData = SPData.SelectCollectionDynamicByHD_ID("HopDong_ID =" + this.ID, null);
            foreach (KDT_GC_QuyetToan_SanPham SPDataItem in SPCollectionData)
            {
                SPDataItem.LuongXuatDK = 0;
                SPDataItem.LuongXuatTK = 0;
                SPDataItem.LuongXuatNK = 0;
                SPDataItem.NamQT = dateFrom.Year.ToString();
            }
            #endregion Xu ly SP
            #region Xu ly TB
            this.OnProcess(new HopDongEventArgs("Xử lý thiết bị", 8));
            KDT_GC_QuyetToan_ThietBi TBData = new KDT_GC_QuyetToan_ThietBi();
            TBData.HopDong_ID = this.ID;
            TBData.DeleteDynamic("HopDong_ID =" + this.ID);
            List<KDT_GC_QuyetToan_ThietBi> TBCollectionData = TBData.SelectCollectionDynamicByHD_ID("HopDong_ID =" + this.ID,null);
            foreach (KDT_GC_QuyetToan_ThietBi TBDataItem in TBCollectionData)
            {
                TBDataItem.LuongNhapDK = 0;
                TBDataItem.LuongNhapTK = 0;
                TBDataItem.LuongNhapNK = 0;
                TBDataItem.NamQT = dateFrom.Year.ToString();
            }
            #endregion Xu ly TB
            #region Xu ly HM
            this.OnProcess(new HopDongEventArgs("Xử lý thiết bị", 8));
            KDT_GC_QuyetToan_HangMau HMData = new KDT_GC_QuyetToan_HangMau();
            HMData.HopDong_ID = this.ID;
            HMData.DeleteDynamic("HopDong_ID =" + this.ID);
            List<KDT_GC_QuyetToan_HangMau> HMCollectionData = HMData.SelectCollectionDynamicByHopDong_ID("HopDong_ID =" + this.ID,null);
            foreach (KDT_GC_QuyetToan_HangMau HMDataItem in HMCollectionData)
            {
                HMDataItem.LuongNhapDK = 0;
                HMDataItem.LuongNhapTK = 0;
                HMDataItem.LuongNhapNK = 0;
                HMDataItem.NamQT = dateFrom.Year.ToString();
            }
            #endregion Xu ly HM
            #region Xu ly To khai chuyen tiep
            this.OnProcess(new HopDongEventArgs("Xử lý Tờ khai chuyển tiếp", 9));
            List<ToKhaiChuyenTiep> TKCTCollection = (List<ToKhaiChuyenTiep>)ToKhaiChuyenTiep.SelectCollectionBy_IDHopDong(this.ID);
            //List<ToKhaiChuyenTiep> TKCTCollection = (List<ToKhaiChuyenTiep>)ToKhaiChuyenTiep.SelectCollectionBy_IDHopDong_XNT(this.ID,dateFrom,dateTo);

            foreach (ToKhaiChuyenTiep tkct in TKCTCollection)
            {
                //TODO: Hungtq updated 03/04/2013. Loai tru to khai co trang thai: Cho huy (11), Da huy (10)
                if (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || (tkct.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && tkct.MaLoaiHinh.Substring(0, 1) == "N")
                    )
                {
                    continue;
                }
                int dayTKCT = tkct.NgayDangKy.Day;
                int monthTKCT = tkct.NgayDangKy.Month;
                int yearTKCT = tkct.NgayDangKy.Year;
                DateTime dateTKCT = new DateTime(yearTKCT, monthTKCT, dayTKCT);
                #region xu ly to khai chuyen tiep truoc
                tkct.LoadHCTCollection();
                string LoaiHangHoa = "N";
                if (tkct.MaLoaiHinh.IndexOf("SP") > 0 || tkct.MaLoaiHinh.IndexOf("19") > 0)
                {
                    if (tkct.MaLoaiHinh.EndsWith("X") || tkct.MaLoaiHinh.StartsWith("X"))
                        LoaiHangHoa = "S";
                }
                else if (tkct.MaLoaiHinh.IndexOf("TB") > 0 || tkct.MaLoaiHinh.IndexOf("20") > 0)
                    LoaiHangHoa = "T";
                else if (!string.IsNullOrEmpty(tkct.LoaiHangHoa))
                    LoaiHangHoa = tkct.LoaiHangHoa;
                foreach (HangChuyenTiep HCT in tkct.HCTCollection)
                {
                    decimal i = 1;
                    // Kiểm tra là tờ khai xuất chuyển tiếp hay là nhập chuyển tiếp
                    if (!tkct.MaLoaiHinh.Trim().EndsWith("N") && !tkct.MaLoaiHinh.Trim().Substring(0, 1).Equals("N"))
                    {
                        // Tờ khai xuất
                        i = -1;
                    }
                    else
                    {
                        // Tờ khai nhập
                        i = 1;
                    }

                    if (LoaiHangHoa == "N")
                    {
                        //Nguyên phụ liệu
                        KDT_GC_QuyetToan_NguyenPhuLieu npl = new KDT_GC_QuyetToan_NguyenPhuLieu();
                        ////if(Company.GC.BLL.GC.NguyenPhuLieu.Load_NPL())
                        npl = CheckNPLQT(HCT.MaHang, NPLCollectionData);
                        if (npl == null)
                        {
                            //continue;
                            string msg = "Mã nguyên phụ liệu : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            if (tkct.SoToKhai == 0)
                                msg += "ID=" + tkct.ID;
                            else
                            {
                                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            }
                            msg += " chưa được đăng ký.";
                            throw new Exception(msg);
                        }
                        if (dateTKCT < dateFrom)
                        {
                            if (i > 0)
                            {
                                npl.LuongNhapDK += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                                npl.TriGiaNhapDK += Math.Round(HCT.TriGiaTT, SoThapPhanNPL);
                            }
                            else
                            {
                                npl.LuongSuDungDK += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                                npl.TriGiaSuDungDK += Math.Round(HCT.TriGiaTT, SoThapPhanNPL);
                            }

                        }
                        else if (dateTKCT <= dateTo)
                        {
                            if (i > 0)
                            {
                                npl.LuongNhapTK += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                                npl.TriGiaNhapTK += Math.Round(HCT.TriGiaTT, SoThapPhanNPL);
                            }
                            else
                            {
                                npl.LuongSuDungTK += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                                npl.TriGiaSuDungTK += Math.Round(HCT.TriGiaTT, SoThapPhanNPL);
                            }
                        }
                        else
                        {
                            if (i > 0)
                            {
                                npl.LuongNhapNK += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                                npl.TriGiaNhapNK += Math.Round(HCT.TriGiaTT, SoThapPhanNPL);
                            }
                            else
                            {
                                npl.LuongSuDungNK += Math.Round(HCT.SoLuong, SoThapPhanNPL);
                                npl.TriGiaSuDungNK += Math.Round(HCT.TriGiaTT, SoThapPhanNPL);
                            }
                        }


                    }
                    else if (LoaiHangHoa == "T")
                    {
                        KDT_GC_QuyetToan_ThietBi tb = new KDT_GC_QuyetToan_ThietBi();
                        tb = CheckTBQT(HCT.MaHang, TBCollectionData);
                        if (tb == null)
                        {
                            //continue;
                            string msg = "Mã thiết bị : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                            if (tkct.SoToKhai == 0)
                                msg += "ID=" + tkct.ID;
                            else
                            {
                                msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                            }
                            msg += " chưa được đăng ký.";
                            throw new Exception(msg);
                        }
                        //tb.SoLuongDaNhap += HCT.SoLuong * i;
                        if (dateTKCT < dateFrom)
                        {
                            if (i > 0)
                            {
                                tb.LuongNhapDK += HCT.SoLuong;
                                tb.TriGiaNhapDK += HCT.TriGiaTT;
                            }
                            else
                            {
                                tb.LuongSuDungDK += HCT.SoLuong;
                                tb.TriGiaSuDungDK += HCT.TriGiaTT;
                            }
                        }
                        else if (dateTKCT <= dateTo)
                        {
                            if (i > 0)
                            {
                                tb.LuongNhapTK += HCT.SoLuong;
                                tb.TriGiaNhapTK += HCT.TriGiaTT;
                            }
                            else
                            {
                                tb.LuongSuDungTK += HCT.SoLuong;
                                tb.TriGiaSuDungTK += HCT.TriGiaTT;
                            }
                        }
                        else
                        {
                            if (i > 0)
                            {
                                tb.LuongNhapNK += HCT.SoLuong;
                                tb.TriGiaNhapNK += HCT.TriGiaTT;
                            }
                            else
                            {
                                tb.LuongSuDungNK += HCT.SoLuong;
                                tb.TriGiaSuDungNK += HCT.TriGiaTT;
                            }
                        }

                    }
                    else
                    {
                        //minhnd xử lý sản phẩm
                        #region xử lý SP
                        if (isXuLySP)
                            if (tkct.MaLoaiHinh.Trim().EndsWith("X") || tkct.MaLoaiHinh.Trim().StartsWith("X"))
                            {
                                KDT_GC_QuyetToan_SanPham sp = new KDT_GC_QuyetToan_SanPham();
                                sp = CheckSPQT(HCT.MaHang, SPCollectionData);
                                if (sp == null)
                                {
                                    //continue;
                                    string msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                    if (tkct.SoToKhai == 0)
                                        msg += "ID=" + tkct.ID;
                                    else
                                    {
                                        msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                if (dateTKCT < dateFrom)
                                {
                                    sp.LuongXuatDK += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);
                                    sp.TriGiaXuatDK += (Math.Round(HCT.TriGiaTT, SoThapPhanSP) * i) * (-1);
                                }
                                else if (dateTKCT <= dateTo)
                                {
                                    sp.LuongXuatTK += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);
                                    sp.TriGiaXuatTK += (Math.Round(HCT.TriGiaTT, SoThapPhanSP) * i) * (-1);
                                }
                                else
                                {
                                    sp.LuongXuatNK += (Math.Round(HCT.SoLuong, SoThapPhanSP) * i) * (-1);
                                    sp.TriGiaXuatNK += (Math.Round(HCT.TriGiaTT, SoThapPhanSP) * i) * (-1);
                                }
                                DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HCT.MaHang.Trim(), Math.Round(HCT.SoLuong, SoThapPhanSP), this.ID, SoThapPhanNPL);
                                if (dsLuongNPL.Tables[0].Rows.Count == 0)
                                {
                                    //continue;
                                    string msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                    if (tkct.SoToKhai == 0)
                                        msg += "ID=" + tkct.ID;
                                    else
                                    {
                                        msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                    }
                                    msg += " chưa có định mức.";
                                    throw new Exception(msg);
                                }
                                foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                {
                                    KDT_GC_QuyetToan_NguyenPhuLieu npl = new KDT_GC_QuyetToan_NguyenPhuLieu();
                                    string maNPL = row["MaNguyenPhuLieu"].ToString();
                                    npl = CheckNPLQT(maNPL, NPLCollectionData);
                                    if (npl == null)
                                    {
                                        //continue;
                                        string msg = "Mã sản phẩm : " + HCT.MaHang + " trong tờ khai chuyển tiếp có ";
                                        if (tkct.SoToKhai == 0)
                                            msg += "ID=" + tkct.ID;
                                        else
                                        {
                                            msg += "Số tờ khai chuyển tiếp : " + tkct.SoToKhai.ToString();
                                        }
                                        msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                        throw new Exception(msg);
                                    }
                                    if (dateTKCT < dateFrom)
                                    {
                                        npl.LuongSuDungDK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                        npl.TriGiaSuDungDK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                    }
                                    else if (dateTKCT <= dateTo)
                                    {
                                        npl.LuongSuDungTK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                        npl.TriGiaSuDungTK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                    }
                                    else
                                    {
                                        npl.LuongSuDungNK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                        npl.TriGiaSuDungNK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                    }

                                }
                            }
                        #endregion xử lý SP

                    }
                }
                #endregion

                #region xu ly bang ke cung ung cho to khai chuyen tiep nay

                if (tkct.MaLoaiHinh.Trim().EndsWith("SPX") || tkct.MaLoaiHinh.Trim().Equals("XGC19"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkct.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKCT_ID=" + tkct.ID, "");
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection();
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            KDT_GC_QuyetToan_SanPham sp = new KDT_GC_QuyetToan_SanPham();
                            sp = CheckSPQT(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                //continue;
                                string msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                if (BKCungUng.SoBangKe == 0)
                                    msg += "ID=" + BKCungUng.ID;
                                else
                                {
                                    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            spCungUng.LoadNPLCungUngCollection();
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                KDT_GC_QuyetToan_NguyenPhuLieu NPLDuyet = new KDT_GC_QuyetToan_NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = CheckNPLQT(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    //continue;
                                    string msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    if (BKCungUng.SoBangKe == 0)
                                        msg += "ID=" + BKCungUng.ID;
                                    else
                                    {
                                        msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                    if (dateTKCT < dateFrom)
                                    {
                                        NPLDuyet.LuongCungUngDK += Math.Round(npl.LuongCung, SoThapPhanNPL);
                                        NPLDuyet.TriGiaCungUngDK += Math.Round(Convert.ToDecimal(npl.TriGia), SoThapPhanNPL);
                                    }
                                    else if (dateTKCT <= dateTo)
                                    {
                                        NPLDuyet.LuongCungUngTK += Math.Round(npl.LuongCung, SoThapPhanNPL);
                                        NPLDuyet.TriGiaCungUngTK += Math.Round(Convert.ToDecimal(npl.TriGia), SoThapPhanNPL);
                                    }
                                    else
                                    {
                                        NPLDuyet.LuongCungUngDK += Math.Round(npl.LuongCung, SoThapPhanNPL);
                                        NPLDuyet.TriGiaCungUngDK += Math.Round(Convert.ToDecimal(npl.TriGia), SoThapPhanNPL);
                                    }
                            }
                        }
                    }
                }

                #endregion xu ly bang ke cung ung cho to khai chuyen tiep nay
            }
            #endregion Xu ly To khai chuyen tiep

            #region xu ly to khai mau dich
            this.OnProcess(new HopDongEventArgs("Xử lý Tờ khai mậu dịch", 12));
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.IDHopDong = this.ID;
            //ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionBy_IDHopDong_XNT();
            ToKhaiMauDichCollection TKMDCollection = TKMD.SelectCollectionBy_IDHopDong();
            int indexTKMD = 0;
            foreach (ToKhaiMauDich tkmd in TKMDCollection)
            {
                indexTKMD++;
                this.OnProcess(new HopDongEventArgs("Xử lý Tờ khai mậu dịch", (indexTKMD * 80 / TKMDCollection.Count) + 12));
                //TODO: Hungtq updated 03/04/2013. Loai tru to khai co trang thai: Cho huy (11), Da huy (10)
                if (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                    || (tkmd.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && tkmd.MaLoaiHinh.Substring(0, 1) == "N")
                    )
                {
                    continue;
                }
                int dayTKMD = tkmd.NgayDangKy.Day;
                int monthTKMD = tkmd.NgayDangKy.Month;
                int yearTKMD = tkmd.NgayDangKy.Year;
                DateTime dateTKMD = new DateTime(yearTKMD, monthTKMD, dayTKMD);
                //;// Logger.LocalLogger.Instance().WriteMessage(new Exception("Tờ khai " + tkmd.SoToKhai + "/" + tkmd.MaLoaiHinh));
                #region xu ly to khai truoc
                tkmd.LoadHMDCollection();

                string LoaiHangHoa = tkmd.LoaiHangHoa;
                foreach (HangMauDich HMD in tkmd.HMDCollection)
                {
                    if (tkmd.SoToKhai == 559)
                    {

                    }
                    // Logger.LocalLogger.Instance().WriteMessage(new Exception("Tờ khai " + tkmd.SoToKhai + "/" + tkmd.MaLoaiHinh + "/MaNPL = " + HMD.MaPhu) );
                    if (tkmd.MaLoaiHinh.StartsWith("N"))
                    {
                        if (LoaiHangHoa == "N")
                        {
                            #region Nhap NPL
                            KDT_GC_QuyetToan_NguyenPhuLieu npl = new KDT_GC_QuyetToan_NguyenPhuLieu();
                            npl = CheckNPLQT(HMD.MaPhu, NPLCollectionData);
                            if (npl == null)
                            {
                                #region xử lý hàng mẫu trong tờ khai nhập về dạng NPL
                                KDT_GC_QuyetToan_HangMau hm = new KDT_GC_QuyetToan_HangMau();
                                hm = CheckHMQT(HMD.MaPhu, HMCollectionData);
                                if (hm == null)
                                {
                                    string msg = "Mã nguyên phụ liệu/hàng mẫu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                //else
                                //    hm.SoLuongDaNhap += HMD.SoLuong;
                                if (dateTKMD < dateFrom)
                                {
                                    hm.LuongNhapDK += HMD.SoLuong;
                                    hm.TriGiaNhapDK += HMD.TriGiaTT;
                                }
                                else if (dateTKMD <= dateTo)
                                {
                                    hm.LuongNhapTK += HMD.SoLuong;
                                    hm.TriGiaNhapTK += HMD.TriGiaTT;
                                }
                                else
                                {
                                    hm.LuongNhapNK += HMD.SoLuong;
                                    hm.TriGiaNhapNK += HMD.TriGiaTT;
                                }

                                #endregion xử lý hàng mẫu trong tờ khai nhập về dạng NPL
                                //lỗi

                            }
                            if (npl != null)
                            {
                                if (dateTKMD < dateFrom)
                                {
                                    npl.LuongNhapDK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaNhapDK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                    //npl.InsertUpdate();
                                }
                                else if (dateTKMD <= dateTo)
                                {
                                    npl.LuongNhapTK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaNhapTK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                    //npl.InsertUpdate();
                                }
                                else
                                {
                                    npl.LuongNhapNK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaNhapNK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                    //npl.InsertUpdate();
                                }

                            }

                            #endregion Nhap NPL


                        }
                        else if (LoaiHangHoa == "S")
                        {
                            if (isXuLySP)
                            {
                                //minhnd: fix nhap SP
                                #region Nhap SP
                                KDT_GC_QuyetToan_SanPham sp = new KDT_GC_QuyetToan_SanPham();
                                sp = CheckSPQT(HMD.MaPhu, SPCollectionData);
                                if (sp == null)
                                {
                                    //continue;
                                    string msg = "Mã Sản Phảm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                //sp.SoLuongConLai += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                if (dateTKMD < dateFrom)
                                {
                                    sp.LuongXuatDK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    sp.TriGiaXuatDK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                }
                                else if (dateTKMD <= dateTo)
                                {
                                    sp.LuongXuatTK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    sp.TriGiaXuatTK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                }
                                else
                                {
                                    sp.LuongXuatNK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    sp.TriGiaXuatNK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                }                                

                                #endregion Nhap SP
                                //minhnd: fix nhap SP
                            }

                        }
                        else if (LoaiHangHoa == "T")
                        {
                            #region Nhap Thiet BI
                            KDT_GC_QuyetToan_ThietBi tb = new KDT_GC_QuyetToan_ThietBi();
                            tb = CheckTBQT(HMD.MaPhu, TBCollectionData);
                            if (tb == null)
                            {
                                //tb = new Company.GC.BLL.GC.ThietBi();
                                //tb.Ma = HMD.MaPhu;
                                //tb.Ten = HMD.Ten;
                                //tb.DVT_ID = HMD.DVT_ID;
                                //tb.LuongSuDungDK = HMD.SoLuong;
                                //tb.SoLuongConLai = HMD.SoLuong;
                                //tb.Insert();
                                if (dateTKMD < dateFrom)
                                {
                                    tb = new KDT_GC_QuyetToan_ThietBi();
                                    tb.Ma = HMD.MaPhu;
                                    tb.Ten = HMD.Ten;
                                    tb.DVT_ID = HMD.DVT_ID;
                                    tb.LuongSuDungDK = HMD.SoLuong;
                                    tb.TriGiaSuDungDK = HMD.TriGiaTT;
                                    tb.Insert();
                                }
                                else if (dateTKMD <= dateTo)
                                {
                                    tb = new KDT_GC_QuyetToan_ThietBi();
                                    tb.Ma = HMD.MaPhu;
                                    tb.Ten = HMD.Ten;
                                    tb.DVT_ID = HMD.DVT_ID;
                                    tb.LuongSuDungTK = HMD.SoLuong;
                                    tb.TriGiaSuDungTK = HMD.TriGiaTT; 
                                    tb.Insert();
                                }
                                else
                                {
                                    tb = new KDT_GC_QuyetToan_ThietBi();
                                    tb.Ma = HMD.MaPhu;
                                    tb.Ten = HMD.Ten;
                                    tb.DVT_ID = HMD.DVT_ID;
                                    tb.LuongSuDungNK = HMD.SoLuong;
                                    tb.TriGiaSuDungNK = HMD.TriGiaTT; 
                                    tb.Insert();
                                }

                                //continue;
                                //string msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                //if (tkmd.SoToKhai == 0)
                                //    msg += "ID=" + tkmd.ID;
                                //else
                                //{
                                //    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                //}
                                //msg += " chưa được đăng ký.";
                                //throw new Exception(msg);
                            }
                            else
                                //tb.SoLuongDaNhap += HMD.SoLuong;
                            if (dateTKMD < dateFrom)
                            {
                                tb.LuongNhapDK += HMD.SoLuong;
                            }
                            else if (dateTKMD <= dateTo)
                            {
                                tb.LuongNhapTK += HMD.SoLuong;
                            }
                            else
                            {
                                tb.LuongNhapNK += HMD.SoLuong;
                            }                

                            #endregion Nhap Thiet BI
                        }
                        else
                        {
                            #region Nhap Hang Mau
                            KDT_GC_QuyetToan_HangMau hm = new KDT_GC_QuyetToan_HangMau();
                            hm = CheckHMQT(HMD.MaPhu, HMCollectionData);
                            if (hm == null)
                            {
                                //hm = new Company.GC.BLL.GC.HangMau();
                                //hm.Ma = HMD.MaPhu;
                                //hm.Ten = HMD.Ten;
                                //hm.DVT_ID = HMD.DVT_ID;
                                //hm.SoLuongDaNhap = HMD.SoLuong;
                                ////hm.s = HMD.SoLuong;
                                //hm.Insert();
                                ////continue;
                                ////string msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                ////if (tkmd.SoToKhai == 0)
                                ////    msg += "ID=" + tkmd.ID;
                                ////else
                                ////{
                                ////    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                ////}
                                ////msg += " chưa được đăng ký.";
                                ////throw new Exception(msg);
                                #region Nhap NPL
                                KDT_GC_QuyetToan_NguyenPhuLieu npl = new KDT_GC_QuyetToan_NguyenPhuLieu();
                                npl = CheckNPLQT(HMD.MaPhu, NPLCollectionData);
                                if (npl == null)
                                {
                                    #region xử lý hàng mẫu trong tờ khai nhập về dạng NPL
                                    //KDT_GC_QuyetToan_HangMau hm = new KDT_GC_QuyetToan_HangMau();
                                    hm = CheckHMQT(HMD.MaPhu, HMCollectionData);
                                    if (hm == null)
                                    {
                                        //hm = new Company.GC.BLL.GC.HangMau();
                                        //hm.Ma = HMD.MaPhu;
                                        //hm.Ten = HMD.Ten;
                                        //hm.DVT_ID = HMD.DVT_ID;
                                        //hm.SoLuongDaNhap = HMD.SoLuong;
                                        //hm.Insert();
                                    string msg = "Mã nguyên phụ liệu/hàng mẫu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                    }
                                }
                                //npl.SoLuongDaNhap += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                //npl.InsertUpdate();
                                if (dateTKMD < dateFrom)
                                {
                                    npl.LuongNhapDK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaNhapDK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                    npl.InsertUpdate();
                                }
                                else if (dateTKMD <= dateTo)
                                {
                                    npl.LuongNhapTK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaNhapTK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                    npl.InsertUpdate();
                                }
                                else
                                {
                                    npl.LuongNhapNK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaNhapNK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                    npl.InsertUpdate();
                                }

                                #endregion Nhap NPL
                            }
                            else
                             //   hm.SoLuongDaNhap += HMD.SoLuong;
                            if (dateTKMD < dateFrom)
                            {
                                hm.LuongNhapDK += HMD.SoLuong;
                                hm.TriGiaNhapDK += HMD.TriGiaTT;
                            }
                            else if (dateTKMD <= dateTo)
                            {
                                hm.LuongNhapTK += HMD.SoLuong;
                                hm.TriGiaNhapTK += HMD.TriGiaTT;
                            }
                            else
                            {
                                hm.LuongNhapNK += HMD.SoLuong;
                                hm.TriGiaNhapNK += HMD.TriGiaTT;
                            }

                            #endregion Nhap Hang Mau
                        }
                    }
                    //else if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                    //{
                    //    #region Nhap NPL ben loai hinh sxxk
                    //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                    //    if (npl == null)
                    //    {
                    //        
                    //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                    //        if (tkmd.SoToKhai == 0)
                    //            msg += "ID=" + tkmd.ID;
                    //        else
                    //        {
                    //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                    //        }
                    //        msg += " chưa được đăng ký.";
                    //        throw new Exception(msg);
                    //    }
                    //    npl.SoLuongCungUng += Math.Round(HMD.SoLuong,SoThapPhanNPL);
                    //    #endregion Nhap NPL
                    //}
                    else
                    {
                        //to khai xuat ben loai hinh gc
                        if (tkmd.MaLoaiHinh.StartsWith("X"))
                        {
                            if (LoaiHangHoa == "N")
                            {
                                #region Xuat NPL
                                KDT_GC_QuyetToan_NguyenPhuLieu npl = new KDT_GC_QuyetToan_NguyenPhuLieu();
                                npl = CheckNPLQT(HMD.MaPhu, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                //npl.SoLuongDaDung += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                if (dateTKMD < dateFrom)
                                {
                                    npl.LuongSuDungDK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaSuDungDK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                }
                                else if (dateTKMD <= dateTo)
                                {
                                    npl.LuongSuDungTK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaSuDungTK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                }
                                else
                                {
                                    npl.LuongSuDungNK += Math.Round(HMD.SoLuong, SoThapPhanNPL);
                                    npl.TriGiaSuDungNK += Math.Round(HMD.TriGiaTT, SoThapPhanNPL);
                                }

                                #endregion Xuat NPL
                            }
                            else if (LoaiHangHoa == "T")
                            {
                                #region Xuat TB
                                KDT_GC_QuyetToan_ThietBi tb = new KDT_GC_QuyetToan_ThietBi();
                                tb = CheckTBQT(HMD.MaPhu, TBCollectionData);
                                if (tb == null)
                                {
                                    continue;
                                    String msg = "Mã thiết bị : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                    if (tkmd.SoToKhai == 0)
                                        msg += "ID=" + tkmd.ID;
                                    else
                                    {
                                        msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                    }
                                    msg += " chưa được đăng ký.";
                                    throw new Exception(msg);
                                }
                                //tb.SoLuongDaNhap -= HMD.SoLuong;
                                if (dateTKMD < dateFrom)
                                {
                                    tb.LuongSuDungDK  += HMD.SoLuong;
                                    tb.TriGiaSuDungDK += HMD.TriGiaTT;
                                }
                                else if (dateTKMD <= dateTo)
                                {
                                    tb.LuongSuDungTK += HMD.SoLuong;
                                    tb.TriGiaSuDungTK += HMD.TriGiaTT;
                                }
                                else
                                {
                                    tb.LuongSuDungNK += HMD.SoLuong;
                                    tb.TriGiaSuDungNK += HMD.TriGiaTT;
                                }

                                #endregion Xuat Thiet bi
                            }
                            else
                            {
                                //minhnd
                                #region xuất sản phẩm
                                if (isXuLySP)
                                {
                                    KDT_GC_QuyetToan_SanPham sp = new KDT_GC_QuyetToan_SanPham();
                                    sp = CheckSPQT(HMD.MaPhu, SPCollectionData);
                                    if (sp == null)
                                    {
                                        continue;
                                        String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                        if (tkmd.SoToKhai == 0)
                                            msg += "ID=" + tkmd.ID;
                                        else
                                        {
                                            msg += "Số tờ khai mậu dịch  : " + tkmd.SoToKhai.ToString();
                                        }
                                        msg += " chưa được đăng ký.";
                                        throw new Exception(msg);
                                    }
                                    //sp.SoLuongDaXuat += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                    if (dateTKMD < dateFrom)
                                    {
                                        sp.LuongXuatDK += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                        sp.TriGiaXuatDK += Math.Round(HMD.TriGiaTT, SoThapPhanSP);
                                    }
                                    else if (dateTKMD <= dateTo)
                                    {
                                        sp.LuongXuatTK += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                        sp.TriGiaXuatTK += Math.Round(HMD.TriGiaTT, SoThapPhanSP);
                                    }
                                    else
                                    {
                                        sp.LuongXuatNK += Math.Round(HMD.SoLuong, SoThapPhanSP);
                                        sp.TriGiaXuatNK += Math.Round(HMD.TriGiaTT, SoThapPhanSP);
                                    }



                                    DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(HMD.MaPhu.Trim(), HMD.SoLuong, this.ID, SoThapPhanNPL);
                                    //Nếu là tái xuất thì không xử lý định mức
                                    if (tkmd.MaLoaiHinh.Contains("B13"))
                                    {
                                    }
                                    else
                                    {
                                        //if (dsLuongNPL.Tables[0].Rows.Count == 0)
                                        //{
                                            //continue;
                                        //    string msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                        //    if (tkmd.SoToKhai == 0)
                                        //        msg += "ID=" + tkmd.ID;
                                        //    else
                                        //    {
                                        //        msg += "Số tờ khai : " + tkmd.SoToKhai.ToString();
                                        //    }
                                        //    msg += " chưa có định mức.";
                                        //    throw new Exception(msg);
                                        //}
                                        foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                                        {
                                            KDT_GC_QuyetToan_NguyenPhuLieu npl = new KDT_GC_QuyetToan_NguyenPhuLieu();
                                            string maNPL = row["MaNguyenPhuLieu"].ToString();
                                            npl = CheckNPLQT(maNPL, NPLCollectionData);
                                            if (npl == null)
                                            {
                                                continue;
                                                String msg = "Mã sản phẩm : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                                                if (tkmd.SoToKhai == 0)
                                                    msg += "ID=" + tkmd.ID;
                                                else
                                                {
                                                    msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                                                }
                                                msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                                throw new Exception(msg);
                                            }
                                            //npl.SoLuongDaDung += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                            if (dateTKMD < dateFrom)
                                            {
                                                npl.LuongSuDungDK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                                npl.TriGiaSuDungDK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                            }
                                            else if (dateTKMD <= dateTo)
                                            {
                                                npl.LuongSuDungTK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                                npl.TriGiaSuDungTK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                            }
                                            else
                                            {
                                                npl.LuongSuDungNK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                                npl.TriGiaSuDungNK += Math.Round(Convert.ToDecimal(row["LuongCanDung"]), SoThapPhanNPL);
                                            }

                                        }
                                    }

                                }
                                #endregion xuất sản phẩm

                            }
                        }
                        //else
                        //{
                        //    #region Tai Xuat NPL ben loai hinh SXXK
                        //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //    npl = getNPL(HMD.MaPhu, NPLCollectionData);
                        //    if (npl == null)
                        //    {

                        //        
                        //        string msg = "Mã nguyên phụ liệu : " + HMD.MaPhu + " trong tờ khai mậu dịch có ";
                        //        if (tkmd.SoToKhai == 0)
                        //            msg += "ID=" + tkmd.ID;
                        //        else
                        //        {
                        //            msg += "Số tờ khai mậu dịch : " + tkmd.SoToKhai.ToString();
                        //        }
                        //        msg += " chưa được đăng ký.";
                        //        throw new Exception(msg);
                        //    }
                        //    if (npl.Ma == "410799000015")
                        //    {

                        //    }
                        //    npl.SoLuongCungUng -=Math.Round(HMD.SoLuong,SoThapPhanNPL);
                        //    #endregion Xuat NPL
                        //}
                    }

                }
                #endregion

                #region xu ly bang ke cung ung cho to khai

                if (tkmd.MaLoaiHinh.StartsWith("X") || tkmd.MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    BKCungUngDangKy BKCU = new BKCungUngDangKy();
                    BKCU.TKCT_ID = tkmd.ID;
                    BKCungUngDangKyCollection BKCUCollection = BKCU.SelectCollectionDynamic("TKMD_ID=" + tkmd.ID, "");
                    foreach (BKCungUngDangKy BKCungUng in BKCUCollection)
                    {
                        BKCungUng.LoadSanPhamCungUngCollection();
                        foreach (SanPhanCungUng spCungUng in BKCungUng.SanPhamCungUngCollection)
                        {
                            KDT_GC_QuyetToan_SanPham sp = new KDT_GC_QuyetToan_SanPham();
                            sp = CheckSPQT(spCungUng.MaSanPham, SPCollectionData);
                            if (sp == null)
                            {
                                continue;
                                String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                if (BKCungUng.SoBangKe == 0)
                                    msg += "ID=" + BKCungUng.ID;
                                else
                                {
                                    msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                }
                                msg += " chưa được đăng ký.";
                                throw new Exception(msg);
                            }
                            spCungUng.LoadNPLCungUngCollection();
                            foreach (NguyenPhuLieuCungUng npl in spCungUng.NPLCungUngCollection)
                            {
                                KDT_GC_QuyetToan_NguyenPhuLieu NPLDuyet = new KDT_GC_QuyetToan_NguyenPhuLieu();
                                string maNPL = npl.MaNguyenPhuLieu;
                                NPLDuyet = CheckNPLQT(maNPL, NPLCollectionData);
                                if (npl == null)
                                {
                                    continue;
                                    String msg = "Mã sản phẩm : " + spCungUng.MaSanPham + " trong bảng kê cung ứng có ";
                                    if (BKCungUng.SoBangKe == 0)
                                        msg += "ID=" + BKCungUng.ID;
                                    else
                                    {
                                        msg += "Số bảng kê : " + BKCungUng.SoBangKe.ToString();
                                    }
                                    msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                                    throw new Exception(msg);
                                }
                                //NPLDuyet.SoLuongCungUng += Math.Round(npl.LuongCung, SoThapPhanNPL);
                                if (dateTKMD < dateFrom)
                                {
                                    NPLDuyet.LuongCungUngDK += Math.Round(npl.LuongCung, SoThapPhanNPL);
                                    NPLDuyet.TriGiaCungUngDK += Math.Round(Convert.ToDecimal(npl.TriGia), SoThapPhanNPL);
                                }
                                else if (dateTKMD <= dateTo)
                                {
                                    NPLDuyet.LuongCungUngTK += Math.Round(npl.LuongCung, SoThapPhanNPL);
                                    NPLDuyet.TriGiaCungUngTK += Math.Round(Convert.ToDecimal(npl.TriGia), SoThapPhanNPL);
                                }
                                else
                                {
                                    NPLDuyet.LuongCungUngNK += Math.Round(npl.LuongCung, SoThapPhanNPL);
                                    NPLDuyet.TriGiaCungUngNK += Math.Round(Convert.ToDecimal(npl.TriGia), SoThapPhanNPL);
                                }
                            }
                        }
                    }
                }

                #endregion
            }
            #endregion xu ly to khai mau dich

            #region Xu ly phu kien mua vn
            this.OnProcess(new HopDongEventArgs("Xử lý cung ứng", 93));
            // DataSet dsMuaVN = KDT_GC_CungUng.GetDSNPLCungUngByHopDong(this.ID);
            if (dsMuaVN != null && dsMuaVN.Rows.Count > 0)
            {
                foreach (KDT_GC_QuyetToan_NguyenPhuLieu item in NPLCollectionData)
                {

                    foreach (DataRow row in dsMuaVN.Rows)
                    {
                        if (row["MaNPL"].ToString().Trim().ToUpper() == item.MaNPL.Trim().ToUpper())
                        {
                            item.LuongCungUngTK += Convert.ToDecimal(row["LuongCungUng"]);
                            item.TriGiaCungUngTK += Convert.ToDecimal(row["TriGia"]);
                            break;
                        }
                    }
                }
            }


            #endregion Xu ly phu kien mua vn
            this.OnProcess(new HopDongEventArgs("Lưu dữ liệu", 95));
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (KDT_GC_QuyetToan_NguyenPhuLieu NPLDataItem in NPLCollectionData)
                    {
                        NPLDataItem.InsertUpdate();
                    }
                    //minhnd xử lý cho những hợp đồng không xuất.
                    if (isXuLySP)
                    {
                        foreach (KDT_GC_QuyetToan_SanPham SPDataItem in SPCollectionData)
                        {
                            SPDataItem.InsertUpdate();
                        }
                    }

                    foreach (KDT_GC_QuyetToan_ThietBi TBDataItem in TBCollectionData)
                    {
                        TBDataItem.InsertUpdate();
                    }
                    foreach (KDT_GC_QuyetToan_HangMau HMDataItem in HMCollectionData)
                    {
                        HMDataItem.InsertUpdate();
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw ex;
                    this.OnProcess(new HopDongEventArgs(ex, "Lỗi :", 0));
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void DongBoToKhaiNhapSXXK(DateTime fromDate, DateTime toDate)
        {
            Database db = DatabaseFactory.CreateDatabase();
            Database dbSXXK = DatabaseFactory.CreateDatabase("SXXK");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                //connection.Open();
                //SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "SELECT * FROM t_SXXK_ToKhaiMauDich WHERE NgayDangKy BETWEEN @FromDate AND @ToDate AND ( MaLoaiHinh = 'NSX01' OR MaLoaiHinh = 'NSX06') ";
                    SqlCommand dbCommand = (SqlCommand)dbSXXK.GetSqlStringCommand(sql);
                    dbSXXK.AddInParameter(dbCommand, "@FromDate", DbType.DateTime, fromDate);
                    dbSXXK.AddInParameter(dbCommand, "@ToDate", DbType.DateTime, toDate);
                    DataSet dsSXXK = dbSXXK.ExecuteDataSet(dbCommand);
                    foreach (DataRow drSXXK in dsSXXK.Tables[0].Rows)
                    {
                        ToKhaiMauDich TKMD = new ToKhaiMauDich();
                        if (!TKMD.CheckExistToKhaiMauDich(drSXXK["MaHaiQuan"].ToString(), drSXXK["MaLoaiHinh"].ToString(), Convert.ToInt32(drSXXK["SoToKhai"]), Convert.ToDateTime(drSXXK["NgayDangKy"])))
                        {
                            TKMD.CuaKhau_ID = drSXXK["CuaKhau_ID"].ToString();
                            TKMD.DiaDiemXepHang = drSXXK["DiaDiemXepHang"].ToString();
                            TKMD.DKGH_ID = drSXXK["DKGH_ID"].ToString();
                            //TKMD.GiayTo = drSXXK["GiayTo"].ToString();
                            TKMD.SoVanDon = drSXXK["SoVanDon"].ToString();
                            TKMD.MaDoanhNghiep = this.MaDoanhNghiep;
                            TKMD.MaHaiQuan = this.MaHaiQuan;
                            TKMD.MaLoaiHinh = drSXXK["MaLoaiHinh"].ToString();
                            TKMD.NgayDangKy = Convert.ToDateTime(drSXXK["NgayDangKy"].ToString());
                            if (drSXXK["NgayDenPTVT"].ToString() != "")
                                TKMD.NgayDenPTVT = Convert.ToDateTime(drSXXK["NgayDenPTVT"]);
                            if (drSXXK["NgayGiayPhep"].ToString() != "")
                                TKMD.NgayGiayPhep = Convert.ToDateTime(drSXXK["NgayGiayPhep"]);
                            if (drSXXK["NgayHetHanGiayPhep"].ToString() != "")
                                TKMD.NgayHetHanGiayPhep = Convert.ToDateTime(drSXXK["NgayHetHanGiayPhep"]);
                            if (drSXXK["NgayHetHanHopDong"].ToString() != "")
                                TKMD.NgayHetHanHopDong = Convert.ToDateTime(drSXXK["NgayHetHanHopDong"]);
                            if (drSXXK["NgayHoaDonThuongMai"].ToString() != "")
                                TKMD.NgayHoaDonThuongMai = Convert.ToDateTime(drSXXK["NgayHoaDonThuongMai"]);
                            if (drSXXK["NgayHopDong"].ToString() != "")
                                TKMD.NgayHopDong = Convert.ToDateTime(drSXXK["NgayHopDong"]);
                            //TKMD.NgayTiepNhan = Convert.ToDateTime(drSXXK["NgayTiepNhan"]);
                            if (drSXXK["NgayVanDon"].ToString() != "")
                                TKMD.NgayVanDon = Convert.ToDateTime(drSXXK["NgayVanDon"]);
                            TKMD.NguyenTe_ID = drSXXK["NguyenTe_ID"].ToString();
                            TKMD.NuocNK_ID = drSXXK["NuocNK_ID"].ToString();
                            TKMD.NuocXK_ID = drSXXK["NuocXK_ID"].ToString();

                            if (drSXXK["PhiBaoHiem"].ToString() != "")
                                TKMD.PhiBaoHiem = Convert.ToDecimal(drSXXK["PhiBaoHiem"]);
                            if (drSXXK["PhiVanChuyen"].ToString() != "")
                                TKMD.PhiVanChuyen = Convert.ToDecimal(drSXXK["PhiVanChuyen"]);
                            TKMD.PTTT_ID = drSXXK["PTTT_ID"].ToString();
                            TKMD.PTVT_ID = drSXXK["PTVT_ID"].ToString();
                            if (drSXXK["SoContainer20"].ToString() != "")
                                TKMD.SoContainer20 = Convert.ToDecimal(drSXXK["SoContainer20"]);
                            if (drSXXK["SoContainer40"].ToString() != "")
                                TKMD.SoContainer40 = Convert.ToDecimal(drSXXK["SoContainer40"]);
                            TKMD.SoGiayPhep = drSXXK["SoGiayPhep"].ToString();
                            TKMD.SoHieuPTVT = drSXXK["SoHieuPTVT"].ToString();
                            TKMD.SoHoaDonThuongMai = drSXXK["SoHoaDonThuongMai"].ToString();
                            TKMD.SoHopDong = drSXXK["SoHopDong"].ToString();
                            if (drSXXK["SoKien"].ToString() != "")
                                TKMD.SoKien = Convert.ToDecimal(drSXXK["SoKien"]);
                            if (drSXXK["SoLuongPLTK"].ToString() != "")
                                TKMD.SoLuongPLTK = Convert.ToInt16(drSXXK["SoLuongPLTK"]);
                            //TKMD.SoTiepNhan = Convert.ToInt64(drSXXK["SoTiepNhan"]);
                            TKMD.TenChuHang = drSXXK["TenChuHang"].ToString();
                            TKMD.SoToKhai = Convert.ToInt32(drSXXK["SoToKhai"]);
                            TKMD.TrangThaiXuLy = 1;
                            if (drSXXK["TrongLuong"].ToString() != "")
                                TKMD.TrongLuong = Convert.ToDecimal(drSXXK["TrongLuong"]);
                            if (drSXXK["TyGiaTinhThue"].ToString() != "")
                                TKMD.TyGiaTinhThue = Convert.ToDecimal(drSXXK["TyGiaTinhThue"]);
                            if (drSXXK["TyGiaUSD"].ToString() != "")
                                TKMD.TyGiaUSD = Convert.ToDecimal(drSXXK["TyGiaUSD"]);
                            TKMD.TenDonViDoiTac = drSXXK["TenDonViDoiTac"].ToString();
                            TKMD.LoaiHangHoa = "N";

                            TKMD.IDHopDong = this.ID;
                            sql = "SELECT * FROM t_SXXK_HangMauDich WHERE SoToKhai = " + drSXXK["SoToKhai"] + " AND MaLoaiHinh = '" + drSXXK["MaLoaiHinh"] + "'" +
                                  " AND NamDangKy = " + drSXXK["NamDangKy"] + " AND MaHaiQuan = '" + drSXXK["MaHaiQuan"] + "'";
                            dbCommand = (SqlCommand)dbSXXK.GetSqlStringCommand(sql);
                            DataTable dtHangSXXK = dbSXXK.ExecuteDataSet(dbCommand).Tables[0];
                            TKMD.HMDCollection = new List<HangMauDich>();
                            foreach (DataRow row in dtHangSXXK.Rows)
                            {
                                HangMauDich hang = new HangMauDich();
                                if (row["DonGiaKB"].ToString() != "")
                                    hang.DonGiaKB = Convert.ToDecimal(row["DonGiaKB"].ToString());
                                if (row["DonGiaTT"].ToString() != "")
                                    hang.DonGiaTT = Convert.ToDecimal(row["DonGiaTT"].ToString());
                                hang.DVT_ID = row["DVT_ID"].ToString();
                                hang.MaHS = row["MaHS"].ToString().Trim();
                                hang.MaPhu = row["MaPhu"].ToString().Trim();
                                if (row["MienThue"].ToString() != "")
                                    hang.MienThue = Convert.ToByte(row["MienThue"].ToString());
                                hang.NuocXX_ID = row["NuocXX_ID"].ToString();
                                if (row["PhuThu"].ToString() != "")
                                    hang.PhuThu = Convert.ToDecimal(row["PhuThu"].ToString());
                                if (row["SoLuong"].ToString() != "")
                                    hang.SoLuong = Convert.ToDecimal(row["SoLuong"].ToString());
                                hang.TenHang = row["TenHang"].ToString();
                                if (row["ThueGTGT"].ToString() != "")
                                    hang.ThueGTGT = Convert.ToDecimal(row["ThueGTGT"].ToString());
                                if (row["ThueSuatGTGT"].ToString() != "")
                                    hang.ThueSuatGTGT = Convert.ToDecimal(row["ThueSuatGTGT"].ToString());
                                if (row["ThueSuatTTDB"].ToString() != "")
                                    hang.ThueSuatTTDB = Convert.ToDecimal(row["ThueSuatTTDB"].ToString());
                                if (row["ThueSuatXNK"].ToString() != "")
                                    hang.ThueSuatXNK = Convert.ToDecimal(row["ThueSuatXNK"].ToString());
                                if (row["ThueTTDB"].ToString() != "")
                                    hang.ThueTTDB = Convert.ToDecimal(row["ThueTTDB"].ToString());
                                if (row["ThueXNK"].ToString() != "")
                                    hang.ThueXNK = Convert.ToDecimal(row["ThueXNK"].ToString());
                                if (row["TriGiaKB"].ToString() != "")
                                    hang.TriGiaKB = Convert.ToDecimal(row["TriGiaKB"].ToString());
                                if (row["TriGiaKB_VND"].ToString() != "")
                                    hang.TriGiaKB_VND = Convert.ToDecimal(row["TriGiaKB_VND"].ToString());
                                if (row["TriGiaThuKhac"].ToString() != "")
                                    hang.TriGiaThuKhac = Convert.ToDecimal(row["TriGiaThuKhac"].ToString());
                                if (row["TriGiaTT"].ToString() != "")
                                    hang.TriGiaTT = Convert.ToDecimal(row["TriGiaTT"].ToString());
                                if (row["TyLeThuKhac"].ToString() != "")
                                    hang.TyLeThuKhac = Convert.ToDecimal(row["TyLeThuKhac"].ToString());
                                TKMD.HMDCollection.Add(hang);
                                NPLNhapTonThucTe npl = new NPLNhapTonThucTe();
                                npl.ID_HopDong = this.ID;
                                npl.Luong = hang.SoLuong;
                                npl.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                                npl.MaHaiQuan = TKMD.MaHaiQuan;
                                npl.MaLoaiHinh = TKMD.MaLoaiHinh;
                                npl.MaNPL = hang.MaPhu;
                                npl.NamDangKy = Convert.ToInt16(TKMD.NgayDangKy.Year);
                                npl.NgayDangKy = TKMD.NgayDangKy;
                                npl.SoToKhai = TKMD.SoToKhai;
                                npl.Ton = hang.SoLuong;
                                npl.Insert();
                            }
                            TKMD.InsertUpdateFull();

                        }
                    }
                }
                catch (Exception ex)
                {
                    //transaction.Rollback();
                    throw ex;
                }
                //finally
                //{
                //    connection.Close();
                //}
            }
        }

        #region Report

        public DataSet GetNPLMauDichVaChuyenTiepHopDong()
        {
            string query = "Select * from t_View_KDT_GC_NPL where IDHopDong = " + this.ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(query);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }




        public static string GetSoluongSanPhamHD(object idHD)
        {
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT count(*) as [dem] FROM t_GC_SanPham WHERE HopDong_ID = '{0}'", idHD.ToString().PadRight(3));
            System.Data.Common.DbCommand dbCommand = db1.GetSqlStringCommand(query);
            IDataReader reader = db1.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                string dem = reader["dem"].ToString();
                reader.Close();
                return dem;
            }
            reader.Close();
            return idHD.ToString();
        }
        //public static string GetNameSPHD(object idHD)
        //{
        //    Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    string query = string.Format("SELECT * FROM t_KDT_GC_HopDong WHERE ID = '{0}' ORDER BY DonViDoiTac", idHD.ToString().PadRight(3));
        //    System.Data.Common.DbCommand dbCommand = db1.GetSqlStringCommand(query);
        //    IDataReader reader = db1.ExecuteReader(dbCommand);
        //    if (reader.Read())
        //    {
        //        string s = reader["DonViDoiTac"].ToString();
        //        reader.Close();
        //        return s;
        //    }
        //    reader.Close();
        //    return idHD.ToString();
        //}

        public static string GetNameRent(object idHD)
        {
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT * FROM t_KDT_GC_HopDong WHERE ID = '{0}' ORDER BY DonViDoiTac", idHD.ToString().PadRight(3));
            System.Data.Common.DbCommand dbCommand = db1.GetSqlStringCommand(query);
            IDataReader reader = db1.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                string s = reader["DonViDoiTac"].ToString();
                reader.Close();
                return s;
            }
            reader.Close();
            return idHD.ToString();
        }
        public static string GetNameHDRent(object idHD)
        {
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT * FROM t_KDT_GC_HopDong WHERE ID = '{0}' ORDER BY DonViDoiTac", idHD.ToString().PadRight(3));
            System.Data.Common.DbCommand dbCommand = db1.GetSqlStringCommand(query);
            IDataReader reader = db1.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                string s = reader["SoHopDong"].ToString();
                reader.Close();
                return s;
            }
            reader.Close();
            return idHD.ToString();
        }
        public static string GetAddresRent(object idHD)
        {
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT * FROM t_KDT_GC_HopDong WHERE ID = '{0}' ORDER BY DonViDoiTac", idHD.ToString().PadRight(3));
            System.Data.Common.DbCommand dbCommand = db1.GetSqlStringCommand(query);
            IDataReader reader = db1.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                string s = reader["DiaChiDoiTac"].ToString();
                reader.Close();
                return s;
            }
            reader.Close();
            return idHD.ToString();
        }
        public static string GetSignHDDate(object idHD)
        {
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT NgayKy FROM t_KDT_GC_HopDong WHERE ID = '{0}' ORDER BY DonViDoiTac", idHD.ToString().PadRight(3));
            System.Data.Common.DbCommand dbCommand = db1.GetSqlStringCommand(query);
            IDataReader reader = db1.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                DateTime date = Convert.ToDateTime(reader["NgayKy"].ToString());
                string s = date.Date.ToShortDateString().ToString();
                reader.Close();
                return s;
            }
            reader.Close();
            return idHD.ToString();
        }
        public static string GetEndHDDate(object idHD)
        {
            Database db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT NgayHetHan FROM t_KDT_GC_HopDong WHERE ID = '{0}' ORDER BY DonViDoiTac", idHD.ToString().PadRight(3));
            System.Data.Common.DbCommand dbCommand = db1.GetSqlStringCommand(query);
            IDataReader reader = db1.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                DateTime date = Convert.ToDateTime(reader["NgayHetHan"].ToString());
                string s = date.Date.ToShortDateString().ToString();
                reader.Close();
                return s;
            }
            reader.Close();
            return idHD.ToString();
        }

        //public static string GetID(object id)
        //{
        //    string query = string.Format("SELECT * FROM t_KDT_GC_HopDong  WHERE Ten = '{0}' ORDER BY Ten", id.ToString());
        //    DbCommand dbCommand = db.GetSqlStringCommand(query);
        //    IDataReader reader = db.ExecuteReader(dbCommand);
        //    if (reader.Read())
        //    {
        //        string s = reader["ID"].ToString();
        //        reader.Close();
        //        return s;
        //    }
        //    reader.Close();
        //    return id.ToString();
        //}
        #endregion

        #region WS
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            /* LanNT không cho phép thay đổi GUIDSTR
            //DATLMQ bổ sung kiểm tra GUIDSTR Hợp đồng 10/01/2011
            if (this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                nodeReference.InnerText = this.GUIDSTR.ToUpper();
            else if (this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                this.GUIDSTR = System.Guid.NewGuid().ToString().ToUpper();
                nodeReference.InnerText = this.GUIDSTR;
            }
            */
            nodeReference.InnerText = this.GUIDSTR;
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep.Trim();
            this.Update();
            return doc.InnerXml;
        }
        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;
            //nodeReference.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            return doc.InnerXml;
        }
        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            doc.GetElementsByTagName("function")[0].InnerText = MessageFunctions.LayPhanHoi.ToString();
            XmlNode Result = null;
            string kq = "";
            int i = 0;

            for (i = 1; i <= 1; ++i)
            {
                string msgError = string.Empty;
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        msgError = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty)
                    {
                        kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                        throw ex;
                    }
                    else
                    {
                        Logger.LocalLogger.Instance().WriteMessage("Send", new Exception(doc.InnerXml));
                        if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage("Request", new Exception(kq));
                    }
                    throw new Exception("Lỗi do hệ thống của hải quan " + (msgError != string.Empty ? msgError : "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL"));
                }
            }

            if (i > 1)
                return doc.InnerXml;
            try
            {
                if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                }
                bool ok = false;
                if (function == (int)MessageFunctions.KhaiBao)
                {
                    this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                    this.NgayTiepNhan = DateTime.Today;
                    this.TrangThaiXuLy = 0;
                    ok = true;
                    kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQCapSoTiepNhan);

                }
                else if (function == (int)MessageFunctions.HuyKhaiBao)
                {
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.TrangThaiXuLy = -1;
                    ok = true;
                    kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQHuyHopDong);

                }
                else if (function == (int)MessageFunctions.HoiTrangThai)
                {
                    //if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/TrangThai").InnerText == "yes")
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {

                        XmlNode nodephanluong = docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG");
                        if (nodephanluong != null)
                        {
                            this.PhanLuong = docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG").Attributes["MALUONG"].Value.Trim();
                            this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG").Attributes["HUONGDAN"].Value);
                            this.ActionStatus = 1;
                            this.TrangThaiXuLy = 1;
                            this.Update();
                        }

                        kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQPhanLuong, this.HUONGDAN);

                        ok = false;
                        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        if (nodeMess != null)
                        {
                            return nodeMess.OuterXml;
                        }
                    }
                    else
                    {

                        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        if (nodeMess != null)
                        {
                            return nodeMess.OuterXml;
                        }
                    }

                }
                if (ok)
                    this.Update();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            return "";

        }



        private void TranferGC()
        {
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            //using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            //{
            //    connection.Open();
            //    SqlTransaction transaction = connection.BeginTransaction();
            //    try
            //    {
            //        this.UpdateTransaction(transaction);
            //        Company.GC.BLL.GC.HopDong hdDuyet = new Company.GC.BLL.GC.HopDong();
            //        hdDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
            //        hdDuyet.MaHaiQuan = this.MaHaiQuan;
            //        hdDuyet.NgayGiaHan = this.NgayGiaHan;
            //        hdDuyet.NgayHetHan = this.NgayHetHan;
            //        hdDuyet.NgayKy = this.NgayKy;
            //        hdDuyet.NguyenTe_ID = this.NguyenTe_ID;
            //        hdDuyet.NuocThue_ID = this.NuocThue_ID;
            //        hdDuyet.SoHopDong = this.SoHopDong;
            //        hdDuyet.TrangThaiXuLy = 1;
            //        hdDuyet.CanBoDuyet = this.CanBoDuyet;
            //        hdDuyet.CanBoTheoDoi = this.CanBoTheoDoi;
            //        hdDuyet.DonViDoiTac = this.DonViDoiTac;
            //        hdDuyet.DiaChiDoiTac = this.DiaChiDoiTac;
            //        hdDuyet.NgayDangKy = this.NgayDangKy;
            //        hdDuyet.InsertUpdateTransaction(transaction);
            //        List<NhomSanPham> nhomspCollection = new List<NhomSanPham>();
            //        foreach (NhomSanPham entity in this.nhomSPCollection)
            //        {
            //            Company.GC.BLL.GC.NhomSanPham nhomspDuyet = new Company.GC.BLL.GC.NhomSanPham();
            //            nhomspDuyet.Ma = entity.MaSanPham;
            //            nhomspDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
            //            nhomspDuyet.MaHaiQuan = this.MaHaiQuan;
            //            nhomspDuyet.NgayKy = this.NgayKy;
            //            nhomspDuyet.SoHopDong = this.SoHopDong;
            //            nhomspDuyet.SoLuongDangKy = entity.SoLuong;
            //            nhomspDuyet.Ten = entity.TenSanPham;
            //            nhomspDuyet.TriGia = entity.GiaGiaCong;
            //            nhomspDuyet.InsertUpdateTransaction(transaction);
            //        }
            //        foreach (NguyenPhuLieu entity in this.NPLCollection)
            //        {
            //            Company.GC.BLL.GC.NguyenPhuLieu nplDuyet = new Company.GC.BLL.GC.NguyenPhuLieu();
            //            nplDuyet.DVT_ID = entity.DVT_ID;
            //            nplDuyet.Ma = entity.Ma;
            //            nplDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
            //            nplDuyet.MaHaiQuan = this.MaHaiQuan;
            //            nplDuyet.MaHS = entity.MaHS;
            //            nplDuyet.NgayKy = this.NgayKy;
            //            nplDuyet.SoHopDong = this.SoHopDong;
            //            nplDuyet.Ten = entity.Ten;
            //            nplDuyet.SoLuongDangKy = entity.SoLuongDangKy;
            //            nplDuyet.SoLuongNhap = 0;
            //            nplDuyet.SoLuongDaDung = 0;
            //            nplDuyet.SoLuongCungUng = 0;
            //            nplDuyet.InsertUpdateTransaction(transaction);
            //        }
            //        foreach (ThietBi entity in this.TBCollection)
            //        {
            //            Company.GC.BLL.GC.ThietBi tbDuyet = new Company.GC.BLL.GC.ThietBi();
            //            tbDuyet.DonGia = entity.DonGia;
            //            tbDuyet.DVT_ID = entity.DVT_ID;
            //            tbDuyet.Ma = entity.Ma;
            //            tbDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
            //            tbDuyet.MaHaiQuan = this.MaHaiQuan;
            //            tbDuyet.MaHS = entity.MaHS;
            //            tbDuyet.NgayKy = this.NgayKy;
            //            tbDuyet.NguyenTe_ID = entity.NguyenTe_ID;
            //            tbDuyet.NuocXX_ID = entity.NuocXX_ID;
            //            tbDuyet.SoHopDong = this.SoHopDong;
            //            tbDuyet.SoLuongDangKy = entity.SoLuongDangKy;
            //            tbDuyet.Ten = entity.Ten;
            //            tbDuyet.TinhTrang = entity.TinhTrang;
            //            tbDuyet.TriGia = entity.TriGia;
            //            tbDuyet.InsertUpdateTransaction(transaction);
            //        }
            //        foreach (SanPham entity in this.SPCollection)
            //        {
            //            Company.GC.BLL.GC.SanPham spDuyet = new Company.GC.BLL.GC.SanPham();
            //            spDuyet.DVT_ID = entity.DVT_ID;
            //            spDuyet.Ma = entity.Ma;
            //            spDuyet.MaDoanhNghiep = this.MaDoanhNghiep;
            //            spDuyet.MaHaiQuan = this.MaHaiQuan;
            //            spDuyet.MaHS = entity.MaHS;
            //            spDuyet.NgayKy = this.NgayKy;
            //            spDuyet.NhomSanPham = entity.NhomSanPham_ID;
            //            spDuyet.SoHopDong = this.SoHopDong;
            //            spDuyet.SoLuongDangKy = entity.SoLuongDangKy;
            //            spDuyet.SoLuongDaXuat = 0;
            //            spDuyet.Ten = entity.Ten;
            //            spDuyet.InsertUpdateTransaction(transaction);
            //        }
            //        transaction.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        transaction.Rollback();
            //        throw new Exception(ex.Message);
            //    }
            //    finally
            //    {
            //        connection.Close();
            //    }
            //}
        }
        public string WSSend(string pass)
        {
            #region Load XmlDocument
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.HopDong, (int)MessageFunctions.KhaiBao)); // OK
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\KhaiBaoHopDong.xml");

            docHopDong.SelectSingleNode(@"Root/DHDGC").Attributes["ID"].Value = this.ID.ToString();

            XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/DHDGC/So_HD");
            if (this.SoHopDong.Length <= 40)
                NodeSoHD.InnerText = this.SoHopDong;
            else
                NodeSoHD.InnerText = this.SoHopDong.Substring(0, 40);
            XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/DHDGC/Ma_HQHD");
            NodeMaHaiQuan.InnerText = this.MaHaiQuan;
            XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/DHDGC/DVGC");
            NodeMaDoanhNghiep.InnerText = this.MaDoanhNghiep;
            XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/DHDGC/Ngay_Ky");
            NodeNgayKy.InnerText = this.NgayKy.ToString("MM/dd/yyyy");
            XmlNode NodeNgayHetHan = docHopDong.SelectSingleNode(@"Root/DHDGC/Ngay_HH");
            NodeNgayHetHan.InnerText = this.NgayHetHan.ToString("MM/dd/yyyy");
            XmlNode NodeNgayGiaHan = docHopDong.SelectSingleNode(@"Root/DHDGC/Ngay_GH");
            if (this.NgayGiaHan.Year > 1900)
            {
                NodeNgayGiaHan.InnerText = this.NgayGiaHan.ToString("MM/dd/yyyy");
            }
            else
                NodeNgayGiaHan.InnerText = string.Empty;
            XmlNode NodeNuocThue = docHopDong.SelectSingleNode(@"Root/DHDGC/NuocThueGC");
            NodeNuocThue.InnerText = this.NuocThue_ID;
            XmlNode NodeNguyenTe = docHopDong.SelectSingleNode(@"Root/DHDGC/NgTe");
            NodeNguyenTe.InnerText = this.NguyenTe_ID;
            XmlNode NodeDonViDoiTac = docHopDong.SelectSingleNode(@"Root/DHDGC/DVDT");
            if (this.DonViDoiTac.Length > 40)
                NodeDonViDoiTac.InnerText = FontConverter.Unicode2TCVN(this.DonViDoiTac.Substring(0, 40));
            else
                NodeDonViDoiTac.InnerText = FontConverter.Unicode2TCVN(this.DonViDoiTac);
            XmlNode NodeDiaChiDoiTac = docHopDong.SelectSingleNode(@"Root/DHDGC/DCDT");
            if (this.DiaChiDoiTac.Length > 80)
                NodeDiaChiDoiTac.InnerText = FontConverter.Unicode2TCVN(this.DiaChiDoiTac.Substring(0, 80));
            else
                NodeDiaChiDoiTac.InnerText = FontConverter.Unicode2TCVN(this.DiaChiDoiTac);
            CultureInfo culture = new CultureInfo("vi-VN");
            NumberFormatInfo f = new NumberFormatInfo();
            if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            {
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
            }
            //loai san pham gia cong

            XmlNode NodeDLOAISPGCs = docHopDong.SelectSingleNode(@"Root/DLOAISPGCs");

            #region By XML Nhóm sản phẩm
            foreach (NhomSanPham nhosp in this.NhomSPCollection)
            {
                XmlElement NodeDLOAISPGCChild = docHopDong.CreateElement("DLOAISPGC");

                XmlAttribute attLoaiSP = docHopDong.CreateAttribute("ID");
                attLoaiSP.Value = "0";
                NodeDLOAISPGCChild.Attributes.Append(attLoaiSP);

                XmlNode NodeSoHDChild = docHopDong.CreateElement("So_HD");
                if (this.SoHopDong.Length <= 40)
                    NodeSoHDChild.InnerText = this.SoHopDong;
                else
                    NodeSoHDChild.InnerText = this.SoHopDong.Substring(0, 40);


                XmlNode NodeMaHaiQuanChild = docHopDong.CreateElement("Ma_HQHD");
                NodeMaHaiQuanChild.InnerText = this.MaHaiQuan;

                XmlNode NodeMaDoanhNghiepChild = docHopDong.CreateElement("DVGC");
                NodeMaDoanhNghiepChild.InnerText = this.MaDoanhNghiep;

                XmlNode NodeNgayKyChild = docHopDong.CreateElement("Ngay_Ky");
                NodeNgayKyChild.InnerText = this.NgayKy.ToString("MM/dd/yyyy");

                XmlNode NodeMaSP = docHopDong.CreateElement("Ma_SPGC");
                NodeMaSP.InnerText = nhosp.MaSanPham;

                XmlNode NodeSoLuong = docHopDong.CreateElement("So_Luong");
                NodeSoLuong.InnerText = nhosp.SoLuong.ToString(f); ;

                XmlNode NodeGia = docHopDong.CreateElement("Gia_GC");
                NodeGia.InnerText = nhosp.GiaGiaCong.ToString(f);

                NodeDLOAISPGCChild.AppendChild(NodeSoHDChild);
                NodeDLOAISPGCChild.AppendChild(NodeMaHaiQuanChild);
                NodeDLOAISPGCChild.AppendChild(NodeMaDoanhNghiepChild);
                NodeDLOAISPGCChild.AppendChild(NodeNgayKyChild);
                NodeDLOAISPGCChild.AppendChild(NodeMaSP);
                NodeDLOAISPGCChild.AppendChild(NodeSoLuong);
                NodeDLOAISPGCChild.AppendChild(NodeGia);
                NodeDLOAISPGCs.AppendChild(NodeDLOAISPGCChild);

            }
            #endregion By XML Nhóm sản phẩm

            #region By XML  sản phẩm
            XmlNode NodeSP = docHopDong.SelectSingleNode(@"Root/DSPGCs");
            foreach (SanPham sp in this.SPCollection)
            {
                XmlElement NodeSPChild = docHopDong.CreateElement("DSPGC");

                XmlAttribute attSP = docHopDong.CreateAttribute("ID");
                attSP.Value = "0";
                NodeSPChild.Attributes.Append(attSP);

                XmlNode NodeSoHDChild = docHopDong.CreateElement("So_HD");
                if (this.SoHopDong.Length <= 40)
                    NodeSoHDChild.InnerText = this.SoHopDong;
                else
                    NodeSoHDChild.InnerText = this.SoHopDong.Substring(0, 40);

                XmlNode NodeMaHaiQuanChild = docHopDong.CreateElement("Ma_HQHD");
                NodeMaHaiQuanChild.InnerText = this.MaHaiQuan;

                XmlNode NodeMaDoanhNghiepChild = docHopDong.CreateElement("DVGC");
                NodeMaDoanhNghiepChild.InnerText = this.MaDoanhNghiep;

                XmlNode NodeNgayKyChild = docHopDong.CreateElement("Ngay_Ky");
                NodeNgayKyChild.InnerText = this.NgayKy.ToString("MM/dd/yyyy");

                XmlNode NodeMaSP = docHopDong.CreateElement("P_Code");
                if (sp.Ma.Length <= 29)
                    NodeMaSP.InnerText = "S" + sp.Ma;
                else
                    NodeMaSP.InnerText = "S" + sp.Ma.Substring(0, 29);

                XmlNode NodeTenSP = docHopDong.CreateElement("Ten_SP");
                if (sp.Ten.Length <= 80)
                    NodeTenSP.InnerText = FontConverter.Unicode2TCVN(sp.Ten);
                else
                    NodeTenSP.InnerText = FontConverter.Unicode2TCVN(sp.Ten.Substring(0, 80));
                XmlNode NodeMaHS = docHopDong.CreateElement("HS_Code");
                NodeMaHS.InnerText = sp.MaHS;

                XmlNode NodeSoLuongDK = docHopDong.CreateElement("SL_DK");
                NodeSoLuongDK.InnerText = sp.SoLuongDangKy.ToString(f);

                XmlNode NodeSoLuongDC = docHopDong.CreateElement("SL_DC");
                NodeSoLuongDC.InnerText = "0";

                XmlNode NodeDVT = docHopDong.CreateElement("Ma_DVT");
                NodeDVT.InnerText = sp.DVT_ID;

                XmlNode NodeVBDC = docHopDong.CreateElement("VB_DC");
                NodeVBDC.InnerText = "";

                XmlNode NodeNhomSP = docHopDong.CreateElement("Nhom_SP");
                NodeNhomSP.InnerText = (sp.NhomSanPham_ID);

                //Linhhtn
                XmlNode NodeDonGia = docHopDong.CreateElement("DonGia");
                NodeDonGia.InnerText = sp.DonGia.ToString(f);

                NodeSPChild.AppendChild(NodeSoHDChild);
                NodeSPChild.AppendChild(NodeMaHaiQuanChild);
                NodeSPChild.AppendChild(NodeMaDoanhNghiepChild);
                NodeSPChild.AppendChild(NodeNgayKyChild);
                NodeSPChild.AppendChild(NodeMaSP);
                NodeSPChild.AppendChild(NodeTenSP);
                NodeSPChild.AppendChild(NodeMaHS);
                NodeSPChild.AppendChild(NodeSoLuongDK);
                NodeSPChild.AppendChild(NodeDVT);
                NodeSPChild.AppendChild(NodeNhomSP);
                NodeSPChild.AppendChild(NodeSoLuongDC);
                NodeSPChild.AppendChild(NodeVBDC);
                NodeSPChild.AppendChild(NodeDonGia);
                NodeSP.AppendChild(NodeSPChild);

            }
            #endregion By XML sản phẩm

            #region By XML nguyên phụ liệu
            XmlNode NodeNPL = docHopDong.SelectSingleNode(@"Root/DNPLHDs");
            foreach (NguyenPhuLieu npl in this.NPLCollection)
            {
                XmlElement NodeNPLChild = docHopDong.CreateElement("DNPLHD");

                XmlAttribute attNPL = docHopDong.CreateAttribute("ID");
                attNPL.Value = "0";
                NodeNPLChild.Attributes.Append(attNPL);

                XmlNode NodeSoHDChild = docHopDong.CreateElement("So_HD");
                if (this.SoHopDong.Length <= 40)
                    NodeSoHDChild.InnerText = this.SoHopDong;
                else
                    NodeSoHDChild.InnerText = this.SoHopDong.Substring(0, 40);

                XmlNode NodeMaHaiQuanChild = docHopDong.CreateElement("Ma_HQHD");
                NodeMaHaiQuanChild.InnerText = this.MaHaiQuan;

                XmlNode NodeMaDoanhNghiepChild = docHopDong.CreateElement("DVGC");
                NodeMaDoanhNghiepChild.InnerText = this.MaDoanhNghiep;

                XmlNode NodeNgayKyChild = docHopDong.CreateElement("Ngay_Ky");
                NodeNgayKyChild.InnerText = this.NgayKy.ToString("MM/dd/yyyy");

                XmlNode NodeMaNPL = docHopDong.CreateElement("P_Code");
                if (npl.Ma.Length >= 29)
                    NodeMaNPL.InnerText = "N" + npl.Ma.Substring(0, 29);
                else
                    NodeMaNPL.InnerText = "N" + npl.Ma;

                XmlNode NodeTenNPL = docHopDong.CreateElement("Ten_NPL");
                if (npl.Ten.Length > 80)
                    NodeTenNPL.InnerText = FontConverter.Unicode2TCVN(npl.Ten.Substring(0, 80));
                else
                    NodeTenNPL.InnerText = FontConverter.Unicode2TCVN(npl.Ten);
                XmlNode NodeMaHS = docHopDong.CreateElement("HS_Code");
                NodeMaHS.InnerText = npl.MaHS;

                XmlNode NodeSoLuongDK = docHopDong.CreateElement("SL_DK");
                NodeSoLuongDK.InnerText = npl.SoLuongDangKy.ToString(f);


                XmlNode NodeSoLuongDC = docHopDong.CreateElement("SL_DC");
                NodeSoLuongDC.InnerText = "0";

                XmlNode NodeDVT = docHopDong.CreateElement("Ma_DVT");
                NodeDVT.InnerText = npl.DVT_ID;

                XmlNode NodeSTT = docHopDong.CreateElement("Index");
                NodeSTT.InnerText = npl.STTHang.ToString();

                XmlNode NodeVBDC = docHopDong.CreateElement("VB_DC");
                NodeVBDC.InnerText = "";

                //Linhhtn
                XmlNode NodeDonGia = docHopDong.CreateElement("DonGia");
                NodeDonGia.InnerText = npl.DonGia.ToString(f);

                NodeNPLChild.AppendChild(NodeSoHDChild);
                NodeNPLChild.AppendChild(NodeMaHaiQuanChild);
                NodeNPLChild.AppendChild(NodeMaDoanhNghiepChild);
                NodeNPLChild.AppendChild(NodeNgayKyChild);
                NodeNPLChild.AppendChild(NodeMaNPL);
                NodeNPLChild.AppendChild(NodeTenNPL);
                NodeNPLChild.AppendChild(NodeMaHS);
                NodeNPLChild.AppendChild(NodeSoLuongDK);
                NodeNPLChild.AppendChild(NodeDVT);
                NodeNPLChild.AppendChild(NodeSoLuongDC);
                NodeNPLChild.AppendChild(NodeSTT);
                NodeNPLChild.AppendChild(NodeVBDC);
                NodeNPLChild.AppendChild(NodeDonGia);

                NodeNPL.AppendChild(NodeNPLChild);

            }
            #endregion By XML sản phẩm

            #region By XML thiết bị
            XmlNode NodeTB = docHopDong.SelectSingleNode(@"Root/DThietBis");
            foreach (ThietBi tb in this.TBCollection)
            {
                XmlElement NodeTBChild = docHopDong.CreateElement("DThietBi");

                XmlNode NodeSoHDChild = docHopDong.CreateElement("So_HD");

                if (this.SoHopDong.Length <= 40)
                    NodeSoHDChild.InnerText = this.SoHopDong;
                else
                    NodeSoHDChild.InnerText = this.SoHopDong.Substring(0, 40);
                XmlNode NodeMaHaiQuanChild = docHopDong.CreateElement("Ma_HQHD");
                NodeMaHaiQuanChild.InnerText = this.MaHaiQuan;

                XmlNode NodeMaDoanhNghiepChild = docHopDong.CreateElement("DVGC");
                NodeMaDoanhNghiepChild.InnerText = this.MaDoanhNghiep;

                XmlNode NodeNgayKyChild = docHopDong.CreateElement("Ngay_Ky");
                NodeNgayKyChild.InnerText = this.NgayKy.ToString("MM/dd/yyyy");

                XmlNode NodeMaTB = docHopDong.CreateElement("P_Code");
                if (tb.Ma.Length >= 29)
                    NodeMaTB.InnerText = "T" + tb.Ma.Substring(0, 29);
                else
                    NodeMaTB.InnerText = "T" + tb.Ma;

                XmlNode NodeTenTB = docHopDong.CreateElement("Ten_TB");
                if (tb.Ten.Length <= 80)
                    NodeTenTB.InnerText = FontConverter.Unicode2TCVN(tb.Ten);
                else
                    NodeTenTB.InnerText = FontConverter.Unicode2TCVN(tb.Ten.Substring(0, 80));
                XmlNode NodeMaHS = docHopDong.CreateElement("HS_Code");
                NodeMaHS.InnerText = tb.MaHS;

                XmlNode NodeSoLuongDK = docHopDong.CreateElement("SL_DK");
                NodeSoLuongDK.InnerText = tb.SoLuongDangKy.ToString(f);

                XmlNode NodeDVT = docHopDong.CreateElement("Ma_DVT");
                NodeDVT.InnerText = tb.DVT_ID;

                XmlNode NodeDonGia = docHopDong.CreateElement("DonGia");
                NodeDonGia.InnerText = tb.DonGia.ToString(f);

                XmlNode NodeTriGia = docHopDong.CreateElement("TriGia");
                NodeTriGia.InnerText = tb.TriGia.ToString(f);


                XmlNode NodeNguyenTeTB = docHopDong.CreateElement("NGTe");
                NodeNguyenTeTB.InnerText = tb.NguyenTe_ID;

                XmlNode NodeXuatXu = docHopDong.CreateElement("Xuat_Xu");
                NodeXuatXu.InnerText = tb.NuocXX_ID;

                XmlNode NodeGhiChu = docHopDong.CreateElement("GhiChu");
                NodeGhiChu.InnerText = FontConverter.Unicode2TCVN(tb.TinhTrang);

                NodeTBChild.AppendChild(NodeSoHDChild);
                NodeTBChild.AppendChild(NodeMaHaiQuanChild);
                NodeTBChild.AppendChild(NodeMaDoanhNghiepChild);
                NodeTBChild.AppendChild(NodeNgayKyChild);
                NodeTBChild.AppendChild(NodeMaTB);
                NodeTBChild.AppendChild(NodeTenTB);
                NodeTBChild.AppendChild(NodeMaHS);
                NodeTBChild.AppendChild(NodeSoLuongDK);
                NodeTBChild.AppendChild(NodeDVT);
                NodeTBChild.AppendChild(NodeDonGia);
                NodeTBChild.AppendChild(NodeTriGia);
                NodeTBChild.AppendChild(NodeNguyenTeTB);
                NodeTBChild.AppendChild(NodeXuatXu);
                NodeTBChild.AppendChild(NodeGhiChu);
                NodeTB.AppendChild(NodeTBChild);

            }
            #endregion By XML thiết bị

            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);
            #endregion Load XmlDocument

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();

            string kq = "";
            string msgError = string.Empty;
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoHopDong);
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                    throw ex;
                }
                if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                throw new Exception("Lỗi :" + (msgError != string.Empty ? msgError : "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL"));
            }
            return "";
        }

        public string WSCancel(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.HopDong, (int)MessageFunctions.HuyKhaiBao));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\HuyKhaiBaoHopDong.xml");

            XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/So_HD");
            if (this.SoHopDong.Length <= 40)
                NodeSoHD.InnerText = this.SoHopDong;
            else
                NodeSoHD.InnerText = this.SoHopDong.Substring(0, 40);
            XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/Ma_HQHD");
            NodeMaHaiQuan.InnerText = this.MaHaiQuan;
            XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/DVGC");
            NodeMaDoanhNghiep.InnerText = this.MaDoanhNghiep;
            XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/Ngay_Ky");
            NodeNgayKy.InnerText = this.NgayKy.ToString("MM/dd/yyyy");
            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoHuyHopDong);

                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        return doc.InnerXml;
                    }
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Logger.LocalLogger.Instance().WriteMessage("Send", new Exception(doc.InnerXml));
                Logger.LocalLogger.Instance().WriteMessage("Request", new Exception(kq));
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            return "";
        }

        public string WSDownLoad(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBiPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi));
            //XmlDocument docHopDong = new XmlDocument();
            //string path = EntityBase.GetPathProram();
            //docHopDong.Load(path+"\\B03GiaCong\\LayPhanHoiHopDong.XML");

            //XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/So_HD");
            //if (this.SoHopDong.Length <= 40)
            //    NodeSoHD.InnerText = this.SoHopDong;
            //else
            //    NodeSoHD.InnerText = this.SoHopDong.Substring(0, 40);
            //XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/Ma_HQHD");
            //NodeMaHaiQuan.InnerText = this.MaHaiQuan;
            //XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/DVGC");
            //NodeMaDoanhNghiep.InnerText = this.MaDoanhNghiep;
            //XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/Ngay_Ky");
            //NodeNgayKy.InnerText = this.NgayKy.ToString("MM/dd/yyyy");

            //XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            //XmlNode Content = doc.GetElementsByTagName("Content")[0];
            //Content.AppendChild(root);  
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(MaHaiQuan));

            //this.GUIDSTR = (System.Guid.NewGuid().ToString()); ;
            //this.Update();
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        #endregion WS

        public bool InsertHopDongDongBoDuLieu(KDT_GC_IDHopDongCollection IDCollection)
        {
            //bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    long ID = Insert(transaction);
                    foreach (KDT_GC_IDHopDong IDHD in IDCollection)
                    {
                        if (IDHD.SoHopDong == this.SoHopDong)
                            IDHD.ID_MOI = ID;
                    }
                    if (ID > 0)
                    {
                        int i = 1;
                        foreach (NguyenPhuLieu item in NPLCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.Insert(transaction);
                            //cap nhat sang du lieu xu ly
                            BLL.GC.NguyenPhuLieu entity = new Company.GC.BLL.GC.NguyenPhuLieu();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.MaHS = item.MaHS;
                            entity.DVT_ID = item.DVT_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            try
                            {
                                entity.InsertTransaction(transaction);
                            }
                            catch { }
                        }
                        i = 1;
                        foreach (NhomSanPham item in NhomSPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.Insert(transaction);

                            BLL.GC.NhomSanPham entity = new Company.GC.BLL.GC.NhomSanPham();
                            entity.HopDong_ID = this.ID;
                            entity.MaSanPham = item.MaSanPham;
                            entity.GiaGiaCong = item.GiaGiaCong;
                            entity.SoLuong = item.SoLuong;
                            entity.STTHang = item.STTHang;
                            entity.TenSanPham = item.TenSanPham;
                            entity.InsertTransaction(transaction);
                        }
                        i = 1;
                        foreach (SanPham item in SPCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.InsertTransaction(transaction);

                            BLL.GC.SanPham entity = new Company.GC.BLL.GC.SanPham();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.NhomSanPham_ID = item.NhomSanPham_ID;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            try
                            {
                                entity.InsertTransaction(transaction);
                            }
                            catch { }
                        }
                        i = 1;
                        foreach (ThietBi item in TBCollection)
                        {
                            item.HopDong_ID = ID;
                            item.STTHang = i++;
                            item.Insert(transaction);

                            BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            entity.HopDong_ID = this.ID;
                            entity.Ma = item.Ma;
                            entity.DVT_ID = item.DVT_ID;
                            entity.MaHS = item.MaHS;
                            entity.DonGia = item.DonGia;
                            entity.SoLuongDangKy = item.SoLuongDangKy;
                            entity.STTHang = item.STTHang;
                            entity.Ten = item.Ten;
                            entity.NguyenTe_ID = item.NguyenTe_ID;
                            entity.NuocXX_ID = item.NuocXX_ID;
                            entity.TinhTrang = item.TinhTrang;
                            entity.TriGia = item.TriGia;
                            try
                            {
                                entity.InsertTransaction(transaction);
                            }
                            catch { }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return true;
        }

        public void TinhToanTongNhuCauNPLTheoSoLuongDangKyCuaSanPham()
        {
            HopDong.Load(this.ID);
            #region Xu ly NPL
            Company.GC.BLL.GC.NguyenPhuLieu NPLData = new Company.GC.BLL.GC.NguyenPhuLieu();
            NPLData.HopDong_ID = this.ID;
            NguyenPhuLieuCollection NPLCollectionData = NPLData.SelectCollectionBy_HopDong_ID();
            foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
            {
                NPLDataItem.TongNhuCau = 0;
            }
            #endregion Xu ly NPL


            #region Xu ly SP
            Company.GC.BLL.GC.SanPham SPData = new Company.GC.BLL.GC.SanPham();
            SPData.HopDong_ID = this.ID;
            BLL.GC.SanPhamCollection SPCollectionData = SPData.SelectCollectionBy_HopDong_ID();

            #endregion Xu ly SP
            foreach (Company.GC.BLL.GC.SanPham SP in SPCollectionData)
            {
                DataSet dsLuongNPL = Company.GC.BLL.GC.NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(SP.Ma.Trim(), SP.SoLuongDangKy, this.ID);
                if (dsLuongNPL.Tables[0].Rows.Count == 0)
                {
                    //string msg = "Mã sản phẩm : " + SP.Ma;           
                    //msg += " chưa có định mức.";
                    //throw new Exception(msg);

                }
                foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                {
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    string maNPL = row["MaNguyenPhuLieu"].ToString();
                    npl = getNPL(maNPL, NPLCollectionData);
                    if (npl == null)
                    {
                        string msg = "Mã sản phẩm : " + SP.Ma;
                        msg += " có định mức với nguyên phụ liệu có mã " + maNPL + "có trong danh sách định mức nhưng không có trong danh sách nguyên phụ liệu được đăng ký.";
                        throw new Exception(msg);
                    }
                    npl.TongNhuCau += Convert.ToDecimal(row["LuongCanDung"]);
                }
            }



            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (Company.GC.BLL.GC.NguyenPhuLieu NPLDataItem in NPLCollectionData)
                    {
                        NPLDataItem.UpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public bool CheckMaNPLIsExist(string maNPL)
        {

            string sql = "SELECT count(Ma) FROM T_GC_NguyenPhuLieu WHERE Ma = '" + maNPL + "' AND HopDong_ID =" + this.ID;
            SqlCommand command = (SqlCommand)this.db.GetSqlStringCommand(sql);
            int t = 0;
            try
            {
                t = Convert.ToInt32(db.ExecuteScalar(command));
            }
            catch
            {
                t = 0;
            }
            return t > 0;
        }
        public bool CheckMaSPIsExist(string maSP)
        {

            string sql = "SELECT count(Ma) FROM T_GC_SanPham WHERE Ma = '" + maSP + "' AND HopDong_ID =" + this.ID;
            SqlCommand command = (SqlCommand)this.db.GetSqlStringCommand(sql);
            int t = 0;
            try
            {
                t = Convert.ToInt32(db.ExecuteScalar(command));
            }
            catch
            {
                t = 0;
            }
            return t > 0;
        }
        public int DeleteAllHopDong()
        {
            //SetDabaseMoi("MSSQL");
            //string sql = " Delete from t_KDT_GC_HopDong ";
            //SqlCommand command = (SqlCommand)this.db.GetSqlStringCommand(sql);
            int t = 0;
            //try
            //{
            //    return this.db.ExecuteNonQuery(command);
            //}
            //catch
            //{
            //    t = 0;
            //}
            return t;
        }
        public bool DeleteQuyetToan(long ID_HopDong)
        {
            try
            {
                t_GC_QuyetToan_Mau15.DeleteDynamic("HopDong_ID = " + ID_HopDong);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
                throw;
            }

            return true;
        }
        public static bool DeleteQuyetToan(string List_ID_HopDong)
        {
            try
            {
                t_GC_QuyetToan_Mau15.DeleteDynamic("HopDong_ID in(" + List_ID_HopDong + ")");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
                throw;
            }

            return true;
        }
        public static bool UpdateQuyetToan(string ListIdHopDong)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "Update t_KDT_GC_HopDong set TrangThaiThanhKhoan = 2 WHERE ID in (" + ListIdHopDong + ")";
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            int t = 0;
            try
            {
                t = Convert.ToInt32(db.ExecuteScalar(command));
            }
            catch
            {
                t = 0;
            }
            return t > 0;
        }
        public static bool ResetQuyetToan(string ListIdHopDong)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            // 0: chưa thanh khoản, 1 đã thanh khoản, 2 đã quyết toán
            string sql = "Update t_KDT_GC_HopDong set TrangThaiThanhKhoan = 1 WHERE ID in (" + ListIdHopDong + ")";
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            int t = 0;
            try
            {
                t = Convert.ToInt32(db.ExecuteScalar(command));
            }
            catch
            {
                t = 0;
            }
            return t > 0;
        }
        public DataSet GetToKhaiSXXKTon()
        {

            string sql = "SELECT DISTINCT SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy FROM t_GC_NPLNhapTonThucTe WHERE MaLoaiHinh LIKE 'NSX%' AND Ton > 0 AND ID_HopDong =" + this.ID;
            SqlCommand command = (SqlCommand)this.db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        #region DongBoDuLieuPhongKhai
        public static void DongBoDuLieuKhaiDTByIDHopDong(HopDong HD, string nameConnectKDT)
        {
            //nguyen phu lieu
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from DNPLHD where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            DataSet dsNPL = db.ExecuteDataSet(dbCommand);
            HD.NPLCollection = new List<NguyenPhuLieu>();
            foreach (DataRow row in dsNPL.Tables[0].Rows)
            {
                NguyenPhuLieu NPL = new NguyenPhuLieu();
                NPL.DVT_ID = row["Ma_DVT"].ToString();
                NPL.Ma = row["P_Code"].ToString().Substring(1).Trim();
                NPL.MaHS = row["HS_Code"].ToString().Trim();
                NPL.SoLuongDangKy = Convert.ToDecimal(row["SL_DK"]);
                NPL.Ten = FontConverter.TCVN2Unicode(row["Ten_NPL"].ToString().Trim());
                HD.NPLCollection.Add(NPL);
            }
            //loai san pham

            sql = "select * from DLOAISPGC where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD";
            dbCommand.Parameters.Clear();
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            DataSet dsLoaiSP = db.ExecuteDataSet(dbCommand);
            HD.NhomSPCollection = new List<NhomSanPham>();
            foreach (DataRow row in dsLoaiSP.Tables[0].Rows)
            {
                NhomSanPham nhomSP = new NhomSanPham();
                nhomSP.MaSanPham = row["Ma_SPGC"].ToString();
                nhomSP.GiaGiaCong = Convert.ToDecimal(row["Gia_GC"]);
                nhomSP.SoLuong = Convert.ToDecimal(row["So_Luong"]);
                nhomSP.TenSanPham = Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham.getTenSanPham(nhomSP.MaSanPham);
                HD.NhomSPCollection.Add(nhomSP);
            }
            // san pham
            sql = "select * from DSPGC where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD";
            dbCommand.Parameters.Clear();
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            DataSet dsSP = db.ExecuteDataSet(dbCommand);
            HD.SPCollection = new SanPhamCollection();
            foreach (DataRow row in dsSP.Tables[0].Rows)
            {
                SanPham sp = new SanPham();
                sp.Ma = row["P_Code"].ToString().Substring(1).Trim();
                //sp.Ten = (row["Ten_SP"].ToString());
                sp.MaHS = (row["HS_Code"].ToString().Trim());
                sp.Ten = FontConverter.TCVN2Unicode(row["Ten_SP"].ToString().Trim());
                sp.DVT_ID = (row["Ma_DVT"].ToString());
                sp.NhomSanPham_ID = row["Nhom_SP"].ToString();
                sp.SoLuongDangKy = Convert.ToDecimal(row["SL_DK"]);
                HD.SPCollection.Add(sp);
            }
            //thiet bi
            sql = "select * from DThietBi where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD";
            dbCommand.Parameters.Clear();
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            DataSet dsTB = db.ExecuteDataSet(dbCommand);
            HD.TBCollection = new List<ThietBi>();
            foreach (DataRow row in dsTB.Tables[0].Rows)
            {
                ThietBi tb = new ThietBi();
                tb.DonGia = Convert.ToDouble(row["DonGia"]);
                tb.Ma = (row["P_CODE"].ToString().Substring(1).Trim());
                tb.MaHS = (row["HS_CODE"].ToString().Trim());
                tb.Ten = FontConverter.TCVN2Unicode(row["Ten_TB"].ToString().Trim());
                tb.DVT_ID = row["Ma_DVT"].ToString();
                tb.SoLuongDangKy = Convert.ToDecimal(row["SL_DK"]);
                tb.NguyenTe_ID = row["NGTe"].ToString();
                tb.NuocXX_ID = row["Xuat_Xu"].ToString();
                tb.TinhTrang = row["TinhTrang"].ToString();
                tb.TriGia = Convert.ToDouble(row["SL_DK"]);
                HD.TBCollection.Add(tb);
            }
            HD.TrangThaiXuLy = 0;
            try
            {
                HD.InsertUpdateHopDong();
            }
            catch { }
        }
        public static void DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_GC_HopDong where MaDoanhNghiep=@DVGC and TrangThaiXuLy=0 and MaHaiQuan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.ExecuteNonQuery(dbCommandDelete);

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from DHDGC where DVGC=@DVGC and TrangThai<>2 and Ma_HQHD=@Ma_HQHD";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HopDong Hd = new HopDong();
                Hd.CanBoDuyet = FontConverter.TCVN2Unicode(row["Nguoi_Duyet"].ToString());
                Hd.DiaChiDoiTac = FontConverter.TCVN2Unicode(row["DiaChi_DT"].ToString());
                Hd.DonViDoiTac = FontConverter.TCVN2Unicode(row["DVDT"].ToString());
                Hd.MaDoanhNghiep = MaDoanhNghiep;
                Hd.MaHaiQuan = MaHaiQuan;
                Hd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                Hd.NgayHetHan = Convert.ToDateTime(row["Ngay_HH"]);
                Hd.NgayKy = Convert.ToDateTime(row["Ngay_Ky"]);
                Hd.NgayTiepNhan = Hd.NgayDangKy;
                Hd.NguyenTe_ID = row["NgTe"].ToString();
                Hd.NuocThue_ID = row["NuocThueGC"].ToString();
                Hd.SoHopDong = row["So_HD"].ToString();
                Hd.SoTiepNhan = Convert.ToInt64(row["HDID"]);
                DongBoDuLieuKhaiDTByIDHopDong(Hd, nameConnectKDT);
            }

        }
        public bool InsertUpdates(List<HopDong> collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HopDong item in collection)
                    {
                        if (item.InsertUpdate(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        //ThoilV Edit :
        public bool UpdateRegistedToDatabaseHDGC(string maHaiQuan, string maDoanhNghiep, DataSet ds)
        {
            List<HopDong> hdCollection = new List<HopDong>();


            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HopDong hd = new HopDong();
                hd.MaHaiQuan = maHaiQuan;
                hd.MaDoanhNghiep = maDoanhNghiep;
                hd.SoHopDong = row["SoHopDong"].ToString();
                if (row["NgayKy"] != DBNull.Value)
                    hd.NgayKy = Convert.ToDateTime(row["NgayKy"]);
                if (row["NgayDangKy"] != DBNull.Value)
                    hd.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"]);
                if (row["NgayHetHan"] != DBNull.Value)
                    hd.NgayHetHan = Convert.ToDateTime(row["NgayHetHan"]);
                if (row["NgayGiaHan"] != DBNull.Value)
                    hd.NgayGiaHan = Convert.ToDateTime(row["NgayGiaHan"]);
                hd.NuocThue_ID = row["NuocThueGC"].ToString();
                hd.NguyenTe_ID = row["NguyenTe"].ToString();
                hd.TrangThaiXuLy = 1;
                hd.CanBoDuyet = row["CanBoDuyet"].ToString();
                hd.CanBoTheoDoi = row["CanBoTheoDoi"].ToString();

                hdCollection.Add(hd);
            }
            return this.InsertUpdates(hdCollection);
        }


        //======== Dong bo du lieu Da duyet
        public static void DongBoDuLieuDaDuyetByIDHopDong(HopDong HD, string nameConnectKDT)
        {
            //nguyen phu lieu
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from DNPLHD where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            DataSet dsNPL = db.ExecuteDataSet(dbCommand);
            HD.NPLCollection = new List<NguyenPhuLieu>();
            int STTL = 1;
            foreach (DataRow row in dsNPL.Tables[0].Rows)
            {
                NguyenPhuLieu NPL = new NguyenPhuLieu();
                NPL.Ma = row["P_Code"].ToString().Substring(1).Trim();
                if (HD.ID > 0)
                {
                    NPL.HopDong_ID = HD.ID;
                    NPL = NguyenPhuLieu.Load(HD.ID, NPL.Ma);
                }
                NPL.DVT_ID = row["Ma_DVT"].ToString();
                NPL.MaHS = row["HS_Code"].ToString().Trim();
                NPL.SoLuongDangKy = Convert.ToDecimal(row["SL_DK"]) + Convert.ToDecimal(row["SL_DC"]);
                NPL.Ten = FontConverter.TCVN2Unicode(row["Ten_NPL"].ToString().Trim()); ;
                NPL.STTHang = Convert.ToInt32(row["STT"]);
                HD.NPLCollection.Add(NPL);
            }
            //loai san pham

            sql = "select * from DLOAISPGC where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD";
            dbCommand.Parameters.Clear();
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            DataSet dsLoaiSP = db.ExecuteDataSet(dbCommand);
            HD.NhomSPCollection = new List<NhomSanPham>();
            foreach (DataRow row in dsLoaiSP.Tables[0].Rows)
            {
                NhomSanPham nhomSP = new NhomSanPham();
                nhomSP.MaSanPham = row["Ma_SPGC"].ToString().Trim();
                if (HD.ID > 0)
                {
                    nhomSP.HopDong_ID = HD.ID;
                    nhomSP.Load();

                }
                try
                {
                    nhomSP.GiaGiaCong = Convert.ToDecimal(row["Gia_GC"].ToString());
                }
                catch { }
                nhomSP.SoLuong = Convert.ToDecimal(row["So_Luong"]);
                nhomSP.TenSanPham = Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham.getTenSanPham(nhomSP.MaSanPham);
                nhomSP.STTHang = STTL;
                STTL++;
                HD.NhomSPCollection.Add(nhomSP);
            }
            // san pham
            sql = "select * from DSPGC where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD";
            dbCommand.Parameters.Clear();
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            DataSet dsSP = db.ExecuteDataSet(dbCommand);
            HD.SPCollection = new SanPhamCollection();
            foreach (DataRow row in dsSP.Tables[0].Rows)
            {
                SanPham sp = new SanPham();
                sp.Ma = row["P_Code"].ToString().Substring(1).Trim();
                if (HD.ID > 0)
                {
                    sp.HopDong_ID = HD.ID;
                    sp.Load();
                }
                sp.MaHS = (row["HS_Code"].ToString().Trim());
                sp.Ten = FontConverter.TCVN2Unicode(row["Ten_SP"].ToString().Trim());
                sp.NhomSanPham_ID = row["Nhom_SP"].ToString();
                sp.SoLuongDangKy = Convert.ToDecimal(row["SL_DK"]) + Convert.ToDecimal(row["SL_DC"]);
                sp.STTHang = Convert.ToInt32(row["STT"]);
                sp.DVT_ID = row["Ma_DVT"].ToString();
                HD.SPCollection.Add(sp);
            }
            //thiet bi
            sql = "select * from DThietBi where  DVGC=@DVGC and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD and Ngay_Ky=@Ngay_Ky";
            dbCommand.Parameters.Clear();
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            DataSet dsTB = db.ExecuteDataSet(dbCommand);
            HD.TBCollection = new List<ThietBi>();
            foreach (DataRow row in dsTB.Tables[0].Rows)
            {
                ThietBi tb = new ThietBi();
                tb.Ma = (row["P_CODE"].ToString().Substring(1).Trim());
                if (HD.ID > 0)
                {
                    tb.HopDong_ID = HD.ID;
                    tb.Load();
                }
                tb.DonGia = Convert.ToDouble(row["DonGia"]);
                tb.MaHS = (row["HS_CODE"].ToString().Trim());
                tb.Ten = FontConverter.TCVN2Unicode(row["Ten_TB"].ToString().Trim());
                tb.DVT_ID = row["Ma_DVT"].ToString();
                tb.SoLuongDangKy = Convert.ToDecimal(row["SL_DK"]);
                tb.NguyenTe_ID = row["NGTe"].ToString();
                tb.NuocXX_ID = row["Xuat_Xu"].ToString();
                tb.TinhTrang = row["TinhTrang"].ToString();
                tb.TriGia = Convert.ToDouble(row["TriGia"]);
                tb.STTHang = Convert.ToInt32(row["STT"]);
                HD.TBCollection.Add(tb);
            }
            HD.TrangThaiXuLy = 1;
            try
            {
                HD.InsertUpdateHopDong();
            }
            catch { }
        }
        public static void DongBoDuLieuDaDuyet(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            //string sqlDelete = "delete t_KDT_GC_HopDong where MaDoanhNghiep=@DVGC and TrangThaiXuLy=1 and MaHaiQuan=@Ma_HQHD";
            //SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            //db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            //db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.Char, MaHaiQuan);
            //db.ExecuteNonQuery(dbCommandDelete);

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select * from DHDGC where DVGC=@DVGC and Ma_HQHD=@Ma_HQHD AND (Ngay_HH >= GETDATE()) OR " +
                      "((Ngay_GH IS NOT NULL) AND (Ngay_GH >= GETDATE()) and DVGC=@DVGC and Ma_HQHD=@Ma_HQHD) ";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HopDong Hd = new HopDong();
                Hd.CanBoDuyet = FontConverter.TCVN2Unicode(row["Nguoi_Duyet"].ToString());
                Hd.DiaChiDoiTac = FontConverter.TCVN2Unicode(row["DiaChi_DT"].ToString());
                Hd.DonViDoiTac = FontConverter.TCVN2Unicode(row["DVDT"].ToString());
                Hd.MaDoanhNghiep = MaDoanhNghiep;
                Hd.MaHaiQuan = MaHaiQuan;
                Hd.NgayDangKy = Convert.ToDateTime(row["Ngay_DK"]);
                Hd.NgayHetHan = Convert.ToDateTime(row["Ngay_HH"]);
                Hd.NgayKy = Convert.ToDateTime(row["Ngay_Ky"]);
                Hd.NgayTiepNhan = Hd.NgayDangKy;
                Hd.NguyenTe_ID = row["NgTe"].ToString();
                Hd.NuocThue_ID = row["NuocThueGC"].ToString();
                Hd.SoHopDong = row["So_HD"].ToString().Trim();
                Hd.SoTiepNhan = 0;
                Hd.ID = Hd.GetIDHopDongExit(Hd.SoHopDong, Hd.MaHaiQuan, Hd.MaDoanhNghiep, Hd.NgayKy);
                DongBoDuLieuDaDuyetByIDHopDong(Hd, nameConnectKDT);
            }

        }
        //================================
        #endregion DongBoDuLieuPhongKhai

        #region TQDT :
        public string TQDTWSLayPhanHoi(string pass)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.HopDong, (int)MessageFunctions.HoiTrangThai));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            //docHopDong.Load(path + "\\B03GiaCong\\LayPhanHoiHopDong.XML");
            //XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/So_HD");
            //if (this.SoHopDong.Length <= 40)
            //    NodeSoHD.InnerText = this.SoHopDong;
            //else
            //    NodeSoHD.InnerText = this.SoHopDong.Substring(0, 40);
            //XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/Ma_HQHD");
            //NodeMaHaiQuan.InnerText = this.MaHaiQuan;
            //XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/DVGC");
            //NodeMaDoanhNghiep.InnerText = this.MaDoanhNghiep;
            //XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/Ngay_Ky");
            //NodeNgayKy.InnerText = this.NgayKy.ToString("MM/dd/yyyy");

            docHopDong.Load(path + "\\B03GiaCong\\LayPhanHoiDaDuyet.XML");
            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            string msgError = string.Empty;
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    throw new Exception(msgError);
                }
                return "";
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                }
                throw;
            }

        }
        public string TQDTLayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);


            doc.GetElementsByTagName("function")[0].InnerText = MessageFunctions.LayPhanHoi.ToString();
            doc.GetElementsByTagName("type")[0].InnerText = MessgaseType.ThongTin.ToString();
            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);

            XmlNode Result = null;
            string kq = "";
            int i = 0;
            for (i = 1; i <= 1; ++i)
            {
                string msgError = string.Empty;
                try
                {

                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);

                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        msgError = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "DATA_LEVEL");
                        throw new Exception(msgError);
                    }

                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty)
                    {
                        kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                        throw ex;
                    }
                    else
                    {
                        Logger.LocalLogger.Instance().WriteMessage("Send", new Exception(doc.InnerXml));
                        Logger.LocalLogger.Instance().WriteMessage("Request", new Exception(kq));
                    }
                    throw new Exception("Lỗi " + " : " + (msgError != string.Empty ? msgError : "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL"));
                }

            }

            if (i > 1)
                return doc.InnerXml;
            try
            {
                if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                }
                bool ok = false;

                /* if (function == MessageFunctions.KhaiBao)
                 {
                     this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                     this.NgayTiepNhan = DateTime.Today;
                     this.TrangThaiXuLy = 0;
                     ok = true;
                     kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHopDong);

                 }
                 else if (function == MessageFunctions.HuyKhaiBao)
                 {
                     this.SoTiepNhan = 0;
                     this.NgayTiepNhan = new DateTime(1900, 1, 1);
                     this.TrangThaiXuLy = -1;
                     ok = true;
                     kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHuyHopDong);

                 }
                 else*/
                // LanNT không cần thiết phải dùng đoạn trên bởi đây là method lấy phản hồi
                if (function == (int)MessageFunctions.LayPhanHoi)
                {
                    string errorSt = "";
                    try
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi") != null)
                        {
                            Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();

                            if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TuChoi"].Value == "yes")
                            {
                                //nodeTuChoi.InnerText;
                                this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                                //kqxl.ItemID = this.ID;
                                //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                                //kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                                //kqxl.NoiDung = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                                //kqxl.Ngay = DateTime.Now;
                                //kqxl.Insert();
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.SoTiepNhan = 0;
                                this.NgayTiepNhan = new DateTime(1990, 01, 01);
                                this.Update();
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQTuChoiHopDong, kqxl.NoiDung);

                            }
                            //DATLMQ BỔ SUNG THÊM KIỂM TRA NODE TỪ CHỐI = NO 11/11/2010
                            else
                            {
                                XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                                this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SOTN"].Value);
                                this.NamTN = Convert.ToInt32(nodeDuLieu.Attributes["NAMTN"].Value);
                                //this.NgayTiepNhan = Convert.ToDateTime(nodeDuLieu.Attributes["NGAYTN"].Value);
                                this.ActionStatus = 1;
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                this.Update();

                                //kqxl.ItemID = this.ID;
                                //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                                //kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_HopDongDuocDuyet;
                                ////kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNăm tiếp nhận: {1}\r\nNgày tiếp nhận: {2}", this.SoTiepNhan, this.NamTN, this.NgayTiepNhan.ToShortDateString());
                                //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNăm tiếp nhận: {1}", this.SoTiepNhan, this.NamTN);
                                //kqxl.Ngay = DateTime.Now;
                                //kqxl.Insert();
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQDuyet, kqxl.NoiDung);


                            }
                        }
                        else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root") != null
                            && docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "THANH CONG")
                        {
                            XmlNode nodeTrangthai = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                            if (nodeTrangthai.Attributes["TRALOI"].Value == THONG_TIN_DANG_KY.TOKHAINHAP || nodeTrangthai.Attributes["TRALOI"].Value == THONG_TIN_DANG_KY.TOKHAIXUAT
                                || nodeTrangthai.Attributes["TRALOI"].Value == THONG_TIN_DANG_KY.CAPNHATTOKHAINHAP || nodeTrangthai.Attributes["TRALOI"].Value == THONG_TIN_DANG_KY.CAPNHATTOKHAIXUAT)
                            {
                                #region Lấy số tiếp nhận
                                Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();

                                if (nodeTrangthai.Attributes["TrangThai"].Value == "THANH CONG")
                                {
                                    XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU");
                                    this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SOTN"].Value);
                                    this.NamTN = Convert.ToInt32(nodeDuLieu.Attributes["NAMTN"].Value);
                                    this.NgayTiepNhan = DateTime.Today;
                                    this.ActionStatus = 0;
                                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                                    this.Update();

                                    //kqxl.ItemID = this.ID;
                                    //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                    //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                                    //kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                                    //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                                    //kqxl.Ngay = DateTime.Now;
                                    //kqxl.Insert();
                                }
                                #endregion
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQCapSoTiepNhan, kqxl.NoiDung);
                            }
                            else if (nodeTrangthai.Attributes["TRALOI"].Value == THONG_TIN_HUY.TOKHAINHAP || nodeTrangthai.Attributes["TRALOI"].Value == THONG_TIN_HUY.TOKHAIXUAT)
                            {
                                #region Hủy khai báo
                                Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();

                                if (nodeTrangthai.Attributes["TrangThai"].Value == "THANH CONG")
                                {
                                    //kqxl.ItemID = this.ID;
                                    //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                    //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                                    //kqxl.LoaiThongDiep = "Hủy khai báo";
                                    //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nNgày hủy: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), DateTime.Now.ToString())
                                    //    + "\r\n" + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText); kqxl.Ngay = DateTime.Now;
                                    //kqxl.Insert();

                                    this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                    this.ActionStatus = -1;
                                    this.SoTiepNhan = 0;
                                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                                    this.Update();
                                }
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQHuyHopDong, kqxl.NoiDung);
                                #endregion
                            }
                            else if (nodeTrangthai.Attributes["TRALOI"].Value == LAY_THONG_TIN.TOKHAI)
                            {
                                #region Nhận trạng thái hồ sơ
                                XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DU_LIEU");
                                if (nodeDuLieu.Attributes["TrangThai"].Value == "DA_XU_LY")
                                {
                                    string phanHoi = TQDTLayPhanHoi(pass, doc.InnerXml);
                                    Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();

                                    if (phanHoi.Length != 0)
                                    {
                                        XmlDocument docPH = new XmlDocument();
                                        docPH.LoadXml(phanHoi);

                                        XmlNode nodeTuChoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                                        if (nodeTuChoi.Attributes.GetNamedItem("TuChoi") != null
                                            && nodeTuChoi.Attributes["TuChoi"].Value == "yes")
                                        {
                                            //nodeTuChoi.InnerText;
                                            //kqxl.ItemID = this.ID;
                                            //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                            //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                                            //kqxl.LoaiThongDiep = "Hủy khai báo";
                                            //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\nNgày hủy: {2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), DateTime.Now.ToString())
                                            //    + "\r\n" + FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText); kqxl.Ngay = DateTime.Now;
                                            //kqxl.Insert();

                                            this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                                            this.SoTiepNhan = 0;
                                            this.TrangThaiXuLy = -1;
                                            this.NgayTiepNhan = new DateTime(1900, 1, 1);
                                            this.Update();
                                        }
                                    }
                                    kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoLayThongTinHopDong, kqxl.NoiDung);

                                }
                                else if (nodeDuLieu.Attributes["TrangThai"].Value == "TU_CHOI")
                                {
                                    this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                                    this.ActionStatus = 1;
                                    this.Update();

                                    Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                                    //kqxl.ItemID = this.ID;
                                    //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                    //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                                    //kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                                    //kqxl.NoiDung = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                                    //kqxl.Ngay = DateTime.Now;
                                    //kqxl.Insert();
                                    kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQTuChoiHopDong, kqxl.NoiDung);
                                }
                                #endregion
                            }
                        }
                        else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root") != null
                            && docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "LOI")
                        {
                            XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU/TT_LOI/MO_TA");
                            XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU/TT_LOI/MA_LOI");
                            string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;

                            if (stMucLoi == "XML_LEVEL")
                                errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                            else if (stMucLoi == "DATA_LEVEL")
                                errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                            else if (stMucLoi == "SERVICE_LEVEL")
                                errorSt = "Lỗi do Web service trả về ";
                            else if (stMucLoi == "DOTNET_LEVEL")
                                errorSt = "Lỗi do hệ thống của hải quan ";

                            //Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                            //kqxl.ItemID = this.ID;
                            //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                            //kqxl.LoaiThongDiep = errorSt; //Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                            //kqxl.NoiDung = docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText; // FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                            //kqxl.Ngay = DateTime.Now;
                            //kqxl.Insert();

                            errorSt = errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi;


                            throw new Exception(errorSt);
                        }
                        /*Kiem tra lay phan hoi*/
                        else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
                        {
                            #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                            XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                            Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();

                            if (nodeRoot.SelectSingleNode("PHAN_LUONG") == null) //Neu message chua phan luong -> cap nhat SO TN.
                            {
                                this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                                this.NgayTiepNhan = DateTime.Today;
                                this.NamTN = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);

                                this.ActionStatus = 0;
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                                ok = true;
                                string sfmtMessage = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                                //kqxl.ItemID = this.ID;
                                //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                                //kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                                //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                                //kqxl.Ngay = DateTime.Now;
                                //kqxl.Insert();
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQCapSoTiepNhan, sfmtMessage);
                            }

                            /*Lay thong tin phan luong*/
                            if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                            {
                                XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                                this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                                this.HUONGDAN = FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                                this.ActionStatus = 1;
                                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                                //kqxl.ItemID = this.ID;
                                //kqxl.ReferenceID = new Guid(this.GUIDSTR);
                                //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                                //kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_HopDongDuocDuyet;

                                String tenluong = "Xanh";

                                if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                    tenluong = "Vàng";
                                else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                    tenluong = "Đỏ";

                                //kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp: {1}\r\nHải quan: {2}\r\nPhân luồng: {3}\r\nHướng dẫn: {4}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                                //kqxl.Ngay = DateTime.Now;
                                //kqxl.Insert();
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQDuyet, kqxl.NoiDung);

                            }

                            this.Update();

                            #endregion Lấy số tiếp nhận của danh sách NPL
                        }
                        else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root") != null
                            && docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value.Equals("yes"))
                        {
                            if (docNPL.SelectSingleNode("Envelope/Header/Subject/function").InnerText == "2")
                            {
                                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQHuyHopDong,
                                    FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                                this.SoTiepNhan = 0;
                                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                                this.TrangThaiXuLy = -1;

                                ok = true;
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                }
                if (ok)
                    this.Update();
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(kq))
                    kq.XmlSaveMessage(ID, MessageTitle.Error, ex.Message);
                else
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
            return "";
        }
        #endregion

        public void LoadDataHopDongKTX(long idHopDong, string dbName)
        {
            try
            {
                this.ID = idHopDong;

                //Load du lieu Hop dong
                this.Load(null, dbName);

                //Load Nhom San pham. Nguyen phu lieu, San pham, Thiet bi
                this.LoadCollectionKTX(dbName);

                //Load Bang ke cung ung: Nguyen phu lieu cung ung, San pham cung ung
                Company.GC.BLL.KDT.GC.BKCungUngDangKy bkCU = new Company.GC.BLL.KDT.GC.BKCungUngDangKy();
                BKCUDKCollection = bkCU.SelectBangKeCUByHopDong_KhongLayChuaKhaiBao(idHopDong, dbName);
                //Load san pham cung ung
                foreach (Company.GC.BLL.KDT.GC.BKCungUngDangKy bk in BKCUDKCollection)
                {
                    bk.LoadSanPhamCungUngCollection(dbName);
                    //Load nguyen phu lieu cung ung
                    foreach (Company.GC.BLL.KDT.GC.SanPhanCungUng sp in bk.SanPhamCungUngCollection)
                    {
                        sp.LoadNPLCungUngCollection(dbName);
                    }
                }

                //Load Dinh muc dang ky
                //Company.GC.BLL.KDT.GC.DinhMucDangKy dmuc = new Company.GC.BLL.KDT.GC.DinhMucDangKy();
                //dmuc.ID_HopDong = idHopDong;
                //DMDKCollection = dmuc.SelectCollectionBy_ID_HopDong_ByKTX(dbName);
                ////Load Dinh muc
                //foreach (Company.GC.BLL.KDT.GC.DinhMucDangKy dm in DMDKCollection)
                //{
                //    dm.LoadCollection(dbName);
                //}
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.HopDong_ID = idHopDong;
                Company.GC.BLL.GC.DinhMucCollection DMCollection;
                DMCollection = dm.SelectCollectionBy_HopDong_ID(null, dbName);

                //Load Phu kien dang ky
                Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
                pkdk.HopDong_ID = idHopDong;
                PKDKCollection = pkdk.SelectCollectionBy_HopDong_ID_KTX_KhongLayChuakhaiBao(dbName);
                //Load phu kien chi tiet
                foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy pk in PKDKCollection)
                {
                    pk.LoadCollection(dbName);

                    foreach (LoaiPhuKien loaiPK in pk.PKCollection)
                    {
                        loaiPK.LoadCollection(dbName);
                    }
                }

                //Load To khai mau dich
                Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();
                tk.IDHopDong = idHopDong;
                TKMDCollection = tk.SelectCollectionBy_IDHopDong_KTX_KhongLayChuaKhaiBao(null, dbName);
                //Load Hang mau dich, Chung tu dinh kem cua To khai mau dich
                foreach (Company.GC.BLL.KDT.ToKhaiMauDich tkmd in TKMDCollection)
                {
                    //Hang mau dich
                    tkmd.LoadHMDCollection(dbName);
                }

                //Load To khai chuyen tiep
                Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
                tkct.IDHopDong = idHopDong;
                TKCTCollection = tkct.SelectCollectionBy_IDHopDong_KTX_KhongLayChuaKhaiBao(null, dbName);
                //Load Hang chuyen tiep, Chung tu dinh kem cua To khai chuyen tiep
                foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkctObj in TKCTCollection)
                {
                    //Hang chuyen tiep
                    tkctObj.LoadHCTCollectionKTX(null, dbName);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public void LoadDataHopDong(long idHopDong, string dbName)
        {
            try
            {
                this.ID = idHopDong;

                //Load du lieu Hop dong
                this.LoadTQDT(null, dbName);

                //Load Nhom San pham. Nguyen phu lieu, San pham, Thiet bi
                this.LoadCollection(dbName);

                //Load Bang ke cung ung: Nguyen phu lieu cung ung, San pham cung ung
                Company.GC.BLL.KDT.GC.BKCungUngDangKy bkCU = new Company.GC.BLL.KDT.GC.BKCungUngDangKy();
                BKCUDKCollection = bkCU.SelectBangKeCUByHopDong(idHopDong, dbName);
                //Load san pham cung ung
                foreach (Company.GC.BLL.KDT.GC.BKCungUngDangKy bk in BKCUDKCollection)
                {
                    bk.LoadSanPhamCungUngCollection(dbName);
                    //Load nguyen phu lieu cung ung
                    foreach (Company.GC.BLL.KDT.GC.SanPhanCungUng sp in bk.SanPhamCungUngCollection)
                    {
                        sp.LoadNPLCungUngCollection(dbName);
                    }
                }

                //Load Dinh muc dang ky
                Company.GC.BLL.KDT.GC.DinhMucDangKy dmuc = new Company.GC.BLL.KDT.GC.DinhMucDangKy();
                dmuc.ID_HopDong = idHopDong;
                DMDKCollection = dmuc.SelectCollectionBy_ID_HopDong(dbName);
                //Load Dinh muc
                foreach (Company.GC.BLL.KDT.GC.DinhMucDangKy dm in DMDKCollection)
                {
                    dm.LoadCollection(dbName);
                }

                //Load Phu kien dang ky
                Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new Company.GC.BLL.KDT.GC.PhuKienDangKy();
                pkdk.HopDong_ID = idHopDong;
                PKDKCollection = pkdk.SelectCollectionBy_HopDong_ID(dbName);
                //Load phu kien chi tiet
                foreach (Company.GC.BLL.KDT.GC.PhuKienDangKy pk in PKDKCollection)
                {
                    pk.LoadCollection(dbName);

                    foreach (LoaiPhuKien loaiPK in pk.PKCollection)
                    {
                        loaiPK.LoadCollection(dbName);
                    }
                }

                //Load To khai mau dich
                Company.GC.BLL.KDT.ToKhaiMauDich tk = new Company.GC.BLL.KDT.ToKhaiMauDich();
                tk.IDHopDong = idHopDong;
                TKMDCollection = tk.SelectCollectionBy_IDHopDong(dbName);
                //Load Hang mau dich, Chung tu dinh kem cua To khai mau dich
                foreach (Company.GC.BLL.KDT.ToKhaiMauDich tkmd in TKMDCollection)
                {
                    //Hang mau dich
                    tkmd.LoadHMDCollection(dbName);
                    //Chung tu dinh kem
                    tkmd.LoadChungTuHaiQuan(dbName);
                }

                //Load To khai chuyen tiep
                Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
                tkct.IDHopDong = idHopDong;
                TKCTCollection = tkct.SelectCollectionBy_IDHopDong(dbName);
                //Load Hang chuyen tiep, Chung tu dinh kem cua To khai chuyen tiep
                foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkctObj in TKCTCollection)
                {
                    //Hang chuyen tiep
                    tkctObj.LoadHCTCollection(dbName);
                    //Chung tu dinh kem
                    tkctObj.LoadChungTuHaiQuan(dbName);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #region Đồng bộ dữ liệu

        public string InsertFullFromISyncDaTa()
        {
            string error = string.Empty;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    List<Company.GC.BLL.KDT.GC.HopDong> hdco = Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic("SoHopDong = '" + this.SoHopDong.Trim() + "'", null);
                    if (hdco != null && hdco.Count > 0)
                    {
                        this.ID = hdco[0].ID;
                        this.Update(transaction);
                        transaction.Commit();
                    }
                    else
                    {
                        this.ID = 0;
                        this.ID = this.Insert(transaction);

                        foreach (NhomSanPham nhomsp in NhomSPCollection)
                        {
                            nhomsp.HopDong_ID = this.ID;
                            nhomsp.Insert(transaction);
                        }
                        foreach (SanPham sp in this.SPCollection)
                        {
                            sp.HopDong_ID = this.ID;
                            sp.InsertTransaction(transaction);
                        }
                        foreach (NguyenPhuLieu npl in NPLCollection)
                        {
                            npl.HopDong_ID = this.ID;
                            npl.Insert(transaction);
                        }
                        foreach (ThietBi tb in this.TBCollection)
                        {
                            tb.HopDong_ID = this.ID;
                            tb.Insert(transaction);
                        }
                        foreach (HangMau hm in this.HangMauCollection)
                        {
                            hm.HopDong_ID = this.ID;
                            hm.Insert(transaction);
                        }
                        transaction.Commit();
                        this.InsertUpdateHopDong();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    error = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
                return error;
            }

        }

        #endregion

        /// <summary>
        /// Cap nhat thong tin 'LoaiHangHoa' cua to khai bi NULL
        /// </summary>
        public void CapNhatLoaiHangHoaToKhai()
        {
            try
            {
                string sql = "p_GC_CapNhat_LoaiHangHoa_ToKhai";
                SqlCommand command = (SqlCommand)this.db.GetStoredProcCommand(sql);
                db.ExecuteScalar(command);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }


        public static DataTable CanDoiNPLTuCungUng(long IDHopDong)
        {
            const string spName = "[dbo].[p_KDT_GC_CanDoiNPLCungUng]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 10000;
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, IDHopDong);
            //db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

    }
    public class HopDongEventArgs : EventArgs
    {
        public Exception Error { set; get; }
        public string Status { get; set; }
        public int Percent { get; set; }
        public object Data { get; set; }

        public HopDongEventArgs(Exception ex, string TrangThai, int per, object data)
        {
            this.Error = ex;
            this.Status = TrangThai;
            this.Percent = per;
            this.Data = data;
        }
        public HopDongEventArgs(string TrangThai, int percent)
            : this(null, TrangThai, percent, null)
        {

        }
        public HopDongEventArgs(Exception ex, string TrangThai, int percent)
            : this(ex, TrangThai, percent, null)
        {

        }




    }
}
            #endregion