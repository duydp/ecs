﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using HangChuyenTiep = Company.GC.BLL.KDT.GC.HangChuyenTiep;
namespace Company.Interface
{
    public partial class HopDongFormGCCT : BaseForm
    {

        public bool isKhaiBoSung = false;
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT;
        public Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong HopDongTM = new Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong();
        public HopDongFormGCCT()
        {
            InitializeComponent();

            SetEvent_TextBox_DoiTac();
        }

        private bool checkSoHopDong(string soHopDong)
        {
            foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong HDTM in TKCT.HopDongThuongMaiCollection)
            {
                if (HDTM.SoHopDongTM.Trim().ToUpper() == soHopDong.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HopDongChiTiet> HopDongThuongMaiDetailCollection = new List<HopDongChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongChiTiet hdtmdtmp = new HopDongChiTiet();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongChiTiet hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongChiTiet hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }

        }
        private void BindData()
        {
            //Company.GC.BLL.KDT.GC.HangChuyenTiep hmd = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
            //hmd.ID_TK = TKCT.ID;

            //dgList.DataSource = HopDongTM.ConvertListToDataSet(hmd.SelectBy_TKMD_ID().Tables[0]);

            dgList.DataSource = HopDongTM.ListHangMDOfHopDong;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (HopDongTM.ListHangMDOfHopDong.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }


            TKCT.HopDongThuongMaiCollection.Remove(HopDongTM);

            if (checkSoHopDong(txtSoHopDong.Text))
            {
                if (HopDongTM.ID > 0)
                    TKCT.HopDongThuongMaiCollection.Add(HopDongTM);
                ShowMessage("Số hợp đồng này đã tồn tại.", false);
                return;
            }

            if (!ValidateHopDong())
                return;

            HopDongTM.DiaDiemGiaoHang = txtDiaDiemGiaoHang.Text.Trim();
            HopDongTM.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
            HopDongTM.MaDonViBan = txtMaDVBan.Text.Trim();
            HopDongTM.MaDonViMua = txtMaDVMua.Text.Trim();
            HopDongTM.NgayHopDongTM = ccNgayHopDong.Value;
            HopDongTM.NguyenTe_ID = nguyenTeControl1.Ma;
            HopDongTM.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
            HopDongTM.SoHopDongTM = txtSoHopDong.Text.Trim();
            HopDongTM.TenDonViBan = txtTenDVBan.Text.Trim();
            HopDongTM.TenDonViMua = txtTenDVMua.Text.Trim();
            HopDongTM.ThoiHanThanhToan = ccThoiHanTT.Value;
            HopDongTM.TongTriGia = Convert.ToDecimal(txtTongTriGia.Text);
            HopDongTM.ThongTinKhac = txtThongTinKhac.Text.Trim();
            HopDongTM.ID_TK = TKCT.ID;
            if (isKhaiBoSung)
                HopDongTM.LoaiKB = 1;
            else
                HopDongTM.LoaiKB = 0;
            HopDongTM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            GridEXRow[] rowCollection = dgList.GetRows();
            /*
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HopDongChiTiet item in HopDongTM.ListHangMDOfHopDong)
                {
                    if (item.HMD_ID.ToString().Trim() == rowview["HMD_ID"].ToString().Trim())
                    {
                        item.GhiChu = rowview["GhiChu"].ToString();

                        item.MaHS = rowview["MaHS"].ToString();
                        item.MaPhu = rowview["MaPhu"].ToString();
                        item.TenHang = rowview["TenHang"].ToString();
                        item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
                        item.DVT_ID = rowview["DVT_ID"].ToString();
                        item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
                        item.DonGiaKB = Convert.ToDouble(rowview["DonGiaKB"]);
                        item.TriGiaKB = Convert.ToDouble(rowview["TriGiaKB"]);

                        break;
                    }
                }
            }
            */

            try
            {
                HopDongTM.LoaiTK = "TKCT";
                HopDongTM.InsertUpdateFull();
                TKCT.HopDongThuongMaiCollection.Add(HopDongTM);
                BindData();
                ShowMessage("Lưu thành công.", false);
                //if (TKCT.ActionStatus != TrangThaiXuLy.SUATKDADUYET)
                //    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }


        }
        public List<HangChuyenTiep> ConvertListToHangMauDichCollection(List<HopDongChiTiet> listHangHopDong, List<HangChuyenTiep> hangCollection)
        {

            List<HangChuyenTiep> tmp = new List<HangChuyenTiep>();

            foreach (HopDongChiTiet item in listHangHopDong)
            {
                foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichFormGCCT f = new SelectHangMauDichFormGCCT();
            f.TKCT = TKCT;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất HMD
            //if (HopDongTM.ListHangMDOfHopDong.Count > 0)
            //{
            //    f.TKCT.HMDCollection = ConvertListToHangMauDichCollection(HopDongTM.ListHangMDOfHopDong, TKCT.HMDCollection);
            //}
            f.ShowDialog();
            if (f.HCTCollection.Count > 0)
            {
                foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HMD in f.HCTCollection)
                {
                    bool ok = false;
                    foreach (HopDongChiTiet hangHopDongDetail in HopDongTM.ListHangMDOfHopDong)
                    {
                        if (hangHopDongDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HopDongChiTiet hopDongDetail = new HopDongChiTiet();
                        hopDongDetail.HMD_ID = HMD.ID;
                        hopDongDetail.HopDongTM_ID = HopDongTM.ID;
                        hopDongDetail.MaPhu = HMD.MaHang;
                        hopDongDetail.MaHS = HMD.MaHS;
                        hopDongDetail.TenHang = HMD.TenHang;
                        hopDongDetail.DVT_ID = HMD.ID_DVT;
                        hopDongDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hopDongDetail.SoLuong = HMD.SoLuong;
                        hopDongDetail.NuocXX_ID = HMD.ID_NuocXX;
                        hopDongDetail.DonGiaKB = Convert.ToDouble(HMD.DonGia);
                        hopDongDetail.TriGiaKB = Convert.ToDouble(HMD.TriGia);
                        HopDongTM.ListHangMDOfHopDong.Add(hopDongDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void HopDongForm_Load(object sender, EventArgs e)
        {
            dgList.RootTable.Columns["MaPhu"].Selectable = false;
            if (HopDongTM == null)
                HopDongTM = new Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO };
            

            // an cac button truoc khi load
            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;

            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.DisplayMember = cbDKGH.ValueMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            if (HopDongTM != null && HopDongTM.ID > 0)
            {
                txtDiaDiemGiaoHang.Text = HopDongTM.DiaDiemGiaoHang;
                cbDKGH.SelectedValue = HopDongTM.DKGH_ID;
                txtMaDVBan.Text = HopDongTM.MaDonViBan;
                txtMaDVMua.Text = HopDongTM.MaDonViMua;
                ccNgayHopDong.Value = HopDongTM.NgayHopDongTM;
                nguyenTeControl1.Ma = HopDongTM.NguyenTe_ID;
                cbPTTT.SelectedValue = HopDongTM.PTTT_ID;
                txtSoHopDong.Text = HopDongTM.SoHopDongTM;
                txtTenDVBan.Text = HopDongTM.TenDonViBan;
                txtTenDVMua.Text = HopDongTM.TenDonViMua;
                ccThoiHanTT.Value = HopDongTM.ThoiHanThanhToan;
                txtTongTriGia.Text = HopDongTM.TongTriGia.ToString();
                txtThongTinKhac.Text = HopDongTM.ThongTinKhac;
                ccNgayHopDong.Text = HopDongTM.NgayHopDongTM.ToShortDateString();
                ccThoiHanTT.Text = HopDongTM.ThoiHanThanhToan.ToShortDateString();
                BindData();
            }
            if (HopDongTM.SoTiepNhan > 0)
            {
                txtSoTiepNhan.Text = HopDongTM.SoTiepNhan + "";
                ccNgayTiepNhan.Text = HopDongTM.NgayTiepNhan.ToShortDateString();
            }
            if (TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                //btnChonHang.Visible = false;
                //btnGhi.Visible = false;
                //btnXoa.Visible = false;
            }

            //Set ValueList
            Globals.FillHSValueList(this.dgList.RootTable.Columns["MaHS"]);
            Globals.FillDonViTinhValueList(this.dgList.RootTable.Columns["DVT_ID"]);
            Globals.FillNuocXXValueList(this.dgList.RootTable.Columns["NuocXX_ID"]);

            //HUNGTQ, Update 07/06/2010. 
            //Thiết lập trạng thái các nút trên form
            SetButtonStateHOPDONG(TKCT, isKhaiBoSung, HopDongTM);

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (HopDongTM.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            try
            {
                if ((HopDongTM.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO) || (HopDongTM.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET))
                    HopDongTM.GuidStr = Guid.NewGuid().ToString();
                else if (HopDongTM.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    return;
                }

                ObjectSend msgSend = SingleMessage.BoSungChungTuGCCT(TKCT, HopDongTM, GlobalSettings.TEN_DON_VI);

                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);

                if (isSend)
                {
                    sendMessageForm.Message.XmlSaveMessage(HopDongTM.ID, MessageTitle.KhaiBaoBoSungHopDong);
                    HopDongTM.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    HopDongTM.Update();
                    SetButtonStateHOPDONG(TKCT, isKhaiBoSung, HopDongTM);
                    btnLayPhanHoi.Enabled = true;
                    btnKhaiBao.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            ObjectSend msgSend = SingleMessage.FeedBackGCCT(TKCT, HopDongTM.GuidStr);
            SendMessageForm sendMessageForm = new SendMessageForm();
            sendMessageForm.Send += SendMessage;
            sendMessageForm.DoSend(msgSend);
        }


        #region Begin Doi tac TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Mã đơn vị mua, bán.
        /// </summary>
        /// Hungtq, Update 30052010
        private void SetEvent_TextBox_DoiTac()
        {
            txtMaDVMua.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtMaDVBan.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;

            txtMaDVMua.ButtonClick += new EventHandler(txtMaDVMua_ButtonClick);

            txtMaDVBan.ButtonClick += new EventHandler(txtMaDVBan_ButtonClick);

            txtMaDVMua.Leave += new EventHandler(txtMaDVMua_Leave);

            txtMaDVBan.Leave += new EventHandler(txtMaDVBan_Leave);
        }

        private void txtMaDVMua_ButtonClick(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaDVMua.Text = objDoiTac.MaCongTy;
                txtTenDVMua.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVBan_ButtonClick(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null)
            {
                txtMaDVBan.Text = objDoiTac.MaCongTy;
                txtTenDVBan.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVMua_Leave(object sender, EventArgs e)
        {
            if (txtMaDVMua.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVMua.Text.Trim()) != "")
                txtTenDVMua.Text = DoiTac.GetName(txtMaDVMua.Text.Trim());
        }

        private void txtMaDVBan_Leave(object sender, EventArgs e)
        {
            if (txtMaDVBan.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVBan.Text.Trim()) != "")
                txtTenDVBan.Text = DoiTac.GetName(txtMaDVBan.Text.Trim());
        }

        #endregion End Doi tac TextBox

        #region Begin VALIDATE HOP DONG

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateHopDong()
        {
            bool isValid = true;

            //So_CT	varchar(50)
            isValid = Globals.ValidateLength(txtSoHopDong, 50, err, "Số hợp đồng");

            //Ma_PTTT	varchar(10)
            isValid &= Globals.ValidateLength(cbPTTT, 10, err, "Phương thức thanh toán");

            //Ma_GH	varchar(7)
            isValid &= Globals.ValidateLength(cbDKGH, 7, err, "Điều kiện giao hàng");

            //Ma_NT	char(3)

            //Ma_DV	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVMua, 14, err, "Mã đơn vị mua");

            //Ma_DV_DT	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVBan, 14, err, "Mã đơn vị bán");

            return isValid;
        }

        #endregion End VALIDATE HOP DONG

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOP DONG.
        /// </summary>
        /// <param name="tkct"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOPDONG(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hopdong)
        {
            if (hopdong == null)
                return false;

            if (hopdong != null)
            {
                txtSoHopDong.Text = hopdong.SoHopDongTM;
            }
            switch (hopdong.TrangThai)
            {
                case -1:
                    lbTrangThai.Text = "Chưa Khai Báo";
                    break;
                case -3:
                    lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                    break;
                case 0:
                    lbTrangThai.Text = "Chờ Duyệt";
                    break;
                case 1:
                    lbTrangThai.Text = "Đã Duyệt";
                    break;
                case 2:
                    lbTrangThai.Text = "Hải quan không phê duyệt";
                    break;

            }
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else if (tkct.SoToKhai > 0 && tkct.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {

                bool khaiBaoEnable = (hopdong.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     hopdong.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnXoa.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = hopdong.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   hopdong.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   hopdong.TrangThai == TrangThaiXuLy.CHO_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
            txtSoTiepNhan.Text = hopdong.SoTiepNhan > 0 ? hopdong.SoTiepNhan.ToString() : string.Empty;
            if (hopdong.NgayTiepNhan.Year > 1900) ccNgayTiepNhan.Value = hopdong.NgayTiepNhan;
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (HopDongTM.GuidStr != null && HopDongTM.GuidStr != "")
            {
                ThongDiepForm f = new ThongDiepForm();
                f.DeclarationIssuer = AdditionalDocumentType.HOP_DONG;
                f.ItemID = HopDongTM.ID;
                f.ShowDialog();
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }
        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<HopDongChiTiet> HopDongThuongMaiDetailCollection = new List<HopDongChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongChiTiet hdtmdtmp = new HopDongChiTiet();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongChiTiet hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongChiTiet hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        #region  V3

        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(HopDongTM.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                HopDongTM.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                this.ShowMessageTQDT("Hải quan từ chối tiếp nhận", noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            noidung = noidung.Replace(string.Format("Message [{0}]", HopDongTM.GuidStr), string.Empty);
                            this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                HopDongTM.SoTiepNhan = long.Parse(vals[0].Trim());
                                HopDongTM.NamTiepNhan = int.Parse(vals[1].Trim());
                                HopDongTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(HopDongTM.ID, MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan, noidung);
                                HopDongTM.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                txtSoTiepNhan.Text = HopDongTM.SoTiepNhan.ToString();
                                ccNgayTiepNhan.Value = HopDongTM.NgayTiepNhan;
                                this.ShowMessageTQDT("Được cấp số tiếp nhận.\n" + HopDongTM.SoTiepNhan.ToString() + "\n" + HopDongTM.NgayTiepNhan.ToString(), false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HopDongTM.ID, MessageTitle.KhaiBaoBoSungHopDongThanhCong, noidung);
                                HopDongTM.TrangThai = TrangThaiXuLy.DA_DUYET;
                                ShowMessageTQDT("Chứng từ bổ sung đã được duyệt", false);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HopDongTM.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }
                    if (isUpdate)
                    {
                        HopDongTM.Update();
                        SetButtonStateHOPDONG(TKCT, isKhaiBoSung, HopDongTM);
                    }
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion

    }
}

