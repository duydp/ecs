using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.BLL.SXXK
{
    public partial class PhanBoToKhaiXuat
    {
        #region Private members.

        protected long _ID;
        protected int _SoToKhaiXuat;
        protected string _MaLoaiHinhXuat = string.Empty;
        protected string _MaHaiQuanXuat = string.Empty;
        protected short _NamDangKyXuat;
        protected string _MaSP = string.Empty;
        protected decimal _SoLuongXuat;
        protected string _MaDoanhNghiep = string.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public int SoToKhaiXuat
        {
            set { this._SoToKhaiXuat = value; }
            get { return this._SoToKhaiXuat; }
        }

        public string MaLoaiHinhXuat
        {
            set { this._MaLoaiHinhXuat = value; }
            get { return this._MaLoaiHinhXuat; }
        }

        public string MaHaiQuanXuat
        {
            set { this._MaHaiQuanXuat = value.Trim(); }
            get { return this._MaHaiQuanXuat.Trim(); }
        }

        public short NamDangKyXuat
        {
            set { this._NamDangKyXuat = value; }
            get { return this._NamDangKyXuat; }
        }

        public string MaSP
        {
            set { this._MaSP = value; }
            get { return this._MaSP; }
        }

        public decimal SoLuongXuat
        {
            set { this._SoLuongXuat = value; }
            get { return this._SoLuongXuat; }
        }

        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value.Trim(); }
            get { return this._MaDoanhNghiep.Trim(); }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static PhanBoToKhaiXuat Load(long iD)
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            PhanBoToKhaiXuat entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new PhanBoToKhaiXuat();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<PhanBoToKhaiXuat> SelectCollectionAll()
        {
            IList<PhanBoToKhaiXuat> collection = new List<PhanBoToKhaiXuat>();
            IDataReader reader = SelectReaderAll();
            while (reader.Read())
            {
                PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public static IList<PhanBoToKhaiXuat> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IList<PhanBoToKhaiXuat> collection = new List<PhanBoToKhaiXuat>();

            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<PhanBoToKhaiXuat> SelectCollectionBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat(string maHaiQuanXuat, int soToKhaiXuat, string maLoaiHinhXuat, short namDangKyXuat)
        {
            IList<PhanBoToKhaiXuat> collection = new List<PhanBoToKhaiXuat>();
            IDataReader reader = SelectReaderBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat(maHaiQuanXuat, soToKhaiXuat, maLoaiHinhXuat, namDangKyXuat);
            while (reader.Read())
            {
                PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanXuat"))) entity.MaHaiQuanXuat = reader.GetString(reader.GetOrdinal("MaHaiQuanXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyXuat"))) entity.NamDangKyXuat = reader.GetInt16(reader.GetOrdinal("NamDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongXuat"))) entity.SoLuongXuat = reader.GetDecimal(reader.GetOrdinal("SoLuongXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat(string maHaiQuanXuat, int soToKhaiXuat, string maLoaiHinhXuat, short namDangKyXuat)
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_SelectBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, maHaiQuanXuat);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, soToKhaiXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, maLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, namDangKyXuat);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat(string maHaiQuanXuat, int soToKhaiXuat, string maLoaiHinhXuat, short namDangKyXuat)
        {
            const string spName = "p_SXXK_PhanBoToKhaiXuat_SelectBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, maHaiQuanXuat);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, soToKhaiXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, maLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, namDangKyXuat);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertPhanBoToKhaiXuat(int soToKhaiXuat, string maLoaiHinhXuat, string maHaiQuanXuat, short namDangKyXuat, string maSP, decimal soLuongXuat, string maDoanhNghiep)
        {
            PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();
            entity.SoToKhaiXuat = soToKhaiXuat;
            entity.MaLoaiHinhXuat = maLoaiHinhXuat;
            entity.MaHaiQuanXuat = maHaiQuanXuat;
            entity.NamDangKyXuat = namDangKyXuat;
            entity.MaSP = maSP;
            entity.SoLuongXuat = soLuongXuat;
            entity.MaDoanhNghiep = maDoanhNghiep;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, MaHaiQuanXuat);
            db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, NamDangKyXuat);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
            db.AddInParameter(dbCommand, "@SoLuongXuat", SqlDbType.Decimal, SoLuongXuat);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<PhanBoToKhaiXuat> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiXuat item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdatePhanBoToKhaiXuat(long iD, int soToKhaiXuat, string maLoaiHinhXuat, string maHaiQuanXuat, short namDangKyXuat, string maSP, decimal soLuongXuat, string maDoanhNghiep)
        {
            PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();
            entity.SoToKhaiXuat = soToKhaiXuat;
            entity.MaLoaiHinhXuat = maLoaiHinhXuat;
            entity.MaHaiQuanXuat = maHaiQuanXuat;
            entity.NamDangKyXuat = namDangKyXuat;
            entity.MaSP = maSP;
            entity.SoLuongXuat = soLuongXuat;
            entity.MaDoanhNghiep = maDoanhNghiep;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_SXXK_PhanBoToKhaiXuat_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, MaHaiQuanXuat);
            db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, NamDangKyXuat);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
            db.AddInParameter(dbCommand, "@SoLuongXuat", SqlDbType.Decimal, SoLuongXuat);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<PhanBoToKhaiXuat> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiXuat item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdatePhanBoToKhaiXuat(long iD, int soToKhaiXuat, string maLoaiHinhXuat, string maHaiQuanXuat, short namDangKyXuat, string maSP, decimal soLuongXuat, string maDoanhNghiep)
        {
            PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();
            entity.ID = iD;
            entity.SoToKhaiXuat = soToKhaiXuat;
            entity.MaLoaiHinhXuat = maLoaiHinhXuat;
            entity.MaHaiQuanXuat = maHaiQuanXuat;
            entity.NamDangKyXuat = namDangKyXuat;
            entity.MaSP = maSP;
            entity.SoLuongXuat = soLuongXuat;
            entity.MaDoanhNghiep = maDoanhNghiep;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, MaHaiQuanXuat);
            db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, NamDangKyXuat);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
            db.AddInParameter(dbCommand, "@SoLuongXuat", SqlDbType.Decimal, SoLuongXuat);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<PhanBoToKhaiXuat> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiXuat item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeletePhanBoToKhaiXuat(long iD)
        {
            PhanBoToKhaiXuat entity = new PhanBoToKhaiXuat();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat(string maHaiQuanXuat, int soToKhaiXuat, string maLoaiHinhXuat, short namDangKyXuat)
        {
            string spName = "[dbo].[p_SXXK_PhanBoToKhaiXuat_DeleteBy_MaHaiQuanXuat_AND_SoToKhaiXuat_AND_MaLoaiHinhXuat_AND_NamDangKyXuat]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuanXuat", SqlDbType.Char, maHaiQuanXuat);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, soToKhaiXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, maLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@NamDangKyXuat", SqlDbType.SmallInt, namDangKyXuat);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<PhanBoToKhaiXuat> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (PhanBoToKhaiXuat item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}