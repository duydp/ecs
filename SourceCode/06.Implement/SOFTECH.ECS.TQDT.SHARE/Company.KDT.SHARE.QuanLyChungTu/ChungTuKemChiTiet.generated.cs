using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class ChungTuKemChiTiet
	{
		#region Private members.
		
		protected long _ID;
		protected long _ChungTuKemID;
		protected string _FileName = string.Empty;
		protected decimal _FileSize;
		protected byte[] _NoiDung;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		
		public long ChungTuKemID
		{
			set {this._ChungTuKemID = value;}
			get {return this._ChungTuKemID;}
		}
		
		public string FileName
		{
			set {this._FileName = value;}
			get {return this._FileName;}
		}
		
		public decimal FileSize
		{
			set {this._FileSize = value;}
			get {return this._FileSize;}
		}
		
		public byte[] NoiDung
		{
			set {this._NoiDung = value;}
			get {return this._NoiDung;}
		}
		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ChungTuKemChiTiet Load(long iD)
		{
			const string spName = "[dbo].[p_ChungTuKemChiTiet_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
			ChungTuKemChiTiet entity = null;
            SqlDataReader reader = (SqlDataReader) db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new ChungTuKemChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTuKemID"))) entity.ChungTuKemID = reader.GetInt64(reader.GetOrdinal("ChungTuKemID"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileName"))) entity.FileName = reader.GetString(reader.GetOrdinal("FileName"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileSize"))) entity.FileSize = reader.GetDecimal(reader.GetOrdinal("FileSize"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<ChungTuKemChiTiet> SelectCollectionAll()
		{
			List<ChungTuKemChiTiet> collection = new List<ChungTuKemChiTiet>();
			SqlDataReader reader = SelectReaderAll();
			while (reader.Read())
			{
				ChungTuKemChiTiet entity = new ChungTuKemChiTiet();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTuKemID"))) entity.ChungTuKemID = reader.GetInt64(reader.GetOrdinal("ChungTuKemID"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileName"))) entity.FileName = reader.GetString(reader.GetOrdinal("FileName"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileSize"))) entity.FileSize = reader.GetDecimal(reader.GetOrdinal("FileSize"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<ChungTuKemChiTiet> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<ChungTuKemChiTiet> collection = new List<ChungTuKemChiTiet>();

			SqlDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ChungTuKemChiTiet entity = new ChungTuKemChiTiet();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTuKemID"))) entity.ChungTuKemID = reader.GetInt64(reader.GetOrdinal("ChungTuKemID"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileName"))) entity.FileName = reader.GetString(reader.GetOrdinal("FileName"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileSize"))) entity.FileSize = reader.GetDecimal(reader.GetOrdinal("FileSize"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<ChungTuKemChiTiet> SelectCollectionBy_ChungTuKemID(long chungTuKemID)
		{
			List<ChungTuKemChiTiet> collection = new List<ChungTuKemChiTiet>();
            SqlDataReader reader = SelectReaderBy_ChungTuKemID(chungTuKemID);
			while (reader.Read())
			{
				ChungTuKemChiTiet entity = new ChungTuKemChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChungTuKemID"))) entity.ChungTuKemID = reader.GetInt64(reader.GetOrdinal("ChungTuKemID"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileName"))) entity.FileName = reader.GetString(reader.GetOrdinal("FileName"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileSize"))) entity.FileSize = reader.GetDecimal(reader.GetOrdinal("FileSize"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ChungTuKemID(long chungTuKemID)
		{
			const string spName = "[dbo].[p_ChungTuKemChiTiet_SelectBy_ChungTuKemID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, chungTuKemID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_ChungTuKemChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_ChungTuKemChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static SqlDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_ChungTuKemChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return (SqlDataReader) db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static SqlDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_ChungTuKemChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return (SqlDataReader) db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static SqlDataReader SelectReaderBy_ChungTuKemID(long chungTuKemID)
		{
			const string spName = "p_ChungTuKemChiTiet_SelectBy_ChungTuKemID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, chungTuKemID);
			
            return (SqlDataReader) db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertChungTuKemChiTiet(long chungTuKemID, string fileName, decimal fileSize, byte[] noiDung)
		{
			ChungTuKemChiTiet entity = new ChungTuKemChiTiet();	
			entity.ChungTuKemID = chungTuKemID;
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.NoiDung = noiDung;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_ChungTuKemChiTiet_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, ChungTuKemID);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.VarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ChungTuKemChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuKemChiTiet item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateChungTuKemChiTiet(long iD, long chungTuKemID, string fileName, decimal fileSize, byte[] noiDung)
		{
			ChungTuKemChiTiet entity = new ChungTuKemChiTiet();			
			entity.ChungTuKemID = chungTuKemID;
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.NoiDung = noiDung;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_ChungTuKemChiTiet_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, ChungTuKemID);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.VarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ChungTuKemChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuKemChiTiet item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateChungTuKemChiTiet(long iD, long chungTuKemID, string fileName, decimal fileSize, byte[] noiDung)
		{
			ChungTuKemChiTiet entity = new ChungTuKemChiTiet();			
			entity.ID = iD;
			entity.ChungTuKemID = chungTuKemID;
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.NoiDung = noiDung;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_ChungTuKemChiTiet_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, ChungTuKemID);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.VarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ChungTuKemChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuKemChiTiet item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteChungTuKemChiTiet(long iD)
		{
			ChungTuKemChiTiet entity = new ChungTuKemChiTiet();
			entity.ID = iD;
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_ChungTuKemChiTiet_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ChungTuKemID(long chungTuKemID)
		{
			string spName = "[dbo].[p_ChungTuKemChiTiet_DeleteBy_ChungTuKemID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ChungTuKemID", SqlDbType.BigInt, chungTuKemID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ChungTuKemChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuKemChiTiet item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}