﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.SXXK.ToKhai;

namespace Company.Interface.SXXK
{
    public partial class CapNhatChuHangForm : BaseForm
    {
        public CapNhatChuHangForm()
        {
            InitializeComponent();
        }
        Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
        private void search()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                // Xây dựng điều kiện tìm kiếm.
                string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);
                where += " AND MaLoaiHinh LIKE '" + cmbLoaiToKhai.SelectedValue.ToString() + "%'";
                if (txtSoTokhai.TextLength == 0)
                {
                    
                }else if (txtSoTokhai.TextLength < 5)
                {
                    where += " AND SoToKhai = " + txtSoTokhai.Text.Trim();
                }
                if (txtSoTokhai.TextLength >= 5)
                {
                    where += " AND LoaiVanDon LIKE '%" + txtSoTokhai.Text.Trim()+"%'";
                }
                if (txtNamDangKy.TextLength > 0)
                {
                    where += " AND NamDangKy = " + txtNamDangKy.Text.Trim();
                }
             
                this.tkmdCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich().SelectCollectionDynamic(where, "");
                gridEX1.DataSource = this.tkmdCollection;
                gridEX1.Refetch();
            }
            catch (Exception ex) { ShowMessage(ex.Message, false); Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void CapNhatChuHangForm_Load(object sender, EventArgs e)
        {
            cmbLoaiToKhai.SelectedIndex = 0;
            txtNamDangKy.Text = DateTime.Today.Year.ToString();
            txtNamDangKy.TextAlignment = TextAlignment.Far;
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            this.search();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtTenChuHang.Text.Trim()))
                {
                    ShowMessage("Chưa nhập thông tin tên chủ hàng", false);
                    return;
                }
                GridEXRow[] collection = gridEX1.GetCheckedRows();
                if (collection.Length <= 0)
                {
                    ShowMessage("Chưa chọn tờ khai", false);
                    return;
                }
                ToKhaiMauDichCollection listUpdate = new ToKhaiMauDichCollection();
                foreach (GridEXRow item in collection)
                {
                    ToKhaiMauDich tkmd = (ToKhaiMauDich)item.DataRow;
                    tkmd.TenChuHang = txtTenChuHang.Text.Trim();
                    listUpdate.Add(tkmd);
                }
                new ToKhaiMauDich().UpdateCollection(listUpdate);
                search();
            }

            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lưu dữ liệu thất bại. " + ex.Message, false);
            }
        }
    }
}
