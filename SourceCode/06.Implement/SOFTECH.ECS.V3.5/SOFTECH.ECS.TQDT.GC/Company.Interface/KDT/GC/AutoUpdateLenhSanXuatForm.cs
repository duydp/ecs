﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.GC
{
    public partial class AutoUpdateLenhSanXuatForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_LenhSanXuat_QuyTac Config = new KDT_LenhSanXuat_QuyTac();
        public HopDong HD = new HopDong();

        public KDT_LenhSanXuat lenhSanXuat = new KDT_LenhSanXuat();
        public KDT_LenhSanXuat_SP lenhSanXuatSP = new KDT_LenhSanXuat_SP();

        public List<GC_DinhMuc> DinhMucCollection = new List<GC_DinhMuc>();
        public List<KDT_GC_DinhMuc> SPCollection = new List<KDT_GC_DinhMuc>();
        public List<KDT_GC_DinhMuc> LenhSXCollection = new List<KDT_GC_DinhMuc>();
        public AutoUpdateLenhSanXuatForm()
        {
            InitializeComponent();
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            QuyTacTaoLenhSanXuatForm f = new QuyTacTaoLenhSanXuatForm();
            f.Show(this);
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = Company.GC.BLL.Globals.GetDanhSachHopDongDaDangKy(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "", 0);
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void AutoUpdateLenhSanXuatForm_Load(object sender, EventArgs e)
        {
            cbbSoPO.SelectedValue = "0";
            cbbTinhTrang.SelectedValue = "2";
            BindData();
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private string GetConfigAuto(String TinhTrang)
        {
            try
            {
                String LenhSanXuat = "";
                String FirstStringLenhSanXuat = "";
                String LastStringLenhSanXuat = "";
                int lenghtFirst;
                int lenghtLast;
                int lenghtIndex;
                List<KDT_LenhSanXuat_QuyTac> ConfigCollection = KDT_LenhSanXuat_QuyTac.SelectCollectionBy_HopDong_ID(HD.ID);

                foreach (KDT_LenhSanXuat_QuyTac item in ConfigCollection)
                {
                    string Value = "";
                    switch (TinhTrang)
                    {
                        case "0":
                            Value = "M";
                            break;
                        case "1":
                            Value = "Đ";
                            break;
                        case "2":
                            Value = "H";
                            break;
                        default:
                            break;
                    }
                    if (item.TinhTrang == Value)
                    {
                        lenghtFirst = item.TienTo.Length + item.TinhTrang.Length + item.LoaiHinh.Length + Convert.ToInt32(item.DoDaiSo) - 1;
                        lenghtLast = item.HienThi.Length - lenghtFirst;
                        FirstStringLenhSanXuat = item.HienThi.Substring(0, lenghtFirst - 1);
                        LastStringLenhSanXuat = item.HienThi.Substring(lenghtFirst, lenghtLast);
                        string GetLenhSanXuat = KDT_LenhSanXuat_QuyTac.GetLenhSanXuat(TinhTrang);
                        if (!String.IsNullOrEmpty(GetLenhSanXuat))
                        {
                            if (GetLenhSanXuat.Length >= item.HienThi.Length)
                            {
                                if (GetLenhSanXuat.Substring(0, lenghtFirst - 1) != FirstStringLenhSanXuat)
                                {
                                    LenhSanXuat = item.HienThi;
                                }
                                else
                                {
                                    string[] Temp = GetLenhSanXuat.Split(new string[] { "-" }, StringSplitOptions.None);
                                    string First = Temp[0].ToString();
                                    int LastIndex = Convert.ToInt32(First.Substring(lenghtFirst - 1, First.Length - (lenghtFirst - 1)));
                                    LenhSanXuat = FirstStringLenhSanXuat + (LastIndex + 1) + LastStringLenhSanXuat;
                                }
                            }
                            else
                            {
                                LenhSanXuat = item.HienThi;
                            }
                        }
                        else
                        {
                            LenhSanXuat = item.HienThi;
                        }
                    }
                }
                return LenhSanXuat;
            }
            catch (Exception ex)
            {
                return "";
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HD = new HopDong();
                        HD = (HopDong)i.GetRow().DataRow;
                    }
                }
                txtSoHopDong.Text = HD.SoHopDong.ToString();
                clcNgayHĐ.Value = HD.NgayKy;
                clcNgayHH.Value = HD.NgayGiaHan.Year == 1 || HD.NgayGiaHan.Year == 1900 ? HD.NgayHetHan : HD.NgayGiaHan;
                txtMaDDSX.Text = GetConfigAuto(cbbTinhTrang.SelectedValue.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(cbbSoPO, errorProvider, "Số đơn hàng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "Tình trạng", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void bttAuto_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            bttAuto.Enabled = false;
            btnClose.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void DoWork(object obj)
        {
            try
            {
                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow itemSelect = listChecked[i];
                    HD = (HopDong)itemSelect.DataRow;
                    HD.LoadCollection();                   
                    try
                    {
                        bool isExits = false;
                        KDT_GC_DinhMucThucTeDangKy dinhMucThucTeDangKy = new KDT_GC_DinhMucThucTeDangKy();
                        KDT_GC_DinhMucDangKy DMDK = new KDT_GC_DinhMucDangKy();
                        //Cập nhật lệnh sản xuất cho những định mức đã nhập liệu có lệnh sản xuất trước đây nhưng chưa có thông tin chi tiết về lệnh
                        LenhSXCollection = KDT_GC_DinhMuc.SelectDistinctCollectionDynamicLSX(" Master_ID IN (SELECT ID FROM dbo.t_KDT_GC_DinhMucDangKy WHERE ID_HopDong = " + HD.ID + " AND TrangThaiXuLy IN (0,1)) AND MaDDSX NOT IN (SELECT LSX.SoLenhSanXuat FROM dbo.t_KDT_LenhSanXuat LSX INNER JOIN dbo.t_KDT_LenhSanXuat_SP SP ON SP.LenhSanXuat_ID = LSX.ID  WHERE  LSX.HopDong_ID = " + HD.ID + ") AND MaDDSX IS NOT NULL", "");
                        foreach (KDT_GC_DinhMuc lenh in LenhSXCollection)
                        {
                            lenhSanXuat = new KDT_LenhSanXuat();
                            lenhSanXuat.HopDong_ID = HD.ID;
                            lenhSanXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            lenhSanXuat.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                            lenhSanXuat.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            lenhSanXuat.SoLenhSanXuat = lenh.MaDDSX.ToString();//GetConfigAuto(cbbTinhTrang.SelectedValue.ToString());
                            lenhSanXuat.TuNgay = new DateTime (HD.NgayKy.Year,HD.NgayKy.Month,HD.NgayKy.Day,00,00,00);
                            lenhSanXuat.DenNgay = HD.NgayGiaHan.Year == 1 || HD.NgayGiaHan.Year == 1900 ? new DateTime(HD.NgayHetHan.Year, HD.NgayHetHan.Month, HD.NgayHetHan.Day, 23, 59, 59) : new DateTime(HD.NgayGiaHan.Year, HD.NgayGiaHan.Month, HD.NgayGiaHan.Day, 23, 59, 59);
                            lenhSanXuat.SoDonHang = cbbSoPO.SelectedValue == "0" ? HD.DonViDoiTac : HD.TenDonViDoiTac;
                            lenhSanXuat.TinhTrang = Convert.ToInt32(cbbTinhTrang.SelectedValue.ToString());
                            lenhSanXuat.GhiChu = txtGhiChu.Text.ToString();
                            SPCollection = KDT_GC_DinhMuc.SelectDistinctCollectionDynamicSP(" Master_ID = '" + lenh.Master_ID + "'", "");
                            foreach (KDT_GC_DinhMuc items in SPCollection)
                            {
                                lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                                lenhSanXuatSP.MaSanPham = items.MaSanPham;
                                lenhSanXuatSP.TenSanPham = items.TenSanPham;
                                lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                            }
                            if (lenhSanXuat.SPCollection.Count >= 1)
                            {
                                List<KDT_LenhSanXuat> LenhSanXuatCollection = new List<KDT_LenhSanXuat>();
                                LenhSanXuatCollection = KDT_LenhSanXuat.SelectCollectionDynamic("HopDong_ID = " + HD.ID + "", "");

                                KDT_LenhSanXuat LenhSanXuatFind = new KDT_LenhSanXuat();
                                LenhSanXuatFind = LenhSanXuatCollection.Find(items => items.SoLenhSanXuat == lenh.MaDDSX.ToString());
                                if (!String.IsNullOrEmpty(LenhSanXuatFind.SoLenhSanXuat))
                                {
                                    #region KIỂM TRA NẾU ĐÃ CÓ LỆNH NÀY THÌ UPDATE
                                    LenhSanXuatFind.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(LenhSanXuatFind.ID);
                                    foreach (KDT_GC_DinhMuc item in SPCollection)
                                    {
                                        //Cập nhật lệnh sản xuất 
                                        lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                                        lenhSanXuatSP.MaSanPham = item.MaSanPham;
                                        lenhSanXuatSP.TenSanPham = item.TenSanPham;
                                        foreach (Company.GC.BLL.KDT.GC.SanPham sp in HD.SPCollection)
                                        {
                                            isExits = false;
                                            if (lenhSanXuatSP.MaSanPham == sp.Ma)
                                            {
                                                lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                                                lenhSanXuatSP.MaHS = sp.MaHS;
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            foreach (GC_SanPham sp in GC_SanPham.SelectCollectionBy_HopDong_ID(HD.ID))
                                            {
                                                isExits = false;
                                                if (lenhSanXuatSP.MaSanPham == sp.Ma)
                                                {
                                                    lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                                                    lenhSanXuatSP.MaHS = sp.MaHS;
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                        LenhSanXuatFind.SPCollection.Add(lenhSanXuatSP);
                                        //Cập nhật vào định mức thực tế theo từng giai đoạn
                                        if (DMDK.ID==0)
                                        {
                                            DMDK = new KDT_GC_DinhMucDangKy();
                                            DMDK = KDT_GC_DinhMucDangKy.Load(item.Master_ID);

                                            dinhMucThucTeDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                            dinhMucThucTeDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                            dinhMucThucTeDangKy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                                            dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSanXuat.ID;
                                            dinhMucThucTeDangKy.HopDong_ID = HD.ID;
                                            dinhMucThucTeDangKy.SoTiepNhan = DMDK.SoTiepNhan;
                                            dinhMucThucTeDangKy.NgayTiepNhan = DMDK.NgayTiepNhan;
                                            dinhMucThucTeDangKy.TrangThaiXuLy = DMDK.TrangThaiXuLy;
                                            dinhMucThucTeDangKy.GuidString = String.IsNullOrEmpty(DMDK.GUIDSTR) ? Guid.NewGuid().ToString().ToUpper() : DMDK.GUIDSTR;
                                        }

                                        KDT_GC_DinhMucThucTe_SP DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
                                        DinhMucThucTeSP.MaSanPham = lenhSanXuatSP.MaSanPham;
                                        DinhMucThucTeSP.TenSanPham = lenhSanXuatSP.TenSanPham;
                                        DinhMucThucTeSP.DVT_ID = lenhSanXuatSP.DVT_ID;
                                        DinhMucThucTeSP.MaHS = lenhSanXuatSP.MaHS;

                                        List<KDT_GC_DinhMuc> DMCollection = new List<KDT_GC_DinhMuc>();
                                        DMCollection = KDT_GC_DinhMuc.SelectCollectionDynamic("Master_ID = '" + lenhSanXuat.SoLenhSanXuat + "' AND MaSanPham ='" + item.MaSanPham + "'", "");
                                        foreach (KDT_GC_DinhMuc dinhmuc in DMCollection)
                                        {
                                            KDT_GC_DinhMucThucTe_DinhMuc DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
                                            DinhMucThucTeDinhMuc.MaSanPham = dinhmuc.MaSanPham;
                                            DinhMucThucTeDinhMuc.TenSanPham = dinhmuc.TenSanPham;
                                            DinhMucThucTeDinhMuc.DVT_SP = lenhSanXuatSP.DVT_ID;
                                            DinhMucThucTeDinhMuc.MaNPL = dinhmuc.MaNguyenPhuLieu;
                                            DinhMucThucTeDinhMuc.TenNPL = dinhmuc.TenNPL;
                                            foreach (Company.GC.BLL.KDT.GC.NguyenPhuLieu npl in HD.NPLCollection)
                                            {
                                                isExits = false;
                                                if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                                {
                                                    DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                                    DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                foreach (GC_NguyenPhuLieu npl in GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID))
                                                {
                                                    isExits = false;
                                                    if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                                    {
                                                        DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                                        DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                            if (dinhmuc.TyLeHaoHut > 0)
                                            {
                                                DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung + dinhmuc.DinhMucSuDung * dinhmuc.TyLeHaoHut / 100;
                                                DinhMucThucTeDinhMuc.TyLeHaoHut = 0;
                                            }
                                            DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                                        }
                                        dinhMucThucTeDangKy.SPCollection.Add(DinhMucThucTeSP);
                                    }
                                    LenhSanXuatFind.InsertUpdateFull();
                                    dinhMucThucTeDangKy.InsertUpdateFull();
                                    DinhMucCollection = GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + HD.ID + " AND LenhSanXuat_ID = 0 ", "");
                                    foreach (GC_DinhMuc items in DinhMucCollection)
                                    {
                                        foreach (KDT_LenhSanXuat_SP sp in LenhSanXuatFind.SPCollection)
                                        {
                                            if (items.MaSanPham == sp.MaSanPham)
                                            {
                                                items.LenhSanXuat_ID = sp.ID;
                                                items.UpdateLenhSanXuat();
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    foreach (KDT_LenhSanXuat items in LenhSanXuatCollection)
                                    {
                                        items.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(items.ID);
                                        foreach (KDT_LenhSanXuat_SP ite in items.SPCollection)
                                        {
                                            foreach (KDT_GC_DinhMuc dm in SPCollection)
                                            {
                                                if (ite.MaSanPham == dm.MaSanPham)
                                                {
                                                    if (lenhSanXuat.TuNgay >= items.TuNgay && lenhSanXuat.TuNgay <= items.DenNgay || lenhSanXuat.DenNgay >= items.TuNgay && lenhSanXuat.DenNgay <= items.DenNgay || lenhSanXuat.TuNgay <= items.TuNgay && lenhSanXuat.DenNgay >= items.DenNgay)
                                                    {
                                                        ShowMessageTQDT("Thông báo từ Hệ thống", String.Format("LỆNH SẢN XUẤT CỦA SP NÀY CÓ THÔNG TIN TỪ NGÀY VÀ ĐẾN NGÀY NẰM TRONG KHOẢNG TỪ NGÀY VÀ ĐẾN NGÀY CỦA LỆNH SẢN XUẤT KHÁC CỦA SP NÀY TRƯỚC ĐÓ VỚI THÔNG TIN  \r\n ID - SẢN PHẨM - LỆNH SẢN XUẤT TRƯỚC - TỪ NGÀY - ĐẾN NGÀY - SẢN PHẨM - LỆNH SẢN XUẤT HIỆN TẠI - TỪ NGÀY - ĐẾN NGÀY  \r\n - {0} - {1} - {2} - {3} - {4} - {5} - {6} - {7} - {8} \n ", items.ID, ite.MaSanPham, items.SoLenhSanXuat, items.TuNgay.ToString("dd/MM/yyyy"), items.DenNgay.ToString("dd/MM/yyyy"), dm.MaSanPham, lenhSanXuat.SoLenhSanXuat, lenhSanXuat.TuNgay.ToString("dd/MM/yyyy"), lenhSanXuat.DenNgay.ToString("dd/MM/yyyy")), false);
                                                        return;
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    foreach (GC_SanPham ite in GC_SanPham.SelectCollectionBy_HopDong_ID(HD.ID))
                                    {
                                        foreach (KDT_LenhSanXuat_SP items in lenhSanXuat.SPCollection)
                                        {
                                            if (ite.Ma == items.MaSanPham)
                                            {
                                                items.MaHS = ite.MaHS;
                                                items.DVT_ID = ite.DVT_ID;
                                            }
                                        }
                                    }

                                    foreach (KDT_GC_DinhMuc item in SPCollection)
                                    {
                                        //Cập nhật lệnh sản xuất 
                                        foreach (Company.GC.BLL.KDT.GC.SanPham sp in HD.SPCollection)
                                        {
                                            isExits = false;
                                            if (lenhSanXuatSP.MaSanPham == sp.Ma)
                                            {
                                                lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                                                lenhSanXuatSP.MaHS = sp.MaHS;
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            foreach (GC_SanPham sp in GC_SanPham.SelectCollectionBy_HopDong_ID(HD.ID))
                                            {
                                                isExits = false;
                                                if (lenhSanXuatSP.MaSanPham == sp.Ma)
                                                {
                                                    lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                                                    lenhSanXuatSP.MaHS = sp.MaHS;
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                         //Cập nhật vào định mức thực tế theo từng giai đoạn
                                        if (DMDK.ID ==0)
                                        {
                                            DMDK = new KDT_GC_DinhMucDangKy();
                                            DMDK = KDT_GC_DinhMucDangKy.Load(item.Master_ID);

                                            dinhMucThucTeDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                            dinhMucThucTeDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                            dinhMucThucTeDangKy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                                            dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSanXuat.ID;
                                            dinhMucThucTeDangKy.HopDong_ID = HD.ID;
                                            dinhMucThucTeDangKy.SoTiepNhan = DMDK.SoTiepNhan;
                                            dinhMucThucTeDangKy.NgayTiepNhan = DMDK.NgayTiepNhan;
                                            dinhMucThucTeDangKy.TrangThaiXuLy = DMDK.TrangThaiXuLy;
                                            dinhMucThucTeDangKy.GuidString = String.IsNullOrEmpty(DMDK.GUIDSTR) ? Guid.NewGuid().ToString().ToUpper() : DMDK.GUIDSTR;
                                        }

                                        KDT_GC_DinhMucThucTe_SP DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
                                        DinhMucThucTeSP.MaSanPham = lenhSanXuatSP.MaSanPham;
                                        DinhMucThucTeSP.TenSanPham = lenhSanXuatSP.TenSanPham;
                                        DinhMucThucTeSP.DVT_ID = lenhSanXuatSP.DVT_ID;
                                        DinhMucThucTeSP.MaHS = lenhSanXuatSP.MaHS;

                                        List<KDT_GC_DinhMuc> DMCollection = new List<KDT_GC_DinhMuc>();
                                        DMCollection = KDT_GC_DinhMuc.SelectCollectionDynamic("Master_ID = '" + lenhSanXuat.SoLenhSanXuat.ToString() + "' AND MaSanPham ='" + item.MaSanPham + "'", "");
                                        foreach (KDT_GC_DinhMuc dinhmuc in DMCollection)
                                        {
                                            KDT_GC_DinhMucThucTe_DinhMuc DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
                                            DinhMucThucTeDinhMuc.MaSanPham = dinhmuc.MaSanPham;
                                            DinhMucThucTeDinhMuc.TenSanPham = dinhmuc.TenSanPham;
                                            DinhMucThucTeDinhMuc.DVT_SP = lenhSanXuatSP.DVT_ID;
                                            DinhMucThucTeDinhMuc.MaNPL = dinhmuc.MaNguyenPhuLieu;
                                            DinhMucThucTeDinhMuc.TenNPL = dinhmuc.TenNPL;
                                            foreach (Company.GC.BLL.KDT.GC.NguyenPhuLieu npl in HD.NPLCollection)
                                            {
                                                isExits = false;
                                                if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                                {
                                                    DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                                    DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                foreach (GC_NguyenPhuLieu npl in GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID))
                                                {
                                                    isExits = false;
                                                    if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                                    {
                                                        DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                                        DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                            if (dinhmuc.TyLeHaoHut > 0)
                                            {
                                                DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung + dinhmuc.DinhMucSuDung * dinhmuc.TyLeHaoHut / 100;
                                                DinhMucThucTeDinhMuc.TyLeHaoHut = 0;
                                            }
                                            DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                                        }
                                        dinhMucThucTeDangKy.SPCollection.Add(DinhMucThucTeSP);
                                    }
                                    lenhSanXuat.InsertUpdateFull();
                                    dinhMucThucTeDangKy.InsertUpdateFull();
                                    DinhMucCollection = GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + HD.ID + " AND LenhSanXuat_ID = 0 ", "");
                                    foreach (GC_DinhMuc items in DinhMucCollection)
                                    {
                                        foreach (KDT_LenhSanXuat_SP sp in lenhSanXuat.SPCollection)
                                        {
                                            if (items.MaSanPham == sp.MaSanPham)
                                            {
                                                items.LenhSanXuat_ID = sp.ID;
                                                items.UpdateLenhSanXuat();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //Tạo lệnh sản xuất cho những định mức đã khai báo trước đây nhưng chưa có lệnh
                        DinhMucCollection = GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + HD.ID + " AND LenhSanXuat_ID = 0 ", "");
                        if (DinhMucCollection.Count >=1)
                        {
                            lenhSanXuat = new KDT_LenhSanXuat();
                            lenhSanXuat.HopDong_ID = HD.ID;
                            lenhSanXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            lenhSanXuat.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                            lenhSanXuat.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            if (String.IsNullOrEmpty(GetConfigAuto(cbbTinhTrang.SelectedValue.ToString())))
                            {
                                KDT_LenhSanXuat_QuyTac Config = new KDT_LenhSanXuat_QuyTac();
                                Config.TienTo = "LSX";
                                Config.TinhTrang = cbbTinhTrang.SelectedValue.ToString();
                                Config.LoaiHinh = "GC";
                                Config.GiaTriPhanSo = 1000;
                                Config.DoDaiSo = 5;
                                Config.Nam = HD.NgayKy;
                                Config.SoHopDong = HD.SoHopDong;
                                Config.HopDong_ID = HD.ID;
                                string Prefix = "";
                                Prefix = Config.TienTo.ToString().Trim() + Config.TinhTrang.ToString() + Config.LoaiHinh.ToString() + (Config.GiaTriPhanSo + 1) + "-" + Config.Nam.Year.ToString() + "-" + Config.SoHopDong.ToString();
                                Config.HienThi = Prefix;
                                Config.Insert();
                            }
                            lenhSanXuat.SoLenhSanXuat = GetConfigAuto(cbbTinhTrang.SelectedValue.ToString());
                            lenhSanXuat.TuNgay = new DateTime(HD.NgayKy.Year, HD.NgayKy.Month, HD.NgayKy.Day, 00, 00, 00);
                            lenhSanXuat.DenNgay = HD.NgayGiaHan.Year == 1 || HD.NgayGiaHan.Year == 1900 ? new DateTime(HD.NgayHetHan.Year, HD.NgayHetHan.Month, HD.NgayHetHan.Day, 23, 59, 59) : new DateTime(HD.NgayGiaHan.Year, HD.NgayGiaHan.Month, HD.NgayGiaHan.Day, 23, 59, 59);
                            lenhSanXuat.SoDonHang = cbbSoPO.SelectedValue == "0" ? HD.DonViDoiTac : HD.TenDonViDoiTac;
                            lenhSanXuat.TinhTrang = Convert.ToInt32(cbbTinhTrang.SelectedValue.ToString());
                            lenhSanXuat.GhiChu = txtGhiChu.Text.ToString();
                            SPCollection = new List<KDT_GC_DinhMuc>();
                            string MaSPCheck = String.Empty;
                            foreach (GC_DinhMuc itemss in DinhMucCollection)
                            {
                                if (String.IsNullOrEmpty(MaSPCheck))
                                {
                                    lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                                    lenhSanXuatSP.MaSanPham = itemss.MaSanPham;
                                    lenhSanXuatSP.TenSanPham = itemss.TenSanPham;
                                    lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                                    MaSPCheck = itemss.MaSanPham;
                                }
                                else if (itemss.MaSanPham != MaSPCheck)
                                {
                                    lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                                    lenhSanXuatSP.MaSanPham = itemss.MaSanPham;
                                    lenhSanXuatSP.TenSanPham = itemss.TenSanPham;
                                    lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                                    MaSPCheck = itemss.MaSanPham;
                                }
                            }
                            if (lenhSanXuat.SPCollection.Count >=1)
                            {
                                foreach (GC_SanPham ite in GC_SanPham.SelectCollectionBy_HopDong_ID(HD.ID))
                                {
                                    foreach (KDT_LenhSanXuat_SP items in lenhSanXuat.SPCollection)
                                    {
                                        if (ite.Ma == items.MaSanPham)
                                        {
                                            items.MaHS = ite.MaHS;
                                            items.DVT_ID = ite.DVT_ID;
                                        }
                                    }
                                }
                                lenhSanXuat.InsertUpdateFull();
                                foreach (GC_DinhMuc items in DinhMucCollection)
                                {
                                    foreach (KDT_LenhSanXuat_SP sp in lenhSanXuat.SPCollection)
                                    {
                                        if (items.MaSanPham == sp.MaSanPham)
                                        {
                                            items.LenhSanXuat_ID = sp.ID;
                                            items.UpdateLenhSanXuat();
                                        }
                                    }
                                }  
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        BindData();
                        bttAuto.Enabled = true; btnClose.Enabled = true;

                    }));
                }
                else
                {
                    BindData();
                    bttAuto.Enabled = true; btnClose.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SetError(string.Empty);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["SoHopDong"], ConditionOperator.Contains, txtSoHD.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
