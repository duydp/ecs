﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;

namespace Company.Interface
{
    public partial class FrmDongBoDuLieuSimple : BaseForm
    {
        private ToKhaiMauDich tkmd = null;

        public FrmDongBoDuLieuSimple()
        {
            InitializeComponent();
        }

        private void FrmDongBoDuLieuSimple_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                int soTK = Convert.ToInt32(txtSoTiepNhan.Text);
                int namDK = Convert.ToInt32(txtNamTiepNhan.Text);

                tkmd = new ToKhaiMauDich();
                tkmd.SoToKhai = soTK;
                tkmd.NamDK = namDK;

            }
            catch (Exception ex)
            {
                Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Lỗi trong quá trình thực hiện:\r\n" + ex.Message + "\nChi tiết:" + ex.StackTrace, false);
            }
        }
    }
}
