﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
#endif


namespace Company.Interface
{
    public partial class ListContainerTKMDForm : Form
    {
        public ListContainerTKMDForm()
        {
            InitializeComponent();
        }
        public ToKhaiMauDich TKMD;
        private bool notVanDon = false;
        private bool isEdit = false;
        private DataTable dt = new DataTable();
       // DataColumn dcSoTien = new DataColumn("SoTien", typeof(decimal));

        //public List<LePhiHQ> LePhiHQ = new List<LePhiHQ>();
        private void LePhiHQ_Load(object sender, EventArgs e)
        {
            if (TKMD != null && TKMD.SoLuongContainer == null)
                TKMD.SoLuongContainer = new SoContainer();

            isEdit = TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (TKMD.VanTaiDon != null && TKMD.VanTaiDon.ContainerCollection != null && TKMD.VanTaiDon.ContainerCollection.Count > 0)
                dgLePhi.DataSource = TKMD.VanTaiDon.ContainerCollection;
            else
            {
                label1.Text = "Vui lòng nhập container trong vận đơn (nếu có)";
                dgLePhi.Tables[0].Columns["SoHieu"].Visible = false;
                dgLePhi.Tables[0].Columns["Seal_No"].Visible = false;
                dgLePhi.Tables[0].Columns["SoKien"].Visible = false;
                dgLePhi.Tables[0].Columns["SoLuong"].Visible = true;
                dgLePhi.Tables[0].Columns["SoLuong"].EditType = isEdit?  Janus.Windows.GridEX.EditType.TextBox : Janus.Windows.GridEX.EditType.NoEdit;
                dgLePhi.Tables[0].Groups.RemoveAt(0);
                dgLePhi.Tables[0].Columns["LoaiContainer"].Visible = true;
                dgLePhi.Tables[0].Columns["LoaiContainer"].EditType = Janus.Windows.GridEX.EditType.NoEdit;
                
                dgLePhi.Tables[0].AllowEdit = isEdit ? Janus.Windows.GridEX.InheritableBoolean.True : Janus.Windows.GridEX.InheritableBoolean.False;
                dt.Columns.Add(new DataColumn("LoaiContainer", typeof(string)));
                dt.Columns.Add(new DataColumn("SoLuong", typeof(int)));
                SoContainer socont = TKMD.SoLuongContainer;
                this.Width = 300;
                
                DataRow dr2 = dt.NewRow();
                dr2["LoaiContainer"] = "2";
                dr2["SoLuong"] = socont.Cont20;
                dt.Rows.Add(dr2);
                DataRow dr4 = dt.NewRow();
                dr4["LoaiContainer"] = "4";
                dr4["SoLuong"] = socont.Cont40;
                dt.Rows.Add(dr4);
                DataRow dr45 = dt.NewRow();
                dr45["LoaiContainer"] = "5";
                dr45["SoLuong"] = socont.Cont45;
                dt.Rows.Add(dr45);
                DataRow drkhac = dt.NewRow();
                drkhac["LoaiContainer"] = "0";
                drkhac["SoLuong"] = socont.ContKhac;
                dt.Rows.Add(drkhac);

                dgLePhi.DataSource = dt;
                notVanDon = true;
                dgLePhi.Tables[0].Columns["LoaiContainer"].AutoSize();
                dgLePhi.Tables[0].Columns["LoaiContainer"].TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            }
         

        }

        private void LePhiHQ_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (notVanDon && isEdit)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    int SoLuong = 0;
                    switch (dr["LoaiContainer"].ToString())
                    {
                        case "2":
                            TKMD.SoLuongContainer.Cont20 = int.TryParse(dr["SoLuong"].ToString(), out SoLuong) ? SoLuong : 0;
                            break;
                        case "4":
                            TKMD.SoLuongContainer.Cont40 = int.TryParse(dr["SoLuong"].ToString(), out SoLuong) ? SoLuong : 0;
                            break;
                        case "45":
                            TKMD.SoLuongContainer.Cont45 = int.TryParse(dr["SoLuong"].ToString(), out SoLuong) ? SoLuong : 0;
                            break;
                        case "0":
                            TKMD.SoLuongContainer.ContKhac = int.TryParse(dr["SoLuong"].ToString(), out SoLuong) ? SoLuong : 0;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void dgLePhi_FormattingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            //e.Row.Cells["TrangThai"].Value = e.Row.Cells["TrangThai"].Value.ToString() == "1" ? true : false;
          
        }

        private void dgLePhi_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
//             if (e.Row.Cells["TrangThai"].Text == "0")
//                 e.Row.Cells["TrangThai"].Text = "Rỗng";
//             else if (e.Row.Cells["TrangThai"].Text == "1")
//                 e.Row.Cells["TrangThai"].Text = "Đầy";
            if (e.Row.Cells["LoaiContainer"].Value != null)
                switch (e.Row.Cells["LoaiContainer"].Value.ToString())
                {
                    case "2":
                        e.Row.Cells["LoaiContainer"].Text = "Container 20";
                        break;
                    case "4":
                        e.Row.Cells["LoaiContainer"].Text = "Container 40 ";
                        break;
                    case "45":
                        e.Row.Cells["LoaiContainer"].Text = "Container 45";
                        break;
                    case "0":
                        e.Row.Cells["LoaiContainer"].Text = "Loại khác";
                        break;
                    default:
                        break;
                }
        }
        
    }
}
