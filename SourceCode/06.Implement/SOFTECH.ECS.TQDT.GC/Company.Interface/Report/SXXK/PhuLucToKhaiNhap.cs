﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT;

namespace Company.Interface.Report.SXXK
{
    public partial class PhuLucToKhaiNhap : DevExpress.XtraReports.UI.XtraReport
    {
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public Report.ReportViewTKNForm report;
        public int SoToKhai;
        public DateTime NgayDangKy;
        public string MaLoaiHinh = "";
        public bool BanLuuHaiQuan = false;
        public PhuLucToKhaiNhap()
        {
            InitializeComponent();
        }
        public void BindReport(bool inMaHang)
        {
            if (this.BanLuuHaiQuan)
            {
                lblBanLuuHaiQuan.Text = "Bản lưu Hải quan";
            }
            else
            {
                lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            }
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongThueXNK = 0;
            xrLabel2.Text = GlobalSettings.TieuDeInDinhMuc;
            if(NgayDangKy > new DateTime(1900,1,1))
                lblNgayDangKy.Text = NgayDangKy.ToString("dd/MM/yyyy");
            if(SoToKhai!=0)
                lblSoToKhai.Text = SoToKhai + "";
            for (int i = 0; i < this.HMDCollection.Count; i++)
            {
                XRControl control = new XRControl();
                HangMauDich hmd = this.HMDCollection[i];
                control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                control.Text = hmd.TenHang;
                if (inMaHang) control.Text += " / " + hmd.MaPhu; 
                control = this.xrTable1.Rows[i].Controls["MaHS" + (i + 1)];
                control.Text = hmd.MaHS;
                control = this.xrTable1.Rows[i].Controls["XuatXu" + (i + 1)];
                control.Text = hmd.NuocXX_ID;
                control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                control.Text = hmd.SoLuong.ToString("G20");
                control = this.xrTable1.Rows[i].Controls["DVT" + (i + 1)];
                control.Text = DonViTinh.GetName((object)hmd.DVT_ID);
                control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                control.Text = hmd.DonGiaKB.ToString("G20");
                control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                control.Text = hmd.TriGiaKB.ToString("N2");
                //control = this.xrTable2.Rows[i].Controls["TriGiaTT" + (i + 1)];
                //control.Text = hmd.TriGiaTT.ToString("N0");
                //control = this.xrTable2.Rows[i].Controls["ThueSuatXNK" + (i + 1)];
                //if (hmd.ThueSuatXNKGiam.Trim() == "")
                //    control.Text = hmd.ThueSuatXNK.ToString("N0");
                //else
                //    control.Text = hmd.ThueSuatXNKGiam;

                //control = this.xrTable2.Rows[i].Controls["TienThueXNK" + (i + 1)];
                //control.Text = hmd.ThueXNK.ToString("N0");

                tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                //tongThueXNK += hmd.ThueXNK;
            }
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            //lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
        }
        public void setVisibleImage(bool t)
        {
            xrPictureBox1.Visible = t;
            lblBanLuuHaiQuan.Visible = t;
        }
        public void setNhomHang(XRTableCell cell, string tenHang)
        {
            cell.Text = tenHang;
        }
        private void TenHang_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
    }
}
