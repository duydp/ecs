﻿namespace LicenseCode
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout gridList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.cmbSoNgay1 = new System.Windows.Forms.ComboBox();
            this.txtSoLuong = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbLoaiKey1 = new System.Windows.Forms.ComboBox();
            this.cbPrefix1 = new System.Windows.Forms.ComboBox();
            this.cbProduct = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbSoNgay2 = new System.Windows.Forms.ComboBox();
            this.cmbloaiKey2 = new System.Windows.Forms.ComboBox();
            this.txtTenDoanhNghiep = new System.Windows.Forms.TextBox();
            this.txtSoLuongKey = new System.Windows.Forms.TextBox();
            this.txtCustomerDes = new System.Windows.Forms.TextBox();
            this.txtMaDoanhNghiep = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbProduct2 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbProduct3 = new System.Windows.Forms.ComboBox();
            this.cmbLoaiKey3 = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTBFileNames = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtProductID = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtActiveUrl = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.txtSerial = new System.Windows.Forms.TextBox();
            this.txtMachineCode = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.eventLog1 = new System.Diagnostics.EventLog();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageCreateCDKey = new Janus.Windows.UI.Tab.UITabPage();
            this.btnGen = new Janus.Windows.EditControls.UIButton();
            this.btnTaoTienTo = new Janus.Windows.EditControls.UIButton();
            this.btnTaoSanPham = new Janus.Windows.EditControls.UIButton();
            this.uiTabPageGetCDKey = new Janus.Windows.UI.Tab.UITabPage();
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.btnLayKey = new Janus.Windows.EditControls.UIButton();
            this.btnQuanLyDN = new Janus.Windows.EditControls.UIButton();
            this.uiTabPageCheckCDKey = new Janus.Windows.UI.Tab.UITabPage();
            this.btnCheckInfo = new Janus.Windows.EditControls.UIButton();
            this.lblTrangThai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtActiveCode = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNguoiCapKey = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblProductID = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNgayActive = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblNgayCapKey = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblNgayHetHan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblNgayCuoi = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiTabPageUpdateNotification = new Janus.Windows.UI.Tab.UITabPage();
            this.btnCapNhatTB = new Janus.Windows.EditControls.UIButton();
            this.btnHienThiTB = new Janus.Windows.EditControls.UIButton();
            this.btnBrowse = new Janus.Windows.EditControls.UIButton();
            this.uiTabPageUserInfo = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDoiMatKhau = new Janus.Windows.EditControls.UIButton();
            this.txtNhapLaiMatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMatKhauMoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMatKhauCu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblQuyenHan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblEmail = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblChucVu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblPhongBan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblUser = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTenUser = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPageUserManagement = new Janus.Windows.UI.Tab.UITabPage();
            this.gridList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtEmail = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDepartment = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtUserName = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtFirstName = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnDeleteAccount = new Janus.Windows.EditControls.UIButton();
            this.btnCreateAccount = new Janus.Windows.EditControls.UIButton();
            this.uiTabPageActiveCDKey = new Janus.Windows.UI.Tab.UITabPage();
            this.button2 = new Janus.Windows.EditControls.UIButton();
            this.btnOpenFolder = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageCreateCDKey.SuspendLayout();
            this.uiTabPageGetCDKey.SuspendLayout();
            this.uiTabPageCheckCDKey.SuspendLayout();
            this.uiTabPageUpdateNotification.SuspendLayout();
            this.uiTabPageUserInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.uiTabPageUserManagement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.uiTabPageActiveCDKey.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbSoNgay1
            // 
            this.cmbSoNgay1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbSoNgay1.FormattingEnabled = true;
            this.cmbSoNgay1.Location = new System.Drawing.Point(130, 142);
            this.cmbSoNgay1.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSoNgay1.Name = "cmbSoNgay1";
            this.cmbSoNgay1.Size = new System.Drawing.Size(312, 24);
            this.cmbSoNgay1.TabIndex = 4;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoLuong.Location = new System.Drawing.Point(130, 186);
            this.txtSoLuong.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoLuong.MaxLength = 3;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(312, 23);
            this.txtSoLuong.TabIndex = 5;
            this.txtSoLuong.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(32, 189);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Số lượng";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(32, 101);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 16);
            this.label19.TabIndex = 7;
            this.label19.Text = "Loại key";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(32, 59);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Tiền tố";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(32, 142);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 10;
            this.label2.Text = "Số ngày";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(32, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 16);
            this.label1.TabIndex = 9;
            this.label1.Text = "Mã sản phẩm";
            // 
            // cmbLoaiKey1
            // 
            this.cmbLoaiKey1.BackColor = System.Drawing.Color.White;
            this.cmbLoaiKey1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbLoaiKey1.FormattingEnabled = true;
            this.cmbLoaiKey1.Items.AddRange(new object[] {
            "Trial",
            "License"});
            this.cmbLoaiKey1.Location = new System.Drawing.Point(130, 101);
            this.cmbLoaiKey1.Margin = new System.Windows.Forms.Padding(4);
            this.cmbLoaiKey1.Name = "cmbLoaiKey1";
            this.cmbLoaiKey1.Size = new System.Drawing.Size(312, 24);
            this.cmbLoaiKey1.TabIndex = 3;
            this.cmbLoaiKey1.SelectedIndexChanged += new System.EventHandler(this.cmbLoaiKey1_SelectedIndexChanged);
            // 
            // cbPrefix1
            // 
            this.cbPrefix1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbPrefix1.FormattingEnabled = true;
            this.cbPrefix1.Location = new System.Drawing.Point(130, 59);
            this.cbPrefix1.Margin = new System.Windows.Forms.Padding(4);
            this.cbPrefix1.Name = "cbPrefix1";
            this.cbPrefix1.Size = new System.Drawing.Size(312, 24);
            this.cbPrefix1.TabIndex = 2;
            // 
            // cbProduct
            // 
            this.cbProduct.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbProduct.FormattingEnabled = true;
            this.cbProduct.Location = new System.Drawing.Point(130, 20);
            this.cbProduct.Margin = new System.Windows.Forms.Padding(4);
            this.cbProduct.Name = "cbProduct";
            this.cbProduct.Size = new System.Drawing.Size(312, 24);
            this.cbProduct.TabIndex = 1;
            this.cbProduct.SelectedIndexChanged += new System.EventHandler(this.cbProduct_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(30, 71);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "Loại CD Key :";
            // 
            // cmbSoNgay2
            // 
            this.cmbSoNgay2.BackColor = System.Drawing.Color.White;
            this.cmbSoNgay2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSoNgay2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbSoNgay2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbSoNgay2.FormattingEnabled = true;
            this.cmbSoNgay2.Location = new System.Drawing.Point(451, 67);
            this.cmbSoNgay2.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSoNgay2.Name = "cmbSoNgay2";
            this.cmbSoNgay2.Size = new System.Drawing.Size(227, 24);
            this.cmbSoNgay2.TabIndex = 2;
            // 
            // cmbloaiKey2
            // 
            this.cmbloaiKey2.BackColor = System.Drawing.Color.White;
            this.cmbloaiKey2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbloaiKey2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbloaiKey2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbloaiKey2.FormattingEnabled = true;
            this.cmbloaiKey2.Items.AddRange(new object[] {
            "Trial",
            "License"});
            this.cmbloaiKey2.Location = new System.Drawing.Point(171, 67);
            this.cmbloaiKey2.Margin = new System.Windows.Forms.Padding(4);
            this.cmbloaiKey2.Name = "cmbloaiKey2";
            this.cmbloaiKey2.Size = new System.Drawing.Size(151, 24);
            this.cmbloaiKey2.TabIndex = 1;
            this.cmbloaiKey2.SelectedIndexChanged += new System.EventHandler(this.cmbloaiKey2_SelectedIndexChanged);
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtTenDoanhNghiep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(451, 162);
            this.txtTenDoanhNghiep.Margin = new System.Windows.Forms.Padding(4);
            this.txtTenDoanhNghiep.MaxLength = 255;
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.ReadOnly = true;
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(227, 23);
            this.txtTenDoanhNghiep.TabIndex = 5;
            // 
            // txtSoLuongKey
            // 
            this.txtSoLuongKey.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtSoLuongKey.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSoLuongKey.Location = new System.Drawing.Point(171, 115);
            this.txtSoLuongKey.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoLuongKey.MaxLength = 3;
            this.txtSoLuongKey.Name = "txtSoLuongKey";
            this.txtSoLuongKey.Size = new System.Drawing.Size(151, 23);
            this.txtSoLuongKey.TabIndex = 3;
            this.txtSoLuongKey.Text = "1";
            this.txtSoLuongKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCustomerDes
            // 
            this.txtCustomerDes.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtCustomerDes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtCustomerDes.Location = new System.Drawing.Point(171, 208);
            this.txtCustomerDes.Margin = new System.Windows.Forms.Padding(4);
            this.txtCustomerDes.Multiline = true;
            this.txtCustomerDes.Name = "txtCustomerDes";
            this.txtCustomerDes.ReadOnly = true;
            this.txtCustomerDes.Size = new System.Drawing.Size(507, 99);
            this.txtCustomerDes.TabIndex = 6;
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtMaDoanhNghiep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(171, 162);
            this.txtMaDoanhNghiep.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaDoanhNghiep.MaxLength = 255;
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.ReadOnly = true;
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(151, 23);
            this.txtMaDoanhNghiep.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(382, 165);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 16);
            this.label21.TabIndex = 10;
            this.label21.Text = "Tên DN";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Location = new System.Drawing.Point(30, 212);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(118, 16);
            this.label35.TabIndex = 10;
            this.label35.Text = "Mô tả khách hàng :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(30, 165);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 16);
            this.label9.TabIndex = 10;
            this.label9.Text = "Mã doanh nghiệp :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(382, 118);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(213, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "Lưu ý: Nhập số lượng key muốn lấy.";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(30, 118);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 16);
            this.label33.TabIndex = 13;
            this.label33.Text = "Số lượng :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(382, 71);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 16);
            this.label10.TabIndex = 13;
            this.label10.Text = "Số ngày";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(30, 24);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 16);
            this.label11.TabIndex = 11;
            this.label11.Text = "Mã sản phẩm :";
            // 
            // cbProduct2
            // 
            this.cbProduct2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbProduct2.FormattingEnabled = true;
            this.cbProduct2.Location = new System.Drawing.Point(171, 20);
            this.cbProduct2.Margin = new System.Windows.Forms.Padding(4);
            this.cbProduct2.Name = "cbProduct2";
            this.cbProduct2.Size = new System.Drawing.Size(507, 24);
            this.cbProduct2.TabIndex = 0;
            this.cbProduct2.SelectedIndexChanged += new System.EventHandler(this.cbProduct_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(489, 153);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 16);
            this.label22.TabIndex = 4;
            this.label22.Text = "Ngày cấp key :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Location = new System.Drawing.Point(489, 199);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(76, 16);
            this.label40.TabIndex = 4;
            this.label40.Text = "Trạng thái :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(23, 244);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 16);
            this.label18.TabIndex = 4;
            this.label18.Text = "Người cấp key :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(489, 68);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 16);
            this.label17.TabIndex = 4;
            this.label17.Text = "Đăng nhập cuối :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(489, 112);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 16);
            this.label16.TabIndex = 4;
            this.label16.Text = "Ngày hết hạn :";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.BackColor = System.Drawing.Color.Transparent;
            this.lbl.Location = new System.Drawing.Point(23, 153);
            this.lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(94, 16);
            this.lbl.TabIndex = 4;
            this.lbl.Text = "Mã sản phẩm :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(23, 199);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 16);
            this.label15.TabIndex = 4;
            this.label15.Text = "Ngày active :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(23, 112);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 16);
            this.label13.TabIndex = 4;
            this.label13.Text = "Tên doanh nghiệp :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(23, 68);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Mã doanh nghiệp :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(23, 23);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 16);
            this.label14.TabIndex = 4;
            this.label14.Text = "Actived Key :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(18, 122);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(162, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Đường dẫn file thông báo: ";
            // 
            // cbProduct3
            // 
            this.cbProduct3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbProduct3.FormattingEnabled = true;
            this.cbProduct3.Location = new System.Drawing.Point(129, 19);
            this.cbProduct3.Margin = new System.Windows.Forms.Padding(4);
            this.cbProduct3.Name = "cbProduct3";
            this.cbProduct3.Size = new System.Drawing.Size(353, 24);
            this.cbProduct3.TabIndex = 0;
            // 
            // cmbLoaiKey3
            // 
            this.cmbLoaiKey3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbLoaiKey3.FormattingEnabled = true;
            this.cmbLoaiKey3.Items.AddRange(new object[] {
            "Trial",
            "License",
            "All"});
            this.cmbLoaiKey3.Location = new System.Drawing.Point(129, 67);
            this.cmbLoaiKey3.Margin = new System.Windows.Forms.Padding(4);
            this.cmbLoaiKey3.Name = "cmbLoaiKey3";
            this.cmbLoaiKey3.Size = new System.Drawing.Size(353, 24);
            this.cmbLoaiKey3.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(18, 22);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 16);
            this.label24.TabIndex = 11;
            this.label24.Text = "Mã sản phẩm :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(18, 70);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 16);
            this.label23.TabIndex = 10;
            this.label23.Text = "Loại key :";
            // 
            // txtTBFileNames
            // 
            this.txtTBFileNames.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtTBFileNames.Location = new System.Drawing.Point(22, 158);
            this.txtTBFileNames.Margin = new System.Windows.Forms.Padding(4);
            this.txtTBFileNames.Name = "txtTBFileNames";
            this.txtTBFileNames.Size = new System.Drawing.Size(580, 23);
            this.txtTBFileNames.TabIndex = 12;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(27, 201);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 16);
            this.label30.TabIndex = 2;
            this.label30.Text = "Email : ";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Location = new System.Drawing.Point(27, 245);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(78, 16);
            this.label34.TabIndex = 2;
            this.label34.Text = "Quyền hạn :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(27, 157);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(63, 16);
            this.label32.TabIndex = 2;
            this.label32.Text = "Chức vụ :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(27, 113);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 16);
            this.label31.TabIndex = 2;
            this.label31.Text = "Phòng ban :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(27, 69);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(103, 16);
            this.label29.TabIndex = 2;
            this.label29.Text = "Tên đăng nhập :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(27, 28);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(79, 16);
            this.label27.TabIndex = 2;
            this.label27.Text = "Họ và Tên : ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(27, 130);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 16);
            this.label26.TabIndex = 2;
            this.label26.Text = "Nhập lại  :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(27, 85);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(98, 16);
            this.label25.TabIndex = 2;
            this.label25.Text = "Mật khẩu mới : ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(27, 34);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 16);
            this.label12.TabIndex = 2;
            this.label12.Text = "Mật khẩu cũ : ";
            // 
            // txtProductID
            // 
            this.txtProductID.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtProductID.FormattingEnabled = true;
            this.txtProductID.Location = new System.Drawing.Point(125, 18);
            this.txtProductID.Margin = new System.Windows.Forms.Padding(4);
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(340, 24);
            this.txtProductID.TabIndex = 10;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Location = new System.Drawing.Point(22, 207);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(84, 16);
            this.label39.TabIndex = 7;
            this.label39.Text = "Địa chỉ active";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Location = new System.Drawing.Point(22, 160);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(74, 16);
            this.label38.TabIndex = 7;
            this.label38.Text = "Tên file key";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(22, 113);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 16);
            this.label20.TabIndex = 7;
            this.label20.Text = "SerialKey";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Location = new System.Drawing.Point(22, 67);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 16);
            this.label36.TabIndex = 9;
            this.label36.Text = "Mã máy";
            // 
            // txtActiveUrl
            // 
            this.txtActiveUrl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtActiveUrl.Location = new System.Drawing.Point(125, 203);
            this.txtActiveUrl.Margin = new System.Windows.Forms.Padding(4);
            this.txtActiveUrl.Name = "txtActiveUrl";
            this.txtActiveUrl.Size = new System.Drawing.Size(340, 23);
            this.txtActiveUrl.TabIndex = 6;
            this.txtActiveUrl.Text = "http://tfs.softech.vn/{0}.asmx";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Location = new System.Drawing.Point(22, 22);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(85, 16);
            this.label37.TabIndex = 8;
            this.label37.Text = "Mã sản phẩm";
            // 
            // txtFileName
            // 
            this.txtFileName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtFileName.Location = new System.Drawing.Point(125, 156);
            this.txtFileName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(340, 23);
            this.txtFileName.TabIndex = 6;
            this.txtFileName.Text = "Softech.lic";
            // 
            // txtSerial
            // 
            this.txtSerial.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSerial.Location = new System.Drawing.Point(125, 109);
            this.txtSerial.Margin = new System.Windows.Forms.Padding(4);
            this.txtSerial.Name = "txtSerial";
            this.txtSerial.Size = new System.Drawing.Size(340, 23);
            this.txtSerial.TabIndex = 6;
            // 
            // txtMachineCode
            // 
            this.txtMachineCode.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMachineCode.Location = new System.Drawing.Point(125, 64);
            this.txtMachineCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtMachineCode.Name = "txtMachineCode";
            this.txtMachineCode.Size = new System.Drawing.Size(340, 23);
            this.txtMachineCode.TabIndex = 4;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // eventLog1
            // 
            this.eventLog1.SynchronizingObject = this;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiTab1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(973, 546);
            this.uiTab1.TabIndex = 1;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageCreateCDKey,
            this.uiTabPageGetCDKey,
            this.uiTabPageCheckCDKey,
            this.uiTabPageUpdateNotification,
            this.uiTabPageUserInfo,
            this.uiTabPageUserManagement,
            this.uiTabPageActiveCDKey});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPageCreateCDKey
            // 
            this.uiTabPageCreateCDKey.Controls.Add(this.btnGen);
            this.uiTabPageCreateCDKey.Controls.Add(this.btnTaoTienTo);
            this.uiTabPageCreateCDKey.Controls.Add(this.btnTaoSanPham);
            this.uiTabPageCreateCDKey.Controls.Add(this.cmbSoNgay1);
            this.uiTabPageCreateCDKey.Controls.Add(this.cbProduct);
            this.uiTabPageCreateCDKey.Controls.Add(this.txtSoLuong);
            this.uiTabPageCreateCDKey.Controls.Add(this.cbPrefix1);
            this.uiTabPageCreateCDKey.Controls.Add(this.cmbLoaiKey1);
            this.uiTabPageCreateCDKey.Controls.Add(this.label4);
            this.uiTabPageCreateCDKey.Controls.Add(this.label1);
            this.uiTabPageCreateCDKey.Controls.Add(this.label2);
            this.uiTabPageCreateCDKey.Controls.Add(this.label5);
            this.uiTabPageCreateCDKey.Controls.Add(this.label19);
            this.uiTabPageCreateCDKey.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageCreateCDKey.Name = "uiTabPageCreateCDKey";
            this.uiTabPageCreateCDKey.Size = new System.Drawing.Size(971, 521);
            this.uiTabPageCreateCDKey.TabStop = true;
            this.uiTabPageCreateCDKey.Text = "TẠO MỚI CD KEY";
            // 
            // btnGen
            // 
            this.btnGen.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGen.Image = ((System.Drawing.Image)(resources.GetObject("btnGen.Image")));
            this.btnGen.ImageIndex = 0;
            this.btnGen.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGen.Location = new System.Drawing.Point(201, 226);
            this.btnGen.Name = "btnGen";
            this.btnGen.Size = new System.Drawing.Size(137, 25);
            this.btnGen.TabIndex = 59;
            this.btnGen.Text = "Tạo CD Key";
            this.btnGen.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGen.Click += new System.EventHandler(this.btnGen_Click);
            // 
            // btnTaoTienTo
            // 
            this.btnTaoTienTo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaoTienTo.Image = ((System.Drawing.Image)(resources.GetObject("btnTaoTienTo.Image")));
            this.btnTaoTienTo.ImageIndex = 0;
            this.btnTaoTienTo.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTaoTienTo.Location = new System.Drawing.Point(449, 59);
            this.btnTaoTienTo.Name = "btnTaoTienTo";
            this.btnTaoTienTo.Size = new System.Drawing.Size(137, 25);
            this.btnTaoTienTo.TabIndex = 59;
            this.btnTaoTienTo.Text = "Tạo tiền tố mới";
            this.btnTaoTienTo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTaoTienTo.Click += new System.EventHandler(this.btnTaoTienTo_Click);
            // 
            // btnTaoSanPham
            // 
            this.btnTaoSanPham.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaoSanPham.Image = ((System.Drawing.Image)(resources.GetObject("btnTaoSanPham.Image")));
            this.btnTaoSanPham.ImageIndex = 0;
            this.btnTaoSanPham.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTaoSanPham.Location = new System.Drawing.Point(449, 20);
            this.btnTaoSanPham.Name = "btnTaoSanPham";
            this.btnTaoSanPham.Size = new System.Drawing.Size(137, 25);
            this.btnTaoSanPham.TabIndex = 59;
            this.btnTaoSanPham.Text = "Tạo sản phẩm mới";
            this.btnTaoSanPham.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTaoSanPham.Click += new System.EventHandler(this.btnTaoSanPham_Click);
            // 
            // uiTabPageGetCDKey
            // 
            this.uiTabPageGetCDKey.Controls.Add(this.uiButton3);
            this.uiTabPageGetCDKey.Controls.Add(this.btnLayKey);
            this.uiTabPageGetCDKey.Controls.Add(this.btnQuanLyDN);
            this.uiTabPageGetCDKey.Controls.Add(this.label6);
            this.uiTabPageGetCDKey.Controls.Add(this.label11);
            this.uiTabPageGetCDKey.Controls.Add(this.cmbSoNgay2);
            this.uiTabPageGetCDKey.Controls.Add(this.cbProduct2);
            this.uiTabPageGetCDKey.Controls.Add(this.cmbloaiKey2);
            this.uiTabPageGetCDKey.Controls.Add(this.label10);
            this.uiTabPageGetCDKey.Controls.Add(this.label33);
            this.uiTabPageGetCDKey.Controls.Add(this.label8);
            this.uiTabPageGetCDKey.Controls.Add(this.label9);
            this.uiTabPageGetCDKey.Controls.Add(this.txtTenDoanhNghiep);
            this.uiTabPageGetCDKey.Controls.Add(this.label35);
            this.uiTabPageGetCDKey.Controls.Add(this.txtSoLuongKey);
            this.uiTabPageGetCDKey.Controls.Add(this.label21);
            this.uiTabPageGetCDKey.Controls.Add(this.txtCustomerDes);
            this.uiTabPageGetCDKey.Controls.Add(this.txtMaDoanhNghiep);
            this.uiTabPageGetCDKey.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageGetCDKey.Name = "uiTabPageGetCDKey";
            this.uiTabPageGetCDKey.Size = new System.Drawing.Size(971, 521);
            this.uiTabPageGetCDKey.TabStop = true;
            this.uiTabPageGetCDKey.Text = "LẤY CD KEY ĐÃ TẠO";
            // 
            // uiButton3
            // 
            this.uiButton3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton3.Image = ((System.Drawing.Image)(resources.GetObject("uiButton3.Image")));
            this.uiButton3.ImageIndex = 0;
            this.uiButton3.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton3.Location = new System.Drawing.Point(519, 325);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(159, 25);
            this.uiButton3.TabIndex = 61;
            this.uiButton3.Text = "Quản lý CD Key đã lấy";
            this.uiButton3.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton3.Click += new System.EventHandler(this.btnQuanLyKey_Click);
            // 
            // btnLayKey
            // 
            this.btnLayKey.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayKey.Image = ((System.Drawing.Image)(resources.GetObject("btnLayKey.Image")));
            this.btnLayKey.ImageIndex = 0;
            this.btnLayKey.ImageSize = new System.Drawing.Size(20, 20);
            this.btnLayKey.Location = new System.Drawing.Point(345, 325);
            this.btnLayKey.Name = "btnLayKey";
            this.btnLayKey.Size = new System.Drawing.Size(160, 25);
            this.btnLayKey.TabIndex = 61;
            this.btnLayKey.Text = "Lấy CD Key";
            this.btnLayKey.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLayKey.Click += new System.EventHandler(this.btnLayKey_Click);
            // 
            // btnQuanLyDN
            // 
            this.btnQuanLyDN.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuanLyDN.Image = ((System.Drawing.Image)(resources.GetObject("btnQuanLyDN.Image")));
            this.btnQuanLyDN.ImageIndex = 0;
            this.btnQuanLyDN.ImageSize = new System.Drawing.Size(20, 20);
            this.btnQuanLyDN.Location = new System.Drawing.Point(170, 325);
            this.btnQuanLyDN.Name = "btnQuanLyDN";
            this.btnQuanLyDN.Size = new System.Drawing.Size(160, 25);
            this.btnQuanLyDN.TabIndex = 60;
            this.btnQuanLyDN.Text = "Quản lý doanh nghiệp";
            this.btnQuanLyDN.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiTabPageCheckCDKey
            // 
            this.uiTabPageCheckCDKey.Controls.Add(this.btnCheckInfo);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblTrangThai);
            this.uiTabPageCheckCDKey.Controls.Add(this.txtActiveCode);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblNguoiCapKey);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblProductID);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblTenDoanhNghiep);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblMaDoanhNghiep);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblNgayActive);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblNgayCapKey);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblNgayHetHan);
            this.uiTabPageCheckCDKey.Controls.Add(this.lblNgayCuoi);
            this.uiTabPageCheckCDKey.Controls.Add(this.label14);
            this.uiTabPageCheckCDKey.Controls.Add(this.label22);
            this.uiTabPageCheckCDKey.Controls.Add(this.label3);
            this.uiTabPageCheckCDKey.Controls.Add(this.label40);
            this.uiTabPageCheckCDKey.Controls.Add(this.label13);
            this.uiTabPageCheckCDKey.Controls.Add(this.label18);
            this.uiTabPageCheckCDKey.Controls.Add(this.label15);
            this.uiTabPageCheckCDKey.Controls.Add(this.label17);
            this.uiTabPageCheckCDKey.Controls.Add(this.label16);
            this.uiTabPageCheckCDKey.Controls.Add(this.lbl);
            this.uiTabPageCheckCDKey.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageCheckCDKey.Name = "uiTabPageCheckCDKey";
            this.uiTabPageCheckCDKey.Size = new System.Drawing.Size(971, 521);
            this.uiTabPageCheckCDKey.TabStop = true;
            this.uiTabPageCheckCDKey.Text = "KIỂM TRA CD KEY";
            // 
            // btnCheckInfo
            // 
            this.btnCheckInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckInfo.Image")));
            this.btnCheckInfo.ImageIndex = 0;
            this.btnCheckInfo.ImageSize = new System.Drawing.Size(20, 20);
            this.btnCheckInfo.Location = new System.Drawing.Point(365, 286);
            this.btnCheckInfo.Name = "btnCheckInfo";
            this.btnCheckInfo.Size = new System.Drawing.Size(160, 25);
            this.btnCheckInfo.TabIndex = 61;
            this.btnCheckInfo.Text = "Kiểm tra CD Key";
            this.btnCheckInfo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCheckInfo.Click += new System.EventHandler(this.btnCheckInfo_Click);
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.Location = new System.Drawing.Point(612, 196);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.ReadOnly = true;
            this.lblTrangThai.Size = new System.Drawing.Size(152, 23);
            this.lblTrangThai.TabIndex = 8;
            this.lblTrangThai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtActiveCode
            // 
            this.txtActiveCode.Location = new System.Drawing.Point(145, 20);
            this.txtActiveCode.Name = "txtActiveCode";
            this.txtActiveCode.Size = new System.Drawing.Size(324, 23);
            this.txtActiveCode.TabIndex = 8;
            this.txtActiveCode.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.txtActiveCode.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblNguoiCapKey
            // 
            this.lblNguoiCapKey.Location = new System.Drawing.Point(145, 241);
            this.lblNguoiCapKey.Name = "lblNguoiCapKey";
            this.lblNguoiCapKey.ReadOnly = true;
            this.lblNguoiCapKey.Size = new System.Drawing.Size(619, 23);
            this.lblNguoiCapKey.TabIndex = 8;
            this.lblNguoiCapKey.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblProductID
            // 
            this.lblProductID.Location = new System.Drawing.Point(145, 150);
            this.lblProductID.Name = "lblProductID";
            this.lblProductID.ReadOnly = true;
            this.lblProductID.Size = new System.Drawing.Size(324, 23);
            this.lblProductID.TabIndex = 8;
            this.lblProductID.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(145, 109);
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.ReadOnly = true;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(324, 23);
            this.lblTenDoanhNghiep.TabIndex = 8;
            this.lblTenDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(145, 65);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.ReadOnly = true;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(324, 23);
            this.lblMaDoanhNghiep.TabIndex = 8;
            this.lblMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblNgayActive
            // 
            this.lblNgayActive.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.lblNgayActive.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.lblNgayActive.DropDownCalendar.Name = "";
            this.lblNgayActive.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.lblNgayActive.Location = new System.Drawing.Point(145, 196);
            this.lblNgayActive.Name = "lblNgayActive";
            this.lblNgayActive.ReadOnly = true;
            this.lblNgayActive.Size = new System.Drawing.Size(157, 23);
            this.lblNgayActive.TabIndex = 7;
            this.lblNgayActive.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // lblNgayCapKey
            // 
            this.lblNgayCapKey.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.lblNgayCapKey.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.lblNgayCapKey.DropDownCalendar.Name = "";
            this.lblNgayCapKey.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.lblNgayCapKey.Location = new System.Drawing.Point(612, 150);
            this.lblNgayCapKey.Name = "lblNgayCapKey";
            this.lblNgayCapKey.ReadOnly = true;
            this.lblNgayCapKey.Size = new System.Drawing.Size(152, 23);
            this.lblNgayCapKey.TabIndex = 7;
            this.lblNgayCapKey.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // lblNgayHetHan
            // 
            this.lblNgayHetHan.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.lblNgayHetHan.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.lblNgayHetHan.DropDownCalendar.Name = "";
            this.lblNgayHetHan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.lblNgayHetHan.Location = new System.Drawing.Point(612, 109);
            this.lblNgayHetHan.Name = "lblNgayHetHan";
            this.lblNgayHetHan.ReadOnly = true;
            this.lblNgayHetHan.Size = new System.Drawing.Size(152, 23);
            this.lblNgayHetHan.TabIndex = 7;
            this.lblNgayHetHan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // lblNgayCuoi
            // 
            this.lblNgayCuoi.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.lblNgayCuoi.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.lblNgayCuoi.DropDownCalendar.Name = "";
            this.lblNgayCuoi.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.lblNgayCuoi.Location = new System.Drawing.Point(612, 65);
            this.lblNgayCuoi.Name = "lblNgayCuoi";
            this.lblNgayCuoi.ReadOnly = true;
            this.lblNgayCuoi.Size = new System.Drawing.Size(152, 23);
            this.lblNgayCuoi.TabIndex = 7;
            this.lblNgayCuoi.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // uiTabPageUpdateNotification
            // 
            this.uiTabPageUpdateNotification.Controls.Add(this.btnCapNhatTB);
            this.uiTabPageUpdateNotification.Controls.Add(this.btnHienThiTB);
            this.uiTabPageUpdateNotification.Controls.Add(this.btnBrowse);
            this.uiTabPageUpdateNotification.Controls.Add(this.label7);
            this.uiTabPageUpdateNotification.Controls.Add(this.cbProduct3);
            this.uiTabPageUpdateNotification.Controls.Add(this.cmbLoaiKey3);
            this.uiTabPageUpdateNotification.Controls.Add(this.txtTBFileNames);
            this.uiTabPageUpdateNotification.Controls.Add(this.label23);
            this.uiTabPageUpdateNotification.Controls.Add(this.label24);
            this.uiTabPageUpdateNotification.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageUpdateNotification.Name = "uiTabPageUpdateNotification";
            this.uiTabPageUpdateNotification.Size = new System.Drawing.Size(971, 521);
            this.uiTabPageUpdateNotification.TabStop = true;
            this.uiTabPageUpdateNotification.Text = "CẬP NHẬT THÔNG BÁO";
            // 
            // btnCapNhatTB
            // 
            this.btnCapNhatTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatTB.Image = ((System.Drawing.Image)(resources.GetObject("btnCapNhatTB.Image")));
            this.btnCapNhatTB.ImageIndex = 0;
            this.btnCapNhatTB.ImageSize = new System.Drawing.Size(20, 20);
            this.btnCapNhatTB.Location = new System.Drawing.Point(388, 202);
            this.btnCapNhatTB.Name = "btnCapNhatTB";
            this.btnCapNhatTB.Size = new System.Drawing.Size(160, 25);
            this.btnCapNhatTB.TabIndex = 64;
            this.btnCapNhatTB.Text = "Cập nhật thông báo";
            this.btnCapNhatTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCapNhatTB.Click += new System.EventHandler(this.btnCapNhatTB_Click_1);
            // 
            // btnHienThiTB
            // 
            this.btnHienThiTB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHienThiTB.Image = ((System.Drawing.Image)(resources.GetObject("btnHienThiTB.Image")));
            this.btnHienThiTB.ImageIndex = 0;
            this.btnHienThiTB.ImageSize = new System.Drawing.Size(20, 20);
            this.btnHienThiTB.Location = new System.Drawing.Point(206, 202);
            this.btnHienThiTB.Name = "btnHienThiTB";
            this.btnHienThiTB.Size = new System.Drawing.Size(160, 25);
            this.btnHienThiTB.TabIndex = 63;
            this.btnHienThiTB.Text = "Xem trước thông báo";
            this.btnHienThiTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnHienThiTB.Click += new System.EventHandler(this.btnHienThiTB_Click_1);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowse.Image")));
            this.btnBrowse.ImageIndex = 0;
            this.btnBrowse.ImageSize = new System.Drawing.Size(20, 20);
            this.btnBrowse.Location = new System.Drawing.Point(25, 202);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(160, 25);
            this.btnBrowse.TabIndex = 62;
            this.btnBrowse.Text = "Chọn file thông báo";
            this.btnBrowse.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // uiTabPageUserInfo
            // 
            this.uiTabPageUserInfo.Controls.Add(this.uiGroupBox2);
            this.uiTabPageUserInfo.Controls.Add(this.uiGroupBox1);
            this.uiTabPageUserInfo.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageUserInfo.Name = "uiTabPageUserInfo";
            this.uiTabPageUserInfo.Size = new System.Drawing.Size(971, 521);
            this.uiTabPageUserInfo.TabStop = true;
            this.uiTabPageUserInfo.Text = "THÔNG TIN TÀI KHOẢN";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.btnDoiMatKhau);
            this.uiGroupBox2.Controls.Add(this.label26);
            this.uiGroupBox2.Controls.Add(this.txtNhapLaiMatKhau);
            this.uiGroupBox2.Controls.Add(this.txtMatKhauMoi);
            this.uiGroupBox2.Controls.Add(this.txtMatKhauCu);
            this.uiGroupBox2.Controls.Add(this.label25);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(473, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(498, 521);
            this.uiGroupBox2.TabIndex = 63;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDoiMatKhau
            // 
            this.btnDoiMatKhau.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoiMatKhau.Image = ((System.Drawing.Image)(resources.GetObject("btnDoiMatKhau.Image")));
            this.btnDoiMatKhau.ImageIndex = 0;
            this.btnDoiMatKhau.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDoiMatKhau.Location = new System.Drawing.Point(208, 165);
            this.btnDoiMatKhau.Name = "btnDoiMatKhau";
            this.btnDoiMatKhau.Size = new System.Drawing.Size(160, 25);
            this.btnDoiMatKhau.TabIndex = 61;
            this.btnDoiMatKhau.Text = "Đổi mật khẩu";
            this.btnDoiMatKhau.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDoiMatKhau.Click += new System.EventHandler(this.btnDoiMatKhau_Click);
            // 
            // txtNhapLaiMatKhau
            // 
            this.txtNhapLaiMatKhau.Location = new System.Drawing.Point(141, 125);
            this.txtNhapLaiMatKhau.Name = "txtNhapLaiMatKhau";
            this.txtNhapLaiMatKhau.PasswordChar = '*';
            this.txtNhapLaiMatKhau.Size = new System.Drawing.Size(321, 23);
            this.txtNhapLaiMatKhau.TabIndex = 9;
            this.txtNhapLaiMatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMatKhauMoi
            // 
            this.txtMatKhauMoi.Location = new System.Drawing.Point(141, 80);
            this.txtMatKhauMoi.Name = "txtMatKhauMoi";
            this.txtMatKhauMoi.PasswordChar = '*';
            this.txtMatKhauMoi.Size = new System.Drawing.Size(321, 23);
            this.txtMatKhauMoi.TabIndex = 9;
            this.txtMatKhauMoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMatKhauCu
            // 
            this.txtMatKhauCu.Location = new System.Drawing.Point(141, 34);
            this.txtMatKhauCu.Name = "txtMatKhauCu";
            this.txtMatKhauCu.PasswordChar = '*';
            this.txtMatKhauCu.Size = new System.Drawing.Size(321, 23);
            this.txtMatKhauCu.TabIndex = 9;
            this.txtMatKhauCu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.lblQuyenHan);
            this.uiGroupBox1.Controls.Add(this.lblEmail);
            this.uiGroupBox1.Controls.Add(this.lblChucVu);
            this.uiGroupBox1.Controls.Add(this.lblPhongBan);
            this.uiGroupBox1.Controls.Add(this.lblUser);
            this.uiGroupBox1.Controls.Add(this.lblTenUser);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.label31);
            this.uiGroupBox1.Controls.Add(this.label29);
            this.uiGroupBox1.Controls.Add(this.label30);
            this.uiGroupBox1.Controls.Add(this.label32);
            this.uiGroupBox1.Controls.Add(this.label34);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(473, 521);
            this.uiGroupBox1.TabIndex = 62;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // lblQuyenHan
            // 
            this.lblQuyenHan.Location = new System.Drawing.Point(141, 240);
            this.lblQuyenHan.Multiline = true;
            this.lblQuyenHan.Name = "lblQuyenHan";
            this.lblQuyenHan.ReadOnly = true;
            this.lblQuyenHan.Size = new System.Drawing.Size(321, 181);
            this.lblQuyenHan.TabIndex = 9;
            this.lblQuyenHan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblEmail
            // 
            this.lblEmail.Location = new System.Drawing.Point(141, 196);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.ReadOnly = true;
            this.lblEmail.Size = new System.Drawing.Size(321, 23);
            this.lblEmail.TabIndex = 9;
            this.lblEmail.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblChucVu
            // 
            this.lblChucVu.Location = new System.Drawing.Point(141, 150);
            this.lblChucVu.Name = "lblChucVu";
            this.lblChucVu.ReadOnly = true;
            this.lblChucVu.Size = new System.Drawing.Size(321, 23);
            this.lblChucVu.TabIndex = 9;
            this.lblChucVu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblPhongBan
            // 
            this.lblPhongBan.Location = new System.Drawing.Point(141, 104);
            this.lblPhongBan.Name = "lblPhongBan";
            this.lblPhongBan.ReadOnly = true;
            this.lblPhongBan.Size = new System.Drawing.Size(321, 23);
            this.lblPhongBan.TabIndex = 9;
            this.lblPhongBan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblUser
            // 
            this.lblUser.Location = new System.Drawing.Point(141, 62);
            this.lblUser.Name = "lblUser";
            this.lblUser.ReadOnly = true;
            this.lblUser.Size = new System.Drawing.Size(321, 23);
            this.lblUser.TabIndex = 9;
            this.lblUser.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTenUser
            // 
            this.lblTenUser.Location = new System.Drawing.Point(141, 25);
            this.lblTenUser.Name = "lblTenUser";
            this.lblTenUser.ReadOnly = true;
            this.lblTenUser.Size = new System.Drawing.Size(321, 23);
            this.lblTenUser.TabIndex = 9;
            this.lblTenUser.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPageUserManagement
            // 
            this.uiTabPageUserManagement.Controls.Add(this.gridList);
            this.uiTabPageUserManagement.Controls.Add(this.uiGroupBox4);
            this.uiTabPageUserManagement.Controls.Add(this.uiGroupBox3);
            this.uiTabPageUserManagement.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageUserManagement.Name = "uiTabPageUserManagement";
            this.uiTabPageUserManagement.Size = new System.Drawing.Size(971, 521);
            this.uiTabPageUserManagement.TabStop = true;
            this.uiTabPageUserManagement.Text = "QUẢN LÝ TÀI KHOẢN";
            // 
            // gridList
            // 
            this.gridList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridList.ColumnAutoResize = true;
            gridList_DesignTimeLayout.LayoutString = resources.GetString("gridList_DesignTimeLayout.LayoutString");
            this.gridList.DesignTimeLayout = gridList_DesignTimeLayout;
            this.gridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridList.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridList.GridLineColor = System.Drawing.SystemColors.Control;
            this.gridList.GroupByBoxVisible = false;
            this.gridList.HeaderFormatStyle.BackColorGradient = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(238)))), ((int)(((byte)(134)))));
            this.gridList.Location = new System.Drawing.Point(0, 108);
            this.gridList.Name = "gridList";
            this.gridList.RecordNavigator = true;
            this.gridList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridList.ScrollBars = Janus.Windows.GridEX.ScrollBars.Vertical;
            this.gridList.Size = new System.Drawing.Size(971, 365);
            this.gridList.TabIndex = 10;
            this.gridList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridList_RowDoubleClick);
            this.gridList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.gridList_LoadingRow);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label43);
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.label42);
            this.uiGroupBox4.Controls.Add(this.label41);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.txtEmail);
            this.uiGroupBox4.Controls.Add(this.txtDepartment);
            this.uiGroupBox4.Controls.Add(this.txtUserName);
            this.uiGroupBox4.Controls.Add(this.txtFirstName);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(971, 108);
            this.uiGroupBox4.TabIndex = 9;
            this.uiGroupBox4.Text = "Tìm kiếm";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Location = new System.Drawing.Point(316, 64);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(39, 16);
            this.label43.TabIndex = 11;
            this.label43.Text = "Email";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageIndex = 0;
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(623, 57);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(96, 25);
            this.btnSearch.TabIndex = 62;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Location = new System.Drawing.Point(316, 25);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(68, 16);
            this.label42.TabIndex = 11;
            this.label42.Text = "Phòng ban";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Location = new System.Drawing.Point(16, 66);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(94, 16);
            this.label41.TabIndex = 11;
            this.label41.Text = "Tên đăng nhập";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(16, 27);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(45, 16);
            this.label28.TabIndex = 11;
            this.label28.Text = "Họ tên";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(402, 59);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(205, 23);
            this.txtEmail.TabIndex = 10;
            this.txtEmail.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDepartment
            // 
            this.txtDepartment.Location = new System.Drawing.Point(402, 20);
            this.txtDepartment.Name = "txtDepartment";
            this.txtDepartment.Size = new System.Drawing.Size(205, 23);
            this.txtDepartment.TabIndex = 10;
            this.txtDepartment.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(117, 61);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(190, 23);
            this.txtUserName.TabIndex = 10;
            this.txtUserName.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(117, 22);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(190, 23);
            this.txtFirstName.TabIndex = 10;
            this.txtFirstName.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnDeleteAccount);
            this.uiGroupBox3.Controls.Add(this.btnCreateAccount);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 473);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(971, 48);
            this.uiGroupBox3.TabIndex = 8;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnDeleteAccount
            // 
            this.btnDeleteAccount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAccount.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteAccount.Image")));
            this.btnDeleteAccount.ImageIndex = 0;
            this.btnDeleteAccount.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteAccount.Location = new System.Drawing.Point(864, 13);
            this.btnDeleteAccount.Name = "btnDeleteAccount";
            this.btnDeleteAccount.Size = new System.Drawing.Size(96, 25);
            this.btnDeleteAccount.TabIndex = 63;
            this.btnDeleteAccount.Text = "Xóa";
            this.btnDeleteAccount.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteAccount.Click += new System.EventHandler(this.btnDeleteAccount_Click);
            // 
            // btnCreateAccount
            // 
            this.btnCreateAccount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateAccount.Image = ((System.Drawing.Image)(resources.GetObject("btnCreateAccount.Image")));
            this.btnCreateAccount.ImageIndex = 0;
            this.btnCreateAccount.ImageSize = new System.Drawing.Size(20, 20);
            this.btnCreateAccount.Location = new System.Drawing.Point(762, 13);
            this.btnCreateAccount.Name = "btnCreateAccount";
            this.btnCreateAccount.Size = new System.Drawing.Size(96, 25);
            this.btnCreateAccount.TabIndex = 62;
            this.btnCreateAccount.Text = "Tạo mới";
            this.btnCreateAccount.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCreateAccount.Click += new System.EventHandler(this.btnCreateAccount_Click);
            // 
            // uiTabPageActiveCDKey
            // 
            this.uiTabPageActiveCDKey.Controls.Add(this.button2);
            this.uiTabPageActiveCDKey.Controls.Add(this.btnOpenFolder);
            this.uiTabPageActiveCDKey.Controls.Add(this.txtProductID);
            this.uiTabPageActiveCDKey.Controls.Add(this.label39);
            this.uiTabPageActiveCDKey.Controls.Add(this.txtMachineCode);
            this.uiTabPageActiveCDKey.Controls.Add(this.label38);
            this.uiTabPageActiveCDKey.Controls.Add(this.label20);
            this.uiTabPageActiveCDKey.Controls.Add(this.label36);
            this.uiTabPageActiveCDKey.Controls.Add(this.txtSerial);
            this.uiTabPageActiveCDKey.Controls.Add(this.txtActiveUrl);
            this.uiTabPageActiveCDKey.Controls.Add(this.txtFileName);
            this.uiTabPageActiveCDKey.Controls.Add(this.label37);
            this.uiTabPageActiveCDKey.Location = new System.Drawing.Point(1, 24);
            this.uiTabPageActiveCDKey.Name = "uiTabPageActiveCDKey";
            this.uiTabPageActiveCDKey.Size = new System.Drawing.Size(971, 521);
            this.uiTabPageActiveCDKey.TabStop = true;
            this.uiTabPageActiveCDKey.Text = "KÍCH HOẠT KEY";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageIndex = 0;
            this.button2.ImageSize = new System.Drawing.Size(20, 20);
            this.button2.Location = new System.Drawing.Point(278, 244);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 25);
            this.button2.TabIndex = 62;
            this.button2.Text = "Kích hoạt";
            this.button2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnOpenFolder
            // 
            this.btnOpenFolder.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenFolder.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFolder.Image")));
            this.btnOpenFolder.ImageIndex = 0;
            this.btnOpenFolder.ImageSize = new System.Drawing.Size(20, 20);
            this.btnOpenFolder.Location = new System.Drawing.Point(165, 244);
            this.btnOpenFolder.Name = "btnOpenFolder";
            this.btnOpenFolder.Size = new System.Drawing.Size(107, 25);
            this.btnOpenFolder.TabIndex = 62;
            this.btnOpenFolder.Text = "Mở thư mục";
            this.btnOpenFolder.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnOpenFolder.Click += new System.EventHandler(this.btnOpenFolder_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(973, 546);
            this.Controls.Add(this.uiTab1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(118)))), ((int)(((byte)(61)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần mềm Quản lý bản quyền các phần mềm của Công ty cổ phần Softech";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageCreateCDKey.ResumeLayout(false);
            this.uiTabPageCreateCDKey.PerformLayout();
            this.uiTabPageGetCDKey.ResumeLayout(false);
            this.uiTabPageGetCDKey.PerformLayout();
            this.uiTabPageCheckCDKey.ResumeLayout(false);
            this.uiTabPageCheckCDKey.PerformLayout();
            this.uiTabPageUpdateNotification.ResumeLayout(false);
            this.uiTabPageUpdateNotification.PerformLayout();
            this.uiTabPageUserInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.uiTabPageUserManagement.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiTabPageActiveCDKey.ResumeLayout(false);
            this.uiTabPageActiveCDKey.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtSoLuong;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbProduct;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbProduct2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Diagnostics.EventLog eventLog1;
        private System.Windows.Forms.ComboBox cbPrefix1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cmbLoaiKey1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbSoNgay2;
        private System.Windows.Forms.ComboBox cmbloaiKey2;
        private System.Windows.Forms.TextBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTenDoanhNghiep;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmbLoaiKey3;
        private System.Windows.Forms.ComboBox cbProduct3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cmbSoNgay1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtSoLuongKey;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCustomerDes;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTBFileNames;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtSerial;
        private System.Windows.Forms.TextBox txtMachineCode;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtActiveUrl;
        private System.Windows.Forms.ComboBox txtProductID;
        private System.Windows.Forms.Label label40;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageCreateCDKey;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageGetCDKey;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageCheckCDKey;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageUpdateNotification;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageUserInfo;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageActiveCDKey;
        private Janus.Windows.CalendarCombo.CalendarCombo lblNgayCuoi;
        private Janus.Windows.GridEX.EditControls.EditBox lblMaDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox lblTrangThai;
        private Janus.Windows.GridEX.EditControls.EditBox lblNguoiCapKey;
        private Janus.Windows.GridEX.EditControls.EditBox lblProductID;
        private Janus.Windows.GridEX.EditControls.EditBox lblTenDoanhNghiep;
        private Janus.Windows.CalendarCombo.CalendarCombo lblNgayActive;
        private Janus.Windows.CalendarCombo.CalendarCombo lblNgayCapKey;
        private Janus.Windows.CalendarCombo.CalendarCombo lblNgayHetHan;
        private Janus.Windows.GridEX.EditControls.EditBox txtActiveCode;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageUserManagement;
        private Janus.Windows.EditControls.UIButton btnGen;
        private Janus.Windows.EditControls.UIButton btnTaoTienTo;
        private Janus.Windows.EditControls.UIButton btnTaoSanPham;
        private Janus.Windows.EditControls.UIButton uiButton3;
        private Janus.Windows.EditControls.UIButton btnLayKey;
        private Janus.Windows.EditControls.UIButton btnQuanLyDN;
        private Janus.Windows.EditControls.UIButton btnCheckInfo;
        private Janus.Windows.EditControls.UIButton btnCapNhatTB;
        private Janus.Windows.EditControls.UIButton btnHienThiTB;
        private Janus.Windows.EditControls.UIButton btnBrowse;
        private Janus.Windows.EditControls.UIButton btnDoiMatKhau;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox lblQuyenHan;
        private Janus.Windows.GridEX.EditControls.EditBox lblEmail;
        private Janus.Windows.GridEX.EditControls.EditBox lblChucVu;
        private Janus.Windows.GridEX.EditControls.EditBox lblPhongBan;
        private Janus.Windows.GridEX.EditControls.EditBox lblUser;
        private Janus.Windows.GridEX.EditControls.EditBox lblTenUser;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtNhapLaiMatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMatKhauMoi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMatKhauCu;
        private Janus.Windows.EditControls.UIButton button2;
        private Janus.Windows.EditControls.UIButton btnOpenFolder;
        private Janus.Windows.GridEX.GridEX gridList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnDeleteAccount;
        private Janus.Windows.EditControls.UIButton btnCreateAccount;
        private System.Windows.Forms.Label label43;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.GridEX.EditControls.EditBox txtEmail;
        private Janus.Windows.GridEX.EditControls.EditBox txtDepartment;
        private Janus.Windows.GridEX.EditControls.EditBox txtUserName;
        private Janus.Windows.GridEX.EditControls.EditBox txtFirstName;
    }
}