﻿namespace Microsoft.Xml.Serialization.GeneratedAssembly
{
    using Naccs.Common.JobConfig;
    using Naccs.Core.BatchDoc;
    using Naccs.Core.Classes;
    using Naccs.Core.DataView;
    using Naccs.Core.DataView.Arrange;
    using Naccs.Core.Job;
    using Naccs.Core.Main;
    using Naccs.Core.Option;
    using Naccs.Core.Print;
    using Naccs.Core.Settings;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Reflection;
    using System.Security;
    using System.Xml;
    using System.Xml.Serialization;

    public class XmlSerializationReader1 : XmlSerializationReader
    {
        private string id1_AbstractSettings;
        private string id10_Printer;
        private string id100_JobPanelStatus;
        private string id101_VersionuptoolUpdater;
        private string id102_ProgresType;
        private string id103_VersionSettings;
        private string id104_PrintManager;
        private string id105_ReportStatus;
        private string id106_DataStatus;
        private string id107_Signflg;
        private string id108_ContainedType;
        private string id109_OutcodeTbl;
        private string id11_DefaultInfo;
        private string id110_OutcodeRec;
        private string id111_MessageClassify;
        private string id112_Term;
        private string id113_DataView;
        private string id114_ArrayOfTermClass1;
        private string id115_TermClass;
        private string id116_UserKeySet;
        private string id117_ArrayOfTypeClass;
        private string id118_TypeClass;
        private string id119_GStompSettings;
        private string id12_PrinterInfo;
        private string id120_GStompInfo;
        private string id121_FileSave;
        private string id122_AllFileSave;
        private string id123_Type;
        private string id124_FileName;
        private string id125_SendFile;
        private string id126_Details;
        private string id127_ListType;
        private string id128_TermMessage;
        private string id129_TermMessageSet;
        private string id13_Bin;
        private string id130_ArrayOfReceiveInformClass1;
        private string id131_ReceiveInformClass;
        private string id132_ResourceManager;
        private string id133_ExData;
        private string id134_ButtonPatern;
        private string id135_MessageKind;
        private string id136_NaccsData;
        private string id137_ID;
        private string id138_Style;
        private string id139_Status;
        private string id14_Forms;
        private string id140_TimeStamp;
        private string id141_UserFolder;
        private string id142_Contained;
        private string id143_AttachFolder;
        private string id144_Attach;
        private string id145_IsDeleted;
        private string id146_IsRead;
        private string id147_IsSaved;
        private string id148_IsPrinted;
        private string id149_Header;
        private string id15_OutCode;
        private string id150_JobData;
        private string id151_item;
        private string id152_SignFileFolder;
        private string id153_Control;
        private string id154_JobCode;
        private string id155_ServerRecvTime;
        private string id156_UserId;
        private string id157_Path;
        private string id158_MailAddress;
        private string id159_Subject;
        private string id16_path;
        private string id160_RtpInfo;
        private string id161_ServerInfo;
        private string id162_DataInfo;
        private string id163_Div;
        private string id164_EndFlag;
        private string id165_DataType;
        private string id166_SendGuard;
        private string id167_InputInfo;
        private string id168_IndexInfo;
        private string id169_Pattern;
        private string id17_PathItem;
        private string id170_System;
        private string id171_DispCode;
        private string id172_Length;
        private string id173_InformSound;
        private string id174_Code;
        private string id175_Message;
        private string id176_Button;
        private string id177_Description;
        private string id178_Disposition;
        private string id179_TermMessageSetList;
        private string id18_PathType;
        private string id180_Dir;
        private string id181_AutoSave;
        private string id182_SendFileClass;
        private string id183_SendUser;
        private string id184_FileNameClass;
        private string id185_FileNameRule;
        private string id186_FileSaveMode;
        private string id187_AllFileSaveClass;
        private string id188_Mode;
        private string id189_TypeList;
        private string id19_ProtocolType;
        private string id190_FileNaming;
        private string id191_DetailsList;
        private string id192_GStampInfoClass;
        private string id193_GStompOn;
        private string id194_TopItem;
        private string id195_DateItem;
        private string id196_DateAlignment;
        private string id197_UnderItem1;
        private string id198_UnderItem2;
        private string id199_UnderItem3;
        private string id2_Item;
        private string id20_SettingDivType;
        private string id200_GStampSettings;
        private string id201_GStampInfo;
        private string id202_Id;
        private string id203_Priority;
        private string id204_UserCode;
        private string id205_JobCoode;
        private string id206_UnOpen;
        private string id207_BankNumber;
        private string id208_BankWithdrawday;
        private string id209_Folder;
        private string id21_UserKindType;
        private string id210_DataViewClass;
        private string id211_upTime;
        private string id212_AutoBackup;
        private string id213_TermList;
        private string id214_Folders;
        private string id215_DocName;
        private string id216_OutcodeList;
        private string id217_Template;
        private string id218_Name;
        private string id219_DoublePrint;
        private string id22_KeyListClass;
        private string id220_BinList;
        private string id221_No;
        private string id222_L;
        private string id223_T;
        private string id224_Size;
        private string id225_Orientation;
        private string id226_KeyClass;
        private string id227_Sckey;
        private string id228_Kind;
        private string id229_KeyList;
        private string id23_UserKeyType;
        private string id230_JobKeyList;
        private string id231_AllCertificationClass;
        private string id232_Password;
        private string id233_IsCancel;
        private string id234_RemoveCount;
        private string id235_HelpPathClass;
        private string id236_JobErrCodeHtml;
        private string id237_TrmErrCodeHtml;
        private string id238_GuideIndex;
        private string id239_HelpSettings;
        private string id24_ArrayOfJobKeyClass;
        private string id240_Table;
        private string id241_RemoveDate;
        private string id242_ResetID;
        private string id243_CommonPathClass;
        private string id244_Specify;
        private string id245_PathRoot;
        private string id246_FileNames;
        private string id247_UseGateway;
        private string id248_GatewaySMTPServerName;
        private string id249_GatewaySMTPServerPort;
        private string id25_JobKeyClass;
        private string id250_GatewayPOPServerName;
        private string id251_GatewayPOPServerPort;
        private string id252_GatewayMailBox;
        private string id253_GatewaySMTPDomainName;
        private string id254_GatewayPOPDomainName;
        private string id255_GatewayAddDomainName;
        private string id256_HistorySearchClass;
        private string id257_SerchInfo;
        private string id258_HistoryAddressClass;
        private string id259_Address;
        private string id26_UserEnvironment;
        private string id260_HistoryUserClass;
        private string id261_BatchSendClass;
        private string id262_Sleep;
        private string id263_JobOptionClass;
        private string id264_QueryFieldClear;
        private string id265_ResultCodeColorClass;
        private string id266_Normal;
        private string id267_Error;
        private string id268_Warning;
        private string id269_Cmessage;
        private string id27_TerminalInfo;
        private string id270_DataViewFormClass;
        private string id271_ReadVisible;
        private string id272_ColumnWidthClass;
        private string id273_ResCode;
        private string id274_JobInfo;
        private string id275_JonInfo1;
        private string id276_JonInfo2;
        private string id277_JonInfo3;
        private string id278_TVFolder;
        private string id279_AirSea;
        private string id28_DebugFunction;
        private string id280_PrintName;
        private string id281_HistoryJobsClass;
        private string id282_FontSize;
        private string id283_LastLogonClass;
        private string id284_APP;
        private string id285_SSO;
        private string id286_POP;
        private string id287_UPPERCASE;
        private string id288_WindowClass;
        private string id289_UserInput;
        private string id29_InteractiveInfo;
        private string id290_JobInput;
        private string id291_JobMenu;
        private string id292_ToolBarClass;
        private string id293_Standard;
        private string id294_Job;
        private string id295_FunctionBar;
        private string id296_ApplicationClass;
        private string id297_Left;
        private string id298_Top;
        private string id299_Right;
        private string id3_AbstractConfig;
        private string id30_MailInfo;
        private string id300_Bottom;
        private string id301_MainSplitter;
        private string id302_DataViewSplitter;
        private string id303_Maximized;
        private string id304_CloseOnAppend;
        private string id305_Clock;
        private string id306_MaxRetreiveCnt;
        private string id307_DataViewFontSize;
        private string id308_LogoffConfirm;
        private string id309_SettingsDiv;
        private string id31_HttpOption;
        private string id310_HistroyUser;
        private string id311_SearchInfo;
        private string id312_Offset;
        private string id313_URL;
        private string id314_NACCS_ComLog1;
        private string id315_NACCS_ComLog2;
        private string id316_BatchdocInfoSysClass;
        private string id317_Server;
        private string id318_TraceFileName;
        private string id319_TraceFileSize;
        private string id32_LogFileLength;
        private string id320_name;
        private string id321_display;
        private string id322_ServerName;
        private string id323_ObjectName;
        private string id324_ServerPort;
        private string id325_PingPoint;
        private string id326_SendTimeOut;
        private string id327_RecvTimeOut;
        private string id328_POPServerCnctRetry;
        private string id329_ReConnectTime;
        private string id33_ArrayOfOutCodeClass1;
        private string id330_SMTPServerName;
        private string id331_SMTPServerPort;
        private string id332_POPServerName;
        private string id333_POPServerPort;
        private string id334_MailBox;
        private string id335_DomainName;
        private string id336_UserKind;
        private string id337_Protocol;
        private string id338_A2MinimamDelay;
        private string id339_A2MaximamDelay;
        private string id34_OutCodeClass;
        private string id340_A2TimeOut;
        private string id341_CertificateDate;
        private string id342_BackStatusStr;
        private string id343_TestStatusStr;
        private string id344_Debug;
        private string id345_UseInternet;
        private string id346_PortalURL;
        private string id347_SupportURL;
        private string id348_PrintStampMark;
        private string id349_Demo;
        private string id35_ArrayOfBinClass1;
        private string id350_Jetras;
        private string id351_Receiver;
        private string id352_LogFile;
        private string id353_LogSize;
        private string id354_Count;
        private string id355_BinNo;
        private string id356_BinName;
        private string id357_LogFileLengthClass;
        private string id358_ComLog;
        private string id359_MsgLog;
        private string id36_BinClass;
        private string id360_HttpOptionClass;
        private string id361_CertificateHash;
        private string id362_UseProxy;
        private string id363_ProxyName;
        private string id364_ProxyPort;
        private string id365_UseProxyAccount;
        private string id366_ProxyAccount;
        private string id367_ProxyPassword;
        private string id368_MailInfoClass;
        private string id369_ServerConnect;
        private string id37_WriterLog;
        private string id370_AddDomainName;
        private string id371_AutoMode;
        private string id372_AutoTm;
        private string id373_AutoTmKind;
        private string id374_TraceOutput;
        private string id375_InteractiveInfoClass;
        private string id376_AutoSendRecvMode;
        private string id377_AutoSendRecvTm;
        private string id378_JobConnect;
        private string id379_DebugFunctionClass;
        private string id38_NaccsHeader;
        private string id380_Check_Off;
        private string id381_UserKindFlag;
        private string id382_TerminalInfoClass;
        private string id383_TermLogicalName;
        private string id384_TermAccessKey;
        private string id385_SaveTerm;
        private string id386_DiskWarning;
        private string id387_Versionup;
        private string id388_KeyInfoList;
        private string id389_type;
        private string id39_DataControl;
        private string id390_PathInfo;
        private string id391_projectpath;
        private string id392_custompath;
        private string id393_jobhistory;
        private string id394_inputhistoryroot;
        private string id395_outcodetable;
        private string id396_defaulthelppath;
        private string id397_defaultgymerrpath;
        private string id398_systemerrormessage;
        private string id399_defaulttermmessagepath;
        private string id4_ReceiveNotice;
        private string id40_ArrayOfTermMessageSetClass;
        private string id400_supportfile;
        private string id401_websites;
        private string id402_guideroot;
        private string id403_optioncertificationsetup;
        private string id404_userenvironmentsetup;
        private string id405_systemenvironmentsetup;
        private string id406_printersetup;
        private string id407_messageclassifysetup;
        private string id408_dataviewsetup;
        private string id409_filesavesetup;
        private string id41_TermMessageSetClass;
        private string id410_usersetup;
        private string id411_receivenoticesetup;
        private string id412_helpsetup;
        private string id413_user_key_setup;
        private string id414_demoroot;
        private string id415_scenarioroot;
        private string id416_gym_guidelines;
        private string id417_jobsetup;
        private string id418_templateroot;
        private string id419_customizeroot;
        private string id42_SystemEnvironment;
        private string id420_jobtreeroot;
        private string id421_generalapplink;
        private string id422_customlistlink;
        private string id423_dataviewpath;
        private string id424_backuppath;
        private string id425_attachpath;
        private string id426_signfilepath;
        private string id427_logpath;
        private string id428_codelist;
        private string id429_gstompinfo;
        private string id43_TerminalInfoSysClass;
        private string id430_guidanceroot;
        private string id431_kiosklinkroot;
        private string id432_kiosktablesroot;
        private string id433_jetrasexe;
        private string id434_filesaveroot;
        private string id435_printnamelist;
        private string id436_FormsClass;
        private string id437_OutCodeList;
        private string id438_DefaultInfoClass;
        private string id439_SizeWarning;
        private string id44_InteractiveServerClass;
        private string id440_PrintSetups;
        private string id441_PrinterInfoList;
        private string id442_InformClass;
        private string id443_InfoMode;
        private string id444_WarningInformClass;
        private string id445_WaringMode;
        private string id446_SoundClass;
        private string id447_SoundFile;
        private string id448_ReceiveInformList;
        private string id45_InteractiveInfoSysClass;
        private string id46_MailServerClass;
        private string id47_MailInfoSysClass;
        private string id48_BatchdocInfo;
        private string id49_LogFileLengthSysClass;
        private string id5_ListItem;
        private string id50_ListRec;
        private string id51_NodeSorter;
        private string id52_IndexRec;
        private string id53_TestServer;
        private string id54_NetTestMode;
        private string id55_User;
        private string id56_Application;
        private string id57_ToolBar;
        private string id58_Window;
        private string id59_LastLogon;
        private string id6_ReceiveInform;
        private string id60_HistoryJobs;
        private string id61_ColumnWidth;
        private string id62_DataViewForm;
        private string id63_ResultCodeColor;
        private string id64_JobOption;
        private string id65_BatchSend;
        private string id66_HistoryUser;
        private string id67_HistoryAddress;
        private string id68_HistorySearch;
        private string id69_ArrayOfDetailsClass;
        private string id7_Sound;
        private string id70_DetailsClass;
        private string id71_GatewaySettingClass;
        private string id72_DatasetRevert;
        private string id73_PrintStatus;
        private string id74_RevertEventArgs;
        private string id75_DatasetStorage;
        private string id76_ArrangeDataTable;
        private string id77_CustomizeMenu;
        private string id78_UserPathNames;
        private string id79_CommonPath;
        private string id8_WarningInform;
        private string id80_RemoveWorkerArgs;
        private string id81_Help;
        private string id82_HelpPath;
        private string id83_ConfigFiles;
        private string id84_RemoveWorkerResult;
        private string id85_DatasetRemove;
        private string id86_OptionCertification;
        private string id87_AllCertification;
        private string id88_ComboManager;
        private string id89_DataFactory;
        private string id9_Inform;
        private string id90_UserKey;
        private string id91_JobKey;
        private string id92_Key;
        private string id93_ArrayOfPrinterInfoClass1;
        private string id94_PrinterInfoClass;
        private string id95_ArrayOfString4;
        private string id96_string;
        private string id97_JobFormFactory;
        private string id98_ULogClass;
        private string id99_ModeFont;

        protected override void InitCallbacks()
        {
        }

        protected override void InitIDs()
        {
            this.id354_Count = base.Reader.NameTable.Add("Count");
            this.id303_Maximized = base.Reader.NameTable.Add("Maximized");
            this.id143_AttachFolder = base.Reader.NameTable.Add("AttachFolder");
            this.id373_AutoTmKind = base.Reader.NameTable.Add("AutoTmKind");
            this.id206_UnOpen = base.Reader.NameTable.Add("UnOpen");
            this.id108_ContainedType = base.Reader.NameTable.Add("ContainedType");
            this.id153_Control = base.Reader.NameTable.Add("Control");
            this.id22_KeyListClass = base.Reader.NameTable.Add("KeyListClass");
            this.id245_PathRoot = base.Reader.NameTable.Add("PathRoot");
            this.id253_GatewaySMTPDomainName = base.Reader.NameTable.Add("GatewaySMTPDomainName");
            this.id364_ProxyPort = base.Reader.NameTable.Add("ProxyPort");
            this.id431_kiosklinkroot = base.Reader.NameTable.Add("kiosklink-root");
            this.id335_DomainName = base.Reader.NameTable.Add("DomainName");
            this.id119_GStompSettings = base.Reader.NameTable.Add("GStompSettings");
            this.id200_GStampSettings = base.Reader.NameTable.Add("GStampSettings");
            this.id236_JobErrCodeHtml = base.Reader.NameTable.Add("JobErrCodeHtml");
            this.id249_GatewaySMTPServerPort = base.Reader.NameTable.Add("GatewaySMTPServerPort");
            this.id192_GStampInfoClass = base.Reader.NameTable.Add("GStampInfoClass");
            this.id189_TypeList = base.Reader.NameTable.Add("TypeList");
            this.id185_FileNameRule = base.Reader.NameTable.Add("FileNameRule");
            this.id404_userenvironmentsetup = base.Reader.NameTable.Add("user-environment-setup");
            this.id81_Help = base.Reader.NameTable.Add("Help");
            this.id232_Password = base.Reader.NameTable.Add("Password");
            this.id223_T = base.Reader.NameTable.Add("T");
            this.id39_DataControl = base.Reader.NameTable.Add("DataControl");
            this.id260_HistoryUserClass = base.Reader.NameTable.Add("HistoryUserClass");
            this.id369_ServerConnect = base.Reader.NameTable.Add("ServerConnect");
            this.id238_GuideIndex = base.Reader.NameTable.Add("GuideIndex");
            this.id367_ProxyPassword = base.Reader.NameTable.Add("ProxyPassword");
            this.id160_RtpInfo = base.Reader.NameTable.Add("RtpInfo");
            this.id287_UPPERCASE = base.Reader.NameTable.Add("UPPERCASE");
            this.id286_POP = base.Reader.NameTable.Add("POP");
            this.id180_Dir = base.Reader.NameTable.Add("Dir");
            this.id428_codelist = base.Reader.NameTable.Add("code-list");
            this.id203_Priority = base.Reader.NameTable.Add("Priority");
            this.id74_RevertEventArgs = base.Reader.NameTable.Add("RevertEventArgs");
            this.id390_PathInfo = base.Reader.NameTable.Add("PathInfo");
            this.id48_BatchdocInfo = base.Reader.NameTable.Add("BatchdocInfo");
            this.id169_Pattern = base.Reader.NameTable.Add("Pattern");
            this.id355_BinNo = base.Reader.NameTable.Add("BinNo");
            this.id34_OutCodeClass = base.Reader.NameTable.Add("OutCodeClass");
            this.id5_ListItem = base.Reader.NameTable.Add("ListItem");
            this.id158_MailAddress = base.Reader.NameTable.Add("MailAddress");
            this.id230_JobKeyList = base.Reader.NameTable.Add("JobKeyList");
            this.id23_UserKeyType = base.Reader.NameTable.Add("UserKeyType");
            this.id24_ArrayOfJobKeyClass = base.Reader.NameTable.Add("ArrayOfJobKeyClass");
            this.id176_Button = base.Reader.NameTable.Add("Button");
            this.id430_guidanceroot = base.Reader.NameTable.Add("guidance-root");
            this.id197_UnderItem1 = base.Reader.NameTable.Add("UnderItem1");
            this.id205_JobCoode = base.Reader.NameTable.Add("JobCoode");
            this.id224_Size = base.Reader.NameTable.Add("Size");
            this.id403_optioncertificationsetup = base.Reader.NameTable.Add("option-certification-setup");
            this.id407_messageclassifysetup = base.Reader.NameTable.Add("message-classify-setup");
            this.id2_Item = base.Reader.NameTable.Add("");
            this.id147_IsSaved = base.Reader.NameTable.Add("IsSaved");
            this.id401_websites = base.Reader.NameTable.Add("web-sites");
            this.id218_Name = base.Reader.NameTable.Add("Name");
            this.id365_UseProxyAccount = base.Reader.NameTable.Add("UseProxyAccount");
            this.id269_Cmessage = base.Reader.NameTable.Add("Cmessage");
            this.id181_AutoSave = base.Reader.NameTable.Add("AutoSave");
            this.id426_signfilepath = base.Reader.NameTable.Add("signfile-path");
            this.id429_gstompinfo = base.Reader.NameTable.Add("gstomp-info");
            this.id376_AutoSendRecvMode = base.Reader.NameTable.Add("AutoSendRecvMode");
            this.id110_OutcodeRec = base.Reader.NameTable.Add("OutcodeRec");
            this.id440_PrintSetups = base.Reader.NameTable.Add("PrintSetups");
            this.id125_SendFile = base.Reader.NameTable.Add("SendFile");
            this.id321_display = base.Reader.NameTable.Add("display");
            this.id272_ColumnWidthClass = base.Reader.NameTable.Add("ColumnWidthClass");
            this.id320_name = base.Reader.NameTable.Add("name");
            this.id314_NACCS_ComLog1 = base.Reader.NameTable.Add("NACCS_ComLog1");
            this.id304_CloseOnAppend = base.Reader.NameTable.Add("CloseOnAppend");
            this.id113_DataView = base.Reader.NameTable.Add("DataView");
            this.id295_FunctionBar = base.Reader.NameTable.Add("FunctionBar");
            this.id252_GatewayMailBox = base.Reader.NameTable.Add("GatewayMailBox");
            this.id416_gym_guidelines = base.Reader.NameTable.Add("gym_guidelines");
            this.id193_GStompOn = base.Reader.NameTable.Add("GStompOn");
            this.id267_Error = base.Reader.NameTable.Add("Error");
            this.id307_DataViewFontSize = base.Reader.NameTable.Add("DataViewFontSize");
            this.id90_UserKey = base.Reader.NameTable.Add("UserKey");
            this.id328_POPServerCnctRetry = base.Reader.NameTable.Add("POPServerCnctRetry");
            this.id36_BinClass = base.Reader.NameTable.Add("BinClass");
            this.id285_SSO = base.Reader.NameTable.Add("SSO");
            this.id240_Table = base.Reader.NameTable.Add("Table");
            this.id171_DispCode = base.Reader.NameTable.Add("DispCode");
            this.id338_A2MinimamDelay = base.Reader.NameTable.Add("A2MinimamDelay");
            this.id168_IndexInfo = base.Reader.NameTable.Add("IndexInfo");
            this.id123_Type = base.Reader.NameTable.Add("Type");
            this.id114_ArrayOfTermClass1 = base.Reader.NameTable.Add("ArrayOfTermClass1");
            this.id266_Normal = base.Reader.NameTable.Add("Normal");
            this.id278_TVFolder = base.Reader.NameTable.Add("TVFolder");
            this.id99_ModeFont = base.Reader.NameTable.Add("ModeFont");
            this.id82_HelpPath = base.Reader.NameTable.Add("HelpPath");
            this.id78_UserPathNames = base.Reader.NameTable.Add("UserPathNames");
            this.id87_AllCertification = base.Reader.NameTable.Add("AllCertification");
            this.id138_Style = base.Reader.NameTable.Add("Style");
            this.id257_SerchInfo = base.Reader.NameTable.Add("SerchInfo");
            this.id80_RemoveWorkerArgs = base.Reader.NameTable.Add("RemoveWorkerArgs");
            this.id58_Window = base.Reader.NameTable.Add("Window");
            this.id389_type = base.Reader.NameTable.Add("type");
            this.id385_SaveTerm = base.Reader.NameTable.Add("SaveTerm");
            this.id250_GatewayPOPServerName = base.Reader.NameTable.Add("GatewayPOPServerName");
            this.id432_kiosktablesroot = base.Reader.NameTable.Add("kiosktables-root");
            this.id424_backuppath = base.Reader.NameTable.Add("backup-path");
            this.id421_generalapplink = base.Reader.NameTable.Add("general-app-link");
            this.id316_BatchdocInfoSysClass = base.Reader.NameTable.Add("BatchdocInfoSysClass");
            this.id8_WarningInform = base.Reader.NameTable.Add("WarningInform");
            this.id329_ReConnectTime = base.Reader.NameTable.Add("ReConnectTime");
            this.id394_inputhistoryroot = base.Reader.NameTable.Add("input-history-root");
            this.id337_Protocol = base.Reader.NameTable.Add("Protocol");
            this.id397_defaultgymerrpath = base.Reader.NameTable.Add("default-gym-err-path");
            this.id216_OutcodeList = base.Reader.NameTable.Add("OutcodeList");
            this.id167_InputInfo = base.Reader.NameTable.Add("InputInfo");
            this.id414_demoroot = base.Reader.NameTable.Add("demo-root");
            this.id179_TermMessageSetList = base.Reader.NameTable.Add("TermMessageSetList");
            this.id20_SettingDivType = base.Reader.NameTable.Add("SettingDivType");
            this.id154_JobCode = base.Reader.NameTable.Add("JobCode");
            this.id386_DiskWarning = base.Reader.NameTable.Add("DiskWarning");
            this.id435_printnamelist = base.Reader.NameTable.Add("printname-list");
            this.id141_UserFolder = base.Reader.NameTable.Add("UserFolder");
            this.id284_APP = base.Reader.NameTable.Add("APP");
            this.id234_RemoveCount = base.Reader.NameTable.Add("RemoveCount");
            this.id444_WarningInformClass = base.Reader.NameTable.Add("WarningInformClass");
            this.id71_GatewaySettingClass = base.Reader.NameTable.Add("GatewaySettingClass");
            this.id411_receivenoticesetup = base.Reader.NameTable.Add("receive-notice-setup");
            this.id150_JobData = base.Reader.NameTable.Add("JobData");
            this.id291_JobMenu = base.Reader.NameTable.Add("JobMenu");
            this.id45_InteractiveInfoSysClass = base.Reader.NameTable.Add("InteractiveInfoSysClass");
            this.id118_TypeClass = base.Reader.NameTable.Add("TypeClass");
            this.id76_ArrangeDataTable = base.Reader.NameTable.Add("ArrangeDataTable");
            this.id434_filesaveroot = base.Reader.NameTable.Add("file-save-root");
            this.id322_ServerName = base.Reader.NameTable.Add("ServerName");
            this.id276_JonInfo2 = base.Reader.NameTable.Add("JonInfo2");
            this.id228_Kind = base.Reader.NameTable.Add("Kind");
            this.id265_ResultCodeColorClass = base.Reader.NameTable.Add("ResultCodeColorClass");
            this.id12_PrinterInfo = base.Reader.NameTable.Add("PrinterInfo");
            this.id415_scenarioroot = base.Reader.NameTable.Add("scenario-root");
            this.id244_Specify = base.Reader.NameTable.Add("Specify");
            this.id59_LastLogon = base.Reader.NameTable.Add("LastLogon");
            this.id247_UseGateway = base.Reader.NameTable.Add("UseGateway");
            this.id68_HistorySearch = base.Reader.NameTable.Add("HistorySearch");
            this.id271_ReadVisible = base.Reader.NameTable.Add("ReadVisible");
            this.id37_WriterLog = base.Reader.NameTable.Add("WriterLog");
            this.id327_RecvTimeOut = base.Reader.NameTable.Add("RecvTimeOut");
            this.id109_OutcodeTbl = base.Reader.NameTable.Add("OutcodeTbl");
            this.id152_SignFileFolder = base.Reader.NameTable.Add("SignFileFolder");
            this.id332_POPServerName = base.Reader.NameTable.Add("POPServerName");
            this.id155_ServerRecvTime = base.Reader.NameTable.Add("ServerRecvTime");
            this.id412_helpsetup = base.Reader.NameTable.Add("help-setup");
            this.id246_FileNames = base.Reader.NameTable.Add("FileNames");
            this.id237_TrmErrCodeHtml = base.Reader.NameTable.Add("TrmErrCodeHtml");
            this.id157_Path = base.Reader.NameTable.Add("Path");
            this.id183_SendUser = base.Reader.NameTable.Add("SendUser");
            this.id306_MaxRetreiveCnt = base.Reader.NameTable.Add("MaxRetreiveCnt");
            this.id297_Left = base.Reader.NameTable.Add("Left");
            this.id293_Standard = base.Reader.NameTable.Add("Standard");
            this.id127_ListType = base.Reader.NameTable.Add("ListType");
            this.id268_Warning = base.Reader.NameTable.Add("Warning");
            this.id191_DetailsList = base.Reader.NameTable.Add("DetailsList");
            this.id333_POPServerPort = base.Reader.NameTable.Add("POPServerPort");
            this.id222_L = base.Reader.NameTable.Add("L");
            this.id323_ObjectName = base.Reader.NameTable.Add("ObjectName");
            this.id3_AbstractConfig = base.Reader.NameTable.Add("AbstractConfig");
            this.id254_GatewayPOPDomainName = base.Reader.NameTable.Add("GatewayPOPDomainName");
            this.id16_path = base.Reader.NameTable.Add("path");
            this.id178_Disposition = base.Reader.NameTable.Add("Disposition");
            this.id208_BankWithdrawday = base.Reader.NameTable.Add("BankWithdrawday");
            this.id258_HistoryAddressClass = base.Reader.NameTable.Add("HistoryAddressClass");
            this.id137_ID = base.Reader.NameTable.Add("ID");
            this.id139_Status = base.Reader.NameTable.Add("Status");
            this.id213_TermList = base.Reader.NameTable.Add("TermList");
            this.id170_System = base.Reader.NameTable.Add("System");
            this.id84_RemoveWorkerResult = base.Reader.NameTable.Add("RemoveWorkerResult");
            this.id207_BankNumber = base.Reader.NameTable.Add("BankNumber");
            this.id264_QueryFieldClear = base.Reader.NameTable.Add("QueryFieldClear");
            this.id410_usersetup = base.Reader.NameTable.Add("user-setup");
            this.id341_CertificateDate = base.Reader.NameTable.Add("CertificateDate");
            this.id368_MailInfoClass = base.Reader.NameTable.Add("MailInfoClass");
            this.id7_Sound = base.Reader.NameTable.Add("Sound");
            this.id378_JobConnect = base.Reader.NameTable.Add("JobConnect");
            this.id133_ExData = base.Reader.NameTable.Add("ExData");
            this.id447_SoundFile = base.Reader.NameTable.Add("SoundFile");
            this.id128_TermMessage = base.Reader.NameTable.Add("TermMessage");
            this.id408_dataviewsetup = base.Reader.NameTable.Add("dataview-setup");
            this.id344_Debug = base.Reader.NameTable.Add("Debug");
            this.id347_SupportURL = base.Reader.NameTable.Add("SupportURL");
            this.id214_Folders = base.Reader.NameTable.Add("Folders");
            this.id30_MailInfo = base.Reader.NameTable.Add("MailInfo");
            this.id340_A2TimeOut = base.Reader.NameTable.Add("A2TimeOut");
            this.id343_TestStatusStr = base.Reader.NameTable.Add("TestStatusStr");
            this.id52_IndexRec = base.Reader.NameTable.Add("IndexRec");
            this.id350_Jetras = base.Reader.NameTable.Add("Jetras");
            this.id112_Term = base.Reader.NameTable.Add("Term");
            this.id309_SettingsDiv = base.Reader.NameTable.Add("SettingsDiv");
            this.id439_SizeWarning = base.Reader.NameTable.Add("SizeWarning");
            this.id70_DetailsClass = base.Reader.NameTable.Add("DetailsClass");
            this.id273_ResCode = base.Reader.NameTable.Add("ResCode");
            this.id132_ResourceManager = base.Reader.NameTable.Add("ResourceManager");
            this.id25_JobKeyClass = base.Reader.NameTable.Add("JobKeyClass");
            this.id425_attachpath = base.Reader.NameTable.Add("attach-path");
            this.id31_HttpOption = base.Reader.NameTable.Add("HttpOption");
            this.id348_PrintStampMark = base.Reader.NameTable.Add("PrintStampMark");
            this.id111_MessageClassify = base.Reader.NameTable.Add("MessageClassify");
            this.id85_DatasetRemove = base.Reader.NameTable.Add("DatasetRemove");
            this.id44_InteractiveServerClass = base.Reader.NameTable.Add("InteractiveServerClass");
            this.id173_InformSound = base.Reader.NameTable.Add("InformSound");
            this.id311_SearchInfo = base.Reader.NameTable.Add("SearchInfo");
            this.id11_DefaultInfo = base.Reader.NameTable.Add("DefaultInfo");
            this.id198_UnderItem2 = base.Reader.NameTable.Add("UnderItem2");
            this.id339_A2MaximamDelay = base.Reader.NameTable.Add("A2MaximamDelay");
            this.id102_ProgresType = base.Reader.NameTable.Add("ProgresType");
            this.id116_UserKeySet = base.Reader.NameTable.Add("UserKeySet");
            this.id248_GatewaySMTPServerName = base.Reader.NameTable.Add("GatewaySMTPServerName");
            this.id290_JobInput = base.Reader.NameTable.Add("JobInput");
            this.id46_MailServerClass = base.Reader.NameTable.Add("MailServerClass");
            this.id94_PrinterInfoClass = base.Reader.NameTable.Add("PrinterInfoClass");
            this.id362_UseProxy = base.Reader.NameTable.Add("UseProxy");
            this.id120_GStompInfo = base.Reader.NameTable.Add("GStompInfo");
            this.id418_templateroot = base.Reader.NameTable.Add("template-root");
            this.id145_IsDeleted = base.Reader.NameTable.Add("IsDeleted");
            this.id184_FileNameClass = base.Reader.NameTable.Add("FileNameClass");
            this.id275_JonInfo1 = base.Reader.NameTable.Add("JonInfo1");
            this.id255_GatewayAddDomainName = base.Reader.NameTable.Add("GatewayAddDomainName");
            this.id281_HistoryJobsClass = base.Reader.NameTable.Add("HistoryJobsClass");
            this.id384_TermAccessKey = base.Reader.NameTable.Add("TermAccessKey");
            this.id10_Printer = base.Reader.NameTable.Add("Printer");
            this.id308_LogoffConfirm = base.Reader.NameTable.Add("LogoffConfirm");
            this.id243_CommonPathClass = base.Reader.NameTable.Add("CommonPathClass");
            this.id212_AutoBackup = base.Reader.NameTable.Add("AutoBackup");
            this.id211_upTime = base.Reader.NameTable.Add("upTime");
            this.id32_LogFileLength = base.Reader.NameTable.Add("LogFileLength");
            this.id388_KeyInfoList = base.Reader.NameTable.Add("KeyInfoList");
            this.id345_UseInternet = base.Reader.NameTable.Add("UseInternet");
            this.id72_DatasetRevert = base.Reader.NameTable.Add("DatasetRevert");
            this.id282_FontSize = base.Reader.NameTable.Add("FontSize");
            this.id161_ServerInfo = base.Reader.NameTable.Add("ServerInfo");
            this.id187_AllFileSaveClass = base.Reader.NameTable.Add("AllFileSaveClass");
            this.id299_Right = base.Reader.NameTable.Add("Right");
            this.id122_AllFileSave = base.Reader.NameTable.Add("AllFileSave");
            this.id262_Sleep = base.Reader.NameTable.Add("Sleep");
            this.id419_customizeroot = base.Reader.NameTable.Add("customize-root");
            this.id88_ComboManager = base.Reader.NameTable.Add("ComboManager");
            this.id349_Demo = base.Reader.NameTable.Add("Demo");
            this.id357_LogFileLengthClass = base.Reader.NameTable.Add("LogFileLengthClass");
            this.id448_ReceiveInformList = base.Reader.NameTable.Add("ReceiveInformList");
            this.id67_HistoryAddress = base.Reader.NameTable.Add("HistoryAddress");
            this.id358_ComLog = base.Reader.NameTable.Add("ComLog");
            this.id177_Description = base.Reader.NameTable.Add("Description");
            this.id6_ReceiveInform = base.Reader.NameTable.Add("ReceiveInform");
            this.id395_outcodetable = base.Reader.NameTable.Add("outcode-table");
            this.id14_Forms = base.Reader.NameTable.Add("Forms");
            this.id43_TerminalInfoSysClass = base.Reader.NameTable.Add("TerminalInfoSysClass");
            this.id101_VersionuptoolUpdater = base.Reader.NameTable.Add("VersionuptoolUpdater");
            this.id296_ApplicationClass = base.Reader.NameTable.Add("ApplicationClass");
            this.id156_UserId = base.Reader.NameTable.Add("UserId");
            this.id274_JobInfo = base.Reader.NameTable.Add("JobInfo");
            this.id310_HistroyUser = base.Reader.NameTable.Add("HistroyUser");
            this.id27_TerminalInfo = base.Reader.NameTable.Add("TerminalInfo");
            this.id392_custompath = base.Reader.NameTable.Add("custompath");
            this.id302_DataViewSplitter = base.Reader.NameTable.Add("DataViewSplitter");
            this.id342_BackStatusStr = base.Reader.NameTable.Add("BackStatusStr");
            this.id356_BinName = base.Reader.NameTable.Add("BinName");
            this.id106_DataStatus = base.Reader.NameTable.Add("DataStatus");
            this.id66_HistoryUser = base.Reader.NameTable.Add("HistoryUser");
            this.id103_VersionSettings = base.Reader.NameTable.Add("VersionSettings");
            this.id442_InformClass = base.Reader.NameTable.Add("InformClass");
            this.id359_MsgLog = base.Reader.NameTable.Add("MsgLog");
            this.id19_ProtocolType = base.Reader.NameTable.Add("ProtocolType");
            this.id140_TimeStamp = base.Reader.NameTable.Add("TimeStamp");
            this.id33_ArrayOfOutCodeClass1 = base.Reader.NameTable.Add("ArrayOfOutCodeClass1");
            this.id107_Signflg = base.Reader.NameTable.Add("Signflg");
            this.id400_supportfile = base.Reader.NameTable.Add("support-file");
            this.id220_BinList = base.Reader.NameTable.Add("BinList");
            this.id326_SendTimeOut = base.Reader.NameTable.Add("SendTimeOut");
            this.id136_NaccsData = base.Reader.NameTable.Add("NaccsData");
            this.id221_No = base.Reader.NameTable.Add("No");
            this.id4_ReceiveNotice = base.Reader.NameTable.Add("ReceiveNotice");
            this.id443_InfoMode = base.Reader.NameTable.Add("InfoMode");
            this.id313_URL = base.Reader.NameTable.Add("URL");
            this.id17_PathItem = base.Reader.NameTable.Add("PathItem");
            this.id366_ProxyAccount = base.Reader.NameTable.Add("ProxyAccount");
            this.id300_Bottom = base.Reader.NameTable.Add("Bottom");
            this.id241_RemoveDate = base.Reader.NameTable.Add("RemoveDate");
            this.id399_defaulttermmessagepath = base.Reader.NameTable.Add("default-term-message-path");
            this.id144_Attach = base.Reader.NameTable.Add("Attach");
            this.id226_KeyClass = base.Reader.NameTable.Add("KeyClass");
            this.id219_DoublePrint = base.Reader.NameTable.Add("DoublePrint");
            this.id446_SoundClass = base.Reader.NameTable.Add("SoundClass");
            this.id334_MailBox = base.Reader.NameTable.Add("MailBox");
            this.id405_systemenvironmentsetup = base.Reader.NameTable.Add("system-environment-setup");
            this.id165_DataType = base.Reader.NameTable.Add("DataType");
            this.id437_OutCodeList = base.Reader.NameTable.Add("OutCodeList");
            this.id379_DebugFunctionClass = base.Reader.NameTable.Add("DebugFunctionClass");
            this.id423_dataviewpath = base.Reader.NameTable.Add("dataview-path");
            this.id65_BatchSend = base.Reader.NameTable.Add("BatchSend");
            this.id292_ToolBarClass = base.Reader.NameTable.Add("ToolBarClass");
            this.id196_DateAlignment = base.Reader.NameTable.Add("DateAlignment");
            this.id383_TermLogicalName = base.Reader.NameTable.Add("TermLogicalName");
            this.id91_JobKey = base.Reader.NameTable.Add("JobKey");
            this.id319_TraceFileSize = base.Reader.NameTable.Add("TraceFileSize");
            this.id83_ConfigFiles = base.Reader.NameTable.Add("ConfigFiles");
            this.id363_ProxyName = base.Reader.NameTable.Add("ProxyName");
            this.id77_CustomizeMenu = base.Reader.NameTable.Add("CustomizeMenu");
            this.id441_PrinterInfoList = base.Reader.NameTable.Add("PrinterInfoList");
            this.id41_TermMessageSetClass = base.Reader.NameTable.Add("TermMessageSetClass");
            this.id51_NodeSorter = base.Reader.NameTable.Add("NodeSorter");
            this.id104_PrintManager = base.Reader.NameTable.Add("PrintManager");
            this.id259_Address = base.Reader.NameTable.Add("Address");
            this.id202_Id = base.Reader.NameTable.Add("Id");
            this.id175_Message = base.Reader.NameTable.Add("Message");
            this.id130_ArrayOfReceiveInformClass1 = base.Reader.NameTable.Add("ArrayOfReceiveInformClass1");
            this.id215_DocName = base.Reader.NameTable.Add("DocName");
            this.id21_UserKindType = base.Reader.NameTable.Add("UserKindType");
            this.id360_HttpOptionClass = base.Reader.NameTable.Add("HttpOptionClass");
            this.id438_DefaultInfoClass = base.Reader.NameTable.Add("DefaultInfoClass");
            this.id162_DataInfo = base.Reader.NameTable.Add("DataInfo");
            this.id317_Server = base.Reader.NameTable.Add("Server");
            this.id55_User = base.Reader.NameTable.Add("User");
            this.id131_ReceiveInformClass = base.Reader.NameTable.Add("ReceiveInformClass");
            this.id417_jobsetup = base.Reader.NameTable.Add("job-setup");
            this.id427_logpath = base.Reader.NameTable.Add("log-path");
            this.id375_InteractiveInfoClass = base.Reader.NameTable.Add("InteractiveInfoClass");
            this.id374_TraceOutput = base.Reader.NameTable.Add("TraceOutput");
            this.id233_IsCancel = base.Reader.NameTable.Add("IsCancel");
            this.id163_Div = base.Reader.NameTable.Add("Div");
            this.id149_Header = base.Reader.NameTable.Add("Header");
            this.id62_DataViewForm = base.Reader.NameTable.Add("DataViewForm");
            this.id47_MailInfoSysClass = base.Reader.NameTable.Add("MailInfoSysClass");
            this.id117_ArrayOfTypeClass = base.Reader.NameTable.Add("ArrayOfTypeClass");
            this.id318_TraceFileName = base.Reader.NameTable.Add("TraceFileName");
            this.id380_Check_Off = base.Reader.NameTable.Add("Check_Off");
            this.id121_FileSave = base.Reader.NameTable.Add("FileSave");
            this.id164_EndFlag = base.Reader.NameTable.Add("EndFlag");
            this.id63_ResultCodeColor = base.Reader.NameTable.Add("ResultCodeColor");
            this.id351_Receiver = base.Reader.NameTable.Add("Receiver");
            this.id315_NACCS_ComLog2 = base.Reader.NameTable.Add("NACCS_ComLog2");
            this.id18_PathType = base.Reader.NameTable.Add("PathType");
            this.id277_JonInfo3 = base.Reader.NameTable.Add("JonInfo3");
            this.id13_Bin = base.Reader.NameTable.Add("Bin");
            this.id182_SendFileClass = base.Reader.NameTable.Add("SendFileClass");
            this.id263_JobOptionClass = base.Reader.NameTable.Add("JobOptionClass");
            this.id381_UserKindFlag = base.Reader.NameTable.Add("UserKindFlag");
            this.id294_Job = base.Reader.NameTable.Add("Job");
            this.id53_TestServer = base.Reader.NameTable.Add("TestServer");
            this.id148_IsPrinted = base.Reader.NameTable.Add("IsPrinted");
            this.id151_item = base.Reader.NameTable.Add("item");
            this.id288_WindowClass = base.Reader.NameTable.Add("WindowClass");
            this.id330_SMTPServerName = base.Reader.NameTable.Add("SMTPServerName");
            this.id312_Offset = base.Reader.NameTable.Add("Offset");
            this.id239_HelpSettings = base.Reader.NameTable.Add("HelpSettings");
            this.id97_JobFormFactory = base.Reader.NameTable.Add("JobFormFactory");
            this.id26_UserEnvironment = base.Reader.NameTable.Add("UserEnvironment");
            this.id92_Key = base.Reader.NameTable.Add("Key");
            this.id60_HistoryJobs = base.Reader.NameTable.Add("HistoryJobs");
            this.id38_NaccsHeader = base.Reader.NameTable.Add("NaccsHeader");
            this.id372_AutoTm = base.Reader.NameTable.Add("AutoTm");
            this.id28_DebugFunction = base.Reader.NameTable.Add("DebugFunction");
            this.id382_TerminalInfoClass = base.Reader.NameTable.Add("TerminalInfoClass");
            this.id135_MessageKind = base.Reader.NameTable.Add("MessageKind");
            this.id352_LogFile = base.Reader.NameTable.Add("LogFile");
            this.id186_FileSaveMode = base.Reader.NameTable.Add("FileSaveMode");
            this.id172_Length = base.Reader.NameTable.Add("Length");
            this.id279_AirSea = base.Reader.NameTable.Add("AirSea");
            this.id195_DateItem = base.Reader.NameTable.Add("DateItem");
            this.id210_DataViewClass = base.Reader.NameTable.Add("DataViewClass");
            this.id35_ArrayOfBinClass1 = base.Reader.NameTable.Add("ArrayOfBinClass1");
            this.id283_LastLogonClass = base.Reader.NameTable.Add("LastLogonClass");
            this.id142_Contained = base.Reader.NameTable.Add("Contained");
            this.id201_GStampInfo = base.Reader.NameTable.Add("GStampInfo");
            this.id251_GatewayPOPServerPort = base.Reader.NameTable.Add("GatewayPOPServerPort");
            this.id336_UserKind = base.Reader.NameTable.Add("UserKind");
            this.id69_ArrayOfDetailsClass = base.Reader.NameTable.Add("ArrayOfDetailsClass");
            this.id95_ArrayOfString4 = base.Reader.NameTable.Add("ArrayOfString4");
            this.id235_HelpPathClass = base.Reader.NameTable.Add("HelpPathClass");
            this.id229_KeyList = base.Reader.NameTable.Add("KeyList");
            this.id433_jetrasexe = base.Reader.NameTable.Add("jetras-exe");
            this.id194_TopItem = base.Reader.NameTable.Add("TopItem");
            this.id204_UserCode = base.Reader.NameTable.Add("UserCode");
            this.id29_InteractiveInfo = base.Reader.NameTable.Add("InteractiveInfo");
            this.id209_Folder = base.Reader.NameTable.Add("Folder");
            this.id50_ListRec = base.Reader.NameTable.Add("ListRec");
            this.id353_LogSize = base.Reader.NameTable.Add("LogSize");
            this.id54_NetTestMode = base.Reader.NameTable.Add("NetTestMode");
            this.id387_Versionup = base.Reader.NameTable.Add("Versionup");
            this.id398_systemerrormessage = base.Reader.NameTable.Add("system-error-message");
            this.id166_SendGuard = base.Reader.NameTable.Add("SendGuard");
            this.id396_defaulthelppath = base.Reader.NameTable.Add("default-help-path");
            this.id370_AddDomainName = base.Reader.NameTable.Add("AddDomainName");
            this.id49_LogFileLengthSysClass = base.Reader.NameTable.Add("LogFileLengthSysClass");
            this.id73_PrintStatus = base.Reader.NameTable.Add("PrintStatus");
            this.id391_projectpath = base.Reader.NameTable.Add("project-path");
            this.id98_ULogClass = base.Reader.NameTable.Add("ULogClass");
            this.id420_jobtreeroot = base.Reader.NameTable.Add("jobtree-root");
            this.id15_OutCode = base.Reader.NameTable.Add("OutCode");
            this.id227_Sckey = base.Reader.NameTable.Add("Sckey");
            this.id261_BatchSendClass = base.Reader.NameTable.Add("BatchSendClass");
            this.id190_FileNaming = base.Reader.NameTable.Add("FileNaming");
            this.id61_ColumnWidth = base.Reader.NameTable.Add("ColumnWidth");
            this.id301_MainSplitter = base.Reader.NameTable.Add("MainSplitter");
            this.id124_FileName = base.Reader.NameTable.Add("FileName");
            this.id105_ReportStatus = base.Reader.NameTable.Add("ReportStatus");
            this.id242_ResetID = base.Reader.NameTable.Add("ResetID");
            this.id413_user_key_setup = base.Reader.NameTable.Add("user_key_setup");
            this.id64_JobOption = base.Reader.NameTable.Add("JobOption");
            this.id270_DataViewFormClass = base.Reader.NameTable.Add("DataViewFormClass");
            this.id445_WaringMode = base.Reader.NameTable.Add("WaringMode");
            this.id377_AutoSendRecvTm = base.Reader.NameTable.Add("AutoSendRecvTm");
            this.id346_PortalURL = base.Reader.NameTable.Add("PortalURL");
            this.id56_Application = base.Reader.NameTable.Add("Application");
            this.id42_SystemEnvironment = base.Reader.NameTable.Add("SystemEnvironment");
            this.id436_FormsClass = base.Reader.NameTable.Add("FormsClass");
            this.id134_ButtonPatern = base.Reader.NameTable.Add("ButtonPatern");
            this.id57_ToolBar = base.Reader.NameTable.Add("ToolBar");
            this.id402_guideroot = base.Reader.NameTable.Add("guide-root");
            this.id188_Mode = base.Reader.NameTable.Add("Mode");
            this.id199_UnderItem3 = base.Reader.NameTable.Add("UnderItem3");
            this.id298_Top = base.Reader.NameTable.Add("Top");
            this.id406_printersetup = base.Reader.NameTable.Add("printer-setup");
            this.id324_ServerPort = base.Reader.NameTable.Add("ServerPort");
            this.id174_Code = base.Reader.NameTable.Add("Code");
            this.id129_TermMessageSet = base.Reader.NameTable.Add("TermMessageSet");
            this.id93_ArrayOfPrinterInfoClass1 = base.Reader.NameTable.Add("ArrayOfPrinterInfoClass1");
            this.id1_AbstractSettings = base.Reader.NameTable.Add("AbstractSettings");
            this.id9_Inform = base.Reader.NameTable.Add("Inform");
            this.id325_PingPoint = base.Reader.NameTable.Add("PingPoint");
            this.id79_CommonPath = base.Reader.NameTable.Add("CommonPath");
            this.id89_DataFactory = base.Reader.NameTable.Add("DataFactory");
            this.id409_filesavesetup = base.Reader.NameTable.Add("file-save-setup");
            this.id217_Template = base.Reader.NameTable.Add("Template");
            this.id100_JobPanelStatus = base.Reader.NameTable.Add("JobPanelStatus");
            this.id393_jobhistory = base.Reader.NameTable.Add("job-history");
            this.id361_CertificateHash = base.Reader.NameTable.Add("CertificateHash");
            this.id305_Clock = base.Reader.NameTable.Add("Clock");
            this.id371_AutoMode = base.Reader.NameTable.Add("AutoMode");
            this.id225_Orientation = base.Reader.NameTable.Add("Orientation");
            this.id331_SMTPServerPort = base.Reader.NameTable.Add("SMTPServerPort");
            this.id159_Subject = base.Reader.NameTable.Add("Subject");
            this.id146_IsRead = base.Reader.NameTable.Add("IsRead");
            this.id115_TermClass = base.Reader.NameTable.Add("TermClass");
            this.id96_string = base.Reader.NameTable.Add("string");
            this.id280_PrintName = base.Reader.NameTable.Add("PrintName");
            this.id75_DatasetStorage = base.Reader.NameTable.Add("DatasetStorage");
            this.id422_customlistlink = base.Reader.NameTable.Add("custom-list-link");
            this.id86_OptionCertification = base.Reader.NameTable.Add("OptionCertification");
            this.id126_Details = base.Reader.NameTable.Add("Details");
            this.id40_ArrayOfTermMessageSetClass = base.Reader.NameTable.Add("ArrayOfTermMessageSetClass");
            this.id231_AllCertificationClass = base.Reader.NameTable.Add("AllCertificationClass");
            this.id289_UserInput = base.Reader.NameTable.Add("UserInput");
            this.id256_HistorySearchClass = base.Reader.NameTable.Add("HistorySearchClass");
        }

        private DefaultInfoClass Read10_DefaultInfoClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id438_DefaultInfoClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DefaultInfoClass o = new DefaultInfoClass();
            bool[] flagArray = new bool[5];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id188_Mode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Mode = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id10_Printer)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Printer = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id355_BinNo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.BinNo = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id356_BinName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.BinName = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id439_SizeWarning)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.SizeWarning = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[4] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Mode, :Printer, :BinNo, :BinName, :SizeWarning");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Mode, :Printer, :BinNo, :BinName, :SizeWarning");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private TermClass Read100_TermClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id115_TermClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            TermClass o = new TermClass();
            bool[] flagArray = new bool[9];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id202_Id)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.Id = XmlConvert.ToInt32(base.Reader.Value);
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":Id");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id203_Priority)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Priority = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id204_UserCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UserCode = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id205_JobCoode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.JobCoode = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id15_OutCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.OutCode = base.Reader.ReadElementString();
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id206_UnOpen)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UnOpen = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id207_BankNumber)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.BankNumber = base.Reader.ReadElementString();
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id208_BankWithdrawday)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.BankWithdrawday = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id209_Folder)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Folder = base.Reader.ReadElementString();
                        flagArray[8] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Priority, :UserCode, :JobCoode, :OutCode, :UnOpen, :BankNumber, :BankWithdrawday, :Folder");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Priority, :UserCode, :JobCoode, :OutCode, :UnOpen, :BankNumber, :BankWithdrawday, :Folder");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DataViewClass Read101_DataViewClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id210_DataViewClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DataViewClass o = new DataViewClass();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id211_upTime)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.upTime = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id212_AutoBackup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AutoBackup = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":upTime, :AutoBackup");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":upTime, :AutoBackup");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private MessageClassify Read102_MessageClassify(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id111_MessageClassify) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            MessageClassify o = new MessageClassify();
            if (o.TermList == null)
            {
                o.TermList = new TermListClass();
            }
            TermListClass termList = o.TermList;
            if (o.Folders == null)
            {
                o.Folders = new StringList();
            }
            StringList folders = o.Folders;
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id213_TermList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.TermList == null)
                            {
                                o.TermList = new TermListClass();
                            }
                            TermListClass class2 = o.TermList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id112_Term) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class2 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class2.Add(this.Read100_TermClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":Term");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":Term");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else if ((base.Reader.LocalName == this.id214_Folders) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.Folders == null)
                            {
                                o.Folders = new StringList();
                            }
                            StringList list = o.Folders;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num5 = 0;
                                int num6 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id141_UserFolder) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (base.ReadNull())
                                            {
                                                list.Add(null);
                                            }
                                            else
                                            {
                                                list.Add(base.Reader.ReadElementString());
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":UserFolder");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":UserFolder");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num5, ref num6);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id113_DataView)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DataView = this.Read101_DataViewClass(false, true);
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":TermList, :Folders, :DataView");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":TermList, :Folders, :DataView");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private UserKeySet Read103_UserKeySet(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id116_UserKeySet) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            UserKeySet o = new UserKeySet();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private TypeClass Read104_TypeClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id118_TypeClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            TypeClass o = new TypeClass();
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id165_DataType)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.DataType = base.Reader.Value;
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":DataType");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id186_FileSaveMode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.FileSaveMode = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id180_Dir)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Dir = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":FileSaveMode, :Dir");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":FileSaveMode, :Dir");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private GStampInfoClass Read105_GStampInfoClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id192_GStampInfoClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            GStampInfoClass o = new GStampInfoClass();
            bool[] flagArray = new bool[7];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id193_GStompOn)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.GStampOn = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id194_TopItem)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TopItem = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id195_DateItem)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DateItem = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id196_DateAlignment)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DateAlignment = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id197_UnderItem1)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UnderItem1 = base.Reader.ReadElementString();
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id198_UnderItem2)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UnderItem2 = base.Reader.ReadElementString();
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id199_UnderItem3)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UnderItem3 = base.Reader.ReadElementString();
                        flagArray[6] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":GStompOn, :TopItem, :DateItem, :DateAlignment, :UnderItem1, :UnderItem2, :UnderItem3");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":GStompOn, :TopItem, :DateItem, :DateAlignment, :UnderItem1, :UnderItem2, :UnderItem3");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private GStampSettings Read106_GStampSettings(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id200_GStampSettings) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            GStampSettings o = new GStampSettings();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id201_GStampInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GStampInfo = this.Read105_GStampInfoClass(false, true);
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":GStampInfo");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":GStampInfo");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private AllFileSaveClass Read107_AllFileSaveClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id187_AllFileSaveClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            AllFileSaveClass o = new AllFileSaveClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id188_Mode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Mode = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Mode");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Mode");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private FileNameClass Read108_FileNameClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id184_FileNameClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            FileNameClass o = new FileNameClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id185_FileNameRule)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.FileNameRule = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":FileNameRule");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":FileNameRule");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private SendFileClass Read109_SendFileClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id182_SendFileClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            SendFileClass o = new SendFileClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id183_SendUser)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SendUser = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":SendUser");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":SendUser");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private BinClass Read11_BinClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id36_BinClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            BinClass o = new BinClass();
            bool[] flagArray = new bool[6];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id221_No)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.No = XmlConvert.ToInt32(base.Reader.Value);
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":No");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id218_Name)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Name = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id222_L)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.L = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id223_T)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.T = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id224_Size)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Size = base.Reader.ReadElementString();
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id225_Orientation)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Orientation = base.Reader.ReadElementString();
                        flagArray[5] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Name, :L, :T, :Size, :Orientation");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Name, :L, :T, :Size, :Orientation");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private FileSave Read110_FileSave(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id121_FileSave) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            FileSave o = new FileSave();
            if (o.TypeList == null)
            {
                o.TypeList = new TypeListClass();
            }
            TypeListClass typeList = o.TypeList;
            if (o.DetailsList == null)
            {
                o.DetailsList = new DetailsListClass();
            }
            DetailsListClass detailsList = o.DetailsList;
            bool[] flagArray = new bool[5];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id122_AllFileSave)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.AllFileSave = this.Read107_AllFileSaveClass(false, true);
                        flagArray[0] = true;
                    }
                    else if ((base.Reader.LocalName == this.id189_TypeList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.TypeList == null)
                            {
                                o.TypeList = new TypeListClass();
                            }
                            TypeListClass class2 = o.TypeList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id123_Type) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class2 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class2.Add(this.Read104_TypeClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":Type");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":Type");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id190_FileNaming)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.FileNaming = this.Read108_FileNameClass(false, true);
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id125_SendFile)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SendFile = this.Read109_SendFileClass(false, true);
                        flagArray[3] = true;
                    }
                    else if ((base.Reader.LocalName == this.id191_DetailsList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.DetailsList == null)
                            {
                                o.DetailsList = new DetailsListClass();
                            }
                            DetailsListClass class3 = o.DetailsList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num5 = 0;
                                int num6 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id126_Details) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class3 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class3.Add(this.Read64_DetailsClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":Details");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":Details");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num5, ref num6);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":AllFileSave, :TypeList, :FileNaming, :SendFile, :DetailsList");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":AllFileSave, :TypeList, :FileNaming, :SendFile, :DetailsList");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ListType Read111_ListType(string s)
        {
            switch (s)
            {
                case "UserList":
                    return ListType.UserList;

                case "MailList":
                    return ListType.MailList;
            }
            throw base.CreateUnknownConstantException(s, typeof(ListType));
        }

        private TermMessage Read112_TermMessage(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id128_TermMessage) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            TermMessage o = new TermMessage();
            if (o.TermMessageSetList == null)
            {
                o.TermMessageSetList = new TermMessageSetListClass();
            }
            TermMessageSetListClass termMessageSetList = o.TermMessageSetList;
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id179_TermMessageSetList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.TermMessageSetList == null)
                            {
                                o.TermMessageSetList = new TermMessageSetListClass();
                            }
                            TermMessageSetListClass class2 = o.TermMessageSetList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id129_TermMessageSet) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class2 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class2.Add(this.Read36_TermMessageSetClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":TermMessageSet");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":TermMessageSet");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":TermMessageSetList");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":TermMessageSetList");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ResourceManager Read113_ResourceManager(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id132_ResourceManager) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ResourceManager o = new ResourceManager();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private JobStyle Read114_JobStyle(string s)
        {
            switch (s)
            {
                case "normal":
                    return JobStyle.normal;

                case "combi":
                    return JobStyle.combi;

                case "divide":
                    return JobStyle.divide;
            }
            throw base.CreateUnknownConstantException(s, typeof(JobStyle));
        }

        private NaccsData Read115_NaccsData(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id136_NaccsData) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            NaccsData o = new NaccsData();
            List<string> items = o.Items;
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            int num = 0;
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                string[] strArray;
                int num4;
                List<string> list;
                if (base.Reader.NodeType != XmlNodeType.Element)
                {
                    goto Label_07AD;
                }
                switch (num)
                {
                    case 0:
                        if ((base.Reader.LocalName == this.id137_ID) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.ID = base.Reader.ReadElementString();
                        }
                        num = 1;
                        goto Label_07B5;

                    case 1:
                        if ((base.Reader.LocalName == this.id138_Style) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.Style = this.Read114_JobStyle(base.Reader.ReadElementString());
                        }
                        num = 2;
                        goto Label_07B5;

                    case 2:
                        if ((base.Reader.LocalName == this.id139_Status) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.Status = this.Read95_DataStatus(base.Reader.ReadElementString());
                        }
                        num = 3;
                        goto Label_07B5;

                    case 3:
                        if ((base.Reader.LocalName == this.id140_TimeStamp) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.TimeStamp = XmlSerializationReader.ToDateTime(base.Reader.ReadElementString());
                        }
                        num = 4;
                        goto Label_07B5;

                    case 4:
                        if ((base.Reader.LocalName == this.id141_UserFolder) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.UserFolder = base.Reader.ReadElementString();
                        }
                        num = 5;
                        goto Label_07B5;

                    case 5:
                        if ((base.Reader.LocalName == this.id142_Contained) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.Contained = this.Read97_ContainedType(base.Reader.ReadElementString());
                        }
                        num = 6;
                        goto Label_07B5;

                    case 6:
                        if ((base.Reader.LocalName == this.id143_AttachFolder) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.AttachFolder = base.Reader.ReadElementString();
                        }
                        num = 7;
                        goto Label_07B5;

                    case 7:
                        if ((base.Reader.LocalName != this.id144_Attach) || (base.Reader.NamespaceURI != this.id2_Item))
                        {
                            goto Label_0490;
                        }
                        if (base.ReadNull())
                        {
                            goto Label_07B5;
                        }
                        strArray = null;
                        num4 = 0;
                        if (!base.Reader.IsEmptyElement)
                        {
                            break;
                        }
                        base.Reader.Skip();
                        goto Label_046B;

                    case 8:
                        if ((base.Reader.LocalName == this.id145_IsDeleted) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.IsDeleted = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        num = 9;
                        goto Label_07B5;

                    case 9:
                        if ((base.Reader.LocalName == this.id146_IsRead) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.IsRead = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        num = 10;
                        goto Label_07B5;

                    case 10:
                        if ((base.Reader.LocalName == this.id147_IsSaved) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.IsSaved = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        num = 11;
                        goto Label_07B5;

                    case 11:
                        if ((base.Reader.LocalName == this.id148_IsPrinted) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.IsPrinted = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        num = 12;
                        goto Label_07B5;

                    case 12:
                        if ((base.Reader.LocalName == this.id149_Header) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.NaccsHeader = this.Read34_NaccsHeader(false, true);
                        }
                        num = 13;
                        goto Label_07B5;

                    case 13:
                        if ((base.Reader.LocalName != this.id150_JobData) || (base.Reader.NamespaceURI != this.id2_Item))
                        {
                            goto Label_071E;
                        }
                        if (!base.ReadNull())
                        {
                            list = o.Items;
                            if ((list != null) && !base.Reader.IsEmptyElement)
                            {
                                goto Label_0643;
                            }
                            base.Reader.Skip();
                        }
                        goto Label_07B5;

                    case 14:
                        if ((base.Reader.LocalName == this.id107_Signflg) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.Signflg = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        num = 15;
                        goto Label_07B5;

                    case 15:
                        if ((base.Reader.LocalName == this.id152_SignFileFolder) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            o.SignFileFolder = base.Reader.ReadElementString();
                        }
                        num = 0x10;
                        goto Label_07B5;

                    default:
                        base.UnknownNode(o, null);
                        goto Label_07B5;
                }
                base.Reader.ReadStartElement();
                base.Reader.MoveToContent();
                int num5 = 0;
                int num6 = base.ReaderCount;
                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                {
                    if (base.Reader.NodeType == XmlNodeType.Element)
                    {
                        if ((base.Reader.LocalName == this.id124_FileName) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            if (base.ReadNull())
                            {
                                strArray = (string[]) base.EnsureArrayIndex(strArray, num4, typeof(string));
                                strArray[num4++] = null;
                            }
                            else
                            {
                                strArray = (string[]) base.EnsureArrayIndex(strArray, num4, typeof(string));
                                strArray[num4++] = base.Reader.ReadElementString();
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":FileName");
                        }
                    }
                    else
                    {
                        base.UnknownNode(null, ":FileName");
                    }
                    base.Reader.MoveToContent();
                    base.CheckReaderCount(ref num5, ref num6);
                }
                base.ReadEndElement();
            Label_046B:
                o.AttachFile = (string[]) base.ShrinkArray(strArray, num4, typeof(string), false);
                goto Label_07B5;
            Label_0490:
                num = 8;
                goto Label_07B5;
            Label_0643:
                base.Reader.ReadStartElement();
                base.Reader.MoveToContent();
                int num7 = 0;
                int num8 = base.ReaderCount;
                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                {
                    if (base.Reader.NodeType == XmlNodeType.Element)
                    {
                        if ((base.Reader.LocalName == this.id151_item) && (base.Reader.NamespaceURI == this.id2_Item))
                        {
                            if (base.ReadNull())
                            {
                                list.Add(null);
                            }
                            else
                            {
                                list.Add(base.Reader.ReadElementString());
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":item");
                        }
                    }
                    else
                    {
                        base.UnknownNode(null, ":item");
                    }
                    base.Reader.MoveToContent();
                    base.CheckReaderCount(ref num7, ref num8);
                }
                base.ReadEndElement();
                goto Label_07B5;
            Label_071E:
                num = 14;
                goto Label_07B5;
            Label_07AD:
                base.UnknownNode(o, null);
            Label_07B5:
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ButtonPatern Read116_ButtonPatern(string s)
        {
            switch (s)
            {
                case "OK_ONLY":
                    return ButtonPatern.OK_ONLY;

                case "OK_CANCEL":
                    return ButtonPatern.OK_CANCEL;

                case "YES_NO":
                    return ButtonPatern.YES_NO;

                case "YES_NO_CANCEL":
                    return ButtonPatern.YES_NO_CANCEL;
            }
            throw base.CreateUnknownConstantException(s, typeof(ButtonPatern));
        }

        private MessageKind Read117_MessageKind(string s)
        {
            switch (s)
            {
                case "Information":
                    return MessageKind.Information;

                case "Error":
                    return MessageKind.Error;

                case "Warning":
                    return MessageKind.Warning;

                case "Confirmation":
                    return MessageKind.Confirmation;
            }
            throw base.CreateUnknownConstantException(s, typeof(MessageKind));
        }

        public object Read118_AbstractSettings()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id1_AbstractSettings) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read2_AbstractSettings(true, true);
            }
            base.UnknownNode(null, ":AbstractSettings");
            return null;
        }

        public object Read119_AbstractConfig()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id3_AbstractConfig) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read3_AbstractConfig(true, true);
            }
            base.UnknownNode(null, ":AbstractConfig");
            return null;
        }

        private PrinterInfoClass Read12_PrinterInfoClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id94_PrinterInfoClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            PrinterInfoClass o = new PrinterInfoClass();
            if (o.BinList == null)
            {
                o.BinList = new BinListClass();
            }
            BinListClass binList = o.BinList;
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id218_Name)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.Name = base.Reader.Value;
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":Name");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id219_DoublePrint)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DoublePrint = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else if ((base.Reader.LocalName == this.id220_BinList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.BinList == null)
                            {
                                o.BinList = new BinListClass();
                            }
                            BinListClass class3 = o.BinList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id13_Bin) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class3 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class3.Add(this.Read11_BinClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":Bin");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":Bin");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":DoublePrint, :BinList");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":DoublePrint, :BinList");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        public object Read120_ReceiveNotice()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id4_ReceiveNotice) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read9_ReceiveNotice(true, true);
            }
            base.UnknownNode(null, ":ReceiveNotice");
            return null;
        }

        public object Read121_ListItem()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id5_ListItem) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read4_ListItem(true, true);
            }
            base.UnknownNode(null, ":ListItem");
            return null;
        }

        public object Read122_ReceiveInform()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id6_ReceiveInform) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read5_ReceiveInformClass(true, true);
            }
            base.UnknownNode(null, ":ReceiveInform");
            return null;
        }

        public object Read123_Sound()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id7_Sound) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read6_SoundClass(true, true);
            }
            base.UnknownNode(null, ":Sound");
            return null;
        }

        public object Read124_WarningInform()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id8_WarningInform) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read7_WarningInformClass(true, true);
            }
            base.UnknownNode(null, ":WarningInform");
            return null;
        }

        public object Read125_Inform()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id9_Inform) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read8_InformClass(true, true);
            }
            base.UnknownNode(null, ":Inform");
            return null;
        }

        public object Read126_Printer()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id10_Printer) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read15_PrintSetups(true, true);
            }
            base.UnknownNode(null, ":Printer");
            return null;
        }

        public object Read127_DefaultInfo()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id11_DefaultInfo) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read10_DefaultInfoClass(true, true);
            }
            base.UnknownNode(null, ":DefaultInfo");
            return null;
        }

        public object Read128_PrinterInfo()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id12_PrinterInfo) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read12_PrinterInfoClass(true, true);
            }
            base.UnknownNode(null, ":PrinterInfo");
            return null;
        }

        public object Read129_Bin()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id13_Bin) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read11_BinClass(true, true);
            }
            base.UnknownNode(null, ":Bin");
            return null;
        }

        private OutCodeClass Read13_OutCodeClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id34_OutCodeClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            OutCodeClass o = new OutCodeClass();
            bool[] flagArray = new bool[6];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id218_Name)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.Name = base.Reader.Value;
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":Name");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id188_Mode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Mode = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id354_Count)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Count = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id10_Printer)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Printer = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id355_BinNo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.BinNo = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id356_BinName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.BinName = base.Reader.ReadElementString();
                        flagArray[5] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Mode, :Count, :Printer, :BinNo, :BinName");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Mode, :Count, :Printer, :BinNo, :BinName");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        public object Read130_Forms()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id14_Forms) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read14_FormsClass(true, true);
            }
            base.UnknownNode(null, ":Forms");
            return null;
        }

        public object Read131_OutCode()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id15_OutCode) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read13_OutCodeClass(true, true);
            }
            base.UnknownNode(null, ":OutCode");
            return null;
        }

        public object Read132_path()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id16_path) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read18_PathInfo(true, true);
            }
            base.UnknownNode(null, ":path");
            return null;
        }

        public object Read133_PathItem()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id17_PathItem) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read17_PathItem(true);
            }
            base.UnknownNode(null, ":PathItem");
            return null;
        }

        public object Read134_PathType()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id18_PathType) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read16_PathType(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":PathType");
            return null;
        }

        public object Read135_ProtocolType()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id19_ProtocolType) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read19_ProtocolType(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":ProtocolType");
            return null;
        }

        public object Read136_SettingDivType()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id20_SettingDivType) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read20_SettingDivType(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":SettingDivType");
            return null;
        }

        public object Read137_UserKindType()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id21_UserKindType) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read21_UserKindType(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":UserKindType");
            return null;
        }

        public object Read138_KeyListClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id22_KeyListClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read24_KeyListClass(true, true);
            }
            base.UnknownNode(null, ":KeyListClass");
            return null;
        }

        public object Read139_UserKeyType()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id23_UserKeyType) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read25_UserKeyType(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":UserKeyType");
            return null;
        }

        private FormsClass Read14_FormsClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id436_FormsClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            FormsClass o = new FormsClass();
            if (o.OutCodeList == null)
            {
                o.OutCodeList = new OutCodeListClass();
            }
            OutCodeListClass outCodeList = o.OutCodeList;
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id437_OutCodeList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.OutCodeList == null)
                            {
                                o.OutCodeList = new OutCodeListClass();
                            }
                            OutCodeListClass class3 = o.OutCodeList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id15_OutCode) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class3 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class3.Add(this.Read13_OutCodeClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":OutCode");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":OutCode");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":OutCodeList");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":OutCodeList");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        public object Read140_ArrayOfJobKeyClass()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id24_ArrayOfJobKeyClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new JobKeyListClass();
                    }
                    JobKeyListClass class2 = (JobKeyListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id25_JobKeyClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read23_JobKeyClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":JobKeyClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":JobKeyClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new JobKeyListClass();
                }
                JobKeyListClass class1 = (JobKeyListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfJobKeyClass");
            return obj2;
        }

        public object Read141_UserEnvironment()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id26_UserEnvironment) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read32_UserEnvironment(true, true);
            }
            base.UnknownNode(null, ":UserEnvironment");
            return null;
        }

        public object Read142_TerminalInfo()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id27_TerminalInfo) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read26_TerminalInfoClass(true, true);
            }
            base.UnknownNode(null, ":TerminalInfo");
            return null;
        }

        public object Read143_DebugFunction()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id28_DebugFunction) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read27_DebugFunctionClass(true, true);
            }
            base.UnknownNode(null, ":DebugFunction");
            return null;
        }

        public object Read144_InteractiveInfo()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id29_InteractiveInfo) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read28_InteractiveInfoClass(true, true);
            }
            base.UnknownNode(null, ":InteractiveInfo");
            return null;
        }

        public object Read145_MailInfo()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id30_MailInfo) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read29_MailInfoClass(true, true);
            }
            base.UnknownNode(null, ":MailInfo");
            return null;
        }

        public object Read146_HttpOption()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id31_HttpOption) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read30_HttpOptionClass(true, true);
            }
            base.UnknownNode(null, ":HttpOption");
            return null;
        }

        public object Read147_LogFileLength()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id32_LogFileLength) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read31_LogFileLengthClass(true, true);
            }
            base.UnknownNode(null, ":LogFileLength");
            return null;
        }

        public object Read148_ArrayOfOutCodeClass1()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id33_ArrayOfOutCodeClass1) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new OutCodeListClass();
                    }
                    OutCodeListClass class2 = (OutCodeListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id34_OutCodeClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read13_OutCodeClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":OutCodeClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":OutCodeClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new OutCodeListClass();
                }
                OutCodeListClass class1 = (OutCodeListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfOutCodeClass1");
            return obj2;
        }

        public object Read149_ArrayOfBinClass1()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id35_ArrayOfBinClass1) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new BinListClass();
                    }
                    BinListClass class2 = (BinListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id36_BinClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read11_BinClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":BinClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":BinClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new BinListClass();
                }
                BinListClass class1 = (BinListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfBinClass1");
            return obj2;
        }

        private PrintSetups Read15_PrintSetups(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id440_PrintSetups) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            PrintSetups o = new PrintSetups();
            if (o.PrinterInfoList == null)
            {
                o.PrinterInfoList = new PrinterInfoListClass();
            }
            PrinterInfoListClass printerInfoList = o.PrinterInfoList;
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id11_DefaultInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DefaultInfo = this.Read10_DefaultInfoClass(false, true);
                        flagArray[0] = true;
                    }
                    else if ((base.Reader.LocalName == this.id441_PrinterInfoList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.PrinterInfoList == null)
                            {
                                o.PrinterInfoList = new PrinterInfoListClass();
                            }
                            PrinterInfoListClass class2 = o.PrinterInfoList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id12_PrinterInfo) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class2 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class2.Add(this.Read12_PrinterInfoClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":PrinterInfo");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":PrinterInfo");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id14_Forms)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Forms = this.Read14_FormsClass(false, true);
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":DefaultInfo, :PrinterInfoList, :Forms");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":DefaultInfo, :PrinterInfoList, :Forms");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        public object Read150_WriterLog()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id37_WriterLog) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read33_WriterLog(true, true);
            }
            base.UnknownNode(null, ":WriterLog");
            return null;
        }

        public object Read151_NaccsHeader()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id38_NaccsHeader) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read34_NaccsHeader(true, true);
            }
            base.UnknownNode(null, ":NaccsHeader");
            return null;
        }

        public object Read152_DataControl()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id39_DataControl) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read35_DataControl(true, true);
            }
            base.UnknownNode(null, ":DataControl");
            return null;
        }

        public object Read153_ArrayOfTermMessageSetClass()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id40_ArrayOfTermMessageSetClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new TermMessageSetListClass();
                    }
                    TermMessageSetListClass class2 = (TermMessageSetListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id41_TermMessageSetClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read36_TermMessageSetClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":TermMessageSetClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":TermMessageSetClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new TermMessageSetListClass();
                }
                TermMessageSetListClass class1 = (TermMessageSetListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfTermMessageSetClass");
            return obj2;
        }

        public object Read154_SystemEnvironment()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id42_SystemEnvironment) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read44_SystemEnvironment(true, true);
            }
            base.UnknownNode(null, ":SystemEnvironment");
            return null;
        }

        public object Read155_TerminalInfoSysClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id43_TerminalInfoSysClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read37_TerminalInfoSysClass(true, true);
            }
            base.UnknownNode(null, ":TerminalInfoSysClass");
            return null;
        }

        public object Read156_InteractiveServerClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id44_InteractiveServerClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read38_InteractiveServerClass(true, true);
            }
            base.UnknownNode(null, ":InteractiveServerClass");
            return null;
        }

        public object Read157_InteractiveInfoSysClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id45_InteractiveInfoSysClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read39_InteractiveInfoSysClass(true, true);
            }
            base.UnknownNode(null, ":InteractiveInfoSysClass");
            return null;
        }

        public object Read158_MailServerClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id46_MailServerClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read40_MailServerClass(true, true);
            }
            base.UnknownNode(null, ":MailServerClass");
            return null;
        }

        public object Read159_MailInfoSysClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id47_MailInfoSysClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read41_MailInfoSysClass(true, true);
            }
            base.UnknownNode(null, ":MailInfoSysClass");
            return null;
        }

        private PathType Read16_PathType(string s)
        {
            switch (s)
            {
                case "User":
                    return PathType.User;

                case "Protocol":
                    return PathType.Protocol;

                case "Install":
                    return PathType.Install;

                case "Environment":
                    return PathType.Environment;

                case "MyDocuments":
                    return PathType.MyDocuments;

                case "Debug":
                    return PathType.Debug;
            }
            throw base.CreateUnknownConstantException(s, typeof(PathType));
        }

        public object Read160_BatchdocInfo()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id48_BatchdocInfo) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read42_BatchdocInfoSysClass(true, true);
            }
            base.UnknownNode(null, ":BatchdocInfo");
            return null;
        }

        public object Read161_LogFileLengthSysClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id49_LogFileLengthSysClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read43_LogFileLengthSysClass(true, true);
            }
            base.UnknownNode(null, ":LogFileLengthSysClass");
            return null;
        }

        public object Read162_ListRec()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id50_ListRec) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read45_ListRec(true);
            }
            base.UnknownNode(null, ":ListRec");
            return null;
        }

        public object Read163_NodeSorter()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id51_NodeSorter) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read46_NodeSorter(true, true);
            }
            base.UnknownNode(null, ":NodeSorter");
            return null;
        }

        public object Read164_IndexRec()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id52_IndexRec) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read47_IndexRec(true);
            }
            base.UnknownNode(null, ":IndexRec");
            return null;
        }

        public object Read165_TestServer()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id53_TestServer) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read48_TestServer(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":TestServer");
            return null;
        }

        public object Read166_NetTestMode()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id54_NetTestMode) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read49_NetTestMode(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":NetTestMode");
            return null;
        }

        public object Read167_User()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id55_User) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read59_User(true, true);
            }
            base.UnknownNode(null, ":User");
            return null;
        }

        public object Read168_Application()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id56_Application) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read50_ApplicationClass(true, true);
            }
            base.UnknownNode(null, ":Application");
            return null;
        }

        public object Read169_ToolBar()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id57_ToolBar) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read51_ToolBarClass(true, true);
            }
            base.UnknownNode(null, ":ToolBar");
            return null;
        }

        private PathItem Read17_PathItem(bool checkType)
        {
            PathItem item;
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            if ((checkType && (type != null)) && ((type.Name != this.id17_PathItem) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            try
            {
                item = (PathItem) Activator.CreateInstance(typeof(PathItem), BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, new object[0], null);
            }
            catch (MissingMethodException)
            {
                throw base.CreateInaccessibleConstructorException("global::Naccs.Core.Settings.PathItem");
            }
            catch (SecurityException)
            {
                throw base.CreateCtorHasSecurityException("global::Naccs.Core.Settings.PathItem");
            }
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id389_type)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    item.PathType = this.Read16_PathType(base.Reader.Value);
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(item, ":type");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return item;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                string str = null;
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(item, "");
                }
                else if (((base.Reader.NodeType == XmlNodeType.Text) || (base.Reader.NodeType == XmlNodeType.CDATA)) || ((base.Reader.NodeType == XmlNodeType.Whitespace) || (base.Reader.NodeType == XmlNodeType.SignificantWhitespace)))
                {
                    str = base.ReadString(str, false);
                    item.Value = str;
                }
                else
                {
                    base.UnknownNode(item, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return item;
        }

        public object Read170_Window()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id58_Window) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read52_WindowClass(true, true);
            }
            base.UnknownNode(null, ":Window");
            return null;
        }

        public object Read171_LastLogon()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id59_LastLogon) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read53_LastLogonClass(true, true);
            }
            base.UnknownNode(null, ":LastLogon");
            return null;
        }

        public object Read172_HistoryJobs()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id60_HistoryJobs) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read60_HistoryJobsClass(true, true);
            }
            base.UnknownNode(null, ":HistoryJobs");
            return null;
        }

        public object Read173_ColumnWidth()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id61_ColumnWidth) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read54_ColumnWidthClass(true, true);
            }
            base.UnknownNode(null, ":ColumnWidth");
            return null;
        }

        public object Read174_DataViewForm()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id62_DataViewForm) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read55_DataViewFormClass(true, true);
            }
            base.UnknownNode(null, ":DataViewForm");
            return null;
        }

        public object Read175_ResultCodeColor()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id63_ResultCodeColor) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read56_ResultCodeColorClass(true, true);
            }
            base.UnknownNode(null, ":ResultCodeColor");
            return null;
        }

        public object Read176_JobOption()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id64_JobOption) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read57_JobOptionClass(true, true);
            }
            base.UnknownNode(null, ":JobOption");
            return null;
        }

        public object Read177_BatchSend()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id65_BatchSend) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read58_BatchSendClass(true, true);
            }
            base.UnknownNode(null, ":BatchSend");
            return null;
        }

        public object Read178_HistoryUser()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id66_HistoryUser) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read61_HistoryUserClass(true, true);
            }
            base.UnknownNode(null, ":HistoryUser");
            return null;
        }

        public object Read179_HistoryAddress()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id67_HistoryAddress) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read62_HistoryAddressClass(true, true);
            }
            base.UnknownNode(null, ":HistoryAddress");
            return null;
        }

        private PathInfo Read18_PathInfo(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id390_PathInfo) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            PathInfo o = new PathInfo();
            bool[] flagArray = new bool[0x2d];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id391_projectpath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._projectpath = this.Read17_PathItem(true);
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id392_custompath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._custompath = this.Read17_PathItem(true);
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id393_jobhistory)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._jobHistoryName = this.Read17_PathItem(true);
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id394_inputhistoryroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._input_history_root = this.Read17_PathItem(true);
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id395_outcodetable)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._outCodeTable = this.Read17_PathItem(true);
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id396_defaulthelppath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._default_help_path = this.Read17_PathItem(true);
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id397_defaultgymerrpath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._default_gym_err_path = this.Read17_PathItem(true);
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id398_systemerrormessage)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._system_error_message = this.Read17_PathItem(true);
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id399_defaulttermmessagepath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._default_term_msg_path = this.Read17_PathItem(true);
                        flagArray[8] = true;
                    }
                    else if ((!flagArray[9] && (base.Reader.LocalName == this.id400_supportfile)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._supportFile = this.Read17_PathItem(true);
                        flagArray[9] = true;
                    }
                    else if ((!flagArray[10] && (base.Reader.LocalName == this.id401_websites)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._webSites = this.Read17_PathItem(true);
                        flagArray[10] = true;
                    }
                    else if ((!flagArray[11] && (base.Reader.LocalName == this.id402_guideroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._guideRoot = this.Read17_PathItem(true);
                        flagArray[11] = true;
                    }
                    else if ((!flagArray[12] && (base.Reader.LocalName == this.id403_optioncertificationsetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._optionCertificationSetup = this.Read17_PathItem(true);
                        flagArray[12] = true;
                    }
                    else if ((!flagArray[13] && (base.Reader.LocalName == this.id404_userenvironmentsetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._userEnvironmentSetup = this.Read17_PathItem(true);
                        flagArray[13] = true;
                    }
                    else if ((!flagArray[14] && (base.Reader.LocalName == this.id405_systemenvironmentsetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._systemEnvironmentSetup = this.Read17_PathItem(true);
                        flagArray[14] = true;
                    }
                    else if ((!flagArray[15] && (base.Reader.LocalName == this.id406_printersetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._printerSetup = this.Read17_PathItem(true);
                        flagArray[15] = true;
                    }
                    else if ((!flagArray[0x10] && (base.Reader.LocalName == this.id407_messageclassifysetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._messageClassifySetup = this.Read17_PathItem(true);
                        flagArray[0x10] = true;
                    }
                    else if ((!flagArray[0x11] && (base.Reader.LocalName == this.id408_dataviewsetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._dataviewSetup = this.Read17_PathItem(true);
                        flagArray[0x11] = true;
                    }
                    else if ((!flagArray[0x12] && (base.Reader.LocalName == this.id409_filesavesetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._file_save_setup = this.Read17_PathItem(true);
                        flagArray[0x12] = true;
                    }
                    else if ((!flagArray[0x13] && (base.Reader.LocalName == this.id410_usersetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._user_setup = this.Read17_PathItem(true);
                        flagArray[0x13] = true;
                    }
                    else if ((!flagArray[20] && (base.Reader.LocalName == this.id411_receivenoticesetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._receive_notice_setup = this.Read17_PathItem(true);
                        flagArray[20] = true;
                    }
                    else if ((!flagArray[0x15] && (base.Reader.LocalName == this.id412_helpsetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._help_setup = this.Read17_PathItem(true);
                        flagArray[0x15] = true;
                    }
                    else if ((!flagArray[0x16] && (base.Reader.LocalName == this.id413_user_key_setup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._user_key_setup = this.Read17_PathItem(true);
                        flagArray[0x16] = true;
                    }
                    else if ((!flagArray[0x17] && (base.Reader.LocalName == this.id414_demoroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._demoRoot = this.Read17_PathItem(true);
                        flagArray[0x17] = true;
                    }
                    else if ((!flagArray[0x18] && (base.Reader.LocalName == this.id415_scenarioroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._scenarioRoot = this.Read17_PathItem(true);
                        flagArray[0x18] = true;
                    }
                    else if ((!flagArray[0x19] && (base.Reader.LocalName == this.id416_gym_guidelines)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._gym_guidelines = this.Read17_PathItem(true);
                        flagArray[0x19] = true;
                    }
                    else if ((!flagArray[0x1a] && (base.Reader.LocalName == this.id417_jobsetup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._Job_setup = this.Read17_PathItem(true);
                        flagArray[0x1a] = true;
                    }
                    else if ((!flagArray[0x1b] && (base.Reader.LocalName == this.id418_templateroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._templateRoot = this.Read17_PathItem(true);
                        flagArray[0x1b] = true;
                    }
                    else if ((!flagArray[0x1c] && (base.Reader.LocalName == this.id419_customizeroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._customizeRoot = this.Read17_PathItem(true);
                        flagArray[0x1c] = true;
                    }
                    else if ((!flagArray[0x1d] && (base.Reader.LocalName == this.id420_jobtreeroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._jobtreeRoot = this.Read17_PathItem(true);
                        flagArray[0x1d] = true;
                    }
                    else if ((!flagArray[30] && (base.Reader.LocalName == this.id421_generalapplink)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._generalApp = this.Read17_PathItem(true);
                        flagArray[30] = true;
                    }
                    else if ((!flagArray[0x1f] && (base.Reader.LocalName == this.id422_customlistlink)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._customList = this.Read17_PathItem(true);
                        flagArray[0x1f] = true;
                    }
                    else if ((!flagArray[0x20] && (base.Reader.LocalName == this.id423_dataviewpath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._dataView = this.Read17_PathItem(true);
                        flagArray[0x20] = true;
                    }
                    else if ((!flagArray[0x21] && (base.Reader.LocalName == this.id424_backuppath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._backup = this.Read17_PathItem(true);
                        flagArray[0x21] = true;
                    }
                    else if ((!flagArray[0x22] && (base.Reader.LocalName == this.id425_attachpath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._attach = this.Read17_PathItem(true);
                        flagArray[0x22] = true;
                    }
                    else if ((!flagArray[0x23] && (base.Reader.LocalName == this.id426_signfilepath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._sFPath = this.Read17_PathItem(true);
                        flagArray[0x23] = true;
                    }
                    else if ((!flagArray[0x24] && (base.Reader.LocalName == this.id427_logpath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._logPath = this.Read17_PathItem(true);
                        flagArray[0x24] = true;
                    }
                    else if ((!flagArray[0x25] && (base.Reader.LocalName == this.id428_codelist)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._codeList = this.Read17_PathItem(true);
                        flagArray[0x25] = true;
                    }
                    else if ((!flagArray[0x26] && (base.Reader.LocalName == this.id429_gstompinfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._gstampInfo = this.Read17_PathItem(true);
                        flagArray[0x26] = true;
                    }
                    else if ((!flagArray[0x27] && (base.Reader.LocalName == this.id430_guidanceroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._guidanceRoot = this.Read17_PathItem(true);
                        flagArray[0x27] = true;
                    }
                    else if ((!flagArray[40] && (base.Reader.LocalName == this.id431_kiosklinkroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._kiosklinkRoot = this.Read17_PathItem(true);
                        flagArray[40] = true;
                    }
                    else if ((!flagArray[0x29] && (base.Reader.LocalName == this.id432_kiosktablesroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._kiosktablesRoot = this.Read17_PathItem(true);
                        flagArray[0x29] = true;
                    }
                    else if ((!flagArray[0x2a] && (base.Reader.LocalName == this.id433_jetrasexe)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._jetras = this.Read17_PathItem(true);
                        flagArray[0x2a] = true;
                    }
                    else if ((!flagArray[0x2b] && (base.Reader.LocalName == this.id434_filesaveroot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._file_save_root = this.Read17_PathItem(true);
                        flagArray[0x2b] = true;
                    }
                    else if ((!flagArray[0x2c] && (base.Reader.LocalName == this.id435_printnamelist)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o._printNameList = this.Read17_PathItem(true);
                        flagArray[0x2c] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":project-path, :custompath, :job-history, :input-history-root, :outcode-table, :default-help-path, :default-gym-err-path, :system-error-message, :default-term-message-path, :support-file, :web-sites, :guide-root, :option-certification-setup, :user-environment-setup, :system-environment-setup, :printer-setup, :message-classify-setup, :dataview-setup, :file-save-setup, :user-setup, :receive-notice-setup, :help-setup, :user_key_setup, :demo-root, :scenario-root, :gym_guidelines, :job-setup, :template-root, :customize-root, :jobtree-root, :general-app-link, :custom-list-link, :dataview-path, :backup-path, :attach-path, :signfile-path, :log-path, :code-list, :gstomp-info, :guidance-root, :kiosklink-root, :kiosktables-root, :jetras-exe, :file-save-root, :printname-list");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":project-path, :custompath, :job-history, :input-history-root, :outcode-table, :default-help-path, :default-gym-err-path, :system-error-message, :default-term-message-path, :support-file, :web-sites, :guide-root, :option-certification-setup, :user-environment-setup, :system-environment-setup, :printer-setup, :message-classify-setup, :dataview-setup, :file-save-setup, :user-setup, :receive-notice-setup, :help-setup, :user_key_setup, :demo-root, :scenario-root, :gym_guidelines, :job-setup, :template-root, :customize-root, :jobtree-root, :general-app-link, :custom-list-link, :dataview-path, :backup-path, :attach-path, :signfile-path, :log-path, :code-list, :gstomp-info, :guidance-root, :kiosklink-root, :kiosktables-root, :jetras-exe, :file-save-root, :printname-list");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        public object Read180_HistorySearch()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id68_HistorySearch) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read63_HistorySearchClass(true, true);
            }
            base.UnknownNode(null, ":HistorySearch");
            return null;
        }

        public object Read181_ArrayOfDetailsClass()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id69_ArrayOfDetailsClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new DetailsListClass();
                    }
                    DetailsListClass class2 = (DetailsListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id70_DetailsClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read64_DetailsClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":DetailsClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":DetailsClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new DetailsListClass();
                }
                DetailsListClass class1 = (DetailsListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfDetailsClass");
            return obj2;
        }

        public object Read182_GatewaySettingClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id71_GatewaySettingClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read65_GatewaySettingClass(true, true);
            }
            base.UnknownNode(null, ":GatewaySettingClass");
            return null;
        }

        public object Read183_DatasetRevert()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id72_DatasetRevert) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read66_DatasetRevert(true, true);
            }
            base.UnknownNode(null, ":DatasetRevert");
            return null;
        }

        public object Read184_PrintStatus()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id73_PrintStatus) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read67_PrintStatus(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":PrintStatus");
            return null;
        }

        public object Read185_RevertEventArgs()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id74_RevertEventArgs) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read69_RevertEventArgs(true, true);
            }
            base.UnknownNode(null, ":RevertEventArgs");
            return null;
        }

        public object Read186_DatasetStorage()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id75_DatasetStorage) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read70_DatasetStorage(true, true);
            }
            base.UnknownNode(null, ":DatasetStorage");
            return null;
        }

        public object Read187_ArrangeDataTable()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id76_ArrangeDataTable) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read71_ArrangeDataTable(true, true);
            }
            base.UnknownNode(null, ":ArrangeDataTable");
            return null;
        }

        public object Read188_CustomizeMenu()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id77_CustomizeMenu) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read72_CustomizeMenu(true, true);
            }
            base.UnknownNode(null, ":CustomizeMenu");
            return null;
        }

        public object Read189_UserPathNames()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id78_UserPathNames) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read74_UserPathNames(true, true);
            }
            base.UnknownNode(null, ":UserPathNames");
            return null;
        }

        private ProtocolType Read19_ProtocolType(string s)
        {
            switch (s)
            {
                case "Interactive":
                    return ProtocolType.Interactive;

                case "Mail":
                    return ProtocolType.Mail;

                case "netNACCS":
                    return ProtocolType.netNACCS;

                case "KIOSK":
                    return ProtocolType.KIOSK;
            }
            throw base.CreateUnknownConstantException(s, typeof(ProtocolType));
        }

        public object Read190_CommonPath()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id79_CommonPath) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read73_CommonPathClass(true, true);
            }
            base.UnknownNode(null, ":CommonPath");
            return null;
        }

        public object Read191_RemoveWorkerArgs()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id80_RemoveWorkerArgs) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read75_RemoveWorkerArgs(true, true);
            }
            base.UnknownNode(null, ":RemoveWorkerArgs");
            return null;
        }

        public object Read192_Help()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id81_Help) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read77_HelpSettings(true, true);
            }
            base.UnknownNode(null, ":Help");
            return null;
        }

        public object Read193_HelpPath()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id82_HelpPath) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read76_HelpPathClass(true, true);
            }
            base.UnknownNode(null, ":HelpPath");
            return null;
        }

        public object Read194_ConfigFiles()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id83_ConfigFiles) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read78_ConfigFiles(true, true);
            }
            base.UnknownNode(null, ":ConfigFiles");
            return null;
        }

        public object Read195_RemoveWorkerResult()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id84_RemoveWorkerResult) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read79_RemoveWorkerResult(true, true);
            }
            base.UnknownNode(null, ":RemoveWorkerResult");
            return null;
        }

        public object Read196_DatasetRemove()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id85_DatasetRemove) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read80_DatasetRemove(true, true);
            }
            base.UnknownNode(null, ":DatasetRemove");
            return null;
        }

        public object Read197_OptionCertification()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id86_OptionCertification) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read82_OptionCertification(true, true);
            }
            base.UnknownNode(null, ":OptionCertification");
            return null;
        }

        public object Read198_AllCertification()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id87_AllCertification) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read81_AllCertificationClass(true, true);
            }
            base.UnknownNode(null, ":AllCertification");
            return null;
        }

        public object Read199_ComboManager()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id88_ComboManager) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read83_ComboManager(true, true);
            }
            base.UnknownNode(null, ":ComboManager");
            return null;
        }

        private AbstractSettings Read2_AbstractSettings(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id1_AbstractSettings) || (type.Namespace != this.id2_Item)))
            {
                if ((type.Name == this.id182_SendFileClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read109_SendFileClass(isNullable, false);
                }
                if ((type.Name == this.id184_FileNameClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read108_FileNameClass(isNullable, false);
                }
                if ((type.Name == this.id187_AllFileSaveClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read107_AllFileSaveClass(isNullable, false);
                }
                if ((type.Name == this.id192_GStampInfoClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read105_GStampInfoClass(isNullable, false);
                }
                if ((type.Name == this.id210_DataViewClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read101_DataViewClass(isNullable, false);
                }
                if ((type.Name == this.id231_AllCertificationClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read81_AllCertificationClass(isNullable, false);
                }
                if ((type.Name == this.id235_HelpPathClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read76_HelpPathClass(isNullable, false);
                }
                if ((type.Name == this.id243_CommonPathClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read73_CommonPathClass(isNullable, false);
                }
                if ((type.Name == this.id261_BatchSendClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read58_BatchSendClass(isNullable, false);
                }
                if ((type.Name == this.id263_JobOptionClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read57_JobOptionClass(isNullable, false);
                }
                if ((type.Name == this.id265_ResultCodeColorClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read56_ResultCodeColorClass(isNullable, false);
                }
                if ((type.Name == this.id270_DataViewFormClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read55_DataViewFormClass(isNullable, false);
                }
                if ((type.Name == this.id272_ColumnWidthClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read54_ColumnWidthClass(isNullable, false);
                }
                if ((type.Name == this.id283_LastLogonClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read53_LastLogonClass(isNullable, false);
                }
                if ((type.Name == this.id288_WindowClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read52_WindowClass(isNullable, false);
                }
                if ((type.Name == this.id292_ToolBarClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read51_ToolBarClass(isNullable, false);
                }
                if ((type.Name == this.id296_ApplicationClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read50_ApplicationClass(isNullable, false);
                }
                if ((type.Name == this.id357_LogFileLengthClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read31_LogFileLengthClass(isNullable, false);
                }
                if ((type.Name == this.id360_HttpOptionClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read30_HttpOptionClass(isNullable, false);
                }
                if ((type.Name == this.id368_MailInfoClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read29_MailInfoClass(isNullable, false);
                }
                if ((type.Name == this.id375_InteractiveInfoClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read28_InteractiveInfoClass(isNullable, false);
                }
                if ((type.Name == this.id379_DebugFunctionClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read27_DebugFunctionClass(isNullable, false);
                }
                if ((type.Name == this.id382_TerminalInfoClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read26_TerminalInfoClass(isNullable, false);
                }
                if ((type.Name == this.id436_FormsClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read14_FormsClass(isNullable, false);
                }
                if ((type.Name == this.id438_DefaultInfoClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read10_DefaultInfoClass(isNullable, false);
                }
                if ((type.Name == this.id442_InformClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read8_InformClass(isNullable, false);
                }
                if ((type.Name == this.id444_WarningInformClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read7_WarningInformClass(isNullable, false);
                }
                if ((type.Name == this.id446_SoundClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read6_SoundClass(isNullable, false);
                }
                if ((type.Name == this.id3_AbstractConfig) && (type.Namespace == this.id2_Item))
                {
                    return this.Read3_AbstractConfig(isNullable, false);
                }
                if ((type.Name == this.id128_TermMessage) && (type.Namespace == this.id2_Item))
                {
                    return this.Read112_TermMessage(isNullable, false);
                }
                if ((type.Name == this.id121_FileSave) && (type.Namespace == this.id2_Item))
                {
                    return this.Read110_FileSave(isNullable, false);
                }
                if ((type.Name == this.id200_GStampSettings) && (type.Namespace == this.id2_Item))
                {
                    return this.Read106_GStampSettings(isNullable, false);
                }
                if ((type.Name == this.id111_MessageClassify) && (type.Namespace == this.id2_Item))
                {
                    return this.Read102_MessageClassify(isNullable, false);
                }
                if ((type.Name == this.id90_UserKey) && (type.Namespace == this.id2_Item))
                {
                    return this.Read85_UserKey(isNullable, false);
                }
                if ((type.Name == this.id86_OptionCertification) && (type.Namespace == this.id2_Item))
                {
                    return this.Read82_OptionCertification(isNullable, false);
                }
                if ((type.Name == this.id239_HelpSettings) && (type.Namespace == this.id2_Item))
                {
                    return this.Read77_HelpSettings(isNullable, false);
                }
                if ((type.Name == this.id78_UserPathNames) && (type.Namespace == this.id2_Item))
                {
                    return this.Read74_UserPathNames(isNullable, false);
                }
                if ((type.Name == this.id55_User) && (type.Namespace == this.id2_Item))
                {
                    return this.Read59_User(isNullable, false);
                }
                if ((type.Name == this.id42_SystemEnvironment) && (type.Namespace == this.id2_Item))
                {
                    return this.Read44_SystemEnvironment(isNullable, false);
                }
                if ((type.Name == this.id26_UserEnvironment) && (type.Namespace == this.id2_Item))
                {
                    return this.Read32_UserEnvironment(isNullable, false);
                }
                if ((type.Name == this.id440_PrintSetups) && (type.Namespace == this.id2_Item))
                {
                    return this.Read15_PrintSetups(isNullable, false);
                }
                if ((type.Name != this.id4_ReceiveNotice) || (type.Namespace != this.id2_Item))
                {
                    throw base.CreateUnknownTypeException(type);
                }
                return this.Read9_ReceiveNotice(isNullable, false);
            }
            if (flag)
            {
                return null;
            }
            AbstractSettings o = new AbstractSettings();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private SettingDivType Read20_SettingDivType(string s)
        {
            switch (s)
            {
                case "User":
                    return SettingDivType.User;

                case "AllUsers":
                    return SettingDivType.AllUsers;
            }
            throw base.CreateUnknownConstantException(s, typeof(SettingDivType));
        }

        public object Read200_DataFactory()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id89_DataFactory) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read84_DataFactory(true, true);
            }
            base.UnknownNode(null, ":DataFactory");
            return null;
        }

        public object Read201_UserKey()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id90_UserKey) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read85_UserKey(true, true);
            }
            base.UnknownNode(null, ":UserKey");
            return null;
        }

        public object Read202_JobKey()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id91_JobKey) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read23_JobKeyClass(true, true);
            }
            base.UnknownNode(null, ":JobKey");
            return null;
        }

        public object Read203_Key()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id92_Key) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read22_KeyClass(true, true);
            }
            base.UnknownNode(null, ":Key");
            return null;
        }

        public object Read204_ArrayOfPrinterInfoClass1()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id93_ArrayOfPrinterInfoClass1) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new PrinterInfoListClass();
                    }
                    PrinterInfoListClass class2 = (PrinterInfoListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id94_PrinterInfoClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read12_PrinterInfoClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":PrinterInfoClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":PrinterInfoClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new PrinterInfoListClass();
                }
                PrinterInfoListClass class1 = (PrinterInfoListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfPrinterInfoClass1");
            return obj2;
        }

        public object Read205_ArrayOfString4()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id95_ArrayOfString4) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new StringList();
                    }
                    StringList list = (StringList) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id96_string) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (base.ReadNull())
                                {
                                    list.Add(null);
                                }
                                else
                                {
                                    list.Add(base.Reader.ReadElementString());
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":string");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":string");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new StringList();
                }
                StringList list1 = (StringList) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfString4");
            return obj2;
        }

        public object Read206_JobFormFactory()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id97_JobFormFactory) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read86_JobFormFactory(true, true);
            }
            base.UnknownNode(null, ":JobFormFactory");
            return null;
        }

        public object Read207_ULogClass()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id98_ULogClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read87_ULogClass(true, true);
            }
            base.UnknownNode(null, ":ULogClass");
            return null;
        }

        public object Read208_ModeFont()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id99_ModeFont) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read88_ModeFont(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":ModeFont");
            return null;
        }

        public object Read209_JobPanelStatus()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id100_JobPanelStatus) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read89_JobPanelStatus(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":JobPanelStatus");
            return null;
        }

        private UserKindType Read21_UserKindType(string s)
        {
            switch (s)
            {
                case "General":
                    return UserKindType.General;

                case "Bank":
                    return UserKindType.Bank;

                case "Center":
                    return UserKindType.Center;

                case "Private":
                    return UserKindType.Private;
            }
            throw base.CreateUnknownConstantException(s, typeof(UserKindType));
        }

        public object Read210_VersionuptoolUpdater()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id101_VersionuptoolUpdater) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read90_VersionuptoolUpdater(true, true);
            }
            base.UnknownNode(null, ":VersionuptoolUpdater");
            return null;
        }

        public object Read211_ProgresType()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id102_ProgresType) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read91_ProgresType(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":ProgresType");
            return null;
        }

        public object Read212_VersionSettings()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id103_VersionSettings) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read92_VersionSettings(true, true);
            }
            base.UnknownNode(null, ":VersionSettings");
            return null;
        }

        public object Read213_PrintManager()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id104_PrintManager) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read93_PrintManager(true, true);
            }
            base.UnknownNode(null, ":PrintManager");
            return null;
        }

        public object Read214_ReportStatus()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id105_ReportStatus) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read94_ReportStatus(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":ReportStatus");
            return null;
        }

        public object Read215_DataStatus()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id106_DataStatus) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read95_DataStatus(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":DataStatus");
            return null;
        }

        public object Read216_Signflg()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id107_Signflg) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read96_Signflg(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":Signflg");
            return null;
        }

        public object Read217_ContainedType()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id108_ContainedType) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read97_ContainedType(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":ContainedType");
            return null;
        }

        public object Read218_OutcodeTbl()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id109_OutcodeTbl) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read99_OutcodeTbl(true, true);
            }
            base.UnknownNode(null, ":OutcodeTbl");
            return null;
        }

        public object Read219_OutcodeRec()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id110_OutcodeRec) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read98_OutcodeRec(true);
            }
            base.UnknownNode(null, ":OutcodeRec");
            return null;
        }

        private KeyClass Read22_KeyClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id226_KeyClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            KeyClass o = new KeyClass();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id218_Name)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Name = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id227_Sckey)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Sckey = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Name, :Sckey");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Name, :Sckey");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        public object Read220_MessageClassify()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id111_MessageClassify) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read102_MessageClassify(true, true);
            }
            base.UnknownNode(null, ":MessageClassify");
            return null;
        }

        public object Read221_Term()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id112_Term) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read100_TermClass(true, true);
            }
            base.UnknownNode(null, ":Term");
            return null;
        }

        public object Read222_DataView()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id113_DataView) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read101_DataViewClass(true, true);
            }
            base.UnknownNode(null, ":DataView");
            return null;
        }

        public object Read223_ArrayOfTermClass1()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id114_ArrayOfTermClass1) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new TermListClass();
                    }
                    TermListClass class2 = (TermListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id115_TermClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read100_TermClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":TermClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":TermClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new TermListClass();
                }
                TermListClass class1 = (TermListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfTermClass1");
            return obj2;
        }

        public object Read224_UserKeySet()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id116_UserKeySet) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read103_UserKeySet(true, true);
            }
            base.UnknownNode(null, ":UserKeySet");
            return null;
        }

        public object Read225_ArrayOfTypeClass()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id117_ArrayOfTypeClass) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new TypeListClass();
                    }
                    TypeListClass class2 = (TypeListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id118_TypeClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read104_TypeClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":TypeClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":TypeClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new TypeListClass();
                }
                TypeListClass class1 = (TypeListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfTypeClass");
            return obj2;
        }

        public object Read226_GStompSettings()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id119_GStompSettings) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read106_GStampSettings(true, true);
            }
            base.UnknownNode(null, ":GStompSettings");
            return null;
        }

        public object Read227_GStompInfo()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id120_GStompInfo) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read105_GStampInfoClass(true, true);
            }
            base.UnknownNode(null, ":GStompInfo");
            return null;
        }

        public object Read228_FileSave()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id121_FileSave) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read110_FileSave(true, true);
            }
            base.UnknownNode(null, ":FileSave");
            return null;
        }

        public object Read229_AllFileSave()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id122_AllFileSave) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read107_AllFileSaveClass(true, true);
            }
            base.UnknownNode(null, ":AllFileSave");
            return null;
        }

        private JobKeyClass Read23_JobKeyClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id25_JobKeyClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            JobKeyClass o = new JobKeyClass();
            if (o.KeyList == null)
            {
                o.KeyList = new List<KeyClass>();
            }
            List<KeyClass> keyList = o.KeyList;
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id228_Kind)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.Kind = XmlConvert.ToInt32(base.Reader.Value);
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":Kind");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id229_KeyList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.KeyList == null)
                            {
                                o.KeyList = new List<KeyClass>();
                            }
                            List<KeyClass> list = o.KeyList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id92_Key) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (list == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                list.Add(this.Read22_KeyClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":Key");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":Key");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":KeyList");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":KeyList");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        public object Read230_Type()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id123_Type) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read104_TypeClass(true, true);
            }
            base.UnknownNode(null, ":Type");
            return null;
        }

        public object Read231_FileName()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id124_FileName) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read108_FileNameClass(true, true);
            }
            base.UnknownNode(null, ":FileName");
            return null;
        }

        public object Read232_SendFile()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id125_SendFile) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read109_SendFileClass(true, true);
            }
            base.UnknownNode(null, ":SendFile");
            return null;
        }

        public object Read233_Details()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id126_Details) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read64_DetailsClass(true, true);
            }
            base.UnknownNode(null, ":Details");
            return null;
        }

        public object Read234_ListType()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id127_ListType) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read111_ListType(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":ListType");
            return null;
        }

        public object Read235_TermMessage()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id128_TermMessage) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read112_TermMessage(true, true);
            }
            base.UnknownNode(null, ":TermMessage");
            return null;
        }

        public object Read236_TermMessageSet()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id129_TermMessageSet) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read36_TermMessageSetClass(true, true);
            }
            base.UnknownNode(null, ":TermMessageSet");
            return null;
        }

        public object Read237_ArrayOfReceiveInformClass1()
        {
            object obj2 = null;
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id130_ArrayOfReceiveInformClass1) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                if (!base.ReadNull())
                {
                    if (obj2 == null)
                    {
                        obj2 = new ReceiveInformListClass();
                    }
                    ReceiveInformListClass class2 = (ReceiveInformListClass) obj2;
                    if (base.Reader.IsEmptyElement)
                    {
                        base.Reader.Skip();
                        return obj2;
                    }
                    base.Reader.ReadStartElement();
                    base.Reader.MoveToContent();
                    int whileIterations = 0;
                    int readerCount = base.ReaderCount;
                    while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                    {
                        if (base.Reader.NodeType == XmlNodeType.Element)
                        {
                            if ((base.Reader.LocalName == this.id131_ReceiveInformClass) && (base.Reader.NamespaceURI == this.id2_Item))
                            {
                                if (class2 == null)
                                {
                                    base.Reader.Skip();
                                }
                                else
                                {
                                    class2.Add(this.Read5_ReceiveInformClass(true, true));
                                }
                            }
                            else
                            {
                                base.UnknownNode(null, ":ReceiveInformClass");
                            }
                        }
                        else
                        {
                            base.UnknownNode(null, ":ReceiveInformClass");
                        }
                        base.Reader.MoveToContent();
                        base.CheckReaderCount(ref whileIterations, ref readerCount);
                    }
                    base.ReadEndElement();
                    return obj2;
                }
                if (obj2 == null)
                {
                    obj2 = new ReceiveInformListClass();
                }
                ReceiveInformListClass class1 = (ReceiveInformListClass) obj2;
                return obj2;
            }
            base.UnknownNode(null, ":ArrayOfReceiveInformClass1");
            return obj2;
        }

        public object Read238_ResourceManager()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id132_ResourceManager) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read113_ResourceManager(true, true);
            }
            base.UnknownNode(null, ":ResourceManager");
            return null;
        }

        public object Read239_ExData()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id133_ExData) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read115_NaccsData(true, true);
            }
            base.UnknownNode(null, ":ExData");
            return null;
        }

        private KeyListClass Read24_KeyListClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id22_KeyListClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            KeyListClass o = new KeyListClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id388_KeyInfoList)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.KeyInfoList = this.Read23_JobKeyClass(false, true);
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":KeyInfoList");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":KeyInfoList");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        public object Read240_ButtonPatern()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id134_ButtonPatern) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read116_ButtonPatern(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":ButtonPatern");
            return null;
        }

        public object Read241_MessageKind()
        {
            base.Reader.MoveToContent();
            if (base.Reader.NodeType == XmlNodeType.Element)
            {
                if ((base.Reader.LocalName != this.id135_MessageKind) || (base.Reader.NamespaceURI != this.id2_Item))
                {
                    throw base.CreateUnknownNodeException();
                }
                return this.Read117_MessageKind(base.Reader.ReadElementString());
            }
            base.UnknownNode(null, ":MessageKind");
            return null;
        }

        private KeyListClass.UserKeyType Read25_UserKeyType(string s)
        {
            switch (s)
            {
                case "Jkey":
                    return KeyListClass.UserKeyType.Jkey;

                case "SKey":
                    return KeyListClass.UserKeyType.SKey;

                case "RKey":
                    return KeyListClass.UserKeyType.RKey;

                case "GKey":
                    return KeyListClass.UserKeyType.GKey;
            }
            throw base.CreateUnknownConstantException(s, typeof(KeyListClass.UserKeyType));
        }

        private TerminalInfoClass Read26_TerminalInfoClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id382_TerminalInfoClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            TerminalInfoClass o = new TerminalInfoClass();
            bool[] flagArray = new bool[6];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id383_TermLogicalName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TermLogicalName = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id384_TermAccessKey)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TermAccessKey = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id385_SaveTerm)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.SaveTerm = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id386_DiskWarning)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.DiskWarning = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id212_AutoBackup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AutoBackup = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id387_Versionup)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Versionup = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[5] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":TermLogicalName, :TermAccessKey, :SaveTerm, :DiskWarning, :AutoBackup, :Versionup");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":TermLogicalName, :TermAccessKey, :SaveTerm, :DiskWarning, :AutoBackup, :Versionup");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DebugFunctionClass Read27_DebugFunctionClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id379_DebugFunctionClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DebugFunctionClass o = new DebugFunctionClass();
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id380_Check_Off)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Check_Off = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id153_Control)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Control = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id381_UserKindFlag)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.UserKindFlag = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Check_Off, :Control, :UserKindFlag");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Check_Off, :Control, :UserKindFlag");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private InteractiveInfoClass Read28_InteractiveInfoClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id375_InteractiveInfoClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            InteractiveInfoClass o = new InteractiveInfoClass();
            bool[] flagArray = new bool[5];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id369_ServerConnect)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ServerConnect = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id376_AutoSendRecvMode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AutoSendRecvMode = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id377_AutoSendRecvTm)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AutoSendRecvTm = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id378_JobConnect)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.JobConnect = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id374_TraceOutput)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.TraceOutput = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[4] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":ServerConnect, :AutoSendRecvMode, :AutoSendRecvTm, :JobConnect, :TraceOutput");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":ServerConnect, :AutoSendRecvMode, :AutoSendRecvTm, :JobConnect, :TraceOutput");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private MailInfoClass Read29_MailInfoClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id368_MailInfoClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            MailInfoClass o = new MailInfoClass();
            bool[] flagArray = new bool[0x10];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id369_ServerConnect)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ServerConnect = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id247_UseGateway)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.UseGateway = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id248_GatewaySMTPServerName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewaySMTPServerName = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id249_GatewaySMTPServerPort)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.GatewaySMTPServerPort = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id250_GatewayPOPServerName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewayPOPServerName = base.Reader.ReadElementString();
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id251_GatewayPOPServerPort)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.GatewayPOPServerPort = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id252_GatewayMailBox)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewayMailBox = base.Reader.ReadElementString();
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id253_GatewaySMTPDomainName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewaySMTPDomainName = base.Reader.ReadElementString();
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id254_GatewayPOPDomainName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewayPOPDomainName = base.Reader.ReadElementString();
                        flagArray[8] = true;
                    }
                    else if ((!flagArray[9] && (base.Reader.LocalName == this.id255_GatewayAddDomainName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.GatewayAddDomainName = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[9] = true;
                    }
                    else if ((!flagArray[10] && (base.Reader.LocalName == this.id370_AddDomainName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AddDomainName = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[10] = true;
                    }
                    else if ((!flagArray[11] && (base.Reader.LocalName == this.id371_AutoMode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AutoMode = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[11] = true;
                    }
                    else if ((!flagArray[12] && (base.Reader.LocalName == this.id372_AutoTm)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AutoTm = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[12] = true;
                    }
                    else if ((!flagArray[13] && (base.Reader.LocalName == this.id373_AutoTmKind)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AutoTmKind = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[13] = true;
                    }
                    else if ((!flagArray[14] && (base.Reader.LocalName == this.id306_MaxRetreiveCnt)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.MaxRetreiveCnt = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[14] = true;
                    }
                    else if ((!flagArray[15] && (base.Reader.LocalName == this.id374_TraceOutput)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.TraceOutput = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[15] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":ServerConnect, :UseGateway, :GatewaySMTPServerName, :GatewaySMTPServerPort, :GatewayPOPServerName, :GatewayPOPServerPort, :GatewayMailBox, :GatewaySMTPDomainName, :GatewayPOPDomainName, :GatewayAddDomainName, :AddDomainName, :AutoMode, :AutoTm, :AutoTmKind, :MaxRetreiveCnt, :TraceOutput");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":ServerConnect, :UseGateway, :GatewaySMTPServerName, :GatewaySMTPServerPort, :GatewayPOPServerName, :GatewayPOPServerPort, :GatewayMailBox, :GatewaySMTPDomainName, :GatewayPOPDomainName, :GatewayAddDomainName, :AddDomainName, :AutoMode, :AutoTm, :AutoTmKind, :MaxRetreiveCnt, :TraceOutput");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private AbstractConfig Read3_AbstractConfig(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id3_AbstractConfig) || (type.Namespace != this.id2_Item)))
            {
                if ((type.Name == this.id128_TermMessage) && (type.Namespace == this.id2_Item))
                {
                    return this.Read112_TermMessage(isNullable, false);
                }
                if ((type.Name == this.id121_FileSave) && (type.Namespace == this.id2_Item))
                {
                    return this.Read110_FileSave(isNullable, false);
                }
                if ((type.Name == this.id200_GStampSettings) && (type.Namespace == this.id2_Item))
                {
                    return this.Read106_GStampSettings(isNullable, false);
                }
                if ((type.Name == this.id111_MessageClassify) && (type.Namespace == this.id2_Item))
                {
                    return this.Read102_MessageClassify(isNullable, false);
                }
                if ((type.Name == this.id90_UserKey) && (type.Namespace == this.id2_Item))
                {
                    return this.Read85_UserKey(isNullable, false);
                }
                if ((type.Name == this.id86_OptionCertification) && (type.Namespace == this.id2_Item))
                {
                    return this.Read82_OptionCertification(isNullable, false);
                }
                if ((type.Name == this.id239_HelpSettings) && (type.Namespace == this.id2_Item))
                {
                    return this.Read77_HelpSettings(isNullable, false);
                }
                if ((type.Name == this.id78_UserPathNames) && (type.Namespace == this.id2_Item))
                {
                    return this.Read74_UserPathNames(isNullable, false);
                }
                if ((type.Name == this.id55_User) && (type.Namespace == this.id2_Item))
                {
                    return this.Read59_User(isNullable, false);
                }
                if ((type.Name == this.id42_SystemEnvironment) && (type.Namespace == this.id2_Item))
                {
                    return this.Read44_SystemEnvironment(isNullable, false);
                }
                if ((type.Name == this.id26_UserEnvironment) && (type.Namespace == this.id2_Item))
                {
                    return this.Read32_UserEnvironment(isNullable, false);
                }
                if ((type.Name == this.id440_PrintSetups) && (type.Namespace == this.id2_Item))
                {
                    return this.Read15_PrintSetups(isNullable, false);
                }
                if ((type.Name != this.id4_ReceiveNotice) || (type.Namespace != this.id2_Item))
                {
                    throw base.CreateUnknownTypeException(type);
                }
                return this.Read9_ReceiveNotice(isNullable, false);
            }
            if (!flag)
            {
                throw base.CreateAbstractTypeException("AbstractConfig", "");
            }
            return null;
        }

        private HttpOptionClass Read30_HttpOptionClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id360_HttpOptionClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            HttpOptionClass o = new HttpOptionClass();
            bool[] flagArray = new bool[7];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id361_CertificateHash)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.CertificateHash = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id362_UseProxy)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.UseProxy = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id363_ProxyName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ProxyName = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id364_ProxyPort)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.ProxyPort = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id365_UseProxyAccount)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.UseProxyAccount = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id366_ProxyAccount)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ProxyAccount = base.Reader.ReadElementString();
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id367_ProxyPassword)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.EncryptProxyPassword = base.ToByteArrayBase64(false);
                        flagArray[6] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":CertificateHash, :UseProxy, :ProxyName, :ProxyPort, :UseProxyAccount, :ProxyAccount, :ProxyPassword");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":CertificateHash, :UseProxy, :ProxyName, :ProxyPort, :UseProxyAccount, :ProxyAccount, :ProxyPassword");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private LogFileLengthClass Read31_LogFileLengthClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id357_LogFileLengthClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            LogFileLengthClass o = new LogFileLengthClass();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id358_ComLog)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.ComLog = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id359_MsgLog)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.MsgLog = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":ComLog, :MsgLog");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":ComLog, :MsgLog");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private UserEnvironment Read32_UserEnvironment(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id26_UserEnvironment) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            UserEnvironment o = new UserEnvironment();
            bool[] flagArray = new bool[6];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id27_TerminalInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TerminalInfo = this.Read26_TerminalInfoClass(false, true);
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id28_DebugFunction)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DebugFunction = this.Read27_DebugFunctionClass(false, true);
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id29_InteractiveInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.InteractiveInfo = this.Read28_InteractiveInfoClass(false, true);
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id30_MailInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.MailInfo = this.Read29_MailInfoClass(false, true);
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id31_HttpOption)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.HttpOption = this.Read30_HttpOptionClass(false, true);
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id32_LogFileLength)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.LogFileLength = this.Read31_LogFileLengthClass(false, true);
                        flagArray[5] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":TerminalInfo, :DebugFunction, :InteractiveInfo, :MailInfo, :HttpOption, :LogFileLength");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":TerminalInfo, :DebugFunction, :InteractiveInfo, :MailInfo, :HttpOption, :LogFileLength");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private WriterLog Read33_WriterLog(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id37_WriterLog) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            WriterLog o = new WriterLog();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id352_LogFile)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.LogFile = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id353_LogSize)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.LogSize = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":LogFile, :LogSize");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":LogFile, :LogSize");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private NaccsHeader Read34_NaccsHeader(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id38_NaccsHeader) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            NaccsHeader o = new NaccsHeader();
            bool[] flagArray = new bool[0x15];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id153_Control)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Control = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id154_JobCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.JobCode = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id15_OutCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.OutCode = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id155_ServerRecvTime)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ServerRecvTime = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id156_UserId)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UserId = base.Reader.ReadElementString();
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id157_Path)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Path = base.Reader.ReadElementString();
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id158_MailAddress)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.MailAddress = base.Reader.ReadElementString();
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id159_Subject)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Subject = base.Reader.ReadElementString();
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id160_RtpInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.RtpInfo = base.Reader.ReadElementString();
                        flagArray[8] = true;
                    }
                    else if ((!flagArray[9] && (base.Reader.LocalName == this.id161_ServerInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ServerInfo = base.Reader.ReadElementString();
                        flagArray[9] = true;
                    }
                    else if ((!flagArray[10] && (base.Reader.LocalName == this.id162_DataInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DataInfo = base.Reader.ReadElementString();
                        flagArray[10] = true;
                    }
                    else if ((!flagArray[11] && (base.Reader.LocalName == this.id163_Div)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Div = base.Reader.ReadElementString();
                        flagArray[11] = true;
                    }
                    else if ((!flagArray[12] && (base.Reader.LocalName == this.id164_EndFlag)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.EndFlag = base.Reader.ReadElementString();
                        flagArray[12] = true;
                    }
                    else if ((!flagArray[13] && (base.Reader.LocalName == this.id165_DataType)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DataType = base.Reader.ReadElementString();
                        flagArray[13] = true;
                    }
                    else if ((!flagArray[14] && (base.Reader.LocalName == this.id166_SendGuard)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SendGuard = base.Reader.ReadElementString();
                        flagArray[14] = true;
                    }
                    else if ((!flagArray[15] && (base.Reader.LocalName == this.id167_InputInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.InputInfo = base.Reader.ReadElementString();
                        flagArray[15] = true;
                    }
                    else if ((!flagArray[0x10] && (base.Reader.LocalName == this.id168_IndexInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.IndexInfo = base.Reader.ReadElementString();
                        flagArray[0x10] = true;
                    }
                    else if ((!flagArray[0x11] && (base.Reader.LocalName == this.id169_Pattern)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Pattern = base.Reader.ReadElementString();
                        flagArray[0x11] = true;
                    }
                    else if ((!flagArray[0x12] && (base.Reader.LocalName == this.id170_System)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.System = base.Reader.ReadElementString();
                        flagArray[0x12] = true;
                    }
                    else if ((!flagArray[0x13] && (base.Reader.LocalName == this.id171_DispCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DispCode = base.Reader.ReadElementString();
                        flagArray[0x13] = true;
                    }
                    else if ((!flagArray[20] && (base.Reader.LocalName == this.id172_Length)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Length = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[20] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Control, :JobCode, :OutCode, :ServerRecvTime, :UserId, :Path, :MailAddress, :Subject, :RtpInfo, :ServerInfo, :DataInfo, :Div, :EndFlag, :DataType, :SendGuard, :InputInfo, :IndexInfo, :Pattern, :System, :DispCode, :Length");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Control, :JobCode, :OutCode, :ServerRecvTime, :UserId, :Path, :MailAddress, :Subject, :RtpInfo, :ServerInfo, :DataInfo, :Div, :EndFlag, :DataType, :SendGuard, :InputInfo, :IndexInfo, :Pattern, :System, :DispCode, :Length");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DataControl Read35_DataControl(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id39_DataControl) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DataControl o = new DataControl();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private TermMessageSetClass Read36_TermMessageSetClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id41_TermMessageSetClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            TermMessageSetClass o = new TermMessageSetClass();
            bool[] flagArray = new bool[5];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id174_Code)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.Code = base.Reader.Value;
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":Code");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id175_Message)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Message = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id176_Button)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Button = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id177_Description)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Description = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id178_Disposition)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Disposition = base.Reader.ReadElementString();
                        flagArray[4] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Message, :Button, :Description, :Disposition");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Message, :Button, :Description, :Disposition");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private TerminalInfoSysClass Read37_TerminalInfoSysClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id43_TerminalInfoSysClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            TerminalInfoSysClass o = new TerminalInfoSysClass();
            bool[] flagArray = new bool[0x12];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id336_UserKind)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UserKind = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id337_Protocol)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Protocol = this.Read19_ProtocolType(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id326_SendTimeOut)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SendTimeOut = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id338_A2MinimamDelay)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.A2MinimamDelay = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id339_A2MaximamDelay)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.A2MaximamDelay = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id340_A2TimeOut)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.A2TimeOut = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id341_CertificateDate)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.CertificateDate = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id342_BackStatusStr)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.BackStatusStr = base.Reader.ReadElementString();
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id343_TestStatusStr)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TestStatusStr = base.Reader.ReadElementString();
                        flagArray[8] = true;
                    }
                    else if ((!flagArray[9] && (base.Reader.LocalName == this.id344_Debug)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Debug = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[9] = true;
                    }
                    else if ((!flagArray[10] && (base.Reader.LocalName == this.id345_UseInternet)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UseInternet = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[10] = true;
                    }
                    else if ((!flagArray[11] && (base.Reader.LocalName == this.id313_URL)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.URL = base.Reader.ReadElementString();
                        flagArray[11] = true;
                    }
                    else if ((!flagArray[12] && (base.Reader.LocalName == this.id346_PortalURL)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.PortalURL = base.Reader.ReadElementString();
                        flagArray[12] = true;
                    }
                    else if ((!flagArray[13] && (base.Reader.LocalName == this.id347_SupportURL)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SupportURL = base.Reader.ReadElementString();
                        flagArray[13] = true;
                    }
                    else if ((!flagArray[14] && (base.Reader.LocalName == this.id348_PrintStampMark)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.PrintStampMark = base.Reader.ReadElementString();
                        flagArray[14] = true;
                    }
                    else if ((!flagArray[15] && (base.Reader.LocalName == this.id349_Demo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Demo = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[15] = true;
                    }
                    else if ((!flagArray[0x10] && (base.Reader.LocalName == this.id350_Jetras)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Jetras = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[0x10] = true;
                    }
                    else if ((!flagArray[0x11] && (base.Reader.LocalName == this.id351_Receiver)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Receiver = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[0x11] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":UserKind, :Protocol, :SendTimeOut, :A2MinimamDelay, :A2MaximamDelay, :A2TimeOut, :CertificateDate, :BackStatusStr, :TestStatusStr, :Debug, :UseInternet, :URL, :PortalURL, :SupportURL, :PrintStampMark, :Demo, :Jetras, :Receiver");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":UserKind, :Protocol, :SendTimeOut, :A2MinimamDelay, :A2MaximamDelay, :A2TimeOut, :CertificateDate, :BackStatusStr, :TestStatusStr, :Debug, :UseInternet, :URL, :PortalURL, :SupportURL, :PrintStampMark, :Demo, :Jetras, :Receiver");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private InteractiveServerClass Read38_InteractiveServerClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id44_InteractiveServerClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            InteractiveServerClass o = new InteractiveServerClass();
            bool[] flagArray = new bool[6];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id320_name)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.name = base.Reader.Value;
                    flagArray[0] = true;
                }
                else
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id321_display)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.display = base.Reader.Value;
                        flagArray[1] = true;
                        continue;
                    }
                    if (!base.IsXmlnsAttribute(base.Reader.Name))
                    {
                        base.UnknownNode(o, ":name, :display");
                    }
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[2] && (base.Reader.LocalName == this.id322_ServerName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ServerName = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id323_ObjectName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ObjectName = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id324_ServerPort)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ServerPort = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id325_PingPoint)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.PingPoint = base.Reader.ReadElementString();
                        flagArray[5] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":ServerName, :ObjectName, :ServerPort, :PingPoint");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":ServerName, :ObjectName, :ServerPort, :PingPoint");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private InteractiveInfoSysClass Read39_InteractiveInfoSysClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id45_InteractiveInfoSysClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            InteractiveInfoSysClass o = new InteractiveInfoSysClass();
            if (o.Server == null)
            {
                o.Server = new List<InteractiveServerClass>();
            }
            List<InteractiveServerClass> server = o.Server;
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id317_Server) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (server == null)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            server.Add(this.Read38_InteractiveServerClass(false, true));
                        }
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id318_TraceFileName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TraceFileName = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id319_TraceFileSize)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TraceFileSize = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Server, :TraceFileName, :TraceFileSize");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Server, :TraceFileName, :TraceFileSize");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ListItem Read4_ListItem(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id5_ListItem) || (type.Namespace != this.id2_Item)))
            {
                if ((type.Name == this.id118_TypeClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read104_TypeClass(isNullable, false);
                }
                if ((type.Name == this.id115_TermClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read100_TermClass(isNullable, false);
                }
                if ((type.Name == this.id70_DetailsClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read64_DetailsClass(isNullable, false);
                }
                if ((type.Name == this.id256_HistorySearchClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read63_HistorySearchClass(isNullable, false);
                }
                if ((type.Name == this.id258_HistoryAddressClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read62_HistoryAddressClass(isNullable, false);
                }
                if ((type.Name == this.id260_HistoryUserClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read61_HistoryUserClass(isNullable, false);
                }
                if ((type.Name == this.id281_HistoryJobsClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read60_HistoryJobsClass(isNullable, false);
                }
                if ((type.Name == this.id41_TermMessageSetClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read36_TermMessageSetClass(isNullable, false);
                }
                if ((type.Name == this.id25_JobKeyClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read23_JobKeyClass(isNullable, false);
                }
                if ((type.Name == this.id34_OutCodeClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read13_OutCodeClass(isNullable, false);
                }
                if ((type.Name == this.id94_PrinterInfoClass) && (type.Namespace == this.id2_Item))
                {
                    return this.Read12_PrinterInfoClass(isNullable, false);
                }
                if ((type.Name != this.id131_ReceiveInformClass) || (type.Namespace != this.id2_Item))
                {
                    throw base.CreateUnknownTypeException(type);
                }
                return this.Read5_ReceiveInformClass(isNullable, false);
            }
            if (flag)
            {
                return null;
            }
            ListItem o = new ListItem();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private MailServerClass Read40_MailServerClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id46_MailServerClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            MailServerClass o = new MailServerClass();
            bool[] flagArray = new bool[9];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id320_name)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.name = base.Reader.Value;
                    flagArray[0] = true;
                }
                else
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id321_display)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.display = base.Reader.Value;
                        flagArray[1] = true;
                        continue;
                    }
                    if (!base.IsXmlnsAttribute(base.Reader.Name))
                    {
                        base.UnknownNode(o, ":name, :display");
                    }
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[2] && (base.Reader.LocalName == this.id330_SMTPServerName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SMTPServerName = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id331_SMTPServerPort)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SMTPServerPort = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id332_POPServerName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.POPServerName = base.Reader.ReadElementString();
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id333_POPServerPort)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.POPServerPort = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id334_MailBox)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.MailBox = base.Reader.ReadElementString();
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id335_DomainName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DomainName = base.Reader.ReadElementString();
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id325_PingPoint)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.PingPoint = base.Reader.ReadElementString();
                        flagArray[8] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":SMTPServerName, :SMTPServerPort, :POPServerName, :POPServerPort, :MailBox, :DomainName, :PingPoint");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":SMTPServerName, :SMTPServerPort, :POPServerName, :POPServerPort, :MailBox, :DomainName, :PingPoint");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private MailInfoSysClass Read41_MailInfoSysClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id47_MailInfoSysClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            MailInfoSysClass o = new MailInfoSysClass();
            if (o.Server == null)
            {
                o.Server = new List<MailServerClass>();
            }
            List<MailServerClass> server = o.Server;
            bool[] flagArray = new bool[7];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id317_Server) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (server == null)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            server.Add(this.Read40_MailServerClass(false, true));
                        }
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id326_SendTimeOut)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SendTimeOut = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id327_RecvTimeOut)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.RecvTimeOut = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id328_POPServerCnctRetry)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.POPServerCnctRetry = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id318_TraceFileName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TraceFileName = base.Reader.ReadElementString();
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id319_TraceFileSize)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TraceFileSize = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id329_ReConnectTime)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ReConnectTime = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[6] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Server, :SendTimeOut, :RecvTimeOut, :POPServerCnctRetry, :TraceFileName, :TraceFileSize, :ReConnectTime");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Server, :SendTimeOut, :RecvTimeOut, :POPServerCnctRetry, :TraceFileName, :TraceFileSize, :ReConnectTime");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private BatchdocInfoSysClass Read42_BatchdocInfoSysClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id316_BatchdocInfoSysClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            BatchdocInfoSysClass o = new BatchdocInfoSysClass();
            if (o.Server == null)
            {
                o.Server = new List<InteractiveServerClass>();
            }
            List<InteractiveServerClass> server = o.Server;
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id317_Server) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (server == null)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            server.Add(this.Read38_InteractiveServerClass(false, true));
                        }
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id318_TraceFileName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TraceFileName = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id319_TraceFileSize)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TraceFileSize = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Server, :TraceFileName, :TraceFileSize");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Server, :TraceFileName, :TraceFileSize");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private LogFileLengthSysClass Read43_LogFileLengthSysClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id49_LogFileLengthSysClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            LogFileLengthSysClass o = new LogFileLengthSysClass();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id314_NACCS_ComLog1)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.NACCS_ComLog1 = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id315_NACCS_ComLog2)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.NACCS_ComLog2 = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":NACCS_ComLog1, :NACCS_ComLog2");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":NACCS_ComLog1, :NACCS_ComLog2");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private SystemEnvironment Read44_SystemEnvironment(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id42_SystemEnvironment) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            SystemEnvironment o = new SystemEnvironment();
            bool[] flagArray = new bool[5];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id27_TerminalInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TerminalInfo = this.Read37_TerminalInfoSysClass(false, true);
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id29_InteractiveInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.InteractiveInfo = this.Read39_InteractiveInfoSysClass(false, true);
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id30_MailInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.MailInfo = this.Read41_MailInfoSysClass(false, true);
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id48_BatchdocInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.BatchdocInfo = this.Read42_BatchdocInfoSysClass(false, true);
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id32_LogFileLength)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.LogFileLength = this.Read43_LogFileLengthSysClass(false, true);
                        flagArray[4] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":TerminalInfo, :InteractiveInfo, :MailInfo, :BatchdocInfo, :LogFileLength");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":TerminalInfo, :InteractiveInfo, :MailInfo, :BatchdocInfo, :LogFileLength");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private UJobMenu.ListRec Read45_ListRec(bool checkType)
        {
            UJobMenu.ListRec rec;
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            if ((checkType && (type != null)) && ((type.Name != this.id50_ListRec) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            try
            {
                rec = (UJobMenu.ListRec) Activator.CreateInstance(typeof(UJobMenu.ListRec), BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, new object[0], null);
            }
            catch (MissingMethodException)
            {
                throw base.CreateInaccessibleConstructorException("global::Naccs.Core.Main.UJobMenu.ListRec");
            }
            catch (SecurityException)
            {
                throw base.CreateCtorHasSecurityException("global::Naccs.Core.Main.UJobMenu.ListRec");
            }
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(rec);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return rec;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id218_Name)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        rec.Name = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id313_URL)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        rec.URL = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(rec, ":Name, :URL");
                    }
                }
                else
                {
                    base.UnknownNode(rec, ":Name, :URL");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return rec;
        }

        private UDataView.NodeSorter Read46_NodeSorter(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id51_NodeSorter) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            UDataView.NodeSorter o = new UDataView.NodeSorter();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DataCompress.IndexRec Read47_IndexRec(bool checkType)
        {
            DataCompress.IndexRec rec;
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            if ((checkType && (type != null)) && ((type.Name != this.id52_IndexRec) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            try
            {
                rec = (DataCompress.IndexRec) Activator.CreateInstance(typeof(DataCompress.IndexRec), BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, new object[0], null);
            }
            catch (MissingMethodException)
            {
                throw base.CreateInaccessibleConstructorException("global::Naccs.Core.DataView.DataCompress.IndexRec");
            }
            catch (SecurityException)
            {
                throw base.CreateCtorHasSecurityException("global::Naccs.Core.DataView.DataCompress.IndexRec");
            }
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(rec);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return rec;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id312_Offset)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        rec.Offset = XmlConvert.ToInt64(base.Reader.ReadElementString());
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id224_Size)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        rec.Size = XmlConvert.ToInt64(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(rec, ":Offset, :Size");
                    }
                }
                else
                {
                    base.UnknownNode(rec, ":Offset, :Size");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return rec;
        }

        private TestServer Read48_TestServer(string s)
        {
            switch (s)
            {
                case "Interactive_MainCenter":
                    return TestServer.Interactive_MainCenter;

                case "Interactive_BackupCenter":
                    return TestServer.Interactive_BackupCenter;

                case "Interactive_Test":
                    return TestServer.Interactive_Test;

                case "netNACCS_MainNode_MainCenter":
                    return TestServer.netNACCS_MainNode_MainCenter;

                case "netNACCS_MainNode_BackupCenter":
                    return TestServer.netNACCS_MainNode_BackupCenter;

                case "netNACCS_MainNode_Test":
                    return TestServer.netNACCS_MainNode_Test;

                case "netNACCS_BackupNode_MainCenter":
                    return TestServer.netNACCS_BackupNode_MainCenter;

                case "netNACCS_BackupNode_BackupCenter":
                    return TestServer.netNACCS_BackupNode_BackupCenter;

                case "netNACCS_BackupNode_Test":
                    return TestServer.netNACCS_BackupNode_Test;

                case "Mail_MainCenter":
                    return TestServer.Mail_MainCenter;

                case "Mail_BackupCenter":
                    return TestServer.Mail_BackupCenter;

                case "Mail_Test":
                    return TestServer.Mail_Test;
            }
            throw base.CreateUnknownConstantException(s, typeof(TestServer));
        }

        private NetTestMode Read49_NetTestMode(string s)
        {
            switch (s)
            {
                case "All":
                    return NetTestMode.All;

                case "ping":
                    return NetTestMode.ping;

                case "ipconfig":
                    return NetTestMode.ipconfig;
            }
            throw base.CreateUnknownConstantException(s, typeof(NetTestMode));
        }

        private ReceiveInformClass Read5_ReceiveInformClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id131_ReceiveInformClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ReceiveInformClass o = new ReceiveInformClass();
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id15_OutCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.OutCode = base.Reader.Value;
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":OutCode");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id9_Inform)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Inform = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id173_InformSound)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.InformSound = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Inform, :InformSound");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Inform, :InformSound");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ApplicationClass Read50_ApplicationClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id296_ApplicationClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ApplicationClass o = new ApplicationClass();
            bool[] flagArray = new bool[13];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id297_Left)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Left = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id298_Top)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Top = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id299_Right)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Right = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id300_Bottom)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Bottom = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id301_MainSplitter)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.MainSplitter = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id302_DataViewSplitter)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.DataViewSplitter = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id303_Maximized)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Maximized = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id304_CloseOnAppend)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.CloseOnAppend = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id305_Clock)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Clock = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[8] = true;
                    }
                    else if ((!flagArray[9] && (base.Reader.LocalName == this.id306_MaxRetreiveCnt)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.MaxRetreiveCnt = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[9] = true;
                    }
                    else if ((!flagArray[10] && (base.Reader.LocalName == this.id307_DataViewFontSize)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.DataViewFontSize = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[10] = true;
                    }
                    else if ((!flagArray[11] && (base.Reader.LocalName == this.id308_LogoffConfirm)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.LogoffConfirm = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[11] = true;
                    }
                    else if ((!flagArray[12] && (base.Reader.LocalName == this.id309_SettingsDiv)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.SettingsDiv = this.Read20_SettingDivType(base.Reader.ReadElementString());
                        }
                        flagArray[12] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Left, :Top, :Right, :Bottom, :MainSplitter, :DataViewSplitter, :Maximized, :CloseOnAppend, :Clock, :MaxRetreiveCnt, :DataViewFontSize, :LogoffConfirm, :SettingsDiv");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Left, :Top, :Right, :Bottom, :MainSplitter, :DataViewSplitter, :Maximized, :CloseOnAppend, :Clock, :MaxRetreiveCnt, :DataViewFontSize, :LogoffConfirm, :SettingsDiv");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ToolBarClass Read51_ToolBarClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id292_ToolBarClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ToolBarClass o = new ToolBarClass();
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id293_Standard)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Standard = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id294_Job)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Job = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id295_FunctionBar)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.FunctionBar = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Standard, :Job, :FunctionBar");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Standard, :Job, :FunctionBar");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private WindowClass Read52_WindowClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id288_WindowClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            WindowClass o = new WindowClass();
            bool[] flagArray = new bool[4];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id289_UserInput)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.UserInput = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id290_JobInput)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.JobInput = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id291_JobMenu)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.JobMenu = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id113_DataView)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.DataView = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":UserInput, :JobInput, :JobMenu, :DataView");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":UserInput, :JobInput, :JobMenu, :DataView");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private LastLogonClass Read53_LastLogonClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id283_LastLogonClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            LastLogonClass o = new LastLogonClass();
            bool[] flagArray = new bool[4];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id284_APP)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.APP = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id285_SSO)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.SSO = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id286_POP)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.POP = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id287_UPPERCASE)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.UPPERCASE = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":APP, :SSO, :POP, :UPPERCASE");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":APP, :SSO, :POP, :UPPERCASE");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ColumnWidthClass Read54_ColumnWidthClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id272_ColumnWidthClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ColumnWidthClass o = new ColumnWidthClass();
            bool[] flagArray = new bool[15];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id154_JobCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.JobCode = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id15_OutCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.OutCode = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id167_InputInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.InputInfo = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id273_ResCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.ResCode = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id169_Pattern)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Pattern = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id274_JobInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.JobInfo = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id275_JonInfo1)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.JonInfo1 = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id276_JonInfo2)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.JonInfo2 = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id277_JonInfo3)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.JonInfo3 = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[8] = true;
                    }
                    else if ((!flagArray[9] && (base.Reader.LocalName == this.id140_TimeStamp)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.TimeStamp = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[9] = true;
                    }
                    else if ((!flagArray[10] && (base.Reader.LocalName == this.id165_DataType)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.DataType = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[10] = true;
                    }
                    else if ((!flagArray[11] && (base.Reader.LocalName == this.id164_EndFlag)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.EndFlag = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[11] = true;
                    }
                    else if ((!flagArray[12] && (base.Reader.LocalName == this.id278_TVFolder)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.TVFolder = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[12] = true;
                    }
                    else if ((!flagArray[13] && (base.Reader.LocalName == this.id279_AirSea)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.AirSea = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[13] = true;
                    }
                    else if ((!flagArray[14] && (base.Reader.LocalName == this.id280_PrintName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.PrintName = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[14] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":JobCode, :OutCode, :InputInfo, :ResCode, :Pattern, :JobInfo, :JonInfo1, :JonInfo2, :JonInfo3, :TimeStamp, :DataType, :EndFlag, :TVFolder, :AirSea, :PrintName");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":JobCode, :OutCode, :InputInfo, :ResCode, :Pattern, :JobInfo, :JonInfo1, :JonInfo2, :JonInfo3, :TimeStamp, :DataType, :EndFlag, :TVFolder, :AirSea, :PrintName");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DataViewFormClass Read55_DataViewFormClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id270_DataViewFormClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DataViewFormClass o = new DataViewFormClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id271_ReadVisible)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.ReadVisible = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":ReadVisible");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":ReadVisible");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ResultCodeColorClass Read56_ResultCodeColorClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id265_ResultCodeColorClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ResultCodeColorClass o = new ResultCodeColorClass();
            bool[] flagArray = new bool[4];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id266_Normal)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.NormalColorName = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id267_Error)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ErrorColorName = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id268_Warning)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.WarningColorName = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id269_Cmessage)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.CMessageColorName = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Normal, :Error, :Warning, :Cmessage");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Normal, :Error, :Warning, :Cmessage");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private JobOptionClass Read57_JobOptionClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id263_JobOptionClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            JobOptionClass o = new JobOptionClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id264_QueryFieldClear)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.QueryFieldClear = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":QueryFieldClear");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":QueryFieldClear");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private BatchSendClass Read58_BatchSendClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id261_BatchSendClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            BatchSendClass o = new BatchSendClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id262_Sleep)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Sleep = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Sleep");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Sleep");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private User Read59_User(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id55_User) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            User o = new User();
            if (o.HistoryJobsList == null)
            {
                o.HistoryJobsList = new StringList();
            }
            StringList historyJobsList = o.HistoryJobsList;
            if (o.HistoryUserList == null)
            {
                o.HistoryUserList = new StringList();
            }
            StringList historyUserList = o.HistoryUserList;
            if (o.HistoryAddressList == null)
            {
                o.HistoryAddressList = new StringList();
            }
            StringList historyAddressList = o.HistoryAddressList;
            if (o.HistorySearchList == null)
            {
                o.HistorySearchList = new StringList();
            }
            StringList historySearchList = o.HistorySearchList;
            bool[] flagArray = new bool[13];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id56_Application)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Application = this.Read50_ApplicationClass(false, true);
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id57_ToolBar)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ToolBar = this.Read51_ToolBarClass(false, true);
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id58_Window)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Window = this.Read52_WindowClass(false, true);
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id59_LastLogon)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.LastLogon = this.Read53_LastLogonClass(false, true);
                        flagArray[3] = true;
                    }
                    else if ((base.Reader.LocalName == this.id60_HistoryJobs) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.HistoryJobsList == null)
                            {
                                o.HistoryJobsList = new StringList();
                            }
                            StringList list = o.HistoryJobsList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id294_Job) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (base.ReadNull())
                                            {
                                                list.Add(null);
                                            }
                                            else
                                            {
                                                list.Add(base.Reader.ReadElementString());
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":Job");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":Job");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id61_ColumnWidth)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ColumnWidth = this.Read54_ColumnWidthClass(false, true);
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id62_DataViewForm)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.DataViewForm = this.Read55_DataViewFormClass(false, true);
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id63_ResultCodeColor)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ResultCodeColor = this.Read56_ResultCodeColorClass(false, true);
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id64_JobOption)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.JobOption = this.Read57_JobOptionClass(false, true);
                        flagArray[8] = true;
                    }
                    else if ((!flagArray[9] && (base.Reader.LocalName == this.id65_BatchSend)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.BatchSend = this.Read58_BatchSendClass(false, true);
                        flagArray[9] = true;
                    }
                    else if ((base.Reader.LocalName == this.id310_HistroyUser) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.HistoryUserList == null)
                            {
                                o.HistoryUserList = new StringList();
                            }
                            StringList list2 = o.HistoryUserList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num5 = 0;
                                int num6 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id204_UserCode) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (base.ReadNull())
                                            {
                                                list2.Add(null);
                                            }
                                            else
                                            {
                                                list2.Add(base.Reader.ReadElementString());
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":UserCode");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":UserCode");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num5, ref num6);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else if ((base.Reader.LocalName == this.id67_HistoryAddress) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.HistoryAddressList == null)
                            {
                                o.HistoryAddressList = new StringList();
                            }
                            StringList list3 = o.HistoryAddressList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num7 = 0;
                                int num8 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id259_Address) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (base.ReadNull())
                                            {
                                                list3.Add(null);
                                            }
                                            else
                                            {
                                                list3.Add(base.Reader.ReadElementString());
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":Address");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":Address");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num7, ref num8);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else if ((base.Reader.LocalName == this.id68_HistorySearch) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.HistorySearchList == null)
                            {
                                o.HistorySearchList = new StringList();
                            }
                            StringList list4 = o.HistorySearchList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num9 = 0;
                                int num10 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id311_SearchInfo) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (base.ReadNull())
                                            {
                                                list4.Add(null);
                                            }
                                            else
                                            {
                                                list4.Add(base.Reader.ReadElementString());
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":SearchInfo");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":SearchInfo");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num9, ref num10);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":Application, :ToolBar, :Window, :LastLogon, :HistoryJobs, :ColumnWidth, :DataViewForm, :ResultCodeColor, :JobOption, :BatchSend, :HistroyUser, :HistoryAddress, :HistorySearch");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Application, :ToolBar, :Window, :LastLogon, :HistoryJobs, :ColumnWidth, :DataViewForm, :ResultCodeColor, :JobOption, :BatchSend, :HistroyUser, :HistoryAddress, :HistorySearch");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private SoundClass Read6_SoundClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id446_SoundClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            SoundClass o = new SoundClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id447_SoundFile)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SoundFile = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":SoundFile");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":SoundFile");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private HistoryJobsClass Read60_HistoryJobsClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id281_HistoryJobsClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            HistoryJobsClass o = new HistoryJobsClass();
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id221_No)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.No = XmlConvert.ToInt32(base.Reader.Value);
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":No");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id154_JobCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.JobCode = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id282_FontSize)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.FontSize = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":JobCode, :FontSize");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":JobCode, :FontSize");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private HistoryUserClass Read61_HistoryUserClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id260_HistoryUserClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            HistoryUserClass o = new HistoryUserClass();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id221_No)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.No = XmlConvert.ToInt32(base.Reader.Value);
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":No");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id204_UserCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UserCode = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":UserCode");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":UserCode");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private HistoryAddressClass Read62_HistoryAddressClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id258_HistoryAddressClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            HistoryAddressClass o = new HistoryAddressClass();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id221_No)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.No = XmlConvert.ToInt32(base.Reader.Value);
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":No");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id259_Address)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Address = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Address");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Address");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private HistorySearchClass Read63_HistorySearchClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id256_HistorySearchClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            HistorySearchClass o = new HistorySearchClass();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id221_No)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.No = XmlConvert.ToInt32(base.Reader.Value);
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":No");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id257_SerchInfo)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.SerchInfo = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":SerchInfo");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":SerchInfo");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DetailsClass Read64_DetailsClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id70_DetailsClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DetailsClass o = new DetailsClass();
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if ((!flagArray[0] && (base.Reader.LocalName == this.id15_OutCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                {
                    o.OutCode = base.Reader.Value;
                    flagArray[0] = true;
                }
                else if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o, ":OutCode");
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[1] && (base.Reader.LocalName == this.id180_Dir)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Dir = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id181_AutoSave)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.AutoSave = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Dir, :AutoSave");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Dir, :AutoSave");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private GatewaySettingClass Read65_GatewaySettingClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id71_GatewaySettingClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            GatewaySettingClass o = new GatewaySettingClass();
            bool[] flagArray = new bool[9];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id247_UseGateway)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.UseGateway = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id248_GatewaySMTPServerName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewaySMTPServerName = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id249_GatewaySMTPServerPort)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewaySMTPServerPort = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id250_GatewayPOPServerName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewayPOPServerName = base.Reader.ReadElementString();
                        flagArray[3] = true;
                    }
                    else if ((!flagArray[4] && (base.Reader.LocalName == this.id251_GatewayPOPServerPort)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewayPOPServerPort = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[4] = true;
                    }
                    else if ((!flagArray[5] && (base.Reader.LocalName == this.id252_GatewayMailBox)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewayMailBox = base.Reader.ReadElementString();
                        flagArray[5] = true;
                    }
                    else if ((!flagArray[6] && (base.Reader.LocalName == this.id253_GatewaySMTPDomainName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewaySMTPDomainName = base.Reader.ReadElementString();
                        flagArray[6] = true;
                    }
                    else if ((!flagArray[7] && (base.Reader.LocalName == this.id254_GatewayPOPDomainName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewayPOPDomainName = base.Reader.ReadElementString();
                        flagArray[7] = true;
                    }
                    else if ((!flagArray[8] && (base.Reader.LocalName == this.id255_GatewayAddDomainName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GatewayAddDomainName = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[8] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":UseGateway, :GatewaySMTPServerName, :GatewaySMTPServerPort, :GatewayPOPServerName, :GatewayPOPServerPort, :GatewayMailBox, :GatewaySMTPDomainName, :GatewayPOPDomainName, :GatewayAddDomainName");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":UseGateway, :GatewaySMTPServerName, :GatewaySMTPServerPort, :GatewayPOPServerName, :GatewayPOPServerPort, :GatewayMailBox, :GatewaySMTPDomainName, :GatewayPOPDomainName, :GatewayAddDomainName");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DatasetRevert Read66_DatasetRevert(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id72_DatasetRevert) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DatasetRevert o = new DatasetRevert();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private PrintStatus Read67_PrintStatus(string s)
        {
            switch (s)
            {
                case "Print":
                    return PrintStatus.Print;

                case "HeardcopyPrint":
                    return PrintStatus.HeardcopyPrint;

                case "DataviewPrint":
                    return PrintStatus.DataviewPrint;
            }
            throw base.CreateUnknownConstantException(s, typeof(PrintStatus));
        }

        private RevertEventArgs Read69_RevertEventArgs(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id74_RevertEventArgs) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            RevertEventArgs o = new RevertEventArgs();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id245_PathRoot)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.PathRoot = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((base.Reader.LocalName == this.id246_FileNames) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            string[] a = null;
                            int index = 0;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num4 = 0;
                                int num5 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id96_string) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (base.ReadNull())
                                            {
                                                a = (string[]) base.EnsureArrayIndex(a, index, typeof(string));
                                                a[index++] = null;
                                            }
                                            else
                                            {
                                                a = (string[]) base.EnsureArrayIndex(a, index, typeof(string));
                                                a[index++] = base.Reader.ReadElementString();
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":string");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":string");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num4, ref num5);
                                }
                                base.ReadEndElement();
                            }
                            o.FileNames = (string[]) base.ShrinkArray(a, index, typeof(string), false);
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":PathRoot, :FileNames");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":PathRoot, :FileNames");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private WarningInformClass Read7_WarningInformClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id444_WarningInformClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            WarningInformClass o = new WarningInformClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id445_WaringMode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.WaringMode = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":WaringMode");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":WaringMode");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DatasetStorage Read70_DatasetStorage(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id75_DatasetStorage) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DatasetStorage o = new DatasetStorage();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ArrangeDataTable Read71_ArrangeDataTable(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id76_ArrangeDataTable) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ArrangeDataTable o = new ArrangeDataTable();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private CustomizeMenu Read72_CustomizeMenu(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id77_CustomizeMenu) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            CustomizeMenu o = new CustomizeMenu();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private CommonPathClass Read73_CommonPathClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id243_CommonPathClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            CommonPathClass o = new CommonPathClass();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id244_Specify)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.Specify = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id209_Folder)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Folder = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Specify, :Folder");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Specify, :Folder");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private UserPathNames Read74_UserPathNames(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id78_UserPathNames) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            UserPathNames o = new UserPathNames();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id79_CommonPath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.CommonPath = this.Read73_CommonPathClass(false, true);
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":CommonPath");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":CommonPath");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private RemoveWorkerArgs Read75_RemoveWorkerArgs(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id80_RemoveWorkerArgs) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            RemoveWorkerArgs o = new RemoveWorkerArgs();
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id240_Table)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Table = (DataTable) base.ReadSerializable(new DataTable());
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id241_RemoveDate)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.RemoveDate = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id242_ResetID)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.ResetID = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Table, :RemoveDate, :ResetID");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Table, :RemoveDate, :ResetID");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private HelpPathClass Read76_HelpPathClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id235_HelpPathClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            HelpPathClass o = new HelpPathClass();
            bool[] flagArray = new bool[3];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id236_JobErrCodeHtml)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GymErrIndexHtmlUser = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id237_TrmErrCodeHtml)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.TermMsgIndexHtmlUser = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id238_GuideIndex)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.GuideIndexHtmlUser = base.Reader.ReadElementString();
                        flagArray[2] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":JobErrCodeHtml, :TrmErrCodeHtml, :GuideIndex");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":JobErrCodeHtml, :TrmErrCodeHtml, :GuideIndex");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private HelpSettings Read77_HelpSettings(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id239_HelpSettings) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            HelpSettings o = new HelpSettings();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id82_HelpPath)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.HelpPath = this.Read76_HelpPathClass(false, true);
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":HelpPath");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":HelpPath");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ConfigFiles Read78_ConfigFiles(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id83_ConfigFiles) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ConfigFiles o = new ConfigFiles();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private RemoveWorkerResult Read79_RemoveWorkerResult(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id84_RemoveWorkerResult) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            RemoveWorkerResult o = new RemoveWorkerResult();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id233_IsCancel)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.IsCancel = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id234_RemoveCount)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.RemoveCount = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":IsCancel, :RemoveCount");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":IsCancel, :RemoveCount");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private InformClass Read8_InformClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id442_InformClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            InformClass o = new InformClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id443_InfoMode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (base.Reader.IsEmptyElement)
                        {
                            base.Reader.Skip();
                        }
                        else
                        {
                            o.InfoMode = XmlConvert.ToBoolean(base.Reader.ReadElementString());
                        }
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":InfoMode");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":InfoMode");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DatasetRemove Read80_DatasetRemove(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id85_DatasetRemove) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DatasetRemove o = new DatasetRemove();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private AllCertificationClass Read81_AllCertificationClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id231_AllCertificationClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            AllCertificationClass o = new AllCertificationClass();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id232_Password)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.EncryptPassword = base.ToByteArrayBase64(false);
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Password");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Password");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private OptionCertification Read82_OptionCertification(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id86_OptionCertification) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            OptionCertification o = new OptionCertification();
            bool[] flagArray = new bool[1];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id87_AllCertification)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.AllCertification = this.Read81_AllCertificationClass(false, true);
                        flagArray[0] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":AllCertification");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":AllCertification");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ComboManager Read83_ComboManager(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id88_ComboManager) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ComboManager o = new ComboManager();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private DataFactory Read84_DataFactory(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id89_DataFactory) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            DataFactory o = new DataFactory();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private UserKey Read85_UserKey(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id90_UserKey) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            UserKey o = new UserKey();
            if (o.JobKeyList == null)
            {
                o.JobKeyList = new JobKeyListClass();
            }
            JobKeyListClass jobKeyList = o.JobKeyList;
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id230_JobKeyList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.JobKeyList == null)
                            {
                                o.JobKeyList = new JobKeyListClass();
                            }
                            JobKeyListClass class2 = o.JobKeyList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id91_JobKey) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class2 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class2.Add(this.Read23_JobKeyClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":JobKey");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":JobKey");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":JobKeyList");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":JobKeyList");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private JobFormFactory Read86_JobFormFactory(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id97_JobFormFactory) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            JobFormFactory o = new JobFormFactory();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ULogClass Read87_ULogClass(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id98_ULogClass) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ULogClass o = new ULogClass();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ModeFont Read88_ModeFont(string s)
        {
            switch (s)
            {
                case "normal":
                    return ModeFont.normal;

                case "large":
                    return ModeFont.large;

                case "small":
                    return ModeFont.small;
            }
            throw base.CreateUnknownConstantException(s, typeof(ModeFont));
        }

        private JobPanelStatus Read89_JobPanelStatus(string s)
        {
            switch (s)
            {
                case "Open":
                    return JobPanelStatus.Open;

                case "Closed":
                    return JobPanelStatus.Closed;

                case "Error":
                    return JobPanelStatus.Error;
            }
            throw base.CreateUnknownConstantException(s, typeof(JobPanelStatus));
        }

        private ReceiveNotice Read9_ReceiveNotice(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id4_ReceiveNotice) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            ReceiveNotice o = new ReceiveNotice();
            if (o.ReceiveInformList == null)
            {
                o.ReceiveInformList = new ReceiveInformListClass();
            }
            ReceiveInformListClass receiveInformList = o.ReceiveInformList;
            bool[] flagArray = new bool[4];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id448_ReceiveInformList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.ReceiveInformList == null)
                            {
                                o.ReceiveInformList = new ReceiveInformListClass();
                            }
                            ReceiveInformListClass class2 = o.ReceiveInformList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id6_ReceiveInform) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (class2 == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                class2.Add(this.Read5_ReceiveInformClass(true, true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":ReceiveInform");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":ReceiveInform");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id7_Sound)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Sound = this.Read6_SoundClass(false, true);
                        flagArray[1] = true;
                    }
                    else if ((!flagArray[2] && (base.Reader.LocalName == this.id8_WarningInform)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.WarningInform = this.Read7_WarningInformClass(false, true);
                        flagArray[2] = true;
                    }
                    else if ((!flagArray[3] && (base.Reader.LocalName == this.id9_Inform)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Inform = this.Read8_InformClass(false, true);
                        flagArray[3] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":ReceiveInformList, :Sound, :WarningInform, :Inform");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":ReceiveInformList, :Sound, :WarningInform, :Inform");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private VersionuptoolUpdater Read90_VersionuptoolUpdater(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id101_VersionuptoolUpdater) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            VersionuptoolUpdater o = new VersionuptoolUpdater();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private ArrangeProgress.ProgresType Read91_ProgresType(string s)
        {
            switch (s)
            {
                case "Remove":
                    return ArrangeProgress.ProgresType.Remove;

                case "Revert":
                    return ArrangeProgress.ProgresType.Revert;

                case "NoType":
                    return ArrangeProgress.ProgresType.NoType;
            }
            throw base.CreateUnknownConstantException(s, typeof(ArrangeProgress.ProgresType));
        }

        private VersionSettings Read92_VersionSettings(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id103_VersionSettings) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            VersionSettings o = new VersionSettings();
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id56_Application)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Application = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id217_Template)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        o.Template = XmlConvert.ToInt32(base.Reader.ReadElementString());
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(o, ":Application, :Template");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":Application, :Template");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private PrintManager Read93_PrintManager(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id104_PrintManager) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            PrintManager o = new PrintManager();
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    base.UnknownNode(o, "");
                }
                else
                {
                    base.UnknownNode(o, "");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }

        private USendReportDlg.ReportStatus Read94_ReportStatus(string s)
        {
            switch (s)
            {
                case "None":
                    return USendReportDlg.ReportStatus.None;

                case "Wait":
                    return USendReportDlg.ReportStatus.Wait;

                case "Sent":
                    return USendReportDlg.ReportStatus.Sent;

                case "Error":
                    return USendReportDlg.ReportStatus.Error;

                case "Delete":
                    return USendReportDlg.ReportStatus.Delete;
            }
            throw base.CreateUnknownConstantException(s, typeof(USendReportDlg.ReportStatus));
        }

        private DataStatus Read95_DataStatus(string s)
        {
            switch (s)
            {
                case "Send":
                    return DataStatus.Send;

                case "Sent":
                    return DataStatus.Sent;

                case "Recept":
                    return DataStatus.Recept;
            }
            throw base.CreateUnknownConstantException(s, typeof(DataStatus));
        }

        private Signflg Read96_Signflg(string s)
        {
            switch (s)
            {
                case "NoSigned":
                    return Signflg.NoSigned;

                case "Signed":
                    return Signflg.Signed;

                case "VerOK":
                    return Signflg.VerOK;

                case "VerNG":
                    return Signflg.VerNG;
            }
            throw base.CreateUnknownConstantException(s, typeof(Signflg));
        }

        private ContainedType Read97_ContainedType(string s)
        {
            switch (s)
            {
                case "SendOnly":
                    return ContainedType.SendOnly;

                case "ReceiveOnly":
                    return ContainedType.ReceiveOnly;

                case "SendReceive":
                    return ContainedType.SendReceive;
            }
            throw base.CreateUnknownConstantException(s, typeof(ContainedType));
        }

        private OutcodeTbl.OutcodeRec Read98_OutcodeRec(bool checkType)
        {
            OutcodeTbl.OutcodeRec rec;
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            if ((checkType && (type != null)) && ((type.Name != this.id110_OutcodeRec) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            try
            {
                rec = (OutcodeTbl.OutcodeRec) Activator.CreateInstance(typeof(OutcodeTbl.OutcodeRec), BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, new object[0], null);
            }
            catch (MissingMethodException)
            {
                throw base.CreateInaccessibleConstructorException("global::Naccs.Core.BatchDoc.OutcodeTbl.OutcodeRec");
            }
            catch (SecurityException)
            {
                throw base.CreateCtorHasSecurityException("global::Naccs.Core.BatchDoc.OutcodeTbl.OutcodeRec");
            }
            bool[] flagArray = new bool[2];
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(rec);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return rec;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((!flagArray[0] && (base.Reader.LocalName == this.id15_OutCode)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        rec.OutCode = base.Reader.ReadElementString();
                        flagArray[0] = true;
                    }
                    else if ((!flagArray[1] && (base.Reader.LocalName == this.id215_DocName)) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        rec.DocName = base.Reader.ReadElementString();
                        flagArray[1] = true;
                    }
                    else
                    {
                        base.UnknownNode(rec, ":OutCode, :DocName");
                    }
                }
                else
                {
                    base.UnknownNode(rec, ":OutCode, :DocName");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return rec;
        }

        private OutcodeTbl Read99_OutcodeTbl(bool isNullable, bool checkType)
        {
            XmlQualifiedName type = checkType ? base.GetXsiType() : null;
            bool flag = false;
            if (isNullable)
            {
                flag = base.ReadNull();
            }
            if ((checkType && (type != null)) && ((type.Name != this.id109_OutcodeTbl) || (type.Namespace != this.id2_Item)))
            {
                throw base.CreateUnknownTypeException(type);
            }
            if (flag)
            {
                return null;
            }
            OutcodeTbl o = new OutcodeTbl();
            if (o.OutcodeList == null)
            {
                o.OutcodeList = new List<OutcodeTbl.OutcodeRec>();
            }
            List<OutcodeTbl.OutcodeRec> outcodeList = o.OutcodeList;
            while (base.Reader.MoveToNextAttribute())
            {
                if (!base.IsXmlnsAttribute(base.Reader.Name))
                {
                    base.UnknownNode(o);
                }
            }
            base.Reader.MoveToElement();
            if (base.Reader.IsEmptyElement)
            {
                base.Reader.Skip();
                return o;
            }
            base.Reader.ReadStartElement();
            base.Reader.MoveToContent();
            int whileIterations = 0;
            int readerCount = base.ReaderCount;
            while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
            {
                if (base.Reader.NodeType == XmlNodeType.Element)
                {
                    if ((base.Reader.LocalName == this.id216_OutcodeList) && (base.Reader.NamespaceURI == this.id2_Item))
                    {
                        if (!base.ReadNull())
                        {
                            if (o.OutcodeList == null)
                            {
                                o.OutcodeList = new List<OutcodeTbl.OutcodeRec>();
                            }
                            List<OutcodeTbl.OutcodeRec> list = o.OutcodeList;
                            if (base.Reader.IsEmptyElement)
                            {
                                base.Reader.Skip();
                            }
                            else
                            {
                                base.Reader.ReadStartElement();
                                base.Reader.MoveToContent();
                                int num3 = 0;
                                int num4 = base.ReaderCount;
                                while ((base.Reader.NodeType != XmlNodeType.EndElement) && (base.Reader.NodeType != XmlNodeType.None))
                                {
                                    if (base.Reader.NodeType == XmlNodeType.Element)
                                    {
                                        if ((base.Reader.LocalName == this.id110_OutcodeRec) && (base.Reader.NamespaceURI == this.id2_Item))
                                        {
                                            if (list == null)
                                            {
                                                base.Reader.Skip();
                                            }
                                            else
                                            {
                                                list.Add(this.Read98_OutcodeRec(true));
                                            }
                                        }
                                        else
                                        {
                                            base.UnknownNode(null, ":OutcodeRec");
                                        }
                                    }
                                    else
                                    {
                                        base.UnknownNode(null, ":OutcodeRec");
                                    }
                                    base.Reader.MoveToContent();
                                    base.CheckReaderCount(ref num3, ref num4);
                                }
                                base.ReadEndElement();
                            }
                        }
                    }
                    else
                    {
                        base.UnknownNode(o, ":OutcodeList");
                    }
                }
                else
                {
                    base.UnknownNode(o, ":OutcodeList");
                }
                base.Reader.MoveToContent();
                base.CheckReaderCount(ref whileIterations, ref readerCount);
            }
            base.ReadEndElement();
            return o;
        }
    }
}

