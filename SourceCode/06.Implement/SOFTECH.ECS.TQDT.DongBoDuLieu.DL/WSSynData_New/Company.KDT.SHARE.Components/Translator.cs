﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Company.KDT.SHARE.Components
{
    public class Translator
    {
        public static void CopyData(object source, object destination)
        {
            TranslateObject(destination.GetType(), destination, source);
        }

        public static List<T> TranslateListObject<T>(List<object> list)
        {
            return (TranslateListObject(typeof(List<T>), list) as List<T>);
        }

        public static List<T> TranslateListObject<T>(IEnumerable list)
        {
            return (TranslateListObject(typeof(List<T>), list) as List<T>);
        }

        public static object TranslateListObject(Type typ, object Source)
        {
            object destination = null;
            if (typ.IsGenericType)
            {
                destination = Activator.CreateInstance(typ);
            }
            else if (typ.IsArray)
            {
                destination = new ArrayList();
            }
            return TranslateListObject(typ, destination, Source);
        }

        public static object TranslateListObject(Type TType, object Destination, object Source)
        {
            if (Source == null)
            {
                return null;
            }
            if (Destination == null)
            {
                return TranslateListObject(TType, Source);
            }
            Type type = Source.GetType();
            if (TType.IsGenericType || type.BaseType.IsGenericType)
            {
                Type typ = TType.GetGenericArguments()[0];
                IList list = Destination as IList;
                if (type.IsGenericType || type.BaseType.IsGenericType)
                {
                    if (type.GetInterface("IList", true) != null)
                    {
                        foreach (object obj2 in Source as IList)
                        {
                            list.Add(TranslateObject(typ, obj2));
                        }
                        return list;
                    }
                    if (type.GetInterface("IEnumerable", true) != null)
                    {
                        foreach (object obj3 in Source as IEnumerable)
                        {
                            list.Add(TranslateObject(typ, obj3));
                        }
                    }
                    return list;
                }
                if (type.IsArray)
                {
                    foreach (object obj4 in Source as Array)
                    {
                        list.Add(TranslateObject(typ, obj4));
                    }
                }
                return list;
            }
            if (!type.IsArray)
            {
                return null;
            }
            Type elementType = TType.GetElementType();
            ArrayList list2 = Destination as ArrayList;
            if (type.IsGenericType || TType.BaseType.IsGenericType)
            {
                if (type.GetInterface("IList", true) != null)
                {
                    foreach (object obj5 in Source as IList)
                    {
                        list2.Add(TranslateObject(elementType, obj5));
                    }
                }
                else if (type.GetInterface("IEnumerable", true) != null)
                {
                    foreach (object obj6 in Source as IEnumerable)
                    {
                        list2.Add(TranslateObject(elementType, obj6));
                    }
                }
            }
            else if (type.IsArray)
            {
                foreach (object obj7 in Source as IList)
                {
                    list2.Add(TranslateObject(elementType, obj7));
                }
            }
            return list2.ToArray(elementType);
        }

        public static T TranslateObject<T>(object value)
        {
            return (T)TranslateObject(typeof(T), value);
        }

        public static object TranslateObject(Type typ, object value)
        {
            if (typ.IsSealed)
            {
                return value;
            }
            return TranslateObject(typ, Activator.CreateInstance(typ), value);
        }

        public static object TranslateObject(Type typ, object objRtn, object value)
        {
            if (value == null)
            {
                return null;
            }
            if (objRtn == null)
            {
                TranslateObject(typ, value);
            }
            foreach (PropertyInfo info in value.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase))
            {
                if (info.CanRead)
                {
                    PropertyInfo property = objRtn.GetType().GetProperty(info.Name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                    if (((property != null) && property.CanRead) && property.CanWrite)
                    {
                        if (property.PropertyType.IsSealed)
                        {
                            try
                            {
                                property.SetValue(objRtn, info.GetValue(value, null), null);
                            }
                            catch
                            {
                            }
                        }
                        else if (property.PropertyType.IsGenericType || property.PropertyType.IsArray)
                        {
                            property.SetValue(objRtn, TranslateListObject(property.PropertyType, info.GetValue(value, null)), null);
                        }
                        else
                        {
                            property.SetValue(objRtn, TranslateObject(property.PropertyType, info.GetValue(value, null)), null);
                        }
                    }
                }
            }
            return objRtn;
        }
    }
}

