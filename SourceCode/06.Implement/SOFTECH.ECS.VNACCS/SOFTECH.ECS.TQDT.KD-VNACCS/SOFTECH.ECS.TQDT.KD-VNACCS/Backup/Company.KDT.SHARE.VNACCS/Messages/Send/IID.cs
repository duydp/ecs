﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class IID : BasicVNACC
    {

        public PropertiesAttribute ICN { get; set; }
        public IID()
        {
            ICN = new PropertiesAttribute(12, typeof(decimal));
        }
    }
}
