﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
   public class KD_BoSungCo:DeclarationBase
    {
      
     
       //[XmlElement("Importer")]
       //public NameBase Importer { get; set; }
       [XmlElement("DeclarationDocument")]
       public DeclarationBase DeclarationDocument { get; set; }
       [XmlArray("CertificateOfOrigins")]
       [XmlArrayItem("CertificateOfOrigin")]
       public List<CertificateOfOrigin> CertificateOfOrigins { get; set; }

    }
}
