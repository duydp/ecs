using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class SAA_HANGHOA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute GDA { get; set; }
public PropertiesAttribute HSC { get; set; }
public PropertiesAttribute SEX { get; set; }
public PropertiesAttribute AGE { get; set; }
public PropertiesAttribute PRC { get; set; }
public PropertiesAttribute PCM { get; set; }
public PropertiesAttribute TQ { get; set; }
public PropertiesAttribute TQU { get; set; }
public PropertiesAttribute SOG { get; set; }
public PropertiesAttribute NW { get; set; }
public PropertiesAttribute NWU { get; set; }
public PropertiesAttribute GW { get; set; }
public PropertiesAttribute GWU { get; set; }
public PropertiesAttribute QQ { get; set; }
public PropertiesAttribute QQU { get; set; }
public PropertiesAttribute KPG { get; set; }
public PropertiesAttribute UP { get; set; }

public SAA_HANGHOA()
        {
GDA = new PropertiesAttribute(768, typeof(string));
HSC = new PropertiesAttribute(12, typeof(string));
SEX = new PropertiesAttribute(1, typeof(string));
AGE = new PropertiesAttribute(3, typeof(int));
PRC = new PropertiesAttribute(150, typeof(string));
PCM = new PropertiesAttribute(50, typeof(string));
TQ = new PropertiesAttribute(8, typeof(int));
TQU = new PropertiesAttribute(3, typeof(string));
SOG = new PropertiesAttribute(10, typeof(string));
NW = new PropertiesAttribute(10, typeof(int));
NWU = new PropertiesAttribute(3, typeof(string));
GW = new PropertiesAttribute(10, typeof(int));
GWU = new PropertiesAttribute(3, typeof(string));
QQ = new PropertiesAttribute(8, typeof(int));
QQU = new PropertiesAttribute(3, typeof(string));
KPG = new PropertiesAttribute(100, typeof(string));
UP = new PropertiesAttribute(300, typeof(string));
TongSoByte = 1476;
}

}public partial class EnumGroupID
    {
}

}
