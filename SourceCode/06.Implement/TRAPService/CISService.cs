﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KDTService
{
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Services.WebServiceBinding(ConformsTo = (System.Web.Services.WsiProfiles)1)]
    [System.Web.Services.WebService(Namespace = "http://cis.customs.gov.vn/")]
    public class CISService
    {
        static CISService()
        {
        }
        [System.Diagnostics.DebuggerNonUserCode]
        public CISService()
        {
        }
        [System.Web.Services.WebMethod(Description = "Ti\u1EBFp nh\u1EADn Message c\u1EE7a doanh nghi\u1EC7p.")]
        public string Send(string MessageXML, string UserId, string Password)
        {
            string ret = string.Empty;

            try
            {
                Helpers.SaveMessage(MessageXML, Password, "Gửi đi");
                HQCisService.CISService cis = new HQCisService.CISService();
                cis.Url = Properties.Settings.Default.KDTService_HQCisService_CISService;
                //cis.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                ret = cis.Send(MessageXML, UserId, Password);
                Helpers.SaveMessage(ret, Password, "Trả về từ hải quan");

                return ret;
            }
            catch (Exception ex)
            {
                Helpers.SaveMessage(ex.StackTrace, Password, "Lỗi trả về từ service");
                return ret;

            }

        }
        [System.Web.Services.WebMethod(Description = "Ki\u1EC3m tra tr\u1EA1ng th\u00E1i Webservice ti\u1EBFp nh\u1EADn.")]
        public string TestWebservie()
        {
            return "SUCCESSFULL";
        }
    }
}
