using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class ITI : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute IEI { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute TED { get; set; }
public PropertiesAttribute UCD { get; set; }
public PropertiesAttribute ICN { get; set; }

public ITI()
        {
IEI = new PropertiesAttribute(1, typeof(string));
CH = new PropertiesAttribute(6, typeof(string));
TED = new PropertiesAttribute(8, typeof(DateTime));
UCD = new PropertiesAttribute(13, typeof(string));
ICN = new PropertiesAttribute(12, typeof(int));
TongSoByte = 50;
}

}public partial class EnumGroupID
    {
}

}
