﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.OleDb;

namespace ServerRemote.BLL
{
    public partial class SignLogger
    {
        public static List<SignLogger> SelectCollectionAccess(string AppStaupPath, int numTop, string where, string orderBy)
        {
            string querry = string.Empty;
            if (numTop == 0)
                querry = "Select * from t_SignLogger";
            else
                querry = "Select Top " + numTop + " * From t_SignLogger";
            if (!string.IsNullOrEmpty(where))
                querry += " Where " + where;
            if (!string.IsNullOrEmpty(orderBy))
                querry += " Order by " + orderBy;
            string StrConnection = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};
Jet OLEDB:Database Password={1}", AppStaupPath + "\\DATA\\SignRemote.mdb", "ECS@123$%^");
            List<SignLogger> collection = new List<SignLogger>();
            using (OleDbConnection conn = new OleDbConnection(StrConnection))
            {
                try
                {
                    OleDbCommand command = new OleDbCommand(querry, conn);
                    conn.Open();
                    OleDbDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    SignLogger entity = new SignLogger();
                    if (!dataReader.IsDBNull(dataReader.GetOrdinal("ID"))) entity.ID = System.Convert.ToInt64(dataReader.GetValue(dataReader.GetOrdinal("ID")));
                    if (!dataReader.IsDBNull(dataReader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = dataReader.GetValue(dataReader.GetOrdinal("MSG_FROM")).ToString();
                    if (!dataReader.IsDBNull(dataReader.GetOrdinal("MSG_TO"))) entity.MSG_TO = dataReader.GetValue(dataReader.GetOrdinal("MSG_TO")).ToString();
                    if (!dataReader.IsDBNull(dataReader.GetOrdinal("TIME_PROCESS"))) entity.TIME_PROCESS = System.Convert.ToDateTime(dataReader.GetValue(dataReader.GetOrdinal("TIME_PROCESS")));
                    if (!dataReader.IsDBNull(dataReader.GetOrdinal("MESSAGE"))) entity.MESSAGE = dataReader.GetValue(dataReader.GetOrdinal("MESSAGE")).ToString();
                    if (!dataReader.IsDBNull(dataReader.GetOrdinal("DATECREATE"))) entity.DATECREATE =System.Convert.ToDateTime(dataReader.GetValue(dataReader.GetOrdinal("DATECREATE")));
                    collection.Add(entity);
                }

                dataReader.Close();
                }
                catch (System.Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    conn.Close();
                    throw ex;
                }
                conn.Close();
            }
            return collection;
        }


        public long InsertAccess(string AppStaupPath)
        {
            string InsertStr = @"Insert into t_SignLogger (MSG_FROM,MSG_TO,TIME_PROCESS,MESSAGE,DATECREATE) 
                                Values(@msg_from,@msg_to,@time_process,@message,@datecreate)";
            string query2 = "Select @@Identity from t_SignLogger";
            string StrConnection = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};
Jet OLEDB:Database Password={1}", AppStaupPath + "\\Data\\SignRemote.mdb", "ECS@123$%^");
            using (OleDbConnection conn = new OleDbConnection(StrConnection))
            {
                try
                {
                    OleDbCommand command = new OleDbCommand(InsertStr, conn);
                    conn.Open();
                    
                    command.Parameters.AddWithValue("@msg_from",this.MSG_FROM);
                    command.Parameters.AddWithValue("@msg_to",this.MSG_TO);
                    command.Parameters.AddWithValue("@time_process", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    command.Parameters.AddWithValue("@message",this.MESSAGE);
                    command.Parameters.AddWithValue("@datecreate", this.DATECREATE.ToString("yyyy-MM-dd HH:mm:ss"));
                    
                    command.ExecuteNonQuery();

                    command.CommandText = query2;
                    
                    this.ID = System.Convert.ToInt32(command.ExecuteScalar());
                    conn.Close();
                    return this.ID;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    conn.Close();
                    return 0;
                }
            }

        }
    }
}
