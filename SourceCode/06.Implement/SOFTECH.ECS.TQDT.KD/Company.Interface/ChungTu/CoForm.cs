﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Janus.Windows.GridEX;

using Company.KD.BLL;
using Company.KD.BLL.KDT;

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class CoForm : Company.Interface.BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public CO Co = new CO();
        public CoForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            if (CO.checkSoCOExit(txtSoCO.Text.Trim(), GlobalSettings.MA_DON_VI, TKMD.ID, Co.ID))
            {
                ShowMessage("Số CO này đã tồn tại.", false);
                return;
            }
            if (chkNoCo.Checked)
            {
                if (ccNgayNopCO.Text == "" || ccNgayNopCO.Value.Year <= 1900)
                {
                    ShowMessage("Nhập thông tin ngày nộp CO.", false);
                    return;
                }
            }
            Co.LoaiCO = cbLoaiCO.SelectedValue.ToString();
            Co.MaNuocNKTrenCO = ctrMaNuocNK.Ma;
            Co.MaNuocXKTrenCO = ctrMaNuocXK.Ma;
            Co.NgayCO = ccNgayCO.Value;
            Co.NuocCapCO = ctrNuocCapCO.Ma;
            Co.SoCO = txtSoCO.Text.Trim();
            Co.TenDiaChiNguoiNK = txtDiaChiNguoiNK.Text.Trim();
            Co.TenDiaChiNguoiXK = txtDiaChiNguoiXK.Text.Trim();
            Co.ThongTinMoTaChiTiet = txtThongTinMoTa.Text.Trim();
            Co.ToChucCap = txtToChucCap.Text.Trim();
            Co.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            Co.TKMD_ID = TKMD.ID;
            Co.NguoiKy = txtNguoiKy.Text.Trim();
            Co.ThoiHanNop = ccNgayNopCO.Value;
            if (string.IsNullOrEmpty(Co.GuidStr)) Co.GuidStr = Guid.NewGuid().ToString();
            if (chkNoCo.Checked)
                Co.NoCo = 1;
            else
                Co.NoCo = 0;
            if (isKhaiBoSung)
                Co.LoaiKB = 1;
            else
                Co.LoaiKB = 0;
            bool isUpdate = false;
            try
            {
                if (Co.ID == 0)
                    Co.Insert();
                else
                {
                    Co.Update();
                    isUpdate = true;
                }
                if (!isUpdate)
                    TKMD.COCollection.Add(Co);
                ShowMessage("Lưu thành công.", false);
                if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET && TKMD.SoToKhai > 0)
                    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }


        private void CoForm_Load(object sender, EventArgs e)
        {
            if (isKhaiBoSung)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }


            cbLoaiCO.DataSource = Company.KD.BLL.DuLieuChuan.LoaiCO.SelectAll().Tables[0];
            cbLoaiCO.ValueMember = "Ma";
            cbLoaiCO.DisplayMember = "Ten";
            cbLoaiCO.SelectedIndex = 0;

            //DATLMQ bổ sung tạo mới Co 17/01/2011
            if (Co != null && Co.ID <= 0)
            {
                txtDiaChiNguoiXK.Text = TKMD.TenDonViDoiTac;
                txtDiaChiNguoiNK.Text = TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI;
            }
            else if (Co != null && Co.ID > 0)
            {
                ctrMaNuocNK.Ma = Co.MaNuocNKTrenCO;
                ctrMaNuocXK.Ma = Co.MaNuocXKTrenCO;
                ccNgayCO.Text = Co.NgayCO.ToShortDateString();
                ctrNuocCapCO.Ma = Co.NuocCapCO;
                txtSoCO.Text = Co.SoCO;
                txtDiaChiNguoiNK.Text = Co.TenDiaChiNguoiNK;
                txtDiaChiNguoiXK.Text = Co.TenDiaChiNguoiXK;
                txtThongTinMoTa.Text = Co.ThongTinMoTaChiTiet;
                txtToChucCap.Text = Co.ToChucCap;
                cbLoaiCO.SelectedValue = Co.LoaiCO;
                txtNguoiKy.Text = Co.NguoiKy;
                if (Co.NoCo == 0)
                    chkNoCo.Checked = false;
                else
                {
                    chkNoCo.Checked = true;
                    ccNgayNopCO.Value = Co.ThoiHanNop;
                }
            }
            if (TKMD.ID > 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;

            }
            if (Co.SoTiepNhan > 0)
            {
                ccNgayTiepNhan.Text = Co.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = Co.SoTiepNhan + "";
            }
            SetButtonStateCO(TKMD, isKhaiBoSung, Co);
        }

        private void btnChonCO_Click(object sender, EventArgs e)
        {
            ManageCOForm f = new ManageCOForm();
            f.isBrower = true;
            f.TKMD_ID = TKMD.ID;
            f.ShowDialog();
            if (f.Co != null)
            {
                Co = f.Co;
                ctrMaNuocNK.Ma = Co.MaNuocNKTrenCO;
                ctrMaNuocXK.Ma = Co.MaNuocXKTrenCO;
                ccNgayCO.Text = Co.NgayCO.ToShortDateString();
                ctrNuocCapCO.Ma = Co.NuocCapCO;
                txtSoCO.Text = Co.SoCO;
                txtDiaChiNguoiNK.Text = Co.TenDiaChiNguoiNK;
                txtDiaChiNguoiXK.Text = Co.TenDiaChiNguoiXK;
                txtThongTinMoTa.Text = Co.ThongTinMoTaChiTiet;
                txtToChucCap.Text = Co.ToChucCap;
                cbLoaiCO.SelectedValue = Co.LoaiCO;
                txtNguoiKy.Text = Co.NguoiKy;
                Co.ID = 0;
                Co.GuidStr = "";
                Co.SoTiepNhan = 0;
                Co.NgayTiepNhan = new DateTime(1900, 1, 1);
                Co.NamTiepNhan = 0;
                Co.TKMD_ID = TKMD.ID;
                if (Co.NoCo == 0)
                    chkNoCo.Checked = false;
                else
                {
                    chkNoCo.Checked = true;
                    ccNgayNopCO.Value = Co.ThoiHanNop;
                }

            }
        }



        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void chkNoCo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNoCo.Checked)
            {
                lblNgayNopCo.Visible = true;
                ccNgayNopCO.Visible = true;
            }
            else
            {
                lblNgayNopCo.Visible = false;
                ccNgayNopCO.Visible = false;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (Co != null && Co.ID > 0)
            {
                Co.Delete();
                this.Close();
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (Co.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                bool thanhcong = Co.WSKhaiBaoBoSungCO(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NgayDangKy.Year, TKMD.ID, TKMD.MaDoanhNghiep, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao);
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Khai báo không thành công : " + ex.Message.ToString(), false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                if (this.Co.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.Co.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Nội dung :" + ex.Message.ToString(), false);
            }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (Co.GuidStr != null && Co.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(Co.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CO.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCO(Company.KD.BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.CO co)
        {
            if (co == null)
                return false;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnChonGP.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    btnXoa.Enabled = status;
                    btnChonGP.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = true;
                    //Neu hua co so tiep nhan -> phai khai bao
                    if (co.SoTiepNhan == 0)
                    {
                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                    }
                    //Neu da co so tiep nhan -> co the lay phan hoi
                    else
                    {
                        btnKhaiBao.Enabled = false;
                        btnLayPhanHoi.Enabled = true;
                    }
                }
            }

            return true;
        }

        #endregion

        /// <summary>
        /// Lấy phản hồi sau khi khai báo. Mục đích là lấy số tiếp nhận điện tử.
        /// </summary>
        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = Co.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungCOThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.Co.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.Co.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.Co.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        /// <summary>
        /// Lấy thông tin phản hồi sau khi đã khai báo có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = Co.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungCODuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.Co.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    this.ShowMessage(message, false);
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }
    }
}

