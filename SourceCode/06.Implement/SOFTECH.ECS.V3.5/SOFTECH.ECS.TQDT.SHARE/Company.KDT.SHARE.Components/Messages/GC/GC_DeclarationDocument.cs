﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Người khai HQ
    /// </summary>
    public class GC_DeclarationDocuments
    {

        [XmlElement("DeclarationDocument")]
        public List<GC_DeclarationDocument> DeclarationDocument { get; set; }


    }
    public class GC_DeclarationDocument : DeclarationBase
    {

        [XmlElement("Product")]
        public List<GC_Product> Product { get; set; }

        [XmlElement("Material")]
        public List<GC_Material> Material { get; set; }

        [XmlElement("ExportDeclarationDocument")]
        public List<ExportDeclarationDocument> exportDeclarationDocuments { get; set; }
    }

}
