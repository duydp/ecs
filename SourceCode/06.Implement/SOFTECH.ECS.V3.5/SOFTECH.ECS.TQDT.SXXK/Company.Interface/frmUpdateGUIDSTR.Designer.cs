﻿namespace Company.Interface
{
    partial class frmUpdateGUIDSTR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdateGUIDSTR));
            this.txtGUIDSTR = new System.Windows.Forms.TextBox();
            this.btnDo = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnDo);
            this.grbMain.Controls.Add(this.txtGUIDSTR);
            this.grbMain.Size = new System.Drawing.Size(572, 69);
            // 
            // txtGUIDSTR
            // 
            this.txtGUIDSTR.Location = new System.Drawing.Point(12, 12);
            this.txtGUIDSTR.Name = "txtGUIDSTR";
            this.txtGUIDSTR.Size = new System.Drawing.Size(548, 21);
            this.txtGUIDSTR.TabIndex = 0;
            // 
            // btnDo
            // 
            this.btnDo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDo.Image = ((System.Drawing.Image)(resources.GetObject("btnDo.Image")));
            this.btnDo.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDo.Location = new System.Drawing.Point(216, 39);
            this.btnDo.Name = "btnDo";
            this.btnDo.Size = new System.Drawing.Size(93, 23);
            this.btnDo.TabIndex = 33;
            this.btnDo.Text = "Thực hiện";
            this.btnDo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDo.Click += new System.EventHandler(this.btnDo_Click);
            // 
            // frmUpdateGUIDSTR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 69);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximumSize = new System.Drawing.Size(588, 108);
            this.MinimumSize = new System.Drawing.Size(588, 108);
            this.Name = "frmUpdateGUIDSTR";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cập nhập GUIDSTR";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtGUIDSTR;
        private Janus.Windows.EditControls.UIButton btnDo;

    }
}