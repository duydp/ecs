using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAL8000 : BasicVNACC
    {
        public List<VAL8000_HANG> HangMD { get; set; }
        public void LoadVAL8000(string strResult)
        {
            try
            {

                this.GetObject<VAL8000>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAL8000, false, VAL8000.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAL8000.TongSoByte);
                while (true)
                {
                    VAL8000_HANG vad1agHang = new VAL8000_HANG();
                    vad1agHang.GetObject<VAL8000_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAL8000_HANG", false, VAL8000_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAL8000_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAL8000_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAL8000_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAL8000_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}