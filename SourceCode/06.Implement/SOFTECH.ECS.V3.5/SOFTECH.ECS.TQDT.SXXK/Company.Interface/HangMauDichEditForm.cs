﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX.EditControls;
using Company.BLL.Utils;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class HangMauDichEditForm : BaseForm
    {
        public bool IsEdited = false;
        public bool IsDeleted = false;
        //-----------------------------------------------------------------------------------------
        public HangMauDich HMD = new HangMauDich();
        public List<HangMauDich> collection = new List<HangMauDich>();
        public string LoaiHangHoa = "";
        public string NhomLoaiHinh = string.Empty;

        public decimal TyGiaTT;
        public string MaNguyenTe;
        public string MaNuocXX;

        //-----------------------------------------------------------------------------------------				
        // Tính thuế
        private decimal luong;
        private decimal dgnt;
        private decimal tgnt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tgtt_gtgt;
        private decimal clg;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal ts_gtgt;
        private decimal tl_clg;

        private decimal tt_nk;
        private decimal tt_ttdb;
        private decimal tt_gtgt;
        private decimal st_clg;
        //-----------------------------------------------------------------------------------------
        private string MaHangOld = "";
        private NguyenPhuLieuRegistedForm NPLRegistedForm;
        private SanPhamRegistedForm SPRegistedForm;
        private decimal LuongCon = 0;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------
        //DATLMQ bổ sung biến phục vụ cho việc tự động lưu nội dung tờ khai sửa đổi bổ sung 19/05/2011
        public static string maHS_Old = string.Empty;
        public static string tenHang_Old = string.Empty;
        public static string maHang_Old = string.Empty;
        public static string xuatXu_Old = string.Empty;
        public static decimal soLuong_Old;
        public static string dvt_Old = string.Empty;
        public static decimal donGiaNT_Old;
        public static decimal triGiaNT_Old;
        public HangMauDichEditForm()
        {
            InitializeComponent();
        }

        private void khoitao_GiaoDien()
        {
            if (this.NhomLoaiHinh.StartsWith("N")) SetVisibleHTS(false);
            else
            {
                if (this.LoaiHangHoa != "S" || GlobalSettings.MaHTS == 0) SetVisibleHTS(false);
            }
            if (this.NhomLoaiHinh.Substring(1, 2) == "GC" || this.NhomLoaiHinh.Substring(1, 2) == "SX")
            {
                txtMaHang.ButtonStyle = EditButtonStyle.Ellipsis;
                cbDonViTinh.ReadOnly = true;
                //txtMaHang.BackColor = txtTenHang.BackColor = cbDonViTinh.BackColor = Color.FromArgb(255, 255, 192);
            }
            if (this.LoaiHangHoa == "S") txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            else if (this.LoaiHangHoa == "N") txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
        }
        private void SetVisibleHTS(bool b)
        {
            txtMa_HTS.Visible = lblMa_HTS.Visible = txtSoLuong_HTS.Visible = lblSoLuong_HTS.Visible = cbbDVT_HTS.Visible = lblDVT_HTS.Visible = b;
        }
        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            cbbDVT_HTS.DataSource = this._DonViTinh;
            cbbDVT_HTS.SelectedValue = "15 ";
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
        }

        private decimal tinhthue()
        {

            this.tgnt = this.dgnt * this.luong;
            this.tgtt_nk = Math.Round(this.tgnt * this.TyGiaTT, MidpointRounding.AwayFromZero);

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            //txtTGTT_TTDB.Value = this.tgtt_ttdb;
            //txtTGTT_GTGT.Value = this.tgtt_gtgt;
            //txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            //txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb,0);
            //txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt,0);
            //txtTien_CLG.Value = Math.Round(this.st_clg,0);

            //txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg,0);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }

        private decimal tinhthue2()
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Text);
            this.luong = Convert.ToDecimal(txtLuong.Text);
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.tgnt = Convert.ToDecimal(txtTGNT.Text);
            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Text);
            if (tgtt_nk == 0)
                tgtt_nk = tgnt * TyGiaTT;
            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            //txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            //txtTGTT_TTDB.Value = this.tgtt_ttdb;
            //txtTGTT_GTGT.Value = this.tgtt_gtgt;
            //txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            //txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb,0);
            //txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt,0);
            //txtTien_CLG.Value = Math.Round(this.st_clg,0);

            //txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg,0);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }

        //-----------------------------------------------------------------------------------------

        private void HangMauDichEditForm_Load(object sender, EventArgs e)
        {
            txtPhuThu.Text = "0";

            ctrNuocXX.Ma = this.HMD.NuocXX_ID;
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();
            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            // Tính lại thuế.
            //this.HMD.TinhThue(this.TyGiaTT);
            if (GlobalSettings.NGON_NGU == "0")
            {
                lblTyGiaTT.Text = "Tỷ giá VNĐ: " + this.TyGiaTT.ToString("N");
            }
            else
            {
                lblTyGiaTT.Text = "VND rate: " + this.TyGiaTT.ToString("N");
            }


            // Bind data.
            txtMaHang.Text = this.HMD.MaPhu;
            //txtPhuThu.Value = this.HMD.PhuThu;
            txtPhuThu.Value = this.HMD.TriGiaThuKhac;
            txtTyLeThuKhac.Value = this.HMD.TyLeThuKhac;
            txtTenHang.Text = this.HMD.TenHang;
            txtMaHS.Text = this.HMD.MaHS;
            txtMa_HTS.Text = this.HMD.Ma_HTS;
            txtSoLuong_HTS.Value = this.HMD.SoLuong_HTS;
            cbDonViTinh.SelectedValue = this.HMD.DVT_ID;
            cbbDVT_HTS.SelectedValue = this.HMD.DVT_HTS;
            //luu lai ma hang
            MaHangOld = this.HMD.MaPhu;

            maHS_Old = this.HMD.MaHS;
            tenHang_Old = this.HMD.TenHang;
            maHang_Old = this.HMD.MaPhu;
            xuatXu_Old = this.HMD.NuocXX_ID;
            soLuong_Old = this.HMD.SoLuong;
            dvt_Old = this.HMD.DVT_ID;
            triGiaNT_Old = this.HMD.TriGiaKB;
            donGiaNT_Old = this.HMD.DonGiaKB;

            txtDGNT.Value = this.dgnt = this.HMD.DonGiaKB;
            txtLuong.Value = this.luong = this.HMD.SoLuong;
            txtTGNT.Value = this.HMD.TriGiaKB;
            txtTriGiaKB.Value = this.HMD.TriGiaKB_VND;
            txtTGTT_NK.Value = this.HMD.TriGiaTT;
            txtTS_NK.Value = this.ts_nk = this.HMD.ThueSuatXNK;
            //txtTS_TTDB.Value = this.ts_ttdb = this.HMD.ThueSuatTTDB;
            //txtTS_GTGT.Value = this.ts_gtgt = this.HMD.ThueSuatGTGT;
            //txtTL_CLG.Value = this.tl_clg = this.HMD.TyLeThuKhac;
            ctrNuocXX.Ma = this.HMD.NuocXX_ID;
            this.ts_nk = this.HMD.ThueSuatXNK / 100;
            //this.ts_ttdb = this.HMD.ThueSuatTTDB / 100;
            //this.ts_gtgt = this.HMD.ThueSuatGTGT / 100;
            //this.tl_clg = this.HMD.TyLeThuKhac / 100;
            txtTSXNKGiam.Text = HMD.ThueSuatGiam;
            this.tinhthue2();
            if (this.OpenType == OpenFormType.View)
                btnGhi.Enabled = false;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                btnGhi.Enabled = true;
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            try
            {
                this.luong = Convert.ToDecimal(txtLuong.Value);
                txtSoLuong_HTS.Value = Math.Round(this.luong / 12);
                if (this.NhomLoaiHinh.IndexOf("GC") > 0)
                {
                    if (this.luong > LuongCon)
                    {
                        this.luong = 0;
                        //ShowMessage("Không được nhập quá lượng còn trong hệ thống là : " + LuongCon.ToString(), false);
                        MLMessages("Không được nhập quá lượng còn trong hệ thống là : " + LuongCon.ToString(), "MSG_WRN10", "", false);
                        return;
                    }
                    LuongCon = 0;
                }
                this.tinhthue();
                epError.SetError(txtDGNT, null);
                epError.SetError(txtLuong, null);
            }
            catch
            {
                epError.SetError(txtDGNT, setText("Dữ liệu không hợp lệ", "Invalid input value"));
                epError.SetError(txtLuong, setText("Dữ liệu không hợp lệ", "Invalid input value"));
            }
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            try
            {
                this.dgnt = Convert.ToDecimal(txtDGNT.Value);
                this.tinhthue();
                epError.SetError(txtDGNT, null);
                epError.SetError(txtLuong, null);
            }
            catch
            {
                epError.SetError(txtDGNT, setText("Dữ liệu không hợp lệ", "Invalid input value"));
                epError.SetError(txtLuong, setText("Dữ liệu không hợp lệ", "Invalid input value"));
            }
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.tinhthue2();
        }

        //private void txtTS_TTDB_Leave(object sender, EventArgs e)
        //{
        //    this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
        //    this.tinhthue2();
        //}

        //private void txtTS_GTGT_Leave(object sender, EventArgs e)
        //{
        //    this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
        //    this.tinhthue2();
        //}

        //private void txtTS_CLG_Leave(object sender, EventArgs e)
        //{
        //    this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
        //    this.tinhthue2();
        //}

        //-----------------------------------------------------------------------------------------------

        //private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        //{
        //    grbThue.Enabled = !chkMienThue.Checked;
        //    txtTS_NK.Value = 0;
        //    txtTS_TTDB.Value = 0;
        //    txtTS_GTGT.Value = 0;
        //    txtTL_CLG.Value = 0;
        //    this.tinhthue();
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh)
            {
                case "NSX":
                    if (this.NPLRegistedForm == null)
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    this.NPLRegistedForm.ShowDialog(this);
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                    {
                        txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    break;
                case "XSX":
                    if (this.LoaiHangHoa == "N")
                    {
                        if (this.NPLRegistedForm == null)
                            this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                        this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                        this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                        this.NPLRegistedForm.ShowDialog(this);
                        if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                        {
                            txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                    }
                    else
                    {
                        if (this.SPRegistedForm == null)
                            this.SPRegistedForm = new SanPhamRegistedForm();
                        this.SPRegistedForm.CalledForm = "HangMauDichForm";
                        this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        this.SPRegistedForm.ShowDialog(this);
                        if (this.SPRegistedForm.SanPhamSelected.Ma != "" && this.SPRegistedForm.SanPhamSelected != null)
                        {
                            txtMaHang.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                            txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                            txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                    }
                    break;

            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMaHang.Text.Trim() == "") return;
                //this.fillNguyenPhuLieu(txtMaNPL.Text);
                if (NhomLoaiHinh.IndexOf("SX") > 0)
                {
                    if (LoaiHangHoa == "N")
                    {
                        Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                        npl.Ma = txtMaHang.Text.Trim();
                        npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        if (npl.Load())
                        {
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            epError.SetIconPadding(txtMaHang, -8);

                            // epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                            }
                            else
                            {
                                epError.SetError(txtMaHang, "This material does not exist.");
                            }

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                            cbDonViTinh.SelectedValue = "";
                        }
                    }
                    else
                    {
                        Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                        sp.Ma = txtMaHang.Text.Trim();
                        sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        sp.MaHaiQuan = MaHaiQuan;
                        if (sp.Load())
                        {
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            epError.SetIconPadding(txtMaHang, -8);
                            //epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                            }
                            else
                            {
                                epError.SetError(txtMaHang, "This product does not exist.");
                            }

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                            cbDonViTinh.SelectedValue = "";
                        }
                    }

                }
                tinhthue2();
                cvError.Validate();
                if (!cvError.IsValid) return;

                //Nếu là tờ khai xuất thì kiểm tra mã HTS<14ký tự
                if (NhomLoaiHinh.StartsWith("X"))
                {
                    if (!Validate())
                        return;
                }
                if (!MaHS.Validate(txtMaHS.Text, 10))
                {
                    epError.SetIconPadding(txtMaHS, -8);
                    // epError.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        epError.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                    }
                    else
                    {
                        epError.SetError(txtMaHS, "HS code is invalid");
                    }
                    return;
                }
                if (checkMaHangExit(txtMaHang.Text.Trim()))
                {
                    string result = ShowMessage("Mặt hàng này đã có rồi, bạn có muốn tiếp tục không.", true);
                    if (result != "Yes")
                        return;
                }

                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    if (maHS_Old != txtMaHS.Text)
                        ToKhaiMauDichForm.maHS_Edit = txtMaHS.Text;
                    else
                        ToKhaiMauDichForm.maHS_Edit = maHS_Old;
                    if (tenHang_Old != txtTenHang.Text)
                        ToKhaiMauDichForm.tenHang_Edit = txtTenHang.Text;
                    else
                        ToKhaiMauDichForm.tenHang_Edit = tenHang_Old;
                    if (maHang_Old != txtMaHang.Text)
                        ToKhaiMauDichForm.maHang_Edit = txtMaHang.Text;
                    else
                        ToKhaiMauDichForm.maHang_Edit = maHang_Old;
                    if (xuatXu_Old != ctrNuocXX.Ma)
                        ToKhaiMauDichForm.xuatXu_Edit = ctrNuocXX.Ma;
                    else
                        ToKhaiMauDichForm.xuatXu_Edit = xuatXu_Old;
                    if (dvt_Old != cbDonViTinh.SelectedValue.ToString())
                        ToKhaiMauDichForm.dvt_Edit = cbDonViTinh.SelectedValue.ToString();
                    else
                        ToKhaiMauDichForm.dvt_Edit = dvt_Old;
                    if (soLuong_Old != Convert.ToDecimal(txtLuong.Text))
                        ToKhaiMauDichForm.soLuong_Edit = Convert.ToDecimal(txtLuong.Text);
                    else
                        ToKhaiMauDichForm.soLuong_Edit = soLuong_Old;
                    if (donGiaNT_Old != Convert.ToDecimal(txtDGNT.Text))
                        ToKhaiMauDichForm.dongiaNT_Edit = Convert.ToDecimal(txtDGNT.Text);
                    else
                        ToKhaiMauDichForm.dongiaNT_Edit = donGiaNT_Old;
                    if (triGiaNT_Old != Convert.ToDecimal(txtTGNT.Text))
                        ToKhaiMauDichForm.trigiaNT_Edit = Convert.ToDecimal(txtTGNT.Text);
                    else
                        ToKhaiMauDichForm.trigiaNT_Edit = triGiaNT_Old;

                    try
                    {
                        this.HMD.MaHS = txtMaHS.Text;
                        this.HMD.TyLeThuKhac = decimal.Parse(txtTyLeThuKhac.Text);
                        this.HMD.TriGiaThuKhac = (txtPhuThu.Text != "" ? Convert.ToDecimal(txtPhuThu.Text) : 0);
                        this.HMD.Ma_HTS = txtMa_HTS.Text;
                        this.HMD.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
                        this.HMD.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
                        this.HMD.MaPhu = txtMaHang.Text.Trim();
                        this.HMD.TenHang = txtTenHang.Text.Trim();
                        this.HMD.NuocXX_ID = ctrNuocXX.Ma;
                        this.HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                        this.HMD.SoLuong = Convert.ToDecimal(txtLuong.Text);
                        this.HMD.DonGiaKB = Convert.ToDecimal(txtDGNT.Text);
                        this.HMD.TriGiaKB = Convert.ToDecimal(txtTGNT.Text);
                        this.HMD.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Text);
                        this.HMD.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
                        this.HMD.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Text);
                        //this.HMD.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Text);
                        //this.HMD.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Text);
                        this.HMD.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Text), 0);
                        //this.HMD.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Text),0);
                        //this.HMD.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Text),0);
                        this.HMD.DonGiaTT = HMD.TriGiaTT / HMD.SoLuong;
                        this.HMD.Ma_HTS = txtMa_HTS.Text;
                        this.HMD.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
                        this.HMD.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
                        HMD.ThueSuatGiam = txtTSXNKGiam.Text.Trim();
                        //if (chkMienThue.Checked)
                        //    this.HMD.MienThue = 1;
                        //else
                        //    this.HMD.MienThue = 0;

                        // Tính thuế.            
                        this.IsEdited = true;
                        txtMaHang.Text = "";
                        txtTenHang.Text = "";
                        txtMaHS.Text = "";
                        txtLuong.Value = 0;
                        txtDGNT.Value = 0;
                        LuongCon = 0;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    this.HMD.MaHS = txtMaHS.Text;
                    this.HMD.TyLeThuKhac = decimal.Parse(txtTyLeThuKhac.Text);
                    this.HMD.TriGiaThuKhac = (txtPhuThu.Text != "" ? Convert.ToDecimal(txtPhuThu.Text) : 0);
                    this.HMD.Ma_HTS = txtMa_HTS.Text;
                    this.HMD.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
                    this.HMD.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
                    this.HMD.MaPhu = txtMaHang.Text.Trim();
                    this.HMD.TenHang = txtTenHang.Text.Trim();
                    this.HMD.NuocXX_ID = ctrNuocXX.Ma;
                    this.HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    this.HMD.SoLuong = Convert.ToDecimal(txtLuong.Text);
                    this.HMD.DonGiaKB = Convert.ToDecimal(txtDGNT.Text);
                    this.HMD.TriGiaKB = Convert.ToDecimal(txtTGNT.Text);
                    this.HMD.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Text);
                    this.HMD.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
                    this.HMD.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Text);
                    //this.HMD.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Text);
                    //this.HMD.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Text);
                    this.HMD.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Text), 0);
                    //this.HMD.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Text),0);
                    //this.HMD.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Text),0);
                    this.HMD.DonGiaTT = HMD.TriGiaTT / HMD.SoLuong;
                    this.HMD.Ma_HTS = txtMa_HTS.Text;
                    this.HMD.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
                    this.HMD.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
                    HMD.ThueSuatGiam = txtTSXNKGiam.Text.Trim();
                    //if (chkMienThue.Checked)
                    //    this.HMD.MienThue = 1;
                    //else
                    //    this.HMD.MienThue = 0;

                    // Tính thuế.            
                    this.IsEdited = true;
                    txtMaHang.Text = "";
                    txtTenHang.Text = "";
                    txtMaHS.Text = "";
                    txtLuong.Value = 0;
                    txtDGNT.Value = 0;
                    LuongCon = 0;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value") + ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void TinhTriGiaTinhThue()
        {
            decimal Phi = TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen;
            decimal TongTriGiaHang = 0;
            foreach (HangMauDich hmd in collection)
            {
                TongTriGiaHang += hmd.TriGiaKB;

            }
            decimal TriGiaTTMotDong = Phi / TongTriGiaHang;
            foreach (HangMauDich hmd in collection)
            {
                hmd.TriGiaTT = (TriGiaTTMotDong * hmd.TriGiaKB + hmd.TriGiaKB) * TyGiaTT;
                hmd.DonGiaTT = hmd.TriGiaTT / hmd.SoLuong;
                hmd.ThueXNK = (hmd.TriGiaTT * hmd.ThueSuatXNK) / 100;
            }

        }
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in collection)
            {
                if (hmd.MaPhu.Trim() == maHang && maHang != MaHangOld) return true;
            }
            return false;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.IsDeleted = true;
            this.Close();
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            if (txtMaHang.Text.Trim() == "") return;
            if (NhomLoaiHinh.IndexOf("SX") > 0)
            {
                if (LoaiHangHoa == "N")
                {
                    Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                    npl.Ma = txtMaHang.Text.Trim();
                    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    if (npl.Load())
                    {
                        txtMaHang.Text = npl.Ma;
                        //if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                        txtTenHang.Text = npl.Ten;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);
                        // epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This material does not exist.");
                        }

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }
                else
                {
                    Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                    sp.Ma = txtMaHang.Text.Trim();
                    sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ; ;
                    if (sp.Load())
                    {
                        txtMaHang.Text = sp.Ma;
                        //if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                        txtTenHang.Text = sp.Ten;
                        cbDonViTinh.SelectedValue = sp.DVT_ID;
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);
                        // epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This product does not exist .");
                        }
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }

            }

        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue2();
            txtPhuThu.Value = (decimal.Parse(txtTGTT_NK.Text) / 100) * decimal.Parse(txtTyLeThuKhac.Text);
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            txtMaHS_Leave();
        }

        private void txtMaHS_Leave()
        {
            epError.SetError(txtMaHS, string.Empty);

            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);

                if (GlobalSettings.NGON_NGU == "0")
                {
                    epError.SetError(txtMaHS, "Mã HS không hợp lệ.");
                }
                else
                {
                    epError.SetError(txtMaHS, "HS code is invalid.");
                }
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }

            string MoTa = MaHS.CheckExist(txtMaHS.Text);
            if (MoTa == "")
            {
                epError.SetIconPadding(txtMaHS, -8);

                if (GlobalSettings.NGON_NGU == "0")
                {
                    epError.SetError(txtMaHS, "Mã HS không có trong danh mục mã HS.");
                }
                else
                {
                    epError.SetError(txtMaHS, "HS code haven't exist in HS list.");
                }

            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
        }

        private void grbThue_Click(object sender, EventArgs e)
        {

        }

        private void txtTGNT_Click(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtLuong.Value) > 0)
                txtDGNT.Value = Convert.ToDecimal(txtTGNT.Value) / Convert.ToDecimal(txtLuong.Value);
            this.tinhthue2();
        }

        #region Begin VALIDATE GIAP PHEP

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool Validate()
        {
            bool isValid = true;

            isValid &= ValidateControl.ValidateLength(txtMa_HTS, 14, epError, "Mã HTS");

            return isValid;
        }

        #endregion End VALIDATE GIAP PHEP

        private void txtTyLeThuKhac_Leave(object sender, EventArgs e)
        {
            txtPhuThu.Value = (decimal.Parse(txtTGTT_NK.Text) / 100) * decimal.Parse(txtTyLeThuKhac.Text);
        }

        private void txtPhuThu_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtTGTT_NK.Value) == 0) return;
            txtTyLeThuKhac.Value = Convert.ToDecimal(txtPhuThu.Value) / (Convert.ToDecimal(txtTGTT_NK.Value) / 100);
        }

    }
}
