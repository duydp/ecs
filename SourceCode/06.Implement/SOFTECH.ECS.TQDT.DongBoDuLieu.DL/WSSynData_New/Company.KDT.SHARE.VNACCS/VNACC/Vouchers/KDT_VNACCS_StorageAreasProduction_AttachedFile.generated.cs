﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_StorageAreasProduction_AttachedFile : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long StorageAreasProduction_ID { set; get; }
		public string FileName { set; get; }
		public long FileSize { set; get; }
		public string Content { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_StorageAreasProduction_AttachedFile> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_StorageAreasProduction_AttachedFile> collection = new List<KDT_VNACCS_StorageAreasProduction_AttachedFile>();
			while (reader.Read())
			{
				KDT_VNACCS_StorageAreasProduction_AttachedFile entity = new KDT_VNACCS_StorageAreasProduction_AttachedFile();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("StorageAreasProduction_ID"))) entity.StorageAreasProduction_ID = reader.GetInt64(reader.GetOrdinal("StorageAreasProduction_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileName"))) entity.FileName = reader.GetString(reader.GetOrdinal("FileName"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileSize"))) entity.FileSize = reader.GetInt64(reader.GetOrdinal("FileSize"));
				if (!reader.IsDBNull(reader.GetOrdinal("Content"))) entity.Content = reader.GetString(reader.GetOrdinal("Content"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_StorageAreasProduction_AttachedFile> collection, long id)
        {
            foreach (KDT_VNACCS_StorageAreasProduction_AttachedFile item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_StorageAreasProduction_AttachedFiles VALUES(@StorageAreasProduction_ID, @FileName, @FileSize, @Content)";
            string update = "UPDATE t_KDT_VNACCS_StorageAreasProduction_AttachedFiles SET StorageAreasProduction_ID = @StorageAreasProduction_ID, FileName = @FileName, FileSize = @FileSize, Content = @Content WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_StorageAreasProduction_AttachedFiles WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FileSize", SqlDbType.BigInt, "FileSize", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Content", SqlDbType.NVarChar, "Content", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FileSize", SqlDbType.BigInt, "FileSize", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Content", SqlDbType.NVarChar, "Content", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_StorageAreasProduction_AttachedFiles VALUES(@StorageAreasProduction_ID, @FileName, @FileSize, @Content)";
            string update = "UPDATE t_KDT_VNACCS_StorageAreasProduction_AttachedFiles SET StorageAreasProduction_ID = @StorageAreasProduction_ID, FileName = @FileName, FileSize = @FileSize, Content = @Content WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_StorageAreasProduction_AttachedFiles WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FileSize", SqlDbType.BigInt, "FileSize", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Content", SqlDbType.NVarChar, "Content", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FileSize", SqlDbType.BigInt, "FileSize", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Content", SqlDbType.NVarChar, "Content", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_StorageAreasProduction_AttachedFile Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_StorageAreasProduction_AttachedFile> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_StorageAreasProduction_AttachedFile> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_StorageAreasProduction_AttachedFile> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_StorageAreasProduction_AttachedFile> SelectCollectionBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
            IDataReader reader = SelectReaderBy_StorageAreasProduction_ID(storageAreasProduction_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_StorageAreasProduction_AttachedFile(long storageAreasProduction_ID, string fileName, long fileSize, string content)
		{
			KDT_VNACCS_StorageAreasProduction_AttachedFile entity = new KDT_VNACCS_StorageAreasProduction_AttachedFile();	
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.Content = content;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.BigInt, FileSize);
			db.AddInParameter(dbCommand, "@Content", SqlDbType.NVarChar, Content);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_StorageAreasProduction_AttachedFile> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_StorageAreasProduction_AttachedFile item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_StorageAreasProduction_AttachedFile(long id, long storageAreasProduction_ID, string fileName, long fileSize, string content)
		{
			KDT_VNACCS_StorageAreasProduction_AttachedFile entity = new KDT_VNACCS_StorageAreasProduction_AttachedFile();			
			entity.ID = id;
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.Content = content;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.BigInt, FileSize);
			db.AddInParameter(dbCommand, "@Content", SqlDbType.NVarChar, Content);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_StorageAreasProduction_AttachedFile> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_StorageAreasProduction_AttachedFile item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_StorageAreasProduction_AttachedFile(long id, long storageAreasProduction_ID, string fileName, long fileSize, string content)
		{
			KDT_VNACCS_StorageAreasProduction_AttachedFile entity = new KDT_VNACCS_StorageAreasProduction_AttachedFile();			
			entity.ID = id;
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.Content = content;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.BigInt, FileSize);
			db.AddInParameter(dbCommand, "@Content", SqlDbType.NVarChar, Content);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_StorageAreasProduction_AttachedFile> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_StorageAreasProduction_AttachedFile item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_StorageAreasProduction_AttachedFile(long id)
		{
			KDT_VNACCS_StorageAreasProduction_AttachedFile entity = new KDT_VNACCS_StorageAreasProduction_AttachedFile();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_StorageAreasProduction_AttachedFile> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_StorageAreasProduction_AttachedFile item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}