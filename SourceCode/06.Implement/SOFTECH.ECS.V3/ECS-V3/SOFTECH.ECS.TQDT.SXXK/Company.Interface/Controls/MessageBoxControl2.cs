using System;
using System.Windows.Forms;
using Company.Interface;

namespace Company.Controls
{
    public partial class MessageBoxControl : BaseForm
    {
        public string ReturnValue = "Cancel";
        public string exceptionString = "";
        public bool ShowYesNoButton
        {
            set 
            { 
                this.btnNo.Visible = this.btnYes.Visible = value;
                this.btnCancel.Visible = !value;
                if (!value) this.CancelButton = btnCancel;
                else this.CancelButton = btnNo;
            }
        }
        public bool ShowErrorButton
        {
            set
            {
                this.btnError.Visible = value;
                this.btnCancel.Visible = !this.btnNo.Visible;
                //if (!value) this.CancelButton = btnCancel;
                //else this.CancelButton = btnNo;
            }
        }
        public string MessageString
        {
            set { this.txtMessage.Text = value; }
            get { return this.txtMessage.Text; }
        }

        public MessageBoxControl()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "Yes";
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "No";
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MessageBoxControl_Load(object sender, EventArgs e)
        {

        }

        private void btnError_Click(object sender, EventArgs e)
        {
            try
            {
                //Ghi ảnh màn hình lỗi
                if (System.IO.Directory.Exists("Errors") == false)
                {
                    System.IO.Directory.CreateDirectory("Errors");
                }
                string fileName = Application.StartupPath + string.Format("\\Errors\\{0} - {1}.jpg", GlobalSettings.MA_DON_VI, DateTime.Now.ToString().Replace("/", "").Replace("\\","").Replace(":", ""));
                if (!Company.KDT.SHARE.Components.Globals.SaveAsImage(fileName))
                    fileName = "";
                //Thông tin gởi email
                string subject = string.Format("{0} - ECS.TQDT.SXXK", GlobalSettings.TEN_DON_VI);
                string tenDN = GlobalSettings.TEN_DON_VI;
                string maDN = GlobalSettings.MA_DON_VI;
                string soDT = GlobalSettings.DienThoai;
                string soFax = GlobalSettings.Fax;
                string nguoiLH = GlobalSettings.NguoiLienHe;

                //Hungtq update 16/07/2012.
                string loaiHinh = "SXXK";
                string appVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(Application.ExecutablePath).ProductVersion.ToString();
                string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                dataVersion = (dataVersion != "" ? dataVersion : "?");

                Company.KDT.SHARE.Components.Globals.sendEmail(subject, this.MessageString, fileName, tenDN, maDN, soDT, soFax, nguoiLH, loaiHinh, appVersion, dataVersion, this.exceptionString);
                
                ShowMessage("Đã gởi thông tin lỗi tới nhà cung cấp phần mềm", false);
            }
            catch (Exception ex) 
            {
                ShowMessage("Xảy ra lỗi khi gởi thông tin lỗi tới nhà cung cấp phần mềm", false);
            }
        }
    }
}