﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS.Vouchers;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.Interface.KDT.GC;
using Company.Interface.KDT.SXXK;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.GC;

namespace Company.Interface.GC
{
    public partial class BaoCaoChotTonHangHoaForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_TotalInventoryReport totalInventoryReport = new KDT_VNACCS_TotalInventoryReport();
        public KDT_VNACCS_TotalInventoryReport_ContractReference contractReference = new KDT_VNACCS_TotalInventoryReport_ContractReference();
        public KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReport_Detail = new KDT_VNACCS_TotalInventoryReport_Detail();
        public bool isAdd = true;
        public List<HopDong> HDCollection = new List<HopDong>();
        public HopDong HD = new HopDong();
        public bool isExits = false;
        public string LoaiHangHoa = "1";
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
        List<GC_NguyenPhuLieu> NPLCollection = new List<GC_NguyenPhuLieu>();
        List<GC_SanPham> SPCollection = new List<GC_SanPham>();
        List<GC_ThietBi> TBCollection = new List<GC_ThietBi>();
        List<GC_HangMau> HMCollection = new List<GC_HangMau>();
#endif
        public BaoCaoChotTonHangHoaForm()
        {
            InitializeComponent();
            this._DonViTinh = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            txtDonViTinh.DataSource = this._DonViTinh;
            txtDonViTinh.DisplayMember = "Ten";
            txtDonViTinh.ValueMember = "Ten";
        }

        private void BaoCaoChotTonHangHoaForm_Load(object sender, EventArgs e)
        {
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
            {
                btnLuu.Enabled = false;
                btnAddHĐ.Enabled = false;
                btnDelete.Enabled = false;
                btnXoa.Enabled = false;
                btnAddExcel.Enabled = false;
            }
            else
            {
                btnLuu.Enabled = true;
                btnAddHĐ.Enabled = true;
                btnDelete.Enabled = true;
                btnXoa.Enabled = true;
                btnAddExcel.Enabled = true;
            }
            BindHopDong();
        }
        private void BindHopDong()
        {
            try
            {
                dgListHopDong.Refresh();
                dgListHopDong.DataSource = totalInventoryReport.ContractCollection;
                dgListHopDong.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindHangHoa()
        {
            try
            {
                dgList2.Refresh();
                dgList2.DataSource = contractReference.DetailCollection;
                dgList2.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAddHĐ_Click(object sender, EventArgs e)
        {
            try
            {
                HopDongManageForm f = new HopDongManageForm();
                f.isQuyetToan = true;
                f.IsBrowseForm = true;
                f.ShowDialog();
                HDCollection = f.list_HopDong;
                if (HDCollection.Count > 0)
                {
                    foreach (HopDong items in HDCollection)
                    {
                        foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in totalInventoryReport.ContractCollection)
                        {
                            if (items.SoHopDong == item.SoHopDong)
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            contractReference = new KDT_VNACCS_TotalInventoryReport_ContractReference();
                            contractReference.SoHopDong = items.SoHopDong;
                            contractReference.NgayHopDong = items.NgayKy;
                            contractReference.NgayHetHan = items.NgayGiaHan.Year == 1900 ? items.NgayHetHan : items.NgayGiaHan;
                            contractReference.MaHQ = items.MaHaiQuan;
                            totalInventoryReport.ContractCollection.Add(contractReference);
                            int k = 1;
                            foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in totalInventoryReport.ContractCollection)
                            {
                                //item.STT = k;
                                k++;
                            }
                        }
                    }
                }
                else if (f.HopDongSelected != null)
                {
                    foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in totalInventoryReport.ContractCollection)
                    {
                        if (f.HopDongSelected.SoHopDong == item.SoHopDong)
                        {
                            isExits = true;
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        contractReference = new KDT_VNACCS_TotalInventoryReport_ContractReference();
                        contractReference.SoHopDong = f.HopDongSelected.SoHopDong;
                        contractReference.NgayHopDong = f.HopDongSelected.NgayKy;
                        contractReference.NgayHetHan = f.HopDongSelected.NgayGiaHan.Year == 1900 ? f.HopDongSelected.NgayHetHan : f.HopDongSelected.NgayGiaHan;
                        contractReference.MaHQ = f.HopDongSelected.MaHaiQuan;
                        totalInventoryReport.ContractCollection.Add(contractReference);
                        int k = 1;
                        foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in totalInventoryReport.ContractCollection)
                        {
                            //item.STT = k;
                            k++;
                        }
                    }
                    BindHopDong();
                }
                else
                {
                    ShowMessage("Doanh nghiệp chưa chọn Hợp đồng để đưa vào báo cáo chốt tồn .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {

                GridEXSelectedItemCollection items = dgListHopDong.SelectedItems;
                List<KDT_VNACCS_TotalInventoryReport_ContractReference> ItemColl = new List<KDT_VNACCS_TotalInventoryReport_ContractReference>();
                if (dgListHopDong.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp có muốn xóa hợp đồng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TotalInventoryReport_ContractReference)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TotalInventoryReport_ContractReference item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.DeleteFull();
                        totalInventoryReport.ContractCollection.Remove(item);
                    }
                    BindHopDong();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateChoose(cbbMaHangHoa, errorProvider, "Mã hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "Tên hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHang, errorProvider, "Loại hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtLuongSoSach, errorProvider, "Số lượng sổ sách", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtLuongThucTe, errorProvider, "Số lượng thực tế", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (contractReference == null)
                {
                    ShowMessage("Doanh nghiệp chưa chọn Hợp đồng để đưa vào Báo cáo chốt tồn .", false);
                    return;
                }
                totalInventoryReport_Detail.TenHangHoa = txtTenNPL.Text;
                totalInventoryReport_Detail.MaHangHoa = cbbMaHangHoa.Value.ToString();
                totalInventoryReport_Detail.DVT = txtDonViTinh.ValueMember.ToString();
                totalInventoryReport_Detail.SoLuongTonKhoSoSach = Convert.ToDecimal(txtLuongSoSach.Text);
                totalInventoryReport_Detail.SoLuongTonKhoThucTe = Convert.ToDecimal(txtLuongThucTe.Text);
                if(isAdd)
                    contractReference.DetailCollection.Add(totalInventoryReport_Detail);
                BindHangHoa();
                isAdd = true;
                totalInventoryReport_Detail = new KDT_VNACCS_TotalInventoryReport_Detail();
                txtTenNPL.Text = String.Empty;
                cbbMaHangHoa.Text = String.Empty;
                txtLuongThucTe.Text = String.Empty;
                txtLuongSoSach.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                GridEXSelectedItemCollection items = dgList2.SelectedItems;
                List<KDT_VNACCS_TotalInventoryReport_Detail> ItemColl = new List<KDT_VNACCS_TotalInventoryReport_Detail>();
                if (dgList2.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp có muốn xóa dòng hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_TotalInventoryReport_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_TotalInventoryReport_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        contractReference.DetailCollection.Remove(item);
                    }
                    BindHangHoa();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList2_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                totalInventoryReport_Detail = (KDT_VNACCS_TotalInventoryReport_Detail)dgList2.GetRow().DataRow;
                cbbMaHangHoa.Value = totalInventoryReport_Detail.MaHangHoa;
                txtTenNPL.Text = totalInventoryReport_Detail.TenHangHoa;
                txtDonViTinh.Text = totalInventoryReport_Detail.DVT;
                txtLuongSoSach.Text = totalInventoryReport_Detail.SoLuongTonKhoSoSach.ToString();
                txtLuongThucTe.Text = totalInventoryReport_Detail.SoLuongTonKhoThucTe.ToString();
                isAdd = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListHopDong_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                contractReference = (KDT_VNACCS_TotalInventoryReport_ContractReference)dgListHopDong.GetRow().DataRow;
                BindHangHoa();
                cbbLoaiHang_SelectedValueChanged(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            if (cbbLoaiHang.SelectedValue == null)
            {
                ShowMessage("Bạn chưa chọn loại hàng hóa", false);
                return;
            }
            this.LoaiHangHoa = cbbLoaiHang.SelectedValue.ToString();
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (this.LoaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    txtTenHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    txtDonViTinh.Name = DonViTinh_GetName(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID);
                    //ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                   
                }

            }
            else if (this.LoaiHangHoa == "N")
            {
                if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; 
                this.NPLRegistedForm.ShowDialog(this);
                if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtTenHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    txtDonViTinh.Name = DonViTinh_GetName(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID);

                }
            }
            else 
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    txtTenHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                    txtDonViTinh.Name = DonViTinh_GetName(this.SPRegistedForm.SanPhamSelected.DVT_ID);
                }
            }
#elif GC_V4
            //try
            //{
            //    switch (this.LoaiHangHoa)
            //    {
            //        case "1":
            //            this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
            //            this.NPLRegistedForm.isBrower = true;
            //            this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //            HD = HopDong.Load(HopDong.GetID(contractReference.SoHopDong));
            //            this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
            //            this.NPLRegistedForm.ShowDialog();
            //            if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
            //            {
            //                txtMaNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
            //                txtTenNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
            //                txtDonViTinh.Text = DonViTinh_GetName(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID);
            //                txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
            //                //cbbDVT.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
            //            }
            //            break;
            //        case "2":
            //            this.SPRegistedForm = new SanPhamRegistedForm();
            //            this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //            HD = HopDong.Load(HopDong.GetID(contractReference.SoHopDong));
            //            this.SPRegistedForm.SanPhamSelected.HopDong_ID = HD.ID;
            //            this.SPRegistedForm.ShowDialog();
            //            if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
            //            {
            //                txtMaNPL.Text = this.SPRegistedForm.SanPhamSelected.Ma;
            //                txtTenNPL.Text = this.SPRegistedForm.SanPhamSelected.Ten;
            //                txtDonViTinh.Text = DonViTinh_GetName(this.SPRegistedForm.SanPhamSelected.DVT_ID);
            //                txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
            //                //cbbDVT.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
            //            }
            //            break;

            //        case "3":
            //            ThietBiRegistedForm ftb = new ThietBiRegistedForm();
            //            HD = HopDong.Load(HopDong.GetID(contractReference.SoHopDong));
            //            ftb.ThietBiSelected.HopDong_ID = HD.ID;
            //            ftb.isBrower = true;
            //            ftb.ShowDialog();
            //            if (!string.IsNullOrEmpty(ftb.ThietBiSelected.Ma))
            //            {
            //                txtMaNPL.Text = ftb.ThietBiSelected.Ma;
            //                txtTenNPL.Text = ftb.ThietBiSelected.Ten;
            //                txtDonViTinh.Text = DonViTinh_GetName(ftb.ThietBiSelected.DVT_ID);
            //                txtMaHS.Text = ftb.ThietBiSelected.MaHS;
            //                //cbbDVT.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
            //            }
            //            break;
            //        case "4":
            //            HangMauRegistedForm hangmauForm = new HangMauRegistedForm();
            //            hangmauForm.isBrower = true;
            //            hangmauForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //            HD = HopDong.Load(HopDong.GetID(contractReference.SoHopDong));
            //            hangmauForm.HangMauSelected.HopDong_ID = HD.ID;
            //            hangmauForm.ShowDialog();
            //            if (!string.IsNullOrEmpty(hangmauForm.HangMauSelected.Ma))
            //            {
            //                txtMaNPL.Text = hangmauForm.HangMauSelected.Ma;
            //                txtTenNPL.Text = hangmauForm.HangMauSelected.Ten;
            //                txtDonViTinh.Text = DonViTinh_GetName(hangmauForm.HangMauSelected.DVT_ID);
            //                txtMaHS.Text = hangmauForm.HangMauSelected.MaHS;
            //                //cbbDVT.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
            //            }
            //            break;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}

#endif 
        }

        private void btnAddExcel_Click(object sender, EventArgs e)
        {
            BaoCaoChotTonReadExcelForm f = new BaoCaoChotTonReadExcelForm();
            if(contractReference==null)
            {
                ShowMessage("Doanh nghiệp Chưa chọn Hợp đồng để đưa vào Báo cáo chốt tồn .", false);
                return;
            }
            f.TotalInventoryReport = totalInventoryReport;
            f.ContractReference = contractReference;
            f.ShowDialog(this);
            BindHangHoa();
        }

        private void dgList2_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList2.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            totalInventoryReport_Detail = (KDT_VNACCS_TotalInventoryReport_Detail)i.GetRow().DataRow;
                            cbbMaHangHoa.Value = totalInventoryReport_Detail.MaHangHoa;
                            txtTenNPL.Text = totalInventoryReport_Detail.TenHangHoa;
                            txtDonViTinh.Text = totalInventoryReport_Detail.DVT;
                            txtLuongSoSach.Text = totalInventoryReport_Detail.SoLuongTonKhoSoSach.ToString();
                            txtLuongThucTe.Text = totalInventoryReport_Detail.SoLuongTonKhoThucTe.ToString();
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgListHopDong.RootTable.Columns["SoHopDong"], ConditionOperator.Contains, txtSoHopDong.Text);
                dgListHopDong.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLoaiHang_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                string LoaiHang = cbbLoaiHang.SelectedValue.ToString();
                switch (LoaiHang)
                {
                    case "1":
                        NPLCollection = GC_NguyenPhuLieu.SelectCollectionDynamic("HopDong_ID =" + HopDong.GetID(contractReference.SoHopDong) + "", "");
                        cbbMaHangHoa.DataSource = NPLCollection;
                        cbbMaHangHoa.DisplayMember = "Ma";
                        cbbMaHangHoa.ValueMember = "Ma";
                        break;
                    case "2":
                        SPCollection = GC_SanPham.SelectCollectionDynamic("HopDong_ID =" + HopDong.GetID(contractReference.SoHopDong) + "", "");
                        cbbMaHangHoa.DataSource = SPCollection;
                        cbbMaHangHoa.DisplayMember = "Ma";
                        cbbMaHangHoa.ValueMember = "Ma";
                        break;
                    case "3":
                        TBCollection = GC_ThietBi.SelectCollectionDynamic("HopDong_ID =" + HopDong.GetID(contractReference.SoHopDong) + "", "");
                        cbbMaHangHoa.DisplayMember = "Ma";
                        cbbMaHangHoa.ValueMember = "Ma";
                        break;
                    case "4":
                        HMCollection = GC_HangMau.SelectCollectionDynamic("HopDong_ID =" + HopDong.GetID(contractReference.SoHopDong) + "", "");
                        cbbMaHangHoa.DataSource = SPCollection;
                        cbbMaHangHoa.DisplayMember = "Ma";
                        cbbMaHangHoa.ValueMember = "Ma";
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaHangHoa_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaHangHoa.Value != null)
                {
                    switch (cbbLoaiHang.SelectedValue.ToString())
                    {
                        case "1":
                            foreach (GC_NguyenPhuLieu item in NPLCollection)
                            {
                                if (item.Ma == cbbMaHangHoa.Value.ToString())
                                {
                                    txtDonViTinh.Text = DonViTinh_GetName(item.DVT_ID.PadRight(3));
                                    txtTenNPL.Text = item.Ten;
                                    txtMaHS.Text = item.MaHS;
                                    break;
                                }
                            }
                            break;
                        case "2":
                            foreach (GC_SanPham item in SPCollection)
                            {
                                if (item.Ma == cbbMaHangHoa.Value.ToString())
                                {
                                    txtDonViTinh.Text = DonViTinh_GetName(item.DVT_ID.PadRight(3));
                                    txtTenNPL.Text = item.Ten;
                                    txtMaHS.Text = item.MaHS;
                                    break;
                                }
                            }
                            break;
                        case "3":
                            foreach (GC_ThietBi item in TBCollection)
                            {
                                if (item.Ma == cbbMaHangHoa.Value.ToString())
                                {
                                    txtDonViTinh.Text = DonViTinh_GetName(item.DVT_ID.PadRight(3));
                                    txtTenNPL.Text = item.Ten;
                                    txtMaHS.Text = item.MaHS;
                                    break;
                                }
                            }
                            break;
                        case "4":
                            foreach (GC_HangMau item in HMCollection)
                            {
                                if (item.Ma == cbbMaHangHoa.Value.ToString())
                                {
                                    txtDonViTinh.Text = DonViTinh_GetName(item.DVT_ID.PadRight(3));
                                    txtTenNPL.Text = item.Ten;
                                    txtMaHS.Text = item.MaHS;
                                    break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
