﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucSendForm : BaseForm
    {
        private DinhMucDangKy dmDangKy = new DinhMucDangKy();
        private string maSP = "";
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        //-----------------------------------------------------------------------------------------
        public DinhMucSendForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }
        private void LaySoTiepNhanDT()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "DM";
            sendXML.master_id = dmDangKy.ID;
            string password = "";
            if (!sendXML.Load())
            {
                MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_SEN03", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                this.Cursor = Cursors.WaitCursor;
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                {
                    xmlCurrent = dmDangKy.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmDangKy.SoTiepNhan, "MSG_SEN05", "" + dmDangKy.SoTiepNhan, false);

                    txtSoTiepNhan.Text = this.dmDangKy.SoTiepNhan.ToString();

                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    else
                        lblTrangThai.Text = "Wait to approve";
                }

                //xoa thông tin msg nay trong database
                //sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = dmDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                            else
                            {
                                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                dgList.AllowDelete = InheritableBoolean.True;
                                cmdImportExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                                //sendXML.Delete();
                            }
                        }
                    }
                    else
                    {
                        MLMessages("LaySoTiepNhanDT(): Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách ĐỊNH MỨC. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "DM";
                sendXML.master_id = dmDangKy.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = dmDangKy.LayPhanHoi(pass, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + dmDangKy.SoTiepNhan, "MSG_SEN05", "" + dmDangKy.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.dmDangKy.SoTiepNhan.ToString();
                    if (GlobalSettings.NGON_NGU == "0")
                        lblTrangThai.Text = "Chờ duyệt chính thức";
                    else
                        lblTrangThai.Text = "Wait to approve";
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = dmDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                            cmdImportExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            //sendXML.Delete();   
                        }
                    }
                    else
                    {
                        MLMessages("LayPhanHoi(): Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo ĐỊNH MỨC. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void DinhMucSendForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            this.khoitao_DuLieuChuan();
            dgList.DataSource = this.dmDangKy.DMCollection;
            this.cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        }
        private int CheckNPLVaSPDuyet()
        {
            foreach (DinhMuc dm in this.dmDangKy.DMCollection)
            {
                BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.Ma = dm.MaNguyenPhuLieu;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (!npl.Load())
                    return 0;
                BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = dm.MaSanPham;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (!sp.Load())
                    return 1;
            }
            return 2;
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "DM";
            sendXML.master_id = dmDangKy.ID;
            string password = "";
            if (sendXML.Load())
            {
                // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                //Message("MSG_SEN03", "", false);
                MLMessages("Định mức đã gửi thông tin tới hải quan nhưng chưa có phản hồi từ hệ thông hải quan.\nHãy chọn chức năng 'Lấy phản hồi' cho định mức này.", "MSG_SEN03", "", false);
                return;
            }
            WSForm wsForm = new WSForm();
            try
            {
                if (this.dmDangKy.DMCollection.Count > 0)
                {
                    // Master.
                    int s = CheckNPLVaSPDuyet();
                    if (s != 2)
                    {
                        string msg = "";
                        if (s == 1)
                            msg = "Trong danh sách này có nguyên phụ liệu chưa được hải quan duyệt.";
                        else msg = "Trong danh sách này có sản phẩm chưa được hải quan duyệt. ";
                        msg += "\n Bạn có muốn gửi lên hay không ?";

                        string kq = MLMessages(msg, "MSG_PUB20", "", true);
                        if (kq != "Yes")
                            return;
                    }

                    this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    string returnMessage = string.Empty;

                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    this.Cursor = Cursors.WaitCursor;
                    password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                    xmlCurrent = this.dmDangKy.WSSendXML(password);

                    //cap nhat thong tin vao database
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = "DM";
                    sendXML.master_id = dmDangKy.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTrangThai.Text = "Chờ xác nhận phía hải quan";
                    }
                    else
                    {
                        lblTrangThai.Text = "Wait to confirm from Customs";
                    }
                    this.Cursor = Cursors.Default;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdImportExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    LayPhanHoi(password);

                    if (this.dmDangKy.SoTiepNhan > 0)
                        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = dmDangKy.ID;
                                hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                hd.TrangThai = dmDangKy.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.KHAI_BAO;
                                hd.PassWord = password;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                                GlobalSettings.PassWordDT = "";
                        }
                    }
                    else
                    {
                        MLMessages("send(): Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo DM. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới định mức.
        /// </summary>
        private void add()
        {
            dmDangKy = new DinhMucDangKy();

            DinhMucEditForm f = new DinhMucEditForm();
            f.dmDangKy = dmDangKy;
            f.WindowState = FormWindowState.Normal;
            f.ShowDialog(this);
            try
            {
                dgList.DataSource = this.dmDangKy.DMCollection;

                dgList.Refetch();
            }
            catch { dgList.Refresh(); }

        }

        //-----------------------------------------------------------------------------------------
        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }
        private int getPostion(DinhMuc dm)
        {
            for (int i = 0; i < this.dmDangKy.DMCollection.Count; i++)
            {
                if (this.dmDangKy.DMCollection[i].MaSanPham == dm.MaSanPham && this.dmDangKy.DMCollection[i].MaNguyenPhuLieu == dm.MaNguyenPhuLieu) return i;
            }
            return -1;
        }
        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                DinhMucEditForm f = new DinhMucEditForm();
                f.DMDetail = (DinhMuc)e.Row.DataRow;
                f.dmDangKy = dmDangKy;
                f.ShowDialog(this);
                try
                {
                    dgList.Refetch();
                }
                catch { dgList.Refresh(); }
            }

        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void save()
        {
            try
            {

                if (this.dmDangKy.DMCollection.Count > 0)
                {
                    // Master.
                    this.Cursor = Cursors.WaitCursor;
                    this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    if (this.dmDangKy.ID == 0)
                    {
                        this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                        this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        this.dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.dmDangKy.SoTiepNhan = 0;
                        this.dmDangKy.NgayTiepNhan = new DateTime(1900, 1, 1);
                        this.dmDangKy.NgayDangKy = new DateTime(1900, 1, 1);
                        this.dmDangKy.NgayApDung = new DateTime(1900, 1, 1);
                        this.dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                    }
                    // Detail.
                    int sttHang = 1;
                    foreach (DinhMuc nplD in this.dmDangKy.DMCollection)
                    {
                        nplD.STTHang = sttHang++;
                    }
                    if (this.dmDangKy.InsertUpdateFull())
                    {
                        this.Cursor = Cursors.Default;

                        #region Lưu log thao tác
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", dmDangKy.ID, LoaiKhaiBao.DinhMuc);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = LoaiKhaiBao.DinhMuc;
                            log.ID_DK = dmDangKy.ID;
                            log.GUIDSTR_DK = dmDangKy.GUIDSTR;
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                        #endregion

                        this.cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                        MLMessages("Lưu thành công", "MSG_SAV02", "", false);
                    }
                    else
                    {
                        this.Cursor = Cursors.Default;
                        MLMessages("Lưu không thành công", "MSG_SAV01", "", false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Source);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdImportExcel":
                    this.ShowExcelForm();
                    break;
                case "cmdSend":
                    //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        this.SendV3();
                    //else
                    //    this.send();
                    break;
                case "XacNhan":
                    //if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        this.FeedBackV3();
                    //else
                    //    this.LaySoTiepNhanDT();
                    break;
                case "SaoChep":
                    this.SaoChepDinhMuc();
                    break;
                case "InDM":
                    this.InDinhMuc();
                    break;
                case "Dinhmuc":
                    this.InDinhMuc();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.dmDangKy.SoTiepNhan == 0)
            {
                return;
            }
            Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
            phieuTN.phieu = "ĐỊNH MỨC";
            phieuTN.soTN = this.dmDangKy.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.dmDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void InDinhMuc()
        {
            if (dmDangKy.ID == 0)
            {
                ShowMessage("Lưu thông tin trước ghi in.", false);
                return;
            }
            Report.ReportViewDinhMucForm f = new Company.Interface.Report.ReportViewDinhMucForm();
            f.DMDangKy = this.dmDangKy;
            f.ShowDialog(this);
        }
        private void ShowExcelForm()
        {
            ImportKDTDMForm importExcel = new ImportKDTDMForm();
            importExcel.dmDangKy = dmDangKy;
            importExcel.ShowDialog(this);
            try
            {
                dgList.DataSource = dmDangKy.DMCollection;
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }
        private void SaoChepDinhMuc()
        {
            Company.Interface.SXXK.DinhMucRegistedForm f = new Company.Interface.SXXK.DinhMucRegistedForm();

        }


        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MLMessages("Bạn có muốn xóa định mức này không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
                        // Thực hiện xóa trong CSDL.
                        if (dmSelected.ID > 0)
                        {
                            dmSelected.Delete();
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaDinhMuc(GridEXSelectedItemCollection items)
        {

            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            List<DinhMuc> itemsDelete = new List<DinhMuc>(); ;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    DinhMuc item = (DinhMuc)i.GetRow().DataRow;
                    itemsDelete.Add(item);
                    if (item.ID > 0)
                        msgWarning += string.Format("Mã sp ={0},Mã NPL ={1}, Định mức {2} [Xóa]\r\n", item.MaSanPham, item.MaNguyenPhuLieu, item.DinhMucSuDung);
                }
            }
            try
            {
                msgWarning += "Bạn có đồng ý xóa không?";
                if (Globals.ShowMessage(msgWarning, true) != "Yes") return;

                foreach (DinhMuc item in itemsDelete)
                {
                    if (item.ID > 0 && item.CloneToDB(null))
                        item.Delete();
                }
                foreach (DinhMuc item in itemsDelete)
                {

                    dmDangKy.DMCollection.Remove(item);
                }

            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaDinhMuc(items);

            #region old
            //DinhMucCollection DMCollectionTMP = new DinhMucCollection();
            //if (MLMessages("Bạn có muốn xóa định mức này không?", "", "MSG_DEL01", true) == "Yes")
            //{
            //    GridEXSelectedItemCollection items = dgList.SelectedItems;
            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
            //            // Thực hiện xóa trong CSDL.
            //            if (dmSelected.ID > 0)
            //            {
            //                dmSelected.Delete();
            //                DMCollectionTMP.Add(dmSelected);
            //            }
            //            else
            //            {
            //                ShowMessage("Nhấn 'Lưu thông tin' trước khi xóa", false);
            //                return;
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    return;
            //}
            //foreach (DinhMuc dm in DMCollectionTMP)
            //{
            //    dmDangKy.DMCollection.Remove(dm);
            //}
            #endregion old
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        //----------------------------------------------------------------------------------------------
        #region Send V3 Create by LANNT
        private void SendV3()
        {
            if (dmDangKy.ID == 0)
            {
                this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                return;
            }
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (sendXML.Load())
            {
                ShowMessage("Thôn tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            try
            {
                if (this.dmDangKy.DMCollection.Count > 0)
                {
                    // Master.
                    int s = CheckNPLVaSPDuyet();
                    if (s != 2)
                    {
                        string msg = "";
                        if (s == 0)
                            msg = "Trong danh sách này có nguyên phụ liệu chưa được hải quan duyệt.";
                        else msg = "Trong danh sách này có sản phẩm chưa được hải quan duyệt. ";
                        msg += "\n Bạn có muốn gửi lên hay không ?";

                        string kq = MLMessages(msg, "MSG_PUB20", "", true);
                        if (kq != "Yes")
                            return;
                    }
                    this.dmDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.dmDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.dmDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    string returnMessage = string.Empty;
                    dmDangKy.GUIDSTR = Guid.NewGuid().ToString();
                    SXXK_DinhMucSP dmSp = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc(dmDangKy, false);
                    //string msgSend = Helpers.BuildSend(
                    //               new NameBase()
                    //               {
                    //                   Name = GlobalSettings.TEN_DON_VI,
                    //                   Identity = dmDangKy.MaDoanhNghiep
                    //               }
                    //                 , new NameBase()
                    //                 {
                    //                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                    //                     Identity = dmDangKy.MaHaiQuan
                    //                 }
                    //              ,
                    //                new SubjectBase()
                    //                {
                    //                    Type = DeclarationIssuer.SXXK_DINHMUC_SP,
                    //                    Function = DeclarationFunction.KHAI_BAO,
                    //                    Reference = dmDangKy.GUIDSTR,
                    //                }
                    //                ,
                    //                dmSp, null, true);
                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = dmDangKy.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan),
                              Identity = dmDangKy.MaHaiQuan
                          },
                          new SubjectBase()
                          {
                              Type = dmSp.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = dmDangKy.GUIDSTR,
                          },
                          dmSp
                        );
                    dmDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(dmDangKy.ID, MessageTitle.KhaiBaoDinhMuc);
                        XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        btnDelete.Enabled = false;
                        sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                        sendXML.master_id = dmDangKy.ID;
                       // sendXML.msg = msgSend;
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        dmDangKy.TransferDataToSXXK();
                        dmDangKy.Update();
                        FeedBackV3();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dmDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            while (isFeedBack)
            {
                string reference = dmDangKy.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_DINH_MUC,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = dmDangKy.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmDangKy.MaHaiQuan.Trim()),
                                                  Identity = dmDangKy.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    //if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    //{
                    //    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    //    {
                    //        isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                    //        ShowMessageTQDT(msgInfor, false);
                    //    }
                    //    count--;
                    //}
                    //else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    //{
                    //    ShowMessageTQDT(msgInfor, false);
                    //    isFeedBack = false;
                    //}
                    //else if (!string.IsNullOrEmpty(msgInfor))
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    //else isFeedBack = false;
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) SetCommand();
                }

            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dmDangKy, ref msgInfor, e);
        }

        private void SetCommand()
        {
            bool allowSend = (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO);
            XacNhan.Enabled = XacNhan1.Enabled = allowSend ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;
            cmdSend.Enabled = cmdSend1.Enabled = cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = allowSend ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            btnDelete.Enabled = allowSend;
            if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Chờ duyệt chính thức";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Đã duyệt";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Không phê duyệt";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Hủy khai báo";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Chờ hủy";
            }
            else if (dmDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                txtSoTiepNhan.Text = dmDangKy.SoTiepNhan.ToString();
                lblTrangThai.Text = "Đã hủy thành công";
            }
        }
        #endregion


    }
}