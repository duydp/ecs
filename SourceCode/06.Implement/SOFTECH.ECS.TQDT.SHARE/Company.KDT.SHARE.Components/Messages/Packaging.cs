﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Packaging
    {
       ///<sumary>
        ///Tổng số kiện n..8
       ///</sumary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }
        /// <summary>
        /// Loại kiện  an..2
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
        /// <summary>
        /// So Hieu Kien an..35
        /// </summary>
        [XmlElement("markNumber")]
        public string MarkNumber { get; set; }
    }
}
