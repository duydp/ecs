﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class LoaiPhuKien
    {
        #region Properties.

        public long ID { set; get; }
        public string MaPhuKien { set; get; }
        public string NoiDung { set; get; }
        public string ThongTinMoi { set; get; }
        public long Master_ID { set; get; }
        public string ThongTinCu { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<LoaiPhuKien> ConvertToCollection(IDataReader reader)
        {
            IList<LoaiPhuKien> collection = new List<LoaiPhuKien>();
            while (reader.Read())
            {
                LoaiPhuKien entity = new LoaiPhuKien();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhuKien"))) entity.MaPhuKien = reader.GetString(reader.GetOrdinal("MaPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoi"))) entity.ThongTinMoi = reader.GetString(reader.GetOrdinal("ThongTinMoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static LoaiPhuKien Load(long id)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<LoaiPhuKien> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<LoaiPhuKien> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<LoaiPhuKien> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<LoaiPhuKien> SelectCollectionBy_Master_ID(long master_ID)
        {
            IDataReader reader = SelectReaderBy_Master_ID(master_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_Master_ID(long master_ID)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_SelectBy_Master_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_Master_ID(long master_ID)
        {
            const string spName = "p_KDT_GC_LoaiPhuKien_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertLoaiPhuKien(string maPhuKien, string noiDung, string thongTinMoi, long master_ID, string thongTinCu)
        {
            LoaiPhuKien entity = new LoaiPhuKien();
            entity.MaPhuKien = maPhuKien;
            entity.NoiDung = noiDung;
            entity.ThongTinMoi = thongTinMoi;
            entity.Master_ID = master_ID;
            entity.ThongTinCu = thongTinCu;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, MaPhuKien);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
            db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, ThongTinMoi);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, ThongTinCu);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<LoaiPhuKien> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (LoaiPhuKien item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateLoaiPhuKien(long id, string maPhuKien, string noiDung, string thongTinMoi, long master_ID, string thongTinCu)
        {
            LoaiPhuKien entity = new LoaiPhuKien();
            entity.ID = id;
            entity.MaPhuKien = maPhuKien;
            entity.NoiDung = noiDung;
            entity.ThongTinMoi = thongTinMoi;
            entity.Master_ID = master_ID;
            entity.ThongTinCu = thongTinCu;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_GC_LoaiPhuKien_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, MaPhuKien);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
            db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, ThongTinMoi);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, ThongTinCu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<LoaiPhuKien> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (LoaiPhuKien item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateLoaiPhuKien(long id, string maPhuKien, string noiDung, string thongTinMoi, long master_ID, string thongTinCu)
        {
            LoaiPhuKien entity = new LoaiPhuKien();
            entity.ID = id;
            entity.MaPhuKien = maPhuKien;
            entity.NoiDung = noiDung;
            entity.ThongTinMoi = thongTinMoi;
            entity.Master_ID = master_ID;
            entity.ThongTinCu = thongTinCu;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, MaPhuKien);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
            db.AddInParameter(dbCommand, "@ThongTinMoi", SqlDbType.VarChar, ThongTinMoi);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
            db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.VarChar, ThongTinCu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<LoaiPhuKien> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (LoaiPhuKien item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteLoaiPhuKien(long id)
        {
            LoaiPhuKien entity = new LoaiPhuKien();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_Master_ID(long master_ID)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_DeleteBy_Master_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_GC_LoaiPhuKien_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<LoaiPhuKien> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (LoaiPhuKien item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}