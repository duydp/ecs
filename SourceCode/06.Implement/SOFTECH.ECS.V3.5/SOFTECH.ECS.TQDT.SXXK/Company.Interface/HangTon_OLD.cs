﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL.SXXK.ToKhai;
using Company.BLL;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;
using Company.QuanTri;
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;
using Company.KDT.SHARE.Components;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Company.Interface.Report.SXXK;
using System.Data.SqlClient;
using Logger;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface
{
    public partial class HangTon_OLD : BaseForm
    {
        private DataSet ds = new DataSet();
        private Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection tkmdCollection = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTon clsnhapton = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
        private DataSet dsTKKhongDuyet = new DataSet();
        private DataRow[] results = null;
        public HangTon_OLD()
        {
            InitializeComponent();
            //backgroundHangTon.RunWorkerAsync();
        }

        private void HangTon_Load(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                dgList.ColumnAutoResize = false;

                cbTrangThai.SelectedIndex = 0;
                dgList.Tables[0].Columns["MaNPL"].Width = 80;
                dgList.Tables[0].Columns["TenNPL"].Width = 150;
                dgList.Tables[0].Columns["Luong"].Width = 80;
                dgList.Tables[0].Columns["Ton"].Width = 70;
                dgList.Tables[0].Columns["ThueXNK"].Width = 80;
                dgList.Tables[0].Columns["Ton"].Width = 70;
                dgList.Tables[0].Columns["SolanThanhLy"].Width = 60;
                dgList.Tables[0].Columns["LanThanhLy"].Width = 80;
                dgList.Tables[0].Columns["TrangThaiThanhKhoan"].Width = 90;
                dgList.Tables[0].Columns["saisoluong"].Width = 80;
                dgList.Tables[0].Columns["SoLuongDangKy"].Width = 80;
                dgList.Tables[0].Columns["TienTrinhChayThanhLy"].Width = 90;

                dgList.Tables[0].Columns["Luong"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Ton"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Luong"].TotalFormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["Ton"].TotalFormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = new Company.BLL.SXXK.NguyenPhuLieu().SelectDynamic("MaDoanhNghiep = '" + MaDoanhNghiep + "'", "").Tables[0];
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["Ma"].ToString());
                txtMaNPL.AutoCompleteCustomSource = col;
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
                {
                    //uiButton2.Visible = false;
                }
                if (!((SiteIdentity)MainForm.EcsQuanTri.Identity).user.isAdmin)
                {
                    //uiButton4.Visible = false;
                }
                txtNamTiepNhan.Text = DateTime.Today.Year.ToString();

                //Updated by Hungtq, 08/08/2012.
                //Lay danh dach to khai co trang thai khong phai da duyet
              //  string whereCondition = string.Format("MaDoanhNghiep = '{0}' AND MaHaiQuan = '{1}'", GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
               // whereCondition += string.Format("AND NamDK = {0} AND MaLoaiHinh LIKE 'N%' AND TrangThaiXuLy != 1 AND SoToKhai != 0", txtNamTiepNhan.Text);

               // dsTKKhongDuyet = new Company.BLL.KDT.ToKhaiMauDich().SelectDynamic(whereCondition, "");

                //DialogResult rs = MessageBox.Show("Bạn có muốn kiểm tra các tờ khai nhập trong năm " + txtNamTiepNhan.Text, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                //if (rs == DialogResult.Yes) 
                //{
                //    this.Cursor = Cursors.WaitCursor;
                //    btnCheckNPLTon_Click(sender, e);
                //    this.Cursor = Cursors.Default;
                //}
                //searchNew();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            ToKhaiMauDich tk = new ToKhaiMauDich();
            if (dgList.GetRow().RowType == RowType.Record)
            {
                GridEXRow dr = dgList.GetRow();
                Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)dr.DataRow;
                tk.MaHaiQuan = npl.MaHaiQuan;
                tk.SoToKhai = (int)npl.SoToKhai;
                tk.MaLoaiHinh = npl.MaLoaiHinh;
                tk.NamDangKy = npl.NamDangKy;

            }
            else if (dgList.GetRow().RowType == RowType.GroupHeader)
            {
                object[] obj = (object[])dgList.GetRow().GroupValue;
                string a = obj[0].ToString();
                tk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                tk.SoToKhai = (int)obj[0];
                tk.MaLoaiHinh = obj[1].ToString();
                tk.NamDangKy = (short)Convert.ToDateTime(obj[2].ToString()).Year;
            }

            tk.Load();

            ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
            f.TKMD = tk;
            f.NhomLoaiHinh = tk.MaLoaiHinh.Substring(0, 3);
            f.MdiParent = this.ParentForm;
            f.Name = "Tờ khai nhập khẩu số " + tk.SoToKhai + "/" + tk.NamDangKy;
            f.Show();
        }

        private void search()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoToKhai = " + txtSoTiepNhan.Value;
                }

                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND NamDangKy = " + txtNamTiepNhan.Value;
                }
                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }
                string temp = "";
                if (cbTrangThai.SelectedIndex > 0)
                {
                    temp += " AND ThanhLy = '" + cbTrangThai.SelectedValue.ToString() + "'";
                }

                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                    {
                        temp += " AND TenChuHang LIKE '%" + txtTenChuHang.Text.Trim() + "%'";
                    }
                }
                else
                {
                    temp += " AND TenChuHang LIKE ''";

                }
                if (txtLanThanhLy.Text.Trim().Length > 0)
                {
                    if (cbbLanThanhLy.Checked)
                    {
                        where += " AND LanThanhLy =" + txtLanThanhLy.Text;
                    }
                }


                where += " AND Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan IN " +
                "(SELECT Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan FROM dbo.t_SXXK_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'N%'" + temp + ")";

                // Thực hiện tìm kiếm.            
                tkmdCollection = clsnhapton.SelectCollectionDynamic(where, "t_SXXK_ThanhLy_NPLNhapTon.SoToKhai");

                dgList.DataSource = tkmdCollection;
                dgList.Refetch();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //Update by HUNGTQ, 17/08/2011.
        private void searchNew()
        {
            try
            {
                string whereCondition = string.Format("MaDoanhNghiep = '{0}' AND MaHaiQuan = '{1}' ", GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                whereCondition += string.Format("AND NamDK like '%{0}%' AND MaLoaiHinh LIKE 'N%' AND TrangThaiXuLy != 1 AND SoToKhai != 0", txtNamTiepNhan.Text);

                dsTKKhongDuyet = new Company.BLL.KDT.ToKhaiMauDich().SelectDynamic(whereCondition, "");

                Cursor = Cursors.WaitCursor;

                // Xây dựng điều kiện tìm kiếm.
                string where = "t.MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND t.MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoToKhaiVNACCS  like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND t.NamDangKy = " + txtNamTiepNhan.Value;
                }
                if (txtMaNPL.TextLength > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }

                if (cbTrangThai.SelectedIndex > 0)
                {
                    where += " AND t.ThanhLy = '" + cbTrangThai.SelectedValue.ToString() + "'";
                }

                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                    {
                        where += " AND t.TenChuHang LIKE N'%" + txtTenChuHang.Text.Trim() + "%'";
                    }
                }
                else
                {
                    where += " AND t.TenChuHang LIKE N'%%'";

                }


                //where += " AND Cast(t.SoToKhai as varchar(10))+ t.MaLoaiHinh +  Cast(t.NamDangKy as varchar(4))+ t.MaHaiQuan IN " +
                //"(SELECT Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan FROM dbo.t_SXXK_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'N%'" + temp + ")";

                if (chkHienThiLech.Checked)
                {
                    //where += "AND ( SELECT COUNT(*)  FROM[dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu  AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan WHERE nt.SoToKhai = t.SoToKhai AND nt.MaLoaiHinh = t.MaLoaiHinh AND nt.NamDangKy = t.NamDangKy AND nt.MaNPL = t.MaNPL AND nt.MaHaiQuan = t.MaHaiQuan AND nt.MaDoanhNghiep = t.MaDoanhNghiep AND nt.Luong <> h.SoLuong ) != 0 ";
                    where += " AND t.SaiSoLuong = 1";

                    where += " OR (t.SaiSoLuong = 0 AND t.SoLanThanhLy = 0 AND t.Luong <> t.Ton AND t.NamDangKy = " + txtNamTiepNhan.Value;

                    if (txtSoTiepNhan.TextLength > 0)
                    {
                        where += " AND SoToKhaiVNACCS = " + txtSoTiepNhan.Value;
                    }

                    if (txtMaNPL.Text.Length > 0)
                    {
                        if (chkTimChinhXac.Checked)
                        {
                            where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                        }
                        else
                            where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                    }

                    where += ")";

                    //Bo sung 22/09/2011 by HUNGTQ
                    where += " OR (t.TienTrinhChayThanhLy LIKE '%Sai%' AND t.SoLanThanhLy != 0 AND t.NamDangKy = " + txtNamTiepNhan.Value;

                    if (txtSoTiepNhan.TextLength > 0)
                    {
                        where += " AND t.SoToKhai = " + txtSoTiepNhan.Value;
                    }

                    if (txtMaNPL.Text.Length > 0)
                    {
                        if (chkTimChinhXac.Checked)
                        {
                            where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                        }
                        else
                            where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                    }

                    where += ")";

                    where += " OR (t.TenNPL IS NULL AND t.NamDangKy = " + txtNamTiepNhan.Value + ")";
                }
                if (txtLanThanhLy.Text.Trim().Length > 0)
                {
                    if (cbbLanThanhLy.Checked)
                    {
                        where += " AND t.LanThanhLy =" + txtLanThanhLy.Text;
                    }
                }
                if (chkHienThiTKKhongDuyet.Checked)
                {
                    //Bo sung 09/08/2012 by HUNGTQ. Chi hien thi thong tin NPL cua TK khong duoc duyet.
                    where += string.Format(" AND t.SoToKhai IN (SELECT SoToKhai FROM dbo.t_KDT_ToKhaiMauDich WHERE TrangThaiXuLy != 1 AND SoToKhai != 0 AND MaDoanhNghiep = '{0}' AND MaHaiQuan = '{1}' AND NamDK = {2} AND MaLoaiHinh LIKE 'N%')", GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, txtNamTiepNhan.Text);
                }

                // Thực hiện tìm kiếm.            
                tkmdCollection = clsnhapton.SelectCollection_ByYear(where, "t.SoToKhai");
                //ds = clsnhapton.SelectAll_ByYear(where, "t.SoToKhai");

                dgList.DataSource = tkmdCollection;
                //dgList.DataSource = ds.Tables[0];
                dgList.Refetch();
                //GridEXGroup group = new GridEXGroup();
                //GridEXCustomGroup customGr = new GridEXCustomGroup();
                //customGr.CustomGroupType = CustomGroupType.CompositeColumns;
                //customGr.CompositeColumns = new Janus.Windows.GridEX.GridEXColumn[] { dgList.RootTable.Columns["SoToKhai"], dgList.RootTable.Columns["MaLoaiHinh"] };
                //group.GroupInterval = GroupInterval.Alphabetical;
                //dgList.RootTable.Columns["SoToKhai"].DefaultGroupInterval = GroupInterval.Text;
                //group.CustomGroup = customGr;


                //dgList.RootTable.Groups.Add(group);

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally
            { 
                Cursor = Cursors.Default; 
            }
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.searchNew();
            //this.InitializeBackgroundWorker();
        }

        private void Thongke()
        {
            //Company.BLL.SXXK.ThanhKhoan.NPLNhapTon clsNhapton = new NPLNhapTon();
            //NPLNhapTonCollection NhaptonCollection = new NPLNhapTonCollection();
            //foreach (NPLNhapTon nplnhapton in NhaptonCollection)
            //{

            //}
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSoTiepNhan.Text.Trim() == "")
                {
                    ShowMessage("Bạn hãy nhập số tờ khai trước khi lưu.", false);
                    return;
                }
                foreach (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl in tkmdCollection)
                {
                    if (npl.Ton < 0)
                    {
                        ShowMessage("Dữ liệu không hợp lệ. Hãy kiểm tra lại", false);
                        return;
                    }
                    if (npl.ThueTon < 0)
                    {
                        ShowMessage("Dữ liệu không hợp lệ. Hãy kiểm tra lại", false);
                        return;
                    }
                    Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npltemp = npl.Copy();
                    if ((npl.Ton + (decimal)npl.ThueXNKTon) != (npltemp.Ton + (decimal)npltemp.ThueXNKTon) && npl.Luong != npl.Ton)
                        npl.PhuThu = 1;
                    else
                        npl.PhuThu = 0;

                }
                clsnhapton.InsertUpdate(tkmdCollection, chkTuDongTinhThue.Checked);

                //this.search();
                searchNew();

                //ShowMessage("Cập nhật thông tin thành công", false);
                MLMessages("Cập nhật thông tin thành công", "MSG_THK64", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Dữ liệu không hợp lệ. Không lưu được.Hãy kiểm tra lại." + ex.Message, false);
            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            gridEXPrintDocument1.Print();
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            try
            {
                new Company.BLL.KDT.SXXK.HoSoThanhLyDangKy().FixOldData(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                //ShowMessage("Điều chỉnh thành công.", false);
                MLMessages("Điều chỉnh thành công.", "MSG_THK99", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
        }

        private void cbTrangThai_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.search();
            searchNew();
        }

        private void mnuGrid_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "toolStripThanhkhoan":
                    this.doThanhKhoan();
                    break;
            }
        }

        private void doThanhKhoan()
        {
            try
            {
                if (dgList.GetRow().RowType == RowType.Record)
                {
                    GridEXRow dr = dgList.GetRow();
                    Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)dr.DataRow;
                    TheoDoiThanhKhoanNPLToKhaiForm f = new TheoDoiThanhKhoanNPLToKhaiForm();
                    f.NPL = npl;
                    f.ShowDialog(this);

                }
                else if (dgList.GetRow().RowType == RowType.GroupHeader)
                {
                    ToKhaiMauDich tk = new ToKhaiMauDich();
                    object[] obj = (object[])dgList.GetRow().GroupValue;
                    string a = obj[0].ToString();
                    if (a.Length != 12)
                    {
                        tk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        tk.SoToKhai = (int)obj[0];
                        tk.MaLoaiHinh = obj[1].ToString();
                        tk.NamDangKy = (short)Convert.ToDateTime(obj[2]).Year;
                        tk.Load();
                        TheoDoiThanhKhoanToKhaiForm f = new TheoDoiThanhKhoanToKhaiForm();
                        f.TKMD = tk;
                        f.ShowDialog(this);
                    }
                    else
                    {
                        tk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        tk.LoadVNACCSBy(tk.MaHaiQuan, GlobalSettings.MA_DON_VI, a);
                        TheoDoiThanhKhoanToKhaiForm f = new TheoDoiThanhKhoanToKhaiForm();
                        f.TKMD = tk;
                        f.ShowDialog(this);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }


        }

        private DateTime getMaxDateBKTKX(HoSoThanhLyDangKy HSTL)
        {
            int i = HSTL.getBKToKhaiXuat();
            HSTL.BKCollection[i].LoadChiTietBangKe();
            BKToKhaiXuatCollection bkTKXCollection = HSTL.BKCollection[i].bkTKXColletion;
            DateTime dt = bkTKXCollection[0].NgayDangKy;
            for (int j = 1; j < bkTKXCollection.Count; j++)
            {
                if (dt < bkTKXCollection[j].NgayDangKy) dt = bkTKXCollection[j].NgayDangKy;
            }
            return dt;
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TenNPL"].Text == "")
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["SaiSoLuong"].Value.ToString() != "0")
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["SaiSoLuong"].Value.ToString() == "0"
                    && e.Row.Cells["SoLanThanhLy"].Value.ToString() == "0"
                    && (Convert.ToDecimal(e.Row.Cells["Luong"].Value) != Convert.ToDecimal(e.Row.Cells["Ton"].Value)))
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }

                else if (e.Row.Cells["TienTrinhChayThanhLy"].Value.ToString().Contains("Sai"))
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                    e.Row.Cells["TienTrinhChayThanhLy"].ToolTipText = e.Row.Cells["TienTrinhChayThanhLy"].Text;
                }

                //Updated by Hungtq, 08/08/2012.
                //Thiet lap mau to khai co trang thai khong phai da duyet
                results = dsTKKhongDuyet.Tables[0].Select(string.Format("SoToKhai = {0} And NamDK = {1} And MaLoaiHinh = '{2}'", e.Row.Cells["SoToKhai"].Text, e.Row.Cells["NamDangKY"].Text,e.Row.Cells["MaLoaiHinh"].Text.Trim()) );

                if (results.Length != 0)
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.BackColor = Color.Yellow;
                    e.Row.RowStyle.ForeColor = Color.Black;
                }
            }
        }

        private void dgList_EditingCell(object sender, EditingCellEventArgs e)
        {
            try
            {
                GridEXRow row = dgList.GetRow();
                Company.BLL.SXXK.ThanhKhoan.NPLNhapTon npl = (Company.BLL.SXXK.ThanhKhoan.NPLNhapTon)row.DataRow;
                if (npl.CheckMaNPLDaThanhKhoan())
                {
                    ShowMessage("Nguyên phụ liệu '" + npl.MaNPL + "' của tờ khai " + npl.SoToKhai + "/" + npl.NamDangKy + " đã thanh khoản máy, không thể sửa.", false);
                    e.Cancel = true;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private Company.BLL.SXXK.ThanhKhoan.NPLNhapTon ConvertDataRowViewToObject(GridEXRow row)
        {
            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplNT = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();

            nplNT.SoToKhai = Convert.ToInt32(row.Cells["SoToKhai"].Value);

            nplNT.SoToKhai = Convert.ToInt32(row.Cells["SoToKhai"].Value);
            nplNT.MaLoaiHinh = Convert.ToString(row.Cells["MaLoaiHinh"].Value);
            nplNT.NamDangKy = Convert.ToInt16(row.Cells["NamDangKy"].Value);
            nplNT.MaHaiQuan = Convert.ToString(row.Cells["MaHaiQuan"].Value);
            nplNT.MaNPL = Convert.ToString(row.Cells["MaNPL"].Value);
            nplNT.MaDoanhNghiep = Convert.ToString(row.Cells["MaDoanhNghiep"].Value);
            nplNT.Luong = Convert.ToDecimal(row.Cells["Luong"].Value);
            nplNT.Ton = Convert.ToDecimal(row.Cells["Ton"].Value);
            nplNT.ThueXNK = Convert.ToDouble(row.Cells["ThueXNK"].Value);
            nplNT.ThueTTDB = Convert.ToDouble(row.Cells["ThueTTDB"].Value);
            nplNT.ThueVAT = Convert.ToDouble(row.Cells["ThueVAT"].Value);
            nplNT.PhuThu = Convert.ToDouble(row.Cells["PhuThu"].Value);
            nplNT.ThueCLGia = Convert.ToDouble(row.Cells["ThueCLGia"].Value);
            nplNT.ThueXNKTon = Convert.ToDouble(row.Cells["ThueXNKTon"].Value);

            nplNT.TenNPL = Convert.ToString(row.Cells["TenNPL"].Value);
            nplNT.TenDVT = Convert.ToString(row.Cells["TenDVT"].Value);
            nplNT.SoLanThanhLy = Convert.ToInt32(row.Cells["SoLanThanhLy"].Value);
            nplNT.LanThanhLy = Convert.ToInt32(row.Cells["LanThanhLy"].Value);
            nplNT.TrangThaiThanhKhoan = Convert.ToInt32(row.Cells["TrangThaiThanhKhoan"].Value);
            nplNT.SaiSoLuong = Convert.ToInt32(row.Cells["SaiSoLuong"].Value);
            nplNT.SoLuongDangKy = Convert.ToDecimal(row.Cells["SoLuongDangKy"].Value);
            nplNT.TienTrinhChayThanhLy = Convert.ToString(row.Cells["TienTrinhChayThanhLy"].Value);

            return nplNT;
        }

        private void btnXuatExcel_Click(object sender, EventArgs e)
        {

            dgList.Tables[0].Columns["SoToKhaiVNACCS"].Visible = true;
            //dgList.Tables[0].Columns["SoToKhai"].Visible = true;
            dgList.Tables[0].Columns["NgayDangKy"].Visible = true;
            dgList.Tables[0].Columns["MaLoaiHinh"].Visible = true;
            GridEXGroup customG = dgList.Tables[0].Groups[0].Clone();
            dgList.Tables[0].Groups.Clear();
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog(this);
            if (sfNPL.FileName != "")
            {
                gridEXExporter1.GridEX = dgList;
                Stream str = sfNPL.OpenFile();
                gridEXExporter1.Export(str);
                str.Close();
                dgList.Tables[0].Groups.Add(customG);
                dgList.Tables[0].Columns["SoToKhaiVNACCS"].Visible = false;
                dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
                dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;

                if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
            else
            {
                dgList.Tables[0].Columns["SoToKhaiVNACCS"].Visible = false;
                dgList.Tables[0].Columns["NgayDangKy"].Visible = false;
                dgList.Tables[0].Columns["MaLoaiHinh"].Visible = false;
                dgList.Tables[0].Groups.Add(customG);
            }


        }

        private void uiButton5_Click(object sender, EventArgs e)
        {
            dgList.ShowFieldChooser();
            dgList.SaveSettings = true;
        }

        private void btnTimKiemNPL_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = Company.BLL.SXXK.ThanhKhoan.NPLNhapTon.SelectTongNPLbyTenChuHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, txtTenChuHang2.Text);
                dgListNPL.DataSource = ds.Tables[0];
                dgListNPL.Refetch();
            }
            catch { dgListNPL.Refresh(); }
        }

        private void uiTab1_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void uiTab1_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            if (uiTab1.SelectedIndex > 0)
            {
                this.AcceptButton = btnTimKiemNPL;
                btnTimKiemNPL_Click(null, null);

            }
            else
                this.AcceptButton = btnSearch;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (int.Parse(e.Row.Cells["TrangThaiThanhKhoan"].Value.ToString()) == 400)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã chạy thanh khoản";
                    e.Row.Cells["TrangThaiThanhKhoan"].ToolTipText = "Đã chạy thanh khoản";
                }

                else if (int.Parse(e.Row.Cells["TrangThaiThanhKhoan"].Value.ToString()) == 401)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đóng hồ sơ";
                    e.Row.Cells["TrangThaiThanhKhoan"].ToolTipText = "Đóng hồ sơ";
                }

                else if (int.Parse(e.Row.Cells["TrangThaiThanhKhoan"].Value.ToString()) == 0)
                {
                    e.Row.Cells["TrangThaiThanhKhoan"].Text = "Chưa thanh khoản/ Đang nhập liệu";
                    e.Row.Cells["TrangThaiThanhKhoan"].ToolTipText = "Chưa thanh khoản/ Đang nhập liệu";
                }
                //if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("V"))
                //{
                //    e.Row.Cells["MaLoaiHinh"].Text = e.Row.Cells["MaLoaiHinh"].Value.ToString().Substring(2);
                //    e.Row.Cells["SoToKhai"].Text = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(System.Convert.ToInt16(e.Row.Cells["SoToKhai"].Value.ToString())).ToString();
                //}
//                 else
//                     e.Row.Cells["SoTKVNACCS"].Text = e.Row.Cells["SoToKhai"].Value.ToString();
            }
        }

        private void btnDieuChinhLuongTon_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection nplSelect = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();

                // Xây dựng điều kiện tìm kiếm.
                string where = "t.MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND t.MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND t.SoToKhai  like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND t.NamDangKy = " + txtNamTiepNhan.Value;
                }
                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }

                if (cbTrangThai.SelectedIndex > 0)
                {
                    where += " AND t.ThanhLy = '" + cbTrangThai.SelectedValue.ToString() + "'";
                }

                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                    {
                        where += " AND t.TenChuHang LIKE N'%" + txtTenChuHang.Text.Trim() + "%'";
                    }
                }
                else
                {
                    where += " AND t.TenChuHang LIKE N'%%'";

                }


                //where += "AND ( SELECT COUNT(*)  FROM[dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu  AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan WHERE nt.SoToKhai = t.SoToKhai AND nt.MaLoaiHinh = t.MaLoaiHinh AND nt.NamDangKy = t.NamDangKy AND nt.MaNPL = t.MaNPL AND nt.MaHaiQuan = t.MaHaiQuan AND nt.MaDoanhNghiep = t.MaDoanhNghiep AND nt.Luong <> h.SoLuong ) != 0 ";
                where += " AND t.SaiSoLuong = 1";

                where += " OR (t.SaiSoLuong = 0 AND t.SoLanThanhLy = 0 AND t.Luong <> t.Ton AND t.NamDangKy = " + txtNamTiepNhan.Value;

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND t.SoToKhai = " + txtSoTiepNhan.Value;
                }

                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }

                where += ")";

                //Bo sung 22/09/2011 by HUNGTQ
                where += " OR (t.TienTrinhChayThanhLy LIKE '%Sai%' AND t.SoLanThanhLy != 0 AND t.NamDangKy = " + txtNamTiepNhan.Value;

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND t.SoToKhai = " + txtSoTiepNhan.Value;
                }

                if (txtMaNPL.Text.Length > 0)
                {
                    if (chkTimChinhXac.Checked)
                    {
                        where += " AND t.MaNPL = '" + txtMaNPL.Text.Trim() + "'";
                    }
                    else
                        where += " AND t.MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
                }

                where += ")";

                // Thực hiện tìm kiếm.            
                nplSelect = clsnhapton.SelectCollection_ByYear(where, "t.SoToKhai");

                if (nplSelect.Count == 0)
                {
                    Globals.ShowMessage("Không có thông tin nguyên phụ liệu bị lệch lượng nhập và tồn đầu của Năm " + txtNamTiepNhan.Text + ".", false);
                    return;
                }
                else
                {
                    HangTonDieuChinh f = new HangTonDieuChinh();
                    f.NplTonSelect = nplSelect;
                    f.ShowDialog(this);

                    searchNew();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Điều chỉnh lượng tồn", ex);

                Globals.ShowMessage("Quá trình thực hiện có lỗi: " + ex.Message, false);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnCheckNPLTon_Click(object sender, EventArgs e)
        {
            //minhnd Kiem Tra Tờ khai chưa có Tồn
            ToKhaiChuaCoNPLTon f = new ToKhaiChuaCoNPLTon();
            f.ds = GetDSTKChuaCoTon();
            if (f.ds.Tables[0].Rows.Count > 0)
                f.ShowDialog();
            else
                MessageBox.Show("Không có tờ khai nào thiếu NPL trong năm "+txtNamTiepNhan.Text,"Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
        public DataSet GetDSTKChuaCoTon()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DataSet ds = new DataSet();
            //Lấy danh sách TK chưa có Ton
            try
            {
                string sp = "SELECT  SoToKhai ,CASE WHEN MaLoaiHinh LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhai) ELSE 0 END as SoTKVNACCS,MaLoaiHinh ,NgayDangKy ,NamDangKy " +
                            "FROM    dbo.t_SXXK_ToKhaiMauDich " +
                            "WHERE   SoToKhai NOT IN ( SELECT SoToKhai FROM dbo.v_HangTon WHERE  MaLoaiHinh LIKE 'NV%' )AND dbo.t_SXXK_ToKhaiMauDich.MaLoaiHinh LIKE 'NV%' And NamDangKy={0}";
                DbCommand cmd = db.GetSqlStringCommand(string.Format(sp,txtNamTiepNhan.Value));
                db.LoadDataSet(cmd, ds, "t_hangton");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return ds;

        }
       
        private void toolStripChuyenTieuThuNoiDia_Click(object sender, EventArgs e)
        {

        }

        #region chay background

        private void InitializeBackgroundWorker()
        {
            backgroundHangTon.RunWorkerAsync();
            backgroundHangTon.DoWork +=
                new DoWorkEventHandler(backgroundHangTon_DoWork);
            backgroundHangTon.RunWorkerCompleted +=
                new RunWorkerCompletedEventHandler(backgroundHangTon_RunWorkerCompleted);
            backgroundHangTon.ProgressChanged +=
                new ProgressChangedEventHandler(backgroundHangTon_ProgressChanged);
        }
        private void backgroundHangTon_DoWork(object sender, DoWorkEventArgs e)
        {
            searchNew();
        }

        private void backgroundHangTon_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundHangTon_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            dgList.DataSource = tkmdCollection;
            dgList.Refresh();
        }
        #endregion
        private DataTable GetDataSource()
        {
            try
            {
                string spName = "[p_sxxk_Mau15_Details]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, Int32.Parse(txtNamTiepNhan.Text));
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void btn_Mau15_ChiTiet_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (int.Parse(txtNamTiepNhan.Text) >= 2015 && txtNamTiepNhan.Text.Trim() != "")
                {
                    //Mau15_TT38_ChiTiet rp = new Mau15_TT38_ChiTiet();
                    //rp.BindReport(txtNamTiepNhan.Text.Trim());
                    //rp.ShowPreview();
                    this.Cursor = Cursors.WaitCursor;
                    Mau15ChiTietForm f = new Mau15ChiTietForm();
                    f.tb = GetDataSource();
                    f.namDK = int.Parse(txtNamTiepNhan.Text);
                    f.Show();
                }
                else
                    MessageBox.Show("Nhập năm đăng ký để xuất mẫu 15 theo năm quyết toán","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Error);

                
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            finally 
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnSearchSP_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            // Xây dựng điều kiện tìm kiếm.
            string where = "1=1";
            if (txtSoTK.TextLength > 0)
            {
                if (txtSoTK.Text.Substring(0, 1).Contains("3"))
                {
                    where += " AND SoToKhaiXuat = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS ='" + txtSoTK.Value + "')";
                }
                else
                {
                    where += " AND SoToKhaiNhap = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTKVNACCS ='" + txtSoTK.Value + "')";                
                }


            }

            if (txtNamDK.TextLength > 0)
            {
                where += String.Format(" AND Year(NgayDangKyNhap) = {0} AND Year(NgayDangKyXuat) = {1} ",txtNamDK.Value,txtNamDK.Value);
            }
            if (txtMaSP.Text.Length > 0)
            {
                    where += " AND MaSP LIKE '%" + txtMaSP.Text.Trim() + "%'";
            }
            if (txtTenSP.Text.Length > 0)
            {
                where += " AND TenSP LIKE '%" + txtTenSP.Text.Trim() + "%'";
            }
            if (txtLanTL.Text.Length > 0)
            {
                where += " AND LanThanhLy =" + txtLanTL.Text.Trim();
            }
            //DataSet ds = Company.BLL.SXXK.ThanhKhoan.NPLXuatTon.SelectViewNPLXuatTon(where,null);
            DataSet ds = Company.BLL.SXXK.ThanhKhoan.BCXuatNhapTon.SelectBCXuatNhapTon(where,null);
            dgListSP.DataSource = ds.Tables[0];
            dgListSP.Refetch();
            this.Cursor = Cursors.Default;
        }

        private void btnExportExcelSP_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "Bảng kê sản phẩm_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
            //sfNPL.ShowDialog(this);
             if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgListSP;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
        }

        private void btnExportExcelNPL_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "Bảng kê NPL_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
            //sfNPL.ShowDialog(this);
            if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
            {
                Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                gridEXExporter1.GridEX = dgListNPL;
                System.IO.Stream str = sfNPL.OpenFile();
                gridEXExporter1.Export(str);
                str.Close();

                if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    System.Diagnostics.Process.Start(sfNPL.FileName);
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < tkmdCollection.Count; i++)
                {
                    if (tkmdCollection[i+1].SoToKhai != tkmdCollection[i].SoToKhai)
                    {
                        Company.BLL.KDT.ToKhaiMauDich TK = new Company.BLL.KDT.ToKhaiMauDich();
                        TK.SoToKhai = tkmdCollection[i].SoToKhai;
                        TK.MaLoaiHinh = tkmdCollection[i].MaLoaiHinh;
                        TK.NamDK = tkmdCollection[i].NamDangKy;
                        TK.MaHaiQuan = tkmdCollection[i].MaHaiQuan;
                        TK.MaDoanhNghiep = tkmdCollection[i].MaDoanhNghiep;
                        TK.Load(TK.MaHaiQuan, TK.MaDoanhNghiep, TK.SoToKhai, TK.NamDK, TK.MaLoaiHinh);
                        TK.LoadHMDCollection();
                        TK.CapNhatThongTinHangToKhaiSua();                        
                    }
                }
                ShowMessage("Cập nhật thành công", false);
                btnSearch_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            string dateFrom = dtpFrom.Value.ToString("yyyy-MM-dd 00:00:00");
            string dateTo = dtpTo.Value.ToString("yyyy-MM-dd 23:59:59");
            DataTable dtToTalNPLNhap = new DataTable();
            DataTable dtToTalNPLXuat = new DataTable();
            DataTable dtTotal = new DataTable();
            DataColumn dtColMANPL = new DataColumn("MANPL");
            dtColMANPL.DataType = typeof(System.String);
            DataColumn dtColTENNPL = new DataColumn("TENNPL");
            dtColTENNPL.DataType = typeof(System.String);
            DataColumn dtColDVT = new DataColumn("DVT");
            dtColDVT.DataType = typeof(System.String);
            DataColumn dtColLUONGNHAPTK = new DataColumn("LUONGNHAPTK");
            dtColLUONGNHAPTK.DataType = typeof(System.Decimal);
            DataColumn dtColLUONGXUATTK = new DataColumn("LUONGXUATTK");
            dtColLUONGXUATTK.DataType = typeof(System.Decimal);
            DataColumn dtColLUONGTONCK = new DataColumn("LUONGTONTK");
            dtColLUONGTONCK.DataType = typeof(System.Decimal);

            dtTotal.Columns.Add(dtColMANPL);
            dtTotal.Columns.Add(dtColTENNPL);
            dtTotal.Columns.Add(dtColDVT);
            dtTotal.Columns.Add(dtColLUONGNHAPTK);
            dtTotal.Columns.Add(dtColLUONGXUATTK);
            dtTotal.Columns.Add(dtColLUONGTONCK);
            #region Chi tiết các tờ khai nhập và xuất
            try
            {
                string spName = "t_KDT_SXXK_BCXuatNhapTon_SelectNPLDetailByTime";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@dateFrom", SqlDbType.NVarChar, dateFrom);
                db.AddInParameter(dbCommand, "@dateTo", SqlDbType.NVarChar, dateTo);

                dgListDetail.DataSource = db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            #endregion
            #region Tính tổng lượng nhập
            try
            {
                string spName = "t_KDT_SXXK_BCXuatNhapTon_SelectNPLNhapDetailByTime";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@dateFrom", SqlDbType.NVarChar, dateFrom);
                db.AddInParameter(dbCommand, "@dateTo", SqlDbType.NVarChar, dateTo);
                dtToTalNPLNhap = db.ExecuteDataSet(dbCommand).Tables[0];
                foreach (DataRow item in dtToTalNPLNhap.Rows)
                {
                    //Thêm dữ liệu ban đầu
                    DataRow dr = dtTotal.NewRow();
                    dr["MANPL"] = item["MaNPL"].ToString();
                    dr["TENNPL"] = item["TenNPL"].ToString();
                    dr["DVT"] = item["TenDVT_NPL"].ToString();
                    dr["LUONGNHAPTK"] = item["LuongNhap"].ToString();
                    dr["LUONGXUATTK"] = 0;
                    dr["LUONGTONTK"] = 0;
                    dtTotal.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            #endregion
            #region Tính tổng lượng xuất
            try
            {
                string spName = "t_KDT_SXXK_BCXuatNhapTon_SelectNPLXuatDetailByTime";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@dateFrom", SqlDbType.NVarChar, dateFrom);
                db.AddInParameter(dbCommand, "@dateTo", SqlDbType.NVarChar, dateTo);
                dtToTalNPLXuat = db.ExecuteDataSet(dbCommand).Tables[0];
                foreach (DataRow item in dtTotal.Rows)
                {
                    String MaNPL = item["MANPL"].ToString();
                    foreach (DataRow dr in dtToTalNPLXuat.Rows)
                    {
                        String MaNPLTemp = dr["MaNPL"].ToString();
                        if (MaNPL==MaNPLTemp)
                        {
                            //Gán lại lượng xuất
                            item["LUONGXUATTK"] = dr["LuongXuat"].ToString();                        
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            #endregion
            #region Xử lý lượng tồn
            try
            {
                foreach (DataRow dr in dtTotal.Rows)
                {
                    decimal LuongNhap = Convert.ToDecimal(dr["LUONGNHAPTK"].ToString());
                    decimal LuongXuat = Convert.ToDecimal(dr["LUONGXUATTK"].ToString());
                    decimal LuongTon = LuongNhap - LuongXuat;
                    dr["LUONGTONTK"] = LuongTon;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
            #endregion

            dgListToTal.DataSource = dtTotal;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel("Danh sách NPL ",dgListToTal);
        }
        private void ExportExcel(string tenFile, GridEX dg)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = tenFile + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dg;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ExportExcel("Danh sách NPL chi tiết ", dgListDetail);
        }

        private void btnUpdateNew_Click(object sender, EventArgs e)
        {
            try
            {
                int NamDangKy = Convert.ToInt32(txtNamTiepNhan.Text);
                #region Thêm mới các dòng NPL vào table t_SXXK_ThanhLy_NPLNhapTon
                Company.BLL.KDT.SXXK.BCXuatNhapTonCollection BCXNTCollection = Company.BLL.KDT.SXXK.BCXuatNhapTon.SelectCollectionDynamicBy(" SoToKhaiNhap IN (SELECT DISTINCT SoToKhai FROM t_SXXK_ToKhaiMauDich WHERE SoToKhai NOT IN (SELECT DISTINCT SoToKhai FROM dbo.t_SXXK_ThanhLy_NPLNhapTon WHERE NamDangKy = " + NamDangKy + ") AND NamDangKy = " + NamDangKy + " AND MaLoaiHinh LIKE '%N%')", "");
                ToKhaiMauDichCollection TKMDCollection = ToKhaiMauDich.SelectToKhaiKhongCoNPLTon(NamDangKy);
                foreach (ToKhaiMauDich TKMD in TKMDCollection)
                {
                    TKMD.LoadHMDCollection();
                    foreach (HangMauDich HMD in TKMD.HMDCollection)
                    {
                        try
                        {
                            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon NPL = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                            NPL.SoToKhai = TKMD.SoToKhai;
                            NPL.MaLoaiHinh = TKMD.MaLoaiHinh;
                            NPL.NamDangKy = TKMD.NamDangKy;
                            NPL.MaHaiQuan = TKMD.MaHaiQuan;
                            NPL.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                            NPL.MaNPL = HMD.MaPhu;
                            NPL.Luong = HMD.SoLuong;
                            NPL.Ton = HMD.Ton;
                            NPL.ThueXNK = Convert.ToDouble(HMD.ThueXNK);
                            NPL.ThueTTDB = Convert.ToDouble(HMD.ThueSuatTTDB);
                            NPL.ThueVAT = Convert.ToDouble(HMD.ThueGTGT);
                            NPL.PhuThu = Convert.ToDouble(HMD.PhuThu);
                            NPL.ThueCLGia = Convert.ToDouble(HMD.TyLeThuKhac);
                            NPL.ThueXNKTon = Convert.ToDouble(HMD.ThueXNK);
                            NPL.SoThuTuHang = HMD.SoThuTuHang;
                            NPL.Insert();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                #endregion
                #region Thêm mới các dòng NPL vào table t_KDT_SXXK_NPLNhapTon
                #endregion
                foreach (ToKhaiMauDich TKMD in TKMDCollection)
                {
                    TKMD.LoadHMDCollection();                    
                    foreach (Company.BLL.KDT.SXXK.BCXuatNhapTon BC in BCXNTCollection)
                    {
                        //string NPLTemp = BC.MaNPL;
                        //int LanThanhLy = BC.LanThanhLy;
                        //for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                        //{
                        //    if (NPLTemp == TKMD.HMDCollection[i].MaPhu)
                        //    {
                        try
                        {
                            Company.BLL.KDT.SXXK.NPLNhapTon NPL = new Company.BLL.KDT.SXXK.NPLNhapTon();
                            NPL.SoToKhai = BC.SoToKhaiNhap;
                            NPL.MaLoaiHinh = BC.MaLoaiHinhNhap;
                            NPL.NamDangKy = Convert.ToInt16(BC.NgayDangKyNhap.Year);
                            NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            NPL.MaNPL = BC.MaNPL;
                            NPL.LanThanhLy = BC.LanThanhLy;
                            NPL.Luong = BC.LuongNhap;
                            NPL.TonDau = BC.LuongTonDau;
                            if (BC.LuongTonCuoi >= 0)
                            {
                                NPL.TonCuoi = BC.LuongTonCuoi;
                                NPL.TonCuoiThueXNK = (BC.ThueXNK / Convert.ToDouble(BC.LuongNhap)) * Convert.ToDouble(BC.LuongTonCuoi);
                            }
                            else
                            {
                                NPL.TonCuoi = 0;
                                NPL.TonCuoiThueXNK = 0;
                            }
                            NPL.ThueXNK = BC.ThueXNK;
                            NPL.ThueVAT = 0;
                            NPL.ThueTTDB = 0;
                            NPL.PhuThu = 0;
                            NPL.ThueCLGia = 0;
                            NPL.TonDauThueXNK = BC.ThueXNK;
                            NPL.MaDoanhNghiep = BC.MaDoanhNghiep;
                            NPL.SoThuTuHang = BC.SoThuTuHang;
                            NPL.Insert();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }


                        //    }
                        //    else
                        //    {
                        //        Company.BLL.KDT.SXXK.NPLNhapTon NPL = new Company.BLL.KDT.SXXK.NPLNhapTon();
                        //        NPL.SoToKhai = BC.SoToKhaiNhap;
                        //        NPL.MaLoaiHinh = BC.MaLoaiHinhNhap;
                        //        NPL.NamDangKy = Convert.ToInt16(BC.NgayDangKyNhap.Year);
                        //        NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        //        NPL.MaNPL = BC.MaNPL;
                        //        NPL.LanThanhLy = BC.LanThanhLy;
                        //        NPL.Luong = BC.LuongNhap;
                        //        NPL.TonDau = BC.LuongTonDau;
                        //        NPL.TonCuoi = BC.LuongTonCuoi;
                        //        NPL.ThueXNK = BC.ThueXNK;
                        //        NPL.ThueVAT = 0;
                        //        NPL.ThueTTDB = 0;
                        //        NPL.PhuThu = 0;
                        //        NPL.ThueCLGia = 0;
                        //        NPL.TonDauThueXNK = BC.ThueXNK;
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdDieuChinhThue":
                    uiButton4_Click(null, null);
                    break;
                case "cmdDieuChinhTon":
                    btnDieuChinhLuongTon_Click(null, null);
                    break;
                case "cmdKiemTraNPL":
                    btnCheckNPLTon_Click(null, null);
                    break;
                case "cmdCapNhatTKSai":
                    btnUpdate_Click(null, null);
                    break;
                case "cmdCapNhatTK":
                    btnUpdateNew_Click(null, null);
                    break;
                case "cmdSave":
                    uiButton2_Click(null, null);
                    break;
                case "cmdPrint":
                    uiButton1_Click(null, null);
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null, null);
                    break;
            }
        }
        private void ProcessData()
        {
            try
            {
                ProcessForm processForm = new ProcessForm();
                processForm.StartPosition = FormStartPosition.CenterScreen;
                processForm.dateFrom = dtpFrom.Value;
                processForm.dateTo = dtpTo.Value;
                processForm.ShowDialog(this);
                if (processForm.DialogResult == DialogResult.OK)
                    ShowMessage("Xử lý thành công ", false);
                else
                {
                    this.ShowMessage("Lỗi xử lý  + " + processForm.ExceptionForm.Message, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnTotalExport_Click(object sender, EventArgs e)
        {
            ProcessData();
            BindDataTotalExport();
        }
        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        public DataTable GetToKhaiNhapNPL(string maNPL, string dateFrom, string dateTo)
        {
            string sql = "SELECT tkmd.ID,CASE WHEN tkmd.MaLoaiHinh LIKE '%V%' THEN tkmd.LoaiVanDon ELSE CONVERT(NVARCHAR(12), tkmd.SoToKhai) END AS SoToKhaiVNACCS,tkmd.SoToKhai,tkmd.SoTiepNhan, tkmd.MaLoaiHinh, tkmd.NgayDangKy, tkmd.NgayTiepNhan, hmd.MaPhu, hmd.SoLuong  " +
                         "FROM t_KDT_ToKhaiMauDich tkmd INNER JOIN t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID " +
                         "WHERE hmd.MaPhu = @MaPhu AND tkmd.MaLoaiHinh LIKE 'N%' AND tkmd.LoaiHangHoa = 'N' AND NgayDangKy BETWEEN '" + dateFrom + "' AND '" + dateTo + "' AND tkmd.TrangThaiXuLy not in (10,11)";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        private SXXK_NguyenPhuLieu_Ton getNPL(string MaNPL, List<SXXK_NguyenPhuLieu_Ton> collection)
        {
            foreach (SXXK_NguyenPhuLieu_Ton npl in collection)
            {
                if (npl.Ma.ToUpper().Trim() == MaNPL.ToUpper().Trim())
                    return npl;
            }
            return null;
        }
        private void BindDataTotalExport()
        {
            try
            {
                List<SXXK_NguyenPhuLieu_Ton> NPLCollection = SXXK_NguyenPhuLieu_Ton.SelectCollectionAll();
                List<SXXK_NguyenPhuLieu_Ton> NPLCollectionNew = new List<SXXK_NguyenPhuLieu_Ton>();
                foreach (SXXK_NguyenPhuLieu_Ton item in NPLCollection)
                {
                    if (item.LuongNhap!=0 || item.LuongXuat!=0)
                    {
                        NPLCollectionNew.Add(item);
                    }
                }
                dgListTotalExport.Refetch();
                dgListTotalExport.DataSource = NPLCollectionNew;
                dgListTotalExport.Refresh();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi \r\n " + ex.Message,false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void mnuTKXuatSP_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgListTotalExport.GetRow();
            if (row == null) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            string dateFrom = dtpFrom.Value.ToString("yyyy-MM-dd 00:00:00");
            string dateTo = dtpTo.Value.ToString("yyyy-MM-dd 23:59:59");

            NPLSuDungTrongTKXuatForm f = new NPLSuDungTrongTKXuatForm();
            f.MaNPL = maNPL;
            f.dateFrom = dateFrom;
            f.dateTo = dateTo;
            f.ShowDialog();
        }

        private void mnuTKNhapNPL_Click(object sender, EventArgs e)
        {
            GridEXRow row = dgListTotalExport.GetRow();
            if (row == null) return;
            string maNPL = row.Cells["Ma"].Value.ToString();
            Company.Interface.ToKhaiMauDichRegistedForm f = new Company.Interface.ToKhaiMauDichRegistedForm();
            f.Text = setText("Danh sách tờ khai nhập NPL '" + maNPL + "'", "List of import raw material declaration  '" + maNPL + "'");
            string dateFrom = dtpFrom.Value.ToString("yyyy-MM-dd 00:00:00");
            string dateTo = dtpTo.Value.ToString("yyyy-MM-dd 23:59:59");
            f.dt = GetToKhaiNhapNPL(maNPL,dateFrom,dateTo);
            f.ShowDialog();
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel("Danh sách NPL ", dgListTotalExport);
        }

        private void btnSearchDetail_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string dateFrom = dateFromDetail.Value.ToString("yyyy-MM-dd 00:00:00");
                string dateTo = dateToDetail.Value.ToString("yyyy-MM-dd 23:59:59");
                // Xây dựng điều kiện tìm kiếm.
                string where = "1=1";
                where += String.Format(" AND NgayDangKyNhap  BETWEEN '{0}' AND '{1}' AND  NgayDangKyXuat BETWEEN  '{2}' AND '{3}'", dateFrom, dateTo, dateFrom, dateTo);
                //DataSet ds = Company.BLL.SXXK.ThanhKhoan.NPLXuatTon.SelectViewNPLXuatTon(where,null);
                DataSet ds = Company.BLL.SXXK.ThanhKhoan.BCXuatNhapTon.SelectBCXuatNhapTon(where, null);
                DataTable dtDetail = ds.Tables[0];
                DataTable dtTemp = dtDetail.Clone();
                int i = 0;
                string MaNPLTemp = "";
                string SoTKNhapTemp = "";
                decimal LuongTonTemp = 0;
                foreach (DataRow dr in dtDetail.Rows)
                {
                    string MaNPL = dr["MaNPL"].ToString();
                    string SoTKNhap = dr["SoToKhaiNhap"].ToString();
                    Decimal LuongTonCuoi =Convert.ToDecimal(dr["LuongTonCuoi"].ToString());
                    Decimal LuongTonDau = Convert.ToDecimal(dr["LuongTonDau"].ToString());
                    Decimal LuongNPLSD = Convert.ToDecimal(dr["LuongNPLSuDung"].ToString());
                    if ( LuongTonCuoi < 0)
                    {
                        DataRow drNew = dtTemp.NewRow();
                        drNew["LanThanhLy"] = dr["LanThanhLy"].ToString();
                        drNew["SoTKNhapVNACCS"] = dr["SoTKNhapVNACCS"].ToString();
                        drNew["SoToKhaiNhap"] = dr["SoToKhaiNhap"].ToString();
                        drNew["NgayDangKyNhap"] = Convert.ToDateTime(dr["NgayDangKyNhap"].ToString());
                        drNew["MaLoaiHinhNhap"] = dr["MaLoaiHinhNhap"].ToString();
                        drNew["MaNPL"] = dr["MaNPL"].ToString();
                        drNew["TenNPL"] = dr["TenNPL"].ToString();
                        drNew["TenDVT_NPL"] = dr["TenDVT_NPL"].ToString();
                        //drNew["SoThuTuHang"] = dr["SoThuTuHang"].ToString();
                        drNew["LuongNhap"] = Convert.ToDecimal(dr["LuongNhap"].ToString());
                        drNew["LuongTonDau"] = Convert.ToDecimal(dr["LuongTonDau"].ToString());
                        drNew["SoTKXuatVNACCS"] = dr["SoTKXuatVNACCS"].ToString();
                        drNew["SoToKhaiXuat"] = dr["SoToKhaiXuat"].ToString();
                        drNew["NgayDangKyXuat"] = Convert.ToDateTime(dr["NgayDangKyXuat"].ToString());
                        drNew["MaLoaiHinhXuat"] = dr["MaLoaiHinhXuat"].ToString();
                        drNew["MaSP"] = dr["MaSP"].ToString();
                        drNew["TenSP"] = dr["TenSP"].ToString();
                        drNew["TenDVT_SP"] = dr["TenDVT_SP"].ToString();
                        drNew["LuongSPXuat"] = Convert.ToDecimal(dr["LuongSPXuat"].ToString());
                        drNew["DinhMuc"] = Convert.ToDecimal(dr["DinhMuc"].ToString());
                        decimal LuongNPLSuDung = Company.BLL.SXXK.ThanhKhoan.BCXuatNhapTon.GetNPLSuDung(MaNPL,Convert.ToInt32(SoTKNhap));
                        if (LuongNPLSuDung  > 0)
                        {
                            drNew["LuongNPLSuDung"] = LuongNPLSuDung;//;Convert.ToDecimal(dr["LuongNPLSuDung"].ToString());//dr["LuongNPLSuDung"].ToString();
                        }
                        else
                        {
                            drNew["LuongNPLSuDung"] = Convert.ToDecimal(dr["LuongTonDau"].ToString());//dr["LuongNPLSuDung"].ToString();
                        }
                        drNew["LuongTonCuoi"] = 0;//dr["LuongTonCuoi"].ToString();
                        drNew["ThanhKhoanTiep"] = dr["ThanhKhoanTiep"].ToString();
                        dtTemp.Rows.Add(drNew);
                    }
                    else
                    {
                        DataRow drNew = dtTemp.NewRow();
                        drNew["LanThanhLy"] = dr["LanThanhLy"].ToString();
                        drNew["SoTKNhapVNACCS"] = dr["SoTKNhapVNACCS"].ToString();
                        drNew["SoToKhaiNhap"] = dr["SoToKhaiNhap"].ToString();
                        drNew["NgayDangKyNhap"] = Convert.ToDateTime(dr["NgayDangKyNhap"].ToString());
                        drNew["MaLoaiHinhNhap"] = dr["MaLoaiHinhNhap"].ToString();
                        drNew["MaNPL"] = dr["MaNPL"].ToString();
                        drNew["TenNPL"] = dr["TenNPL"].ToString();
                        drNew["TenDVT_NPL"] = dr["TenDVT_NPL"].ToString();
                        //drNew["SoThuTuHang"] = dr["SoThuTuHang"].ToString();
                        drNew["LuongNhap"] = Convert.ToDecimal(dr["LuongNhap"].ToString());
                        drNew["LuongTonDau"] =Convert.ToDecimal(dr["LuongTonDau"].ToString());
                        drNew["SoTKXuatVNACCS"] = dr["SoTKXuatVNACCS"].ToString();
                        drNew["SoToKhaiXuat"] = dr["SoToKhaiXuat"].ToString();
                        drNew["NgayDangKyXuat"] = Convert.ToDateTime(dr["NgayDangKyXuat"].ToString());
                        drNew["MaLoaiHinhXuat"] = dr["MaLoaiHinhXuat"].ToString();
                        drNew["MaSP"] = dr["MaSP"].ToString();
                        drNew["TenSP"] = dr["TenSP"].ToString();
                        drNew["TenDVT_SP"] = dr["TenDVT_SP"].ToString();
                        drNew["LuongSPXuat"] =Convert.ToDecimal(dr["LuongSPXuat"].ToString());
                        drNew["DinhMuc"] =Convert.ToDecimal(dr["DinhMuc"].ToString());
                        if (LuongTonCuoi == LuongTonDau - LuongNPLSD)
                        {
                            drNew["LuongNPLSuDung"] = Convert.ToDecimal(dr["LuongNPLSuDung"].ToString());
                        }
                        else
                        {
                            if (MaNPL==MaNPLTemp && SoTKNhap==SoTKNhapTemp)
                            {
                                drNew["LuongNPLSuDung"] = Convert.ToDecimal(dr["LuongNPLSuDung"].ToString());
                            }
                            else
                            {
                                //drNew["LuongNPLSuDung"]=LuongTonTemp;
                                drNew["LuongNPLSuDung"] = LuongTonDau - LuongTonCuoi;
                            }
                        }
                        drNew["LuongTonCuoi"] = Convert.ToDecimal(dr["LuongTonCuoi"].ToString());
                        drNew["ThanhKhoanTiep"] = dr["ThanhKhoanTiep"].ToString();
                        dtTemp.Rows.Add(drNew);
                    }
                     MaNPLTemp = dr["MaNPL"].ToString();
                     SoTKNhapTemp = dr["SoToKhaiNhap"].ToString();
                     LuongTonTemp = Convert.ToDecimal(dr["LuongTonCuoi"].ToString());
                }
                gridListDetail.DataSource = dtTemp;
                gridListDetail.Refetch();
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcelDetail_Click(object sender, EventArgs e)
        {
            ExportExcel("Chi tiết thanh khoản ", gridListDetail);
        }
    }
}