﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Collections;
namespace DongBoDuLieu.BLL.GC
{

    public class HopDongGC
    {
        public DataTable NguyenPhuLieu { get; set; }
        public DataTable NhomSanPham { get; set; }
        public DataTable ThietBi { get; set; }
        public DataTable DinhMuc { get; set; }
        public DataTable PhuKienDK { get; set; }
        public DataTable ToKhaiMauDich { get; set; }
        public DataTable ToKhaiChuyenTiep { get; set; }
        public HangChuyenTiepList HangChuyenTiepList { get; set; }
        public DataTable BKCungUngDangKy { get; set; }
        public DataTable SanPham { get; set; }
        public HopDongInfo HopDongInfo { get; set; }            
        
    }
}
