﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace SignRemote
{
    public partial class MSG_FILE_INBOX
    {
        public static void InsertSignFileContent(string cnnString, string guidID, string FileName, string FileData, int Status, string MSG_FROM)
        {
            try
            {
                SqlConnection cnn = new SqlConnection(cnnString);
                cnn.Open();
                string MSG_TO = System.Environment.MachineName.ToString();
                //SqlCommand cmd = new SqlCommand("dbo.test", cnn);
                //cmd.CommandType = CommandType.StoredProcedure;
                //SqlDataReader  rdr = cmd.ExecuteReader();

                //string cmdText = string.Format("Insert into t_MSG_FILE_INBOX(GUIDSTR,FILE_NAME,FILE_DATA, STATUS) values ('{0}','{1}',{2},{3})", guidID, FileName,FileData, Status);
                //SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);

                //sqlCmd.ExecuteNonQuery();
                //cnn.Close();

                const string spName = "[dbo].[p_MSG_FILE_INBOX_Insert]";
                //SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = new SqlCommand(spName,cnn);

                dbCommand.CommandType = CommandType.StoredProcedure;

                dbCommand.Parameters.Add(new SqlParameter("@ID", 8));
                dbCommand.Parameters.Add(new SqlParameter("@MSG_FROM", MSG_FROM));
                dbCommand.Parameters.Add(new SqlParameter("@MSG_TO", MSG_TO));
                dbCommand.Parameters.Add(new SqlParameter("@FILE_NAME", FileName));
                dbCommand.Parameters.Add(new SqlParameter("@FILE_DATA", FileData));
                dbCommand.Parameters.Add(new SqlParameter("@CREATE_TIME", DateTime.Now));
                dbCommand.Parameters.Add(new SqlParameter("@STATUS", Status));
                dbCommand.Parameters.Add(new SqlParameter("@GUIDSTR", guidID));

                SqlDataReader rdr = dbCommand.ExecuteReader();

                //db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
                //db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.VarChar, "");
                //db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.VarChar, "");
                //db.AddInParameter(dbCommand, "@FILE_NAME", SqlDbType.NVarChar, FileName);
                //db.AddInParameter(dbCommand, "@FILE_DATA", SqlDbType.NVarChar, FileData);
                //db.AddInParameter(dbCommand, "@CREATE_TIME", SqlDbType.DateTime,DateTime.Now);
                //db.AddInParameter(dbCommand, "@STATUS", SqlDbType.Int, Status);
                //db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, guidID);
                //db.ExecuteNonQuery(dbCommand);
                cnn.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static MSG_FILE_INBOX GetTopDataContent()
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                string cmd = string.Format("Select top(1) * from t_MSG_FILE_INBOX order by CREATE_TIME desc");
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
                IDataReader reader = db.ExecuteReader(dbCommand);
                MSG_FILE_INBOX entity = null;
                List<MSG_FILE_INBOX> collection = new List<MSG_FILE_INBOX>();
                if (reader.Read())
                {
                    entity = new MSG_FILE_INBOX();
                    if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
                    if (!reader.IsDBNull(reader.GetOrdinal("FILE_NAME"))) entity.FILE_NAME = reader.GetString(reader.GetOrdinal("FILE_NAME"));
                    if (!reader.IsDBNull(reader.GetOrdinal("FILE_DATA"))) entity.FILE_DATA = reader.GetString(reader.GetOrdinal("FILE_DATA"));
                    if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
                    if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) entity.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
                    if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                    collection.Add(entity);
                }
                reader.Close();
                return entity;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
    }
}
