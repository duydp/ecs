﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.GC;

namespace Company.Interface.KDT.GC
{
    public partial class UpdateStatusForm : BaseForm
    {
        public HopDong HD;
        public DinhMucDangKy DMDK;
        public PhuKienDangKy PKDK;
        public KDT_GC_DinhMucThucTeDangKy DMTTDK;
        public T_KDT_VNACCS_WarehouseImport WarehouseImport;
        public T_KDT_VNACCS_WarehouseExport WarehouseExport;
        public KDT_VNACCS_BaoCaoQuyetToan_TT39 BCQT;
        public KDT_VNACCS_StorageAreasProduction CSSX;
        public Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport BCCT;
        public string formType = "";
        public UpdateStatusForm()
        {
            InitializeComponent();
        }

        private void UpdateStatusForm_Load(object sender, EventArgs e)
        {
            txtSoTN.Enabled = false;
            txtSoTN.ForeColor = Color.Red;
            clcNgayTN.Enabled = false;
            clcNgayTN.ForeColor = Color.Red;
            txtChiCucHQ.Enabled = false;
            txtChiCucHQ.ForeColor = Color.Red;
            cbbTrangThai.SelectedValue = 1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            if (formType=="HD")
            {
                txtSoTN.Text = HD.SoTiepNhan.ToString();
                txtSoTN.ForeColor = Color.Red;
                clcNgayTN.Value = HD.NgayTiepNhan;
                clcNgayTN.ForeColor = Color.Red;
                txtChiCucHQ.Text = HD.MaHaiQuan;
                txtChiCucHQ.ForeColor = Color.Red;
                switch (HD.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }                
                cbbTrangThai.SelectedValue = HD.TrangThaiXuLy;
            }
            else if (formType=="DM")
            {
                txtSoTN.Text = DMDK.SoTiepNhan.ToString();
                clcNgayTN.Value = DMDK.NgayTiepNhan;
                txtChiCucHQ.Text = DMDK.MaHaiQuan;
                switch (DMDK.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }                
                cbbTrangThai.SelectedValue = DMDK.TrangThaiXuLy;
            }
            else if (formType=="PK")
            {
                txtSoTN.Text = PKDK.SoTiepNhan.ToString();
                clcNgayTN.Value = PKDK.NgayTiepNhan;
                txtChiCucHQ.Text = PKDK.MaHaiQuan;
                switch (PKDK.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }                
                cbbTrangThai.SelectedValue = PKDK.TrangThaiXuLy;
            }
            else if (formType=="PNK")
            {
                txtSoTN.Text = WarehouseImport.SoTN.ToString();
                clcNgayTN.Value = WarehouseImport.NgayTN;
                txtChiCucHQ.Text = WarehouseImport.MaHQ;
                switch (WarehouseImport.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = WarehouseImport.TrangThaiXuLy;
            }
            else if (formType == "PXK")
            {
                txtSoTN.Text = WarehouseExport.SoTN.ToString();
                clcNgayTN.Value = WarehouseExport.NgayTN;
                txtChiCucHQ.Text = WarehouseExport.MaHQ;
                switch (WarehouseExport.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = WarehouseExport.TrangThaiXuLy;
            }
            else if (formType == "BCQT")
            {
                txtSoTN.Text = BCQT.SoTN.ToString();
                clcNgayTN.Value = BCQT.NgayTN;
                txtChiCucHQ.Text = BCQT.MaHQ;
                switch (BCQT.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = BCQT.TrangThaiXuLy;
            }
            else if (formType == "BCCT")
            {
                txtSoTN.Text = BCCT.SoTiepNhan.ToString();
                clcNgayTN.Value = BCCT.NgayTiepNhan;
                txtChiCucHQ.Text = BCCT.MaHaiQuan;
                switch (BCCT.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = BCCT.TrangThaiXuLy;
            }
            else if (formType == "DMTT")
            {
                txtSoTN.Text = DMTTDK.SoTiepNhan.ToString();
                clcNgayTN.Value = DMTTDK.NgayTiepNhan;
                txtChiCucHQ.Text = DMTTDK.MaHaiQuan;
                switch (DMTTDK.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = DMTTDK.TrangThaiXuLy;
            }
            else if (formType == "CSSX")
            {
                txtSoTN.Text = CSSX.SoTN.ToString();
                clcNgayTN.Value = CSSX.NgayTN;
                txtChiCucHQ.Text = CSSX.MaHQ;
                switch (CSSX.TrangThaiXuLy.ToString())
                {
                    case "-1":
                        txtTrangThai.Text = "Chưa khai báo";
                        break;
                    case "0":
                        txtTrangThai.Text = "Chờ duyệt";
                        break;
                    case "1":
                        txtTrangThai.Text = "Đã duyệt";
                        break;
                    case "5":
                        txtTrangThai.Text = "Đang sửa";
                        break;
                    case "2":
                        txtTrangThai.Text = "Không phê duyệt";
                        break;
                    case "-2":
                        txtTrangThai.Text = "Chờ duyệt sửa";
                        break;
                    default:
                        break;
                }
                cbbTrangThai.SelectedValue = CSSX.TrangThaiXuLy;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                int lenghtSoTN = txtSoTN.Text.ToString().Length;
                if (formType == "HD")
                {
                    if (lenghtSoTN <= 6 && cbbTrangThai.SelectedValue == "1" || cbbTrangThai.SelectedValue == "0" && lenghtSoTN <= 6 || cbbTrangThai.SelectedValue == "5" && lenghtSoTN <= 6)
                    {
                        ShowMessage("HỢP ĐỒNG NÀY CHƯA ĐƯỢC KHAI BÁO ĐẾN HQ \nDOANH NGHIỆP THỰC HIỆN KHAI BÁO HỢP ĐỒNG NÀY ĐẾN HQ", false);
                        return;
                    }
                    else
                    {
                        HD.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        HD.Update();
                        ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                }
                else if (formType == "DM")
                {
                    if (lenghtSoTN <= 6 && cbbTrangThai.SelectedValue == "1" || cbbTrangThai.SelectedValue == "0" && lenghtSoTN <= 6 || cbbTrangThai.SelectedValue == "5" && lenghtSoTN <= 6)
                    {
                        ShowMessage("ĐỊNH MỨC NÀY CHƯA ĐƯỢC KHAI BÁO ĐẾN HQ \nDOANH NGHIỆP THỰC HIỆN KHAI BÁO ĐỊNH MỨC NÀY ĐẾN HQ", false);
                        return;
                    }
                    else if (lenghtSoTN > 7 || cbbTrangThai.SelectedValue == "1" )
                    {
                        DMDK.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        DMDK.Update();
                        DMDK.DeleteDinhMucGC();
                        DMDK.TransferGC();
                        ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();                        
                    }
                    else
                    {
                        DMDK.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        DMDK.Update();
                        ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                }
                else if (formType == "PK")
                {
                    if (lenghtSoTN <= 6 && cbbTrangThai.SelectedValue == "1" || cbbTrangThai.SelectedValue == "0" && lenghtSoTN <= 6 || cbbTrangThai.SelectedValue == "5" && lenghtSoTN <= 6)
                    {
                        ShowMessage("PHỤ KIỆN NÀY CHƯA ĐƯỢC KHAI BÁO ĐẾN HQ \nDOANH NGHIỆP THỰC HIỆN KHAI BÁO PHỤ KIỆN NÀY ĐẾN HQ", false);
                        return;
                    }
                    else if (lenghtSoTN > 7 || cbbTrangThai.SelectedValue == "1")
                    {
                        PKDK.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        PKDK.Update();
                        PKDK.updateTrangThaiDuLieu();
                        ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();                        
                    }
                    {
                        PKDK.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        PKDK.Update();
                        ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                }
                else if (formType == "PNK")
                {
                    WarehouseImport.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    WarehouseImport.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
                else if (formType == "PXK")
                {
                    WarehouseExport.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    WarehouseExport.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
                else if (formType == "BCQT")
                {
                    BCQT.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    BCQT.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
                else if (formType == "BCCT")
                {
                    BCCT.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    BCCT.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
                else if (formType == "DMTT")
                {
                    if (lenghtSoTN <= 6 && cbbTrangThai.SelectedValue == "1" || cbbTrangThai.SelectedValue == "0" && lenghtSoTN <= 6 || cbbTrangThai.SelectedValue == "5" && lenghtSoTN <= 6)
                    {
                        ShowMessage("ĐỊNH MỨC NÀY CHƯA ĐƯỢC KHAI BÁO ĐẾN HQ \nDOANH NGHIỆP THỰC HIỆN KHAI BÁO ĐỊNH MỨC NÀY ĐẾN HQ", false);
                        return;
                    }
                    else if (lenghtSoTN > 7 || cbbTrangThai.SelectedValue == "1")
                    {
                        DMTTDK.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        DMTTDK.Update();
                        DMTTDK.DeleteDinhMucGC();
                        DMTTDK.TransferGC();
                        ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                    else
                    {
                        DMTTDK.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                        DMTTDK.Update();
                        ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                        this.Close();
                    }
                }
                else if (formType == "CSSX")
                {
                    CSSX.TrangThaiXuLy = Convert.ToInt32(cbbTrangThai.SelectedValue.ToString());
                    CSSX.Update();
                    ShowMessage("CẬP NHẬT THÀNH CÔNG", false);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
