﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CA_QuanLyGCN.Report
{
    public partial class CauHinhFont : Form
    {
        public CauHinhFont()
        {
            InitializeComponent();
        }

        private void setForm()
        {
            numericEditBox1.Value = Global.sizeFont.TenDoanhNghiep;
            numericEditBox2.Value = Global.sizeFont.DiaChiDoanhNghiep;
            numericEditBox3.Value = Global.sizeFont.MaDoanhNghiep;
            numericEditBox4.Value = Global.sizeFont.CoQuanCapMaDoanhNghiep;
            numericEditBox5.Value = Global.sizeFont.NgayCapMST;
            numericEditBox6.Value = Global.sizeFont.TenNguoiDaiDien;
            numericEditBox7.Value = Global.sizeFont.QuocTich;
            numericEditBox8.Value = Global.sizeFont.ChucDanh;
            numericEditBox9.Value = Global.sizeFont.SoCMND;
            numericEditBox10.Value = Global.sizeFont.NgayCapCMND;
            numericEditBox11.Value = Global.sizeFont.CoQuanCapCMND;
            numericEditBox12.Value = Global.sizeFont.HoKhau;
            numericEditBox13.Value = Global.sizeFont.TenCoQuan;
            numericEditBox14.Value = Global.sizeFont.TenPhongBan;
            numericEditBox15.Value = Global.sizeFont.TenDayDuPhongBan;
            numericEditBox16.Value = Global.sizeFont.SoGiayCN;
            numericEditBox17.Value = Global.sizeFont.TenCoSoKD;
            numericEditBox18.Value = Global.sizeFont.NganhNgheKD;
            numericEditBox19.Value = Global.sizeFont.DiaChiCoSoKinhDoanh;
            numericEditBox20.Value = Global.sizeFont.NgayChungNhan;

            uiCheckBox1.Checked = Global.sizeFont.TenDoanhNghiepBold;
            uiCheckBox3.Checked = Global.sizeFont.DiaChiDoanhNghiepBold;
            uiCheckBox5.Checked = Global.sizeFont.MaDoanhNghiepBold;
            uiCheckBox7.Checked = Global.sizeFont.CoQuanCapMaDoanhNghiepBold;
            uiCheckBox9.Checked = Global.sizeFont.NgayCapMSTBold;
            uiCheckBox11.Checked = Global.sizeFont.TenNguoiDaiDienBold;
            uiCheckBox13.Checked = Global.sizeFont.QuocTichBold;
            uiCheckBox15.Checked = Global.sizeFont.ChucDanhBold;
            uiCheckBox17.Checked = Global.sizeFont.SoCMNDBold;
            uiCheckBox19.Checked = Global.sizeFont.NgayCapCMNDBold;
            uiCheckBox21.Checked = Global.sizeFont.CoQuanCapCMNDBold;
            uiCheckBox23.Checked = Global.sizeFont.HoKhauBold;
            uiCheckBox25.Checked = Global.sizeFont.TenCoQuanBold;
            uiCheckBox27.Checked = Global.sizeFont.TenPhongBanBold;
            uiCheckBox29.Checked = Global.sizeFont.TenDayDuPhongBanBold;
            uiCheckBox31.Checked = Global.sizeFont.SoGiayCNBold;
            uiCheckBox33.Checked = Global.sizeFont.TenCoSoKDBold;
            uiCheckBox35.Checked = Global.sizeFont.NganhNgheKDBold;
            uiCheckBox37.Checked = Global.sizeFont.DiaChiCoSoKinhDoanhBold;
            uiCheckBox39.Checked = Global.sizeFont.NgayChungNhanBold;

            uiCheckBox2.Checked = Global.sizeFont.TenDoanhNghiepItalic;
            uiCheckBox4.Checked = Global.sizeFont.DiaChiDoanhNghiepItalic;
            uiCheckBox6.Checked = Global.sizeFont.MaDoanhNghiepItalic;
            uiCheckBox8.Checked = Global.sizeFont.CoQuanCapMaDoanhNghiepItalic;
            uiCheckBox10.Checked = Global.sizeFont.NgayCapMSTItalic;
            uiCheckBox12.Checked = Global.sizeFont.TenNguoiDaiDienItalic;
            uiCheckBox14.Checked = Global.sizeFont.QuocTichItalic;
            uiCheckBox16.Checked = Global.sizeFont.ChucDanhItalic;
            uiCheckBox18.Checked = Global.sizeFont.SoCMNDItalic;
            uiCheckBox20.Checked = Global.sizeFont.NgayCapCMNDItalic;
            uiCheckBox22.Checked = Global.sizeFont.CoQuanCapCMNDItalic;
            uiCheckBox24.Checked = Global.sizeFont.HoKhauItalic;
            uiCheckBox26.Checked = Global.sizeFont.TenCoQuanItalic;
            uiCheckBox28.Checked = Global.sizeFont.TenPhongBanItalic;
            uiCheckBox30.Checked = Global.sizeFont.TenDayDuPhongBanItalic;
            uiCheckBox32.Checked = Global.sizeFont.SoGiayCNItalic;
            uiCheckBox34.Checked = Global.sizeFont.TenCoSoKDItalic;
            uiCheckBox36.Checked = Global.sizeFont.NganhNgheKDItalic;
            uiCheckBox38.Checked = Global.sizeFont.DiaChiCoSoKinhDoanhItalic;
            uiCheckBox40.Checked = Global.sizeFont.NgayChungNhanItalic;


        }
        private void getForm()
        {
            Global.sizeFont.TenDoanhNghiep = (float)System.Convert.ToDouble(numericEditBox1.Value);
            Global.sizeFont.DiaChiDoanhNghiep = (float)System.Convert.ToDouble(numericEditBox2.Value);
            Global.sizeFont.MaDoanhNghiep = (float)System.Convert.ToDouble(numericEditBox3.Value);
            Global.sizeFont.CoQuanCapMaDoanhNghiep = (float)System.Convert.ToDouble(numericEditBox4.Value);
            Global.sizeFont.NgayCapMST = (float)System.Convert.ToDouble(numericEditBox5.Value);
            Global.sizeFont.TenNguoiDaiDien = (float)System.Convert.ToDouble(numericEditBox6.Value);
            Global.sizeFont.QuocTich = (float)System.Convert.ToDouble(numericEditBox7.Value);
            Global.sizeFont.ChucDanh = (float)System.Convert.ToDouble(numericEditBox8.Value);
            Global.sizeFont.SoCMND = (float)System.Convert.ToDouble(numericEditBox9.Value);
            Global.sizeFont.NgayCapCMND = (float)System.Convert.ToDouble(numericEditBox10.Value);
            Global.sizeFont.CoQuanCapCMND = (float)System.Convert.ToDouble(numericEditBox11.Value);
            Global.sizeFont.HoKhau = (float)System.Convert.ToDouble(numericEditBox12.Value);
            Global.sizeFont.TenCoQuan = (float)System.Convert.ToDouble(numericEditBox13.Value);
            Global.sizeFont.TenPhongBan = (float)System.Convert.ToDouble(numericEditBox14.Value);
            Global.sizeFont.TenDayDuPhongBan = (float)System.Convert.ToDouble(numericEditBox15.Value);
            Global.sizeFont.SoGiayCN = (float)System.Convert.ToDouble(numericEditBox16.Value);
            Global.sizeFont.TenCoSoKD = (float)System.Convert.ToDouble(numericEditBox17.Value);
            Global.sizeFont.NganhNgheKD = (float)System.Convert.ToDouble(numericEditBox18.Value);
            Global.sizeFont.DiaChiCoSoKinhDoanh = (float)System.Convert.ToDouble(numericEditBox19.Value);
            Global.sizeFont.NgayChungNhan = (float)System.Convert.ToDouble(numericEditBox20.Value);
            Global.sizeFont.TenDoanhNghiepBold = uiCheckBox1.Checked;
            Global.sizeFont.DiaChiDoanhNghiepBold = uiCheckBox3.Checked;
            Global.sizeFont.MaDoanhNghiepBold = uiCheckBox5.Checked;
            Global.sizeFont.CoQuanCapMaDoanhNghiepBold = uiCheckBox7.Checked;
            Global.sizeFont.NgayCapMSTBold = uiCheckBox9.Checked;
            Global.sizeFont.TenNguoiDaiDienBold = uiCheckBox11.Checked;
            Global.sizeFont.QuocTichBold = uiCheckBox13.Checked;
            Global.sizeFont.ChucDanhBold = uiCheckBox15.Checked;
            Global.sizeFont.SoCMNDBold = uiCheckBox17.Checked;
            Global.sizeFont.NgayCapCMNDBold = uiCheckBox19.Checked;
            Global.sizeFont.CoQuanCapCMNDBold = uiCheckBox21.Checked;
            Global.sizeFont.HoKhauBold = uiCheckBox23.Checked;
            Global.sizeFont.TenCoQuanBold = uiCheckBox25.Checked;
            Global.sizeFont.TenPhongBanBold = uiCheckBox27.Checked;
            Global.sizeFont.TenDayDuPhongBanBold = uiCheckBox29.Checked;
            Global.sizeFont.SoGiayCNBold = uiCheckBox31.Checked;
            Global.sizeFont.TenCoSoKDBold = uiCheckBox33.Checked;
            Global.sizeFont.NganhNgheKDBold = uiCheckBox35.Checked;
            Global.sizeFont.DiaChiCoSoKinhDoanhBold = uiCheckBox37.Checked;
            Global.sizeFont.NgayChungNhanBold = uiCheckBox39.Checked;


            Global.sizeFont.TenDoanhNghiepItalic = uiCheckBox2.Checked;
            Global.sizeFont.DiaChiDoanhNghiepItalic = uiCheckBox4.Checked;
            Global.sizeFont.MaDoanhNghiepItalic = uiCheckBox6.Checked;
            Global.sizeFont.CoQuanCapMaDoanhNghiepItalic = uiCheckBox8.Checked;
            Global.sizeFont.NgayCapMSTItalic = uiCheckBox10.Checked;
            Global.sizeFont.TenNguoiDaiDienItalic = uiCheckBox12.Checked;
            Global.sizeFont.QuocTichItalic = uiCheckBox14.Checked;
            Global.sizeFont.ChucDanhItalic = uiCheckBox16.Checked;
            Global.sizeFont.SoCMNDItalic = uiCheckBox18.Checked;
            Global.sizeFont.NgayCapCMNDItalic = uiCheckBox20.Checked;
            Global.sizeFont.CoQuanCapCMNDItalic = uiCheckBox22.Checked;
            Global.sizeFont.HoKhauItalic = uiCheckBox24.Checked;
            Global.sizeFont.TenCoQuanItalic = uiCheckBox26.Checked;
            Global.sizeFont.TenPhongBanItalic = uiCheckBox28.Checked;
            Global.sizeFont.TenDayDuPhongBanItalic = uiCheckBox30.Checked;
            Global.sizeFont.SoGiayCNItalic = uiCheckBox32.Checked;
            Global.sizeFont.TenCoSoKDItalic = uiCheckBox34.Checked;
            Global.sizeFont.NganhNgheKDItalic = uiCheckBox36.Checked;
            Global.sizeFont.DiaChiCoSoKinhDoanhItalic = uiCheckBox38.Checked;
            Global.sizeFont.NgayChungNhanItalic = uiCheckBox40.Checked;



        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                getForm();
                Global.sizeFont.writeXml(Application.StartupPath);
                MessageBox.Show("Lưu thành công");
                this.Close();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Lưu thất bại" + ex.Message);
            }
            
        }

        private void CauHinhFont_Load(object sender, EventArgs e)
        {
            setForm();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            GiayChungNhanReport re = new GiayChungNhanReport();
            re.GetDefaultFont();
            setForm();
        }

        private void uiCheckBox42_CheckedChanged(object sender, EventArgs e)
        {
            uiCheckBox2.Checked = chkCheckInNghieng.Checked;
            uiCheckBox4.Checked = chkCheckInNghieng.Checked;
            uiCheckBox6.Checked = chkCheckInNghieng.Checked;
            uiCheckBox8.Checked = chkCheckInNghieng.Checked;
            uiCheckBox10.Checked = chkCheckInNghieng.Checked;
            uiCheckBox12.Checked = chkCheckInNghieng.Checked;
            uiCheckBox14.Checked = chkCheckInNghieng.Checked;
            uiCheckBox16.Checked = chkCheckInNghieng.Checked;
            uiCheckBox18.Checked = chkCheckInNghieng.Checked;
            uiCheckBox20.Checked = chkCheckInNghieng.Checked;
            uiCheckBox22.Checked = chkCheckInNghieng.Checked;
            uiCheckBox24.Checked = chkCheckInNghieng.Checked;
            uiCheckBox26.Checked = chkCheckInNghieng.Checked;
            uiCheckBox28.Checked = chkCheckInNghieng.Checked;
            uiCheckBox30.Checked = chkCheckInNghieng.Checked;
            uiCheckBox32.Checked = chkCheckInNghieng.Checked;
            uiCheckBox34.Checked = chkCheckInNghieng.Checked;
            uiCheckBox36.Checked = chkCheckInNghieng.Checked;
            uiCheckBox38.Checked = chkCheckInNghieng.Checked;
            uiCheckBox40.Checked = chkCheckInNghieng.Checked;
        }

        private void chkCheckInDam_CheckedChanged(object sender, EventArgs e)
        {
            uiCheckBox1.Checked = chkCheckInDam.Checked;
            uiCheckBox3.Checked = chkCheckInDam.Checked;
            uiCheckBox5.Checked = chkCheckInDam.Checked;
            uiCheckBox7.Checked = chkCheckInDam.Checked;
            uiCheckBox9.Checked = chkCheckInDam.Checked;
            uiCheckBox11.Checked = chkCheckInDam.Checked;
            uiCheckBox13.Checked = chkCheckInDam.Checked;
            uiCheckBox15.Checked = chkCheckInDam.Checked;
            uiCheckBox17.Checked = chkCheckInDam.Checked;
            uiCheckBox19.Checked = chkCheckInDam.Checked;
            uiCheckBox21.Checked = chkCheckInDam.Checked;
            uiCheckBox23.Checked = chkCheckInDam.Checked;
            uiCheckBox25.Checked = chkCheckInDam.Checked;
            uiCheckBox27.Checked = chkCheckInDam.Checked;
            uiCheckBox29.Checked = chkCheckInDam.Checked;
            uiCheckBox31.Checked = chkCheckInDam.Checked;
            uiCheckBox33.Checked = chkCheckInDam.Checked;
            uiCheckBox35.Checked = chkCheckInDam.Checked;
            uiCheckBox37.Checked = chkCheckInDam.Checked;
            uiCheckBox39.Checked = chkCheckInDam.Checked;

        }

    }
}
