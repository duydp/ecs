﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAHP010 : BasicVNACC
    {

        public List<VAHP010_HANG> HangHoa { get; set; }
        public void LoadVAHP010(string strResult)
        {
            try
            {
                this.GetObject<VAHP010>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAHP010, false, VAHP010.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAHP010.TongSoByte);
                while (true)
                {
                    VAHP010_HANG ivaHang = new VAHP010_HANG();
                    ivaHang.GetObject<VAHP010_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAHP010_HANG", false, VAHP010_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAHP010_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAHP010_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAHP010_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAHP010_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
