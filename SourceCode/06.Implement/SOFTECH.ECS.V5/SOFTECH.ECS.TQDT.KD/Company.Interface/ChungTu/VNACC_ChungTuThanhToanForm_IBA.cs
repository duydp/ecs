﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.Interface
{
    public partial class VNACC_ChungTuThanhToanForm_IBA : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ChungTuThanhToan CTTT = new KDT_VNACC_ChungTuThanhToan();
        public KDT_VNACC_ChungTuThanhToan_ChiTiet CTTT_ChiTiet = new KDT_VNACC_ChungTuThanhToan_ChiTiet();
        private ELoaiThongTin LoaiChungTuThanhToan = ELoaiThongTin.IBA;

        public VNACC_ChungTuThanhToanForm_IBA()
        {
            InitializeComponent();

            this.Load += new EventHandler(VNACC_ChungTuThanhToanForm_IBA_Load);
            cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmbMain_CommandClick);

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EPayment.IBA.ToString());
        }

        void VNACC_ChungTuThanhToanForm_IBA_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SetIDControl();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdKhaiBao":
                    SendVnaccsIDC(false);
                    break;
                case "cmdLayPhanHoi":
                    break;
            }
        }

        private void GetCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                CTTT.MaNganHangTraThay = txtMaNganHangTraThay.Text;
                CTTT.TenNganHangTraThay = txtTenNganHangTraThay.Text;
                CTTT.NamPhatHanh = Convert.ToInt32(txtNamPhatHanh.Value);
                CTTT.KiHieuChungTuPhatHanhHanMuc = txtKiHieuChungTuPhatHanhHanMuc.Text;
                CTTT.SoHieuPhatHanhHanMuc = txtSoHieuPhatHanhHanMuc.Text;
                CTTT.CoBaoVoHieu = txtCoBaoVohieu.Text;
                CTTT.MaDonViSuDungHanMuc = txtMaDonViSuDungHanMuc.Text;
                CTTT.TenDonViSuDungHanMuc = txtTenDonViSuDungHanMuc.Text;
                CTTT.NgayBatDauHieuLuc = dtNgayBatDauHieuLuc.Value;
                CTTT.NgayHetHieuLuc = dtNgayHetHieuLuc.Value;
                CTTT.SoTienHanMucDangKi = Convert.ToDecimal(txtSoTienHanMucDangKi.Value);
                CTTT.SoDuHanMuc = Convert.ToDecimal(txtSoDuHanMuc.Value);
                CTTT.SoThuTuHanMuc = txtSoThuTuHanMuc.Text;
                CTTT.MaTienTe = ucMaTienTe.Code;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNganHangTraThay.Text = CTTT.MaNganHangTraThay;
                txtTenNganHangTraThay.Text = CTTT.TenNganHangTraThay;
                txtNamPhatHanh.Value = CTTT.NamPhatHanh;
                txtKiHieuChungTuPhatHanhHanMuc.Text = CTTT.KiHieuChungTuPhatHanhHanMuc;
                txtSoHieuPhatHanhHanMuc.Text = CTTT.SoHieuPhatHanhHanMuc;
                txtCoBaoVohieu.Text = CTTT.CoBaoVoHieu;
                txtMaDonViSuDungHanMuc.Text = CTTT.MaDonViSuDungHanMuc;
                txtTenDonViSuDungHanMuc.Text = CTTT.TenDonViSuDungHanMuc;
                dtNgayBatDauHieuLuc.Value = CTTT.NgayBatDauHieuLuc;
                dtNgayHetHieuLuc.Value = CTTT.NgayHetHieuLuc;
                txtSoTienHanMucDangKi.Value = CTTT.SoTienDangKyBaoLanh;
                txtSoDuHanMuc.Value = CTTT.SoDuHanMuc;
                txtSoThuTuHanMuc.Text = CTTT.SoThuTuHanMuc;
                ucMaTienTe.Code = CTTT.MaTienTe;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                CTTT_ChiTiet = (KDT_VNACC_ChungTuThanhToan_ChiTiet)items[0].GetRow().DataRow;
                SetHang(CTTT_ChiTiet);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHang(KDT_VNACC_ChungTuThanhToan_ChiTiet hang)
        {
            hang.SoThuTuGiaoDich = Convert.ToInt32(txtSoThuTuGiaoDich.Value);
            hang.NgayTaoDuLieu = dtNgayTaoDuLieu.Value;
            hang.ThoiGianTaoDuLieu = dtThoiGianTaoDuLieu.Value;
            hang.SoTienTruLui = Convert.ToDecimal(txtSoTienTruLui.Value);
            hang.SoTienTangLen = Convert.ToDecimal(txtSoTienTangLen.Value);
            hang.SoToKhai = Convert.ToInt32(txtSoToKhaiChiTiet.Value);
            hang.NgayDangKyToKhai = dtNgayDangKyToKhai.Value;
            hang.MaLoaiHinh = txtMaLoaiHinhChiTiet.Text;
            hang.CoQuanHaiQuan = txtCoQuanhaiQuanChiTiet.Text;
            hang.TenCucHaiQuan = txtTenCucHaiQuanChiTiet.Text;
        }

        private void SetHang(KDT_VNACC_ChungTuThanhToan_ChiTiet hang)
        {
            txtSoThuTuGiaoDich.Value = hang.SoThuTuGiaoDich;
            dtNgayTaoDuLieu.Value = hang.NgayTaoDuLieu;
            dtThoiGianTaoDuLieu.Value = hang.ThoiGianTaoDuLieu;
            txtSoTienTruLui.Value = hang.SoTienTruLui;
            txtSoTienTangLen.Value = hang.SoTienTangLen;
            txtSoToKhaiChiTiet.Value = hang.SoToKhai;
            dtNgayDangKyToKhai.Value = hang.NgayDangKyToKhai;
            txtMaLoaiHinhChiTiet.Text = hang.MaLoaiHinh;
            txtCoQuanhaiQuanChiTiet.Text = hang.CoQuanHaiQuan;
            txtTenCucHaiQuanChiTiet.Text = hang.TenCucHaiQuan;

        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNganHangTraThay.Tag = "BRC"; //Mã ngân hàng trả thay
                txtMaDonViSuDungHanMuc.Tag = "UBP"; //Mã đơn vị sử dụng hạn mức
                txtNamPhatHanh.Tag = "RYA"; //Năm phát hành
                txtKiHieuChungTuPhatHanhHanMuc.Tag = "BPS"; //Kí hiệu chứng từ phát hành hạn mức
                txtSoHieuPhatHanhHanMuc.Tag = "BPN"; //Số hiệu phát hành hạn mức
                ucMaTienTe.TagName = "CCC"; //Mã tiền tệ

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (CTTT.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    CTTT.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    CTTT.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    MessagesSend msg = null; //Form khai báo
                    //IBA saa = VNACCMaperFromObject.IBAMapper(CTTT); //Set Mapper
                    //if (saa == null)
                    //{
                    //    this.ShowMessage("Lỗi khi tạo messages !", false);
                    //    return;
                    //}
                    //msg = MessagesSend.Load<IBA>(saa, CTTT.InputMessageID);

                    MsgLog.SaveMessages(msg, CTTT.ID, EnumThongBao.SendMess, ""); //Lưu thông tin trước khai báo
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = true; //Có nhận phản hồi
                    f.inputMSGID = CTTT.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số chứng từ: " + soTiepNhan;

                                CTTT.SoHieuPhatHanhHanMuc = soTiepNhan.ToString();
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }

                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_ChungTuThanhToan_IBA(msgResult, "", CTTT);
            CTTT.InsertUpdateFull();
            SetCTTT();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (CTTT != null && CTTT.SoHieuPhatHanhHanMuc != "" && CTTT.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", CTTT.SoHieuPhatHanhHanMuc.ToString(), CTTT.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

    }
}
