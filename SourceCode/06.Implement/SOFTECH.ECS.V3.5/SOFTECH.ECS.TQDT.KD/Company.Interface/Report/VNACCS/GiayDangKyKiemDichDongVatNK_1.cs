using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class GiayDangKyKiemDichDongVatNK_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public GiayDangKyKiemDichDongVatNK_1()
        {
            InitializeComponent();
        }
        public void BindingReport(VAJP010 vajp)
        {
            lblMaNguoiKhai.Text = vajp.SBC.GetValue().ToString().ToUpper();
            lblTenNguoiKhai.Text = vajp.SBN.GetValue().ToString().ToUpper();
            lblMaBuuChinhNguoiKhai.Text = vajp.SPC.GetValue().ToString().ToUpper();
            lblDiaChiNguoiKhai.Text = vajp.ADS.GetValue().ToString().ToUpper();
            lblMaNuocNguoiKhai.Text = vajp.SCC.GetValue().ToString().ToUpper();
            lblSoDienThoaiNguoiKhai.Text = vajp.SUN.GetValue().ToString().ToUpper();
            lblSoFaxNguoiKhai.Text = vajp.SFN.GetValue().ToString().ToUpper();
            lblEmailNguoiKhai.Text = vajp.SBE.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.DOS.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vajp.DOS.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            //lblNgayKhaiBao.Text = vajp.DOS.GetValue().ToString().ToUpper();
            lblSoDonXinCapPhep.Text = vajp.ONO.GetValue().ToString().ToUpper();
            lblChucNangChungTu.Text = vajp.FNC.GetValue().ToString().ToUpper();
            lblLoaiGiayPhep.Text = vajp.PRT.GetValue().ToString().ToUpper();
            lblMaDonViCapPhep.Text = vajp.OAC.GetValue().ToString().ToUpper();
            lblTenDonViCapPhep.Text = vajp.OAN.GetValue().ToString().ToUpper();
            lblSoHopDong.Text = vajp.CNO.GetValue().ToString().ToUpper();
            lblToChucCaNhanXK.Text = vajp.EXT.GetValue().ToString().ToUpper();
            lblMaBuuChinhXK.Text = vajp.EPC.GetValue().ToString().ToUpper();
            lblSoNhaTenDuongXK.Text = vajp.EBN.GetValue().ToString().ToUpper();
            lblPhuongXaXK.Text = vajp.ESN.GetValue().ToString().ToUpper();
            lblQuanHuyenXK.Text = vajp.EDN.GetValue().ToString().ToUpper();
            lblTinhThanhXK.Text = vajp.ECN.GetValue().ToString().ToUpper();
            lblMaQuocGiaXK.Text = vajp.ERC.GetValue().ToString().ToUpper();
            lblSoDienthoaiXK.Text = vajp.EPN.GetValue().ToString().ToUpper();
            lblSoFaxXK.Text = vajp.EFX.GetValue().ToString().ToUpper();
            lblEmailXK.Text = vajp.EEM.GetValue().ToString().ToUpper();
            lblNuocXuatKhau.Text = vajp.ECC.GetValue().ToString().ToUpper();
            lblMaToChucCaNhanXK.Text = vajp.IMC.GetValue().ToString().ToUpper();
            lblTenToChucCaNhanXK.Text = vajp.IMN.GetValue().ToString().ToUpper();
            lblSoDangKyKD.Text = vajp.BRN.GetValue().ToString().ToUpper();
            lblMaBuuChinhNK.Text = vajp.BPC.GetValue().ToString().ToUpper();
            lblDiaChiNK.Text = vajp.AOI.GetValue().ToString().ToUpper();
            lblMaQuocGiaNK.Text = vajp.BCC.GetValue().ToString().ToUpper();
            lblSoDienThoaiNK.Text = vajp.BPN.GetValue().ToString().ToUpper();
            lblSoFaxNK.Text = vajp.BFX.GetValue().ToString().ToUpper();
            lblEmailNK.Text = vajp.BEM.GetValue().ToString().ToUpper();
            lblNuocQuaCanh.Text = vajp.TCC.GetValue().ToString().ToUpper();
            lblMaPhuongTienVC.Text = vajp.MOT.GetValue().ToString().ToUpper();
            lblTenPhuongTienVC.Text = vajp.MTN.GetValue().ToString().ToUpper();
            lblMaCuaKhauNhap.Text = vajp.IBC.GetValue().ToString().ToUpper();
            lblCuaKhauNhap.Text = vajp.IBG.GetValue().ToString().ToUpper();
            lblSoGiayChungNhan.Text = vajp.PRN.GetValue().ToString().ToUpper();
            lblDiaDiemKiemDich.Text = vajp.QUL.GetValue().ToString().ToUpper();
            lblBenhMienDich.Text = vajp.VAN.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.DOV.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayTiemPhong.Text = Convert.ToDateTime(vajp.DOV.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayTiemPhong.Text = "";
            }
            lblKhuTrung.Text = vajp.ANT.GetValue().ToString().ToUpper();
            lblNongDo.Text = vajp.CON.GetValue().ToString().ToUpper();
            lblDiaDiemNuoiTrong.Text = vajp.PRC.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.QUT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKiemDich.Text = Convert.ToDateTime(vajp.QUT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKiemDich.Text = "";
            }
            if (vajp.QUH.GetValue().ToString().ToUpper() != "" && vajp.QUH.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiGianKiemDich.Text = vajp.QUH.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vajp.QUH.GetValue().ToString().ToUpper().Substring(2, 2) ;
            }
            else
            {
                lblThoiGianKiemDich.Text = "";
            }
            lblDiaDiemGiamSat.Text = vajp.CTL.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vajp.CTT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayGiamSat.Text = Convert.ToDateTime(vajp.CTT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayGiamSat.Text = "";
            }
            if (vajp.CTH.GetValue().ToString().ToUpper() != "" && vajp.CTH.GetValue().ToString().ToUpper() != "0")
            {
                lblThoiGianGiamSat.Text = vajp.CTH.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vajp.CTH.GetValue().ToString().ToUpper().Substring(2, 2);
            }
            else
            {
                lblThoiGianGiamSat.Text = "";
            }
            lblSoBanCanCap.Text = vajp.NOC.GetValue().ToString().ToUpper();
            lblVatDungKhac.Text = vajp.ORE.GetValue().ToString().ToUpper();
            lblHoSoLienQuan.Text = vajp.AD.GetValue().ToString().ToUpper();
            lblNoiChuyenDen.Text = vajp.AVL.GetValue().ToString().ToUpper();
            lblNguoiKiemTra.Text = vajp.TET.GetValue().ToString().ToUpper();
            lblKetQuaKiemTra.Text = vajp.TRS.GetValue().ToString().ToUpper();


            int HangHoa = vajp.HangHoa.Count;
            if (HangHoa <= 5)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 5, 0, MidpointRounding.AwayFromZero)).ToString();
            }
           

        }



    }
}
