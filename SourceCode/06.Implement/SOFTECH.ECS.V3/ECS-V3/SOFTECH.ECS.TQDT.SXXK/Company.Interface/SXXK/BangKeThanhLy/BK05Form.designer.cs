namespace Company.Interface.SXXK.BangKe
{
    partial class BK05Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout gridEX2_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK05Form));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.gridEX2 = new Janus.Windows.GridEX.GridEX();
            this.dgList = new Janus.Windows.EditControls.UIGroupBox();
            this.txtLoaiHinhXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamDKNhap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamDKXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cmdAdd = new Janus.Windows.EditControls.UIButton();
            this.txtSoLuongXuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtLoaiHinhNhap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtToKhaiNhap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtToKhaiXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvToKhaiXuat = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmdSave = new Janus.Windows.EditControls.UIButton();
            this.rfvToKhaiNhap = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.dgList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiNhap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.cmdSave);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Size = new System.Drawing.Size(651, 384);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(12, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(225, 14);
            this.label5.TabIndex = 1;
            this.label5.Text = "Danh sách nguyên phụ liệu tái xuất";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.gridEX2);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(11, 190);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(628, 157);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // gridEX2
            // 
            this.gridEX2.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX2.AlternatingColors = true;
            this.gridEX2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridEX2.AutomaticSort = false;
            this.gridEX2.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridEX2.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            gridEX2_DesignTimeLayout.LayoutString = resources.GetString("gridEX2_DesignTimeLayout.LayoutString");
            this.gridEX2.DesignTimeLayout = gridEX2_DesignTimeLayout;
            this.gridEX2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX2.GroupByBoxVisible = false;
            this.gridEX2.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX2.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX2.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX2.ImageList = this.ImageList1;
            this.gridEX2.Location = new System.Drawing.Point(2, 8);
            this.gridEX2.Name = "gridEX2";
            this.gridEX2.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX2.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridEX2.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX2.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.gridEX2.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridEX2.Size = new System.Drawing.Size(625, 147);
            this.gridEX2.TabIndex = 0;
            this.gridEX2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridEX2.VisualStyleManager = this.vsmMain;
            this.gridEX2.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.gridEX2_LoadingRow);
            // 
            // dgList
            // 
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BackColor = System.Drawing.Color.Transparent;
            this.dgList.Controls.Add(this.txtLoaiHinhXuat);
            this.dgList.Controls.Add(this.txtNamDKNhap);
            this.dgList.Controls.Add(this.txtNamDKXuat);
            this.dgList.Controls.Add(this.cmdAdd);
            this.dgList.Controls.Add(this.txtSoLuongXuat);
            this.dgList.Controls.Add(this.label14);
            this.dgList.Controls.Add(this.label12);
            this.dgList.Controls.Add(this.label11);
            this.dgList.Controls.Add(this.label10);
            this.dgList.Controls.Add(this.label9);
            this.dgList.Controls.Add(this.txtLoaiHinhNhap);
            this.dgList.Controls.Add(this.txtTenNPL);
            this.dgList.Controls.Add(this.label8);
            this.dgList.Controls.Add(this.txtToKhaiNhap);
            this.dgList.Controls.Add(this.txtMaNPL);
            this.dgList.Controls.Add(this.label7);
            this.dgList.Controls.Add(this.txtToKhaiXuat);
            this.dgList.Controls.Add(this.label4);
            this.dgList.Controls.Add(this.label6);
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgList.Location = new System.Drawing.Point(11, 12);
            this.dgList.Name = "dgList";
            this.dgList.Size = new System.Drawing.Size(627, 147);
            this.dgList.TabIndex = 0;
            this.dgList.Text = "Thông tin NPL tái xuất";
            this.dgList.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // txtLoaiHinhXuat
            // 
            this.txtLoaiHinhXuat.Location = new System.Drawing.Point(295, 23);
            this.txtLoaiHinhXuat.Name = "txtLoaiHinhXuat";
            this.txtLoaiHinhXuat.ReadOnly = true;
            this.txtLoaiHinhXuat.Size = new System.Drawing.Size(100, 21);
            this.txtLoaiHinhXuat.TabIndex = 3;
            this.txtLoaiHinhXuat.VisualStyleManager = this.vsmMain;
            // 
            // txtNamDKNhap
            // 
            this.txtNamDKNhap.Location = new System.Drawing.Point(491, 78);
            this.txtNamDKNhap.Name = "txtNamDKNhap";
            this.txtNamDKNhap.ReadOnly = true;
            this.txtNamDKNhap.Size = new System.Drawing.Size(84, 21);
            this.txtNamDKNhap.TabIndex = 15;
            this.txtNamDKNhap.VisualStyleManager = this.vsmMain;
            // 
            // txtNamDKXuat
            // 
            this.txtNamDKXuat.Location = new System.Drawing.Point(491, 24);
            this.txtNamDKXuat.Name = "txtNamDKXuat";
            this.txtNamDKXuat.ReadOnly = true;
            this.txtNamDKXuat.Size = new System.Drawing.Size(84, 21);
            this.txtNamDKXuat.TabIndex = 5;
            this.txtNamDKXuat.VisualStyleManager = this.vsmMain;
            this.txtNamDKXuat.TextChanged += new System.EventHandler(this.txtNamDKXuat_TextChanged);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAdd.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAdd.Icon")));
            this.cmdAdd.Location = new System.Drawing.Point(363, 107);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(81, 23);
            this.cmdAdd.TabIndex = 18;
            this.cmdAdd.Text = "Thêm";
            this.cmdAdd.VisualStyleManager = this.vsmMain;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // txtSoLuongXuat
            // 
            this.txtSoLuongXuat.DecimalDigits = 3;
            this.txtSoLuongXuat.Location = new System.Drawing.Point(178, 108);
            this.txtSoLuongXuat.Name = "txtSoLuongXuat";
            this.txtSoLuongXuat.Size = new System.Drawing.Size(176, 21);
            this.txtSoLuongXuat.TabIndex = 17;
            this.txtSoLuongXuat.Text = "0.000";
            this.txtSoLuongXuat.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoLuongXuat.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(68, 113);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Lượng tái xuất";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(232, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Tên NPL";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(405, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Năm đăng ký";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(405, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Năm đăng ký";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(232, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Loại hình";
            // 
            // txtLoaiHinhNhap
            // 
            this.txtLoaiHinhNhap.Location = new System.Drawing.Point(295, 78);
            this.txtLoaiHinhNhap.Name = "txtLoaiHinhNhap";
            this.txtLoaiHinhNhap.ReadOnly = true;
            this.txtLoaiHinhNhap.Size = new System.Drawing.Size(100, 21);
            this.txtLoaiHinhNhap.TabIndex = 13;
            this.txtLoaiHinhNhap.VisualStyleManager = this.vsmMain;
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.Location = new System.Drawing.Point(295, 51);
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.ReadOnly = true;
            this.txtTenNPL.Size = new System.Drawing.Size(280, 21);
            this.txtTenNPL.TabIndex = 9;
            this.txtTenNPL.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(232, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Loại hình";
            // 
            // txtToKhaiNhap
            // 
            this.txtToKhaiNhap.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtToKhaiNhap.Location = new System.Drawing.Point(112, 78);
            this.txtToKhaiNhap.Name = "txtToKhaiNhap";
            this.txtToKhaiNhap.ReadOnly = true;
            this.txtToKhaiNhap.Size = new System.Drawing.Size(100, 21);
            this.txtToKhaiNhap.TabIndex = 11;
            this.txtToKhaiNhap.VisualStyleManager = this.vsmMain;
            this.txtToKhaiNhap.ButtonClick += new System.EventHandler(this.txtToKhaiNhap_ButtonClick);
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.Location = new System.Drawing.Point(112, 51);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.ReadOnly = true;
            this.txtMaNPL.Size = new System.Drawing.Size(100, 21);
            this.txtMaNPL.TabIndex = 7;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Tờ khai nhập";
            // 
            // txtToKhaiXuat
            // 
            this.txtToKhaiXuat.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtToKhaiXuat.Location = new System.Drawing.Point(112, 24);
            this.txtToKhaiXuat.Name = "txtToKhaiXuat";
            this.txtToKhaiXuat.ReadOnly = true;
            this.txtToKhaiXuat.Size = new System.Drawing.Size(100, 21);
            this.txtToKhaiXuat.TabIndex = 1;
            this.txtToKhaiXuat.VisualStyleManager = this.vsmMain;
            this.txtToKhaiXuat.ButtonClick += new System.EventHandler(this.txtToKhaiXuat_ButtonClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "NPL tái xuất";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tờ khai xuất";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.dgList;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvToKhaiXuat
            // 
            this.rfvToKhaiXuat.ControlToValidate = this.txtToKhaiXuat;
            this.rfvToKhaiXuat.ErrorMessage = "\"Tờ khai xuất\" không được để trống.";
            this.rfvToKhaiXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvToKhaiXuat.Icon")));
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(562, 354);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Location = new System.Drawing.Point(440, 354);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(116, 23);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Lưu bảng kê";
            this.cmdSave.VisualStyleManager = this.vsmMain;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // rfvToKhaiNhap
            // 
            this.rfvToKhaiNhap.ControlToValidate = this.txtToKhaiNhap;
            this.rfvToKhaiNhap.ErrorMessage = "\"Tờ khai nhập\" không được để trống.";
            this.rfvToKhaiNhap.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvToKhaiNhap.Icon")));
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToValidate = this.txtSoLuongXuat;
            this.cvSoLuong.ErrorMessage = "\"Lượng tái xuất\" không hợp lệ.";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // BK05Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(651, 384);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK05Form";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Bảng kê nguyên phụ liệu tái xuất";
            this.Load += new System.EventHandler(this.BK05Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.dgList.ResumeLayout(false);
            this.dgList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvToKhaiNhap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX gridEX2;
        private Janus.Windows.EditControls.UIGroupBox dgList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtToKhaiNhap;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtToKhaiXuat;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiHinhNhap;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiHinhXuat;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongXuat;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIButton cmdAdd;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvToKhaiXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDKNhap;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDKXuat;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton cmdSave;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvToKhaiNhap;
        private Company.Controls.CustomValidation.CompareValidator cvSoLuong;
    }
}